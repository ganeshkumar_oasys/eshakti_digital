# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /home/user1/Android/Sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
# for gson (GSON @Expose annotation)


-keep class com.oasys.eshakti.Dto** { <fields>; }


-dontwarn android.support.v7.**
-keep class android.support.v7.** { *; }
-keep interface android.support.v7.** { *; }

-dontwarn java.nio.file.Files
-dontwarn java.nio.file.Path
-dontwarn java.nio.file.OpenOption
-dontwarn com.squareup.**
-dontwarn okio.**
-keep public class org.codehaus.**
-keep public class java.nio.*
-dontwarn org.apache.lang.**
-dontwarn okio.**
-dontwarn org.apache.lang.**

-dontwarn javax.servlet.**
-dontwarn org.w3c.dom.**


-keep public class com.google.android.gms.**
-dontwarn com.google.android.gms.**
-dontwarn org.apache.http.**
-dontwarn android.net.http.AndroidHttpClient
-dontwarn com.google.android.gms.**
-dontwarn com.android.volley.toolbox.**
-dontwarn org.w3c.dom.bootstrap.DOMImplementationRegistry
-dontwarn org.xmlpull.v1.**

-keep class *{
public private *;
}
-keepattributes Signature
-keepattributes *Annotation*
-keep class org.projectlombok.lombok.** { *; }
-dontwarn lombok.**

-dontwarn org.simpleframework.xml.stream.**
-keep class org.simpleframework.xml.**{ *; }
-keepclassmembers,allowobfuscation class * {
    @org.simpleframework.xml.* <fields>;
    @org.simpleframework.xml.* <init>(...);
}

-ignorewarnings
-keep class org.kobjects.** { *; }
-keep class org.ksoap2.** { *; }
-keep class org.kxml2.** { *; }
-keep class org.xmlpull.** { *; }

-dontskipnonpubliclibraryclasses
-dontobfuscate
-forceprocessing
-optimizationpasses 3

-keep class * extends android.app.Activity

-assumenosideeffects class android.util.Log {
  public static *** v(...);
  public static *** d(...);
  public static *** i(...);
  public static *** w(...);
  public static *** e(...);
}
-assumenosideeffects class java.io.PrintStream {
     public void println(%);
     public void println(**);
}


