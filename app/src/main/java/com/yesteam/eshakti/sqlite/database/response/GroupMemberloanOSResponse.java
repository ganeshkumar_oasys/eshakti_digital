package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class GroupMemberloanOSResponse extends BaseResponse {

	public GroupMemberloanOSResponse(final TransactionResult transactionResult) {
		super(transactionResult);
	}

	public GroupMemberloanOSResponse(final boolean result) {
		super(TransactionResult.SUCCESS);
	}

}
