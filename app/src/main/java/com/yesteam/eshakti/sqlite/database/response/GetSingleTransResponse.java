package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class GetSingleTransResponse extends BaseResponse {

	public GetSingleTransResponse(final TransactionResult transactionResult) {
		super(transactionResult);
	}

	public GetSingleTransResponse(final boolean result) {
		super(TransactionResult.SUCCESS);
	}

}