package com.yesteam.eshakti.sqlite.db.model;

import java.io.Serializable;

public class GroupMemberDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final int localId;
	private final String mTrainerUserId;
	private final String mGroupId;
	private final String mGroupresponse;
	private final String mPersonalloanOS;
	private final String mMemberloanOS;
	private final String mGrouploanOS;
	private final String mFixeddepositOS;

	public GroupMemberDetails(final int localId,
			final String grp_traineruserid, final String grp_groupid,
			final String grp_grpresponse, final String grp_personalloanos,
			final String grp_memberloanos, final String grp_grouploanos,
			final String grp_fixeddepositos) {
		this.localId = localId;
		this.mTrainerUserId = grp_traineruserid;
		this.mGroupId = grp_groupid;
		this.mGroupresponse = grp_grpresponse;
		this.mPersonalloanOS = grp_personalloanos;
		this.mMemberloanOS = grp_memberloanos;
		this.mGrouploanOS = grp_grouploanos;
		this.mFixeddepositOS = grp_fixeddepositos;

	}

	public int getLocalId() {
		return localId;
	}

	public String getTrainerUserId() {
		return mTrainerUserId;
	}

	public String getGroupId() {
		return mGroupId;
	}

	public String getGroupresponse() {
		return mGroupresponse;
	}

	public String getPersonaloanOS() {
		return mPersonalloanOS;
	}

	public String getMemberloanOS() {
		return mMemberloanOS;
	}

	public String getGrouploanOS() {
		return mGrouploanOS;
	}

	public String getFixeddepositOS() {
		return mFixeddepositOS;
	}

}
