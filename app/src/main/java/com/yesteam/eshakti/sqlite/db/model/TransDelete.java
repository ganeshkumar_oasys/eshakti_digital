package com.yesteam.eshakti.sqlite.db.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class TransDelete implements Serializable {

	private static String mUniqueId_delete;

	public TransDelete(final String grp_uniqueid) {

		TransDelete.mUniqueId_delete = grp_uniqueid;

	}

	public static String getUniqueId() {
		return mUniqueId_delete;
	}
}