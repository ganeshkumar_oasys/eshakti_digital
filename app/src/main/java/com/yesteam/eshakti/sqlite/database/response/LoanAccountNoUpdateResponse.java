package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class LoanAccountNoUpdateResponse extends BaseResponse {

	public LoanAccountNoUpdateResponse(final TransactionResult transactionResult) {
		super(transactionResult);
	}

	public LoanAccountNoUpdateResponse(final boolean result) {
		super(TransactionResult.SUCCESS);
	}
}
