package com.yesteam.eshakti.sqlite.db.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class GroupMaster implements Serializable {

	private final int localId;
	private final String mTrainerUserId_master;
	private final String mGroupId_master;
	private final String mGroupMasterresponse_master;
	private final String mPersonalloanOS_master;
	private final String mMemberloanOS_master;
	private final String mGrouploanOS_master;
	private final String mFixeddepositOS_master;
	private final String mAgentprofile;
	private final String mGroupprofile;
	private final String mPurposeofloan;
	private final String mMasterminutes;
	private final String mIndividualgroupminutes;
	private final String mGrouptransId;
	private final String mLoanAccBankDetails;
	private final String mBankAccNoDetails;
	private final String mLoanOSDetails;

	public GroupMaster(final int localID, final String mas_traineruserid, final String mas_groupid,
			final String mas_groupresponse, final String mas_personalloanas, final String mas_memberloanos,
			final String mas_grouploanos, final String mas_fixeddepositos, final String mas_agentprofile,
			final String mas_groupprofile, final String mas_purposeofloan, final String mas_masterminutes,
			final String mas_individualgrpminutes, final String mas_grouptransid, final String mas_loanaccBankdetails,
			final String mas_BankAccNoDetails, final String mas_LoanOSDetails) {
		this.localId = localID;
		this.mTrainerUserId_master = mas_traineruserid;
		this.mGroupId_master = mas_groupid;
		this.mGroupMasterresponse_master = mas_groupresponse;
		this.mPersonalloanOS_master = mas_personalloanas;
		this.mMemberloanOS_master = mas_memberloanos;
		this.mGrouploanOS_master = mas_grouploanos;
		this.mFixeddepositOS_master = mas_fixeddepositos;
		this.mAgentprofile = mas_agentprofile;
		this.mGroupprofile = mas_groupprofile;
		this.mPurposeofloan = mas_purposeofloan;
		this.mMasterminutes = mas_masterminutes;
		this.mIndividualgroupminutes = mas_individualgrpminutes;
		this.mGrouptransId = mas_grouptransid;
		this.mLoanAccBankDetails = mas_loanaccBankdetails;
		this.mBankAccNoDetails = mas_BankAccNoDetails;
		this.mLoanOSDetails = mas_LoanOSDetails;
	}

	public int getLocalId() {
		return localId;
	}

	public String getTrainerUserId_master() {
		return mTrainerUserId_master;
	}

	public String getGroupId_master() {
		return mGroupId_master;
	}

	public String getGroupMasterresponse_master() {
		return mGroupMasterresponse_master;
	}

	public String getPersonalloanOS_master() {
		return mPersonalloanOS_master;
	}

	public String getMemberloanOS_master() {
		return mMemberloanOS_master;
	}

	public String getGrouploanOS_master() {
		return mGrouploanOS_master;
	}

	public String getFixeddepositOS_master() {
		return mFixeddepositOS_master;
	}

	public String getAgentprofile() {
		return mAgentprofile;
	}

	public String getGroupprofile() {
		return mGroupprofile;
	}

	public String getPurposeofloan() {
		return mPurposeofloan;
	}

	public String getMasterminutes() {
		return mMasterminutes;
	}

	public String getIndividualgroupminutes() {
		return mIndividualgroupminutes;
	}

	public String getGrouptransId() {
		return mGrouptransId;
	}

	public String getLoanAccBankDetails() {
		return mLoanAccBankDetails;
	}

	public String getBankAccNoDetails() {
		return mBankAccNoDetails;
	}

	public String getLoanOSDetails() {
		return mLoanOSDetails;
	}

}
