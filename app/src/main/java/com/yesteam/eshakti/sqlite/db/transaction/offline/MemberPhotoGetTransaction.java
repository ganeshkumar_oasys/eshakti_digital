package com.yesteam.eshakti.sqlite.db.transaction.offline;

import java.util.List;

import com.yesteam.eshakti.params.NoParams;
import com.yesteam.eshakti.sqlite.database.MemberPhotoProvider;
import com.yesteam.eshakti.sqlite.db.model.MemberPhotoMaster;
import com.yesteam.eshakti.sqlite.db.transactions.AbstractTransaction;

public class MemberPhotoGetTransaction extends AbstractTransaction<NoParams, List<MemberPhotoMaster>> {

	@Override
	public int onPreExecute() {
		return EXECUTE_IN_WORKER_THREAD;
	}

	@Override
	public void run() {
		deliverResponse(MemberPhotoProvider.getImageDetails());
	}
}
