package com.yesteam.eshakti.sqlite.db.transaction.offline;

import com.yesteam.eshakti.sqlite.database.LoginProvider;
import com.yesteam.eshakti.sqlite.database.response.LoginResponse;
import com.yesteam.eshakti.sqlite.db.model.Login;
import com.yesteam.eshakti.sqlite.db.transactions.AbstractTransaction;

public class LoginAddTransaction extends
		AbstractTransaction<Login, LoginResponse> {

	@Override
	public int onPreExecute() {
		return EXECUTE_IN_WORKER_THREAD;
	}

	@Override
	public void run() {
		boolean value = LoginProvider.addLoginDetails(getParams());
		LoginResponse transResponse = new LoginResponse(value);
		deliverResponse(transResponse);
	}
}