package com.yesteam.eshakti.sqlite.db.transaction.offline;

import com.yesteam.eshakti.sqlite.database.GroupDetailsProvider;
import com.yesteam.eshakti.sqlite.database.response.GroupMasterPLDB_FD_Response;
import com.yesteam.eshakti.sqlite.db.model.GroupMasterSingle;
import com.yesteam.eshakti.sqlite.db.transactions.AbstractTransaction;

public class GroupMasterPLDB_FD_Transaction extends
		AbstractTransaction<GroupMasterSingle, GroupMasterPLDB_FD_Response> {

	@Override
	public int onPreExecute() {
		return EXECUTE_IN_WORKER_THREAD;
	}

	@Override
	public void run() {

		boolean value = GroupDetailsProvider.getSinlgeGroupMaster(getParams());
		GroupMasterPLDB_FD_Response transResponse = new GroupMasterPLDB_FD_Response(
				value);
		deliverResponse(transResponse);

	}

}