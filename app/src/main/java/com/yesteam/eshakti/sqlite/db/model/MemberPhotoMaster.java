package com.yesteam.eshakti.sqlite.db.model;

import java.io.Serializable;

public class MemberPhotoMaster implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6703269253867253450L;
	private final int localId;
	private final String mUniqueid;
	private final String mUsername;
	private final String mNgoId;
	private final String mGroupId;
	private final String mMemberId;
	private final byte[] image;
	private final String mUpdateValues;

	public MemberPhotoMaster(final int localID, final String uniqueid, final String username, final String ngoid,
			final String groupid, final String memberid, final byte[] masterimage, final String updatevalues) {
		this.localId = localID;
		this.mUniqueid = uniqueid;
		this.mUsername = username;
		this.mNgoId = ngoid;
		this.mGroupId = groupid;
		this.mMemberId = memberid;
		this.image = masterimage;
		this.mUpdateValues = updatevalues;

	}

	public int getLocalId() {
		return localId;
	}

	public String getUniqueid() {
		return mUniqueid;
	}

	public String getUsername() {
		return mUsername;
	}

	public String getNgoId() {
		return mNgoId;
	}

	public String getGroupId() {
		return mGroupId;
	}

	public String getMemberId() {
		return mMemberId;
	}

	public byte[] getImage() {
		return image;
	}

	public String getUpdateValues() {
		return mUpdateValues;
	}
}
