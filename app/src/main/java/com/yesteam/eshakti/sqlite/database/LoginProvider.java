package com.yesteam.eshakti.sqlite.database;

import java.util.ArrayList;
import java.util.List;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.yesteam.eshakti.sqlite.db.model.Login;
import com.yesteam.eshakti.sqlite.db.model.LoginCheck;
import com.yesteam.eshakti.sqlite.db.model.LoginUpdate;
import com.yesteam.eshakti.webservices.LoginTask;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class LoginProvider {
	private static SQLiteDatabase sqLiteDatabase;
	private static DataHelper dataHelper;
	static String password, trainerid_groupID;
	static int localId = -1;

	public LoginProvider(Activity activity) {
		dataHelper = new DataHelper(activity);
	}

	public static void openDatabase() {
		dataHelper = new DataHelper(EShaktiApplication.getInstance());
		dataHelper.onOpen(sqLiteDatabase);
		sqLiteDatabase = dataHelper.getWritableDatabase();
	}

	public static void closeDatabase() {
		dataHelper.close();
	}

	public interface LoginColumn {
		public static final String L_ID = "local_Id";
		public static final String TRAINERUSERID = "Traineruserid";
		public static final String NGOID = "Ngoid";
		public static final String TRAINERID = "Trainerid";
		public static final String PASSWORD = "Password";
		public static final String LANGUAGE = "Language";

	}

	public static final String CREATE_LOGIN_TABLE = "create table " + DataHelper.TABLES.TABLE_LOGIN + "("
			+ LoginColumn.L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + LoginColumn.TRAINERUSERID + " TEXT,"
			+ LoginColumn.NGOID + " text," + LoginColumn.TRAINERID + " text," + LoginColumn.PASSWORD + " text,"
			+ LoginColumn.LANGUAGE + " text" + ")";

	public static boolean addLoginDetails(Login login) {
		try {
			openDatabase();

			ContentValues contentValues = new ContentValues();
			contentValues.put(LoginColumn.TRAINERUSERID, login.getTrainerUserId());
			contentValues.put(LoginColumn.NGOID, login.getNgoId());
			contentValues.put(LoginColumn.TRAINERID, login.getTrainerId());
			contentValues.put(LoginColumn.PASSWORD, login.getPassword());
			contentValues.put(LoginColumn.LANGUAGE, login.getLanguage());
			long result = sqLiteDatabase.insert(DataHelper.TABLES.TABLE_LOGIN, null, contentValues);

			Log.v("Login Insert Sucessfully", "Yes Working@@@!!!!!");
			return result != -1;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			closeDatabase();
		}
		return false;
	}

	public static List<Login> getLoginDetails() {
		try {
			openDatabase();
			final List<Login> logindetails = new ArrayList<Login>();
			Cursor cursor = sqLiteDatabase.query(DataHelper.TABLES.TABLE_LOGIN, null, null, null, null, null, null);
			String log_password = null;
			if (cursor.moveToFirst()) {
				String log_trainerUserid = null;
				String log_ngoId = null;
				String log_trainerId = null;

				String log_language = null;

				localId = -1;
				do {
					localId = cursor.getInt(cursor.getColumnIndex(LoginColumn.L_ID));
					log_trainerUserid = cursor.getString(cursor.getColumnIndex(LoginColumn.TRAINERUSERID));

					log_ngoId = cursor.getString(cursor.getColumnIndex(LoginColumn.NGOID));
					log_trainerId = cursor.getString(cursor.getColumnIndex(LoginColumn.TRAINERID));
					log_password = cursor.getString(cursor.getColumnIndex(LoginColumn.PASSWORD));
					log_language = cursor.getString(cursor.getColumnIndex(LoginColumn.LANGUAGE));

					logindetails.add(new Login(localId, log_trainerUserid, log_ngoId, log_trainerId, log_password,
							log_language));
				} while (cursor.moveToNext());
			}
			Log.e("Login Cursor Count", cursor.getCount() + "");
			Log.e("Login Password", log_password);
			cursor.close();
			return logindetails;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

			closeDatabase();
		}
		return null;
	}

	public static boolean getSinlgeEntry(LoginCheck username) {
		try {
			openDatabase();
			Cursor cursor = sqLiteDatabase.query(DataHelper.TABLES.TABLE_LOGIN, null, LoginColumn.TRAINERUSERID + " =?",
					new String[] { LoginCheck.getUsername() }, null, null, null);
			String mNgoId = null;
			String mUserLang = null;
			if (cursor.getCount() < 1) // UserName Not Exist
			{

				cursor.close();
				// return "NOT EXIST";
				LoginCheck.setUserAvabl(false);
				return false;
			}
			cursor.moveToFirst();
			password = cursor.getString(cursor.getColumnIndex(LoginColumn.PASSWORD));
			trainerid_groupID = cursor.getString(cursor.getColumnIndex(LoginColumn.TRAINERID));
			mNgoId = cursor.getString(cursor.getColumnIndex(LoginColumn.NGOID));
			mUserLang = cursor.getString(cursor.getColumnIndex(LoginColumn.LANGUAGE));
			new LoginCheck(password);
			LoginCheck.setUserAvabl(true);
			LoginCheck.setTrainerId(trainerid_groupID);
			LoginCheck.setUserNgoId(mNgoId);
			LoginTask.User_RegLanguage = mUserLang;

			cursor.close();
			Log.e("Username and Password is", "Right" + password);
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

			closeDatabase();
		}
		return true;

	}

	public static boolean updateEntry(LoginUpdate loginUpdate) {

		try {
			openDatabase();
			// Define the updated row content.
			ContentValues updatedValues = new ContentValues();
			// Assign values for each row.
			updatedValues.put(LoginColumn.TRAINERUSERID, LoginUpdate.getUsername());
			// updatedValues.put("PASSWORD", password);

			sqLiteDatabase.update(DataHelper.TABLES.TABLE_LOGIN, updatedValues, LoginColumn.TRAINERID + "=" + 2, null);

			return false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

			closeDatabase();
		}
		return true;
	}

	public static void deleteLogin() {

		try {
			openDatabase();

			Log.e("Yes Delete Executed", "Yes Working");

			sqLiteDatabase.delete(DataHelper.TABLES.TABLE_LOGIN, null, null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

			closeDatabase();
		}
	}

}
