package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class GetMinutesofMeetingResponse extends BaseResponse {

	public GetMinutesofMeetingResponse(final TransactionResult transactionResult) {
		super(transactionResult);
	}

	public GetMinutesofMeetingResponse(final boolean result) {
		super(TransactionResult.SUCCESS);
	}
}
