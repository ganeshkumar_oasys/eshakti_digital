package com.yesteam.eshakti.sqlite.db.transaction.offline;

import java.util.List;

import com.yesteam.eshakti.params.NoParams;
import com.yesteam.eshakti.sqlite.database.GroupProvider;
import com.yesteam.eshakti.sqlite.db.model.Groupdetails;
import com.yesteam.eshakti.sqlite.db.transactions.AbstractTransaction;

public class GroupGetTransaction extends AbstractTransaction<NoParams, List<Groupdetails>> {

	@Override
	public int onPreExecute() {
		return EXECUTE_IN_WORKER_THREAD;
	}

	@Override
	public void run() {
		deliverResponse(GroupProvider.getGroupDetails());
	}
}
