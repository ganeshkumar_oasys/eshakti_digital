package com.yesteam.eshakti.sqlite.db.transactions;

import android.os.Handler;

import com.android.volley.Request;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;

public interface ITransaction<P, T> extends Listener<T>, ErrorListener, Runnable {

	public static final int EXECUTE_IN_VOLLEY_THREAD = 1;
	public static final int EXECUTE_IN_WORKER_THREAD = 2;
	public static final int EXECUTE_IN_MAIN_THREAD = 3;

	public int onPreExecute();

	public Request<T> getVolleyRequest();

	public void setParams(final P param);

	public void setResponseHandler(final Handler responseHandler);
}
