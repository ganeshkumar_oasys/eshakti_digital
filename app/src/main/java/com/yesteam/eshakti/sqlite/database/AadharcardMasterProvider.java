package com.yesteam.eshakti.sqlite.database;

import java.util.ArrayList;
import java.util.List;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.yesteam.eshakti.sqlite.db.model.AadharCardMaster;
import com.yesteam.eshakti.sqlite.db.model.AadharCardMasterUpdate;
import com.yesteam.eshakti.sqlite.db.model.AadharcardDelete;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class AadharcardMasterProvider {
	private static SQLiteDatabase sqLiteDatabase;
	private static DataHelper dataHelper;
	static Cursor cursor;
	public static List<AadharCardMaster> aadharCardMasters = new ArrayList<AadharCardMaster>();

	public AadharcardMasterProvider(Activity activity) {
		dataHelper = new DataHelper(activity);
	}

	public static void openDatabase() {
		dataHelper = new DataHelper(EShaktiApplication.getInstance());
		dataHelper.onOpen(sqLiteDatabase);
		sqLiteDatabase = dataHelper.getWritableDatabase();

	}

	public static void closeDatabase() {
		dataHelper.close();
	}

	public interface AadharCardMasterColumn {
		public static final String A_ID = "local_Id";
		public static final String UNIQUEID = "Uniqueid";
		public static final String USERNAME = "Username";
		public static final String NGOID = "NgoId";
		public static final String GROUPID = "Groupid";
		public static final String MEMBERID = "MemberId";
		public static final String MASTERVALUES = "MasterValues";
		public static final String UPDATEVALUES = "UpdateValues";

	}

	public static final String CREATE_AADHARCARD_DETAILS_TABLE = "create table "
			+ DataHelper.TABLES.TABLE_AADHARCARD_MASTER + "(" + AadharCardMasterColumn.A_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + AadharCardMasterColumn.UNIQUEID + " text,"
			+ AadharCardMasterColumn.USERNAME + " TEXT," + AadharCardMasterColumn.NGOID + " text,"
			+ AadharCardMasterColumn.GROUPID + " text," + AadharCardMasterColumn.MEMBERID + " text,"
			+ AadharCardMasterColumn.MASTERVALUES + " text," + AadharCardMasterColumn.UPDATEVALUES + " text" + ")";

	public static boolean addAadharcardDetails(AadharCardMaster aadharCardMaster) {
		try {
			openDatabase();
			ContentValues contentValues = new ContentValues();
			contentValues.put(AadharCardMasterColumn.UNIQUEID, aadharCardMaster.getUniqueid());
			contentValues.put(AadharCardMasterColumn.USERNAME, aadharCardMaster.getUsername());
			contentValues.put(AadharCardMasterColumn.NGOID, aadharCardMaster.getNgoId());
			contentValues.put(AadharCardMasterColumn.GROUPID, aadharCardMaster.getGroupId());
			contentValues.put(AadharCardMasterColumn.MEMBERID, aadharCardMaster.getMemberId());

			contentValues.put(AadharCardMasterColumn.MASTERVALUES, aadharCardMaster.getMasterValues());

			contentValues.put(AadharCardMasterColumn.UPDATEVALUES, aadharCardMaster.getUpdateValues());

			long result = sqLiteDatabase.insert(DataHelper.TABLES.TABLE_AADHARCARD_MASTER, null, contentValues);
			return result != -1;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			closeDatabase();
		}
		return false;
	}

	public static boolean getAadharCardMasterValues() {

		try {
			openDatabase();

			cursor = sqLiteDatabase.query(DataHelper.TABLES.TABLE_AADHARCARD_MASTER, null, null, null, null, null,
					null);
			if (cursor.moveToFirst()) {
				String uniqueid = null;
				String username = null;
				String ngoid = null;
				String groupid = null;
				String memberid = null;
				String mastervalues = null;
				String updatevalues = null;

				int localId = -1;
				do {
					localId = cursor.getInt(cursor.getColumnIndex(AadharCardMasterColumn.A_ID));
					uniqueid = cursor.getString(cursor.getColumnIndex(AadharCardMasterColumn.UNIQUEID));
					username = cursor.getString(cursor.getColumnIndex(AadharCardMasterColumn.USERNAME));

					ngoid = cursor.getString(cursor.getColumnIndex(AadharCardMasterColumn.NGOID));
					groupid = cursor.getString(cursor.getColumnIndex(AadharCardMasterColumn.GROUPID));
					memberid = cursor.getString(cursor.getColumnIndex(AadharCardMasterColumn.MEMBERID));

					mastervalues = cursor.getString(cursor.getColumnIndex(AadharCardMasterColumn.MASTERVALUES));

					updatevalues = cursor.getString(cursor.getColumnIndex(AadharCardMasterColumn.UPDATEVALUES));

					aadharCardMasters.add(new AadharCardMaster(localId, uniqueid, username, ngoid, groupid, memberid,
							mastervalues, updatevalues));
				} while (cursor.moveToNext());
			}

			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (cursor != null) {

				cursor.close();
			}
			closeDatabase();
		}

		return false;

	}

	public static List<AadharCardMaster> getAadharcardDetails() {
		try {
			openDatabase();
			final List<AadharCardMaster> aadharCardMasters = new ArrayList<AadharCardMaster>();
			cursor = sqLiteDatabase.query(DataHelper.TABLES.TABLE_AADHARCARD_MASTER, null, null, null, null, null,
					null);
			if (cursor.moveToFirst()) {
				String uniqueid = null;
				String username = null;
				String ngoid = null;
				String groupid = null;
				String memberid = null;
				String mastervalues = null;
				String updatevalues = null;

				int localId = -1;
				do {
					localId = cursor.getInt(cursor.getColumnIndex(AadharCardMasterColumn.A_ID));
					uniqueid = cursor.getString(cursor.getColumnIndex(AadharCardMasterColumn.UNIQUEID));
					username = cursor.getString(cursor.getColumnIndex(AadharCardMasterColumn.USERNAME));

					ngoid = cursor.getString(cursor.getColumnIndex(AadharCardMasterColumn.NGOID));
					groupid = cursor.getString(cursor.getColumnIndex(AadharCardMasterColumn.GROUPID));
					memberid = cursor.getString(cursor.getColumnIndex(AadharCardMasterColumn.MEMBERID));

					mastervalues = cursor.getString(cursor.getColumnIndex(AadharCardMasterColumn.MASTERVALUES));

					updatevalues = cursor.getString(cursor.getColumnIndex(AadharCardMasterColumn.UPDATEVALUES));

					aadharCardMasters.add(new AadharCardMaster(localId, uniqueid, username, ngoid, groupid, memberid,
							mastervalues, updatevalues));
				} while (cursor.moveToNext());
			}

			return aadharCardMasters;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			cursor.close();
			closeDatabase();
		}
		return null;
	}

	public static boolean updateMemberAadharCard(AadharCardMasterUpdate cardMasterUpdate) {

		try {
			openDatabase();
			// Define the updated row content.
			ContentValues updatedValues = new ContentValues();

			updatedValues.put(AadharCardMasterColumn.UPDATEVALUES, AadharCardMasterUpdate.getMasterUpdateValues());
			Log.e("------------------->>>>", AadharCardMasterUpdate.getMasterUpdateValues());

			sqLiteDatabase.update(DataHelper.TABLES.TABLE_AADHARCARD_MASTER, updatedValues,
					AadharCardMasterColumn.UNIQUEID + "=" + AadharCardMasterUpdate.getMasterUniqueId(), null);

			return false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			closeDatabase();
		}
		return true;
	}

	public static boolean deleteAadharcard(AadharcardDelete aadharcardDelete) {
		try {
			DataHelper dataHelper = DataHelper.getInstance(EShaktiApplication.getInstance());
			SQLiteDatabase sqLiteDatabase = dataHelper.getWritableDatabase();
			sqLiteDatabase.delete(DataHelper.TABLES.TABLE_AADHARCARD_MASTER, AadharCardMasterColumn.UNIQUEID + "= ?",
					new String[] { AadharcardDelete.getUniqueId() });
			sqLiteDatabase.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			closeDatabase();
		}
		return true;
	}

}
