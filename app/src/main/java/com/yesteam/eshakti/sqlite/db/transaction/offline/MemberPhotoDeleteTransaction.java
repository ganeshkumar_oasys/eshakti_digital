package com.yesteam.eshakti.sqlite.db.transaction.offline;

import com.yesteam.eshakti.sqlite.database.MemberPhotoProvider;
import com.yesteam.eshakti.sqlite.database.response.MemberPhotoDeleteResponse;
import com.yesteam.eshakti.sqlite.db.model.MemberPhotoDelete;
import com.yesteam.eshakti.sqlite.db.transactions.AbstractTransaction;

public class MemberPhotoDeleteTransaction extends AbstractTransaction<MemberPhotoDelete, MemberPhotoDeleteResponse> {

	@Override
	public int onPreExecute() {
		return EXECUTE_IN_WORKER_THREAD;
	}

	@Override
	public void run() {

		boolean value = MemberPhotoProvider.deleteAadharcard(getParams());

		MemberPhotoDeleteResponse transResponse = new MemberPhotoDeleteResponse(value);
		deliverResponse(transResponse);
	}
}
