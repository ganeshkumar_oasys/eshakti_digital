package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class AadharcardDeleteResponse extends BaseResponse {

	public AadharcardDeleteResponse(final TransactionResult transactionResult) {
		super(transactionResult);
	}

	public AadharcardDeleteResponse(final boolean result) {
		super(TransactionResult.SUCCESS);
	}
}