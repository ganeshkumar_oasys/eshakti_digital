package com.yesteam.eshakti.sqlite.db.transactions;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.yesteam.eshakti.sqlite.db.transaction.offline.AadharCardDeleteTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.AadharCardGetMasterDetailsTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.AadharCardMasterTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.AadharcardMemberUpdateTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.AddTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.Default_minutesTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.Default_pldis_Transaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.Default_plosTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.DeleteTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.GetBankAccountNoDetailsTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.GetFixedOSTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.GetGroupLoanOutstandingTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.GetGroupMasterDetailsTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.GetLoanAccountNoDetailsTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.GetLoanAccountNoDetailsUpdateTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.GetLoanAccountTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.GetMinutesofMeetingTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.GetOfflineMasterTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.GetSingleGroupLoanOSTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.GetSingleGroupMasterTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.GetSingleMasterTrasnaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.GetSingleMemberloanTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.GetSinglePersonalLoanTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.GetSingleProfileTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.GetSingleTransIdTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.GetTransValuesTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.Get_Min_MeetingTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.Get_OfflineReports_MinutesOfMeetingTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.GroupAddTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.GroupDetailsAddTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.GroupDetailsDeleteTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.GroupGetTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.GroupLOSUpdateTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.GroupLoanUpdateTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.GroupMLOSUpdateTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.GroupMasterFDOSTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.GroupMasterPLDBTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.GroupMasterPLDB_FD_Transaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.GroupMasterUpdateTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.GroupNameDeleteTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.GroupOfflineDataUpdateTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.GroupPLDSUpdateTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.GroupPLOSUpdateTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.LoanaccDataUpdateTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.LoginAddTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.LoginCheckTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.LoginDeleteTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.LoginUpdateTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.MemberGetPhotoMasterTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.MemberPhotoDeleteTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.MemberPhotoMasterTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.MemberPhotoUpdateTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.OfflineTransDeleteTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.TransIdUpdateTransaction;
import com.yesteam.eshakti.sqlite.db.transaction.offline.TransValueUpdateTransaction;
import com.yesteam.eshakti.utils.SBus;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

public final class TransactionManager {

	private static TransactionManager sTransactionManager;
	private final Map<DataType, Class<? extends ITransaction<?, ?>>> mFetcherMap = new HashMap<DataType, Class<? extends ITransaction<?, ?>>>();
	private RequestQueue mRequestQueue;

	private final ExecutorService mThreadPoolExecutor = Executors.newFixedThreadPool(5, new ThreadFactory() {

		private final AtomicInteger atomicInteger = new AtomicInteger();

		@Override
		public Thread newThread(Runnable r) {
			return new Thread(r, "TransactionThread-" + atomicInteger.incrementAndGet());
		}
	});

	private static final Handler mResponseHandler = new Handler() {

		@Override
		public void handleMessage(final Message msg) {
			if (msg.obj != null) {
				SBus.INST.post(msg.obj);
			}
		};
	};

	public enum DataType {

		LOGIN_ADD, LOGIN_CHECK, LOGINUPDATE, LOGINDELETE, GROUPNAMEADD, GROUPNAME_GET, GROUPNAMEDELETE, GROUPDETAILSADD, GROUPDETAILS_GETMASTER, GROUPDETAILSDELETE, GROUPMASTERUPDATE, GROUP_MASTER_MEMBERLOANOS, GROUP_MASTER_PERSONALOS, GROUP_MASTER_MLOS, GROUP_MASTER_PLOS, GET_TRANS_SINGLE, TRANS_VALUES_UPDATE, TRANSACTIONADD, GET_GROUPMASTER_GLOS, GROUP_MASTER_GLOS, GROUP_MASTER_PLDB, GROUP_UPDATE_PLDB, GET_TRANS_MASTERVALUES, GET_FIXED_OS, GROUP_MASTER_FDOS, GET_MASTER_MINUTESOFMEETINGS, GET_OFFLINE_MASTER_TRANSACTION, GET_MASTER_MIN_MEETING, GET_MASTER_TRANSID, TRANS_ID_UPDATE, TRANS_DELETE, GET_OFFLINE_REPORTS_MINUTESOFMEETING, GROUP_MASTER_PLDB_FD, OFFLINE_TRANS_DELETE, GROUPPROFILE_GETMASTER, DEFAULT_PLOS, DEFAULT_PLDISOS, DEFAULT_MINUTESOFMEETINGS, AADHARCARDMASTER, MEMBERPHOTOUPLOAD, AADHARCARDMASTERGET, MEMBERPHOTOUPLOADGET, AADHARCARDMASTERDELETE, MEMBERPHOTOUPLOADDELETE, MEMBERPHOTOUPDATE, MEMBERAADHARCARDUPDATE, GROUPLOANUPDATE, GET_LOANACCOUNT, GET_LOANACC_UPDATE, GROUPOFFLINEDATAUPDATE, GET_GROUP_MASTER_DETAILS, GET_BANKACCOUNT_NUMBER, GET_LOANACCOUNT_NUMBER, GET_LOANACCNO_DETAILS_UPDATE, GET_GROUPLOAN_OUTSTANDING;
	}

	private TransactionManager(final Context context) {
		initialize(context);
	}

	public static TransactionManager getInstance(final Context context) {
		if (sTransactionManager == null) {
			sTransactionManager = new TransactionManager(context);
		}
		return sTransactionManager;
	}

	private void initialize(final Context context) {
		mFetcherMap.put(DataType.LOGIN_ADD, LoginAddTransaction.class);
		mFetcherMap.put(DataType.LOGIN_CHECK, LoginCheckTransaction.class);
		mFetcherMap.put(DataType.LOGINDELETE, LoginDeleteTransaction.class);
		mFetcherMap.put(DataType.GROUPNAMEADD, GroupAddTransaction.class);
		mFetcherMap.put(DataType.GROUPNAME_GET, GroupGetTransaction.class);
		mFetcherMap.put(DataType.GROUPNAMEDELETE, GroupNameDeleteTransaction.class);
		mFetcherMap.put(DataType.GROUPDETAILSADD, GroupDetailsAddTransaction.class);
		mFetcherMap.put(DataType.GROUPDETAILS_GETMASTER, GetSingleGroupMasterTransaction.class);
		mFetcherMap.put(DataType.GROUPPROFILE_GETMASTER, GetSingleProfileTransaction.class);
		mFetcherMap.put(DataType.GROUPDETAILSDELETE, GroupDetailsDeleteTransaction.class);
		mFetcherMap.put(DataType.GROUPMASTERUPDATE, GroupMasterUpdateTransaction.class);
		mFetcherMap.put(DataType.GROUP_MASTER_MEMBERLOANOS, GetSingleMemberloanTransaction.class);
		mFetcherMap.put(DataType.GROUP_MASTER_PERSONALOS, GetSinglePersonalLoanTransaction.class);
		mFetcherMap.put(DataType.GROUP_MASTER_MLOS, GroupMLOSUpdateTransaction.class);
		mFetcherMap.put(DataType.GROUP_MASTER_PLOS, GroupPLOSUpdateTransaction.class);
		mFetcherMap.put(DataType.LOGINUPDATE, LoginUpdateTransaction.class);
		mFetcherMap.put(DataType.TRANSACTIONADD, AddTransaction.class);
		mFetcherMap.put(DataType.GET_TRANS_SINGLE, GetSingleMasterTrasnaction.class);
		mFetcherMap.put(DataType.TRANS_VALUES_UPDATE, TransValueUpdateTransaction.class);
		mFetcherMap.put(DataType.GET_GROUPMASTER_GLOS, GetSingleGroupLoanOSTransaction.class);
		mFetcherMap.put(DataType.GROUP_MASTER_GLOS, GroupLOSUpdateTransaction.class);
		mFetcherMap.put(DataType.GROUP_MASTER_PLDB, GroupMasterPLDBTransaction.class);
		mFetcherMap.put(DataType.GROUP_UPDATE_PLDB, GroupPLDSUpdateTransaction.class);
		mFetcherMap.put(DataType.GET_TRANS_MASTERVALUES, GetTransValuesTransaction.class);
		mFetcherMap.put(DataType.GET_FIXED_OS, GetFixedOSTransaction.class);
		mFetcherMap.put(DataType.GROUP_MASTER_FDOS, GroupMasterFDOSTransaction.class);
		mFetcherMap.put(DataType.GET_MASTER_MINUTESOFMEETINGS, GetMinutesofMeetingTransaction.class);
		mFetcherMap.put(DataType.GET_OFFLINE_MASTER_TRANSACTION, GetOfflineMasterTransaction.class);
		mFetcherMap.put(DataType.GET_MASTER_MIN_MEETING, Get_Min_MeetingTransaction.class);
		mFetcherMap.put(DataType.GET_MASTER_TRANSID, GetSingleTransIdTransaction.class);
		mFetcherMap.put(DataType.TRANS_ID_UPDATE, TransIdUpdateTransaction.class);
		mFetcherMap.put(DataType.TRANS_DELETE, DeleteTransaction.class);
		mFetcherMap.put(DataType.GROUP_MASTER_PLDB_FD, GroupMasterPLDB_FD_Transaction.class);
		mFetcherMap.put(DataType.GET_OFFLINE_REPORTS_MINUTESOFMEETING,
				Get_OfflineReports_MinutesOfMeetingTransaction.class);
		mFetcherMap.put(DataType.OFFLINE_TRANS_DELETE, OfflineTransDeleteTransaction.class);
		mFetcherMap.put(DataType.DEFAULT_PLOS, Default_plosTransaction.class);
		mFetcherMap.put(DataType.DEFAULT_PLDISOS, Default_pldis_Transaction.class);
		mFetcherMap.put(DataType.DEFAULT_MINUTESOFMEETINGS, Default_minutesTransaction.class);
		mFetcherMap.put(DataType.AADHARCARDMASTER, AadharCardMasterTransaction.class);
		mFetcherMap.put(DataType.MEMBERPHOTOUPLOAD, MemberPhotoMasterTransaction.class);
		mFetcherMap.put(DataType.AADHARCARDMASTERGET, AadharCardGetMasterDetailsTransaction.class);
		mFetcherMap.put(DataType.MEMBERPHOTOUPLOADGET, MemberGetPhotoMasterTransaction.class);
		mFetcherMap.put(DataType.AADHARCARDMASTERDELETE, AadharCardDeleteTransaction.class);
		mFetcherMap.put(DataType.MEMBERPHOTOUPLOADDELETE, MemberPhotoDeleteTransaction.class);
		mFetcherMap.put(DataType.MEMBERPHOTOUPDATE, MemberPhotoUpdateTransaction.class);
		mFetcherMap.put(DataType.MEMBERAADHARCARDUPDATE, AadharcardMemberUpdateTransaction.class);
		mFetcherMap.put(DataType.GROUPLOANUPDATE, GroupLoanUpdateTransaction.class);
		mFetcherMap.put(DataType.GET_LOANACCOUNT, GetLoanAccountTransaction.class);
		mFetcherMap.put(DataType.GET_LOANACC_UPDATE, LoanaccDataUpdateTransaction.class);
		mFetcherMap.put(DataType.GROUPOFFLINEDATAUPDATE, GroupOfflineDataUpdateTransaction.class);
		mFetcherMap.put(DataType.GET_GROUP_MASTER_DETAILS, GetGroupMasterDetailsTransaction.class);
		mFetcherMap.put(DataType.GET_BANKACCOUNT_NUMBER, GetBankAccountNoDetailsTransaction.class);
		mFetcherMap.put(DataType.GET_LOANACCOUNT_NUMBER, GetLoanAccountNoDetailsTransaction.class);
		mFetcherMap.put(DataType.GET_LOANACCNO_DETAILS_UPDATE, GetLoanAccountNoDetailsUpdateTransaction.class);
		mFetcherMap.put(DataType.GET_GROUPLOAN_OUTSTANDING, GetGroupLoanOutstandingTransaction.class);

		mRequestQueue = Volley.newRequestQueue(context);
	}

	@SuppressWarnings("unchecked")
	public void startTransaction(final DataType dataType, final Object filterParam) {
		@SuppressWarnings("rawtypes")
		ITransaction fetcher = getFetcherInstance(dataType);
		fetcher.setParams(filterParam);
		fetcher.setResponseHandler(mResponseHandler);

		switch (fetcher.onPreExecute()) {
		case ITransaction.EXECUTE_IN_VOLLEY_THREAD:
			Request<?> volleyRequest = fetcher.getVolleyRequest();
			if (volleyRequest != null) {
				mRequestQueue.add(volleyRequest);
			}
			break;
		case ITransaction.EXECUTE_IN_WORKER_THREAD:
			mThreadPoolExecutor.execute(fetcher);
			break;
		default:
			break;
		}
	}

	public RequestQueue getVolleyQueue() {
		return mRequestQueue;
	}

	private ITransaction<?, ?> getFetcherInstance(final DataType dataType) {
		Class<? extends ITransaction<?, ?>> fetcherClass = mFetcherMap.get(dataType);
		ITransaction<?, ?> fetcher = null;
		if (fetcherClass != null) {
			try {
				fetcher = fetcherClass.newInstance();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return fetcher;
	}

	public void cancelRequests() {
		if (mRequestQueue != null) {
			mRequestQueue.cancelAll(new RequestQueue.RequestFilter() {
				@Override
				public boolean apply(Request<?> request) {
					return true;
				}
			});
		}
	}
}