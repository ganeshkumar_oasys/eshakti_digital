package com.yesteam.eshakti.sqlite.db.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class GetTransactionSinglevalues implements Serializable {

	private static int localId;
	private static String mTrainerUserId;
	private static String mUsername;
	private static String mNgoId;
	private static String mGroupId;
	private static String mUniqueId;
	private static String mTransactionId;
	private static String mTransactionDate;
	private static String mSavings;
	private static String mVolunteersavings;
	private static String mPersonalloanRepayment;
	private static String mMemberloanRepayment;
	private static String mExpenses;
	private static String mOtherIncome;
	private static String mSubscriptionIncome;
	private static String mPenaltyIncome;
	private static String mDonationIncome;
	private static String mFederationIncome;
	private static String mGrouploanrepayment;
	private static String mBankDeposit;
	private static String mBankFixedDeposit;
	private static String mPersonalloandisbursement;
	private static String mAttendance;
	private static String mMinutesofmeeting;
	private static String mAuditting;
	private static String mTraining;
	private static String mLoanwithdrawal;
	private static String mAcctoacctransfer;
	private static String mAcctoLoanAccTransfer;
	private static String mSeedFund;

	public GetTransactionSinglevalues(final int localId, final String trans_trainerid, final String trans_username,
			final String trans_ngoid, final String trans_groupid, final String trans_uniqueid,
			final String trans_transactionid, final String trans_transactiondate, final String trans_savings,
			final String trans_volunteersavings, final String trans_personalloanrepayment,
			final String trans_memberloanrepayment, final String trans_expenses, final String trans_otherincome,
			final String trans_subscriptionincome, final String trans_penaltyincome, final String trans_donationincome,
			final String trans_federationincome, final String trans_grouploanrepayment, final String trans_bankdeposit,
			final String trans_bankfixeddeposit, final String trans_personalloandisbursement,
			final String trans_attendance, final String trans_minutesofmeeting, final String trans_auditting,
			final String trans_training, final String trans_loanwithdrawal, final String trans_acctoacctransfer,
			final String trans_AcctoLoanAcctransfer, final String trans_Seedfund) {
		GetTransactionSinglevalues.localId = localId;
		GetTransactionSinglevalues.mTrainerUserId = trans_trainerid;
		GetTransactionSinglevalues.mUsername = trans_username;
		GetTransactionSinglevalues.mNgoId = trans_ngoid;
		GetTransactionSinglevalues.mGroupId = trans_groupid;
		GetTransactionSinglevalues.mUniqueId = trans_uniqueid;
		GetTransactionSinglevalues.mTransactionId = trans_transactionid;
		GetTransactionSinglevalues.mTransactionDate = trans_transactiondate;
		GetTransactionSinglevalues.mSavings = trans_savings;
		GetTransactionSinglevalues.mVolunteersavings = trans_volunteersavings;
		GetTransactionSinglevalues.mPersonalloanRepayment = trans_personalloanrepayment;
		GetTransactionSinglevalues.mMemberloanRepayment = trans_memberloanrepayment;
		GetTransactionSinglevalues.mExpenses = trans_expenses;
		GetTransactionSinglevalues.mOtherIncome = trans_otherincome;
		GetTransactionSinglevalues.mSubscriptionIncome = trans_subscriptionincome;
		GetTransactionSinglevalues.mPenaltyIncome = trans_penaltyincome;
		GetTransactionSinglevalues.mDonationIncome = trans_donationincome;
		GetTransactionSinglevalues.mFederationIncome = trans_federationincome;
		GetTransactionSinglevalues.mGrouploanrepayment = trans_grouploanrepayment;
		GetTransactionSinglevalues.mBankDeposit = trans_bankdeposit;
		GetTransactionSinglevalues.mBankFixedDeposit = trans_bankfixeddeposit;
		GetTransactionSinglevalues.mPersonalloandisbursement = trans_personalloandisbursement;
		GetTransactionSinglevalues.mAttendance = trans_attendance;
		GetTransactionSinglevalues.mMinutesofmeeting = trans_minutesofmeeting;
		GetTransactionSinglevalues.mAuditting = trans_auditting;
		GetTransactionSinglevalues.mTraining = trans_training;
		GetTransactionSinglevalues.mLoanwithdrawal = trans_loanwithdrawal;
		GetTransactionSinglevalues.mAcctoacctransfer = trans_acctoacctransfer;
		GetTransactionSinglevalues.mAcctoLoanAccTransfer = trans_AcctoLoanAcctransfer;
		GetTransactionSinglevalues.mSeedFund = trans_Seedfund;

	}

	/**
	 * @return the localId
	 */
	public int getLocalId() {
		return localId;
	}

	public static String getTrainerUserId() {
		return mTrainerUserId;
	}

	public static String getUsername() {
		return mUsername;
	}

	public static String getNgoId() {
		return mNgoId;
	}

	public static String getGroupId() {
		return mGroupId;
	}

	public static String getUniqueId() {
		return mUniqueId;
	}

	public static String getTransactionId() {
		return mTransactionId;
	}

	public static String getTransactionDate() {
		return mTransactionDate;
	}

	public static String getSavings() {
		return mSavings;
	}

	public static String getVolunteerSavings() {
		return mVolunteersavings;

	}

	public static String getPersonalloanRepayment() {
		return mPersonalloanRepayment;
	}

	public static String getMemberloanRepayment() {
		return mMemberloanRepayment;
	}

	public static String getExpenses() {
		return mExpenses;
	}

	public static String getOtherIncome() {
		return mOtherIncome;
	}

	public static String getSubscriptionIncome() {
		return mSubscriptionIncome;
	}

	public static String getPenaltyIncome() {
		return mPenaltyIncome;
	}

	public static String getDonationIncome() {
		return mDonationIncome;
	}

	public static String getFederationIncome() {
		return mFederationIncome;
	}

	public static String getGrouploanrepayment() {
		return mGrouploanrepayment;
	}

	public static String getBankDeposit() {
		return mBankDeposit;
	}

	public static String getBankFixedDeposit() {
		return mBankFixedDeposit;
	}

	public static String getAttendance() {
		return mAttendance;
	}

	public static String getMinutesofmeeting() {
		return mMinutesofmeeting;
	}

	public static String getAuditting() {
		return mAuditting;
	}

	/**
	 * @return the mTraining
	 */
	public static String getTraining() {
		return mTraining;
	}

	/**
	 * @return the mPersonalloandisbursement
	 */
	public static String getPersonalloandisbursement() {
		return mPersonalloandisbursement;
	}

	public static String getLoanwithdrawal() {
		return mLoanwithdrawal;
	}

	public static String getAcctoacctransfer() {
		return mAcctoacctransfer;
	}

	public static String getAcctoLoanAccTransfer() {
		return mAcctoLoanAccTransfer;
	}

	public static void setAcctoLoanAccTransfer(String mAcctoLoanAccTransfer) {
		GetTransactionSinglevalues.mAcctoLoanAccTransfer = mAcctoLoanAccTransfer;
	}

	public static String getSeedFund() {
		return mSeedFund;
	}

	public static void setSeedFund(String mSeedFund) {
		GetTransactionSinglevalues.mSeedFund = mSeedFund;
	}

}
