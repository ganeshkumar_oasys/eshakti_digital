package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class GroupDeleteResponse extends BaseResponse {

	public GroupDeleteResponse(final TransactionResult transactionResult) {
		super(transactionResult);
	}

	public GroupDeleteResponse(final boolean result) {
		super(TransactionResult.SUCCESS);
	}
}
