package com.yesteam.eshakti.sqlite.db.transaction.offline;

import com.yesteam.eshakti.sqlite.database.GroupDetailsProvider;
import com.yesteam.eshakti.sqlite.database.response.GroupFixedOSResponse;
import com.yesteam.eshakti.sqlite.db.model.GroupFOSUpdate;
import com.yesteam.eshakti.sqlite.db.transactions.AbstractTransaction;

public class GroupMasterFDOSTransaction extends
		AbstractTransaction<GroupFOSUpdate, GroupFixedOSResponse> {

	@Override
	public int onPreExecute() {
		return EXECUTE_IN_WORKER_THREAD;
	}

	@Override
	public void run() {

		boolean value = GroupDetailsProvider.updateFixedOS(getParams());

		GroupFixedOSResponse transResponse = new GroupFixedOSResponse(value);

		deliverResponse(transResponse);

	}
}
