package com.yesteam.eshakti.sqlite.db.transaction.offline;

import com.yesteam.eshakti.sqlite.database.GroupDetailsProvider;
import com.yesteam.eshakti.sqlite.database.response.GroupLoanOSResponse;
import com.yesteam.eshakti.sqlite.db.model.GroupLoanOSUpdate;
import com.yesteam.eshakti.sqlite.db.transactions.AbstractTransaction;

public class GroupLOSUpdateTransaction extends
		AbstractTransaction<GroupLoanOSUpdate, GroupLoanOSResponse> {

	@Override
	public int onPreExecute() {
		return EXECUTE_IN_WORKER_THREAD;
	}

	@Override
	public void run() {

		boolean value = GroupDetailsProvider.updateGroupLOS(getParams());

		GroupLoanOSResponse transResponse = new GroupLoanOSResponse(value);

		deliverResponse(transResponse);

	}

}