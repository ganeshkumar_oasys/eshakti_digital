package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class GroupMemberResponse extends BaseResponse {

	public GroupMemberResponse(final TransactionResult transactionResult) {
		super(transactionResult);
	}

	public GroupMemberResponse(final boolean result) {
		super(TransactionResult.SUCCESS);
	}
}
