package com.yesteam.eshakti.sqlite.db.transaction.offline;

import com.yesteam.eshakti.params.NoParams;
import com.yesteam.eshakti.sqlite.database.TransactionProvider;
import com.yesteam.eshakti.sqlite.database.response.OfflineTransDeleteResponse;
import com.yesteam.eshakti.sqlite.db.transactions.AbstractTransaction;

public class OfflineTransDeleteTransaction extends
		AbstractTransaction<NoParams, OfflineTransDeleteResponse> {

	@Override
	public int onPreExecute() {
		return EXECUTE_IN_WORKER_THREAD;
	}

	@Override
	public void run() {

		boolean value = TransactionProvider.deleteOfflinetransaction();

		OfflineTransDeleteResponse transResponse = new OfflineTransDeleteResponse(
				value);
		deliverResponse(transResponse);
	}
}
