package com.yesteam.eshakti.sqlite.database.response;

public enum TransactionResult {

	SUCCESS, FAIL, NO_NETWORK_CONNECTION;

	private String message;

	public void setMessage(final String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	@Override
	public String toString() {
		return message;
	}
}
