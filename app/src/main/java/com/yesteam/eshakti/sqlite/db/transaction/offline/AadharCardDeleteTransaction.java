package com.yesteam.eshakti.sqlite.db.transaction.offline;

import com.yesteam.eshakti.sqlite.database.AadharcardMasterProvider;
import com.yesteam.eshakti.sqlite.database.response.AadharcardDeleteResponse;
import com.yesteam.eshakti.sqlite.db.model.AadharcardDelete;
import com.yesteam.eshakti.sqlite.db.transactions.AbstractTransaction;

public class AadharCardDeleteTransaction extends AbstractTransaction<AadharcardDelete, AadharcardDeleteResponse> {

	@Override
	public int onPreExecute() {
		return EXECUTE_IN_WORKER_THREAD;
	}

	@Override
	public void run() {

		boolean value = AadharcardMasterProvider.deleteAadharcard(getParams());

		AadharcardDeleteResponse transResponse = new AadharcardDeleteResponse(value);
		deliverResponse(transResponse);
	}
}