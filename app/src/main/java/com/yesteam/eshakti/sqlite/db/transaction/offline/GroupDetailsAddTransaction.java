package com.yesteam.eshakti.sqlite.db.transaction.offline;

import com.yesteam.eshakti.sqlite.database.GroupDetailsProvider;
import com.yesteam.eshakti.sqlite.database.response.GroupMemberResponse;
import com.yesteam.eshakti.sqlite.db.model.GroupMaster;
import com.yesteam.eshakti.sqlite.db.transactions.AbstractTransaction;

public class GroupDetailsAddTransaction extends AbstractTransaction<GroupMaster, GroupMemberResponse> {

	@Override
	public int onPreExecute() {
		return EXECUTE_IN_WORKER_THREAD;
	}

	@Override
	public void run() {
		boolean value = GroupDetailsProvider.addGroupDetails(getParams());
		
		GroupMemberResponse transResponse = new GroupMemberResponse(value);
		deliverResponse(transResponse);
	}
}
