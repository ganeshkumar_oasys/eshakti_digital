package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class GroupOfflineDataUpdateResponse extends BaseResponse {

	public GroupOfflineDataUpdateResponse(final TransactionResult transactionResult) {
		super(transactionResult);
	}

	public GroupOfflineDataUpdateResponse(final boolean result) {
		super(TransactionResult.SUCCESS);
	}
}