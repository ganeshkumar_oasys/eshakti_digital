package com.yesteam.eshakti.sqlite.db.transaction.offline;

import com.yesteam.eshakti.params.NoParams;
import com.yesteam.eshakti.sqlite.database.MemberPhotoProvider;
import com.yesteam.eshakti.sqlite.database.response.MemberGetPhotoMasterResponse;
import com.yesteam.eshakti.sqlite.db.transactions.AbstractTransaction;

public class MemberGetPhotoMasterTransaction extends AbstractTransaction<NoParams, MemberGetPhotoMasterResponse> {

	@Override
	public int onPreExecute() {
		return EXECUTE_IN_WORKER_THREAD;
	}

	@Override
	public void run() {
		boolean value = MemberPhotoProvider.getMemberPhotoMasterValues();
		MemberGetPhotoMasterResponse transResponse = new MemberGetPhotoMasterResponse(value);
		deliverResponse(transResponse);
	}
}