package com.yesteam.eshakti.sqlite.db.model;

import java.io.Serializable;

public class MemberPhotoDelete implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5601963273562389950L;
	private static String mUniqueId_delete;

	public MemberPhotoDelete(final String grp_uniqueid) {

		MemberPhotoDelete.mUniqueId_delete = grp_uniqueid;

	}

	public static String getUniqueId() {
		return mUniqueId_delete;
	}
}