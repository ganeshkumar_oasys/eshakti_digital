package com.yesteam.eshakti.sqlite.db.transactions;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.oasys.eshakti.digitization.EShaktiApplication;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

public abstract class AbstractTransaction<P, T> implements ITransaction<P, T> {

	private P mParam;
	private Handler mResponseHandler;

	@Override
	public int onPreExecute() {
		return ITransaction.EXECUTE_IN_VOLLEY_THREAD;
	}

	@Override
	public void run() {

	}

	@Override
	public Request<T> getVolleyRequest() {
		return null;
	}

	@Override
	public void onResponse(T response) {

	}

	@Override
	public void onErrorResponse(VolleyError error) {

	}

	@Override
	public void setParams(final P param) {
		this.mParam = param;
	}

	protected P getParams() {
		return mParam;
	}

	@Override
	public void setResponseHandler(final Handler responseHandler) {
		this.mResponseHandler = responseHandler;
	}

	protected void deliverResponse(final T response) {
		Message message = Message.obtain(mResponseHandler);
		message.obj = response;
		mResponseHandler.sendMessage(message);
	}

	protected Context getContext() {
		return EShaktiApplication.getInstance();
	}
}
