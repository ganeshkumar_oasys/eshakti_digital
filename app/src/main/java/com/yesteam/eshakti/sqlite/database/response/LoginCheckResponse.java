package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class LoginCheckResponse extends BaseResponse {

	private String mPassword;

	public LoginCheckResponse(final TransactionResult transactionResult) {
		super(transactionResult);
	}

	public LoginCheckResponse(final boolean result) {
		super(TransactionResult.SUCCESS);
	}

	public String getPassword() {
		return mPassword;
	}

	public void setPassword(String mPassword) {
		this.mPassword = mPassword;
	}

}
