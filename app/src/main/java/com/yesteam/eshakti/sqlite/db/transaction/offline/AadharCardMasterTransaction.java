package com.yesteam.eshakti.sqlite.db.transaction.offline;

import com.yesteam.eshakti.sqlite.database.AadharcardMasterProvider;
import com.yesteam.eshakti.sqlite.database.response.AadharCardMasterResponse;
import com.yesteam.eshakti.sqlite.db.model.AadharCardMaster;
import com.yesteam.eshakti.sqlite.db.transactions.AbstractTransaction;

public class AadharCardMasterTransaction extends AbstractTransaction<AadharCardMaster, AadharCardMasterResponse> {

	@Override
	public int onPreExecute() {
		return EXECUTE_IN_WORKER_THREAD;
	}

	@Override
	public void run() {

		boolean value = AadharcardMasterProvider.addAadharcardDetails(getParams());

		AadharCardMasterResponse transResponse = new AadharCardMasterResponse(value);
		deliverResponse(transResponse);
	}

}
