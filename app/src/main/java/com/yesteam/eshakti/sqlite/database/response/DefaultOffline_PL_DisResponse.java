package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class DefaultOffline_PL_DisResponse extends BaseResponse {

	public DefaultOffline_PL_DisResponse(final TransactionResult transactionResult) {
		super(transactionResult);
		// TODO Auto-generated constructor stub
	}

	public DefaultOffline_PL_DisResponse(final boolean result) {
		super(TransactionResult.SUCCESS);
	}

}
