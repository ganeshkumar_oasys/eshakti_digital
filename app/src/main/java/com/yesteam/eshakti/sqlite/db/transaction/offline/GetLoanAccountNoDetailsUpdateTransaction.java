package com.yesteam.eshakti.sqlite.db.transaction.offline;

import com.yesteam.eshakti.sqlite.database.GroupDetailsProvider;
import com.yesteam.eshakti.sqlite.database.response.LoanAccountNoUpdateResponse;
import com.yesteam.eshakti.sqlite.db.model.LoanAccountNoDetailsUpdate;
import com.yesteam.eshakti.sqlite.db.transactions.AbstractTransaction;

public class GetLoanAccountNoDetailsUpdateTransaction
		extends AbstractTransaction<LoanAccountNoDetailsUpdate, LoanAccountNoUpdateResponse> {

	@Override
	public int onPreExecute() {
		return EXECUTE_IN_WORKER_THREAD;
	}

	@Override
	public void run() {

		boolean value = GroupDetailsProvider.updateLoanAccountNumber(getParams());

		LoanAccountNoUpdateResponse transResponse = new LoanAccountNoUpdateResponse(value);

		deliverResponse(transResponse);

	}

}