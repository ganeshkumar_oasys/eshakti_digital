package com.yesteam.eshakti.sqlite.db.transaction.offline;

import java.util.List;

import com.yesteam.eshakti.sqlite.database.LoginProvider;
import com.yesteam.eshakti.sqlite.database.response.LoginDeleteResponse;
import com.yesteam.eshakti.sqlite.db.transactions.AbstractTransaction;


public class LoginDeleteTransaction extends
		AbstractTransaction<Integer, List<LoginDeleteResponse>> {

	@Override
	public int onPreExecute() {
		return EXECUTE_IN_WORKER_THREAD;
	}

	@Override
	public void run() {
		LoginProvider.deleteLogin();

	}
}
