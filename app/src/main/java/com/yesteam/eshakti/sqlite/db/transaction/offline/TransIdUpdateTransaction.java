package com.yesteam.eshakti.sqlite.db.transaction.offline;

import com.yesteam.eshakti.sqlite.database.GroupDetailsProvider;
import com.yesteam.eshakti.sqlite.database.response.TransIdUpdateResponse;
import com.yesteam.eshakti.sqlite.db.model.TransIdUpdate;
import com.yesteam.eshakti.sqlite.db.transactions.AbstractTransaction;

import android.util.Log;


public class TransIdUpdateTransaction extends
		AbstractTransaction<TransIdUpdate, TransIdUpdateResponse> {

	@Override
	public int onPreExecute() {
		return EXECUTE_IN_WORKER_THREAD;
	}

	@Override
	public void run() {

		boolean value = GroupDetailsProvider
				.updateGroupLastTransId(getParams());

		TransIdUpdateResponse transResponse = new TransIdUpdateResponse(value);

		deliverResponse(transResponse);
		Log.e("Update Executed", "Update Sucessfully");

	}

}
