package com.yesteam.eshakti.sqlite.db.model;

import java.io.Serializable;

public class AadharCardMaster implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5752131107841856974L;

	private final int localId;
	private final String mUniqueid;
	private final String mUsername;
	private final String mNgoId;
	private final String mGroupId;
	private final String mMemberId;
	private final String mMasterValues;
	private final String mUpdateValues;

	public AadharCardMaster(final int localID, final String uniqueid, final String username, final String ngoid,
			final String groupid, final String memberid, final String mastervalues, final String updatevalues) {
		this.localId = localID;
		this.mUniqueid = uniqueid;
		this.mUsername = username;
		this.mNgoId = ngoid;
		this.mGroupId = groupid;
		this.mMemberId = memberid;
		this.mMasterValues = mastervalues;
		this.mUpdateValues = updatevalues;

	}

	public int getLocalId() {
		return localId;
	}

	public String getUsername() {
		return mUsername;
	}

	public String getNgoId() {
		return mNgoId;
	}

	public String getGroupId() {
		return mGroupId;
	}

	public String getMemberId() {
		return mMemberId;
	}

	public String getMasterValues() {
		return mMasterValues;
	}

	public String getUpdateValues() {
		return mUpdateValues;
	}

	public String getUniqueid() {
		return mUniqueid;
	}
}
