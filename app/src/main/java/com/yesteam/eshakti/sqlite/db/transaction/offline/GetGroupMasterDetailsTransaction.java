package com.yesteam.eshakti.sqlite.db.transaction.offline;

import java.util.List;

import com.yesteam.eshakti.params.NoParams;
import com.yesteam.eshakti.sqlite.database.GroupDetailsProvider;
import com.yesteam.eshakti.sqlite.db.model.GroupMaster;
import com.yesteam.eshakti.sqlite.db.transactions.AbstractTransaction;

public class GetGroupMasterDetailsTransaction extends AbstractTransaction<NoParams, List<GroupMaster>> {

	@Override
	public int onPreExecute() {
		return EXECUTE_IN_WORKER_THREAD;
	}

	@Override
	public void run() {
		deliverResponse(GroupDetailsProvider.getGroupDetails());
	}
}
