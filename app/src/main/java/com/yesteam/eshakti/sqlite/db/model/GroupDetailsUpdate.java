package com.yesteam.eshakti.sqlite.db.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class GroupDetailsUpdate implements Serializable {

	private static String mGroupId;
	private static String mGroupResponse;

	public GroupDetailsUpdate(final String grp_groupId,
			final String grp_groupresponse) {

		GroupDetailsUpdate.mGroupId = grp_groupId;
		GroupDetailsUpdate.mGroupResponse = grp_groupresponse;
	}

	public static String getGroupId() {
		return mGroupId;
	}

	public static String getGroupResponse() {
		return mGroupResponse;
	}

}
