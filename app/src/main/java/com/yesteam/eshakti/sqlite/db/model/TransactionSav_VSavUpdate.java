package com.yesteam.eshakti.sqlite.db.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class TransactionSav_VSavUpdate implements Serializable {

	private static String mUniqueId;
	private static String mUpdateTransactionvalues;
	private static String mUpdateTransactionVSavings;

	public TransactionSav_VSavUpdate(final String grp_uniqueId,
			final String grp_updatetransvalues, final String grp_vsavings) {

		TransactionSav_VSavUpdate.mUniqueId = grp_uniqueId;
		TransactionSav_VSavUpdate.mUpdateTransactionvalues = grp_updatetransvalues;
		TransactionSav_VSavUpdate.mUpdateTransactionVSavings = grp_vsavings;
	}

	public static String getUniqueId() {
		return mUniqueId;
	}

	public static String getTransactionValues() {
		return mUpdateTransactionvalues;
	}

	public static String getTransactionVSavings() {
		return mUpdateTransactionVSavings;
	}

}
