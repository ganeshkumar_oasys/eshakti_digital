package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class TransIdUpdateResponse extends BaseResponse {

	public TransIdUpdateResponse(final TransactionResult transactionResult) {
		super(transactionResult);
	}

	public TransIdUpdateResponse(final boolean result) {
		super(TransactionResult.SUCCESS);
	}

}