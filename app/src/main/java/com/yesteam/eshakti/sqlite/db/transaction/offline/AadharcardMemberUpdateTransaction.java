package com.yesteam.eshakti.sqlite.db.transaction.offline;

import com.yesteam.eshakti.sqlite.database.AadharcardMasterProvider;
import com.yesteam.eshakti.sqlite.database.response.MemberAadharCardMasterUpdateResponse;
import com.yesteam.eshakti.sqlite.db.model.AadharCardMasterUpdate;
import com.yesteam.eshakti.sqlite.db.transactions.AbstractTransaction;

public class AadharcardMemberUpdateTransaction
		extends AbstractTransaction<AadharCardMasterUpdate, MemberAadharCardMasterUpdateResponse> {

	@Override
	public int onPreExecute() {
		return EXECUTE_IN_WORKER_THREAD;
	}

	@Override
	public void run() {

		boolean value = AadharcardMasterProvider.updateMemberAadharCard(getParams());

		MemberAadharCardMasterUpdateResponse transResponse = new MemberAadharCardMasterUpdateResponse(value);

		deliverResponse(transResponse);

	}

}