package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class GroupLoanUpdateResponse extends BaseResponse {

	public GroupLoanUpdateResponse(final TransactionResult transactionResult) {
		super(transactionResult);
	}

	public GroupLoanUpdateResponse(final boolean result) {
		super(TransactionResult.SUCCESS);
	}

}
