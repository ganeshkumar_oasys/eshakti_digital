package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class GroupMLOSUpdateResponse extends BaseResponse {

	public GroupMLOSUpdateResponse(final TransactionResult transactionResult) {
		super(transactionResult);
	}

	public GroupMLOSUpdateResponse(final boolean result) {
		super(TransactionResult.SUCCESS);
	}

}
