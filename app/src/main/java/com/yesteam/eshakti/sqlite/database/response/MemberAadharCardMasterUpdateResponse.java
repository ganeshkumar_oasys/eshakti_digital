package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class MemberAadharCardMasterUpdateResponse extends BaseResponse {

	public MemberAadharCardMasterUpdateResponse(final TransactionResult transactionResult) {
		super(transactionResult);
	}

	public MemberAadharCardMasterUpdateResponse(final boolean result) {
		super(TransactionResult.SUCCESS);
	}
}
