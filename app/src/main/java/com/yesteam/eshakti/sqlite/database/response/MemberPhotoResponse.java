package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class MemberPhotoResponse extends BaseResponse {

	public MemberPhotoResponse(final TransactionResult transactionResult) {
		super(transactionResult);
	}

	public MemberPhotoResponse(final boolean result) {
		super(TransactionResult.SUCCESS);
	}
}
