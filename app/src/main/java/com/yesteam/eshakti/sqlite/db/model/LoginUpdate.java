package com.yesteam.eshakti.sqlite.db.model;

import java.io.Serializable;

public class LoginUpdate implements Serializable {
	private static final long serialVersionUID = 1L;
	private static String mUsername;

	public LoginUpdate(final String log_username) {

		LoginUpdate.mUsername = log_username;
	}

	public static String getUsername() {
		return mUsername;
	}
}
