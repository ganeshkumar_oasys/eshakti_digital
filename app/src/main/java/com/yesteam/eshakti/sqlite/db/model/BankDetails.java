package com.yesteam.eshakti.sqlite.db.model;

import java.io.Serializable;

public class BankDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final int localId;
	private final String mBankName;
	private final String mBranchName;
	private final String mIfscCode;

	public BankDetails(final int localID, final String mas_BankName, final String mas_BranchName,
			final String mas_IfscCode) {
		this.localId = localID;
		this.mBankName = mas_BankName;
		this.mBranchName = mas_BranchName;
		this.mIfscCode = mas_IfscCode;

	}

	public int getLocalId() {
		return localId;
	}

	public String getBankName() {
		return mBankName;
	}

	public String getBranchName() {
		return mBranchName;
	}

	public String getIfscCode() {
		return mIfscCode;
	}

}