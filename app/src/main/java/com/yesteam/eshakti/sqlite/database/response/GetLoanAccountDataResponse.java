package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class GetLoanAccountDataResponse extends BaseResponse {

	public GetLoanAccountDataResponse(final TransactionResult transactionResult) {
		super(transactionResult);
		// TODO Auto-generated constructor stub
	}

	public GetLoanAccountDataResponse(final boolean result) {
		super(TransactionResult.SUCCESS);
	}

}
