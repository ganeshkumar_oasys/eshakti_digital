package com.yesteam.eshakti.sqlite.db.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Groupdetails implements Serializable {

	private final int localId;
	private final String mTrainerUserId;
	private final String mNgoId;
	private final String mGroupId;
	private final String mGroupname;
	private final String mGroupnameresponse;
	private final String mGroupLastTransactiondate;
	private final String mGroupEntrySystemDate;

	public Groupdetails(final int localId, final String grp_traineruserid, final String grp_ngoid,
			final String grp_groupid, final String grp_groupname, final String grp_grpnameresponse,
			final String grp_lastTransactionDate, final String grp_grpsystementrydate) {
		this.localId = localId;
		this.mTrainerUserId = grp_traineruserid;
		this.mNgoId = grp_ngoid;
		this.mGroupId = grp_groupid;
		this.mGroupname = grp_groupname;
		this.mGroupnameresponse = grp_grpnameresponse;
		this.mGroupLastTransactiondate = grp_lastTransactionDate;
		this.mGroupEntrySystemDate = grp_grpsystementrydate;

	}

	public int getLocalId() {
		return localId;
	}

	public String getTrainerUserId() {
		return mTrainerUserId;
	}

	public String getNgoId() {
		return mNgoId;
	}

	public String getGroupId() {
		return mGroupId;
	}

	public String getGroupname() {
		return mGroupname;
	}

	public String getGroupnameresponse() {
		return mGroupnameresponse;
	}

	public String getGroupLastTransactiondate() {
		return mGroupLastTransactiondate;
	}

	public String getGroupEntrySystemDate() {
		return mGroupEntrySystemDate;
	}

}
