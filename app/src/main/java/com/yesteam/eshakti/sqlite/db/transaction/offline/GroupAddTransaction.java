package com.yesteam.eshakti.sqlite.db.transaction.offline;

import com.yesteam.eshakti.sqlite.database.GroupProvider;
import com.yesteam.eshakti.sqlite.database.response.GroupNameResponse;
import com.yesteam.eshakti.sqlite.db.model.Groupdetails;
import com.yesteam.eshakti.sqlite.db.transactions.AbstractTransaction;

public class GroupAddTransaction extends
		AbstractTransaction<Groupdetails, GroupNameResponse> {

	@Override
	public int onPreExecute() {
		return EXECUTE_IN_WORKER_THREAD;
	}

	@Override
	public void run() {
		boolean value = GroupProvider.addGroupDetails(getParams());
		GroupNameResponse transResponse = new GroupNameResponse(value);
		deliverResponse(transResponse);
	}
}