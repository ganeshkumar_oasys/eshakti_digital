package com.yesteam.eshakti.sqlite.db.model;

import java.io.Serializable;

public class LoginCheck implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static String mUsername;
	private static boolean mUserAvabl;
	private static String mUserLanguage;
	private static String mUserNgoId;
	private static String mTrainerId;
	
	public LoginCheck(final String log_username) {

		LoginCheck.mUsername = log_username;
	}

	public static String getUsername() {
		return mUsername;
	}

	public static boolean isUserAvabl() {
		return mUserAvabl;
	}

	public static void setUserAvabl(boolean mUserAvabl) {
		LoginCheck.mUserAvabl = mUserAvabl;
	}

	public static String getUserLanguage() {
		return mUserLanguage;
	}

	public static void setUserLanguage(String mUserLanguage) {
		LoginCheck.mUserLanguage = mUserLanguage;
	}

	public static String getUserNgoId() {
		return mUserNgoId;
	}

	public static void setUserNgoId(String mUserNgoId) {
		LoginCheck.mUserNgoId = mUserNgoId;
	}

	public static String getTrainerId() {
		return mTrainerId;
	}

	public static void setTrainerId(String mTrainerId) {
		LoginCheck.mTrainerId = mTrainerId;
	}
}