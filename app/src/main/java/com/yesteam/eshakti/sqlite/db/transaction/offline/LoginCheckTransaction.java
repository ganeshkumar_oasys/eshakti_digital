package com.yesteam.eshakti.sqlite.db.transaction.offline;

import com.yesteam.eshakti.sqlite.database.LoginProvider;
import com.yesteam.eshakti.sqlite.database.response.LoginCheckResponse;
import com.yesteam.eshakti.sqlite.db.model.LoginCheck;
import com.yesteam.eshakti.sqlite.db.transactions.AbstractTransaction;

public class LoginCheckTransaction extends
		AbstractTransaction<LoginCheck, LoginCheckResponse> {

	@Override
	public int onPreExecute() {
		return EXECUTE_IN_WORKER_THREAD;
	}

	@Override
	public void run() {

		boolean value = LoginProvider.getSinlgeEntry(getParams());

		LoginCheckResponse transResponse = new LoginCheckResponse(value);
		deliverResponse(transResponse);

	}

}
