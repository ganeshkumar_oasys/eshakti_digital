package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class GroupProfileResponse extends BaseResponse {

	public GroupProfileResponse(final TransactionResult transactionResult) {
		super(transactionResult);
	}

	public GroupProfileResponse(final boolean result) {
		super(TransactionResult.SUCCESS);
	}
}
