package com.yesteam.eshakti.sqlite.db.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class GroupMLUpdate implements Serializable {
	private static String mGroupId;
	private static String mGroupResponse;
	private static String mGroupMLoutstanding;

	public GroupMLUpdate(final String grp_groupId,
			final String grp_groupresponse, final String grp_groupMLOS) {

		GroupMLUpdate.mGroupId = grp_groupId;
		GroupMLUpdate.mGroupResponse = grp_groupresponse;
		GroupMLUpdate.mGroupMLoutstanding = grp_groupMLOS;
	}

	public static String getGroupId() {
		return mGroupId;
	}

	public static String getGroupResponse() {
		return mGroupResponse;
	}

	public static String getGroupMLoutstanding() {
		return mGroupMLoutstanding;
	}

}
