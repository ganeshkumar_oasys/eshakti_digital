package com.yesteam.eshakti.sqlite.db.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class GroupLoanOSUpdate implements Serializable {
	private static String mGroupId;
	private static String mGroupResponse;
	private static String mGroupGLoutstanding;

	public GroupLoanOSUpdate(final String grp_groupId,
			final String grp_groupresponse, final String grp_groupMLOS) {

		GroupLoanOSUpdate.mGroupId = grp_groupId;
		GroupLoanOSUpdate.mGroupResponse = grp_groupresponse;
		GroupLoanOSUpdate.mGroupGLoutstanding = grp_groupMLOS;
	}

	public static String getGroupId() {
		return mGroupId;
	}

	public static String getGroupResponse() {
		return mGroupResponse;
	}

	public static String getGroupGLoutstanding() {
		return mGroupGLoutstanding;
	}

}
