package com.yesteam.eshakti.sqlite.db.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class GetTransactionSingle implements Serializable {

	private static String mTransUniqueId;

	public GetTransactionSingle(final String mastergroupid) {
		GetTransactionSingle.mTransUniqueId = mastergroupid;

	}

	public static String getTransUniqueId() {
		return mTransUniqueId;
	}

}
