package com.yesteam.eshakti.sqlite.database;

import java.util.ArrayList;
import java.util.List;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.yesteam.eshakti.sqlite.db.model.MemberPhotoDelete;
import com.yesteam.eshakti.sqlite.db.model.MemberPhotoMaster;
import com.yesteam.eshakti.sqlite.db.model.MemberPhotoMasterUpdate;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class MemberPhotoProvider {
	private static SQLiteDatabase sqLiteDatabase;
	private static DataHelper dataHelper;
	static Cursor cursor;
	public static List<MemberPhotoMaster> memberPhotoMasters = new ArrayList<MemberPhotoMaster>();

	public MemberPhotoProvider(Activity activity) {
		dataHelper = new DataHelper(activity);
	}

	public static void openDatabase() {
		dataHelper = new DataHelper(EShaktiApplication.getInstance());
		dataHelper.onOpen(sqLiteDatabase);
		sqLiteDatabase = dataHelper.getWritableDatabase();

	}

	public static void closeDatabase() {
		dataHelper.close();
	}

	public interface PhotoMasterColumn {
		public static final String M_ID = "local_Id";
		public static final String UNIQUEID = "Uniqueid";
		public static final String USERNAME = "Username";
		public static final String NGOID = "NgoId";
		public static final String GROUPID = "Groupid";
		public static final String MEMBERID = "MemberId";
		public static final String IMAGE = "Image";
		public static final String UPDATEVALUES = "UpdateValues";

	}

	public static final String CREATE_MEMBERPHOTO_DETAILS_TABLE = "create table "
			+ DataHelper.TABLES.TABLE_MEMBER_PHOTO_MASTER + "(" + PhotoMasterColumn.M_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + PhotoMasterColumn.UNIQUEID + " text," + PhotoMasterColumn.USERNAME
			+ " TEXT," + PhotoMasterColumn.NGOID + " text," + PhotoMasterColumn.GROUPID + " text,"
			+ PhotoMasterColumn.MEMBERID + " text," + PhotoMasterColumn.IMAGE + " BLOB NOT NULL,"
			+ PhotoMasterColumn.UPDATEVALUES + " text" + ")";

	public static boolean addMemberPhotDetails(MemberPhotoMaster memberPhotoMaster) {
		try {
			openDatabase();
			ContentValues contentValues = new ContentValues();
			contentValues.put(PhotoMasterColumn.UNIQUEID, memberPhotoMaster.getUniqueid());
			contentValues.put(PhotoMasterColumn.USERNAME, memberPhotoMaster.getUsername());
			contentValues.put(PhotoMasterColumn.NGOID, memberPhotoMaster.getNgoId());
			contentValues.put(PhotoMasterColumn.GROUPID, memberPhotoMaster.getGroupId());
			contentValues.put(PhotoMasterColumn.MEMBERID, memberPhotoMaster.getMemberId());

			contentValues.put(PhotoMasterColumn.IMAGE, memberPhotoMaster.getImage());

			contentValues.put(PhotoMasterColumn.UPDATEVALUES, memberPhotoMaster.getUpdateValues());

			long result = sqLiteDatabase.insert(DataHelper.TABLES.TABLE_MEMBER_PHOTO_MASTER, null, contentValues);
			return result != -1;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			closeDatabase();
		}
		return false;
	}

	public static boolean getMemberPhotoMasterValues() {
		try {
			openDatabase();

			cursor = sqLiteDatabase.query(DataHelper.TABLES.TABLE_MEMBER_PHOTO_MASTER, null, null, null, null, null,
					null);
			if (cursor.moveToFirst()) {
				String uniqueid = null;
				String username = null;
				String ngoid = null;
				String groupid = null;
				String memberid = null;
				byte[] image = null;
				String updatevalues = null;

				int localId = -1;
				do {
					localId = cursor.getInt(cursor.getColumnIndex(PhotoMasterColumn.M_ID));
					uniqueid = cursor.getString(cursor.getColumnIndex(PhotoMasterColumn.UNIQUEID));
					username = cursor.getString(cursor.getColumnIndex(PhotoMasterColumn.USERNAME));

					ngoid = cursor.getString(cursor.getColumnIndex(PhotoMasterColumn.NGOID));
					groupid = cursor.getString(cursor.getColumnIndex(PhotoMasterColumn.GROUPID));
					memberid = cursor.getString(cursor.getColumnIndex(PhotoMasterColumn.MEMBERID));

					image = cursor.getBlob(cursor.getColumnIndex(PhotoMasterColumn.IMAGE));

					updatevalues = cursor.getString(cursor.getColumnIndex(PhotoMasterColumn.UPDATEVALUES));

					memberPhotoMasters.add(new MemberPhotoMaster(localId, uniqueid, username, ngoid, groupid, memberid,
							image, updatevalues));
				} while (cursor.moveToNext());
			}

			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			cursor.close();
			closeDatabase();
		}
		return true;
	}

	public static List<MemberPhotoMaster> getImageDetails() {
		try {
			openDatabase();
			final List<MemberPhotoMaster> memberPhotoMasters = new ArrayList<MemberPhotoMaster>();
			cursor = sqLiteDatabase.query(DataHelper.TABLES.TABLE_MEMBER_PHOTO_MASTER, null, null, null, null, null,
					null);
			if (cursor.moveToFirst()) {
				String uniqueid = null;
				String username = null;
				String ngoid = null;
				String groupid = null;
				String memberid = null;
				byte[] image = null;
				String updatevalues = null;

				int localId = -1;
				do {
					localId = cursor.getInt(cursor.getColumnIndex(PhotoMasterColumn.M_ID));
					uniqueid = cursor.getString(cursor.getColumnIndex(PhotoMasterColumn.UNIQUEID));
					username = cursor.getString(cursor.getColumnIndex(PhotoMasterColumn.USERNAME));

					ngoid = cursor.getString(cursor.getColumnIndex(PhotoMasterColumn.NGOID));
					groupid = cursor.getString(cursor.getColumnIndex(PhotoMasterColumn.GROUPID));
					memberid = cursor.getString(cursor.getColumnIndex(PhotoMasterColumn.MEMBERID));

					image = cursor.getBlob(cursor.getColumnIndex(PhotoMasterColumn.IMAGE));

					updatevalues = cursor.getString(cursor.getColumnIndex(PhotoMasterColumn.UPDATEVALUES));

					memberPhotoMasters.add(new MemberPhotoMaster(localId, uniqueid, username, ngoid, groupid, memberid,
							image, updatevalues));
				} while (cursor.moveToNext());
			}

			return memberPhotoMasters;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			cursor.close();
			closeDatabase();
		}
		return null;
	}

	public static boolean updateMemberPhoto(MemberPhotoMasterUpdate memberPhotoMasterUpdate) {

		try {
			openDatabase();
			// Define the updated row content.
			ContentValues updatedValues = new ContentValues();

			updatedValues.put(PhotoMasterColumn.UPDATEVALUES, MemberPhotoMasterUpdate.getMasterUpdateValues());

			sqLiteDatabase.update(DataHelper.TABLES.TABLE_MEMBER_PHOTO_MASTER, updatedValues,
					PhotoMasterColumn.UNIQUEID + "=" + MemberPhotoMasterUpdate.getMasterUniqueId(), null);

			return false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			closeDatabase();
		}
		return true;
	}

	public static boolean deleteAadharcard(MemberPhotoDelete memberPhotoDelete) {
		try {
			DataHelper dataHelper = DataHelper.getInstance(EShaktiApplication.getInstance());
			SQLiteDatabase sqLiteDatabase = dataHelper.getWritableDatabase();

			sqLiteDatabase.delete(DataHelper.TABLES.TABLE_MEMBER_PHOTO_MASTER, "Uniqueid=?",
					new String[] { MemberPhotoDelete.getUniqueId() });

			sqLiteDatabase.close();
			Log.e("Deleteeeeeeeeeee", "Checking6767676");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			closeDatabase();
		}
		return true;
	}

}
