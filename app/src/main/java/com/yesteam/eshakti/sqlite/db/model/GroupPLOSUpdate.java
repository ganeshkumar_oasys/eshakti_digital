package com.yesteam.eshakti.sqlite.db.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class GroupPLOSUpdate implements Serializable {

	private static String mGroupId;
	private static String mGroupResponse;
	private static String mGroupPLoutstanding;

	public GroupPLOSUpdate(final String grp_groupId,
			final String grp_groupresponse, final String grp_groupMLOS) {

		GroupPLOSUpdate.mGroupId = grp_groupId;
		GroupPLOSUpdate.mGroupResponse = grp_groupresponse;
		GroupPLOSUpdate.mGroupPLoutstanding = grp_groupMLOS;
	}

	public static String getGroupId() {
		return mGroupId;
	}

	public static String getGroupResponse() {
		return mGroupResponse;
	}

	public static String getGroupMLoutstanding() {
		return mGroupPLoutstanding;
	}

}
