package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class GroupNameResponse extends BaseResponse {

	public GroupNameResponse(final TransactionResult transactionResult) {
		super(transactionResult);
	}

	public GroupNameResponse(final boolean result) {
		super(TransactionResult.SUCCESS);
	}
}
