package com.yesteam.eshakti.sqlite.db.transaction.offline;

import com.yesteam.eshakti.sqlite.database.GroupDetailsProvider;
import com.yesteam.eshakti.sqlite.database.response.GroupMLOSUpdateResponse;
import com.yesteam.eshakti.sqlite.db.model.GroupMLUpdate;
import com.yesteam.eshakti.sqlite.db.transactions.AbstractTransaction;

public class GroupMLOSUpdateTransaction extends
		AbstractTransaction<GroupMLUpdate, GroupMLOSUpdateResponse> {

	@Override
	public int onPreExecute() {
		return EXECUTE_IN_WORKER_THREAD;
	}

	@Override
	public void run() {

		boolean value = GroupDetailsProvider.updateGroupMLOS(getParams());

		GroupMLOSUpdateResponse transResponse = new GroupMLOSUpdateResponse(
				value);

		deliverResponse(transResponse);

	}

}