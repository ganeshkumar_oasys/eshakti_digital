package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class TransactionUpdateResponse extends BaseResponse {

	public TransactionUpdateResponse(final TransactionResult transactionResult) {
		super(transactionResult);
	}

	public TransactionUpdateResponse(final boolean result) {
		super(TransactionResult.SUCCESS);
	}
}