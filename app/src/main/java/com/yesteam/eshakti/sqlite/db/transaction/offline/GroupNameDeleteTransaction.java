package com.yesteam.eshakti.sqlite.db.transaction.offline;

import java.util.List;

import com.yesteam.eshakti.sqlite.database.GroupProvider;
import com.yesteam.eshakti.sqlite.database.response.GroupDeleteResponse;
import com.yesteam.eshakti.sqlite.db.transactions.AbstractTransaction;


public class GroupNameDeleteTransaction extends
		AbstractTransaction<Integer, List<GroupDeleteResponse>> {

	@Override
	public int onPreExecute() {
		return EXECUTE_IN_WORKER_THREAD;
	}

	@Override
	public void run() {
		GroupProvider.deleteGroupNames();

	}
}
