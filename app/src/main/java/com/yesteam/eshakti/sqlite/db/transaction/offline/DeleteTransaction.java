package com.yesteam.eshakti.sqlite.db.transaction.offline;

import com.yesteam.eshakti.sqlite.database.TransactionProvider;
import com.yesteam.eshakti.sqlite.database.response.TransDeleteResponse;
import com.yesteam.eshakti.sqlite.db.model.TransDelete;
import com.yesteam.eshakti.sqlite.db.transactions.AbstractTransaction;

public class DeleteTransaction extends AbstractTransaction<TransDelete, TransDeleteResponse> {

	@Override
	public int onPreExecute() {
		return EXECUTE_IN_WORKER_THREAD;
	}

	@Override
	public void run() {

		boolean value = TransactionProvider.deleteTransaction(getParams());

		TransDeleteResponse transResponse = new TransDeleteResponse(value);
		deliverResponse(transResponse);
	}
}
