package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class LoanAccountDataUpdateResponse extends BaseResponse {

	public LoanAccountDataUpdateResponse(final TransactionResult transactionResult) {
		super(transactionResult);
	}

	public LoanAccountDataUpdateResponse(final boolean result) {
		super(TransactionResult.SUCCESS);
	}
}
