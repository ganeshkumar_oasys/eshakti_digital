package com.yesteam.eshakti.sqlite.db.model;

import java.io.Serializable;

public class AadharcardDelete implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5601963273562389950L;
	private static String mUniqueId_delete;

	public AadharcardDelete(final String grp_uniqueid) {

		AadharcardDelete.mUniqueId_delete = grp_uniqueid;

	}

	public static String getUniqueId() {
		return mUniqueId_delete;
	}
}