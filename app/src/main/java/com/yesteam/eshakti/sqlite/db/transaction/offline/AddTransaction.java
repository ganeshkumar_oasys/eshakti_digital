package com.yesteam.eshakti.sqlite.db.transaction.offline;

import com.yesteam.eshakti.sqlite.database.TransactionProvider;
import com.yesteam.eshakti.sqlite.database.response.TransactionResponse;
import com.yesteam.eshakti.sqlite.db.model.Transaction;
import com.yesteam.eshakti.sqlite.db.transactions.AbstractTransaction;

import android.util.Log;

public class AddTransaction extends AbstractTransaction<Transaction, TransactionResponse> {

	@Override
	public int onPreExecute() {
		return EXECUTE_IN_WORKER_THREAD;
	}

	@Override
	public void run() {
		Log.i("RUN", "Execute");
		boolean value = TransactionProvider.addTransactionValuers(getParams());
		Log.i("RUN", "Execute12122");
		TransactionResponse transResponse = new TransactionResponse(value);
		deliverResponse(transResponse);
	}

}
