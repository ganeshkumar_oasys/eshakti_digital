package com.yesteam.eshakti.sqlite.database;

import java.util.ArrayList;
import java.util.List;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.yesteam.eshakti.sqlite.database.GroupDetailsProvider.GroupMasterColumn;
import com.yesteam.eshakti.sqlite.db.model.BankDetails;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class BankDetailsProvider {
	private static SQLiteDatabase sqLiteDatabase;
	private static DataHelper dataHelper;
	static Cursor cursor;

	public BankDetailsProvider(Activity activity) {
		dataHelper = new DataHelper(activity);
	}

	public static void openDatabase() {
		dataHelper = new DataHelper(EShaktiApplication.getInstance());
		dataHelper.onOpen(sqLiteDatabase);
		sqLiteDatabase = dataHelper.getWritableDatabase();

	}

	public static void closeDatabase() {
		// dataHelper.close();
		if (sqLiteDatabase != null && sqLiteDatabase.isOpen()) {
			sqLiteDatabase.close();
		}
	}

	public interface BankMasterColumn {
		public static final String B_ID = "local_Id";
		public static final String BANKNAME = "BankName";
		public static final String BRANCHNAME = "BranchName";
		public static final String IFSCCODE = "IfscCode";

	}

	public static final String CREATE_BANK_DETAILS_TABLE = "create table " + DataHelper.TABLES.TABLE_BANK_MASTER + "("
			+ BankMasterColumn.B_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + BankMasterColumn.BANKNAME + " TEXT,"
			+ BankMasterColumn.BRANCHNAME + " TEXT," + BankMasterColumn.IFSCCODE + " text" + ")";

	public static boolean addBankMasterDetails(BankDetails bankDetails) {
		try {

			openDatabase();
			ContentValues contentValues = new ContentValues();
			contentValues.put(BankMasterColumn.BANKNAME, bankDetails.getBankName());
			contentValues.put(BankMasterColumn.BRANCHNAME, bankDetails.getBranchName());
			contentValues.put(BankMasterColumn.IFSCCODE, bankDetails.getIfscCode());

			long result = sqLiteDatabase.insert(DataHelper.TABLES.TABLE_BANK_MASTER, null, contentValues);
			return result != -1;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			closeDatabase();
		}
		return false;
	}

	public static List<BankDetails> getBankDetails() {
		try {
			openDatabase();
			final List<BankDetails> _BankMaster = new ArrayList<BankDetails>();
			cursor = sqLiteDatabase.query(DataHelper.TABLES.TABLE_BANK_MASTER, null, null, null, null, null, null);
			if (cursor.moveToFirst()) {
				String _BankName = null;
				String _BranchName = null;
				String _IfscCode = null;

				int localId = -1;
				do {
					localId = cursor.getInt(cursor.getColumnIndex(GroupMasterColumn.G_ID));
					_BankName = cursor.getString(cursor.getColumnIndex(BankMasterColumn.BANKNAME));
					_BranchName = cursor.getString(cursor.getColumnIndex(BankMasterColumn.BRANCHNAME));
					_IfscCode = cursor.getString(cursor.getColumnIndex(BankMasterColumn.IFSCCODE));

					_BankMaster.add(new BankDetails(localId, _BankName, _BranchName, _IfscCode));
				} while (cursor.moveToNext());
			}

			return _BankMaster;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			cursor.close();
			closeDatabase();
		}
		return null;
	}

	public static List<BankDetails> getBranchMasterDetails() {
		try {
			openDatabase();
			final List<BankDetails> _BranchNameMaster = new ArrayList<BankDetails>();
			Cursor cursor = null;

			cursor = sqLiteDatabase.query(DataHelper.TABLES.TABLE_BANK_MASTER, null, BankMasterColumn.BANKNAME + " =?",
					new String[] { EShaktiApplication.getShg_selected_bankName() }, null, null, null);

			if (cursor.moveToFirst()) {

				String bankName = null;
				String branchName = null;
				String ifscCode = null;
				int localId = -1;
				do {
					localId = cursor.getInt(cursor.getColumnIndex(BankMasterColumn.B_ID));

					bankName = cursor.getString(cursor.getColumnIndex(BankMasterColumn.BANKNAME));
					branchName = cursor.getString(cursor.getColumnIndex(BankMasterColumn.BRANCHNAME));
					ifscCode = cursor.getString(cursor.getColumnIndex(BankMasterColumn.IFSCCODE));

					_BranchNameMaster.add(new BankDetails(localId, bankName, branchName, ifscCode));
				} while (cursor.moveToNext());
			}
			Log.e("Login Cursor Count", cursor.getCount() + "");

			cursor.close();
			return _BranchNameMaster;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

			closeDatabase();
		}
		return null;
	}

	public static void deleteBankDetails() {

		try {
			openDatabase();
			sqLiteDatabase.delete(DataHelper.TABLES.TABLE_BANK_MASTER, null, null);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			closeDatabase();
		}
	}

}
