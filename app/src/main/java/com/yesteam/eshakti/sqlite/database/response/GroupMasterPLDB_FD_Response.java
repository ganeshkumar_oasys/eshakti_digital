package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class GroupMasterPLDB_FD_Response extends BaseResponse {

	public GroupMasterPLDB_FD_Response(final TransactionResult transactionResult) {
		super(transactionResult);
	}

	public GroupMasterPLDB_FD_Response(final boolean result) {
		super(TransactionResult.SUCCESS);
	}

}
