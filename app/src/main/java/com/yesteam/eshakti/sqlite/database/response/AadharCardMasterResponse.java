package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class AadharCardMasterResponse extends BaseResponse {

	public AadharCardMasterResponse(final TransactionResult transactionResult) {
		super(transactionResult);
	}

	public AadharCardMasterResponse(final boolean result) {
		super(TransactionResult.SUCCESS);
	}
}
