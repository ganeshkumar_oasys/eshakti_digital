package com.yesteam.eshakti.sqlite.db.model;

import java.io.Serializable;

public class LoanAccountUpdate implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -753971567836487791L;
	private static String mGroupId;
	private static String mGroupLoanAccount;

	public LoanAccountUpdate(final String grp_groupId, final String grp_transloanacc) {

		LoanAccountUpdate.mGroupId = grp_groupId;
		LoanAccountUpdate.mGroupLoanAccount = grp_transloanacc;
	}

	public static String getGroupId() {
		return mGroupId;
	}

	public static String getTransLoanacc() {
		return mGroupLoanAccount;
	}
}
