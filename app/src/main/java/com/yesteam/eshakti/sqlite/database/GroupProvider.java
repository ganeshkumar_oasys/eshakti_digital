package com.yesteam.eshakti.sqlite.database;

import java.util.ArrayList;
import java.util.List;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.yesteam.eshakti.sqlite.db.model.GroupResponseUpdate;
import com.yesteam.eshakti.sqlite.db.model.GroupTempDBLastTransDate;
import com.yesteam.eshakti.sqlite.db.model.Groupdetails;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class GroupProvider {
	private static SQLiteDatabase sqLiteDatabase;
	private static DataHelper dataHelper;
	static Cursor cursor;

	public GroupProvider(Activity activity) {
		dataHelper = new DataHelper(activity);
	}

	public static void openDatabase() {
		dataHelper = new DataHelper(EShaktiApplication.getInstance());
		dataHelper.onOpen(sqLiteDatabase);
		sqLiteDatabase = dataHelper.getWritableDatabase();
	}

	public static void closeDatabase() {

		if (sqLiteDatabase != null && sqLiteDatabase.isOpen()) {
			sqLiteDatabase.close();
		}
		// dataHelper.close();
	}

	interface GroupColumn {
		public static final String G_ID = "local_Id";
		public static final String TRAINERUSERID = "Traineruserid";
		public static final String NGOID = "Ngoid";
		public static final String GROUPID = "Groupid";
		public static final String GROUPNAME = "Groupname";
		public static final String GROUPRESPONSE = "Groupresponse";
		public static final String GROUP_LAST_TRANSACTIONDATE = "Group_Last_Transaction_Date";
		public static final String GROUP_SYSTEM_ENTRY_DATE = "System_Entry_Date";

	}

	public static final String CREATE_GROUP_NAME_TABLE = "create table " + DataHelper.TABLES.TABLE_GROUP_NAME_DETAILS
			+ "(" + GroupColumn.G_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + GroupColumn.TRAINERUSERID + " TEXT,"
			+ GroupColumn.NGOID + " text," + GroupColumn.GROUPID + " text," + GroupColumn.GROUPNAME + " text,"
			+ GroupColumn.GROUPRESPONSE + " text" + ")";

	public static final String CREATE_GROUP_NAME_TABLE_NEW = "create table "
			+ DataHelper.TABLES.TABLE_GROUP_NAME_DETAILS + "(" + GroupColumn.G_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + GroupColumn.TRAINERUSERID + " TEXT," + GroupColumn.NGOID
			+ " text," + GroupColumn.GROUPID + " text," + GroupColumn.GROUPNAME + " text," + GroupColumn.GROUPRESPONSE
			+ " text," + GroupColumn.GROUP_LAST_TRANSACTIONDATE + " text," + GroupColumn.GROUP_SYSTEM_ENTRY_DATE
			+ " text" + ")";

	public static boolean addGroupDetails(Groupdetails groupdetails) {
		try {
			openDatabase();
			ContentValues contentValues = new ContentValues();
			contentValues.put(GroupColumn.TRAINERUSERID, groupdetails.getTrainerUserId());
			contentValues.put(GroupColumn.NGOID, groupdetails.getNgoId());
			contentValues.put(GroupColumn.GROUPID, groupdetails.getGroupId());
			contentValues.put(GroupColumn.GROUPNAME, groupdetails.getGroupname());
			contentValues.put(GroupColumn.GROUPRESPONSE, groupdetails.getGroupnameresponse());
			contentValues.put(GroupColumn.GROUP_LAST_TRANSACTIONDATE, groupdetails.getGroupLastTransactiondate());
			contentValues.put(GroupColumn.GROUP_SYSTEM_ENTRY_DATE, groupdetails.getGroupEntrySystemDate());

			long result = sqLiteDatabase.insert(DataHelper.TABLES.TABLE_GROUP_NAME_DETAILS, null, contentValues);

			return result != -1;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// LoginProvider.getLoginDetails();
			closeDatabase();
		}
		return false;
	}

	public static List<Groupdetails> getGroupDetails() {
		try {
			openDatabase();
			final List<Groupdetails> groupdetails = new ArrayList<Groupdetails>();
			cursor = sqLiteDatabase.query(DataHelper.TABLES.TABLE_GROUP_NAME_DETAILS, null, null, null, null, null,
					null);
			if (cursor.moveToFirst()) {
				String grp_trainerUserid = null;
				String grp_ngoId = null;
				String grp_GroupId = null;
				String grp_GroupName = null;
				String grp_Groupresponse = null;
				String grp_LastTransactionDate = null;
				String grp_SystemEntryDate = null;

				int localId = -1;
				do {
					localId = cursor.getInt(cursor.getColumnIndex(GroupColumn.G_ID));
					grp_trainerUserid = cursor.getString(cursor.getColumnIndex(GroupColumn.TRAINERUSERID));

					grp_ngoId = cursor.getString(cursor.getColumnIndex(GroupColumn.NGOID));
					grp_GroupId = cursor.getString(cursor.getColumnIndex(GroupColumn.GROUPID));
					grp_GroupName = cursor.getString(cursor.getColumnIndex(GroupColumn.GROUPNAME));
					grp_Groupresponse = cursor.getString(cursor.getColumnIndex(GroupColumn.GROUPRESPONSE));
					grp_LastTransactionDate = cursor
							.getString(cursor.getColumnIndex(GroupColumn.GROUP_LAST_TRANSACTIONDATE));
					grp_SystemEntryDate = cursor.getString(cursor.getColumnIndex(GroupColumn.GROUP_SYSTEM_ENTRY_DATE));

					groupdetails.add(new Groupdetails(localId, grp_trainerUserid, grp_ngoId, grp_GroupId, grp_GroupName,
							grp_Groupresponse, grp_LastTransactionDate, grp_SystemEntryDate));
				} while (cursor.moveToNext());
			}

			return groupdetails;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			cursor.close();
			closeDatabase();
		}
		return null;
	}

	public static boolean updateGroupResponse(GroupResponseUpdate groupResponseUpdate) {
		try {
			openDatabase();

			// Define the updated row content.
			ContentValues updatedValues = new ContentValues();
			// Assign values for each row.

			updatedValues.put(GroupColumn.GROUPRESPONSE, GroupResponseUpdate.getGroupResponse());

			sqLiteDatabase.update(DataHelper.TABLES.TABLE_GROUP_NAME_DETAILS, updatedValues,
					GroupColumn.GROUPID + "=" + GroupResponseUpdate.getGroupId(), null);

			return false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			closeDatabase();
		}

		return true;

	}

	public static boolean getSinlgeGroupMaster() {

		try {

			if (EShaktiApplication.getLastTransDate_GroupId() != null) {
				openDatabase();

				Cursor cursor = sqLiteDatabase.query(DataHelper.TABLES.TABLE_GROUP_NAME_DETAILS, null,
						GroupColumn.GROUPID + " =?", new String[] { EShaktiApplication.getLastTransDate_GroupId() },
						null, null, null);

				cursor.moveToFirst();
				String grp_trainerUserid = null;
				String grp_ngoId = null;
				String grp_GroupId = null;
				String grp_GroupName = null;
				String grp_Groupresponse = null;
				String grp_LastTransactionDate = null;
				String grp_SystemEntryDate = null;

				int localId = -1;

				if (cursor.getCount() > 0) {

					localId = cursor.getInt(cursor.getColumnIndex(GroupColumn.G_ID));
					grp_trainerUserid = cursor.getString(cursor.getColumnIndex(GroupColumn.TRAINERUSERID));

					grp_ngoId = cursor.getString(cursor.getColumnIndex(GroupColumn.NGOID));
					grp_GroupId = cursor.getString(cursor.getColumnIndex(GroupColumn.GROUPID));
					grp_GroupName = cursor.getString(cursor.getColumnIndex(GroupColumn.GROUPNAME));
					grp_Groupresponse = cursor.getString(cursor.getColumnIndex(GroupColumn.GROUPRESPONSE));
					grp_LastTransactionDate = cursor
							.getString(cursor.getColumnIndex(GroupColumn.GROUP_LAST_TRANSACTIONDATE));
					grp_SystemEntryDate = cursor.getString(cursor.getColumnIndex(GroupColumn.GROUP_SYSTEM_ENTRY_DATE));

					new Groupdetails(localId, grp_trainerUserid, grp_ngoId, grp_GroupId, grp_GroupName,
							grp_Groupresponse, grp_LastTransactionDate, grp_SystemEntryDate);

					System.out.println("grp_SystemEntryDate ----------->>>" + grp_SystemEntryDate);

					System.out.println("grp_LastTransactionDate------>>>>>" + grp_LastTransactionDate);

					EShaktiApplication.setLastTransDate_DB(grp_LastTransactionDate);
					EShaktiApplication.setSystemEntryDate(grp_SystemEntryDate);

					cursor.close();
					closeDatabase();
				} else {

					new Groupdetails(localId, grp_trainerUserid, grp_ngoId, grp_GroupId, grp_GroupName,
							grp_Groupresponse, grp_LastTransactionDate, grp_SystemEntryDate);

					closeDatabase();
				}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return true;
	}

	public static boolean updateGroupLastTransDateResponse() {
		try {
			openDatabase();

			// Define the updated row content.
			ContentValues updatedValues = new ContentValues();
			// Assign values for each row.

			updatedValues.put(GroupColumn.GROUP_LAST_TRANSACTIONDATE, GroupTempDBLastTransDate.getGroupLastTransDate());
			updatedValues.put(GroupColumn.GROUP_SYSTEM_ENTRY_DATE, GroupTempDBLastTransDate.getGroupSystemEntryDate());

			System.out.println("grp_SystemEntryDate_Update" + GroupTempDBLastTransDate.getGroupSystemEntryDate());

			System.out.println("grp_LastTransactionDate_Update" + GroupTempDBLastTransDate.getGroupLastTransDate());
			Log.e("Group Id", EShaktiApplication.getLastTransDate_GroupId());

			sqLiteDatabase.update(DataHelper.TABLES.TABLE_GROUP_NAME_DETAILS, updatedValues,
					GroupColumn.GROUPID + "=" + EShaktiApplication.getLastTransDate_GroupId(), null);

			return false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			closeDatabase();
		}

		return true;

	}

	public static void deleteGroupNames() {
		try {
			openDatabase();

			sqLiteDatabase.delete(DataHelper.TABLES.TABLE_GROUP_NAME_DETAILS, null, null);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

			closeDatabase();
		}
	}

}