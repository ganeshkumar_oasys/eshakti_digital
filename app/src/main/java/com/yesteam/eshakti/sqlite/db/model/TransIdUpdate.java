package com.yesteam.eshakti.sqlite.db.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class TransIdUpdate implements Serializable {

	private static String mGroupId;
	private static String mGroupLastTransId;

	public TransIdUpdate(final String grp_groupId,
			final String grp_transactionid) {

		TransIdUpdate.mGroupId = grp_groupId;
		TransIdUpdate.mGroupLastTransId = grp_transactionid;
	}

	public static String getGroupId() {
		return mGroupId;
	}

	public static String getTransactionId() {
		return mGroupLastTransId;
	}
}
