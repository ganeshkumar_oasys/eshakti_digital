package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class GetLoanAccountNoResponse extends BaseResponse {

	public GetLoanAccountNoResponse(final TransactionResult transactionResult) {
		super(transactionResult);
		// TODO Auto-generated constructor stub
	}

	public GetLoanAccountNoResponse(final boolean result) {
		super(TransactionResult.SUCCESS);
	}

}
