package com.yesteam.eshakti.sqlite.db.model;

import java.io.Serializable;

public class GroupTempDBLastTransDate implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4450369885775111938L;
	private static String mGroupId;
	private static String mGroupLastTransDate;

	private static String mGroupSystemEntryDate;

	public GroupTempDBLastTransDate(final String grp_groupId, final String grp_lastTransDate,
			final String grp_SystemEntryDate) {

		GroupTempDBLastTransDate.setGroupId(grp_groupId);
		GroupTempDBLastTransDate.setGroupLastTransDate(grp_lastTransDate);
		GroupTempDBLastTransDate.setGroupSystemEntryDate(grp_SystemEntryDate);

	}

	public static String getGroupId() {
		return mGroupId;
	}

	public static String getGroupLastTransDate() {
		return mGroupLastTransDate;
	}

	public static void setGroupLastTransDate(String mGroupLastTransDate) {
		GroupTempDBLastTransDate.mGroupLastTransDate = mGroupLastTransDate;
	}

	public static String getGroupSystemEntryDate() {
		return mGroupSystemEntryDate;
	}

	public static void setGroupSystemEntryDate(String mGroupSystemEntryDate) {
		GroupTempDBLastTransDate.mGroupSystemEntryDate = mGroupSystemEntryDate;
	}

	private static void setGroupId(String mGroupId) {
		GroupTempDBLastTransDate.mGroupId = mGroupId;
	}

}
