package com.yesteam.eshakti.sqlite.db.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class TransactionUpdate implements Serializable {

	private static String mUniqueId;
	private static String mUpdateTransactionvalues;

	public TransactionUpdate(final String grp_uniqueId,
			final String grp_updatetransvalues) {

		TransactionUpdate.mUniqueId = grp_uniqueId;
		TransactionUpdate.mUpdateTransactionvalues = grp_updatetransvalues;
	}

	public static String getUniqueId() {
		return mUniqueId;
	}

	public static String getTransactionValues() {
		return mUpdateTransactionvalues;
	}
}
