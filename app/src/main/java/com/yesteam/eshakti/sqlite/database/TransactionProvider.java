package com.yesteam.eshakti.sqlite.database;

import java.util.ArrayList;
import java.util.List;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.yesteam.eshakti.appConstants.TransactionOffConstants;
import com.yesteam.eshakti.sqlite.db.model.GetTransactionSingle;
import com.yesteam.eshakti.sqlite.db.model.GetTransactionSinglevalues;
import com.yesteam.eshakti.sqlite.db.model.TransDelete;
import com.yesteam.eshakti.sqlite.db.model.Transaction;
import com.yesteam.eshakti.sqlite.db.model.TransactionSav_VSavUpdate;
import com.yesteam.eshakti.sqlite.db.model.TransactionUpdate;
import com.yesteam.eshakti.sqlite.db.model.Transaction_UniqueIdSingle;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.view.fragment.Offline_ReportDateListFragment;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class TransactionProvider {
	private static SQLiteDatabase sqLiteDatabase;
	private static DataHelper dataHelper;

	public TransactionProvider(Activity activity) {
		dataHelper = new DataHelper(activity);
	}

	public static void openDatabase() {
		dataHelper = new DataHelper(EShaktiApplication.getInstance());
		dataHelper.onOpen(sqLiteDatabase);
		sqLiteDatabase = dataHelper.getWritableDatabase();
	}

	public static void closeDatabase() {

		dataHelper.close();

	}

	public interface TransactionColumn {
		public static final String T_ID = "local_Id";
		public static final String TRAINERUSERID = "Traineruserid";
		public static final String USERNAME = "Username";
		public static final String NGOID = "Ngoid";
		public static final String GROUPID = "Groupid";
		public static final String UNIQUEID = "Uniqueid";
		public static final String TRANSACTIONID = "Transactionid";
		public static final String TRANSACTIONDATE = "Transactiondate";
		public static final String SAVINGS = "Savings";
		public static final String VOLUNTEERSAVINGS = "Volunteersavings";
		public static final String PERSONALLOANREPAYMENT = "Personalloanrepayment";
		public static final String MEMBERLOANREPAYMENT = "Memberloanrepayment";
		public static final String EXPENSES = "Expenses";
		public static final String OTHERINCOME = "Otherincome";
		public static final String SUBCRIPTIONINCOME = "Subcriptionincome";
		public static final String PENALTYINCOME = "Penaltyincome";
		public static final String DONATIONINCOME = "Donationincome";
		public static final String FEDERATIONINCOME = "Federationincome";
		public static final String GROUPLOANREPAYMENT = "Grouploanrepayment";
		public static final String BANKDEPOSIT = "Bankdeposit";
		public static final String BANKFIXEDDEPOSIT = "Bankfixeddeposit";
		public static final String PERSONALLOANDISBURSEMENT = "Personalloandisbursement";
		public static final String ATTENDANCE = "Attendance";
		public static final String MINUTESOFMEETING = "Minutesofmeeting";
		public static final String AUDITTING = "Auditting";
		public static final String TRAINING = "Training";
		public static final String LOANWITHDRAWAL = "LoanWithdrawal";
		public static final String ACCTOACCTRANSFER = "AccToAccTransfer";
		public static final String ACCTOLOANACCTRANSFER = "AccTOLoanAccTransfer";
		public static final String SEEDFUND = "SeedFund";

	}

	public static final String CREATE_TRANSACTION_VALUES_TABLE = "create table "
			+ DataHelper.TABLES.TABLE_TRANSACTION_VALUES + "(" + TransactionColumn.T_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + TransactionColumn.TRAINERUSERID + " TEXT,"
			+ TransactionColumn.USERNAME + " text," + TransactionColumn.NGOID + " text," + TransactionColumn.GROUPID
			+ " text," + TransactionColumn.UNIQUEID + " text," + TransactionColumn.TRANSACTIONID + " text,"
			+ TransactionColumn.TRANSACTIONDATE + " text," + TransactionColumn.SAVINGS + " text,"
			+ TransactionColumn.VOLUNTEERSAVINGS + " text," + TransactionColumn.PERSONALLOANREPAYMENT + " text,"
			+ TransactionColumn.MEMBERLOANREPAYMENT + " text," + TransactionColumn.EXPENSES + " text,"
			+ TransactionColumn.OTHERINCOME + " text," + TransactionColumn.SUBCRIPTIONINCOME + " text,"
			+ TransactionColumn.PENALTYINCOME + " text," + TransactionColumn.DONATIONINCOME + " text,"
			+ TransactionColumn.FEDERATIONINCOME + " text," + TransactionColumn.GROUPLOANREPAYMENT + " text,"
			+ TransactionColumn.BANKDEPOSIT + " text," + TransactionColumn.BANKFIXEDDEPOSIT + " text,"
			+ TransactionColumn.PERSONALLOANDISBURSEMENT + " text," + TransactionColumn.ATTENDANCE + " text,"
			+ TransactionColumn.MINUTESOFMEETING + " text," + TransactionColumn.AUDITTING + " text,"
			+ TransactionColumn.TRAINING + " text," + TransactionColumn.LOANWITHDRAWAL + " text,"
			+ TransactionColumn.ACCTOACCTRANSFER + " text" + ")";

	public static final String CREATE_TRANSACTION_VALUES_TABLE_NEW = "create table "
			+ DataHelper.TABLES.TABLE_TRANSACTION_VALUES + "(" + TransactionColumn.T_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + TransactionColumn.TRAINERUSERID + " TEXT,"
			+ TransactionColumn.USERNAME + " text," + TransactionColumn.NGOID + " text," + TransactionColumn.GROUPID
			+ " text," + TransactionColumn.UNIQUEID + " text," + TransactionColumn.TRANSACTIONID + " text,"
			+ TransactionColumn.TRANSACTIONDATE + " text," + TransactionColumn.SAVINGS + " text,"
			+ TransactionColumn.VOLUNTEERSAVINGS + " text," + TransactionColumn.PERSONALLOANREPAYMENT + " text,"
			+ TransactionColumn.MEMBERLOANREPAYMENT + " text," + TransactionColumn.EXPENSES + " text,"
			+ TransactionColumn.OTHERINCOME + " text," + TransactionColumn.SUBCRIPTIONINCOME + " text,"
			+ TransactionColumn.PENALTYINCOME + " text," + TransactionColumn.DONATIONINCOME + " text,"
			+ TransactionColumn.FEDERATIONINCOME + " text," + TransactionColumn.GROUPLOANREPAYMENT + " text,"
			+ TransactionColumn.BANKDEPOSIT + " text," + TransactionColumn.BANKFIXEDDEPOSIT + " text,"
			+ TransactionColumn.PERSONALLOANDISBURSEMENT + " text," + TransactionColumn.ATTENDANCE + " text,"
			+ TransactionColumn.MINUTESOFMEETING + " text," + TransactionColumn.AUDITTING + " text,"
			+ TransactionColumn.TRAINING + " text," + TransactionColumn.LOANWITHDRAWAL + " text,"
			+ TransactionColumn.ACCTOACCTRANSFER + " text," + TransactionColumn.ACCTOLOANACCTRANSFER + " text,"
			+ TransactionColumn.SEEDFUND + " text" + ")";

	public static boolean addTransactionValuers(Transaction transaction) {
		try {
			openDatabase();
			ContentValues contentValues = new ContentValues();
			contentValues.put(TransactionColumn.TRAINERUSERID, transaction.getTrainerUserId());
			contentValues.put(TransactionColumn.USERNAME, transaction.getUsername());

			contentValues.put(TransactionColumn.NGOID, transaction.getNgoId());

			contentValues.put(TransactionColumn.GROUPID, transaction.getGroupId());
			contentValues.put(TransactionColumn.UNIQUEID, transaction.getUniqueId());

			contentValues.put(TransactionColumn.TRANSACTIONID, transaction.getTransactionId());

			contentValues.put(TransactionColumn.TRANSACTIONDATE, transaction.getTransactionDate());

			contentValues.put(TransactionColumn.SAVINGS, transaction.getSavings());

			contentValues.put(TransactionColumn.VOLUNTEERSAVINGS, transaction.getVolunteerSavings());

			contentValues.put(TransactionColumn.PERSONALLOANREPAYMENT, transaction.getPersonalloanRepayment());

			contentValues.put(TransactionColumn.MEMBERLOANREPAYMENT, transaction.getMemberloanRepayment());

			contentValues.put(TransactionColumn.EXPENSES, transaction.getExpenses());

			contentValues.put(TransactionColumn.OTHERINCOME, transaction.getOtherIncome());

			contentValues.put(TransactionColumn.SUBCRIPTIONINCOME, transaction.getSubscriptionIncome());

			contentValues.put(TransactionColumn.PENALTYINCOME, transaction.getPenaltyIncome());

			contentValues.put(TransactionColumn.DONATIONINCOME, transaction.getDonationIncome());

			contentValues.put(TransactionColumn.FEDERATIONINCOME, transaction.getFederationIncome());

			contentValues.put(TransactionColumn.GROUPLOANREPAYMENT, transaction.getGrouploanrepayment());

			contentValues.put(TransactionColumn.BANKDEPOSIT, transaction.getBankDeposit());

			contentValues.put(TransactionColumn.BANKFIXEDDEPOSIT, transaction.getBankFixedDeposit());

			contentValues.put(TransactionColumn.PERSONALLOANDISBURSEMENT, transaction.getPersonalloandisbursement());

			contentValues.put(TransactionColumn.ATTENDANCE, transaction.getAttendance());

			contentValues.put(TransactionColumn.MINUTESOFMEETING, transaction.getMinutesofmeeting());

			contentValues.put(TransactionColumn.PERSONALLOANREPAYMENT, transaction.getPersonalloanRepayment());

			contentValues.put(TransactionColumn.AUDITTING, transaction.getAuditting());

			contentValues.put(TransactionColumn.TRAINING, transaction.getTraining());

			contentValues.put(TransactionColumn.LOANWITHDRAWAL, transaction.getLoanWithdrawal());

			contentValues.put(TransactionColumn.ACCTOACCTRANSFER, transaction.getAcctoAccTransfer());

			contentValues.put(TransactionColumn.ACCTOLOANACCTRANSFER, transaction.getAccToLoanAccTransfer());

			contentValues.put(TransactionColumn.SEEDFUND, transaction.getSeedFund());

			long result = sqLiteDatabase.insert(DataHelper.TABLES.TABLE_TRANSACTION_VALUES, null, contentValues);
			PrefUtils.setStatus_Data_Insert_Success("true");
			return result != -1;
			
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeDatabase();
		}
		return false;
	}

	public static boolean getSinlgeTransaction(GetTransactionSingle getTransactionSingle) {

		try {
			openDatabase();

			Cursor cursor = sqLiteDatabase.query(DataHelper.TABLES.TABLE_TRANSACTION_VALUES, null,
					TransactionColumn.UNIQUEID + " =?", new String[] { GetTransactionSingle.getTransUniqueId() }, null,
					null, null);
			String trans_traineruserid = null;
			String trans_username = null;
			String trans_ngoid = null;
			String trans_groupid = null;
			String trans_uniqueid = null;
			String trans_transactionid = null;
			String trans_transactiondate = null;
			String trans_savings = null;
			String trans_VolunteerSavings = null;
			String trans_personalloanrepay = null;
			String trans_memberloanrepay = null;
			String trans_expense = null;
			String trans_otherincome = null;
			String trans_subcription = null;
			String trans_Penalty = null;
			String trans_donation = null;
			String trans_federation = null;
			String trans_grouploanrepay = null;
			String trans_bankdeposit = null;
			String trans_fixeddeposit = null;
			String trans_personalloandisbus = null;
			String trans_minutesofmeeting = null;
			String trans_attendance = null;
			String trans_auditting = null;
			String trans_training = null;
			String trans_loanwithdrawal = null;
			String trans_acctoacctransfer = null;
			String trans_acc_to_loan_acc_transfer = null;
			String trans_seedFund = null;

			if (cursor.moveToFirst()) {

				int localId = -1;
				do {
					localId = cursor.getInt(cursor.getColumnIndex(TransactionColumn.T_ID));
					trans_traineruserid = cursor.getString(cursor.getColumnIndex(TransactionColumn.TRAINERUSERID));

					trans_username = cursor.getString(cursor.getColumnIndex(TransactionColumn.USERNAME));
					trans_ngoid = cursor.getString(cursor.getColumnIndex(TransactionColumn.NGOID));
					trans_groupid = cursor.getString(cursor.getColumnIndex(TransactionColumn.GROUPID));

					trans_uniqueid = cursor.getString(cursor.getColumnIndex(TransactionColumn.UNIQUEID));

					trans_transactionid = cursor.getString(cursor.getColumnIndex(TransactionColumn.TRANSACTIONID));

					trans_transactiondate = cursor.getString(cursor.getColumnIndex(TransactionColumn.TRANSACTIONDATE));
					trans_savings = cursor.getString(cursor.getColumnIndex(TransactionColumn.SAVINGS));

					trans_VolunteerSavings = cursor
							.getString(cursor.getColumnIndex(TransactionColumn.VOLUNTEERSAVINGS));

					trans_personalloanrepay = cursor
							.getString(cursor.getColumnIndex(TransactionColumn.PERSONALLOANREPAYMENT));

					trans_memberloanrepay = cursor
							.getString(cursor.getColumnIndex(TransactionColumn.MEMBERLOANREPAYMENT));

					trans_expense = cursor.getString(cursor.getColumnIndex(TransactionColumn.EXPENSES));

					trans_otherincome = cursor.getString(cursor.getColumnIndex(TransactionColumn.OTHERINCOME));

					trans_subcription = cursor.getString(cursor.getColumnIndex(TransactionColumn.SUBCRIPTIONINCOME));
					trans_Penalty = cursor.getString(cursor.getColumnIndex(TransactionColumn.PENALTYINCOME));
					trans_donation = cursor.getString(cursor.getColumnIndex(TransactionColumn.DONATIONINCOME));
					trans_federation = cursor.getString(cursor.getColumnIndex(TransactionColumn.FEDERATIONINCOME));
					trans_grouploanrepay = cursor
							.getString(cursor.getColumnIndex(TransactionColumn.GROUPLOANREPAYMENT));
					trans_bankdeposit = cursor.getString(cursor.getColumnIndex(TransactionColumn.BANKDEPOSIT));
					trans_fixeddeposit = cursor.getString(cursor.getColumnIndex(TransactionColumn.BANKFIXEDDEPOSIT));
					trans_personalloandisbus = cursor
							.getString(cursor.getColumnIndex(TransactionColumn.PERSONALLOANDISBURSEMENT));
					trans_attendance = cursor.getString(cursor.getColumnIndex(TransactionColumn.ATTENDANCE));
					trans_minutesofmeeting = cursor
							.getString(cursor.getColumnIndex(TransactionColumn.MINUTESOFMEETING));
					trans_auditting = cursor.getString(cursor.getColumnIndex(TransactionColumn.AUDITTING));
					trans_training = cursor.getString(cursor.getColumnIndex(TransactionColumn.TRAINING));
					trans_loanwithdrawal = cursor.getString(cursor.getColumnIndex(TransactionColumn.LOANWITHDRAWAL));
					trans_acctoacctransfer = cursor
							.getString(cursor.getColumnIndex(TransactionColumn.ACCTOACCTRANSFER));
					trans_acc_to_loan_acc_transfer = cursor
							.getString(cursor.getColumnIndex(TransactionColumn.ACCTOLOANACCTRANSFER));
					trans_seedFund = cursor.getString(cursor.getColumnIndex(TransactionColumn.SEEDFUND));
					new GetTransactionSinglevalues(localId, trans_traineruserid, trans_username, trans_ngoid,
							trans_groupid, trans_uniqueid, trans_transactionid, trans_transactiondate, trans_savings,
							trans_VolunteerSavings, trans_personalloanrepay, trans_memberloanrepay, trans_expense,
							trans_otherincome, trans_subcription, trans_Penalty, trans_donation, trans_federation,
							trans_grouploanrepay, trans_bankdeposit, trans_fixeddeposit, trans_personalloandisbus,
							trans_attendance, trans_minutesofmeeting, trans_auditting, trans_training,
							trans_loanwithdrawal, trans_acctoacctransfer, trans_acc_to_loan_acc_transfer,
							trans_seedFund);
				} while (cursor.moveToNext());

			} else {

				int localId = -1;

				new GetTransactionSinglevalues(localId, trans_traineruserid, trans_username, trans_ngoid, trans_groupid,
						trans_uniqueid, trans_transactionid, trans_transactiondate, trans_savings,
						trans_VolunteerSavings, trans_personalloanrepay, trans_memberloanrepay, trans_expense,
						trans_otherincome, trans_subcription, trans_Penalty, trans_donation, trans_federation,
						trans_grouploanrepay, trans_bankdeposit, trans_fixeddeposit, trans_personalloandisbus,
						trans_attendance, trans_minutesofmeeting, trans_auditting, trans_training, trans_loanwithdrawal,
						trans_acctoacctransfer, trans_acc_to_loan_acc_transfer, trans_seedFund);

			}
			cursor.close();

			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			closeDatabase();

		}
		return true;

	}

	public static boolean updateTransactionEntry(TransactionUpdate transactionUpdate) {

		try {
			openDatabase();
			// Define the updated row content.
			ContentValues updatedValues = new ContentValues();
			// Assign values for each row.
			if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_SAV) {
				updatedValues.put(TransactionColumn.SAVINGS, TransactionUpdate.getTransactionValues());
				updatedValues.put(TransactionColumn.VOLUNTEERSAVINGS,
						TransactionSav_VSavUpdate.getTransactionVSavings());

			} else if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_PLRP) {
				updatedValues.put(TransactionColumn.PERSONALLOANREPAYMENT, TransactionUpdate.getTransactionValues());
			} else if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_MLRP) {
				updatedValues.put(TransactionColumn.MEMBERLOANREPAYMENT, TransactionUpdate.getTransactionValues());

			} else if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_EXP) {
				updatedValues.put(TransactionColumn.EXPENSES, TransactionUpdate.getTransactionValues());
			} else if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_OTH) {
				updatedValues.put(TransactionColumn.OTHERINCOME, TransactionUpdate.getTransactionValues());
			} else if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_SUB) {
				updatedValues.put(TransactionColumn.SUBCRIPTIONINCOME, TransactionUpdate.getTransactionValues());
			} else if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_PEN) {
				updatedValues.put(TransactionColumn.PENALTYINCOME, TransactionUpdate.getTransactionValues());
			} else if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_DON) {
				updatedValues.put(TransactionColumn.DONATIONINCOME, TransactionUpdate.getTransactionValues());
			} else if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_FED) {
				updatedValues.put(TransactionColumn.FEDERATIONINCOME, TransactionUpdate.getTransactionValues());
			} else if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_GLRP) {
				updatedValues.put(TransactionColumn.GROUPLOANREPAYMENT, TransactionUpdate.getTransactionValues());
			} else if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_BDEP) {
				updatedValues.put(TransactionColumn.BANKDEPOSIT, TransactionUpdate.getTransactionValues());
			} else if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_FIXD) {
				updatedValues.put(TransactionColumn.BANKFIXEDDEPOSIT, TransactionUpdate.getTransactionValues());
			} else if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_PDB) {
				updatedValues.put(TransactionColumn.PERSONALLOANDISBURSEMENT, TransactionUpdate.getTransactionValues());
			} else if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_ATT) {
				updatedValues.put(TransactionColumn.ATTENDANCE, TransactionUpdate.getTransactionValues());
			} else if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_MINM) {
				updatedValues.put(TransactionColumn.MINUTESOFMEETING, TransactionUpdate.getTransactionValues());
			} else if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_AUD) {
				updatedValues.put(TransactionColumn.AUDITTING, TransactionUpdate.getTransactionValues());
			} else if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_TRAN) {
				updatedValues.put(TransactionColumn.TRAINING, TransactionUpdate.getTransactionValues());
			} else if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_LOANWITHD) {
				updatedValues.put(TransactionColumn.LOANWITHDRAWAL, TransactionUpdate.getTransactionValues());
			} else if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_ACCTO) {
				updatedValues.put(TransactionColumn.ACCTOACCTRANSFER, TransactionUpdate.getTransactionValues());
			} else if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_LOANACC) {
				updatedValues.put(TransactionColumn.ACCTOLOANACCTRANSFER, TransactionUpdate.getTransactionValues());
			} else if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_SEED) {
				updatedValues.put(TransactionColumn.SEEDFUND, TransactionUpdate.getTransactionValues());
			}

			sqLiteDatabase.update(DataHelper.TABLES.TABLE_TRANSACTION_VALUES, updatedValues,
					"Uniqueid = '" + TransactionUpdate.getUniqueId() + "'", null);
			PrefUtils.setStatus_Data_Insert_Success("true");
			return false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			closeDatabase();
		}
		return true;
	}

	public static List<Transaction> getTransactionDetails() {
		try {
			openDatabase();
			Cursor cursor = null;
			final List<Transaction> getTrasnactions = new ArrayList<Transaction>();
			String mTrans_TrainerUserId = null;
			String mTrans_Username = null;
			String mTrans_NgoId = null;
			String mTrans_GroupId = null;
			String mTrans_UniqueId = null;
			String mTrans_TransactionId = null;
			String mTrans_TransactionDate = null;
			String mTrans_Savings = null;
			String mTrans_VolunteerSavings = null;
			String mTrans_PersonalloanRepayment = null;
			String mTrans_MemberloanRepayment = null;
			String mTrans_Expenses = null;
			String mTrans_OtherIncome = null;
			String mTrans_SubscriptionIncome = null;
			String mTrans_PenaltyIncome = null;
			String mTrans_DonationIncome = null;
			String mTrans_FederationIncome = null;
			String mTrans_Grouploanrepayment = null;
			String mTrans_BankDeposit = null;
			String mTrans_BankFixedDeposit = null;
			String mTrans_Personalloandisbursement = null;
			String mTrans_Attendance = null;
			String mTrans_Minutesofmeeting = null;
			String mTrans_Auditting = null;
			String mTrans_Training = null;
			String mTrans_Loanwithdrawal = null;
			String mTrans_Acctoacctransfer = null;
			String mTrans_AcctoLoanaccTransfer = null;
			String mTrans_Seedfund = null;

			int localId = -1;

			if (EShaktiApplication.isOfflineTransDate()) {
				cursor = sqLiteDatabase.query(DataHelper.TABLES.TABLE_TRANSACTION_VALUES, null,
						TransactionColumn.GROUPID + " =?", new String[] { SelectedGroupsTask.Group_Id }, null, null,
						null);

			} else if (EShaktiApplication.isOfflineTrans()) {
				cursor = sqLiteDatabase.query(DataHelper.TABLES.TABLE_TRANSACTION_VALUES, null, null, null, null, null,
						null);
			} else {
				cursor = sqLiteDatabase.query(DataHelper.TABLES.TABLE_TRANSACTION_VALUES, null,
						TransactionColumn.GROUPID + " =?" + " AND " + TransactionColumn.TRANSACTIONDATE + " =?",
						new String[] { SelectedGroupsTask.Group_Id, Offline_ReportDateListFragment.sSelectedTransDate },
						null, null, null);

			}

			if (cursor.moveToFirst()) {

				do {
					localId = cursor.getInt(cursor.getColumnIndex(TransactionColumn.T_ID));
					mTrans_TrainerUserId = cursor.getString(cursor.getColumnIndex(TransactionColumn.TRAINERUSERID));

					mTrans_Username = cursor.getString(cursor.getColumnIndex(TransactionColumn.USERNAME));

					mTrans_NgoId = cursor.getString(cursor.getColumnIndex(TransactionColumn.NGOID));

					mTrans_GroupId = cursor.getString(cursor.getColumnIndex(TransactionColumn.GROUPID));

					mTrans_UniqueId = cursor.getString(cursor.getColumnIndex(TransactionColumn.UNIQUEID));

					mTrans_TransactionId = cursor.getString(cursor.getColumnIndex(TransactionColumn.TRANSACTIONID));

					mTrans_TransactionDate = cursor.getString(cursor.getColumnIndex(TransactionColumn.TRANSACTIONDATE));

					mTrans_Savings = cursor.getString(cursor.getColumnIndex(TransactionColumn.SAVINGS));

					mTrans_VolunteerSavings = cursor
							.getString(cursor.getColumnIndex(TransactionColumn.VOLUNTEERSAVINGS));

					mTrans_PersonalloanRepayment = cursor
							.getString(cursor.getColumnIndex(TransactionColumn.PERSONALLOANREPAYMENT));

					mTrans_MemberloanRepayment = cursor
							.getString(cursor.getColumnIndex(TransactionColumn.MEMBERLOANREPAYMENT));

					mTrans_Expenses = cursor.getString(cursor.getColumnIndex(TransactionColumn.EXPENSES));
					mTrans_OtherIncome = cursor.getString(cursor.getColumnIndex(TransactionColumn.OTHERINCOME));

					mTrans_SubscriptionIncome = cursor
							.getString(cursor.getColumnIndex(TransactionColumn.SUBCRIPTIONINCOME));

					mTrans_PenaltyIncome = cursor.getString(cursor.getColumnIndex(TransactionColumn.PENALTYINCOME));

					mTrans_DonationIncome = cursor.getString(cursor.getColumnIndex(TransactionColumn.DONATIONINCOME));

					mTrans_FederationIncome = cursor
							.getString(cursor.getColumnIndex(TransactionColumn.FEDERATIONINCOME));

					mTrans_Grouploanrepayment = cursor
							.getString(cursor.getColumnIndex(TransactionColumn.GROUPLOANREPAYMENT));

					mTrans_BankDeposit = cursor.getString(cursor.getColumnIndex(TransactionColumn.BANKDEPOSIT));

					mTrans_BankFixedDeposit = cursor
							.getString(cursor.getColumnIndex(TransactionColumn.BANKFIXEDDEPOSIT));

					mTrans_Personalloandisbursement = cursor
							.getString(cursor.getColumnIndex(TransactionColumn.PERSONALLOANDISBURSEMENT));

					mTrans_Attendance = cursor.getString(cursor.getColumnIndex(TransactionColumn.ATTENDANCE));

					mTrans_Minutesofmeeting = cursor
							.getString(cursor.getColumnIndex(TransactionColumn.MINUTESOFMEETING));

					mTrans_Auditting = cursor.getString(cursor.getColumnIndex(TransactionColumn.AUDITTING));

					mTrans_Training = cursor.getString(cursor.getColumnIndex(TransactionColumn.TRAINING));
					mTrans_Loanwithdrawal = cursor.getString(cursor.getColumnIndex(TransactionColumn.LOANWITHDRAWAL));
					mTrans_Acctoacctransfer = cursor
							.getString(cursor.getColumnIndex(TransactionColumn.ACCTOACCTRANSFER));

					mTrans_AcctoLoanaccTransfer = cursor
							.getString(cursor.getColumnIndex(TransactionColumn.ACCTOLOANACCTRANSFER));
					mTrans_Seedfund = cursor.getString(cursor.getColumnIndex(TransactionColumn.SEEDFUND));

					getTrasnactions.add(new Transaction(localId, mTrans_TrainerUserId, mTrans_Username, mTrans_NgoId,
							mTrans_GroupId, mTrans_UniqueId, mTrans_TransactionId, mTrans_TransactionDate,
							mTrans_Savings, mTrans_VolunteerSavings, mTrans_PersonalloanRepayment,
							mTrans_MemberloanRepayment, mTrans_Expenses, mTrans_OtherIncome, mTrans_SubscriptionIncome,
							mTrans_PenaltyIncome, mTrans_DonationIncome, mTrans_FederationIncome,
							mTrans_Grouploanrepayment, mTrans_BankDeposit, mTrans_BankFixedDeposit,
							mTrans_Personalloandisbursement, mTrans_Attendance, mTrans_Minutesofmeeting,
							mTrans_Auditting, mTrans_Training, mTrans_Loanwithdrawal, mTrans_Acctoacctransfer,
							mTrans_AcctoLoanaccTransfer, mTrans_Seedfund));
				} while (cursor.moveToNext());
			} else {
				getTrasnactions.add(new Transaction(localId, mTrans_TrainerUserId, mTrans_Username, mTrans_NgoId,
						mTrans_GroupId, mTrans_UniqueId, mTrans_TransactionId, mTrans_TransactionDate, mTrans_Savings,
						mTrans_VolunteerSavings, mTrans_PersonalloanRepayment, mTrans_MemberloanRepayment,
						mTrans_Expenses, mTrans_OtherIncome, mTrans_SubscriptionIncome, mTrans_PenaltyIncome,
						mTrans_DonationIncome, mTrans_FederationIncome, mTrans_Grouploanrepayment, mTrans_BankDeposit,
						mTrans_BankFixedDeposit, mTrans_Personalloandisbursement, mTrans_Attendance,
						mTrans_Minutesofmeeting, mTrans_Auditting, mTrans_Training, mTrans_Loanwithdrawal,
						mTrans_Acctoacctransfer, mTrans_AcctoLoanaccTransfer, mTrans_Seedfund));
			}
			cursor.close();

			return getTrasnactions;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			closeDatabase();
		}
		return null;
	}

	public static boolean deleteTransaction(TransDelete transDelete) {
		try {
			DataHelper dataHelper = DataHelper.getInstance(EShaktiApplication.getInstance());
			SQLiteDatabase sqLiteDatabase = dataHelper.getWritableDatabase();
			sqLiteDatabase.delete(DataHelper.TABLES.TABLE_TRANSACTION_VALUES, TransactionColumn.UNIQUEID + "= ?",
					new String[] { TransDelete.getUniqueId() });

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			closeDatabase();
		}
		return true;
	}

	public static boolean getSinlgeGroupMaster_Transaction(Transaction_UniqueIdSingle transaction_UniqueIdSingle) {

		try {

			if (Transaction_UniqueIdSingle.getTransaction_UniqueId() != null) {
				openDatabase();

				Cursor cursor = sqLiteDatabase.query(DataHelper.TABLES.TABLE_TRANSACTION_VALUES, null,
						TransactionColumn.UNIQUEID + " =?",
						new String[] { Transaction_UniqueIdSingle.getTransaction_UniqueId() }, null, null, null);

				cursor.moveToFirst();

				String mTrans_TrainerUserId = null;
				String mTrans_Username = null;
				String mTrans_NgoId = null;
				String mTrans_GroupId = null;
				String mTrans_UniqueId = null;
				String mTrans_TransactionId = null;
				String mTrans_TransactionDate = null;
				String mTrans_Savings = null;
				String mTrans_VolunteerSavings = null;
				String mTrans_PersonalloanRepayment = null;
				String mTrans_MemberloanRepayment = null;
				String mTrans_Expenses = null;
				String mTrans_OtherIncome = null;
				String mTrans_SubscriptionIncome = null;
				String mTrans_PenaltyIncome = null;
				String mTrans_DonationIncome = null;
				String mTrans_FederationIncome = null;
				String mTrans_Grouploanrepayment = null;
				String mTrans_BankDeposit = null;
				String mTrans_BankFixedDeposit = null;
				String mTrans_Personalloandisbursement = null;
				String mTrans_Attendance = null;
				String mTrans_Minutesofmeeting = null;
				String mTrans_Auditting = null;
				String mTrans_Training = null;
				String mTrans_Loanwithdrawal = null;
				String mTrans_Acctoacctransfer = null;
				String mTrans_AcctoLoanaccTransfer = null;
				String mTrans_Seedfund = null;

				int localId = -1;

				if (cursor.moveToFirst()) {

					do {
						localId = cursor.getInt(cursor.getColumnIndex(TransactionColumn.T_ID));
						mTrans_TrainerUserId = cursor.getString(cursor.getColumnIndex(TransactionColumn.TRAINERUSERID));

						mTrans_Username = cursor.getString(cursor.getColumnIndex(TransactionColumn.USERNAME));

						mTrans_NgoId = cursor.getString(cursor.getColumnIndex(TransactionColumn.NGOID));

						mTrans_GroupId = cursor.getString(cursor.getColumnIndex(TransactionColumn.GROUPID));

						mTrans_UniqueId = cursor.getString(cursor.getColumnIndex(TransactionColumn.UNIQUEID));

						mTrans_TransactionId = cursor.getString(cursor.getColumnIndex(TransactionColumn.TRANSACTIONID));

						mTrans_TransactionDate = cursor
								.getString(cursor.getColumnIndex(TransactionColumn.TRANSACTIONDATE));

						mTrans_Savings = cursor.getString(cursor.getColumnIndex(TransactionColumn.SAVINGS));

						mTrans_VolunteerSavings = cursor
								.getString(cursor.getColumnIndex(TransactionColumn.VOLUNTEERSAVINGS));

						mTrans_PersonalloanRepayment = cursor
								.getString(cursor.getColumnIndex(TransactionColumn.PERSONALLOANREPAYMENT));

						mTrans_MemberloanRepayment = cursor
								.getString(cursor.getColumnIndex(TransactionColumn.MEMBERLOANREPAYMENT));

						mTrans_Expenses = cursor.getString(cursor.getColumnIndex(TransactionColumn.EXPENSES));
						mTrans_OtherIncome = cursor.getString(cursor.getColumnIndex(TransactionColumn.OTHERINCOME));

						mTrans_SubscriptionIncome = cursor
								.getString(cursor.getColumnIndex(TransactionColumn.SUBCRIPTIONINCOME));

						mTrans_PenaltyIncome = cursor.getString(cursor.getColumnIndex(TransactionColumn.PENALTYINCOME));

						mTrans_DonationIncome = cursor
								.getString(cursor.getColumnIndex(TransactionColumn.DONATIONINCOME));

						mTrans_FederationIncome = cursor
								.getString(cursor.getColumnIndex(TransactionColumn.FEDERATIONINCOME));

						mTrans_Grouploanrepayment = cursor
								.getString(cursor.getColumnIndex(TransactionColumn.GROUPLOANREPAYMENT));

						mTrans_BankDeposit = cursor.getString(cursor.getColumnIndex(TransactionColumn.BANKDEPOSIT));

						mTrans_BankFixedDeposit = cursor
								.getString(cursor.getColumnIndex(TransactionColumn.BANKFIXEDDEPOSIT));

						mTrans_Personalloandisbursement = cursor
								.getString(cursor.getColumnIndex(TransactionColumn.PERSONALLOANDISBURSEMENT));

						mTrans_Attendance = cursor.getString(cursor.getColumnIndex(TransactionColumn.ATTENDANCE));

						mTrans_Minutesofmeeting = cursor
								.getString(cursor.getColumnIndex(TransactionColumn.MINUTESOFMEETING));

						mTrans_Auditting = cursor.getString(cursor.getColumnIndex(TransactionColumn.AUDITTING));

						mTrans_Training = cursor.getString(cursor.getColumnIndex(TransactionColumn.TRAINING));
						mTrans_Loanwithdrawal = cursor
								.getString(cursor.getColumnIndex(TransactionColumn.LOANWITHDRAWAL));
						mTrans_Acctoacctransfer = cursor
								.getString(cursor.getColumnIndex(TransactionColumn.ACCTOACCTRANSFER));

						mTrans_AcctoLoanaccTransfer = cursor
								.getString(cursor.getColumnIndex(TransactionColumn.ACCTOLOANACCTRANSFER));
						mTrans_Seedfund = cursor.getString(cursor.getColumnIndex(TransactionColumn.SEEDFUND));

						new GetTransactionSinglevalues(localId, mTrans_TrainerUserId, mTrans_Username, mTrans_NgoId,
								mTrans_GroupId, mTrans_UniqueId, mTrans_TransactionId, mTrans_TransactionDate,
								mTrans_Savings, mTrans_VolunteerSavings, mTrans_PersonalloanRepayment,
								mTrans_MemberloanRepayment, mTrans_Expenses, mTrans_OtherIncome,
								mTrans_SubscriptionIncome, mTrans_PenaltyIncome, mTrans_DonationIncome,
								mTrans_FederationIncome, mTrans_Grouploanrepayment, mTrans_BankDeposit,
								mTrans_BankFixedDeposit, mTrans_Personalloandisbursement, mTrans_Attendance,
								mTrans_Minutesofmeeting, mTrans_Auditting, mTrans_Training, mTrans_Loanwithdrawal,
								mTrans_Acctoacctransfer, mTrans_AcctoLoanaccTransfer, mTrans_Seedfund);

					} while (cursor.moveToNext());
				} else {
					new GetTransactionSinglevalues(localId, mTrans_TrainerUserId, mTrans_Username, mTrans_NgoId,
							mTrans_GroupId, mTrans_UniqueId, mTrans_TransactionId, mTrans_TransactionDate,
							mTrans_Savings, mTrans_VolunteerSavings, mTrans_PersonalloanRepayment,
							mTrans_MemberloanRepayment, mTrans_Expenses, mTrans_OtherIncome, mTrans_SubscriptionIncome,
							mTrans_PenaltyIncome, mTrans_DonationIncome, mTrans_FederationIncome,
							mTrans_Grouploanrepayment, mTrans_BankDeposit, mTrans_BankFixedDeposit,
							mTrans_Personalloandisbursement, mTrans_Attendance, mTrans_Minutesofmeeting,
							mTrans_Auditting, mTrans_Training, mTrans_Loanwithdrawal, mTrans_Acctoacctransfer,
							mTrans_AcctoLoanaccTransfer, mTrans_Seedfund);

				}
				cursor.close();

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	public static boolean updateTransactionEntry_StatusUpdate(String transactionUpdate) {

		try {
			openDatabase();
			// Define the updated row content.
			ContentValues updatedValues = new ContentValues();
			
			transactionUpdate = null;
			// Assign values for each row.
			if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_SAV) {
				updatedValues.put(TransactionColumn.SAVINGS, transactionUpdate);
				updatedValues.put(TransactionColumn.VOLUNTEERSAVINGS,
						transactionUpdate);

			} else if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_PLRP) {
				updatedValues.put(TransactionColumn.PERSONALLOANREPAYMENT, transactionUpdate);
			} else if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_MLRP) {
				updatedValues.put(TransactionColumn.MEMBERLOANREPAYMENT, transactionUpdate);

			} else if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_EXP) {
				updatedValues.put(TransactionColumn.EXPENSES, transactionUpdate);
			} else if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_OTH) {
				updatedValues.put(TransactionColumn.OTHERINCOME, transactionUpdate);
			} else if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_SUB) {
				updatedValues.put(TransactionColumn.SUBCRIPTIONINCOME, transactionUpdate);
			} else if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_PEN) {
				updatedValues.put(TransactionColumn.PENALTYINCOME, transactionUpdate);
			} else if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_DON) {
				updatedValues.put(TransactionColumn.DONATIONINCOME, transactionUpdate);
			} else if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_FED) {
				updatedValues.put(TransactionColumn.FEDERATIONINCOME, transactionUpdate);
			} else if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_GLRP) {
				updatedValues.put(TransactionColumn.GROUPLOANREPAYMENT, transactionUpdate);
			} else if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_BDEP) {
				updatedValues.put(TransactionColumn.BANKDEPOSIT, transactionUpdate);
			} else if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_FIXD) {
				updatedValues.put(TransactionColumn.BANKFIXEDDEPOSIT, transactionUpdate);
			} else if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_PDB) {
				updatedValues.put(TransactionColumn.PERSONALLOANDISBURSEMENT, transactionUpdate);
			} else if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_ATT) {
				updatedValues.put(TransactionColumn.ATTENDANCE, transactionUpdate);
			} else if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_MINM) {
				updatedValues.put(TransactionColumn.MINUTESOFMEETING, transactionUpdate);
			} else if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_AUD) {
				updatedValues.put(TransactionColumn.AUDITTING, transactionUpdate);
			} else if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_TRAN) {
				updatedValues.put(TransactionColumn.TRAINING, transactionUpdate);
			} else if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_LOANWITHD) {
				updatedValues.put(TransactionColumn.LOANWITHDRAWAL, transactionUpdate);
			} else if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_ACCTO) {
				updatedValues.put(TransactionColumn.ACCTOACCTRANSFER, transactionUpdate);
			} else if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_LOANACC) {
				updatedValues.put(TransactionColumn.ACCTOLOANACCTRANSFER, transactionUpdate);
			} else if (EShaktiApplication.getSetTransValues() == TransactionOffConstants.TRANS_SEED) {
				updatedValues.put(TransactionColumn.SEEDFUND, transactionUpdate);
			}

			sqLiteDatabase.update(DataHelper.TABLES.TABLE_TRANSACTION_VALUES, updatedValues,
					"Uniqueid = '" + TransactionUpdate.getUniqueId() + "'", null);
		 
			return false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			closeDatabase();
		}
		return true;
	}

	public static boolean deleteOfflinetransaction() {
		try {

			DataHelper dataHelper = DataHelper.getInstance(EShaktiApplication.getInstance());
			SQLiteDatabase sqLiteDatabase = dataHelper.getWritableDatabase();
			sqLiteDatabase.delete(DataHelper.TABLES.TABLE_TRANSACTION_VALUES, null, null);

			dataHelper.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

		}
		return true;
	}

}
