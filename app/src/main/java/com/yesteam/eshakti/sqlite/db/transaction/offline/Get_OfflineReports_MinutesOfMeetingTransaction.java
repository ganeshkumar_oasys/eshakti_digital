package com.yesteam.eshakti.sqlite.db.transaction.offline;

import com.yesteam.eshakti.sqlite.database.GroupDetailsProvider;
import com.yesteam.eshakti.sqlite.database.response.Get_OfflineReports_MinutesOfMeetingResponse;
import com.yesteam.eshakti.sqlite.db.model.GroupMasterSingle;
import com.yesteam.eshakti.sqlite.db.transactions.AbstractTransaction;

public class Get_OfflineReports_MinutesOfMeetingTransaction
		extends
		AbstractTransaction<GroupMasterSingle, Get_OfflineReports_MinutesOfMeetingResponse> {

	@Override
	public int onPreExecute() {
		return EXECUTE_IN_WORKER_THREAD;
	}

	@Override
	public void run() {

		boolean value = GroupDetailsProvider.getSinlgeGroupMaster(getParams());

		Get_OfflineReports_MinutesOfMeetingResponse transResponse = new Get_OfflineReports_MinutesOfMeetingResponse(
				value);
		deliverResponse(transResponse);

	}

}
