package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class MemberPhotoDeleteResponse extends BaseResponse {

	public MemberPhotoDeleteResponse(final TransactionResult transactionResult) {
		super(transactionResult);
	}

	public MemberPhotoDeleteResponse(final boolean result) {
		super(TransactionResult.SUCCESS);
	}
}
