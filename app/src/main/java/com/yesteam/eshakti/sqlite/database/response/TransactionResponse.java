package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class TransactionResponse extends BaseResponse {

	public TransactionResponse(final TransactionResult transactionResult) {
		super(transactionResult);
	}

	public TransactionResponse(final boolean result) {
		super(TransactionResult.SUCCESS);
	}
}
