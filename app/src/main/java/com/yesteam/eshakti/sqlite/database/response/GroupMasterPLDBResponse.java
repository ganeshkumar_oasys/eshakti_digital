package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class GroupMasterPLDBResponse extends BaseResponse {

	public GroupMasterPLDBResponse(final TransactionResult transactionResult) {
		super(transactionResult);
	}

	public GroupMasterPLDBResponse(final boolean result) {
		super(TransactionResult.SUCCESS);
	}

}
