package com.yesteam.eshakti.sqlite.db.transaction.offline;

import com.yesteam.eshakti.sqlite.database.GroupDetailsProvider;
import com.yesteam.eshakti.sqlite.database.response.LoanAccountDataUpdateResponse;
import com.yesteam.eshakti.sqlite.db.model.LoanAccountUpdate;
import com.yesteam.eshakti.sqlite.db.transactions.AbstractTransaction;

public class LoanaccDataUpdateTransaction
		extends AbstractTransaction<LoanAccountUpdate, LoanAccountDataUpdateResponse> {

	@Override
	public int onPreExecute() {
		return EXECUTE_IN_WORKER_THREAD;
	}

	@Override
	public void run() {

		boolean value = GroupDetailsProvider.updateLoanAccountData(getParams());

		LoanAccountDataUpdateResponse transResponse = new LoanAccountDataUpdateResponse(value);

		deliverResponse(transResponse);

	}

}