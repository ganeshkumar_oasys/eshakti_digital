package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class GroupMasUpdateResponse extends BaseResponse {

	public GroupMasUpdateResponse(final TransactionResult transactionResult) {
		super(transactionResult);
	}

	public GroupMasUpdateResponse(final boolean result) {
		super(TransactionResult.SUCCESS);
	}
}
