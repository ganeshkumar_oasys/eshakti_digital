package com.yesteam.eshakti.sqlite.db.model;

import java.io.Serializable;

public class LoanAccountNoDetailsUpdate implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4450369885775111938L;
	private static String mGroupId;
	private static String mGroupLoanAccountNO;

	public LoanAccountNoDetailsUpdate(final String grp_groupId, final String grp_transloanaccNo) {

		LoanAccountNoDetailsUpdate.mGroupId = grp_groupId;
		LoanAccountNoDetailsUpdate.mGroupLoanAccountNO = grp_transloanaccNo;
	}

	public static String getGroupId() {
		return mGroupId;
	}

	public static String getTransLoanacc() {
		return mGroupLoanAccountNO;
	}

}
