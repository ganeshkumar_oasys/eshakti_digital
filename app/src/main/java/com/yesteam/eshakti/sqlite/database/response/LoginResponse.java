package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class LoginResponse extends BaseResponse {

	public LoginResponse(final TransactionResult transactionResult) {
		super(transactionResult);
	}

	public LoginResponse(final boolean result) {
		super(TransactionResult.SUCCESS);
	}
}
