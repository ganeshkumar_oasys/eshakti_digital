package com.yesteam.eshakti.sqlite.db.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class GroupMasterSingle implements Serializable {

	private static String mMasterGroupId;

	public GroupMasterSingle(final String mastergroupid) {
		GroupMasterSingle.setMasterGroupId(mastergroupid);
	}

	public static String getMasterGroupId() {
		return mMasterGroupId;
	}

	public static void setMasterGroupId(String mMasterGroupId) {
		GroupMasterSingle.mMasterGroupId = mMasterGroupId;
	}

}
