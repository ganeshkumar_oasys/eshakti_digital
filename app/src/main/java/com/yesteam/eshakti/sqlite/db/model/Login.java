package com.yesteam.eshakti.sqlite.db.model;

import java.io.Serializable;

public class Login implements Serializable {

	private static final long serialVersionUID = 1L;
	private final int localId;
	private final String mTrainerUserId;
	private final String mNgoId;
	private final String mTrainerId;
	private final String mPassword;
	private final String mLanguage;

	public Login(final int localId, final String log_traineruserid,
			final String log_ngoid, final String log_trainerid,
			final String log_password, final String log_language) {
		this.localId = localId;
		this.mTrainerUserId = log_traineruserid;
		this.mNgoId = log_ngoid;
		this.mTrainerId = log_trainerid;
		this.mPassword = log_password;
		this.mLanguage = log_language;
	}

	public int getLocalId() {
		return localId;
	}

	public String getTrainerUserId() {
		return mTrainerUserId;
	}

	public String getNgoId() {
		return mNgoId;
	}

	public String getTrainerId() {
		return mTrainerId;
	}

	public String getPassword() {
		return mPassword;
	}

	public String getLanguage() {
		return mLanguage;
	}

}
