package com.yesteam.eshakti.sqlite.db.model;

import java.io.Serializable;

public class Transaction implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3364815883533541521L;
	private final int localId;
	private final String mTrainerUserId;
	private final String mUsername;
	private final String mNgoId;
	private final String mGroupId;
	private final String mUniqueId;
	private final String mTransactionId;
	private final String mTransactionDate;
	private final String mSavings;
	private final String mVolunteerSavings;
	private final String mPersonalloanRepayment;
	private final String mMemberloanRepayment;
	private final String mExpenses;
	private final String mOtherIncome;
	private final String mSubscriptionIncome;
	private final String mPenaltyIncome;
	private final String mDonationIncome;
	private final String mFederationIncome;
	private final String mGrouploanrepayment;
	private final String mBankDeposit;
	private final String mBankFixedDeposit;
	private final String mPersonalloandisbursement;
	private final String mAttendance;
	private final String mMinutesofmeeting;
	private final String mAuditting;
	private final String mTraining;
	private final String mLoanWithdrawal;
	private final String mAcctoAccTransfer;
	private final String mAccToLoanAccTransfer;
	private final String mSeedFund;

	public Transaction(final int localId, final String trans_trainerid, final String trans_username,
			final String trans_ngoid, final String trans_groupid, final String trans_uniqueid,
			final String trans_transactionid, final String trans_transactiondate, final String trans_savings,
			final String trans_volunteersavings, final String trans_personalloanrepayment,
			final String trans_memberloanrepayment, final String trans_expenses, final String trans_otherincome,
			final String trans_subscriptionincome, final String trans_penaltyincome, final String trans_donationincome,
			final String trans_federationincome, final String trans_grouploanrepayment, final String trans_bankdeposit,
			final String trans_bankfixeddeposit, final String trans_personalloandisbursement,
			final String trans_attendance, final String trans_minutesofmeeting, final String trans_auditting,
			final String trans_training, final String trans_loanwithdrawal, final String trans_acctoacctransfer,
			final String trans_AcctoLoanAccTransfer, final String trans_Seedfund) {
		this.localId = localId;
		this.mTrainerUserId = trans_trainerid;
		this.mUsername = trans_username;
		this.mNgoId = trans_ngoid;
		this.mGroupId = trans_groupid;
		this.mUniqueId = trans_uniqueid;
		this.mTransactionId = trans_transactionid;
		this.mTransactionDate = trans_transactiondate;
		this.mSavings = trans_savings;
		this.mVolunteerSavings = trans_volunteersavings;
		this.mPersonalloanRepayment = trans_personalloanrepayment;
		this.mMemberloanRepayment = trans_memberloanrepayment;
		this.mExpenses = trans_expenses;
		this.mOtherIncome = trans_otherincome;
		this.mSubscriptionIncome = trans_subscriptionincome;
		this.mPenaltyIncome = trans_penaltyincome;
		this.mDonationIncome = trans_donationincome;
		this.mFederationIncome = trans_federationincome;
		this.mGrouploanrepayment = trans_grouploanrepayment;
		this.mBankDeposit = trans_bankdeposit;
		this.mBankFixedDeposit = trans_bankfixeddeposit;
		this.mPersonalloandisbursement = trans_personalloandisbursement;
		this.mAttendance = trans_attendance;
		this.mMinutesofmeeting = trans_minutesofmeeting;
		this.mAuditting = trans_auditting;
		this.mTraining = trans_training;
		this.mLoanWithdrawal = trans_loanwithdrawal;
		this.mAcctoAccTransfer = trans_acctoacctransfer;
		this.mAccToLoanAccTransfer = trans_AcctoLoanAccTransfer;
		this.mSeedFund = trans_Seedfund;

	}

	/**
	 * @return the localId
	 */
	public int getLocalId() {
		return localId;
	}

	public String getTrainerUserId() {
		return mTrainerUserId;
	}

	public String getUsername() {
		return mUsername;
	}

	public String getNgoId() {
		return mNgoId;
	}

	public String getGroupId() {
		return mGroupId;
	}

	public String getUniqueId() {
		return mUniqueId;
	}

	public String getTransactionId() {
		return mTransactionId;
	}

	public String getTransactionDate() {
		return mTransactionDate;
	}

	public String getSavings() {
		return mSavings;
	}

	public String getVolunteerSavings() {
		return mVolunteerSavings;
	}

	public String getPersonalloanRepayment() {
		return mPersonalloanRepayment;
	}

	public String getMemberloanRepayment() {
		return mMemberloanRepayment;
	}

	public String getExpenses() {
		return mExpenses;
	}

	public String getOtherIncome() {
		return mOtherIncome;
	}

	public String getSubscriptionIncome() {
		return mSubscriptionIncome;
	}

	public String getPenaltyIncome() {
		return mPenaltyIncome;
	}

	public String getDonationIncome() {
		return mDonationIncome;
	}

	public String getFederationIncome() {
		return mFederationIncome;
	}

	public String getGrouploanrepayment() {
		return mGrouploanrepayment;
	}

	public String getBankDeposit() {
		return mBankDeposit;
	}

	public String getBankFixedDeposit() {
		return mBankFixedDeposit;
	}

	public String getAttendance() {
		return mAttendance;
	}

	public String getMinutesofmeeting() {
		return mMinutesofmeeting;
	}

	public String getAuditting() {
		return mAuditting;
	}

	/**
	 * @return the mTraining
	 */
	public String getTraining() {
		return mTraining;
	}

	/**
	 * @return the mPersonalloandisbursement
	 */
	public String getPersonalloandisbursement() {
		return mPersonalloandisbursement;
	}

	public String getLoanWithdrawal() {
		return mLoanWithdrawal;
	}

	public String getAcctoAccTransfer() {
		return mAcctoAccTransfer;
	}

	public String getAccToLoanAccTransfer() {
		return mAccToLoanAccTransfer;
	}

	public String getSeedFund() {
		return mSeedFund;
	}

}
