package com.yesteam.eshakti.sqlite.db.transaction.offline;

import com.yesteam.eshakti.sqlite.database.GroupDetailsProvider;
import com.yesteam.eshakti.sqlite.database.response.GroupOfflineDataUpdateResponse;
import com.yesteam.eshakti.sqlite.db.model.GroupMaster;
import com.yesteam.eshakti.sqlite.db.transactions.AbstractTransaction;

import android.util.Log;

public class GroupOfflineDataUpdateTransaction
		extends AbstractTransaction<GroupMaster, GroupOfflineDataUpdateResponse> {

	@Override
	public int onPreExecute() {
		return EXECUTE_IN_WORKER_THREAD;
	}

	@Override
	public void run() {

		boolean value = GroupDetailsProvider.updateGroupOfflineData(getParams());

		Log.e("Transaction", "Updated");

		GroupOfflineDataUpdateResponse transResponse = new GroupOfflineDataUpdateResponse(value);

		deliverResponse(transResponse);

	}

}