package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class GroupPersonalloanOSResponse extends BaseResponse {

	public GroupPersonalloanOSResponse(final TransactionResult transactionResult) {
		super(transactionResult);
	}

	public GroupPersonalloanOSResponse(final boolean result) {
		super(TransactionResult.SUCCESS);
	}

}