package com.yesteam.eshakti.sqlite.db.transaction.offline;

import com.yesteam.eshakti.sqlite.database.MemberPhotoProvider;
import com.yesteam.eshakti.sqlite.database.response.MemberPhotoResponse;
import com.yesteam.eshakti.sqlite.db.model.MemberPhotoMaster;
import com.yesteam.eshakti.sqlite.db.transactions.AbstractTransaction;

public class MemberPhotoMasterTransaction extends AbstractTransaction<MemberPhotoMaster, MemberPhotoResponse> {

	@Override
	public int onPreExecute() {
		return EXECUTE_IN_WORKER_THREAD;
	}

	@Override
	public void run() {

		boolean value = MemberPhotoProvider.addMemberPhotDetails(getParams());

		MemberPhotoResponse transResponse = new MemberPhotoResponse(value);
		deliverResponse(transResponse);
	}

}
