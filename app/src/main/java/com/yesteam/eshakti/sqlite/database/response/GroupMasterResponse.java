package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class GroupMasterResponse extends BaseResponse {

	public GroupMasterResponse(final TransactionResult transactionResult) {
		super(transactionResult);
	}

	public GroupMasterResponse(final boolean result) {
		super(TransactionResult.SUCCESS);
	}

}
