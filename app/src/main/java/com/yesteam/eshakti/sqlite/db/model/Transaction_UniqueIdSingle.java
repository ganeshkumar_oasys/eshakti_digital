package com.yesteam.eshakti.sqlite.db.model;

import java.io.Serializable;

public class Transaction_UniqueIdSingle implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3823033702683986937L;
	private static String mTransactionUniqueId;

	public Transaction_UniqueIdSingle(final String transactionUnique) {
		Transaction_UniqueIdSingle.setTransaction_UniqueId(transactionUnique);
	}

	public static String getTransaction_UniqueId() {
		return mTransactionUniqueId;
	}

	public static void setTransaction_UniqueId(String mtransactionUniqueId) {
		Transaction_UniqueIdSingle.mTransactionUniqueId = mtransactionUniqueId;
	}

}
