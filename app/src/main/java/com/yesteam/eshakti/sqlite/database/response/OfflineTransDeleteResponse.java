package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class OfflineTransDeleteResponse extends BaseResponse {

	public OfflineTransDeleteResponse(final TransactionResult transactionResult) {
		super(transactionResult);
	}

	public OfflineTransDeleteResponse(final boolean result) {
		super(TransactionResult.SUCCESS);
	}
}