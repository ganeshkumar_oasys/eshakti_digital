package com.yesteam.eshakti.sqlite.db.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class GroupFOSUpdate implements Serializable {
	private static String mGroupId;
	private static String mGroupResponse;
	private static String mGroupFDoutstanding;

	public GroupFOSUpdate(final String grp_groupId,
			final String grp_groupresponse, final String grp_groupMLOS) {

		GroupFOSUpdate.mGroupId = grp_groupId;
		GroupFOSUpdate.mGroupResponse = grp_groupresponse;
		GroupFOSUpdate.mGroupFDoutstanding = grp_groupMLOS;
	}

	public static String getGroupId() {
		return mGroupId;
	}

	public static String getGroupResponse() {
		return mGroupResponse;
	}

	public static String getGroupFDoutstanding() {
		return mGroupFDoutstanding;
	}

}
