package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class GroupFixedOSResponse extends BaseResponse {

	public GroupFixedOSResponse(final TransactionResult transactionResult) {
		super(transactionResult);
	}

	public GroupFixedOSResponse(final boolean result) {
		super(TransactionResult.SUCCESS);
	}

}