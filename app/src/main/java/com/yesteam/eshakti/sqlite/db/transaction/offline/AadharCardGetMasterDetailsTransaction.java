package com.yesteam.eshakti.sqlite.db.transaction.offline;

import com.yesteam.eshakti.params.NoParams;
import com.yesteam.eshakti.sqlite.database.AadharcardMasterProvider;
import com.yesteam.eshakti.sqlite.database.response.AadharCardGetMasterResponse;
import com.yesteam.eshakti.sqlite.db.transactions.AbstractTransaction;

public class AadharCardGetMasterDetailsTransaction extends AbstractTransaction<NoParams, AadharCardGetMasterResponse> {

	@Override
	public int onPreExecute() {
		return EXECUTE_IN_WORKER_THREAD;
	}

	@Override
	public void run() {
		boolean value = AadharcardMasterProvider.getAadharCardMasterValues();
		AadharCardGetMasterResponse transResponse = new AadharCardGetMasterResponse(value);
		deliverResponse(transResponse);
	}
}