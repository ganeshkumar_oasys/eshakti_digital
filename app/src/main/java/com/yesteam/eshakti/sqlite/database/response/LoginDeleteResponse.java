package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class LoginDeleteResponse extends BaseResponse {

	public LoginDeleteResponse(final TransactionResult transactionResult) {
		super(transactionResult);
	}

	public LoginDeleteResponse(final boolean result) {
		super(TransactionResult.SUCCESS);
	}
}
