package com.yesteam.eshakti.sqlite.db.transaction.offline;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.yesteam.eshakti.sqlite.database.GroupDetailsProvider;
import com.yesteam.eshakti.sqlite.database.response.GetTransIdResponse;
import com.yesteam.eshakti.sqlite.db.model.GetGroupMemberDetails;
import com.yesteam.eshakti.sqlite.db.model.GroupMasterSingle;
import com.yesteam.eshakti.sqlite.db.transactions.AbstractTransaction;

public class GetSingleTransIdTransaction extends AbstractTransaction<GroupMasterSingle, GetTransIdResponse> {

	@Override
	public int onPreExecute() {
		return EXECUTE_IN_WORKER_THREAD;
	}

	@Override
	public void run() {

		boolean value = GroupDetailsProvider.getSinlgeGroupMaster(getParams());

		GetTransIdResponse transResponse = new GetTransIdResponse(value);
		deliverResponse(transResponse);
		String mLastTransId = GetGroupMemberDetails.getGroupTransId();
		EShaktiApplication.setLastTransId(mLastTransId);
	}

}