package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class GroupPLOS_Update_Response extends BaseResponse {

	public GroupPLOS_Update_Response(final TransactionResult transactionResult) {
		super(transactionResult);
	}

	public GroupPLOS_Update_Response(final boolean result) {
		super(TransactionResult.SUCCESS);
	}
}
