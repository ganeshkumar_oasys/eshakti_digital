package com.yesteam.eshakti.sqlite.db.model;

import java.io.Serializable;

public class GroupResponseUpdate implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6144010394138271202L;
	private static String mGroupId;
	private static String mGroupResponseValues;

	public GroupResponseUpdate(final String grp_groupId,
			final String grp_transactionid) {

		GroupResponseUpdate.mGroupId = grp_groupId;
		GroupResponseUpdate.mGroupResponseValues = grp_transactionid;
	}

	public static String getGroupId() {
		return mGroupId;
	}

	public static String getGroupResponse() {
		return mGroupResponseValues;
	}
}