package com.yesteam.eshakti.sqlite.db.model;

import java.io.Serializable;

public class MemberPhotoMasterUpdate implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -703836790072035269L;
	private static String mMasterUpdateValues;
	private static String mMasterUniqueId;

	public static String getMasterUpdateValues() {
		return mMasterUpdateValues;
	}

	public static void setMasterUpdateValues(String mMasterUpdateValues) {
		MemberPhotoMasterUpdate.mMasterUpdateValues = mMasterUpdateValues;
	}

	public static String getMasterUniqueId() {
		return mMasterUniqueId;
	}

	public static void setMasterUniqueId(String mMasterUniqueId) {
		MemberPhotoMasterUpdate.mMasterUniqueId = mMasterUniqueId;
	}

}