package com.yesteam.eshakti.sqlite.database.response;
import com.yesteam.eshakti.transaction.response.BaseResponse;

public class GroupPLOSUpdateResponse extends BaseResponse {

	public GroupPLOSUpdateResponse(final TransactionResult transactionResult) {
		super(transactionResult);
	}

	public GroupPLOSUpdateResponse(final boolean result) {
		super(TransactionResult.SUCCESS);
	}
}
