package com.yesteam.eshakti.sqlite.db.transaction.offline;

import com.yesteam.eshakti.sqlite.database.GroupDetailsProvider;
import com.yesteam.eshakti.sqlite.database.response.GroupLoanUpdateResponse;
import com.yesteam.eshakti.sqlite.db.model.GroupDetailsUpdate;
import com.yesteam.eshakti.sqlite.db.transactions.AbstractTransaction;

public class GroupLoanUpdateTransaction extends AbstractTransaction<GroupDetailsUpdate, GroupLoanUpdateResponse> {

	@Override
	public int onPreExecute() {
		return EXECUTE_IN_WORKER_THREAD;
	}

	@Override
	public void run() {

		boolean value = GroupDetailsProvider.updateGroupDetailsEntry(getParams());

		GroupLoanUpdateResponse transResponse = new GroupLoanUpdateResponse(value);
		deliverResponse(transResponse);

	}

}
