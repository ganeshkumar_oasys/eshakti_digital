package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class MemberGetPhotoMasterResponse extends BaseResponse {

	public MemberGetPhotoMasterResponse(final TransactionResult transactionResult) {
		super(transactionResult);
	}

	public MemberGetPhotoMasterResponse(final boolean result) {
		super(TransactionResult.SUCCESS);
	}
}
