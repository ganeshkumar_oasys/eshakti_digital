package com.yesteam.eshakti.sqlite.db.transaction.offline;

import java.util.List;

import com.yesteam.eshakti.params.NoParams;
import com.yesteam.eshakti.sqlite.database.TransactionProvider;
import com.yesteam.eshakti.sqlite.db.model.Transaction;
import com.yesteam.eshakti.sqlite.db.transactions.AbstractTransaction;

import android.util.Log;


public class GetTransValuesTransaction extends
		AbstractTransaction<NoParams, List<Transaction>> {

	@Override
	public int onPreExecute() {
		return EXECUTE_IN_WORKER_THREAD;
	}

	@Override
	public void run() {
		deliverResponse(TransactionProvider.getTransactionDetails());
		Log.v("Run Executed", "12122112");
	}
}
