package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class Trans_MeetingResponse extends BaseResponse {

	public Trans_MeetingResponse(final TransactionResult transactionResult) {
		super(transactionResult);
	}

	public Trans_MeetingResponse(final boolean result) {
		super(TransactionResult.SUCCESS);
	}
}
