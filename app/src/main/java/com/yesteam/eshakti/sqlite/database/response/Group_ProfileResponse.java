package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class Group_ProfileResponse extends BaseResponse {

	public Group_ProfileResponse(final TransactionResult transactionResult) {
		super(transactionResult);
	}

	public Group_ProfileResponse(final boolean result) {
		super(TransactionResult.SUCCESS);
	}

}
