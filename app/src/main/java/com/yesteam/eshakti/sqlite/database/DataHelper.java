package com.yesteam.eshakti.sqlite.database;

import com.yesteam.eshakti.sqlite.database.GroupDetailsProvider.GroupMasterColumn;
import com.yesteam.eshakti.sqlite.database.GroupProvider.GroupColumn;
import com.yesteam.eshakti.sqlite.database.TransactionProvider.TransactionColumn;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public final class DataHelper extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "yesteam_eshakti.db";
	private static final int DATABASE_VERSION = 5;

	private static DataHelper sDataHelper;

	public static class TABLES {

		public static final String TABLE_LOGIN = "LoginValues";
		public static final String TABLE_GROUP_NAME_DETAILS = "GroupNameDetails";
		public static final String TABLE_GROUP_DETAILS = "GroupDetails";
		public static final String TABLE_TRANSACTION_VALUES = "TransactionValues";
		public static final String TABLE_AADHARCARD_MASTER = "AadharcardMaster";
		public static final String TABLE_MEMBER_PHOTO_MASTER = "MemberPhotoMaster";
		public static final String TABLE_BANK_MASTER = "BankMaster";

	}

	public DataHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	public static DataHelper getInstance(final Context context) {
		if (sDataHelper == null) {
			sDataHelper = new DataHelper(context);
		}
		return sDataHelper;
	}

	public static DataHelper getInstance() {
		return sDataHelper;
	}

	public void init() {
		getReadableDatabase();
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		db.execSQL(LoginProvider.CREATE_LOGIN_TABLE);
		db.execSQL(GroupProvider.CREATE_GROUP_NAME_TABLE_NEW);
		db.execSQL(GroupDetailsProvider.CREATE_GROUP_DETAILS_TABLE_NEW);
		db.execSQL(TransactionProvider.CREATE_TRANSACTION_VALUES_TABLE_NEW);
		db.execSQL(AadharcardMasterProvider.CREATE_AADHARCARD_DETAILS_TABLE);
		db.execSQL(MemberPhotoProvider.CREATE_MEMBERPHOTO_DETAILS_TABLE);
		db.execSQL(BankDetailsProvider.CREATE_BANK_DETAILS_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		switch (oldVersion) {
		case 1:
			String alter_query1 = "ALTER TABLE " + TABLES.TABLE_GROUP_DETAILS + " ADD "
					+ GroupMasterColumn.BANKACCOUNTNODETAILS + " text" + ";";
			db.execSQL(alter_query1);
			String alter_query2 = "ALTER TABLE " + TABLES.TABLE_GROUP_DETAILS + " ADD "
					+ GroupMasterColumn.LOANACCOUNTOUTSTANDINGDETAILS + " text" + ";";
			db.execSQL(alter_query2);

			String alter_query3 = "ALTER TABLE " + TABLES.TABLE_TRANSACTION_VALUES + " ADD "
					+ TransactionColumn.ACCTOLOANACCTRANSFER + " text" + ";";
			db.execSQL(alter_query3);
			String alter_query4 = "ALTER TABLE " + TABLES.TABLE_TRANSACTION_VALUES + " ADD "
					+ TransactionColumn.SEEDFUND + " text" + ";";
			db.execSQL(alter_query4);

			String alter_query5 = "ALTER TABLE " + TABLES.TABLE_GROUP_NAME_DETAILS + " ADD "
					+ GroupColumn.GROUP_LAST_TRANSACTIONDATE + " text" + ";";
			db.execSQL(alter_query5);
			String alter_query6 = "ALTER TABLE " + TABLES.TABLE_GROUP_NAME_DETAILS + " ADD "
					+ GroupColumn.GROUP_SYSTEM_ENTRY_DATE + " text" + ";";
			db.execSQL(alter_query6);

			break;
		case 3:
			String alter_query7 = "ALTER TABLE " + TABLES.TABLE_GROUP_NAME_DETAILS + " ADD "
					+ GroupColumn.GROUP_LAST_TRANSACTIONDATE + " text" + ";";
			db.execSQL(alter_query7);
			String alter_query8 = "ALTER TABLE " + TABLES.TABLE_GROUP_NAME_DETAILS + " ADD "
					+ GroupColumn.GROUP_SYSTEM_ENTRY_DATE + " text" + ";";
			db.execSQL(alter_query8);
			break;
		case 4:
			db.execSQL(BankDetailsProvider.CREATE_BANK_DETAILS_TABLE);
			break;

		}

	}

}