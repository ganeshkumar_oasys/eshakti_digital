package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class GroupGLOSUpdateResponse extends BaseResponse {

	public GroupGLOSUpdateResponse(final TransactionResult transactionResult) {
		super(transactionResult);
	}

	public GroupGLOSUpdateResponse(final boolean result) {
		super(TransactionResult.SUCCESS);
	}
}