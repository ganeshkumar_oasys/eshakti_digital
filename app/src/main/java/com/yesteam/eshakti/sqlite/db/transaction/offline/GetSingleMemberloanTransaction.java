package com.yesteam.eshakti.sqlite.db.transaction.offline;

import com.yesteam.eshakti.sqlite.database.GroupDetailsProvider;
import com.yesteam.eshakti.sqlite.database.response.GroupMemberloanOSResponse;
import com.yesteam.eshakti.sqlite.db.model.GroupMasterSingle;
import com.yesteam.eshakti.sqlite.db.transactions.AbstractTransaction;

public class GetSingleMemberloanTransaction extends
		AbstractTransaction<GroupMasterSingle, GroupMemberloanOSResponse> {

	@Override
	public int onPreExecute() {
		return EXECUTE_IN_WORKER_THREAD;
	}

	@Override
	public void run() {

		boolean value = GroupDetailsProvider.getSinlgeGroupMaster(getParams());

		GroupMemberloanOSResponse transResponse = new GroupMemberloanOSResponse(
				value);
		deliverResponse(transResponse);

	}

}