package com.yesteam.eshakti.sqlite.db.transaction.offline;

import com.yesteam.eshakti.sqlite.database.MemberPhotoProvider;
import com.yesteam.eshakti.sqlite.database.response.MemberPhotoMasterUpdateResponse;
import com.yesteam.eshakti.sqlite.db.model.MemberPhotoMasterUpdate;
import com.yesteam.eshakti.sqlite.db.transactions.AbstractTransaction;

public class MemberPhotoUpdateTransaction
		extends AbstractTransaction<MemberPhotoMasterUpdate, MemberPhotoMasterUpdateResponse> {

	@Override
	public int onPreExecute() {
		return EXECUTE_IN_WORKER_THREAD;
	}

	@Override
	public void run() {

		boolean value = MemberPhotoProvider.updateMemberPhoto(getParams());

		MemberPhotoMasterUpdateResponse transResponse = new MemberPhotoMasterUpdateResponse(value);

		deliverResponse(transResponse);

	}

}