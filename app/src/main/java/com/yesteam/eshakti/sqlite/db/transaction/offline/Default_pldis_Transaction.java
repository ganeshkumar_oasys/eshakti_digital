package com.yesteam.eshakti.sqlite.db.transaction.offline;

import com.yesteam.eshakti.sqlite.database.GroupDetailsProvider;
import com.yesteam.eshakti.sqlite.database.response.DefaultOffline_PL_DisResponse;
import com.yesteam.eshakti.sqlite.db.model.GroupMasterSingle;
import com.yesteam.eshakti.sqlite.db.transactions.AbstractTransaction;

public class Default_pldis_Transaction extends AbstractTransaction<GroupMasterSingle, DefaultOffline_PL_DisResponse> {

	@Override
	public int onPreExecute() {
		return EXECUTE_IN_WORKER_THREAD;
	}

	@Override
	public void run() {

		boolean value = GroupDetailsProvider.getSinlgeGroupMaster(getParams());
		DefaultOffline_PL_DisResponse transResponse = new DefaultOffline_PL_DisResponse(value);
		deliverResponse(transResponse);

	}

}
