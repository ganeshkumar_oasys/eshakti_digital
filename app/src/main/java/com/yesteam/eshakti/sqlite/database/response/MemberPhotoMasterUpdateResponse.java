package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class MemberPhotoMasterUpdateResponse extends BaseResponse {

	public MemberPhotoMasterUpdateResponse(final TransactionResult transactionResult) {
		super(transactionResult);
	}

	public MemberPhotoMasterUpdateResponse(final boolean result) {
		super(TransactionResult.SUCCESS);
	}
}
