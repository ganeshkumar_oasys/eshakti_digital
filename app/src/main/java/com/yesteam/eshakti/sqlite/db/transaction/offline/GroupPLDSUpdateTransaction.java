package com.yesteam.eshakti.sqlite.db.transaction.offline;

import com.yesteam.eshakti.sqlite.database.GroupDetailsProvider;
import com.yesteam.eshakti.sqlite.database.response.GroupPLOSUpdateResponse;
import com.yesteam.eshakti.sqlite.db.model.GroupMLUpdate;
import com.yesteam.eshakti.sqlite.db.transactions.AbstractTransaction;

public class GroupPLDSUpdateTransaction extends AbstractTransaction<GroupMLUpdate, GroupPLOSUpdateResponse> {

	@Override
	public int onPreExecute() {
		return EXECUTE_IN_WORKER_THREAD;
	}

	@Override
	public void run() {

		boolean value = GroupDetailsProvider.updateGroupMLOS(getParams());

		GroupPLOSUpdateResponse transResponse = new GroupPLOSUpdateResponse(value);

		deliverResponse(transResponse);

	}
}
