package com.yesteam.eshakti.sqlite.db.transaction.offline;

import com.yesteam.eshakti.sqlite.database.GroupDetailsProvider;
import com.yesteam.eshakti.sqlite.database.response.GetMin_MeetingResponse;
import com.yesteam.eshakti.sqlite.db.model.GroupMasterSingle;
import com.yesteam.eshakti.sqlite.db.transactions.AbstractTransaction;

public class Get_Min_MeetingTransaction extends AbstractTransaction<GroupMasterSingle, GetMin_MeetingResponse> {

	@Override
	public int onPreExecute() {
		return EXECUTE_IN_WORKER_THREAD;
	}

	@Override
	public void run() {

		boolean value = GroupDetailsProvider.getSinlgeGroupMaster(getParams());

		GetMin_MeetingResponse transResponse = new GetMin_MeetingResponse(value);
		deliverResponse(transResponse);

	}

}