package com.yesteam.eshakti.sqlite.db.transaction.offline;

import com.yesteam.eshakti.sqlite.database.LoginProvider;
import com.yesteam.eshakti.sqlite.database.response.LoginUpdateResponse;
import com.yesteam.eshakti.sqlite.db.model.LoginUpdate;
import com.yesteam.eshakti.sqlite.db.transactions.AbstractTransaction;

public class LoginUpdateTransaction extends
		AbstractTransaction<LoginUpdate, LoginUpdateResponse> {

	@Override
	public int onPreExecute() {
		return EXECUTE_IN_WORKER_THREAD;
	}

	@Override
	public void run() {

		boolean value = LoginProvider.updateEntry(getParams());

		LoginUpdateResponse transResponse = new LoginUpdateResponse(value);
		deliverResponse(transResponse);

	}

}