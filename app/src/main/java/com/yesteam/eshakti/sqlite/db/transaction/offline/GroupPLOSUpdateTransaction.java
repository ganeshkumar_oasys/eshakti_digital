package com.yesteam.eshakti.sqlite.db.transaction.offline;

import com.yesteam.eshakti.sqlite.database.GroupDetailsProvider;
import com.yesteam.eshakti.sqlite.database.response.GroupPLOS_Update_Response;
import com.yesteam.eshakti.sqlite.db.model.GroupMLUpdate;
import com.yesteam.eshakti.sqlite.db.transactions.AbstractTransaction;

public class GroupPLOSUpdateTransaction extends
		AbstractTransaction<GroupMLUpdate, GroupPLOS_Update_Response> {

	@Override
	public int onPreExecute() {
		return EXECUTE_IN_WORKER_THREAD;
	}

	@Override
	public void run() {

		boolean value = GroupDetailsProvider.updateGroupMLOS(getParams());

		GroupPLOS_Update_Response transResponse = new GroupPLOS_Update_Response(
				value);

		deliverResponse(transResponse);

	}
}
