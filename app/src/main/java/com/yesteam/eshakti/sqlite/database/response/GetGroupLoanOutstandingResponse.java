package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class GetGroupLoanOutstandingResponse extends BaseResponse {

	public GetGroupLoanOutstandingResponse(final TransactionResult transactionResult) {
		super(transactionResult);
	}

	public GetGroupLoanOutstandingResponse(final boolean result) {
		super(TransactionResult.SUCCESS);
	}

}