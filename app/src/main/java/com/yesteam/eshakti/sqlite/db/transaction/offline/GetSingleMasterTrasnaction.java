package com.yesteam.eshakti.sqlite.db.transaction.offline;

import com.yesteam.eshakti.sqlite.database.TransactionProvider;
import com.yesteam.eshakti.sqlite.database.response.GetSingleTransResponse;
import com.yesteam.eshakti.sqlite.db.model.GetTransactionSingle;
import com.yesteam.eshakti.sqlite.db.transactions.AbstractTransaction;

public class GetSingleMasterTrasnaction extends
		AbstractTransaction<GetTransactionSingle, GetSingleTransResponse> {

	@Override
	public int onPreExecute() {
		return EXECUTE_IN_WORKER_THREAD;
	}

	@Override
	public void run() {

		boolean value = TransactionProvider.getSinlgeTransaction(getParams());

		GetSingleTransResponse transResponse = new GetSingleTransResponse(value);
		deliverResponse(transResponse);

	}

}
