package com.yesteam.eshakti.sqlite.db.model;

import java.io.Serializable;

public class AadharCardMasterUpdate implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8965542762624734237L;
	private static String mMasterUpdateValues;
	private static String mMasterUniqueId;

	public static String getMasterUpdateValues() {
		return mMasterUpdateValues;
	}

	public static void setMasterUpdateValues(String mMasterUpdateValues) {
		AadharCardMasterUpdate.mMasterUpdateValues = mMasterUpdateValues;
	}

	public static String getMasterUniqueId() {
		return mMasterUniqueId;
	}

	public static void setMasterUniqueId(String mMasterUniqueId) {
		AadharCardMasterUpdate.mMasterUniqueId = mMasterUniqueId;
	}

}