package com.yesteam.eshakti.sqlite.database.response;

import com.yesteam.eshakti.transaction.response.BaseResponse;

public class TransDeleteResponse extends BaseResponse {

	public TransDeleteResponse(final TransactionResult transactionResult) {
		super(transactionResult);
	}

	public TransDeleteResponse(final boolean result) {
		super(TransactionResult.SUCCESS);
	}

}