package com.yesteam.eshakti.intro.utils;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.List;

class PagerAdapter extends FragmentPagerAdapter {
	private List<Fragment> fragments;

	public PagerAdapter(FragmentManager fm, @NonNull List<Fragment> fragments) {
		super(fm);
		this.fragments = fragments;
	}

	@Override
	public Fragment getItem(int position) {
		return this.fragments.get(position);
	}

	@Override
	public int getCount() {
		return this.fragments.size();
	}

	@NonNull
	public List<Fragment> getFragments() {
		return fragments;
	}
}
