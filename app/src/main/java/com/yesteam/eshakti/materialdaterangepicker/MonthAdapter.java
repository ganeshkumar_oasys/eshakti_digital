
package com.yesteam.eshakti.materialdaterangepicker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import com.oasys.eshakti.digitization.R;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.materialdaterangepicker.MonthView.OnDayClickListener;
import com.yesteam.eshakti.view.activity.Audit_Date_selectionActivity;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.view.fragment.Meeting_AuditingFragment;
import com.yesteam.eshakti.view.fragment.Meeting_TrainingFragment;
import com.yesteam.eshakti.view.fragment.Reports_Trial_BalanceSheetFragment;
import com.yesteam.eshakti.view.fragment.Transaction_InternalBank_MFI_LoanFragment;
import com.yesteam.eshakti.view.fragment.Transaction_LoanDisbursementFromRepaidFragment;
import com.yesteam.eshakti.view.fragment.Transaction_Loan_disburse_LoanAccFragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.LayoutParams;
import android.widget.BaseAdapter;

/**
 * An adapter for a list of {@link MonthView} items.
 */
public abstract class MonthAdapter extends BaseAdapter implements OnDayClickListener {

    // private static final String TAG = "SimpleMonthAdapter";

    private final Context mContext;
    protected final DatePickerController mController;

    private CalendarDay mSelectedDay;

    private int mAccentColor = -1;

    protected static int WEEK_7_OVERHANG_HEIGHT = 7;
    protected static final int MONTHS_IN_YEAR = 12;

    public static String sSelectedDate = "", sSelectedFromDate = "", balanceSheetDate = "", sTrainingDate = "";
    public static String sCurrentDate;
    private Date date1, date2, date3;
    Calendar mCalendar = null;

    /**
     * A convenience class to represent a specific date.
     */
    public static class CalendarDay {
        private Calendar calendar;
        int year;
        int month;
        int day;

        public CalendarDay() {
            setTime(System.currentTimeMillis());
        }

        public CalendarDay(long timeInMillis) {
            setTime(timeInMillis);
        }

        public CalendarDay(Calendar calendar) {
            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            day = calendar.get(Calendar.DAY_OF_MONTH);
        }

        public CalendarDay(int year, int month, int day) {
            setDay(year, month, day);
        }

        public void set(CalendarDay date) {
            year = date.year;
            month = date.month;
            day = date.day;
        }

        public void setDay(int year, int month, int day) {
            this.year = year;
            this.month = month;
            this.day = day;
        }

        private void setTime(long timeInMillis) {
            if (calendar == null) {
                calendar = Calendar.getInstance();
            }
            calendar.setTimeInMillis(timeInMillis);
            month = calendar.get(Calendar.MONTH);
            year = calendar.get(Calendar.YEAR);
            day = calendar.get(Calendar.DAY_OF_MONTH);
        }

        public int getYear() {
            return year;
        }

        public int getMonth() {
            return month;
        }

        public int getDay() {
            return day;
        }
    }

    public MonthAdapter(Context context, DatePickerController controller) {
        mContext = context;
        mController = controller;
        init();
        setSelectedDay(mController.getSelectedDay());
    }

    public void setAccentColor(int color) {
        mAccentColor = color;
    }

    /**
     * Updates the selected day and related parameters.
     *
     * @param day The day to highlight
     */
    public void setSelectedDay(CalendarDay day) {
        mSelectedDay = day;
        notifyDataSetChanged();
    }

    public CalendarDay getSelectedDay() {
        return mSelectedDay;
    }

    /**
     * Set up the gesture detector and selected time
     */

    protected void init() {

        if (Transaction_InternalBank_MFI_LoanFragment.bankLoan_check.equals("1")
                || Transaction_InternalBank_MFI_LoanFragment.bankLoan_check.equals("2")) {

            mCalendar = Calendar.getInstance();
            String lastTrDateArr[] = DatePickerDialog.sDashboardDate.split("-");
            mCalendar.set(Integer.parseInt(lastTrDateArr[2]), Integer.parseInt(lastTrDateArr[1]) - 1,
                    Integer.parseInt(lastTrDateArr[0]));

            mSelectedDay = new CalendarDay(mCalendar.getTimeInMillis());
            sCurrentDate = (mSelectedDay.getDay() < 10 ? ("0" + mSelectedDay.getDay()) : (mSelectedDay.getDay())) + "-"
                    + (mSelectedDay.getMonth() < 9 ? ("0" + (mSelectedDay.getMonth() + 1))
                    : (mSelectedDay.getMonth() + 1))
                    + "-" + mSelectedDay.getYear();
            Log.e("CURRENT DATER", sCurrentDate);
            sSelectedDate = sCurrentDate;

        } else if (Meeting_AuditingFragment.audit_check.equals("1")
                || Transaction_Loan_disburse_LoanAccFragment.disbursementDate_check.equals("1")
                || Transaction_LoanDisbursementFromRepaidFragment.disbursement_Date_check.equals("1")) {

            mCalendar = Calendar.getInstance();
            String lastTrDateArr[] = DatePickerDialog.sDashboardDate.split("-");
            mCalendar.set(Integer.parseInt(lastTrDateArr[2]), Integer.parseInt(lastTrDateArr[1]) - 1,
                    Integer.parseInt(lastTrDateArr[0]));

            mSelectedDay = new CalendarDay(mCalendar.getTimeInMillis());
            sCurrentDate = (mSelectedDay.getDay() < 10 ? ("0" + mSelectedDay.getDay()) : (mSelectedDay.getDay())) + "-"
                    + (mSelectedDay.getMonth() < 9 ? ("0" + (mSelectedDay.getMonth() + 1))
                    : (mSelectedDay.getMonth() + 1))
                    + "-" + mSelectedDay.getYear();
            Log.e("CURRENT DATER", sCurrentDate);
            sSelectedDate = sCurrentDate;

        } else if (Meeting_AuditingFragment.audit_check.equals("2")
                || Meeting_AuditingFragment.audit_check.equals("3")) {

            mSelectedDay = new CalendarDay(System.currentTimeMillis());
            sCurrentDate = (mSelectedDay.getDay() < 10 ? ("0" + mSelectedDay.getDay()) : (mSelectedDay.getDay())) + "-"
                    + (mSelectedDay.getMonth() < 9 ? ("0" + (mSelectedDay.getMonth() + 1))
                    : (mSelectedDay.getMonth() + 1))
                    + "-" + mSelectedDay.getYear();
            Log.e("CURRENT DATE", sCurrentDate);
            sSelectedDate = sCurrentDate;

        } else {

            if (EShaktiApplication.getNextMonthLastDate() == null) {
                mSelectedDay = new CalendarDay(System.currentTimeMillis());
                sCurrentDate = (mSelectedDay.getDay() < 10 ? ("0" + mSelectedDay.getDay()) : (mSelectedDay.getDay()))
                        + "-" + (mSelectedDay.getMonth() < 9 ? ("0" + (mSelectedDay.getMonth() + 1))
                        : (mSelectedDay.getMonth() + 1))
                        + "-" + mSelectedDay.getYear();
                Log.e("CURRENT DATE", sCurrentDate);
                sSelectedDate = sCurrentDate;
            } else {

                mCalendar = Calendar.getInstance();
                String lastTrDateArr[] = EShaktiApplication.getNextMonthLastDate().split("-");
                mCalendar.set(Integer.parseInt(lastTrDateArr[2]), Integer.parseInt(lastTrDateArr[1]) - 1,
                        Integer.parseInt(lastTrDateArr[0]));

                mSelectedDay = new CalendarDay(mCalendar.getTimeInMillis());
                sCurrentDate = (mSelectedDay.getDay() < 10 ? ("0" + mSelectedDay.getDay()) : (mSelectedDay.getDay()))
                        + "-" + (mSelectedDay.getMonth() < 9 ? ("0" + (mSelectedDay.getMonth() + 1))
                        : (mSelectedDay.getMonth() + 1))
                        + "-" + mSelectedDay.getYear();
                Log.e("CURRENT DATER", sCurrentDate);
                sSelectedDate = sCurrentDate;
            }
        }

    }

    @Override
    public int getCount() {
        return ((mController.getMaxYear() - mController.getMinYear()) + 1) * MONTHS_IN_YEAR;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @SuppressLint("NewApi")
    @SuppressWarnings("unchecked")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MonthView v;
        HashMap<String, Integer> drawingParams = null;
        if (convertView != null) {
            v = (MonthView) convertView;
            // We store the drawing parameters in the view so it can be recycled
            drawingParams = (HashMap<String, Integer>) v.getTag();
        } else {
            v = createMonthView(mContext);
            // Set up the new view
            LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
            v.setLayoutParams(params);
            v.setClickable(true);
            v.setOnDayClickListener(this);
            if (mAccentColor != -1) {
                v.setAccentColor(mAccentColor);
            }
        }
        if (drawingParams == null) {
            drawingParams = new HashMap<String, Integer>();
        }
        drawingParams.clear();

        final int month = position % MONTHS_IN_YEAR;
        final int year = position / MONTHS_IN_YEAR + mController.getMinYear();

        int selectedDay = -1;
        if (isSelectedDayInMonth(year, month)) {
            selectedDay = mSelectedDay.day;
        }

        // Invokes requestLayout() to ensure that the recycled view is set with
        // the appropriate
        // height/number of weeks before being displayed.
        v.reuse();

        drawingParams.put(MonthView.VIEW_PARAMS_SELECTED_DAY, selectedDay);
        drawingParams.put(MonthView.VIEW_PARAMS_YEAR, year);
        drawingParams.put(MonthView.VIEW_PARAMS_MONTH, month);
        drawingParams.put(MonthView.VIEW_PARAMS_WEEK_START, mController.getFirstDayOfWeek());
        v.setMonthParams(drawingParams);
        v.invalidate();
        return v;
    }

    public abstract MonthView createMonthView(Context context);

    private boolean isSelectedDayInMonth(int year, int month) {
        return mSelectedDay.year == year && mSelectedDay.month == month;
    }

    public void onDayClick(MonthView view, CalendarDay day) {
        if (day != null) {
            onDayTapped(day);
        }
    }

    /**
     * Maintains the same hour/min/sec but moves the day to the tapped
     * day.(month<10?("0"+month):(month))
     *
     * @param day The day that was tapped
     */
    protected void onDayTapped(CalendarDay day) {
        mController.tryVibrate();
        mController.onDayOfMonthSelected(day.year, day.month, day.day);
        setSelectedDay(day);
        DatePickerDialog.okButton.setClickable(true);

        sSelectedDate = (day.getDay() < 10 ? ("0" + day.getDay()) : (day.getDay())) + "-"
                + (day.getMonth() < 9 ? ("0" + (day.getMonth() + 1)) : (day.getMonth() + 1)) + "-" + day.getYear();
        Log.e("SELECTED DATE ", sSelectedDate);

        DatePickerDialog.selectedDateText
                .setText(RegionalConversion.getRegionalConversion(AppStrings.selectedDate) + " : " + sSelectedDate);
        DatePickerDialog.selectedDateText.setTypeface(LoginActivity.sTypeface);
        DatePickerDialog.selectedDateText.setTextColor(R.color.colorPrimary);

        if (Reports_Trial_BalanceSheetFragment.trial_balance_check.equals("2")
                && Meeting_AuditingFragment.audit_check.equals("")
                && Meeting_TrainingFragment.training_check.equals("")) {

            String fromDateArr[] = DatePickerDialog.sSelectedFromDate.split("/");
            sSelectedFromDate = fromDateArr[1] + "-" + fromDateArr[0] + "-" + fromDateArr[2];
            compareDates(sSelectedFromDate);
        } else if (Meeting_AuditingFragment.audit_check.equals("2")) {
            String fromDateArr[] = DatePickerDialog.sAuditFromDate.split("/");
            sSelectedFromDate = fromDateArr[1] + "-" + fromDateArr[0] + "-" + fromDateArr[2];
            compareDates(sSelectedFromDate);
        } else if (Transaction_InternalBank_MFI_LoanFragment.bankLoan_check.equals("2")) {
            if (Transaction_InternalBank_MFI_LoanFragment.loanSancDate != null
                    && !Transaction_InternalBank_MFI_LoanFragment.loanSancDate.equals("")) {
                String loanSanctionDateArr[] = Transaction_InternalBank_MFI_LoanFragment.loanSancDate.split("/");
                sSelectedFromDate = loanSanctionDateArr[1] + "-" + loanSanctionDateArr[0] + "-"
                        + loanSanctionDateArr[2];
                compareDates(sSelectedFromDate);
            }
        } else if (Audit_Date_selectionActivity.audit_trial_balance_check.equals("2")) {
            String fromDateArr[] = DatePickerDialog.sSelectedFromDate.split("/");
            sSelectedFromDate = fromDateArr[1] + "-" + fromDateArr[0] + "-" + fromDateArr[2];
            compareDates(sSelectedFromDate);
        } else if (Transaction_Loan_disburse_LoanAccFragment.disbursementDate_check.equals("1")) {
            if (EShaktiApplication.getLoanAcc_LoanDisbursementDate() != null) {
                String loanDisbDateArr[] = EShaktiApplication.getLoanAcc_LoanDisbursementDate().split("/");
                sSelectedFromDate = loanDisbDateArr[1] + "-" + loanDisbDateArr[0] + "-" + loanDisbDateArr[2];
                compareDates(sSelectedFromDate);
            }
        } else if (Transaction_LoanDisbursementFromRepaidFragment.disbursement_Date_check.equals("1")) {
            if (EShaktiApplication.getLoanAcc_LoanDisbursementDate() != null) {
                String loanDisburesementDateArr[] = EShaktiApplication.getLoanAcc_LoanDisbursementDate().split("/");
                sSelectedFromDate = loanDisburesementDateArr[1] + "-" + loanDisburesementDateArr[0] + "-"
                        + loanDisburesementDateArr[2];
                compareDates(sSelectedFromDate);
            }
        }

    }

    private void compareDates(String balanceDate) {
        // TODO Auto-generated method stub

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        /*
         * String calResponse[] = String.valueOf(
         * SelectedGroupsTask.sBalanceSheetDate_Response).split("/"); balanceSheetDate =
         * calResponse[0] + "-" + calResponse[1] + "-" + calResponse[2];
         */
        /** User Selected Date **/
        try {
            date1 = sdf.parse(sSelectedDate);
        } catch (ParseException e) {
            // TODO: handle exception
            e.printStackTrace();
        }

        /** BalanceSheetDate **/
        try {
            date2 = sdf.parse(balanceDate);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        /** Current Date **/
        try {

            date3 = sdf.parse(sCurrentDate);

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        /**
         * Compares the date of SelectedDate with BalanceSheetDate & CurrentDate
         **/

        if ((date1.compareTo(date2)) >= 0 && (date1.compareTo(date3) <= 0)) {

            DatePickerDialog.okButton.setClickable(true);

        } else {

            if (Transaction_InternalBank_MFI_LoanFragment.bankLoan_check != null) {
                if (!Transaction_InternalBank_MFI_LoanFragment.bankLoan_check.equals("1")
                        && !Transaction_InternalBank_MFI_LoanFragment.bankLoan_check.equals("2")) {

                    System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ 1");
                    DatePickerDialog.okButton.setClickable(false);

                    TastyToast.makeText(mContext, AppStrings.calAlert, TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                }

            } else if (Meeting_AuditingFragment.audit_check != null) {
                if (!Meeting_AuditingFragment.audit_check.equals("1")
                        && !Meeting_AuditingFragment.audit_check.equals("2")
                        && !Meeting_AuditingFragment.audit_check.equals("3")) {

                    System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ 2");
                    DatePickerDialog.okButton.setClickable(false);

                    TastyToast.makeText(mContext, AppStrings.calAlert, TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                }
            } else {
                System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ 3");
                DatePickerDialog.okButton.setClickable(false);

                TastyToast.makeText(mContext, AppStrings.calAlert, TastyToast.LENGTH_SHORT, TastyToast.WARNING);

            }

        }

    }

}
