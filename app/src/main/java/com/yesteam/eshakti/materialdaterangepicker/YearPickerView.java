package com.yesteam.eshakti.materialdaterangepicker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.oasys.eshakti.digitization.R;
import com.tutorialsee.lib.TastyToast;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog.OnDateChangedListener;
import com.yesteam.eshakti.view.fragment.Meeting_AuditingFragment;
import com.yesteam.eshakti.view.fragment.Transaction_InternalBank_MFI_LoanFragment;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.StateListDrawable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Displays a selectable list of years.
 */
public class YearPickerView extends ListView implements OnItemClickListener, OnDateChangedListener {
	// private static final String TAG = "YearPickerView";

	private final DatePickerController mController;
	private YearAdapter mAdapter;
	private int mViewSize;
	private int mChildSize;
	private TextViewWithCircularIndicator mSelectedView;
	private int mAccentColor;

	/**
	 * @param context
	 */
	public YearPickerView(Context context, DatePickerController controller) {
		super(context);
		mController = controller;
		mController.registerOnDateChangedListener(this);
		ViewGroup.LayoutParams frame = new ViewGroup.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		setLayoutParams(frame);
		Resources res = context.getResources();
		mViewSize = res.getDimensionPixelOffset(R.dimen.mdtp_date_picker_view_animator_height);
		mChildSize = res.getDimensionPixelOffset(R.dimen.mdtp_year_label_height);
		setVerticalFadingEdgeEnabled(true);
		setFadingEdgeLength(mChildSize / 3);
		init(context);
		setOnItemClickListener(this);
		setSelector(new StateListDrawable());
		setDividerHeight(0);
		onDateChanged();
	}

	private void init(Context context) {
		ArrayList<String> years = new ArrayList<String>();
		for (int year = mController.getMinYear(); year <= mController.getMaxYear(); year++) {
			years.add(String.format("%d", year));
		}
		mAdapter = new YearAdapter(context, R.layout.mdtp_year_label_text_view, years);
		setAdapter(mAdapter);
	}

	public void setAccentColor(int accentColor) {
		mAccentColor = accentColor;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		mController.tryVibrate();
		TextViewWithCircularIndicator clickedView = (TextViewWithCircularIndicator) view;
		if (clickedView != null) {
			if (clickedView != mSelectedView) {
				if (mSelectedView != null) {
					mSelectedView.drawIndicator(false);
					mSelectedView.requestLayout();
				}
				clickedView.drawIndicator(true);
				clickedView.requestLayout();
				mSelectedView = clickedView;
			}
			mController.onYearSelected(getYearFromTextView(clickedView));
			mAdapter.notifyDataSetChanged();
		}
	}

	private static int getYearFromTextView(TextView view) {
		return Integer.valueOf(view.getText().toString());
	}

	private class YearAdapter extends ArrayAdapter<String> {

		public YearAdapter(Context context, int resource, List<String> objects) {
			super(context, resource, objects);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			TextViewWithCircularIndicator v = (TextViewWithCircularIndicator) super.getView(position, convertView,
					parent);
			v.setAccentColor(mAccentColor);
			v.requestLayout();
			int year = getYearFromTextView(v);
			boolean selected = mController.getSelectedDay().year == year;
			v.drawIndicator(selected);
			if (selected) {
				mSelectedView = v;
			}
			return v;
		}
	}

	public void postSetSelectionCentered(final int position) {
		postSetSelectionFromTop(position, mViewSize / 2 - mChildSize / 2);
	}

	public void postSetSelectionFromTop(final int position, final int offset) {
		post(new Runnable() {

			@Override
			public void run() {
				setSelectionFromTop(position, offset);
				requestLayout();
			}
		});
	}

	public int getFirstPositionOffset() {
		final View firstChild = getChildAt(0);
		if (firstChild == null) {
			return 0;
		}
		return firstChild.getTop();
	}

	@Override
	public void onDateChanged() {
		mAdapter.notifyDataSetChanged();
		postSetSelectionCentered(mController.getSelectedDay().year - mController.getMinYear());

		String balancesheet_date = String.valueOf(SelectedGroupsTask.sBalanceSheetDate_Response);
		String dateArr[] = balancesheet_date.split("/");
		String bal_date = dateArr[0] + "-" + dateArr[1] + "-" + dateArr[2];
		Log.e("Balancesheet date", bal_date);

		String selectedDate = (mController.getSelectedDay().day < 10 ? ("0" + mController.getSelectedDay().day)
				: (mController.getSelectedDay().day)) + "-"
				+ (mController.getSelectedDay().month < 9 ? ("0" + (mController.getSelectedDay().month + 1))
						: (mController.getSelectedDay().month + 1))
				+ "-" + mController.getSelectedDay().year;
		Log.e("selectedDate ", selectedDate);
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		Date balanceDate = null, currentDate = null;
		try {
			balanceDate = sdf.parse(bal_date);
			currentDate = sdf.parse(selectedDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if ((currentDate.compareTo(balanceDate)) < 0) {

			if (Transaction_InternalBank_MFI_LoanFragment.bankLoan_check != null) {
				if (Transaction_InternalBank_MFI_LoanFragment.bankLoan_check.equals("1")
						&& Transaction_InternalBank_MFI_LoanFragment.bankLoan_check.equals("2")) {

					System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& 1");
					DatePickerDialog.okButton.setClickable(false);

					TastyToast.makeText(getContext(), AppStrings.calAlert, TastyToast.LENGTH_SHORT, TastyToast.WARNING);
				}

			} else if (Meeting_AuditingFragment.audit_check != null) {
				if (Meeting_AuditingFragment.audit_check.equals("1") && Meeting_AuditingFragment.audit_check.equals("2")
						&& Meeting_AuditingFragment.audit_check.equals("3")) {

					System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& 2");
					DatePickerDialog.okButton.setClickable(false);

					TastyToast.makeText(getContext(), AppStrings.calAlert, TastyToast.LENGTH_SHORT, TastyToast.WARNING);
				}
			} else {
				System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& 3");
				DatePickerDialog.okButton.setClickable(false);

				TastyToast.makeText(getContext(), AppStrings.calAlert, TastyToast.LENGTH_SHORT, TastyToast.WARNING);

			}
		}
	}

	@Override
	public void onInitializeAccessibilityEvent(AccessibilityEvent event) {
		super.onInitializeAccessibilityEvent(event);
		if (event.getEventType() == AccessibilityEvent.TYPE_VIEW_SCROLLED) {
			event.setFromIndex(0);
			event.setToIndex(0);
		}
	}
}
