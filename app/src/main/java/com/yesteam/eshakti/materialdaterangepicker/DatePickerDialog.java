package com.yesteam.eshakti.materialdaterangepicker;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.tutorialsee.lib.TastyToast;
import com.yesteam.eshakti.Config.utils.RegionalConversion;

import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.materialdaterangepicker.MonthAdapter.CalendarDay;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.utils.Reset;
import com.yesteam.eshakti.view.activity.Audit_Date_selectionActivity;
import com.yesteam.eshakti.view.activity.EditOpenBalanceDialogActivity;
import com.yesteam.eshakti.view.activity.EditOpeningBalanceSavingsActivity;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.view.activity.MainActivity;
import com.yesteam.eshakti.view.fragment.FragmentDrawer;
import com.yesteam.eshakti.view.fragment.MainFragment_Dashboard;
import com.yesteam.eshakti.view.fragment.Meeting_AttendanceFragment;
import com.yesteam.eshakti.view.fragment.Meeting_AuditingFragment;
import com.yesteam.eshakti.view.fragment.Meeting_MinutesOfMeetingFragment;
import com.yesteam.eshakti.view.fragment.Meeting_TrainingFragment;
import com.yesteam.eshakti.view.fragment.Reports_Trial_BalanceSheetFragment;
import com.yesteam.eshakti.view.fragment.Transaction_BankDepositFragment;
import com.yesteam.eshakti.view.fragment.Transaction_ExpensesFragment;
import com.yesteam.eshakti.view.fragment.Transaction_GroupLoanRepaidMenuFragment;
import com.yesteam.eshakti.view.fragment.Transaction_IncomeMenuFragment;
import com.yesteam.eshakti.view.fragment.Transaction_InternalBank_MFI_LoanFragment;
import com.yesteam.eshakti.view.fragment.Transaction_InternalloanMenuFragment;
import com.yesteam.eshakti.view.fragment.Transaction_LoanDisbursementFromRepaidFragment;
import com.yesteam.eshakti.view.fragment.Transaction_Loan_disburse_LoanAccFragment;
import com.yesteam.eshakti.view.fragment.Transaction_Loandisbursement_MentFragment;
import com.yesteam.eshakti.view.fragment.Transaction_MemLoanRepaid_MenuFragment;
import com.yesteam.eshakti.view.fragment.Transaction_SavingsFragment;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;

/**
 * Dialog allowing users to select a date.
 */
public class DatePickerDialog extends DialogFragment implements OnClickListener, DatePickerController {

	private static final String TAG = "DatePickerDialog";

	private static final int UNINITIALIZED = -1;
	private static final int MONTH_AND_DAY_VIEW = 0;
	private static final int YEAR_VIEW = 1;

	private static final String KEY_SELECTED_YEAR = "year";
	private static final String KEY_SELECTED_MONTH = "month";
	private static final String KEY_SELECTED_DAY = "day";
	private static final String KEY_LIST_POSITION = "list_position";
	private static final String KEY_WEEK_START = "week_start";
	private static final String KEY_YEAR_START = "year_start";
	private static final String KEY_YEAR_END = "year_end";
	private static final String KEY_CURRENT_VIEW = "current_view";
	private static final String KEY_LIST_POSITION_OFFSET = "list_position_offset";
	private static final String KEY_MIN_DATE = "min_date";
	private static final String KEY_MAX_DATE = "max_date";
	private static final String KEY_HIGHLIGHTED_DAYS = "highlighted_days";
	private static final String KEY_SELECTABLE_DAYS = "selectable_days";
	private static final String KEY_THEME_DARK = "theme_dark";
	private static final String KEY_ACCENT = "accent";
	private static final String KEY_VIBRATE = "vibrate";
	private static final String KEY_DISMISS = "dismiss";

	private static final int DEFAULT_START_YEAR = 1900;
	private static final int DEFAULT_END_YEAR = 2100;

	private static final int ANIMATION_DURATION = 300;
	private static final int ANIMATION_DELAY = 500;

	private static SimpleDateFormat YEAR_FORMAT = new SimpleDateFormat("yyyy", Locale.ENGLISH);
	private static SimpleDateFormat DAY_FORMAT = new SimpleDateFormat("dd", Locale.ENGLISH);

	private Calendar mCalendar = Calendar.getInstance();
	private OnDateSetListener mCallBack;
	private HashSet<OnDateChangedListener> mListeners = new HashSet<OnDateChangedListener>();
	private DialogInterface.OnCancelListener mOnCancelListener;
	private DialogInterface.OnDismissListener mOnDismissListener;

	private AccessibleDateAnimator mAnimator;

	private TextView mDayOfWeekView;
	private LinearLayout mMonthAndDayView;
	private TextView mSelectedMonthTextView;
	private TextView mSelectedDayTextView;
	private TextView mYearView;
	private DayPickerView mDayPickerView;
	private YearPickerView mYearPickerView;

	private int mCurrentView = UNINITIALIZED;

	private int mWeekStart = mCalendar.getFirstDayOfWeek();
	private int mMinYear = DEFAULT_START_YEAR;
	private int mMaxYear = DEFAULT_END_YEAR;
	private Calendar mMinDate;
	private Calendar mMaxDate;
	private Calendar[] highlightedDays;
	private Calendar[] selectableDays;

	private boolean mThemeDark;
	private int mAccentColor = -1;
	private boolean mVibrate;
	private boolean mDismissOnPause;

	private HapticFeedbackController mHapticFeedbackController;

	private boolean mDelayAnimation = true;

	// Accessibility strings.
	private String mDayPickerDescription;
	private String mSelectDay;
	private String mYearPickerDescription;
	private String mSelectYear;

	public static TextView balnceSheet_DateText, selectedDateText;
	public static String sSelectedDate = "", sDashboardDate = "", sSend_To_Server_Date = "";
	public static String sSelectedFromDate = null, sSelectedToDate = null, sTrainingDate = null;
	public static String sAuditFromDate = null, sAuditToDate = null, sAuditDate = null;
	public static String sLoan_Sanc_Date = null, sLoan_Dist_Date = null, sDist_Date = null, sRepaid_DisbDate;

	public static TextView okButton;
	int mMonthCount = 0;
	int mYearCount = 0;

	// private int tabTag=1;

	Fragment fragment;

	/**
	 * The callback used to indicate the user is done filling in the date.
	 */
	public interface OnDateSetListener {

		/**
		 * @param view
		 *            The view associated with this listener.
		 * @param year
		 *            The year that was set.
		 * @param monthOfYear
		 *            The month that was set (0-11) for compatibility with
		 *            {@link Calendar}.
		 * @param dayOfMonth
		 *            The day of the month that was set.
		 */
		void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth);// ,int
																							// yearEnd,
																							// int
																							// monthOfYearEnd,
																							// int
																							// dayOfMonthEnd);
	}

	/**
	 * The callback used to notify other date picker components of a change in
	 * selected date.
	 */
	public interface OnDateChangedListener {

		void onDateChanged();
	}

	public DatePickerDialog() {
		// Empty constructor required for dialog fragment.
	}

	/**
	 * @param callBack
	 *            How the parent is notified that the date is set.
	 * @param year
	 *            The initial year of the dialog.
	 * @param monthOfYear
	 *            The initial month of the dialog.
	 * @param dayOfMonth
	 *            The initial day of the dialog.
	 */
	public static DatePickerDialog newInstance(OnDateSetListener callBack, int year, int monthOfYear, int dayOfMonth) {
		DatePickerDialog ret = new DatePickerDialog();
		ret.initialize(callBack, year, monthOfYear, dayOfMonth);
		return ret;
	}

	public void initialize(OnDateSetListener callBack, int year, int monthOfYear, int dayOfMonth) {
		mCallBack = callBack;
		mCalendar.set(Calendar.YEAR, year);
		mCalendar.set(Calendar.MONTH, monthOfYear);
		mCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
		mThemeDark = false;
		mAccentColor = -1;
		mVibrate = true;
		mDismissOnPause = false;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		final Activity activity = getActivity();
		activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		if (savedInstanceState != null) {
			mCalendar.set(Calendar.YEAR, savedInstanceState.getInt(KEY_SELECTED_YEAR));
			mCalendar.set(Calendar.MONTH, savedInstanceState.getInt(KEY_SELECTED_MONTH));
			mCalendar.set(Calendar.DAY_OF_MONTH, savedInstanceState.getInt(KEY_SELECTED_DAY));
		}
	}

	@Override
	public void onSaveInstanceState(@NonNull Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(KEY_SELECTED_YEAR, mCalendar.get(Calendar.YEAR));
		outState.putInt(KEY_SELECTED_MONTH, mCalendar.get(Calendar.MONTH));
		outState.putInt(KEY_SELECTED_DAY, mCalendar.get(Calendar.DAY_OF_MONTH));
		outState.putInt(KEY_WEEK_START, mWeekStart);
		outState.putInt(KEY_YEAR_START, mMinYear);
		outState.putInt(KEY_YEAR_END, mMaxYear);
		outState.putInt(KEY_CURRENT_VIEW, mCurrentView);

		int listPosition = -1;
		// int listPositionEnd = -1;
		if (mCurrentView == MONTH_AND_DAY_VIEW) {
			listPosition = mDayPickerView.getMostVisiblePosition();
		} else if (mCurrentView == YEAR_VIEW) {
			listPosition = mYearPickerView.getFirstVisiblePosition();
			outState.putInt(KEY_LIST_POSITION_OFFSET, mYearPickerView.getFirstPositionOffset());
		}
		outState.putInt(KEY_LIST_POSITION, listPosition);
		outState.putSerializable(KEY_MIN_DATE, mMinDate);
		outState.putSerializable(KEY_MAX_DATE, mMaxDate);
		outState.putSerializable(KEY_HIGHLIGHTED_DAYS, highlightedDays);
		outState.putSerializable(KEY_SELECTABLE_DAYS, selectableDays);
		outState.putBoolean(KEY_THEME_DARK, mThemeDark);
		outState.putInt(KEY_ACCENT, mAccentColor);
		outState.putBoolean(KEY_VIBRATE, mVibrate);
		outState.putBoolean(KEY_DISMISS, mDismissOnPause);
	}

	@SuppressWarnings("deprecation")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		Log.d(TAG, "onCreateView: ");
		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

		View view = inflater.inflate(R.layout.range_date_picker_dialog, null);

		final Activity activity = getActivity();

		balnceSheet_DateText = (TextView) view.findViewById(R.id.fragment_Calendar_bsDate);

		/*if (EShaktiApplication.isAuditFragment()) {

			balnceSheet_DateText.setText(GetSpanText.getSpanString(getActivity(),
					String.valueOf(SelectedGroupsTask.sBalanceSheetDate_Response) + "  "
							+ RegionalConversion.getRegionalConversion(AppStrings.afterDate)));
		} else {
			balnceSheet_DateText.setText(GetSpanText.getSpanString(getActivity(),
					String.valueOf(SelectedGroupsTask.sLastTransactionDate_Response) + "  "
							+ RegionalConversion.getRegionalConversion(AppStrings.afterDate)));
		}*/
		
		if (EShaktiApplication.getCalendarDialog_MinDate() != null) {
			balnceSheet_DateText.setText(
					GetSpanText.getSpanString(getActivity(), String.valueOf(EShaktiApplication.getCalendarDialog_MinDate())
							+ "  " + RegionalConversion.getRegionalConversion(AppStrings.afterDate)));
		} else {
			balnceSheet_DateText.setVisibility(View.GONE);
		}



		balnceSheet_DateText.setTypeface(LoginActivity.sTypeface);
		balnceSheet_DateText.setTextColor(R.color.black);

		selectedDateText = (TextView) view.findViewById(R.id.fragment_UserselectedDate);
		selectedDateText
				.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.selectedDate)) + " : ");
		selectedDateText.setTypeface(LoginActivity.sTypeface);

		mDayOfWeekView = (TextView) view.findViewById(R.id.date_picker_header);
		mMonthAndDayView = (LinearLayout) view.findViewById(R.id.date_picker_month_and_day);
		mMonthAndDayView.setOnClickListener(this);

		mSelectedMonthTextView = (TextView) view.findViewById(R.id.date_picker_month);

		mSelectedDayTextView = (TextView) view.findViewById(R.id.date_picker_day);

		mYearView = (TextView) view.findViewById(R.id.date_picker_year);
		mYearView.setOnClickListener(this);

		int listPosition = -1;
		int listPositionOffset = 0;
		int listPositionEnd = -1;
		// int listPositionOffsetEnd = 0;
		int currentView = MONTH_AND_DAY_VIEW;
		int currentViewEnd = MONTH_AND_DAY_VIEW;
		if (savedInstanceState != null) {
			mWeekStart = savedInstanceState.getInt(KEY_WEEK_START);
			mMinYear = savedInstanceState.getInt(KEY_YEAR_START);
			mMaxYear = savedInstanceState.getInt(KEY_YEAR_END);
			currentView = savedInstanceState.getInt(KEY_CURRENT_VIEW);
			listPosition = savedInstanceState.getInt(KEY_LIST_POSITION);
			listPositionOffset = savedInstanceState.getInt(KEY_LIST_POSITION_OFFSET);
			mMinDate = (Calendar) savedInstanceState.getSerializable(KEY_MIN_DATE);
			mMaxDate = (Calendar) savedInstanceState.getSerializable(KEY_MAX_DATE);
			highlightedDays = (Calendar[]) savedInstanceState.getSerializable(KEY_HIGHLIGHTED_DAYS);
			selectableDays = (Calendar[]) savedInstanceState.getSerializable(KEY_SELECTABLE_DAYS);
			mThemeDark = savedInstanceState.getBoolean(KEY_THEME_DARK);
			mAccentColor = savedInstanceState.getInt(KEY_ACCENT);
			mVibrate = savedInstanceState.getBoolean(KEY_VIBRATE);
			mDismissOnPause = savedInstanceState.getBoolean(KEY_DISMISS);
		}

		okButton = (TextView) view.findViewById(R.id.ok);
		okButton.setText(RegionalConversion.getRegionalConversion(AppStrings.dialogOk));
		okButton.setTypeface(LoginActivity.sTypeface, Typeface.BOLD);

		TextView cancelButton = (TextView) view.findViewById(R.id.cancel);
		cancelButton.setText(RegionalConversion.getRegionalConversion(AppStrings.dialogNo));
		cancelButton.setTypeface(LoginActivity.sTypeface);

		mDayPickerView = new SimpleDayPickerView(activity, this);
		mYearPickerView = new YearPickerView(activity, this);

		Resources res = getResources();
		mDayPickerDescription = res.getString(R.string.mdtp_day_picker_description);
		mSelectDay = res.getString(R.string.mdtp_select_day);
		mYearPickerDescription = res.getString(R.string.mdtp_year_picker_description);
		mSelectYear = res.getString(R.string.mdtp_select_year);

		int bgColorResource = mThemeDark ? R.color.mdtp_date_picker_view_animator_dark_theme
				: R.color.mdtp_date_picker_view_animator;
		view.setBackgroundColor(activity.getResources().getColor(bgColorResource));

		mAnimator = (AccessibleDateAnimator) view.findViewById(R.id.animator);

		mAnimator.addView(mDayPickerView);
		mAnimator.addView(mYearPickerView);

		//

		if (EShaktiApplication.getNextMonthLastDate() != null) {
			if (!Transaction_InternalBank_MFI_LoanFragment.bankLoan_check.equals("1")
					&& !Transaction_InternalBank_MFI_LoanFragment.bankLoan_check.equals("2")
					&& !Meeting_AuditingFragment.audit_check.equals("1")
					&& !Meeting_AuditingFragment.audit_check.equals("2")
					&& !Meeting_AuditingFragment.audit_check.equals("3")
					&& !EditOpenBalanceDialogActivity.mCheck_EditOpening_BalncesheetChange.equals("1")
					&& !Transaction_Loan_disburse_LoanAccFragment.disbursementDate_check.equals("1")
					&& !Transaction_LoanDisbursementFromRepaidFragment.disbursement_Date_check.equals("1")) {
				String lastTrDateArr[] = EShaktiApplication.getNextMonthLastDate().split("-");
				mCalendar.set(Integer.parseInt(lastTrDateArr[2]), Integer.parseInt(lastTrDateArr[1]) - 1,
						Integer.parseInt(lastTrDateArr[0]));
			} else if (Transaction_InternalBank_MFI_LoanFragment.bankLoan_check.equals("1")
					|| Transaction_InternalBank_MFI_LoanFragment.bankLoan_check.equals("2")
					|| Meeting_AuditingFragment.audit_check.equals("1")
					|| Transaction_Loan_disburse_LoanAccFragment.disbursementDate_check.equals("1")
					|| Transaction_LoanDisbursementFromRepaidFragment.disbursement_Date_check.equals("1")) {
				String lastTrDateArr[] = DatePickerDialog.sDashboardDate.split("-");
				mCalendar.set(Integer.parseInt(lastTrDateArr[2]), Integer.parseInt(lastTrDateArr[1]) - 1,
						Integer.parseInt(lastTrDateArr[0]));
			}
		}
		//
		mAnimator.setDateMillis(mCalendar.getTimeInMillis());
		// TODO: Replace with animation decided upon by the design team.
		Animation animation = new AlphaAnimation(0.0f, 1.0f);
		animation.setDuration(ANIMATION_DURATION);
		mAnimator.setInAnimation(animation);
		// TODO: Replace with animation decided upon by the design team.
		Animation animation2 = new AlphaAnimation(1.0f, 0.0f);
		animation2.setDuration(ANIMATION_DURATION);
		mAnimator.setOutAnimation(animation2);

		// TODO: Replace with animation decided upon by the design team.
		Animation animationEnd = new AlphaAnimation(0.0f, 1.0f);
		animationEnd.setDuration(ANIMATION_DURATION);
		// TODO: Replace with animation decided upon by the design team.
		Animation animation2End = new AlphaAnimation(1.0f, 0.0f);
		animation2End.setDuration(ANIMATION_DURATION);

		getDialog().setCanceledOnTouchOutside(false);
		if (EShaktiApplication.isDefault()) {
			MainActivity.mDateView.setClickable(false);
		} else {
			if (!EShaktiApplication.isEditOpeningScreen()) {

				MainActivity.mDateView.setClickable(true);
			}
		}
		okButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				tryVibrate();
				if (mCallBack != null) {

					mCallBack.onDateSet(DatePickerDialog.this, mCalendar.get(Calendar.YEAR),
							mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH));
					CalendarDay day = getSelectedDay();
					if (!EShaktiApplication.isAudit_TrainingFragment()) {

					}

					if (!EShaktiApplication.isCalendarDateVisibleFlag()) {

						if (EShaktiApplication.isEditOBTransDate()) {

							setDashboardDate(day);

						} else {

							if (Audit_Date_selectionActivity.audit_trial_balance_check.equals("1")
									|| Audit_Date_selectionActivity.audit_trial_balance_check.equals("2")) {
								if (Audit_Date_selectionActivity.audit_trial_balance_check.equals("1")) {
									String dateArr[] = MonthAdapter.sSelectedDate.split("-");
									sSelectedFromDate = dateArr[1] + "/" + dateArr[0] + "/" + dateArr[2];
									Audit_Date_selectionActivity.fromDate = sSelectedFromDate;
									Audit_Date_selectionActivity.mAuditFromDate_Value_texview
											.setText(dateArr[0] + "/" + dateArr[1] + "/" + dateArr[2]);
									System.out.println("---------- FROM DATE------------- :"
											+ Audit_Date_selectionActivity.fromDate);

									Audit_Date_selectionActivity.mAuditTodate_Value_Textview.setText("");

								} else if (Audit_Date_selectionActivity.audit_trial_balance_check.equals("2")) {
									String dateArr[] = MonthAdapter.sSelectedDate.split("-");
									sSelectedToDate = dateArr[1] + "/" + dateArr[0] + "/" + dateArr[2];
									Audit_Date_selectionActivity.toDate = sSelectedToDate;
									Audit_Date_selectionActivity.mAuditTodate_Value_Textview
											.setText(dateArr[0] + "/" + dateArr[1] + "/" + dateArr[2]);
									System.out.println(
											"----------TO DATE------------- :" + Audit_Date_selectionActivity.toDate);
								}
							} else if (MainActivity.calNavItem.equals("1")
									&& Reports_Trial_BalanceSheetFragment.trial_balance_check.equals("")
									&& Meeting_AuditingFragment.audit_check.equals("")
									&& Meeting_TrainingFragment.training_check.equals("")
									&& Audit_Date_selectionActivity.audit_trial_balance_check.equals("")) {

								MainActivity.calNavItem = Reset.reset(MainActivity.calNavItem);
								EShaktiApplication.setIsNewLoanDisburseDate(false);
								setDashboardDate(day);

							} else if ((MainFragment_Dashboard.sSubMenu_Item.equals("1"))
									|| (FragmentDrawer.sSubMenu_Item.equals("1"))) {

								setDashboardDate(day);

								fragment = new Transaction_SavingsFragment();
								setFragment(fragment);

							} else if ((MainFragment_Dashboard.sSubMenu_Item.equals("2"))
									|| (FragmentDrawer.sSubMenu_Item.equals("2"))) {

								setDashboardDate(day);

								fragment = new Transaction_MemLoanRepaid_MenuFragment();
								setFragment(fragment);

							} else if ((MainFragment_Dashboard.sSubMenu_Item.equals("3"))
									|| (FragmentDrawer.sSubMenu_Item.equals("3"))) {

								setDashboardDate(day);

								fragment = new Transaction_ExpensesFragment();
								setFragment(fragment);

							} else if ((MainFragment_Dashboard.sSubMenu_Item.equals("4"))
									|| (FragmentDrawer.sSubMenu_Item.equals("4"))) {

								setDashboardDate(day);

								fragment = new Transaction_IncomeMenuFragment();
								setFragment(fragment);

							} else if ((MainFragment_Dashboard.sSubMenu_Item.equals("5"))
									|| (FragmentDrawer.sSubMenu_Item.equals("5"))) {

								setDashboardDate(day);

								fragment = new Transaction_GroupLoanRepaidMenuFragment();
								setFragment(fragment);

							} else if ((MainFragment_Dashboard.sSubMenu_Item.equals("6"))
									|| (FragmentDrawer.sSubMenu_Item.equals("6"))) {

								setDashboardDate(day);

								fragment = new Transaction_BankDepositFragment();
								setFragment(fragment);

							} else if ((MainFragment_Dashboard.sSubMenu_Item.equals("7"))
									|| (FragmentDrawer.sSubMenu_Item.equals("7"))) {

								setDashboardDate(day);

								if (ConnectionUtils.isNetworkAvailable(getActivity())) {
									fragment = new Transaction_Loandisbursement_MentFragment();
								} else {
									fragment = new Transaction_InternalloanMenuFragment();
								}

								setFragment(fragment);

							} else if ((MainFragment_Dashboard.sSubMenu_Item.equals("8"))
									|| (FragmentDrawer.sSubMenu_Item.equals("8"))) {

								setDashboardDate(day);

								fragment = new Meeting_AttendanceFragment();
								setFragment(fragment);

							} else if ((MainFragment_Dashboard.sSubMenu_Item.equals("9"))
									|| (FragmentDrawer.sSubMenu_Item.equals("9"))) {

								setDashboardDate(day);

								fragment = new Meeting_MinutesOfMeetingFragment();
								setFragment(fragment);

							} else if ((MainFragment_Dashboard.sSubMenu_Item.equals("10"))
									|| (FragmentDrawer.sSubMenu_Item.equals("10"))) {

								setDashboardDate(day);

								fragment = new Meeting_AuditingFragment();
								setFragment(fragment);

							} else if ((MainFragment_Dashboard.sSubMenu_Item.equals("11"))
									|| (FragmentDrawer.sSubMenu_Item.equals("11"))) {

								setDashboardDate(day);

								fragment = new Meeting_TrainingFragment();
								setFragment(fragment);

							} else if ((MainFragment_Dashboard.sSubMenu_Item.equals("12"))
									|| (FragmentDrawer.sSubMenu_Item.equals("12"))) {

								setDashboardDate(day);

								EShaktiApplication.setDefault(true);
								EShaktiApplication.setStepwiseSavings(0);
								EShaktiApplication.setStepwiseInternalloan(0);
								EShaktiApplication.setStepWiseFragment(true);

								EShaktiApplication.setGroupLoanTotalInterest(0);
								EShaktiApplication.setGroupLoanTotalCharges(0);
								EShaktiApplication.setGroupLoanTotalRepayment(0);
								EShaktiApplication.setGroupLoanTotalInterestSubventionRecevied(0);
								EShaktiApplication.setGroupLoanTotalBankcharges(0);

								if (SelectedGroupsTask.loanAcc_loanDisbursementDate.size() != 0) {

									SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
									Log.e("First Loan Disbursement Date",
											SelectedGroupsTask.loanAcc_loanDisbursementDate.elementAt(0).toString()
													+ "");
									String[] loanDisbDateArr = SelectedGroupsTask.loanAcc_loanDisbursementDate
											.elementAt(0).toString().split("/");
									String temp_maxDate = loanDisbDateArr[1] + "-" + loanDisbDateArr[0] + "-"
											+ loanDisbDateArr[2];
									Date maxDate = null, nextDate = null;
									String maxDate_str = null;

									try {
										maxDate = sdf.parse(temp_maxDate);
									} catch (ParseException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}

									if (SelectedGroupsTask.loanAcc_loanDisbursementDate.size() > 1) {
										for (int i = 1; i < SelectedGroupsTask.loanAcc_loanDisbursementDate
												.size(); i++) {
											String[] temp_date = SelectedGroupsTask.loanAcc_loanDisbursementDate
													.elementAt(i).toString().split("/");
											String date = temp_date[1] + "-" + temp_date[0] + "-" + temp_date[2];
											try {
												maxDate = sdf.parse(temp_maxDate);
												nextDate = sdf.parse(date);
											} catch (ParseException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}

											if (maxDate.compareTo(nextDate) >= 0) {
												maxDate_str = temp_maxDate;
											} else {
												maxDate_str = date;
											}
										}
									} else if (SelectedGroupsTask.loanAcc_loanDisbursementDate.size() == 1) {
										maxDate_str = temp_maxDate;
									}

									Log.e("Max Loan Disbursement Date !!!!!!", maxDate_str + "");

									// String lastTrDate_str = DatePickerDialog.sDashboardDate;

									String lastTrDate_str = null;
									if (DatePickerDialog.sDashboardDate.contains("-")) {
										lastTrDate_str = DatePickerDialog.sDashboardDate;
									} else if (DatePickerDialog.sDashboardDate.contains("/")) {
										String[] dateArr = DatePickerDialog.sDashboardDate.split("/");
										lastTrDate_str = dateArr[0] + "-" + dateArr[1] + "-" + dateArr[2];
										;
									}

									Date lastTrDate = null, maxLoanDisbDate = null;
									String minDate_str = null;

									try {
										lastTrDate = sdf.parse(lastTrDate_str);
										maxLoanDisbDate = sdf.parse(maxDate_str);
									} catch (ParseException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}

									if (maxLoanDisbDate.compareTo(lastTrDate) >= 0) {
										minDate_str = maxDate_str;
										TastyToast.makeText(getActivity(), AppStrings.mCheckDisbursementDate,
												TastyToast.LENGTH_SHORT, TastyToast.WARNING);
									} else {
										minDate_str = lastTrDate_str;
										fragment = new Meeting_AttendanceFragment();
										setFragment(fragment);
									}

									Log.e("STEPWISE Final Min Date    ---------", minDate_str.toString() + "");

								} else if (SelectedGroupsTask.loanAcc_loanDisbursementDate.size() == 0) {
									fragment = new Meeting_AttendanceFragment();
									setFragment(fragment);
								}

								MainActivity.mStepwiseBackKey = true;

							} else if ((Transaction_InternalBank_MFI_LoanFragment.bankLoan_check.equals("1")
									|| Transaction_InternalBank_MFI_LoanFragment.bankLoan_check.equals("2"))
									&& (Reports_Trial_BalanceSheetFragment.trial_balance_check.equals(""))
									&& (Meeting_AuditingFragment.audit_check.equals(""))
									&& (Meeting_TrainingFragment.training_check.equals(""))) {

								if (Transaction_InternalBank_MFI_LoanFragment.bankLoan_check.equals("1")) {

									String dateArr[] = MonthAdapter.sSelectedDate.split("-");
									sLoan_Sanc_Date = dateArr[1] + "/" + dateArr[0] + "/" + dateArr[2];
									Transaction_InternalBank_MFI_LoanFragment.loanSancDate = sLoan_Sanc_Date;
									Transaction_InternalBank_MFI_LoanFragment.mLoanSancDateTxt
											.setText(dateArr[0] + "/" + dateArr[1] + "/" + dateArr[2]);
									System.out.println("----------Loan Sanction DATE------------- :"
											+ Transaction_InternalBank_MFI_LoanFragment.loanSancDate);

									String loanDisbMinDate = dateArr[0] + "/" + dateArr[1] + "/" + dateArr[2];
									EShaktiApplication.setNewLoanDisbursementMinDate(loanDisbMinDate);
									Transaction_InternalBank_MFI_LoanFragment.loanDistDate = "";
									Transaction_InternalBank_MFI_LoanFragment.mLoanDistDateTxt.setText("");
								} else if (Transaction_InternalBank_MFI_LoanFragment.bankLoan_check.equals("2")) {
									String dateArr[] = MonthAdapter.sSelectedDate.split("-");
									sLoan_Dist_Date = dateArr[1] + "/" + dateArr[0] + "/" + dateArr[2];
									Transaction_InternalBank_MFI_LoanFragment.loanDistDate = sLoan_Dist_Date;
									Transaction_InternalBank_MFI_LoanFragment.mLoanDistDateTxt
											.setText(dateArr[0] + "/" + dateArr[1] + "/" + dateArr[2]);
									System.out.println("----------Loan Disbursement DATE------------- :"
											+ Transaction_InternalBank_MFI_LoanFragment.loanDistDate);

									Log.e("Transaction Date", DatePickerDialog.sSend_To_Server_Date);
								}
							} else if (Transaction_Loan_disburse_LoanAccFragment.disbursementDate_check.equals("1")) {
								String dateArr[] = MonthAdapter.sSelectedDate.split("-");
								sDist_Date = dateArr[1] + "/" + dateArr[0] + "/" + dateArr[2];
								Transaction_Loan_disburse_LoanAccFragment.disbursementDate = sDist_Date;
								Transaction_Loan_disburse_LoanAccFragment.mDis_DateText
										.setText(dateArr[0] + "/" + dateArr[1] + "/" + dateArr[2]);
								System.out.println("----------DISBURSEMENT DATE------------- :"
										+ Transaction_Loan_disburse_LoanAccFragment.disbursementDate);
							} else if (Transaction_LoanDisbursementFromRepaidFragment.disbursement_Date_check
									.equals("1")) {
								String dateArr[] = MonthAdapter.sSelectedDate.split("-");
								sRepaid_DisbDate = dateArr[1] + "/" + dateArr[0] + "/" + dateArr[2];
								Transaction_LoanDisbursementFromRepaidFragment.disbursement_Date = sRepaid_DisbDate;
								Transaction_LoanDisbursementFromRepaidFragment.mDis_DateText
										.setText(dateArr[0] + "/" + dateArr[1] + "/" + dateArr[2]);
								System.out.println("---------- REPAID DISBURSEMENT DATE------------- :"
										+ Transaction_LoanDisbursementFromRepaidFragment.disbursement_Date);
							} else if ((Reports_Trial_BalanceSheetFragment.trial_balance_check.equals("1")
									|| Reports_Trial_BalanceSheetFragment.trial_balance_check.equals("2"))
									&& (Meeting_AuditingFragment.audit_check.equals(""))
									&& (Meeting_TrainingFragment.training_check.equals(""))) {

								if (Reports_Trial_BalanceSheetFragment.trial_balance_check.equals("1")) {
									String dateArr[] = MonthAdapter.sSelectedDate.split("-");
									sSelectedFromDate = dateArr[1] + "/" + dateArr[0] + "/" + dateArr[2];
									Reports_Trial_BalanceSheetFragment.fromDate = sSelectedFromDate;
									Reports_Trial_BalanceSheetFragment.mFromDateEdit_Text
											.setText(dateArr[0] + "/" + dateArr[1] + "/" + dateArr[2]);
									System.out.println("----------BALANCESHEET FROM DATE------------- :"
											+ Reports_Trial_BalanceSheetFragment.fromDate);

									Reports_Trial_BalanceSheetFragment.mToDateEdit_Text.setText("");

								} else if (Reports_Trial_BalanceSheetFragment.trial_balance_check.equals("2")) {
									String dateArr[] = MonthAdapter.sSelectedDate.split("-");
									sSelectedToDate = dateArr[1] + "/" + dateArr[0] + "/" + dateArr[2];
									Reports_Trial_BalanceSheetFragment.toDate = sSelectedToDate;
									Reports_Trial_BalanceSheetFragment.mToDateEdit_Text
											.setText(dateArr[0] + "/" + dateArr[1] + "/" + dateArr[2]);
									System.out.println("----------BALANCESHEET TO DATE------------- :"
											+ Reports_Trial_BalanceSheetFragment.toDate);
								}

							} else if ((Meeting_AuditingFragment.audit_check.equals("1")
									|| Meeting_AuditingFragment.audit_check.equals("2")
									|| Meeting_AuditingFragment.audit_check.equals("3"))
									&& (Reports_Trial_BalanceSheetFragment.trial_balance_check.equals(""))
									&& (Meeting_TrainingFragment.training_check.equals(""))) {

								if (Meeting_AuditingFragment.audit_check.equals("1")) {
									Meeting_AuditingFragment.calFromDate = MonthAdapter.sSelectedDate;
									System.out.println(
											"CAL FROM DATE ************ " + Meeting_AuditingFragment.calFromDate);

									String dateArr[] = MonthAdapter.sSelectedDate.split("-");
									sAuditFromDate = dateArr[1] + "/" + dateArr[0] + "/" + dateArr[2];
									Meeting_AuditingFragment.auditFromDate = sAuditFromDate;
									Meeting_AuditingFragment.mAuditFromDate_editText
											.setText(dateArr[0] + "/" + dateArr[1] + "/" + dateArr[2]);
									System.out.println("----------AUDITING FROM DATE------------- :"
											+ Meeting_AuditingFragment.auditFromDate);

									Meeting_AuditingFragment.mAuditToDate_editText.setText("");
									Meeting_AuditingFragment.mAuditDate_editText.setText("");

									String auditFromDate = dateArr[0] + "/" + dateArr[1] + "/" + dateArr[2];
									EShaktiApplication.setAuditFromDate(auditFromDate);
								} else if (Meeting_AuditingFragment.audit_check.equals("2")) {

									Meeting_AuditingFragment.calToDate = MonthAdapter.sSelectedDate;
									System.out.println("----------AUDITING TO DATE------------- :"
											+ Meeting_AuditingFragment.calToDate);

									String dateArr[] = MonthAdapter.sSelectedDate.split("-");
									sAuditToDate = dateArr[1] + "/" + dateArr[0] + "/" + dateArr[2];
									Meeting_AuditingFragment.auditToDate = sAuditToDate;
									Meeting_AuditingFragment.mAuditToDate_editText
											.setText(dateArr[0] + "/" + dateArr[1] + "/" + dateArr[2]);
									System.out.println("----------AUDITING TO DATE------------- :"
											+ Meeting_AuditingFragment.auditToDate);

									String auditToDate = dateArr[0] + "/" + dateArr[1] + "/" + dateArr[2];
									EShaktiApplication.setAuditToDate(auditToDate);

								} else if (Meeting_AuditingFragment.audit_check.equals("3")) {
									String dateArr[] = MonthAdapter.sSelectedDate.split("-");
									sAuditDate = dateArr[1] + "/" + dateArr[0] + "/" + dateArr[2];

									Meeting_AuditingFragment.auditingDate = sAuditDate;
									Meeting_AuditingFragment.mAuditDate_editText
											.setText(dateArr[0] + "/" + dateArr[1] + "/" + dateArr[2]);
									System.out.println("----------AUDITING DATE------------- :"
											+ Meeting_AuditingFragment.auditingDate);
								}

							} else if ((Meeting_TrainingFragment.training_check.equals("1"))
									&& (Meeting_AuditingFragment.audit_check.equals(""))
									&& (Reports_Trial_BalanceSheetFragment.trial_balance_check.equals(""))) {

								String dateArr[] = MonthAdapter.sSelectedDate.split("-");
								sTrainingDate = dateArr[1] + "/" + dateArr[0] + "/" + dateArr[2];
								Meeting_TrainingFragment.trainingDate = sTrainingDate;
								Meeting_TrainingFragment.mTrainingDate_editText
										.setText(dateArr[0] + "/" + dateArr[1] + "/" + dateArr[2]);
								System.out.println("----------TRAINING DATE------------- :"
										+ Meeting_TrainingFragment.trainingDate);

							} else if (MainFragment_Dashboard.sSubMenu_Item.equals("12")
									|| (FragmentDrawer.sSubMenu_Item.equals("12"))) {
								setDashboardDate(day);
								EShaktiApplication.setDefault(true);
								fragment = new Meeting_AttendanceFragment();
								setFragment(fragment);
								Log.e("---------------------->>>", "00-0-00000000000000-0-0-0");
							}

							if ((MainActivity.calNavItem.equals("")) && ((!MainFragment_Dashboard.sSubMenu_Item
									.equals("")) || (!FragmentDrawer.sSubMenu_Item.equals(""))
									|| (!Reports_Trial_BalanceSheetFragment.trial_balance_check.equals(""))
									|| (!Meeting_AuditingFragment.audit_check.equals(""))
									|| (!Meeting_TrainingFragment.training_check.equals(""))
									|| (!Transaction_Loan_disburse_LoanAccFragment.disbursementDate_check.equals(""))
									|| (!Transaction_LoanDisbursementFromRepaidFragment.disbursement_Date_check
											.equals(""))
									|| (!Transaction_InternalBank_MFI_LoanFragment.bankLoan_check.equals(""))
									|| (!Audit_Date_selectionActivity.audit_trial_balance_check.equals("")))) {

								EditOpenBalanceDialogActivity.mCheck_EditOpening_BalncesheetChange = "";
								Reports_Trial_BalanceSheetFragment.trial_balance_check = "";
								Meeting_AuditingFragment.audit_check = "";
								Meeting_TrainingFragment.training_check = "";
								MainFragment_Dashboard.sSubMenu_Item = "";
								FragmentDrawer.sSubMenu_Item = "";
								Transaction_Loan_disburse_LoanAccFragment.disbursementDate_check = "";
								Transaction_LoanDisbursementFromRepaidFragment.disbursement_Date_check = "";
								Transaction_InternalBank_MFI_LoanFragment.bankLoan_check = "";
								Audit_Date_selectionActivity.audit_trial_balance_check = "";

								Log.e("CALENDAR FRAGMENT ?????????????? ", MainFragment_Dashboard.sSubMenu_Item);

								System.out.println(
										"BALANCE SHEET ////////// " + MainFragment_Dashboard.isBalanceSheetReport);

								System.out.println(
										"TRIAL BALANCE ////////// " + MainFragment_Dashboard.isTrialBalanceReport);

							}

							// }
							// }
						}
					} else {
						TastyToast.makeText(getActivity(), "PLEASE SET YOUR DEVICE DATE CORRECTLY",
								TastyToast.LENGTH_SHORT, TastyToast.ERROR);
						EShaktiApplication.setNextMonthLastDate(null);
					}
				}
				dismiss();

				if (EShaktiApplication.isEditOBTransDate()) {

					publicValues.mEdit_OB_Sendtoserver_Savings = null;
					publicValues.mEdit_OB_Sendtoserver_InternalLoan = null;
					publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan = null;
					publicValues.mEdit_OB_Sendtoserver_Bank_Group_Loan = null;
					publicValues.mEdit_OB_Sendtiserver_Bank_CashinHandDetails = null;

					Intent intent = new Intent(getActivity(), EditOpeningBalanceSavingsActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
					getActivity().overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

					getActivity().finish();

					EShaktiApplication.setEditOBTransDate(false);
				}
			}

			private void setDashboardDate(CalendarDay day) {
				// TODO Auto-generated method stub
				sDashboardDate = (day.getDay() < 10 ? ("0" + day.getDay()) : (day.getDay())) + "-"
						+ (day.getMonth() < 9 ? ("0" + (day.getMonth() + 1)) : (day.getMonth() + 1)) + "-"
						+ day.getYear();
				Log.e("DASHBOARD DATE ", sDashboardDate);

				sSend_To_Server_Date = (day.getMonth() < 9 ? ("0" + (day.getMonth() + 1)) : (day.getMonth() + 1)) + "/"
						+ (day.getDay() < 10 ? ("0" + day.getDay()) : (day.getDay())) + "/" + day.getYear();
				Log.e("SEND_TO_SERVER_DATE", sSend_To_Server_Date);
				if (!EShaktiApplication.IsNewLoanDisburseDate()) {
					EShaktiApplication.setTransactionDate(sSend_To_Server_Date);
				}
				EShaktiApplication.setEditOBTransactionDate(sSend_To_Server_Date);

			}
		});
		// okButton.setTypeface(TypefaceHelper.get(activity, "Roboto-Medium"));

		cancelButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				tryVibrate();
				if (getDialog() != null)
					getDialog().cancel();
				EditOpenBalanceDialogActivity.mCheck_EditOpening_BalncesheetChange = "";
				MainActivity.calNavItem = "";
				MainFragment_Dashboard.sSubMenu_Item = "";
				FragmentDrawer.sSubMenu_Item = "";
				Reports_Trial_BalanceSheetFragment.trial_balance_check = "";
				Meeting_AuditingFragment.audit_check = "";
				Meeting_TrainingFragment.training_check = "";
				Transaction_Loan_disburse_LoanAccFragment.disbursementDate_check = "";
				Transaction_LoanDisbursementFromRepaidFragment.disbursement_Date_check = "";
				Transaction_InternalBank_MFI_LoanFragment.bankLoan_check = "";
				Audit_Date_selectionActivity.audit_trial_balance_check = "";
				if (EShaktiApplication.isCalendarDateVisibleFlag()) {
					EShaktiApplication.setCalendarDateVisibleFlag(false);
				}

			}
		});
		// cancelButton.setTypeface(TypefaceHelper.get(activity,"Roboto-Medium"));
		cancelButton.setVisibility(isCancelable() ? View.VISIBLE : View.GONE);

		// If an accent color has not been set manually, try and get it from the
		// context
		if (mAccentColor == -1) {
			int accentColor = Utils.getAccentColorFromThemeIfAvailable(getActivity());
			if (accentColor != -1) {
				mAccentColor = accentColor;
			}
		}
		if (mAccentColor != -1) {
			if (mDayOfWeekView != null)
				mDayOfWeekView.setBackgroundColor(Utils.darkenColor(mAccentColor));
			view.findViewById(R.id.day_picker_selected_date_layout).setBackgroundColor(mAccentColor);
			okButton.setTextColor(mAccentColor);
			cancelButton.setTextColor(mAccentColor);
			mYearPickerView.setAccentColor(mAccentColor);
			mDayPickerView.setAccentColor(mAccentColor);
		}

		updateDisplay(false);
		setCurrentView(currentView);

		if (listPosition != -1) {
			if (currentView == MONTH_AND_DAY_VIEW) {
				mDayPickerView.postSetSelection(listPosition);
			} else if (currentView == YEAR_VIEW) {
				mYearPickerView.postSetSelectionFromTop(listPosition, listPositionOffset);
			}
		}

		if (listPositionEnd != -1) {
			if (currentViewEnd == MONTH_AND_DAY_VIEW) {
			} else if (currentViewEnd == YEAR_VIEW) {
			}
		}

		mHapticFeedbackController = new HapticFeedbackController(activity);

		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
		mHapticFeedbackController.start();
	}

	@Override
	public void onPause() {
		super.onPause();
		mHapticFeedbackController.stop();
		if (mDismissOnPause)
			dismiss();
	}

	@Override
	public void onCancel(DialogInterface dialog) {
		super.onCancel(dialog);
		if (mOnCancelListener != null)
			mOnCancelListener.onCancel(dialog);
	}

	@Override
	public void onDismiss(DialogInterface dialog) {
		super.onDismiss(dialog);
		if (mOnDismissListener != null)
			mOnDismissListener.onDismiss(dialog);
	}

	private void setCurrentView(final int viewIndex) {
		long millis = mCalendar.getTimeInMillis();

		switch (viewIndex) {
		case MONTH_AND_DAY_VIEW:
			ObjectAnimator pulseAnimator = Utils.getPulseAnimator(mMonthAndDayView, 0.9f, 1.05f);
			if (mDelayAnimation) {
				pulseAnimator.setStartDelay(ANIMATION_DELAY);
				mDelayAnimation = false;
			}
			mDayPickerView.onDateChanged();
			if (mCurrentView != viewIndex) {
				mMonthAndDayView.setSelected(true);
				mYearView.setSelected(false);
				mAnimator.setDisplayedChild(MONTH_AND_DAY_VIEW);
				mCurrentView = viewIndex;
			}
			pulseAnimator.start();
			int flags = DateUtils.FORMAT_SHOW_DATE;
			String dayString = DateUtils.formatDateTime(getActivity(), millis, flags);
			mAnimator.setContentDescription(mDayPickerDescription + ": " + dayString);
			Utils.tryAccessibilityAnnounce(mAnimator, mSelectDay);
			break;
		case YEAR_VIEW:
			pulseAnimator = Utils.getPulseAnimator(mYearView, 0.85f, 1.1f);
			if (mDelayAnimation) {
				pulseAnimator.setStartDelay(ANIMATION_DELAY);
				mDelayAnimation = false;
			}
			mYearPickerView.onDateChanged();
			if (mCurrentView != viewIndex) {
				mMonthAndDayView.setSelected(false);
				mYearView.setSelected(true);
				mAnimator.setDisplayedChild(YEAR_VIEW);
				mCurrentView = viewIndex;

			}
			pulseAnimator.start();

			CharSequence yearString = YEAR_FORMAT.format(millis);
			mAnimator.setContentDescription(mYearPickerDescription + ": " + yearString);
			Utils.tryAccessibilityAnnounce(mAnimator, mSelectYear);
			break;
		}
	}

	private void updateDisplay(boolean announce) {
		if (mDayOfWeekView != null) {
			mDayOfWeekView.setText(mCalendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.ENGLISH)
					.toUpperCase(Locale.ENGLISH));
		}

		mSelectedMonthTextView.setText(
				mCalendar.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.ENGLISH).toUpperCase(Locale.ENGLISH));
		mSelectedDayTextView.setText(DAY_FORMAT.format(mCalendar.getTime()));
		mYearView.setText(YEAR_FORMAT.format(mCalendar.getTime()));

		// Accessibility.
		long millis = mCalendar.getTimeInMillis();
		mAnimator.setDateMillis(millis);
		int flags = DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_NO_YEAR;
		String monthAndDayText = DateUtils.formatDateTime(getActivity(), millis, flags);
		mMonthAndDayView.setContentDescription(monthAndDayText);

		if (announce) {
			flags = DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_YEAR;
			String fullDateText = DateUtils.formatDateTime(getActivity(), millis, flags);
			Utils.tryAccessibilityAnnounce(mAnimator, fullDateText);
		}
	}

	/**
	 * Set whether the device should vibrate when touching fields
	 * 
	 * @param vibrate
	 *            true if the device should vibrate when touching a field
	 */
	public void vibrate(boolean vibrate) {
		mVibrate = vibrate;
	}

	/**
	 * Set whether the picker should dismiss itself when being paused or whether it
	 * should try to survive an orientation change
	 * 
	 * @param dismissOnPause
	 *            true if the dialog should dismiss itself when it's pausing
	 */
	public void dismissOnPause(boolean dismissOnPause) {
		mDismissOnPause = dismissOnPause;
	}

	/**
	 * Set whether the dark theme should be used
	 * 
	 * @param themeDark
	 *            true if the dark theme should be used, false if the default theme
	 *            should be used
	 */
	public void setThemeDark(boolean themeDark) {
		mThemeDark = themeDark;
	}

	/**
	 * Returns true when the dark theme should be used
	 * 
	 * @return true if the dark theme should be used, false if the default theme
	 *         should be used
	 */
	@Override
	public boolean isThemeDark() {
		return mThemeDark;
	}

	/**
	 * Set the accent color of this dialog
	 * 
	 * @param accentColor
	 *            the accent color you want
	 */
	public void setAccentColor(int accentColor) {
		mAccentColor = accentColor;
	}

	/**
	 * Get the accent color of this dialog
	 * 
	 * @return accent color
	 */
	public int getAccentColor() {
		return mAccentColor;
	}

	public void setFirstDayOfWeek(int startOfWeek, int startWeekEnd) {
		if (startOfWeek < Calendar.SUNDAY || startOfWeek > Calendar.SATURDAY) {
			throw new IllegalArgumentException("Value must be between Calendar.SUNDAY and " + "Calendar.SATURDAY");
		}
		mWeekStart = startOfWeek;

		if (mDayPickerView != null) {
			mDayPickerView.onChange();
		}
	}

	public void setYearRange(int startYear, int endYear) {
		if (endYear < startYear) {
			throw new IllegalArgumentException("Year end must be larger than or equal to year start");
		}

		mMinYear = startYear;
		mMaxYear = endYear;
		if (mDayPickerView != null) {
			mDayPickerView.onChange();
		}
	}

	/**
	 * Sets the minimal date supported by this DatePicker. Dates before (but not
	 * including) the specified date will be disallowed from being selected.
	 * 
	 * @param calendar
	 *            a Calendar object set to the year, month, day desired as the
	 *            mindate.
	 */

	public void setMinDate(Calendar calendar) {
		mMinDate = calendar;

		if (mDayPickerView != null) {
			mDayPickerView.onChange();
		}
	}

	/**
	 * @return The minimal date supported by this DatePicker. Null if it has not
	 *         been set.
	 */
	@Override
	public Calendar getMinDate() {
		return mMinDate;
	}

	/**
	 * Sets the minimal date supported by this DatePicker. Dates after (but not
	 * including) the specified date will be disallowed from being selected.
	 * 
	 * @param calendar
	 *            a Calendar object set to the year, month, day desired as the
	 *            maxdate.
	 */

	public void setMaxDate(Calendar calendar) {
		mMaxDate = calendar;

		if (mDayPickerView != null) {
			mDayPickerView.onChange();
		}
	}

	/**
	 * @return The maximal date supported by this DatePicker. Null if it has not
	 *         been set.
	 */
	@Override
	public Calendar getMaxDate() {
		return mMaxDate;
	}

	/**
	 * Sets an array of dates which should be highlighted when the picker is drawn
	 * 
	 * @param highlightedDays
	 *            an Array of Calendar objects containing the dates to be
	 *            highlighted
	 */

	public void setHighlightedDays(Calendar[] highlightedDays, Calendar[] highlightedDaysEnd) {
		// Sort the array to optimize searching over it later on
		Arrays.sort(highlightedDays);
		Arrays.sort(highlightedDaysEnd);
		this.highlightedDays = highlightedDays;
	}

	/**
	 * @return The list of dates, as Calendar Objects, which should be highlighted.
	 *         null is no dates should be highlighted
	 */
	@Override
	public Calendar[] getHighlightedDays() {
		return highlightedDays;
	}

	/**
	 * Set's a list of days which are the only valid selections. Setting this value
	 * will take precedence over using setMinDate() and setMaxDate()
	 * 
	 * @param selectableDays
	 *            an Array of Calendar Objects containing the selectable dates
	 */

	public void setSelectableDays(Calendar[] selectableDays) {
		// Sort the array to optimize searching over it later on
		Arrays.sort(selectableDays);
		this.selectableDays = selectableDays;
	}

	public void setSelectableDaysEnd(Calendar[] selectableDaysEnd) {
		// Sort the array to optimize searching over it later on
		Arrays.sort(selectableDaysEnd);
		// this.selectableDaysEnd = selectableDaysEnd;
	}

	/**
	 * @return an Array of Calendar objects containing the list with selectable
	 *         items. null if no restriction is set
	 */
	@Override
	public Calendar[] getSelectableDays() {
		return selectableDays;
	}

	public void setOnDateSetListener(OnDateSetListener listener) {
		mCallBack = listener;
	}

	public void setOnCancelListener(DialogInterface.OnCancelListener onCancelListener) {
		mOnCancelListener = onCancelListener;
	}

	public void setOnDismissListener(DialogInterface.OnDismissListener onDismissListener) {
		mOnDismissListener = onDismissListener;
	}

	// If the newly selected month / year does not contain the currently
	// selected day number,
	// change the selected day number to the last day of the selected month or
	// year.
	// e.g. Switching from Mar to Apr when Mar 31 is selected -> Apr 30
	// e.g. Switching from 2012 to 2013 when Feb 29, 2012 is selected -> Feb 28,
	// 2013
	private void adjustDayInMonthIfNeeded(Calendar calendar) {
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		int daysInMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		if (day > daysInMonth) {
			calendar.set(Calendar.DAY_OF_MONTH, daysInMonth);
		}
	}

	@Override
	public void onClick(View v) {
		tryVibrate();
		if (v.getId() == R.id.date_picker_year) {
			setCurrentView(YEAR_VIEW);
		} else if (v.getId() == R.id.date_picker_month_and_day) {
			setCurrentView(MONTH_AND_DAY_VIEW);
		}
	}

	@Override
	public void onYearSelected(int year) {
		adjustDayInMonthIfNeeded(mCalendar);
		mCalendar.set(Calendar.YEAR, year);
		updatePickers();
		setCurrentView(MONTH_AND_DAY_VIEW);
		updateDisplay(true);
	}

	@Override
	public void onDayOfMonthSelected(int year, int month, int day) {

		mCalendar.set(Calendar.YEAR, year);
		mCalendar.set(Calendar.MONTH, month);
		mCalendar.set(Calendar.DAY_OF_MONTH, day);
		updatePickers();
		updateDisplay(true);
	}

	private void updatePickers() {
		for (OnDateChangedListener listener : mListeners)
			listener.onDateChanged();
	}

	@Override
	public MonthAdapter.CalendarDay getSelectedDay() {
		return new MonthAdapter.CalendarDay(mCalendar);
	}

	@Override
	public int getMinYear() {
		if (selectableDays != null)
			return selectableDays[0].get(Calendar.YEAR);
		// Ensure no years can be selected outside of the given minimum date
		return mMinDate != null && mMinDate.get(Calendar.YEAR) > mMinYear ? mMinDate.get(Calendar.YEAR) : mMinYear;
	}

	@Override
	public int getMaxYear() {
		if (selectableDays != null)
			return selectableDays[selectableDays.length - 1].get(Calendar.YEAR);
		// Ensure no years can be selected outside of the given maximum date
		return mMaxDate != null && mMaxDate.get(Calendar.YEAR) < mMaxYear ? mMaxDate.get(Calendar.YEAR) : mMaxYear;
	}

	@Override
	public int getFirstDayOfWeek() {
		return mWeekStart;
	}

	@Override
	public void registerOnDateChangedListener(OnDateChangedListener listener) {
		mListeners.add(listener);
	}

	@Override
	public void unregisterOnDateChangedListener(OnDateChangedListener listener) {
		mListeners.remove(listener);
	}

	@Override
	public void tryVibrate() {
		if (mVibrate)
			mHapticFeedbackController.tryVibrate();
	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub
		Log.e("Current Class Name!!!!!!!!!!!!!!", fragment.getClass().getName());
		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

	}

}
