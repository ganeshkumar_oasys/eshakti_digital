package com.yesteam.eshakti.materialdaterangepicker;

import java.util.Calendar;

import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog.OnDateChangedListener;

/**
 * Controller class to communicate among the various components of the date
 * picker dialog.
 */
public interface DatePickerController {

	void onYearSelected(int year);

	void onDayOfMonthSelected(int year, int month, int day);

	void registerOnDateChangedListener(OnDateChangedListener listener);

	void unregisterOnDateChangedListener(OnDateChangedListener listener);

	MonthAdapter.CalendarDay getSelectedDay();

	boolean isThemeDark();

	Calendar[] getHighlightedDays();

	Calendar[] getSelectableDays();

	int getFirstDayOfWeek();

	int getMinYear();

	int getMaxYear();

	Calendar getMinDate();

	Calendar getMaxDate();

	void tryVibrate();
}
