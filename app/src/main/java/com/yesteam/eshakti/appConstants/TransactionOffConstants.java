package com.yesteam.eshakti.appConstants;

import android.app.Application;

public class TransactionOffConstants extends Application {

	public static String TRANS_SAV = "TRANS_SAV";
	public static String TRANS_VSAV = "TRANS_VSAV";
	public static String TRANS_PLRP = "TRANS_PLRP";
	public static String TRANS_MLRP = "TRANS_MLRP";
	public static String TRANS_EXP = "TRANS_EXP";
	public static String TRANS_OTH = "TRANS_OTH";
	public static String TRANS_SUB = "TRANS_SUB";
	public static String TRANS_PEN = "TRANS_PEN";
	public static String TRANS_DON = "TRANS_DON";
	public static String TRANS_FED = "TRANS_FED";
	public static String TRANS_GLRP = "TRANS_GLRP";
	public static String TRANS_BDEP = "TRANS_BDEP";
	public static String TRANS_FIXD = "TRANS_FIXD";
	public static String TRANS_PDB = "TRANS_PDB";
	public static String TRANS_ATT = "TRANS_ATT";
	public static String TRANS_MINM = "TRANS_MINM";
	public static String TRANS_AUD = "TRANS_AUD";
	public static String TRANS_TRAN = "TRANS_TRAN";
	public static String TRANS_LOANWITHD = "TRANS_LOANWITHD";
	public static String TRANS_ACCTO = "TRANS_ACCTO";
	public static String TRANS_LOANACC = "TRANS_LOANACC";
	public static String TRANS_SEED = "TRANS_SEED";

}
