package com.yesteam.eshakti.appConstants;

public class RegionalLanguage_hindi {
	public RegionalLanguage_hindi() {
		// TODO Auto-generated constructor stub
	}

	public static void RegionalStrings() {

		AppStrings.LoginScreen = "लॉगिन स्क्रीन";
		AppStrings.userName = "यूजरनेम";
		AppStrings.passWord = "पासवर्ड";
		AppStrings.transaction = "लेन देन";
		AppStrings.reports = "रिपोर्ट";
		AppStrings.profile = "प्रोफाइल";
		AppStrings.meeting = "बैठक";
		AppStrings.settings = "सेटिंग्स";
		AppStrings.help = "मदद";
		AppStrings.Attendance = "उपस्थितिः";
		AppStrings.MinutesofMeeting = "बैठक के कार्यवृत्त";
		AppStrings.savings = "जमा पूंजी";
		AppStrings.InternalLoanDisbursement = "ऋण भुगतान";
		AppStrings.expenses = "खर्च";
		AppStrings.cashinhand = "हाथ में पैसे :  ₹ ";
		AppStrings.cashatBank = "बैंक में नकदी :  ₹ ";
		AppStrings.memberloanrepayment = "सदस्य ऋण चुकौती";
		AppStrings.grouploanrepayment = "ग्रुप ऋण चुकौती";
		AppStrings.income = "आय";
		AppStrings.bankTransaction = "बैंक का लेन - देन";
		AppStrings.fixedDeposit = "सावधि जमा";
		AppStrings.otherincome = "अन्य आय";
		AppStrings.subscriptioncharges = "अंशदान";
		AppStrings.penalty = "दंड";
		AppStrings.donation = "दान";
		AppStrings.federationincome = "संघ को अंशदान";
		AppStrings.date = "तारीख";
		AppStrings.passwordchange = "पासवर्ड बदलें";
		AppStrings.listofexpenses = "खर्च की सूची";
		AppStrings.transport = "परिवहन";
		AppStrings.snacks = "चाय";
		AppStrings.telephone = "टेलीफोन";
		AppStrings.stationary = "अंशदान व्यय";
		AppStrings.otherExpenses = "अन्य खर्चे";
		AppStrings.federationIncomePaid = "संघ आय (भुगतान)";
		AppStrings.bankInterest = "बैंक का ब्याज";
		AppStrings.bankExpenses = "बैंक व्यय";
		AppStrings.bankrepayment = "बैंक चुकौती";
		AppStrings.total = "कुल";
		AppStrings.summary = "सारांश";
		AppStrings.savingsSummary = "बचत सारांश";
		AppStrings.loanSummary = "ऋण सारांश";
		AppStrings.lastMonthReport = "मासिक रिपोर्ट";
		AppStrings.Memberreports = "सदस्य रिपोर्ट";
		AppStrings.GroupReports = "समूह की रिपोर्ट";
		AppStrings.transactionsummary = "बैंक लेनदेन सारांश";
		AppStrings.balanceSheet = "बैलेंस शीट";
		AppStrings.balanceSheetHeader = "बैलेंस शीट पर";
		AppStrings.trialBalance = "ट्रायल बैलेंस";
		AppStrings.outstanding = "बकाया";
		AppStrings.totalSavings = "कुल बचत";
		AppStrings.groupSavingssummary = "समूह बचत सारांश";
		AppStrings.groupLoansummary = "ग्रुप ऋण सारांश";
		AppStrings.amount = "रकम";
		AppStrings.principleAmount = "रकम";
		AppStrings.interest = "ब्याज";
		AppStrings.groupLoan = "ग्रुप ऋण";
		AppStrings.balance = "संतुलन";
		AppStrings.Name = "नाम";
		AppStrings.Repaymenybalance = "चुकौती संतुलन";
		AppStrings.details = "विवरण";
		AppStrings.payment = "भुगतान";
		AppStrings.receipt = "रसीद";
		AppStrings.FromDate = "तारीख से";
		AppStrings.ToDate = "तारीख तक";
		AppStrings.fromDate = "से";
		AppStrings.toDate = " सेवा में";
		AppStrings.OldPassword = "पुराना पासवर्ड";
		AppStrings.NewPassword = "नया पासवर्ड";
		AppStrings.ConfirmPassword = "पासवर्ड की पुष्टि कीजिये";
		AppStrings.agentProfile = "एजेंट प्रोफ़ाइल";
		AppStrings.groupProfile = "ग्रूप प्रोफाइल";
		AppStrings.bankDeposit = "बैंक जमा";
		AppStrings.withdrawl = "वापसी";
		AppStrings.OutstandingAmount = "बकाया";
		AppStrings.GroupLogin = "ग्रूप लॉगिन";
		AppStrings.AgentLogin = "एजेंट लॉगिन";
		AppStrings.AboutLicense = "लाइसेंस";
		AppStrings.contacts = "संपर्क";
		AppStrings.contactNo = "संपर्क नंबर";
		AppStrings.cashFlowStatement = "नकदी प्रवाह विवरण";
		AppStrings.IncomeExpenditure = "आय व्यय";
		AppStrings.transactionCompleted = "सौदा पूर्ण";
		AppStrings.loginAlert = "यूज़रनेम और पासवर्ड गलत कर रहे हैं";
		AppStrings.loginUserAlert = "प्रदान उपयोक्ता विवरण";
		AppStrings.loginPwdAlert = "प्रदान पासवर्ड विवरण";
		AppStrings.pwdChangeAlert = "पासवर्ड सफलतापूर्वक बदल दिया गया है";
		AppStrings.pwdIncorrectAlert = "सही पासवर्ड दर्ज";
		AppStrings.groupRepaidAlert = "अपने बकाया राशि की जांच करें";//"आपकी राशि की जांच";
		AppStrings.cashinHandAlert = "हाथ में नकदी की जांच";
		AppStrings.cashatBankAlert = "बैंक में नकदी की जांच";
		AppStrings.nullAlert = "प्रदान नकद विवरण";
		AppStrings.DepositnullAlert = "सभी उपलब्ध कराने के नकद विवरण";
		AppStrings.MinutesAlert = "कृपया ऊपर से किसी को भी उठाओ";
		AppStrings.afterDate = "इस तिथि के बाद एक तारीख लें";
		AppStrings.last5Transaction = "पिछले पांच लेनदेन";
		AppStrings.internetError = "कृपया अपने इंटरनेट कनेक्शन की जाँच करें";
		AppStrings.lastTransactionDate = "पिछले लेनदेन की तारीख";
		AppStrings.yes = "ठीक";
		AppStrings.credit = "क्रेडिट";
		AppStrings.debit = "डेबिट";
		AppStrings.memberName = "सदस्य का नाम";
		AppStrings.OutsatndingAmt = "बकाया";
		AppStrings.calFromToDateAlert = " दोनों तारीख का चयन करें";
		AppStrings.selectedDate = "चयनित तिथि";
		AppStrings.calAlert = "अलग तिथि उठाओ";
		AppStrings.dialogMsg = " क्या आप इस तिथि के साथ जारी रखना चाहते हैं";
		AppStrings.dialogOk = "हाँ";
		AppStrings.dialogNo = "नहीं";

		AppStrings.aboutYesbooks = "EShakti या स्वयं सहायता समूहों का डिजिटाइजेशन हमारे माननीय प्रधानमंत्री के बयान के साथ नाबार्ड के माइक्रो क्रेडिट और"
				+ " नवाचार विभाग की एक पहल है 'हम इलेक्ट्रॉनिक डिजिटल भारत के सपने के साथ ले जाने के लिए ...'। डिजिटल इंडिया भारत सरकार की एक रुपये 1.13 लाख करोड़ "
				+ "पहल सरकारी विभागों और भारत के लोगों को एकीकृत करने के लिए और प्रभावी शासन सुनिश्चित करने के लिए है। यह डिजिटल सशक्त समाज और ज्ञान अर्थव्यवस्था के साथ भारत को बदलना है।";

		AppStrings.groupList = "समूह का चयन";// GROUP LIST
		AppStrings.login = "लॉग इन करें";
		AppStrings.submit = "जमा करें";
		AppStrings.edit = "संपादित करें";
		AppStrings.bankBalance = "बैंक राशि";
		AppStrings.balanceAsOn = "   पर संतुलन : ";
		AppStrings.savingsBook = "  बचत शेष पुस्तक अनुसार : ";
		AppStrings.savingsBank = "  बचत शेष बैंक अनुसार : ";
		AppStrings.difference = " अंतर : ";
		AppStrings.loanOutstandingBook = " बकाया पुस्तक अनुसार : ";
		AppStrings.loanOutstandingBank = " बकाया बैंक अनुसार : ";
		AppStrings.values = new String[] { "मूल (गांव)",

				"सभी सदस्य उपस्थित हैं",

				"बचत राशि एकत्र",

				"व्यक्तिगत ऋण संवितरण के बारे में चर्चा",

				"भुगतान राशि और ब्याज एकत्र",

				"प्रशिक्षण होने के बारे में चर्चा",

				"बैंक ऋण प्राप्त करने का निर्णय लिया",

				"आम विचारों के बारे में चर्चा",

				"साफ-सफाई के बारे में चर्चा",

				"शिक्षा के महत्व के बारे में चर्चा की" };

		AppStrings.January = "जनवरी";
		AppStrings.February = "फ़रवरी";
		AppStrings.March = "मार्च";
		AppStrings.April = "अप्रैल";
		AppStrings.May = "मई";
		AppStrings.June = "जून";
		AppStrings.July = "जुलाई";
		AppStrings.August = "अगस्त";
		AppStrings.September = "सितंबर";
		AppStrings.October = "अक्टूबर";
		AppStrings.November = "नवम्बर";
		AppStrings.December = "दिसम्बर";
		AppStrings.uploadPhoto = "फोटो अपलोड करें";
		AppStrings.Il_Disbursed = "आंतरिक ऋण वितरित की";
		AppStrings.IL_Loan = "आंतरिक ऋण";
		AppStrings.Il_Repaid = "आंतरिक कर्ज चुकाया";
		AppStrings.offlineTransactionCompleted = "अपने ऑफ़लाइन लेनदेन सफलतापूर्वक सहेजा गया है";
		AppStrings.offlineTransactionreports = "ऑफ़लाइन लेनदेन रिपोर्ट";
		AppStrings.offlineTransaction = "ऑफ़लाइन लेनदेन";
		AppStrings.disconnectInternet = "ऑफ़लाइन सेवा जारी रखने के लिए कृपया अपने इंटरनेट को डिस्कनेक्ट करें ";
		AppStrings.Dashboard = "डैशबोर्ड";
		AppStrings.bankTransactionSummary = "बैंक लेनदेन सारांश";
		AppStrings.BL_Disbursed = "संवितरित";
		AppStrings.BL_Repaid = "चुकाया";
		AppStrings.auditing = "लेखा परीक्षा";
		AppStrings.training = "प्रशिक्षण";
		AppStrings.auditor_Name = "ऑडिटर का नाम";
		AppStrings.auditing_Date = "लेखा परीक्षा तिथि";
		AppStrings.training_Date = "प्रशिक्षण तिथि";
		AppStrings.auditing_Period = "लेखा परीक्षा अवधि चुनें";
		AppStrings.training_types = new String[] { "स्वास्थ्य जागरूकता", "बुक कीपर जागरूकता", "आजीविका जागरूकता",
				"सामाजिक जागरूकता", "शैक्षणिक जागरूकता" };
		AppStrings.changeLanguage = "भाषा बदलो";
		AppStrings.interestRepayAmount = "मौजूदा देय";
		AppStrings.interestRate = "ब्याज दर";
		AppStrings.savingsAmount = "बचत राशि";
		AppStrings.noGroupLoan_Alert = "कोई समूह ऋण उपलब्ध।";
		AppStrings.confirmation = "पुष्टि";
		AppStrings.logOut = "लॉग आउट";
		AppStrings.fromDateAlert = "तिथि से उपलब्ध कराएं";
		AppStrings.toDateAlert = "तारीख तक उपलब्ध कराएं";
		AppStrings.auditingDateAlert = "कृपया प्रदान लेखा परीक्षा तिथि";
		AppStrings.auditorAlert = "कृपया प्रदान ऑडिटर का नाम";
		AppStrings.nullDetailsAlert = "सभी उपलब्ध कराने के विवरण";
		AppStrings.trainingDateAlert = "प्रशिक्षण तारीख प्रदान";
		AppStrings.trainingTypeAlert = "कम से कम एक प्रशिक्षण प्रकार का चयन";
		AppStrings.offline_ChangePwdAlert = "आप इंटरनेट के बिना पासवर्ड बदल नहीं सकते";
		AppStrings.transactionFailAlert = "अपने लेनदेन असफल है";
		AppStrings.voluntarySavings = "स्वैच्छिक बचत";
		AppStrings.tenure = "कार्यकाल";
		AppStrings.purposeOfLoan = "ऋण का उद्देश्य";
		AppStrings.chooseLabel = "ऋण का चयन";
		AppStrings.Tenure_POL_Alert = "कृपया ऋण और कार्यकाल की अवधि के उद्देश्य दर्ज करें";
		AppStrings.interestSubvention = "ब्याज आर्थिक सहायता";
		AppStrings.adminAlert = "कृपया अपने व्यवस्थापक से संपर्क करें";
		AppStrings.userNotExist = "प्रवेश जानकारी मौजूद नहीं है। कृपया ऑनलाइन प्रवेश जारी है।";
		AppStrings.tryLater = "बाद में कोशिश करें";
		AppStrings.offlineDataAvailable = "ऑफ़लाइन आंकड़ों उपलब्ध है। कृपया लॉग आउट करें और फिर कोशिश करें।";
		AppStrings.choosePOLAlert = "ऋण का उद्देश्य चयन";
		AppStrings.InternalLoan = "आंतरिक ऋण";
		AppStrings.TrainingTypes = "प्रशिक्षण प्रकार";
		AppStrings.uploadInfo = "सदस्य विवरण";
		AppStrings.uploadAadhar = "अपलोड आधार";
		AppStrings.month = "-- महीने का चयन --";
		AppStrings.year = "-- वर्ष का चयन --";
		AppStrings.monthYear_Alert = "कृपया महीना और वर्ष चयन करें ";
		AppStrings.chooseLanguage = "भाषा चुनें";
		AppStrings.autoFill = "ऑटो भरें";
		AppStrings.viewAadhar = "देखें आधार";
		AppStrings.viewPhoto = "चित्र देखें";
		AppStrings.uploadAlert = "आप आधार और फोटो इंटरनेट कनेक्शन के बिना अपलोड नहीं कर सकते";// "आप
																								// आधार
																								// और
																								// फोटो
																								// इंटरनेट
																								// कनेक्शन
																								// के
																								// बिना
																								// अपलोड
																								// नहीं
																								// कर
																								// सकते";
		AppStrings.upload = "अपलोड";
		AppStrings.view = "देखे";
		AppStrings.showAadharAlert = "कृपया अपने आधार कार्ड दिखायें";
		AppStrings.deactivateAccount = "खाता निष्क्रिय करें";
		AppStrings.deactivateAccount_SuccessAlert = "आपका खाता सफलतापूर्वक निष्क्रिय किया गया";
		AppStrings.deactivateAccount_FailAlert = "अपने खाते को निष्क्रिय करने में विफल किया गया है";
		AppStrings.deactivateNetworkAlert = "आप इंटरनेट कनेक्शन के बिना अपने खाते निष्क्रिय नहीं कर सकते";
		AppStrings.offlineReports = "ऑफ़लाइन रिपोर्ट";
		AppStrings.registrationSuccess = "आप सफलतापूर्वक पंजीकृत हैं";
		AppStrings.reg_MobileNo = "पंजीकृत मोबाइल नंबर दर्ज";
		AppStrings.reg_IMEINo = "पहुंच अस्वीकृत।";
		AppStrings.reg_BothNo = "पहुंच अस्वीकृत।";
		AppStrings.reg_IMEIUsed = "पहुंच अस्वीकृत। डिवाइस बेमेल";
		AppStrings.signInAsDiffUser = " विभिन्न उपयोगकर्ता के रूप में प्रवेश";
		AppStrings.incorrectUserNameAlert = "ग़लत उपयोगकर्ता नाम";

		AppStrings.noofflinedatas = "कोई ऑफ़लाइन हस्तांतरण प्रविष्टि नहीं है";
		AppStrings.uploadprofilealert = "अपनी स्वयं के विवरण अपलोड करें";
		AppStrings.monthYear_Warning = "उचित महीने और साल का चयन करें";
		AppStrings.of = " का ";
		AppStrings.previous_DateAlert = "चयन पिछली दिनांक";
		AppStrings.nobanktransactionreportdatas = "कोई भी बैंक लेनदेन रिपोर्ट डेटा";

		AppStrings.mDefault = "चूक";
		AppStrings.mTotalDisbursement = "कुल संवितरण";
		AppStrings.mInterloan = "व्यक्तिगत ऋण चुकौती";
		AppStrings.mTotalSavings = "कुल संग्रह";
		AppStrings.mCommonNetworkErrorMsg = " कृपया अपने नेटवर्क कनेक्शन जाँच लें";
		AppStrings.mLoginAlert_ONOFF = " इंटरनेट कनेक्शन का उपयोग करके कृपया  फिर से  लॉगिन करें ";
		AppStrings.changeLanguageNetworkException = "कृपया लॉगआउट, फिर लॉगइन करने के लिए इंटरनेट कनेक्शन का उपयोग कर प्रयास";
		AppStrings.photouploadmsg = "फोटो सफलतापूर्वक अपलोड";
		AppStrings.loginAgainAlert = "पृष्ठभूमि डेटा अभी भी लोड हो रहा है कुछ समय के लिए कृपया प्रतीक्षा करें।";//"कृपया फिर से लॉगिन करें ";

		/* Interloan Disbursement menu */
		AppStrings.mInternalInterloanMenu = "आंतरिक ऋण";
		AppStrings.mInternalBankLoanMenu = "बैंक ऋण";
		AppStrings.mInternalMFILoanMenu = "एमएफआई ऋण";
		AppStrings.mInternalFederationMenu = "संघ आय";

		AppStrings.bankName = "बैंक का नाम";
		AppStrings.mfiname = "एमएफआई नाम";
		AppStrings.loanType = "प्रकार के ऋण";
		AppStrings.bankBranch = "बैंक शाखा";
		AppStrings.installment_type = "किस्त प्रकार";
		AppStrings.NBFC_Name = "फाइनान्स कंपनी";
		AppStrings.Loan_Account_No = "ऋण खाता संख्या";
		AppStrings.Loan_Sanction_Amount = "ऋण स्वीकृति राशि";
		AppStrings.Loan_Sanction_Date = "ऋण मंजूरी की तारीख";
		AppStrings.Loan_Disbursement_Amount = "ऋण वितरण राशि";

		AppStrings.Loan_Disbursement_Date = "ऋण वितरण की तारीख";
		AppStrings.Loan_Tenure = "लोन की अवधि";
		AppStrings.Subsidy_Amount = "सब्सिडी की राशि";
		AppStrings.Subsidy_Reserve_Fund = "सब्सिडी आरक्षित राशि";
		AppStrings.Interest_Rate = "ब्याज दर";

		AppStrings.mMonthly = "महीने के";
		AppStrings.mQuarterly = "त्रैमासिक";
		AppStrings.mHalf_Yearly = "अर्धवार्षिक";
		AppStrings.mYearly = "वार्षिक";
		AppStrings.mAgri = "कृषि";
		AppStrings.mMSME = "एमएसएमई";
		AppStrings.mOthers = " अन्य";

		AppStrings.mMFI_NAME_SPINNER = "एमएफआई नाम";
		AppStrings.mMFINabfins = "NABFINS";
		AppStrings.mMFIOthers = " अन्य";

		AppStrings.mInternalTypeLoan = "आंतरिक ऋण";

		AppStrings.mPervious = "पिछला";
		AppStrings.mNext = "अगला";
		AppStrings.mPasswordUpdate = "अपने पासवर्ड को सफलतापूर्वक अपडेट";

		AppStrings.mFixedDepositeAmount = "सावधि जमा राशि";
		AppStrings.mFedBankName = "संघ नाम";// "BANK/NBFC NAME";
		AppStrings.mLoanSanc_loanDist = "ऋण वितरण राशि की जाँच करें";
		AppStrings.mStepWise_LoanDibursement = "आंतरिक ऋण वितरण";

		AppStrings.mExpense_meeting = "बैठक खर्च";// बैठक खर्च";
		AppStrings.mSubmitbutton = "ठीक";

		AppStrings.mNewUserSignup = "पंजीकरण";
		AppStrings.mMobilenumber = "मोबाइल नंबर";
		AppStrings.mIsAadharAvailable = "आधार जानकारी अद्यतन नहीं";
		AppStrings.mSavingsTransaction = "बचत लेनदेन";
		AppStrings.mBankCharges = "बैंक प्रभार";
		AppStrings.mCashDeposit = "नकद जमा";
		AppStrings.mLimit = "सीमा";

		AppStrings.mAccountToAccountTransfer = "खाता स्थानांतरण करने के लिए खाते";
		AppStrings.mTransferFromBank = "बैंक से";
		AppStrings.mTransferToBank = "बैंक के लिए";
		AppStrings.mTransferAmount = "स्थानांतरण राशि";
		AppStrings.mTransferCharges = "स्थानांतरण शुल्क";
		AppStrings.mTransferSpinnerFloating = "बैंक के लिए";
		AppStrings.mTransferNullToast = "प्रदान करें सभी विवरण";
		AppStrings.mAccToAccTransferToast = "आप केवल एक ही बैंक खाता है";

		/* Loan Account */
		AppStrings.mLoanaccHeader = "ऋण खाता";
		AppStrings.mLoanaccCash = "नकदी";
		AppStrings.mLoanaccBank = "बैंक";
		AppStrings.mLoanaccWithdrawal = "वापसी";
		AppStrings.mLoanaccExapenses = "खर्च";
		AppStrings.mLoanaccSpinnerFloating = "ऋण खाता बैंक";
		AppStrings.mLoanaccCash_BankToast = "कृपया चुनें एक नकद या बैंक";
		AppStrings.mLoanaccNullToast = "प्रदान करें सभी विवरण";
		AppStrings.mLoanaccBankNullToast = "कृपया चुनें एक बैंक नाम";

		AppStrings.mBankTransSavingsAccount = "बचत खाता";
		AppStrings.mBankTransLoanAccount = "ऋण खाता";

		AppStrings.mLoanAccType = "प्रकार";
		AppStrings.mLoanAccBankAmountAlert = "कृपया जाँच बैंक बैलेंस";

		AppStrings.mCashAtBank = "बैंक में नकदी";
		AppStrings.mCashInHand = "हाथ में पैसे";
		AppStrings.mShgSeedFund = "स्वयं सहायता समूह के बीज कोष";
		AppStrings.mFixedAssets = "अचल संपत्तियां";
		AppStrings.mVerified = "सत्यापित";
		AppStrings.mConfirm = "पुष्टि करें";
		AppStrings.mProceed = "बढ़ना";
		AppStrings.mDialogAlertText = "अपने उद्घाटन संतुलन datas सफलतापूर्वक अद्यतन कर रहे हैं, अब आप आगे बढ़ सकते हैं";
		AppStrings.mBalanceSheetDate = "बैलेंस शीट पर अपलोड किया गया है";
		AppStrings.mCheckMonthlyEntries = "पिछले महीने के कोई प्रविष्टि नहीं है, तो आप पिछले महीने के datas रखा जाना चाहिए";
		AppStrings.mTermLoanOutstanding = "OUTSTANDING";
		AppStrings.mCashCreditOutstanding = "OUTSTANDING";
		AppStrings.mGroupLoanOutstanding = "समूह ऋण बकाया";
		AppStrings.mMemberLoanOutstanding = "सदस्य ऋण बकाया";
		AppStrings.mContinueWithDate = "आप इस तिथि में परिवर्तन करना चाहते हैं";
		AppStrings.mCheckList = "सूची की जांच";
		AppStrings.mMicro_Credit_Plan = "माइक्रो क्रेडिट योजना";
		AppStrings.mAadharCard_Invalid = "अपने आधार कार्ड अमान्य है";
		AppStrings.mIsNegativeOpeningBalance = "नकारात्मक संतुलन को सुधारने";
		AppStrings.mLoginDetailsDelete = "पिछले प्रवेश जानकारी को हटा दिया जाएगा। आप डेटा को नष्ट करना चाहते हैं?";
		AppStrings.mVerifyGroupAlert = "आप को सत्यापित करने के लिए ऑनलाइन होना चाहिए";
		AppStrings.next = "आगामी";
		AppStrings.mNone = "कोई नहीं";
		AppStrings.mSelect = "चुनते हैं";
		AppStrings.mAnimatorName = "एनिमेटर नाम";
		AppStrings.mSavingsAccount = "बचत खाता";
		AppStrings.mAccountNumber = "खाता संख्या";
		AppStrings.mLoanAccountAlert = "ऑफ़लाइन मोड में ऋण खाता ट्रांसफर नहीं कर सकता";
		AppStrings.mSeedFund = "बीज निधि";
		AppStrings.mLoanTypeAlert = "कृपया एक ऋण खाता प्रकार चुनें";
		AppStrings.mOutstandingAlert = "कृपया अपना बकाया राशि जांचें";
		AppStrings.mLoanAccTransfer = "ऋण खाता अंतरण";
		AppStrings.mLoanName = "ऋण नाम";
		AppStrings.mLoanId = "ऋण आईडी";
		AppStrings.mLoanDisbursementDate = "ऋण संवितरण की तारीख";
		AppStrings.mAdd = "जोड़ें";
		AppStrings.mLess = "कम से";
		AppStrings.mRepaymentWithInterest = "पुनर्भुगतान (ब्याज के साथ)";
		AppStrings.mCharges = "शुल्क";
		AppStrings.mExistingLoan = "मौजूदा ऋण";
		AppStrings.mNewLoan = "नया ऋण";
		AppStrings.mIncreaseLimit = "सीमा बढ़ाएं";
		AppStrings.mAvailableLimit = "उपलब्ध सीमा";
		AppStrings.mDisbursementDate = "संवितरण तिथि";
		AppStrings.mDisbursementAmount = "संवितरण राशि";
		AppStrings.mSanctionAmount = "मंजूरी राशि";
		AppStrings.mBalanceAmount = "शेष राशि";
		AppStrings.mMemberDisbursementAmount = "सदस्य संवितरण राशि";
		AppStrings.mLoanDisbursementFromLoanAcc = "ऋण खाते से ऋण वितरण";
		AppStrings.mLoanDisbursementFromSbAcc = "एसबी खाते से ऋण वितरण";
		AppStrings.mRepaid = "चुकाया";
		AppStrings.mCheckBackLog = "बैकलॉग की जांच करें";
		AppStrings.mTarget = "लक्ष्य";
		AppStrings.mCompleted = "पूरा कर लिया है";
		AppStrings.mPending = "लंबित";
		AppStrings.mAskLogout = "क्या आप लॉगआउट करना चाहते हैं?";
		AppStrings.mCheckDisbursementAlert = "कृपया अपने संवितरण राशि की जांच करें";
		AppStrings.mCheckbalanceAlert = "कृपया अपनी शेष राशि की जांच करें";
		AppStrings.mVideoManual = "वीडियो मैनुअल";
		AppStrings.mPdfManual = "पीडीएफ मैनुअल";
		AppStrings.mAmountDisbursingAlert = "क्या आप सदस्यों को संवितरण के बिना जारी रखना चाहते हैं?";
		AppStrings.mOpeningDate = "खोलने की तिथि";
		AppStrings.mCheckLoanSancDate_LoanDisbDate = "कृपया अपने ऋण स्वीकृति दिनांक और ऋण वितरण दिनांक की जाँच करें।";
		AppStrings.mLoanDisbursementFromRepaid = "सीसी ऋण वापसी";
		AppStrings.mExpense = "खर्चों";
		AppStrings.mBeforeLoanPay = "ऋण भुगतान से पहले";
		AppStrings.mLoans = "ऋण";
		AppStrings.mSurplus = "अतिरिक्त";
		AppStrings.mPOLAlert = "कृपया ऋण का उद्देश्य दर्ज करें";
		AppStrings.mCheckLoanAmounts = "कृपया अपने ऋण स्वीकृति राशि और ऋण वितरण राशि की जांच करें";
		AppStrings.mCheckLoanSanctionAmount = "अपने ऋण स्वीकृति राशि दर्ज करें";
		AppStrings.mCheckLoanDisbursementAmount = "अपना ऋण संवितरण राशि दर्ज करें";
		AppStrings.mIsPasswordSame = "कृपया अपने पुराने पासवर्ड की जांच करें और नया पासवर्ड समान नहीं होना चाहिए।";
		AppStrings.mSavings_Banktransaction = "बचत - बैंक लेनदेन";
		AppStrings.mCheckBackLogOffline = "ऑफ़लाइन जांच बैकलाग नहीं देख सकता है";
		AppStrings.mCredit = "श्रेय";
		AppStrings.mDebit = "नामे";
		AppStrings.mCheckFixedDepositAmount = "कृपया अपनी सावधि जमा राशि जांचें";
		AppStrings.mCheckDisbursementDate = "कृपया अपने ऋण संवितरण की तारीख जांचें";
		AppStrings.mSendEmail = "EMAIL";
		AppStrings.mTotalInterest = "कुल ब्याज";
		AppStrings.mTotalCharges = "कुल शुल्क";
		AppStrings.mTotalRepayment = "कुल चुकौती";
		AppStrings.mTotalInterestSubventionRecevied = "कुल ब्याज सबवेंशन प्राप्त हुआ";
		AppStrings.mTotalBankCharges = "कुल बैंक शुल्क";
		AppStrings.mDone = "DONE";
		AppStrings.mPayment = "भुगतान";
		AppStrings.mCashWithdrawal = "नकद निकासी";
		AppStrings.mFundTransfer = "फंड ट्रांसफर";
		AppStrings.mDepositAmount = "जमा राशि";
		AppStrings.mWithdrawalAmount = "निकाली गयी राशि";
		AppStrings.mTransactionMode = "लेनदेन मोड";
		AppStrings.mBankLoanDisbursement = "बैंक ऋण वितरण";
		AppStrings.mMemberToMember = "सदस्य से दूसरे सदस्य तक";
		AppStrings.mDebitAccount = "डेबिट अकाउंट";
		AppStrings.mSHG_SavingsAccount = "शॉर्ट बचत खाता";
		AppStrings.mDestinationMemberName = "गंतव्य सदस्य का नाम";
		AppStrings.mSourceMemberName = "स्रोत सदस्य का नाम";
		AppStrings.mMobileNoUpdation = "मोबाइल नंबर अद्यतन";
		AppStrings.mMobileNo = "मोबाइल नंबर";
		AppStrings.mMobileNoUpdateSuccessAlert = "मोबाइल नंबर सफलतापूर्वक अपडेट किया गया।";
		AppStrings.mAadhaarNoUpdation = "आधार संख्या ऊपर";
		AppStrings.mSHGAccountNoUpdation = "शॉ खाता संख्या संख्या";
		AppStrings.mAccountNoUpdation = "सदस्य खाता संख्या अद्यतन";
		AppStrings.mAadhaarNo = "आधार नंबर एक";
		AppStrings.mBranchName = "शाखा का नाम";
		AppStrings.mAadhaarNoUpdateSuccessAlert = "आधार संख्या सफलतापूर्वक अपडेट की गई";
		AppStrings.mMemberAccNoUpdateSuccessAlert = "सदस्य खाता संख्या सफलतापूर्वक अपडेट की गई";
		AppStrings.mSHGAccNoUpdateSuccessAlert = "एसएचजी खाता संख्या सफलतापूर्वक अपडेट की गई";
		AppStrings.mCheckValidMobileNo = "कृपया एक वैध मोबाइल नंबर दर्ज करें";
		AppStrings.mInvalidAadhaarNo = "अमान्य आधार संख्या";
		AppStrings.mCheckValidAadhaarNo = "कृपया वैध आधार संख्या दर्ज करें";
		AppStrings.mInvalidAccountNo = "अमान्य खाता संख्या";
		AppStrings.mCheckAccountNumber = "कृपया अपना खाता नंबर देखें";
		AppStrings.mInvalidMobileNo = "अमान्य मोबाइल नंबर";
		AppStrings.mUploadScheduleAlert = "CAN'T UPLOAD SCHEDULE VI IN OFFLINE MODE";
		AppStrings.mIsMobileNoRepeat = "मोबाइल नंबर दोहराया";
		AppStrings.mMobileNoUpdationNetworkCheck = "ऑफ़लाइन मोड में मोबाइल नंबर अपडेट नहीं कर सकता";
		AppStrings.mAadhaarNoNetworkCheck = "ऑफ़लाइन मोड में आधार संख्या अपडेट नहीं कर सकता";
		AppStrings.mShgAccNoNetworkCheck = "ऑफलाइन मोड में एसएचजी खाता संख्या अपडेट नहीं कर सकता";
		AppStrings.mAccNoNetworkCheck = "ऑफ़लाइन मोड में सदस्य खाता संख्या अपडेट नहीं कर सकता";
		AppStrings.mAccountNoRepeat = "खाता संख्या दोहराई गई";
		AppStrings.mCheckBankAndBranchNameSelected = "कृपया संबंधित खाता संख्या के लिए चयनित बैंक और शाखा का नाम देखें";
		AppStrings.mCheckNewLoanSancDate = "कृपया ऋण स्वीकृति तिथि का चयन करें";
		AppStrings.mCreditLinkageInfo = "क्रेडिट लिंकेज जानकारी";
		AppStrings.mCreditLinkage = "क्रेडिट जुड़ाव";
		AppStrings.mCreditLinkage_content = "Number of times bank loans were taken by the SHG and repaid completely.";
		AppStrings.mCreditLinkageAlert = "ऑफ़लाइन मोड में क्रेडिट लिंकेज जानकारी अपडेट नहीं कर सकता";
	}
	
}
