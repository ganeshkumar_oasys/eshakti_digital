package com.yesteam.eshakti.appConstants;

public class RegionalLanguage_Kannada {
	public RegionalLanguage_Kannada() {
		// TODO Auto-generated constructor stub
	}

	public static void RegionalStrings() {

		AppStrings.LoginScreen = "ಲಾಗಿನ್ ಸ್ಕ್ರೀನ್";
		AppStrings.userName = "ಮೊಬೈಲ್ ನಂಬರ";
		AppStrings.passWord = "ಪಾಸ್ವರ್ಡ್";
		AppStrings.transaction = "ವ್ಯವಹಾರ";
		AppStrings.reports = "ವರದಿಗಳು";
		AppStrings.profile = "ಪ್ರೊಫೈಲ್";
		AppStrings.meeting = "ಸಭೆಯಲ್ಲಿ";
		AppStrings.settings = "ಸೆಟ್ಟಿಂಗ್ಗಳು";
		AppStrings.help = "ಸಹಾಯ";
		AppStrings.Attendance = "ಅಟೆಂಡೆನ್ಸ್";
		AppStrings.MinutesofMeeting = "ಸಭೆಯ ನಿಮಿಷಗಳ";
		AppStrings.savings = "ಉಳಿತಾಯ";
		AppStrings.InternalLoanDisbursement = "ಸಾಲ ವಿತರಣೆ";
		AppStrings.expenses = "ವೆಚ್ಚಗಳಿಗೆ";
		AppStrings.cashinhand = "ಕೈಯಲ್ಲಿ ನಗದು :  ₹ ";
		AppStrings.cashatBank = "ನಗದು ಬ್ಯಾಂಕಿನ :  ₹ ";
		AppStrings.memberloanrepayment = "ಸದಸ್ಯ ಸಾಲದ ಮರುಪಾವತಿಯ";
		AppStrings.grouploanrepayment = "ಗುಂಪು ಸಾಲದ ಮರುಪಾವತಿಯ";
		AppStrings.income = "ಆದಾಯ";
		AppStrings.bankTransaction = "ಬ್ಯಾಂಕ್ ವ್ಯವಹಾರ";
		AppStrings.fixedDeposit = "ಸ್ಥಿರ ಠೇವಣಿ";
		AppStrings.otherincome = "ಇತರೆ ಆದಾಯ";
		AppStrings.subscriptioncharges = "ಚಂದಾದಾರಿಕೆ";
		AppStrings.penalty = "ದಂಡದ";
		AppStrings.donation = "ಕೊಡುಗೆ";
		AppStrings.federationincome = "ಒಕ್ಕೂಟ ಆದಾಯ";
		AppStrings.date = "ದಿನಾಂಕ";
		AppStrings.passwordchange = "ಗುಪ್ತಪದವನ್ನು ಬದಲಿಸಿ";
		AppStrings.listofexpenses = "ವೆಚ್ಚಗಳಿಗೆ ಪಟ್ಟಿ";
		AppStrings.transport = "ಸಾರಿಗೆ";
		AppStrings.snacks = "ಚಹಾ";
		AppStrings.telephone = "ದೂರವಾಣಿ";
		AppStrings.stationary = "ಸ್ಥಾಯಿ";
		AppStrings.otherExpenses = "ಇತರ ವೆಚ್ಚಗಳನ್ನು";
		AppStrings.federationIncomePaid = "ಒಕ್ಕೂಟ ಆದಾಯ (ಹಣ)";
		AppStrings.bankInterest = "ಬ್ಯಾಂಕ್ ಬಡ್ಡಿ";
		AppStrings.bankExpenses = "ಬ್ಯಾಂಕ್ ವೆಚ್ಚಗಳಿಗೆ";
		AppStrings.bankrepayment = "ಬ್ಯಾಂಕ್ ಮರುಪಾವತಿಯ";
		AppStrings.total = "ಒಟ್ಟು";
		AppStrings.summary = "ಸಾರಾಂಶ";
		AppStrings.savingsSummary = "ಉಳಿತಾಯ ಸಾರಾಂಶ";
		AppStrings.loanSummary = "ಸಾಲ ಸಾರಾಂಶ";
		AppStrings.lastMonthReport = "ಮಾಸಿಕ ವರದಿ";
		AppStrings.Memberreports = "ಸದಸ್ಯ ವರದಿಗಳು";
		AppStrings.GroupReports = "ಗುಂಪು ವರದಿಗಳು";
		AppStrings.transactionsummary = "ಬ್ಯಾಂಕ್ ವ್ಯವಹಾರ ಸಾರಾಂಶ";
		AppStrings.balanceSheet = "ಆಯವ್ಯಯ";
		AppStrings.balanceSheetHeader = "ಆಯವ್ಯಯ ಮೇಲೆ";
		AppStrings.trialBalance = "ಪರೀಕ್ಷೆ ಬ್ಯಾಲೆನ್ಸ್";
		AppStrings.outstanding = "ಅತ್ಯುತ್ತಮ";
		AppStrings.totalSavings = "ಒಟ್ಟು ಉಳಿತಾಯಗಳು";
		AppStrings.groupSavingssummary = "ಗುಂಪು ಉಳಿತಾಯ ಸಾರಾಂಶ";
		AppStrings.groupLoansummary = "ಗುಂಪು ಸಾಲ ಸಾರಾಂಶ";
		AppStrings.amount = "ಮೊತ್ತ";
		AppStrings.principleAmount = "ಮೊತ್ತ";
		AppStrings.interest = "ಆಸಕ್ತಿ";
		AppStrings.groupLoan = "ಗುಂಪು ಸಾಲ";
		AppStrings.balance = "ಬ್ಯಾಲೆನ್ಸ್";
		AppStrings.Name = "ಹೆಸರು";
		AppStrings.Repaymenybalance = "ಬ್ಯಾಲೆನ್ಸ್";
		AppStrings.details = "ವಿವರಗಳು";
		AppStrings.payment = "ಪಾವತಿ";
		AppStrings.receipt = "ರಸೀತಿ";
		AppStrings.FromDate = "ದಿನಾಂಕದಿಂದ";
		AppStrings.ToDate = "ಇಲ್ಲಿಯವರೆಗೆ";
		AppStrings.fromDate = "ರಿಂದ";
		AppStrings.toDate = "ಗೆ";
		AppStrings.OldPassword = "ಹಳೆಯ ಪಾಸ್ವರ್ಡ್";
		AppStrings.NewPassword = "ಹೊಸ ಪಾಸ್ವರ್ಡ್";
		AppStrings.ConfirmPassword = "ಗುಪ್ತಪದವನ್ನು ಖಚಿತಪಡಿಸಿ";
		AppStrings.agentProfile = "ಏಜೆಂಟ್ ಪ್ರೊಫೈಲ್";
		AppStrings.groupProfile = "ಗುಂಪು ಪ್ರೊಫೈಲ್";
		AppStrings.bankDeposit = "ಬ್ಯಾಂಕ್ ಠೇವಣಿ";
		AppStrings.withdrawl = "ನಿವರ್ತನ";
		AppStrings.OutstandingAmount = "ಅತ್ಯುತ್ತಮ";
		AppStrings.GroupLogin = "ಗುಂಪು ಲಾಗಿನ್";
		AppStrings.AgentLogin = "ಏಜೆಂಟ್ ಲಾಗಿನ್";
		AppStrings.AboutLicense = "ಪರವಾನಗಿ";
		AppStrings.contacts = "ಸಂಪರ್ಕಗಳು";
		AppStrings.contactNo = "ಸಂಪರ್ಕ ಸಂಖ್ಯೆ";
		AppStrings.cashFlowStatement = "ಕ್ಯಾಶ್ ಫ್ಲೋ ಹೇಳಿಕೆ";
		AppStrings.IncomeExpenditure = "ಆದಾಯ ವೆಚ್ಚ";
		AppStrings.transactionCompleted = "ವ್ಯವಹಾರ ಮುಗಿದ";
		AppStrings.loginAlert = "ಬಳಕೆದಾರ ಹೆಸರು ಮತ್ತು ಪಾಸ್ವರ್ಡ್ ತಪ್ಪಾಗಿದೆ";
		AppStrings.loginUserAlert = "ಬಳಕೆದಾರ ವಿವರಗಳನ್ನು ಒದಗಿಸುವಲ್ಲಿ";
		AppStrings.loginPwdAlert = "ಪಾಸ್ವರ್ಡ್ ವಿವರಗಳನ್ನು";
		AppStrings.pwdChangeAlert = "ಪಾಸ್ವರ್ಡ್ ಅನ್ನು ಯಶಸ್ವಿಯಾಗಿ ಬದಲಾಯಿಸಲಾಗಿದೆ";
		AppStrings.pwdIncorrectAlert = "ಸರಿಯಾದ ಪಾಸ್ವರ್ಡ್ ನಮೂದಿಸಿ";
		AppStrings.groupRepaidAlert = "ನಿಮ್ಮ ಅತ್ಯುತ್ತಮ ಮೊತ್ತವನ್ನು ಪರಿಶೀಲಿಸಿ";// "ನಿಮ್ಮ MOTHTHAVANNU ಪರಿಶೀಲಿಸಿ";
		AppStrings.cashinHandAlert = "ಕೈಯಲ್ಲಿ ನಗದು ಪರಿಶೀಲಿಸಿ";
		AppStrings.cashatBankAlert = "ಬ್ಯಾಂಕ್ನಲ್ಲಿ ನಗದು ಪರಿಶೀಲಿಸಿ";
		AppStrings.nullAlert = "ನಗದು ವಿವರಗಳನ್ನು";
		AppStrings.DepositnullAlert = "ನಗದು ವಿವರಗಳನ್ನು";
		AppStrings.MinutesAlert = "ಮೇಲಿನಿಂದ ಯಾರಾದರೂ ಆಯ್ಕೆ ಮಾಡಿ";
		AppStrings.afterDate = "ದಿನಾಂಕವನ್ನು ಆರಿಸಿ ಈ ದಿನಾಂಕದ ನಂತರ";
		AppStrings.last5Transaction = "ಕಳೆದ ಐದು ವ್ಯವಹಾರ";
		AppStrings.internetError = "ನಿಮ್ಮ ಇಂಟರ್ನೆಟ್ ಸಂಪರ್ಕವನ್ನು ಪರಿಶೀಲಿಸಿ";
		AppStrings.lastTransactionDate = "ಕೊನೆಯ ವ್ಯವಹಾರ ದಿನಾಂಕ";
		AppStrings.yes = "ಸರಿ";
		AppStrings.credit = "ಕ್ರೆಡಿಟ್";
		AppStrings.debit = "ಡೆಬಿಟ್";
		AppStrings.memberName = "ಸದಸ್ಯ ಹೆಸರು";
		AppStrings.OutsatndingAmt = "ಅತ್ಯುತ್ತಮ";
		AppStrings.calFromToDateAlert = "ಎರಡೂ ದಿನಾಂಕ ಆಯ್ಕೆ";
		AppStrings.selectedDate = "ಆಯ್ಕೆ ದಿನಾಂಕ";
		AppStrings.calAlert = "ವಿವಿಧ ದಿನಾಂಕ ಆಯ್ಕೆ";
		AppStrings.dialogMsg = "ನೀವು ಈ ದಿನಾಂಕ ಮುಂದುವರಿಸಲು ಬಯಸುತ್ತೀರಾ";
		AppStrings.dialogOk = "ಹೌದು";
		AppStrings.dialogNo = "ಯಾವುದೇ";

		AppStrings.aboutYesbooks = "EShakti ಅಥವಾ ಎಸ್.ಹೆಚ್.ಜಿಗಳ ಗಣಕೀಕರಣ ನಮ್ಮ ಗೌರವಾನ್ವಿತ ಪ್ರಧಾನಿ ಹೇಳಿಕೆ, ಸಾಲಿನಲ್ಲಿ ಆಫ್ ನಬಾರ್ಡ್ ಮೈಕ್ರೋ ಕ್ರೆಡಿಟ್ ಮತ್ತು ಇನ್ನೋವೇಷನ್ಸ್ ಇಲಾಖೆ ವಿಚಾರ 'ನಾವು ಎಲೆಕ್ಟ್ರಾನಿಕ್ ಡಿಜಿಟಲ್ ಭಾರತ ಕನಸಿನ ಸರಿಸಲು ...'. ಡಿಜಿಟಲ್ ಭಾರತ ಸರ್ಕಾರಿ ಇಲಾಖೆಗಳು ಮತ್ತು ಭಾರತದ ಜನರು ಸಂಯೋಜಿಸಲು ಮತ್ತು ಪರಿಣಾಮಕಾರಿ ಆಡಳಿತ ಖಚಿತಪಡಿಸಿಕೊಳ್ಳಲು ಭಾರತ ಸರ್ಕಾರದ ರೂ 1.13 ಲಕ್ಷ ಕೋಟಿ ಉಪಕ್ರಮ. ಇದು ಡಿಜಿಟಲ್ ಅಧಿಕಾರವನ್ನು ಸಮಾಜ ಮತ್ತು ಜ್ಞಾನ ಆರ್ಥಿಕತೆಗೆ ಭಾರತ ರೂಪಾಂತರ ಆಗಿದೆ.";

		AppStrings.groupList = "ಆಯ್ದ ಗುಂಪಿನ";// GROUP LIST
		AppStrings.login = "ಲಾಗಿನ್";
		AppStrings.submit = "ಸಲ್ಲಿಸಲು";
		AppStrings.edit = "ಸಂಪಾದಿಸಿ";
		AppStrings.bankBalance = "ಬ್ಯಾಂಕ್ ಬ್ಯಾಲೆನ್ಸ್";
		AppStrings.balanceAsOn = "ಎಎಸ್ ಸಮತೋಲನ : ";
		AppStrings.savingsBook = " ಉಳಿತಾಯ ಖಾತೆ ಪರ್ ಪುಸ್ತಕ : ";
		AppStrings.savingsBank = "  ಉಳಿತಾಯ ಪ್ರತಿ ಬ್ಯಾಂಕ್ ಖಾತೆಗೆ : ";
		AppStrings.difference = " ವ್ಯತ್ಯಾಸವನ್ನು : ";
		AppStrings.loanOutstandingBook = "ಪ್ರತಿ ಪುಸ್ತಕ ಅತ್ಯುತ್ತಮ : ";
		AppStrings.loanOutstandingBank = " ಪ್ರತಿ ಬ್ಯಾಂಕ್ ಅತ್ಯುತ್ತಮ : ";
		AppStrings.values = new String[] { "ಸ್ಥಳೀಯ (ವಿಲೇಜ್)",

				"ಎಲ್ಲಾ ಸದಸ್ಯರು ಇರುತ್ತವೆ",

				"ಉಳಿತಾಯ ಪ್ರಮಾಣದ ಸಂಗ್ರಹಿಸಿದ",

				"ಬಗ್ಗೆ ವೈಯಕ್ತಿಕ ಸಾಲ ವಿತರಿಸಿ ಚರ್ಚಿಸಲಾಗಿದೆ",

				"ಮರುಪಾವತಿಯ ಪ್ರಮಾಣವನ್ನು ಮತ್ತು ಕಲೆಕ್ಟೆಡ್",

				"ತರಬೇತಿ ಬಗ್ಗೆ ಚರ್ಚಿಸಲಾಗಿದೆ",

				"ಬ್ಯಾಂಕ್ ಸಾಲ ಪಡೆಯಲು ನಿರ್ಧರಿಸಿದ್ದಾರೆ",

				"ಸಾಮಾನ್ಯ ಅಭಿಪ್ರಾಯ ಚರ್ಚಿಸಲಾಗಿದೆ",

				"ಶುಚಿತ್ವ ಬಗ್ಗೆ ಚರ್ಚಿಸಲಾಗಿದೆ",

				"ಶಿಕ್ಷಣ ಪ್ರಾಮುಖ್ಯತೆಯ ಬಗ್ಗೆ ಚರ್ಚಿಸಲಾಗಿದೆ" };

		AppStrings.January = "ಜನವರಿ";
		AppStrings.February = "ಫೆಬ್ರವರಿ";
		AppStrings.March = "ಮಾರ್ಚ್";
		AppStrings.April = "ಏಪ್ರಿಲ್";
		AppStrings.May = "ಮೇ";
		AppStrings.June = "ಜೂನ್";
		AppStrings.July = "ಜುಲೈ";
		AppStrings.August = "ಆಗಸ್ಟ್";
		AppStrings.September = "ಸೆಪ್ಟೆಂಬರ್";
		AppStrings.October = "ಅಕ್ಟೋಬರ್";
		AppStrings.November = "ನವೆಂಬರ್";
		AppStrings.December = "ಡಿಸೆಂಬರ್";
		AppStrings.uploadPhoto = "ಅಪ್ಲೋಡ್ ಫೋಟೋ";
		AppStrings.Il_Disbursed = "ಆಂತರಿಕ ಸಾಲ ಪಾವತಿಸಲಾಗುತ್ತದೆ";
		AppStrings.IL_Loan = "ಆಂತರಿಕ ಸಾಲ";
		AppStrings.Il_Repaid = "ಆಂತರಿಕ ಸಾಲ ಮರುಪಾವತಿ";
		AppStrings.offlineTransactionCompleted = "ನಿಮ್ಮ ಆಫ್ಲೈನ್ ವ್ಯವಹಾರ ಯಶಸ್ವಿಯಾಗಿ ಉಳಿಸಲಾಗಿದೆ";
		AppStrings.offlineTransactionreports = "ಆಫ್ಲೈನ್ ವ್ಯವಹಾರ ವರದಿಗಳನ್ನು";
		AppStrings.offlineTransaction = "ಆಫ್ಲೈನ್ ವ್ಯವಹಾರ";
		AppStrings.disconnectInternet = "ಆಫ್ಲೈನ್ ಸೇವೆ ಮುಂದುವರಿಸಲು ನಿಮ್ಮ ಇಂಟರ್ನೆಟ್ ಡಿಸ್ಕನೆಕ್ಟ್ ಮಾಡಿ";
		AppStrings.Dashboard = "ಡ್ಯಾಶ್ಬೋರ್ಡ್";
		AppStrings.bankTransactionSummary = "ಬ್ಯಾಂಕ್ ವ್ಯವಹಾರ ಸಾರಾಂಶ";
		AppStrings.BL_Disbursed = "ಪಾವತಿಸಲಾಗುತ್ತದೆ";
		AppStrings.BL_Repaid = "ಮರುಪಾವತಿ";
		AppStrings.auditing = "ಆಡಿಟಿಂಗ್";
		AppStrings.training = "ತರಬೇತಿ";
		AppStrings.auditor_Name = "ಆಡಿಟರ್ ಹೆಸರು";
		AppStrings.auditing_Date = "ಆಡಿಟಿಂಗ್ ದಿನಾಂಕ";
		AppStrings.training_Date = "ತರಬೇತಿ ದಿನಾಂಕ";
		AppStrings.auditing_Period = "ಲೆಕ್ಕಪರಿಶೋಧನೆ ಅವಧಿಯನ್ನು ಆರಿಸಿ";
		AppStrings.training_types = new String[] { "ಆರೋಗ್ಯ ಅರಿವು", "ಪುಸ್ತಕ ಕೀಪರ್ ಜಾಗೃತಿ", "ಜೀವನಾಧಾರ ಜಾಗೃತಿ",
				"ಸಾಮಾಜಿಕ ಜಾಗೃತಿ", "ಶೈಕ್ಷಣಿಕ ಜಾಗೃತಿ" };
		AppStrings.changeLanguage = "ಭಾಷೆ ಬದಲಾಯಿಸಿ";
		AppStrings.interestRepayAmount = "ಪ್ರಸ್ತುತ ಬಾಕಿಯಿರುವ";
		AppStrings.interestRate = "ಬಡ್ಡಿ ದರ";
		AppStrings.savingsAmount = "ಉಳಿತಾಯ ಮೊತ್ತ";
		AppStrings.noGroupLoan_Alert = "ಯಾವುದೇ ಗುಂಪು ಸಾಲ ಲಭ್ಯವಾಗುವಂತೆ.";
		AppStrings.confirmation = "ದೃಢೀಕರಣ";
		AppStrings.logOut = "ಲಾಗ್ ಔಟ್";
		AppStrings.fromDateAlert = "ದಿನಾಂಕದಿಂದ ದಯವಿಟ್ಟು";
		AppStrings.toDateAlert = "ಇಲ್ಲಿಯವರೆಗೆ ದಯವಿಟ್ಟು";
		AppStrings.auditingDateAlert = "ಆಡಿಟಿಂಗ್ ಡೇಟ್ ಮಾಡಿ";
		AppStrings.auditorAlert = "ಆಡಿಟರ್ ಹೆಸರನ್ನು ದಯವಿಟ್ಟು";
		AppStrings.nullDetailsAlert = "ಎಲ್ಲಾ ವಿವರಗಳನ್ನು";
		AppStrings.trainingDateAlert = "ತರಬೇತಿ ಡೇಟ್";
		AppStrings.trainingTypeAlert = "ಕನಿಷ್ಠ ಒಂದು ತರಬೇತಿ ಪ್ರಕಾರವನ್ನು ಆರಿಸಿ";
		AppStrings.offline_ChangePwdAlert = "ನೀವು ಇಂಟರ್ನೆಟ್ ಇಲ್ಲದೆ ಪಾಸ್ವರ್ಡ್ ಬದಲಿಸಬಹುದು";
		AppStrings.transactionFailAlert = "ನಿಮ್ಮ ವ್ಯವಹಾರ ವಿಫಲ";
		AppStrings.voluntarySavings = "ಸ್ವಯಂಪ್ರೇರಿತ ಉಳಿತಾಯ";
		AppStrings.tenure = "ಅಧಿಕಾರಾವಧಿಯಲ್ಲಿನ";
		AppStrings.purposeOfLoan = "ಸಾಲದ ಕಾರಣ";
		AppStrings.chooseLabel = "ಸಾಲದ ಆಯ್ಕೆ";
		AppStrings.Tenure_POL_Alert = "ಸಾಲ ಮತ್ತು ಅಧಿಕಾರಾವಧಿಯಲ್ಲಿ ಅವಧಿಯ ಉದ್ದೇಶಕ್ಕಾಗಿ ನಮೂದಿಸಿ";
		AppStrings.interestSubvention = "ಬಡ್ಡಿ ಸಹಾಯಧನ";
		AppStrings.adminAlert = "ನಿಮ್ಮ ನಿರ್ವಹಣೆ ಸಂಪರ್ಕಿಸಿ";
		AppStrings.userNotExist = "ಲಾಗಿನ್ ವಿವರಗಳನ್ನು ಅಸ್ತಿತ್ವದಲ್ಲಿಲ್ಲ. ಆನ್ಲೈನ್ನಲ್ಲಿ ಲಾಗಿನ್ ಮುಂದುವರಿಸಿ ದಯವಿಟ್ಟು.";
		AppStrings.tryLater = "ನಂತರ ಪ್ರಯತ್ನಿಸಿ";
		AppStrings.offlineDataAvailable = "ಆಫ್ಲೈನ್ ಲಭ್ಯವಿದೆ ಡೇಟಾ. ಲಾಗ್ಔಟ್ ಮತ್ತು ನಂತರ ಪ್ರಯತ್ನಿಸಿ.";
		AppStrings.choosePOLAlert = "ಸಾಲದ ಕಾರಣ ಆಯ್ಕೆ";
		AppStrings.InternalLoan = "ಆಂತರಿಕ ಸಾಲ";
		AppStrings.TrainingTypes = "ತರಬೇತಿ ರೀತಿಯ";
		AppStrings.uploadInfo = "ಸದಸ್ಯ ವಿವರಗಳು";
		AppStrings.uploadAadhar = "ಅಪ್ಲೋಡ್ ಆಧಾರ್";
		AppStrings.month = "-- ತಿಂಗಳ ಆಯ್ಕೆ --";
		AppStrings.year = "-- ವರ್ಷದ ಆಯ್ಕೆ --";
		AppStrings.monthYear_Alert = "ತಿಂಗಳು ವರ್ಷದ ಆಯ್ಕೆ ಮಾಡಿ.";
		AppStrings.chooseLanguage = "ಭಾಷೆಯನ್ನು ಆಯ್ಕೆ";
		AppStrings.autoFill = "ಆಟೋ ಫಿಲ್";
		AppStrings.viewAadhar = "ವೀಕ್ಷಿಸಿ ಆಧಾರ್";
		AppStrings.viewPhoto = "ನೋಟ ಫೋಟೋ";
		AppStrings.uploadAlert = "ನೀವು ಆಧಾರ್ ಮತ್ತು ಫೋಟೋ ಇಂಟರ್ನೆಟ್ ಸಂಪರ್ಕವಿಲ್ಲದೆ ಅಪ್ಲೋಡ್ ಮಾಡಬಹುದು";
		AppStrings.upload = "ಅಪ್ಲೋಡ್";
		AppStrings.view = "ನೋಟ";
		AppStrings.showAadharAlert = "ನಿಮ್ಮ ಆಧಾರ್ ಕಾರ್ಡ್ ತೋರಿಸು";
		AppStrings.deactivateAccount = "ಖಾತೆಯನ್ನು ನಿಷ್ಕ್ರಿಯಗೊಳಿಸು";
		AppStrings.deactivateAccount_SuccessAlert = "ನಿಮ್ಮ ಖಾತೆಯನ್ನು ಯಶಸ್ವಿಯಾಗಿ ನಿಷ್ಕ್ರಿಯಗೊಳಿಸಲಾಗಿದೆ";
		AppStrings.deactivateAccount_FailAlert = "ನಿಮ್ಮ ಖಾತೆಯನ್ನು ನಿಷ್ಕ್ರಿಯಗೊಳಿಸಲು ವಿಫಲವಾಗಿದೆ";
		AppStrings.deactivateNetworkAlert = "ನೀವು ಇಂಟರ್ನೆಟ್ ಸಂಪರ್ಕವನ್ನು ಇಲ್ಲದೆ ನಿಮ್ಮ ಖಾತೆಯನ್ನು ನಿಷ್ಕ್ರಿಯಗೊಳಿಸಲು";
		AppStrings.offlineReports = "ಆಫ್ಲೈನ್ ವರದಿಗಳು";
		AppStrings.registrationSuccess = "ನೀವು ಯಶಸ್ವಿಯಾಗಿ ನೋಂದಣಿ";
		AppStrings.reg_MobileNo = "ನೋಂದಾಯಿತ ಮೊಬೈಲ್ ಸಂಖ್ಯೆ ನಮೂದಿಸಿ";
		AppStrings.reg_IMEINo = "ನಿರಾಕರಿಸಿದರು.";
		AppStrings.reg_BothNo = "ನಿರಾಕರಿಸಿದರು.";
		AppStrings.reg_IMEIUsed = "ನಿರಾಕರಿಸಿದರು. ಸಾಧನ ಅಸಾಮರಸ್ಯವು";
		AppStrings.signInAsDiffUser = "ವಿವಿಧ ಬಳಕೆದಾರ ಸೈನ್ ಇನ್";
		AppStrings.incorrectUserNameAlert = "ತಪ್ಪಾದ ಬಳಕೆದಾರ ಹೆಸರು";

		AppStrings.noofflinedatas = "ಆಫ್ಲೈನ್ ವ್ಯವಹಾರ ಪ್ರವೇಶ";
		AppStrings.uploadprofilealert = "ನಿಮ್ಮ ಸ್ವಂತ ಅಪ್ಲೋಡ್ ವಿವರಗಳು";
		AppStrings.monthYear_Warning = "ದಯವಿಟ್ಟು ಸರಿಯಾದ ತಿಂಗಳು ವರ್ಷದ ಆಯ್ಕೆ";
		AppStrings.of = " ಆಫ್";
		AppStrings.previous_DateAlert = "ಆಯ್ಕೆ ಹಿಂದಿನ ದಿನಾಂಕ";
		AppStrings.nobanktransactionreportdatas = "ಬ್ಯಾಂಕ್ ವ್ಯವಹಾರ ವರದಿಗಳನ್ನು ಡಾಟಾ";

		AppStrings.mDefault = "ಡೀಫಾಲ್ಟ್";
		AppStrings.mTotalDisbursement = "ಒಟ್ಟು ಸಾಲದಲ್ಲಿ";
		AppStrings.mInterloan = "ವೈಯಕ್ತಿಕ ಸಾಲದ ಮರುಪಾವತಿಯ";
		AppStrings.mTotalSavings = "ಒಟ್ಟು ಸಂಗ್ರಹಣೆಗಳು";
		AppStrings.mCommonNetworkErrorMsg = "ನಿಮ್ಮ ನೆಟ್ವರ್ಕ್ ಸಂಪರ್ಕವನ್ನು ಪರಿಶೀಲಿಸಿ";
		AppStrings.mLoginAlert_ONOFF = "ದಯವಿಟ್ಟು ಮತ್ತೆ ಲಾಗಿನ್";
		AppStrings.changeLanguageNetworkException = "ಲಾಗ್ಔಟ್ ಮಾಡಿ, ಮತ್ತೆ ಲಾಗಿನ್ ಇಂಟರ್ನೆಟ್ ಸಂಪರ್ಕವನ್ನು ಬಳಸಿಕೊಂಡು ಪ್ರಯತ್ನಿಸಿ";
		AppStrings.photouploadmsg = "ಫೋಟೋವನ್ನು ಯಶಸ್ವಿಯಾಗಿ ಅಪ್ಲೋಡ್";
		AppStrings.loginAgainAlert = "ಹಿನ್ನೆಲೆ ಡೇಟಾ ಇನ್ನೂ ಲೋಡ್ ಮಾಡಿ ಕೆಲವು ಸಮಯ ನಿರೀಕ್ಷಿಸಿ.";// "ದಯವಿಟ್ಟು ಮತ್ತೆ ಲಾಗಿನ್
																								// AAGI";

		/* Interloan Disbursement menu */
		AppStrings.mInternalInterloanMenu = "ಆಂತರಿಕ ಸಾಲ";
		AppStrings.mInternalBankLoanMenu = "ಬ್ಯಾಂಕ್ ಸಾಲ";
		AppStrings.mInternalMFILoanMenu = "MFI ಸಾಲ";
		AppStrings.mInternalFederationMenu = "ಫೆಡರೇಶನ್ ಸಾಲ";

		AppStrings.bankName = "ಬ್ಯಾಂಕ್ ಹೆಸರು";
		AppStrings.mfiname = "MFI ಹೆಸರು";
		AppStrings.loanType = "ಸಾಲದ ಬಗೆಯ";
		AppStrings.bankBranch = "ಬ್ಯಾಂಕ್ ಶಾಖೆ";
		AppStrings.installment_type = "ಕಂತು ಕೌಟುಂಬಿಕತೆ";
		AppStrings.NBFC_Name = "NBFC ಹೆಸರು";
		AppStrings.Loan_Account_No = "ಸಾಲ ಖಾತೆ ಸಂಖ್ಯೆ";
		AppStrings.Loan_Sanction_Amount = "ಸಾಲದ ಮಂಜೂರಾತಿ ಮೊತ್ತ";
		AppStrings.Loan_Sanction_Date = "ಸಾಲದ ಮಂಜೂರಾತಿ ದಿನಾಂಕ";
		AppStrings.Loan_Disbursement_Amount = "ಸಾಲ ವಿತರಣೆ ಮೊತ್ತ";

		AppStrings.Loan_Disbursement_Date = "ಸಾಲ ವಿತರಣೆ ದಿನಾಂಕ";
		AppStrings.Loan_Tenure = "ಸಾಲದ ಅವಧಿ";
		AppStrings.Subsidy_Amount = "ಸಬ್ಸಿಡಿ ಮೊತ್ತವ";
		AppStrings.Subsidy_Reserve_Fund = "ಸಬ್ಸಿಡಿ ರಿಸರ್ವ್ ಮೊತ್ತ";
		AppStrings.Interest_Rate = "ಬಡ್ಡಿ ದರ";

		AppStrings.mMonthly = "ಮಾಸಿಕ";
		AppStrings.mQuarterly = "ತ್ರೈಮಾಸಿಕ";
		AppStrings.mHalf_Yearly = "ಅರ್ಧ ವಾರ್ಷಿಕ";
		AppStrings.mYearly = "ವಾರ್ಷಿಕ";
		AppStrings.mAgri = "ಕೃಷಿ";
		AppStrings.mMSME = "ಎಂಎಸ್ಎಂಇ";
		AppStrings.mOthers = "ಇತರರ";

		AppStrings.mMFI_NAME_SPINNER = "MFI ಹೆಸರು";
		AppStrings.mMFINabfins = "NABFINS";
		AppStrings.mMFIOthers = "ಇತರರ";

		AppStrings.mInternalTypeLoan = "ಆಂತರಿಕ ಸಾಲ";

		AppStrings.mPervious = "ಹಿಂದಿನ";
		AppStrings.mNext = "ಮುಂದಿನ";
		AppStrings.mPasswordUpdate = "ಯಶಸ್ವಿಯಾಗಿ ನಿಮ್ಮ ಪಾಸ್ವರ್ಡ್ ಅಪ್ಡೇಟ್ಗೊಳಿಸಲಾಗಿದೆ";

		AppStrings.mExpense_meeting = "ಸಭೆಯಲ್ಲಿ ವೆಚ್ಚದ";

		AppStrings.mFixedDepositeAmount = "ಸ್ಥಿರ ಠೇವಣಿ ಮೊತ್ತ";
		AppStrings.mFedBankName = "ಫೆಡರೇಶನ್ ಹೆಸರು";// "BANK/NBFC NAME";
		AppStrings.mLoanSanc_loanDist = "ದಯವಿಟ್ಟು ಸಾಲ ವಿತರಣೆ MOTTHAVANNU ಪರಿಶೀಲಿಸಿ";

		AppStrings.mSubmitbutton = "ಸರಿ";

		AppStrings.mStepWise_LoanDibursement = "ಆಂತರಿಕ ಸಾಲ ವಿತರಣೆ";
		AppStrings.mNewUserSignup = "ನೋಂದಣಿ";
		AppStrings.mMobilenumber = "ಮೊಬೈಲ್ ನಂಬ";
		AppStrings.mIsAadharAvailable = "ಆಧಾರ್ ಮಾಹಿತಿ ಅಪ್ಡೇಟ್ಗೊಳಿಸಲಾಗಿLLA";
		AppStrings.mSavingsTransaction = "ಉಳಿತಾಯ ವ್ಯವಹಾರ";
		AppStrings.mBankCharges = "ಬ್ಯಾಂಕ್ ಶುಲ್ಕಗಳು";
		AppStrings.mCashDeposit = "ನಗದು ಠೇವಣಿ";
		AppStrings.mLimit = "ಸಾಲ MITHI";

		AppStrings.mAccountToAccountTransfer = "ಖಾತೆ ವರ್ಗಾವಣೆ ಖಾತೆಯನ್ನು";
		AppStrings.mTransferFromBank = "ಬ್ಯಾಂಕಿನಿಂದ";
		AppStrings.mTransferToBank = "ಬ್ಯಾಂಕ್";
		AppStrings.mTransferAmount = "ವರ್ಗಾಯಿಸಿ";
		AppStrings.mTransferCharges = "ವರ್ಗಾವಣೆ ಆರೋಪಗಳನ್ನು";
		AppStrings.mTransferSpinnerFloating = "ಬ್ಯಾಂಕ್";
		AppStrings.mTransferNullToast = "ಎಲ್ಲಾ ವಿವರಗಳನ್ನು ಒದಗಿಸಿ";
		AppStrings.mAccToAccTransferToast = "ನೀವು ಕೇವಲ ಒಂದು ಬ್ಯಾಂಕ್ ಖಾತೆಯನ್ನು ಹೊಂದಿರಬೇಕು";

		/* Loan Account */
		AppStrings.mLoanaccHeader = "ಸಾಲದ ಖಾತೆ";
		AppStrings.mLoanaccCash = "ನಗದು";
		AppStrings.mLoanaccBank = "ಬ್ಯಾಂಕ್";
		AppStrings.mLoanaccWithdrawal = "ವಿತ್ಡ್ರಾವಲ್";
		AppStrings.mLoanaccExapenses = "ವೆಚ್ಚಗಳಿಗೆ";
		AppStrings.mLoanaccSpinnerFloating = "ಸಾಲದ ಖಾತೆಗೆ ಬ್ಯಾಂಕ್";
		AppStrings.mLoanaccCash_BankToast = "ನಗದು ಅಥವಾ ಬ್ಯಾಂಕ್ ಆಯ್ಕೆಮಾಡಿ";
		AppStrings.mLoanaccNullToast = "ಎಲ್ಲಾ ವಿವರಗಳನ್ನು ಒದಗಿಸಿ";
		AppStrings.mLoanaccBankNullToast = "ಆಯ್ಕೆ ಬ್ಯಾಂಕ್ ಹೆಸರು ದಯವಿಟ್ಟು";

		AppStrings.mBankTransSavingsAccount = "ಉಳಿತಾಯ ಖಾತೆ";
		AppStrings.mBankTransLoanAccount = "ಸಾಲದ ಖಾತೆ";

		AppStrings.mLoanAccType = "ಕೌಟುಂಬಿಕತೆ";
		AppStrings.mLoanAccBankAmountAlert = "ಬ್ಯಾಂಕ್ ಬ್ಯಾಲೆನ್ಸ್ ಪರಿಶೀಲಿಸಿ";

		AppStrings.mCashAtBank = "ನಗದು ಬ್ಯಾಂಕಿನ";
		AppStrings.mCashInHand = "ಕೈಯಲ್ಲಿ ನಗದು";
		AppStrings.mShgSeedFund = "ಸ್ವಸಹಾಯ ಸೀಡ್ ನಿಧಿ";
		AppStrings.mFixedAssets = "ಸ್ಥಿರ ಆಸ್ತಿ";
		AppStrings.mVerified = "ಪರಿಶೀಲಿಸಿದ";
		AppStrings.mConfirm = "ಖಚಿತಪಡಿಸಲು";
		AppStrings.mProceed = "ಮುಂದುವರೆಯಲು";
		AppStrings.mDialogAlertText = "ನಿಮ್ಮ ಆರಂಭಿಕ ಸಮತೋಲನ datas ಈಗ ನೀವು ಮುಂದುವರಿಯುವ ಯಶಸ್ವಿಯಾಗಿ ನವೀಕರಿಸಲಾಗುತ್ತದೆ";
		AppStrings.mBalanceSheetDate = "ಆಯವ್ಯಯ ಅಪ್ಲೋಡ್ ಮಾಡಲಾಗಿದೆ";
		AppStrings.mCheckMonthlyEntries = "ಹಿಂದಿನ ತಿಂಗಳು ಯಾವುದೇ ನಮೂದುಗಳನ್ನು, ಆದ್ದರಿಂದ ನೀವು ಹಿಂದಿನ ತಿಂಗಳು datas ತರಬೇಕಿರುವ";
		AppStrings.mTermLoanOutstanding = "ಅತ್ಯುತ್ತಮ";
		AppStrings.mCashCreditOutstanding = "ಅತ್ಯುತ್ತಮ";
		AppStrings.mGroupLoanOutstanding = "ಗುಂಪು ಸಾಲದ ಬಾಕಿ";
		AppStrings.mMemberLoanOutstanding = "ಸದಸ್ಯ ಸಾಲದ ಬಾಕಿ";
		AppStrings.mContinueWithDate = "ಈ ದಿನಾಂಕದ ಬದಲಾಯಿಸಲು ಬಯಸುತ್ತೀರಿ";
		AppStrings.mCheckList = "ಪಟ್ಟಿ ಪರಿಶೀಲಿಸಿ";
		AppStrings.mMicro_Credit_Plan = "ಸೂಕ್ಷ್ಮ ಸಾಲ ಯೋಜನೆ";
		AppStrings.mAadharCard_Invalid = "ನಿಮ್ಮ ಆಧಾರ್ ಕಾರ್ಡ್ ಅಮಾನ್ಯವಾಗಿದೆ";
		AppStrings.mIsNegativeOpeningBalance = "ಋಣಾತ್ಮಕ ಸಮತೋಲನ ನಿವಾರಿಸಿಕೊಳ್ಳಲು";
		AppStrings.mLoginDetailsDelete = "ಹಿಂದಿನ ಲಾಗಿನ್ ವಿವರಗಳನ್ನು ಅಳಿಸಲ್ಪಡುತ್ತದೆ. ನೀವು ಡೇಟಾವನ್ನು ಅಳಿಸಲು ಬಯಸುವಿರಾ?";
		AppStrings.mVerifyGroupAlert = "ಪರಿಶೀಲಿಸಲು ನೀವು ಆನ್ಲೈನ್ ಹೊಂದಿರಬೇಕು";
		AppStrings.next = "ಮುಂದಿನ";
		AppStrings.mNone = "ಯಾವುದೂ";
		AppStrings.mSelect = "ಆಯ್ಕೆ";
		AppStrings.mAnimatorName = "ಅನಿಮೇಟರ್ ಹೆಸರು";
		AppStrings.mSavingsAccount = "ಉಳಿತಾಯ ಖಾತೆ";
		AppStrings.mAccountNumber = "ಖಾತೆ ಸಂಖ್ಯೆ";
		AppStrings.mLoanAccountAlert = "ಅಲ್ಲ ಆಫ್ಲೈನ್ ಕ್ರಮದಲ್ಲಿ ಸಾಲದ ಖಾತೆಗೆ ವರ್ಗಾಯಿಸುತ್ತದೆ";
		AppStrings.mSeedFund = "ಬೀಜ ನಿಧಿ";
		AppStrings.mLoanTypeAlert = "ಸಾಲ ಖಾತೆ ಕೌಟುಂಬಿಕತೆ ಆಯ್ಕೆ ಮಾಡಿ";
		AppStrings.mOutstandingAlert = "ನಿಮ್ಮ ಬಾಕಿ ಮೊತ್ತದ ಪರಿಶೀಲಿಸಿ";
		AppStrings.mLoanAccTransfer = "ಸಾಲದ ಖಾತೆಗೆ ವರ್ಗಾವಣೆ";
		AppStrings.mLoanName = "ಸಾಲ ಹೆಸರು";
		AppStrings.mLoanId = "ಸಾಲ ಐಡಿ";
		AppStrings.mLoanDisbursementDate = "ಸಾಲ ವಿತರಣೆ ದಿನಾಂಕ";
		AppStrings.mAdd = "ಸೇರಿಸು";
		AppStrings.mLess = "ಕಡಿಮೆ";
		AppStrings.mRepaymentWithInterest = "ಮರುಪಾವತಿ (ಆಸಕ್ತಿಯಿಂದ)";
		AppStrings.mCharges = "ಆರೋಪಗಳನ್ನು";
		AppStrings.mExistingLoan = "ಅಸ್ತಿತ್ವದಲ್ಲಿರುವ ಸಾಲದ";
		AppStrings.mNewLoan = "ಹೊಸ ಸಾಲ";
		AppStrings.mIncreaseLimit = "ಹೆಚ್ಚಳ ಮಿತಿಯನ್ನು";
		AppStrings.mAvailableLimit = "ಲಭ್ಯವಿರುವ ಮಿತಿಯನ್ನು";
		AppStrings.mDisbursementDate = "ವಿತರಣೆ ದಿನಾಂಕ";
		AppStrings.mDisbursementAmount = "ವಿತರಣೆ ಪ್ರಮಾಣವನ್ನು";
		AppStrings.mSanctionAmount = "ಸಮ್ಮತಿ ಪ್ರಮಾಣದ";
		AppStrings.mBalanceAmount = "ಬಾಕಿ ಮೊಬಲಗನ್ನು";
		AppStrings.mMemberDisbursementAmount = "ಸದಸ್ಯ ವಿತರಣೆ ಪ್ರಮಾಣವನ್ನು";
		AppStrings.mLoanDisbursementFromLoanAcc = "ಸಾಲ ಖಾತೆಯಿಂದ ಸಾಲ ವಿತರಣೆ";
		AppStrings.mLoanDisbursementFromSbAcc = "SB ಖಾತೆಯಿಂದ ಸಾಲ ವಿತರಣೆ";
		AppStrings.mRepaid = "ಮರುಪಾವತಿ";
		AppStrings.mCheckBackLog = "ಬಾಕಿ ಪರಿಶೀಲಿಸಿ";
		AppStrings.mTarget = "ಗುರಿ";
		AppStrings.mCompleted = "ಪೂರ್ಣಗೊಂಡಿತು";
		AppStrings.mPending = "ಬಾಕಿ";
		AppStrings.mAskLogout = "ನೀವು ಲಾಗ್ ಔಟ್ ಬಯಸುತ್ತೀರಿ ?.";
		AppStrings.mCheckDisbursementAlert = "ದಯವಿಟ್ಟು ನಿಮ್ಮ ಮಂಜೂರಾತಿಯ ಪ್ರಮಾಣದ ಪರಿಶೀಲಿಸಿ";
		AppStrings.mCheckbalanceAlert = "ದಯವಿಟ್ಟು ನಿಮ್ಮ ಸಮತೋಲನ ಪ್ರಮಾಣವನ್ನು ಪರಿಶೀಲಿಸಿ";
		AppStrings.mVideoManual = "ವೀಡಿಯೊವನ್ನು ಹಸ್ತಚಾಲಿತ";
		AppStrings.mPdfManual = "ಪಿಡಿಎಫ್ ಕೈಪಿಡಿ";
		AppStrings.mAmountDisbursingAlert = "ನೀವು ಸದಸ್ಯರಿಗೆ Disbursing ಮುಂದುವರಿಯಲು ಬಯಸುತ್ತೀರಿ ?.";
		AppStrings.mOpeningDate = "ತೆರೆಯುವ ದಿನಾಂಕ";
		AppStrings.mCheckLoanSancDate_LoanDisbDate = "ದಯವಿಟ್ಟು ನಿಮ್ಮ ಸಾಲದ ಮಂಜೂರಾತಿ ದಿನಾಂಕ ಮತ್ತು ಸಾಲ ವಿತರಣೆ ದಿನಾಂಕವನ್ನು ಪರಿಶೀಲಿಸಿ.";
		AppStrings.mLoanDisbursementFromRepaid = "ಸಿ.ಸಿ. ಸಾಲ ಹಿಂಪಡೆಯುವಿಕೆ";
		AppStrings.mExpense = "ವೆಚ್ಚಗಳು";
		AppStrings.mBeforeLoanPay = "ಸಾಲ ಪಾವತಿಗೆ ಮುನ್ನ";
		AppStrings.mLoans = "ಸಾಲಗಳು";
		AppStrings.mSurplus = "ಹೆಚ್ಚುವರಿ";
		AppStrings.mPOLAlert = "ದಯವಿಟ್ಟು ಸಾಲದ ಉದ್ದೇಶವನ್ನು ನಮೂದಿಸಿ";
		AppStrings.mCheckLoanAmounts = "ದಯವಿಟ್ಟು ನಿಮ್ಮ ಸಾಲದ ಅನುಮೋದನೆ ಮೊತ್ತ ಮತ್ತು ಸಾಲ ವಿತರಣೆ ಮೊತ್ತವನ್ನು ಪರಿಶೀಲಿಸಿ";
		AppStrings.mCheckLoanSanctionAmount = "ನಿಮ್ಮ ಸಾಲದ ಅನುಮೋದನೆ ಮೊತ್ತವನ್ನು ನಮೂದಿಸಿ";
		AppStrings.mCheckLoanDisbursementAmount = "ನಿಮ್ಮ ಸಾಲದ ವಿತರಣೆ ಮೊತ್ತವನ್ನು ನಮೂದಿಸಿ";
		AppStrings.mIsPasswordSame = "ದಯವಿಟ್ಟು ನಿಮ್ಮ ಹಳೆಯ ಪಾಸ್ವರ್ಡ್ ಅನ್ನು ಪರಿಶೀಲಿಸಿ ಮತ್ತು ಹೊಸ ಪಾಸ್ವರ್ಡ್ ಒಂದೇ ಆಗಿರಬಾರದು.";
		AppStrings.mSavings_Banktransaction = "ಉಳಿತಾಯ - ಬ್ಯಾಂಕ್ ವ್ಯವಹಾರ";
		AppStrings.mCheckBackLogOffline = "ಆಫ್ಲೈನ್ನಲ್ಲಿ ಬ್ಯಾಕಪ್ ಅನ್ನು ಪರಿಶೀಲಿಸಲು ಸಾಧ್ಯವಿಲ್ಲ";
		AppStrings.mCredit = "ಕ್ರೆಡಿಟ್";
		AppStrings.mDebit = "ಡೆಬಿಟ್";
		AppStrings.mCheckFixedDepositAmount = "ದಯವಿಟ್ಟು ನಿಮ್ಮ ಸ್ಥಿರ ಠೇವಣಿ ಮೊತ್ತವನ್ನು ಪರಿಶೀಲಿಸಿ.";
		AppStrings.mCheckDisbursementDate = "ದಯವಿಟ್ಟು ನಿಮ್ಮ ಸಾಲದ ವಿತರಣಾ ದಿನಾಂಕವನ್ನು ಪರಿಶೀಲಿಸಿ";
		AppStrings.mSendEmail = "EMAIL";
		AppStrings.mTotalInterest = "ಒಟ್ಟು ಆಸಕ್ತಿ";
		AppStrings.mTotalCharges = "ಒಟ್ಟು ಆರೋಪಗಳು";
		AppStrings.mTotalRepayment = "ಒಟ್ಟು ಮರುಪಾವತಿ";
		AppStrings.mTotalInterestSubventionRecevied = "ಒಟ್ಟು ಬಡ್ಡಿ ಸಬ್ವೆನ್ಷನ್ ಸ್ವೀಕರಿಸಲಾಗಿದೆ";
		AppStrings.mTotalBankCharges = "ಒಟ್ಟು ಬ್ಯಾಂಕ್ ಶುಲ್ಕಗಳು";
		AppStrings.mDone = "DONE";
		AppStrings.mPayment = "ಪಾವತಿ";
		AppStrings.mCashWithdrawal = "ಹಣ ತೆಗೆಯುವದು";
		AppStrings.mFundTransfer = "ಹಣ ವರ್ಗಾವಣೆ";
		AppStrings.mDepositAmount = "ಠೇವಣಿ ಮೊತ್ತ";
		AppStrings.mWithdrawalAmount = "ಹಿಂತೆಗೆದುಕೊಳ್ಳುವ ಮೊತ್ತ";
		AppStrings.mTransactionMode = "ವಹಿವಾಟು ಮೋಡ್";
		AppStrings.mBankLoanDisbursement = "ಬ್ಯಾಂಕ್ ಸಾಲ ವಿತರಣೆ";
		AppStrings.mMemberToMember = "ಸದಸ್ಯರಿಂದ ಇನ್ನೊಬ್ಬ ಸದಸ್ಯರಿಗೆ";
		AppStrings.mDebitAccount = "ಡೆಬಿಟ್ ಖಾತೆ";
		AppStrings.mSHG_SavingsAccount = "shg ಉಳಿತಾಯ ಖಾತೆ";
		AppStrings.mDestinationMemberName = "ಗಮ್ಯಸ್ಥಾನ ಸದಸ್ಯ ಹೆಸರು";
		AppStrings.mSourceMemberName = "ಮೂಲ ಸದಸ್ಯ ಹೆಸರು";
		AppStrings.mMobileNoUpdation = "ಮೊಬೈಲ್ ಸಂಖ್ಯೆ ಅಪ್ಡೇಟ್";
		AppStrings.mMobileNo = "ಮೊಬೈಲ್ ನಂಬರ";
		AppStrings.mMobileNoUpdateSuccessAlert = "ಮೊಬೈಲ್ ಸಂಖ್ಯೆ ಯಶಸ್ವಿಯಾಗಿ ನವೀಕರಿಸಲಾಗಿದೆ.";
		AppStrings.mAadhaarNoUpdation = "ಆಧಾರ್ ಸಂಖ್ಯೆ ಉಪದಿನ್";
		AppStrings.mSHGAccountNoUpdation = "ಶಾ ಖಾತೆ ಸಂಖ್ಯೆ ಸಂಖ್ಯೆಗಳು";
		AppStrings.mAccountNoUpdation = "ಸದಸ್ಯ ಖಾತೆ ಸಂಖ್ಯೆ ನವೀಕರಣ";
		AppStrings.mAadhaarNo = "ಆಧಾರ್ ಸಂಖ್ಯೆ ಒಂದು";
		AppStrings.mBranchName = "ಶಾಖೆಯ ಹೆಸರು";
		AppStrings.mAadhaarNoUpdateSuccessAlert = "ಆಧಾರ್ ಸಂಖ್ಯೆ ಯಶಸ್ವಿಯಾಗಿ ನವೀಕರಿಸಿದೆ";
		AppStrings.mMemberAccNoUpdateSuccessAlert = "ಸದಸ್ಯ ಖಾತೆ ಸಂಖ್ಯೆ ಯಶಸ್ವಿಯಾಗಿ ನವೀಕರಿಸಲಾಗಿದೆ";
		AppStrings.mSHGAccNoUpdateSuccessAlert = "ಎಸ್ಎಚ್ಜಿ ಖಾತೆ ಸಂಖ್ಯೆ ಯಶಸ್ವಿಯಾಗಿ ನವೀಕರಿಸಲಾಗಿದೆ";
		AppStrings.mCheckValidMobileNo = "ದಯವಿಟ್ಟು ಮಾನ್ಯ ಮೊಬೈಲ್ ಸಂಖ್ಯೆಯನ್ನು ನಮೂದಿಸಿ";
		AppStrings.mInvalidAadhaarNo = "ಅಮಾನ್ಯವಾದ ಆಧಾರ್ ಸಂಖ್ಯೆ";
		AppStrings.mCheckValidAadhaarNo = "ದಯವಿಟ್ಟು ಮಾನ್ಯವಾದ ಆಧಾರ್ ಸಂಖ್ಯೆಯನ್ನು ನಮೂದಿಸಿ";
		AppStrings.mInvalidAccountNo = "ಅಮಾನ್ಯವಾದ ಖಾತೆ ಸಂಖ್ಯೆ";
		AppStrings.mCheckAccountNumber = "ದಯವಿಟ್ಟು ನಿಮ್ಮ ಖಾತೆ ಸಂಖ್ಯೆಯನ್ನು ಪರಿಶೀಲಿಸಿ";
		AppStrings.mInvalidMobileNo = "ಅಮಾನ್ಯವಾದ ಮೊಬೈಲ್ ಸಂಖ್ಯೆ";
		AppStrings.mUploadScheduleAlert = "CAN'T UPLOAD SCHEDULE VI IN OFFLINE MODE";
		AppStrings.mIsMobileNoRepeat = "ಮೊಬೈಲ್ ಸಂಖ್ಯೆಗಳನ್ನು ಪುನರಾವರ್ತಿಸಲಾಗಿದೆ";
		AppStrings.mMobileNoUpdationNetworkCheck = "ಮೊಬೈಲ್ ಸಂಖ್ಯೆಯನ್ನು ಆಫ್ಲೈನ್ ಮೋಡ್ನಲ್ಲಿ ನವೀಕರಿಸಲು ಸಾಧ್ಯವಿಲ್ಲ";
		AppStrings.mAadhaarNoNetworkCheck = "ಆಧಾರ್ ಸಂಖ್ಯೆಯನ್ನು ಆಫ್ಲೈನ್ ಮೋಡ್ನಲ್ಲಿ ನವೀಕರಿಸಲು ಸಾಧ್ಯವಿಲ್ಲ";
		AppStrings.mShgAccNoNetworkCheck = "ಆಫ್ಲೈನ್ ಮೋಡ್ನಲ್ಲಿ SHG ಖಾತೆ ಸಂಖ್ಯೆಯನ್ನು ನವೀಕರಿಸಲು ಸಾಧ್ಯವಿಲ್ಲ";
		AppStrings.mAccNoNetworkCheck = "ಆಫ್ಲೈನ್ ಮೋಡ್ನಲ್ಲಿ ಸದಸ್ಯ ಖಾತೆ ಸಂಖ್ಯೆಯನ್ನು ನವೀಕರಿಸಲು ಸಾಧ್ಯವಿಲ್ಲ";
		AppStrings.mAccountNoRepeat = "ಖಾತೆ ಸಂಖ್ಯೆಗಳು ಪುನರಾವರ್ತಿತವಾಗಿದೆ";
		AppStrings.mCheckBankAndBranchNameSelected = "ದಯವಿಟ್ಟು ಬ್ಯಾಂಕ್ ಮತ್ತು ಅನುಗುಣವಾದ ಖಾತೆ ಸಂಖ್ಯೆಗಾಗಿ ಆಯ್ಕೆ ಮಾಡಿದ ಶಾಖೆ ಹೆಸರನ್ನು ಪರಿಶೀಲಿಸಿ";
		AppStrings.mCheckNewLoanSancDate = "ದಯವಿಟ್ಟು ಸಾಲ ಅನುಮೋದನೆ ದಿನಾಂಕವನ್ನು ಆಯ್ಕೆಮಾಡಿ";
		AppStrings.mCreditLinkageInfo = "ಕ್ರೆಡಿಟ್ ಲಿಂಕೇಜ್ ಮಾಹಿತಿ";
		AppStrings.mCreditLinkage = "ಕ್ರೆಡಿಟ್ ಲಿಂಕ್";
		AppStrings.mCreditLinkage_content = "Number of times bank loans were taken by the SHG and repaid completely.";
		AppStrings.mCreditLinkageAlert = "ಆಫ್ಲೈನ್ ಮೋಡ್ನಲ್ಲಿ ಕ್ರೆಡಿಟ್ ಲಿಂಕ್ ಮಾಹಿತಿಯನ್ನು ನವೀಕರಿಸಲು ಸಾಧ್ಯವಿಲ್ಲ";
	}

}
