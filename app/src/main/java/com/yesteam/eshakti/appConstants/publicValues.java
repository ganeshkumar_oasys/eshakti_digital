package com.yesteam.eshakti.appConstants;

public class publicValues {
	public static String[] mBankCheckLabel = null;
	public static String[] mFixedCheckLabel = null;

	public static String[] mMemberloanCheckLabel = null;
	public static String[] mGrouploanCheckLabel = null;

	public static String mOffline_Trans_CurrentDate = null;

	public static String mMFILoanType = null;
	public static String mBANKLoanType = null;
	public static String mBankList = null;
	public static String mBankBranchList = null;
	public static String mFEDLoanType = null;
	public static String mFedName = null;
	public static String mSelectedInfoGroupResponse = null;
	public static String mSelectedGRoupId = null;

	public static String mLoanAccBankListWebservices = null;

	public static String mMasterEditOpeningBalanceValues = null;
	public static String mMasterEditOpeningBalance_UpdateValues = null;
	public static String mMaster_SendtoserverValues = null;

	public static String mEdit_OB_Sendtoserver_Savings = null;
	public static String mEdit_OB_Sendtoserver_InternalLoan = null;
	public static String mEdit_OB_Sendtoserver_Bank_MemberLoan = null;
	public static String mEdit_OB_Sendtoserver_Bank_Group_Loan = null;

	public static int mBankLoanSize_BankLoan = 0;
	public static int mCurrentBankLoanSize_BankLoan = 0;

	public static int mBankLoanSize_GroupLoan = 0;
	public static int mCurrentBankLoanSize_GroupLoan = 0;

	public static String mEdit_OB_Sendtiserver_Bank_CashinHandDetails = null;

	public static String mCheckListWebserviceValues = null;

	public static String mGetMinutesLanguageValues = null;

	public static String mGetSbAccountNumber = null;
	public static String mGetLoanAccountNumber = null;
	public static String mGetLoanAccTransFromSB = null;

	public static String mGetLoanBalnceValues = null;

	public static String mGetAnimatorPendingGroupwiseValues = null;

	public static String mGetSHGResultionValues = null;

	public static String mGetAlertMessageValues = null;

	public static String mGetMemberMobileNumberValues = null;

	public static String mGetMemberAadhaarNumberValues = null;

	public static String mGetShgAccountNumberValues = null;

	public static String mGetBankBranchValues = null;

	public static String mGetMemberAccountNumberValues = null;
	
	public static String mTransAuditVerifiedValues = null;
}
