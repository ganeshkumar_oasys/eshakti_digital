package com.yesteam.eshakti.appConstants;

public class SOAP_OperationConstants {

	public static final String OPERATION_NAME_TRAINERS_LOGIN = "Trainers_Login";
	public static final String OPERATION_NAME_GROUP_LOGIN = "UserLogin";
	public static final String OPERATION_NAME_VIEWGROUPS = "View_groups";
	public static final String OPERATION_NAME_SELECTED_GROUPS = "selected_groups";
	public static final String OPERATION_NAME_GET_MEMBER_SAVINGS = "GetMember_Savings";
	public static final String OPERATION_NAME_GET_MEMBER_LOAN_REPAID = "GetMember_Loan_repaid";
	public static final String OPERATION_NAME_GET_PERSONAL_LOAN_REPAID = "Get_Personal_loanRepaid";
	public static final String OPERATION_NAME_GET_ALL_MEM_OUTSTANDING = "Get_All_Mem_Outstanding_New";
	public static final String OPERATION_NAME_GET_LOAN_OUTSTANDING = "Get_Loan_outstanding";
	public static final String OPERATION_NAME_GET_AD = "Get_Add";
	public static final String OPERATION_NAME_GET_PL_DISBURSEMENT = "GetPersonal_Loan";
	public static final String OPERATION_NAME_GET_PL_INTEREST = "GetPersonal_Loan_Interest";
	public static final String OPERATION_NAME_FIXED_DEPOSIT_BALANCE = "Fixed_Deposit_Balance";
	public static final String OPERATION_NAME_MEMBER_SAVINGS_SUMMARY = "Member_savings_summery";
	public static final String OPERATION_NAME_MEMBER_LOAN_SUMMARY = "Member_Loan_summery";
	public static final String OPERATION_NAME_GET_MINUTES = "Get_Minutes_New";// "Get_Minutes";
	public static final String OPERATION_NAME_GET_TRAINING = "Get_Training_New";// "Get_Training";
	public static final String OPERATION_NAME_GET_OTHERINCOME = "Get_OtherIncome";
	public static final String OPERATION_NAME_GET_PENALTY = "Get_penality";
	public static final String OPERATION_NAME_GET_SUBSCRIPTION_CHARGES = "Get_Subscription_Charges";
	public static final String OPERATION_NAME_GET_DONATION = "Get_Donation";
	public static final String OPERATION_NAME_GET_FEDERATIONINCOME = "Get_FederationIncome";
	public static final String OPERATION_NAME_GET_ALL_LOAN_OUTSTANDING = "Get_All_Loan_Outstanding";
	public static final String OPERATION_NAME_BANK_SUMMARY = "Bank_summary";
	public static final String OPERATION_NAME_Get_GROUP_SAVING_DETAILS = "GetGroupSaving_details_New";// "GetGroupSaving_details";
	public static final String OPERATION_NAME_GROUP_LOAN_SUMMARY = "Group_Loan_summery";
	public static final String OPERATION_NAME_GET_BALANCESHEET = "GetBalanceSheet";
	public static final String OPERATION_NAME_GET_TRIALBALANCE = "GetTrailBalance";
	public static final String OPERATION_NAME_GET_EXPENSES = "GetExpenses";
	public static final String OPERATION_NAME_GET_LASTTRANSACTIONDATE = "GetLastTransactionDate";
	public static final String OPERATION_NAME_GET_BALANCE_DETAILS = "Get_Balance_Details";
	public static final String OPERATION_NAME_GET_FIXED_DEPOSIT = "Get_Fixed_Deposit";
	public static final String OPERATION_NAME_GET_MONTH_SUMMARY = "GetMonthSummary";
	public static final String OPERATION_NAME_GET_ALL_INFO = "Get_All_Info";
	public static final String OPERATION_NAME_GET_BANK_BALANCE = "Get_Bank_Balance";
	public static final String OPERATION_NAME_GETGROUPPROFILE = "GetGroupProfile";
	public static final String OPERATION_NAME_GETAGENTPROFILE = "GetAgentProfile";
	public static final String OPERATION_NAME_GET_AGENTCHANGEPASSWORD = "Get_AgentChangpassword";
	public static final String OPERATION_NAME_GET_CAHNGEPASSWORD = "Get_Changpassword";
	public static final String OPERATION_NAME_GET_GROUP_LOAN_REPAID = "GetGroup_Loan_repaid_New";
	public static final String OPERATION_NAME_GET_ATTENDANCE = "Get_Attendance_New";// "Get_Attendance";
	public static final String OPERATION_NAME_GET_MEMBER_VOLUNTARY_SAVINGS = "GetMember_VoluntarySavings";
	public static final String OPERATION_NAME_GET_PURPOSE_OF_LOAN = "Get_PurposeOfLoan";
	public static final String OPERATION_NAME_GET_GROUP_MINUTES = "Get_Group_Minutes";
	public static final String OPERATION_NAME_OFFLINE_MODE = "Offline_Mode";
	public static final String OPERTAION_NAME_GET_TR_ID = "Get_TrId";
	public static final String OPERTAION_NAME_GROUP_LOGIN = "Group_Login";
	public static final String OPERATION_NAME_GET_ALL_INFO_GROUP = "Get_All_Info_Group";
	public static final String OPERATION_NAME_MEMBER_AADHAR_INFO = "Member_Aadhar_Info";
	public static final String OPERATION_NAME_GET_IMAGE = "GetimageFile";
	public static final String OPERATION_NAME_GET_MEMBER_PHOTO = "Get_Member_Photo";
	public static final String OPERATION_NAME_GET_MEMBER_AADHAAR_INFO = "Get_Member_Aadhar_Info";
	public static final String OPERATION_NAME_REGISTRATION = "Registration";
	public static final String OPERATION_NAME_DEACTIVATE = "Deactivate";
	public static final String OPERATION_NAME_GET_AUDIT = "Get_Audit_New";// "Get_Audit";

	public static final String OPERATION_NAME_GET_MFI_TYPE = "GetMFIType";
	public static final String OPERATION_NAME_GET_BANKLOAN_TYPE = "GetBankLoanType";
	public static final String OPERATION_NAME_GET_BANKLIST = "GetBanklist";
	public static final String OPERATION_NAME_GET_ADDLOAN = "GetAddLoan";
	public static final String OPERATION_NAME_GET_BANKBRANCH = "GetBankBrachlist";
	public static final String OPERATION_NAME_GET_FEDTYPE = "GetFedType";
	public static final String OPERATION_NAME_GET_FEDNAME = "GetFedName";
	public static final String OPERATION_NAME_GET_LOGINCHOOSE = "Login";

	public static final String OPERATION_NAME_GET_GL_FIXED_BANKNAME = "Get_GL_Fixed_BankName";
	public static final String OPERATION_NAME_GET_GL_FIXED_DEPOSIT_WITHDRAWAL = "Get_GL_Fixed_Deposit_Withdrawal";
	public static final String OPERATION_NAME_GET_ACCOUNT_TRANSFER = "Get_Account_trans";
	public static final String OPERATION_NAME_GET_BANK_ACCOUNT_TRANSFER = "Get_Account_transaction";
	public static final String OPERATION_NAME_GET_LOGIN = "Login";
	public static final String OPERATION_NAME_GET_EDIT_OPENINGBALANCE = "Get_Edit_Openingbalance";
	public static final String OPERATION_NAME_GET_EDIT_OPENINGBALANCE_UPDATE = "Get_Edit_Openingbalance_Update";
	public static final String OPERATION_NAME_GET_CHECKLIST = "Get_Checklist";
	public static final String OPERATION_NAME_GET_MINUTES_LANGUAGE = "Get_Minutes_Language";
	public static final String OPERATION_NAME_GET_MICRO_CREDIT_PLAN = "Get_MicroCredit_Plan";
	public static final String OPERATION_NAME_GET_GROUP_MINUTES_NEW = "Get_Group_Minutes_New";

	public static final String OPERATION_NAME_GET_SB_ACCOUNT_NUMBER = "Get_SB_Account_Number";

	public static final String OPERATION_NAME_GET_LOAN_ACCOUNT_NUMBER = "Get_Loan_Account_Number";
	public static final String OPERATION_NAME_GET_LOAN_REPAID_FROMSB = "GetGroup_Loan_repaid_FromSB";

	public static final String OPERATION_NAME_GET_SEEDFUND = "Get_SeedFund";
	public static final String OPERATION_NAME_GET_ADDLOAN_NEW = "GetAddLoan_New";
	public static final String OPERATION_NAME_GET_ALL_INFO_GROUP_NEW = "Get_All_Info_Group_New";

	public static final String OPERATION_NAME_OFFLINE_MODE_NEW = "Offline_Mode_Final";
	public static final String OPERATION_NAME_GETADDLOAN_INSTALLMENT_BALANCE = "GetAddLoan_Installment_Balance";
	public static final String OPERATION_NAME_ANIMATOR_PENDING = "Get_Animator_Pending_Trans";
	public static final String OPERATION_NAME_GET_ADDLOAN_LIMIT = "GetAddLoan_Limit";
	public static final String OPERATION_NAME_GET_ADDLOAN_INSTALLMENT = "GetAddLoan_Installment";
	public static final String OPERATION_NAME_GET_ADDLOAN_INSTALLMENT_GROUP_ACCOUNT = "GetAddLoan_Installment_GroupAcc";
	public static final String OPERATION_NAME_GET_ALL_MEM_OUTSTANDING_FINAL = "Get_All_Mem_Outstanding_Final";

	public static final String OPERATION_NAME_GET_ANIMATOR_PENDING_GROUPWISE = "Get_Animator_Pending_Groupwise";

	public static final String OPERATION_NAME_GET_ADDLOAN_FINAL = "GetAddLoan_Final";
	public static final String OPERATION_NAME_GET_ADDLOAN_INSTALLMENT_CC = "GetAddLoan_Installment_CC";

	public static final String OPERATION_NAME_GET_MICRO_CREDIT_PLAN_NEW = "Get_MicroCredit_Plan_New";

	public static final String OPERATION_NAME_GET_SHG_RESOLUTION = "GetSHGResolution";
	public static final String OPERATION_NAME_GET_MESSAGE_NOTIFICATION = "GetMessage";

	public static final String OPERATION_NAME_GET_MEMBER_MOBILE_NUMBER = "GetMemberMobileNumber";
	public static final String OPERATION_NAME_UPDATE_MEMBER_MOBILE_NUMBER = "UpdateMemberMobileNumber";

	public static final String OPERATION_NAME_GET_MEMBER_AADHAAR_NUMBER = "GetMemberAadhaarNumber";
	public static final String OPERATION_NAME_UPDATE_MEMBER_AADHAAR_NUMBER = "UpdateMemberAadhaarNumber";

	public static final String OPERATION_NAME_GET_SHG_ACCOUNT_NUMBER = "GetSHGAccountNumber";
	public static final String OPERATION_NAME_UPDATE_SHG_ACCOUNT_NUMBER = "UpdateSHGAccountNumber";

	public static final String OPERATION_NAME_GET_BANK_BRANCH_NAME = "GetBankBranchName";

	public static final String OPERATION_NAME_GET_MEMBER_ACCOUNT_NUMBER = "GetMemberAccountNumber";

	public static final String OPERATION_NAME_UPDATE_MEMBER_ACCOUNT_NUMBER = "UpdateMemberAccountNumber";
	
	public static final String OPERATION_NAME_TRANSACTION_AUDIT_VERIFIED = "TRA_EditValues";
	public static final String OPERATION_NAME_TRANSACTION_AUDIT_VERIFIED_UPDATE = "TRA_EditValues_Update";
	public static final String OPERATION_NAME_GET_LOGIN_FINAL = "Login_Final";
	public static final String OPERATION_NAME_UPDATE_SHG_CREDIT_LINKAGE = "UpdateSHGCreditLinkage";

}
