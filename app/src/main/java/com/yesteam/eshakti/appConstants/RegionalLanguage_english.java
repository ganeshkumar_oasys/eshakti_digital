package com.yesteam.eshakti.appConstants;

public class RegionalLanguage_english {

	public RegionalLanguage_english() {

	}

	public static void RegionalStrings() {

		AppStrings.LoginScreen = "LOGIN SCREEN";
		AppStrings.userName = "MOBILE NUMBER";
		AppStrings.passWord = "PASSWORD";
		AppStrings.transaction = "TRANSACTION";
		AppStrings.reports = "REPORTS";
		AppStrings.profile = "PROFILE";
		AppStrings.meeting = "MEETING";
		AppStrings.settings = "SETTINGS";
		AppStrings.help = "HELP";
		AppStrings.Attendance = "ATTENDANCE";
		AppStrings.MinutesofMeeting = "MINUTES OF MEETING";
		AppStrings.savings = "SAVINGS";

		AppStrings.InternalLoanDisbursement = "INTERNAL LOAN DISBURSEMENT";// "LOAN
																			// DISBURSEMENT";

		AppStrings.expenses = "EXPENSES";
		AppStrings.cashinhand = "CASH IN HAND :  ₹ ";
		AppStrings.cashatBank = "CASH AT BANK :  ₹ ";
		AppStrings.memberloanrepayment = "MEMBER LOAN REPAYMENT";
		AppStrings.grouploanrepayment = "GROUP LOAN REPAYMENT";
		AppStrings.income = "INCOME";
		AppStrings.bankTransaction = "BANK TRANSACTION";
		AppStrings.fixedDeposit = "FIXED DEPOSIT";
		AppStrings.otherincome = "OTHER INCOME";
		AppStrings.subscriptioncharges = "SUBSCRIPTION";
		AppStrings.penalty = "PENALTY";
		AppStrings.donation = "DONATION";
		AppStrings.federationincome = "SUBSCRIPTION TO FEDS";
		AppStrings.date = "DATE";
		AppStrings.passwordchange = "CHANGE PASSWORD";
		AppStrings.listofexpenses = "LIST OF EXPENSES";
		AppStrings.transport = "TRAVELLING";
		AppStrings.snacks = "TEA";
		AppStrings.telephone = "TELEPHONE";
		AppStrings.stationary = "SUBSCRIPTION CHARGES";
		AppStrings.otherExpenses = "OTHER EXPENSES";
		AppStrings.federationIncomePaid = "FEDERATION INCOME(PAID)";
		AppStrings.bankInterest = "BANK INTEREST";
		AppStrings.bankExpenses = "BANK EXPENSES";
		AppStrings.bankrepayment = "BANK REPAYMENT";
		AppStrings.total = "TOTAL";
		AppStrings.summary = "SUMMARY";
		AppStrings.savingsSummary = "SAVINGS SUMMARY";
		AppStrings.loanSummary = "LOAN SUMMARY";
		AppStrings.lastMonthReport = "MONTHLY REPORT";
		AppStrings.Memberreports = "MEMBER REPORTS";
		AppStrings.GroupReports = "GROUP REPORTS";
		AppStrings.transactionsummary = "BANK TRANSACTION SUMMARY";
		AppStrings.balanceSheet = "BALANCE SHEET ";
		AppStrings.balanceSheetHeader = "BALANCE SHEET AS ON ";
		AppStrings.trialBalance = "TRIAL BALANCE";
		AppStrings.outstanding = "OUTSTANDING";
		AppStrings.totalSavings = "TOTAL SAVINGS";
		AppStrings.groupSavingssummary = "GROUP SAVINGS SUMMARY";
		AppStrings.groupLoansummary = "GROUP LOAN SUMMARY";
		AppStrings.amount = "AMOUNT";
		AppStrings.principleAmount = "AMOUNT";
		AppStrings.interest = "INTEREST";
		AppStrings.groupLoan = "GROUP LOAN";
		AppStrings.balance = "BALANCE";
		AppStrings.Name = "NAME";
		AppStrings.Repaymenybalance = "BALANCE";
		AppStrings.details = "DETAILS";
		AppStrings.payment = "PAYMENT";
		AppStrings.receipt = "RECEIPT";
		AppStrings.FromDate = "FROM DATE";
		AppStrings.ToDate = "TO DATE";
		AppStrings.fromDate = "FROM ";
		AppStrings.toDate = "TO ";
		AppStrings.OldPassword = "OLD PASSWORD";
		AppStrings.NewPassword = "NEW PASSWORD";
		AppStrings.ConfirmPassword = "CONFIRM PASSWORD";
		AppStrings.agentProfile = "ANIMATOR PROFILE";// "AGENT PROFILE";
		AppStrings.groupProfile = "GROUP PROFILE";
		AppStrings.bankDeposit = "BANK DEPOSIT";
		AppStrings.withdrawl = "WITHDRAWAL";
		AppStrings.OutstandingAmount = "OUTSTANDING";
		AppStrings.GroupLogin = "GROUP LOGIN";
		AppStrings.AgentLogin = "ANIMATOR LOGIN";// "AGENT LOGIN";
		AppStrings.AboutLicense = "LICENSE";
		AppStrings.contacts = "CONTACTS";
		AppStrings.contactNo = "CONTACT NO";
		AppStrings.cashFlowStatement = "CASH FLOW STATEMENT";
		AppStrings.IncomeExpenditure = "INCOME EXPENDITURE";
		AppStrings.transactionCompleted = "TRANSACTION COMPLETED";
		AppStrings.loginAlert = "USERNAME AND PASSWORD ARE INCORRECT";
		AppStrings.loginUserAlert = "PROVIDE THE USER DETAILS";
		AppStrings.loginPwdAlert = "PROVIDE THE PASSWORD DETAILS";
		AppStrings.pwdChangeAlert = "PASSWORD HAS BEEN SUCCESSFULLY CHANGED";
		AppStrings.pwdIncorrectAlert = "ENTER THE CORRECT PASSWORD";
		AppStrings.groupRepaidAlert = "CHECK YOUR OUTSTANDING AMOUNT";
		AppStrings.cashinHandAlert = "CHECK THE CASH IN HAND";
		AppStrings.cashatBankAlert = "CHECK THE CASH AT BANK";
		AppStrings.nullAlert = "PROVIDE THE CASH DETAILS";
		AppStrings.DepositnullAlert = "PROVIDE ALL THE CASH DETAILS";
		AppStrings.MinutesAlert = "PLEASE PICK ANYONE FROM THE ABOVE";
		AppStrings.afterDate = "PICK A DATE AFTER THIS DATE";
		AppStrings.last5Transaction = "LAST FIVE TRANSACTION";
		AppStrings.internetError = "PLEASE CHECK YOUR INTERNET CONNECTION";
		AppStrings.lastTransactionDate = "LAST TRANSACTION DATE";
		AppStrings.yes = "OK";
		AppStrings.credit = "CREDIT";
		AppStrings.debit = "DEBIT";
		AppStrings.memberName = "MEMBER NAME";
		AppStrings.OutsatndingAmt = "OUTSTANDING";
		AppStrings.calFromToDateAlert = "SELECT BOTH DATE'S";
		AppStrings.selectedDate = "SELECTED DATE";
		AppStrings.calAlert = "PICK DIFFERENT DATE";
		AppStrings.dialogMsg = "DO YOU WANT TO CONTINUE WITH THIS DATE";
		AppStrings.dialogOk = "YES";
		AppStrings.dialogNo = "NO";

		AppStrings.aboutYesbooks = "EShakti or Digitisation of SHGs is an initiative of Micro Credit and Innovations Department of NABARD in line with our Hon'ble PM statement,"
				+ "we move with the dream of electronic digital India...'. Digital India is a Rs 1.13-lakh crore initiative of Government of India to integrate the"
				+ " government departments and the people of India and to ensure effective governance. " + "It is to "
				+ " transform India into digital empowered society and knowledge economy";

		AppStrings.groupList = "SELECT GROUP";// GROUP LIST
		AppStrings.login = "   LOGIN   ";
		AppStrings.submit = "SUBMIT";
		AppStrings.edit = "EDIT";
		AppStrings.bankBalance = "BANK BALANCE";
		AppStrings.balanceAsOn = " BALANCE AS ON : ";
		AppStrings.savingsBook = " SAVINGS ACCOUNT AS PER BOOK : ";
		AppStrings.savingsBank = " SAVINGS ACCOUNT AS PER BANK : ";
		AppStrings.difference = " DIFFERENCE : ";
		AppStrings.loanOutstandingBook = " OUTSTANDING AS PER BOOK : ";
		AppStrings.loanOutstandingBank = " OUTSTANDING AS PER BANK : ";
		AppStrings.values = new String[] { "Native (Village).",

				"All Members are present.",

				"Collected the Savings amount.",

				"Discussed about disbursing the PersonalLoan.",

				"Collected the Repayment amount and Interest.",

				"Discussed about having the Training.",

				"Decided to get the BankLoan.",

				"Discussed about common views.",

				"Discussed about Cleanliness.",

				"Discussed about the imporatnce of Education." };

		AppStrings.January = "JANUARY";
		AppStrings.February = "FEBRUARY";
		AppStrings.March = "MARCH";
		AppStrings.April = "APRIL";
		AppStrings.May = "MAY";
		AppStrings.June = "JUNE";
		AppStrings.July = "JULY";
		AppStrings.August = "AUGUST";
		AppStrings.September = "SEPTEMBER";
		AppStrings.October = "OCTOBER";
		AppStrings.November = "NOVEMBER";
		AppStrings.December = "DECEMBER";
		AppStrings.uploadPhoto = "UPLOAD PHOTO";
		AppStrings.Il_Disbursed = "INTERNAL LOAN DISBURSED";
		AppStrings.IL_Loan = "INTERNAL LOAN";
		AppStrings.Il_Repaid = "INTERNAL LOAN REPAID";
		AppStrings.offlineTransactionCompleted = "YOUR OFFLINE TRANSACTION HAS BEEN SUCCESSFULLY SAVED";
		AppStrings.offlineTransactionreports = "OFFLINE TRANSACTION REPORTS";
		AppStrings.offlineTransaction = "OFFLINE TRANSACTIONS";
		AppStrings.disconnectInternet = "PLEASE DISCONNECT YOUR INTERNET TO CONTINUE OFFLINE SERVICE";
		AppStrings.Dashboard = "DASHBOARD";
		AppStrings.bankTransactionSummary = "BANK TRANSACTION SUMMARY";
		AppStrings.BL_Disbursed = "DISBURSED";
		AppStrings.BL_Repaid = "REPAID";
		AppStrings.auditing = "AUDITING";
		AppStrings.training = "TRAINING";
		AppStrings.auditor_Name = "AUDITOR NAME";
		AppStrings.auditing_Date = "AUDITING DATE";
		AppStrings.training_Date = "TRAINING DATE";
		AppStrings.auditing_Period = "CHOOSE THE AUDIT PERIOD";
		AppStrings.training_types = new String[] { "HEALTH AWARENESS", "BOOK KEEPER AWARENESS", "LIVELIHOOD AWARENESS",
				"SOCIAL AWARENESS", "EDUCATIONAL AWARENESS" };
		AppStrings.changeLanguage = "CHANGE LANGUAGE";
		AppStrings.interestRepayAmount = "CURRENT DUE";
		AppStrings.interestRate = "INTEREST RATE";
		AppStrings.savingsAmount = "SAVINGS AMOUNT";
		AppStrings.noGroupLoan_Alert = "NO GROUP LOAN AVAILABLE.";
		AppStrings.confirmation = "CONFIRMATION";
		AppStrings.logOut = "LOG OUT";
		AppStrings.fromDateAlert = "PLEASE PROVIDE FROM DATE";
		AppStrings.toDateAlert = "PLEASE PROVIDE TO DATE";
		AppStrings.auditingDateAlert = "PLEASE PROVIDE AUDITING DATE";
		AppStrings.auditorAlert = "PLEASE PROVIDE AUDITOR NAME";
		AppStrings.nullDetailsAlert = "PROVIDE ALL THE DETAILS";
		AppStrings.trainingDateAlert = "PROVIDE THE TRAINING DATE";
		AppStrings.trainingTypeAlert = "CHOOSE ATLEAST ONE TRAINING TYPE";
		AppStrings.offline_ChangePwdAlert = "YOU CAN'T CHANGE THE PASSWORD WITHOUT INTERNET.";
		AppStrings.transactionFailAlert = "YOUR TRANSACTION IS UNSUCCESSFULL.";
		AppStrings.voluntarySavings = "VOLUNTARY SAVINGS";
		AppStrings.tenure = "TENURE";
		AppStrings.purposeOfLoan = "PURPOSE OF LOAN";
		AppStrings.chooseLabel = "CHOOSE THE LOAN";
		AppStrings.Tenure_POL_Alert = "PLEASE ENTER THE PURPOSE OF LOAN AND TENURE PERIOD ";
		AppStrings.interestSubvention = "INTEREST SUBVENTION RECEVIED";
		AppStrings.adminAlert = "PLEASE CONTACT YOUR ADMIN.";
		AppStrings.userNotExist = "LOGIN DETAILS DOES NOT EXIST. PLEASE CONTINUE ONLINE LOGIN.";
		AppStrings.tryLater = "TRY LATER";
		AppStrings.offlineDataAvailable = "OFFLINE DATA AVIALABLE.PLEASE LOGOUT AND TRY AGIAN.";
		AppStrings.choosePOLAlert = "SELECT THE PURPOSE OF LOAN";
		AppStrings.InternalLoan = "INTERNAL LOAN";
		AppStrings.TrainingTypes = "TRAINING TYPES";
		AppStrings.uploadInfo = "MEMBER DETAILS";
		AppStrings.uploadAadhar = "UPLOAD AADHAAR";
		AppStrings.month = "-- Choose Month --";
		AppStrings.year = "-- Choose Year --";
		AppStrings.monthYear_Alert = "PLEASE SELECT THE MONTH AND YEAR.";
		AppStrings.chooseLanguage = "CHOOSE LANGUAGE";
		AppStrings.autoFill = "AUTO FILL";
		AppStrings.viewAadhar = "VIEW AADHAAR";
		AppStrings.viewPhoto = "VIEW PHOTO";
		AppStrings.uploadAlert = "YOU CAN'T VIEW THE AADHAAR AND PHOTO WITHOUT INTERNET CONNECTION";
		AppStrings.upload = "UPLOAD";
		AppStrings.view = "VIEW";
		AppStrings.showAadharAlert = "PLEASE SHOW YOUR AADHAAR CARD.";
		AppStrings.deactivateAccount = "DEACTIVATE ACCOUNT";
		AppStrings.deactivateAccount_SuccessAlert = "YOUR ACCOUNT HAS BEEN SUCCESSFULLY DEACTIVATED";
		AppStrings.deactivateAccount_FailAlert = "YOUR ACCOUNT HAS BEEN FAILED TO DEACTIVATE";
		AppStrings.deactivateNetworkAlert = "YOU CAN'T DEACTIVATE YOUR ACCOUNT WITHOUT INTERNET CONNECTION";
		AppStrings.offlineReports = "OFFLINE REPORTS";
		AppStrings.registrationSuccess = "YOU ARE REGISTERED SUCCESSFULLY";
		AppStrings.reg_MobileNo = "ENTER THE REGISTERED MOBILE NUMBER";
		AppStrings.reg_IMEINo = "ACCESS DENIED.";
		AppStrings.reg_BothNo = "ACCESS DENIED";
		AppStrings.reg_IMEIUsed = "ACCESS DENIED. DEVICE MISMATCH";
		AppStrings.signInAsDiffUser = "SIGN IN AS DIFFERENT USER";
		AppStrings.incorrectUserNameAlert = "INCORRECT USERNAME";

		AppStrings.noofflinedatas = "NO OFFLINE TRANSACTION ENTRY";
		AppStrings.uploadprofilealert = "UPLOAD YOUR OWN  DETAILS";
		AppStrings.monthYear_Warning = "PLEASE SELECT PROPER MONTH AND YEAR";
		AppStrings.of = " OF ";
		AppStrings.previous_DateAlert = "SELECT PREVIOUS DATE";
		AppStrings.nobanktransactionreportdatas = "NO BANKTRANSACTION REPORTS DATA";

		AppStrings.mDefault = "STEP WISE";
		AppStrings.mTotalDisbursement = "TOTAL DISBURSEMENT";
		AppStrings.mInterloan = "INTERNAL LOAN REPAYMENT";// "PERSONAL LOAN
															// REPAYMENT";
		AppStrings.mTotalSavings = "TOTAL COLLECTIONS";
		AppStrings.mCommonNetworkErrorMsg = "PLEASE CHECK YOUR NETWORK CONNECTION";
		AppStrings.mLoginAlert_ONOFF = "PLEASE LOGOUT AND LOGIN AGIAN";
		AppStrings.changeLanguageNetworkException = "PLEASE LOGOUT,TRY TO LOGIN AGIAN USING INTERNET CONNECTION";
		AppStrings.photouploadmsg = "PHOTO UPLOADED SUCCESSFULLY";
		AppStrings.loginAgainAlert = "BACKGROUND DATA IS STILL LOADING PLEASE WAIT FOR SOME TIME.";//"PLEASE LOGIN AGAIN";

		/* Interloan Disbursement menu */
		AppStrings.mInternalInterloanMenu = "INTERNAL LOAN";
		AppStrings.mInternalBankLoanMenu = "BANK LOAN";
		AppStrings.mInternalMFILoanMenu = "MFI LOAN";
		AppStrings.mInternalFederationMenu = "FEDERATION LOAN";

		AppStrings.bankName = "BANK NAME";
		AppStrings.mfiname = "MFI NAME";
		AppStrings.loanType = "LOAN TYPE";
		AppStrings.bankBranch = "BANK BRANCH";
		AppStrings.installment_type = "INSTALLMENT TYPE";
		AppStrings.NBFC_Name = "NBFC NAME";
		AppStrings.Loan_Account_No = "LOAN ACCOUNT NUMBER";
		AppStrings.Loan_Sanction_Amount = "LOAN SANCTION AMOUNT";
		AppStrings.Loan_Sanction_Date = "LOAN SANCTION DATE";
		AppStrings.Loan_Disbursement_Amount = "LOAN DISBURSEMENT AMOUNT";

		AppStrings.Loan_Disbursement_Date = "LOAN DISBURSEMENT DATE";
		AppStrings.Loan_Tenure = "LOAN TENURE";
		AppStrings.Subsidy_Amount = "SUBSIDY AMOUNT";
		AppStrings.Subsidy_Reserve_Fund = "SUBSIDY RESERVE AMOUNT";
		AppStrings.Interest_Rate = "INTEREST RATE";

		AppStrings.mMonthly = "MONTHLY";
		AppStrings.mQuarterly = "QUARTERLY";
		AppStrings.mHalf_Yearly = "HALF YEARLY";
		AppStrings.mYearly = "YEARLY";
		AppStrings.mAgri = "AGRI";
		AppStrings.mMSME = "MSME";
		AppStrings.mOthers = "OTHERS";

		AppStrings.mMFI_NAME_SPINNER = "MFI NAME";
		AppStrings.mMFINabfins = "NABFINS";
		AppStrings.mMFIOthers = "OTHERS";

		AppStrings.mInternalTypeLoan = "INTERNAL LOAN";

		AppStrings.mPervious = "PREVIOUS";
		AppStrings.mNext = "SKIP";
		AppStrings.mPasswordUpdate = "YOUR PASSWORD UPDATED SUCCESSFULLY";

		AppStrings.mExpense_meeting = "MEETING EXPENSES";

		AppStrings.mFixedDepositeAmount = "FIXED DEPOSIT AMOUNT";
		AppStrings.mFedBankName = "FEDERATION NAME";// "BANK/NBFC NAME";
		AppStrings.mLoanSanc_loanDist = "PLEASE CHECK LOAN DISBURSEMENT AMOUNT";

		AppStrings.mSubmitbutton = "OK";

		AppStrings.mStepWise_LoanDibursement = "INTERNAL LOAN DISBURSEMENT";
		AppStrings.mNewUserSignup = "REGISTRATION";
		AppStrings.mMobilenumber = "MOBILE NUMBER";
		AppStrings.mIsAadharAvailable = "AADHAAR INFORMATION NOT UPDATED";
		AppStrings.mSavingsTransaction = "SAVINGS TRANSACTION";
		AppStrings.mBankCharges = "BANK CHARGES";
		AppStrings.mCashDeposit = "CASH DEPOSIT";
		AppStrings.mLimit = "LIMIT";

		AppStrings.mAccountToAccountTransfer = "ACCOUNT TO ACCOUNT TRANSFER";
		AppStrings.mTransferFromBank = "FROM BANK";
		AppStrings.mTransferToBank = "TO BANK";
		AppStrings.mTransferAmount = "TRANSFER AMOUNT";
		AppStrings.mTransferCharges = "TRANSFER CHARGES";
		AppStrings.mTransferSpinnerFloating = "TO BANK";
		AppStrings.mTransferNullToast = "PLEASE PROVIDE ALL DETAILS";
		AppStrings.mAccToAccTransferToast = "YOU HAVE ONLY ONE BANK ACCOUNT";

		/* Loan Account */
		AppStrings.mLoanaccHeader = "LOAN ACCOUNT";
		AppStrings.mLoanaccCash = "CASH";
		AppStrings.mLoanaccBank = "BANK";
		AppStrings.mLoanaccWithdrawal = "WITHDRAWAL";
		AppStrings.mLoanaccExapenses = "EXPENSES";
		AppStrings.mLoanaccSpinnerFloating = "LOAN ACCOUNT BANK ";
		AppStrings.mLoanaccCash_BankToast = "PLEASE SELECT A CASH OR BANK";
		AppStrings.mLoanaccNullToast = "PLEASE PROVIDE ALL DETAILS";
		AppStrings.mLoanaccBankNullToast = "PLEASE SELECT A BANK NAME";

		AppStrings.mBankTransSavingsAccount = "SAVINGS ACCOUNT";
		AppStrings.mBankTransLoanAccount = "LOAN ACCOUNT";

		AppStrings.mLoanAccType = "TYPE";
		AppStrings.mLoanAccBankAmountAlert = "PLEASE CHECK BANK BALANCE";

		AppStrings.mCashAtBank = "CASH AT BANK";
		AppStrings.mCashInHand = "CASH IN HAND";
		AppStrings.mShgSeedFund = "SHG SEED FUND";
		AppStrings.mFixedAssets = "FIXED ASSETS";
		AppStrings.mVerified = "VERIFIED";
		AppStrings.mConfirm = "CONFIRM";
		AppStrings.mProceed = "PROCEED";
		AppStrings.mDialogAlertText = "YOUR OPENING BALANCE DATAS ARE UPDATED SUCCESSFULLY, NOW YOU CAN PROCEED";
		AppStrings.mBalanceSheetDate = "BALANCE SHEET HAS BEEN UPLOADED ON ";
		AppStrings.mCheckMonthlyEntries = "NO ENTRIES FOR PREVIOUS MONTH, SO YOU SHOULD BE PUT A DATAS FOR PREVIOUS MONTH";
		AppStrings.mTermLoanOutstanding = "OUTSTANDING";
		AppStrings.mCashCreditOutstanding = "OUTSTANDING";
		AppStrings.mGroupLoanOutstanding = "GROUP LOAN OUTSTANDING";
		AppStrings.mMemberLoanOutstanding = "MEMBER LOAN OUTSTANDING";
		AppStrings.mContinueWithDate = "DO YOU WANT TO CHANGE THIS DATE";
		AppStrings.mCheckList = "CHECK LIST";
		AppStrings.mMicro_Credit_Plan = "MICRO CREDIT PLAN";
		AppStrings.mAadharCard_Invalid = "YOUR AADHAAR CARD IS INVALID";
		AppStrings.mIsNegativeOpeningBalance = "RECTIFY NEGATIVE BALANCE";
		AppStrings.mLoginDetailsDelete = "PREVIOUS LOGIN DETAILS WILL BE DELETED. ARE YOU WANT TO DELETE DATA?";
		AppStrings.mVerifyGroupAlert = "YOU MUST BE ONLINE TO VERIFY";
		AppStrings.next = "NEXT";
		AppStrings.mNone = "NONE";
		AppStrings.mSelect = "SELECT";
		AppStrings.mAnimatorName = "ANIMATOR NAME";
		AppStrings.mSavingsAccount = "SAVINGS ACCOUNT";
		AppStrings.mAccountNumber = "ACCOUNT NUMBER";
		AppStrings.mLoanAccountAlert = "CAN'T TRANSFER LOAN ACCOUNT IN OFFLINE MODE";
		AppStrings.mSeedFund = "SEED FUND";
		AppStrings.mLoanTypeAlert = "PLEASE CHOOSE A LOAN ACCOUNT TYPE";
		AppStrings.mOutstandingAlert = "PLEASE CHECK YOUR OUTSTANDING AMOUNT";
		AppStrings.mLoanAccTransfer = "LOAN ACCOUNT TRANSFER";
		AppStrings.mLoanName = "LOAN NAME";
		AppStrings.mLoanId = "LOAN ID";
		AppStrings.mLoanDisbursementDate = "LOAN DISBURSEMENT DATE";
		AppStrings.mAdd = "ADD";
		AppStrings.mLess = "LESS";
		AppStrings.mRepaymentWithInterest = "REPAYMENT ( WITH INTEREST )";
		AppStrings.mCharges = "CHARGES";
		AppStrings.mExistingLoan = "EXISTING LOAN";
		AppStrings.mNewLoan = "NEW LOAN";
		AppStrings.mIncreaseLimit = "INCREASE LIMIT";
		AppStrings.mAvailableLimit = "AVAILABLE LIMIT";
		AppStrings.mDisbursementDate = "DISBURSEMENT DATE";
		AppStrings.mDisbursementAmount = "DISBURSEMENT AMOUNT";
		AppStrings.mSanctionAmount = "SANCTION AMOUNT";
		AppStrings.mBalanceAmount = "BALANCE AMOUNT";
		AppStrings.mMemberDisbursementAmount = "MEMBER DISBURSEMENT AMOUNT";
		AppStrings.mLoanDisbursementFromLoanAcc = "LOAN DISBURSEMENT FROM LOAN ACCOUNT";
		AppStrings.mLoanDisbursementFromSbAcc = "LOAN DISBURSEMENT FROM SB ACCOUNT";
		AppStrings.mRepaid = "REPAID";
		AppStrings.mCheckBackLog = "CHECK BACKLOG";
		AppStrings.mTarget = "TARGET";
		AppStrings.mCompleted = "COMPLETED";
		AppStrings.mPending = "PENDING";
		AppStrings.mAskLogout = "DO YOU WANT TO LOGOUT  ?.";
		AppStrings.mCheckDisbursementAlert = "PLEASE CHECK YOUR DISBURSEMENT AMOUNT";
		AppStrings.mCheckbalanceAlert = "PLEASE CHECK YOUR BALANCE AMOUNT";
		AppStrings.mVideoManual = "VIDEO MANUAL";
		AppStrings.mPdfManual = "PDF MANUAL";
		AppStrings.mAmountDisbursingAlert = "DO YOU WANT TO CONTINUE WITHOUT DISBURSING TO MEMBERS?.";
		AppStrings.mOpeningDate = "OPENING DATE";
		AppStrings.mCheckLoanSancDate_LoanDisbDate = "PLEASE CHECK YOUR LOAN SANCTION DATE AND LOAN DISBURSEMENT DATE.";
		AppStrings.mLoanDisbursementFromRepaid = "CC LOAN WITHDRAWAL";
		AppStrings.mExpense = "EXPENSE";
		AppStrings.mBeforeLoanPay = "BEFORE LOAN PAY";
		AppStrings.mLoans = "LOANS";
		AppStrings.mSurplus = "SURPLUS";
		AppStrings.mPOLAlert = "PLEASE ENTER THE PURPOSE OF LOAN";
		AppStrings.mCheckLoanAmounts = "PLEASE CHECK YOUR LOAN SANCTION AND LOAN DISBURSEMENT AMOUNT";
		AppStrings.mCheckLoanSanctionAmount = "ENTER YOUR LOAN SANCTION AMOUNT";
		AppStrings.mCheckLoanDisbursementAmount = "ENTER YOUR LOAN DISBURSEMENT AMOUNT";
		AppStrings.mIsPasswordSame = "PLEASE CHECK YOUR OLD PASSWORD AND NEW PASSWORD SHOULD NOT BE SAME.";
		AppStrings.mSavings_Banktransaction = "SAVINGS - BANK TRANSACTION";
		AppStrings.mCheckBackLogOffline = "CAN'T VIEW CHECK BACKLOG IN OFFLINE";
		AppStrings.mCredit = "CREDIT";
		AppStrings.mDebit = "DEBIT";
		AppStrings.mCheckFixedDepositAmount = "PLEASE CHECK YOUR FIXED DEPOSIT AMOUNT";
		AppStrings.mCheckDisbursementDate = "PLEASE CHECK YOUR LOAN DISBURSEMENT DATE";
		AppStrings.mSendEmail = "EMAIL";
		AppStrings.mTotalInterest = "TOTAL INTEREST";
		AppStrings.mTotalCharges = "TOTAL CHARGES";
		AppStrings.mTotalRepayment = "TOTAL REPAYMENT";
		AppStrings.mTotalInterestSubventionRecevied = "TOTAL INTEREST SUBVENTION RECEVIED";
		AppStrings.mTotalBankCharges = "TOTAL BANK CHARGES";
		AppStrings.mDone = "DONE";
		AppStrings.mPayment = "PAYMENT";
		AppStrings.mCashWithdrawal = "CASH WITHDRAWAL";
		AppStrings.mFundTransfer = "FUND TRANSFER";
		AppStrings.mDepositAmount = "DEPOSIT AMOUNT";
		AppStrings.mWithdrawalAmount = "WITHDRAWAL AMOUNT";
		AppStrings.mTransactionMode = "TRANSACTION MODE";
		AppStrings.mBankLoanDisbursement = "BANK LOAN DISBURSEMENT";
		AppStrings.mMemberToMember = "MEMBER TO MEMBER";
		AppStrings.mDebitAccount = "DEBIT ACCOUNT";
		AppStrings.mSHG_SavingsAccount = "SHG SAVINGS ACCOUNT";
		AppStrings.mDestinationMemberName = "DESTINATION MEMBER NAME";
		AppStrings.mSourceMemberName = "SOURCE MEMBER NAME";
		AppStrings.mMobileNoUpdation = "MOBILE NUMBER UPDATION";
		AppStrings.mMobileNo = "MOBILE NUMBER";
		AppStrings.mMobileNoUpdateSuccessAlert = "MOBILE NUMBER UPDATED SUCCESSFULLY.";
		AppStrings.mAadhaarNoUpdation = "AADHAAR NUMBER UPDATION";
		AppStrings.mSHGAccountNoUpdation = "SHG ACCOUNT NUMBER UPDATION";
		AppStrings.mAccountNoUpdation = "MEMBER ACCOUNT NUMBER UPDATION";
		AppStrings.mAadhaarNo = "AADHAAR NUMBER";
		AppStrings.mBranchName = "BRANCH NAME";
		AppStrings.mAadhaarNoUpdateSuccessAlert = "AADHAAR NUMBER UPDATED SUCCESSFULLY.";
		AppStrings.mMemberAccNoUpdateSuccessAlert = "MEMBER ACCOUNT NUMBER UPDATED SUCCESSFULLY.";
		AppStrings.mSHGAccNoUpdateSuccessAlert = "SHG ACCOUNT NUMBER UPDATED SUCCESSFULLY.";
		AppStrings.mCheckValidMobileNo = "PLEASE ENTER A VAILD MOBILE NUMBER";
		AppStrings.mInvalidAadhaarNo = "INVALID AADHAAR NUMBER";
		AppStrings.mCheckValidAadhaarNo = "PLEASE ENTER A VAILD AADHAAR NUMBER";
		AppStrings.mInvalidAccountNo = "INVALID ACCOUNT NUMBER";
		AppStrings.mCheckAccountNumber = "PLEASE CHECK YOUR ACCOUNT NUMBER";
		AppStrings.mInvalidMobileNo = "INVALID MOBILE NUMBER";
		AppStrings.mUploadScheduleAlert = "CAN'T UPLOAD SCHEDULE VI IN OFFLINE MODE";
		AppStrings.mIsMobileNoRepeat = "MOBILE NUMBERS REPEATED";
		AppStrings.mMobileNoUpdationNetworkCheck = "CAN'T UPDATE MOBILE NUMBER IN OFFLINE MODE";
		AppStrings.mAadhaarNoNetworkCheck = "CAN'T UPDATE AADHAAR NUMBER IN OFFLINE MODE";
		AppStrings.mShgAccNoNetworkCheck = "CAN'T UPDATE SHG ACCOUNT NUMBER IN OFFLINE MODE";
		AppStrings.mAccNoNetworkCheck = "CAN'T UPDATE MEMBER ACCOUNT NUMBER IN OFFLINE MODE";
		AppStrings.mAccountNoRepeat = "ACCOUNT NUMBERS REPEATED";
		AppStrings.mCheckBankAndBranchNameSelected = "PLEASE CHECK BANK AND BRANCH NAME SELECTED FOR A CORRESPONDING ACCOUNT NUMBER";
		AppStrings.mCheckNewLoanSancDate = "PLEASE SELECT A LOAN SANCTION DATE";
		AppStrings.mCreditLinkageInfo = "CREDIT LINKAGE INFO";
		AppStrings.mCreditLinkage = "CREDIT LINKAGE";
		AppStrings.mCreditLinkage_content = "Number of times bank loans were taken by the SHG and repaid completely.";
		AppStrings.mCreditLinkageAlert = "CAN'T UPDATE CREDIT LINKAGE INFO IN OFFLINE MODE";
		
	}

}
