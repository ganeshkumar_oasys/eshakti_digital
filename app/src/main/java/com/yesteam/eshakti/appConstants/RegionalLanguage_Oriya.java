package com.yesteam.eshakti.appConstants;

public class RegionalLanguage_Oriya {

	public RegionalLanguage_Oriya() {

	}

	public static void RegionalStrings() {

		AppStrings.LoginScreen = "ଲଗ୍ଇନ୍ ପୃଷ୍ଠା";
		AppStrings.userName = "ମୋବାଇଲ୍ ନମ୍ବର";//
		AppStrings.passWord = "ପାସ୍ୱାର୍ଡ";
		AppStrings.transaction = "ନେଣଦେଣ";
		AppStrings.reports = "ରିପୋର୍ଟ / ବିବରଣୀ";
		AppStrings.profile = "ପୋ୍ରପଂାଇଲ୍";
		AppStrings.meeting = "ସଭା଼";
		AppStrings.settings = "ସେଟିଂ";
		AppStrings.help = "ସାହାଯ୍ୟ";
		AppStrings.Attendance = "ଉପସ୍ଥାନ";
		AppStrings.MinutesofMeeting = "ସଭାର କାର୍ଯ୍ୟ ବିବରଣୀ";
		AppStrings.savings = "ସଂଚୟ";

		AppStrings.InternalLoanDisbursement = "ଆଭ୍ୟନ୍ତରୀଣ ଋଣ ପ୍ରଦାନ";// "LOAN
																		// DISBURSEMENT";

		AppStrings.InternalLoanDisbursement = "ଋଣ ବିତରଣ";

		AppStrings.expenses = "ଖର୍ଚ୍ଚ";
		AppStrings.cashinhand = "ହାତନଗଦ      :  ₹ ";
		AppStrings.cashatBank = "ବ୍ୟାଙ୍କରେ ଜମା ନଗଦ    :  ₹ ";
		AppStrings.memberloanrepayment = "ସଭ୍ୟଙ୍କ ଋଣ ପରିଶୋଧ";
		AppStrings.grouploanrepayment = "ଗୋଷ୍ଠୀଋଣ ପରିଶୋଧ";
		AppStrings.income = "ଆୟ";
		AppStrings.bankTransaction = "ବ୍ୟାଙ୍କ ନେଣଦେଣ";
		AppStrings.fixedDeposit = "ସ୍ଥାୟୀ ଜମା";
		AppStrings.otherincome = "ଅନ୍ୟାନ୍ୟ ଆୟ";
		AppStrings.subscriptioncharges = "ସବ୍ସ୍କ୍ରିପ୍ସନ୍";
		AppStrings.penalty = "ଅର୍ଥଦଣ୍ଡ";
		AppStrings.donation = "ଦାନ";
		AppStrings.federationincome = "ସଂଘର ଆୟ";//
		AppStrings.date = "ତାରିଖ";
		AppStrings.passwordchange = "ପାସ୍ୱାର୍ଡ଼୍ ପରିବର୍ତନ";
		AppStrings.listofexpenses = "ଖର୍ଚ୍ଚର ତାଲିକା";
		AppStrings.transport = "ପରିବହନ";//
		AppStrings.snacks = "ଚା";
		AppStrings.telephone = "ଟେଲିଫୋନ୍";
		AppStrings.stationary = "ଲେଖନ ସାମଗ୍ରୀ";//
		AppStrings.otherExpenses = "ଅନ୍ୟାନ୍ୟ ଖର୍ଚ୍ଚ";
		AppStrings.federationIncomePaid = "ସଂଘର ଆୟ (ପେଡ୍)";
		AppStrings.bankInterest = "ବ୍ୟାଙ୍କ ସୁଧ";
		AppStrings.bankExpenses = "ବ୍ୟାଙ୍କ ଖର୍ଚ୍ଚ";
		AppStrings.bankrepayment = "ବ୍ୟାଙ୍କ ପରିଶୋଧ";
		AppStrings.total = "ସମୁଦାୟ";
		AppStrings.summary = "ସାରାଂଶ";
		AppStrings.savingsSummary = "ସଂଚୟ ସାରାଂଶ";
		AppStrings.loanSummary = "ଋଣ ସାରାଂଶ";
		AppStrings.lastMonthReport = "ମାସିକ ରିପୋର୍ଟ";
		AppStrings.Memberreports = "ସଦସ୍ୟ ରିପୋର୍ଟ";
		AppStrings.GroupReports = "ଗୋଷ୍ଠୀ ରିପୋର୍ଟ";
		AppStrings.transactionsummary = "ବ୍ୟାଙ୍କର ନେଣଦେଣ ସାରାଂଶ";
		AppStrings.balanceSheet = "ନେଣଦେଣ ହିସାବପତ୍ର";
		AppStrings.balanceSheetHeader = "ଏଯାବତ ନେଣଦେଣର ହିସାବପତ୍ର";
		AppStrings.trialBalance = "ଟ୍ରାଏଲ ବାଲାନ୍ସ୍";
		AppStrings.outstanding = "ବକେୟା";
		AppStrings.totalSavings = "ମୋଟ ସଂଚୟ";
		AppStrings.groupSavingssummary = "ଗୋଷ୍ଠୀ ସଂଚୟ ସାରାଂଶ";
		AppStrings.groupLoansummary = "ଗୋଷ୍ଠୀ ଋଣ ସାରାଂଶ";
		AppStrings.amount = "ପରିମାଣ";
		AppStrings.principleAmount = "ମୂଳଧନ";
		AppStrings.interest = "ସୁଧ";
		AppStrings.groupLoan = "ଗୋଷ୍ଠୀ ଋଣ";
		AppStrings.balance = "BALANCE";
		AppStrings.Name = "ନାମ";
		AppStrings.Repaymenybalance = "ପରିଶୋଧ ବାଲାନ୍ସ ୍";
		AppStrings.details = "ସବିଶେଷ ବିବରଣୀ";
		AppStrings.payment = "ପେମେଁଟ୍";
		AppStrings.receipt = "ରିସିପ୍ଟ ୍";
		AppStrings.FromDate = "ତାରିଖରୁ";
		AppStrings.ToDate = "ତାରିଖ ପର୍ଯ୍ୟନ୍ତ";
		AppStrings.fromDate = "ଠାରୁ";
		AppStrings.toDate = "ଯାଏ / ପର୍ଯ୍ୟନ୍ତ";
		AppStrings.OldPassword = "ପୁରୁଣା ପାସ୍ୱାର୍ଡ଼";
		AppStrings.NewPassword = "ନୂଆ ପାସ୍ୱାର୍ଡ଼";
		AppStrings.ConfirmPassword = "ପାସ୍ୱାର୍ଡ଼ ନିଶ୍ଚିନ୍ତ କରନ୍ତୁ";
		AppStrings.agentProfile = "ଆନିମେଟର ପ୍ରୋଫାଇଲ";// "AGENT PROFILE";
		AppStrings.groupProfile = "ଗୋଷ୍ଠୀ ପ୍ରୋଫାଇଲ୍";
		AppStrings.bankDeposit = "ବ୍ୟାଙ୍କ ଜମା";
		AppStrings.withdrawl = "ଉଠାଣ";
		AppStrings.OutstandingAmount = "ବକାୟା";
		AppStrings.GroupLogin = "ଗୋଷ୍ଠୀ ଲଗ୍ଇନ୍";
		AppStrings.AgentLogin = "ଆନିମେଟର ଲଗ୍ଇନ୍";// "AGENT LOGIN";
		AppStrings.AboutLicense = "ଲାଇସେନ୍ସ";
		AppStrings.contacts = "ଯୋଗାଯୋଗ";
		AppStrings.contactNo = "ଯୋଗାଯୋଗ ନମ୍ବର";
		AppStrings.cashFlowStatement = "କ୍ୟାସ୍ ଫ୍ଲୋ ବିବରଣୀ";
		AppStrings.IncomeExpenditure = "ଆୟବ୍ୟୟ";
		AppStrings.transactionCompleted = "କାରବାର ସମାପ୍ତି";
		AppStrings.loginAlert = "ବ୍ୟବହାରକାରୀଙ୍କ ନାମ ଏବଂ ପାସ୍ୱାର୍ଡ଼ ଭୁଲ୍ ଅଛି";
		AppStrings.loginUserAlert = "ବ୍ୟବହାରକାରୀଙ୍କ ଡିଟେଲ୍ସ";
		AppStrings.loginPwdAlert = "ପାସ୍ୱାର୍ଡ଼ ଡିଟେଲ୍ସ";
		AppStrings.pwdChangeAlert = "ପାସ୍ୱାର୍ଡ଼୍ ସଫଳଭାବେ ପରିବର୍ତନ ହେଲା";
		AppStrings.pwdIncorrectAlert = "ଠିକ୍ ପାସ୍ୱାର୍ଡ଼୍ ପ୍ରଦାନ କରନ୍ତୁ";
		AppStrings.groupRepaidAlert = "CHECK YOUR OUTSTANDING AMOUNT";//"ନିଜର ରାଶି ଯାଂଚ୍ କରନ୍ତୁ";
		AppStrings.cashinHandAlert = "ହାତ ନଗଦ ଯାଂଚ୍ କରନ୍ତୁ";
		AppStrings.cashatBankAlert = "ବ୍ୟାଙ୍କ ନଗଦ ଯାଂଚ୍ କରନ୍ତୁ";
		AppStrings.nullAlert = "ନଗଦ ବିବରଣୀ ପ୍ରଦାନ କରନ୍ତୁ";
		AppStrings.DepositnullAlert = "ସମସ୍ତ ନଗଦର ବିବରଣୀ ପ୍ରଦାନ କରନ୍ତୁ";
		AppStrings.MinutesAlert = "ଉପରୋକ୍ତରୁ ଯେକୌଣସି ଚୟନ କରନ୍ତୁ ";
		AppStrings.afterDate = "ଏହି ତାରିଖ ପରେ ଏକ ତାରିଖଚୟନ କରନ୍ତୁ";
		AppStrings.last5Transaction = "ଶେଷ  ପାଂଚ କାରବାର";
		AppStrings.internetError = "ଦୟାକରି ଇଁଟର୍ନେଟ୍ ସଂଯୋଗ ଯାଂଚ୍ କରନ୍ତୁ";
		AppStrings.lastTransactionDate = "ଶେଷ କାରବାରର ତାରିଖ";
		AppStrings.yes = "ଠିକ୍";
		AppStrings.credit = "କ୍ରେଡିଟ୍";
		AppStrings.debit = "ଡେବିଟ୍";
		AppStrings.memberName = "ସଭ୍ୟଙ୍କ ନାମ";
		AppStrings.OutsatndingAmt = "ବକେୟା";
		AppStrings.calFromToDateAlert = "ଉଭୟ ତାରିଖ ଚୟନ କରନ୍ତୁ";
		AppStrings.selectedDate = "ଚୟନ କରାଯାଇଥିବା ତାରିଖ";
		AppStrings.calAlert = "ଅନ୍ୟ ଏକ ତାରିଖ ଚୟନ କରନ୍ତୁ";
		AppStrings.dialogMsg = "ଏହି ତାରିଖରୁ ଜାରି ରଖିବାକୁ ଚାହୁଁଛନ୍ତି କି";
		AppStrings.dialogOk = "ର୍ହ";
		AppStrings.dialogNo = "ନା";

		AppStrings.aboutYesbooks = "ଇଲେକ୍ଟ୍ରୋନିକ୍ ଡିଜିଟାଲ ଭାରତ ଗଠନ ଆମ ପ୍ରଧାନମନ୍ତ୍ରୀଙ୍କ ଏକ ସ୍ୱପ୍ନ ।  ଏହାକୁ ଆଧାର କରି ଇ-ଶକ୍ତି ଅଥବା ସ୍ୱୟଂ ସହାୟକ ଗୋଷ୍ଠୀ ଡିଜିଟାଇଜେସନ୍ ନାବାର୍ଡର "
				+ "ଏକ ଅଭିନବ ପ୍ରୟାସ । ଏହା ଭାରତକୁ ଏକ ସଶକ୍ତ ସମାଜ ଓ ଜ୍ଞାନଭିତିିକ ଅର୍ଥନୀତିରେ ପରିଣତ କରାଇବ ।";

		AppStrings.groupList = "ଗୋଷ୍ଠୀ ଚୟନ କରନ୍ତୁ";// GROUP LIST
		AppStrings.login = "ଲଗଇନ୍";
		AppStrings.submit = "ଦାଖଲ କରନ୍ତୁ";
		AppStrings.edit = "ଏଡ଼ିଟ୍";
		AppStrings.bankBalance = "ବ୍ୟାଙ୍କ ଜମାରାଶି";
		AppStrings.balanceAsOn = "ଏଯାବତ୍ ଜମାରାଶି    : ";
		AppStrings.savingsBook = "ଖାତା ଅନୁଯାୟୀ ଜମାରାଶି  : ";
		AppStrings.savingsBank = "ବ୍ୟାଙ୍କ ଅନୁଯାୟୀ ଜମାରାଶି  : ";
		AppStrings.difference = "ଅନ୍ତର : ";
		AppStrings.loanOutstandingBook = "ଖାତା ଅନୁଯାୟୀ ବକେୟା : ";
		AppStrings.loanOutstandingBank = "ବ୍ୟାଙ୍କ ଅନୁଯାୟୀ ବକେୟା : ";
		AppStrings.values = new String[] { "ବାସସ୍ଥାନ (ଗ୍ରାମ)",

		"ଉପସ୍ଥିତ ସମସ୍ତ ସଭ୍ୟ/ସଭ୍ୟା",

		"ଜମାରାଶି ସଂଗ୍ରହ",

		"ବ୍ୟକ୍ତିଗତ ଋଣପ୍ରଦାନ ସଂପର୍କରେ ଆଲୋଚନା",

		"ଋଣ ପରିଶୋଧ ଓ ସୁଧ ସଂଗ୍ରହ",

		"ପ୍ରଶିକ୍ଷଣ ସଂପର୍କରେ ଆଲୋଚନା",

		"ବ୍ୟାଙ୍କ ଋଣ ପାଇବା ପାଇଁ ନିଷ୍ପତି",

		"ସାଧାରଣ ମତାମତ ସଂପର୍କରେ ଆଲୋଚନା",

		"ସ୍ୱଚ୍ଛତା ସଂପର୍କରେ ଆଲୋଚନା",

		"ଶିକ୍ଷାର ଗୁରୁତ୍ୱ ସଂପର୍କରେ ଆଲୋଚନା" };

		AppStrings.January = "ଜାନୁୟାରୀ";
		AppStrings.February = "ଫେବୃୟାରୀ";
		AppStrings.March = "ମାର୍ଚ୍ଚ";
		AppStrings.April = "ଅପ୍ରେଲ";
		AppStrings.May = "ମଇ";
		AppStrings.June = "ଜୁନ୍";
		AppStrings.July = "ଜୁଲାଇ";
		AppStrings.August = "ଅଗଷ୍ଟ";
		AppStrings.September = "ସେପ୍ଟେମ୍ବର";
		AppStrings.October = "ଅକ୍ଟୋବର";
		AppStrings.November = "ନଭେମ୍ବର";
		AppStrings.December = "ଡିସେମ୍ବର";
		AppStrings.uploadPhoto = "ଫଟୋ ଅପ୍ଲୋଡ଼ କରନ୍ତୁ";
		AppStrings.Il_Disbursed = "ଆଭ୍ୟନ୍ତରୀଣ ଋଣ ବଂଟନ";
		AppStrings.IL_Loan = "ଅଭ୍ୟନ୍ତରୀଣ ଋଣ";
		AppStrings.Il_Repaid = "ଅଭ୍ୟନ୍ତରୀଣ ଋଣ ପରିଶୋଧ";
		AppStrings.offlineTransactionCompleted = "ଆପଣଙ୍କ ଅଫଲାଇନ୍ ନେଣଦେଣ ସଫଳଭାବେ ସଂଚିତ ହେଲା";
		AppStrings.offlineTransactionreports = "ଅଫଲାଇନ୍ ନେଣଦେଣ ବିବରଣୀ";
		AppStrings.offlineTransaction = "ଅଫଲାଇନ୍ ନେଣଦେଣ";
		AppStrings.disconnectInternet = "ଅଫଲାଇନ ସେବାକୁ ଚାଲୁରଖିବା ପାଇଁ  ଦୟାକରି ଆପଣଙ୍କ ଇଁଟରନେଟ ବିଚ୍ଛିନ୍ନ କରନ୍ତୁ";
		AppStrings.Dashboard = "ଦାସ୍ବୋର୍ଡ଼";
		AppStrings.bankTransactionSummary = "ବ୍ୟାଙ୍କ ନେଣଦେଣ ସାରାଂଶ ";
		AppStrings.BL_Disbursed = "ବଂଟନ";
		AppStrings.BL_Repaid = "ପରିଶୋଧ";
		AppStrings.auditing = "ହିସାବ ନିରୀକ୍ଷଣ";
		AppStrings.training = "ପ୍ରଶିକ୍ଷଣ";
		AppStrings.auditor_Name = " ଅଡ଼ିଟରଙ୍କ ନାମ";
		AppStrings.auditing_Date = "ଅଡ଼ିଟ୍ ତାରିଖ";
		AppStrings.training_Date = "ପ୍ରଶିକ୍ଷଣ ତାରିଖ";
		AppStrings.auditing_Period = "ଅଡ଼ିଟ୍ ଅବଧି";
		AppStrings.training_types = new String[] { "ସ୍ୱାସ୍ଥ୍ୟ ସଚେତନତା",
				"ହିସାବ କିତାବ ସଚେତନତା", "ଜୀବିକା ଉପାର୍ଜନ ସଚେତନତା",
				"ସମାଜିକ ସଚେତନତା", "ଶିକ୍ଷାଗତ ସଚେତନତା" };
		AppStrings.changeLanguage = "ଭାଷା ପରିବର୍ତନ କରନ୍ତୁ";
		AppStrings.interestRepayAmount = "ଚଳନ୍ତି ଦେୟ";
		AppStrings.interestRate = "ସୁଧହାର";
		AppStrings.savingsAmount = "ଜମାରାଶି";
		AppStrings.noGroupLoan_Alert = "ଗୋଷ୍ଠୀଋଣ ଉପଲବ୍ଧ ନୁହେଁ";
		AppStrings.confirmation = "ନଶ୍ଚିତ କରନ୍ତୁ";
		AppStrings.logOut = "ଲଗ୍ଆଉଟ୍";
		AppStrings.fromDateAlert = "ଦୟାକରି ଏହି ତାରିଖରୁ ପ୍ରଦାନ କରନ୍ତୁ";
		AppStrings.toDateAlert = "ଦୟାକରି ଏହି ତାରିଖ ପର୍ଯ୍ୟନ୍ତ ପ୍ରଦାନ କରନ୍ତୁ";
		AppStrings.auditingDateAlert = "ଦୟାକରି ଅଡ଼ିଟ୍ ତାରିଖ ପ୍ରଦାନ କରନ୍ତୁ";
		AppStrings.auditorAlert = "ଦୟାକରି ଅଡ଼ିଟରଙ୍କ ନାମ ପ୍ରଦାନ କରନ୍ତୁ";
		AppStrings.nullDetailsAlert = "ସଂପୂର୍ଣ୍ଣ ବିବରଣୀ ପ୍ରଦାନ କରନ୍ତୁ";
		AppStrings.trainingDateAlert = "ପ୍ରଶିକ୍ଷଣ ତାରିଖ ପ୍ରଦାନ କରନ୍ତୁ";
		AppStrings.trainingTypeAlert = "ଯେକୌଣସି ଗୋଟିଏ ପ୍ରକାର ପ୍ରଶିକ୍ଷଣ ଚୟନ କରନ୍ତୁ";
		AppStrings.offline_ChangePwdAlert = "ଆପଣଙ୍କ ଇଁଟରନେଟ ସଂଯୋଗ ବିନା ପାସ୍ୱାର୍ଡ଼ ପରିବର୍ତନ କରିପାରିବେ ନାହଁି";
		AppStrings.transactionFailAlert = "ଆପଣଙ୍କ ନେଣଦେଣ ଅସଫଳ";
		AppStrings.voluntarySavings = "ସ୍ୱେଚ୍ଛାକୃତ ଜମାରାଶି";
		AppStrings.tenure = "ଅବଧି";
		AppStrings.purposeOfLoan = "ଋଣର ଉଦ୍ଧେଶ୍ୟ";
		AppStrings.chooseLabel = "ଋଣ ଚୟନ";
		AppStrings.Tenure_POL_Alert = "ଦୟାକରି ଋଣର ଉଦ୍ଦେଶ୍ୟ ଓ ଅବଧି ପ୍ରଦାନ କରନ୍ତୁ";
		AppStrings.interestSubvention = "ସୁଧ ରିହାତି (ସବ୍ଭେନ୍ସନ୍) ପ୍ରାପ୍ତି";
		AppStrings.adminAlert = "ଆଡ଼ମିନ୍ ସହିତ ଯୋଗାଯୋଗ କରନ୍ତୁ";
		AppStrings.userNotExist = "ଲଗ୍ଇନ୍ ଉପଲବ୍ଧ ନୁହଁ ।ଦୟାକରି ଅନ୍ଲାଇନ୍ରେ ଲଗ୍ଇନ୍ ଜାରି ରଖନ୍ତୁ";
		AppStrings.tryLater = "ପରେ ଚେଷ୍ଟା କରନ୍ତ ୁ";
		AppStrings.offlineDataAvailable = "ଅପଂ ଲାଇନ୍ ତଥ୍ୟ ଉପଲବ୍ଧ ।ଦୟାକରି ଲଗ୍ଆଉଟ୍ କରନ୍ତୁ ଓ ପୁଣିଥରେ ଚେଷ୍ଟାକରନ୍ତୁ";
		AppStrings.choosePOLAlert = "ଋଣର ଉଦ୍ଦେଶ୍ୟ  ଚୟନ କରନ୍ତୁ";
		AppStrings.InternalLoan = "ଆଭ୍ୟନ୍ତରୀଣ ଋଣ";
		AppStrings.TrainingTypes = "ପ୍ରଶିକ୍ଷଣର ପ୍ରକାର";
		AppStrings.uploadInfo = "ସଭ୍ୟଙ୍କର ସଂପୂଣ୍ଣ ବିବରଣୀ";
		AppStrings.uploadAadhar = "ଆଧାର ଅପଲୋଡ୍ କରନ୍ତୁ";
		AppStrings.month = "-- ମାସ ଚୟନ କର   --";
		AppStrings.year = "-- ବର୍ଷ ଚୟନ କରନ୍ତୁ   --";
		AppStrings.monthYear_Alert = "ଦୟାକରି ମାସ ଓ ବର୍ଷ ଚୟନ କରନ୍ତୁ";
		AppStrings.chooseLanguage = "ଭାଷା ପରିବର୍ତନ କରନ୍ତୁ";
		AppStrings.autoFill = "ସ୍ୱତଃ ପୂରଣ";
		AppStrings.viewAadhar = "ଆଧାର ଦେଖନ୍ତୁ";
		AppStrings.viewPhoto = "ଫଟୋ ଦେଖନ୍ତୁ";
		AppStrings.uploadAlert = "ଆପଣ ଇଁଟର୍ନେଟ୍ ସଂଯୋଗ ବିନା ଆଧାର ଏବଂ ଫଟୋ ଅପଲୋଡ଼ କରିପାରିବେ ନାହଁି";// "YOU
																								// CAN'T
																								// UPLOAD
																								// THE
																								// AADHAR
																								// AND
																								// PHOTO
																								// WITHOUT
																								// INTERNET
																								// CONNECTION";
		AppStrings.upload = "ଅପଲୋଡ଼ କରନ୍ତୁ";
		AppStrings.view = "ଦେଖନ୍ତୁ";
		AppStrings.showAadharAlert = "ଦୟାକରି ଆପଣଙ୍କର ଆଧାର କାର୍ଡ ଦେଖାନ୍ତୁ";
		AppStrings.deactivateAccount = "ଆକାଉଂଟ ନିଷ୍କ୍ରିୟ କରନ୍ତୁ";
		AppStrings.deactivateAccount_SuccessAlert = "ଆପଣଙ୍କର ଆକାଉଂଟ ନିଷ୍କ୍ରିୟ ସଫଳ ହେଲା";
		AppStrings.deactivateAccount_FailAlert = "ଆପଣଙ୍କର ଆକାଉଂଟ ନିଷ୍କ୍ରିୟ ଅସଫଳ";
		AppStrings.deactivateNetworkAlert = "ଆପଣ ଇଁଟରନେଟର ସଂଯୋଗ ବିନା ଆକାଉଂଟ ନିଷ୍କ୍ରିୟ କରିପାରିବେ ନାହଁି";
		AppStrings.offlineReports = "ଅଫଲାଇନ ବିବରଣୀ";
		AppStrings.registrationSuccess = "ଆପଣଙ୍କ ପଞ୍ଜୀକରଣ ସଫଳ ହେଲା";
		AppStrings.reg_MobileNo = "ପଞ୍ଜୀକୃତ ମୋବାଇଲ ନମ୍ବର ପ୍ରଦାନ କରନ୍ତୁ";
		AppStrings.reg_IMEINo = "ପ୍ରବେଶ ନିଷେଧ";
		AppStrings.reg_BothNo = "ପ୍ରବେଶ ନିଷେଧ";
		AppStrings.reg_IMEIUsed = "ପ୍ରବେଶ ନିଷେଧ । ଡିଭାଇସ୍ ମେଳ ଖାଉନାହିଁ";
		AppStrings.signInAsDiffUser = "ଭିନ୍ନ ବ୍ୟବହାର୍ଯ୍ୟରେ ସାଇନ୍ ଇନ୍ କରନ୍ତୁ";
		AppStrings.incorrectUserNameAlert = "ଭୁଲ୍ ବ୍ୟବହାର୍ଯ୍ୟ ନାମ";

		AppStrings.noofflinedatas = "କୌଣସି ଅଫଲାଇନ୍ ନେଣଦେଣ  ପ୍ରବେଶ ନୁହେଁ";
		AppStrings.uploadprofilealert = "ନିଜର ସଂପୂର୍ଣ୍ଣ ବିବରରୀ ପ୍ରଦାନ କରନ୍ତୁ";
		AppStrings.monthYear_Warning = "ଦୟାକରି ସଠିକ୍ ମାସ ଏବଂ ବର୍ଷ ଚୟନ କରନ୍ତୁ";
		AppStrings.of = "ସମ୍ବନ୍ଧରେ";
		AppStrings.previous_DateAlert = "ପୂର୍ବ ତାରିଖ ଚୟନ କରନ୍ତୁ";
		AppStrings.nobanktransactionreportdatas = "ବ୍ୟାଙ୍କ ନେଣଦେଣର କୌଣସି ବିବରଣୀ ତଥ୍ୟ ନାହଁି";

		AppStrings.mDefault = "ଡିଫଲ୍ଟ";// "STEP WISE";
		AppStrings.mTotalDisbursement = "ସମୁଦାୟ ବଂଟନ";
		AppStrings.mInterloan = "INTERNAL LOAN REPAYMENT";// "PERSONAL LOAN
															// REPAYMENT";
		AppStrings.mTotalSavings = "ସମୁଦାୟ ଆଦାୟ";
		AppStrings.mCommonNetworkErrorMsg = "ଦୟାକରି ଆପଣଙ୍କର ନେଟୱର୍କ ଯାଂଚ କରନ୍ତୁ";
		AppStrings.mLoginAlert_ONOFF = "ଦୟାକରି ଇଁଟରନେଟ ସଂଯୋଗ ପୂନର୍ବାର ଲଗ୍ଇନ୍ କରନ୍ତୁ";
		AppStrings.changeLanguageNetworkException = "ଦୟାକରି  ଲଗ୍ଆଉଟ୍ କରନ୍ତୁ, ଇଁଟରନେଟ ସଂଯୋଗ କରି ପୂନର୍ବାର ଲଗ୍ଇନ୍ କରିବା ପାଇଁ ଚେଷ୍ଟା କରନ୍ତୁ";
		AppStrings.photouploadmsg = "ଫଟୋ ଅପ୍ଲୋଡ଼ ସଫଳ ହେଲା";
		AppStrings.loginAgainAlert = "BACKGROUND DATA IS STILL LOADING PLEASE WAIT FOR SOME TIME.";// "ଦୟାକରି ପୁନର୍ବାର ଲଗ୍ଇନ୍ କରନ୍ତୁ";

		/* Interloan Disbursement menu */
		AppStrings.mInternalInterloanMenu = "ଆଭ୍ୟନ୍ତରୀଣ ଋଣ";
		AppStrings.mInternalBankLoanMenu = "ବ୍ୟାଙ୍କ ଋଣ";
		AppStrings.mInternalMFILoanMenu = "ଏମ୍ ଏଫ୍ ଆଇ ଋଣ";
		AppStrings.mInternalFederationMenu = "ସଂଘ ଋଣ";

		AppStrings.bankName = "ବ୍ୟାଙ୍କ ନାମ";
		AppStrings.mfiname = "ଏମ୍ ଏଫ୍ ଆଇ ନାମ";
		AppStrings.loanType = "ଋଣର ପ୍ରକାର";
		AppStrings.bankBranch = "ବ୍ୟାଙ୍କ ଶାଖା";
		AppStrings.installment_type = "କିସ୍ତି ପ୍ରକାର";
		AppStrings.NBFC_Name = "ଏନ୍ ବି ଏଫ୍ ସି ନାମ";
		AppStrings.Loan_Account_No = "ଋଣ ଖାତା ନମ୍ବର";
		AppStrings.Loan_Sanction_Amount = "ଋଣ ମଞ୍ଜୁର ପରିମାଣ";
		AppStrings.Loan_Sanction_Date = "ଋଣ ମଞ୍ଜୁର ତାରିଖ";
		AppStrings.Loan_Disbursement_Amount = "ଋଣ ପ୍ରଦାନ ପରିମାଣ";

		AppStrings.Loan_Disbursement_Date = "ଋଣ ବଂଟନ ତାରିଖ";
		AppStrings.Loan_Tenure = "ଋଣ ଅବଧି";
		AppStrings.Subsidy_Amount = "ଛାଡ଼ ପରିମାଣ";
		AppStrings.Subsidy_Reserve_Fund = "ସବ୍ସିଡ଼୍ ରିଜର୍ଭ ନିଧି";
		AppStrings.Interest_Rate = "ସୁଧ ହାର";

		AppStrings.mMonthly = "ସାସିକ";
		AppStrings.mQuarterly = "ପାକ୍ଷିକ";
		AppStrings.mHalf_Yearly = "ଅର୍ଦ୍ଧବାର୍ଷିକ";
		AppStrings.mYearly = "ବାର୍ଷିକ";
		AppStrings.mAgri = "କୃଷି";
		AppStrings.mMSME = "ଏମ୍ ଏସ୍ ଏମ୍ ଇ";
		AppStrings.mOthers = "ଅନ୍ୟାନ୍ୟ";

		AppStrings.mMFI_NAME_SPINNER = "ଏମ୍ ଏଫ୍ ଆଇ ନାମ";
		AppStrings.mMFINabfins = "ନାବ୍ଫିନ୍ସ୍";
		AppStrings.mMFIOthers = "ଅନ୍ୟାନ୍ୟ";

		AppStrings.mInternalTypeLoan = "ଅଭ୍ୟନ୍ତରୀଣ ଋଣ";

		AppStrings.mPervious = "ପୂର୍ବବର୍ତୀ";
		AppStrings.mNext = "ପରବର୍ତୀ";
		AppStrings.mPasswordUpdate = "ଆପଣଙ୍କର ପାସ୍ୱାର୍ଡ଼ ସଫଳତାର ସହିତ ନବୀକରଣ ହେଲା";

		AppStrings.mExpense_meeting = "ସଭାଖର୍ଚ୍ଚ";

		AppStrings.mFixedDepositeAmount = "ସ୍ଥାୟୀ ଜମାରାଶି";
		AppStrings.mFedBankName = "ସଂଘ ନାମ";// "BANK/NBFC NAME";
		AppStrings.mLoanSanc_loanDist = "ଦୟାକରି ଋଣ ପ୍ରଦାନର ପରିମାଣ ଯାଁଚ କରନ୍ତୁ";

		AppStrings.mSubmitbutton = "ଠିକ୍";

		AppStrings.mStepWise_LoanDibursement = "ଆଭ୍ୟନ୍ତରୀଣ ଋଣ ପ୍ରଦାନ";
		AppStrings.mNewUserSignup = "ପଞ୍ଜୀକରଣ";
		AppStrings.mMobilenumber = "ମୋବାଇଲ୍ ନମ୍ବର";
		AppStrings.mIsAadharAvailable = "ଆଧାର ସୂଚନା ଅପ୍ଡେଟେଡ଼୍ ହୋଇନି";
		AppStrings.mSavingsTransaction = "ସଂଚୟ ନେଣଦେଣ";
		AppStrings.mBankCharges = "ବ୍ୟାଙ୍କ ଚାର୍ଜ୍";
		AppStrings.mCashDeposit = "ନଗଦ ଜମାରାଶି";
		AppStrings.mLimit = "ଋଣ ସୀମ";

		AppStrings.mAccountToAccountTransfer = "ACCOUNT TO ACCOUNT TRANSFER";
		AppStrings.mTransferFromBank = "FROM BANK";
		AppStrings.mTransferToBank = "TO BANK";
		AppStrings.mTransferAmount = "TRANSFER AMOUNT";
		AppStrings.mTransferCharges = "TRANSFER CHARGES";
		AppStrings.mTransferSpinnerFloating = "TO BANK";
		AppStrings.mTransferNullToast = "PLEASE PROVIDE ALL DETAILS";
		AppStrings.mAccToAccTransferToast = "YOU HAVE ONLY ONE BANK ACCOUNT";

		/* Loan Account */
		AppStrings.mLoanaccHeader = "LOAN ACCOUNT";
		AppStrings.mLoanaccCash = "CASH";
		AppStrings.mLoanaccBank = "BANK";
		AppStrings.mLoanaccWithdrawal = "WITHDRAWAL";
		AppStrings.mLoanaccExapenses = "EXPENSES";
		AppStrings.mLoanaccSpinnerFloating = "LOAN ACCOUNT BANK ";
		AppStrings.mLoanaccCash_BankToast = "PLEASE SELECT A CASH OR BANK";
		AppStrings.mLoanaccNullToast = "PLEASE PROVIDE ALL DETAILS";
		AppStrings.mLoanaccBankNullToast = "PLEASE SELECT A BANK NAME";

		AppStrings.mBankTransSavingsAccount = "SAVINGS ACCOUNT";
		AppStrings.mBankTransLoanAccount = "LOAN ACCOUNT";

		AppStrings.mLoanAccType = "TYPE";
		AppStrings.mLoanAccBankAmountAlert = "PLEASE CHECK BANK BALANCE";

		AppStrings.mCashAtBank = "ବ୍ୟାଙ୍କରେ ଜମା ନଗଦ";
		AppStrings.mCashInHand = "ହାତନଗଦ";
		AppStrings.mShgSeedFund = "SHG SEED FUND";
		AppStrings.mFixedAssets = "FIXED ASSETS";
		AppStrings.mVerified = "VERIFIED";
		AppStrings.mConfirm = "CONFIRM";
		AppStrings.mProceed = "PROCEED";
		AppStrings.mDialogAlertText = "YOUR OPENING BALANCE DATAS ARE UPDATED SUCCESSFULLY, NOW YOU CAN PROCEED";
		AppStrings.mBalanceSheetDate = "BALANCE SHEET DATE";
		AppStrings.mCheckMonthlyEntries = "NO ENTRIES FOR PREVIOUS MONTH, SO YOU SHOULD PUT DATAS FOR PREVIOUS MONTH";
		AppStrings.mTermLoanOutstanding = "OUTSTANDING";
		AppStrings.mCashCreditOutstanding = "OUTSTANDING";
		AppStrings.mGroupLoanOutstanding = "GROUP LOAN OUTSTANDING";
		AppStrings.mMemberLoanOutstanding = "MEMBER LOAN OUTSTANDING";
		AppStrings.mContinueWithDate = "DO YOU WANT TO CHANGE THIS DATE";
		AppStrings.mCheckList = "CHECK LIST";
		AppStrings.mMicro_Credit_Plan = "MICRO CREDIT PLAN";
		AppStrings.mAadharCard_Invalid = "YOUR AADHAR CARD IS INVALID";
		AppStrings.mIsNegativeOpeningBalance = "RECTIFY NEGATIVE BALANCE";
		AppStrings.mLoginDetailsDelete = "PREVIOUS LOGIN DETAILS WILL BE DELETED. ARE YOU WANT TO DELETE DATA?";
		AppStrings.mVerifyGroupAlert = "YOU MUST BE ONLINE TO VERIFY";
		AppStrings.next = "NEXT";
		AppStrings.mNone = "NONE";
		AppStrings.mSelect = "SELECT";
		AppStrings.mAnimatorName = "ANIMATOR NAME";
		AppStrings.mSavingsAccount = "SAVINGS ACCOUNT";
		AppStrings.mAccountNumber = "ACCOUNT NUMBER";
		AppStrings.mLoanAccountAlert = "CAN'T TRANSFER LOAN ACCOUNT IN OFFLINE MODE";
		AppStrings.mSeedFund = "SEED FUND";
		AppStrings.mLoanTypeAlert = "PLEASE CHOOSE A LOAN ACCOUNT TYPE";
		AppStrings.mOutstandingAlert = "PLEASE CHECK YOUR OUTSTANDING AMOUNT";
		AppStrings.mLoanAccTransfer = "LOAN ACCOUNT";
		AppStrings.mLoanName = "LOAN NAME";
		AppStrings.mLoanId = "LOAN ID";
		AppStrings.mLoanDisbursementDate = "LOAN DISBURSEMENT DATE";
		AppStrings.mAdd = "ADD";
		AppStrings.mLess = "LESS";
		AppStrings.mRepaymentWithInterest = "REPAYMENT ( WITH INTEREST )";
		AppStrings.mCharges = "CHARGES";
		AppStrings.mExistingLoan = "EXISTING LOAN";
		AppStrings.mNewLoan = "NEW LOAN";
		AppStrings.mIncreaseLimit = "INCREASE LIMIT";
		AppStrings.mAvailableLimit = "AVAILABLE LIMIT";
		AppStrings.mDisbursementDate = "DISBURSEMENT DATE";
		AppStrings.mDisbursementAmount = "DISBURSEMENT AMOUNT";
		AppStrings.mSanctionAmount = "SANCTION AMOUNT";
		AppStrings.mBalanceAmount = "BALANCE AMOUNT";
		AppStrings.mMemberDisbursementAmount = "MEMBER DISBURSEMENT AMOUNT";
		AppStrings.mLoanDisbursementFromLoanAcc = "LOAN DISBURSEMENT FROM LOAN ACCOUNT";
		AppStrings.mLoanDisbursementFromSbAcc = "LOAN DISBURSEMENT FROM SB ACCOUNT";
		AppStrings.mRepaid = "REPAID";
		AppStrings.mCheckBackLog = "CHECK BACKLOG";
		AppStrings.mTarget = "TARGET";
		AppStrings.mCompleted = "COMPLETED";
		AppStrings.mPending = "PENDING";
		AppStrings.mAskLogout = "DO YOU WANT TO LOGOUT?";
		AppStrings.mCheckDisbursementAlert = "PLEASE CHECK YOUR DISBURSEMENT AMOUNT";
		AppStrings.mCheckbalanceAlert = "PLEASE CHECK YOUR BALANCE AMOUNT";
		AppStrings.mVideoManual = "VIDEO MANUAL";
		AppStrings.mPdfManual = "PDF MANUAL";
		AppStrings.mAmountDisbursingAlert = "DO YOU WANT TO CONTINUE WITHOUT DISBURSING TO MEMBERS?.";
		AppStrings.mOpeningDate = "OPENING DATE";
		AppStrings.mCheckLoanSancDate_LoanDisbDate = "PLEASE CHECK YOUR LOAN SANCTION DATE AND LOAN DISBURSEMENT DATE.";
		AppStrings.mLoanDisbursementFromRepaid = "CC LOAN WITHDRAWAL";
		AppStrings.mExpense = "EXPENSE";
		AppStrings.mBeforeLoanPay = "BEFORE LOAN PAY";
		AppStrings.mLoans = "LOANS";
		AppStrings.mSurplus = "SURPLUS";
		AppStrings.mPOLAlert = "PLEASE ENTER THE PURPOSE OF LOAN";
		AppStrings.mCheckLoanAmounts = "PLEASE CHECK YOUR LOAN SANCTION AMOUNT AND LOAN DISBURSEMENT AMOUNT";
		AppStrings.mCheckLoanSanctionAmount = "ENTER YOUR LOAN SANCTION AMOUNT";
		AppStrings.mCheckLoanDisbursementAmount = "ENTER YOUR LOAN DISBURSEMENT AMOUNT";
		AppStrings.mIsPasswordSame = "PLEASE CHECK YOUR OLD PASSWORD AND NEW PASSWORD SHOULD NOT BE SAME.";
		AppStrings.mSavings_Banktransaction = "SAVINGS - BANK TRANSACTION";
		AppStrings.mCheckBackLogOffline = "CAN'T VIEW CHECK BACKLOG IN OFFLINE";
		AppStrings.mCheckFixedDepositAmount = "PLEASE CHECK YOUR FIXED DEPOSIT AMOUNT";
		AppStrings.mCheckDisbursementDate = "PLEASE CHECK YOUR LOAN DISBURSEMENT DATE";
		AppStrings.mSendEmail = "EMAIL";
		AppStrings.mTotalInterest = "TOTAL INTEREST";
		AppStrings.mTotalCharges = "TOTAL CHARGES";
		AppStrings.mTotalRepayment = "TOTAL REPAYMENT";
		AppStrings.mTotalInterestSubventionRecevied = "TOTAL INTEREST SUBVENTION RECEVIED";
		AppStrings.mTotalBankCharges = "TOTAL BANK CHARGES";
		AppStrings.mDone = "DONE";
		AppStrings.mPayment = "PAYMENT";
		AppStrings.mCashWithdrawal = "CASH WITHDRAWAL";
		AppStrings.mFundTransfer = "FUND TRANSFER";
		AppStrings.mDepositAmount = "DEPOSIT AMOUNT";
		AppStrings.mWithdrawalAmount = "WITHDRAWAL AMOUNT";
		AppStrings.mTransactionMode = "TRANSACTION MODE";
		AppStrings.mBankLoanDisbursement = "BANK LOAN DISBURSEMENT";
		AppStrings.mMemberToMember = "MEMBER TO MEMBER";
		AppStrings.mDebitAccount = "DEBIT ACCOUNT";
		AppStrings.mSHG_SavingsAccount = "SHG SAVINGS ACCOUNT";
		AppStrings.mDestinationMemberName = "DESTINATION MEMBER NAME";
		AppStrings.mSourceMemberName = "SOURCE MEMBER NAME";
		AppStrings.mMobileNoUpdation = "MOBILE NUMBER UPDATION";
		AppStrings.mMobileNo = "MOBILE NUMBER";
		AppStrings.mMobileNoUpdateSuccessAlert = "MOBILE NUMBER UPDATED SUCCESSFULLY.";
		AppStrings.mAadhaarNoUpdation = "AADHAAR NUMBER UPDATION";
		AppStrings.mSHGAccountNoUpdation = "SHG ACCOUNT NUMBER UPDATION";
		AppStrings.mAccountNoUpdation = "MEMBER ACCOUNT NUMBER UPDATION";
		AppStrings.mAadhaarNo = "AADHAAR NUMBER";
		AppStrings.mBranchName = "BRANCH NAME";
		AppStrings.mAadhaarNoUpdateSuccessAlert = "AADHAAR NUMBER UPDATED SUCCESSFULLY.";
		AppStrings.mMemberAccNoUpdateSuccessAlert = "MEMBER ACCOUNT NUMBER UPDATED SUCCESSFULLY.";
		AppStrings.mSHGAccNoUpdateSuccessAlert = "SHG ACCOUNT NUMBER UPDATED SUCCESSFULLY.";
		AppStrings.mCheckValidMobileNo = "PLEASE ENTER A VAILD MOBILE NUMBER";
		AppStrings.mInvalidAadhaarNo = "INVALID AADHAAR NUMBER";
		AppStrings.mCheckValidAadhaarNo = "PLEASE ENTER A VAILD AADHAAR NUMBER";
		AppStrings.mInvalidAccountNo = "INVALID ACCOUNT NUMBER";
		AppStrings.mCheckAccountNumber = "PLEASE CHECK YOUR ACCOUNT NUMBER";
		AppStrings.mInvalidMobileNo = "INVALID MOBILE NUMBER";
		AppStrings.mUploadScheduleAlert = "CAN'T UPLOAD SCHEDULE VI IN OFFLINE MODE";
		AppStrings.mIsMobileNoRepeat = "MOBILE NUMBERS REPEATED";
		AppStrings.mMobileNoUpdationNetworkCheck = "CAN'T UPDATE MOBILE NUMBER IN OFFLINE MODE";
		AppStrings.mAadhaarNoNetworkCheck = "CAN'T UPDATE AADHAAR NUMBER IN OFFLINE MODE";
		AppStrings.mShgAccNoNetworkCheck = "CAN'T UPDATE SHG ACCOUNT NUMBER IN OFFLINE MODE";
		AppStrings.mAccNoNetworkCheck = "CAN'T UPDATE MEMBER ACCOUNT NUMBER IN OFFLINE MODE";
		AppStrings.mAccountNoRepeat = "ACCOUNT NUMBERS REPEATED";
		AppStrings.mCheckBankAndBranchNameSelected = "PLEASE CHECK BANK AND BRANCH NAME SELECTED FOR A CORRESPONDING ACCOUNT NUMBER";
		AppStrings.mCheckNewLoanSancDate = "PLEASE SELECT A LOAN SANCTION DATE";
		AppStrings.mCreditLinkageInfo = "CREDIT LINKAGE INFO";
		AppStrings.mCreditLinkage = "CREDIT LINKAGE";
		AppStrings.mCreditLinkage_content = "Number of times bank loans were taken by the SHG and repaid completely.";
		AppStrings.mCreditLinkageAlert = "CAN'T UPDATE CREDIT LINKAGE INFO IN OFFLINE MODE";
	}
}
