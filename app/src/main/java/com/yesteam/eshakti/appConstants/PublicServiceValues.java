package com.yesteam.eshakti.appConstants;

public class PublicServiceValues {
	/* Non Secure WebService Namespace */
	public final static String PRODUCTION_NAMESPACE = "http://shpiaws.yesbooks.in/";
	public final static String PRODUCTION_ENGLISH_NAMESPACE = "http://shpiaews.yesbooks.in/";
	/* Secure WebService Namespace */
	public final static String PRODUCTION_CRYPTOGRAPHY_NAMESPACE = "http://yshpiedaws.yesbooks.in/";
	public final static String PRODUCTION_CRYPTOGRAPHY_ENGLISH_NAMESPACE = "http://yshpiedaews.yesbooks.in/";
	/* Non Secure WebService SoapAddress */
	public final static String PRODUCTION_SOAP_ADDRESS = "http://shpiaws.yesbooks.in/service.asmx";
	public final static String PRODUCTION_ENGLISH_SOAP_ADDRESS = "http://shpiaews.yesbooks.in/service.asmx";
	/* Secure WebService SoapAddress */
	public final static String PRODUCTION_CRYPTOGRAPHY_SOAP_ADDRESS = "http://yshpiedaws.yesbooks.in/service.asmx";
	public final static String PRODUCTION_CRYPTOGRAPHY_ENGLISH_SOAP_ADDRESS = "http://yshpiedaews.yesbooks.in/service.asmx";

	/* Orisa webservices */
	public final static String PRODUCTION_NAMESPACE_ORISA = "http://trickleupws.yesbooks.in/";
	public final static String PRODUCTION_ENGLISH_NAMESPACE_ORISA = "http://trickleupews.yesbooks.in/";

	public final static String PRODUCTION_SOAP_ADDRESS_ORISA = "http://trickleupws.yesbooks.in/service.asmx";
	public final static String PRODUCTION_ENGLISH_SOAP_ADDRESS_ORISA = "http://trickleupews.yesbooks.in/service.asmx";

	/* Testing Webservices */

	public final static String PRODUCTION_NAMESPACE_TEST = "http://tshpiaws.yesbooks.in/";
	public final static String PRODUCTION_ENGLISH_NAMESPACE_TEST = "http://tshpiaews.yesbooks.in/";

	public final static String PRODUCTION_SOAP_ADDRESS_TEST = "http://tshpiaws.yesbooks.in/service.asmx";
	public final static String PRODUCTION_ENGLISH_SOAP_ADDRESS_TEST = "http://tshpiaews.yesbooks.in/service.asmx";

	public final static String BASE_NAMESPACE = PRODUCTION_NAMESPACE;
	public final static String BASE_SOAP_ADDRESS = PRODUCTION_SOAP_ADDRESS;

	public final static String BASE_NAMESPACE_ENGLISH = PRODUCTION_ENGLISH_NAMESPACE;
	public final static String BASE_SOAP_ADDRESS_ENGLISH = PRODUCTION_ENGLISH_SOAP_ADDRESS;
	
	public static final String downloadPdfUrl = "https://com.yesteam.eshakti.nabard.org/Downloads/mobileapplication.pdf";//"http://androhub.com/demo/demo.pdf";
}
