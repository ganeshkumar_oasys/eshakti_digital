package com.yesteam.eshakti.appConstants;

public class Constants {
	public static final String FILE_PATH = "file_path";
	public static int RESULT_CANCEL = 102;
	public static int RESULT_OK = 101;
	public static String EXCEPTION = "Network Error";
	public static String BUTTON_CLICK_FLAG = "0";

	public static String FRAG_INSTANCE_CONSTANT = "0";

	public static String FRAG_BACKPRESS_CONSTANT = "0";

	public static String FRAG_INSTANCE_SAVINGS = "1";
	public static String FRAG_INSTANCE_MEMBERLOANREPAID_MENU = "2";
	public static String FRAG_INSTANCE_MEMBERLOANREPAID = "3";
	public static String FRAG_INSTANCE_PERSONALLOANREPAID = "4";
	public static String FRAG_INSTANCE_EXPENSES = "5";
	public static String FRAG_INSTANCE_INCOME_MENU = "6";
	public static String FRAG_INSTANCE_OI_S_P = "7";
	public static String FRAG_INSTANCE_DONATION_FINCOME = "8";
	public static String FRAG_INSTANCE_GROUPLOAN_REPAID_MENU = "9";
	public static String FRAG_INSTANCE_GROUPLOAN_REPAID = "10";

	public static String FRAG_INSTANCE_BANKDEPOSIT = "11";
	public static String FRAG_INSTANCE_DEPOSIT_MENU = "12";
	public static String FRAG_INSTANCE_DEPOSIT_ENTRY = "13";
	public static String FRAG_INSTANCE_FIXEDDEPOSIT_ENTRY = "14";

	public static String FRAG_INSTANCE_PL_DISBURSE = "15";

	public static String FRAG_INSTANCE_GROUP_PROFILE = "16";
	public static String FRAG_INSTANCE_GROUP_AGENT = "17";
	public static String FRAG_INSTANCE_GROUP_MEMBER_LIST = "18";

	public static String FRAG_INSTANCE_MEMBER_REPORTS_MEMBERLIST = "19";
	public static String FRAG_INSTANCE_MEMBER_REPORTS_MENU = "20";
	public static String FRAG_INSTANCE_MEMBER_REPORTS_MEMBER_SAVING_SUMM = "21";
	public static String FRAG_INSTANCE_MEMBER_REPORTS_MEMBER_LOAN_SUMM_MENU = "22";
	public static String FRAG_INSTANCE_MEMBER_REPORTS_MEMBER_LOAN_SUMM = "23";
	public static String FRAG_INSTANCE_MEMBER_REPORTS_MONTH_YEAR_PICK = "24";
	public static String FRAG_INSTANCE_MEMBER_REPORTS_MONTH_SUMM = "25";

	public static String FRAG_INSTANCE_GROUP_REPORTS_MENU = "26";
	public static String FRAG_INSTANCE_GROUP_REPORTS_SAVING_SUMM = "27";
	public static String FRAG_INSTANCE_GROUP_REPORTS_GROUP_LOAN_LIST = "28";
	public static String FRAG_INSTANCE_GROUP_REPORTS_LOAN_SUMM = "29";
	public static String FRAG_INSTANCE_GROUP_REPORTS_PERSONAL_LOAN_SUMM = "30";

	public static String FRAG_INSTANCE_BANK_TRANSACTION = "31";
	public static String FRAG_INSTANCE_BALANCE_SHEET_TRAIL = "32";
	public static String FRAG_INSTANCE_BALANCE_SHEET_REPORTS = "33";

	public static String FRAG_INSTANCE_TRAIL_BALANCE = "34";
	public static String FRAG_INSTANCE_TRAIL_BALANCE_REPORTS = "35";

	public static String FRAG_INSTANCE_BANK_BALANCE = "36";

	public static String FRAG_INSTANCE_ATTENDANCE = "37";
	public static String FRAG_INSTANCE_MINUTESOFMEETING = "38";
	public static String FRAG_INSTANCE_AUDITING = "39";
	public static String FRAG_INSTANCE_TRAINING = "40";

	public static String FRAG_INSTANCE_SETTING_CHANGE_PASSWORD = "41";

	public static String FRAG_INSTANCE_ABOUT_YESBOOKS = "42";
	public static String FRAG_INSTANCE_CONTACTS = "43";

	public static String FRAG_INSTANCE_MAIN_DASHBOARD = "44";

	public static String FRAG_INSTANCE_AADHAAR_IMAGE_MENU = "45";
	public static String FRAG_INSTANCE_PHOTO_AADHAAR_INFOMENU = "46";
	public static String FRAG_INSTANCE_AADHAAR_IMAGE_VIEW_MENU = "47";
	public static String FRAG_INSTANCE_AADHAAR_UPLOAD = "48";
	public static String FRAG_INSTANCE_IMAGE_VIEW = "49";
	public static String FRAG_INSTANCE_AADHAAR_VIEW = "50";
	public static String FRAG_INSTANCE_AADHAAR_DIALOG = "51";

	public static String NETWORKCOMMONFLAG = "SUCESS";
	public static String ONLINEFLAG = "ONLINE";
	public static String OFFLINEFLAG = "OFFLINE";

	public static String EN_DE_KEY = "!#%&@$^94271";

	public static String FRAG_INSTANCE_INTERLAONMENU = "52";
	public static String FRAG_INSTANCE_INTERLAONMENU_DISBURSE = "53";
	public static String FRAG_INSTANCE_INTERLAONMENU_MFI = "54";

	public static String FRAG_INSTANCE_LOANACCWITHDRAWAL = "55";
	public static String FRAG_INSTANCE_ACCTOACCTRANSFER = "56";
	public static String FRAG_INSTANCE_CHECKLIST = "57";

	public static String FRAG_INSTANCE_LOAN_DISBURSE_MENU = "58";
	public static String FRAG_INSTANCE_LOAN_DISBURSE_LOANS = "59";
	public static String FRAG_INSTANCE_LOAN_DISBURSE_LIMIT_LOAN_SB = "60";
	public static String FRAG_INSTANCE_LOAN_LIMIT = "61";
	public static String FRAG_INSTANCE_LOAN_GROUP_INSTALLMENT = "62";
	public static String FRAG_INSTANCE_LOAN_SB_INSTALLMENT = "63";
	public static String FRAG_INSTANCE_LOAN_SB_FINAL_DISBURSE="64";

}
