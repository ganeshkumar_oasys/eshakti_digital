package com.yesteam.eshakti.interfaces;


public interface TaskListener {
	
	void onTaskStarted();
	
	void onTaskFinished(String result);

}
