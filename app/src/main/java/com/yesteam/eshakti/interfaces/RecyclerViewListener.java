package com.yesteam.eshakti.interfaces;

import java.io.InterruptedIOException;

import android.view.View;

public interface RecyclerViewListener {

	public void recyclerViewListClicked(View view, int position)
			throws InterruptedIOException;

}
