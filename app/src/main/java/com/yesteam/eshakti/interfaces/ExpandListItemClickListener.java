package com.yesteam.eshakti.interfaces;

import android.view.View;
import android.view.ViewGroup;

public interface ExpandListItemClickListener {

	void onItemClick(ViewGroup parent, View view, int position);

	void onItemClickVerification(ViewGroup parent, View view, int position);
}
