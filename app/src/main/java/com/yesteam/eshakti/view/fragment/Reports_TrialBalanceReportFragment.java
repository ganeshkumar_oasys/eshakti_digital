package com.yesteam.eshakti.view.fragment;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.Get_TrialBalanceTask;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Reports_TrialBalanceReportFragment extends Fragment {

	private TextView mGroupName, mHeader;
	public static String serviceResponseArr[];

	public Reports_TrialBalanceReportFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_TRAIL_BALANCE_REPORTS;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_trailbalancereport, container, false);

		MainFragment_Dashboard.isBackpressed = false;

		try {
			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mHeader = (TextView) rootView.findViewById(R.id.fragment_trialbalance_headertext);
			mHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.trialBalance));
			mHeader.setTypeface(LoginActivity.sTypeface);

			serviceResponseArr = Get_TrialBalanceTask.responseArr[1].split("~");
			
			TableLayout headerTable = (TableLayout) rootView.findViewById(R.id.trialbalace_headerTable);
			TableLayout table3 = (TableLayout) rootView.findViewById(R.id.trialReportTable);

			TableRow head_row = new TableRow(getActivity());
			TableRow.LayoutParams headerParams = new TableRow.LayoutParams(0, LayoutParams.WRAP_CONTENT, 1f);

			TextView head_details = new TextView(getActivity());
			head_details.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.details)));
			head_details.setTypeface(LoginActivity.sTypeface);
			head_details.setTextColor(Color.WHITE);
			head_details.setPadding(10, 5, 10, 5);
			head_details.setBackgroundResource(R.color.tableHeader);
			head_details.setLayoutParams(headerParams);
			head_row.addView(head_details);

			TextView creditAmount = new TextView(getActivity());
			creditAmount.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.credit)));
			creditAmount.setTypeface(LoginActivity.sTypeface);
			creditAmount.setTextColor(Color.WHITE);
			creditAmount.setPadding(10, 5, 10, 5);
			creditAmount.setGravity(Gravity.RIGHT);
			creditAmount.setBackgroundResource(R.color.tableHeader);
			creditAmount.setLayoutParams(headerParams);
			head_row.addView(creditAmount);

			TextView debitAmount = new TextView(getActivity());
			debitAmount.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.debit)));
			debitAmount.setTypeface(LoginActivity.sTypeface);
			debitAmount.setTextColor(Color.WHITE);
			debitAmount.setPadding(10, 5, 10, 5);
			debitAmount.setGravity(Gravity.RIGHT);
			debitAmount.setBackgroundResource(R.color.tableHeader);
			debitAmount.setLayoutParams(headerParams);
			head_row.addView(debitAmount);

			headerTable.addView(head_row, new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
					TableLayout.LayoutParams.WRAP_CONTENT));

			try {
				for (int i = 0; i < serviceResponseArr.length - 3; i = i + 3) {
					TableRow contentRow = new TableRow(getActivity());
					/*
					 * TableRow.LayoutParams contentParams = new
					 * TableRow.LayoutParams( LayoutParams.MATCH_PARENT,
					 * LayoutParams.WRAP_CONTENT, 1f);
					 */
					TextView details = new TextView(getActivity());
					details.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(serviceResponseArr[i])));
					details.setPadding(10, 5, 10, 5);
					details.setTextColor(color.black);
					details.setLayoutParams(headerParams);// (contentParams);
					contentRow.addView(details);

					TextView credit_amount = new TextView(getActivity());
					credit_amount.setText(
							GetSpanText.getSpanString(getActivity(), String.valueOf(serviceResponseArr[i + 1])));
					credit_amount.setPadding(50, 5, 10, 5);
					credit_amount.setTextColor(color.black);
					credit_amount.setGravity(Gravity.RIGHT);
					credit_amount.setLayoutParams(headerParams);// (contentParams);
					contentRow.addView(credit_amount);

					TextView debit_amount = new TextView(getActivity());
					debit_amount.setText(
							GetSpanText.getSpanString(getActivity(), String.valueOf(serviceResponseArr[i + 2])));
					debit_amount.setPadding(10, 5, 10, 5);
					debit_amount.setTextColor(color.black);
					debit_amount.setGravity(Gravity.RIGHT);
					debit_amount.setLayoutParams(headerParams);// (contentParams);
					contentRow.addView(debit_amount);

					table3.addView(contentRow,
							new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

					View rullerView = new View(getActivity());
					rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
					rullerView.setBackgroundColor(Color.rgb(220, 220, 220));

					table3.addView(rullerView);

				}
				TableRow totalRow = new TableRow(getActivity());

				TextView total = new TextView(getActivity());
				total.setText(GetSpanText.getSpanString(getActivity(),
						String.valueOf(serviceResponseArr[serviceResponseArr.length - 3])));
				total.setPadding(10, 5, 10, 5);
				total.setTextColor(Color.BLACK);
				total.setTypeface(null, Typeface.BOLD);
				total.setLayoutParams(headerParams);// (contentParams);
				totalRow.addView(total);

				TextView credit_amount = new TextView(getActivity());
				credit_amount.setText(GetSpanText.getSpanString(getActivity(),
						String.valueOf(serviceResponseArr[serviceResponseArr.length - 2])));
				credit_amount.setPadding(50, 5, 10, 5);
				credit_amount.setTextColor(Color.BLACK);
				credit_amount.setGravity(Gravity.RIGHT);
				credit_amount.setTypeface(null, Typeface.BOLD);
				credit_amount.setLayoutParams(headerParams);// (contentParams);
				totalRow.addView(credit_amount);

				TextView debit_amount = new TextView(getActivity());
				debit_amount.setText(GetSpanText.getSpanString(getActivity(),
						String.valueOf(serviceResponseArr[serviceResponseArr.length - 1])));
				debit_amount.setPadding(10, 5, 10, 5);
				debit_amount.setTextColor(Color.BLACK);
				debit_amount.setGravity(Gravity.RIGHT);
				debit_amount.setTypeface(null, Typeface.BOLD);
				debit_amount.setLayoutParams(headerParams);// (contentParams);
				totalRow.addView(debit_amount);

				table3.addView(totalRow,
						new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

				View rullerView = new View(getActivity());
				rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
				rullerView.setBackgroundColor(Color.rgb(220, 220, 220));

				table3.addView(rullerView);

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return rootView;
	}
	
	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		MainFragment_Dashboard.isTrialBalanceReport = true;
		FragmentDrawer.isTrialBalanceReport = true;
	}
}
