package com.yesteam.eshakti.view.fragment;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.oasys.eshakti.digitization.R;
import com.squareup.otto.Subscribe;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.adapter.ExpandableListAdapter;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.model.NavDrawerItem;
import com.yesteam.eshakti.service.GroupDetailsService;
import com.yesteam.eshakti.sqlite.database.response.GetMin_MeetingResponse;
import com.yesteam.eshakti.sqlite.database.response.GroupMasterPLDB_FD_Response;
import com.yesteam.eshakti.sqlite.database.response.GroupMasterResponse;
import com.yesteam.eshakti.sqlite.db.model.GetGroupMemberDetails;
import com.yesteam.eshakti.sqlite.db.model.GroupMasterSingle;
import com.yesteam.eshakti.sqlite.db.model.Groupdetails;
import com.yesteam.eshakti.sqlite.db.model.Transaction;
import com.yesteam.eshakti.sqlite.db.transactions.TransactionManager.DataType;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.CalculateDateUtils;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.utils.DateUtils;
import com.yesteam.eshakti.utils.GetExit;
import com.yesteam.eshakti.utils.GetTypeface;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.utils.RegionalserviceUtil;
import com.yesteam.eshakti.utils.Reset;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.view.activity.GroupListActivity;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.view.activity.MainActivity;
import com.yesteam.eshakti.views.ButtonFlat;
import com.yesteam.eshakti.views.MyExpandableListview;
import com.yesteam.eshakti.webservices.DownloadPdfFileTask;
import com.yesteam.eshakti.webservices.GetMemberMobileNumberWebservices;
import com.yesteam.eshakti.webservices.Get_AadhaarNumber_Webservices;
import com.yesteam.eshakti.webservices.Get_AgentProfileTask;
import com.yesteam.eshakti.webservices.Get_All_Mem_OutstandingTask;
import com.yesteam.eshakti.webservices.Get_BankBranchName_Webservices;
import com.yesteam.eshakti.webservices.Get_BankTransactionSummaryTask;
import com.yesteam.eshakti.webservices.Get_Bank_BalanceTask;
import com.yesteam.eshakti.webservices.Get_Checklist_webservice;
import com.yesteam.eshakti.webservices.Get_GroupProfileTask;
import com.yesteam.eshakti.webservices.Get_Group_Minutes_NewTask;
import com.yesteam.eshakti.webservices.Get_MemberAccountNumber_Webservices;
import com.yesteam.eshakti.webservices.Get_Shg_Account_Details_Webservices;
import com.yesteam.eshakti.webservices.LoginTask;
import com.yesteam.eshakti.webservices.Login_webserviceTask;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;
import com.yesteam.eshakti.widget.Dialog_TransactionDate;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.core.content.FileProvider;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class FragmentDrawer extends Fragment implements TaskListener {

	private ActionBarDrawerToggle mDrawerToggle;
	public static DrawerLayout mDrawerLayout;
	private View containerView;
	@SuppressWarnings("unused")
	private FragmentDrawerListener drawerListener;
	private static TypedArray navMenuIcons;

	private static String[] sNavMenuTitles;
	private static String[] sTransactionChild;
	private static String[] sAgentProfileChild;
	private static String[] sGroupProfileChild;
	private static String[] sreportChild;
	private static String[] sMeetingChild;
	private static String[] sSettingsChild;
	private static String[] sHelpChild;

	Fragment fragment = null;
	HashMap<String, List<String>> listDataChild;
	ExpandableListAdapter listAdapter;
	List<NavDrawerItem> listDataHeader;
	private int lastExpandedPosition = -1;
	private Dialog mProgressDialog, mProgressDialog_thread;
	boolean isNavigate;
	private MyExpandableListview m_exListview;
	private boolean isGroupProfile = false;
	private boolean isAgentProfile = false;
	private boolean isBankTransactionReport = false;
	public static boolean isBalanceSheetReport = false;
	public static boolean isTrialBalanceReport = false;
	boolean isBankBalance = false, isMinutes = false;

	public static String sSubMenu_Item = "";

	boolean isLanguageSelection = false;
	public static boolean mOfflineNavigate = false;
	boolean iSChecklist = false;
	boolean isServiceCall = false;
	public static TextView drawer_CashinHand, drawer_CashatBank, drawer_lastTR_Date;
	private TextView mVersionNo;
	boolean isGetMemberMobileNumber = false;
	boolean isGetMemberAadhaarNumber = false;
	boolean isGetShgAccountNumber = false;
	boolean isGetBankBranchName = false;
	boolean isGetMemberAccountNumber = false;
	boolean isPdfDownload = false;
	boolean isSelectGroup = false;

	public FragmentDrawer() {

	}

	public void setDrawerListener(FragmentDrawerListener listener) {
		this.drawerListener = listener;
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		SBus.INST.register(this);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setMenuVisibility(false);
		EShaktiApplication.setFragmentMenuListView(true);

		mProgressDialog_thread = AppDialogUtils.createProgressDialog(getActivity());

		Meeting_TrainingFragment.trainingDate = Reset.reset(Meeting_TrainingFragment.trainingDate);

		try {

			sNavMenuTitles = new String[] { RegionalConversion.getRegionalConversion(AppStrings.transaction),
					RegionalConversion.getRegionalConversion(AppStrings.profile),
					RegionalConversion.getRegionalConversion(AppStrings.reports),
					RegionalConversion.getRegionalConversion(AppStrings.meeting),
					RegionalConversion.getRegionalConversion(AppStrings.settings),
					RegionalConversion.getRegionalConversion(AppStrings.help) };

			if (ConnectionUtils.isNetworkAvailable(getActivity())) {
				sTransactionChild = new String[] { RegionalConversion.getRegionalConversion(AppStrings.savings),
						RegionalConversion.getRegionalConversion(AppStrings.income),
						RegionalConversion.getRegionalConversion(AppStrings.expenses),
						RegionalConversion.getRegionalConversion(AppStrings.memberloanrepayment),
						RegionalConversion.getRegionalConversion(AppStrings.grouploanrepayment),
						RegionalConversion.getRegionalConversion(AppStrings.bankTransaction),
						RegionalConversion.getRegionalConversion(AppStrings.InternalLoanDisbursement),
						RegionalConversion.getRegionalConversion(AppStrings.mDefault),
						RegionalConversion.getRegionalConversion(AppStrings.mCheckList) };
			} else {
				sTransactionChild = new String[] { RegionalConversion.getRegionalConversion(AppStrings.savings),
						RegionalConversion.getRegionalConversion(AppStrings.income),
						RegionalConversion.getRegionalConversion(AppStrings.expenses),
						RegionalConversion.getRegionalConversion(AppStrings.memberloanrepayment),
						RegionalConversion.getRegionalConversion(AppStrings.grouploanrepayment),
						RegionalConversion.getRegionalConversion(AppStrings.bankTransaction),
						RegionalConversion.getRegionalConversion(AppStrings.InternalLoanDisbursement),
						RegionalConversion.getRegionalConversion(AppStrings.mDefault) };
			}

			sAgentProfileChild = new String[] { RegionalConversion.getRegionalConversion(AppStrings.mCreditLinkageInfo),
					RegionalConversion.getRegionalConversion(AppStrings.mMobileNoUpdation),
					RegionalConversion.getRegionalConversion(AppStrings.mAadhaarNoUpdation),
					RegionalConversion.getRegionalConversion(AppStrings.mAccountNoUpdation),
					RegionalConversion.getRegionalConversion(AppStrings.mSHGAccountNoUpdation),
					RegionalConversion.getRegionalConversion(AppStrings.uploadInfo),
					RegionalConversion.getRegionalConversion(AppStrings.groupProfile),
					RegionalConversion.getRegionalConversion(AppStrings.agentProfile) };

			sGroupProfileChild = new String[] { RegionalConversion.getRegionalConversion(AppStrings.groupProfile),
					RegionalConversion.getRegionalConversion(AppStrings.uploadInfo) };

			if (Boolean.valueOf(ConnectionUtils.isNetworkAvailable(getActivity()))) {

				sreportChild = new String[] { RegionalConversion.getRegionalConversion(AppStrings.Memberreports),
						RegionalConversion.getRegionalConversion(AppStrings.GroupReports),
						RegionalConversion.getRegionalConversion(AppStrings.transactionsummary),
						RegionalConversion.getRegionalConversion(AppStrings.bankBalance),
						RegionalConversion.getRegionalConversion(AppStrings.offlineReports) };
			} else if (!Boolean.valueOf(ConnectionUtils.isNetworkAvailable(getActivity()))) {
				if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
					sreportChild = new String[] { RegionalConversion.getRegionalConversion(AppStrings.offlineReports) };
				}
			}

			if (Boolean.valueOf(ConnectionUtils.isNetworkAvailable(getActivity()))) {
				sMeetingChild = new String[] { RegionalConversion.getRegionalConversion(AppStrings.Attendance),
						RegionalConversion.getRegionalConversion(AppStrings.MinutesofMeeting),
						RegionalConversion.getRegionalConversion(AppStrings.auditing),
						RegionalConversion.getRegionalConversion(AppStrings.training), "UPLOAD SCHEDULE VI" };
			} else if (!Boolean.valueOf(ConnectionUtils.isNetworkAvailable(getActivity()))) {
				if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
					sMeetingChild = new String[] { RegionalConversion.getRegionalConversion(AppStrings.Attendance),
							RegionalConversion.getRegionalConversion(AppStrings.MinutesofMeeting),
							RegionalConversion.getRegionalConversion(AppStrings.auditing),
							RegionalConversion.getRegionalConversion(AppStrings.training) };
				}
			}
			if (Boolean.valueOf(ConnectionUtils.isNetworkAvailable(getActivity()))) {

				sSettingsChild = new String[] { RegionalConversion.getRegionalConversion(AppStrings.passwordchange),
						RegionalConversion.getRegionalConversion(AppStrings.changeLanguage),
						RegionalConversion.getRegionalConversion(AppStrings.deactivateAccount),
						RegionalConversion.getRegionalConversion(AppStrings.mSendEmail) };

			} else if (!Boolean.valueOf(ConnectionUtils.isNetworkAvailable(getActivity()))) {
				if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
					sSettingsChild = new String[] {
							RegionalConversion.getRegionalConversion(AppStrings.passwordchange) };
				}

			}

			sHelpChild = new String[] { "ESHAKTI", RegionalConversion.getRegionalConversion(AppStrings.contacts),

					RegionalConversion.getRegionalConversion(AppStrings.mPdfManual) };

			Transaction_PersonalLoanRepaidFragment.memLoanCount = 0;

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// Inflating view layout
		View layout = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
		try {

			TextView drawer_GroupName = (TextView) layout.findViewById(R.id.activity_profilename);
			drawer_GroupName.setText(RegionalConversion.getRegionalConversion(SelectedGroupsTask.Group_Name));
			drawer_GroupName.setTypeface(LoginActivity.sTypeface);

			drawer_CashinHand = (TextView) layout.findViewById(R.id.activity_CashinHand);
			drawer_CashinHand.setText(RegionalConversion.getRegionalConversion(AppStrings.cashinhand)

					+ SelectedGroupsTask.sCashinHand);
			drawer_CashinHand.setTypeface(LoginActivity.sTypeface);

			drawer_CashatBank = (TextView) layout.findViewById(R.id.activity_CashatBank);
			drawer_CashatBank.setText(RegionalConversion.getRegionalConversion(AppStrings.cashatBank)

					+ SelectedGroupsTask.sCashatBank);
			drawer_CashatBank.setTypeface(LoginActivity.sTypeface);

			drawer_lastTR_Date = (TextView) layout.findViewById(R.id.activity_last_TR_Date);
			drawer_lastTR_Date.setText(RegionalConversion.getRegionalConversion(AppStrings.lastTransactionDate) + " : "
					+ SelectedGroupsTask.sLastTransactionDate_Response);
			drawer_lastTR_Date.setTypeface(LoginActivity.sTypeface);

			TextView drawer_Opening_Date = (TextView) layout.findViewById(R.id.activity_OpeningTr_Date);
			String mDate = DateUtils.getYesterdayDateString(SelectedGroupsTask.sBalanceSheetDate_Response);

			drawer_Opening_Date
					.setText(RegionalConversion.getRegionalConversion(AppStrings.mOpeningDate) + " : " + mDate);
			drawer_Opening_Date.setTypeface(LoginActivity.sTypeface);
			TextView drawer_animator_name = (TextView) layout.findViewById(R.id.fragment_animator_name);

			// drawer_animator_name.setText(Login_webserviceTask.mAnimatorName +
			// " " + LoginTask.UserName);
			drawer_animator_name.setText(PrefUtils.getAnimatorName() + "    " + PrefUtils.getAgentUserName());
			drawer_animator_name.setTypeface(LoginActivity.sTypeface);

			mDrawerLayout = (DrawerLayout) layout.findViewById(R.id.drawer_layout);

			navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);

			m_exListview = (MyExpandableListview) layout.findViewById(R.id.fragment_expandablelistview);

			prepareListData();
			listAdapter = new ExpandableListAdapter(getActivity(), listDataHeader, listDataChild);
			// setting list adapter
			m_exListview.setAdapter(listAdapter);
			fragment = new MainFragment_Dashboard();
			fragment.setRetainInstance(true);
			getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame, fragment).commit();
			m_exListview.setOnGroupClickListener(new OnGroupClickListener() {

				@Override
				public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
					// TODO Auto-generated method stub
					switch (groupPosition) {
					case 6:

						try {
							if (Boolean.valueOf(EShaktiApplication.isAgent)) {
								try {

									SelectedGroupsTask.loan_Name.clear();
									SelectedGroupsTask.loan_Id.clear();
									SelectedGroupsTask.loan_EngName.clear();
									SelectedGroupsTask.member_Id.clear();
									SelectedGroupsTask.member_Name.clear();
									SelectedGroupsTask.sBankNames.clear();
									SelectedGroupsTask.sEngBankNames.clear();
									SelectedGroupsTask.sBankAmt.clear();
									SelectedGroupsTask.sCashatBank = " ";
									SelectedGroupsTask.sCashinHand = " ";

									EShaktiApplication.setSelectedgrouptask(false);
									EShaktiApplication.setPLOS(false);
									EShaktiApplication.setBankDeposit(false);
									EShaktiApplication.setOfflineTrans(false);
									EShaktiApplication.setOfflineTransDate(false);
									EShaktiApplication.setLastTransId("");
									EShaktiApplication.setSetTransValues("");

									Constants.BUTTON_CLICK_FLAG = "0";
									EShaktiApplication.setSubmenuclicked(false);
									Constants.FRAG_INSTANCE_CONSTANT = "0";
									EShaktiApplication.setOnSavedFragment(false);

								} catch (Exception e) {
									// TODO: handle exception
									e.printStackTrace();
								}

								if (ConnectionUtils.isNetworkAvailable(getActivity())) {
									PrefUtils.setLoginGroupService("2");
									isSelectGroup = true;
									new Login_webserviceTask(FragmentDrawer.this).execute();
								} else {
									startActivity(new Intent(getActivity(), GroupListActivity.class));
									getActivity().overridePendingTransition(R.anim.right_to_left_in,
											R.anim.right_to_left_out);
									getActivity().finish();
								}

							} else {

								startActivity(new Intent(GetExit.getExitIntent(getActivity())));
								getActivity().finish();

							}

						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}

						break;
					case 7:
						try {
							startActivity(new Intent(GetExit.getExitIntent(getActivity())));
							getActivity().finish();

						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}
						break;
					default:
						break;
					}
					return false;
				}
			});

			onSetCalendarValues();
			m_exListview.setOnChildClickListener(new OnChildClickListener() {

				@Override
				public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition,
						long id) {
					switch (groupPosition) {
					case 0:
						switch (childPosition) {

						case 0:

							Transaction_InternalLoan_DisbursementFragment.isPL_Disburse_Submit = false;

							sSubMenu_Item = "1";
							try {
								FragmentManager fm = getChildFragmentManager();
								Dialog_TransactionDate dialog = new Dialog_TransactionDate(getActivity(),
										AppStrings.savings);
								dialog.show(fm, "");

							} catch (Exception e) {
								e.printStackTrace();
							}
							if (EShaktiApplication.isDefault()) {
								EShaktiApplication.setDefault(false);
							} else {
								EShaktiApplication.setDefault(false);
							}
							break;
						case 3:

							sSubMenu_Item = "2";
							try {
								FragmentManager fm = getChildFragmentManager();

								Dialog_TransactionDate dialog = new Dialog_TransactionDate(getActivity(),
										AppStrings.memberloanrepayment);
								dialog.show(fm, "");
							} catch (Exception e) {
								e.printStackTrace();
							}
							if (EShaktiApplication.isDefault()) {
								EShaktiApplication.setDefault(false);
							}
							break;
						case 2:

							sSubMenu_Item = "3";
							try {
								FragmentManager fm = getChildFragmentManager();

								Dialog_TransactionDate dialog = new Dialog_TransactionDate(getActivity(),
										AppStrings.expenses);
								dialog.show(fm, "");
							} catch (Exception e) {
								e.printStackTrace();
							}
							if (EShaktiApplication.isDefault()) {
								EShaktiApplication.setDefault(false);
							}
							break;

						case 1:
							// INCOME

							sSubMenu_Item = "4";
							try {
								FragmentManager fm = getChildFragmentManager();

								Dialog_TransactionDate dialog = new Dialog_TransactionDate(getActivity(),
										AppStrings.income);
								dialog.show(fm, "");
							} catch (Exception e) {
								e.printStackTrace();
							}

							break;

						case 4:
							// GroupLoan Repaid
							int size = SelectedGroupsTask.loan_Name.size() - 1;
							System.out.println("Group Loan Size:>>>" + size);
							if (size != 0) {

								sSubMenu_Item = "5";
								try {
									FragmentManager fm = getChildFragmentManager();

									Dialog_TransactionDate dialog = new Dialog_TransactionDate(getActivity(),
											AppStrings.grouploanrepayment);
									dialog.show(fm, "");
								} catch (Exception e) {
									e.printStackTrace();
								}

							} else {

								TastyToast.makeText(getActivity(), AppStrings.noGroupLoan_Alert,
										TastyToast.LENGTH_SHORT, TastyToast.WARNING);
							}
							if (EShaktiApplication.isDefault()) {
								EShaktiApplication.setDefault(false);
							}
							break;

						case 5:
							// BankDeposit
							sSubMenu_Item = "6";
							try {
								FragmentManager fm = getChildFragmentManager();

								Dialog_TransactionDate dialog = new Dialog_TransactionDate(getActivity(),
										AppStrings.bankTransaction);
								dialog.show(fm, "");
							} catch (Exception e) {
								e.printStackTrace();
							}
							if (EShaktiApplication.isDefault()) {
								EShaktiApplication.setDefault(false);
							}

							break;

						case 6:
							try {

								Transaction_SavingsFragment.isSavings = false;
								// sSubMenu_Item = "7";

								Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID = "0";

								if (Boolean.valueOf(ConnectionUtils.isNetworkAvailable(getActivity()))) {

									if (PrefUtils.getGroupMasterResValues() != null
											&& PrefUtils.getGroupMasterResValues().equals("1")) {

										EShaktiApplication.getInstance().getTransactionManager().startTransaction(
												DataType.GROUP_MASTER_PLDB_FD,
												new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));

									} else {
										if (!isServiceCall) {

											isServiceCall = true;
											new Get_All_Mem_OutstandingTask(FragmentDrawer.this).execute();

										}
									}

								} else {
									// Do offline stuffs
									if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
										EShaktiApplication.getInstance().getTransactionManager().startTransaction(
												DataType.GROUP_MASTER_PLDB_FD,
												new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));
									} else {

										TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF,
												TastyToast.LENGTH_SHORT, TastyToast.ERROR);
									}

								}

							} catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();

								TastyToast.makeText(getActivity(), "Time Out Exception ", TastyToast.LENGTH_SHORT,
										TastyToast.ERROR);
								getActivity().finish();
							}
							isNavigate = true;
							if (EShaktiApplication.isDefault()) {
								EShaktiApplication.setDefault(false);
							}
							break;
						case 7:

							try {
								sSubMenu_Item = "12";
								EShaktiApplication.setDefault(true);
								FragmentManager fm = getChildFragmentManager();

								Dialog_TransactionDate dialog = new Dialog_TransactionDate(getActivity(),
										AppStrings.mDefault);
								dialog.show(fm, "");
							} catch (Exception e) {
								e.printStackTrace();
							}

							break;
						case 8:
							// Check list
							if (ConnectionUtils.isNetworkAvailable(getActivity())) {

								iSChecklist = true;
								new Get_Checklist_webservice(FragmentDrawer.this).execute();
							}
							break;

						default:
							break;
						}

						break;

					case 1: // PROFILE

						MainFragment_Dashboard.isProfile = true;
						if (Boolean.valueOf(EShaktiApplication.isAgent)) {
							switch (childPosition) {

							case 0:
								if (ConnectionUtils.isNetworkAvailable(getActivity())) {
									fragment = new Profile_CreditLinkageInfoFragment();
								} else {

									TastyToast.makeText(getActivity(), AppStrings.mCreditLinkageAlert,
											TastyToast.LENGTH_SHORT, TastyToast.ERROR);

								}
								break;
							case 1:

								if (ConnectionUtils.isNetworkAvailable(getActivity())) {
									isGetMemberMobileNumber = true;
									new GetMemberMobileNumberWebservices(FragmentDrawer.this).execute();

								} else {
									TastyToast.makeText(getActivity(), AppStrings.mMobileNoUpdationNetworkCheck,
											TastyToast.LENGTH_SHORT, TastyToast.ERROR);
								}

								break;

							case 2:

								if (ConnectionUtils.isNetworkAvailable(getActivity())) {
									isGetMemberAadhaarNumber = true;
									new Get_AadhaarNumber_Webservices(FragmentDrawer.this).execute();

								} else {
									TastyToast.makeText(getActivity(), AppStrings.mAadhaarNoNetworkCheck,
											TastyToast.LENGTH_SHORT, TastyToast.ERROR);
								}
								break;
							case 3:

								if (ConnectionUtils.isNetworkAvailable(getActivity())) {
									isGetBankBranchName = true;
									new Get_BankBranchName_Webservices(FragmentDrawer.this).execute();

								} else {
									TastyToast.makeText(getActivity(), AppStrings.mAccNoNetworkCheck,
											TastyToast.LENGTH_SHORT, TastyToast.ERROR);
								}
								break;
							case 4:

								if (ConnectionUtils.isNetworkAvailable(getActivity())) {
									isGetShgAccountNumber = true;
									new Get_Shg_Account_Details_Webservices(FragmentDrawer.this).execute();

								} else {
									TastyToast.makeText(getActivity(), AppStrings.mShgAccNoNetworkCheck,
											TastyToast.LENGTH_SHORT, TastyToast.ERROR);
								}
								break;

							case 5:
								fragment = new MemberList_Fragment();
								break;

							case 6:

								if (Boolean.valueOf(ConnectionUtils.isNetworkAvailable(getActivity()))) {

									try {
										new Get_GroupProfileTask(FragmentDrawer.this).execute();
										Thread.sleep(2000);
									} catch (Exception e) {
										// TODO: handle exception
										e.printStackTrace();
										TastyToast.makeText(getActivity(), "Time Out Exception ",
												TastyToast.LENGTH_SHORT, TastyToast.ERROR);

										getActivity().finish();
									}

								} else {
									// Do offline Stuffs
									if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
										EShaktiApplication.getInstance().getTransactionManager().startTransaction(
												DataType.GROUPDETAILS_GETMASTER,
												new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));
									} else {

										TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF,
												TastyToast.LENGTH_SHORT, TastyToast.ERROR);
									}

								}

								isGroupProfile = true;

								break;

							case 7:
								try {

									if (Boolean.valueOf(ConnectionUtils.isNetworkAvailable(getActivity()))) {

										new Get_AgentProfileTask(FragmentDrawer.this).execute();
										Thread.sleep(2000);

									} else {
										if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
											EShaktiApplication.getInstance().getTransactionManager().startTransaction(
													DataType.GROUPDETAILS_GETMASTER,
													new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));
										} else {

											TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF,
													TastyToast.LENGTH_SHORT, TastyToast.ERROR);
										}
									}
								} catch (Exception e) {
									// TODO: handle exception
									e.printStackTrace();
									TastyToast.makeText(getActivity(), "Time Out Exception ", TastyToast.LENGTH_SHORT,
											TastyToast.ERROR);

									getActivity().finish();
								}

								isAgentProfile = true;

								break;

							default:
								break;
							}
						} else if (!Boolean.valueOf(EShaktiApplication.isAgent)) {
							switch (childPosition) {

							case 0:

								if (Boolean.valueOf(ConnectionUtils.isNetworkAvailable(getActivity()))) {

									try {
										new Get_GroupProfileTask(FragmentDrawer.this).execute();
										Thread.sleep(2000);
									} catch (Exception e) {
										// TODO: handle exception
										e.printStackTrace();
										TastyToast.makeText(getActivity(), "Time Out Exception ",
												TastyToast.LENGTH_SHORT, TastyToast.ERROR);
										getActivity().finish();
									}

								} else {
									// Do offline Stuffs
									if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
										EShaktiApplication.getInstance().getTransactionManager().startTransaction(
												DataType.GROUPDETAILS_GETMASTER,
												new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));
									} else {

										TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF,
												TastyToast.LENGTH_SHORT, TastyToast.ERROR);
									}

								}

								isGroupProfile = true;

								break;

							case 1:
								fragment = new MemberList_Fragment();
								break;
							default:
								break;
							}

						}

						break;

					case 2: // REPORTS

						MainFragment_Dashboard.isProfile = false;

						// if
						// (Boolean.valueOf(ConnectionUtils.isNetworkAvailable(getActivity())))
						// {
						switch (childPosition) {
						case 0:
							if (EShaktiApplication.getLoginFlag().equals(Constants.ONLINEFLAG)) {
								fragment = new MemberList_Fragment();
							} else if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
								mOfflineNavigate = true;
								EShaktiApplication.setOfflineTransDate(true);
								EShaktiApplication.getInstance().getTransactionManager()
										.startTransaction(DataType.GET_TRANS_MASTERVALUES, null);
							}
							break;
						case 1:
							fragment = new Reports_GroupMenuReportFragment();
							break;
						case 2:
							try {

								if (Boolean.valueOf(ConnectionUtils.isNetworkAvailable(getActivity()))) {

									new Get_BankTransactionSummaryTask(FragmentDrawer.this).execute();
									Thread.sleep(2000);

								} else if (!Boolean.valueOf(ConnectionUtils.isNetworkAvailable(getActivity()))) {
									// Do offline Stuffs
									if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {

									} else {

										TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF,
												TastyToast.LENGTH_SHORT, TastyToast.ERROR);
									}

								}
							} catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();
								TastyToast.makeText(getActivity(), "Time Out Exception ", TastyToast.LENGTH_SHORT,
										TastyToast.ERROR);
								getActivity().finish();
							}
							isBankTransactionReport = true;
							break;
						case 3:
							isBankBalance = true;
							try {

								if (Boolean.valueOf(ConnectionUtils.isNetworkAvailable(getActivity()))) {

									new Get_Bank_BalanceTask(FragmentDrawer.this).execute();

								} else if (!Boolean.valueOf(ConnectionUtils.isNetworkAvailable(getActivity()))) {
									// Do Offline Stuffs
									if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {

									} else {

										TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF,
												TastyToast.LENGTH_SHORT, TastyToast.ERROR);
									}
								}
							} catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();
							}

							break;
						case 4:
							mOfflineNavigate = true;
							EShaktiApplication.setOfflineTransDate(true);
							EShaktiApplication.getInstance().getTransactionManager()
									.startTransaction(DataType.GET_TRANS_MASTERVALUES, null);
							break;
						default:
							break;
						}
						break;
					case 3: // MEETING
						switch (childPosition) {
						case 0:
							sSubMenu_Item = "8";
							try {
								FragmentManager fm = getChildFragmentManager();

								Dialog_TransactionDate dialog = new Dialog_TransactionDate(getActivity(),
										AppStrings.Attendance);
								dialog.show(fm, "");
							} catch (Exception e) {
								e.printStackTrace();
							}
							break;
						case 1:
							if (Boolean.valueOf(ConnectionUtils.isNetworkAvailable(getActivity()))) {
								try {

									new Get_Group_Minutes_NewTask(FragmentDrawer.this).execute();

								} catch (Exception e) {
									e.printStackTrace();
								}

								isMinutes = true;
							} else if (!Boolean.valueOf(ConnectionUtils.isNetworkAvailable(getActivity()))) {
								// Do offline stuffs

								if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
									EShaktiApplication.getInstance().getTransactionManager().startTransaction(
											DataType.GET_MASTER_MIN_MEETING,
											new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));
								} else {

									TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF,
											TastyToast.LENGTH_SHORT, TastyToast.ERROR);
								}
							}
							break;
						case 2:
							Meeting_AuditingFragment.auditFromDate = Reset
									.reset(Meeting_AuditingFragment.auditFromDate);
							Meeting_AuditingFragment.auditToDate = Reset.reset(Meeting_AuditingFragment.auditToDate);
							Meeting_AuditingFragment.auditingDate = Reset.reset(Meeting_AuditingFragment.auditingDate);
							Meeting_AuditingFragment.auditorName = Reset.reset(Meeting_AuditingFragment.auditorName);
							sSubMenu_Item = "10";

							FragmentManager fm = getChildFragmentManager();

							Dialog_TransactionDate dialog = new Dialog_TransactionDate(getActivity(),
									AppStrings.auditing);
							dialog.show(fm, "");

							break;
						case 3:
							try {
								sSubMenu_Item = "11";
								Meeting_TrainingFragment.trainingDate = Reset
										.reset(Meeting_TrainingFragment.trainingDate);
								FragmentManager fManager = getChildFragmentManager();

								Dialog_TransactionDate dialog_Training = new Dialog_TransactionDate(getActivity(),
										AppStrings.training);
								dialog_Training.show(fManager, "");
							} catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();
							}
							break;

						case 4:
							if (ConnectionUtils.isNetworkAvailable(getActivity())) {
								fragment = new Meeting_Upload_Schedule_form();
								setFragment(fragment);
							} else {
								TastyToast.makeText(getActivity(), AppStrings.mUploadScheduleAlert,
										TastyToast.LENGTH_SHORT, TastyToast.ERROR);
							}
							break;
						default:
							break;
						}
						if (EShaktiApplication.isDefault()) {
							EShaktiApplication.setDefault(false);
						}
						break;

					case 4: // SETTINGS
						switch (childPosition) {
						case 0:
							if (ConnectionUtils.isNetworkAvailable(getActivity())) {
								fragment = new Settings_ChangePasswordFragment();
							} else {
								if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {

									TastyToast.makeText(getActivity(), AppStrings.offline_ChangePwdAlert,
											TastyToast.LENGTH_SHORT, TastyToast.WARNING);
								} else {

									TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF,
											TastyToast.LENGTH_SHORT, TastyToast.ERROR);
								}
							}

							break;
						case 1:
							if (ConnectionUtils.isNetworkAvailable(getActivity())) {

								final Dialog ChangeLanguageDialog = new Dialog(getActivity());

								LayoutInflater li = (LayoutInflater) getActivity()
										.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
								final View dialogView = li.inflate(R.layout.dialog_choose_language, null, false);

								TextView confirmationHeader = (TextView) dialogView
										.findViewById(R.id.chooseLanguageHeader);
								confirmationHeader
										.setText(RegionalConversion.getRegionalConversion(AppStrings.chooseLanguage));
								confirmationHeader.setTypeface(LoginActivity.sTypeface);
								final RadioGroup radioGroup = (RadioGroup) dialogView.findViewById(R.id.radioLanguage);
								RadioButton radioButton = (RadioButton) dialogView.findViewById(R.id.radioEnglish);
								RadioButton radioButton_reg = (RadioButton) dialogView.findViewById(R.id.radioRegional);
								radioButton.setText("English");
								radioButton.setTypeface(LoginActivity.sTypeface);

								if (LoginTask.User_RegLanguage.equals("Hindi")) {
									radioButton_reg.setText("हिंदी");
									Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),
                                            "font/MANGAL.TTF");
									radioButton_reg.setTypeface(typeface);
								} else if (LoginTask.User_RegLanguage.equals("Marathi")) {
									radioButton_reg.setText("मराठी");
									Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),
                                            "font/MANGALHindiMarathi.TTF");
									radioButton_reg.setTypeface(typeface);
								} else if (LoginTask.User_RegLanguage.equals("Kannada")) {
									radioButton_reg.setText("ಕನ್ನಡ");
									Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),
                                            "font/tungaKannada.ttf");
									radioButton_reg.setTypeface(typeface);
								} else if (LoginTask.User_RegLanguage.equals("Malayalam")) {
									radioButton_reg.setText("മലയാളം");
									Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),
                                            "font/MLKR0nttMalayalam.ttf");
									radioButton_reg.setTypeface(typeface);
								} else if (LoginTask.User_RegLanguage.equals("Punjabi")) {
									radioButton_reg.setText("ਪੰਜਾਬੀ ");
									Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),
                                            "font/mangal-1361510185.ttf");
									radioButton_reg.setTypeface(typeface);
								} else if (LoginTask.User_RegLanguage.equals("Gujarathi")) {
									radioButton_reg.setText("ગુજરાતી");
									Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),
                                            "font/shrutiGujarathi.ttf");
									radioButton_reg.setTypeface(typeface);
								} else if (LoginTask.User_RegLanguage.equals("Bengali")) {
									radioButton_reg.setText("বাঙ্গালী");
									Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),
                                            "font/kalpurushBengali.ttf");
									radioButton_reg.setTypeface(typeface);
								} else if (LoginTask.User_RegLanguage.equals("Tamil")) {
									radioButton_reg.setText("தமிழ்");
									Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),
                                            "font/TSCu_SaiIndira.ttf");
									radioButton_reg.setTypeface(typeface);
								} else if (LoginTask.User_RegLanguage.equals("Assamese")) {
									radioButton_reg.setText("Assamese");
									Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),
                                            "font/KirtanUni_Assamese.ttf");
									radioButton_reg.setTypeface(typeface);
								} else {
									radioButton_reg.setText(
											RegionalConversion.getRegionalConversion(LoginTask.User_RegLanguage));
									Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),
                                            "font/Exo-Medium.ttf");
									radioButton_reg.setTypeface(typeface);
								}

								// radioButton_reg.setTypeface(LoginActivity.sTypeface);

								ButtonFlat okButton = (ButtonFlat) dialogView.findViewById(R.id.dialog_yes_button);
								okButton.setText(RegionalConversion.getRegionalConversion(AppStrings.dialogOk));
								okButton.setTypeface(LoginActivity.sTypeface);
								okButton.setOnClickListener(new OnClickListener() {

									@Override
									public void onClick(View v) {
										// TODO Auto-generated
										// method stub

										int selectedId = radioGroup.getCheckedRadioButtonId();

										// find the radiobutton by
										// returned id
										RadioButton radioLanguageButton = (RadioButton) dialogView
												.findViewById(selectedId);

										String mSelectedLang = radioLanguageButton.getText().toString();
										Log.v("On Selected language", mSelectedLang);
										if (mSelectedLang.equals("हिंदी")) {
											mSelectedLang = "Hindi";
										} else if (mSelectedLang.equals("मराठी")) {
											mSelectedLang = "Marathi";
										} else if (mSelectedLang.equals("ಕನ್ನಡ")) {
											mSelectedLang = "Kannada";
										} else if (mSelectedLang.equals("മലയാളം")) {
											mSelectedLang = "Malayalam";
										} else if (mSelectedLang.equals("ਪੰਜਾਬੀ ")) {
											mSelectedLang = "Punjabi";
										} else if (mSelectedLang.equals("ગુજરાતી")) {
											mSelectedLang = "Gujarathi";
										} else if (mSelectedLang.equals("বাঙ্গালী")) {
											mSelectedLang = "Bengali";
										} else if (mSelectedLang.equals("தமிழ்")) {
											mSelectedLang = "Tamil";
										} else if (mSelectedLang.equals("Assamese")) {
											mSelectedLang = "Assamese";
										}
										try {
											PrefUtils.setUserlangcode(mSelectedLang);
										} catch (Exception e) {
											e.printStackTrace();
										}

										LoginActivity.sTypeface = GetTypeface.getTypeface(getActivity(), mSelectedLang);

										RegionalserviceUtil.getRegionalService(getActivity(), mSelectedLang);
										try {

											SelectedGroupsTask.member_Id.clear();
											SelectedGroupsTask.member_Name.clear();
											SelectedGroupsTask.loan_EngName.clear();
											SelectedGroupsTask.loan_Id.clear();
											SelectedGroupsTask.loan_Name.clear();
											SelectedGroupsTask.sBankNames.clear();
											SelectedGroupsTask.sBankAmt.clear();
											SelectedGroupsTask.sEngBankNames.clear();
											SelectedGroupsTask.sCashatBank = "";
											SelectedGroupsTask.sCashinHand = "";
											EShaktiApplication.setSelectedgrouptask(false);
											EShaktiApplication.setPLOS(false);
											EShaktiApplication.setBankDeposit(false);
											EShaktiApplication.setOfflineTrans(false);
											EShaktiApplication.setOfflineTransDate(false);
											EShaktiApplication.setLastTransId("");
											EShaktiApplication.setSetTransValues("");

										} catch (Exception e) {
											e.printStackTrace();
										}
										try {
											PrefUtils.clearGroup_Offlinedata();
											PrefUtils.setLoginGroupService("1");
											EShaktiApplication.setIsChangeLanguage(true);
											EShaktiApplication.getInstance().getTransactionManager()
													.startTransaction(DataType.GROUPDETAILSDELETE, null);

											Thread.sleep(300);

											new Login_webserviceTask(FragmentDrawer.this).execute();
											Thread.sleep(2000);
										} catch (InterruptedException e) {
											e.printStackTrace();

										}
										isLanguageSelection = true;
										ChangeLanguageDialog.dismiss();
									}
								});

								ChangeLanguageDialog.getWindow()
										.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
								ChangeLanguageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
								ChangeLanguageDialog.setCanceledOnTouchOutside(false);
								ChangeLanguageDialog.setContentView(dialogView);
								ChangeLanguageDialog.setCancelable(true);
								ChangeLanguageDialog.show();
							} else {
								if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
									TastyToast.makeText(getActivity(), "Plz Go to Online", TastyToast.LENGTH_SHORT,
											TastyToast.ERROR);
								} else {

									TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF,
											TastyToast.LENGTH_SHORT, TastyToast.ERROR);
								}
							}
							break;

						case 2:
							if (Boolean.valueOf(ConnectionUtils.isNetworkAvailable(getActivity()))) {
								fragment = new Settings_DeactivateAccountFragment();
							} else if (Boolean.valueOf(!ConnectionUtils.isNetworkAvailable(getActivity()))) {
								if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {

									TastyToast.makeText(getActivity(), AppStrings.deactivateNetworkAlert,
											TastyToast.LENGTH_SHORT, TastyToast.WARNING);
								} else {

									TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF,
											TastyToast.LENGTH_SHORT, TastyToast.ERROR);
								}
							}
							break;
						case 3:
							if (ConnectionUtils.isNetworkAvailable(getActivity())) {
								// AppDialogUtils.showSendEMailAlertDialog(getActivity());
								AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
								builder.setMessage("Do you want to send an email with database attachment?.");

								String positiveText = "YES";
								builder.setPositiveButton(positiveText, new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										// positive button logic
										sendFile(getActivity());
									}
								});

								String negativeText = "NO";
								builder.setNegativeButton(negativeText, new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										// negative button logic
										dialog.dismiss();
									}
								});

								AlertDialog dialog = builder.create();
								// display dialog
								dialog.show();

								TextView dialogMessage = (TextView) dialog.findViewById(android.R.id.message);
								dialogMessage.setTypeface(LoginActivity.sTypeface);

								Button yesButton = (Button) dialog.findViewById(android.R.id.button1);
								yesButton.setTypeface(LoginActivity.sTypeface);

								Button noButton = (Button) dialog.findViewById(android.R.id.button2);
								noButton.setTypeface(LoginActivity.sTypeface);
							} else {
								TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg,
										TastyToast.LENGTH_SHORT, TastyToast.WARNING);
							}
							break;
						default:
							break;
						}
						break;

					case 5: // HELP
						switch (childPosition) {
						case 0:
							fragment = new Helps_AboutYesbooksFragment();
							break;
						case 1:
							fragment = new Helps_ContactsFragment();
							break;
						case 2:
							File file = new File(Environment.getExternalStorageDirectory(), "mobileapplication.pdf");
							if (file.exists()) {
								CopyReadAssets();
							} else {
								if (ConnectionUtils.isNetworkAvailable(getActivity())) {
									isPdfDownload = true;
									new DownloadPdfFileTask(FragmentDrawer.this).execute();
								} else {
									TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg,
											TastyToast.LENGTH_SHORT, TastyToast.ERROR);
								}
							}
							break;

						}
						break;

					default:
						break;
					}

					setFragment(fragment);
					mDrawerLayout.closeDrawer(containerView);
					return false;
				}
			});
			try {
				PackageManager manager = getActivity().getPackageManager();
				PackageInfo info = manager.getPackageInfo(getActivity().getPackageName(), 0);
				String version = info.versionName;
				mVersionNo = (TextView) layout.findViewById(R.id.activity_fragment_drawer_footer);
				mVersionNo.setTypeface(LoginActivity.sTypeface);
				mVersionNo.setText("VERSION   " + version);
				mVersionNo.setTextSize(15);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			m_exListview.setOnGroupExpandListener(new OnGroupExpandListener() {

				@Override
				public void onGroupExpand(int groupPosition) {
					if (lastExpandedPosition != -1 && groupPosition != lastExpandedPosition) {
						m_exListview.collapseGroup(lastExpandedPosition);
					}
					lastExpandedPosition = groupPosition;

				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}

		return layout;
	}

	@SuppressWarnings("deprecation")
	private void CopyReadAssets() {

		File file = new File(Environment.getExternalStorageDirectory(), "mobileapplication.pdf");
		System.out.println("Pdf file Path  =  " + file + "");
		try {

			Intent intent = new Intent(Intent.ACTION_VIEW);
			Uri fileUri = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", file);
			intent.setDataAndType(fileUri, "application/pdf");
			intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
			intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

			Intent intent1 = Intent.createChooser(intent, "Open File");

			PackageManager pm = getActivity().getPackageManager();
			if (intent.resolveActivity(pm) != null) {
				startActivity(intent1);
			}

		} catch (ActivityNotFoundException e) {
			// TODO: handle exception
			TastyToast.makeText(getActivity(), "No pdf viewer installed, please install any pdf viewer.",
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
		}
	}

	@SuppressWarnings("deprecation")
	public void setUp(int fragmentId, DrawerLayout drawerLayout, final Toolbar toolbar) {
		containerView = getActivity().findViewById(fragmentId);
		mDrawerLayout = drawerLayout;
		mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.drawer_open,
				R.string.drawer_close) {
			@Override
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				getActivity().invalidateOptionsMenu();
			}

			@Override
			public void onDrawerClosed(View drawerView) {
				super.onDrawerClosed(drawerView);
				getActivity().invalidateOptionsMenu();
				EShaktiApplication.setFragmentMenuListView(false);
			}

			@Override
			public void onDrawerSlide(View drawerView, float slideOffset) {
				super.onDrawerSlide(drawerView, slideOffset);
				toolbar.setAlpha(1 - slideOffset / 2);
			}
		};

		mDrawerLayout.setDrawerListener(mDrawerToggle);
		mDrawerLayout.post(new Runnable() {
			@Override
			public void run() {
				mDrawerToggle.syncState();
			}
		});

	}

	private void prepareListData() {

		try {

			listDataHeader = new ArrayList<NavDrawerItem>();
			listDataChild = new HashMap<String, List<String>>();

			// Adding Parent Items
			listDataHeader.add(new NavDrawerItem(sNavMenuTitles[0], navMenuIcons.getResourceId(0, -1)));
			listDataHeader.add(new NavDrawerItem(sNavMenuTitles[1], navMenuIcons.getResourceId(1, -1)));
			listDataHeader.add(new NavDrawerItem(sNavMenuTitles[2], navMenuIcons.getResourceId(2, -1)));
			listDataHeader.add(new NavDrawerItem(sNavMenuTitles[3], navMenuIcons.getResourceId(3, -1)));
			listDataHeader.add(new NavDrawerItem(sNavMenuTitles[4], navMenuIcons.getResourceId(4, -1)));
			listDataHeader.add(new NavDrawerItem(sNavMenuTitles[5], navMenuIcons.getResourceId(5, -1)));
			if (Boolean.valueOf(EShaktiApplication.isAgent) == true) {
				listDataHeader.add(new NavDrawerItem(RegionalConversion.getRegionalConversion(AppStrings.groupList),
						navMenuIcons.getResourceId(6, -1)));
			}
			listDataHeader.add(new NavDrawerItem(RegionalConversion.getRegionalConversion(AppStrings.logOut),
					navMenuIcons.getResourceId(7, -1)));

			// Adding child Items
			listDataChild.put(sNavMenuTitles[0], Arrays.asList(sTransactionChild)); // Header,
																					// Child
																					// data

			if (Boolean.valueOf(EShaktiApplication.isAgent)) {
				listDataChild.put(sNavMenuTitles[1], Arrays.asList(sAgentProfileChild));
			} else {
				listDataChild.put(sNavMenuTitles[1], Arrays.asList(sGroupProfileChild));
			}
			listDataChild.put(sNavMenuTitles[2], Arrays.asList(sreportChild));
			listDataChild.put(sNavMenuTitles[3], Arrays.asList(sMeetingChild));
			listDataChild.put(sNavMenuTitles[4], Arrays.asList(sSettingsChild));
			listDataChild.put(sNavMenuTitles[5], Arrays.asList(sHelpChild));
			if (Boolean.valueOf(EShaktiApplication.isAgent) == true) {
				listDataChild.put(AppStrings.groupList, new ArrayList<String>());
			}
			listDataChild.put(AppStrings.logOut, new ArrayList<String>());

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static interface ClickListener {
		public void onClick(View view, int position);

		public void onLongClick(View view, int position);
	}

	public interface FragmentDrawerListener {
		public void onDrawerItemSelected(View view, int position);
	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub
		mProgressDialog = AppDialogUtils.createProgressDialog(getActivity());
		mProgressDialog.show();
	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub

		if (mProgressDialog != null) {

			mProgressDialog.dismiss();
			if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {
				getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						if (!result.equals("FAIL")) {

							TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg,
									TastyToast.LENGTH_SHORT, TastyToast.ERROR);

							Constants.NETWORKCOMMONFLAG = "SUCCESS";
						}
					}
				});
			} else {

				if (isPdfDownload) {
					isPdfDownload = false;
					CopyReadAssets();
				} else {

					if (iSChecklist) {
						iSChecklist = false;
						Transaction_CheckListFragment checkListFragment = new Transaction_CheckListFragment();
						setFragment(checkListFragment);

					} else {
						if (Boolean.valueOf(isLanguageSelection)) {
							if (Boolean.valueOf(EShaktiApplication.isAgent)) {

								mProgressDialog_thread.show();
								new Thread(new Runnable() {

									@Override
									public void run() {
										// TODO Auto-generated method stub
										EShaktiApplication.getInstance().getTransactionManager()
												.startTransaction(DataType.GROUPNAMEDELETE, null);

										final String mGroupDetails = Login_webserviceTask.mViewGroupValues;
										String arr[] = Login_webserviceTask.mViewGroupValues.split("#");

										final String mTrainerId = arr[0];
										final String mNgoId = arr[1];
										Log.e("Trainer ID", mTrainerId);
										Log.e("NGO ID ", mNgoId);

										try {
											for (int i = 0; i < Login_webserviceTask.sAgent_Gname.size(); i++) {

												final String mGroupName = Login_webserviceTask.sAgent_Gname.elementAt(i)
														.toString();
												final String mGroupId = Login_webserviceTask.sAgent_GIDs.elementAt(i)
														.toString();
												String mLastTransDate = Login_webserviceTask.sSHG_TransactionDate
														.elementAt(i).toString();
												EShaktiApplication.getInstance().getTransactionManager()
														.startTransaction(DataType.GROUPNAMEADD,
																new Groupdetails(0, mTrainerId, mNgoId, mGroupId,
																		mGroupName, mGroupDetails, null, null));
												Thread.sleep(200);

											}
										} catch (Exception e) {
											e.printStackTrace();
										}

										mProgressDialog_thread.dismiss();

										isLanguageSelection = false;

										Constants.BUTTON_CLICK_FLAG = "0";
										EShaktiApplication.setSubmenuclicked(false);

										PrefUtils.clearLoginValues();
										PrefUtils.clearGrouplistValues();

										startActivity(new Intent(getActivity(), GroupListActivity.class));
										getActivity().overridePendingTransition(R.anim.right_to_left_in,
												R.anim.right_to_left_out);
										getActivity().finish();
									}
								}).start();
								/** DB **/

							} else if (Boolean.valueOf(!EShaktiApplication.isAgent)) {
								isLanguageSelection = false;
								getActivity().startService(new Intent(getActivity(), GroupDetailsService.class));
								mProgressDialog.dismiss();
								Constants.BUTTON_CLICK_FLAG = "0";
								EShaktiApplication.setSubmenuclicked(false);
								startActivity(new Intent(getActivity(), MainActivity.class));
								getActivity().overridePendingTransition(R.anim.right_to_left_in,
										R.anim.right_to_left_out);
							}
						} else if (Boolean.valueOf(isBankBalance)) {
							isBankBalance = false;
							Reports_BankBalanceFragment fragment = new Reports_BankBalanceFragment();
							setFragment(fragment);

						} else if (Boolean.valueOf(isAgentProfile)) {
							isAgentProfile = false;
							Profile_AgentProfileFragment fragment = new Profile_AgentProfileFragment();
							setFragment(fragment);

						} else if (Boolean.valueOf(isGroupProfile)) {
							isGroupProfile = false;
							Profile_GroupProfileFragment fragment = new Profile_GroupProfileFragment();
							setFragment(fragment);

						} else if (Boolean.valueOf(isBankTransactionReport)) {
							isBankTransactionReport = false;
							String responseArr[] = Get_BankTransactionSummaryTask.sBank_summary_Response.split("~");
							Log.e("BANKTRANSACTION RESPONSE LENGTH	", responseArr.length + "");

							if (responseArr.length > 1) {

								Reports_BankTransactionReportFragment fragment = new Reports_BankTransactionReportFragment();
								setFragment(fragment);

							} else {

								TastyToast.makeText(getActivity(), AppStrings.nobanktransactionreportdatas,
										TastyToast.LENGTH_SHORT, TastyToast.WARNING);
							}
						} else if (Boolean.valueOf(isMinutes)) {
							sSubMenu_Item = "9";
							isMinutes = false;
							try {
								FragmentManager fm = getActivity().getSupportFragmentManager();

								Dialog_TransactionDate dialog = new Dialog_TransactionDate(getActivity(),
										AppStrings.MinutesofMeeting);
								dialog.show(fm, "");
							} catch (Exception e) {
								e.printStackTrace();
							}

						} else if (isGetMemberMobileNumber) {
							isGetMemberMobileNumber = false;
							if (publicValues.mGetMemberMobileNumberValues != null
									&& !publicValues.mGetMemberMobileNumberValues.equals("No")) {

								Profile_Member_Mobileno_Update_Fragment fragment = new Profile_Member_Mobileno_Update_Fragment();
								setFragment(fragment);
							} else {
								TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT,
										TastyToast.ERROR);
							}
						} else if (isGetMemberAadhaarNumber) {
							isGetMemberAadhaarNumber = false;
							if (publicValues.mGetMemberAadhaarNumberValues != null
									&& !publicValues.mGetMemberAadhaarNumberValues.equals("No")) {

								Profile_Member_Aadhaar_UpdationFragment fragment = new Profile_Member_Aadhaar_UpdationFragment();
								setFragment(fragment);
							} else {
								TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT,
										TastyToast.ERROR);
							}
						} else if (isGetShgAccountNumber) {
							isGetShgAccountNumber = false;
							if (publicValues.mGetShgAccountNumberValues != null
									&& !publicValues.mGetShgAccountNumberValues.equals("No")) {

								Profile_Shg_AccountNumberUpdation_Fragment fragment = new Profile_Shg_AccountNumberUpdation_Fragment();
								setFragment(fragment);
							} else {
								TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT,
										TastyToast.ERROR);
							}
						} else if (isGetBankBranchName) {

							isGetMemberAccountNumber = true;

							new Get_MemberAccountNumber_Webservices(this).execute();

							isGetBankBranchName = false;

						} else if (isGetMemberAccountNumber) {
							isGetMemberAccountNumber = false;

							if (publicValues.mGetBankBranchValues != null
									&& !publicValues.mGetBankBranchValues.equals("No")) {

								Profile_Member_Account_Number_UpdationFragment fragment = new Profile_Member_Account_Number_UpdationFragment();
								setFragment(fragment);
							} else {
								TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT,
										TastyToast.ERROR);
							}
						} else if (isSelectGroup) {
							isSelectGroup = false;
							startActivity(new Intent(getActivity(), GroupListActivity.class));
							getActivity().overridePendingTransition(R.anim.right_to_left_in, R.anim.right_to_left_out);
							getActivity().finish();
						}

						if (Boolean.valueOf(isNavigate)) {

							if (Boolean.valueOf(ConnectionUtils.isNetworkAvailable(getActivity()))) {

								Transaction_SavingsFragment.isSavings = false;

								sSubMenu_Item = "7";
								isServiceCall = false;
								FragmentManager fm = getChildFragmentManager();
								Dialog_TransactionDate dialog = new Dialog_TransactionDate(getActivity(),
										AppStrings.InternalLoanDisbursement);
								dialog.show(fm, "");

							} else if (!Boolean.valueOf(ConnectionUtils.isNetworkAvailable(getActivity()))) {
								// Do offlline Stuffs
								if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {

								} else {

									TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF,
											TastyToast.LENGTH_SHORT, TastyToast.ERROR);
								}

							}
							isNavigate = false;// doubt check carefully
						}
					}
				}

			}
		}

	}

	public int GetDipsFromPixel(float pixels) {
		// Get the screen's density scale
		final float scale = getResources().getDisplayMetrics().density;
		// Convert the dps to pixels, based on density scale
		return (int) (pixels * scale + 0.5f);
	}

	@Subscribe
	public void OnGroupMasterPLDBResponse(final GroupMasterPLDB_FD_Response groupMasterPLDBResponse) {
		switch (groupMasterPLDBResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), groupMasterPLDBResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), groupMasterPLDBResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			MainFragment_Dashboard.mPLDBResponse = GetGroupMemberDetails.getPersonaloanOS();
			MainFragment_Dashboard.mPLDBPurposeofloan = GetGroupMemberDetails.getPurposeofloan();

			try {
				sSubMenu_Item = "7";

				FragmentManager fManager = getChildFragmentManager();

				Dialog_TransactionDate dialog_Training = new Dialog_TransactionDate(getActivity(),
						AppStrings.InternalLoanDisbursement);
				dialog_Training.show(fManager, "");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

			break;
		}

	}

	@Subscribe
	public void OnGroupMasterResponse(final GroupMasterResponse groupProfileResponse) {
		switch (groupProfileResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), groupProfileResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), groupProfileResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:
			Fragment fragment = null;

			if (Boolean.valueOf(isAgentProfile)) {
				isAgentProfile = false;
				if (GetGroupMemberDetails.getAgentprofile() != null) {
					Get_AgentProfileTask.sGetAgentProfile_Response = GetGroupMemberDetails.getAgentprofile();

					fragment = new Profile_AgentProfileFragment();
					setFragment(fragment);
				} else {
					TastyToast.makeText(getActivity(), "Agent profile is empty", TastyToast.LENGTH_SHORT,
							TastyToast.ERROR);
				}

			} else if (Boolean.valueOf(isGroupProfile)) {
				isGroupProfile = false;

				if (GetGroupMemberDetails.getGroupprofile() != null) {
					Get_GroupProfileTask.sGetGroupProfile_Response = GetGroupMemberDetails.getGroupprofile();

					fragment = new Profile_GroupProfileFragment();
					setFragment(fragment);
				} else {
					TastyToast.makeText(getActivity(), "Group profile is empty", TastyToast.LENGTH_SHORT,
							TastyToast.ERROR);
				}
			}

			break;
		}

	}

	private void sendEmailWithDBAttachment(Context context) {
		// TODO Auto-generated method stub
		try {
			Intent emailIntent = new Intent(Intent.ACTION_SEND);// works fine
			emailIntent.setType("message/rfc822");// ("application/octet-stream");////("text/plain");
			emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] { "prabhuraja@yesteam.in" });
			emailIntent.putExtra(Intent.EXTRA_SUBJECT, "EShakti Application Database attached");
			emailIntent.putExtra(Intent.EXTRA_TEXT, "");
			File root = Environment.getExternalStorageDirectory();
			String pathToMyAttachedFile = "backupname.db";
			File file = new File(root, pathToMyAttachedFile);
			Log.e("File Location  !!!!", file.getAbsolutePath().toString() + "");
			if (!file.exists() || !file.canRead()) {
				return;
			}
			Uri uri = Uri.fromFile(file);
			emailIntent.putExtra(Intent.EXTRA_STREAM, uri);
			final PackageManager pm = getActivity().getPackageManager();
			final List<ResolveInfo> matches = pm.queryIntentActivities(emailIntent, 0);
			ResolveInfo best = null;
			for (final ResolveInfo info : matches)
				if (info.activityInfo.packageName.endsWith(".gm")
						|| info.activityInfo.name.toLowerCase().contains("gmail"))
					best = info;
			if (best != null)
				emailIntent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
			startActivity(emailIntent);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Subscribe
	public void OnGroupMasterMinutesofmeetingResponse(final GetMin_MeetingResponse getMinutesofMeetingResponse) {
		switch (getMinutesofMeetingResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), getMinutesofMeetingResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), getMinutesofMeetingResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			String mMasterMinutes = GetGroupMemberDetails.getMasterminutes();
			String mIndividualMinutes = GetGroupMemberDetails.getIndividualgroupminutes();
			String mMasterMinut[] = mMasterMinutes.split("~");
			String mIndi[] = mIndividualMinutes.split("~");
			int mSize;
			mSize = mMasterMinut.length / 2;// GroupListActivity.offline_sAgent_Gname.size();
			String mMasterValues[] = null, mMasterId[] = null;
			StringBuilder builder = new StringBuilder();
			String mMasterresponse, result;
			mMasterValues = new String[mSize];
			mMasterId = new String[mSize];
			int j = 0, k = 0;
			for (int i = 0; i < mMasterMinut.length; i++) {

				if ((i + 2) % 2 == 0) {
					mMasterId[j] = mMasterMinut[i];

					j++;
				}
			}

			for (int i = 0; i < mMasterMinut.length; i++) {

				if (!((i + 2) % 2 == 0)) {
					mMasterValues[k] = mMasterMinut[i];
					k++;
				}

			}

			for (int i = 0; i < mIndi.length; i++) {
				String mIndiValues = mIndi[i];

				for (int m = 0; m < mMasterId.length; m++) {

					if (mIndiValues.equals(mMasterId[m])) {

						mMasterresponse = mIndiValues + "~" + mMasterValues[m] + "~";

						builder.append(mMasterresponse);
					}
				}

			}
			result = builder.toString();
			System.out.println("mMasterValues " + result);

			Get_Group_Minutes_NewTask.sGet_Group_Minutes__new_Response = result;

			sSubMenu_Item = "9";
			try {
				FragmentManager fm = getChildFragmentManager();

				Dialog_TransactionDate dialog = new Dialog_TransactionDate(getActivity(), AppStrings.MinutesofMeeting);
				dialog.show(fm, "");
			} catch (Exception e) {
				e.printStackTrace();
			}

			break;
		}

	}

	@Override
	public void onAttach(Context context) {
		// TODO Auto-generated method stub
		super.onAttach(context);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	@Subscribe
	public void OnValues(final ArrayList<Transaction> arrayList) {
		if (mOfflineNavigate) {
			EShaktiApplication.setOfflineTransDate(false);
			mOfflineNavigate = false;
			if (arrayList.size() != 0 && arrayList.get(0).getUniqueId() != null) {
				try {

					fragment = new Offline_ReportDateListFragment();
					setFragment(fragment);
				} catch (Exception e) {
					e.printStackTrace();
				}

			} else {
				TastyToast.makeText(getActivity(), AppStrings.noofflinedatas, TastyToast.LENGTH_SHORT,
						TastyToast.WARNING);
			}
		}
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		SBus.INST.unRegister(this);
	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub
		getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(null).commit();

	}

	private void onSetCalendarValues() {
		// TODO Auto-generated method stub

		if (EShaktiApplication.getNextMonthLastDate() == null) {
			try {

				if (EShaktiApplication.isCalendarDateVisibleFlag()) {
					EShaktiApplication.setCalendarDateVisibleFlag(false);
				}
				Calendar calender = Calendar.getInstance();

				SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String formattedDate = df.format(calender.getTime());
				Log.e("Device Date  =  ", formattedDate + "");

				String mBalanceSheetDate = SelectedGroupsTask.sLastTransactionDate_Response; // dd/MM/yyyy
				String formatted_balancesheetDate = mBalanceSheetDate.replace("/", "-");
				Log.e("Balancesheet Date = ", formatted_balancesheetDate + "");

				Date balanceDate = df.parse(formatted_balancesheetDate);
				Date systemDate = df.parse(formattedDate);

				if (balanceDate.compareTo(systemDate) < 0 || balanceDate.compareTo(systemDate) == 0) {
					System.err.println(" balancesheet date is less than sys date");

					String mLastTransactionDate = SelectedGroupsTask.sLastTransactionDate_Response;

					String formattedDate_LastTransaction = mLastTransactionDate.replace("/", "-");

					String days = CalculateDateUtils.get_count_of_days(formattedDate_LastTransaction, formattedDate);

					int mDaycount = Integer.parseInt(days);
					Log.e("Total Days Count =====_____----", mDaycount + "");

					EShaktiApplication.setLastTransDate_GroupId(SelectedGroupsTask.Group_Id);

					if (mDaycount > 30) {

						SimpleDateFormat df_after = new SimpleDateFormat("dd-MM-yyyy");

						Calendar calendar = Calendar.getInstance();
						calendar.setTime(df_after.parse(formattedDate_LastTransaction));
						calendar.add(Calendar.MONTH, 1);
						calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
						Date nextMonthFirstDay = calendar.getTime();
						System.out.println(
								"------ Next month first date   =  " + df_after.format(nextMonthFirstDay) + "");

						Calendar cal_last_date = Calendar.getInstance();
						cal_last_date.setTime(df_after.parse(formattedDate_LastTransaction));
						cal_last_date.add(Calendar.MONTH, 1);
						cal_last_date.set(Calendar.DATE, cal_last_date.getActualMaximum(Calendar.DAY_OF_MONTH));
						Date nextMonthLastDay = cal_last_date.getTime();
						System.out
								.println("------ Next month Last date   =  " + df_after.format(nextMonthLastDay) + "");

						//
						String lastDateOfNextMonth = df_after.format(nextMonthLastDay);
						Calendar currentDate = Calendar.getInstance();
						int current_Day = currentDate.get(Calendar.DAY_OF_MONTH);
						int current_month = currentDate.get(Calendar.MONTH) + 1;
						int current_year = currentDate.get(Calendar.YEAR);
						String current_date = current_Day + "-" + current_month + "-" + current_year;

						Date lastdate = null, currentdate = null;
						String min_date_str = null;

						try {

							lastdate = df_after.parse(lastDateOfNextMonth);
							currentdate = df_after.parse(current_date);

						} catch (ParseException e) {
							// TODO: handle exception
							e.printStackTrace();
						}

						if (lastdate.compareTo(currentdate) <= 0) {
							min_date_str = lastDateOfNextMonth;
						} else {
							min_date_str = current_date;
						}

						Log.e("Minimum Date !!!!!	", min_date_str + "");

						EShaktiApplication.setNextMonthLastDate(min_date_str);

					} else {
						Calendar currentDate = Calendar.getInstance();
						SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
						EShaktiApplication.setNextMonthLastDate(sdf.format(currentDate.getTime()));

					}
				}

			} catch (Exception e) {
				// TODO: handle exception
			}
		}

	}

	public static void sendFile(Context context) {

		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("message/rfc822");// ("application/octet-stream");////("text/plain");
		intent.putExtra(Intent.EXTRA_EMAIL, new String[] { "prabhuraja@yesteam.in" });
		intent.putExtra(Intent.EXTRA_SUBJECT, "EShakti Application Database attached");
		intent.putExtra(Intent.EXTRA_TEXT, "");
		File root = Environment.getExternalStorageDirectory();
		String pathToMyAttachedFile = "backupname.db";
		File file = new File(root, pathToMyAttachedFile);
		Log.e("File Location  !!!!", file.getAbsolutePath().toString() + "");
		if (!file.exists() || !file.canRead()) {
			return;
		}

		Uri uri = FileProvider.getUriForFile(context, context.getPackageName() + ".provider", file);
		System.out.println("URI  Path %%%%%%%%%%%   =  " + uri);
		intent.putExtra(Intent.EXTRA_STREAM, uri);
		intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
		// context.startActivity(intent);
		final PackageManager pm = context.getPackageManager();
		final List<ResolveInfo> matches = pm.queryIntentActivities(intent, 0);
		ResolveInfo best = null;
		for (final ResolveInfo info : matches)
			if (info.activityInfo.packageName.endsWith(".gm") || info.activityInfo.name.toLowerCase().contains("gmail"))
				best = info;
		if (best != null)
			intent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
		context.startActivity(intent);
	}

}
