package com.yesteam.eshakti.view.fragment;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.view.activity.MainActivity;
import com.yesteam.eshakti.views.RaisedButton;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class DefaultReportsFragment extends Fragment implements OnClickListener {

	private TextView header, totalAmountLabel, plDisbursementLabel, cashInHandLabel, cashAtBankLabel,
				mGroupLoanHeader, mInterestLabel, mChargesLabel, mRepaymentLabel, mIntSubReceviedLabel, mBankChargesLabel;
	private TextView totalAmount, plDisbursementAmount, cashInHandAmount, cashAtBankAmount, mInterestAmount, mChargesAmount, 
				mRepaymentAmount, mIntSubReceviedAmount, mBankChargesAmount;
	private RaisedButton mSubmitButton;

	public DefaultReportsFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		SBus.INST.register(this);
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		SBus.INST.unRegister(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.default_reports, container, false);
		try {
			header = (TextView) rootView.findViewById(R.id.header);
			header.setText(RegionalConversion.getRegionalConversion(AppStrings.reports));
			header.setTypeface(LoginActivity.sTypeface);

			totalAmountLabel = (TextView) rootView.findViewById(R.id.lefttotalAmount);
			totalAmountLabel.setText(RegionalConversion.getRegionalConversion(AppStrings.mTotalSavings));
			totalAmountLabel.setTypeface(LoginActivity.sTypeface);
			totalAmountLabel.setTextColor(color.black);

			totalAmount = (TextView) rootView.findViewById(R.id.righttotalAmount);
			totalAmount.setText(Integer.toString(Transaction_InternalLoan_DisbursementFragment.mTotalCollection));
			totalAmount.setTypeface(LoginActivity.sTypeface);
			totalAmount.setTextColor(color.black);

			plDisbursementLabel = (TextView) rootView.findViewById(R.id.leftPLAmount);
			plDisbursementLabel.setText(RegionalConversion.getRegionalConversion(AppStrings.mTotalDisbursement));
			plDisbursementLabel.setTypeface(LoginActivity.sTypeface);
			plDisbursementLabel.setTextColor(color.black);

			plDisbursementAmount = (TextView) rootView.findViewById(R.id.rightPLAmount);
			plDisbursementAmount
					.setText(Integer.toString(Transaction_InternalLoan_DisbursementFragment.mTotalDisbursement));
			plDisbursementAmount.setTypeface(LoginActivity.sTypeface);
			plDisbursementAmount.setTextColor(color.black);

			cashInHandLabel = (TextView) rootView.findViewById(R.id.leftCIHAmount);
			cashInHandLabel.setText(RegionalConversion.getRegionalConversion(AppStrings.cashinhand));
			cashInHandLabel.setTypeface(LoginActivity.sTypeface);
			cashInHandLabel.setTextColor(color.black);

			cashInHandAmount = (TextView) rootView.findViewById(R.id.rightCIHAmount);
			cashInHandAmount.setText(SelectedGroupsTask.sCashinHand.toString().trim());
			cashInHandAmount.setTypeface(LoginActivity.sTypeface);
			cashInHandAmount.setTextColor(color.black);

			cashAtBankLabel = (TextView) rootView.findViewById(R.id.leftCABAmount);
			cashAtBankLabel.setText(RegionalConversion.getRegionalConversion(AppStrings.cashatBank));
			cashAtBankLabel.setTypeface(LoginActivity.sTypeface);
			cashAtBankLabel.setTextColor(color.black);

			cashAtBankAmount = (TextView) rootView.findViewById(R.id.rightCABAmount);
			cashAtBankAmount.setText(SelectedGroupsTask.sCashatBank.toString().trim());
			cashAtBankAmount.setTypeface(LoginActivity.sTypeface);
			cashAtBankAmount.setTextColor(color.black);
			
			mGroupLoanHeader = (TextView) rootView.findViewById(R.id.groupLoanHeader);
			mGroupLoanHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.grouploanrepayment));
			mGroupLoanHeader.setTypeface(LoginActivity.sTypeface);
			
			mInterestLabel = (TextView) rootView.findViewById(R.id.leftInterestAmount);
			mInterestLabel.setText(RegionalConversion.getRegionalConversion(AppStrings.mTotalInterest));
			mInterestLabel.setTypeface(LoginActivity.sTypeface);
			mInterestLabel.setTextColor(color.black);
			
			mInterestAmount = (TextView) rootView.findViewById(R.id.rightInterestAmount);
			mInterestAmount.setText(String.valueOf(EShaktiApplication.getGroupLoanTotalInterest()));
			mInterestAmount.setTypeface(LoginActivity.sTypeface);
			mInterestAmount.setTextColor(color.black);
			
			mChargesLabel = (TextView) rootView.findViewById(R.id.leftChargesAmount);
			mChargesLabel.setText(RegionalConversion.getRegionalConversion(AppStrings.mTotalCharges));
			mChargesLabel.setTypeface(LoginActivity.sTypeface);
			mChargesLabel.setTextColor(color.black);
			
			mChargesAmount = (TextView) rootView.findViewById(R.id.rightChargesAmount);
			mChargesAmount.setText(String.valueOf(EShaktiApplication.getGroupLoanTotalCharges()));
			mChargesAmount.setTypeface(LoginActivity.sTypeface);
			mChargesAmount.setTextColor(color.black);
			
			mRepaymentLabel = (TextView) rootView.findViewById(R.id.leftRepaymentAmount);
			mRepaymentLabel.setText(RegionalConversion.getRegionalConversion(AppStrings.mTotalRepayment));
			mRepaymentLabel.setTypeface(LoginActivity.sTypeface);
			mRepaymentLabel.setTextColor(color.black);
			
			mRepaymentAmount = (TextView) rootView.findViewById(R.id.rightRepaymentAmount);
			mRepaymentAmount.setText(String.valueOf(EShaktiApplication.getGroupLoanTotalRepayment()));
			mRepaymentAmount.setTypeface(LoginActivity.sTypeface);
			mRepaymentAmount.setTextColor(color.black);
			
			mIntSubReceviedLabel = (TextView) rootView.findViewById(R.id.leftIntSubReceviedAmount);
			mIntSubReceviedLabel.setText(RegionalConversion.getRegionalConversion(AppStrings.mTotalInterestSubventionRecevied));
			mIntSubReceviedLabel.setTypeface(LoginActivity.sTypeface);
			mIntSubReceviedLabel.setTextColor(color.black);
			
			mIntSubReceviedAmount = (TextView) rootView.findViewById(R.id.rightIntSubReceviedAmount);
			mIntSubReceviedAmount.setText(String.valueOf(EShaktiApplication.getGroupLoanTotalInterestSubventionRecevied()));
			mIntSubReceviedAmount.setTypeface(LoginActivity.sTypeface);
			mIntSubReceviedAmount.setTextColor(color.black);
			
			mBankChargesLabel = (TextView) rootView.findViewById(R.id.leftBankcharegsAmount);
			mBankChargesLabel.setText(RegionalConversion.getRegionalConversion(AppStrings.mTotalBankCharges));
			mBankChargesLabel.setTypeface(LoginActivity.sTypeface);
			mBankChargesLabel.setTextColor(color.black);
			
			mBankChargesAmount = (TextView) rootView.findViewById(R.id.rightBankchargesAmount);
			mBankChargesAmount.setText(String.valueOf(EShaktiApplication.getGroupLoanTotalBankcharges()));
			mBankChargesAmount.setTypeface(LoginActivity.sTypeface);
			mBankChargesAmount.setTextColor(color.black);

			mSubmitButton = (RaisedButton) rootView.findViewById(R.id.fragment_Submitbutton);
			mSubmitButton.setText(RegionalConversion.getRegionalConversion(AppStrings.mDone));
			mSubmitButton.setTypeface(LoginActivity.sTypeface);
			mSubmitButton.setOnClickListener(this);

			EShaktiApplication.setDefault(true);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return rootView;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		MainActivity.mStepwiseBackKey = false;
		
		PrefUtils.clearStepWise_GroupId();
		PrefUtils.clearStepWiseScreenCount();
		PrefUtils.clearStepWise_MemberloanrepayScreenCount();
		MainFragment_Dashboard fragment = new MainFragment_Dashboard();
		setFragment(fragment);
	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub
		Log.e("Current Class Name!!!!!!!!!!!!!!", fragment.getClass().getName());
		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

	}

}
