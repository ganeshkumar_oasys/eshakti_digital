package com.yesteam.eshakti.view.fragment;

import java.util.ArrayList;
import java.util.List;

import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.adapter.CustomItemAdapter;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.model.RowItem;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.views.MaterialSpinner;
import com.yesteam.eshakti.views.RaisedButton;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.TextView;

public class Payments_Fundtransfer_Fragment extends Fragment {

	public static String TAG = MemberList_Fragment.class.getSimpleName();
	String sSelected_MemberId = null;

	private TextView mGroupName, mCashinHand, mCashatBank;

	MaterialSpinner mSpinner_Transaction_mode, mSpinner_Membername, mSpinner_debit_account_type,
			mSpinner_destination_member;
	CustomItemAdapter mTransaction_modeAdapter, mMembernameAdapter, mDebit_accounttypeAdapter, mDestinationAdapter;
	private List<RowItem> mTransaction_modeItems, mMembernameItems, mDebit_accounttypeItems;
	public static String selectedTransaction_mode, selectedMembername, selectedDebit_accounttype,
			selectedDestinationMemberName;
	public static String mTransaction_modeValue = null, mMembernameValue = null, mDebitAccountTypeValue = null,
			mDestinationMemberNameValue = null;
	View rootView;

	private String[] mTransactionModeArr, mTransactionModeArray, mMemberNameArray, mDebitAccountArray;

	TextView mFundTransfer_text;
	EditText mFundtransferAmount_edittext;
	RaisedButton mSubmit_Button;

	public Payments_Fundtransfer_Fragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_MEMBER_REPORTS_MEMBERLIST;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		rootView = inflater.inflate(R.layout.fragment_payments_fundtransfer, container, false);

		MainFragment_Dashboard.isBackpressed = false;

		try {

			EShaktiApplication.setCheckGroupListTextColor(false);
			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashinHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashinHand.setTypeface(LoginActivity.sTypeface);

			mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashatBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashatBank.setTypeface(LoginActivity.sTypeface);
			mTransactionModeArr = new String[] { "SAVINGS", "INTERNAL LOAN REPAYMENT", "INTERNAL LOAN DISBURSEMENT",
					"BANK LOAN DISBURSEMENT", "GROUP LOAN REPAYMENT", "MEMBER TO MEMBER" };
			init();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return rootView;
	}

	private void init() {
		// TODO Auto-generated method stub

		mSpinner_Transaction_mode = (MaterialSpinner) rootView.findViewById(R.id.transaction_type_spinner);
		mSpinner_Transaction_mode.setBaseColor(color.grey_400);
		mSpinner_Transaction_mode.setFloatingLabelText("Transaction Mode");
		mSpinner_Transaction_mode.setPaddingSafe(10, 0, 10, 0);

		mSpinner_Membername = (MaterialSpinner) rootView.findViewById(R.id.memberlist_spinner);
		mSpinner_Membername.setBaseColor(color.grey_400);
		mSpinner_Membername.setFloatingLabelText("Member Name");
		mSpinner_Membername.setPaddingSafe(10, 0, 10, 0);

		mSpinner_debit_account_type = (MaterialSpinner) rootView.findViewById(R.id.debit_account_type_spinner);
		mSpinner_debit_account_type.setBaseColor(color.grey_400);
		mSpinner_debit_account_type.setFloatingLabelText("Debit Account");
		mSpinner_debit_account_type.setPaddingSafe(10, 0, 10, 0);

		mSpinner_destination_member = (MaterialSpinner) rootView.findViewById(R.id.destination_member_list_spinner);
		mSpinner_destination_member.setBaseColor(color.grey_400);
		mSpinner_destination_member.setFloatingLabelText("Member Name");
		mSpinner_destination_member.setPaddingSafe(10, 0, 10, 0);

		mFundTransfer_text = (TextView) rootView.findViewById(R.id.payments_transferamount_TextView);
		mFundtransferAmount_edittext = (EditText) rootView.findViewById(R.id.payments_transferamount);
		mSubmit_Button = (RaisedButton) rootView.findViewById(R.id.payments_fundtransfer_submit);

		mFundTransfer_text.setTypeface(LoginActivity.sTypeface);
		mFundtransferAmount_edittext.setTypeface(LoginActivity.sTypeface);
		mSubmit_Button.setTypeface(LoginActivity.sTypeface);

		mSpinner_debit_account_type.setVisibility(View.GONE);
		mSpinner_Membername.setVisibility(View.GONE);
		mSpinner_destination_member.setVisibility(View.GONE);
		mFundTransfer_text.setVisibility(View.GONE);
		mFundtransferAmount_edittext.setVisibility(View.GONE);
		mSubmit_Button.setVisibility(View.GONE);

		mTransaction_modeItems = new ArrayList<RowItem>();

		mTransactionModeArray = new String[mTransactionModeArr.length + 1];
		mTransactionModeArray[0] = String.valueOf("Transaction Mode");
		for (int i = 0; i < mTransactionModeArr.length; i++) {
			mTransactionModeArray[i + 1] = mTransactionModeArr[i].toString();
		}

		for (int i = 0; i < mTransactionModeArray.length; i++) {
			RowItem rowItem = new RowItem(mTransactionModeArray[i]);
			mTransaction_modeItems.add(rowItem);
		}
		mTransaction_modeAdapter = new CustomItemAdapter(getActivity(), mTransaction_modeItems);
		mSpinner_Transaction_mode.setAdapter(mTransaction_modeAdapter);
		mSpinner_Transaction_mode.setSelection(0);

		mMembernameItems = new ArrayList<RowItem>();

		mMemberNameArray = new String[SelectedGroupsTask.member_Name.size() + 1];
		mMemberNameArray[0] = String.valueOf("Member Name");
		for (int i = 0; i < SelectedGroupsTask.member_Name.size(); i++) {
			mMemberNameArray[i + 1] = SelectedGroupsTask.member_Name.elementAt(i).toString();
		}

		for (int i = 0; i < mMemberNameArray.length; i++) {
			RowItem rowItem = new RowItem(mMemberNameArray[i]);
			mMembernameItems.add(rowItem);
		}
		mMembernameAdapter = new CustomItemAdapter(getActivity(), mMembernameItems);
		mSpinner_Membername.setAdapter(mMembernameAdapter);

		mSpinner_destination_member.setAdapter(mMembernameAdapter);

		mSpinner_Transaction_mode.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub

				if (position == 0) {
					selectedTransaction_mode = mTransactionModeArray[0];
					mTransaction_modeValue = "0";
					mSpinner_Membername.setVisibility(View.GONE);
					mSpinner_debit_account_type.setVisibility(View.GONE);
					mSpinner_destination_member.setVisibility(View.GONE);
					mFundTransfer_text.setVisibility(View.GONE);
					mFundtransferAmount_edittext.setVisibility(View.GONE);
					mSubmit_Button.setVisibility(View.GONE);

					mSpinner_Membername.setSelection(0);
				} else {
					mSpinner_Membername.setSelection(0);
					selectedTransaction_mode = mTransactionModeArray[position];

					mTransaction_modeValue = selectedTransaction_mode;
					System.out.println("Transaction Mode : " + mTransaction_modeValue);

					Log.e("Transaction Mode--->>>", mTransaction_modeValue);

					if (mTransaction_modeValue.equals("SAVINGS")) {
						setvisible_gone("SAVINGS");
					} else if (mTransaction_modeValue.equals("INTERNAL LOAN REPAYMENT")) {
						setvisible_gone("INTERNAL LOAN REPAYMENT");
					} else if (mTransaction_modeValue.equals("INTERNAL LOAN DISBURSEMENT")) {
						setvisible_gone("INTERNAL LOAN DISBURSEMENT");
					} else if (mTransaction_modeValue.equals("BANK LOAN DISBURSEMENT")) {

						/* Debit account type */
						mDebit_accounttypeItems = new ArrayList<RowItem>();
						String[] mDebitAccountArr;
						if (SelectedGroupsTask.loan_Id.size() > 1) {
							mDebitAccountArr = new String[SelectedGroupsTask.loan_Name.size() + 1];
						} else {
							mDebitAccountArr = new String[SelectedGroupsTask.loan_Name.size()];
						}

						Log.e("Loan English name size", SelectedGroupsTask.loan_Name.size() + "");
						Log.e("English Name ---->>>>", SelectedGroupsTask.loan_Name.elementAt(0));

						Log.e("Debit Account size---->>>", mDebitAccountArr.length + "");

						for (int i = 0; i < mDebitAccountArr.length; i++) {
							if (i == 0) {
								mDebitAccountArr[i] = "SHG Savings Account";
							} else {
								System.out.println("-------------->>> Loan Name  =  "
										+ SelectedGroupsTask.loan_Name.elementAt(i - 1) + "");
								if (SelectedGroupsTask.loan_Id.size() > 1) {

									String loanName = null;
									if (i == (mDebitAccountArr.length - 1)) {
										loanName = SelectedGroupsTask.loan_Name.elementAt(i - 1) + " - " + "0";
									} else {
										loanName = SelectedGroupsTask.loan_Name.elementAt(i - 1) + " - "
												+ SelectedGroupsTask.loanAcc_loanAccNo.elementAt(i - 1);
									}
									System.out.println(
											"------------------->>> Loan Name with Acc Number   =  " + loanName + "");
									mDebitAccountArr[i] = loanName;

									Log.e("Loan Account Size____________>>>>",
											SelectedGroupsTask.loanAcc_loanAccNo.size() + "");

									Log.e("Loan Account Name____________>>>>",
											SelectedGroupsTask.loanAcc_loanAccNo.elementAt(0));
								}

							}
						}

						mDebitAccountArray = new String[mDebitAccountArr.length + 1];
						mDebitAccountArray[0] = String.valueOf("Debit Account");
						Log.e("mDebitAccountArray", mDebitAccountArray.length + "");

						Log.e("mDebitAccountArr----->>>", mDebitAccountArr.length + "");

						for (int i = 0; i < mDebitAccountArray.length - 1; i++) {
							mDebitAccountArray[i + 1] = mDebitAccountArr[i];
						}

						for (int i = 0; i < mDebitAccountArray.length; i++) {
							RowItem rowItem = new RowItem(mDebitAccountArray[i]);
							mDebit_accounttypeItems.add(rowItem);
						}

						if (SelectedGroupsTask.loan_Id.size() > 1) {
							mDebit_accounttypeItems.remove(mDebitAccountArray.length - 1);
						}

						mDebit_accounttypeAdapter = new CustomItemAdapter(getActivity(), mDebit_accounttypeItems);
						mSpinner_debit_account_type.setAdapter(mDebit_accounttypeAdapter);
						setvisible_gone("BANK LOAN DISBURSEMENT");

					} else if (mTransaction_modeValue.equals("GROUP LOAN REPAYMENT")) {

						/* Debit account type */
						mDebit_accounttypeItems = new ArrayList<RowItem>();

						String[] mDebitAccountArr = new String[] { "SHG Savings Account", "Member Account" };

						mDebitAccountArray = new String[mDebitAccountArr.length + 1];
						mDebitAccountArray[0] = String.valueOf("Debit Account");

						for (int i = 0; i < mDebitAccountArray.length - 1; i++) {
							mDebitAccountArray[i + 1] = mDebitAccountArr[i];
						}

						for (int i = 0; i < mDebitAccountArray.length; i++) {
							RowItem rowItem = new RowItem(mDebitAccountArray[i]);
							mDebit_accounttypeItems.add(rowItem);
						}

						mDebit_accounttypeAdapter = new CustomItemAdapter(getActivity(), mDebit_accounttypeItems);
						mSpinner_debit_account_type.setAdapter(mDebit_accounttypeAdapter);

						setvisible_gone("GROUP LOAN REPAYMENT");
					} else if (mTransaction_modeValue.equals("MEMBER TO MEMBER")) {
						setvisible_gone("MEMBER TO MEMBER");
					}

					if (mTransaction_modeValue.equals("INTERNAL LOAN DISBURSEMENT")) {

						if (!Payments_Internalloandisbursement_Fragment.isPaymentDisbursementBackPressed) {
							Payments_Internalloandisbursement_Fragment internalloandisbursement_Fragment = new Payments_Internalloandisbursement_Fragment();
							setFragment(internalloandisbursement_Fragment);
						} else {
							mSpinner_Transaction_mode.setSelection(0);
							mSpinner_Membername.setSelection(0);
							Payments_Internalloandisbursement_Fragment.isPaymentDisbursementBackPressed = false;

						}

					}

				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});

		mSpinner_Membername.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub

				if (position == 0) {
					selectedMembername = mMemberNameArray[0];
					mMembernameValue = "0";

				} else {
					selectedMembername = mMemberNameArray[position];

					mMembernameValue = selectedMembername;
					System.out.println("Member NAME : " + mMembernameValue);

					Log.e("Member Name--->>>", mMembernameValue);
					Log.e("Member Id---->>>", SelectedGroupsTask.member_Id.elementAt(position - 1));
				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});

		mSpinner_debit_account_type.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub

				if (position == 0) {
					selectedDebit_accounttype = mDebitAccountArray[0];
					mDebitAccountTypeValue = "0";
					mSpinner_destination_member.setVisibility(View.GONE);
					mFundTransfer_text.setVisibility(View.GONE);
					mFundtransferAmount_edittext.setVisibility(View.GONE);
					mSpinner_destination_member.setSelection(0);

				} else {
					selectedDebit_accounttype = mDebitAccountArray[position];

					mDebitAccountTypeValue = selectedDebit_accounttype;
					System.out.println("Member NAME : " + mDebitAccountTypeValue);

					if (mTransaction_modeValue.equals("BANK LOAN DISBURSEMENT")) {
						Payments_Internalloandisbursement_Fragment internalloandisbursement_Fragment = new Payments_Internalloandisbursement_Fragment();
						setFragment(internalloandisbursement_Fragment);
					} else if (mTransaction_modeValue.equals("GROUP LOAN REPAYMENT")) {

						setvisible_gone("GROUP LOAN REPAYMENT");

					}

				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});

		mSubmit_Button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Log.e("Transaction mode", mTransaction_modeValue);
				Log.e("Member Name--->>>", mMembernameValue);

			}
		});
	}

	private void setvisible_gone(String mValue) {
		// TODO Auto-generated method stub

		if (mValue.equals("SAVINGS")) {
			mSpinner_Membername.setVisibility(View.VISIBLE);
			mFundTransfer_text.setVisibility(View.VISIBLE);
			mFundtransferAmount_edittext.setVisibility(View.VISIBLE);
			mSubmit_Button.setVisibility(View.VISIBLE);
			mSpinner_debit_account_type.setVisibility(View.GONE);
			mSpinner_destination_member.setVisibility(View.GONE);
		} else if (mValue.equals("INTERNAL LOAN REPAYMENT")) {
			mSpinner_Membername.setVisibility(View.VISIBLE);
			mFundTransfer_text.setVisibility(View.VISIBLE);
			mFundtransferAmount_edittext.setVisibility(View.VISIBLE);
			mSubmit_Button.setVisibility(View.VISIBLE);
			mSpinner_destination_member.setVisibility(View.GONE);
			mSpinner_debit_account_type.setVisibility(View.GONE);
		} else if (mValue.equals("BANK LOAN DISBURSEMENT")) {
			mSpinner_debit_account_type.setVisibility(View.VISIBLE);

			mSpinner_destination_member.setVisibility(View.GONE);
			mSpinner_Membername.setVisibility(View.GONE);
			mFundTransfer_text.setVisibility(View.GONE);
			mFundtransferAmount_edittext.setVisibility(View.GONE);
			mSubmit_Button.setVisibility(View.GONE);

		} else if (mValue.equals("GROUP LOAN REPAYMENT")) {

			mSpinner_debit_account_type.setVisibility(View.VISIBLE);

			mSpinner_Membername.setVisibility(View.GONE);
			mSpinner_destination_member.setVisibility(View.GONE);
			mFundTransfer_text.setVisibility(View.GONE);
			mFundtransferAmount_edittext.setVisibility(View.GONE);
			mSubmit_Button.setVisibility(View.GONE);

			if (selectedDebit_accounttype != null && !selectedDebit_accounttype.equals("0")) {

				if (selectedDebit_accounttype.equals("Member Account")) {
					mSpinner_debit_account_type.setVisibility(View.VISIBLE);

					mSpinner_destination_member.setVisibility(View.VISIBLE);

					mFundTransfer_text.setVisibility(View.VISIBLE);
					mFundtransferAmount_edittext.setVisibility(View.VISIBLE);
					mSubmit_Button.setVisibility(View.VISIBLE);
				} else if (selectedDebit_accounttype.equals("SHG Savings Account")) {
					mSpinner_debit_account_type.setVisibility(View.VISIBLE);

					mSpinner_destination_member.setVisibility(View.GONE);
					mSpinner_Membername.setVisibility(View.GONE);
					mFundTransfer_text.setVisibility(View.VISIBLE);
					mFundtransferAmount_edittext.setVisibility(View.VISIBLE);
					mSubmit_Button.setVisibility(View.VISIBLE);
				}
			}
		} else if (mValue.equals("MEMBER TO MEMBER")) {

			mSpinner_debit_account_type.setVisibility(View.GONE);

			mSpinner_destination_member.setVisibility(View.VISIBLE);
			mSpinner_Membername.setVisibility(View.VISIBLE);
			mFundTransfer_text.setVisibility(View.VISIBLE);
			mFundtransferAmount_edittext.setVisibility(View.VISIBLE);
			mSubmit_Button.setVisibility(View.VISIBLE);

			mSpinner_destination_member.setFloatingLabelText("Destination Member Name");

			mMembernameItems = new ArrayList<RowItem>();
			mMemberNameArray = new String[SelectedGroupsTask.member_Name.size() + 1];
			mMemberNameArray[0] = String.valueOf("Destination Member Name");
			for (int i = 0; i < SelectedGroupsTask.member_Name.size(); i++) {
				mMemberNameArray[i + 1] = SelectedGroupsTask.member_Name.elementAt(i).toString();
			}
			for (int i = 0; i < mMemberNameArray.length; i++) {
				RowItem rowItem = new RowItem(mMemberNameArray[i]);
				mMembernameItems.add(rowItem);
			}
			mMembernameAdapter = new CustomItemAdapter(getActivity(), mMembernameItems);
			mSpinner_destination_member.setAdapter(mMembernameAdapter);

		}

	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub
		Log.e("Current Class Name!!!!!!!!!!!!!!", fragment.getClass().getName());
		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

	}

}