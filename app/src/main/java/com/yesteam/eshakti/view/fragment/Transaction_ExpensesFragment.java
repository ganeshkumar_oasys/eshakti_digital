package com.yesteam.eshakti.view.fragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.squareup.otto.Subscribe;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.TransactionOffConstants;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;
import com.yesteam.eshakti.sqlite.database.GroupProvider;
import com.yesteam.eshakti.sqlite.database.TransactionProvider;
import com.yesteam.eshakti.sqlite.database.response.GetSingleTransResponse;
import com.yesteam.eshakti.sqlite.database.response.GroupMasUpdateResponse;
import com.yesteam.eshakti.sqlite.database.response.TransactionResponse;
import com.yesteam.eshakti.sqlite.database.response.TransactionUpdateResponse;
import com.yesteam.eshakti.sqlite.db.model.GetTransactionSingle;
import com.yesteam.eshakti.sqlite.db.model.GetTransactionSinglevalues;
import com.yesteam.eshakti.sqlite.db.model.GroupDetailsUpdate;
import com.yesteam.eshakti.sqlite.db.model.GroupResponseUpdate;
import com.yesteam.eshakti.sqlite.db.model.GroupTempDBLastTransDate;
import com.yesteam.eshakti.sqlite.db.model.Transaction;
import com.yesteam.eshakti.sqlite.db.model.TransactionUpdate;
import com.yesteam.eshakti.sqlite.db.model.Transaction_UniqueIdSingle;
import com.yesteam.eshakti.sqlite.db.transactions.TransactionManager.DataType;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.utils.GetOfflineTransactionValues;
import com.yesteam.eshakti.utils.GetResponseInfo;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.utils.Get_EdiText_Filter;
import com.yesteam.eshakti.utils.Get_Offline_MobileDate;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.utils.Put_DB_GroupNameDetail_Response;
import com.yesteam.eshakti.utils.Put_DB_GroupResponse;
import com.yesteam.eshakti.utils.Reset;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.utils.UpdateCalendarMinMaxDateUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.Get_ExpensesTask;
import com.yesteam.eshakti.webservices.Get_LastTransactionID;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Transaction_ExpensesFragment extends Fragment implements OnClickListener, TaskListener {

	public static final String TAG = Transaction_ExpensesFragment.class.getSimpleName();
	private TextView mGroupName, mCashInHand, mCashAtBank, mHeader;
	private Button mRaised_SubmitButton;
	private Button mEdit_RaisedButton, mOk_RaisedButton;
	private EditText mExpenses_values;

	public static List<EditText> sExpensesFields = new ArrayList<EditText>();
	public static String sExpensesAmounts[];
	public static String[] sExpensesName;
	public static String sSendToServer_Expenses;
	public static int sExpenses_total;

	private Dialog mProgressDialog;
	Dialog confirmationDialog;

	private int mSize;
	String nullVlaue = "0";
	String mLastTrDate = null, mLastTr_ID = null;
	boolean isGetTrid = false;
	boolean isServiceCall = false;
	String mSqliteDBStoredValues_Expenses = null;

	public Transaction_ExpensesFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_EXPENSES;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		SBus.INST.register(this);
	}

	@Override
	public void onAttach(Context context) {
		// TODO Auto-generated method stub
		super.onAttach(context);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		sExpensesFields.clear();
		sExpenses_total = 0;
		sSendToServer_Expenses = Reset.reset(sSendToServer_Expenses);

		sExpensesName = new String[] {

				RegionalConversion.getRegionalConversion(AppStrings.stationary),
				RegionalConversion.getRegionalConversion(AppStrings.federationincome),
				RegionalConversion.getRegionalConversion(AppStrings.mExpense_meeting),
				RegionalConversion.getRegionalConversion(AppStrings.otherExpenses) };
	}

	@SuppressWarnings("deprecation")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_expenses, container, false);

		MainFragment_Dashboard.isBackpressed = false;
		mSqliteDBStoredValues_Expenses = null;

		try {
			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashInHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashInHand.setTypeface(LoginActivity.sTypeface);

			mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashAtBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashAtBank.setTypeface(LoginActivity.sTypeface);

			mHeader = (TextView) rootView.findViewById(R.id.fragmentHeader);
			mHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.expenses));
			mHeader.setTypeface(LoginActivity.sTypeface);

			mRaised_SubmitButton = (Button) rootView.findViewById(R.id.fragment_Submit_button);
			mRaised_SubmitButton.setText(RegionalConversion.getRegionalConversion(AppStrings.submit));
			mRaised_SubmitButton.setTypeface(LoginActivity.sTypeface);
			mRaised_SubmitButton.setOnClickListener(this);

			TableLayout headerTable = (TableLayout) rootView.findViewById(R.id.expenses_headerTable);

			TableLayout expensesTable = (TableLayout) rootView.findViewById(R.id.fragment_expensesTable);

			TableRow.LayoutParams headerParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);

			TableRow headRow = new TableRow(getActivity());

			TextView expensesText = new TextView(getActivity());
			expensesText.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.expenses)));
			expensesText.setTypeface(LoginActivity.sTypeface);
			expensesText.setTextColor(Color.WHITE);
			expensesText.setPadding(35, 5, 20, 5);
			expensesText.setLayoutParams(headerParams);

			expensesText.setBackgroundResource(R.color.tableHeader);
			headRow.addView(expensesText);

			TableRow.LayoutParams amountParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);

			TextView amountText = new TextView(getActivity());
			amountText.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.amount)));
			amountText.setTypeface(LoginActivity.sTypeface);
			amountText.setTextColor(Color.WHITE);
			amountText.setLayoutParams(amountParams);
			amountText.setGravity(Gravity.CENTER);
			amountText.setPadding(60, 5, 50, 5);
			amountText.setBackgroundResource(R.color.tableHeader);
			headRow.addView(amountText);

			headerTable.addView(headRow,
					new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

			mSize = sExpensesName.length;

			for (int i = 0; i < mSize; i++) {

				TableRow indv_ExpensesRow = new TableRow(getActivity());

				TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
						LayoutParams.WRAP_CONTENT, 1f);
				contentParams.setMargins(10, 5, 10, 5);

				TextView expenses_list = new TextView(getActivity());
				expenses_list.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sExpensesName[i])));
				expenses_list.setTypeface(LoginActivity.sTypeface);
				expenses_list.setTextColor(color.black);
				expenses_list.setPadding(25, 0, 5, 0);
				expenses_list.setLayoutParams(contentParams);
				indv_ExpensesRow.addView(expenses_list);

				TableRow.LayoutParams contentEditParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
						LayoutParams.WRAP_CONTENT, 1f);
				contentEditParams.setMargins(10, 5, 50, 5);

				mExpenses_values = new EditText(getActivity());
				mExpenses_values.setId(i);
				sExpensesFields.add(mExpenses_values);
				mExpenses_values.setPadding(5, 5, 5, 5);
				mExpenses_values.setBackgroundResource(R.drawable.edittext_background);
				mExpenses_values.setLayoutParams(contentEditParams);// contentParams
				// lParams
				mExpenses_values.setTextAppearance(getActivity(), R.style.MyMaterialTheme);
				mExpenses_values.setFilters(Get_EdiText_Filter.editText_filter());
				mExpenses_values.setInputType(InputType.TYPE_CLASS_NUMBER);
				mExpenses_values.setTextColor(Color.BLACK);
				mExpenses_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

					@Override
					public void onFocusChange(View v, boolean hasFocus) {
						// TODO Auto-generated method stub
						if (hasFocus) {
							((EditText) v).setGravity(Gravity.LEFT);
						} else {
							((EditText) v).setGravity(Gravity.RIGHT);
						}
					}
				});
				indv_ExpensesRow.addView(mExpenses_values);

				expensesTable.addView(indv_ExpensesRow,
						new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return rootView;
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		sExpensesAmounts = new String[sExpensesFields.size()];
		Log.d(TAG, "sExpensesAmounts size : " + sExpensesAmounts.length);

		switch (v.getId()) {
		case R.id.fragment_Submit_button:

			try {

				sExpenses_total = 0;
				sSendToServer_Expenses = Reset.reset(sSendToServer_Expenses);

				for (int i = 0; i < sExpensesAmounts.length; i++) {

					sExpensesAmounts[i] = String.valueOf(sExpensesFields.get(i).getText());

					if ((sExpensesAmounts[i].equals("")) || (sExpensesAmounts[i] == null)) {
						sExpensesAmounts[i] = nullVlaue;
					}

					if (sExpensesAmounts[i].matches("\\d*\\.?\\d+")) { // match
																		// a
																		// decimal
																		// number

						int amount = (int) Math.round(Double.parseDouble(sExpensesAmounts[i]));
						sExpensesAmounts[i] = String.valueOf(amount);
					}

					sSendToServer_Expenses = sSendToServer_Expenses + sExpensesAmounts[i] + "~";

					sExpenses_total = sExpenses_total + Integer.parseInt(sExpensesAmounts[i]);

				}

				Log.d(TAG, "Expenses Total:" + sExpenses_total);
				Log.d(TAG, "Send to server value:" + sSendToServer_Expenses);

				if (sExpenses_total != 0 || sExpenses_total <= Integer.parseInt(SelectedGroupsTask.sCashinHand)) {
					if (sExpenses_total != 0) {
						if (sExpenses_total <= Integer.parseInt(SelectedGroupsTask.sCashinHand)) {

							confirmationDialog = new Dialog(getActivity());

							LayoutInflater inflater = getActivity().getLayoutInflater();
							View dialogView = inflater.inflate(R.layout.dialog_confirmation, null);
							dialogView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
									LayoutParams.WRAP_CONTENT));

							TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
							confirmationHeader
									.setText(RegionalConversion.getRegionalConversion(AppStrings.confirmation));
							confirmationHeader.setTypeface(LoginActivity.sTypeface);

							TableLayout confirmationTable = (TableLayout) dialogView
									.findViewById(R.id.confirmationTable);

							for (int i = 0; i < mSize; i++) {

								TableRow indv_DepositEntryRow = new TableRow(getActivity());

								@SuppressWarnings("deprecation")
								TableRow.LayoutParams contentParams = new TableRow.LayoutParams(
										LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT, 1f);
								contentParams.setMargins(10, 5, 10, 5);

								TextView memberName_Text = new TextView(getActivity());
								memberName_Text.setText(
										GetSpanText.getSpanString(getActivity(), String.valueOf(sExpensesName[i])));
								memberName_Text.setTypeface(LoginActivity.sTypeface);
								memberName_Text.setTextColor(color.white);
								memberName_Text.setPadding(5, 5, 5, 5);
								memberName_Text.setLayoutParams(contentParams);
								indv_DepositEntryRow.addView(memberName_Text);

								TextView confirm_values = new TextView(getActivity());
								confirm_values.setText(
										GetSpanText.getSpanString(getActivity(), String.valueOf(sExpensesAmounts[i])));
								confirm_values.setTextColor(color.white);
								confirm_values.setPadding(5, 5, 5, 5);
								confirm_values.setGravity(Gravity.RIGHT);
								confirm_values.setLayoutParams(contentParams);
								indv_DepositEntryRow.addView(confirm_values);

								confirmationTable.addView(indv_DepositEntryRow, new TableLayout.LayoutParams(
										LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
							}

							View rullerView = new View(getActivity());
							rullerView
									.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
							rullerView.setBackgroundColor(Color.rgb(0, 199, 140));// rgb(255,
																					// 229,
																					// 242));
							confirmationTable.addView(rullerView);

							TableRow totalRow = new TableRow(getActivity());

							@SuppressWarnings("deprecation")
							TableRow.LayoutParams totalParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
									LayoutParams.WRAP_CONTENT, 1f);
							totalParams.setMargins(10, 5, 10, 5);

							TextView totalText = new TextView(getActivity());
							totalText.setText(
									GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.total)));
							totalText.setTypeface(LoginActivity.sTypeface);
							totalText.setTextColor(color.white);
							totalText.setPadding(5, 5, 5, 5);// (5, 10, 5, 10);
							totalText.setLayoutParams(totalParams);
							totalRow.addView(totalText);

							TextView totalAmount = new TextView(getActivity());
							totalAmount
									.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sExpenses_total)));
							totalAmount.setTextColor(color.white);
							totalAmount.setPadding(5, 5, 5, 5);
							totalAmount.setGravity(Gravity.RIGHT);
							totalAmount.setLayoutParams(totalParams);
							totalRow.addView(totalAmount);

							confirmationTable.addView(totalRow,
									new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

							mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit_button);
							mEdit_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.edit));
							mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
							mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
																					// 205,
																					// 0));
							mEdit_RaisedButton.setOnClickListener(this);

							mOk_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Ok_button);
							mOk_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.yes));
							mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
							mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
							mOk_RaisedButton.setOnClickListener(this);

							confirmationDialog.getWindow()
									.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
							confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
							confirmationDialog.setCanceledOnTouchOutside(false);
							confirmationDialog.setContentView(dialogView);
							confirmationDialog.setCancelable(true);
							confirmationDialog.show();

							MarginLayoutParams margin = (MarginLayoutParams) dialogView.getLayoutParams();
							margin.leftMargin = 10;
							margin.rightMargin = 10;
							margin.topMargin = 10;
							margin.bottomMargin = 10;
							margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin,
									margin.bottomMargin);

						} else {
							sSendToServer_Expenses = Reset.reset(sSendToServer_Expenses);
							sExpenses_total = Integer.valueOf("0");
							TastyToast.makeText(getActivity(), AppStrings.cashinHandAlert, TastyToast.LENGTH_SHORT,
									TastyToast.WARNING);
						}
					} else {
						sSendToServer_Expenses = Reset.reset(sSendToServer_Expenses);
						sExpenses_total = Integer.valueOf("0");
						TastyToast.makeText(getActivity(), AppStrings.nullAlert, TastyToast.LENGTH_SHORT,
								TastyToast.WARNING);
					}

				}

			} catch (Exception e) {
				// TODO: handle exception
			}
			break;

		case R.id.fragment_Edit_button:
			sSendToServer_Expenses = Reset.reset(sSendToServer_Expenses);
			sExpenses_total = Integer.valueOf("0");
			mRaised_SubmitButton.setClickable(true);
			// alertDialog.dismiss();
			isServiceCall = false;
			confirmationDialog.dismiss();

			break;
		case R.id.fragment_Ok_button:
			
			if (ConnectionUtils.isNetworkAvailable(getActivity())) {

				try {

					if (EShaktiApplication.getLoginFlag().equals(Constants.ONLINEFLAG)) {

						if (!isServiceCall) {
							isServiceCall = true;

							new Get_ExpensesTask(Transaction_ExpensesFragment.this).execute();
						}
					} else {

						TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
								TastyToast.ERROR);
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}

			} else {
				// Do offline Stuffs
				if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
					EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GET_TRANS_SINGLE,
							new GetTransactionSingle(PrefUtils.getOfflineUniqueID()));
				} else {
					TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
							TastyToast.ERROR);
				}
			}

			break;
		default:
			break;
		}
	}

	@Subscribe
	public void OnGetSingleTransactionExpense(final GetSingleTransResponse getSingleTransResponse) {
		switch (getSingleTransResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), getSingleTransResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), getSingleTransResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			try {
				System.out.println("---------OnGetSingleTransactionExpense-----------");
				String mUniqueId = GetTransactionSinglevalues.getUniqueId();
				String mExpenseValues_Offline = GetTransactionSinglevalues.getExpenses();

				String mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
				String mMobileDate = Get_Offline_MobileDate.getOffline_MobileDate(getActivity());
				String mTransaction_UniqueId = PrefUtils.getOfflineUniqueID();
				String mExpensesvalues = sSendToServer_Expenses + "#" + mTrasactiondate + "#" + mMobileDate;

				Log.e("mExpensesvalues *** ", mExpensesvalues + "");
				Log.e(TAG, mExpensesvalues);
				String mCurrentTransDate = null;

				if (publicValues.mOffline_Trans_CurrentDate != null) {
					mCurrentTransDate = publicValues.mOffline_Trans_CurrentDate;
				}

				mLastTrDate = DatePickerDialog.sDashboardDate;
				mLastTr_ID = SelectedGroupsTask.sTr_ID.toString();
				if (!mExpensesvalues.contains("?") && !mCurrentTransDate.contains("?")
						&& !mTransaction_UniqueId.contains("?")) {

					mSqliteDBStoredValues_Expenses = mExpensesvalues;
					if (mUniqueId == null && mExpenseValues_Offline == null) {

						if (mLastTrDate != null && mMobileDate != null && mTransaction_UniqueId != null
								&& mCurrentTransDate != null) {
							EShaktiApplication.getInstance().getTransactionManager().startTransaction(
									DataType.TRANSACTIONADD,
									new Transaction(0, PrefUtils.getUsernameKey(), SelectedGroupsTask.UserName,
											SelectedGroupsTask.Ngo_Id, SelectedGroupsTask.Group_Id, mTransaction_UniqueId,
											mLastTr_ID, mCurrentTransDate, null, null, null, null, mExpensesvalues, null,
											null, null, null, null, null, null, null, null, null, null, null, null, null,
											null, null, null));
						} else {

							TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT,
									TastyToast.ERROR);

						}

					} else if (mUniqueId.equalsIgnoreCase(PrefUtils.getOfflineUniqueID())
							&& mExpenseValues_Offline == null) {
						Log.v("ContentValues", "Step 4");
						EShaktiApplication.setSetTransValues(TransactionOffConstants.TRANS_EXP);

						EShaktiApplication.getInstance().getTransactionManager().startTransaction(
								DataType.TRANS_VALUES_UPDATE, new TransactionUpdate(mUniqueId, mExpensesvalues));

					} else if (mExpenseValues_Offline != null) {
						confirmationDialog.dismiss();

						TastyToast.makeText(getActivity(), AppStrings.offlineDataAvailable, TastyToast.LENGTH_SHORT,
								TastyToast.WARNING);
						DatePickerDialog.sDashboardDate = SelectedGroupsTask.sLastTransactionDate_Response;
						MainFragment_Dashboard fragment = new MainFragment_Dashboard();
						setFragment(fragment);
					}
				} else {

					if (confirmationDialog.isShowing() && confirmationDialog != null) {
						confirmationDialog.dismiss();
					}

					TastyToast.makeText(getActivity(), "Please Try Again", TastyToast.LENGTH_SHORT, TastyToast.ERROR);

					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		}
	}

	@Subscribe
	public void OnTransUpdateExpense(final TransactionUpdateResponse transactionUpdateResponse) {
		switch (transactionUpdateResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), transactionUpdateResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), transactionUpdateResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			try {
				TransactionProvider
						.getSinlgeGroupMaster_Transaction(new Transaction_UniqueIdSingle(PrefUtils.getOfflineUniqueID()));

				if (GetTransactionSinglevalues.getExpenses() != null
						&& GetTransactionSinglevalues.getExpenses().equals(mSqliteDBStoredValues_Expenses)
						&& !GetTransactionSinglevalues.getExpenses().equals("")) {
					System.out.println("----------OnTransUpdateExpense-----------");
					confirmationDialog.dismiss();
					String mCashinHand = String.valueOf(Integer.parseInt(SelectedGroupsTask.sCashinHand) - sExpenses_total);
					String mCashatBank = SelectedGroupsTask.sCashatBank;
					String mBankDetails = GetOfflineTransactionValues.getBankDetails();
					SelectedGroupsTask.sCashinHand = mCashinHand;
					String mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
					SelectedGroupsTask.sLastTransactionDate_Response = mTrasactiondate;

					String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mTrasactiondate, mCashinHand,
							mCashatBank, mBankDetails);

					Log.e("mGroupMasterResponse $$$ ", mGroupMasterResponse + "");
					String mSelectedGroupId = SelectedGroupsTask.Group_Id;
					EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GROUPMASTERUPDATE,
							new GroupDetailsUpdate(mSelectedGroupId, mGroupMasterResponse));

				} else {
					if (confirmationDialog.isShowing() && confirmationDialog != null) {
						confirmationDialog.dismiss();
					}

					TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT, TastyToast.ERROR);

					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);

				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		}
	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub
		mProgressDialog = AppDialogUtils.createProgressDialog(getActivity());
		mProgressDialog.show();
	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub
		if (mProgressDialog != null) {
			mProgressDialog.dismiss();
			if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {
				getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub

						if (!result.equals("FAIL")) {
							isServiceCall = false;
							TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg,
									TastyToast.LENGTH_SHORT, TastyToast.ERROR);
							Constants.NETWORKCOMMONFLAG = "SUCCESS";
						}

					}
				});

			} else {
				try {
					if (isGetTrid) {
						isGetTrid = false;
						callOfflineDataUpdate();
					} else {
						if (GetResponseInfo.sResponseUpdate[0].equals("Yes")) {

							new Get_LastTransactionID(Transaction_ExpensesFragment.this).execute();
							isGetTrid = true;
						} else {

							TastyToast.makeText(getActivity(), AppStrings.transactionFailAlert, TastyToast.LENGTH_SHORT,
									TastyToast.ERROR);
							MainFragment_Dashboard fragment = new MainFragment_Dashboard();
							setFragment(fragment);
						}
					}

					confirmationDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void callOfflineDataUpdate() {
		// TODO Auto-generated method stub
		String mCashinHand = SelectedGroupsTask.sCashinHand;
		String mCashatBank = SelectedGroupsTask.sCashatBank;
		String mBankDetails = GetOfflineTransactionValues.getBankDetails();
		mLastTrDate = SelectedGroupsTask.sLastTransactionDate_Response;

		String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mLastTrDate, mCashinHand, mCashatBank,
				mBankDetails);
		String mSelectedGroupId = SelectedGroupsTask.Group_Id;
		isServiceCall = false;
		EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GROUPMASTERUPDATE,
				new GroupDetailsUpdate(mSelectedGroupId, mGroupMasterResponse));
	}

	@Subscribe
	public void onAddTransactionExpense(final TransactionResponse transactionResponse) {
		switch (transactionResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), transactionResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), transactionResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			try {
				System.out.println("------------onAddTransactionExpense---------------");

				TransactionProvider
						.getSinlgeGroupMaster_Transaction(new Transaction_UniqueIdSingle(PrefUtils.getOfflineUniqueID()));

				if (GetTransactionSinglevalues.getExpenses() != null
						&& GetTransactionSinglevalues.getExpenses().equals(mSqliteDBStoredValues_Expenses)
						&& !GetTransactionSinglevalues.getExpenses().equals("")) {
					confirmationDialog.dismiss();

					String mCashinHand = String.valueOf(Integer.parseInt(SelectedGroupsTask.sCashinHand) - sExpenses_total);
					String mCashatBank = SelectedGroupsTask.sCashatBank;
					String mBankDetails = GetOfflineTransactionValues.getBankDetails();
					SelectedGroupsTask.sCashinHand = mCashinHand;
					String mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
					SelectedGroupsTask.sLastTransactionDate_Response = mTrasactiondate;
					String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mTrasactiondate, mCashinHand,
							mCashatBank, mBankDetails);
					String mSelectedGroupId = SelectedGroupsTask.Group_Id;
					EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GROUPMASTERUPDATE,
							new GroupDetailsUpdate(mSelectedGroupId, mGroupMasterResponse));
				} else {
					if (confirmationDialog.isShowing() && confirmationDialog != null) {
						confirmationDialog.dismiss();
					}

					TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT, TastyToast.ERROR);

					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);

				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		}
	}

	@Subscribe
	public void OnGroupMasExpenseUpdate(final GroupMasUpdateResponse masterResponse) {
		switch (masterResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), masterResponse.getFetcherResult().getMessage(), TastyToast.LENGTH_SHORT,
					TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), masterResponse.getFetcherResult().getMessage(), TastyToast.LENGTH_SHORT,
					TastyToast.ERROR);
			break;
		case SUCCESS:

			System.out.println("-----------OnGroupMasExpenseUpdate--------------");
			confirmationDialog.dismiss();
			try {

				TastyToast.makeText(getActivity(), AppStrings.transactionCompleted, TastyToast.LENGTH_SHORT,
						TastyToast.SUCCESS);

				if (!ConnectionUtils.isNetworkAvailable(getActivity())) {

					String mValues = Put_DB_GroupNameDetail_Response
							.put_DB_GroupNameDetails_Response(EShaktiApplication.getGroupResponse());

					GroupProvider.updateGroupResponse(
							new GroupResponseUpdate(EShaktiApplication.getGroupId_GroupLastTransDate(), mValues));
				}

				MainFragment_Dashboard fragment = new MainFragment_Dashboard();
				setFragment(fragment);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		}
	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub
		Log.e("Current Class Name!!!!!!!!!!!!!!", fragment.getClass().getName());
		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();
		
		FragmentDrawer.drawer_CashinHand.setText(RegionalConversion.getRegionalConversion(AppStrings.cashinhand)

				+ SelectedGroupsTask.sCashinHand);
		FragmentDrawer.drawer_CashinHand.setTypeface(LoginActivity.sTypeface);

		 
		FragmentDrawer.drawer_CashatBank.setText(RegionalConversion.getRegionalConversion(AppStrings.cashatBank)

				+ SelectedGroupsTask.sCashatBank);
		FragmentDrawer.drawer_CashatBank.setTypeface(LoginActivity.sTypeface);
		
		FragmentDrawer.drawer_lastTR_Date.setText(RegionalConversion.getRegionalConversion(AppStrings.lastTransactionDate)
				+ " : " + SelectedGroupsTask.sLastTransactionDate_Response);
		FragmentDrawer.drawer_lastTR_Date.setTypeface(LoginActivity.sTypeface);
		
		UpdateCalendarMinMaxDateUtils.OnUpdateCalendarDate();
		
		Calendar calender = Calendar.getInstance();

		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String formattedDate = df.format(calender.getTime());
		EShaktiApplication.setLastTransDate_GroupId(SelectedGroupsTask.Group_Id);

		new GroupTempDBLastTransDate(EShaktiApplication.getLastTransDate_GroupId(),
				SelectedGroupsTask.sLastTransactionDate_Response, formattedDate);

		GroupProvider.updateGroupLastTransDateResponse();
				
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		SBus.INST.unRegister(this);
	}

}
