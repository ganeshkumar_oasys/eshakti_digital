package com.yesteam.eshakti.view.fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.adapter.CustomExpandableMenuListAdapter;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.interfaces.ExpandListItemClickListener;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.model.ListItem;
import com.yesteam.eshakti.model.RowItem;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.views.MyExpandableListview;
import com.yesteam.eshakti.views.RaisedButton;
import com.yesteam.eshakti.webservices.Get_GroupLoanSummaryTask;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.TextView;

public class Reports_GroupLoanListReportFragment extends Fragment implements TaskListener, OnItemClickListener, ExpandListItemClickListener {

	private TextView mGroupName, mCashInHand, mCashAtBank;
	String groupLoanNames[];
	public static List<RowItem> rowItems;
	public static String SEND_TO_SERVER_LOANID;
	private Dialog mProgressDialog;
	int size;
	String LoanSummary_ServiceResponse;

	// private ListView mListView;
	//private ExpandableLayoutListView mListView;
	private MyExpandableListview mListView;
	private List<ListItem> listItems;
	//private CustomMenuListAdapter mAdapter;
	private CustomExpandableMenuListAdapter mAdapter;
	int listImage;

	public static String loanName = null;
	private TextView mHeader;
	private ArrayList<HashMap<String, String>> childList;
	private int lastExpandedPosition = -1;

	public Reports_GroupLoanListReportFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_GROUP_REPORTS_GROUP_LOAN_LIST;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_menulist_expandable, container, false);

		MainFragment_Dashboard.isBackpressed = false;
		EShaktiApplication.setFragmentMenuListView(false);

		mGroupName = (TextView) rootView.findViewById(R.id.groupname);
		mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
		mGroupName.setTypeface(LoginActivity.sTypeface);

		mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
		mCashInHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
				+ SelectedGroupsTask.sCashinHand);
		mCashInHand.setTypeface(LoginActivity.sTypeface);

		mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
		mCashAtBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
				+ SelectedGroupsTask.sCashatBank);
		mCashAtBank.setTypeface(LoginActivity.sTypeface);

		mHeader = (TextView) rootView.findViewById(R.id.expandableSubmenuHeaderTextview);
		mHeader.setVisibility(View.VISIBLE);
		mHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.groupLoansummary));
		mHeader.setTypeface(LoginActivity.sTypeface);

		size = SelectedGroupsTask.loan_Name.size();
		System.out.println("Size of Loans :" + size);

		groupLoanNames = new String[size];

		for (int i = 0; i < groupLoanNames.length; i++) {

			groupLoanNames[i] = SelectedGroupsTask.loan_Name.elementAt(i).toString();
		}

		listItems = new ArrayList<ListItem>();
		//mListView = (ExpandableLayoutListView) rootView.findViewById(R.id.fragment_List_loanrepaid);
		mListView = (MyExpandableListview) rootView.findViewById(R.id.fragment_List_loanrepaid);
		listImage = R.drawable.ic_navigate_next_white_24dp;

		for (int i = 0; i < groupLoanNames.length; i++) {
			ListItem rowItem = new ListItem(RegionalConversion.getRegionalConversion(groupLoanNames[i]), listImage);
			listItems.add(rowItem);
		}

		childList = new ArrayList<HashMap<String,String>>();
		
		for (int i = 0; i < groupLoanNames.length; i++) {
			
			if (i != groupLoanNames.length-1) {
			
				HashMap<String, String> temp = new HashMap<String, String>();
				
				temp.put("AccNo", SelectedGroupsTask.loanAcc_loanAccNo.elementAt(i));
				temp.put("BankName", SelectedGroupsTask.loanAcc_bankName.elementAt(i));
				String[] disbursementDateArr = SelectedGroupsTask.loanAcc_loanDisbursementDate.elementAt(i).split("/");
				temp.put("DisbursementDate", disbursementDateArr[1] + "/" + disbursementDateArr[0] + "/" + disbursementDateArr[2]);
				childList.add(temp);
			} else {
				HashMap<String, String> temp = new HashMap<String, String>();
				
				temp.put("AccNo", "");
				temp.put("BankName", "");
				temp.put("DisbursementDate", "");
				childList.add(temp);
			}
			
		}
		
		//mAdapter = new CustomMenuListAdapter(getActivity(), listItems);
		mAdapter = new CustomExpandableMenuListAdapter(getActivity(), listItems, childList, this);
		mListView.setAdapter(mAdapter);
		//mListView.setOnItemClickListener(this);
		
		mListView.setOnGroupExpandListener(new OnGroupExpandListener() {
			
			@Override
			public void onGroupExpand(int groupPosition) {
				if (lastExpandedPosition != -1 && groupPosition != lastExpandedPosition) {
					mListView.collapseGroup(lastExpandedPosition);
				}
				lastExpandedPosition = groupPosition;

			}
		});

		return rootView;
	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub
		mProgressDialog = AppDialogUtils.createProgressDialog(getActivity());
		mProgressDialog.show();

	}

	@SuppressWarnings("unused")
	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub
		if (mProgressDialog != null) {
			mProgressDialog.dismiss();
			if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {
				getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub

						if (!result.equals("FAIL")) {

							TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg,
									TastyToast.LENGTH_SHORT, TastyToast.ERROR);
							Constants.NETWORKCOMMONFLAG = "SUCESS";
						}

					}
				});

			} else {
				try {
					LoanSummary_ServiceResponse = Get_GroupLoanSummaryTask.sGroup_Loan_summary_response;
					if (SEND_TO_SERVER_LOANID.equals("0")) {

						System.out.println("LoanSummary_ServiceResponse :" + LoanSummary_ServiceResponse);
						Reports_GroupPersonalLoanSummaryFragment fragment = new Reports_GroupPersonalLoanSummaryFragment();
						FragmentTransaction frTransaction = getFragmentManager().beginTransaction();
						frTransaction.replace(R.id.frame, fragment);
						frTransaction.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out)
								.show(fragment);
						frTransaction.addToBackStack(null);
						frTransaction.commit();
					} else {
						String offlineServiceResponse = Get_GroupLoanSummaryTask.sGroup_Loan_summary_response;
						Reports_GroupLoanSummaryFragment fragment = new Reports_GroupLoanSummaryFragment();
						setFragment(fragment);
					}

				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
		// TODO Auto-generated method stub

		try {
			loanName = SelectedGroupsTask.loan_Name.elementAt(position).toString();

			RaisedButton button = (RaisedButton) view.findViewById(R.id.fragment_menulist_next_button);
			TextView mAccNo = (TextView) view.findViewById(R.id.accNumber);
			TextView mAccNo_Values = (TextView) view.findViewById(R.id.AccNumber_values);
			TextView mBankName = (TextView) view.findViewById(R.id.bankName);
			TextView mBankNameValues = (TextView) view.findViewById(R.id.bankName_values);
			TextView mdisbursementTime = (TextView) view.findViewById(R.id.disbursementTime);
			TextView mdisbursementTimeValues = (TextView) view.findViewById(R.id.disbursementTime_values);

			if (position == (SelectedGroupsTask.loan_Name.size() - 1)) {
				mAccNo.setVisibility(View.GONE);
				mAccNo_Values.setVisibility(View.GONE);

				mBankName.setVisibility(View.GONE);
				mBankNameValues.setVisibility(View.GONE);

				mdisbursementTime.setVisibility(View.GONE);
				mdisbursementTimeValues.setVisibility(View.GONE);
			} else {

				System.out.println(
						" ---------------------------------______________________--------------------->>>>>>>>         "
								+ SelectedGroupsTask.loanAcc_loanAccNo.get(position));
				mAccNo.setText(AppStrings.mAccountNumber + " :  ");
				mAccNo_Values.setText(SelectedGroupsTask.loanAcc_loanAccNo.get(position));

				mBankName.setText(AppStrings.bankName + " :  ");
				mBankName.setPadding(5, 5, 5, 5);
				mBankNameValues.setText(SelectedGroupsTask.loanAcc_bankName.get(position));
				mBankNameValues.setPadding(5, 5, 5, 5);
				mBankName.setTypeface(LoginActivity.sTypeface);
				mBankNameValues.setTypeface(LoginActivity.sTypeface);

				mdisbursementTime.setText(AppStrings.mLoanDisbursementDate + " :  ");
				mdisbursementTime.setPadding(5, 5, 5, 5);
				mdisbursementTimeValues.setText(SelectedGroupsTask.loanAcc_loanDisbursementDate.get(position));
				mdisbursementTimeValues.setPadding(5, 5, 5, 5);
				mdisbursementTime.setTypeface(LoginActivity.sTypeface);
				mdisbursementTimeValues.setTypeface(LoginActivity.sTypeface);

			}
			mAccNo.setPadding(5, 5, 5, 5);
			mAccNo_Values.setPadding(5, 5, 5, 5);
			mAccNo.setTypeface(LoginActivity.sTypeface);
			mAccNo_Values.setTypeface(LoginActivity.sTypeface);

			mAccNo.setTextColor(Color.BLACK);
			mAccNo_Values.setTextColor(Color.BLACK);
			mBankName.setTextColor(Color.BLACK);
			mBankNameValues.setTextColor(Color.BLACK);

			mdisbursementTime.setTextColor(Color.BLACK);
			mdisbursementTimeValues.setTextColor(Color.BLACK);

			button.setTypeface(LoginActivity.sTypeface);
			button.setText(RegionalConversion.getRegionalConversion(AppStrings.mSelect));

			button.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					try {

						SEND_TO_SERVER_LOANID = SelectedGroupsTask.loan_Id.elementAt(position).toString();
						System.out.println("loan :" + SEND_TO_SERVER_LOANID);

						Log.e("Loan Id------------------>>>>>", SEND_TO_SERVER_LOANID);

						if (ConnectionUtils.isNetworkAvailable(getActivity())) {

							new Get_GroupLoanSummaryTask(Reports_GroupLoanListReportFragment.this).execute();

						} else if (!ConnectionUtils.isNetworkAvailable(getActivity())) {
							TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
									TastyToast.ERROR);
						}

					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
				}
			});

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub
		Log.e("Current Class Name!!!!!!!!!!!!!!", fragment.getClass().getName());
		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

	}

	@Override
	public void onItemClick(ViewGroup parent, View view, int position) {
		// TODO Auto-generated method stub
		
		try {
			loanName = SelectedGroupsTask.loan_Name.elementAt(position).toString();
			
			SEND_TO_SERVER_LOANID = SelectedGroupsTask.loan_Id.elementAt(position).toString();
			System.out.println("loan :" + SEND_TO_SERVER_LOANID);

			Log.e("Loan Id------------------>>>>>", SEND_TO_SERVER_LOANID);

			if (ConnectionUtils.isNetworkAvailable(getActivity())) {

				new Get_GroupLoanSummaryTask(Reports_GroupLoanListReportFragment.this).execute();

			} else if (!ConnectionUtils.isNetworkAvailable(getActivity())) {
				TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
						TastyToast.ERROR);
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

    @Override
    public void onItemClickVerification(ViewGroup parent, View view, int position) {

    }

}
