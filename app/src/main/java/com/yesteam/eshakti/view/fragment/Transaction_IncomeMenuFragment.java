package com.yesteam.eshakti.view.fragment;

import java.io.InterruptedIOException;
import java.util.ArrayList;
import java.util.List;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.adapter.CustomListAdapter;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.interfaces.RecyclerViewListener;
import com.yesteam.eshakti.model.ListItem;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

public class Transaction_IncomeMenuFragment extends Fragment implements RecyclerViewListener, OnItemClickListener {

	public static String TAG = Transaction_IncomeMenuFragment.class.getSimpleName();

	private TextView mGroupName, mCashinHand, mCashatBank;
	public static String sSelectedIncomeMenu = null;

	String[] incomeMenu;/*
						 * = { AppStrings.subscriptioncharges,
						 * AppStrings.penalty,AppStrings.otherincome,
						 * AppStrings.donation, AppStrings.mSeedFund};
						 */

	private ListView mListView;
	private List<ListItem> listItems;
	private CustomListAdapter mAdapter;
	int listImage;
	private TextView mHeader;

	public Transaction_IncomeMenuFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_INCOME_MENU;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		incomeMenu = new String[] { AppStrings.subscriptioncharges, AppStrings.penalty, AppStrings.otherincome,
				AppStrings.donation, AppStrings.mSeedFund };

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_menulist, container, false);

		MainFragment_Dashboard.isBackpressed = false;

		try {
			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashinHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashinHand.setTypeface(LoginActivity.sTypeface);

			mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashatBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashatBank.setTypeface(LoginActivity.sTypeface);
			
			mHeader = (TextView) rootView.findViewById(R.id.submenuHeaderTextview);
			mHeader.setVisibility(View.VISIBLE);
			mHeader.setText(
					RegionalConversion.getRegionalConversion(AppStrings.income));
			mHeader.setTypeface(LoginActivity.sTypeface);

			listItems = new ArrayList<ListItem>();
			mListView = (ListView) rootView.findViewById(R.id.fragment_List);
			listImage = R.drawable.ic_navigate_next_white_24dp;

			for (int i = 0; i < incomeMenu.length; i++) {
				ListItem rowItem = new ListItem(incomeMenu[i].toString(), listImage);
				listItems.add(rowItem);
			}

			mAdapter = new CustomListAdapter(getActivity(), listItems);
			mListView.setAdapter(mAdapter);
			mListView.setOnItemClickListener(this);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return rootView;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	@Override
	public void recyclerViewListClicked(View view, int position) throws InterruptedIOException {
		// TODO Auto-generated method stub

		sSelectedIncomeMenu = String.valueOf(incomeMenu[position]);

		if ((position == 0) || (position == 1) || (position == 2)) {

			Transaction_OI_S_P_Fragment fragment = new Transaction_OI_S_P_Fragment();
			setFragment(fragment);

		} else if ((position == 3)) {

			Transaction_Donation_FIncomeFragment fragment = new Transaction_Donation_FIncomeFragment();
			setFragment(fragment);
		} else if (position == 5) {
			Transaction_SeedFundFragment fragment = new Transaction_SeedFundFragment();
			setFragment(fragment);
		}

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		// TODO Auto-generated method stub

		TextView textColor_Change = (TextView) view.findViewById(R.id.dynamicText);
		textColor_Change.setText(String.valueOf(incomeMenu[position]));
		textColor_Change.setTextColor(Color.rgb(251, 161, 108));

		sSelectedIncomeMenu = String.valueOf(incomeMenu[position]);

		if ((position == 0) || (position == 1) || (position == 2)) {

			if (position == 2) {
				EShaktiApplication.setOtherIncomeFragment(true);
			} else {
				EShaktiApplication.setOtherIncomeFragment(false);
			}
			Transaction_OI_S_P_Fragment fragment = new Transaction_OI_S_P_Fragment();
			setFragment(fragment);

		} else if ((position == 3)) {

			Transaction_Donation_FIncomeFragment fragment = new Transaction_Donation_FIncomeFragment();
			setFragment(fragment);
		} else if (position == 4) {
			Transaction_SeedFundFragment fragment = new Transaction_SeedFundFragment();
			setFragment(fragment);
		}

	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub
		Log.e("Income Menu Current Class Name!!!!!!!!!!!!!!", fragment.getClass().getName());
		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

	}

}
