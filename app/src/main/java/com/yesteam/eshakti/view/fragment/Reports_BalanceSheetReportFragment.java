package com.yesteam.eshakti.view.fragment;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.Get_BalanceSheetTask;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;


public class Reports_BalanceSheetReportFragment extends Fragment {

	private TextView mGroupName, mCashInHand, mCashAtBank, mHeadertext;
	private String mLanguagelocale = "";

	public Reports_BalanceSheetReportFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_BALANCE_SHEET_REPORTS;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_balancesheet,
				container, false);

		MainFragment_Dashboard.isBackpressed = false;
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_BALANCE_SHEET_REPORTS;

		try {
			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String
					.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashInHand.setText(RegionalConversion.getRegionalConversion(String
					.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashInHand.setTypeface(LoginActivity.sTypeface);

			mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashAtBank.setText(RegionalConversion.getRegionalConversion(String
					.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashAtBank.setTypeface(LoginActivity.sTypeface);

			mHeadertext = (TextView) rootView
					.findViewById(R.id.fragment_BalanceSheet_headertext);
			mHeadertext.setText(RegionalConversion
					.getRegionalConversion(AppStrings.balanceSheet));
			mHeadertext.setTypeface(LoginActivity.sTypeface);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		TableLayout.LayoutParams params = new TableLayout.LayoutParams(
				TableLayout.LayoutParams.MATCH_PARENT,
				TableLayout.LayoutParams.WRAP_CONTENT, 1f);
		params.setMargins(10, 5, 10, 5);

		/************ CASHFLOW HEADER TABLE **********/

		TableLayout cashflowHeadertable = (TableLayout) rootView
				.findViewById(R.id.fragment_cashflowTable);
		cashflowHeadertable
				.setBackgroundResource(R.drawable.heading_background);
		cashflowHeadertable.setLayoutParams(params);
		mLanguagelocale = PrefUtils.getUserlangcode();
		try {

			TableRow cashflowheader = new TableRow(getActivity());

			TextView cashflowstmt = new TextView(getActivity());
			cashflowstmt.setText(RegionalConversion.getRegionalConversion(AppStrings.cashFlowStatement));
			cashflowstmt.setTypeface(LoginActivity.sTypeface);
			cashflowstmt.setPadding(5, 5, 5, 5);
			cashflowstmt.setTextColor(Color.WHITE);
			cashflowheader.addView(cashflowstmt);

			TextView cashFromDate = new TextView(getActivity());
			cashFromDate.setTypeface(LoginActivity.sTypeface);
			cashFromDate.setPadding(10, 5, 5, 5);
			cashFromDate.setTextColor(Color.WHITE);

			TextView calFromDate = new TextView(getActivity());
			calFromDate.setPadding(10, 5, 5, 5);
			calFromDate.setTextColor(Color.WHITE);

			TextView cashToDate = new TextView(getActivity());
			cashToDate.setTypeface(LoginActivity.sTypeface);
			cashToDate.setPadding(10, 5, 5, 5);
			cashToDate.setGravity(Gravity.CENTER);
			cashToDate.setTextColor(Color.WHITE);

			TextView calToDate = new TextView(getActivity());
			calToDate.setPadding(10, 5, 10, 5);
			calToDate.setTextColor(Color.WHITE);

			if (mLanguagelocale.equalsIgnoreCase("English")) {

				cashFromDate.setText(RegionalConversion.getRegionalConversion(AppStrings.fromDate));
				cashflowheader.addView(cashFromDate);

				calFromDate.setText(RegionalConversion.getRegionalConversion(Reports_Trial_BalanceSheetFragment.fromDate));
				cashflowheader.addView(calFromDate);

				cashToDate.setText(RegionalConversion.getRegionalConversion(AppStrings.toDate));
				cashflowheader.addView(cashToDate);

				calToDate.setText(RegionalConversion.getRegionalConversion(Reports_Trial_BalanceSheetFragment.toDate));
				cashflowheader.addView(calToDate);

			} else if (mLanguagelocale.equalsIgnoreCase("Tamil")) {

				calFromDate.setText(RegionalConversion.getRegionalConversion(Reports_Trial_BalanceSheetFragment.fromDate));
				cashflowheader.addView(calFromDate);

				cashFromDate.setText(RegionalConversion.getRegionalConversion(AppStrings.fromDate));
				cashflowheader.addView(cashFromDate);

				calToDate.setText(RegionalConversion.getRegionalConversion(Reports_Trial_BalanceSheetFragment.toDate));
				cashflowheader.addView(calToDate);

				cashToDate.setText(RegionalConversion.getRegionalConversion(AppStrings.toDate));
				cashflowheader.addView(cashToDate);

			} else if (mLanguagelocale.equalsIgnoreCase("Hindi")) {

				calFromDate.setText(RegionalConversion.getRegionalConversion(Reports_Trial_BalanceSheetFragment.fromDate));
				cashflowheader.addView(calFromDate);

				cashFromDate.setText(RegionalConversion.getRegionalConversion(AppStrings.fromDate));
				cashflowheader.addView(cashFromDate);

				calToDate.setText(RegionalConversion.getRegionalConversion(Reports_Trial_BalanceSheetFragment.toDate));
				cashflowheader.addView(calToDate);

				cashToDate.setText(RegionalConversion.getRegionalConversion(AppStrings.toDate));
				cashflowheader.addView(cashToDate);

			} else if (mLanguagelocale.equalsIgnoreCase("Marathi")) {

				calFromDate.setText(RegionalConversion.getRegionalConversion(Reports_Trial_BalanceSheetFragment.fromDate));
				cashflowheader.addView(calFromDate);

				cashFromDate.setText(RegionalConversion.getRegionalConversion(AppStrings.fromDate));
				cashflowheader.addView(cashFromDate);

				calToDate.setText(RegionalConversion.getRegionalConversion(Reports_Trial_BalanceSheetFragment.toDate));
				cashflowheader.addView(calToDate);

				cashToDate.setText(RegionalConversion.getRegionalConversion(AppStrings.toDate));
				cashflowheader.addView(cashToDate);

			}else {
				cashFromDate.setText(RegionalConversion.getRegionalConversion(AppStrings.fromDate));
				cashflowheader.addView(cashFromDate);

				calFromDate.setText(RegionalConversion.getRegionalConversion(Reports_Trial_BalanceSheetFragment.fromDate));
				cashflowheader.addView(calFromDate);

				cashToDate.setText(RegionalConversion.getRegionalConversion(AppStrings.toDate));
				cashflowheader.addView(cashToDate);

				calToDate.setText(RegionalConversion.getRegionalConversion(Reports_Trial_BalanceSheetFragment.toDate));
				cashflowheader.addView(calToDate);

			}

			cashflowHeadertable.addView(cashflowheader, params);

		} catch (Exception e) {
			e.printStackTrace();
		}

		/************ CASHFLOW CONTENT TABLE **********/

		TableLayout cashflowContentTable = (TableLayout) rootView
				.findViewById(R.id.fragment_cashflowContentTable);
		cashflowContentTable
				.setBackgroundResource(R.drawable.content_background);
		cashflowContentTable.setLayoutParams(params);// doubt

		try {
			TableRow cashRowHeader = new TableRow(getActivity());
			cashRowHeader.setId(10);

			TextView payment = new TextView(getActivity());
			payment.setText(GetSpanText.getSpanString(getActivity(),
					String.valueOf(AppStrings.payment)));
			payment.setTypeface(LoginActivity.sTypeface);
			payment.setPadding(20, 5, 5, 5);
			payment.setTextColor(Color.rgb(59, 133, 72));
			cashRowHeader.addView(payment);

			TextView paymentAmount = new TextView(getActivity());
			paymentAmount.setText(GetSpanText.getSpanString(getActivity(),
					String.valueOf(AppStrings.amount)));
			paymentAmount.setTypeface(LoginActivity.sTypeface);
			paymentAmount.setGravity(Gravity.RIGHT);
			paymentAmount.setPadding(10, 5, 5, 5);
			paymentAmount.setTextColor(Color.rgb(59, 133, 72));
			cashRowHeader.addView(paymentAmount);

			TextView receipt = new TextView(getActivity());
			receipt.setText(GetSpanText.getSpanString(getActivity(),
					String.valueOf(AppStrings.receipt)));
			receipt.setTypeface(LoginActivity.sTypeface);
			receipt.setPadding(10, 5, 5, 5);
			receipt.setTextColor(Color.rgb(59, 133, 72));
			cashRowHeader.addView(receipt);

			TextView amount = new TextView(getActivity());
			amount.setText(GetSpanText.getSpanString(getActivity(),
					String.valueOf(AppStrings.amount)));
			amount.setTypeface(LoginActivity.sTypeface);
			amount.setGravity(Gravity.RIGHT);
			amount.setPadding(10, 5, 5, 5);
			amount.setTextColor(Color.rgb(59, 133, 72));
			cashRowHeader.addView(amount);

			cashflowContentTable.addView(cashRowHeader);
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*	
					*//********** Splitter **********/
		/*
		 * 
		 * try { View splitRow = new View(getActivity()); TableRow.LayoutParams
		 * splitParams = new
		 * TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,2,1f);
		 * splitRow.setBackgroundColor(Color.DKGRAY);
		 * splitRow.setLayoutParams(splitParams);
		 * 
		 * cashflowContentTable.addView(splitRow); } catch (Exception e) { //
		 * TODO: handle exception e.printStackTrace(); }
		 */try {
			int size = Get_BalanceSheetTask.cashFlowPaymentVector.size();

			for (int i = 0; i < size; i = i + 2) {
				TableRow cashflowContentRow = new TableRow(getActivity());
				cashflowContentRow.setId(10);

				TextView payment = new TextView(getActivity());
				payment.setId(1);
				payment.setText(GetSpanText.getSpanString(getActivity(), String
						.valueOf(Get_BalanceSheetTask.cashFlowPaymentVector
								.elementAt(i))));
				payment.setTypeface(LoginActivity.sTypeface);
				payment.setPadding(20, 5, 10, 5);
				payment.setTextColor(color.black);
				cashflowContentRow.addView(payment);

				TextView paymentAmount = new TextView(getActivity());
				paymentAmount.setId(2);
				paymentAmount
						.setText(GetSpanText.getSpanString(
								getActivity(),
								String.valueOf(Get_BalanceSheetTask.cashFlowPaymentVector
										.elementAt(i + 1))));
				paymentAmount.setPadding(10, 5, 10, 5);
				paymentAmount.setGravity(Gravity.RIGHT);
				paymentAmount.setTextColor(color.black);
				cashflowContentRow.addView(paymentAmount);

				TextView receipt = new TextView(getActivity());
				receipt.setId(3);
				receipt.setText(GetSpanText.getSpanString(getActivity(), String
						.valueOf(Get_BalanceSheetTask.cashFlowReceiptVector
								.elementAt(i))));
				receipt.setTypeface(LoginActivity.sTypeface);
				receipt.setPadding(10, 5, 10, 5);
				receipt.setTextColor(color.black);
				cashflowContentRow.addView(receipt);

				TextView receiptAmount = new TextView(getActivity());
				receiptAmount.setId(4);
				receiptAmount
						.setText(GetSpanText.getSpanString(
								getActivity(),
								String.valueOf(Get_BalanceSheetTask.cashFlowReceiptVector
										.elementAt(i + 1))));
				receiptAmount.setGravity(Gravity.RIGHT);
				receiptAmount.setPadding(10, 5, 10, 5);
				receiptAmount.setTextColor(color.black);
				cashflowContentRow.addView(receiptAmount);

				cashflowContentTable.addView(cashflowContentRow);

			}
		} catch (Exception e) {
			System.out.println("CashFlow Table content Exception :"
					+ e.toString());
		}

		/************ CASHFLOW TOTAL **********/
		try {
			TableRow cashflowTotalRow = new TableRow(getActivity());
			cashflowTotalRow.setId(10);

			TextView paymentTotal = new TextView(getActivity());
			paymentTotal.setId(1);
			paymentTotal.setText(GetSpanText.getSpanString(getActivity(),
					String.valueOf(AppStrings.total)));
			paymentTotal.setTypeface(LoginActivity.sTypeface, Typeface.BOLD);
			paymentTotal.setPadding(20, 5, 10, 5);
			paymentTotal.setTextColor(Color.BLACK);
			cashflowTotalRow.addView(paymentTotal);

			TextView paymentAmount = new TextView(getActivity());
			paymentAmount.setId(1);

			paymentAmount.setText(GetSpanText.getSpanString(getActivity(),
					String.valueOf((Get_BalanceSheetTask.leftTotal))));
			paymentAmount.setGravity(Gravity.RIGHT);
			paymentAmount.setPadding(10, 5, 10, 5);
			paymentAmount.setTypeface(null, Typeface.BOLD);
			paymentAmount.setTextColor(Color.BLACK);
			cashflowTotalRow.addView(paymentAmount);

			TextView receiptTotal = new TextView(getActivity());
			receiptTotal.setId(1);
			receiptTotal.setText(GetSpanText.getSpanString(getActivity(),
					String.valueOf(AppStrings.total)));
			receiptTotal.setTypeface(LoginActivity.sTypeface, Typeface.BOLD);
			receiptTotal.setPadding(10, 5, 10, 5);
			receiptTotal.setTextColor(Color.BLACK);
			cashflowTotalRow.addView(receiptTotal);

			TextView receiptAmount = new TextView(getActivity());
			receiptAmount.setId(1);
			receiptAmount.setText(GetSpanText.getSpanString(getActivity(),
					String.valueOf(Get_BalanceSheetTask.rightTotal)));
			receiptAmount.setGravity(Gravity.RIGHT);
			receiptAmount.setPadding(10, 5, 10, 5);
			receiptAmount.setTypeface(null, Typeface.BOLD);
			receiptAmount.setTextColor(Color.BLACK);
			cashflowTotalRow.addView(receiptAmount);

			cashflowContentTable.addView(cashflowTotalRow);

		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("CashFlow Table Total Exception :"
					+ e.toString());
		}

		/*********** INCOME EXPENDITURE HEADER TABLE **********/

		TableLayout incomeExpenditureHeaderTable = (TableLayout) rootView
				.findViewById(R.id.fragment_incomeExpenditureTable);
		incomeExpenditureHeaderTable
				.setBackgroundResource(R.drawable.heading_background);
		incomeExpenditureHeaderTable.setLayoutParams(params);// doubt

		try {

			TableRow incomeheader = new TableRow(getActivity());

			TextView income = new TextView(getActivity());
			income.setText(GetSpanText.getSpanString(getActivity(),
					String.valueOf(AppStrings.IncomeExpenditure))
					+ " ");
			income.setTypeface(LoginActivity.sTypeface);
			income.setPadding(10, 5, 10, 5);
			income.setTextColor(Color.WHITE);
			incomeheader.addView(income);

			TextView incomeFromDate = new TextView(getActivity());
			incomeFromDate.setTypeface(LoginActivity.sTypeface);
			incomeFromDate.setPadding(10, 5, 10, 5);
			incomeFromDate.setTextColor(Color.WHITE);

			TextView calFromDate = new TextView(getActivity());
			calFromDate.setPadding(10, 5, 10, 5);
			calFromDate.setTextColor(Color.WHITE);

			TextView incomeToDate = new TextView(getActivity());
			incomeToDate.setTypeface(LoginActivity.sTypeface);
			incomeToDate.setPadding(10, 5, 20, 5);
			incomeToDate.setTextColor(Color.WHITE);

			TextView calToDate = new TextView(getActivity());
			calToDate.setPadding(10, 5, 10, 5);
			calToDate.setTextColor(Color.WHITE);

			if (mLanguagelocale.equalsIgnoreCase("English")) {

				incomeFromDate.setText(RegionalConversion.getRegionalConversion(" " + AppStrings.fromDate));
				incomeheader.addView(incomeFromDate);

				calFromDate.setText(RegionalConversion.getRegionalConversion(Reports_Trial_BalanceSheetFragment.fromDate));
				incomeheader.addView(calFromDate);

				incomeToDate.setText(RegionalConversion.getRegionalConversion(" " + AppStrings.toDate));
				incomeheader.addView(incomeToDate);

				calToDate.setText(RegionalConversion.getRegionalConversion(Reports_Trial_BalanceSheetFragment.toDate));
				incomeheader.addView(calToDate);

			} else if (mLanguagelocale.equalsIgnoreCase("Tamil")) {

				calFromDate.setText(RegionalConversion.getRegionalConversion(Reports_Trial_BalanceSheetFragment.fromDate));
				incomeheader.addView(calFromDate);

				incomeFromDate.setText(RegionalConversion.getRegionalConversion(" " + AppStrings.fromDate));
				incomeheader.addView(incomeFromDate);

				calToDate.setText(RegionalConversion.getRegionalConversion(Reports_Trial_BalanceSheetFragment.toDate));
				incomeheader.addView(calToDate);

				incomeToDate.setText(RegionalConversion.getRegionalConversion(" " + AppStrings.toDate));
				incomeheader.addView(incomeToDate);

			} else if (mLanguagelocale.equalsIgnoreCase("Hindi")) {

				calFromDate.setText(RegionalConversion.getRegionalConversion(Reports_Trial_BalanceSheetFragment.fromDate));
				incomeheader.addView(calFromDate);

				incomeFromDate.setText(RegionalConversion.getRegionalConversion(" " + AppStrings.fromDate));
				incomeheader.addView(incomeFromDate);

				calToDate.setText(RegionalConversion.getRegionalConversion(Reports_Trial_BalanceSheetFragment.toDate));
				incomeheader.addView(calToDate);

				incomeToDate.setText(RegionalConversion.getRegionalConversion(" " + AppStrings.toDate));
				incomeheader.addView(incomeToDate);

			} else if (mLanguagelocale.equalsIgnoreCase("Marathi")) {

				calFromDate.setText(RegionalConversion.getRegionalConversion(Reports_Trial_BalanceSheetFragment.fromDate));
				incomeheader.addView(calFromDate);

				incomeFromDate.setText(RegionalConversion.getRegionalConversion(" " + AppStrings.fromDate));
				incomeheader.addView(incomeFromDate);

				calToDate.setText(RegionalConversion.getRegionalConversion(Reports_Trial_BalanceSheetFragment.toDate));
				incomeheader.addView(calToDate);

				incomeToDate.setText(RegionalConversion.getRegionalConversion(" " + AppStrings.toDate));
				incomeheader.addView(incomeToDate);

			} else {
				incomeFromDate.setText(RegionalConversion.getRegionalConversion(" " + AppStrings.fromDate));
				incomeheader.addView(incomeFromDate);

				calFromDate.setText(RegionalConversion.getRegionalConversion(Reports_Trial_BalanceSheetFragment.fromDate));
				incomeheader.addView(calFromDate);

				incomeToDate.setText(RegionalConversion.getRegionalConversion(" " + AppStrings.toDate));
				incomeheader.addView(incomeToDate);

				calToDate.setText(RegionalConversion.getRegionalConversion(Reports_Trial_BalanceSheetFragment.toDate));
				incomeheader.addView(calToDate);

			}



			incomeExpenditureHeaderTable.addView(incomeheader);
		} catch (Exception e) {
			System.out.println("INCOME EXPENDITURE HEADER TABLE EXCEPTION :"
					+ e.toString());
		}

		/*********** INCOME EXPENDITURE CONTENT TABLE **********/

		TableLayout incomeExpenditureContentTable = (TableLayout) rootView
				.findViewById(R.id.fragment_incomeExpenditureContentTable);
		incomeExpenditureContentTable
				.setBackgroundResource(R.drawable.content_background);
		incomeExpenditureContentTable.setLayoutParams(params);

		try {
			TableRow incomeRowHeader = new TableRow(getActivity());
			incomeRowHeader.setId(10);

			TextView payment = new TextView(getActivity());
			payment.setText(GetSpanText.getSpanString(getActivity(),
					String.valueOf(AppStrings.payment)));
			payment.setTypeface(LoginActivity.sTypeface);
			payment.setPadding(20, 5, 10, 5);
			payment.setTextColor(Color.rgb(59, 133, 72));
			incomeRowHeader.addView(payment);

			TextView paymentAmount = new TextView(getActivity());
			paymentAmount.setText(GetSpanText.getSpanString(getActivity(),
					String.valueOf(AppStrings.amount)));
			paymentAmount.setTypeface(LoginActivity.sTypeface);
			paymentAmount.setGravity(Gravity.RIGHT);
			paymentAmount.setPadding(10, 5, 10, 5);
			paymentAmount.setTextColor(Color.rgb(59, 133, 72));
			incomeRowHeader.addView(paymentAmount);

			TextView receipt = new TextView(getActivity());
			receipt.setText(GetSpanText.getSpanString(getActivity(),
					String.valueOf(AppStrings.receipt)));
			receipt.setTypeface(LoginActivity.sTypeface);
			receipt.setPadding(10, 5, 10, 5);
			receipt.setTextColor(Color.rgb(59, 133, 72));
			incomeRowHeader.addView(receipt);

			TextView amount = new TextView(getActivity());
			amount.setText(GetSpanText.getSpanString(getActivity(),
					String.valueOf(AppStrings.amount)));
			amount.setTypeface(LoginActivity.sTypeface);
			amount.setGravity(Gravity.RIGHT);
			amount.setPadding(10, 5, 20, 5);
			amount.setTextColor(Color.rgb(59, 133, 72));
			incomeRowHeader.addView(amount);

			incomeExpenditureContentTable.addView(incomeRowHeader);

		} catch (Exception e) {
			// TODO: handle exception
			System.out
					.println("IncomeExpenditure Content Table header Exception :"
							+ e.toString());
		}

		try {

			int size = Get_BalanceSheetTask.income_total.size();
			System.out.println("Income size :" + size);
			for (int i = 0; i < size; i = i + 2) {
				TableRow incomeContentRow = new TableRow(getActivity());
				incomeContentRow.setId(10);

				TextView payment = new TextView(getActivity());
				payment.setId(1);
				payment.setText(GetSpanText.getSpanString(getActivity(),
						String.valueOf(Get_BalanceSheetTask.income_total
								.elementAt(i))));
				payment.setTypeface(LoginActivity.sTypeface);
				payment.setPadding(20, 5, 10, 5);
				payment.setTextColor(color.black);
				incomeContentRow.addView(payment);

				TextView paymentAmount = new TextView(getActivity());
				paymentAmount.setId(2);
				paymentAmount.setText(GetSpanText.getSpanString(getActivity(),
						String.valueOf(Get_BalanceSheetTask.income_total
								.elementAt(i + 1))));
				paymentAmount.setPadding(10, 5, 10, 5);
				paymentAmount.setGravity(Gravity.RIGHT);
				paymentAmount.setTextColor(color.black);
				incomeContentRow.addView(paymentAmount);

				TextView receipt = new TextView(getActivity());
				receipt.setId(3);
				receipt.setText(GetSpanText.getSpanString(getActivity(), String
						.valueOf(Get_BalanceSheetTask.expenditure_total
								.elementAt(i))));
				receipt.setTypeface(LoginActivity.sTypeface);
				receipt.setPadding(10, 5, 10, 5);
				receipt.setTextColor(color.black);
				incomeContentRow.addView(receipt);

				TextView receiptAmount = new TextView(getActivity());
				receiptAmount.setId(4);
				receiptAmount.setText(GetSpanText.getSpanString(getActivity(),
						String.valueOf(Get_BalanceSheetTask.expenditure_total
								.elementAt(i + 1))));
				receiptAmount.setGravity(Gravity.RIGHT);
				receiptAmount.setPadding(10, 5, 20, 5);
				receiptAmount.setTextColor(color.black);
				incomeContentRow.addView(receiptAmount);

				incomeExpenditureContentTable.addView(incomeContentRow);

			}

		} catch (Exception e) {
			System.out.println("Incomeexpenditure Table Content Exception :"
					+ e.toString());
		}

		/*********** INCOME EXPENDITURE TOTAL **********/
		try {
			TableRow incomeTotalRow = new TableRow(getActivity());
			incomeTotalRow.setId(10);

			TextView paymentTotal = new TextView(getActivity());
			paymentTotal.setId(1);
			paymentTotal.setText(GetSpanText.getSpanString(getActivity(),
					String.valueOf(AppStrings.total)));
			paymentTotal.setTypeface(LoginActivity.sTypeface, Typeface.BOLD);
			paymentTotal.setPadding(20, 5, 10, 5);
			paymentTotal.setTextColor(Color.BLACK);
			incomeTotalRow.addView(paymentTotal);

			TextView paymentAmount = new TextView(getActivity());
			paymentAmount.setId(1);
			paymentAmount.setText(GetSpanText.getSpanString(getActivity(),
					String.valueOf(Get_BalanceSheetTask.incomeTotal)));
			paymentAmount.setGravity(Gravity.RIGHT);
			paymentAmount.setPadding(10, 5, 10, 5);
			paymentAmount.setTypeface(null, Typeface.BOLD);
			paymentAmount.setTextColor(Color.BLACK);
			incomeTotalRow.addView(paymentAmount);

			TextView receiptTotal = new TextView(getActivity());
			receiptTotal.setId(1);
			receiptTotal.setText(GetSpanText.getSpanString(getActivity(),
					String.valueOf(AppStrings.total)));
			receiptTotal.setTypeface(LoginActivity.sTypeface, Typeface.BOLD);
			receiptTotal.setPadding(10, 5, 10, 5);
			receiptTotal.setTextColor(Color.BLACK);
			incomeTotalRow.addView(receiptTotal);

			TextView receiptAmount = new TextView(getActivity());
			receiptAmount.setId(1);
			receiptAmount.setText(GetSpanText.getSpanString(getActivity(),
					String.valueOf(Get_BalanceSheetTask.expenditureTotal)));
			receiptAmount.setGravity(Gravity.RIGHT);
			receiptAmount.setPadding(10, 5, 20, 5);
			receiptAmount.setTypeface(null, Typeface.BOLD);
			receiptAmount.setTextColor(Color.BLACK);
			incomeTotalRow.addView(receiptAmount);

			incomeExpenditureContentTable.addView(incomeTotalRow);

		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Incomeexpenditure Table Total Exception :"
					+ e.toString());
		}

		/*********** BALANCESHEET TABLE HEADER **********/

		TableLayout balancesheetHeaderTable = (TableLayout) rootView
				.findViewById(R.id.fragment_balancesheetTable);
		balancesheetHeaderTable
				.setBackgroundResource(R.drawable.heading_background);
		balancesheetHeaderTable.setLayoutParams(params);

		try {
			TableRow balanceSheetHeader = new TableRow(getActivity());

			TextView balancesheet = new TextView(getActivity());
			balancesheet.setText(RegionalConversion.getRegionalConversion(AppStrings.balanceSheetHeader + " : "
							+ Reports_Trial_BalanceSheetFragment.toDate));// change
																	// date
			balancesheet.setTypeface(LoginActivity.sTypeface);
			balancesheet.setPadding(20, 5, 10, 5);
			balancesheet.setTextColor(Color.WHITE);
			balanceSheetHeader.addView(balancesheet);

			balancesheetHeaderTable.addView(balanceSheetHeader);

		} catch (Exception e) {
			System.out.println("BalanceSheet Table Header Exception :"
					+ e.toString());
		}

		/*********** BALANCESHEET CONTENT TABLE **********/

		TableLayout balancesheetContentTable = (TableLayout) rootView
				.findViewById(R.id.fragment_balancesheetContentTable);
		balancesheetContentTable
				.setBackgroundResource(R.drawable.content_background);
		balancesheetContentTable.setLayoutParams(params);
		try {
			TableRow balanceSheetRowHeader = new TableRow(getActivity());
			balanceSheetRowHeader.setId(10);

			TextView payment = new TextView(getActivity());
			payment.setText(GetSpanText.getSpanString(getActivity(),
					String.valueOf(AppStrings.payment)));
			payment.setTypeface(LoginActivity.sTypeface);
			payment.setPadding(20, 5, 10, 5);
			payment.setTextColor(Color.rgb(59, 133, 72));
			balanceSheetRowHeader.addView(payment);

			TextView paymentAmount = new TextView(getActivity());
			paymentAmount.setText(GetSpanText.getSpanString(getActivity(),
					String.valueOf(AppStrings.amount)));
			paymentAmount.setTypeface(LoginActivity.sTypeface);
			paymentAmount.setGravity(Gravity.RIGHT);
			paymentAmount.setPadding(10, 5, 10, 5);
			paymentAmount.setTextColor(Color.rgb(59, 133, 72));
			balanceSheetRowHeader.addView(paymentAmount);

			TextView receipt = new TextView(getActivity());
			receipt.setText(GetSpanText.getSpanString(getActivity(),
					String.valueOf(AppStrings.receipt)));
			receipt.setTypeface(LoginActivity.sTypeface);
			receipt.setPadding(10, 5, 10, 5);
			receipt.setTextColor(Color.rgb(59, 133, 72));
			balanceSheetRowHeader.addView(receipt);

			TextView amount = new TextView(getActivity());
			amount.setText(GetSpanText.getSpanString(getActivity(),
					String.valueOf(AppStrings.amount)));
			amount.setTypeface(LoginActivity.sTypeface);
			amount.setGravity(Gravity.RIGHT);
			amount.setPadding(10, 5, 20, 5);
			amount.setTextColor(Color.rgb(59, 133, 72));
			balanceSheetRowHeader.addView(amount);

			balancesheetContentTable.addView(balanceSheetRowHeader);
		} catch (Exception e) {
			System.out.println("BALANCESHEET CONTENT HEADER EXCEPTION :"
					+ e.toString());
		}

		/*********** BALANCESHEET CONTENT TABLE **********/

		int size = Get_BalanceSheetTask.balance_left.size();
		System.out.println("Size of BalanceSheet :" + size);
		try {
			for (int i = 0; i < size; i = i + 2) {
				TableRow balanceSheetContentRow = new TableRow(getActivity());
				balanceSheetContentRow.setId(10);

				TextView payment = new TextView(getActivity());
				payment.setId(15);
				payment.setText(GetSpanText.getSpanString(getActivity(),
						String.valueOf(Get_BalanceSheetTask.balance_left
								.elementAt(i))));
				payment.setTypeface(LoginActivity.sTypeface);
				payment.setPadding(20, 5, 10, 5);
				payment.setTextColor(color.black);
				balanceSheetContentRow.addView(payment);

				TextView paymentAmount = new TextView(getActivity());
				paymentAmount.setId(16);
				paymentAmount.setText(GetSpanText.getSpanString(getActivity(),
						String.valueOf(Get_BalanceSheetTask.balance_left
								.elementAt(i + 1))));
				paymentAmount.setPadding(10, 5, 10, 5);
				paymentAmount.setGravity(Gravity.RIGHT);
				paymentAmount.setTextColor(color.black);
				balanceSheetContentRow.addView(paymentAmount);

				TextView receipt = new TextView(getActivity());
				receipt.setId(17);
				receipt.setText(GetSpanText.getSpanString(getActivity(), String
						.valueOf(Get_BalanceSheetTask.balance_right
								.elementAt(i))));
				receipt.setTypeface(LoginActivity.sTypeface);
				receipt.setPadding(10, 5, 10, 5);
				receipt.setTextColor(color.black);
				balanceSheetContentRow.addView(receipt);

				TextView receiptAmount = new TextView(getActivity());
				receiptAmount.setId(18);
				receiptAmount.setText(GetSpanText.getSpanString(getActivity(),
						String.valueOf(Get_BalanceSheetTask.balance_right
								.elementAt(i + 1))));
				receiptAmount.setGravity(Gravity.RIGHT);
				receiptAmount.setPadding(10, 5, 20, 5);
				receiptAmount.setTextColor(color.black);
				balanceSheetContentRow.addView(receiptAmount);

				balancesheetContentTable.addView(balanceSheetContentRow);
			}
		} catch (Exception e) {
			System.out.println("BalanceSheet Table content Exception :"
					+ e.toString());
		}

		/*********** BALANCESHEET TOTAL **********/

		try {
			TableRow balanceSheetTotalRow = new TableRow(getActivity());
			balanceSheetTotalRow.setId(10);

			TextView paymentTotal = new TextView(getActivity());
			paymentTotal.setId(1);
			paymentTotal.setText(GetSpanText.getSpanString(getActivity(),
					String.valueOf(AppStrings.total)));
			paymentTotal.setTypeface(LoginActivity.sTypeface, Typeface.BOLD);
			paymentTotal.setPadding(20, 5, 10, 5);
			paymentTotal.setTextColor(Color.BLACK);
			balanceSheetTotalRow.addView(paymentTotal);

			TextView paymentAmount1 = new TextView(getActivity());
			paymentAmount1.setId(1);
			paymentAmount1.setText(GetSpanText.getSpanString(getActivity(),
					String.valueOf(Get_BalanceSheetTask.last_left_Total)));
			paymentAmount1.setTypeface(LoginActivity.sTypeface);
			paymentAmount1.setGravity(Gravity.RIGHT);
			paymentAmount1.setPadding(10, 5, 10, 5);
			paymentAmount1.setTypeface(null, Typeface.BOLD);
			paymentAmount1.setTextColor(Color.BLACK);
			balanceSheetTotalRow.addView(paymentAmount1);

			TextView receiptTotal = new TextView(getActivity());
			receiptTotal.setId(1);
			receiptTotal.setText(GetSpanText.getSpanString(getActivity(),
					String.valueOf(AppStrings.total)));
			receiptTotal.setTypeface(LoginActivity.sTypeface, Typeface.BOLD);
			receiptTotal.setPadding(10, 5, 10, 5);
			receiptTotal.setTextColor(Color.BLACK);
			balanceSheetTotalRow.addView(receiptTotal);

			TextView receiptAmount1 = new TextView(getActivity());
			receiptAmount1.setId(1);
			receiptAmount1.setText(GetSpanText.getSpanString(getActivity(),
					String.valueOf(Get_BalanceSheetTask.last_right_Total)));
			receiptAmount1.setGravity(Gravity.RIGHT);
			receiptAmount1.setTypeface(null, Typeface.BOLD);
			receiptAmount1.setPadding(10, 5, 20, 5);
			receiptAmount1.setTextColor(Color.BLACK);
			balanceSheetTotalRow.addView(receiptAmount1);

			balancesheetContentTable.addView(balanceSheetTotalRow);

		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("BalanceSheet Table Total Row Exception :"
					+ e.toString());
		}
		return rootView;
	}
	
	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		
		MainFragment_Dashboard.isBalanceSheetReport = true;
		FragmentDrawer.isTrialBalanceReport = true;
	}

}
