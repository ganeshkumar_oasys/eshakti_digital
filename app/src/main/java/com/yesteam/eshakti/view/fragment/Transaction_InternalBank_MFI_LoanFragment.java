package com.yesteam.eshakti.view.fragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.adapter.CustomItemAdapter;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog.OnDateSetListener;
import com.yesteam.eshakti.model.InternalBankMFIModel;
import com.yesteam.eshakti.model.RowItem;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.CalculateDateUtils;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.views.MaterialSpinner;
import com.yesteam.eshakti.views.RaisedButton;
import com.yesteam.eshakti.webservices.GetBankBranchListTask;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Dialog;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Transaction_InternalBank_MFI_LoanFragment extends Fragment
		implements TaskListener, OnClickListener, OnDateSetListener {

	private TextView mGroupName, mCashInHand, mCashAtBank;

	public static TextView mLoanSancDateTxt;
	public static TextView mLoanDistDateTxt;
	private RaisedButton mRaised_SubmitButton;
	private MaterialSpinner spn_label_bankName, spn_label_loanType, spn_label_installmentType, spn_label_POL,
			spn_label_Bankbranch, materialSpinner_Bank, mTenureSpinner;
	private EditText mLoan_Acc_NoEditText, mLoan_Sanc_AmountEditText, mLoan_Dist_AmountEditText, // mLoan_TenureEditText,
			mSubsidy_AmountEditText, mSubsidy_Reserve_fundEditText, mInterest_RateEditText, mNBFCEditText, mBranchName,
			mBankCharges;
	CustomItemAdapter bankNameAdapter, loanTypeAdapter, installmentTypeAdapter, POLAdapter, mBankBranchAdapter,
			bankAdapter, tenureAdapter;
	private Dialog mProgressDialog;
	private List<RowItem> bankNameItems, loanTypeItems, installmentTypeItems, POLItems, bankBranchItems, bankItems,
			tenureItems;
	private String[] bankNameArr, loanTypeArr, installmentArr, POLArr, mBankBranchArr;
	private String mBankNameValue = "", mLoanTypeValue = "", mInstallmentTypeValue = "", mPOLValue = "",
			mNBFCValue = "", mBankBranchValue = "";
	private String mLoan_Acc_NoValue = "", mLoan_Sanc_AmountValue = "", mLoan_Dist_AmountValue = "",
			mLoan_TenureValue = "", mSubsidy_AmountValue = "", mSubsidy_Reserve_fundValue = "",
			mInterest_RateValue = "", mBankChargesValues = "";
	String[] mLoanTypeArray, mBankNameArray, mBankBranchArray;
	private String mLanguageLocale = "";
	Locale locale;
	public static String bankLoan_check = "";

	public static String loanSancDate = "", loanDistDate = "";
	String mLoanType, mLoanSource;
	private TextView mFedBankNameHeader, mFedBankName;
	private String[] mConfirm_BankLoan, mConfirmation_values;
	Dialog confirmationDialog;
	int mSize;
	private Button mEdit_RaisedButton, mOk_RaisedButton;
	RadioButton mCashRadio, mBankRadio;
	public static String selectedType, selectedItemBank;

	public static String mBankValue = null;
	LinearLayout mSpinnerLayout;
	ArrayList<String> mBanknames_Array = new ArrayList<String>();
	ArrayList<String> mBanknamesId_Array = new ArrayList<String>();
	ArrayList<String> mEngSendtoServerBank_Array = new ArrayList<String>();
	ArrayList<String> mEngSendtoServerBankId_Array = new ArrayList<String>();

	private String[] mTenureArr, mTenureArray;

	Date sanctionDate, disbursementDate;

	public Transaction_InternalBank_MFI_LoanFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_INTERLAONMENU;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		loanSancDate = "";
		loanDistDate = "";
		selectedType = "";
		mBankValue = "";
		EShaktiApplication.setNewLoanDisbursementMinDate(null);
		if (EShaktiApplication.getInternalLoanType() != null) {

			if (EShaktiApplication.getInternalLoanType().equals("BANKLOAN")) {
				mConfirm_BankLoan = new String[] { RegionalConversion.getRegionalConversion(AppStrings.bankName),
						RegionalConversion.getRegionalConversion(AppStrings.loanType),
						RegionalConversion.getRegionalConversion(AppStrings.bankBranch),
						RegionalConversion.getRegionalConversion(AppStrings.Loan_Account_No),
						RegionalConversion.getRegionalConversion(AppStrings.Loan_Sanction_Date),
						RegionalConversion.getRegionalConversion(AppStrings.Loan_Sanction_Amount),
						RegionalConversion.getRegionalConversion(AppStrings.mBankCharges),
						RegionalConversion.getRegionalConversion(AppStrings.mLoanAccType),
						RegionalConversion.getRegionalConversion(AppStrings.Loan_Disbursement_Date),
						RegionalConversion.getRegionalConversion(AppStrings.Loan_Disbursement_Amount),
						RegionalConversion.getRegionalConversion(AppStrings.Loan_Tenure),
						RegionalConversion.getRegionalConversion(AppStrings.installment_type),
						RegionalConversion.getRegionalConversion(AppStrings.Subsidy_Amount),
						RegionalConversion.getRegionalConversion(AppStrings.Subsidy_Reserve_Fund),
						RegionalConversion.getRegionalConversion(AppStrings.purposeOfLoan),
						RegionalConversion.getRegionalConversion(AppStrings.Interest_Rate), };
			} else if (EShaktiApplication.getInternalLoanType().equals("MFILOAN")) {
				mConfirm_BankLoan = new String[] { RegionalConversion.getRegionalConversion(AppStrings.mfiname),
						RegionalConversion.getRegionalConversion(AppStrings.bankBranch),
						RegionalConversion.getRegionalConversion(AppStrings.Loan_Account_No),
						RegionalConversion.getRegionalConversion(AppStrings.Loan_Sanction_Date),
						RegionalConversion.getRegionalConversion(AppStrings.Loan_Sanction_Amount),
						RegionalConversion.getRegionalConversion(AppStrings.mBankCharges),
						RegionalConversion.getRegionalConversion(AppStrings.mLoanAccType),
						RegionalConversion.getRegionalConversion(AppStrings.Loan_Disbursement_Date),
						RegionalConversion.getRegionalConversion(AppStrings.Loan_Disbursement_Amount),
						RegionalConversion.getRegionalConversion(AppStrings.Loan_Tenure),
						RegionalConversion.getRegionalConversion(AppStrings.installment_type),
						RegionalConversion.getRegionalConversion(AppStrings.purposeOfLoan),
						RegionalConversion.getRegionalConversion(AppStrings.Interest_Rate), };
			} else if (EShaktiApplication.getInternalLoanType().equals("FEDLOAN")) {
				mConfirm_BankLoan = new String[] {

						RegionalConversion.getRegionalConversion(AppStrings.mFedBankName),
						RegionalConversion.getRegionalConversion(AppStrings.loanType),
						RegionalConversion.getRegionalConversion(AppStrings.Loan_Sanction_Date),
						RegionalConversion.getRegionalConversion(AppStrings.Loan_Sanction_Amount),
						RegionalConversion.getRegionalConversion(AppStrings.mBankCharges),
						RegionalConversion.getRegionalConversion(AppStrings.mLoanAccType),
						RegionalConversion.getRegionalConversion(AppStrings.Loan_Disbursement_Date),
						RegionalConversion.getRegionalConversion(AppStrings.Loan_Disbursement_Amount),
						RegionalConversion.getRegionalConversion(AppStrings.Loan_Tenure),
						RegionalConversion.getRegionalConversion(AppStrings.installment_type),
						RegionalConversion.getRegionalConversion(AppStrings.purposeOfLoan),
						RegionalConversion.getRegionalConversion(AppStrings.Interest_Rate),

				};
			}
		} else {
			TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT, TastyToast.WARNING);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View rootView = inflater.inflate(R.layout.fragment_bank_mfi_loan, container, false);

		MainFragment_Dashboard.isBackpressed = false;

		try {

			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashInHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashInHand.setTypeface(LoginActivity.sTypeface);

			mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashAtBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashAtBank.setTypeface(LoginActivity.sTypeface);

			mRaised_SubmitButton = (RaisedButton) rootView.findViewById(R.id.fragment_monthyear_picker_Submitbutton);
			mRaised_SubmitButton.setText(RegionalConversion.getRegionalConversion(AppStrings.next));
			mRaised_SubmitButton.setTypeface(LoginActivity.sTypeface);
			mRaised_SubmitButton.setOnClickListener(this);

			spn_label_bankName = (MaterialSpinner) rootView.findViewById(R.id.spinner_bankName);
			spn_label_loanType = (MaterialSpinner) rootView.findViewById(R.id.spinner_loanType);
			spn_label_installmentType = (MaterialSpinner) rootView.findViewById(R.id.spinner_installment_type);
			spn_label_POL = (MaterialSpinner) rootView.findViewById(R.id.spinner_purposeOfLoan);
			spn_label_Bankbranch = (MaterialSpinner) rootView.findViewById(R.id.spinner_bankBranch);

			spn_label_bankName.setBaseColor(color.grey_400);
			spn_label_loanType.setBaseColor(color.grey_400);
			spn_label_installmentType.setBaseColor(color.grey_400);
			spn_label_POL.setBaseColor(color.grey_400);
			spn_label_Bankbranch.setBaseColor(color.grey_400);
			if (EShaktiApplication.getInternalLoanType().equals("BANKLOAN")) {
				spn_label_bankName.setFloatingLabelText(String.valueOf(AppStrings.bankName));
			} else if (EShaktiApplication.getInternalLoanType().equals("MFILOAN")) {
				spn_label_bankName.setFloatingLabelText(String.valueOf(AppStrings.mfiname));// (AppStrings.mfiname));
			}
			spn_label_loanType.setFloatingLabelText(String.valueOf(AppStrings.loanType));
			spn_label_installmentType.setFloatingLabelText(String.valueOf(AppStrings.installment_type));
			spn_label_POL.setFloatingLabelText(String.valueOf(AppStrings.purposeOfLoan));
			spn_label_Bankbranch.setFloatingLabelText(String.valueOf(AppStrings.bankBranch));

			spn_label_bankName.setPaddingSafe(10, 0, 10, 0);
			spn_label_loanType.setPaddingSafe(10, 0, 10, 0);
			spn_label_installmentType.setPaddingSafe(10, 0, 10, 0);
			spn_label_POL.setPaddingSafe(10, 0, 10, 0);
			spn_label_Bankbranch.setPaddingSafe(10, 0, 10, 0);

			mFedBankNameHeader = (TextView) rootView.findViewById(R.id.fedBankNameHeader);
			mFedBankName = (TextView) rootView.findViewById(R.id.fedBankName);

			if (EShaktiApplication.getInternalLoanType().equals("FEDLOAN")) {

				mFedBankNameHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.mFedBankName));
				mFedBankNameHeader.setTypeface(LoginActivity.sTypeface);
				mFedBankNameHeader.setVisibility(View.VISIBLE);

				mFedBankName.setText(RegionalConversion.getRegionalConversion(publicValues.mFedName));
				mFedBankName.setTypeface(LoginActivity.sTypeface);
				mFedBankName.setVisibility(View.VISIBLE);

			}

			mLoan_Acc_NoEditText = (EditText) rootView.findViewById(R.id.editText_LoanAccNo);
			mLoan_Acc_NoEditText.setHint(RegionalConversion.getRegionalConversion(AppStrings.Loan_Account_No));
			mLoan_Acc_NoEditText.setBackgroundResource(R.drawable.edittext_background);
			// mLoan_Acc_NoEditText.setInputType(InputType.TYPE_CLASS_PHONE);
			mLoan_Acc_NoEditText.setTypeface(LoginActivity.sTypeface);

			mLoan_Sanc_AmountEditText = (EditText) rootView.findViewById(R.id.editText_LoanSanctionAmount);
			mLoan_Sanc_AmountEditText
					.setHint(RegionalConversion.getRegionalConversion(AppStrings.Loan_Sanction_Amount));
			mLoan_Sanc_AmountEditText.setBackgroundResource(R.drawable.edittext_background);
			mLoan_Sanc_AmountEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
			mLoan_Sanc_AmountEditText.setTypeface(LoginActivity.sTypeface);

			mBranchName = (EditText) rootView.findViewById(R.id.editText_BankBranch_Name);
			mBranchName.setHint(RegionalConversion.getRegionalConversion(AppStrings.bankBranch));
			mBranchName.setBackgroundResource(R.drawable.edittext_background);
			mBranchName.setInputType(InputType.TYPE_CLASS_TEXT);
			mBranchName.setTypeface(LoginActivity.sTypeface);
			if (EShaktiApplication.getInternalLoanType().equals("FEDLOAN")) {

				mBranchName.setText(RegionalConversion.getRegionalConversion(publicValues.mFedName));
				mBranchName.setTextColor(color.black);
				mBranchName.setEnabled(false);
			}

			mLoan_Dist_AmountEditText = (EditText) rootView.findViewById(R.id.editText_loan_disbursement_amount);
			mLoan_Dist_AmountEditText
					.setHint(RegionalConversion.getRegionalConversion(AppStrings.Loan_Disbursement_Amount));
			mLoan_Dist_AmountEditText.setBackgroundResource(R.drawable.edittext_background);
			mLoan_Dist_AmountEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
			mLoan_Dist_AmountEditText.setTypeface(LoginActivity.sTypeface);

			// Tenure spinner
			mTenureSpinner = (MaterialSpinner) rootView.findViewById(R.id.bank_mfi_tenure_spinner);
			mTenureSpinner.setBaseColor(color.grey_400);
			mTenureSpinner.setFloatingLabelText(AppStrings.tenure);
			mTenureSpinner.setPaddingSafe(10, 0, 10, 0);

			mTenureArr = new String[] { "6", "10", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22",
					"23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38",
					"39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54",
					"55", "56", "57", "58", "59", "60" };
			mTenureArray = new String[mTenureArr.length + 1];
			mTenureArray[0] = String.valueOf(AppStrings.tenure);
			for (int i = 0; i < mTenureArr.length; i++) {
				mTenureArray[i + 1] = mTenureArr[i].toString();
			}

			tenureItems = new ArrayList<RowItem>();
			for (int i = 0; i < mTenureArray.length; i++) {
				RowItem rowItem = new RowItem(mTenureArray[i]);
				tenureItems.add(rowItem);
			}
			tenureAdapter = new CustomItemAdapter(getActivity(), tenureItems);
			mTenureSpinner.setAdapter(tenureAdapter);
			mTenureSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
					// TODO Auto-generated method stub
					Log.d("TENURE POSITION", String.valueOf(position));

					String selectedItem;
					if (position == 0) {
						selectedItem = mTenureArray[0];
						mLoan_TenureValue = "0";
					} else {
						selectedItem = mTenureArray[position];
						System.out.println("SELECTED TENURE:" + selectedItem);

						mLoan_TenureValue = selectedItem;

					}
					Log.e("Tenure value", mLoan_TenureValue);

				}

				@Override
				public void onNothingSelected(AdapterView<?> parent) {
					// TODO Auto-generated method stub

				}
			});

			mSubsidy_AmountEditText = (EditText) rootView.findViewById(R.id.editText_Subsidy_amount);
			mSubsidy_AmountEditText.setHint(RegionalConversion.getRegionalConversion(AppStrings.Subsidy_Amount));
			mSubsidy_AmountEditText.setBackgroundResource(R.drawable.edittext_background);
			mSubsidy_AmountEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
			mSubsidy_AmountEditText.setTypeface(LoginActivity.sTypeface);

			mSubsidy_Reserve_fundEditText = (EditText) rootView.findViewById(R.id.editText_Subsidy_Reserve_fund);
			mSubsidy_Reserve_fundEditText
					.setHint(RegionalConversion.getRegionalConversion(AppStrings.Subsidy_Reserve_Fund));
			mSubsidy_Reserve_fundEditText.setBackgroundResource(R.drawable.edittext_background);
			mSubsidy_Reserve_fundEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
			mSubsidy_Reserve_fundEditText.setTypeface(LoginActivity.sTypeface);

			mInterest_RateEditText = (EditText) rootView.findViewById(R.id.editText_interest_rate);
			mInterest_RateEditText.setHint(RegionalConversion.getRegionalConversion(AppStrings.Interest_Rate));
			mInterest_RateEditText.setBackgroundResource(R.drawable.edittext_background);
			// mInterest_RateEditText.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
			mInterest_RateEditText.setTypeface(LoginActivity.sTypeface);

			mLoanSancDateTxt = (TextView) rootView.findViewById(R.id.loan_sanction_date);

			mLoanSancDateTxt.setHint(GetSpanText.getSpanString(getActivity(), AppStrings.Loan_Sanction_Date));
			mLoanSancDateTxt.setTypeface(LoginActivity.sTypeface);
			mLoanSancDateTxt.setText(Transaction_InternalBank_MFI_LoanFragment.loanSancDate);
			mLoanSancDateTxt.setOnClickListener(this);

			mLoanDistDateTxt = (TextView) rootView.findViewById(R.id.loan_disbursement_date);
			mLoanDistDateTxt.setHint(GetSpanText.getSpanString(getActivity(), AppStrings.Loan_Disbursement_Date));
			mLoanDistDateTxt.setTypeface(LoginActivity.sTypeface);
			mLoanDistDateTxt.setText(Transaction_InternalBank_MFI_LoanFragment.loanDistDate);
			mLoanDistDateTxt.setOnClickListener(this);

			mNBFCEditText = (EditText) rootView.findViewById(R.id.editText_NBFC_Name);
			mNBFCEditText.setHint(RegionalConversion.getRegionalConversion(AppStrings.NBFC_Name));
			mNBFCEditText.setBackgroundResource(R.drawable.edittext_background);
			mNBFCEditText.setTypeface(LoginActivity.sTypeface);
			mNBFCEditText.setVisibility(View.GONE);

			mBankCharges = (EditText) rootView.findViewById(R.id.editText_LoanBankCharges);
			mBankCharges.setHint(RegionalConversion.getRegionalConversion(AppStrings.mBankCharges));
			mBankCharges.setBackgroundResource(R.drawable.edittext_background);
			mBankCharges.setInputType(InputType.TYPE_CLASS_NUMBER);
			mBankCharges.setTypeface(LoginActivity.sTypeface);

			mCashRadio = (RadioButton) rootView.findViewById(R.id.radiobank_mfi_cash);
			mBankRadio = (RadioButton) rootView.findViewById(R.id.radiobank_mfi_bank);

			mCashRadio.setTypeface(LoginActivity.sTypeface);
			mBankRadio.setTypeface(LoginActivity.sTypeface);

			mSpinnerLayout = (LinearLayout) rootView.findViewById(R.id.bank_mfi_bankspinner_layout);
			mSpinnerLayout.setVisibility(View.GONE);

			RadioGroup radioGroup = (RadioGroup) rootView.findViewById(R.id.radiobank_mfi);
			radioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(RadioGroup group, int checkedId) {
					// checkedId is the RadioButton selected
					switch (checkedId) {
					case R.id.radiobank_mfi_cash:
						selectedType = "Cash";
						mSpinnerLayout.setVisibility(View.GONE);
						break;

					case R.id.radiobank_mfi_bank:
						selectedType = "Bank";
						mSpinnerLayout.setVisibility(View.VISIBLE);

						break;
					}
				}
			});

			for (int i = 0; i < SelectedGroupsTask.sBankNames.size(); i++) {
				mBanknames_Array.add(SelectedGroupsTask.sBankNames.elementAt(i).toString());
				mBanknamesId_Array.add(String.valueOf(i));
			}

			for (int i = 0; i < SelectedGroupsTask.sEngBankNames.size(); i++) {
				mEngSendtoServerBank_Array.add(SelectedGroupsTask.sEngBankNames.elementAt(i).toString());
				mEngSendtoServerBankId_Array.add(String.valueOf(i));
			}

			materialSpinner_Bank = (MaterialSpinner) rootView.findViewById(R.id.bank_mfi_bankspinner);
			materialSpinner_Bank.setBaseColor(color.grey_400);
			materialSpinner_Bank.setFloatingLabelText(AppStrings.bankName);
			materialSpinner_Bank.setPaddingSafe(10, 0, 10, 0);

			final String[] bankNames = new String[SelectedGroupsTask.sBankNames.size() + 1];

			final String[] bankNames_BankID = new String[SelectedGroupsTask.sEngBankNames.size() + 1];

			bankNames[0] = String.valueOf(RegionalConversion.getRegionalConversion(AppStrings.bankName));
			for (int i = 0; i < SelectedGroupsTask.sBankNames.size(); i++) {
				bankNames[i + 1] = SelectedGroupsTask.sBankNames.elementAt(i).toString();
			}

			bankNames_BankID[0] = String.valueOf(RegionalConversion.getRegionalConversion(AppStrings.bankName));
			for (int i = 0; i < SelectedGroupsTask.sEngBankNames.size(); i++) {
				bankNames_BankID[i + 1] = SelectedGroupsTask.sEngBankNames.elementAt(i).toString();
			}

			int size = bankNames.length;

			bankItems = new ArrayList<RowItem>();
			for (int i = 0; i < size; i++) {
				RowItem rowItem = new RowItem(bankNames[i]);// SelectedGroupsTask.sBankNames.elementAt(i).toString());
				bankItems.add(rowItem);
			}
			bankAdapter = new CustomItemAdapter(getActivity(), bankItems);
			materialSpinner_Bank.setAdapter(bankAdapter);

			materialSpinner_Bank.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
					// TODO Auto-generated method stub

					if (position == 0) {
						selectedItemBank = bankNames_BankID[0];
						mBankValue = "0";

					} else {
						selectedItemBank = bankNames_BankID[position];
						System.out.println("SELECTED BANK NAME : " + selectedItemBank);
						mBankValue = selectedItemBank;
						String mBankname = null;
						for (int i = 0; i < mBanknames_Array.size(); i++) {
							if (selectedItemBank.equals(mEngSendtoServerBank_Array.get(i))) {
								mBankname = mEngSendtoServerBank_Array.get(i);
							}
						}

						mBankValue = mBankname;

					}
					Log.e("Selected Bank Name value", mBankValue);

				}

				@Override
				public void onNothingSelected(AdapterView<?> parent) {
					// TODO Auto-generated method stub

				}
			});

			setVisibility();

			if (EShaktiApplication.getInternalLoanType().equals("BANKLOAN")) {

				mBankNameArray = publicValues.mBankList.split("~");
				bankNameArr = new String[mBankNameArray.length + 1];
				bankNameArr[0] = String.valueOf(RegionalConversion.getRegionalConversion(AppStrings.bankName));
				Log.e("Length Values", bankNameArr.length + "");
				Log.e("Next Length", mBankNameArray.length + "");
				for (int i = 0; i < mBankNameArray.length; i++) {
					bankNameArr[i + 1] = mBankNameArray[i].toString();
					Log.i("Bank Name", i + "     " + bankNameArr[i + 1].toString());
				}

				bankNameItems = new ArrayList<RowItem>();
				for (int i = 0; i < bankNameArr.length; i++) {
					RowItem rowItem = new RowItem(bankNameArr[i]);
					bankNameItems.add(rowItem);
				}
				bankNameAdapter = new CustomItemAdapter(getActivity(), bankNameItems);
				spn_label_bankName.setAdapter(bankNameAdapter);

			} else if (EShaktiApplication.getInternalLoanType().equals("MFILOAN")) {

				mBankNameArray = publicValues.mMFILoanType.split("~");
				bankNameArr = new String[mBankNameArray.length + 1];
				bankNameArr[0] = String.valueOf(RegionalConversion.getRegionalConversion(AppStrings.mfiname));
				for (int i = 0; i < mBankNameArray.length; i++) {
					bankNameArr[i + 1] = mBankNameArray[i].toString();

				}

				bankNameItems = new ArrayList<RowItem>();
				for (int i = 0; i < bankNameArr.length; i++) {
					RowItem rowItem = new RowItem(bankNameArr[i]);
					bankNameItems.add(rowItem);
				}
				bankNameAdapter = new CustomItemAdapter(getActivity(), bankNameItems);
				spn_label_bankName.setAdapter(bankNameAdapter);

			}
			spn_label_bankName.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
					// TODO Auto-generated method stub
					Log.d("BankName POSITION", String.valueOf(position));

					String selectedItem;
					if (position == 0) {
						selectedItem = bankNameArr[0];
						mBankNameValue = "0";
					} else {
						selectedItem = bankNameArr[position];
						System.out.println("SELECTED Bank Name :" + selectedItem);

						mBankNameValue = selectedItem;
						if (EShaktiApplication.getInternalLoanType().equals("BANKLOAN")) {
							EShaktiApplication.setBankName_InternalLoan(mBankNameValue);
							new GetBankBranchListTask(Transaction_InternalBank_MFI_LoanFragment.this).execute();
						}
					}
					Log.e("Bank Name value", mBankNameValue);

				}

				@Override
				public void onNothingSelected(AdapterView<?> parent) {
					// TODO Auto-generated method stub

				}
			});

			if (EShaktiApplication.getInternalLoanType().equals("BANKLOAN")) {
				mLoanTypeArray = publicValues.mBANKLoanType.split("~");
				loanTypeArr = new String[mLoanTypeArray.length + 1];
				loanTypeArr[0] = String.valueOf(RegionalConversion.getRegionalConversion(AppStrings.loanType));
				for (int i = 0; i < mLoanTypeArray.length; i++) {
					loanTypeArr[i + 1] = mLoanTypeArray[i].toString();
					Log.i("Loan Type", loanTypeArr[i].toString());
				}
			} else if (EShaktiApplication.getInternalLoanType().equals("MFILOAN")) {

			} else if (EShaktiApplication.getInternalLoanType().endsWith("FEDLOAN")) {
				mLoanTypeArray = publicValues.mFEDLoanType.split("~");
				loanTypeArr = new String[mLoanTypeArray.length + 1];
				loanTypeArr[0] = String.valueOf(RegionalConversion.getRegionalConversion(AppStrings.loanType));
				for (int i = 0; i < mLoanTypeArray.length; i++) {
					loanTypeArr[i + 1] = mLoanTypeArray[i].toString();
					Log.i("Loan Type", loanTypeArr[i].toString());
				}

				loanTypeItems = new ArrayList<RowItem>();
				for (int i = 0; i < loanTypeArr.length; i++) {
					RowItem rowItem = new RowItem(loanTypeArr[i]);
					loanTypeItems.add(rowItem);
				}

				loanTypeAdapter = new CustomItemAdapter(getActivity(), loanTypeItems);
				spn_label_loanType.setAdapter(loanTypeAdapter);

			}
			if (EShaktiApplication.getInternalLoanType().equals("BANKLOAN")) {
				loanTypeItems = new ArrayList<RowItem>();
				for (int i = 0; i < loanTypeArr.length; i++) {
					RowItem rowItem = new RowItem(loanTypeArr[i]);
					loanTypeItems.add(rowItem);
				}

				loanTypeAdapter = new CustomItemAdapter(getActivity(), loanTypeItems);
				spn_label_loanType.setAdapter(loanTypeAdapter);
			}
			spn_label_loanType.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
					// TODO Auto-generated method stub
					Log.d("LOAN TYPE POSITION", String.valueOf(position));

					String selectedItem;
					if (position == 0) {
						selectedItem = loanTypeArr[0];
						mLoanTypeValue = "0";
					} else {
						selectedItem = loanTypeArr[position];
						System.out.println("SELECTED LOAN TYPE : " + selectedItem);
						mLoanTypeValue = selectedItem;

						if (PrefUtils.getUserlangcode().equals("English")) {
							if (mLoanTypeValue.equals("CASH CREDIT")) {
								mTenureSpinner.setVisibility(View.GONE);
								spn_label_installmentType.setVisibility(View.GONE);
							} else {
								mTenureSpinner.setVisibility(View.VISIBLE);
								spn_label_installmentType.setVisibility(View.VISIBLE);
							}
						} else if (PrefUtils.getUserlangcode().equals("Tamil")) {
							if (mLoanTypeValue.equals("பண கடன்")) {
								mTenureSpinner.setVisibility(View.GONE);
								spn_label_installmentType.setVisibility(View.GONE);
							} else {
								mTenureSpinner.setVisibility(View.VISIBLE);
								spn_label_installmentType.setVisibility(View.VISIBLE);
							}
						} else if (PrefUtils.getUserlangcode().equals("Hindi")) {
							if (mLoanTypeValue.equals("कैश क्रेडिट")) {
								mTenureSpinner.setVisibility(View.GONE);
								spn_label_installmentType.setVisibility(View.GONE);
							} else {
								mTenureSpinner.setVisibility(View.VISIBLE);
								spn_label_installmentType.setVisibility(View.VISIBLE);
							}
						} else {
							if (mLoanTypeValue.equals("CASH CREDIT")) {
								mTenureSpinner.setVisibility(View.GONE);
								spn_label_installmentType.setVisibility(View.GONE);
							} else {
								mTenureSpinner.setVisibility(View.VISIBLE);
								spn_label_installmentType.setVisibility(View.VISIBLE);
							}
						}

					}
					Log.e("Loan Type value", mLoanTypeValue);

				}

				@Override
				public void onNothingSelected(AdapterView<?> parent) {
					// TODO Auto-generated method stub

				}
			});

			installmentArr = new String[] { AppStrings.installment_type, AppStrings.mMonthly, AppStrings.mQuarterly,
					AppStrings.mHalf_Yearly, AppStrings.mYearly };

			installmentTypeItems = new ArrayList<RowItem>();
			for (int i = 0; i < installmentArr.length; i++) {
				RowItem rowItem = new RowItem(String.valueOf(installmentArr[i]));
				installmentTypeItems.add(rowItem);
			}

			installmentTypeAdapter = new CustomItemAdapter(getActivity(), installmentTypeItems);
			spn_label_installmentType.setAdapter(installmentTypeAdapter);
			spn_label_installmentType.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
					// TODO Auto-generated method stub
					Log.d("InstallmentType POSITION", String.valueOf(position));

					String selectedItem;
					if (position == 0) {
						selectedItem = installmentArr[0];
						mInstallmentTypeValue = "0";
					} else {
						selectedItem = installmentArr[position];
						System.out.println("SELECTED InstallmentType :" + selectedItem);
						if (position == 1) {
							mInstallmentTypeValue = "MONTHLY";
						} else if (position == 2) {
							mInstallmentTypeValue = "QUARTERLY";
						} else if (position == 3) {
							mInstallmentTypeValue = "HALF YEARLY";
						} else if (position == 4) {
							mInstallmentTypeValue = "YEARLY";
						}

						Log.e("Install ment Type", mInstallmentTypeValue);
					}

				}

				@Override
				public void onNothingSelected(AdapterView<?> parent) {
					// TODO Auto-generated method stub

				}
			});

			POLArr = new String[] { AppStrings.purposeOfLoan, AppStrings.mAgri, AppStrings.mMSME, AppStrings.mOthers };

			POLItems = new ArrayList<RowItem>();
			for (int i = 0; i < POLArr.length; i++) {
				RowItem rowItem = new RowItem(POLArr[i]);
				POLItems.add(rowItem);
			}

			POLAdapter = new CustomItemAdapter(getActivity(), POLItems);
			spn_label_POL.setAdapter(POLAdapter);
			spn_label_POL.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
					// TODO Auto-generated method stub
					Log.d("POL POSITION", String.valueOf(position));

					String selectedItem;
					if (position == 0) {
						selectedItem = POLArr[0];
						mPOLValue = "0";
					} else {
						selectedItem = POLArr[position];
						System.out.println("SELECTED POL :" + selectedItem);

						if (position == 1) {
							mPOLValue = "AGRI";
						} else if (position == 2) {
							mPOLValue = "MSME";
						} else if (position == 3) {
							mPOLValue = "OTHERS";
						}

						Log.e("Pol Values-------->", mPOLValue);

					}

				}

				@Override
				public void onNothingSelected(AdapterView<?> parent) {
					// TODO Auto-generated method stub

				}
			});

			mBankBranchArr = new String[] { RegionalConversion.getRegionalConversion(AppStrings.bankBranch) };

			bankBranchItems = new ArrayList<RowItem>();
			for (int i = 0; i < mBankBranchArr.length; i++) {
				RowItem rowItem = new RowItem(mBankBranchArr[i]);
				bankBranchItems.add(rowItem);
			}

			mBankBranchAdapter = new CustomItemAdapter(getActivity(), bankBranchItems);
			spn_label_Bankbranch.setAdapter(mBankBranchAdapter);
			spn_label_Bankbranch.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
					// TODO Auto-generated method stub
					Log.d("BankName POSITION", String.valueOf(position));

					String selectedItem;
					if (position == 0) {
						selectedItem = mBankBranchArr[0];
						mBankBranchValue = "0";
					} else {
						selectedItem = mBankBranchArr[position];
						System.out.println("SELECTED Bank Name :" + selectedItem);

						mBankBranchValue = selectedItem;

					}
					Log.e("Bank Branch Name value", mBankBranchValue);

				}

				@Override
				public void onNothingSelected(AdapterView<?> parent) {
					// TODO Auto-generated method stub

				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}

		return rootView;
	}

	private void setVisibility() {
		// TODO Auto-generated method stub

		if (EShaktiApplication.getInternalLoanType().equals("BANKLOAN")) {
			mBranchName.setVisibility(View.GONE);
			mFedBankNameHeader.setVisibility(View.GONE);
			mFedBankName.setVisibility(View.GONE);

		} else if (EShaktiApplication.getInternalLoanType().equals("MFILOAN")) {
			mNBFCEditText.setVisibility(View.GONE);
			spn_label_loanType.setVisibility(View.GONE);
			spn_label_Bankbranch.setVisibility(View.GONE);
			mFedBankNameHeader.setVisibility(View.GONE);
			mFedBankName.setVisibility(View.GONE);
			mSubsidy_AmountEditText.setVisibility(View.GONE);
			mSubsidy_Reserve_fundEditText.setVisibility(View.GONE);

		} else if (EShaktiApplication.getInternalLoanType().equals("FEDLOAN")) {
			mNBFCEditText.setVisibility(View.GONE);
			spn_label_Bankbranch.setVisibility(View.GONE);
			spn_label_bankName.setVisibility(View.GONE);
			mSubsidy_AmountEditText.setVisibility(View.GONE);
			mSubsidy_Reserve_fundEditText.setVisibility(View.GONE);
			mLoan_Acc_NoEditText.setVisibility(View.GONE);
			mBranchName.setVisibility(View.GONE);
		}

	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub
		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub
		mProgressDialog = AppDialogUtils.createProgressDialog(getActivity());
		mProgressDialog.show();
	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub

		try {

			if (mProgressDialog != null) {
				mProgressDialog.dismiss();

				if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {
					getActivity().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub

							if (!result.equals("FAIL")) {

								TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg,
										TastyToast.LENGTH_SHORT, TastyToast.ERROR);
								Constants.NETWORKCOMMONFLAG = "SUCESS";
							}

						}
					});
				} else {

					mBankBranchArray = publicValues.mBankBranchList.split("~");
					mBankBranchArr = new String[mBankBranchArray.length + 1];
					mBankBranchArr[0] = String.valueOf(RegionalConversion.getRegionalConversion(AppStrings.bankBranch));

					for (int i = 0; i < mBankBranchArray.length; i++) {
						mBankBranchArr[i + 1] = mBankBranchArray[i].toString();
						Log.i("Bank Branch Name", i + "     " + mBankBranchArr[i + 1].toString());
					}

					bankBranchItems = new ArrayList<RowItem>();
					for (int i = 0; i < mBankBranchArr.length; i++) {
						RowItem rowItem = new RowItem(mBankBranchArr[i]);
						bankBranchItems.add(rowItem);
					}

					mBankBranchAdapter = new CustomItemAdapter(getActivity(), bankBranchItems);
					spn_label_Bankbranch.setAdapter(mBankBranchAdapter);

				}
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.loan_sanction_date:
			bankLoan_check = "1";
			EShaktiApplication.setIsNewLoanDisburseDate(true);

			onSetCalendarValues();
			try {
				Calendar calender = Calendar.getInstance();

				SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String formattedDate = df.format(calender.getTime());
				Log.e("Device Date  =  ", formattedDate + "");

				String mBalanceSheetDate = SelectedGroupsTask.sLastTransactionDate_Response; // dd/MM/yyyy
				String formatted_balancesheetDate = mBalanceSheetDate.replace("/", "-");
				Log.e("Balancesheet Date = ", formatted_balancesheetDate + "");

				Date balanceDate = df.parse(formatted_balancesheetDate);
				Date systemDate = df.parse(formattedDate);

				if (balanceDate.compareTo(systemDate) < 0 || balanceDate.compareTo(systemDate) == 0) {

					calendarDialogShow();
				} else {
					TastyToast.makeText(getActivity(), "PLEASE SET YOUR DEVICE DATE CORRECTLY", TastyToast.LENGTH_SHORT,
							TastyToast.ERROR);
					EShaktiApplication.setNextMonthLastDate(null);
					EShaktiApplication.setCalendarDateVisibleFlag(true);
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			break;

		case R.id.loan_disbursement_date:
			bankLoan_check = "2";
			EShaktiApplication.setIsNewLoanDisburseDate(true);

			onSetCalendarValues();
			try {
				Calendar calender = Calendar.getInstance();

				SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String formattedDate = df.format(calender.getTime());
				Log.e("Device Date  =  ", formattedDate + "");

				String mBalanceSheetDate = SelectedGroupsTask.sLastTransactionDate_Response; // dd/MM/yyyy
				String formatted_balancesheetDate = mBalanceSheetDate.replace("/", "-");
				Log.e("Balancesheet Date = ", formatted_balancesheetDate + "");

				Date balanceDate = df.parse(formatted_balancesheetDate);
				Date systemDate = df.parse(formattedDate);

				if (balanceDate.compareTo(systemDate) < 0 || balanceDate.compareTo(systemDate) == 0) {

					if (EShaktiApplication.getNewLoanDisbursementMinDate() != null) {
						calendarDialogShow();
					} else {
						TastyToast.makeText(getActivity(), AppStrings.mCheckNewLoanSancDate, TastyToast.LENGTH_SHORT,
								TastyToast.WARNING);
					}
				} else {
					TastyToast.makeText(getActivity(), "PLEASE SET YOUR DEVICE DATE CORRECTLY", TastyToast.LENGTH_SHORT,
							TastyToast.ERROR);
					EShaktiApplication.setNextMonthLastDate(null);
					EShaktiApplication.setCalendarDateVisibleFlag(true);
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			break;

		case R.id.fragment_monthyear_picker_Submitbutton:

			mLoan_Acc_NoValue = mLoan_Acc_NoEditText.getText().toString().trim();
			mLoan_Sanc_AmountValue = mLoan_Sanc_AmountEditText.getText().toString().trim();
			mLoan_Dist_AmountValue = mLoan_Dist_AmountEditText.getText().toString().trim();
			// mLoan_TenureValue =
			// mLoan_TenureEditText.getText().toString().trim();
			mSubsidy_AmountValue = mSubsidy_AmountEditText.getText().toString().trim();
			mSubsidy_Reserve_fundValue = mSubsidy_Reserve_fundEditText.getText().toString().trim();
			mInterest_RateValue = mInterest_RateEditText.getText().toString().trim();
			mNBFCValue = mNBFCEditText.getText().toString().trim();
			mBankChargesValues = mBankCharges.getText().toString().trim();
			if (mBankChargesValues.equals("")) {
				mBankChargesValues = "0";
			}
			if (mLoan_Sanc_AmountValue.equals("")) {
				mLoan_Sanc_AmountValue = "0";
			}

			int availableLoanDisAmount = Integer.parseInt(mLoan_Sanc_AmountValue)
					- Integer.parseInt(mBankChargesValues);
			System.out.println("---- AVAILABLE LOAN  DISBURSEMENT AMOUNT =  " + availableLoanDisAmount);

			if (!loanSancDate.equals("") && !loanDistDate.equals("")) {

				String sancDateArr[] = loanSancDate.split("/");
				String disbDateArr[] = loanDistDate.split("/");

				String sancdate = sancDateArr[1] + "-" + sancDateArr[0] + "-" + sancDateArr[2];
				String disbdate = disbDateArr[1] + "-" + disbDateArr[0] + "-" + disbDateArr[2];

				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

				try {
					sanctionDate = sdf.parse(sancdate);
					disbursementDate = sdf.parse(disbdate);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {

				TastyToast.makeText(getActivity(), AppStrings.nullDetailsAlert, TastyToast.LENGTH_SHORT,
						TastyToast.WARNING);

			}
			if (EShaktiApplication.getInternalLoanType().equals("BANKLOAN")) {
				boolean iSCashCreditLoan = false;
				if (PrefUtils.getUserlangcode().equals("English")) {
					if (mLoanTypeValue.equals("CASH CREDIT")) {
						iSCashCreditLoan = true;
						mInstallmentTypeValue = "YEARLY";
						mLoan_TenureValue = "12";

					}
				} else if (PrefUtils.getUserlangcode().equals("Tamil")) {
					if (mLoanTypeValue.equals("பண கடன்")) {
						iSCashCreditLoan = true;
						mInstallmentTypeValue = "YEARLY";
						mLoan_TenureValue = "12";
					}
				} else if (PrefUtils.getUserlangcode().equals("Hindi")) {
					if (mLoanTypeValue.equals("कैश क्रेडिट")) {
						iSCashCreditLoan = true;
						mInstallmentTypeValue = "YEARLY";
						mLoan_TenureValue = "12";
					}
				} else {
					if (mLoanTypeValue.equals("CASH CREDIT")) {
						iSCashCreditLoan = true;
						mInstallmentTypeValue = "YEARLY";
						mLoan_TenureValue = "12";
					}
				}

				if ((!mBankNameValue.equals("")) && (!mBankNameValue.equals("0")) && (!mBankBranchValue.equals(""))
						&& (!mBankBranchValue.equals("0")) && (!mLoanTypeValue.equals(""))
						&& (!mLoanTypeValue.equals("0"))
						&& (!mInstallmentTypeValue.equals("") && !(mInstallmentTypeValue.equals("0")))
						&& (!mPOLValue.equals("") && (!mPOLValue.equals("0"))) && !mLoan_Acc_NoValue.isEmpty()
						&& !mLoan_Sanc_AmountValue.isEmpty() && !mLoan_Dist_AmountValue.isEmpty()
						&& !mLoan_TenureValue.isEmpty() && !mInterest_RateValue.isEmpty() && !loanDistDate.isEmpty()
						&& !loanSancDate.isEmpty() && !mBankBranchValue.equals("") && !mBankBranchValue.equals("0")
						&& !mBankChargesValues.isEmpty() && !selectedType.isEmpty()) {

					if (disbursementDate.compareTo(sanctionDate) >= 0) {
						if (!mLoan_Sanc_AmountValue.equals("0") && !mLoan_Dist_AmountValue.equals("0")) {

							if (Integer.parseInt(mLoan_Sanc_AmountValue) >= Integer.parseInt(mLoan_Dist_AmountValue)) {

								if (Integer.parseInt(mLoan_Dist_AmountValue) <= availableLoanDisAmount) {

									if (EShaktiApplication.getInternalLoanType().equals("BANKLOAN")) {
										mLoanType = "BANK LOAN";
										mLoanSource = "BANK";
									}
									if ((mSubsidy_AmountValue.equals("") && mSubsidy_Reserve_fundValue.equals(""))
											|| (mSubsidy_AmountValue == null
													&& mSubsidy_Reserve_fundEditText == null)) {
										mSubsidy_AmountValue = "0";
										mSubsidy_Reserve_fundValue = "0";
									}
									mNBFCValue = "0";
									// mNBFCValue = "";
									System.out.println("-------Subsidy AMount --------" + mSubsidy_AmountValue);
									System.out.println(
											"-------Subsidy Reserve AMount --------" + mSubsidy_Reserve_fundValue);

									EShaktiApplication.setSubsidyAmount(mSubsidy_AmountValue);
									EShaktiApplication.setSubsidyReserveFund(mSubsidy_Reserve_fundValue);

									Log.e("SUBSIDY AMOUNT", EShaktiApplication.getSubsidyAmount() + "");
									Log.e("SUBSIDY RESERVE FUND", EShaktiApplication.getSubsidyReserveFund() + "");

									if (mBankRadio.isChecked()) {
										if (!mBankValue.equals("0") && mBankValue != null) {

											if (PrefUtils.getUserlangcode().equals("English")) {
												if (mLoanTypeValue.equals("CASH CREDIT")) {
													mInstallmentTypeValue = "0";
													mLoan_TenureValue = "0";

												}
											} else if (PrefUtils.getUserlangcode().equals("Tamil")) {
												if (mLoanTypeValue.equals("பண கடன்")) {
													mInstallmentTypeValue = "0";
													mLoan_TenureValue = "0";
												}
											} else if (PrefUtils.getUserlangcode().equals("Hindi")) {
												if (mLoanTypeValue.equals("कैश क्रेडिट")) {
													mInstallmentTypeValue = "0";
													mLoan_TenureValue = "0";
												}
											} else {
												if (mLoanTypeValue.equals("CASH CREDIT")) {
													mInstallmentTypeValue = "0";
													mLoan_TenureValue = "0";
												}
											}

											new InternalBankMFIModel(mLoanType, mLoanSource, mBankNameValue, mNBFCValue,
													mLoan_Acc_NoValue, loanSancDate, mLoan_Sanc_AmountValue,
													mBankChargesValues,
													Transaction_InternalBank_MFI_LoanFragment.selectedType,
													loanDistDate, mLoan_Dist_AmountValue, mLoan_TenureValue,
													mInstallmentTypeValue, mSubsidy_AmountValue,
													mSubsidy_Reserve_fundValue, mPOLValue, "", mInterest_RateValue,
													mLoanTypeValue, mBankBranchValue);
											if (iSCashCreditLoan) {
												mConfirm_BankLoan = new String[] {
														RegionalConversion.getRegionalConversion(AppStrings.bankName),
														RegionalConversion.getRegionalConversion(AppStrings.loanType),
														RegionalConversion.getRegionalConversion(AppStrings.bankBranch),
														RegionalConversion
																.getRegionalConversion(AppStrings.Loan_Account_No),
														RegionalConversion
																.getRegionalConversion(AppStrings.Loan_Sanction_Date),
														RegionalConversion
																.getRegionalConversion(AppStrings.Loan_Sanction_Amount),
														RegionalConversion
																.getRegionalConversion(AppStrings.mBankCharges),
														RegionalConversion
																.getRegionalConversion(AppStrings.mLoanAccType),
														RegionalConversion.getRegionalConversion(
																AppStrings.Loan_Disbursement_Date),
														RegionalConversion.getRegionalConversion(
																AppStrings.Loan_Disbursement_Amount),

														RegionalConversion
																.getRegionalConversion(AppStrings.Subsidy_Amount),
														RegionalConversion
																.getRegionalConversion(AppStrings.Subsidy_Reserve_Fund),
														RegionalConversion
																.getRegionalConversion(AppStrings.purposeOfLoan),
														RegionalConversion
																.getRegionalConversion(AppStrings.Interest_Rate), };

												mConfirmation_values = new String[] {
														InternalBankMFIModel.getBankName(),
														InternalBankMFIModel.getLoanName(),
														InternalBankMFIModel.getBankBranchName(),
														InternalBankMFIModel.getLoanAccNo(),
														InternalBankMFIModel.getLoan_Sanction_Date(),
														InternalBankMFIModel.getLoan_Sanction_Amount(),
														InternalBankMFIModel.getLoanBankCharges(),
														InternalBankMFIModel.getType(),
														InternalBankMFIModel.getLoan_Disbursement_Date(),
														InternalBankMFIModel.getLoan_Disbursement_Amount(),
														InternalBankMFIModel.getSubsidy_Amount(),
														InternalBankMFIModel.getSubsidy_Reserve_Fund(),
														InternalBankMFIModel.getPurpose_of_Loan(),
														InternalBankMFIModel.getInterest_Rate() };

												showConfirmationDialog(mConfirm_BankLoan, mConfirmation_values);
											} else {

												mConfirmation_values = new String[] {
														InternalBankMFIModel.getBankName(),
														InternalBankMFIModel.getLoanName(),
														InternalBankMFIModel.getBankBranchName(),
														InternalBankMFIModel.getLoanAccNo(),
														InternalBankMFIModel.getLoan_Sanction_Date(),
														InternalBankMFIModel.getLoan_Sanction_Amount(),
														InternalBankMFIModel.getLoanBankCharges(),
														InternalBankMFIModel.getType(),
														InternalBankMFIModel.getLoan_Disbursement_Date(),
														InternalBankMFIModel.getLoan_Disbursement_Amount(),
														InternalBankMFIModel.getLoan_Tenure(),
														InternalBankMFIModel.getInstallment_Type(),
														InternalBankMFIModel.getSubsidy_Amount(),
														InternalBankMFIModel.getSubsidy_Reserve_Fund(),
														InternalBankMFIModel.getPurpose_of_Loan(),
														InternalBankMFIModel.getInterest_Rate() };

												showConfirmationDialog(mConfirm_BankLoan, mConfirmation_values);
											}

										} else {
											TastyToast.makeText(getActivity(), AppStrings.mLoanaccBankNullToast,
													TastyToast.LENGTH_SHORT, TastyToast.WARNING);
										}

									} else {

										if (PrefUtils.getUserlangcode().equals("English")) {
											if (mLoanTypeValue.equals("CASH CREDIT")) {
												mInstallmentTypeValue = "0";
												mLoan_TenureValue = "0";

											}
										} else if (PrefUtils.getUserlangcode().equals("Tamil")) {
											if (mLoanTypeValue.equals("பண கடன்")) {
												mInstallmentTypeValue = "0";
												mLoan_TenureValue = "0";
											}
										} else if (PrefUtils.getUserlangcode().equals("Hindi")) {
											if (mLoanTypeValue.equals("कैश क्रेडिट")) {
												mInstallmentTypeValue = "0";
												mLoan_TenureValue = "0";
											}
										} else {
											if (mLoanTypeValue.equals("CASH CREDIT")) {
												mInstallmentTypeValue = "0";
												mLoan_TenureValue = "0";
											}
										}
										new InternalBankMFIModel(mLoanType, mLoanSource, mBankNameValue, mNBFCValue,
												mLoan_Acc_NoValue, loanSancDate, mLoan_Sanc_AmountValue,
												mBankChargesValues,
												Transaction_InternalBank_MFI_LoanFragment.selectedType, loanDistDate,
												mLoan_Dist_AmountValue, mLoan_TenureValue, mInstallmentTypeValue,
												mSubsidy_AmountValue, mSubsidy_Reserve_fundValue, mPOLValue, "",
												mInterest_RateValue, mLoanTypeValue, mBankBranchValue);
										if (iSCashCreditLoan) {
											mConfirm_BankLoan = new String[] {
													RegionalConversion.getRegionalConversion(AppStrings.bankName),
													RegionalConversion.getRegionalConversion(AppStrings.loanType),
													RegionalConversion.getRegionalConversion(AppStrings.bankBranch),
													RegionalConversion
															.getRegionalConversion(AppStrings.Loan_Account_No),
													RegionalConversion
															.getRegionalConversion(AppStrings.Loan_Sanction_Date),
													RegionalConversion
															.getRegionalConversion(AppStrings.Loan_Sanction_Amount),
													RegionalConversion.getRegionalConversion(AppStrings.mBankCharges),
													RegionalConversion.getRegionalConversion(AppStrings.mLoanAccType),
													RegionalConversion
															.getRegionalConversion(AppStrings.Loan_Disbursement_Date),
													RegionalConversion
															.getRegionalConversion(AppStrings.Loan_Disbursement_Amount),

													RegionalConversion.getRegionalConversion(AppStrings.Subsidy_Amount),
													RegionalConversion
															.getRegionalConversion(AppStrings.Subsidy_Reserve_Fund),
													RegionalConversion.getRegionalConversion(AppStrings.purposeOfLoan),
													RegionalConversion
															.getRegionalConversion(AppStrings.Interest_Rate), };
											mConfirmation_values = new String[] { InternalBankMFIModel.getBankName(),
													InternalBankMFIModel.getLoanName(),
													InternalBankMFIModel.getBankBranchName(),
													InternalBankMFIModel.getLoanAccNo(),
													InternalBankMFIModel.getLoan_Sanction_Date(),
													InternalBankMFIModel.getLoan_Sanction_Amount(),
													InternalBankMFIModel.getLoanBankCharges(),
													InternalBankMFIModel.getType(),
													InternalBankMFIModel.getLoan_Disbursement_Date(),
													InternalBankMFIModel.getLoan_Disbursement_Amount(),

													InternalBankMFIModel.getSubsidy_Amount(),
													InternalBankMFIModel.getSubsidy_Reserve_Fund(),
													InternalBankMFIModel.getPurpose_of_Loan(),
													InternalBankMFIModel.getInterest_Rate() };
											showConfirmationDialog(mConfirm_BankLoan, mConfirmation_values);
										} else {
											mConfirmation_values = new String[] { InternalBankMFIModel.getBankName(),
													InternalBankMFIModel.getLoanName(),
													InternalBankMFIModel.getBankBranchName(),
													InternalBankMFIModel.getLoanAccNo(),
													InternalBankMFIModel.getLoan_Sanction_Date(),
													InternalBankMFIModel.getLoan_Sanction_Amount(),
													InternalBankMFIModel.getLoanBankCharges(),
													InternalBankMFIModel.getType(),
													InternalBankMFIModel.getLoan_Disbursement_Date(),
													InternalBankMFIModel.getLoan_Disbursement_Amount(),
													InternalBankMFIModel.getLoan_Tenure(),
													InternalBankMFIModel.getInstallment_Type(),
													InternalBankMFIModel.getSubsidy_Amount(),
													InternalBankMFIModel.getSubsidy_Reserve_Fund(),
													InternalBankMFIModel.getPurpose_of_Loan(),
													InternalBankMFIModel.getInterest_Rate() };
											showConfirmationDialog(mConfirm_BankLoan, mConfirmation_values);
										}

									}

								} else {

									mLoan_Dist_AmountEditText.setError(
											"AVAILABLE LOAN DISBURSEMENT AMOUNT IS " + availableLoanDisAmount);
									TastyToast.makeText(getActivity(), AppStrings.mLoanSanc_loanDist,
											TastyToast.LENGTH_SHORT, TastyToast.WARNING);
								}

							} else {

								TastyToast.makeText(getActivity(), AppStrings.mLoanSanc_loanDist,
										TastyToast.LENGTH_SHORT, TastyToast.WARNING);
							}
						} else {

							if (mLoan_Sanc_AmountValue.equals("0") && mLoan_Dist_AmountValue.equals("0")) {
								mLoan_Sanc_AmountEditText.setError(AppStrings.mCheckLoanSanctionAmount);
								mLoan_Dist_AmountEditText.setError(AppStrings.mCheckLoanDisbursementAmount);
								TastyToast.makeText(getActivity(), AppStrings.mCheckLoanAmounts,
										TastyToast.LENGTH_SHORT, TastyToast.WARNING);
							} else if (mLoan_Sanc_AmountValue.equals("0") && !mLoan_Dist_AmountValue.equals("0")) {
								mLoan_Sanc_AmountEditText.setError(AppStrings.mCheckLoanSanctionAmount);
								TastyToast.makeText(getActivity(), AppStrings.mCheckLoanAmounts,
										TastyToast.LENGTH_SHORT, TastyToast.WARNING);
							} else if (!mLoan_Sanc_AmountValue.equals("0") && mLoan_Dist_AmountValue.equals("0")) {
								mLoan_Dist_AmountEditText.setError(AppStrings.mCheckLoanDisbursementAmount);
								TastyToast.makeText(getActivity(), AppStrings.mCheckLoanAmounts,
										TastyToast.LENGTH_SHORT, TastyToast.WARNING);
							}

						}
					} else {
						TastyToast.makeText(getActivity(), AppStrings.mCheckLoanSancDate_LoanDisbDate,
								TastyToast.LENGTH_SHORT, TastyToast.WARNING);
					}
				} else {

					TastyToast.makeText(getActivity(), AppStrings.nullDetailsAlert, TastyToast.LENGTH_SHORT,
							TastyToast.WARNING);
				}

			} else if (EShaktiApplication.getInternalLoanType().equals("MFILOAN")) {
				mBankBranchValue = mBranchName.getText().toString();
				Log.e("MFI Bank branch value@@@@@@@@", mBankBranchValue + "");

				if ((!mBankNameValue.equals("")) && (!mBankNameValue.equals("0"))
						&& (!mInstallmentTypeValue.equals("") && !(mInstallmentTypeValue.equals("0")))
						&& (!mPOLValue.equals("") && (!mPOLValue.equals("0"))) && !mLoan_Acc_NoValue.isEmpty()
						&& !mLoan_Sanc_AmountValue.isEmpty() && !mLoan_Dist_AmountValue.isEmpty()
						&& !mLoan_TenureValue.isEmpty() && !mInterest_RateValue.isEmpty() && !loanDistDate.isEmpty()
						&& !loanSancDate.isEmpty() && !mBankBranchValue.equals("") && !mBankBranchValue.equals("0")
						&& !mBankChargesValues.isEmpty() && !selectedType.isEmpty()) {

					if (disbursementDate.compareTo(sanctionDate) >= 0) {
						if (!mLoan_Sanc_AmountValue.equals("0") && !mLoan_Dist_AmountValue.equals("0")) {
							if (Integer.parseInt(mLoan_Sanc_AmountValue) >= Integer.parseInt(mLoan_Dist_AmountValue)) {
								if (Integer.parseInt(mLoan_Dist_AmountValue) <= availableLoanDisAmount) {
									if (mSubsidy_AmountValue.isEmpty() && mSubsidy_Reserve_fundValue.isEmpty()) {

										mSubsidy_AmountValue = "0";
										mSubsidy_Reserve_fundValue = "0";
									}
									if (EShaktiApplication.getInternalLoanType().equals("MFILOAN")) {
										mLoanType = "OTHER LOAN";
										mLoanSource = "MFI";
										mLoanTypeValue = "MFI LOAN";
									}

									mNBFCValue = "0";
									System.out.println("-------Subsidy AMount --------" + mSubsidy_AmountValue);
									System.out.println(
											"-------Subsidy Reserve AMount --------" + mSubsidy_Reserve_fundValue);

									if (mBankRadio.isChecked()) {
										if (!mBankValue.equals("0") && mBankValue != null) {
											new InternalBankMFIModel(mLoanType, mLoanSource, mBankNameValue, mNBFCValue,
													mLoan_Acc_NoValue, loanSancDate, mLoan_Sanc_AmountValue,
													mBankChargesValues,
													Transaction_InternalBank_MFI_LoanFragment.selectedType,
													loanDistDate, mLoan_Dist_AmountValue, mLoan_TenureValue,
													mInstallmentTypeValue, mSubsidy_AmountValue,
													mSubsidy_Reserve_fundValue, mPOLValue, "", mInterest_RateValue,
													mLoanTypeValue, mBankBranchValue);

											mConfirmation_values = new String[] { InternalBankMFIModel.getBankName(),
													InternalBankMFIModel.getBankBranchName(),
													InternalBankMFIModel.getLoanAccNo(),
													InternalBankMFIModel.getLoan_Sanction_Date(),
													InternalBankMFIModel.getLoan_Sanction_Amount(),
													InternalBankMFIModel.getLoanBankCharges(),
													InternalBankMFIModel.getType(),
													InternalBankMFIModel.getLoan_Disbursement_Date(),
													InternalBankMFIModel.getLoan_Disbursement_Amount(),
													InternalBankMFIModel.getLoan_Tenure(),
													InternalBankMFIModel.getInstallment_Type(),
													InternalBankMFIModel.getPurpose_of_Loan(),
													InternalBankMFIModel.getInterest_Rate() };
											showConfirmationDialog(mConfirm_BankLoan, mConfirmation_values);
										} else {
											TastyToast.makeText(getActivity(), AppStrings.mLoanaccBankNullToast,
													TastyToast.LENGTH_SHORT, TastyToast.WARNING);
										}

									} else {
										new InternalBankMFIModel(mLoanType, mLoanSource, mBankNameValue, mNBFCValue,
												mLoan_Acc_NoValue, loanSancDate, mLoan_Sanc_AmountValue,
												mBankChargesValues,
												Transaction_InternalBank_MFI_LoanFragment.selectedType, loanDistDate,
												mLoan_Dist_AmountValue, mLoan_TenureValue, mInstallmentTypeValue,
												mSubsidy_AmountValue, mSubsidy_Reserve_fundValue, mPOLValue, "",
												mInterest_RateValue, mLoanTypeValue, mBankBranchValue);

										mConfirmation_values = new String[] { InternalBankMFIModel.getBankName(),
												InternalBankMFIModel.getBankBranchName(),
												InternalBankMFIModel.getLoanAccNo(),
												InternalBankMFIModel.getLoan_Sanction_Date(),
												InternalBankMFIModel.getLoan_Sanction_Amount(),
												InternalBankMFIModel.getLoanBankCharges(),
												InternalBankMFIModel.getType(),
												InternalBankMFIModel.getLoan_Disbursement_Date(),
												InternalBankMFIModel.getLoan_Disbursement_Amount(),
												InternalBankMFIModel.getLoan_Tenure(),
												InternalBankMFIModel.getInstallment_Type(),
												InternalBankMFIModel.getPurpose_of_Loan(),
												InternalBankMFIModel.getInterest_Rate() };
										showConfirmationDialog(mConfirm_BankLoan, mConfirmation_values);

									}
								} else {

									mLoan_Dist_AmountEditText.setError(
											"AVAILABLE LOAN DISBURSEMENT AMOUNT IS " + availableLoanDisAmount);
									TastyToast.makeText(getActivity(), AppStrings.mLoanSanc_loanDist,
											TastyToast.LENGTH_SHORT, TastyToast.WARNING);
								}
							} else {
								TastyToast.makeText(getActivity(), AppStrings.mLoanSanc_loanDist,
										TastyToast.LENGTH_SHORT, TastyToast.WARNING);
							}
						} else {

							if (mLoan_Sanc_AmountValue.equals("0") && mLoan_Dist_AmountValue.equals("0")) {
								mLoan_Sanc_AmountEditText.setError(AppStrings.mCheckLoanSanctionAmount);
								mLoan_Dist_AmountEditText.setError(AppStrings.mCheckLoanDisbursementAmount);
								TastyToast.makeText(getActivity(), AppStrings.mCheckLoanAmounts,
										TastyToast.LENGTH_SHORT, TastyToast.WARNING);
							} else if (mLoan_Sanc_AmountValue.equals("0") && !mLoan_Dist_AmountValue.equals("0")) {
								mLoan_Sanc_AmountEditText.setError(AppStrings.mCheckLoanSanctionAmount);
								TastyToast.makeText(getActivity(), AppStrings.mCheckLoanAmounts,
										TastyToast.LENGTH_SHORT, TastyToast.WARNING);
							} else if (!mLoan_Sanc_AmountValue.equals("0") && mLoan_Dist_AmountValue.equals("0")) {
								mLoan_Dist_AmountEditText.setError(AppStrings.mCheckLoanDisbursementAmount);
								TastyToast.makeText(getActivity(), AppStrings.mCheckLoanAmounts,
										TastyToast.LENGTH_SHORT, TastyToast.WARNING);
							}

						}
					} else {
						TastyToast.makeText(getActivity(), AppStrings.mCheckLoanSancDate_LoanDisbDate,
								TastyToast.LENGTH_SHORT, TastyToast.WARNING);
					}

				} else {

					TastyToast.makeText(getActivity(), AppStrings.nullDetailsAlert, TastyToast.LENGTH_SHORT,
							TastyToast.WARNING);
				}
			} else if (EShaktiApplication.getInternalLoanType().equals("FEDLOAN")) {
				mBankNameValue = publicValues.mFedName;
				mBankBranchValue = publicValues.mFedName;
				mLoan_Acc_NoValue = "0";
				Log.e("FEDLOAN Bank branch value@@@@@@@@", mBankBranchValue + "");
				if ((!mBankNameValue.equals("")) && (!mBankNameValue.equals("0"))
						&& (!mInstallmentTypeValue.equals("") && !(mInstallmentTypeValue.equals("0")))
						&& !mLoan_Acc_NoValue.isEmpty() && (!mPOLValue.equals("") && (!mPOLValue.equals("0")))
						&& !mLoan_Sanc_AmountValue.isEmpty() && !mLoan_Dist_AmountValue.isEmpty()
						&& !mLoan_TenureValue.isEmpty() && !mInterest_RateValue.isEmpty() && !loanDistDate.isEmpty()
						&& !loanSancDate.isEmpty() && !mBankBranchValue.equals("") && !mBankBranchValue.equals("0")
						&& !mBankChargesValues.isEmpty() && !selectedType.isEmpty() && !mLoanTypeValue.equals("0")) {

					if (disbursementDate.compareTo(sanctionDate) >= 0) {
						if (!mLoan_Sanc_AmountValue.equals("0") && !mLoan_Dist_AmountValue.equals("0")) {
							if (Integer.parseInt(mLoan_Sanc_AmountValue) >= Integer.parseInt(mLoan_Dist_AmountValue)) {

								System.out.println("---------------------1");
								if (Integer.parseInt(mLoan_Dist_AmountValue) <= availableLoanDisAmount) {
									if (EShaktiApplication.getInternalLoanType().equals("FEDLOAN")) {
										mLoanType = "OTHER LOAN";
										mLoanSource = "FEDERATION";
									}
									if (mSubsidy_AmountValue.isEmpty() && mSubsidy_Reserve_fundValue.isEmpty()) {

										mSubsidy_AmountValue = "0";
										mSubsidy_Reserve_fundValue = "0";
									}

									mNBFCValue = "0";
									// mNBFCValue = "";
									System.out.println("-------Subsidy AMount --------" + mSubsidy_AmountValue);
									System.out.println(
											"-------Subsidy Reserve AMount --------" + mSubsidy_Reserve_fundValue);

									if (mBankRadio.isChecked()) {
										if (!mBankValue.equals("0") && mBankValue != null) {
											new InternalBankMFIModel(mLoanType, mLoanSource, mBankNameValue, mNBFCValue,
													mLoan_Acc_NoValue, loanSancDate, mLoan_Sanc_AmountValue,
													mBankChargesValues,
													Transaction_InternalBank_MFI_LoanFragment.selectedType,
													loanDistDate, mLoan_Dist_AmountValue, mLoan_TenureValue,
													mInstallmentTypeValue, mSubsidy_AmountValue,
													mSubsidy_Reserve_fundValue, mPOLValue, "", mInterest_RateValue,
													mLoanTypeValue, mBankBranchValue);

											mConfirmation_values = new String[] { InternalBankMFIModel.getBankName(),
													InternalBankMFIModel.getLoanName(),
													InternalBankMFIModel.getLoan_Sanction_Date(),
													InternalBankMFIModel.getLoan_Sanction_Amount(),
													InternalBankMFIModel.getLoanBankCharges(),
													InternalBankMFIModel.getType(),
													InternalBankMFIModel.getLoan_Disbursement_Date(),
													InternalBankMFIModel.getLoan_Disbursement_Amount(),
													InternalBankMFIModel.getLoan_Tenure(),
													InternalBankMFIModel.getInstallment_Type(),
													InternalBankMFIModel.getPurpose_of_Loan(),
													InternalBankMFIModel.getInterest_Rate() };
											showConfirmationDialog(mConfirm_BankLoan, mConfirmation_values);
										} else {
											TastyToast.makeText(getActivity(), AppStrings.mLoanaccBankNullToast,
													TastyToast.LENGTH_SHORT, TastyToast.WARNING);
										}
									} else {
										new InternalBankMFIModel(mLoanType, mLoanSource, mBankNameValue, mNBFCValue,
												mLoan_Acc_NoValue, loanSancDate, mLoan_Sanc_AmountValue,
												mBankChargesValues,
												Transaction_InternalBank_MFI_LoanFragment.selectedType, loanDistDate,
												mLoan_Dist_AmountValue, mLoan_TenureValue, mInstallmentTypeValue,
												mSubsidy_AmountValue, mSubsidy_Reserve_fundValue, mPOLValue, "",
												mInterest_RateValue, mLoanTypeValue, mBankBranchValue);

										mConfirmation_values = new String[] { InternalBankMFIModel.getBankName(),
												InternalBankMFIModel.getLoanName(),
												InternalBankMFIModel.getLoan_Sanction_Date(),
												InternalBankMFIModel.getLoan_Sanction_Amount(),
												InternalBankMFIModel.getLoanBankCharges(),
												InternalBankMFIModel.getType(),
												InternalBankMFIModel.getLoan_Disbursement_Date(),
												InternalBankMFIModel.getLoan_Disbursement_Amount(),
												InternalBankMFIModel.getLoan_Tenure(),
												InternalBankMFIModel.getInstallment_Type(),
												InternalBankMFIModel.getPurpose_of_Loan(),
												InternalBankMFIModel.getInterest_Rate() };
										showConfirmationDialog(mConfirm_BankLoan, mConfirmation_values);
									}
								} else {

									mLoan_Dist_AmountEditText.setError(
											"AVAILABLE LOAN DISBURSEMENT AMOUNT IS " + availableLoanDisAmount);
									TastyToast.makeText(getActivity(), AppStrings.mLoanSanc_loanDist,
											TastyToast.LENGTH_SHORT, TastyToast.WARNING);
								}

							} else {

								TastyToast.makeText(getActivity(), AppStrings.mLoanSanc_loanDist,
										TastyToast.LENGTH_SHORT, TastyToast.WARNING);
							}
						} else {

							if (mLoan_Sanc_AmountValue.equals("0") && mLoan_Dist_AmountValue.equals("0")) {
								mLoan_Sanc_AmountEditText.setError(AppStrings.mCheckLoanSanctionAmount);
								mLoan_Dist_AmountEditText.setError(AppStrings.mCheckLoanDisbursementAmount);
								TastyToast.makeText(getActivity(), AppStrings.mCheckLoanAmounts,
										TastyToast.LENGTH_SHORT, TastyToast.WARNING);
							} else if (mLoan_Sanc_AmountValue.equals("0") && !mLoan_Dist_AmountValue.equals("0")) {
								mLoan_Sanc_AmountEditText.setError(AppStrings.mCheckLoanSanctionAmount);
								TastyToast.makeText(getActivity(), AppStrings.mCheckLoanAmounts,
										TastyToast.LENGTH_SHORT, TastyToast.WARNING);
							} else if (!mLoan_Sanc_AmountValue.equals("0") && mLoan_Dist_AmountValue.equals("0")) {
								mLoan_Dist_AmountEditText.setError(AppStrings.mCheckLoanDisbursementAmount);
								TastyToast.makeText(getActivity(), AppStrings.mCheckLoanAmounts,
										TastyToast.LENGTH_SHORT, TastyToast.WARNING);
							}

						}
					} else {
						TastyToast.makeText(getActivity(), AppStrings.mCheckLoanSancDate_LoanDisbDate,
								TastyToast.LENGTH_SHORT, TastyToast.WARNING);
					}

				} else {

					TastyToast.makeText(getActivity(), AppStrings.nullDetailsAlert, TastyToast.LENGTH_SHORT,
							TastyToast.WARNING);
				}
			}

			break;

		case R.id.fragment_Edit_button:

			mRaised_SubmitButton.setClickable(true);
			confirmationDialog.dismiss();

			break;
		case R.id.fragment_Ok_button:

			confirmationDialog.dismiss();
			EShaktiApplication.setIsNewLoanDisburseDate(false);

			if (EShaktiApplication.getTransactionDate() != null) {

				Transaction_InternalLoanDisburseMentFragment internalLoanDisburseMentFragment = new Transaction_InternalLoanDisburseMentFragment();
				setFragment(internalLoanDisburseMentFragment);
			} else {

				TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT, TastyToast.WARNING);

			}

			break;
		}
	}

	private void showConfirmationDialog(String[] mConfirm_text, String[] mConfirmation_values) {
		// TODO Auto-generated method stub

		confirmationDialog = new Dialog(getActivity());

		LayoutInflater inflater = getActivity().getLayoutInflater();
		View dialogView = inflater.inflate(R.layout.dialog_confirmation, null);
		dialogView.setLayoutParams(
				new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

		TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
		confirmationHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.confirmation));
		confirmationHeader.setTypeface(LoginActivity.sTypeface);

		TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

		mSize = mConfirm_BankLoan.length;

		for (int i = 0; i < mSize; i++) {

			TableRow indv_Row = new TableRow(getActivity());

			@SuppressWarnings("deprecation")
			TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);
			contentParams.setMargins(10, 5, 10, 5);

			TextView bankName_Text = new TextView(getActivity());
			bankName_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(mConfirm_text[i])));
			bankName_Text.setTypeface(LoginActivity.sTypeface);
			bankName_Text.setTextColor(color.white);
			bankName_Text.setPadding(5, 5, 5, 5);
			bankName_Text.setLayoutParams(contentParams);
			indv_Row.addView(bankName_Text);

			TextView confirm_values = new TextView(getActivity());
			confirm_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(mConfirmation_values[i])));
			confirm_values.setTextColor(color.white);
			confirm_values.setPadding(5, 5, 5, 5);
			confirm_values.setGravity(Gravity.RIGHT);
			confirm_values.setLayoutParams(contentParams);
			indv_Row.addView(confirm_values);

			confirmationTable.addView(indv_Row,
					new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

			if (mConfirmation_values[i].equals("Bank")) {

				if (selectedType.equals("Bank")) {

					TableRow increaseLimitRow1 = new TableRow(getActivity());

					TextView increaseLimitText1 = new TextView(getActivity());
					increaseLimitText1
							.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.bankName)));
					increaseLimitText1.setTypeface(LoginActivity.sTypeface);
					increaseLimitText1.setTextColor(color.white);
					increaseLimitText1.setPadding(5, 5, 5, 5);
					increaseLimitText1.setLayoutParams(contentParams);
					increaseLimitRow1.addView(increaseLimitText1);

					TextView increaseLimit_values1 = new TextView(getActivity());
					increaseLimit_values1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(mBankValue)));
					increaseLimit_values1.setTextColor(color.white);
					increaseLimit_values1.setPadding(5, 5, 5, 5);
					increaseLimit_values1.setGravity(Gravity.RIGHT);
					increaseLimit_values1.setLayoutParams(contentParams);
					increaseLimitRow1.addView(increaseLimit_values1);

					confirmationTable.addView(increaseLimitRow1,
							new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

				}
			}
		}

		mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit_button);
		mEdit_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.edit));
		mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
		mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
																// 205,
																// 0));
		mEdit_RaisedButton.setOnClickListener(this);

		mOk_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Ok_button);
		mOk_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.yes));
		mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
		mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
		mOk_RaisedButton.setOnClickListener(this);

		confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		confirmationDialog.setCanceledOnTouchOutside(false);
		confirmationDialog.setContentView(dialogView);
		confirmationDialog.setCancelable(true);
		confirmationDialog.show();

		MarginLayoutParams margin = (MarginLayoutParams) dialogView.getLayoutParams();
		margin.leftMargin = 10;
		margin.rightMargin = 10;
		margin.topMargin = 10;
		margin.bottomMargin = 10;
		margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);

	}

	private void calendarDialogShow() {
		// TODO Auto-generated method stub
		mLanguageLocale = PrefUtils.getUserlangcode();
		if (mLanguageLocale.equalsIgnoreCase("English")) {
			locale = new Locale("en");
		} else if (mLanguageLocale.equalsIgnoreCase("Tamil")) {
			locale = new Locale("ta");
		} else if (mLanguageLocale.equalsIgnoreCase("Hindi")) {
			locale = new Locale("hi");
		} else if (mLanguageLocale.equalsIgnoreCase("Marathi")) {
			locale = new Locale("ma");
		} else if (mLanguageLocale.equalsIgnoreCase("Malayalam")) {
			locale = new Locale("ml");
		} else if (mLanguageLocale.equalsIgnoreCase("Kannada")) {
			locale = new Locale("kn");
		} else if (mLanguageLocale.equalsIgnoreCase("Bengali")) {
			locale = new Locale("bn");
		} else if (mLanguageLocale.equalsIgnoreCase("Gujarati")) {
			locale = new Locale("gu");
		} else if (mLanguageLocale.equalsIgnoreCase("Punjabi")) {
			locale = new Locale("pa");
		} else if (mLanguageLocale.equalsIgnoreCase("Assamese")) {
			locale = new Locale("as");
		} else {
			locale = new Locale("en");
		}
		locale = new Locale("en");
		Locale.setDefault(locale);
		Configuration config = new Configuration();
		config.locale = locale;
		getActivity().getResources().updateConfiguration(config, getActivity().getResources().getDisplayMetrics());

		OnDateSetListener datelistener = Transaction_InternalBank_MFI_LoanFragment.this;
		Calendar now = Calendar.getInstance();
		FragmentManager fm = getFragmentManager();
		DatePickerDialog dialog = DatePickerDialog.newInstance(datelistener, now.get(Calendar.YEAR),
				now.get(Calendar.MONDAY), now.get(Calendar.DAY_OF_MONTH));

		if (bankLoan_check.equals("1")) {
			String lastTr_date = String.valueOf(DatePickerDialog.sDashboardDate);
			String dateArr[] = lastTr_date.split("-");

			Calendar max_Cal = Calendar.getInstance();

			int day = Integer.parseInt(dateArr[0]);
			int month = Integer.parseInt(dateArr[1]);
			int year = Integer.parseInt(dateArr[2]);
			EShaktiApplication.setCalendarDialog_MinDate(null);
			max_Cal.set(year, (month - 1), day);
			dialog.setMaxDate(max_Cal);
			dialog.show(fm, "");
		} else if (bankLoan_check.equals("2")) {

			if (EShaktiApplication.getNewLoanDisbursementMinDate() != null) {

				String sanc_date = String.valueOf(EShaktiApplication.getNewLoanDisbursementMinDate());
				String dateArr[] = sanc_date.split("/");

				Calendar min_Cal = Calendar.getInstance();

				int day = Integer.parseInt(dateArr[0]);
				int month = Integer.parseInt(dateArr[1]);
				int year = Integer.parseInt(dateArr[2]);

				min_Cal.set(year, (month - 1), day);
				dialog.setMinDate(min_Cal);
				EShaktiApplication.setCalendarDialog_MinDate(EShaktiApplication.getNewLoanDisbursementMinDate());
				//

				String lastTr_date = String.valueOf(DatePickerDialog.sDashboardDate);// (EShaktiApplication.getNextMonthLastDate());
				String dateArray[] = lastTr_date.split("-");

				Calendar max_Cal = Calendar.getInstance();

				int day_ = Integer.parseInt(dateArray[0]);
				int month_ = Integer.parseInt(dateArray[1]);
				int year_ = Integer.parseInt(dateArray[2]);

				max_Cal.set(year_, (month_ - 1), day_);
				dialog.setMaxDate(max_Cal);
				dialog.show(fm, "");
			} else {
				TastyToast.makeText(getActivity(), AppStrings.mCheckNewLoanSancDate, TastyToast.LENGTH_SHORT,
						TastyToast.WARNING);
			}

		}

	}

	@Override
	public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();

	}

	private void onSetCalendarValues() {
		// TODO Auto-generated method stub

		if (EShaktiApplication.getNextMonthLastDate() == null) {
			try {
				if (EShaktiApplication.isCalendarDateVisibleFlag()) {
					EShaktiApplication.setCalendarDateVisibleFlag(false);
				}

				Calendar calender = Calendar.getInstance();

				SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String formattedDate = df.format(calender.getTime());
				Log.e("Device Date  =  ", formattedDate + "");

				String mBalanceSheetDate = SelectedGroupsTask.sLastTransactionDate_Response; // dd/MM/yyyy
				String formatted_balancesheetDate = mBalanceSheetDate.replace("/", "-");
				Log.e("Balancesheet Date = ", formatted_balancesheetDate + "");

				Date balanceDate = df.parse(formatted_balancesheetDate);
				Date systemDate = df.parse(formattedDate);

				if (balanceDate.compareTo(systemDate) < 0 || balanceDate.compareTo(systemDate) == 0) {
					System.err.println(" balancesheet date is less than sys date");

					String mLastTransactionDate = SelectedGroupsTask.sLastTransactionDate_Response;

					String formattedDate_LastTransaction = mLastTransactionDate.replace("/", "-");

					String days = CalculateDateUtils.get_count_of_days(formattedDate_LastTransaction, formattedDate);

					int mDaycount = Integer.parseInt(days);
					Log.e("Total Days Count =====_____----", mDaycount + "");

					EShaktiApplication.setLastTransDate_GroupId(SelectedGroupsTask.Group_Id);

					if (mDaycount > 30) {

						SimpleDateFormat df_after = new SimpleDateFormat("dd-MM-yyyy");

						Calendar calendar = Calendar.getInstance();
						calendar.setTime(df_after.parse(formattedDate_LastTransaction));
						calendar.add(Calendar.MONTH, 1);
						calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
						Date nextMonthFirstDay = calendar.getTime();
						System.out.println(
								"------ Next month first date   =  " + df_after.format(nextMonthFirstDay) + "");

						Calendar cal_last_date = Calendar.getInstance();
						cal_last_date.setTime(df_after.parse(formattedDate_LastTransaction));
						cal_last_date.add(Calendar.MONTH, 1);
						cal_last_date.set(Calendar.DATE, cal_last_date.getActualMaximum(Calendar.DAY_OF_MONTH));
						Date nextMonthLastDay = cal_last_date.getTime();
						System.out
								.println("------ Next month Last date   =  " + df_after.format(nextMonthLastDay) + "");

						//
						String lastDateOfNextMonth = df_after.format(nextMonthLastDay);
						Calendar currentDate = Calendar.getInstance();
						int current_Day = currentDate.get(Calendar.DAY_OF_MONTH);
						int current_month = currentDate.get(Calendar.MONTH) + 1;
						int current_year = currentDate.get(Calendar.YEAR);
						String current_date = current_Day + "-" + current_month + "-" + current_year;

						Date lastdate = null, currentdate = null;
						String min_date_str = null;

						try {

							lastdate = df_after.parse(lastDateOfNextMonth);
							currentdate = df_after.parse(current_date);

						} catch (ParseException e) {
							// TODO: handle exception
							e.printStackTrace();
						}

						if (lastdate.compareTo(currentdate) <= 0) {
							min_date_str = lastDateOfNextMonth;
						} else {
							min_date_str = current_date;
						}

						Log.e("Minimum Date !!!!!	", min_date_str + "");

						EShaktiApplication.setNextMonthLastDate(min_date_str);

					} else {
						Calendar currentDate = Calendar.getInstance();
						SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
						EShaktiApplication.setNextMonthLastDate(sdf.format(currentDate.getTime()));

					}
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

	}

}
