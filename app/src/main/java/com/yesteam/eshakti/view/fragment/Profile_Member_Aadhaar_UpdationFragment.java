package com.yesteam.eshakti.view.fragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.utils.AadhaarValidationUtils;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.utils.Get_EdiText_Filter;
import com.yesteam.eshakti.utils.Reset;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.utils.TextviewUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.views.RaisedButton;
import com.yesteam.eshakti.webservices.Get_Update_AadhaarNumber_Webservices;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Profile_Member_Aadhaar_UpdationFragment extends Fragment implements OnClickListener, TaskListener {

	public static String TAG = Transaction_InternalLoanDisburseMentFragment.class.getSimpleName();
	private TextView mGroupName, mCashinHand, mCashatBank, mHeader, mAutoFill_label;
	private CheckBox mAutoFill;
	private Button mSubmit_Raised_Button;
	int mSize;
	private TableLayout mAadharNumberTable;
	private EditText mAadhaarNumber_values;
	private static List<EditText> sAadhaarnumberFields = new ArrayList<EditText>();
	private static String mAadhaarnumberValues[];
	String[] confirmArr;
	String nullVlaue = "0";
	public static String sSendToServer_MemberAadhaarNumber;
	public static int sIncome_Total;

	Dialog confirmationDialog;
	private RaisedButton mEdit_RaisedButton, mOk_RaisedButton;
	private Dialog mProgressDialog;

	boolean isNaviMain = false;
	boolean isServiceCall = false;

	LinearLayout mMemberNameLayout;
	TextView mMemberName;

	Vector<String> _Member_Id = new Vector<String>();
	Vector<String> _Memberaadhaarnumber = new Vector<String>();

	public Profile_Member_Aadhaar_UpdationFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_INTERLAONMENU;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		sSendToServer_MemberAadhaarNumber = Reset.reset(sSendToServer_MemberAadhaarNumber);
		sIncome_Total = 0;
		sAadhaarnumberFields.clear();

		_Member_Id.clear();
		_Memberaadhaarnumber.clear();

		_Member_Id = new Vector<String>();
		_Memberaadhaarnumber = new Vector<String>();

	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		SBus.INST.register(this);
	}

	@SuppressWarnings("deprecation")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_internalloan_disbursement, container, false);

		MainFragment_Dashboard.isBackpressed = false;

		try {

			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashinHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashinHand.setTypeface(LoginActivity.sTypeface);

			mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashatBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashatBank.setTypeface(LoginActivity.sTypeface);

			mMemberNameLayout = (LinearLayout) rootView.findViewById(R.id.member_name_layout);
			mMemberName = (TextView) rootView.findViewById(R.id.member_name);

			/** UI Mapping **/

			mHeader = (TextView) rootView.findViewById(R.id.internal_fragmentHeader);
			// mHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.mInternalTypeLoan));
			mHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.mAadhaarNoUpdation));
			mHeader.setTypeface(LoginActivity.sTypeface);

			mAutoFill_label = (TextView) rootView.findViewById(R.id.internal_autofillLabel);
			mAutoFill_label.setText(RegionalConversion.getRegionalConversion(AppStrings.autoFill));
			mAutoFill_label.setTypeface(LoginActivity.sTypeface);
			mAutoFill_label.setVisibility(View.GONE);

			mAutoFill = (CheckBox) rootView.findViewById(R.id.internal_autoFill);
			mAutoFill.setVisibility(View.GONE);
			mAutoFill.setOnClickListener(this);

			mSize = SelectedGroupsTask.member_Name.size();
			Log.d(TAG, String.valueOf(mSize));

			if (publicValues.mGetMemberAadhaarNumberValues != null) {

				String[] _StringArray = publicValues.mGetMemberAadhaarNumberValues.split("#");

				_Member_Id.clear();
				_Memberaadhaarnumber.clear();

				for (int i = 0; i < _StringArray.length; i++) {
					String[] _IndiArray = _StringArray[i].split("~");
					_Member_Id.add(_IndiArray[0]);
					if (_IndiArray[1].equals("No")) {
						_Memberaadhaarnumber.add("");
					} else {
						_Memberaadhaarnumber.add(_IndiArray[1]);
					}

				}
			}

			TableLayout headerTable = (TableLayout) rootView.findViewById(R.id.internal_savingsTable);

			mAadharNumberTable = (TableLayout) rootView.findViewById(R.id.internal_fragment_contentTable);

			TableRow savingsHeader = new TableRow(getActivity());
			savingsHeader.setBackgroundResource(color.tableHeader);

			LayoutParams headerParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT,
					1f);

			TextView mMemberName_headerText = new TextView(getActivity());
			mMemberName_headerText
					.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.memberName)));
			mMemberName_headerText.setTypeface(LoginActivity.sTypeface);
			mMemberName_headerText.setTextColor(Color.WHITE);
			mMemberName_headerText.setPadding(20, 5, 10, 5);
			mMemberName_headerText.setLayoutParams(headerParams);
			savingsHeader.addView(mMemberName_headerText);

			TextView mIncomeAmount_HeaderText = new TextView(getActivity());
			mIncomeAmount_HeaderText
					.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.mAadhaarNo)));
			mIncomeAmount_HeaderText.setTypeface(LoginActivity.sTypeface);
			mIncomeAmount_HeaderText.setTextColor(Color.WHITE);
			mIncomeAmount_HeaderText.setPadding(10, 5, 40, 5);
			mIncomeAmount_HeaderText.setLayoutParams(headerParams);
			mIncomeAmount_HeaderText.setBackgroundResource(color.tableHeader);
			savingsHeader.addView(mIncomeAmount_HeaderText);

			headerTable.addView(savingsHeader,
					new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

			for (int i = 0; i < mSize; i++) {

				TableRow indv_IncomeRow = new TableRow(getActivity());

				TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
						LayoutParams.WRAP_CONTENT, 1f);
				contentParams.setMargins(10, 5, 10, 5);

				final TextView memberName_Text = new TextView(getActivity());
				memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
						String.valueOf(SelectedGroupsTask.member_Name.elementAt(i))));
				memberName_Text.setTypeface(LoginActivity.sTypeface);
				memberName_Text.setTextColor(color.black);
				memberName_Text.setPadding(10, 0, 10, 5);
				memberName_Text.setLayoutParams(contentParams);
				memberName_Text.setWidth(200);
				memberName_Text.setSingleLine(true);
				memberName_Text.setEllipsize(TextUtils.TruncateAt.END);
				indv_IncomeRow.addView(memberName_Text);

				TableRow.LayoutParams contentEditParams = new TableRow.LayoutParams(150, LayoutParams.WRAP_CONTENT);
				contentEditParams.setMargins(30, 5, 100, 5);

				mAadhaarNumber_values = new EditText(getActivity());

				mAadhaarNumber_values.setId(i);
				sAadhaarnumberFields.add(mAadhaarNumber_values);
				mAadhaarNumber_values.setGravity(Gravity.END);
				mAadhaarNumber_values.setTextColor(Color.BLACK);
				mAadhaarNumber_values.setPadding(5, 5, 5, 5);
				mAadhaarNumber_values.setBackgroundResource(R.drawable.edittext_background);
				mAadhaarNumber_values.setLayoutParams(contentEditParams);// contentParams
				// lParams
				mAadhaarNumber_values.setTextAppearance(getActivity(), R.style.MyMaterialTheme);
				mAadhaarNumber_values.setFilters(Get_EdiText_Filter.editText_aadhaar_number_filter());
				mAadhaarNumber_values.setInputType(InputType.TYPE_CLASS_NUMBER);
				mAadhaarNumber_values.setTextColor(color.black);
				mAadhaarNumber_values.setText(_Memberaadhaarnumber.elementAt(i));
				mAadhaarNumber_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

					@Override
					public void onFocusChange(View v, boolean hasFocus) {
						// TODO Auto-generated method stub
						if (hasFocus) {
							((EditText) v).setGravity(Gravity.LEFT);

							mMemberNameLayout.setVisibility(View.VISIBLE);
							mMemberName.setText(memberName_Text.getText().toString().trim());
							mMemberName.setTypeface(LoginActivity.sTypeface);
							TextviewUtils.manageBlinkEffect(mMemberName, getActivity());
						} else {
							((EditText) v).setGravity(Gravity.RIGHT);

							mMemberNameLayout.setVisibility(View.GONE);
							mMemberName.setText("");
						}

					}
				});
				indv_IncomeRow.addView(mAadhaarNumber_values);

				mAadharNumberTable.addView(indv_IncomeRow,
						new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

			}

			mSubmit_Raised_Button = (Button) rootView.findViewById(R.id.internal_fragment_Submit_button);
			mSubmit_Raised_Button.setText(RegionalConversion.getRegionalConversion(AppStrings.submit));
			mSubmit_Raised_Button.setTypeface(LoginActivity.sTypeface);
			mSubmit_Raised_Button.setOnClickListener(this);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return rootView;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		mAadhaarnumberValues = new String[sAadhaarnumberFields.size()];
		Log.d(TAG, "Member Mobile number size ------>>> : " + mAadhaarnumberValues.length);
		switch (v.getId()) {
		case R.id.internal_fragment_Submit_button:
			boolean _MasterisValid = true;
			boolean _IsEmpty = true;
			boolean _SameAadhaarNumber = false;
			sSendToServer_MemberAadhaarNumber = Reset.reset(sSendToServer_MemberAadhaarNumber);
			try {
				for (int i = 0; i < mAadhaarnumberValues.length; i++) {
					boolean isValid = false;
					mAadhaarnumberValues[i] = String.valueOf(sAadhaarnumberFields.get(i).getText());
					Log.d(TAG, "Entered Values : " + mAadhaarnumberValues[i] + " POS : " + i);

					if (!sAadhaarnumberFields.get(i).getText().toString().isEmpty()) {
						_IsEmpty = false;
					}

					isValid = AadhaarValidationUtils.validateAadharNumber(mAadhaarnumberValues[i]);
					if (!isValid) {
						sAadhaarnumberFields.get(i)
								.setError(RegionalConversion.getRegionalConversion(AppStrings.mInvalidAadhaarNo));
						_MasterisValid = false;
					}

				}
				String mValues = null;
				for (int i = 0; i < mAadhaarnumberValues.length; i++) {

					mValues = mAadhaarnumberValues[i];

					for (int j = 0; j < mAadhaarnumberValues.length; j++) {
						if (i != j && !mValues.equals("")) {
							if (mValues.equals(mAadhaarnumberValues[j])) {
								_SameAadhaarNumber = true;
							}
						}
					}

				}

				if (_MasterisValid && !_IsEmpty && !_SameAadhaarNumber) {
					for (int i = 0; i < mAadhaarnumberValues.length; i++) {

						sSendToServer_MemberAadhaarNumber = sSendToServer_MemberAadhaarNumber
								+ String.valueOf(SelectedGroupsTask.member_Id.elementAt(i)) + "~"
								+ _Memberaadhaarnumber.elementAt(i).replace("No", "") + "~" + mAadhaarnumberValues[i]
								+ "#";

					}

					callConfirmationDialog();

					Log.e("Master Send to server values ---->>.", sSendToServer_MemberAadhaarNumber);
				} else {
					if (_SameAadhaarNumber) {
						TastyToast.makeText(getActivity(), "Member's having same aadhaar number",
								TastyToast.LENGTH_SHORT, TastyToast.ERROR);
					} else {
						TastyToast.makeText(getActivity(), AppStrings.mCheckValidAadhaarNo, TastyToast.LENGTH_SHORT,
								TastyToast.ERROR);
					}

				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

			break;

		case R.id.fragment_Edit_button:

			sSendToServer_MemberAadhaarNumber = Reset.reset(sSendToServer_MemberAadhaarNumber);
			sIncome_Total = Integer.valueOf(nullVlaue);
			mSubmit_Raised_Button.setClickable(true);
			isServiceCall = false;
			confirmationDialog.dismiss();
			break;

		case R.id.fragment_Ok_button:

			if (ConnectionUtils.isNetworkAvailable(getActivity())) {

				new Get_Update_AadhaarNumber_Webservices(this).execute();

			} else {
				// Do offline Stuffs

				TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT, TastyToast.ERROR);

			}
			break;

		}
	}

	private void callConfirmationDialog() {
		// TODO Auto-generated method stub

		confirmationDialog = new Dialog(getActivity());

		LayoutInflater inflater = getActivity().getLayoutInflater();
		View dialogView = inflater.inflate(R.layout.dialog_confirmation, null);
		dialogView.setLayoutParams(
				new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

		TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
		confirmationHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.confirmation));
		confirmationHeader.setTypeface(LoginActivity.sTypeface);

		TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

		for (int i = 0; i < SelectedGroupsTask.member_Name.size(); i++) {

			TableRow indv_SavingsRow = new TableRow(getActivity());

			@SuppressWarnings("deprecation")
			TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);
			contentParams.setMargins(10, 5, 10, 5);

			TextView memberName_Text = new TextView(getActivity());
			memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
					String.valueOf(SelectedGroupsTask.member_Name.elementAt(i))));
			memberName_Text.setTypeface(LoginActivity.sTypeface);
			memberName_Text.setTextColor(color.black);
			memberName_Text.setPadding(5, 5, 5, 5);
			memberName_Text.setLayoutParams(contentParams);
			indv_SavingsRow.addView(memberName_Text);

			TextView confirm_values = new TextView(getActivity());
			confirm_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(mAadhaarnumberValues[i])));
			confirm_values.setTextColor(color.black);
			confirm_values.setPadding(5, 5, 5, 5);
			confirm_values.setGravity(Gravity.RIGHT);
			confirm_values.setLayoutParams(contentParams);
			indv_SavingsRow.addView(confirm_values);

			confirmationTable.addView(indv_SavingsRow,
					new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

		}

		mEdit_RaisedButton = (RaisedButton) dialogView.findViewById(R.id.fragment_Edit_button);
		mEdit_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.edit));
		mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
		mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
		mEdit_RaisedButton.setOnClickListener(this);

		mOk_RaisedButton = (RaisedButton) dialogView.findViewById(R.id.fragment_Ok_button);
		mOk_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.yes));
		mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
		mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
		mOk_RaisedButton.setOnClickListener(this);

		confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		confirmationDialog.setCanceledOnTouchOutside(false);
		confirmationDialog.setContentView(dialogView);
		confirmationDialog.setCancelable(true);
		confirmationDialog.show();

		MarginLayoutParams margin = (MarginLayoutParams) dialogView.getLayoutParams();
		margin.leftMargin = 10;
		margin.rightMargin = 10;
		margin.topMargin = 10;
		margin.bottomMargin = 10;
		margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);

	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub
		mProgressDialog = AppDialogUtils.createProgressDialog(getActivity());
		mProgressDialog.show();
	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub

		try {

			if (mProgressDialog != null) {
				mProgressDialog.dismiss();

				if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {
					getActivity().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub

							if (!result.equals("FAIL")) {
								isServiceCall = false;
								TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg,
										TastyToast.LENGTH_SHORT, TastyToast.ERROR);
								Constants.NETWORKCOMMONFLAG = "SUCESS";
							}

						}
					});
				} else {
					confirmationDialog.dismiss();
					TastyToast.makeText(getActivity(), AppStrings.mAadhaarNoUpdateSuccessAlert, TastyToast.LENGTH_SHORT,
							TastyToast.SUCCESS);

					MainFragment_Dashboard dashboard = new MainFragment_Dashboard();
					setFragment(dashboard);
				}
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub
		Log.e("Current Class Name!!!!!!!!!!!!!!", fragment.getClass().getName());
		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		SBus.INST.unRegister(this);
	}
}
