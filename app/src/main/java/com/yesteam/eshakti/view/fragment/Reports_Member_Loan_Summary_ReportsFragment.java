package com.yesteam.eshakti.view.fragment;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.Member_Loan_summaryTask;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Reports_Member_Loan_Summary_ReportsFragment extends Fragment {

	String mTag = Reports_Member_Loan_Summary_ReportsFragment.class.getSimpleName();

	private TextView mGroupName, mCashinHand, mCashatBank, mHeader, mMemberName;

	private String responseArr[];
	private TableLayout mReportsTable;
	private String mLanguagelocale = "";

	public Reports_Member_Loan_Summary_ReportsFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_MEMBER_REPORTS_MEMBER_LOAN_SUMM;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		try {

			responseArr = (Member_Loan_summaryTask.sMember_Loan_summary_ServiceResponse.toString()).split("~");
			Log.d(mTag, String.valueOf(responseArr.length));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.frgament_reports_summary, container, false);

		MainFragment_Dashboard.isBackpressed = false;

		try {

			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mMemberName = (TextView) rootView.findViewById(R.id.memberName);
			mMemberName.setText(EShaktiApplication.getSelectedMemberName());
			mMemberName.setTypeface(LoginActivity.sTypeface);
			mMemberName.setVisibility(View.VISIBLE);

			mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashinHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashinHand.setTypeface(LoginActivity.sTypeface);

			mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashatBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashatBank.setTypeface(LoginActivity.sTypeface);

			mHeader = (TextView) rootView.findViewById(R.id.fragmentHeader);
			mHeader.setText(RegionalConversion.getRegionalConversion(
					Reports_MemberReports_LoanSummaryMenuFragment.sReports_SelectedLaonName.toString() + " "
							+ AppStrings.summary));
			mHeader.setTypeface(LoginActivity.sTypeface, Typeface.BOLD);

			mReportsTable = (TableLayout) rootView.findViewById(R.id.fragment_ReportsTable);

			mLanguagelocale = PrefUtils.getUserlangcode();

			TableLayout headerTable = (TableLayout) rootView.findViewById(R.id.memberReports_headerTable);
			TableLayout openingBalanceTable = (TableLayout) rootView.findViewById(R.id.fragment_OpeningBalanceTable);

			TableRow totalLoan_Row = new TableRow(getActivity());
			totalLoan_Row.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
			totalLoan_Row.setPadding(15, 10, 5, 5);
			totalLoan_Row.setGravity(Gravity.CENTER_HORIZONTAL);

			TextView totalLoan_Text = new TextView(getActivity());
			totalLoan_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.total)));
			totalLoan_Text.setTypeface(LoginActivity.sTypeface, Typeface.BOLD);
			totalLoan_Text.setTextColor(Color.BLACK);
			totalLoan_Text.setPadding(15, 5, 5, 5);
			totalLoan_Row.addView(totalLoan_Text);

			TextView totalLoan_Outstanding = new TextView(getActivity());
			totalLoan_Outstanding.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(responseArr[1])));
			totalLoan_Outstanding.setTextColor(Color.BLACK);
			totalLoan_Outstanding.setTypeface(null, Typeface.BOLD);
			totalLoan_Outstanding.setPadding(5, 5, 5, 5);
			totalLoan_Row.addView(totalLoan_Outstanding);

			openingBalanceTable.addView(totalLoan_Row,
					new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

			TableRow outstandingRow = new TableRow(getActivity());
			outstandingRow.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
			outstandingRow.setPadding(15, 0, 5, 5);
			outstandingRow.setGravity(Gravity.CENTER_HORIZONTAL);

			TextView outstanding_Text = new TextView(getActivity());
			outstanding_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.outstanding)));
			outstanding_Text.setTypeface(LoginActivity.sTypeface, Typeface.BOLD);
			outstanding_Text.setTextColor(color.black);
			outstanding_Text.setPadding(5, 5, 5, 5);
			outstandingRow.addView(outstanding_Text);

			TextView outstandingAmount = new TextView(getActivity());
			outstandingAmount.setText(
					GetSpanText.getSpanString(getActivity(), String.valueOf(responseArr[responseArr.length - 1])));
			outstandingAmount.setTextColor(color.black);
			outstandingAmount.setTypeface(null, Typeface.BOLD);
			outstandingAmount.setPadding(5, 5, 5, 5);
			outstandingRow.addView(outstandingAmount);

			openingBalanceTable.addView(outstandingRow,
					new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

			TableRow loanSummaryHeader_Row = new TableRow(getActivity());

			TableRow.LayoutParams loanSummaryParams = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
					TableRow.LayoutParams.WRAP_CONTENT, 1f);

			TextView date_headerText = new TextView(getActivity());
			date_headerText.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.date)));
			date_headerText.setTypeface(LoginActivity.sTypeface);
			date_headerText.setTextColor(Color.WHITE);
			date_headerText.setPadding(40, 5, 10, 5);
			// date_headerText.setGravity(Gravity.CENTER);
			date_headerText.setLayoutParams(loanSummaryParams);
			date_headerText.setBackgroundResource(color.tableHeader);
			loanSummaryHeader_Row.addView(date_headerText);

			TextView principle_headerText = new TextView(getActivity());
			principle_headerText
					.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.principleAmount)));
			principle_headerText.setTypeface(LoginActivity.sTypeface);
			principle_headerText.setTextColor(Color.WHITE);
			if (mLanguagelocale.equalsIgnoreCase("English")) {
				principle_headerText.setPadding(20, 5, 10, 5);
			} else if (mLanguagelocale.equalsIgnoreCase("Tamil")) {
				principle_headerText.setPadding(20, 5, 20, 5);
			} else if (mLanguagelocale.equalsIgnoreCase("Hindi")) {
				principle_headerText.setPadding(20, 5, 20, 5);
			} else if (mLanguagelocale.equalsIgnoreCase("Marathi")) {
				principle_headerText.setPadding(20, 5, 20, 5);
			} else {
				principle_headerText.setPadding(20, 5, 10, 5);
			}
			principle_headerText.setGravity(Gravity.RIGHT);
			principle_headerText.setLayoutParams(loanSummaryParams);
			principle_headerText.setBackgroundResource(color.tableHeader);
			loanSummaryHeader_Row.addView(principle_headerText);

			TextView interest_headerText = new TextView(getActivity());
			interest_headerText.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.interest)));
			interest_headerText.setTypeface(LoginActivity.sTypeface);
			interest_headerText.setTextColor(Color.WHITE);
			interest_headerText.setPadding(10, 5, 10, 5);
			interest_headerText.setGravity(Gravity.RIGHT);
			interest_headerText.setLayoutParams(loanSummaryParams);
			interest_headerText.setBackgroundResource(color.tableHeader);
			loanSummaryHeader_Row.addView(interest_headerText);

			headerTable.addView(loanSummaryHeader_Row,
					new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

			for (int i = 2; i < (responseArr.length - 4); i = i + 3) {

				TableRow loanSummary_Row = new TableRow(getActivity());

				TableRow.LayoutParams loanSummary_ContentParams = new TableRow.LayoutParams(
						TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

				TextView transaction_Date = new TextView(getActivity());
				transaction_Date.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(responseArr[i])));
				transaction_Date.setPadding(10, 5, 10, 5);
				transaction_Date.setLayoutParams(loanSummary_ContentParams);
				loanSummary_Row.addView(transaction_Date);

				TextView transaction_PA = new TextView(getActivity());
				transaction_PA.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(responseArr[i + 1])));
				transaction_PA.setPadding(10, 5, 25, 5);
				transaction_PA.setGravity(Gravity.RIGHT);
				transaction_PA.setLayoutParams(loanSummary_ContentParams);
				loanSummary_Row.addView(transaction_PA);

				TextView transaction_Interest = new TextView(getActivity());
				transaction_Interest
						.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(responseArr[i + 2])));
				transaction_Interest.setPadding(10, 5, 10, 5);
				transaction_Interest.setGravity(Gravity.RIGHT);
				transaction_Interest.setLayoutParams(loanSummary_ContentParams);
				loanSummary_Row.addView(transaction_Interest);

				mReportsTable.addView(loanSummary_Row,
						new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

				View rullerView = new View(getActivity());
				rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
				rullerView.setBackgroundColor(Color.rgb(220, 220, 220));

				mReportsTable.addView(rullerView);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return rootView;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

}
