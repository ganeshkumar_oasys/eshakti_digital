package com.yesteam.eshakti.view.fragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.squareup.otto.Subscribe;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.sqlite.database.GroupProvider;
import com.yesteam.eshakti.sqlite.database.response.GetLoanAccountNoResponse;
import com.yesteam.eshakti.sqlite.database.response.GroupLoanOSResponse;
import com.yesteam.eshakti.sqlite.database.response.GroupMLOSUpdateResponse;
import com.yesteam.eshakti.sqlite.database.response.GroupMemberloanOSResponse;
import com.yesteam.eshakti.sqlite.database.response.LoanAccountNoUpdateResponse;
import com.yesteam.eshakti.sqlite.db.model.GetGroupMemberDetails;
import com.yesteam.eshakti.sqlite.db.model.GroupLoanOSUpdate;
import com.yesteam.eshakti.sqlite.db.model.GroupMLUpdate;
import com.yesteam.eshakti.sqlite.db.model.GroupMasterSingle;
import com.yesteam.eshakti.sqlite.db.model.GroupResponseUpdate;
import com.yesteam.eshakti.sqlite.db.model.GroupTempDBLastTransDate;
import com.yesteam.eshakti.sqlite.db.model.LoanAccountNoDetailsUpdate;
import com.yesteam.eshakti.sqlite.db.transactions.TransactionManager.DataType;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.utils.GetOfflineTransactionValues;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.utils.Get_EdiText_Filter;
import com.yesteam.eshakti.utils.Put_DB_GroupNameDetail_Response;
import com.yesteam.eshakti.utils.Put_DB_GroupResponse;
import com.yesteam.eshakti.utils.Reset;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.utils.TextviewUtils;
import com.yesteam.eshakti.utils.UpdateCalendarMinMaxDateUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.views.RaisedButton;
import com.yesteam.eshakti.webservices.GetAddLoan_Installment_CCWebservice;
import com.yesteam.eshakti.webservices.GetAddLoan_Installment_GroupAccWebservice;
import com.yesteam.eshakti.webservices.GetAddLoan_Installment_GroupWebservice;
import com.yesteam.eshakti.webservices.Get_LastTransactionID;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Transaction_Loan_SB_disbursementFragment extends Fragment implements OnClickListener, TaskListener {

	public static String TAG = Transaction_InternalLoanDisburseMentFragment.class.getSimpleName();
	private TextView mGroupName, mCashinHand, mCashatBank, mHeader, mAutoFill_label;
	private CheckBox mAutoFill;
	private Button mSubmit_Raised_Button;
	int mSize;
	private TableLayout mIncomeTable;
	private EditText mIncome_values;
	private static List<EditText> sIncomeFields = new ArrayList<EditText>();
	private static String sIncomeAmounts[];
	String[] confirmArr;
	String nullVlaue = "0";
	public static String sSendToServer_InternalLoanDisbursement;
	public static int sIncome_Total;

	Dialog confirmationDialog;
	private RaisedButton mEdit_RaisedButton, mOk_RaisedButton;
	private Dialog mProgressDialog;

	boolean isNaviMain = false;
	boolean isServiceCall = false;
	String mLastTrDate = null, mLastTr_ID = null;
	public static String mMemberOS_Offlineresponse = null;
	public static String[] response;
	public static String mMemberOsValues = null;
	boolean isLastTransactionValues = false;

	String ToLoanAccNo = "", outstandingAmt = "";
	ArrayList<String> mLoanBankName = new ArrayList<String>();
	ArrayList<String> mLoanName = new ArrayList<String>();
	ArrayList<String> mLoanId = new ArrayList<String>();
	ArrayList<String> mLoanOutstanding = new ArrayList<String>();
	ArrayList<String> mLoanAccNo = new ArrayList<String>();
	int mCount = 0;
	LinearLayout mMemberNameLayout;
	TextView mMemberName;
	
	public Transaction_Loan_SB_disbursementFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_LOAN_SB_FINAL_DISBURSE;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		sSendToServer_InternalLoanDisbursement = Reset.reset(sSendToServer_InternalLoanDisbursement);
		sIncome_Total = 0;
		sIncomeFields.clear();
		mMemberOsValues = Reset.reset(mMemberOsValues);

	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		SBus.INST.register(this);
	}

	@SuppressWarnings("deprecation")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_internalloan_disbursement, container, false);

		MainFragment_Dashboard.isBackpressed = false;

		try {

			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashinHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashinHand.setTypeface(LoginActivity.sTypeface);

			mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashatBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashatBank.setTypeface(LoginActivity.sTypeface);
			
			mMemberNameLayout = (LinearLayout) rootView.findViewById(R.id.member_name_layout);
			mMemberName = (TextView) rootView.findViewById(R.id.member_name);

			/** UI Mapping **/

			mHeader = (TextView) rootView.findViewById(R.id.internal_fragmentHeader);

			mHeader.setText(RegionalConversion
					.getRegionalConversion(Transaction_LoanDisbursement_LoansMenuFragment.mloan_Name));// AppStrings.mInternalTypeLoan));
			mHeader.setTypeface(LoginActivity.sTypeface);

			mAutoFill_label = (TextView) rootView.findViewById(R.id.internal_autofillLabel);
			mAutoFill_label.setText(RegionalConversion.getRegionalConversion(AppStrings.autoFill));
			mAutoFill_label.setTypeface(LoginActivity.sTypeface);
			mAutoFill_label.setVisibility(View.VISIBLE);

			mAutoFill = (CheckBox) rootView.findViewById(R.id.internal_autoFill);
			mAutoFill.setVisibility(View.VISIBLE);
			mAutoFill.setOnClickListener(this);

			mSize = SelectedGroupsTask.member_Name.size();
			Log.d(TAG, String.valueOf(mSize));

			TableLayout headerTable = (TableLayout) rootView.findViewById(R.id.internal_savingsTable);

			mIncomeTable = (TableLayout) rootView.findViewById(R.id.internal_fragment_contentTable);

			TableRow savingsHeader = new TableRow(getActivity());
			savingsHeader.setBackgroundResource(color.tableHeader);

			LayoutParams headerParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT,
					1f);

			TextView mMemberName_headerText = new TextView(getActivity());
			mMemberName_headerText
					.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.memberName)));
			mMemberName_headerText.setTypeface(LoginActivity.sTypeface);
			mMemberName_headerText.setTextColor(Color.WHITE);
			mMemberName_headerText.setPadding(20, 5, 10, 5);
			mMemberName_headerText.setLayoutParams(headerParams);
			savingsHeader.addView(mMemberName_headerText);

			TextView mIncomeAmount_HeaderText = new TextView(getActivity());
			mIncomeAmount_HeaderText
					.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.amount)));
			mIncomeAmount_HeaderText.setTypeface(LoginActivity.sTypeface);
			mIncomeAmount_HeaderText.setTextColor(Color.WHITE);
			mIncomeAmount_HeaderText.setPadding(10, 5, 40, 5);
			mIncomeAmount_HeaderText.setLayoutParams(headerParams);
			mIncomeAmount_HeaderText.setBackgroundResource(color.tableHeader);
			savingsHeader.addView(mIncomeAmount_HeaderText);

			headerTable.addView(savingsHeader,
					new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

			for (int i = 0; i < mSize; i++) {

				TableRow indv_IncomeRow = new TableRow(getActivity());

				TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
						LayoutParams.WRAP_CONTENT, 1f);
				contentParams.setMargins(10, 5, 10, 5);

				final TextView memberName_Text = new TextView(getActivity());
				memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
						String.valueOf(SelectedGroupsTask.member_Name.elementAt(i))));
				memberName_Text.setTypeface(LoginActivity.sTypeface);
				memberName_Text.setTextColor(color.black);
				memberName_Text.setPadding(10, 0, 10, 5);
				memberName_Text.setLayoutParams(contentParams);
				memberName_Text.setWidth(200);
				memberName_Text.setSingleLine(true);
				memberName_Text.setEllipsize(TextUtils.TruncateAt.END);
				indv_IncomeRow.addView(memberName_Text);

				TableRow.LayoutParams contentEditParams = new TableRow.LayoutParams(150, LayoutParams.WRAP_CONTENT);
				contentEditParams.setMargins(30, 5, 100, 5);

				mIncome_values = new EditText(getActivity());

				mIncome_values.setId(i);
				sIncomeFields.add(mIncome_values);
				mIncome_values.setGravity(Gravity.END);
				mIncome_values.setTextColor(Color.BLACK);
				mIncome_values.setPadding(5, 5, 5, 5);
				mIncome_values.setBackgroundResource(R.drawable.edittext_background);
				mIncome_values.setLayoutParams(contentEditParams);// contentParams
																	// lParams
				mIncome_values.setTextAppearance(getActivity(), R.style.MyMaterialTheme);
				mIncome_values.setFilters(Get_EdiText_Filter.editText_filter());
				mIncome_values.setInputType(InputType.TYPE_CLASS_NUMBER);
				mIncome_values.setTextColor(color.black);
				mIncome_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

					@Override
					public void onFocusChange(View v, boolean hasFocus) {
						// TODO Auto-generated method stub
						if (hasFocus) {
							((EditText) v).setGravity(Gravity.LEFT);
							mMemberNameLayout.setVisibility(View.VISIBLE);
							mMemberName.setText(memberName_Text.getText().toString().trim());
							mMemberName.setTypeface(LoginActivity.sTypeface);
							TextviewUtils.manageBlinkEffect(mMemberName, getActivity());
						} else {
							((EditText) v).setGravity(Gravity.RIGHT);
							mMemberNameLayout.setVisibility(View.GONE);
							mMemberName.setText("");
						}

					}
				});
				indv_IncomeRow.addView(mIncome_values);

				mIncomeTable.addView(indv_IncomeRow,
						new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

			}

			mSubmit_Raised_Button = (Button) rootView.findViewById(R.id.internal_fragment_Submit_button);
			mSubmit_Raised_Button.setText(RegionalConversion.getRegionalConversion(AppStrings.submit));
			mSubmit_Raised_Button.setTypeface(LoginActivity.sTypeface);
			mSubmit_Raised_Button.setOnClickListener(this);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return rootView;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		sIncomeAmounts = new String[sIncomeFields.size()];
		Log.d(TAG, "sIncomeAmounts size : " + sIncomeAmounts.length);
		switch (v.getId()) {
		case R.id.internal_fragment_Submit_button:
			try {
				// isIncome = true;

				sSendToServer_InternalLoanDisbursement = "";
				sIncome_Total = 0;

				confirmArr = new String[mSize];

				StringBuilder builder = new StringBuilder();

				for (int i = 0; i < sIncomeAmounts.length; i++) {

					sIncomeAmounts[i] = String.valueOf(sIncomeFields.get(i).getText());
					Log.d(TAG, "Entered Values : " + sIncomeAmounts[i] + " POS : " + i);

					if ((sIncomeAmounts[i].equals("")) || (sIncomeAmounts[i] == null)) {
						sIncomeAmounts[i] = nullVlaue;
					}

					if (sIncomeAmounts[i].matches("\\d*\\.?\\d+")) { // match a
																		// decimal
																		// number

						int amount = (int) Math.round(Double.parseDouble(sIncomeAmounts[i]));
						sIncomeAmounts[i] = String.valueOf(amount);
					}

					sSendToServer_InternalLoanDisbursement = sSendToServer_InternalLoanDisbursement
							+ String.valueOf(SelectedGroupsTask.member_Id.elementAt(i)) + "~" + sIncomeAmounts[i] + "~";

					confirmArr[i] = String.valueOf(SelectedGroupsTask.member_Name.elementAt(i)) + "           "
							+ sIncomeAmounts[i];

					sIncome_Total = sIncome_Total + Integer.parseInt(sIncomeAmounts[i]);

					builder.append(sIncomeAmounts[i]).append(",");
				}

				Log.d(TAG, sSendToServer_InternalLoanDisbursement);

				Log.d(TAG, "TOTAL " + String.valueOf(sIncome_Total));

				if (sIncome_Total != 0) {
					if (EShaktiApplication.getLoanDisburseValues().equals("SBACC")) {

						if (sIncome_Total <= Integer.parseInt(EShaktiApplication.getSBAccBalanceAmount())) {
							if (EShaktiApplication.getSelectedType().equals("CASH")) {

								if (sIncome_Total <= Integer.parseInt(SelectedGroupsTask.sCashinHand.trim())) {
									showConfirmationDialog();
								} else {
									TastyToast.makeText(getActivity(), AppStrings.cashinHandAlert,
											TastyToast.LENGTH_SHORT, TastyToast.WARNING);
								}
							} else if (EShaktiApplication.getSelectedType().equals("BANK")) {

								if (sIncome_Total <= Integer.parseInt(EShaktiApplication.getSelectedBankAmount())
										&& !EShaktiApplication.getSelectedBankAmount().equals("")) {
									showConfirmationDialog();
								} else {
									TastyToast.makeText(getActivity(), AppStrings.cashatBankAlert,
											TastyToast.LENGTH_SHORT, TastyToast.WARNING);
								}
							}

						} else {
							TastyToast.makeText(getActivity(), AppStrings.mCheckbalanceAlert, TastyToast.LENGTH_SHORT,
									TastyToast.WARNING);
						}

					} else if (EShaktiApplication.getLoanDisburseValues().equals("LOANACC")) {
						if (sIncome_Total <= Integer.parseInt(EShaktiApplication.getLoanAccBalanceAmount())) {
							showConfirmationDialog();
						} else {
							TastyToast.makeText(getActivity(), AppStrings.mCheckDisbursementAlert,
									TastyToast.LENGTH_SHORT, TastyToast.WARNING);
						}
					} else if (EShaktiApplication.getLoanDisburseValues().equals("REPAID")) {
						if (sIncome_Total <= Integer.parseInt(EShaktiApplication.getLoanAccBalanceAmount())) {
							showConfirmationDialog();
						} else {
							TastyToast.makeText(getActivity(), AppStrings.mCheckDisbursementAlert,
									TastyToast.LENGTH_SHORT, TastyToast.WARNING);
						}
					}

				} else {
					TastyToast.makeText(getActivity(), AppStrings.nullDetailsAlert, TastyToast.LENGTH_SHORT,
							TastyToast.WARNING);

					sSendToServer_InternalLoanDisbursement = Reset.reset(sSendToServer_InternalLoanDisbursement);
					sIncome_Total = 0;

				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

			break;

		case R.id.internal_autoFill:

			String similiar_Income;
			// isValues = true;

			if (mAutoFill.isChecked()) {

				try {

					// Makes all edit fields holds the same savings
					similiar_Income = sIncomeFields.get(0).getText().toString();

					for (int i = 0; i < sIncomeAmounts.length; i++) {
						sIncomeFields.get(i).setText(similiar_Income);
						sIncomeFields.get(i).setGravity(Gravity.RIGHT);
						sIncomeFields.get(i).clearFocus();
						sIncomeAmounts[i] = similiar_Income;

					}

					/** To clear the values of EditFields in case of uncheck **/

				} catch (ArrayIndexOutOfBoundsException e) {
					e.printStackTrace();

					TastyToast.makeText(getActivity(), AppStrings.adminAlert, TastyToast.LENGTH_SHORT,
							TastyToast.WARNING);
				}
			}
			break;

		case R.id.fragment_Edit_button:

			sSendToServer_InternalLoanDisbursement = Reset.reset(sSendToServer_InternalLoanDisbursement);
			sIncome_Total = Integer.valueOf(nullVlaue);
			mSubmit_Raised_Button.setClickable(true);
			isServiceCall = false;
			confirmationDialog.dismiss();
			break;

		case R.id.fragment_Ok_button:

			if (ConnectionUtils.isNetworkAvailable(getActivity())) {
				if (EShaktiApplication.getLoginFlag().equals(Constants.ONLINEFLAG)) {

					if (!isServiceCall) {
						isServiceCall = true;
						if (EShaktiApplication.getLoanDisburseValues() != null) {
							if (EShaktiApplication.getLoanDisburseValues().equals("LOANACC")) {

								new GetAddLoan_Installment_GroupWebservice(
										Transaction_Loan_SB_disbursementFragment.this).execute();

							} else if (EShaktiApplication.getLoanDisburseValues().equals("REPAID")) {

								new GetAddLoan_Installment_CCWebservice(Transaction_Loan_SB_disbursementFragment.this)
										.execute();

							} else {
								new GetAddLoan_Installment_GroupAccWebservice(
										Transaction_Loan_SB_disbursementFragment.this).execute();
							}

						}
					}
				} else {
					TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
							TastyToast.ERROR);
				}
			} else {
				// Do offline Stuffs

			}
			break;

		}
	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub
		mProgressDialog = AppDialogUtils.createProgressDialog(getActivity());
		mProgressDialog.show();
	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub

		try {

			if (mProgressDialog != null) {
				mProgressDialog.dismiss();

				if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {
					getActivity().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub

							if (!result.equals("FAIL")) {
								isServiceCall = false;
								TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg,
										TastyToast.LENGTH_SHORT, TastyToast.ERROR);
								Constants.NETWORKCOMMONFLAG = "SUCESS";
							}

						}
					});
				} else {
					if (isLastTransactionValues) {
						callOfflineDataUpdate();
					} else {
						isLastTransactionValues = true;
						new Get_LastTransactionID(Transaction_Loan_SB_disbursementFragment.this).execute();
					}

				}
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub
		Log.e("Current Class Name!!!!!!!!!!!!!!", fragment.getClass().getName());
		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();
		
		FragmentDrawer.drawer_CashinHand.setText(RegionalConversion.getRegionalConversion(AppStrings.cashinhand)
				+ SelectedGroupsTask.sCashinHand);
		FragmentDrawer.drawer_CashinHand.setTypeface(LoginActivity.sTypeface);

		FragmentDrawer.drawer_CashatBank.setText(RegionalConversion.getRegionalConversion(AppStrings.cashatBank)
				+ SelectedGroupsTask.sCashatBank);
		FragmentDrawer.drawer_CashatBank.setTypeface(LoginActivity.sTypeface);
		
		FragmentDrawer.drawer_lastTR_Date.setText(RegionalConversion.getRegionalConversion(AppStrings.lastTransactionDate)
				+ " : " + SelectedGroupsTask.sLastTransactionDate_Response);
		FragmentDrawer.drawer_lastTR_Date.setTypeface(LoginActivity.sTypeface);
		
		UpdateCalendarMinMaxDateUtils.OnUpdateCalendarDate();
		
		Calendar calender = Calendar.getInstance();

		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String formattedDate = df.format(calender.getTime());
		EShaktiApplication.setLastTransDate_GroupId(SelectedGroupsTask.Group_Id);

		new GroupTempDBLastTransDate(EShaktiApplication.getLastTransDate_GroupId(),
				SelectedGroupsTask.sLastTransactionDate_Response, formattedDate);

		GroupProvider.updateGroupLastTransDateResponse();
		
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		SBus.INST.unRegister(this);
	}

	private void callOfflineDataUpdate() {

		EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GROUP_MASTER_MEMBERLOANOS,
				new GroupMasterSingle(SelectedGroupsTask.Group_Id));

	}

	@Subscribe
	public void OnGroupMasterResponse(final GroupMemberloanOSResponse groupMasterResponse) {
		switch (groupMasterResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), groupMasterResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), groupMasterResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			String mMemberLoanResponse = GetGroupMemberDetails.getMemberloanOS();
			Log.e("Offline Get Values--->>>", mMemberLoanResponse);

			StringBuilder builder_ML = new StringBuilder();
			String mMLID, mMLOSResponse, mMLMasterresponse, mMLresult;

			if (mMemberLoanResponse != null && !mMemberLoanResponse.equals("")) {
				response = mMemberLoanResponse.split("%");

				for (int i = 0; i < Transaction_LoanDisbursement_LoansMenuFragment.mGroupLoanNames.length; i++) {

					String secondResponse[] = response[i].split("#");
					System.out.println("LOAN ID : " + secondResponse[0]);
					if (Transaction_LoanDisbursement_LoansMenuFragment.mSendTo_ServerLoan_Id
							.equals(secondResponse[0])) {

						secondResponse[1] = mMemberOsValues;

					}
					mMLID = secondResponse[0];
					mMLOSResponse = secondResponse[1];
					mMLMasterresponse = mMLID + "#" + mMLOSResponse + "%";
					builder_ML.append(mMLMasterresponse);

				}

				mMLresult = builder_ML.toString();
				Log.e("Current Member loan Outstanding", mMLresult);
				String mCashatBank = SelectedGroupsTask.sCashatBank;
				String mBankDetails = GetOfflineTransactionValues.getBankDetails();
				String mCashinHand = SelectedGroupsTask.sCashinHand;
				mLastTrDate = SelectedGroupsTask.sLastTransactionDate_Response;

				String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mLastTrDate, mCashinHand,
						mCashatBank, mBankDetails);
				String mSelectedGroupId = SelectedGroupsTask.Group_Id;

				Log.e("Get GroupResponse121221", mGroupMasterResponse);
				isServiceCall = false;
				EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GROUP_MASTER_MLOS,
						new GroupMLUpdate(mSelectedGroupId, mGroupMasterResponse, mMLresult));

			} else {
				TastyToast.makeText(getActivity(), AppStrings.loginAgainAlert, TastyToast.LENGTH_LONG,
						TastyToast.WARNING);
			}

			break;
		}

	}

	@Subscribe
	public void OnGroupMLOSUpdate(final GroupMLOSUpdateResponse masterResponse) {
		switch (masterResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), masterResponse.getFetcherResult().getMessage(), TastyToast.LENGTH_SHORT,
					TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), masterResponse.getFetcherResult().getMessage(), TastyToast.LENGTH_SHORT,
					TastyToast.ERROR);
			break;
		case SUCCESS:

			try {

				EShaktiApplication.getInstance().getTransactionManager().startTransaction(
						DataType.GET_LOANACCOUNT_NUMBER, new GroupMasterSingle(SelectedGroupsTask.Group_Id));

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		}
	}

	private void showConfirmationDialog() {
		// TODO Auto-generated method stub
		confirmationDialog = new Dialog(getActivity());

		LayoutInflater inflater = getActivity().getLayoutInflater();
		View dialogView = inflater.inflate(R.layout.dialog_confirmation, null);
		dialogView.setLayoutParams(
				new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

		TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
		confirmationHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.confirmation));
		confirmationHeader.setTypeface(LoginActivity.sTypeface);

		TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

		for (int i = 0; i < confirmArr.length; i++) {

			TableRow indv_SavingsRow = new TableRow(getActivity());

			@SuppressWarnings("deprecation")
			TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);
			contentParams.setMargins(10, 5, 10, 5);

			TextView memberName_Text = new TextView(getActivity());
			memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
					String.valueOf(SelectedGroupsTask.member_Name.elementAt(i))));
			memberName_Text.setTypeface(LoginActivity.sTypeface);
			memberName_Text.setTextColor(color.black);
			memberName_Text.setPadding(5, 5, 5, 5);
			memberName_Text.setLayoutParams(contentParams);
			indv_SavingsRow.addView(memberName_Text);

			TextView confirm_values = new TextView(getActivity());
			confirm_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sIncomeAmounts[i])));
			confirm_values.setTextColor(color.black);
			confirm_values.setPadding(5, 5, 5, 5);
			confirm_values.setGravity(Gravity.RIGHT);
			confirm_values.setLayoutParams(contentParams);
			indv_SavingsRow.addView(confirm_values);

			confirmationTable.addView(indv_SavingsRow,
					new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

		}

		mEdit_RaisedButton = (RaisedButton) dialogView.findViewById(R.id.fragment_Edit_button);
		mEdit_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.edit));
		mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
		mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
																// 205,
																// 0));
		mEdit_RaisedButton.setOnClickListener(this);

		mOk_RaisedButton = (RaisedButton) dialogView.findViewById(R.id.fragment_Ok_button);
		mOk_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.yes));
		mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
		mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
		mOk_RaisedButton.setOnClickListener(this);

		confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		confirmationDialog.setCanceledOnTouchOutside(false);
		confirmationDialog.setContentView(dialogView);
		confirmationDialog.setCancelable(true);
		confirmationDialog.show();

		MarginLayoutParams margin = (MarginLayoutParams) dialogView.getLayoutParams();
		margin.leftMargin = 10;
		margin.rightMargin = 10;
		margin.topMargin = 10;
		margin.bottomMargin = 10;
		margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);

	}

	@Subscribe
	public void OnGroupMasterLoanAccNoDetailsResponse(final GetLoanAccountNoResponse getLoanAccNoResponse) {
		switch (getLoanAccNoResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), getLoanAccNoResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), getLoanAccNoResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:
			String mGetLoanAccNoResponse = GetGroupMemberDetails.getLoanOSDetails();
			Log.e("Offline get LoanAccountNo response ", mGetLoanAccNoResponse + "");

			if (mGetLoanAccNoResponse != null && !mGetLoanAccNoResponse.equals("")) {
				String[] arrValues = mGetLoanAccNoResponse.split("#");

				mLoanBankName.clear();
				mLoanName.clear();
				mLoanId.clear();
				mLoanOutstanding.clear();
				mLoanAccNo.clear();
				String mSelectedGroupId = SelectedGroupsTask.Group_Id;
				for (int i = 0; i < arrValues.length; i++) {
					String ValuesSplit = arrValues[i];

					String[] arrValuesSplit = ValuesSplit.split("~");

					if (arrValuesSplit.length != 0 && arrValuesSplit.length == 5) {
						mLoanBankName.add(arrValuesSplit[0]);
						mLoanName.add(arrValuesSplit[1]);
						mLoanId.add(arrValuesSplit[2]);
						mLoanOutstanding.add(arrValuesSplit[3]);
						mLoanAccNo.add(arrValuesSplit[4]);
					}

				}

				String mTempLoanBankName = null, mTempLoanname = null, mTempLoanId = null, mTempLoanOustanding = null,
						mTempLoanAccNo = null;
				String response = "";
				for (int i = 0; i < mLoanName.size(); i++) {
					mTempLoanBankName = mLoanBankName.get(i).toString();
					mTempLoanname = mLoanName.get(i).toString();
					mTempLoanId = mLoanId.get(i).toString();
					mTempLoanOustanding = mLoanOutstanding.get(i).toString();
					mTempLoanAccNo = mLoanAccNo.get(i).toString();

					if (EShaktiApplication.getLoanId().equals(mLoanId.get(i))) {
						String mLoan_Outstanding = mLoanOutstanding.get(i).toString();
						String mLoan_AccNo = mLoanAccNo.get(i).toString();
						@SuppressWarnings("unused")
						String mLoan_BankName = mLoanBankName.get(i).toString();

						if (EShaktiApplication.getLoanDisburseValues().equals("LOANACC")) {

							int Loan_Outstanding = Integer.parseInt(mLoan_Outstanding)
									+ Integer.parseInt(Transaction_Loan_disburse_LoanAccFragment.disbursementAmount);

							mLoan_Outstanding = String.valueOf(Loan_Outstanding);
							mTempLoanOustanding = mLoan_Outstanding;
						} else if (EShaktiApplication.getLoanDisburseValues().equals("REPAID")) {

							int Loan_Outstanding = Integer.parseInt(mLoan_Outstanding) + Integer
									.parseInt(Transaction_LoanDisbursementFromRepaidFragment.disbursementAmount);

							mLoan_Outstanding = String.valueOf(Loan_Outstanding);
							mTempLoanOustanding = mLoan_Outstanding;

						}

						ToLoanAccNo = mLoan_AccNo;
						outstandingAmt = mLoan_Outstanding;

					}
					response = response + mTempLoanBankName + "~" + mTempLoanname + "~" + mTempLoanId + "~"
							+ mTempLoanOustanding + "~" + mTempLoanAccNo + "#";

				}

				String result = response.toString();
				Log.e("Values------------------------>>>>>>", result);

				if (mCount == 0) {

					mCount++;

					EShaktiApplication.getInstance().getTransactionManager().startTransaction(
							DataType.GET_LOANACCNO_DETAILS_UPDATE,
							new LoanAccountNoDetailsUpdate(mSelectedGroupId, result));
				}

			} else {

				TastyToast.makeText(getActivity(), AppStrings.loginAgainAlert, TastyToast.LENGTH_LONG,
						TastyToast.WARNING);
			}
			break;
		}

	}

	@Subscribe
	public void OnLoanAccNoUpdate(final LoanAccountNoUpdateResponse loanAccNoResponse) {
		switch (loanAccNoResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), loanAccNoResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), loanAccNoResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			confirmationDialog.dismiss();

			if (!ConnectionUtils.isNetworkAvailable(getActivity())) {

				String mValues = Put_DB_GroupNameDetail_Response
						.put_DB_GroupNameDetails_Response(EShaktiApplication.getGroupResponse());

				GroupProvider.updateGroupResponse(
						new GroupResponseUpdate(EShaktiApplication.getGroupId_GroupLastTransDate(), mValues));
			}

			EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GET_GROUPMASTER_GLOS,
					new GroupMasterSingle(SelectedGroupsTask.Group_Id));

			break;
		}
	}

	@Subscribe
	public void OnGroupLoanOSResponse(final GroupLoanOSResponse groupLoanOSResponse) {
		switch (groupLoanOSResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), groupLoanOSResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), groupLoanOSResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:
			String mGroupLoanOSResponse = GetGroupMemberDetails.getGrouploanOS();
			StringBuilder builder = new StringBuilder();

			String mMasterresponse, mGroupLoanOSId = null, mGLOS = null, result;

			String mCashinHand, mGroupLoanOutStanding = null;
			Log.e("Group response@@@@@@@", mGroupLoanOSResponse + "");
			if (mGroupLoanOSResponse != null && !mGroupLoanOSResponse.equals("")) {

				response = mGroupLoanOSResponse.split("%");

				for (int i = 0; i < response.length; i++) {
					System.out.println("Response : " + response[i]);

					String secondResponse[] = response[i].split("#");
					System.out.println("LOAN ID : " + secondResponse[0]);

					mGLOS = secondResponse[1];
					if (Transaction_LoanDisbursement_LoansMenuFragment.mSendTo_ServerLoan_Id
							.equals(secondResponse[0])) {
						System.out.println("LOAN OUTS : " + secondResponse[1]);
						mGroupLoanOutStanding = secondResponse[1];
						mGroupLoanOSId = secondResponse[0];

						if (EShaktiApplication.getLoanDisburseValues().equals("LOANACC")) {
							mGLOS = String.valueOf(Integer.parseInt(mGroupLoanOutStanding)
									+ Integer.parseInt(Transaction_Loan_disburse_LoanAccFragment.disbursementAmount));

						} else if (EShaktiApplication.getLoanDisburseValues().equals("REPAID")) {
							mGLOS = String.valueOf(Integer.parseInt(mGroupLoanOutStanding) + Integer
									.parseInt(Transaction_LoanDisbursementFromRepaidFragment.disbursementAmount));

						}

					} else {
						mGroupLoanOSId = secondResponse[0];
						mGLOS = secondResponse[1];
					}

					mMasterresponse = mGroupLoanOSId + "#" + mGLOS + "%";

					builder.append(mMasterresponse);

				}
				result = builder.toString();
				Log.v("Update Values", result);

				String mCashatBank = SelectedGroupsTask.sCashatBank;
				mCashinHand = SelectedGroupsTask.sCashinHand;
				String mBankDetails = GetOfflineTransactionValues.getBankDetails();
				mLastTrDate = SelectedGroupsTask.sLastTransactionDate_Response;

				String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mLastTrDate, mCashinHand,
						mCashatBank, mBankDetails);
				String mSelectedGroupId = SelectedGroupsTask.Group_Id;

				EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GROUP_MASTER_GLOS,
						new GroupLoanOSUpdate(mSelectedGroupId, mGroupMasterResponse, result));
			} else {
				TastyToast.makeText(getActivity(), AppStrings.loginAgainAlert, TastyToast.LENGTH_LONG,
						TastyToast.WARNING);
			}
			break;
		}

	}

	@Subscribe
	public void OnGroupGLOSUpdate(final GroupLoanOSResponse groupLoanOSResponse) {
		switch (groupLoanOSResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), groupLoanOSResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), groupLoanOSResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			if (ConnectionUtils.isNetworkAvailable(getActivity())) {
				if (confirmationDialog != null) {

					confirmationDialog.dismiss();
				}

				TastyToast.makeText(getActivity(), AppStrings.transactionCompleted, TastyToast.LENGTH_SHORT,
						TastyToast.SUCCESS);

				MainFragment_Dashboard fragment = new MainFragment_Dashboard();
				setFragment(fragment);
			}
			break;
		}
	}

}
