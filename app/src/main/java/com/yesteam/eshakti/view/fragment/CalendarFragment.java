package com.yesteam.eshakti.view.fragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.adapter.CalendarAdapter;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.utils.Reset;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.view.activity.MainActivity;
import com.yesteam.eshakti.views.RaisedButton;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class CalendarFragment extends Fragment implements OnClickListener {

	static final String sTag = CalendarFragment.class.getSimpleName();

	public GregorianCalendar month, itemMonth, test;

	CalendarAdapter cal_Adapter;

	LinearLayout rlayout;
	RelativeLayout calendarTop_Relative;
	ArrayList<String> date;
	ArrayList<String> desc;

	public static String sSelectedFromDate = null, sSelectedToDate = null,
			sSelectedDate = "", sDashboardDate = "", mCurrentDate = null;
	public static String sSend_To_Server_Date = null, sReportsDate = null;
//	public static String sCalendarDate = "";

	String[] separatedTime;
	String regional_MonthName, balanceSheetDate;

	static String sCurrentDate;
	Date date1, date2, date3;
	TextView balnceSheet_DateText, monthTitle;
	TextView selectedDateText;
	Fragment fragment;
	RaisedButton mRaised_Submit_Button;

	public CalendarFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View rootView = inflater.inflate(R.layout.fragment_calendar, container,
				false);

		MainFragment_Dashboard.isBackpressed = false;

		try {

			Locale.setDefault(Locale.US);

			calendarTop_Relative = (RelativeLayout) rootView
					.findViewById(R.id.calendarHeader);
			if (PrefUtils.getUserlangcode() != null) {
				calendarTop_Relative
						.setBackgroundResource(R.drawable.calendar_top_en);
			} else if (PrefUtils.getUserlangcode().equals("English")) {
				calendarTop_Relative
						.setBackgroundResource(R.drawable.calendar_top_en);
			} else if (PrefUtils.getUserlangcode().equals("Tamil")) {
				calendarTop_Relative
						.setBackgroundResource(R.drawable.calendar_top);
			}

			String calResponse[] = String.valueOf(
					SelectedGroupsTask.sBalanceSheetDate_Response).split("/");
			balanceSheetDate = calResponse[0] + "-" + calResponse[1] + "-"
					+ calResponse[2];

			balnceSheet_DateText = (TextView) rootView
					.findViewById(R.id.fragment_Calendar_bsDate);
			balnceSheet_DateText
					.setText(GetSpanText.getSpanString(
							getActivity(),
							String.valueOf(SelectedGroupsTask.sBalanceSheetDate_Response)
									+ "  "
									+ RegionalConversion
											.getRegionalConversion(AppStrings.afterDate)));
			balnceSheet_DateText.setTypeface(LoginActivity.sTypeface);
			balnceSheet_DateText.setTextColor(color.black);

			selectedDateText = (TextView) rootView
					.findViewById(R.id.fragment_UserselectedDate);
			selectedDateText.setText(GetSpanText.getSpanString(getActivity(),
					String.valueOf(AppStrings.selectedDate)) + " : ");
			selectedDateText.setTypeface(LoginActivity.sTypeface);

			mRaised_Submit_Button = (RaisedButton) rootView
					.findViewById(R.id.fragment_Raised_Submitbutton_calender_);
			mRaised_Submit_Button.setText(RegionalConversion
					.getRegionalConversion(AppStrings.submit));
			mRaised_Submit_Button.setTypeface(LoginActivity.sTypeface);
			mRaised_Submit_Button.setOnClickListener(this);

			rlayout = (LinearLayout) rootView.findViewById(R.id.itemLinear);

			month = (GregorianCalendar) GregorianCalendar.getInstance();

			itemMonth = (GregorianCalendar) month.clone();

			cal_Adapter = new CalendarAdapter(getActivity(), month);

			GridView gridView = (GridView) rootView
					.findViewById(R.id.calendarGrid);
			gridView.setAdapter(cal_Adapter);

			monthTitle = (TextView) rootView.findViewById(R.id.monthTitle);

			// Do function call for refreshCalendar
			refreshCalendar(rootView);

			RelativeLayout previousMonth = (RelativeLayout) rootView
					.findViewById(R.id.previous_MonthSelection);
			previousMonth.setOnClickListener(this);

			RelativeLayout nextMonth = (RelativeLayout) rootView
					.findViewById(R.id.next_MonthSelection);
			nextMonth.setOnClickListener(this);

			if (MainActivity.calNavItem.equals("1")
					&& Reports_Trial_BalanceSheetFragment.trial_balance_check
							.equals("")
					&& Meeting_AuditingFragment.audit_check.equals("")
					&& Meeting_TrainingFragment.training_check.equals("")) {
				gridView.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						// TODO Auto-generated method stub

						try {

							String displayDate[] = getGrid_SelctedDate(parent,
									view, position, balanceSheetDate);

							CalendarFragment.sDashboardDate = displayDate[0];
							CalendarFragment.mCurrentDate = displayDate[0];

							CalendarFragment.sSend_To_Server_Date = displayDate[1];

						} catch (Exception e) {
							e.printStackTrace();
						}

					}

				});

			} else if (Reports_Trial_BalanceSheetFragment.trial_balance_check
					.equals("1")
					&& Meeting_AuditingFragment.audit_check.equals("")
					&& Meeting_TrainingFragment.training_check.equals("")) {
				gridView.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						// TODO Auto-generated method stub

						try {

							String displayDate[] = getGrid_SelctedDate(parent,
									view, position, balanceSheetDate);
							sSelectedFromDate = displayDate[0];
							Reports_Trial_BalanceSheetFragment.fromDate = displayDate[1];

						} catch (Exception e) {
							e.printStackTrace();
						}

					}

				});

			} else if (Reports_Trial_BalanceSheetFragment.trial_balance_check
					.equals("2")
					&& Meeting_AuditingFragment.audit_check.equals("")
					&& Meeting_TrainingFragment.training_check.equals("")) {

				gridView.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						// TODO Auto-generated method stub

						try {

							String displayDate[] = getGrid_SelctedDate(parent,
									view, position, sSelectedFromDate);
							Reports_Trial_BalanceSheetFragment.toDate = displayDate[1];
							
						} catch (Exception e) {
							e.printStackTrace();
						}

					}

				});

			} else if (Meeting_AuditingFragment.audit_check.equals("1")
					&& Reports_Trial_BalanceSheetFragment.trial_balance_check
							.equals("")
					&& Meeting_TrainingFragment.training_check.equals("")) {
				gridView.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						// TODO Auto-generated method stub

						try {

							String displayDate[] = getGrid_SelctedDate(parent,
									view, position, balanceSheetDate);
							Meeting_AuditingFragment.calFromDate = displayDate[0];
							
							Meeting_AuditingFragment.auditFromDate = displayDate[1];
							
						} catch (Exception e) {
							e.printStackTrace();
						}
					}

				});
			} else if (Meeting_AuditingFragment.audit_check.equals("2")
					&& Reports_Trial_BalanceSheetFragment.trial_balance_check
							.equals("")
					&& Meeting_TrainingFragment.training_check.equals("")) {

				gridView.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						// TODO Auto-generated method stub
						try {

							String displayDate[] = getGrid_SelctedDate(parent,
									view, position,
									Meeting_AuditingFragment.calFromDate);
							Meeting_AuditingFragment.calToDate = displayDate[0];
							
							Meeting_AuditingFragment.auditToDate = displayDate[1];
							
						} catch (Exception e) {
							e.printStackTrace();
						}

					}

				});

			} else if (Meeting_AuditingFragment.audit_check.equals("3")
					&& Reports_Trial_BalanceSheetFragment.trial_balance_check
							.equals("")
					&& Meeting_TrainingFragment.training_check.equals("")) {

				gridView.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						// TODO Auto-generated method stub

						try {

							String displayDate[] = getGrid_SelctedDate(parent,
									view, position, Meeting_AuditingFragment.calToDate);

							Meeting_AuditingFragment.auditingDate = displayDate[1];
							
						} catch (Exception e) {
							e.printStackTrace();
						}

					}

				});

			} else if (Meeting_TrainingFragment.training_check.equals("1")
					&& Reports_Trial_BalanceSheetFragment.trial_balance_check
							.equals("")
					&& Meeting_AuditingFragment.audit_check.equals("")) {

				gridView.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						// TODO Auto-generated method stub

						try {

							String displayDate[] = getGrid_SelctedDate(parent,
									view, position, balanceSheetDate);

							Meeting_TrainingFragment.trainingDate = displayDate[1];
							
						} catch (Exception e) {
							e.printStackTrace();
						}
					}

				});

			} else {
				gridView.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						// TODO Auto-generated method stub

						try {

							String displayDate[] = getGrid_SelctedDate(parent,
									view, position, balanceSheetDate);

							CalendarFragment.sDashboardDate = displayDate[0];

							MainActivity.mDateView.setText(sDashboardDate);

							CalendarFragment.sSend_To_Server_Date = displayDate[1];

							System.out.println("%%% DATE %%% "
									+ CalendarFragment.sDashboardDate);
							System.out.println("&&&& SERVER DATE &&&& "
									+ CalendarFragment.sSend_To_Server_Date);

						} catch (Exception e) {
							e.printStackTrace();
						}

					}

				});

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return rootView;
	}

	@Override
	public void onAttach(Context context) {
		// TODO Auto-generated method stub
		super.onAttach(context);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {
		case R.id.fragment_Raised_Submitbutton_calender_:

			if (MainActivity.calNavItem.equals("1")
					&& Reports_Trial_BalanceSheetFragment.trial_balance_check
							.equals("")
					&& Meeting_AuditingFragment.audit_check.equals("")
					&& Meeting_TrainingFragment.training_check.equals("")) {

				MainActivity.calNavItem = Reset.reset(MainActivity.calNavItem);

				Intent intent = new Intent(getActivity(), MainActivity.class);
				startActivity(intent);
				getActivity().finish();

				break;

			} else if ((MainFragment_Dashboard.sSubMenu_Item.equals("1"))
					|| (FragmentDrawer.sSubMenu_Item.equals("1"))) {

				fragment = new Transaction_SavingsFragment();

			} else if ((MainFragment_Dashboard.sSubMenu_Item.equals("2"))
					|| (FragmentDrawer.sSubMenu_Item.equals("2"))) {
				fragment = new Transaction_MemLoanRepaid_MenuFragment();
				getActivity()
						.getSupportFragmentManager()
						.beginTransaction()
						.replace(R.id.frame, fragment)
						.setCustomAnimations(R.anim.right_to_left_in, 0, 0,
								R.anim.right_to_left_out).show(fragment)
						.commit();

			} else if ((MainFragment_Dashboard.sSubMenu_Item.equals("3"))
					|| (FragmentDrawer.sSubMenu_Item.equals("3"))) {
				fragment = new Transaction_ExpensesFragment();

			} else if ((MainFragment_Dashboard.sSubMenu_Item.equals("4"))
					|| (FragmentDrawer.sSubMenu_Item.equals("4"))) {
				fragment = new Transaction_IncomeMenuFragment();

			} else if ((MainFragment_Dashboard.sSubMenu_Item.equals("5"))
					|| (FragmentDrawer.sSubMenu_Item.equals("5"))) {

				fragment = new Transaction_GroupLoanRepaidMenuFragment();

			} else if ((MainFragment_Dashboard.sSubMenu_Item.equals("6"))
					|| (FragmentDrawer.sSubMenu_Item.equals("6"))) {

				fragment = new Transaction_BankDepositFragment();

			} else if ((MainFragment_Dashboard.sSubMenu_Item.equals("7"))
					|| (FragmentDrawer.sSubMenu_Item.equals("7"))) {

				fragment = new Transaction_InternalLoan_DisbursementFragment();

			} else if ((MainFragment_Dashboard.sSubMenu_Item.equals("8"))
					|| (FragmentDrawer.sSubMenu_Item.equals("8"))) {
				fragment = new Meeting_AttendanceFragment();

			} else if ((MainFragment_Dashboard.sSubMenu_Item.equals("9"))
					|| (FragmentDrawer.sSubMenu_Item.equals("9"))) {
				fragment = new Meeting_MinutesOfMeetingFragment();

			} else if ((MainFragment_Dashboard.sSubMenu_Item.equals("10"))
					|| (FragmentDrawer.sSubMenu_Item.equals("10"))) {
				fragment = new Meeting_AuditingFragment();

			} else if ((MainFragment_Dashboard.sSubMenu_Item.equals("11"))
					|| (FragmentDrawer.sSubMenu_Item.equals("11"))) {
				fragment = new Meeting_TrainingFragment();

			} else if ((Reports_Trial_BalanceSheetFragment.trial_balance_check
					.equals("1") || Reports_Trial_BalanceSheetFragment.trial_balance_check
					.equals("2"))
					&& (Meeting_AuditingFragment.audit_check.equals(""))
					&& (Meeting_TrainingFragment.training_check.equals(""))) {

				fragment = new Reports_Trial_BalanceSheetFragment();

			} else if ((Meeting_AuditingFragment.audit_check.equals("1")
					|| Meeting_AuditingFragment.audit_check.equals("2") || Meeting_AuditingFragment.audit_check
						.equals("3"))
					&& (Reports_Trial_BalanceSheetFragment.trial_balance_check
							.equals(""))
					&& (Meeting_TrainingFragment.training_check.equals(""))) {

				fragment = new Meeting_AuditingFragment();

			} else if ((Meeting_TrainingFragment.training_check.equals("1"))
					&& (Meeting_AuditingFragment.audit_check.equals(""))
					&& (Reports_Trial_BalanceSheetFragment.trial_balance_check
							.equals(""))) {

				fragment = new Meeting_TrainingFragment();

			}

			if ((MainActivity.calNavItem.equals(""))
					&& ((!MainFragment_Dashboard.sSubMenu_Item.equals(""))
							|| (!FragmentDrawer.sSubMenu_Item.equals(""))
							|| (!Reports_Trial_BalanceSheetFragment.trial_balance_check
									.equals(""))
							|| (!Meeting_AuditingFragment.audit_check.equals("")) || (!Meeting_TrainingFragment.training_check
								.equals("")))) {

				Reports_Trial_BalanceSheetFragment.trial_balance_check = "";
				Meeting_AuditingFragment.audit_check = "";
				Meeting_TrainingFragment.training_check = "";
				MainFragment_Dashboard.sSubMenu_Item = "";
				FragmentDrawer.sSubMenu_Item = "";

				getActivity()
						.getSupportFragmentManager()
						.beginTransaction()
						.replace(R.id.frame, fragment)
						.setCustomAnimations(R.anim.right_to_left_in, 0, 0,
								R.anim.right_to_left_out).show(fragment)
						.addToBackStack(null).commit();

			}

			break;

		case R.id.previous_MonthSelection:

			setPreviousMonth();
			refreshCalendar(v);

			break;

		case R.id.next_MonthSelection:

			setNextMonth();
			refreshCalendar(v);

			break;

		default:
			break;
		}

	}

	protected void setNextMonth() {
		// TODO Auto-generated method stub
		if (month.get(GregorianCalendar.MONTH) == month
				.getActualMaximum(GregorianCalendar.MONTH)) {
			month.set((month.get(GregorianCalendar.YEAR) + 1),
					month.getActualMinimum(GregorianCalendar.MONTH), 1);
		} else {
			month.set(GregorianCalendar.MONTH,
					month.get(GregorianCalendar.MONTH) + 1);
		}
	}

	protected void setPreviousMonth() {
		// TODO Auto-generated method stub

		if (month.get(GregorianCalendar.MONTH) == month
				.getActualMinimum(GregorianCalendar.MONTH)) {
			month.set(month.get(GregorianCalendar.YEAR) - 1,
					month.getActualMaximum(GregorianCalendar.MONTH), 1);
		} else {
			month.set(GregorianCalendar.MONTH,
					month.get(GregorianCalendar.MONTH) - 1);
		}

	}

	public void refreshCalendar(View view) {

		// String MonthItem = "";

		try {

			cal_Adapter.refreshDays();
			cal_Adapter.notifyDataSetChanged();

			String monthText = (String) android.text.format.DateFormat.format(
					"MMMM yyyy", month);

			String[] items = monthText.split(" ");

			if (items[0].equals("January")) {
				regional_MonthName = AppStrings.January;
			} else if (items[0].equals("February")) {
				regional_MonthName = AppStrings.February;
			} else if (items[0].equals("March")) {
				regional_MonthName = AppStrings.March;
			} else if (items[0].equals("April")) {
				regional_MonthName = AppStrings.April;
			} else if (items[0].equals("May")) {
				regional_MonthName = AppStrings.May;
			} else if (items[0].equals("June")) {
				regional_MonthName = AppStrings.June;
			} else if (items[0].equals("July")) {
				regional_MonthName = AppStrings.July;
			} else if (items[0].equals("August")) {
				regional_MonthName = AppStrings.August;
			} else if (items[0].equals("September")) {
				regional_MonthName = AppStrings.September;
			} else if (items[0].equals("October")) {
				regional_MonthName = AppStrings.October;
			} else if (items[0].equals("November")) {
				regional_MonthName = AppStrings.November;
			} else if (items[0].equals("December")) {
				regional_MonthName = AppStrings.December;
			}

			Log.d("Moth & Year ", regional_MonthName + "  " + items[1]);

			monthTitle.setText(GetSpanText.getSpanString(getActivity(),
					String.valueOf(regional_MonthName + " " + items[1])));
			monthTitle.setTypeface(LoginActivity.sTypeface);
			monthTitle.setTextColor(color.black);

		} catch (Exception e) {

		}
		// return MonthItem;
	}

	private String[] getGrid_SelctedDate(AdapterView<?> parent, View view,
			int position, String bDate) {

		String dateSelected[] = new String[2];

		try {

			// remove the previous view if exists
			if (((LinearLayout) rlayout).getChildCount() > 0) {
				((LinearLayout) rlayout).removeAllViews();
			}

			desc = new ArrayList<String>();
			date = new ArrayList<String>();

			((CalendarAdapter) parent.getAdapter()).setSelected(view);

			String selectedGridDate = CalendarAdapter.dayString.get(position);

			separatedTime = selectedGridDate.split("-");

			/** dd/mm/yyyy ***/
			sSelectedDate = separatedTime[2] + "-" + separatedTime[1] + "-"
					+ separatedTime[0];

			/** mm/dd/yyyy **/
			sReportsDate = separatedTime[1] + "/" + separatedTime[2] + "/"
					+ separatedTime[0];

			balnceSheet_DateText.setTextColor(color.black);

			selectedDateText.setText(RegionalConversion
					.getRegionalConversion(AppStrings.selectedDate)
					+ " : "
					+ sSelectedDate);
			selectedDateText.setTypeface(LoginActivity.sTypeface);
			selectedDateText.setTextColor(Color.GREEN);

			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

			/** User Selected Date **/
			try {
				date1 = sdf.parse(sSelectedDate);
			} catch (ParseException e) {
				// TODO: handle exception
				e.printStackTrace();
			}

			/** BalanceSheetDate **/
			try {
				date2 = sdf.parse(bDate);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			/** Current Date **/
			try {

				String calCurrentDate[] = CalendarAdapter.curentDateString
						.split("-");

				sCurrentDate = calCurrentDate[2] + "-" + calCurrentDate[1]
						+ "-" + calCurrentDate[0];
				Log.v("CURRENT DATE", sCurrentDate);
				date3 = sdf.parse(sCurrentDate);

			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			/**
			 * Compares the date of SelectedDate with BalanceSheetDate &
			 * CurrentDate
			 **/

			if ((date1.compareTo(date2)) >= 0 && (date1.compareTo(date3) <= 0)) {

			} else {

				mRaised_Submit_Button.setVisibility(View.GONE);
				balnceSheet_DateText.setTextColor(Color.RED);

				TastyToast.makeText(getActivity(), AppStrings.calAlert, TastyToast.LENGTH_SHORT,
						TastyToast.WARNING);
				sSelectedDate = "";
				selectedGridDate = "";

				// Fragment Refresh

				Fragment currentFragment = getFragmentManager()
						.findFragmentById(R.id.frame);
				if (currentFragment instanceof CalendarFragment) {
					FragmentTransaction fragTransaction = getFragmentManager()
							.beginTransaction();
					fragTransaction.detach(currentFragment);
					fragTransaction.attach(currentFragment);
					fragTransaction.commit();
				}
			}

			String gridValueStr = separatedTime[2].replaceFirst("^0*", ""); // Considering
																			// only
																			// the
																			// dd
																			// from
																			// yyyy/mm/dd

			int gridValue = Integer.parseInt(gridValueStr);// navigate
															// to
															// next
															// or
															// previous
															// month
															// by
															// clicking
															// the
															// offDays.
			if ((gridValue > 10) && (position < 8)) {
				setPreviousMonth();
				refreshCalendar(view);
			} else if ((gridValue < 7) && (position > 28)) {
				setNextMonth();
				refreshCalendar(view);
			}

			((CalendarAdapter) parent.getAdapter()).setSelected(view);
			desc = null;

			dateSelected[0] = sSelectedDate;
			dateSelected[1] = sReportsDate;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return dateSelected;
	}

}
