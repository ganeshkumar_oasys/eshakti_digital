package com.yesteam.eshakti.view.fragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.squareup.otto.Subscribe;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.adapter.CustomItemAdapter;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.TransactionOffConstants;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;
import com.yesteam.eshakti.model.RowItem;
import com.yesteam.eshakti.sqlite.database.GroupProvider;
import com.yesteam.eshakti.sqlite.database.TransactionProvider;
import com.yesteam.eshakti.sqlite.database.response.GetLoanAccountNoResponse;
import com.yesteam.eshakti.sqlite.database.response.GetSingleTransResponse;
import com.yesteam.eshakti.sqlite.database.response.GroupLoanOSResponse;
import com.yesteam.eshakti.sqlite.database.response.GroupMasUpdateResponse;
import com.yesteam.eshakti.sqlite.database.response.LoanAccountNoUpdateResponse;
import com.yesteam.eshakti.sqlite.database.response.TransactionResponse;
import com.yesteam.eshakti.sqlite.database.response.TransactionUpdateResponse;
import com.yesteam.eshakti.sqlite.db.model.GetGroupMemberDetails;
import com.yesteam.eshakti.sqlite.db.model.GetTransactionSingle;
import com.yesteam.eshakti.sqlite.db.model.GetTransactionSinglevalues;
import com.yesteam.eshakti.sqlite.db.model.GroupDetailsUpdate;
import com.yesteam.eshakti.sqlite.db.model.GroupLoanOSUpdate;
import com.yesteam.eshakti.sqlite.db.model.GroupMasterSingle;
import com.yesteam.eshakti.sqlite.db.model.GroupResponseUpdate;
import com.yesteam.eshakti.sqlite.db.model.GroupTempDBLastTransDate;
import com.yesteam.eshakti.sqlite.db.model.LoanAccountNoDetailsUpdate;
import com.yesteam.eshakti.sqlite.db.model.Transaction;
import com.yesteam.eshakti.sqlite.db.model.TransactionUpdate;
import com.yesteam.eshakti.sqlite.db.model.Transaction_UniqueIdSingle;
import com.yesteam.eshakti.sqlite.db.transactions.TransactionManager.DataType;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.utils.GetOfflineTransactionValues;
import com.yesteam.eshakti.utils.GetResponseInfo;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.utils.Get_Offline_MobileDate;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.utils.Put_DB_GroupNameDetail_Response;
import com.yesteam.eshakti.utils.Put_DB_GroupResponse;
import com.yesteam.eshakti.utils.Reset;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.utils.UpdateCalendarMinMaxDateUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.views.MaterialSpinner;
import com.yesteam.eshakti.views.RaisedButton;
import com.yesteam.eshakti.webservices.Get_Account_To_AccountTransferWebservice;
import com.yesteam.eshakti.webservices.Get_Group_loan_repaidFromSBWebservice;
import com.yesteam.eshakti.webservices.Get_LastTransactionID;
import com.yesteam.eshakti.webservices.Get_Loan_Account_Number_Webservice;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Transaction_AcctoAccTransferFragment extends Fragment implements TaskListener, OnClickListener {
	private TextView mGroupName, mCashInHand, mCashAtBank;

	View rootView;
	private Dialog mProgressDilaog;
	TextView mAcctoaccheader, mAcctoaccFrombank, mAcctoaccFromBankEdittext, mAcctoaccTransferAmountText,
			mAcctoaccTransferChargesText;
	EditText mAcctoacctransferamount, mAcctoacctransfercharges;
	RaisedButton mSubmitButton;
	MaterialSpinner mSpinner_tobank, mSpinner_loanAcc;
	CustomItemAdapter bankNameAdapter, loanAccAdapter;

	private List<RowItem> bankNameItems, loanAccItems;
	public static String selectedItemBank, selectedLoanAcc, selectedLoanId;
	public static String mBankNameValue = null, mBankName = "", mTempBankId = "", mLoanAccValue = null,
			mLoanAccIdValue = null;
	public static String mAcctransferAmount, mAcctransferCharge;
	private Dialog confirmationDialog;
	private Button mEdit_RaisedButton, mOk_RaisedButton;
	ArrayList<String> mBanknames_Array = new ArrayList<String>();
	ArrayList<String> mBanknamesId_Array = new ArrayList<String>();

	ArrayList<String> mEngSendtoServerBank_Array = new ArrayList<String>();
	ArrayList<String> mEngSendtoServerBankId_Array = new ArrayList<String>();

	String mLastTrDate = null, mLastTr_ID = null;

	public static int mFromBankAmount = 0, mToBankAmount = 0;
	boolean isGetTrid = false;
	private LinearLayout mSavingsAccLayout, mLoanAccLayout;
	private RadioButton mSavingsAccRadio, mLoanAccRadio;
	private RadioGroup mRadioGroup;
	private TextView mLoanOutstandingTextView, mBankNameTextView, mLoanAccNoTextView, mAccNoTextView;
	private TextView mLoanOutstanding_value_TextView, mBankName_value_TextView, mLoanAccNo_value_TextView,
			mAccNo_ValeTextView;
	private String[] mLoanTypeArray, mLoanIdArray;
	boolean isLoanAccount = false;
	boolean isSavingAccount = false;
	boolean isOfflineLoanAcc = false;
	boolean isLoanAccDetails = false;
	String selectedRadio = "";
	String ToLoanAccNo = "", outstandingAmt = "";
	ArrayList<String> mLoanBankName = new ArrayList<String>();
	ArrayList<String> mLoanName = new ArrayList<String>();
	ArrayList<String> mLoanId = new ArrayList<String>();
	ArrayList<String> mLoanOutstanding = new ArrayList<String>();
	ArrayList<String> mLoanAccNo = new ArrayList<String>();
	boolean isServiceCall = false;
	boolean isOfflineEntry = false;

	int mCount = 0;

	String mSqliteDBStoredValues_acc_toacc_Values = null;
	Date date_dashboard, date_loanDisb;

	public Transaction_AcctoAccTransferFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		SBus.INST.register(this);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_ACCTOACCTRANSFER;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		try {
			mBankName = "";
			mAcctransferAmount = Reset.reset(mAcctransferAmount);
			mAcctransferCharge = Reset.reset(mAcctransferCharge);
			mBankNameValue = Reset.reset(mBankNameValue);
			mTempBankId = "";
			selectedLoanAcc = Reset.reset(selectedLoanAcc);
			selectedLoanId = Reset.reset(selectedLoanId);
			mLoanAccValue = Reset.reset(mLoanAccValue);
			mLoanAccIdValue = Reset.reset(mLoanAccIdValue);
			selectedRadio = Reset.reset(selectedRadio);
			ToLoanAccNo = Reset.reset(ToLoanAccNo);
			outstandingAmt = Reset.reset(outstandingAmt);
			isOfflineLoanAcc = false;
			isLoanAccDetails = false;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		rootView = inflater.inflate(R.layout.fragment_acc_to_acc_transfer, container, false);

		MainFragment_Dashboard.isBackpressed = false;
		Constants.FRAG_BACKPRESS_CONSTANT = Constants.FRAG_INSTANCE_ACCTOACCTRANSFER;
		mCount = 0;

		mSqliteDBStoredValues_acc_toacc_Values = null;

		try {
			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashInHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashInHand.setTypeface(LoginActivity.sTypeface);

			mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashAtBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashAtBank.setTypeface(LoginActivity.sTypeface);
			init();
			mAcctoaccheader.setTypeface(LoginActivity.sTypeface);
			mAcctoaccFrombank.setTypeface(LoginActivity.sTypeface);
			mAcctoaccFromBankEdittext.setTypeface(LoginActivity.sTypeface);
			mAcctoacctransferamount.setTypeface(LoginActivity.sTypeface);
			mAcctoacctransfercharges.setTypeface(LoginActivity.sTypeface);
			mSubmitButton.setTypeface(LoginActivity.sTypeface);
			mSubmitButton.setText(RegionalConversion.getRegionalConversion(AppStrings.submit));
			mAcctoaccheader.setText(AppStrings.mAccountToAccountTransfer);
			mAcctoaccFrombank.setText(AppStrings.mTransferFromBank);
			// mAcctoacctransferamount.setHint(AppStrings.mTransferAmount);
			// mAcctoacctransfercharges.setHint(AppStrings.mTransferCharges);

			// mAcctoaccFromBankEdittext.setEnabled(false);
			mAcctoaccFromBankEdittext.setText(EShaktiApplication.getAcctoaccSelectBank() + "  :  "
					+ String.valueOf(Transaction_BankDepositFragment.sSelected_BankDepositAmount));

			mAcctoaccTransferAmountText.setText(AppStrings.mTransferAmount);
			mAcctoaccTransferAmountText.setTypeface(LoginActivity.sTypeface);

			mAcctoaccTransferChargesText.setText(AppStrings.mTransferCharges);
			mAcctoaccTransferChargesText.setTypeface(LoginActivity.sTypeface);

			mSavingsAccRadio.setText(RegionalConversion.getRegionalConversion(AppStrings.mSavingsAccount));
			mSavingsAccRadio.setTypeface(LoginActivity.sTypeface);
			mLoanAccRadio.setText(RegionalConversion.getRegionalConversion(AppStrings.mLoanaccHeader));
			mLoanAccRadio.setTypeface(LoginActivity.sTypeface);

			mSpinner_tobank.setBaseColor(color.grey_400);

			mSpinner_tobank.setFloatingLabelText(AppStrings.mTransferSpinnerFloating);

			mSpinner_tobank.setPaddingSafe(10, 0, 10, 0);

			for (int i = 0; i < SelectedGroupsTask.sBankNames.size(); i++) {
				if (!EShaktiApplication.getAcctoaccSendtoserverBank()
						.equals(SelectedGroupsTask.sEngBankNames.elementAt(i))) {

					mBankName = mBankName + SelectedGroupsTask.sBankNames.elementAt(i).toString() + "~";

				}
			}

			for (int i = 0; i < SelectedGroupsTask.sBankNames.size(); i++) {
				if (!EShaktiApplication.getAcctoaccSendtoserverBank()
						.equals(SelectedGroupsTask.sEngBankNames.elementAt(i))) {

					mTempBankId = mTempBankId + SelectedGroupsTask.sEngBankNames.elementAt(i).toString() + "~";

				}

			}

			Log.e("Temp Bank ID  Values____----->>>>", mTempBankId);

			Log.e("Send to acc Bank-->>>", EShaktiApplication.getAcctoaccSendtoserverBank());
			for (int i = 0; i < SelectedGroupsTask.sBankNames.size(); i++) {
				if (EShaktiApplication.getAcctoaccSendtoserverBank()
						.equals(SelectedGroupsTask.sEngBankNames.elementAt(i))) {
					mFromBankAmount = Integer.parseInt(SelectedGroupsTask.sBankAmt.elementAt(i));
				}
			}
			for (int i = 0; i < SelectedGroupsTask.sBankNames.size(); i++) {
				mBanknames_Array.add(SelectedGroupsTask.sBankNames.elementAt(i).toString());
				mBanknamesId_Array.add(String.valueOf(i));
			}

			for (int i = 0; i < SelectedGroupsTask.sEngBankNames.size(); i++) {
				mEngSendtoServerBank_Array.add(SelectedGroupsTask.sEngBankNames.elementAt(i).toString());
				mEngSendtoServerBankId_Array.add(String.valueOf(i));
			}

			String[] tempBankName = mBankName.split("~");

			String[] tempBankName_ID = mTempBankId.split("~");

			final String[] bankNames = new String[tempBankName.length + 1];
			bankNames[0] = String.valueOf("Select Bank Name");
			for (int i = 0; i < tempBankName.length; i++) {
				bankNames[i + 1] = tempBankName[i].toString();
				System.out.println("-----------------------" + bankNames[i + 1]);
			}

			final String[] bankNames_Id = new String[tempBankName_ID.length + 1];
			bankNames_Id[0] = String.valueOf("Select Bank Name");
			for (int i = 0; i < tempBankName_ID.length; i++) {
				bankNames_Id[i + 1] = tempBankName_ID[i].toString();
				System.out.println("-----------------------" + bankNames_Id[i + 1]);
			}

			int size = bankNames.length;

			bankNameItems = new ArrayList<RowItem>();
			for (int i = 0; i < size; i++) {
				RowItem rowItem = new RowItem(bankNames[i]);// SelectedGroupsTask.sBankNames.elementAt(i).toString());
				bankNameItems.add(rowItem);
			}
			bankNameAdapter = new CustomItemAdapter(getActivity(), bankNameItems);
			mSpinner_tobank.setAdapter(bankNameAdapter);

			mSpinner_tobank.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
					// TODO Auto-generated method stub

					if (position == 0) {
						selectedItemBank = bankNames_Id[0];
						mBankNameValue = "0";

					} else {
						selectedItemBank = bankNames_Id[position];
						System.out.println("SELECTED BANK NAME : " + selectedItemBank);
						String mBankname = null;
						for (int i = 0; i < mBanknames_Array.size(); i++) {
							if (selectedItemBank.equals(mEngSendtoServerBank_Array.get(i))) {
								mBankname = mEngSendtoServerBank_Array.get(i);
							}
						}

						mBankNameValue = mBankname;

						Log.e("Bank Name In English Name--->>>", mBankNameValue);

						for (int i = 0; i < SelectedGroupsTask.sBankNames.size(); i++) {
							if (mBankNameValue.equals(SelectedGroupsTask.sEngBankNames.elementAt(i))) {
								mToBankAmount = Integer.parseInt(SelectedGroupsTask.sBankAmt.elementAt(i));
							}
						}

					}

				}

				@Override
				public void onNothingSelected(AdapterView<?> parent) {
					// TODO Auto-generated method stub

				}
			});
			mSubmitButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					System.out.println("------  " + selectedRadio.toString());

					// TODO Auto-generated method stub

					mAcctransferAmount = Reset.reset(mAcctransferAmount);
					mAcctransferCharge = Reset.reset(mAcctransferCharge);

					mAcctransferAmount = mAcctoacctransferamount.getText().toString().trim();
					mAcctransferCharge = mAcctoacctransfercharges.getText().toString().trim();

					if (mAcctransferAmount.equals("")) {
						mAcctransferAmount = "0";
					}

					if (mAcctransferCharge.equals("")) {
						mAcctransferCharge = "0";
					}

					int totalAmount = Integer.parseInt(mAcctransferAmount) + Integer.parseInt(mAcctransferCharge);

					if (!mAcctransferAmount.isEmpty() && !mAcctransferAmount.equals("0")) {

						if (!selectedRadio.equals("")) {

							Log.e("Selected Radio Values --->>>", selectedRadio);

							if (selectedRadio.equals("LOANACCOUNT")) {

								// String dashBoardDate = DatePickerDialog.sDashboardDate;
								String dashBoardDate = null;
								if (DatePickerDialog.sDashboardDate.contains("-")) {
									dashBoardDate = DatePickerDialog.sDashboardDate;
								} else if (DatePickerDialog.sDashboardDate.contains("/")) {
									String[] dateArr = DatePickerDialog.sDashboardDate.split("/");
									dashBoardDate = dateArr[0] + "-" + dateArr[1] + "-" + dateArr[2];
									;
								}
								String loanDisbArr[] = EShaktiApplication.getLoanDisbursementDate().split("/");
								String loanDisbDate = loanDisbArr[1] + "-" + loanDisbArr[0] + "-" + loanDisbArr[2];

								SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

								try {
									date_dashboard = sdf.parse(dashBoardDate);
									date_loanDisb = sdf.parse(loanDisbDate);
								} catch (ParseException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}

								if (date_dashboard.compareTo(date_loanDisb) >= 0) {
									if (!mLoanAccValue.equals("0")) {
										if (totalAmount <= Integer
												.parseInt(Transaction_BankDepositFragment.sSelected_BankDepositAmount)
												&& totalAmount <= Integer.parseInt(outstandingAmt)) {
											onShowConfirmationDialog();
										} else {
											if (totalAmount > Integer.parseInt(
													Transaction_BankDepositFragment.sSelected_BankDepositAmount)) {

												TastyToast.makeText(getActivity(), AppStrings.cashatBankAlert,
														TastyToast.LENGTH_SHORT, TastyToast.WARNING);
											} else if (Integer.parseInt(mAcctransferAmount) > Integer
													.parseInt(outstandingAmt)) {
												TastyToast.makeText(getActivity(), AppStrings.mOutstandingAlert,
														TastyToast.LENGTH_SHORT, TastyToast.WARNING);
											}
											mAcctransferAmount = Reset.reset(mAcctransferAmount);
											mAcctransferCharge = Reset.reset(mAcctransferCharge);
										}

									} else {
										TastyToast.makeText(getActivity(), AppStrings.mLoanTypeAlert,
												TastyToast.LENGTH_SHORT, TastyToast.WARNING);
										mAcctransferAmount = Reset.reset(mAcctransferAmount);
										mAcctransferCharge = Reset.reset(mAcctransferCharge);
									}

								} else {
									TastyToast.makeText(getActivity(), AppStrings.mCheckDisbursementDate,
											TastyToast.LENGTH_SHORT, TastyToast.WARNING);
								}

							} else {
								if ((!mBankNameValue.equals("0") && mBankNameValue != null)) {

									if (totalAmount <= Integer
											.parseInt(Transaction_BankDepositFragment.sSelected_BankDepositAmount)) {

										onShowConfirmationDialog();

									} else {
										TastyToast.makeText(getActivity(), AppStrings.cashatBankAlert,
												TastyToast.LENGTH_SHORT, TastyToast.WARNING);
										mAcctransferAmount = Reset.reset(mAcctransferAmount);
										mAcctransferCharge = Reset.reset(mAcctransferCharge);
									}
								} else {

									TastyToast.makeText(getActivity(), AppStrings.mLoanaccBankNullToast,
											TastyToast.LENGTH_SHORT, TastyToast.WARNING);

								}
							}

						} else {

							if (mBankNameValue.equals("0")) {
								TastyToast.makeText(getActivity(), AppStrings.mLoanaccBankNullToast,
										TastyToast.LENGTH_SHORT, TastyToast.WARNING);
							} else {
								TastyToast.makeText(getActivity(), AppStrings.mTransferNullToast,
										TastyToast.LENGTH_SHORT, TastyToast.WARNING);
							}
							mAcctransferAmount = Reset.reset(mAcctransferAmount);
							mAcctransferCharge = Reset.reset(mAcctransferCharge);
						}
					} else {
						TastyToast.makeText(getActivity(), AppStrings.mTransferNullToast, TastyToast.LENGTH_SHORT,
								TastyToast.WARNING);
						mAcctransferAmount = Reset.reset(mAcctransferAmount);
						mAcctransferCharge = Reset.reset(mAcctransferCharge);
					}
				}
			});

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return rootView;
	}

	private void init() {
		mAcctoaccheader = (TextView) rootView.findViewById(R.id.acctoaccheader);
		mAcctoaccFrombank = (TextView) rootView.findViewById(R.id.acctoaccfrombanktext);
		mAcctoaccFromBankEdittext = (TextView) rootView.findViewById(R.id.acctoaccfrombank);
		mAcctoacctransferamount = (EditText) rootView.findViewById(R.id.acctoacctransferamount);
		mAcctoacctransfercharges = (EditText) rootView.findViewById(R.id.acctoacctransfercharges);
		mSubmitButton = (RaisedButton) rootView.findViewById(R.id.acctoacc_submit);
		mSpinner_tobank = (MaterialSpinner) rootView.findViewById(R.id.acctoacctobankspinner);
		mAcctoaccTransferAmountText = (TextView) rootView.findViewById(R.id.acctoacctransferamountTextView);
		mAcctoaccTransferChargesText = (TextView) rootView.findViewById(R.id.acctoacctransferchargesTextView);

		mAccNoTextView = (TextView) rootView.findViewById(R.id.accNotext);
		mAccNoTextView.setText(RegionalConversion.getRegionalConversion(AppStrings.mAccountNumber));
		mAccNoTextView.setTypeface(LoginActivity.sTypeface);

		mAccNo_ValeTextView = (TextView) rootView.findViewById(R.id.accNo_value);
		// if (ConnectionUtils.isNetworkAvailable(getActivity())) {
		if (!publicValues.mGetSbAccountNumber.isEmpty()) {
			mAccNo_ValeTextView.setText(publicValues.mGetSbAccountNumber);
		}
		// }
		mAccNo_ValeTextView.setTypeface(LoginActivity.sTypeface);

		mSavingsAccLayout = (LinearLayout) rootView.findViewById(R.id.acctoacctobankspinnerlayout);
		mLoanAccLayout = (LinearLayout) rootView.findViewById(R.id.loanAccTransactionspinnerLayout);

		mRadioGroup = (RadioGroup) rootView.findViewById(R.id.radio_loanAccTransaction);
		mSavingsAccRadio = (RadioButton) rootView.findViewById(R.id.radioloanaccTransaction_SavingsAcc);
		mLoanAccRadio = (RadioButton) rootView.findViewById(R.id.radioloanaccTransaction_LoanAcc);

		if (SelectedGroupsTask.sBankNames.size() <= 1) {
			mSavingsAccRadio.setClickable(false);
		}

		mLoanOutstandingTextView = (TextView) rootView.findViewById(R.id.loanAccTransaction_outstandingTextView);
		mLoanOutstandingTextView.setTypeface(LoginActivity.sTypeface);

		mLoanOutstanding_value_TextView = (TextView) rootView.findViewById(R.id.loanAccTransaction_outstanding_value);
		mLoanOutstanding_value_TextView.setText("");
		mLoanOutstanding_value_TextView.setTypeface(LoginActivity.sTypeface);

		mBankNameTextView = (TextView) rootView.findViewById(R.id.loanAccTransaction_BankNameTextView);
		mBankNameTextView.setTypeface(LoginActivity.sTypeface);

		mBankName_value_TextView = (TextView) rootView.findViewById(R.id.loanAccTransaction_BankName_value);
		mBankName_value_TextView.setText("");
		mBankName_value_TextView.setTypeface(LoginActivity.sTypeface);

		mLoanAccNoTextView = (TextView) rootView.findViewById(R.id.loanAccTransaction_accNoTextView);
		mLoanAccNoTextView.setTypeface(LoginActivity.sTypeface);

		mLoanAccNo_value_TextView = (TextView) rootView.findViewById(R.id.loanAccTransaction_accNo_value);
		mLoanAccNo_value_TextView.setText("");
		mLoanAccNo_value_TextView.setTypeface(LoginActivity.sTypeface);

		mLoanOutstandingTextView.setText(RegionalConversion.getRegionalConversion(AppStrings.outstanding));
		mBankNameTextView.setText(RegionalConversion.getRegionalConversion(AppStrings.bankName));
		mLoanAccNoTextView.setText(RegionalConversion.getRegionalConversion(AppStrings.mAccountNumber));

		mSpinner_loanAcc = (MaterialSpinner) rootView.findViewById(R.id.loanAccTransactionspinner);
		mSpinner_loanAcc.setBaseColor(color.grey_400);
		mSpinner_loanAcc.setFloatingLabelText(AppStrings.mLoanAccType);
		mSpinner_loanAcc.setPaddingSafe(10, 0, 10, 0);

		mRadioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				int selectedId = group.getCheckedRadioButtonId();
				mAcctoacctransferamount.setText("");
				mAcctoacctransfercharges.setText("");
				if (selectedId == R.id.radioloanaccTransaction_SavingsAcc) {
					System.out.println("-------Savings_acc----");
					selectedRadio = "SAVINGSACCOUNT";

					if (SelectedGroupsTask.sBankNames.size() <= 1) {
						TastyToast.makeText(getActivity(), AppStrings.mAccToAccTransferToast, TastyToast.LENGTH_SHORT,
								TastyToast.WARNING);
						mSubmitButton.setClickable(false);

					} else {
						mSavingsAccLayout.setVisibility(View.VISIBLE);
						mLoanAccLayout.setVisibility(View.GONE);
						mSubmitButton.setClickable(true);
					}
				} else if (selectedId == R.id.radioloanaccTransaction_LoanAcc) {
					System.out.println("-------Loan_acc----");

					if (SelectedGroupsTask.loan_Id.size() > 1) {
						selectedRadio = "LOANACCOUNT";
						mLoanAccLayout.setVisibility(View.VISIBLE);
						mSavingsAccLayout.setVisibility(View.GONE);

						mLoanTypeArray = new String[SelectedGroupsTask.loan_Name.size()];
						mLoanTypeArray[0] = String.valueOf(AppStrings.mLoanAccType);

						mLoanIdArray = new String[SelectedGroupsTask.loan_Id.size() - 1];
						for (int i = 0; i < SelectedGroupsTask.loan_Name.size() - 1; i++) {
							mLoanTypeArray[i + 1] = SelectedGroupsTask.loan_Name.elementAt(i).toString();
							mLoanIdArray[i] = SelectedGroupsTask.loan_Id.elementAt(i).toString();
						}

						loanAccItems = new ArrayList<RowItem>();
						for (int i = 0; i < mLoanTypeArray.length; i++) {
							RowItem rowItem = new RowItem(mLoanTypeArray[i]);
							loanAccItems.add(rowItem);
						}
						loanAccAdapter = new CustomItemAdapter(getActivity(), loanAccItems);
						mSpinner_loanAcc.setAdapter(loanAccAdapter);
						mSpinner_loanAcc.setOnItemSelectedListener(new OnItemSelectedListener() {

							@Override
							public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
								// TODO Auto-generated method stub

								if (position == 0) {
									selectedLoanAcc = mLoanTypeArray[0];
									mLoanAccValue = "0";
									mLoanAccIdValue = "0";

									mLoanOutstanding_value_TextView.setText("");
									mBankName_value_TextView.setText("");
									mLoanAccNo_value_TextView.setText("");

								} else {
									// String dashBoardDate = DatePickerDialog.sDashboardDate;
									String dashBoardDate = null;
									if (DatePickerDialog.sDashboardDate.contains("-")) {
										dashBoardDate = DatePickerDialog.sDashboardDate;
									} else if (DatePickerDialog.sDashboardDate.contains("/")) {
										String[] dateArr = DatePickerDialog.sDashboardDate.split("/");
										dashBoardDate = dateArr[0] + "-" + dateArr[1] + "-" + dateArr[2];
										;
									}
									String loanDisbArr[] = SelectedGroupsTask.loanAcc_loanDisbursementDate
											.elementAt(position - 1).split("/");
									EShaktiApplication.setLoanDisbursementDate(
											SelectedGroupsTask.loanAcc_loanDisbursementDate.elementAt(position - 1));
									String loanDisbDate = loanDisbArr[1] + "-" + loanDisbArr[0] + "-" + loanDisbArr[2];

									SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

									try {
										date_dashboard = sdf.parse(dashBoardDate);
										date_loanDisb = sdf.parse(loanDisbDate);
									} catch (ParseException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}

									if (date_dashboard.compareTo(date_loanDisb) >= 0) {

										selectedLoanAcc = mLoanTypeArray[position];
										selectedLoanId = mLoanIdArray[position - 1];
										System.out.println("SELECTED LOAN ACC : " + selectedLoanAcc);
										System.out.println("----Id ----" + selectedLoanId);
										mLoanAccValue = selectedLoanAcc;
										mLoanAccIdValue = selectedLoanId;
										EShaktiApplication.setLoanId(mLoanAccIdValue);
										EShaktiApplication.setLoanBankName(mLoanAccValue);

										isLoanAccDetails = true;
										try {
											if (ConnectionUtils.isNetworkAvailable(getActivity())) {

												if (EShaktiApplication.getLoginFlag().equals(Constants.ONLINEFLAG)) {
													new Get_Loan_Account_Number_Webservice(
															Transaction_AcctoAccTransferFragment.this).execute();
													isLoanAccount = true;
												} else {

													TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF,
															TastyToast.LENGTH_SHORT, TastyToast.ERROR);
												}
											} else {
												if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
													EShaktiApplication.getInstance().getTransactionManager()
															.startTransaction(DataType.GET_LOANACCOUNT_NUMBER,
																	new GroupMasterSingle(
																			EShaktiApplication.getGetsinglegrpId()));
												} else {

													TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF,
															TastyToast.LENGTH_SHORT, TastyToast.ERROR);
												}
											}
										} catch (Exception e) {
											// TODO: handle exception
											e.printStackTrace();
										}
									} else {
										TastyToast.makeText(getActivity(), AppStrings.mCheckDisbursementDate,
												TastyToast.LENGTH_SHORT, TastyToast.WARNING);
										selectedRadio = "LOANACCOUNT";
										mLoanTypeArray = new String[SelectedGroupsTask.loan_Name.size()];
										mLoanTypeArray[0] = String.valueOf(AppStrings.mLoanAccType);

										mLoanIdArray = new String[SelectedGroupsTask.loan_Id.size() - 1];
										for (int i = 0; i < SelectedGroupsTask.loan_Name.size() - 1; i++) {
											mLoanTypeArray[i + 1] = SelectedGroupsTask.loan_Name.elementAt(i)
													.toString();
											mLoanIdArray[i] = SelectedGroupsTask.loan_Id.elementAt(i).toString();
										}

										loanAccItems = new ArrayList<RowItem>();
										for (int i = 0; i < mLoanTypeArray.length; i++) {
											RowItem rowItem = new RowItem(mLoanTypeArray[i]);
											loanAccItems.add(rowItem);
										}
										loanAccAdapter = new CustomItemAdapter(getActivity(), loanAccItems);
										mSpinner_loanAcc.setAdapter(loanAccAdapter);

									}

								}

								Log.e("Loan Name", mLoanAccValue + "");
								Log.e("Loan Id", mLoanAccIdValue + "");
							}

							@Override
							public void onNothingSelected(AdapterView<?> parent) {
								// TODO Auto-generated method stub

							}
						});
					} else {

						TastyToast.makeText(getActivity(), AppStrings.noGroupLoan_Alert, TastyToast.LENGTH_SHORT,
								TastyToast.WARNING);
					}

				}
			}
		});

	}

	private void onShowConfirmationDialog() {
		// TODO Auto-generated method stub
		confirmationDialog = new Dialog(getActivity());

		LayoutInflater inflater = getActivity().getLayoutInflater();
		View dialogView = inflater.inflate(R.layout.dialog_confirmation, null);
		dialogView.setLayoutParams(
				new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

		TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
		confirmationHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.confirmation));
		confirmationHeader.setTypeface(LoginActivity.sTypeface);

		TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

		TableRow typeRow = new TableRow(getActivity());

		@SuppressWarnings("deprecation")
		TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT, 1f);
		contentParams.setMargins(10, 5, 10, 5);

		TextView memberName_Text = new TextView(getActivity());
		memberName_Text.setText(
				GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mTransferFromBank) + " " + "SB"));
		memberName_Text.setTypeface(LoginActivity.sTypeface);
		memberName_Text.setTextColor(color.white);
		memberName_Text.setPadding(5, 5, 5, 5);
		memberName_Text.setLayoutParams(contentParams);
		typeRow.addView(memberName_Text);

		TextView confirm_values = new TextView(getActivity());
		confirm_values.setText(
				GetSpanText.getSpanString(getActivity(), String.valueOf(EShaktiApplication.getAcctoaccSelectBank())));
		confirm_values.setTextColor(color.white);
		confirm_values.setPadding(5, 5, 5, 5);
		confirm_values.setGravity(Gravity.RIGHT);
		confirm_values.setLayoutParams(contentParams);
		typeRow.addView(confirm_values);

		confirmationTable.addView(typeRow,
				new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

		// if (ConnectionUtils.isNetworkAvailable(getActivity())) {
		TableRow sbAccNoRow = new TableRow(getActivity());

		@SuppressWarnings("deprecation")
		TableRow.LayoutParams sbAccParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT, 1f);
		sbAccParams.setMargins(10, 5, 10, 5);

		TextView accNo_Text = new TextView(getActivity());
		accNo_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mAccountNumber)));
		accNo_Text.setTypeface(LoginActivity.sTypeface);
		accNo_Text.setTextColor(color.white);
		accNo_Text.setPadding(5, 5, 5, 5);
		accNo_Text.setLayoutParams(sbAccParams);
		sbAccNoRow.addView(accNo_Text);

		TextView accNo_values = new TextView(getActivity());
		accNo_values
				.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(publicValues.mGetSbAccountNumber)));
		accNo_values.setTextColor(color.white);
		accNo_values.setPadding(5, 5, 5, 5);
		accNo_values.setGravity(Gravity.RIGHT);
		accNo_values.setLayoutParams(sbAccParams);
		sbAccNoRow.addView(accNo_values);

		confirmationTable.addView(sbAccNoRow,
				new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		// }

		TableRow bankNameRow = new TableRow(getActivity());

		@SuppressWarnings("deprecation")
		TableRow.LayoutParams bankNameParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT, 1f);
		bankNameParams.setMargins(10, 5, 10, 5);

		TextView bankName_Text = new TextView(getActivity());
		if (selectedRadio.equals("SAVINGSACCOUNT")) {
			bankName_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mTransferToBank)));
		} else {
			bankName_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mLoanAccType)));
		}
		bankName_Text.setTypeface(LoginActivity.sTypeface);
		bankName_Text.setTextColor(color.white);
		bankName_Text.setPadding(5, 5, 5, 5);
		bankName_Text.setLayoutParams(bankNameParams);
		bankNameRow.addView(bankName_Text);

		TextView bankName_values = new TextView(getActivity());
		if (selectedRadio.equals("SAVINGSACCOUNT")) {
			bankName_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(mBankNameValue)));
		} else {
			bankName_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(mLoanAccValue)));
		}

		bankName_values.setTextColor(color.white);
		bankName_values.setPadding(5, 5, 5, 5);
		bankName_values.setGravity(Gravity.RIGHT);
		bankName_values.setLayoutParams(bankNameParams);
		bankNameRow.addView(bankName_values);

		confirmationTable.addView(bankNameRow,
				new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

		if (!selectedRadio.equals("SAVINGSACCOUNT")) {
			TableRow sbAccNoRow1 = new TableRow(getActivity());

			@SuppressWarnings("deprecation")
			TableRow.LayoutParams sbAccParams1 = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);
			sbAccParams1.setMargins(10, 5, 10, 5);

			TextView accNo_Text1 = new TextView(getActivity());
			accNo_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mAccountNumber)));
			accNo_Text1.setTypeface(LoginActivity.sTypeface);
			accNo_Text1.setTextColor(color.white);
			accNo_Text1.setPadding(5, 5, 5, 5);
			accNo_Text1.setLayoutParams(sbAccParams1);
			sbAccNoRow1.addView(accNo_Text1);

			TextView accNo_values1 = new TextView(getActivity());
			accNo_values1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(ToLoanAccNo)));
			accNo_values1.setTextColor(color.white);
			accNo_values1.setPadding(5, 5, 5, 5);
			accNo_values1.setGravity(Gravity.RIGHT);
			accNo_values1.setLayoutParams(sbAccParams1);
			sbAccNoRow1.addView(accNo_values1);

			confirmationTable.addView(sbAccNoRow1,
					new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		}

		TableRow withdrawRow = new TableRow(getActivity());

		@SuppressWarnings("deprecation")
		TableRow.LayoutParams withdrawParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT, 1f);
		withdrawParams.setMargins(10, 5, 10, 5);

		TextView withdraw = new TextView(getActivity());
		withdraw.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mTransferAmount)));
		withdraw.setTypeface(LoginActivity.sTypeface);
		withdraw.setTextColor(color.white);
		withdraw.setPadding(5, 5, 5, 5);
		withdraw.setLayoutParams(withdrawParams);
		withdrawRow.addView(withdraw);

		TextView withdraw_values = new TextView(getActivity());
		withdraw_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(mAcctransferAmount)));
		withdraw_values.setTextColor(color.white);
		withdraw_values.setPadding(5, 5, 5, 5);
		withdraw_values.setGravity(Gravity.RIGHT);
		withdraw_values.setLayoutParams(withdrawParams);
		withdrawRow.addView(withdraw_values);

		confirmationTable.addView(withdrawRow,
				new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

		TableRow expenseRow = new TableRow(getActivity());

		@SuppressWarnings("deprecation")
		TableRow.LayoutParams expensesParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT, 1f);
		expensesParams.setMargins(10, 5, 10, 5);

		TextView expense = new TextView(getActivity());
		expense.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mTransferCharges)));
		expense.setTypeface(LoginActivity.sTypeface);
		expense.setTextColor(color.white);
		expense.setPadding(5, 5, 5, 5);
		expense.setLayoutParams(expensesParams);
		expenseRow.addView(expense);

		TextView expense_values = new TextView(getActivity());
		expense_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(mAcctransferCharge)));
		expense_values.setTextColor(color.white);
		expense_values.setPadding(5, 5, 5, 5);
		expense_values.setGravity(Gravity.RIGHT);
		expense_values.setLayoutParams(expensesParams);
		expenseRow.addView(expense_values);

		confirmationTable.addView(expenseRow,
				new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

		mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit_button);
		mEdit_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.edit));
		mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
		mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
																// 205,
																// 0));
		mEdit_RaisedButton.setOnClickListener(this);

		mOk_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Ok_button);
		mOk_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.yes));
		mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
		mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
		mOk_RaisedButton.setOnClickListener(this);

		confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		confirmationDialog.setCanceledOnTouchOutside(false);
		confirmationDialog.setContentView(dialogView);
		confirmationDialog.setCancelable(true);
		confirmationDialog.show();

		MarginLayoutParams margin = (MarginLayoutParams) dialogView.getLayoutParams();
		margin.leftMargin = 10;
		margin.rightMargin = 10;
		margin.topMargin = 10;
		margin.bottomMargin = 10;
		margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);

	}

	@Subscribe
	public void OnGetSingleTransactionLoanWithdrawal(final GetSingleTransResponse getSingleTransResponse) {
		switch (getSingleTransResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), getSingleTransResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), getSingleTransResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:
			try {
				System.out.println("--------OnGetSingleTransactionLoanWithdrawal---------");
				Log.e("Step 1", "111111111111111111111");
				String mUniqueId = GetTransactionSinglevalues.getUniqueId();
				String mTrainingValues_Offline;
				if (selectedRadio.equals("SAVINGSACCOUNT")) {
					mTrainingValues_Offline = GetTransactionSinglevalues.getAcctoacctransfer();
				} else {
					mTrainingValues_Offline = GetTransactionSinglevalues.getAcctoLoanAccTransfer();

				}

				String mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
				String mMobileDate = Get_Offline_MobileDate.getOffline_MobileDate(getActivity());
				String mTransaction_UniqueId = PrefUtils.getOfflineUniqueID();
				String Send_to_server_values_Offline = null;
				if (selectedRadio.equals("SAVINGSACCOUNT")) {
					Send_to_server_values_Offline = mAcctransferAmount + "~" + mAcctransferCharge + "#"
							+ EShaktiApplication.getAcctoaccSendtoserverBank() + "#" + mBankNameValue;
				} else {
					Send_to_server_values_Offline = mAcctransferAmount + "~" + mAcctransferCharge + "#"
							+ EShaktiApplication.getAcctoaccSendtoserverBank() + "#"
							+ EShaktiApplication.getLoanBankName() + "#" + EShaktiApplication.getLoanId();
				}
				System.out.println("Send_to_server_values_Offline     " + Send_to_server_values_Offline.toString());
				mLastTrDate = DatePickerDialog.sDashboardDate;
				mLastTr_ID = SelectedGroupsTask.sTr_ID.toString();
				String mCurrentTransDate = null;

				if (publicValues.mOffline_Trans_CurrentDate != null) {
					mCurrentTransDate = publicValues.mOffline_Trans_CurrentDate;
				}

				String mAcctoaccTransfervalues = Send_to_server_values_Offline + "#" + mTrasactiondate + "#"
						+ mMobileDate;

				Log.e("Offline Db value", mAcctoaccTransfervalues + "");
				if (!mAcctoaccTransfervalues.contains("?") && !mCurrentTransDate.contains("?")
						&& !mTransaction_UniqueId.contains("?")) {

					mSqliteDBStoredValues_acc_toacc_Values = mAcctoaccTransfervalues;

					System.out.println(" Transaction Date -------->>>" + mTrasactiondate);

					System.out.println(" Mobile  Date -------->>>" + mMobileDate);

					System.out.println(" Transaction Unique Date -------->>>" + mTransaction_UniqueId);

					System.out.println(" Current Date -------->>>" + mCurrentTransDate);

					if (mUniqueId == null && mTrainingValues_Offline == null) {

						if (mTrasactiondate != null && mMobileDate != null && mTransaction_UniqueId != null
								&& mCurrentTransDate != null) {
							if (selectedRadio.equals("SAVINGSACCOUNT")) {
								EShaktiApplication.getInstance().getTransactionManager().startTransaction(
										DataType.TRANSACTIONADD,
										new Transaction(0, PrefUtils.getUsernameKey(), SelectedGroupsTask.UserName,
												SelectedGroupsTask.Ngo_Id, SelectedGroupsTask.Group_Id,
												mTransaction_UniqueId, mLastTr_ID, mCurrentTransDate, null, null, null,
												null, null, null, null, null, null, null, null, null, null, null, null,
												null, null, null, null, mAcctoaccTransfervalues, null, null));
							} else {
								EShaktiApplication.getInstance().getTransactionManager().startTransaction(
										DataType.TRANSACTIONADD,
										new Transaction(0, PrefUtils.getUsernameKey(), SelectedGroupsTask.UserName,
												SelectedGroupsTask.Ngo_Id, SelectedGroupsTask.Group_Id,
												mTransaction_UniqueId, mLastTr_ID, mCurrentTransDate, null, null, null,
												null, null, null, null, null, null, null, null, null, null, null, null,
												null, null, null, null, null, mAcctoaccTransfervalues, null));
							}
						} else {
							Log.e("Else Part", "Workingggggggg!!!!!!");

							TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT,
									TastyToast.ERROR);
						}

					} else if (mUniqueId.equalsIgnoreCase(PrefUtils.getOfflineUniqueID())
							&& mTrainingValues_Offline == null) {
						if (selectedRadio.equals("SAVINGSACCOUNT")) {
							EShaktiApplication.setSetTransValues(TransactionOffConstants.TRANS_ACCTO);
						} else {
							EShaktiApplication.setSetTransValues(TransactionOffConstants.TRANS_LOANACC);
						}

						EShaktiApplication.getInstance().getTransactionManager().startTransaction(
								DataType.TRANS_VALUES_UPDATE,
								new TransactionUpdate(mUniqueId, mAcctoaccTransfervalues));

					} else if (mTrainingValues_Offline != null) {
						confirmationDialog.dismiss();

						TastyToast.makeText(getActivity(), AppStrings.offlineDataAvailable, TastyToast.LENGTH_SHORT,
								TastyToast.WARNING);

						DatePickerDialog.sDashboardDate = SelectedGroupsTask.sLastTransactionDate_Response;
						MainFragment_Dashboard fragment = new MainFragment_Dashboard();
						setFragment(fragment);
					}
				} else {
					if (confirmationDialog.isShowing() && confirmationDialog != null) {
						confirmationDialog.dismiss();
					}

					TastyToast.makeText(getActivity(), "Please Try Again", TastyToast.LENGTH_SHORT, TastyToast.ERROR);

					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		}
	}

	@Subscribe
	public void OnTransUpdateLoanWithdrawal(final TransactionUpdateResponse transactionUpdateResponse) {
		switch (transactionUpdateResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), transactionUpdateResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), transactionUpdateResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			try {
				System.out.println("------OnTransUpdateLoanWithdrawal------");
				Log.e("Step 2", "222222222222222222222222222222222");
				boolean mAccValues = false;

				TransactionProvider.getSinlgeGroupMaster_Transaction(
						new Transaction_UniqueIdSingle(PrefUtils.getOfflineUniqueID()));

				if (selectedRadio.equals("SAVINGSACCOUNT")) {
					if (GetTransactionSinglevalues.getAcctoacctransfer() != null

							&& !GetTransactionSinglevalues.getAcctoacctransfer().equals("")) {
						mAccValues = true;
					}

				} else {
					if (GetTransactionSinglevalues.getAcctoLoanAccTransfer() != null

							&& !GetTransactionSinglevalues.getAcctoLoanAccTransfer().equals("")) {

						mAccValues = true;

					}

				}

				if (mAccValues) {

					if (!isLoanAccDetails) {
						confirmationDialog.dismiss();
					}

					String mCashinHand = SelectedGroupsTask.sCashinHand;

					int mAccTransfer = Integer.parseInt(mAcctransferAmount);
					int mAccTransferExpense = Integer.parseInt(mAcctransferCharge);
					int mCashatBankValue = Integer.parseInt(SelectedGroupsTask.sCashatBank);
					String mCashatBank = null;
					if (selectedRadio.equals("SAVINGSACCOUNT")) {
						mCashatBank = String.valueOf((mCashatBankValue)// -
																		// mAccTransfer)
								- mAccTransferExpense);

					} else {

						mCashatBank = String.valueOf((mCashatBankValue - mAccTransfer) - mAccTransferExpense);
					}
					SelectedGroupsTask.sCashatBank = mCashatBank;
					mFromBankAmount = mFromBankAmount - (mAccTransferExpense + mAccTransfer);
					mToBankAmount = mToBankAmount + mAccTransfer;

					EShaktiApplication.setAcctoaccTransferBank(true);
					String mBankDetails = GetOfflineTransactionValues.getBankDetails();
					String mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
					SelectedGroupsTask.sLastTransactionDate_Response = mTrasactiondate;
					String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mTrasactiondate,
							mCashinHand, mCashatBank, mBankDetails);
					String mSelectedGroupId = SelectedGroupsTask.Group_Id;

					EShaktiApplication.getInstance().getTransactionManager().startTransaction(
							DataType.GROUPMASTERUPDATE, new GroupDetailsUpdate(mSelectedGroupId, mGroupMasterResponse));

				} else {
					if (confirmationDialog.isShowing() && confirmationDialog != null) {
						confirmationDialog.dismiss();
					}

					TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT, TastyToast.ERROR);

					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);

				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		}
	}

	@Subscribe
	public void onAddTransactionLoanWithdrawal(final TransactionResponse transactionResponse) {
		switch (transactionResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), transactionResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), transactionResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:
			try {
				System.out.println("--------onAddTransactionLoanWithdrawal---------");
				boolean mAccValues = false;

				TransactionProvider.getSinlgeGroupMaster_Transaction(
						new Transaction_UniqueIdSingle(PrefUtils.getOfflineUniqueID()));

				if (selectedRadio.equals("SAVINGSACCOUNT")) {
					if (GetTransactionSinglevalues.getAcctoacctransfer() != null

							&& !GetTransactionSinglevalues.getAcctoacctransfer().equals("")) {
						mAccValues = true;
					}

				} else {
					if (GetTransactionSinglevalues.getAcctoLoanAccTransfer() != null

							&& !GetTransactionSinglevalues.getAcctoLoanAccTransfer().equals("")) {

						mAccValues = true;

					}

				}

				if (mAccValues) {
					if (!isLoanAccDetails) {
						confirmationDialog.dismiss();
					}

					String mCashinHand = SelectedGroupsTask.sCashinHand;

					int mAccTransfer = Integer.parseInt(mAcctransferAmount);
					int mAccTransferExpense = Integer.parseInt(mAcctransferCharge);
					int mCashatBankValue = Integer.parseInt(SelectedGroupsTask.sCashatBank);
					Log.e("Cash At Bank---->>>>>>", mCashatBankValue + "");
					String mCashatBank = null;
					if (selectedRadio.equals("SAVINGSACCOUNT")) {
						mCashatBank = String.valueOf((mCashatBankValue)// -
																		// mAccTransfer)
								- mAccTransferExpense);
					} else {
						mCashatBank = String.valueOf((mCashatBankValue - mAccTransfer) - mAccTransferExpense);
					}

					Log.e("Values----------->>>", mCashatBank);

					SelectedGroupsTask.sCashatBank = mCashatBank;
					mFromBankAmount = mFromBankAmount - (mAccTransferExpense + mAccTransfer);
					mToBankAmount = mToBankAmount + mAccTransfer;

					EShaktiApplication.setAcctoaccTransferBank(true);
					String mBankDetails = GetOfflineTransactionValues.getBankDetails();
					String mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
					SelectedGroupsTask.sLastTransactionDate_Response = mTrasactiondate;
					String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mTrasactiondate,
							mCashinHand, mCashatBank, mBankDetails);
					String mSelectedGroupId = SelectedGroupsTask.Group_Id;

					EShaktiApplication.getInstance().getTransactionManager().startTransaction(
							DataType.GROUPMASTERUPDATE, new GroupDetailsUpdate(mSelectedGroupId, mGroupMasterResponse));

				} else {
					if (confirmationDialog.isShowing() && confirmationDialog != null) {
						confirmationDialog.dismiss();
					}

					TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT, TastyToast.ERROR);

					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);

				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		}
	}

	@Subscribe
	public void OnGroupMasUpdate(final GroupMasUpdateResponse masterResponse) {
		switch (masterResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), masterResponse.getFetcherResult().getMessage(), TastyToast.LENGTH_SHORT,
					TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), masterResponse.getFetcherResult().getMessage(), TastyToast.LENGTH_SHORT,
					TastyToast.ERROR);
			break;
		case SUCCESS:

			System.out.println("-------OnGroupMasUpdate---------");
			if (!isLoanAccDetails) {
				confirmationDialog.dismiss();
			}

			if (selectedRadio.equals("SAVINGSACCOUNT")) {
				TastyToast.makeText(getActivity(), AppStrings.transactionCompleted, TastyToast.LENGTH_SHORT,
						TastyToast.SUCCESS);

				MainFragment_Dashboard fragment = new MainFragment_Dashboard();
				setFragment(fragment);

			} else {

				EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GET_GROUPMASTER_GLOS,
						new GroupMasterSingle(SelectedGroupsTask.Group_Id));

			}

			break;
		}
	}

	@Subscribe
	public void OnGroupLoanOSResponse(final GroupLoanOSResponse groupLoanOSResponse) {
		switch (groupLoanOSResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), groupLoanOSResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), groupLoanOSResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:
			try {
				System.out.println("----------OnGroupLoanOSResponse----------");
				String mGroupLoanOSResponse = GetGroupMemberDetails.getGrouploanOS();
				String[] response = mGroupLoanOSResponse.split("%");

				StringBuilder builder = new StringBuilder();

				String mMasterresponse, mGroupLoanOSId = null, mGLOS = null, result;

				Log.e("Group response@@@@@@@", mGroupLoanOSResponse + "");
				if (mGroupLoanOSResponse != null && !mGroupLoanOSResponse.equals("")) {

					for (int i = 0; i < response.length; i++) {

						String secondResponse[] = response[i].split("#");

						mGLOS = secondResponse[1];
						if (mLoanAccIdValue.equals(secondResponse[0])) {

							mGLOS = String
									.valueOf(Integer.parseInt(mLoanOutstanding_value_TextView.getText().toString())
											- Integer.parseInt(mAcctransferAmount));

						}
						mGroupLoanOSId = secondResponse[0];

						mMasterresponse = mGroupLoanOSId + "#" + mGLOS + "%";

						builder.append(mMasterresponse);

					}
					result = builder.toString();
					Log.v("Update Values", result);

					String mCashatBank = SelectedGroupsTask.sCashatBank;
					String mBankDetails = GetOfflineTransactionValues.getBankDetails();

					String mTrasactiondate = null;
					if (ConnectionUtils.isNetworkAvailable(getActivity())) {
						mTrasactiondate = SelectedGroupsTask.sLastTransactionDate_Response;// DatePickerDialog.sSend_To_Server_Date;
					} else {
						mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
					}

					String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mTrasactiondate,
							SelectedGroupsTask.sCashinHand, mCashatBank, mBankDetails);
					String mSelectedGroupId = SelectedGroupsTask.Group_Id;

					Log.e("mGroupMasterResponse $$$", mGroupMasterResponse + "");
					Log.e("result $$$ ", result + "");

					if (!isOfflineEntry) {
						isOfflineEntry = true;

						EShaktiApplication.getInstance().getTransactionManager().startTransaction(
								DataType.GROUP_MASTER_GLOS,
								new GroupLoanOSUpdate(mSelectedGroupId, mGroupMasterResponse, result));
					}
				} else {
					TastyToast.makeText(getActivity(), AppStrings.loginAgainAlert, TastyToast.LENGTH_LONG,
							TastyToast.WARNING);
				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		}

	}

	@Subscribe
	public void OnGroupGLOSUpdate(final GroupLoanOSResponse groupLoanOSResponse) {
		switch (groupLoanOSResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), groupLoanOSResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), groupLoanOSResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {

				if (selectedRadio.equals("SAVINGSACCOUNT")) {

					confirmationDialog.dismiss();
					try {

						TastyToast.makeText(getActivity(), AppStrings.transactionCompleted, TastyToast.LENGTH_SHORT,
								TastyToast.SUCCESS);

						if (!ConnectionUtils.isNetworkAvailable(getActivity())) {

							String mValues = Put_DB_GroupNameDetail_Response
									.put_DB_GroupNameDetails_Response(EShaktiApplication.getGroupResponse());

							GroupProvider.updateGroupResponse(new GroupResponseUpdate(
									EShaktiApplication.getGroupId_GroupLastTransDate(), mValues));
						}

						MainFragment_Dashboard fragment = new MainFragment_Dashboard();
						setFragment(fragment);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} else {

					EShaktiApplication.getInstance().getTransactionManager().startTransaction(
							DataType.GET_LOANACCOUNT_NUMBER,
							new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));
					isOfflineLoanAcc = true;

				}
			} else {
				if (selectedRadio.equals("SAVINGSACCOUNT")) {

					confirmationDialog.dismiss();
					try {

						TastyToast.makeText(getActivity(), AppStrings.transactionCompleted, TastyToast.LENGTH_SHORT,
								TastyToast.SUCCESS);

						MainFragment_Dashboard fragment = new MainFragment_Dashboard();
						setFragment(fragment);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {

					EShaktiApplication.getInstance().getTransactionManager().startTransaction(
							DataType.GET_LOANACCOUNT_NUMBER, new GroupMasterSingle(SelectedGroupsTask.Group_Id));
					isOfflineLoanAcc = true;

				}

			}
			break;
		}
	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub
		mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
		mProgressDilaog.show();
	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub

		if (mProgressDilaog != null) {
			mProgressDilaog.dismiss();
			if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {
				getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub

						confirmationDialog.dismiss();

						if (!result.equals("FAIL")) {

							TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg,
									TastyToast.LENGTH_SHORT, TastyToast.ERROR);
							Constants.NETWORKCOMMONFLAG = "SUCCESS";
						}

					}
				});

			} else {
				if (isLoanAccount) {

					isLoanAccount = false;
					if (!publicValues.mGetLoanAccountNumber.isEmpty()
							&& !publicValues.mGetLoanAccountNumber.equals("")) {
						String[] responseArr = publicValues.mGetLoanAccountNumber.split("~");
						ToLoanAccNo = responseArr[1].toString();
						outstandingAmt = responseArr[2].toString();
						mBankName_value_TextView.setText(responseArr[0].toString());
						mLoanAccNo_value_TextView.setText(responseArr[1].toString());
						mLoanOutstanding_value_TextView.setText(responseArr[2].toString());
					}

				} else {
					if (isSavingAccount) {
						if (isGetTrid) {
							isGetTrid = false;
							Log.e("Valuess---->>>>>", SelectedGroupsTask.sCashatBank);
							callOfflineDataUpdate();
							isSavingAccount = false;
						} else {

							try {
								if (GetResponseInfo.sResponseUpdate[0].equals("Yes")) {

									new Get_LastTransactionID(Transaction_AcctoAccTransferFragment.this).execute();
									isGetTrid = true;

								} else {

									confirmationDialog.dismiss();

									TastyToast.makeText(getActivity(), AppStrings.transactionFailAlert,
											TastyToast.LENGTH_SHORT, TastyToast.ERROR);
									MainFragment_Dashboard fragment = new MainFragment_Dashboard();
									setFragment(fragment);

								}

							} catch (Exception e) { // TODO: handle exception
								e.printStackTrace();
							}
						}
					} else {

						if (isGetTrid) {
							isGetTrid = false;
							callOfflineDataUpdate_LoanAccount();
							isSavingAccount = false;
						} else {

							try {
								if (GetResponseInfo.sResponseUpdate[0].equals("Yes")) {

									new Get_LastTransactionID(Transaction_AcctoAccTransferFragment.this).execute();
									isGetTrid = true;

								} else {

									confirmationDialog.dismiss();

									TastyToast.makeText(getActivity(), AppStrings.transactionFailAlert,
											TastyToast.LENGTH_SHORT, TastyToast.ERROR);
									MainFragment_Dashboard fragment = new MainFragment_Dashboard();
									setFragment(fragment);

								}

							} catch (Exception e) { // TODO: handle exception
								e.printStackTrace();
							}
						}
					}

				}

			}
		}
	}

	private void callOfflineDataUpdate() {
		// TODO Auto-generated method stub
		Log.e("On callOfflineDataUpdate", SelectedGroupsTask.sLastTransactionDate_Response);
		String mCashinHand = SelectedGroupsTask.sCashinHand;
		String mTransactionDate = SelectedGroupsTask.sLastTransactionDate_Response;
		String mCashatBank = SelectedGroupsTask.sCashatBank;

		Log.e("Cash in Hand", mCashinHand);
		Log.e("Cash at Bank", mCashatBank);

		String mBankDetails = GetOfflineTransactionValues.getBankDetails();

		Log.e("Bank Details", mBankDetails);

		String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mTransactionDate, mCashinHand,
				mCashatBank, mBankDetails);

		String mSelectedGroupId = SelectedGroupsTask.Group_Id;
		isServiceCall = false;
		EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GROUPMASTERUPDATE,
				new GroupDetailsUpdate(mSelectedGroupId, mGroupMasterResponse));
	}

	private void callOfflineDataUpdate_LoanAccount() {
		// TODO Auto-generated method stub

		Log.e("On callOfflineDataUpdate_LoanAccount", SelectedGroupsTask.sLastTransactionDate_Response);
		String mCashinHand = SelectedGroupsTask.sCashinHand;
		String mTransactionDate = SelectedGroupsTask.sLastTransactionDate_Response;
		String mCashatBank = SelectedGroupsTask.sCashatBank;

		EShaktiApplication.setSavAccToLoanAccTransfer(true);
		String mBankDetails = GetOfflineTransactionValues.getBankDetails();

		String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mTransactionDate, mCashinHand,
				mCashatBank, mBankDetails);

		String mSelectedGroupId = SelectedGroupsTask.Group_Id;
		isServiceCall = false;
		EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GROUPMASTERUPDATE,
				new GroupDetailsUpdate(mSelectedGroupId, mGroupMasterResponse));

	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub
		isOfflineEntry = false;

		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

		FragmentDrawer.drawer_CashinHand.setText(RegionalConversion.getRegionalConversion(AppStrings.cashinhand)

				+ SelectedGroupsTask.sCashinHand);
		FragmentDrawer.drawer_CashinHand.setTypeface(LoginActivity.sTypeface);

		FragmentDrawer.drawer_CashatBank.setText(RegionalConversion.getRegionalConversion(AppStrings.cashatBank)

				+ SelectedGroupsTask.sCashatBank);
		FragmentDrawer.drawer_CashatBank.setTypeface(LoginActivity.sTypeface);

		FragmentDrawer.drawer_lastTR_Date
				.setText(RegionalConversion.getRegionalConversion(AppStrings.lastTransactionDate) + " : "
						+ SelectedGroupsTask.sLastTransactionDate_Response);
		FragmentDrawer.drawer_lastTR_Date.setTypeface(LoginActivity.sTypeface);

		Log.e("On Setfragment", SelectedGroupsTask.sLastTransactionDate_Response);

		UpdateCalendarMinMaxDateUtils.OnUpdateCalendarDate();

		Calendar calender = Calendar.getInstance();

		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String formattedDate = df.format(calender.getTime());
		EShaktiApplication.setLastTransDate_GroupId(SelectedGroupsTask.Group_Id);

		new GroupTempDBLastTransDate(EShaktiApplication.getLastTransDate_GroupId(),
				SelectedGroupsTask.sLastTransactionDate_Response, formattedDate);

		GroupProvider.updateGroupLastTransDateResponse();

	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		SBus.INST.unRegister(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.fragment_Edit_button:
			mAcctransferAmount = Reset.reset(mAcctransferAmount);
			mAcctransferCharge = Reset.reset(mAcctransferCharge);
			mSubmitButton.setClickable(true);

			confirmationDialog.dismiss();

			break;
		case R.id.fragment_Ok_button:

			if (ConnectionUtils.isNetworkAvailable(getActivity())) {

				if (EShaktiApplication.getLoginFlag().equals(Constants.ONLINEFLAG)) {
					try {
						if (selectedRadio.equals("SAVINGSACCOUNT")) {
							isSavingAccount = true;
							if (!isServiceCall) {
								isServiceCall = true;

								new Get_Account_To_AccountTransferWebservice(Transaction_AcctoAccTransferFragment.this)
										.execute();
							}
						} else if (selectedRadio.equals("LOANACCOUNT")) {
							isSavingAccount = false;
							if (!isServiceCall) {
								isServiceCall = true;

								new Get_Group_loan_repaidFromSBWebservice(Transaction_AcctoAccTransferFragment.this)
										.execute();
							}
						}

					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
				} else {

					TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
							TastyToast.ERROR);
				}

			} else {
				// Do offline Stuffs

				if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {

					EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GET_TRANS_SINGLE,
							new GetTransactionSingle(PrefUtils.getOfflineUniqueID()));
				} else {

					TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
							TastyToast.ERROR);
				}

			}

			break;
		}
	}

	@Subscribe
	public void OnGroupMasterLoanAccNoDetailsResponse(final GetLoanAccountNoResponse getLoanAccNoResponse) {
		switch (getLoanAccNoResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), getLoanAccNoResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), getLoanAccNoResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:
			try {
				String mGetLoanAccNoResponse = GetGroupMemberDetails.getLoanOSDetails();
				Log.e("Offline get LoanAccountNo response ", mGetLoanAccNoResponse + "");

				if (mGetLoanAccNoResponse != null && !mGetLoanAccNoResponse.equals("")) {
					String[] arrValues = mGetLoanAccNoResponse.split("#");

					mLoanBankName.clear();
					mLoanName.clear();
					mLoanId.clear();
					mLoanOutstanding.clear();
					mLoanAccNo.clear();
					String mSelectedGroupId = SelectedGroupsTask.Group_Id;
					for (int i = 0; i < arrValues.length; i++) {
						String ValuesSplit = arrValues[i];

						String[] arrValuesSplit = ValuesSplit.split("~");

						if (arrValuesSplit.length != 0 && arrValuesSplit.length == 5) {
							mLoanBankName.add(arrValuesSplit[0]);
							mLoanName.add(arrValuesSplit[1]);
							mLoanId.add(arrValuesSplit[2]);
							mLoanOutstanding.add(arrValuesSplit[3]);
							mLoanAccNo.add(arrValuesSplit[4]);
						}

					}

					String mTempLoanBankName = null, mTempLoanname = null, mTempLoanId = null,
							mTempLoanOustanding = null, mTempLoanAccNo = null;
					String response = "";
					for (int i = 0; i < mLoanName.size(); i++) {
						mTempLoanBankName = mLoanBankName.get(i).toString();
						mTempLoanname = mLoanName.get(i).toString();
						mTempLoanId = mLoanId.get(i).toString();
						mTempLoanOustanding = mLoanOutstanding.get(i).toString();
						mTempLoanAccNo = mLoanAccNo.get(i).toString();

						if (EShaktiApplication.getLoanId().equals(mLoanId.get(i))) {
							String mLoan_Outstanding = mLoanOutstanding.get(i).toString();
							String mLoan_AccNo = mLoanAccNo.get(i).toString();
							String mLoan_BankName = mLoanBankName.get(i).toString();
							if (isOfflineLoanAcc) {

								int Loan_Outstanding = Integer.parseInt(mLoan_Outstanding)
										- Integer.parseInt(mAcctransferAmount);
								mLoan_Outstanding = String.valueOf(Loan_Outstanding);
								mTempLoanOustanding = mLoan_Outstanding;
							}
							ToLoanAccNo = mLoan_AccNo;
							outstandingAmt = mLoan_Outstanding;
							mBankName_value_TextView.setText(mLoan_BankName);
							mLoanAccNo_value_TextView.setText(mLoan_AccNo);
							mLoanOutstanding_value_TextView.setText(mLoan_Outstanding);
						}
						response = response + mTempLoanBankName + "~" + mTempLoanname + "~" + mTempLoanId + "~"
								+ mTempLoanOustanding + "~" + mTempLoanAccNo + "#";

					}

					if (isOfflineLoanAcc) {
						isOfflineLoanAcc = false;
						String result = response.toString();
						Log.e("Values------------------------>>>>>>", result);

						if (mCount == 0) {

							mCount++;

							EShaktiApplication.getInstance().getTransactionManager().startTransaction(
									DataType.GET_LOANACCNO_DETAILS_UPDATE,
									new LoanAccountNoDetailsUpdate(mSelectedGroupId, result));
						}

					}
				} else {

					TastyToast.makeText(getActivity(), AppStrings.loginAgainAlert, TastyToast.LENGTH_LONG,
							TastyToast.WARNING);
				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		}

	}

	@Subscribe
	public void OnLoanAccNoUpdate(final LoanAccountNoUpdateResponse loanAccNoResponse) {
		switch (loanAccNoResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), loanAccNoResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), loanAccNoResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			isLoanAccDetails = false;
			confirmationDialog.dismiss();

			TastyToast.makeText(getActivity(), AppStrings.transactionCompleted, TastyToast.LENGTH_SHORT,
					TastyToast.SUCCESS);

			if (!ConnectionUtils.isNetworkAvailable(getActivity())) {

				String mValues = Put_DB_GroupNameDetail_Response
						.put_DB_GroupNameDetails_Response(EShaktiApplication.getGroupResponse());

				GroupProvider.updateGroupResponse(
						new GroupResponseUpdate(EShaktiApplication.getGroupId_GroupLastTransDate(), mValues));
			}

			MainFragment_Dashboard fragment = new MainFragment_Dashboard();
			setFragment(fragment);

			break;
		}
	}

}
