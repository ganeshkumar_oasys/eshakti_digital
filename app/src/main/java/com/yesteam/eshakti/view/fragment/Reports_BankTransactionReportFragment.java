package com.yesteam.eshakti.view.fragment;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.Get_BankTransactionSummaryTask;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Reports_BankTransactionReportFragment extends Fragment {
	private TextView mGroupName, mCashInHand, mCashAtBank, mHeader;

	public Reports_BankTransactionReportFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_BANK_TRANSACTION;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_banktransaction,
				container, false);

		MainFragment_Dashboard.isBackpressed = false;

		try {
			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String
					.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashInHand.setText(RegionalConversion.getRegionalConversion(String
					.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashInHand.setTypeface(LoginActivity.sTypeface);

			mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashAtBank.setText(RegionalConversion.getRegionalConversion(String
					.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashAtBank.setTypeface(LoginActivity.sTypeface);

			String responseArr[] = Get_BankTransactionSummaryTask.sBank_summary_Response
					.split("~");

			mHeader = (TextView) rootView
					.findViewById(R.id.fragment_bankTransaction_headertext);
			mHeader.setText(RegionalConversion
					.getRegionalConversion(responseArr[0].toString()));
			mHeader.setTypeface(LoginActivity.sTypeface);

			TableLayout headerTable = (TableLayout) rootView.findViewById(R.id.bankTransaction_headerTable);
			
			TableLayout tableLayout = (TableLayout) rootView
					.findViewById(R.id.fragment_bankTransaction_contentTable);

			TableRow head_row = new TableRow(getActivity());

			TableRow.LayoutParams headerParams = new TableRow.LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1f);

			TextView head_date = new TextView(getActivity());
			head_date.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.date)));
			head_date.setTypeface(LoginActivity.sTypeface);
			head_date.setTextColor(Color.WHITE);
			head_date.setPadding(50, 5, 5, 5);
			head_date.setLayoutParams(headerParams);
			head_date.setBackgroundResource(color.tableHeader);
			head_row.addView(head_date);

			TextView amount = new TextView(getActivity());
			amount.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.amount)));
			amount.setTypeface(LoginActivity.sTypeface);
			amount.setTextColor(Color.WHITE);
			amount.setPadding(5, 5, 50, 5);
			amount.setGravity(Gravity.RIGHT);
			amount.setLayoutParams(headerParams);
			amount.setBackgroundResource(color.tableHeader);
			head_row.addView(amount);

			TextView details = new TextView(getActivity());
			details.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.details)));
			details.setTypeface(LoginActivity.sTypeface);
			details.setTextColor(Color.WHITE);
			details.setPadding(5, 5, 10, 5);
			details.setGravity(Gravity.RIGHT);
			details.setLayoutParams(headerParams);
			details.setBackgroundResource(color.tableHeader);
			head_row.addView(details);

			headerTable.addView(head_row, new TableLayout.LayoutParams(
					TableLayout.LayoutParams.MATCH_PARENT,
					TableLayout.LayoutParams.WRAP_CONTENT));

			try {
				for (int i = 2; i < responseArr.length; i = i + 3) {
					TableRow contentRow = new TableRow(getActivity());

					TableRow.LayoutParams contentParams = new TableRow.LayoutParams(
							LayoutParams.MATCH_PARENT,
							LayoutParams.WRAP_CONTENT, 1f);

					TextView date = new TextView(getActivity());
					date.setText(GetSpanText.getSpanString(getActivity(),
							String.valueOf(responseArr[i])));
					date.setPadding(20, 5, 10, 5);
					date.setTextColor(color.black);
					date.setLayoutParams(contentParams);
					contentRow.addView(date);

					TextView repaymentAmount = new TextView(getActivity());
					repaymentAmount.setText(GetSpanText.getSpanString(
							getActivity(), String.valueOf(responseArr[i + 1])));
					repaymentAmount.setPadding(5, 5, 5, 5);
					repaymentAmount.setLayoutParams(headerParams);
					repaymentAmount.setTextColor(color.black);
					repaymentAmount.setGravity(Gravity.RIGHT);
					System.out.println("responseArr1:"
							+ responseArr[i + 1].toString());
					contentRow.addView(repaymentAmount);

					TextView bankTransactions = new TextView(getActivity());
					bankTransactions.setText(GetSpanText.getSpanString(
							getActivity(), String.valueOf(responseArr[i + 2])));
					bankTransactions.setPadding(10, 5, 10, 5);
					bankTransactions.setGravity(Gravity.RIGHT);
					bankTransactions.setLayoutParams(contentParams);
					bankTransactions.setTextColor(color.black);
					System.out.println("responseArr2:"
							+ responseArr[i + 2].toString());
					contentRow.addView(bankTransactions);

					tableLayout.addView(contentRow,
							new TableLayout.LayoutParams(
									LayoutParams.MATCH_PARENT,
									LayoutParams.WRAP_CONTENT));

					View rullerView = new View(getActivity());
					rullerView.setLayoutParams(new TableRow.LayoutParams(
							TableRow.LayoutParams.MATCH_PARENT, 1));
					rullerView.setBackgroundColor(Color.rgb(220, 220, 220));

					tableLayout.addView(rullerView);

				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return rootView;
	}
}
