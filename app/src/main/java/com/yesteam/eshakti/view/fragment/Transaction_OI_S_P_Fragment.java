package com.yesteam.eshakti.view.fragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.squareup.otto.Subscribe;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.TransactionOffConstants;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;
import com.yesteam.eshakti.model.RowItem;
import com.yesteam.eshakti.sqlite.database.GroupProvider;
import com.yesteam.eshakti.sqlite.database.TransactionProvider;
import com.yesteam.eshakti.sqlite.database.response.GetSingleTransResponse;
import com.yesteam.eshakti.sqlite.database.response.GroupMasUpdateResponse;
import com.yesteam.eshakti.sqlite.database.response.TransactionResponse;
import com.yesteam.eshakti.sqlite.database.response.TransactionUpdateResponse;
import com.yesteam.eshakti.sqlite.db.model.GetTransactionSingle;
import com.yesteam.eshakti.sqlite.db.model.GetTransactionSinglevalues;
import com.yesteam.eshakti.sqlite.db.model.GroupDetailsUpdate;
import com.yesteam.eshakti.sqlite.db.model.GroupResponseUpdate;
import com.yesteam.eshakti.sqlite.db.model.GroupTempDBLastTransDate;
import com.yesteam.eshakti.sqlite.db.model.Transaction;
import com.yesteam.eshakti.sqlite.db.model.TransactionUpdate;
import com.yesteam.eshakti.sqlite.db.model.Transaction_UniqueIdSingle;
import com.yesteam.eshakti.sqlite.db.transactions.TransactionManager.DataType;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.utils.GetOfflineTransactionValues;
import com.yesteam.eshakti.utils.GetResponseInfo;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.utils.Get_EdiText_Filter;
import com.yesteam.eshakti.utils.Get_Offline_MobileDate;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.utils.Put_DB_GroupNameDetail_Response;
import com.yesteam.eshakti.utils.Put_DB_GroupResponse;
import com.yesteam.eshakti.utils.Reset;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.utils.TextviewUtils;
import com.yesteam.eshakti.utils.UpdateCalendarMinMaxDateUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.Get_LastTransactionID;
import com.yesteam.eshakti.webservices.Get_OtherIncomeTask;
import com.yesteam.eshakti.webservices.Get_PenaltyTask;
import com.yesteam.eshakti.webservices.Get_Subscription_Charges;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Transaction_OI_S_P_Fragment extends Fragment implements OnClickListener, TaskListener {

	public static String TAG = Transaction_OI_S_P_Fragment.class.getSimpleName();

	List<EditText> sIncomeFields = new ArrayList<EditText>();
	private static String sIncomeAmounts[];
	public static String sSendToServer_Income;

	private TextView mGroupName, mCashinHand, mCashatBank, mHeader, mAutoFill_label, mAutoFill_label1;
	private CheckBox mAutoFill, mAutoFill1;
	private Button mSubmit_Raised_Button;
	private Button mEdit_RaisedButton, mOk_RaisedButton;
	private EditText mIncome_values;
	private TableLayout mIncomeTable;

	private Dialog mProgressDilaog;
	Dialog confirmationDialog;

	Boolean isValues;
	String nullVlaue = "0";
	String toBeEditArr[];

	String[] confirmArr;
	List<RowItem> rowItems;
	int mSize;
	public static int sIncome_Total;
	public static Boolean isIncome = false;

	AlertDialog alertDialog;
	String mLastTrDate = null, mLastTr_ID = null;
	boolean isGetTrid = false;
	private LinearLayout mOthersLayout;
	private TextView mOthersTextView;
	private EditText mOthersEditText;
	private String mOthersAmount_Values;
	boolean isServiceCall = false;
	String mSqliteDBStoredValues_mOtherincomevalues = null, mSqliteDBStoredValues_mSubscriptionIncomevalues = null,
			mSqliteDBStoredValues_mPenaltyIncomevalues = null;

	LinearLayout mMemberNameLayout;
	TextView mMemberName;

	public Transaction_OI_S_P_Fragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_OI_S_P;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		SBus.INST.register(this);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		sSendToServer_Income = Reset.reset(sSendToServer_Income);
		sIncome_Total = 0;
		sIncomeFields.clear();
		mOthersAmount_Values = Reset.reset(mOthersAmount_Values);
	}

	@SuppressWarnings("deprecation")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View rootView = inflater.inflate(R.layout.fragment_savings, container, false);

		MainFragment_Dashboard.isBackpressed = false;

		mSqliteDBStoredValues_mOtherincomevalues = null;
		mSqliteDBStoredValues_mSubscriptionIncomevalues = null;
		mSqliteDBStoredValues_mPenaltyIncomevalues = null;

		try {

			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashinHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashinHand.setTypeface(LoginActivity.sTypeface);

			mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashatBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashatBank.setTypeface(LoginActivity.sTypeface);

			mMemberNameLayout = (LinearLayout) rootView.findViewById(R.id.member_name_layout);
			mMemberName = (TextView) rootView.findViewById(R.id.member_name);

			/** UI Mapping **/

			mHeader = (TextView) rootView.findViewById(R.id.fragmentHeader);
			mHeader.setText(
					RegionalConversion.getRegionalConversion(Transaction_IncomeMenuFragment.sSelectedIncomeMenu));
			mHeader.setTypeface(LoginActivity.sTypeface);

			if (Transaction_IncomeMenuFragment.sSelectedIncomeMenu.equals(AppStrings.subscriptioncharges)
					|| Transaction_IncomeMenuFragment.sSelectedIncomeMenu.equals(AppStrings.penalty)) {
				mAutoFill_label = (TextView) rootView.findViewById(R.id.autofillLabel);
				mAutoFill_label.setText(RegionalConversion.getRegionalConversion(AppStrings.autoFill));
				mAutoFill_label.setTypeface(LoginActivity.sTypeface);
				mAutoFill_label.setVisibility(View.VISIBLE);

				mAutoFill = (CheckBox) rootView.findViewById(R.id.autoFill);
				mAutoFill.setVisibility(View.VISIBLE);
				mAutoFill.setOnClickListener(this);
			} else if (Transaction_IncomeMenuFragment.sSelectedIncomeMenu.equals(AppStrings.otherincome)) {
				mAutoFill_label1 = (TextView) rootView.findViewById(R.id.autofillLabel1);
				mAutoFill_label1.setText(RegionalConversion.getRegionalConversion(AppStrings.autoFill));
				mAutoFill_label1.setTypeface(LoginActivity.sTypeface);
				mAutoFill_label1.setVisibility(View.VISIBLE);

				mAutoFill1 = (CheckBox) rootView.findViewById(R.id.autoFill1);
				mAutoFill1.setVisibility(View.VISIBLE);
				mAutoFill1.setOnClickListener(this);

			}

			mSize = SelectedGroupsTask.member_Name.size();
			Log.d(TAG, String.valueOf(mSize));

			mOthersLayout = (LinearLayout) rootView.findViewById(R.id.otherIncomeLayout);
			mOthersTextView = (TextView) rootView.findViewById(R.id.otherLoanTextView);
			mOthersEditText = (EditText) rootView.findViewById(R.id.otherLoanEditText);

			mOthersTextView.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.mOthers)));
			mOthersTextView.setTypeface(LoginActivity.sTypeface);

			mOthersEditText.setTextAppearance(getActivity(), R.style.MyMaterialTheme);
			mOthersEditText.setFilters(Get_EdiText_Filter.editText_filter());
			mOthersEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
			mOthersEditText.setTextColor(color.black);
			mOthersEditText.setPadding(5, 5, 5, 5);
			mOthersEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {

				@Override
				public void onFocusChange(View v, boolean hasFocus) {
					// TODO Auto-generated method stub
					if (hasFocus) {
						((EditText) v).setGravity(Gravity.LEFT);

					} else {
						((EditText) v).setGravity(Gravity.RIGHT);

					}
				}
			});

			TableLayout headerTable = (TableLayout) rootView.findViewById(R.id.savingsTable);

			mIncomeTable = (TableLayout) rootView.findViewById(R.id.fragment_contentTable);

			TableRow savingsHeader = new TableRow(getActivity());
			savingsHeader.setBackgroundResource(color.tableHeader);

			LayoutParams headerParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT,
					1f);

			TextView mMemberName_headerText = new TextView(getActivity());
			mMemberName_headerText
					.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.memberName)));
			mMemberName_headerText.setTypeface(LoginActivity.sTypeface);
			mMemberName_headerText.setTextColor(Color.WHITE);
			mMemberName_headerText.setPadding(20, 5, 10, 5);
			mMemberName_headerText.setLayoutParams(headerParams);
			savingsHeader.addView(mMemberName_headerText);

			TextView mIncomeAmount_HeaderText = new TextView(getActivity());
			mIncomeAmount_HeaderText
					.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.amount)));
			mIncomeAmount_HeaderText.setTypeface(LoginActivity.sTypeface);
			mIncomeAmount_HeaderText.setTextColor(Color.WHITE);
			mIncomeAmount_HeaderText.setPadding(10, 5, 80, 5);
			mIncomeAmount_HeaderText.setGravity(Gravity.CENTER);
			mIncomeAmount_HeaderText.setLayoutParams(headerParams);
			mIncomeAmount_HeaderText.setBackgroundResource(color.tableHeader);
			savingsHeader.addView(mIncomeAmount_HeaderText);

			headerTable.addView(savingsHeader,
					new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

			for (int i = 0; i < mSize; i++) {

				TableRow indv_IncomeRow = new TableRow(getActivity());

				TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
						LayoutParams.WRAP_CONTENT, 1f);
				contentParams.setMargins(10, 5, 10, 5);

				final TextView memberName_Text = new TextView(getActivity());
				memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
						String.valueOf(SelectedGroupsTask.member_Name.elementAt(i))));
				memberName_Text.setTypeface(LoginActivity.sTypeface);
				memberName_Text.setTextColor(color.black);
				memberName_Text.setPadding(10, 0, 10, 5);
				memberName_Text.setLayoutParams(contentParams);
				memberName_Text.setWidth(200);
				memberName_Text.setSingleLine(true);
				memberName_Text.setEllipsize(TextUtils.TruncateAt.END);
				indv_IncomeRow.addView(memberName_Text);

				TableRow.LayoutParams contentEditParams = new TableRow.LayoutParams(150, LayoutParams.WRAP_CONTENT);
				contentEditParams.setMargins(30, 5, 100, 5);

				mIncome_values = new EditText(getActivity());

				mIncome_values.setId(i);
				sIncomeFields.add(mIncome_values);
				mIncome_values.setGravity(Gravity.END);
				mIncome_values.setTextColor(Color.BLACK);
				mIncome_values.setPadding(5, 5, 5, 5);
				mIncome_values.setBackgroundResource(R.drawable.edittext_background);
				mIncome_values.setLayoutParams(contentEditParams);// contentParams
																	// lParams
				mIncome_values.setTextAppearance(getActivity(), R.style.MyMaterialTheme);
				mIncome_values.setFilters(Get_EdiText_Filter.editText_filter());
				mIncome_values.setInputType(InputType.TYPE_CLASS_NUMBER);
				mIncome_values.setTextColor(color.black);
				mIncome_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

					@Override
					public void onFocusChange(View v, boolean hasFocus) {
						// TODO Auto-generated method stub
						if (hasFocus) {
							((EditText) v).setGravity(Gravity.LEFT);

							mMemberNameLayout.setVisibility(View.VISIBLE);
							mMemberName.setText(memberName_Text.getText().toString().trim());
							mMemberName.setTypeface(LoginActivity.sTypeface);
							TextviewUtils.manageBlinkEffect(mMemberName, getActivity());

						} else {
							((EditText) v).setGravity(Gravity.RIGHT);

							mMemberNameLayout.setVisibility(View.GONE);
							mMemberName.setText("");
						}

					}
				});
				indv_IncomeRow.addView(mIncome_values);

				mIncomeTable.addView(indv_IncomeRow,
						new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

			}

			mSubmit_Raised_Button = (Button) rootView.findViewById(R.id.fragment_Submit_button);
			mSubmit_Raised_Button.setText(RegionalConversion.getRegionalConversion(AppStrings.submit));
			mSubmit_Raised_Button.setTypeface(LoginActivity.sTypeface);
			mSubmit_Raised_Button.setOnClickListener(this);

			setVisibility();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return rootView;
	}

	private void setVisibility() {
		// TODO Auto-generated method stub

		if (EShaktiApplication.isOtherIncomeFragment()) {
			mOthersLayout.setVisibility(View.VISIBLE);
		} else {
			mOthersLayout.setVisibility(View.GONE);
		}

	}

	@SuppressWarnings("deprecation")
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		sIncomeAmounts = new String[sIncomeFields.size()];
		Log.d(TAG, "sIncomeAmounts size : " + sIncomeAmounts.length);

		switch (v.getId()) {
		case R.id.fragment_Submit_button:

			try {

				sIncome_Total = 0;
				isIncome = true;

				confirmArr = new String[mSize];

				mOthersAmount_Values = "";
				sSendToServer_Income = Reset.reset(sSendToServer_Income);

				if (EShaktiApplication.isOtherIncomeFragment()) {

					mOthersAmount_Values = mOthersEditText.getText().toString();

					if (mOthersAmount_Values.equals("") || mOthersAmount_Values == null) {
						mOthersAmount_Values = nullVlaue;
					} else {
						mOthersAmount_Values = mOthersEditText.getText().toString();
					}

					if (mOthersAmount_Values.matches("\\d*\\.?\\d+")) { // match
																		// a
																		// decimal
																		// number

						int amount = (int) Math.round(Double.parseDouble(mOthersAmount_Values));
						mOthersAmount_Values = String.valueOf(amount);
					}

					System.out.println("-------Other AMount Value-----------------" + mOthersAmount_Values + "");
					EShaktiApplication.setOtherAmountValue(mOthersAmount_Values);
				}

				StringBuilder builder = new StringBuilder();

				for (int i = 0; i < sIncomeAmounts.length; i++) {

					sIncomeAmounts[i] = String.valueOf(sIncomeFields.get(i).getText());

					if ((sIncomeAmounts[i].equals("")) || (sIncomeAmounts[i] == null)) {
						sIncomeAmounts[i] = nullVlaue;
					}

					if (sIncomeAmounts[i].matches("\\d*\\.?\\d+")) { // match a
																		// decimal
																		// number

						int amount = (int) Math.round(Double.parseDouble(sIncomeAmounts[i]));
						sIncomeAmounts[i] = String.valueOf(amount);
					}

					sSendToServer_Income = sSendToServer_Income
							+ String.valueOf(SelectedGroupsTask.member_Id.elementAt(i)) + "~" + sIncomeAmounts[i] + "~";

					confirmArr[i] = String.valueOf(SelectedGroupsTask.member_Name.elementAt(i)) + "           "
							+ sIncomeAmounts[i];

					sIncome_Total = sIncome_Total + Integer.parseInt(sIncomeAmounts[i]);

					builder.append(sIncomeAmounts[i]).append(",");
				}

				PrefUtils.setSIPSCValuesKey(builder.toString());

				Log.d(TAG, sSendToServer_Income);

				if (EShaktiApplication.isOtherIncomeFragment()) {
					sIncome_Total = sIncome_Total + Integer.parseInt(mOthersAmount_Values);
				}

				Log.d(TAG, "TOTAL " + String.valueOf(sIncome_Total));

				// Do the SP insertion

				if (sIncome_Total != 0) {

					confirmationDialog = new Dialog(getActivity());

					LayoutInflater inflater = getActivity().getLayoutInflater();
					View dialogView = inflater.inflate(R.layout.dialog_confirmation, null);
					dialogView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
							LayoutParams.WRAP_CONTENT));

					TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
					confirmationHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.confirmation));
					confirmationHeader.setTypeface(LoginActivity.sTypeface);

					TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

					for (int i = 0; i < confirmArr.length; i++) {

						TableRow indv_SavingsRow = new TableRow(getActivity());

						@SuppressWarnings("deprecation")
						TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
								LayoutParams.WRAP_CONTENT, 1f);
						contentParams.setMargins(10, 5, 10, 5);

						TextView memberName_Text = new TextView(getActivity());
						memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
								String.valueOf(SelectedGroupsTask.member_Name.elementAt(i))));
						memberName_Text.setTypeface(LoginActivity.sTypeface);
						memberName_Text.setTextColor(color.black);
						memberName_Text.setPadding(5, 5, 5, 5);
						memberName_Text.setLayoutParams(contentParams);
						indv_SavingsRow.addView(memberName_Text);

						TextView confirm_values = new TextView(getActivity());
						confirm_values
								.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sIncomeAmounts[i])));
						confirm_values.setTextColor(color.black);
						confirm_values.setPadding(5, 5, 5, 5);
						confirm_values.setGravity(Gravity.RIGHT);
						confirm_values.setLayoutParams(contentParams);
						indv_SavingsRow.addView(confirm_values);

						confirmationTable.addView(indv_SavingsRow,
								new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

					}

					if (EShaktiApplication.isOtherIncomeFragment()) {

						TableRow othersRow = new TableRow(getActivity());

						@SuppressWarnings("deprecation")
						TableRow.LayoutParams others_contentParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
								LayoutParams.WRAP_CONTENT, 1f);
						others_contentParams.setMargins(10, 5, 10, 5);

						TextView memberName_Text = new TextView(getActivity());
						memberName_Text
								.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mOthers)));
						memberName_Text.setTypeface(LoginActivity.sTypeface);
						memberName_Text.setTextColor(color.black);
						memberName_Text.setPadding(5, 5, 5, 5);
						memberName_Text.setLayoutParams(others_contentParams);
						othersRow.addView(memberName_Text);

						TextView confirm_values = new TextView(getActivity());
						confirm_values.setText(
								GetSpanText.getSpanString(getActivity(), String.valueOf(mOthersAmount_Values)));
						confirm_values.setTextColor(color.black);
						confirm_values.setPadding(5, 5, 5, 5);
						confirm_values.setGravity(Gravity.RIGHT);
						confirm_values.setLayoutParams(others_contentParams);
						othersRow.addView(confirm_values);

						confirmationTable.addView(othersRow,
								new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

					}

					View rullerView = new View(getActivity());
					rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
					rullerView.setBackgroundColor(Color.rgb(0, 199, 140));// rgb(255,
																			// 229,
																			// 242));
					confirmationTable.addView(rullerView);

					TableRow totalRow = new TableRow(getActivity());

					@SuppressWarnings("deprecation")
					TableRow.LayoutParams totalParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
							LayoutParams.WRAP_CONTENT, 1f);
					totalParams.setMargins(10, 5, 10, 5);

					TextView totalText = new TextView(getActivity());
					totalText.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.total)));
					totalText.setTypeface(LoginActivity.sTypeface);
					totalText.setTextColor(color.black);
					totalText.setPadding(5, 5, 5, 5);// (5, 10, 5, 10);
					totalText.setLayoutParams(totalParams);
					totalRow.addView(totalText);

					TextView totalAmount = new TextView(getActivity());
					totalAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sIncome_Total)));// SavingsFragment.sSavings_Total
					totalAmount.setTextColor(color.black);
					totalAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
					totalAmount.setGravity(Gravity.RIGHT);
					totalAmount.setLayoutParams(totalParams);
					totalRow.addView(totalAmount);

					confirmationTable.addView(totalRow,
							new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

					mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit_button);
					mEdit_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.edit));
					mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
					mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
																			// 205,
																			// 0));
					mEdit_RaisedButton.setOnClickListener(this);

					mOk_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Ok_button);
					mOk_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.yes));
					mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
					mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
					mOk_RaisedButton.setOnClickListener(this);

					confirmationDialog.getWindow()
							.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
					confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					confirmationDialog.setCanceledOnTouchOutside(false);
					confirmationDialog.setContentView(dialogView);
					confirmationDialog.setCancelable(true);
					confirmationDialog.show();

					MarginLayoutParams margin = (MarginLayoutParams) dialogView.getLayoutParams();
					margin.leftMargin = 10;
					margin.rightMargin = 10;
					margin.topMargin = 10;
					margin.bottomMargin = 10;
					margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);
				} else {

					TastyToast.makeText(getActivity(), AppStrings.nullAlert, TastyToast.LENGTH_SHORT,
							TastyToast.WARNING);

					sSendToServer_Income = Reset.reset(sSendToServer_Income);
					sIncome_Total = 0;
					mOthersAmount_Values = Reset.reset(mOthersAmount_Values);

				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

			break;

		case R.id.autoFill:

			String similiar_Income;
			// isValues = true;

			if (mAutoFill.isChecked()) {

				try {

					// Makes all edit fields holds the same savings
					similiar_Income = sIncomeFields.get(0).getText().toString();

					for (int i = 0; i < sIncomeAmounts.length; i++) {
						sIncomeFields.get(i).setText(similiar_Income);
						sIncomeFields.get(i).setGravity(Gravity.RIGHT);
						sIncomeFields.get(i).clearFocus();
						sIncomeAmounts[i] = similiar_Income;

					}

					/** To clear the values of EditFields in case of uncheck **/

				} catch (ArrayIndexOutOfBoundsException e) {
					e.printStackTrace();

					TastyToast.makeText(getActivity(), AppStrings.adminAlert, TastyToast.LENGTH_SHORT,
							TastyToast.WARNING);
				}
			} else {

			}

			break;

		case R.id.autoFill1:

			String similiar_Income1;
			// isValues = true;

			if (mAutoFill1.isChecked()) {

				try {

					// Makes all edit fields holds the same savings
					similiar_Income1 = sIncomeFields.get(0).getText().toString();

					for (int i = 0; i < sIncomeAmounts.length; i++) {
						sIncomeFields.get(i).setText(similiar_Income1);
						sIncomeFields.get(i).setGravity(Gravity.RIGHT);
						sIncomeFields.get(i).clearFocus();
						sIncomeAmounts[i] = similiar_Income1;

					}

					/** To clear the values of EditFields in case of uncheck **/

				} catch (ArrayIndexOutOfBoundsException e) {
					e.printStackTrace();

					TastyToast.makeText(getActivity(), AppStrings.adminAlert, TastyToast.LENGTH_SHORT,
							TastyToast.WARNING);
				}
			} else {

			}

			break;

		case R.id.fragment_Edit_button:

			sSendToServer_Income = Reset.reset(sSendToServer_Income);
			sIncome_Total = Integer.valueOf(nullVlaue);
			mSubmit_Raised_Button.setClickable(true);

			mOthersAmount_Values = Reset.reset(mOthersAmount_Values);
			isServiceCall = false;
			confirmationDialog.dismiss();
			break;

		case R.id.fragment_Ok_button:

			if (ConnectionUtils.isNetworkAvailable(getActivity())) {

				if (Transaction_IncomeMenuFragment.sSelectedIncomeMenu.equals(AppStrings.otherincome)) {

					try {

						if (EShaktiApplication.getLoginFlag().equals(Constants.ONLINEFLAG)) {

							if (!isServiceCall) {
								isServiceCall = true;

								new Get_OtherIncomeTask(Transaction_OI_S_P_Fragment.this).execute();
							}
						} else {
							TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
									TastyToast.ERROR);
						}
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}

				} else if (Transaction_IncomeMenuFragment.sSelectedIncomeMenu.equals(AppStrings.subscriptioncharges)) {

					try {

						if (EShaktiApplication.getLoginFlag().equals(Constants.ONLINEFLAG)) {

							if (!isServiceCall) {
								isServiceCall = true;
								new Get_Subscription_Charges(Transaction_OI_S_P_Fragment.this).execute();
							}

						} else {
							TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
									TastyToast.ERROR);
						}

					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
				} else if (Transaction_IncomeMenuFragment.sSelectedIncomeMenu.equals(AppStrings.penalty)) {

					try {

						if (EShaktiApplication.getLoginFlag().equals(Constants.ONLINEFLAG)) {

							if (!isServiceCall) {
								isServiceCall = true;
								new Get_PenaltyTask(Transaction_OI_S_P_Fragment.this).execute();
							}

						} else {
							TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
									TastyToast.ERROR);
						}
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
				}
			} else {
				// Do offline Stuffs
				if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
					if (Transaction_IncomeMenuFragment.sSelectedIncomeMenu.equals(AppStrings.otherincome)) {

						EShaktiApplication.getInstance().getTransactionManager().startTransaction(
								DataType.GET_TRANS_SINGLE, new GetTransactionSingle(PrefUtils.getOfflineUniqueID()));

					} else if (Transaction_IncomeMenuFragment.sSelectedIncomeMenu
							.equals(AppStrings.subscriptioncharges)) {
						EShaktiApplication.getInstance().getTransactionManager().startTransaction(
								DataType.GET_TRANS_SINGLE, new GetTransactionSingle(PrefUtils.getOfflineUniqueID()));

					} else if (Transaction_IncomeMenuFragment.sSelectedIncomeMenu.equals(AppStrings.penalty)) {
						EShaktiApplication.getInstance().getTransactionManager().startTransaction(
								DataType.GET_TRANS_SINGLE, new GetTransactionSingle(PrefUtils.getOfflineUniqueID()));

					}

				} else {
					TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
							TastyToast.ERROR);
				}
			}
			break;
		default:
			break;
		}
	}

	@Subscribe
	public void OnGetSingleTransaction(final GetSingleTransResponse getSingleTransResponse) {
		switch (getSingleTransResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), getSingleTransResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), getSingleTransResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			try {
				String mUniqueId = GetTransactionSinglevalues.getUniqueId();
				String mIncomeValues = null, mTrasactiondate = null, mMobileDate = null, mTransaction_UniqueId = null,
						mOtherincomevalues = null, mSubscriptionIncomevalues = null, mPenaltyIncomevalues = null;

				mLastTrDate = DatePickerDialog.sDashboardDate;
				mLastTr_ID = SelectedGroupsTask.sTr_ID.toString();

				String mCurrentTransDate = null;

				if (publicValues.mOffline_Trans_CurrentDate != null) {
					mCurrentTransDate = publicValues.mOffline_Trans_CurrentDate;
				}

				if (Transaction_IncomeMenuFragment.sSelectedIncomeMenu.equals(AppStrings.otherincome)) {
					mIncomeValues = GetTransactionSinglevalues.getOtherIncome();
					mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
					mMobileDate = Get_Offline_MobileDate.getOffline_MobileDate(getActivity());
					mTransaction_UniqueId = PrefUtils.getOfflineUniqueID();
					mOtherincomevalues = sSendToServer_Income + "#" + mTrasactiondate + "#" + mMobileDate + "#"
							+ EShaktiApplication.getOtherAmountValue() + "$";
					Log.e("Fragment Saving Values", mOtherincomevalues);
				} else if (Transaction_IncomeMenuFragment.sSelectedIncomeMenu.equals(AppStrings.subscriptioncharges)) {
					mIncomeValues = GetTransactionSinglevalues.getSubscriptionIncome();

					mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
					mMobileDate = Get_Offline_MobileDate.getOffline_MobileDate(getActivity());
					mTransaction_UniqueId = PrefUtils.getOfflineUniqueID();
					mSubscriptionIncomevalues = sSendToServer_Income + "#" + mTrasactiondate + "#" + mMobileDate + "$";
					Log.e("Fragment Saving Values", mSubscriptionIncomevalues);
				} else if (Transaction_IncomeMenuFragment.sSelectedIncomeMenu.equals(AppStrings.penalty)) {
					mIncomeValues = GetTransactionSinglevalues.getPenaltyIncome();
					mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
					mMobileDate = Get_Offline_MobileDate.getOffline_MobileDate(getActivity());
					mTransaction_UniqueId = PrefUtils.getOfflineUniqueID();
					mPenaltyIncomevalues = sSendToServer_Income + "#" + mTrasactiondate + "#" + mMobileDate + "$";

					Log.e("Fragment Saving Values", mPenaltyIncomevalues);

				}

				Log.v("ContentValues", "Step 2");

				System.out.println("-----------OnGetSingleTransaction----------");

				Log.e("mCurrentTransDate", mCurrentTransDate + "");

				if (Transaction_IncomeMenuFragment.sSelectedIncomeMenu.equals(AppStrings.otherincome)) {

					mSubscriptionIncomevalues = "null";
					mPenaltyIncomevalues = "null";
				} else if (Transaction_IncomeMenuFragment.sSelectedIncomeMenu.equals(AppStrings.subscriptioncharges)) {
					mOtherincomevalues = "null";
					mPenaltyIncomevalues = "null";
				} else if (Transaction_IncomeMenuFragment.sSelectedIncomeMenu.equals(AppStrings.penalty)) {
					mSubscriptionIncomevalues = "null";
					mOtherincomevalues = "null";
				}

				if (!mOtherincomevalues.contains("?") && !mSubscriptionIncomevalues.contains("?")
						&& !mPenaltyIncomevalues.contains("?") && !mCurrentTransDate.contains("?")
						&& !mTransaction_UniqueId.contains("?")) {

					mSqliteDBStoredValues_mOtherincomevalues = mOtherincomevalues;
					mSqliteDBStoredValues_mSubscriptionIncomevalues = mSubscriptionIncomevalues;
					mSqliteDBStoredValues_mPenaltyIncomevalues = mPenaltyIncomevalues;

					if (mUniqueId == null && mIncomeValues == null) {

						Log.v("ContentValues", "Step 3");
						if (mLastTrDate != null && mMobileDate != null && mTransaction_UniqueId != null
								&& Transaction_IncomeMenuFragment.sSelectedIncomeMenu.equals(AppStrings.otherincome)
								&& mCurrentTransDate != null) {
							Log.e("mOtherincomevalues", mOtherincomevalues + "");
							EShaktiApplication.getInstance().getTransactionManager().startTransaction(
									DataType.TRANSACTIONADD,
									new Transaction(0, PrefUtils.getUsernameKey(), SelectedGroupsTask.UserName,
											SelectedGroupsTask.Ngo_Id, SelectedGroupsTask.Group_Id,
											mTransaction_UniqueId, mLastTr_ID, mCurrentTransDate, null, null, null,
											null, null, mOtherincomevalues, null, null, null, null, null, null, null,
											null, null, null, null, null, null, null, null, null));
						} else if (mLastTrDate != null && mMobileDate != null && mTransaction_UniqueId != null
								&& Transaction_IncomeMenuFragment.sSelectedIncomeMenu
										.equals(AppStrings.subscriptioncharges)
								&& mCurrentTransDate != null) {
							Log.e("mSubscriptionIncomevalues", mSubscriptionIncomevalues + "");
							EShaktiApplication.getInstance().getTransactionManager().startTransaction(
									DataType.TRANSACTIONADD,
									new Transaction(0, PrefUtils.getUsernameKey(), SelectedGroupsTask.UserName,
											SelectedGroupsTask.Ngo_Id, SelectedGroupsTask.Group_Id,
											mTransaction_UniqueId, mLastTr_ID, mCurrentTransDate, null, null, null,
											null, null, null, mSubscriptionIncomevalues, null, null, null, null, null,
											null, null, null, null, null, null, null, null, null, null));
						} else if (mLastTrDate != null && mMobileDate != null && mTransaction_UniqueId != null
								&& Transaction_IncomeMenuFragment.sSelectedIncomeMenu.equals(AppStrings.penalty)
								&& mCurrentTransDate != null) {
							Log.e("mPenaltyIncomevalues", mPenaltyIncomevalues + "");
							EShaktiApplication.getInstance().getTransactionManager().startTransaction(
									DataType.TRANSACTIONADD,
									new Transaction(0, PrefUtils.getUsernameKey(), SelectedGroupsTask.UserName,
											SelectedGroupsTask.Ngo_Id, SelectedGroupsTask.Group_Id,
											mTransaction_UniqueId, mLastTr_ID, mCurrentTransDate, null, null, null,
											null, null, null, null, mPenaltyIncomevalues, null, null, null, null, null,
											null, null, null, null, null, null, null, null, null));

						}

					} else if (mUniqueId.equalsIgnoreCase(PrefUtils.getOfflineUniqueID())) {
						if (Transaction_IncomeMenuFragment.sSelectedIncomeMenu.equals(AppStrings.otherincome)
								&& mIncomeValues == null) {
							EShaktiApplication.setSetTransValues(TransactionOffConstants.TRANS_OTH);

							EShaktiApplication.getInstance().getTransactionManager().startTransaction(
									DataType.TRANS_VALUES_UPDATE, new TransactionUpdate(mUniqueId, mOtherincomevalues));
						} else if (Transaction_IncomeMenuFragment.sSelectedIncomeMenu
								.equals(AppStrings.subscriptioncharges) && mIncomeValues == null) {
							EShaktiApplication.setSetTransValues(TransactionOffConstants.TRANS_SUB);

							EShaktiApplication.getInstance().getTransactionManager().startTransaction(
									DataType.TRANS_VALUES_UPDATE,
									new TransactionUpdate(mUniqueId, mSubscriptionIncomevalues));
						} else if (Transaction_IncomeMenuFragment.sSelectedIncomeMenu.equals(AppStrings.penalty)
								&& mIncomeValues == null) {
							EShaktiApplication.setSetTransValues(TransactionOffConstants.TRANS_PEN);

							EShaktiApplication.getInstance().getTransactionManager().startTransaction(
									DataType.TRANS_VALUES_UPDATE,
									new TransactionUpdate(mUniqueId, mPenaltyIncomevalues));
						} else if (mIncomeValues != null) {

							confirmationDialog.dismiss();
							TastyToast.makeText(getActivity(), AppStrings.offlineDataAvailable, TastyToast.LENGTH_SHORT,
									TastyToast.WARNING);
							System.out.println("---------------------");
							DatePickerDialog.sDashboardDate = SelectedGroupsTask.sLastTransactionDate_Response;
							MainFragment_Dashboard fragment = new MainFragment_Dashboard();
							setFragment(fragment);
						}
					}
				} else {
					if (confirmationDialog.isShowing() && confirmationDialog != null) {
						confirmationDialog.dismiss();
					}

					TastyToast.makeText(getActivity(), "Please Try Again", TastyToast.LENGTH_SHORT, TastyToast.ERROR);

					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		}
	}

	@Subscribe
	public void OnTransUpdate(final TransactionUpdateResponse transactionUpdateResponse) {
		switch (transactionUpdateResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), transactionUpdateResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), transactionUpdateResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			try {
				boolean mTransactionSuccess = false;
				TransactionProvider.getSinlgeGroupMaster_Transaction(
						new Transaction_UniqueIdSingle(PrefUtils.getOfflineUniqueID()));

				if (Transaction_IncomeMenuFragment.sSelectedIncomeMenu.equals(AppStrings.otherincome)) {
					if (GetTransactionSinglevalues.getOtherIncome() != null
							&& GetTransactionSinglevalues.getOtherIncome()
									.equals(mSqliteDBStoredValues_mOtherincomevalues)
							&& !GetTransactionSinglevalues.getOtherIncome().equals("")) {
						mTransactionSuccess = true;
					} else {
						mTransactionSuccess = false;
					}

				} else if (Transaction_IncomeMenuFragment.sSelectedIncomeMenu.equals(AppStrings.subscriptioncharges)) {

					if (GetTransactionSinglevalues.getSubscriptionIncome() != null
							&& GetTransactionSinglevalues.getSubscriptionIncome()
									.equals(mSqliteDBStoredValues_mSubscriptionIncomevalues)
							&& !GetTransactionSinglevalues.getSubscriptionIncome().equals("")) {
						mTransactionSuccess = true;
					} else {
						mTransactionSuccess = false;
					}

				} else if (Transaction_IncomeMenuFragment.sSelectedIncomeMenu.equals(AppStrings.penalty)) {

					if (GetTransactionSinglevalues.getPenaltyIncome() != null
							&& GetTransactionSinglevalues.getPenaltyIncome()
									.equals(mSqliteDBStoredValues_mPenaltyIncomevalues)
							&& !GetTransactionSinglevalues.getPenaltyIncome().equals("")) {
						mTransactionSuccess = true;
					} else {
						mTransactionSuccess = false;
					}

				}

				if (mTransactionSuccess) {
					confirmationDialog.dismiss();
					Log.e("OISP Insert Sucessfully", "Sucess");
					Log.e("Transaction Values for Bank Details", GetOfflineTransactionValues.getBankDetails());

					String mCashinHand = String
							.valueOf(Integer.parseInt(SelectedGroupsTask.sCashinHand) + sIncome_Total);
					String mCashatBank = SelectedGroupsTask.sCashatBank;
					String mBankDetails = GetOfflineTransactionValues.getBankDetails();
					SelectedGroupsTask.sCashinHand = mCashinHand;
					String mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
					SelectedGroupsTask.sLastTransactionDate_Response = mTrasactiondate;

					String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mTrasactiondate,
							mCashinHand, mCashatBank, mBankDetails);
					String mSelectedGroupId = SelectedGroupsTask.Group_Id;

					System.out.println("---------OnTransUpdate----------");
					Log.e("mGroupMasterResponse	", mGroupMasterResponse);
					System.out.println("-------------------");
					EShaktiApplication.getInstance().getTransactionManager().startTransaction(
							DataType.GROUPMASTERUPDATE, new GroupDetailsUpdate(mSelectedGroupId, mGroupMasterResponse));

				} else {
					if (confirmationDialog.isShowing() && confirmationDialog != null) {
						confirmationDialog.dismiss();
					}

					TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT, TastyToast.ERROR);

					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);

				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		}
	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub
		mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
		mProgressDilaog.show();
	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub

		if (mProgressDilaog != null) {
			mProgressDilaog.dismiss();
			if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {
				getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub

						if (!result.equals("FAIL")) {
							isServiceCall = false;
							TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg,
									TastyToast.LENGTH_SHORT, TastyToast.ERROR);
							Constants.NETWORKCOMMONFLAG = "SUCCESS";
						}

					}
				});

			} else {
				try {
					if (isGetTrid) {
						isGetTrid = false;
						callOfflineDataUpdate();
					} else {

						if (GetResponseInfo.sResponseUpdate[0].equals("Yes")) {

							new Get_LastTransactionID(Transaction_OI_S_P_Fragment.this).execute();
							isGetTrid = true;

						} else {

							TastyToast.makeText(getActivity(), AppStrings.transactionFailAlert, TastyToast.LENGTH_SHORT,
									TastyToast.ERROR);
							MainFragment_Dashboard fragment = new MainFragment_Dashboard();
							setFragment(fragment);
						}
					}
					confirmationDialog.dismiss();

				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		}

	}

	private void callOfflineDataUpdate() {
		// TODO Auto-generated method stub
		String mCashinHand = SelectedGroupsTask.sCashinHand;
		String mCashatBank = SelectedGroupsTask.sCashatBank;
		String mBankDetails = GetOfflineTransactionValues.getBankDetails();
		mLastTrDate = SelectedGroupsTask.sLastTransactionDate_Response;

		String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mLastTrDate, mCashinHand, mCashatBank,
				mBankDetails);
		String mSelectedGroupId = SelectedGroupsTask.Group_Id;
		isServiceCall = false;
		EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GROUPMASTERUPDATE,
				new GroupDetailsUpdate(mSelectedGroupId, mGroupMasterResponse));
	}

	@Subscribe
	public void onAddTransactionIncome(final TransactionResponse transactionResponse) {
		switch (transactionResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), transactionResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), transactionResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:
			try {
				boolean mTransactionSuccess = false;
				TransactionProvider.getSinlgeGroupMaster_Transaction(
						new Transaction_UniqueIdSingle(PrefUtils.getOfflineUniqueID()));

				if (Transaction_IncomeMenuFragment.sSelectedIncomeMenu.equals(AppStrings.otherincome)) {
					if (GetTransactionSinglevalues.getOtherIncome() != null
							&& GetTransactionSinglevalues.getOtherIncome()
									.equals(mSqliteDBStoredValues_mOtherincomevalues)
							&& !GetTransactionSinglevalues.getOtherIncome().equals("")) {
						mTransactionSuccess = true;
					} else {
						mTransactionSuccess = false;
					}

				} else if (Transaction_IncomeMenuFragment.sSelectedIncomeMenu.equals(AppStrings.subscriptioncharges)) {

					if (GetTransactionSinglevalues.getSubscriptionIncome() != null
							&& GetTransactionSinglevalues.getSubscriptionIncome()
									.equals(mSqliteDBStoredValues_mSubscriptionIncomevalues)
							&& !GetTransactionSinglevalues.getSubscriptionIncome().equals("")) {
						mTransactionSuccess = true;
					} else {
						mTransactionSuccess = false;
					}

				} else if (Transaction_IncomeMenuFragment.sSelectedIncomeMenu.equals(AppStrings.penalty)) {

					if (GetTransactionSinglevalues.getPenaltyIncome() != null
							&& GetTransactionSinglevalues.getPenaltyIncome()
									.equals(mSqliteDBStoredValues_mPenaltyIncomevalues)
							&& !GetTransactionSinglevalues.getPenaltyIncome().equals("")) {
						mTransactionSuccess = true;
					} else {
						mTransactionSuccess = false;
					}

				}

				if (mTransactionSuccess) {

					String mCashinHand = String
							.valueOf(Integer.parseInt(SelectedGroupsTask.sCashinHand) + sIncome_Total);
					String mCashatBank = SelectedGroupsTask.sCashatBank;
					String mBankDetails = GetOfflineTransactionValues.getBankDetails();
					SelectedGroupsTask.sCashinHand = mCashinHand;
					String mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
					SelectedGroupsTask.sLastTransactionDate_Response = mTrasactiondate;
					String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mTrasactiondate,
							mCashinHand, mCashatBank, mBankDetails);
					String mSelectedGroupId = SelectedGroupsTask.Group_Id;
					System.out.println("------onAddTransactionIncome------");
					Log.e("mGroupMasterResponse", mGroupMasterResponse + "");
					EShaktiApplication.getInstance().getTransactionManager().startTransaction(
							DataType.GROUPMASTERUPDATE, new GroupDetailsUpdate(mSelectedGroupId, mGroupMasterResponse));
				} else {
					if (confirmationDialog.isShowing() && confirmationDialog != null) {
						confirmationDialog.dismiss();
					}

					TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT, TastyToast.ERROR);

					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);

				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		}
	}

	@Subscribe
	public void OnGroupMasIncomeUpdate(final GroupMasUpdateResponse masterResponse) {
		switch (masterResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), masterResponse.getFetcherResult().getMessage(), TastyToast.LENGTH_SHORT,
					TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), masterResponse.getFetcherResult().getMessage(), TastyToast.LENGTH_SHORT,
					TastyToast.ERROR);
			break;
		case SUCCESS:

			try {
				confirmationDialog.dismiss();
				System.out.println("------OnGroupMasIncomeUpdate------");
				TastyToast.makeText(getActivity(), AppStrings.transactionCompleted, TastyToast.LENGTH_SHORT,
						TastyToast.SUCCESS);

				if (!ConnectionUtils.isNetworkAvailable(getActivity())) {

					String mValues = Put_DB_GroupNameDetail_Response
							.put_DB_GroupNameDetails_Response(EShaktiApplication.getGroupResponse());

					GroupProvider.updateGroupResponse(
							new GroupResponseUpdate(EShaktiApplication.getGroupId_GroupLastTransDate(), mValues));
				}

				MainFragment_Dashboard fragment = new MainFragment_Dashboard();
				setFragment(fragment);

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		}
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		SBus.INST.unRegister(this);
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub

		Log.e("On pause", "On Fragment---------------->>>>>");

		/*
		 * FragmentManager fm = getActivity().getSupportFragmentManager(); for(int i =
		 * 0; i < fm.getBackStackEntryCount(); ++i) { fm.popBackStack(); }
		 */
		super.onPause();
	}

	private void setFragment(Fragment fragment) {

		Log.e("Current Fragment--------->>>", fragment.toString());
		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

		FragmentDrawer.drawer_CashinHand.setText(RegionalConversion.getRegionalConversion(AppStrings.cashinhand)

				+ SelectedGroupsTask.sCashinHand);
		FragmentDrawer.drawer_CashinHand.setTypeface(LoginActivity.sTypeface);

		FragmentDrawer.drawer_CashatBank.setText(RegionalConversion.getRegionalConversion(AppStrings.cashatBank)

				+ SelectedGroupsTask.sCashatBank);
		FragmentDrawer.drawer_CashatBank.setTypeface(LoginActivity.sTypeface);

		FragmentDrawer.drawer_lastTR_Date
				.setText(RegionalConversion.getRegionalConversion(AppStrings.lastTransactionDate) + " : "
						+ SelectedGroupsTask.sLastTransactionDate_Response);
		FragmentDrawer.drawer_lastTR_Date.setTypeface(LoginActivity.sTypeface);

		UpdateCalendarMinMaxDateUtils.OnUpdateCalendarDate();
		
		Calendar calender = Calendar.getInstance();

		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String formattedDate = df.format(calender.getTime());
		EShaktiApplication.setLastTransDate_GroupId(SelectedGroupsTask.Group_Id);

		new GroupTempDBLastTransDate(EShaktiApplication.getLastTransDate_GroupId(),
				SelectedGroupsTask.sLastTransactionDate_Response, formattedDate);

		GroupProvider.updateGroupLastTransDateResponse();
		
	}

}
