package com.yesteam.eshakti.view.fragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.squareup.otto.Subscribe;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.TransactionOffConstants;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;
import com.yesteam.eshakti.sqlite.database.GroupDetailsProvider;
import com.yesteam.eshakti.sqlite.database.GroupProvider;
import com.yesteam.eshakti.sqlite.database.response.GetSingleTransResponse;
import com.yesteam.eshakti.sqlite.database.response.GroupMasUpdateResponse;
import com.yesteam.eshakti.sqlite.database.response.TransactionResponse;
import com.yesteam.eshakti.sqlite.database.response.TransactionUpdateResponse;
import com.yesteam.eshakti.sqlite.db.model.GetTransactionSingle;
import com.yesteam.eshakti.sqlite.db.model.GetTransactionSinglevalues;
import com.yesteam.eshakti.sqlite.db.model.GroupDetailsUpdate;
import com.yesteam.eshakti.sqlite.db.model.GroupResponseUpdate;
import com.yesteam.eshakti.sqlite.db.model.GroupTempDBLastTransDate;
import com.yesteam.eshakti.sqlite.db.model.Transaction;
import com.yesteam.eshakti.sqlite.db.model.TransactionUpdate;
import com.yesteam.eshakti.sqlite.db.transactions.TransactionManager.DataType;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.utils.GetOfflineTransactionValues;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.utils.Get_Offline_MobileDate;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.utils.Put_DB_GroupNameDetail_Response;
import com.yesteam.eshakti.utils.Put_DB_GroupResponse;
import com.yesteam.eshakti.utils.Reset;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.utils.UpdateCalendarMinMaxDateUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.Get_Group_Minutes_NewTask;
import com.yesteam.eshakti.webservices.Get_MinutesTask;
import com.yesteam.eshakti.webservices.Get_Minutes_LanguageWebservice;
import com.yesteam.eshakti.webservices.Get_PurposeOfLoanTask;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Meeting_MinutesOfMeetingFragment extends Fragment implements OnClickListener, TaskListener {

	public static final String TAG = Meeting_MinutesOfMeetingFragment.class.getSimpleName();
	private TextView mGroupName, mCashInHand, mCashAtBank, mHeadertext;
	private Button mRaised_Submit_Button;
	private Button mEdit_RaisedButton, mOk_RaisedButton;
	private Dialog mProgressDialog;
	LinearLayout mLayout;

	int size;
	public static CheckBox sCheckBox[];
	public static List<CheckBox> sAllCbs = new ArrayList<CheckBox>();
	public static String Send_to_server_values = "";
	public static String tempvaues;
	String mLastTrDate = null, mLastTr_ID = null;
	String responseMinutes[], response_MinutesID[];
	Dialog confirmationDialog;
	Button mPerviousButton, mNextButton;
	boolean isNextButton = false;
	boolean isGetBankLoan = false;
	boolean isMinutesLanguage = false;
	String mMinutesLangValues = null;
	boolean iSPurposeLoanTask = false;
	String[] responseArr;
	TableLayout minutesTable;
	boolean mEngMinutesLanguage = false;
	boolean isServiceCall = false;

	public Meeting_MinutesOfMeetingFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_MINUTESOFMEETING;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_minutesofmeeting, container, false);

		MainFragment_Dashboard.isBackpressed = false;

		try {

			Send_to_server_values = Reset.reset(Send_to_server_values);
			tempvaues = Reset.reset(tempvaues);
			sAllCbs.clear();

			/**
			 * Get_Group_MinutesTask.sGet_Group_Minutes_Response - Holds the minutes
			 * response from WebService like Minutes ID ~MinutesValues ~Minutes ID~Minutes
			 * values~...
			 **/
			String group_min_response = Get_Group_Minutes_NewTask.sGet_Group_Minutes__new_Response;
			Log.e("Minutes of meeting------->>>>>", group_min_response);
			responseArr = group_min_response.split("#");

			String response[] = responseArr[0].split("~");

			String id = "", minutesValues = "";

			for (int i = 0; i < response.length; i++) {

				if (i % 2 == 0) {
					id = id + response[i] + "~";
				} else if (i % 2 != 0) {
					minutesValues = minutesValues + response[i] + "~";
				}

			}

			response_MinutesID = id.split("~");

			responseMinutes = minutesValues.split("~");
			System.out.println("ID : " + id);
			System.out.println("RESPONSE- MINUTES : " + minutesValues);

			size = responseMinutes.length;
			System.out.println("length of MinutesOfMeeting:" + size);
			if (ConnectionUtils.isNetworkAvailable(getActivity())) {
				if (!PrefUtils.getUserlangcode().equals("English")) {

					new Get_Minutes_LanguageWebservice(Meeting_MinutesOfMeetingFragment.this).execute();
					isMinutesLanguage = true;
				} else {
					mMinutesLangValues = "Decided to get the BankLoan";
					mEngMinutesLanguage = true;
				}
			} else {
				mEngMinutesLanguage = true;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashInHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashInHand.setTypeface(LoginActivity.sTypeface);

			mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashAtBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashAtBank.setTypeface(LoginActivity.sTypeface);

			mHeadertext = (TextView) rootView.findViewById(R.id.fragment_attendance_minutes_headertext);
			mHeadertext.setText(RegionalConversion.getRegionalConversion(AppStrings.MinutesofMeeting));
			mHeadertext.setTypeface(LoginActivity.sTypeface);

			mPerviousButton = (Button) rootView.findViewById(R.id.fragment_minutesPreviousbutton_default);
			mPerviousButton.setText(RegionalConversion.getRegionalConversion(AppStrings.mPervious));
			mPerviousButton.setTypeface(LoginActivity.sTypeface);
			mPerviousButton.setOnClickListener(this);

			mRaised_Submit_Button = (Button) rootView.findViewById(R.id.fragment_attendance_minutes_Submitbutton);
			mRaised_Submit_Button.setText(RegionalConversion.getRegionalConversion(AppStrings.submit));
			mRaised_Submit_Button.setTypeface(LoginActivity.sTypeface);
			mRaised_Submit_Button.setOnClickListener(this);

			mNextButton = (Button) rootView.findViewById(R.id.fragment_minutesNextbutton_default);
			mNextButton.setText(RegionalConversion.getRegionalConversion(AppStrings.mNext));
			mNextButton.setTypeface(LoginActivity.sTypeface);
			mNextButton.setOnClickListener(this);
			if (EShaktiApplication.isDefault()) {
				mPerviousButton.setVisibility(View.INVISIBLE);
				mNextButton.setVisibility(View.INVISIBLE);
			}

			mLayout = (LinearLayout) rootView.findViewById(R.id.linearLayout);
			sCheckBox = new CheckBox[size];

			minutesTable = (TableLayout) rootView.findViewById(R.id.minutesTable);

			if (mEngMinutesLanguage) {
				mEngMinutesLanguage = false;

				@SuppressWarnings("deprecation")
				TableLayout.LayoutParams params = new TableLayout.LayoutParams(TableLayout.LayoutParams.FILL_PARENT,
						TableLayout.LayoutParams.WRAP_CONTENT, 1f);
				params.setMargins(20, 5, 5, 5);

				for (int i = 0; i < responseMinutes.length; i++) {
					TableRow indv_row = new TableRow(getActivity());

					indv_row.setLayoutParams(params);

					sCheckBox[i] = new CheckBox(getActivity());
					sCheckBox[i].setBackgroundResource(R.drawable.btn_check_to_off_mtrl_013);
					sCheckBox[i].setChecked(false);
					indv_row.addView(sCheckBox[i]);

					TableRow.LayoutParams textParams = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,
							LayoutParams.WRAP_CONTENT, 1f);
					textParams.setMargins(0, 0, 5, 15);
					TextView minutesOfMeeting = new TextView(getActivity());

					if (ConnectionUtils.isNetworkAvailable(getActivity())) {

						if (responseArr[1].equals("Yes")) {
							if (responseMinutes[i].equals(mMinutesLangValues)) {
								sCheckBox[i].setEnabled(true);
							}
						} else {
							if (responseMinutes[i].equals(mMinutesLangValues)) {
								sCheckBox[i].setEnabled(false);
							}
						}
					}
					minutesOfMeeting
							.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(responseMinutes[i])));
					minutesOfMeeting.setTypeface(LoginActivity.sTypeface);
					minutesOfMeeting.setTextColor(color.black);
					minutesOfMeeting.setSingleLine(false);
					minutesOfMeeting.setLayoutParams(textParams);
					indv_row.addView(minutesOfMeeting);

					minutesTable.addView(indv_row);
				}

			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return rootView;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {
		case R.id.fragment_attendance_minutes_Submitbutton:

			try {

				Send_to_server_values = Reset.reset(Send_to_server_values);
				tempvaues = Reset.reset(tempvaues);

				for (int i = 0; i < size; i++) {
					if (sCheckBox[i].isChecked()) {

						Send_to_server_values = Send_to_server_values + response_MinutesID[i] + "~";
						System.out.println("Send to server value:" + Send_to_server_values);
						tempvaues = tempvaues + responseMinutes[i].toString() + "~";
					}
				}

				if (null != Send_to_server_values && Send_to_server_values.length() > 0) {
					int endIndex = Send_to_server_values.lastIndexOf("~");
					if (endIndex != -1) {
						Send_to_server_values = Send_to_server_values.substring(0, endIndex); // not
																								// forgot
																								// to
																								// put
																								// check
																								// if(endIndex
																								// !=
																								// -1)
					}
				}
				String valuesArr[] = tempvaues.split("~");

				if (!tempvaues.equals("")) {
					// Goto Confirmation page

					confirmationDialog = new Dialog(getActivity());

					LayoutInflater inflater = getActivity().getLayoutInflater();
					View dialogView = inflater.inflate(R.layout.dialog_confirmation, null);
					dialogView.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
							LayoutParams.WRAP_CONTENT));

					TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
					confirmationHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.confirmation));
					confirmationHeader.setTypeface(LoginActivity.sTypeface);

					TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

					try {

						System.out.println("valueArr size:" + valuesArr.length);
						CheckBox checkBox[] = new CheckBox[valuesArr.length];

						for (int j = 0; j < valuesArr.length; j++) {
							TableRow indv_valueRow = new TableRow(getActivity());

							TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
									LayoutParams.WRAP_CONTENT, 1f);
							contentParams.setMargins(10, 5, 10, 5);

							checkBox[j] = new CheckBox(getActivity());
							Log.d(TAG, "Values >>>> :" + valuesArr[j]);
							checkBox[j].setText(GetSpanText.getSpanString(getActivity(), String.valueOf(valuesArr[j])));
							checkBox[j].setChecked(true);
							checkBox[j].setClickable(false);
							checkBox[j].setTextColor(color.black);
							checkBox[j].setTypeface(LoginActivity.sTypeface);
							indv_valueRow.addView(checkBox[j], contentParams);

							if (valuesArr[j].equals(mMinutesLangValues)) {
								isGetBankLoan = true;

							}
							confirmationTable.addView(indv_valueRow,
									new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
						}

					} catch (Exception e) {
						// TODO: handle exception
						System.out.println("checkbox layout error:" + e.toString());
					}

					mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit_button);
					mEdit_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.edit));
					mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
					mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
																			// 205,
																			// 0));
					mEdit_RaisedButton.setOnClickListener(this);

					mOk_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Ok_button);
					mOk_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.yes));
					mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
					mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
					mOk_RaisedButton.setOnClickListener(this);

					confirmationDialog.getWindow()
							.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
					confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					confirmationDialog.setCanceledOnTouchOutside(false);
					confirmationDialog.setContentView(dialogView);
					confirmationDialog.setCancelable(true);
					confirmationDialog.show();

					MarginLayoutParams margin = (MarginLayoutParams) dialogView.getLayoutParams();
					margin.leftMargin = 10;
					margin.rightMargin = 10;
					margin.topMargin = 10;
					margin.bottomMargin = 10;
					margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);
				} else {

					TastyToast.makeText(getActivity(), AppStrings.MinutesAlert, TastyToast.LENGTH_SHORT,
							TastyToast.WARNING);
					Send_to_server_values = Reset.reset(Send_to_server_values);
					tempvaues = Reset.reset(tempvaues);
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			break;
		case R.id.fragment_Edit_button:
			mRaised_Submit_Button.setClickable(true);

			isGetBankLoan = false;

			tempvaues = "";
			Send_to_server_values = Reset.reset(Send_to_server_values);
			confirmationDialog.dismiss();
			isServiceCall = false;
			break;
		case R.id.fragment_Ok_button:

			if (ConnectionUtils.isNetworkAvailable(getActivity())) {
				// Internet is present
				try {
					if (EShaktiApplication.getLoginFlag().equals(Constants.ONLINEFLAG)) {
						if (!isServiceCall) {
							isServiceCall = true;
							new Get_MinutesTask(Meeting_MinutesOfMeetingFragment.this).execute();
						}

					} else {
						TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
								TastyToast.ERROR);
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			} else {
				// Connection is not present
				if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
					EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GET_TRANS_SINGLE,
							new GetTransactionSingle(PrefUtils.getOfflineUniqueID()));
				} else {
					TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
							TastyToast.ERROR);
				}
			}

			break;
		case R.id.fragment_minutesPreviousbutton_default:

			Transaction_InternalLoan_DisbursementFragment defaultReportsFragment = new Transaction_InternalLoan_DisbursementFragment();
			setFragment(defaultReportsFragment);
			break;
		case R.id.fragment_minutesNextbutton_default:

			isNextButton = true;
			DefaultReportsFragment fragment_Dashboard = new DefaultReportsFragment();
			setFragment(fragment_Dashboard);
			break;
		default:
			break;
		}

	}

	@Subscribe
	public void OnGetSingleTransactionMeeting(final GetSingleTransResponse getSingleTransResponse) {
		switch (getSingleTransResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), getSingleTransResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), getSingleTransResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			String mUniqueId = GetTransactionSinglevalues.getUniqueId();
			String mMeetingValues_Offline = GetTransactionSinglevalues.getMinutesofmeeting();
			mLastTrDate = DatePickerDialog.sDashboardDate;
			String mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
			String mMobileDate = Get_Offline_MobileDate.getOffline_MobileDate(getActivity());
			String mTransaction_UniqueId = PrefUtils.getOfflineUniqueID();

			mLastTr_ID = SelectedGroupsTask.sTr_ID.toString();

			String mCurrentTransDate = null;

			if (publicValues.mOffline_Trans_CurrentDate != null) {
				mCurrentTransDate = publicValues.mOffline_Trans_CurrentDate;
			}

			String mMinutesofMeetingvalues = Send_to_server_values + "#" + mTrasactiondate + "#" + mMobileDate;

			if (mUniqueId == null && mMeetingValues_Offline == null) {

				if (mLastTrDate != null && mMobileDate != null && mTransaction_UniqueId != null
						&& mCurrentTransDate != null) {
					EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.TRANSACTIONADD,
							new Transaction(0, PrefUtils.getUsernameKey(), SelectedGroupsTask.UserName,
									SelectedGroupsTask.Ngo_Id, SelectedGroupsTask.Group_Id, mTransaction_UniqueId,
									mLastTr_ID, mCurrentTransDate, null, null, null, null, null, null, null, null, null,
									null, null, null, null, null, null, mMinutesofMeetingvalues, null, null, null, null,
									null, null));
				} else {

					TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT, TastyToast.ERROR);
				}

			} else if (mUniqueId.equalsIgnoreCase(PrefUtils.getOfflineUniqueID()) && mMeetingValues_Offline == null) {
				EShaktiApplication.setSetTransValues(TransactionOffConstants.TRANS_MINM);

				EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.TRANS_VALUES_UPDATE,
						new TransactionUpdate(mUniqueId, mMinutesofMeetingvalues));

			} else if (mMeetingValues_Offline != null) {
				confirmationDialog.dismiss();
				isServiceCall = false;
				TastyToast.makeText(getActivity(), AppStrings.offlineDataAvailable, TastyToast.LENGTH_SHORT,
						TastyToast.WARNING);

				DatePickerDialog.sDashboardDate = SelectedGroupsTask.sLastTransactionDate_Response;
				MainFragment_Dashboard fragment = new MainFragment_Dashboard();
				setFragment(fragment);
			}
			break;
		}
	}

	@Subscribe
	public void OnTransUpdateMinutesOfMeeting(final TransactionUpdateResponse transactionUpdateResponse) {
		switch (transactionUpdateResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), transactionUpdateResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), transactionUpdateResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			confirmationDialog.dismiss();
			isServiceCall = false;

			String mBankDetails_OfflineDate = GetOfflineTransactionValues.getBankDetails();
			String mTransactionDate = DatePickerDialog.sSend_To_Server_Date;
			SelectedGroupsTask.sLastTransactionDate_Response = mTransactionDate;
			String mGroupMasterResponse_OfflineDate = Put_DB_GroupResponse.put_DB_GroupResponse(
					SelectedGroupsTask.sLastTransactionDate_Response, SelectedGroupsTask.sCashinHand,
					SelectedGroupsTask.sCashatBank, mBankDetails_OfflineDate);
			GroupDetailsProvider.updateGroupDetailsEntry(
					new GroupDetailsUpdate(SelectedGroupsTask.Group_Id, mGroupMasterResponse_OfflineDate));

			String mValues = Put_DB_GroupNameDetail_Response
					.put_DB_GroupNameDetails_Response(EShaktiApplication.getGroupResponse());

			GroupProvider.updateGroupResponse(
					new GroupResponseUpdate(EShaktiApplication.getGroupId_GroupLastTransDate(), mValues));

			TastyToast.makeText(getActivity(), AppStrings.transactionCompleted, TastyToast.LENGTH_SHORT,
					TastyToast.SUCCESS);

			if (EShaktiApplication.isDefault()) {
				DefaultReportsFragment fragment_Dashboard = new DefaultReportsFragment();
				setFragment(fragment_Dashboard);
			} else {
				MainFragment_Dashboard fragment = new MainFragment_Dashboard();
				setFragment(fragment);
			}
			break;
		}
	}

	@Subscribe
	public void onAddTransactionMinutesOfMeeting(final TransactionResponse transactionResponse) {
		switch (transactionResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), transactionResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), transactionResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:
			confirmationDialog.dismiss();
			isServiceCall = false;

			String mBankDetails_OfflineDate = GetOfflineTransactionValues.getBankDetails();
			String mTransactionDate = DatePickerDialog.sSend_To_Server_Date;
			SelectedGroupsTask.sLastTransactionDate_Response = mTransactionDate;
			String mGroupMasterResponse_OfflineDate = Put_DB_GroupResponse.put_DB_GroupResponse(
					SelectedGroupsTask.sLastTransactionDate_Response, SelectedGroupsTask.sCashinHand,
					SelectedGroupsTask.sCashatBank, mBankDetails_OfflineDate);
			GroupDetailsProvider.updateGroupDetailsEntry(
					new GroupDetailsUpdate(SelectedGroupsTask.Group_Id, mGroupMasterResponse_OfflineDate));

			String mValues = Put_DB_GroupNameDetail_Response
					.put_DB_GroupNameDetails_Response(EShaktiApplication.getGroupResponse());

			GroupProvider.updateGroupResponse(
					new GroupResponseUpdate(EShaktiApplication.getGroupId_GroupLastTransDate(), mValues));

			TastyToast.makeText(getActivity(), AppStrings.transactionCompleted, TastyToast.LENGTH_SHORT,
					TastyToast.SUCCESS);
			if (EShaktiApplication.isDefault()) {
				DefaultReportsFragment fragment_Dashboard = new DefaultReportsFragment();
				setFragment(fragment_Dashboard);
			} else {
				MainFragment_Dashboard fragment = new MainFragment_Dashboard();
				setFragment(fragment);
			}

			break;
		}
	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub
		mProgressDialog = AppDialogUtils.createProgressDialog(getActivity());
		mProgressDialog.show();
	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub

		try {

			if (mProgressDialog != null) {
				mProgressDialog.dismiss();

				if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {
					getActivity().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub

							if (!result.equals("FAIL")) {
								isServiceCall = false;
								TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg,
										TastyToast.LENGTH_SHORT, TastyToast.ERROR);

								Constants.NETWORKCOMMONFLAG = "SUCESS";
							}

						}
					});
				} else {
					if (iSPurposeLoanTask) {
						iSPurposeLoanTask = false;
						Meeting_Minutes_MicroCreditPlanFragment creditPlanFragment = new Meeting_Minutes_MicroCreditPlanFragment();
						setFragment(creditPlanFragment);
					} else {

						if (isMinutesLanguage) {
							isMinutesLanguage = false;
							mMinutesLangValues = publicValues.mGetMinutesLanguageValues;

							@SuppressWarnings("deprecation")
							TableLayout.LayoutParams params = new TableLayout.LayoutParams(
									TableLayout.LayoutParams.FILL_PARENT, TableLayout.LayoutParams.WRAP_CONTENT, 1f);
							params.setMargins(20, 5, 5, 5);

							for (int i = 0; i < responseMinutes.length; i++) {
								TableRow indv_row = new TableRow(getActivity());

								indv_row.setLayoutParams(params);

								sCheckBox[i] = new CheckBox(getActivity());
								sCheckBox[i].setBackgroundResource(R.drawable.btn_check_to_off_mtrl_013);
								sCheckBox[i].setChecked(false);
								indv_row.addView(sCheckBox[i]);

								TableRow.LayoutParams textParams = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,
										LayoutParams.WRAP_CONTENT, 1f);
								textParams.setMargins(0, 0, 5, 15);
								TextView minutesOfMeeting = new TextView(getActivity());

								if (ConnectionUtils.isNetworkAvailable(getActivity())) {

									if (responseArr[1].equals("Yes")) {
										if (responseMinutes[i].equals(mMinutesLangValues)) {
											sCheckBox[i].setEnabled(true);
										}
									} else {
										if (responseMinutes[i].equals(mMinutesLangValues)) {
											sCheckBox[i].setEnabled(false);
										}
									}
								}
								minutesOfMeeting.setText(
										GetSpanText.getSpanString(getActivity(), String.valueOf(responseMinutes[i])));
								minutesOfMeeting.setTypeface(LoginActivity.sTypeface);
								minutesOfMeeting.setTextColor(color.black);
								minutesOfMeeting.setSingleLine(false);
								minutesOfMeeting.setLayoutParams(textParams);
								indv_row.addView(minutesOfMeeting);

								minutesTable.addView(indv_row);
							}

						} else {
							confirmationDialog.dismiss();
							isServiceCall = false;
							if (Get_MinutesTask.sMinutes_Response.equals("Yes")) {

								String mValues = Put_DB_GroupNameDetail_Response
										.put_DB_GroupNameDetails_Response(EShaktiApplication.getGroupResponse());

								GroupProvider.updateGroupResponse(new GroupResponseUpdate(
										EShaktiApplication.getGroupId_GroupLastTransDate(), mValues));

								String mBankDetails_OfflineDate = GetOfflineTransactionValues.getBankDetails();

								String mGroupMasterResponse_OfflineDate = Put_DB_GroupResponse.put_DB_GroupResponse(
										SelectedGroupsTask.sLastTransactionDate_Response,
										SelectedGroupsTask.sCashinHand, SelectedGroupsTask.sCashatBank,
										mBankDetails_OfflineDate);
								GroupDetailsProvider.updateGroupDetailsEntry(new GroupDetailsUpdate(
										SelectedGroupsTask.Group_Id, mGroupMasterResponse_OfflineDate));

								TastyToast.makeText(getActivity(), AppStrings.transactionCompleted,
										TastyToast.LENGTH_SHORT, TastyToast.SUCCESS);

								if (EShaktiApplication.isDefault()) {

									if (isGetBankLoan) {

										isGetBankLoan = false;
										new Get_PurposeOfLoanTask(Meeting_MinutesOfMeetingFragment.this).execute();
										iSPurposeLoanTask = true;

									} else {

										if (EShaktiApplication.isDefault()) {
											DefaultReportsFragment fragment_Dashboard = new DefaultReportsFragment();
											setFragment(fragment_Dashboard);
										}
									}
								} else {

									if (isGetBankLoan) {

										isGetBankLoan = false;

										new Get_PurposeOfLoanTask(Meeting_MinutesOfMeetingFragment.this).execute();
										iSPurposeLoanTask = true;
									} else {

										String mCashinHand = SelectedGroupsTask.sCashinHand;
										String mTransactionDate = SelectedGroupsTask.sLastTransactionDate_Response;
										String mCashatBank = SelectedGroupsTask.sCashatBank;

										String mBankDetails = GetOfflineTransactionValues.getBankDetails();

										String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(
												mTransactionDate, mCashinHand, mCashatBank, mBankDetails);
										Log.e(TAG, mGroupMasterResponse);
										Log.i(TAG + "Bank Detailssss", mBankDetails);

										String mSelectedGroupId = SelectedGroupsTask.Group_Id;
										EShaktiApplication.getInstance().getTransactionManager().startTransaction(
												DataType.GROUPMASTERUPDATE,
												new GroupDetailsUpdate(mSelectedGroupId, mGroupMasterResponse));

										/*
										 * MainFragment_Dashboard fragment = new MainFragment_Dashboard();
										 * setFragment(fragment);
										 */ }
								}
							} else {
								TastyToast.makeText(getActivity(), AppStrings.transactionFailAlert,
										TastyToast.LENGTH_SHORT, TastyToast.ERROR);
								MainFragment_Dashboard fragment = new MainFragment_Dashboard();
								setFragment(fragment);

							}
						}
					}
				}
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		SBus.INST.register(this);
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		SBus.INST.unRegister(this);
	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub

		if (isNextButton) {

			EShaktiApplication.setDefault(false);
			isNextButton = false;
		}
		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

		FragmentDrawer.drawer_lastTR_Date
				.setText(RegionalConversion.getRegionalConversion(AppStrings.lastTransactionDate) + " : "
						+ SelectedGroupsTask.sLastTransactionDate_Response);
		FragmentDrawer.drawer_lastTR_Date.setTypeface(LoginActivity.sTypeface);

		UpdateCalendarMinMaxDateUtils.OnUpdateCalendarDate();

		Calendar calender = Calendar.getInstance();

		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String formattedDate = df.format(calender.getTime());
		EShaktiApplication.setLastTransDate_GroupId(SelectedGroupsTask.Group_Id);

		new GroupTempDBLastTransDate(EShaktiApplication.getLastTransDate_GroupId(),
				SelectedGroupsTask.sLastTransactionDate_Response, formattedDate);

		GroupProvider.updateGroupLastTransDateResponse();

	}

	@Subscribe
	public void OnGroupMasUpdate(final GroupMasUpdateResponse masterResponse) {
		switch (masterResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), masterResponse.getFetcherResult().getMessage(), TastyToast.LENGTH_SHORT,
					TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), masterResponse.getFetcherResult().getMessage(), TastyToast.LENGTH_SHORT,
					TastyToast.ERROR);
			break;
		case SUCCESS:

			try {
				confirmationDialog.dismiss();
				isServiceCall = false;

				System.out.println("------OnGroupMasUpdate--------");

				String mValues = Put_DB_GroupNameDetail_Response
						.put_DB_GroupNameDetails_Response(EShaktiApplication.getGroupResponse());

				GroupProvider.updateGroupResponse(
						new GroupResponseUpdate(EShaktiApplication.getGroupId_GroupLastTransDate(), mValues));

				MainFragment_Dashboard fragment = new MainFragment_Dashboard();
				setFragment(fragment);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		}
	}

}
