package com.yesteam.eshakti.view.fragment;

import java.io.IOException;

import org.xmlpull.v1.XmlPullParserException;

import com.squareup.otto.Subscribe;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.aadharcard.AadhaarCard;
import com.yesteam.eshakti.aadharcard.AadhaarXMLParser;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.sqlite.database.response.AadharCardMasterResponse;
import com.yesteam.eshakti.sqlite.db.model.AadharCardMaster;
import com.yesteam.eshakti.sqlite.db.transactions.TransactionManager.DataType;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.utils.Get_UniqueID;
import com.yesteam.eshakti.utils.Reset;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.Member_Aadhar_InfoTask;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Profile_AadhaarCardDialogFragment extends Fragment implements OnClickListener, TaskListener {

	private Button mEdit_RaisedButton, mOk_RaisedButton;
	private static String mAadhaarcardValues[] = { "UID", "NAME", "GENDER", "CO", "HOUSE", "VILLAGE", "VC_POST", "POST",
			"DIST", "SUB DIST", "STATE", "PINCODE", "DOB" };
	private static String mQrcardValues[];
	public static AadhaarCard newCard;
	String mQRcodeValues;
	private Dialog mProgressDilaog;
	Bundle bundle;
	boolean mIsAadharCardValid = false;

	public Profile_AadhaarCardDialogFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_AADHAAR_DIALOG;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		SBus.INST.register(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View rootView = inflater.inflate(R.layout.fragment_aadhaarcard_confirmation, container, false);
		bundle = this.getArguments();
		String mAadhaarCard = bundle.getString("QRVALUES", "0");
		mQRcodeValues = mAadhaarCard;
		Log.e("Qr Values ------>>>", mQRcodeValues);

		if (mQRcodeValues.length() == 12) {

			TextView confirmationHeader = (TextView) rootView.findViewById(R.id.confirmationHeader_dialog);
			confirmationHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.confirmation));
			confirmationHeader.setTypeface(LoginActivity.sTypeface);

			TableLayout confirmationTable = (TableLayout) rootView.findViewById(R.id.confirmationTable_dialog);

			TableRow indv_AadhardetailsRow = new TableRow(getActivity());

			TableRow.LayoutParams contentParams = new TableRow.LayoutParams(0, LayoutParams.WRAP_CONTENT, 1f);
			contentParams.setMargins(10, 5, 10, 5);

			TextView memberName_Text = new TextView(getActivity());
			memberName_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf("AADHAAR NO:")));
			memberName_Text.setTypeface(LoginActivity.sTypeface);
			memberName_Text.setTextColor(color.black);
			memberName_Text.setPadding(5, 5, 5, 5);
			memberName_Text.setLayoutParams(contentParams);
			indv_AadhardetailsRow.addView(memberName_Text);

			TextView confirm_values = new TextView(getActivity());
			confirm_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(mQRcodeValues)));
			confirm_values.setTextColor(color.black);
			confirm_values.setPadding(5, 5, 15, 5);
			confirm_values.setGravity(Gravity.RIGHT);
			confirm_values.setLayoutParams(contentParams);
			indv_AadhardetailsRow.addView(confirm_values);

			confirmationTable.addView(indv_AadhardetailsRow,
					new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

			View rullerView = new View(getActivity());
			rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
			rullerView.setBackgroundColor(Color.rgb(0, 199, 140));// rgb(255,
																	// 229,
																	// 242));
			confirmationTable.addView(rullerView);

		} else {

			mQrcardValues = new String[mAadhaarcardValues.length];

			try {
				newCard = new AadhaarXMLParser().parse(mAadhaarCard);
				mIsAadharCardValid = true;
			} catch (XmlPullParserException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();

				mIsAadharCardValid = false;
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();

				mIsAadharCardValid = false;
			}

			try {
				Thread.sleep(300);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (mIsAadharCardValid) {

				mQrcardValues[0] = newCard.uid;
				mQrcardValues[1] = newCard.name;
				mQrcardValues[2] = newCard.gender;
				mQrcardValues[3] = newCard.co;
				mQrcardValues[4] = newCard.house;
				mQrcardValues[5] = newCard.lm;
				mQrcardValues[6] = newCard.vtc;
				mQrcardValues[7] = newCard.po;
				mQrcardValues[8] = newCard.dist;
				mQrcardValues[9] = newCard.subdist;
				mQrcardValues[10] = newCard.state;
				mQrcardValues[11] = newCard.pincode;
				mQrcardValues[12] = newCard.dob;

				TextView confirmationHeader = (TextView) rootView.findViewById(R.id.confirmationHeader_dialog);
				confirmationHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.confirmation));
				confirmationHeader.setTypeface(LoginActivity.sTypeface);

				TableLayout confirmationTable = (TableLayout) rootView.findViewById(R.id.confirmationTable_dialog);
				for (int i = 0; i < mAadhaarcardValues.length; i++) {

					TableRow indv_AadhardetailsRow = new TableRow(getActivity());

					TableRow.LayoutParams contentParams = new TableRow.LayoutParams(0, LayoutParams.WRAP_CONTENT, 1f);
					contentParams.setMargins(10, 5, 10, 5);

					TextView memberName_Text = new TextView(getActivity());
					memberName_Text
							.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(mAadhaarcardValues[i])));
					memberName_Text.setTypeface(LoginActivity.sTypeface);
					memberName_Text.setTextColor(color.black);
					memberName_Text.setPadding(5, 5, 5, 5);
					memberName_Text.setLayoutParams(contentParams);
					indv_AadhardetailsRow.addView(memberName_Text);

					TextView confirm_values = new TextView(getActivity());
					confirm_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(mQrcardValues[i])));
					confirm_values.setTextColor(color.black);
					confirm_values.setPadding(5, 5, 15, 5);
					confirm_values.setGravity(Gravity.RIGHT);
					confirm_values.setLayoutParams(contentParams);
					indv_AadhardetailsRow.addView(confirm_values);

					confirmationTable.addView(indv_AadhardetailsRow,
							new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

				}
				View rullerView = new View(getActivity());
				rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
				rullerView.setBackgroundColor(Color.rgb(0, 199, 140));// rgb(255,
																		// 229,
																		// 242));
				confirmationTable.addView(rullerView);
			} else {

				TastyToast.makeText(getActivity(), AppStrings.mAadharCard_Invalid, TastyToast.LENGTH_SHORT,
						TastyToast.ERROR);

				mQRcodeValues = Reset.reset(mQRcodeValues);
				bundle = null;
				Profile_AadhaarCardFragment fragment = new Profile_AadhaarCardFragment();
				setFragment(fragment);
			}

		}

		mEdit_RaisedButton = (Button) rootView.findViewById(R.id.fragment_Edit_button_dialog);
		mEdit_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.edit));
		mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
		mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
																// 205,
																// 0));
		mEdit_RaisedButton.setOnClickListener(this);
		mOk_RaisedButton = (Button) rootView.findViewById(R.id.fragment_Ok_button_dialog);
		mOk_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.yes));
		mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
		mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
		mOk_RaisedButton.setOnClickListener(this);
		return rootView;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.fragment_Edit_button_dialog:
			mQRcodeValues = Reset.reset(mQRcodeValues);
			bundle = null;
			Profile_AadhaarCardFragment fragment = new Profile_AadhaarCardFragment();
			setFragment(fragment);
			break;
		case R.id.fragment_Ok_button_dialog:
			try {

				if (ConnectionUtils.isNetworkAvailable(getActivity())) {

					new Member_Aadhar_InfoTask(this, mQRcodeValues).execute();
				} else {

					EShaktiApplication.setOfflineAadharCardUniqueId(Get_UniqueID.get_UniqueID(getActivity()));

					String mAadhaarCardValues = newCard.uid + "~" + newCard.name + "~" + newCard.gender + "~"
							+ newCard.co + "~" + newCard.house + "~" + newCard.lm + "~" + newCard.vtc + "~" + newCard.po
							+ "~" + newCard.dist + "~" + newCard.subdist + "~" + newCard.state + "~" + newCard.pincode
							+ "~" + newCard.dob;

					if (SelectedGroupsTask.UserName != null && SelectedGroupsTask.Ngo_Id != null
							&& SelectedGroupsTask.Group_Id != null && MemberList_Fragment.sSelected_MemberId != null
							&& mAadhaarCardValues != null) {

						EShaktiApplication.getInstance().getTransactionManager().startTransaction(
								DataType.AADHARCARDMASTER,
								new AadharCardMaster(0, EShaktiApplication.getOfflineAadharCardUniqueId(),
										SelectedGroupsTask.UserName, String.valueOf(SelectedGroupsTask.Ngo_Id),
										String.valueOf(SelectedGroupsTask.Group_Id),
										String.valueOf(MemberList_Fragment.sSelected_MemberId), mAadhaarCardValues,
										"0"));
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			break;
		default:
			break;
		}
	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub
		mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
		mProgressDilaog.show();
	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub

		if (mProgressDilaog != null) {
			mProgressDilaog.dismiss();
			try {

				if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {
					getActivity().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub

							if (!result.equals("FAIL")) {

								TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg,
										TastyToast.LENGTH_SHORT, TastyToast.ERROR);
								Constants.NETWORKCOMMONFLAG = "SUCCESS";
							}

						}
					});
				} else {

					TastyToast.makeText(getActivity(), AppStrings.transactionCompleted, TastyToast.LENGTH_SHORT,
							TastyToast.SUCCESS);

					MemberList_Fragment fragment = new MemberList_Fragment();
					setFragment(fragment);
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub
		Log.e("Current Class Name!!!!!!!!!!!!!!", fragment.getClass().getName());
		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

	}

	@Subscribe
	public void OnAadharCardMaster(final AadharCardMasterResponse aadharCardMasterResponse) {
		switch (aadharCardMasterResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), aadharCardMasterResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), aadharCardMasterResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:
			TastyToast.makeText(getActivity(), AppStrings.transactionCompleted, TastyToast.LENGTH_SHORT,
					TastyToast.SUCCESS);

			MemberList_Fragment fragment = new MemberList_Fragment();
			setFragment(fragment);
			break;
		}
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		SBus.INST.unRegister(this);
	}

}
