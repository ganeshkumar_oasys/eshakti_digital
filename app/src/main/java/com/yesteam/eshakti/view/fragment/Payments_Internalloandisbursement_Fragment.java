package com.yesteam.eshakti.view.fragment;

import java.util.ArrayList;
import java.util.List;

import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;

import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.model.InternalBankMFIModel;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.utils.Get_EdiText_Filter;
import com.yesteam.eshakti.utils.Reset;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.utils.TextviewUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.views.ButtonFlat;
import com.yesteam.eshakti.views.RaisedButton;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Payments_Internalloandisbursement_Fragment extends Fragment implements OnClickListener {

	public static String TAG = Transaction_InternalLoanDisburseMentFragment.class.getSimpleName();
	private TextView mGroupName, mCashinHand, mCashatBank, mHeader, mAutoFill_label;
	private CheckBox mAutoFill;
	private Button mSubmit_Raised_Button;
	int mSize;
	private TableLayout mIncomeTable;
	private EditText mIncome_values;
	private static List<EditText> sIncomeFields = new ArrayList<EditText>();
	private static String sIncomeAmounts[];
	String[] confirmArr;
	String nullVlaue = "0";
	public static String sSendToServer_InternalLoanDisbursement;
	public static int sIncome_Total;

	Dialog confirmationDialog;
	private RaisedButton mEdit_RaisedButton, mOk_RaisedButton;
	private Dialog mProgressDialog;

	boolean isNaviMain = false;
	boolean isServiceCall = false;

	LinearLayout mMemberNameLayout;
	TextView mMemberName;
	public static boolean isPaymentDisbursementBackPressed = false;

	public Payments_Internalloandisbursement_Fragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_INTERLAONMENU;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		sSendToServer_InternalLoanDisbursement = Reset.reset(sSendToServer_InternalLoanDisbursement);
		sIncome_Total = 0;
		sIncomeFields.clear();
		isPaymentDisbursementBackPressed = false;

	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		SBus.INST.register(this);
	}

	@SuppressWarnings("deprecation")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_internalloan_disbursement, container, false);

		MainFragment_Dashboard.isBackpressed = false;
		
		isPaymentDisbursementBackPressed = true;

		try {

			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashinHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashinHand.setTypeface(LoginActivity.sTypeface);

			mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashatBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashatBank.setTypeface(LoginActivity.sTypeface);

			mMemberNameLayout = (LinearLayout) rootView.findViewById(R.id.member_name_layout);
			mMemberName = (TextView) rootView.findViewById(R.id.member_name);

			/** UI Mapping **/

			mHeader = (TextView) rootView.findViewById(R.id.internal_fragmentHeader);
			// mHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.mInternalTypeLoan));
			mHeader.setText(RegionalConversion.getRegionalConversion("LOAN DISBURSE"));
			mHeader.setTypeface(LoginActivity.sTypeface);

			mAutoFill_label = (TextView) rootView.findViewById(R.id.internal_autofillLabel);
			mAutoFill_label.setText(RegionalConversion.getRegionalConversion(AppStrings.autoFill));
			mAutoFill_label.setTypeface(LoginActivity.sTypeface);
			mAutoFill_label.setVisibility(View.VISIBLE);

			mAutoFill = (CheckBox) rootView.findViewById(R.id.internal_autoFill);
			mAutoFill.setVisibility(View.VISIBLE);
			mAutoFill.setOnClickListener(this);

			mSize = SelectedGroupsTask.member_Name.size();
			Log.d(TAG, String.valueOf(mSize));

			TableLayout headerTable = (TableLayout) rootView.findViewById(R.id.internal_savingsTable);

			mIncomeTable = (TableLayout) rootView.findViewById(R.id.internal_fragment_contentTable);

			TableRow savingsHeader = new TableRow(getActivity());
			savingsHeader.setBackgroundResource(R.color.tableHeader);

			LayoutParams headerParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT,
					1f);

			TextView mMemberName_headerText = new TextView(getActivity());
			mMemberName_headerText
					.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.memberName)));
			mMemberName_headerText.setTypeface(LoginActivity.sTypeface);
			mMemberName_headerText.setTextColor(Color.WHITE);
			mMemberName_headerText.setPadding(20, 5, 10, 5);
			mMemberName_headerText.setLayoutParams(headerParams);
			savingsHeader.addView(mMemberName_headerText);

			TextView mIncomeAmount_HeaderText = new TextView(getActivity());
			mIncomeAmount_HeaderText
					.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.amount)));
			mIncomeAmount_HeaderText.setTypeface(LoginActivity.sTypeface);
			mIncomeAmount_HeaderText.setTextColor(Color.WHITE);
			mIncomeAmount_HeaderText.setPadding(10, 5, 40, 5);
			mIncomeAmount_HeaderText.setLayoutParams(headerParams);
			mIncomeAmount_HeaderText.setBackgroundResource(R.color.tableHeader);
			savingsHeader.addView(mIncomeAmount_HeaderText);

			headerTable.addView(savingsHeader,
					new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

			for (int i = 0; i < mSize; i++) {

				TableRow indv_IncomeRow = new TableRow(getActivity());

				TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
						LayoutParams.WRAP_CONTENT, 1f);
				contentParams.setMargins(10, 5, 10, 5);

				final TextView memberName_Text = new TextView(getActivity());
				memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
						String.valueOf(SelectedGroupsTask.member_Name.elementAt(i))));
				memberName_Text.setTypeface(LoginActivity.sTypeface);
				memberName_Text.setTextColor(color.black);
				memberName_Text.setPadding(10, 0, 10, 5);
				memberName_Text.setLayoutParams(contentParams);
				indv_IncomeRow.addView(memberName_Text);

				TableRow.LayoutParams contentEditParams = new TableRow.LayoutParams(150, LayoutParams.WRAP_CONTENT);
				contentEditParams.setMargins(30, 5, 100, 5);

				mIncome_values = new EditText(getActivity());

				mIncome_values.setId(i);
				sIncomeFields.add(mIncome_values);
				mIncome_values.setGravity(Gravity.END);
				mIncome_values.setTextColor(Color.BLACK);
				mIncome_values.setPadding(5, 5, 5, 5);
				mIncome_values.setBackgroundResource(R.drawable.edittext_background);
				mIncome_values.setLayoutParams(contentEditParams);// contentParams
																	// lParams
				mIncome_values.setTextAppearance(getActivity(), R.style.MyMaterialTheme);
				mIncome_values.setFilters(Get_EdiText_Filter.editText_filter());
				mIncome_values.setInputType(InputType.TYPE_CLASS_NUMBER);
				mIncome_values.setTextColor(color.black);
				mIncome_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

					@Override
					public void onFocusChange(View v, boolean hasFocus) {
						// TODO Auto-generated method stub
						if (hasFocus) {
							((EditText) v).setGravity(Gravity.LEFT);

							mMemberNameLayout.setVisibility(View.VISIBLE);
							mMemberName.setText(memberName_Text.getText().toString().trim());
							mMemberName.setTypeface(LoginActivity.sTypeface);
							TextviewUtils.manageBlinkEffect(mMemberName, getActivity());
						} else {
							((EditText) v).setGravity(Gravity.RIGHT);

							mMemberNameLayout.setVisibility(View.GONE);
							mMemberName.setText("");
						}

					}
				});
				indv_IncomeRow.addView(mIncome_values);

				mIncomeTable.addView(indv_IncomeRow,
						new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

			}

			mSubmit_Raised_Button = (Button) rootView.findViewById(R.id.internal_fragment_Submit_button);
			mSubmit_Raised_Button.setText(RegionalConversion.getRegionalConversion(AppStrings.submit));
			mSubmit_Raised_Button.setTypeface(LoginActivity.sTypeface);
			mSubmit_Raised_Button.setOnClickListener(this);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return rootView;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		sIncomeAmounts = new String[sIncomeFields.size()];
		Log.d(TAG, "sIncomeAmounts size : " + sIncomeAmounts.length);
		switch (v.getId()) {
		case R.id.internal_fragment_Submit_button:

			try {
				// isIncome = true;

				sSendToServer_InternalLoanDisbursement = "";
				sIncome_Total = 0;

				confirmArr = new String[mSize];

				StringBuilder builder = new StringBuilder();

				for (int i = 0; i < sIncomeAmounts.length; i++) {

					sIncomeAmounts[i] = String.valueOf(sIncomeFields.get(i).getText());
					Log.d(TAG, "Entered Values : " + sIncomeAmounts[i] + " POS : " + i);

					if ((sIncomeAmounts[i].equals("")) || (sIncomeAmounts[i] == null)) {
						sIncomeAmounts[i] = nullVlaue;
					}

					if (sIncomeAmounts[i].matches("\\d*\\.?\\d+")) { // match
																		// a
																		// decimal
																		// number

						int amount = (int) Math.round(Double.parseDouble(sIncomeAmounts[i]));
						sIncomeAmounts[i] = String.valueOf(amount);
					}

					sSendToServer_InternalLoanDisbursement = sSendToServer_InternalLoanDisbursement
							+ String.valueOf(SelectedGroupsTask.member_Id.elementAt(i)) + "~" + sIncomeAmounts[i] + "~";

					confirmArr[i] = String.valueOf(SelectedGroupsTask.member_Name.elementAt(i)) + "           "
							+ sIncomeAmounts[i];

					sIncome_Total = sIncome_Total + Integer.parseInt(sIncomeAmounts[i]);

					builder.append(sIncomeAmounts[i]).append(",");
				}

				Log.d(TAG, sSendToServer_InternalLoanDisbursement);

				Log.d(TAG, "TOTAL " + String.valueOf(sIncome_Total));

				InternalBankMFIModel.setValues(sSendToServer_InternalLoanDisbursement);

				int mCheckValues = Integer.parseInt(InternalBankMFIModel.getLoan_Disbursement_Amount())
						+ Integer.parseInt(InternalBankMFIModel.getLoanBankCharges());

				// Do the SP insertion

				if (sIncome_Total <= mCheckValues) {

					confirmationDialog = new Dialog(getActivity());

					LayoutInflater inflater = getActivity().getLayoutInflater();
					View dialogView = inflater.inflate(R.layout.dialog_confirmation, null);
					dialogView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
							LayoutParams.WRAP_CONTENT));

					TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
					confirmationHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.confirmation));
					confirmationHeader.setTypeface(LoginActivity.sTypeface);

					TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

					for (int i = 0; i < confirmArr.length; i++) {

						TableRow indv_SavingsRow = new TableRow(getActivity());

						@SuppressWarnings("deprecation")
						TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
								LayoutParams.WRAP_CONTENT, 1f);
						contentParams.setMargins(10, 5, 10, 5);

						TextView memberName_Text = new TextView(getActivity());
						memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
								String.valueOf(SelectedGroupsTask.member_Name.elementAt(i))));
						memberName_Text.setTypeface(LoginActivity.sTypeface);
						memberName_Text.setTextColor(color.black);
						memberName_Text.setPadding(5, 5, 5, 5);
						memberName_Text.setLayoutParams(contentParams);
						indv_SavingsRow.addView(memberName_Text);

						TextView confirm_values = new TextView(getActivity());
						confirm_values
								.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sIncomeAmounts[i])));
						confirm_values.setTextColor(color.black);
						confirm_values.setPadding(5, 5, 5, 5);
						confirm_values.setGravity(Gravity.RIGHT);
						confirm_values.setLayoutParams(contentParams);
						indv_SavingsRow.addView(confirm_values);

						confirmationTable.addView(indv_SavingsRow,
								new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

					}

					if (EShaktiApplication.getInternalLoanType().equals("BANKLOAN")) {
						if (EShaktiApplication.getSubsidyAmount().equals("0")
								&& EShaktiApplication.getSubsidyReserveFund().equals("0")) {

							int totalValue = sIncome_Total;

							View rullerView1 = new View(getActivity());
							rullerView1
									.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
							rullerView1.setBackgroundColor(Color.rgb(0, 199, 140));// rgb(255,
																					// 229,
																					// 242));
							confirmationTable.addView(rullerView1);

							TableRow totalRow = new TableRow(getActivity());

							@SuppressWarnings("deprecation")
							TableRow.LayoutParams totalParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
									LayoutParams.WRAP_CONTENT, 1f);
							totalParams.setMargins(10, 5, 10, 5);

							TextView totalText = new TextView(getActivity());
							totalText.setText(
									GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.total)));
							totalText.setTypeface(LoginActivity.sTypeface);
							totalText.setTextColor(color.black);
							totalText.setPadding(5, 5, 5, 5);// (5, 10,
																// 5,
																// 10);
							totalText.setLayoutParams(totalParams);
							totalRow.addView(totalText);

							TextView totalAmount = new TextView(getActivity());
							totalAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(totalValue)));// SavingsFragment.sSavings_Total
							totalAmount.setTextColor(color.black);
							totalAmount.setPadding(5, 5, 5, 5);// (5,
																// 10,
																// 100,
																// 10);
							totalAmount.setGravity(Gravity.RIGHT);
							totalAmount.setLayoutParams(totalParams);
							totalRow.addView(totalAmount);

							confirmationTable.addView(totalRow,
									new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

						} else {
							View rullerView1 = new View(getActivity());
							rullerView1
									.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
							rullerView1.setBackgroundColor(Color.rgb(0, 199, 140));// rgb(255,
																					// 229,
																					// 242));
							confirmationTable.addView(rullerView1);

							TableRow totalRow = new TableRow(getActivity());

							@SuppressWarnings("deprecation")
							TableRow.LayoutParams totalParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
									LayoutParams.WRAP_CONTENT, 1f);
							totalParams.setMargins(10, 5, 10, 5);

							TextView totalText = new TextView(getActivity());
							totalText.setText(
									GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.total)));
							totalText.setTypeface(LoginActivity.sTypeface);
							totalText.setTextColor(color.black);
							totalText.setPadding(5, 5, 5, 5);// (5, 10,
																// 5,
																// 10);
							totalText.setLayoutParams(totalParams);
							totalRow.addView(totalText);

							TextView totalAmount = new TextView(getActivity());
							totalAmount
									.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sIncome_Total)));// SavingsFragment.sSavings_Total
							totalAmount.setTextColor(color.black);
							totalAmount.setPadding(5, 5, 5, 5);// (5,
																// 10,
																// 100,
																// 10);
							totalAmount.setGravity(Gravity.RIGHT);
							totalAmount.setLayoutParams(totalParams);
							totalRow.addView(totalAmount);

							confirmationTable.addView(totalRow,
									new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

						}
					} else if (!EShaktiApplication.getInternalLoanType().equals("BANKLOAN")) {

						View rullerView1 = new View(getActivity());
						rullerView1.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
						rullerView1.setBackgroundColor(Color.rgb(0, 199, 140));// rgb(255,
																				// 229,
																				// 242));
						confirmationTable.addView(rullerView1);

						TableRow totalRow = new TableRow(getActivity());

						@SuppressWarnings("deprecation")
						TableRow.LayoutParams totalParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
								LayoutParams.WRAP_CONTENT, 1f);
						totalParams.setMargins(10, 5, 10, 5);

						TextView totalText = new TextView(getActivity());
						totalText.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.total)));
						totalText.setTypeface(LoginActivity.sTypeface);
						totalText.setTextColor(color.black);
						totalText.setPadding(5, 5, 5, 5);// (5, 10, 5,
															// 10);
						totalText.setLayoutParams(totalParams);
						totalRow.addView(totalText);

						TextView totalAmount = new TextView(getActivity());
						totalAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sIncome_Total)));// SavingsFragment.sSavings_Total
						totalAmount.setTextColor(color.black);
						totalAmount.setPadding(5, 5, 5, 5);// (5, 10,
															// 100,
															// 10);
						totalAmount.setGravity(Gravity.RIGHT);
						totalAmount.setLayoutParams(totalParams);
						totalRow.addView(totalAmount);

						confirmationTable.addView(totalRow,
								new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

					}

					mEdit_RaisedButton = (RaisedButton) dialogView.findViewById(R.id.fragment_Edit_button);
					mEdit_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.edit));
					mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
					mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
																			// 205,
																			// 0));
					mEdit_RaisedButton.setOnClickListener(this);

					mOk_RaisedButton = (RaisedButton) dialogView.findViewById(R.id.fragment_Ok_button);
					mOk_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.yes));
					mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
					mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
					mOk_RaisedButton.setOnClickListener(this);

					confirmationDialog.getWindow()
							.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
					confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					confirmationDialog.setCanceledOnTouchOutside(false);
					confirmationDialog.setContentView(dialogView);
					confirmationDialog.setCancelable(true);
					confirmationDialog.show();

					MarginLayoutParams margin = (MarginLayoutParams) dialogView.getLayoutParams();
					margin.leftMargin = 10;
					margin.rightMargin = 10;
					margin.topMargin = 10;
					margin.bottomMargin = 10;
					margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);

				} else {
					TastyToast.makeText(getActivity(), AppStrings.mLoanSanc_loanDist, TastyToast.LENGTH_SHORT,
							TastyToast.WARNING);

					sSendToServer_InternalLoanDisbursement = Reset.reset(sSendToServer_InternalLoanDisbursement);
					sIncome_Total = 0;

				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

			break;

		case R.id.internal_autoFill:

			String similiar_Income;
			// isValues = true;

			if (mAutoFill.isChecked()) {

				try {

					// Makes all edit fields holds the same savings
					similiar_Income = sIncomeFields.get(0).getText().toString();

					for (int i = 0; i < sIncomeAmounts.length; i++) {
						sIncomeFields.get(i).setText(similiar_Income);
						sIncomeFields.get(i).setGravity(Gravity.RIGHT);
						sIncomeFields.get(i).clearFocus();
						sIncomeAmounts[i] = similiar_Income;

					}

					/** To clear the values of EditFields in case of uncheck **/

				} catch (ArrayIndexOutOfBoundsException e) {
					e.printStackTrace();

					TastyToast.makeText(getActivity(), AppStrings.adminAlert, TastyToast.LENGTH_SHORT,
							TastyToast.WARNING);
				}
			}
			break;

		case R.id.fragment_Edit_button:

			sSendToServer_InternalLoanDisbursement = Reset.reset(sSendToServer_InternalLoanDisbursement);
			sIncome_Total = Integer.valueOf(nullVlaue);
			mSubmit_Raised_Button.setClickable(true);
			isServiceCall = false;
			confirmationDialog.dismiss();
			break;

		case R.id.fragment_Ok_button:

			if (ConnectionUtils.isNetworkAvailable(getActivity())) {

			}
			break;

		}
	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub
		Log.e("Current Class Name!!!!!!!!!!!!!!", fragment.getClass().getName());
		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		SBus.INST.unRegister(this);
	}

	void showConfirmation_Dialog() {
		final Dialog confirmationDialog_Dialog = new Dialog(getActivity());

		LayoutInflater li = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View customView = li.inflate(R.layout.dialog_logout, null, false);
		TextView mHeaderView, mMessageView;

		mHeaderView = (TextView) customView.findViewById(R.id.dialog_Title_logout);
		mMessageView = (TextView) customView.findViewById(R.id.dialog_Message_logout);
		mHeaderView.setTypeface(LoginActivity.sTypeface);
		mMessageView.setTypeface(LoginActivity.sTypeface);

		final ButtonFlat mConYesButton, mConNoButton;

		mConYesButton = (ButtonFlat) customView.findViewById(R.id.fragment_ok_button_alert_logout);
		mConNoButton = (ButtonFlat) customView.findViewById(R.id.fragment_cancel_button_alert_logout);
		mConYesButton.setTypeface(LoginActivity.sTypeface);
		mConNoButton.setTypeface(LoginActivity.sTypeface);
		mConYesButton.setText(AppStrings.dialogOk);

		mConNoButton.setText(AppStrings.dialogNo);

		mHeaderView.setText(AppStrings.logOut);

		mMessageView.setText(AppStrings.mAmountDisbursingAlert);
		mConYesButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				confirmationDialog_Dialog.dismiss();

				try {
					// isIncome = true;

					sSendToServer_InternalLoanDisbursement = "";
					sIncome_Total = 0;

					confirmArr = new String[mSize];

					StringBuilder builder = new StringBuilder();

					for (int i = 0; i < sIncomeAmounts.length; i++) {

						sIncomeAmounts[i] = String.valueOf(sIncomeFields.get(i).getText());
						Log.d(TAG, "Entered Values : " + sIncomeAmounts[i] + " POS : " + i);

						if ((sIncomeAmounts[i].equals("")) || (sIncomeAmounts[i] == null)) {
							sIncomeAmounts[i] = nullVlaue;
						}

						if (sIncomeAmounts[i].matches("\\d*\\.?\\d+")) { // match
																			// a
																			// decimal
																			// number

							int amount = (int) Math.round(Double.parseDouble(sIncomeAmounts[i]));
							sIncomeAmounts[i] = String.valueOf(amount);
						}

						sSendToServer_InternalLoanDisbursement = sSendToServer_InternalLoanDisbursement
								+ String.valueOf(SelectedGroupsTask.member_Id.elementAt(i)) + "~" + sIncomeAmounts[i]
								+ "~";

						confirmArr[i] = String.valueOf(SelectedGroupsTask.member_Name.elementAt(i)) + "           "
								+ sIncomeAmounts[i];

						sIncome_Total = sIncome_Total + Integer.parseInt(sIncomeAmounts[i]);

						builder.append(sIncomeAmounts[i]).append(",");
					}

					Log.d(TAG, sSendToServer_InternalLoanDisbursement);

					Log.d(TAG, "TOTAL " + String.valueOf(sIncome_Total));

					InternalBankMFIModel.setValues(sSendToServer_InternalLoanDisbursement);

					// Do the SP insertion

					if (sIncome_Total <= Integer.parseInt(InternalBankMFIModel.getLoan_Disbursement_Amount())) {

						confirmationDialog = new Dialog(getActivity());

						LayoutInflater inflater = getActivity().getLayoutInflater();
						View dialogView = inflater.inflate(R.layout.dialog_confirmation, null);
						dialogView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
								LayoutParams.WRAP_CONTENT));

						TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
						confirmationHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.confirmation));
						confirmationHeader.setTypeface(LoginActivity.sTypeface);

						TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

						for (int i = 0; i < confirmArr.length; i++) {

							TableRow indv_SavingsRow = new TableRow(getActivity());

							@SuppressWarnings("deprecation")
							TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
									LayoutParams.WRAP_CONTENT, 1f);
							contentParams.setMargins(10, 5, 10, 5);

							TextView memberName_Text = new TextView(getActivity());
							memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
									String.valueOf(SelectedGroupsTask.member_Name.elementAt(i))));
							memberName_Text.setTypeface(LoginActivity.sTypeface);
							memberName_Text.setTextColor(color.black);
							memberName_Text.setPadding(5, 5, 5, 5);
							memberName_Text.setLayoutParams(contentParams);
							indv_SavingsRow.addView(memberName_Text);

							TextView confirm_values = new TextView(getActivity());
							confirm_values.setText(
									GetSpanText.getSpanString(getActivity(), String.valueOf(sIncomeAmounts[i])));
							confirm_values.setTextColor(color.black);
							confirm_values.setPadding(5, 5, 5, 5);
							confirm_values.setGravity(Gravity.RIGHT);
							confirm_values.setLayoutParams(contentParams);
							indv_SavingsRow.addView(confirm_values);

							confirmationTable.addView(indv_SavingsRow,
									new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

						}

						if (EShaktiApplication.getInternalLoanType().equals("BANKLOAN")) {
							if (EShaktiApplication.getSubsidyAmount().equals("0")
									&& EShaktiApplication.getSubsidyReserveFund().equals("0")) {

								int totalValue = sIncome_Total;

								View rullerView1 = new View(getActivity());
								rullerView1.setLayoutParams(
										new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
								rullerView1.setBackgroundColor(Color.rgb(0, 199, 140));// rgb(255,
																						// 229,
																						// 242));
								confirmationTable.addView(rullerView1);

								TableRow totalRow = new TableRow(getActivity());

								@SuppressWarnings("deprecation")
								TableRow.LayoutParams totalParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
										LayoutParams.WRAP_CONTENT, 1f);
								totalParams.setMargins(10, 5, 10, 5);

								TextView totalText = new TextView(getActivity());
								totalText.setText(
										GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.total)));
								totalText.setTypeface(LoginActivity.sTypeface);
								totalText.setTextColor(color.black);
								totalText.setPadding(5, 5, 5, 5);// (5, 10,
																	// 5,
																	// 10);
								totalText.setLayoutParams(totalParams);
								totalRow.addView(totalText);

								TextView totalAmount = new TextView(getActivity());
								totalAmount
										.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(totalValue)));// SavingsFragment.sSavings_Total
								totalAmount.setTextColor(color.black);
								totalAmount.setPadding(5, 5, 5, 5);// (5,
																	// 10,
																	// 100,
																	// 10);
								totalAmount.setGravity(Gravity.RIGHT);
								totalAmount.setLayoutParams(totalParams);
								totalRow.addView(totalAmount);

								confirmationTable.addView(totalRow, new TableLayout.LayoutParams(
										LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

							} else {
								View rullerView1 = new View(getActivity());
								rullerView1.setLayoutParams(
										new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
								rullerView1.setBackgroundColor(Color.rgb(0, 199, 140));// rgb(255,
																						// 229,
																						// 242));
								confirmationTable.addView(rullerView1);

								TableRow totalRow = new TableRow(getActivity());

								@SuppressWarnings("deprecation")
								TableRow.LayoutParams totalParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
										LayoutParams.WRAP_CONTENT, 1f);
								totalParams.setMargins(10, 5, 10, 5);

								TextView totalText = new TextView(getActivity());
								totalText.setText(
										GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.total)));
								totalText.setTypeface(LoginActivity.sTypeface);
								totalText.setTextColor(color.black);
								totalText.setPadding(5, 5, 5, 5);// (5, 10,
																	// 5,
																	// 10);
								totalText.setLayoutParams(totalParams);
								totalRow.addView(totalText);

								TextView totalAmount = new TextView(getActivity());
								totalAmount.setText(
										GetSpanText.getSpanString(getActivity(), String.valueOf(sIncome_Total)));// SavingsFragment.sSavings_Total
								totalAmount.setTextColor(color.black);
								totalAmount.setPadding(5, 5, 5, 5);// (5,
																	// 10,
																	// 100,
																	// 10);
								totalAmount.setGravity(Gravity.RIGHT);
								totalAmount.setLayoutParams(totalParams);
								totalRow.addView(totalAmount);

								confirmationTable.addView(totalRow, new TableLayout.LayoutParams(
										LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

							}
						} else if (!EShaktiApplication.getInternalLoanType().equals("BANKLOAN")) {

							View rullerView1 = new View(getActivity());
							rullerView1
									.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
							rullerView1.setBackgroundColor(Color.rgb(0, 199, 140));// rgb(255,
																					// 229,
																					// 242));
							confirmationTable.addView(rullerView1);

							TableRow totalRow = new TableRow(getActivity());

							@SuppressWarnings("deprecation")
							TableRow.LayoutParams totalParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
									LayoutParams.WRAP_CONTENT, 1f);
							totalParams.setMargins(10, 5, 10, 5);

							TextView totalText = new TextView(getActivity());
							totalText.setText(
									GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.total)));
							totalText.setTypeface(LoginActivity.sTypeface);
							totalText.setTextColor(color.black);
							totalText.setPadding(5, 5, 5, 5);// (5, 10, 5,
																// 10);
							totalText.setLayoutParams(totalParams);
							totalRow.addView(totalText);

							TextView totalAmount = new TextView(getActivity());
							totalAmount
									.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sIncome_Total)));// SavingsFragment.sSavings_Total
							totalAmount.setTextColor(color.black);
							totalAmount.setPadding(5, 5, 5, 5);// (5, 10,
																// 100,
																// 10);
							totalAmount.setGravity(Gravity.RIGHT);
							totalAmount.setLayoutParams(totalParams);
							totalRow.addView(totalAmount);

							confirmationTable.addView(totalRow,
									new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

						}

						mEdit_RaisedButton = (RaisedButton) dialogView.findViewById(R.id.fragment_Edit_button);
						mEdit_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.edit));
						mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
						mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
																				// 205,
																				// 0));
						mEdit_RaisedButton.setOnClickListener(this);

						mOk_RaisedButton = (RaisedButton) dialogView.findViewById(R.id.fragment_Ok_button);
						mOk_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.yes));
						mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
						mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
						mOk_RaisedButton.setOnClickListener(this);

						confirmationDialog.getWindow()
								.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
						confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
						confirmationDialog.setCanceledOnTouchOutside(false);
						confirmationDialog.setContentView(dialogView);
						confirmationDialog.setCancelable(true);
						confirmationDialog.show();

						MarginLayoutParams margin = (MarginLayoutParams) dialogView.getLayoutParams();
						margin.leftMargin = 10;
						margin.rightMargin = 10;
						margin.topMargin = 10;
						margin.bottomMargin = 10;
						margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);

					} else {
						TastyToast.makeText(getActivity(), AppStrings.mLoanSanc_loanDist, TastyToast.LENGTH_SHORT,
								TastyToast.WARNING);

						sSendToServer_InternalLoanDisbursement = Reset.reset(sSendToServer_InternalLoanDisbursement);
						sIncome_Total = 0;

					}

				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}

			}
		});

		mConNoButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				confirmationDialog_Dialog.dismiss();
			}
		});

		confirmationDialog_Dialog.getWindow()
				.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		confirmationDialog_Dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		confirmationDialog_Dialog.setCanceledOnTouchOutside(false);
		confirmationDialog_Dialog.setContentView(customView);
		confirmationDialog_Dialog.setCancelable(true);
		confirmationDialog_Dialog.show();

	}
}
