package com.yesteam.eshakti.view.fragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.squareup.otto.Subscribe;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.adapter.CustomListAdapter;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.model.ListItem;
import com.yesteam.eshakti.sqlite.db.model.Transaction;
import com.yesteam.eshakti.sqlite.db.transactions.TransactionManager.DataType;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

public class Offline_SubMenuList extends Fragment implements
		 OnItemClickListener {

	private TextView mGroupName, mCashInHand, mCashAtBank, mHeadertext;

	int size;

	public static String selectedSubMenuItem = "";
	private String[] subItemArr;
	public static String[] subMenuItemList,LoanIdArr,groupIdArr;

	private ListView mListView;
	private List<ListItem> listItems;
	private CustomListAdapter mAdapter;
	int listImage;
	
	public 	static String memberLoanName="",groupLoanName="",groupLoanId="";
	public static int selectedItemPos;

	public Offline_SubMenuList() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		SBus.INST.register(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.offline_transactionlist,
				container, false);

		MainFragment_Dashboard.isBackpressed = false;

		try {

			listItems = new ArrayList<ListItem>();
			mListView = (ListView) rootView.findViewById(R.id.fragment_List);
			listImage = R.drawable.ic_navigate_next_white_24dp;

		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {

			EShaktiApplication.setOfflineTransDate(false);
			EShaktiApplication.getInstance().getTransactionManager()
					.startTransaction(DataType.GET_TRANS_MASTERVALUES, null);

			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String
					.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashInHand.setText(RegionalConversion.getRegionalConversion(String
					.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashInHand.setTypeface(LoginActivity.sTypeface);

			mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashAtBank.setText(RegionalConversion.getRegionalConversion(String
					.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashAtBank.setTypeface(LoginActivity.sTypeface);

			mHeadertext = (TextView) rootView
					.findViewById(R.id.offline_headertext);
			mHeadertext.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.offlineReports)));
			mHeadertext.setTypeface(LoginActivity.sTypeface);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return rootView;

	}

	@Subscribe
	public void OnValues(final ArrayList<Transaction> arrayList) {
		Log.e("CHECKING !23", "YES DONE4545");
		String subItem = "";

		if (arrayList.size() != 0) {
			Log.v("Current Array List Size", arrayList.size() + "");
			try {
				groupLoanId = "";
				groupLoanName = "";
				for (int i = 0; i < arrayList.size(); i++) {
					String loan_id = "";
					if (Offline_ReportDateListFragment.sSelectedTransDate
							.equals(arrayList.get(i).getTransactionDate())) {
					
					if (OfflineReports_TransactionList.selectedItem
							.equals(AppStrings.savings)) {
						if (arrayList.get(i).getSavings() != null) {
							subItem = subItem + AppStrings.savings + "~";
						}
						if (arrayList.get(i).getVolunteerSavings() != null) {
							subItem = subItem + AppStrings.voluntarySavings
									+ "~";
						}
					} else if (OfflineReports_TransactionList.selectedItem
							.equals(AppStrings.memberloanrepayment)) {
						String getResponse = "";
						String[] responseArr,loanIdArr;
						System.out.println("MemberName Length :"+SelectedGroupsTask.member_Name.size());
						int loanId_start_pos = ((SelectedGroupsTask.member_Name.size()*3)+1);
						int loanId_inc_pos = ((SelectedGroupsTask.member_Name.size()*3)+4);
						Log.e("STARTING LOAN ID POS :", loanId_start_pos+"");
						Log.e("LOAN ID INC POS :", loanId_inc_pos+"");
						/*if (arrayList.get(i).getMemberloanRepayment() != null) {
							getResponse = arrayList.get(i).getMemberloanRepayment();
							responseArr = getResponse.split("#");
							System.out.println("-------------loan no ---------------"+responseArr[1]);
							for (int j = 0; j < SelectedGroupsTask.loan_Id.size(); j++) {
								if (responseArr[1].equals(SelectedGroupsTask.loan_Id.elementAt(j).toString())) {
								memberLoanName = SelectedGroupsTask.loan_Name.elementAt(j).toString();
									subItem = subItem + SelectedGroupsTask.loan_Name.elementAt(j).toString()+"~";
								}	
							}
							
						}*/
						if ((arrayList.get(i).getMemberloanRepayment() != null)
								|| arrayList.get(i).getPersonalloanRepayment() != null) {
							if (arrayList.get(i).getMemberloanRepayment() != null) {
								
								getResponse = arrayList.get(i).getMemberloanRepayment();
								System.out.println("getResponse :"+getResponse);
								responseArr = getResponse.split("[~#$]");
								Log.e("responseArr.length", responseArr.length+"");
								for (int j = 0; j < responseArr.length; j++) {
									System.out.println("responseArr[] : "+responseArr[j] +" pos :"+j);
								}
								
								for (int j = loanId_start_pos; j < responseArr.length; j=j+loanId_inc_pos) {
									loan_id = loan_id + responseArr[j]+"~";
									Log.v("--------------", loan_id);
								}
								loanIdArr = loan_id.split("~");
								
								for (int j = 0; j < loanIdArr.length; j++) {
									for (int j2 = 0; j2 < SelectedGroupsTask.loan_Id.size(); j2++) {
										if (loanIdArr[j].equals(SelectedGroupsTask.loan_Id.elementAt(j2).toString())) {
											groupLoanId = groupLoanId +SelectedGroupsTask.loan_Id.elementAt(j2).toString()+"~";
											Log.e("IIIIIIIIIIIIII DDDDDDDD", groupLoanId.toString());
											groupLoanName = groupLoanName+SelectedGroupsTask.loan_Name.elementAt(j2)+"~";
											Log.e("NNNNNNNNNNNNNNName", groupLoanName.toString());
										//	subItem = subItem +SelectedGroupsTask.loan_Name.elementAt(j2).toString()+"~";
											subItem = subItem + SelectedGroupsTask.loan_Name.elementAt(j2).toString()
													+" - "+SelectedGroupsTask.loan_Id.elementAt(j2).toString() + "~";
											Log.e("SSSSSSSSSSSSSSubItem", subItem.toString());
											
										}
									}
								}

							}
													
						}
						if (arrayList.get(i).getPersonalloanRepayment() != null) {
							groupLoanId = groupLoanId + "0"+"~";
							subItem = subItem + AppStrings.InternalLoan + "~";
						}
					} else if (OfflineReports_TransactionList.selectedItem
							.equals(AppStrings.income)) {
						if (arrayList.get(i).getOtherIncome() != null) {
							subItem = subItem + AppStrings.otherincome + "~";
						}
						if (arrayList.get(i).getSubscriptionIncome() != null) {
							subItem = subItem + AppStrings.subscriptioncharges
									+ "~";
						}
						if (arrayList.get(i).getPenaltyIncome() != null) {
							subItem = subItem + AppStrings.penalty + "~";
						}
						if (arrayList.get(i).getDonationIncome() != null) {
							subItem = subItem + AppStrings.donation + "~";
						}
						if (arrayList.get(i).getFederationIncome() != null) {
							subItem = subItem + AppStrings.federationincome
									+ "~";
						}
						if (arrayList.get(i).getSeedFund() != null) {
							subItem = subItem + AppStrings.mSeedFund + "~";
						}
					} else if (OfflineReports_TransactionList.selectedItem
							.equals(AppStrings.bankTransaction)) {
						if (arrayList.get(i).getBankDeposit() != null) {
							subItem = subItem + AppStrings.bankTransaction
									+ "~";
						}
						if (arrayList.get(i).getBankFixedDeposit() != null) {
							subItem = subItem + AppStrings.fixedDeposit + "~";
						}
						if (arrayList.get(i).getLoanWithdrawal() != null) {
							subItem = subItem + AppStrings.mLoanaccHeader + "~";
						}
						if (arrayList.get(i).getAcctoAccTransfer() != null) {
							subItem = subItem + AppStrings.mAccountToAccountTransfer + "~";
						}
						if (arrayList.get(i).getAccToLoanAccTransfer() != null) {
							subItem = subItem + AppStrings.mLoanAccTransfer + "~";
						}
					} else if (OfflineReports_TransactionList.selectedItem
							.equals(AppStrings.meeting)) {
						if (arrayList.get(i).getAttendance() != null) {
							subItem = subItem + AppStrings.Attendance + "~";
						}
						if (arrayList.get(i).getMinutesofmeeting() != null) {
							subItem = subItem + AppStrings.MinutesofMeeting
									+ "~";
						}
						if (arrayList.get(i).getAuditting() != null) {
							subItem = subItem + AppStrings.auditing + "~";
						}
						if (arrayList.get(i).getTraining() != null) {
							subItem = subItem + AppStrings.training + "~";
						}
					}else if (OfflineReports_TransactionList.selectedItem
							.equals(AppStrings.grouploanrepayment)) {
						String getResponse = "", type = "";
						String[] responseArr,loanIdArr,getResponseArr;
						if (arrayList.get(i).getGrouploanrepayment() != null) {
							getResponse = arrayList.get(i).getGrouploanrepayment();
							System.out.println("--------getResponse---------   "+getResponse+"");
							
							/*responseArr = getResponse.split("[~#$]");
							for (int j = 5; j < responseArr.length; j=j+ 8) { //j+9 
								loan_id = loan_id + responseArr[j]+"~";
								Log.v("LOAN ID", loan_id);
							}
							loanIdArr = loan_id.split("~");
							
							for (int j = 0; j < loanIdArr.length; j++) {
								for (int j2 = 0; j2 < SelectedGroupsTask.loan_Id.size(); j2++) {
									if (loanIdArr[j].equals(SelectedGroupsTask.loan_Id.elementAt(j2).toString())) {
										groupLoanId = groupLoanId +SelectedGroupsTask.loan_Id.elementAt(j2).toString()+"~";
									//	Log.e("IIIIIIIIIIIIII DDDDDDDD", groupLoanId.toString());
										groupLoanName = groupLoanName+SelectedGroupsTask.loan_Name.elementAt(j2)+"~";
									//	Log.e("NNNNNNNNNNNNNNName", groupLoanName.toString());
										subItem = subItem +SelectedGroupsTask.loan_Name.elementAt(j2).toString()+"~";
									//	Log.e("SSSSSSSSSSSSSSubItem", subItem.toString());
									}
								}
							}*/
							
							getResponseArr = getResponse.split("[$]");
							
							for (int j = 0; j < getResponseArr.length; j++) {
								responseArr = getResponseArr[j].split("[~#$]");
								//type = type + String.valueOf( responseArr[responseArr.length-1])+"$";
								String transType = responseArr[responseArr.length-1];
								
								if (transType.equals("Cash")) {
									for (int k = 5; k < responseArr.length; k=k+9) {
										loan_id = loan_id + responseArr[k]+"~";
										Log.v("LOAN ID", loan_id);
									}
								} else if (transType.equals("Bank")) {
									for (int k = 6; k < responseArr.length; k=k+10) {
										loan_id = loan_id + responseArr[k]+"~";
										Log.v("LOAN ID", loan_id);
									}
								}
							}
							
							loanIdArr = loan_id.split("~");
							Log.e("Transaction Loan Id", loan_id+"");
							for (int j = 0; j < loanIdArr.length; j++) {
								for (int j2 = 0; j2 < SelectedGroupsTask.loan_Id.size(); j2++) {
									if (loanIdArr[j].equals(SelectedGroupsTask.loan_Id.elementAt(j2).toString())) {
										groupLoanId = groupLoanId +SelectedGroupsTask.loan_Id.elementAt(j2).toString()+"~";
										Log.e("IIIIIIIIIIIIII DDDDDDDD", groupLoanId.toString());
										groupLoanName = groupLoanName+SelectedGroupsTask.loan_Name.elementAt(j2)+"~";
										Log.e("NNNNNNNNNNNNNNName", groupLoanName.toString());
									//	subItem = subItem +SelectedGroupsTask.loan_Name.elementAt(j2).toString()+"~";
										subItem = subItem + SelectedGroupsTask.loan_Name.elementAt(j2).toString()
												+" - "+SelectedGroupsTask.loan_Id.elementAt(j2).toString() + "~";
										Log.e("SSSSSSSSSSSSSSubItem", subItem.toString());
										
									}
								}
							}

						}

					}
				}
				}
				System.out.println(" Group Id :"+groupLoanId.toString());
				System.out.println(" Group Loan Name :"+groupLoanName.toString());
				groupIdArr = groupLoanId.split("~");
				List<String> list = Arrays.asList(groupIdArr);
				Set<String> set = new LinkedHashSet<String>(list);
				LoanIdArr = new String[set.size()];
				set.toArray(LoanIdArr);
				
				for (int j = 0; j < LoanIdArr.length; j++) {
					System.out.println("Loan_Id_Arr[] :"+LoanIdArr[j] + " pos :"+j);
				}

				subItemArr = subItem.split("~");

				for (int j = 0; j < subItemArr.length; j++) {
					System.out
							.println("-------SUB ITEMS WITH DUPLICATE[]------"
									+ subItemArr[j]);
				}

				List<String> list1 = Arrays.asList(subItemArr);
				Set<String> set1 = new LinkedHashSet<String>(list1);
				subMenuItemList = new String[set1.size()];
				set1.toArray(subMenuItemList);

				for (int j = 0; j < subMenuItemList.length; j++) {
					System.out
							.println("----------SUBMENU LIST ITEMS---------"
									+ subMenuItemList[j].toString()
									+ " i pos " + j);
				}

				size = subMenuItemList.length;

				for (int k = 0; k < size; k++) {
					ListItem rowItem = new ListItem(
							RegionalConversion.getRegionalConversion(subMenuItemList[k]
									.toString()), listImage);
					listItems.add(rowItem);
				}
				System.out.println("ROW ITEM " + listItems.size());

				mAdapter = new CustomListAdapter(getActivity(), listItems);
				mListView.setAdapter(mAdapter);
				mListView.setOnItemClickListener(this);

			} catch (Exception e) {
				e.printStackTrace();
			}

		} else {
			/*Toast.makeText(getActivity(), "There is no Local DB Values",
					Toast.LENGTH_LONG).show();*/
			TastyToast.makeText(getActivity(), "There is no Local DB Values", TastyToast.LENGTH_SHORT, TastyToast.WARNING);
		}
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		SBus.INST.unRegister(this);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub

		TextView textColor_Change = (TextView) view
				.findViewById(R.id.dynamicText);
		textColor_Change.setText(String.valueOf(subMenuItemList[position]));
		textColor_Change.setTextColor(Color.rgb(251, 161, 108));
		selectedItemPos  = position;
		selectedSubMenuItem = subMenuItemList[position].toString();
		System.out.println("SELECTED SUB MENU ITEM  :" + selectedSubMenuItem+" pos :"+position);
		Fragment fragment = new Offline_TransactionDateFragment();
		setFragment(fragment);

	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub
		Log.e("Current Class Name!!!!!!!!!!!!!!", fragment.getClass().getName());
		getActivity()
				.getSupportFragmentManager()
				.beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0,
						R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

	}
}
