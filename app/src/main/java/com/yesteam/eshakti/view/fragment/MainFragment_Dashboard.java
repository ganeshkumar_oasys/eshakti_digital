package com.yesteam.eshakti.view.fragment;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.indris.material.RippleView;
import com.squareup.otto.Subscribe;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.adapter.SubmenuCustomAdapter;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.interfaces.RecyclerViewListener;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.model.RowItem;
import com.yesteam.eshakti.model.RowItem_submenu;
import com.yesteam.eshakti.service.GroupDetailsAddService;
import com.yesteam.eshakti.service.GroupDetailsService;
import com.yesteam.eshakti.sqlite.database.response.GetMinutesofMeetingResponse;
import com.yesteam.eshakti.sqlite.database.response.GroupMasterPLDBResponse;
import com.yesteam.eshakti.sqlite.database.response.Group_ProfileResponse;
import com.yesteam.eshakti.sqlite.db.model.GetGroupMemberDetails;
import com.yesteam.eshakti.sqlite.db.model.GroupMasterSingle;
import com.yesteam.eshakti.sqlite.db.model.Transaction;
import com.yesteam.eshakti.sqlite.db.transactions.TransactionManager.DataType;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.CalculateDateUtils;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.utils.GetTypeface;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.utils.RegionalserviceUtil;
import com.yesteam.eshakti.utils.Reset;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.view.activity.GroupListActivity;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.view.activity.MainActivity;
import com.yesteam.eshakti.views.ButtonFlat;
import com.yesteam.eshakti.webservices.DownloadPdfFileTask;
import com.yesteam.eshakti.webservices.GetMemberMobileNumberWebservices;
import com.yesteam.eshakti.webservices.Get_AadhaarNumber_Webservices;
import com.yesteam.eshakti.webservices.Get_AgentProfileTask;
import com.yesteam.eshakti.webservices.Get_All_Mem_OutstandingTask;
import com.yesteam.eshakti.webservices.Get_BankBranchName_Webservices;
import com.yesteam.eshakti.webservices.Get_BankTransactionSummaryTask;
import com.yesteam.eshakti.webservices.Get_Bank_BalanceTask;
import com.yesteam.eshakti.webservices.Get_Checklist_webservice;
import com.yesteam.eshakti.webservices.Get_GroupProfileTask;
import com.yesteam.eshakti.webservices.Get_Group_Minutes_NewTask;
import com.yesteam.eshakti.webservices.Get_MemberAccountNumber_Webservices;
import com.yesteam.eshakti.webservices.Get_Shg_Account_Details_Webservices;
import com.yesteam.eshakti.webservices.Get_animator_pendingTransWebservices;
import com.yesteam.eshakti.webservices.LoginTask;
import com.yesteam.eshakti.webservices.Login_webserviceTask;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;
import com.yesteam.eshakti.widget.Dialog_TransactionDate;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.core.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainFragment_Dashboard extends Fragment
		implements RecyclerViewListener, OnClickListener, OnItemClickListener, TaskListener {

	public static String TAG = MainFragment_Dashboard.class.getSimpleName();

	List<RowItem> rowItems;

	static RippleView mTransactionButton;
	static RippleView mProfileButton;
	static RippleView mReportsButton;
	static RippleView mSettingButton;
	static RippleView mHelpButton;
	static RippleView mMeetingButton;

	private static String[] sTransactionChild;
	private static String[] sAgentProfileChild;
	private static String[] sGroupProfileChild;
	private static String[] sReportChild;
	private static String[] sMeetingChild;
	private static String[] sSettingsChild;
	private static String[] sHelpChild;
	private Dialog mProgressDialog, mProgressDialog_thread;

	ListView mListview;
	List<RowItem_submenu> rowItems_submenu;

	TypedArray mSubmenu_transaction_img, mSubmenu_profile_img, mSubmenu_reports_img, mSubmenu_meeting_img,
			mSubmenu_setting_img, mSubmenu_help_img;
	LinearLayout mSubmenulinearlayout, mSubmenuheaderlinearlayout;
	TextView mSubmenuheader;
	Fragment fragment;
	boolean isNavigate;

	static String sItem = null;
	public static boolean isProfile;
	public static boolean isBalanceSheetReport = false;
	public static boolean isTrialBalanceReport = false;
	private boolean isAgentProfileNavigate = false;
	private boolean isGroupProfileNavigate = false;
	boolean isBankBalance = false, isTransactionSummary = false;;
	boolean isMinutes;

	String sub_Items;
	public static String sSubMenu_Item = "";

	public static String mPLDBResponse, mPLDBPurposeofloan;

	boolean isLanguageSelection = false;

	public static boolean isBackpressed = false;
	public static boolean mOfflineNavigate = false;
	boolean mLanguageValue = false;
	String mLanguageChangeValue = "";
	Dialog ChangeLanguageDialog;
	boolean iSChecklist = false;
	boolean isServiceCall = false;
	LinearLayout mCheckBackLogLayout;
	ImageButton mImageButton_CheckBackLog;
	TextView mCheckBackLog_title;
	boolean isAnimatorValues = false;
	boolean isGetMemberMobileNumber = false;
	boolean isGetMemberAadhaarNumber = false;
	boolean isGetShgAccountNumber = false;
	boolean isGetBankBranchName = false;
	boolean isGetMemberAccountNumber = false;
	boolean isServiceCall_BankTransactionReport = false;
	boolean isPdfDownload = false;

	public MainFragment_Dashboard() {
		// Required empty public constructor
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		SBus.INST.register(this);
		EShaktiApplication.setDefault(false);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		EShaktiApplication.setDefault(false);
		if (EShaktiApplication.isSubmenuclicked()) {
			String mButtonClickFlag = Constants.BUTTON_CLICK_FLAG;

			rowItems_submenu = new ArrayList<RowItem_submenu>();
			mSubmenulinearlayout.setVisibility(View.VISIBLE);
			mSubmenuheaderlinearlayout.setVisibility(View.VISIBLE);
			mCheckBackLogLayout.setVisibility(View.GONE);
			mSubmenuheader.setText("");

			if (mButtonClickFlag.equals("1")) {
				mSubmenuheader.setText(AppStrings.transaction);
				mSubmenuheader.setBackground(getResources().getDrawable(R.drawable.curve_lite_blue));
				for (int i = 0; i < sTransactionChild.length; i++) {

					RowItem_submenu item = new RowItem_submenu(sTransactionChild[i],
							mSubmenu_transaction_img.getResourceId(i, -1));
					rowItems_submenu.add(item);
				}

			} else if (mButtonClickFlag.equals("2")) {

				isProfile = true;

				mSubmenuheader.setText(AppStrings.profile);
				mSubmenuheader.setBackground(getResources().getDrawable(R.drawable.curve_profile));

				if (Boolean.valueOf(EShaktiApplication.isAgent)) {

					for (int i = 0; i < sAgentProfileChild.length; i++) {

						RowItem_submenu rowItem = new RowItem_submenu(sAgentProfileChild[i],
								mSubmenu_profile_img.getResourceId(i, -1));

						rowItems_submenu.add(rowItem);
					}

				} else if (!Boolean.valueOf(EShaktiApplication.isAgent)) {

					for (int i = 0; i < sGroupProfileChild.length; i++) {

						RowItem_submenu rowItem = new RowItem_submenu(sGroupProfileChild[i],
								mSubmenu_profile_img.getResourceId(i, -1));

						rowItems_submenu.add(rowItem);
					}
				}
			} else if (mButtonClickFlag.equals("3")) {
				mSubmenuheader.setText(AppStrings.reports);
				mSubmenuheader.setBackground(getResources().getDrawable(R.drawable.curve_report));

				isProfile = false;

				for (int i = 0; i < sReportChild.length; i++) {

					RowItem_submenu rowItem = new RowItem_submenu(sReportChild[i],
							mSubmenu_reports_img.getResourceId(i, -1));

					rowItems_submenu.add(rowItem);
				}

			} else if (mButtonClickFlag.equals("4")) {
				mSubmenuheader.setText(AppStrings.meeting);
				mSubmenuheader.setBackground(getResources().getDrawable(R.drawable.curve_meeting));

				for (int i = 0; i < sMeetingChild.length; i++) {

					RowItem_submenu rowItem = new RowItem_submenu(sMeetingChild[i],
							mSubmenu_meeting_img.getResourceId(i, -1));

					rowItems_submenu.add(rowItem);
				}
			} else if (mButtonClickFlag.equals("5")) {
				mSubmenuheader.setText(AppStrings.settings);
				mSubmenuheader.setBackground(getResources().getDrawable(R.drawable.curve_setting));

				for (int i = 0; i < sSettingsChild.length; i++) {
					RowItem_submenu rowItem = new RowItem_submenu(sSettingsChild[i],
							mSubmenu_setting_img.getResourceId(i, -1));

					rowItems_submenu.add(rowItem);
				}

			} else if (mButtonClickFlag.equals("6")) {
				mSubmenuheader.setText(AppStrings.help);
				mSubmenuheader.setBackground(getResources().getDrawable(R.drawable.curve_help));

				for (int i = 0; i < sHelpChild.length; i++) {
					RowItem_submenu rowItem = new RowItem_submenu(sHelpChild[i],
							mSubmenu_help_img.getResourceId(i, -1));
					rowItems_submenu.add(rowItem);
				}

			}

			SubmenuCustomAdapter adapter = new SubmenuCustomAdapter(getActivity(), rowItems_submenu);
			mListview.setAdapter(adapter);

		} else {
		}
		isBackpressed = true;

		if (EShaktiApplication.isOnSavedFragment()) {
			/*
			 * if (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.FRAG_INSTANCE_SAVINGS))
			 * { fragment = new Transaction_SavingsFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_MEMBERLOANREPAID_MENU)) { fragment = new
			 * Transaction_MemLoanRepaid_MenuFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_MEMBERLOANREPAID)) { fragment = new
			 * Transaction_MemLoanRepaidFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_PERSONALLOANREPAID)) { fragment = new
			 * Transaction_PersonalLoanRepaidFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.FRAG_INSTANCE_EXPENSES)) {
			 * fragment = new Transaction_ExpensesFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.FRAG_INSTANCE_INCOME_MENU)
			 * ) { fragment = new Transaction_IncomeMenuFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.FRAG_INSTANCE_OI_S_P)) {
			 * fragment = new Transaction_OI_S_P_Fragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_DONATION_FINCOME)) { fragment = new
			 * Transaction_Donation_FIncomeFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_GROUPLOAN_REPAID_MENU)) { fragment = new
			 * Transaction_GroupLoanRepaidMenuFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_GROUPLOAN_REPAID)) { fragment = new
			 * Transaction_GroupLoanRepaidFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.FRAG_INSTANCE_BANKDEPOSIT)
			 * ) { fragment = new Transaction_BankDepositFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.FRAG_INSTANCE_DEPOSIT_MENU
			 * )) { fragment = new Transaction_DepositMenuFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_DEPOSIT_ENTRY)) { fragment = new
			 * Transaction_DepositEntryFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_FIXEDDEPOSIT_ENTRY)) { fragment = new
			 * Transaction_FixedDepositEntryFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.FRAG_INSTANCE_PL_DISBURSE)
			 * ) { fragment = new Transaction_InternalLoan_DisbursementFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_GROUP_PROFILE)) { fragment = new
			 * Profile_GroupProfileFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.FRAG_INSTANCE_GROUP_AGENT)
			 * ) { fragment = new Profile_AgentProfileFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_GROUP_MEMBER_LIST)) { fragment = new MemberList_Fragment(); }
			 * else if (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_MEMBER_REPORTS_MEMBERLIST)) { fragment = new
			 * MemberList_Fragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_MEMBER_REPORTS_MENU)) { fragment = new
			 * Reports_MemberReports_MenuFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT
			 * .equals(Constants.FRAG_INSTANCE_MEMBER_REPORTS_MEMBER_SAVING_SUMM)) {
			 * fragment = new Reports_MemberSavingsSummary_ReportsFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT
			 * .equals(Constants.FRAG_INSTANCE_MEMBER_REPORTS_MEMBER_LOAN_SUMM_MENU)) {
			 * fragment = new Reports_MemberReports_LoanSummaryMenuFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT
			 * .equals(Constants.FRAG_INSTANCE_MEMBER_REPORTS_MEMBER_LOAN_SUMM)) { fragment
			 * = new Reports_Member_Loan_Summary_ReportsFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT
			 * .equals(Constants.FRAG_INSTANCE_MEMBER_REPORTS_MONTH_YEAR_PICK)) { fragment =
			 * new Reports_MonthYear_PickerReportsFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_MEMBER_REPORTS_MONTH_SUMM)) { fragment = new
			 * Reports_MonthSummary_ReportFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_GROUP_REPORTS_MENU)) { fragment = new
			 * Reports_GroupMenuReportFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_GROUP_REPORTS_SAVING_SUMM)) { fragment = new
			 * Reports_GroupSavingsSummaryFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_GROUP_REPORTS_GROUP_LOAN_LIST)) { fragment = new
			 * Reports_GroupLoanListReportFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_GROUP_REPORTS_LOAN_SUMM)) { fragment = new
			 * Reports_GroupLoanSummaryFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT
			 * .equals(Constants.FRAG_INSTANCE_GROUP_REPORTS_PERSONAL_LOAN_SUMM)) { fragment
			 * = new Reports_GroupPersonalLoanSummaryFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_BANK_TRANSACTION)) { fragment = new
			 * Reports_BankTransactionReportFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_BALANCE_SHEET_TRAIL)) { fragment = new
			 * Reports_Trial_BalanceSheetFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_BALANCE_SHEET_REPORTS)) { fragment = new
			 * Reports_BalanceSheetReportFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_TRAIL_BALANCE_REPORTS)) { fragment = new
			 * Reports_TrialBalanceReportFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.FRAG_INSTANCE_BANK_BALANCE
			 * )) { fragment = new Reports_BankBalanceFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.FRAG_INSTANCE_ATTENDANCE))
			 * { fragment = new Meeting_AttendanceFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_MINUTESOFMEETING)) { fragment = new
			 * Meeting_MinutesOfMeetingFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.FRAG_INSTANCE_AUDITING)) {
			 * fragment = new Meeting_AuditingFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.FRAG_INSTANCE_TRAINING)) {
			 * fragment = new Meeting_TrainingFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_SETTING_CHANGE_PASSWORD)) { fragment = new
			 * Settings_ChangePasswordFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_ABOUT_YESBOOKS)) { fragment = new
			 * Helps_AboutYesbooksFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.FRAG_INSTANCE_CONTACTS)) {
			 * fragment = new Helps_ContactsFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_MAIN_DASHBOARD)) { fragment = new MainFragment_Dashboard(); }
			 * else if (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_AADHAAR_IMAGE_MENU)) { fragment = new
			 * Profile_Member_Aadhaar_Image_MenuFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_PHOTO_AADHAAR_INFOMENU)) { fragment = new
			 * Profile_MemberPhoto_AadharInfoMenuFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_AADHAAR_IMAGE_VIEW_MENU)) { fragment = new
			 * Profile_Member_Aadhaar_Image_View_MenuFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.FRAG_INSTANCE_IMAGE_VIEW))
			 * { fragment = new Profile_Member_View_Image_Fragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_AADHAAR_UPLOAD)) { fragment = new
			 * Profile_AadhaarCardFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.FRAG_INSTANCE_AADHAAR_VIEW
			 * )) { fragment = new Profile_Member_View_AadhaarCard_Fragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_AADHAAR_DIALOG)) { fragment = new
			 * Profile_AadhaarCardDialogFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_INTERLAONMENU)) { fragment = new
			 * Transaction_InternalloanMenuFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_INTERLAONMENU_DISBURSE)) { fragment = new
			 * Transaction_InternalLoanDisburseMentFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_INTERLAONMENU_MFI)) { fragment = new
			 * Transaction_InternalBank_MFI_LoanFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_ACCTOACCTRANSFER)) { fragment = new
			 * Transaction_AcctoAccTransferFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_LOANACCWITHDRAWAL)) { fragment = new
			 * Transaction_LoanAccountFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.FRAG_INSTANCE_CHECKLIST))
			 * { fragment = new Transaction_CheckListFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_LOAN_DISBURSE_MENU)) { fragment = new
			 * Transaction_Loandisbursement_MentFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_LOAN_DISBURSE_LOANS)) { fragment = new
			 * Transaction_LoanDisbursement_LoansMenuFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_LOAN_DISBURSE_LIMIT_LOAN_SB)) { fragment = new
			 * Transaction_Limit_Loan_SB_MenuFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.FRAG_INSTANCE_LOAN_LIMIT))
			 * { fragment = new Transaction_IncreaseLimit_Fragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_LOAN_GROUP_INSTALLMENT)) { fragment = new
			 * Transaction_Loan_disburse_LoanAccFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_LOAN_SB_INSTALLMENT)) { fragment = new
			 * Transaction_Sb_Loan_acc_disburseFragment(); } else if
			 * (Constants.FRAG_INSTANCE_CONSTANT.equals(Constants.
			 * FRAG_INSTANCE_LOAN_SB_FINAL_DISBURSE)) { fragment = new
			 * Transaction_Loan_SB_disbursementFragment(); }
			 * EShaktiApplication.setOnSavedFragment(false);
			 * 
			 * setFragment(fragment);
			 */}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_MAIN_DASHBOARD;
		if (ChangeLanguageDialog != null && ChangeLanguageDialog.isShowing()) {
			ChangeLanguageDialog.dismiss();
		}
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		isBackpressed = false;
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_MAIN_DASHBOARD;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setMenuVisibility(false);
		EShaktiApplication.setDefault(false);
		isBackpressed = true;

		sSubMenu_Item = "";

		PrefUtils.setGroupMasterResValues(null);

		mProgressDialog_thread = AppDialogUtils.createProgressDialog(getActivity());
		if (ConnectionUtils.isNetworkAvailable(getActivity())) {
			sTransactionChild = new String[] { AppStrings.savings, AppStrings.income, AppStrings.expenses,
					AppStrings.memberloanrepayment, AppStrings.grouploanrepayment, AppStrings.bankTransaction,
					AppStrings.InternalLoanDisbursement, AppStrings.mDefault, AppStrings.mCheckList };
		} else {
			sTransactionChild = new String[] { AppStrings.savings, AppStrings.income, AppStrings.expenses,
					AppStrings.memberloanrepayment, AppStrings.grouploanrepayment, AppStrings.bankTransaction,
					AppStrings.InternalLoanDisbursement, AppStrings.mDefault };
		}

		sAgentProfileChild = new String[] { AppStrings.mCreditLinkageInfo, AppStrings.mMobileNoUpdation,
				AppStrings.mAadhaarNoUpdation, AppStrings.mAccountNoUpdation, AppStrings.mSHGAccountNoUpdation,
				AppStrings.uploadInfo, AppStrings.groupProfile, AppStrings.agentProfile };

		sGroupProfileChild = new String[] { AppStrings.groupProfile, AppStrings.uploadInfo };

		if (Boolean.valueOf(ConnectionUtils.isNetworkAvailable(getActivity()))) {

			sReportChild = new String[] { AppStrings.Memberreports, AppStrings.GroupReports,
					AppStrings.transactionsummary, AppStrings.bankBalance, AppStrings.offlineReports };
		} else if (!Boolean.valueOf(ConnectionUtils.isNetworkAvailable(getActivity()))) {
			if (EShaktiApplication.getLoginFlag() != null) {

				if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
					sReportChild = new String[] { AppStrings.offlineReports };
				}
			}
		}
		if (Boolean.valueOf(ConnectionUtils.isNetworkAvailable(getActivity()))) {
			sMeetingChild = new String[] { AppStrings.Attendance, AppStrings.MinutesofMeeting, AppStrings.auditing,
					AppStrings.training, "UPLOAD SCHEDULE VI" };
		} else if (!Boolean.valueOf(ConnectionUtils.isNetworkAvailable(getActivity()))) {
			if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
				sMeetingChild = new String[] { AppStrings.Attendance, AppStrings.MinutesofMeeting, AppStrings.auditing,
						AppStrings.training };
			}
		}

		if (Boolean.valueOf(ConnectionUtils.isNetworkAvailable(getActivity()))) {

			sSettingsChild = new String[] { AppStrings.passwordchange, AppStrings.changeLanguage,
					AppStrings.deactivateAccount, AppStrings.mSendEmail };

		} else if (!Boolean.valueOf(ConnectionUtils.isNetworkAvailable(getActivity()))) {
			if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
				sSettingsChild = new String[] { AppStrings.passwordchange };
			}
		}

		sHelpChild = new String[] { "ESHAKTI", AppStrings.contacts, AppStrings.mPdfManual };

		Transaction_PersonalLoanRepaidFragment.memLoanCount = 0;

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_dashboard, container, false);

		PrefUtils.setFirstLoginDashboard("1");

		isServiceCall_BankTransactionReport = false;

		mListview = (ListView) rootView.findViewById(R.id.listview_submenu);
		mSubmenulinearlayout = (LinearLayout) rootView.findViewById(R.id.submenu_linearlayout);
		mSubmenuheaderlinearlayout = (LinearLayout) rootView.findViewById(R.id.submenuheader_linearlayout);
		mSubmenuheader = (TextView) rootView.findViewById(R.id.submenuheader_textview);

		mTransactionButton = (RippleView) rootView.findViewById(R.id.activity_transaction_ripple);
		mTransactionButton.setText(RegionalConversion.getRegionalConversion(AppStrings.transaction));
		mTransactionButton.setTypeface(LoginActivity.sTypeface);

		mProfileButton = (RippleView) rootView.findViewById(R.id.activity_profile_ripple);
		mProfileButton.setText(RegionalConversion.getRegionalConversion(AppStrings.profile));
		mProfileButton.setTypeface(LoginActivity.sTypeface);

		mReportsButton = (RippleView) rootView.findViewById(R.id.activity_reports_ripple);
		mReportsButton.setText(RegionalConversion.getRegionalConversion(AppStrings.reports));
		mReportsButton.setTypeface(LoginActivity.sTypeface);

		mMeetingButton = (RippleView) rootView.findViewById(R.id.activity_meeting_ripple);
		mMeetingButton.setText(RegionalConversion.getRegionalConversion(AppStrings.meeting));
		mMeetingButton.setTypeface(LoginActivity.sTypeface);

		mSettingButton = (RippleView) rootView.findViewById(R.id.activity_setting_ripple);
		mSettingButton.setText(RegionalConversion.getRegionalConversion(AppStrings.settings));
		mSettingButton.setTypeface(LoginActivity.sTypeface);

		mHelpButton = (RippleView) rootView.findViewById(R.id.activity_help_ripple);
		mHelpButton.setText(RegionalConversion.getRegionalConversion(AppStrings.help));
		mHelpButton.setTypeface(LoginActivity.sTypeface);

		mImageButton_CheckBackLog = (ImageButton) rootView.findViewById(R.id.imageButton_check_backlog);
		mImageButton_CheckBackLog.setOnClickListener(this);

		mTransactionButton.setOnClickListener(this);
		mProfileButton.setOnClickListener(this);
		mReportsButton.setOnClickListener(this);
		mMeetingButton.setOnClickListener(this);
		mSettingButton.setOnClickListener(this);
		mHelpButton.setOnClickListener(this);

		mCheckBackLogLayout = (LinearLayout) rootView.findViewById(R.id.checkbacklog_linearlayout);
		mCheckBackLog_title = (TextView) rootView.findViewById(R.id.check_backlog_title);
		mCheckBackLog_title.setTypeface(LoginActivity.sTypeface);
		mCheckBackLog_title.setText(AppStrings.mCheckBackLog);

		mListview.setOnItemClickListener(this);
		rowItems_submenu = new ArrayList<RowItem_submenu>();

		mSubmenu_transaction_img = getResources().obtainTypedArray(R.array.submenuimages_transaction);

		if (Boolean.valueOf(EShaktiApplication.isAgent) == true) {
			mSubmenu_profile_img = getResources().obtainTypedArray(R.array.submenuimages_agentprofile);
		} else if (Boolean.valueOf(EShaktiApplication.isAgent) != true) {
			mSubmenu_profile_img = getResources().obtainTypedArray(R.array.submenuimages_groupprofile);
		}

		mSubmenu_reports_img = getResources().obtainTypedArray(R.array.submenuimages_reports);

		mSubmenu_meeting_img = getResources().obtainTypedArray(R.array.submenuimages_meeting);

		mSubmenu_setting_img = getResources().obtainTypedArray(R.array.submenuimages_setting);

		mSubmenu_help_img = getResources().obtainTypedArray(R.array.submenuimages_help);

		// Inflate the layout for this fragment
		return rootView;
	}

	@Override
	public void recyclerViewListClicked(View view, int position) {
		// TODO Auto-generated method stub

	}

	@SuppressWarnings("deprecation")
	@Override
	public void onClick(View v) {

		rowItems_submenu = new ArrayList<RowItem_submenu>();
		mSubmenulinearlayout.setVisibility(View.VISIBLE);
		mSubmenuheaderlinearlayout.setVisibility(View.VISIBLE);
		mSubmenuheader.setText("");
		switch (v.getId()) {
		case R.id.activity_transaction_ripple:
			mCheckBackLogLayout.setVisibility(View.GONE);
			mSubmenuheader.setText(AppStrings.transaction);
			mSubmenuheader.setBackground(getResources().getDrawable(R.drawable.curve_lite_blue));
			for (int i = 0; i < sTransactionChild.length; i++) {

				RowItem_submenu item = new RowItem_submenu(sTransactionChild[i],
						mSubmenu_transaction_img.getResourceId(i, -1));
				rowItems_submenu.add(item);
			}
			Constants.BUTTON_CLICK_FLAG = "1";
			EShaktiApplication.setSubmenuclicked(true);

			break;
		case R.id.activity_profile_ripple:
			mCheckBackLogLayout.setVisibility(View.GONE);
			isProfile = true;

			mSubmenuheader.setText(AppStrings.profile);
			mSubmenuheader.setBackground(getResources().getDrawable(R.drawable.curve_profile));

			if (Boolean.valueOf(EShaktiApplication.isAgent)) {

				for (int i = 0; i < sAgentProfileChild.length; i++) {

					RowItem_submenu rowItem = new RowItem_submenu(sAgentProfileChild[i],
							mSubmenu_profile_img.getResourceId(i, -1));

					rowItems_submenu.add(rowItem);
				}

			} else if (!Boolean.valueOf(EShaktiApplication.isAgent)) {

				for (int i = 0; i < sGroupProfileChild.length; i++) {

					RowItem_submenu rowItem = new RowItem_submenu(sGroupProfileChild[i],
							mSubmenu_profile_img.getResourceId(i, -1));

					rowItems_submenu.add(rowItem);
				}
			}

			Constants.BUTTON_CLICK_FLAG = "2";
			EShaktiApplication.setSubmenuclicked(true);

			break;

		case R.id.activity_reports_ripple:
			mCheckBackLogLayout.setVisibility(View.GONE);
			mSubmenuheader.setText(AppStrings.reports);
			mSubmenuheader.setBackground(getResources().getDrawable(R.drawable.curve_report));

			isProfile = false;

			for (int i = 0; i < sReportChild.length; i++) {

				RowItem_submenu rowItem = new RowItem_submenu(sReportChild[i],
						mSubmenu_reports_img.getResourceId(i, -1));

				rowItems_submenu.add(rowItem);
			}
			Constants.BUTTON_CLICK_FLAG = "3";
			EShaktiApplication.setSubmenuclicked(true);

			break;

		case R.id.activity_meeting_ripple:
			mCheckBackLogLayout.setVisibility(View.GONE);
			mSubmenuheader.setText(AppStrings.meeting);
			mSubmenuheader.setBackground(getResources().getDrawable(R.drawable.curve_meeting));

			for (int i = 0; i < sMeetingChild.length; i++) {

				RowItem_submenu rowItem = new RowItem_submenu(sMeetingChild[i],
						mSubmenu_meeting_img.getResourceId(i, -1));

				rowItems_submenu.add(rowItem);
			}
			Constants.BUTTON_CLICK_FLAG = "4";
			EShaktiApplication.setSubmenuclicked(true);
			break;

		case R.id.activity_setting_ripple:
			mCheckBackLogLayout.setVisibility(View.GONE);
			mSubmenuheader.setText(AppStrings.settings);
			mSubmenuheader.setBackground(getResources().getDrawable(R.drawable.curve_setting));

			System.out.println("SETTINGS LENGTH : " + sSettingsChild.length);

			for (int i = 0; i < sSettingsChild.length; i++) {
				RowItem_submenu rowItem = new RowItem_submenu(sSettingsChild[i],
						mSubmenu_setting_img.getResourceId(i, -1));

				rowItems_submenu.add(rowItem);
			}

			Constants.BUTTON_CLICK_FLAG = "5";
			EShaktiApplication.setSubmenuclicked(true);
			break;

		case R.id.activity_help_ripple:
			mCheckBackLogLayout.setVisibility(View.GONE);
			mSubmenuheader.setText(AppStrings.help);
			mSubmenuheader.setBackground(getResources().getDrawable(R.drawable.curve_help));

			for (int i = 0; i < sHelpChild.length; i++) {
				RowItem_submenu rowItem = new RowItem_submenu(sHelpChild[i], mSubmenu_help_img.getResourceId(i, -1));
				rowItems_submenu.add(rowItem);
			}

			Constants.BUTTON_CLICK_FLAG = "6";
			EShaktiApplication.setSubmenuclicked(true);
			break;
		case R.id.imageButton_check_backlog:

			mSubmenulinearlayout.setVisibility(View.GONE);
			mSubmenuheaderlinearlayout.setVisibility(View.GONE);

			if (ConnectionUtils.isNetworkAvailable(getActivity())) {
				isAnimatorValues = true;
				new Get_animator_pendingTransWebservices(MainFragment_Dashboard.this).execute();
			} else {
				TastyToast.makeText(getActivity(), AppStrings.mCheckBackLogOffline, TastyToast.LENGTH_SHORT,
						TastyToast.WARNING);
			}

			break;
		default:
			break;
		}
		SubmenuCustomAdapter adapter = new SubmenuCustomAdapter(getActivity(), rowItems_submenu);
		mListview.setAdapter(adapter);
		// mSubmenu_transaction_img.recycle();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

		onSetCalendarValues();
		sub_Items = rowItems_submenu.get(position).getSubmenu_name();
		System.out.println("ITEM " + sub_Items);

		view.setSelected(true);
		EShaktiApplication.setFragmentMenuListView(false);

		if (sub_Items.equals(AppStrings.savings)) {
			sSubMenu_Item = "1";
			int autoTime = 0;
			/*
			 * try { // android.provider.Settings.Global.getInt(getContentResolver(),
			 * android.provider.Settings.Global.AUTO_TIME, 0);
			 * 
			 * autoTime =
			 * android.provider.Settings.Global.getInt(getActivity().getContentResolver(),
			 * android.provider.Settings.Global.AUTO_TIME); } catch
			 * (SettingNotFoundException e1) { // TODO Auto-generated catch block
			 * e1.printStackTrace(); }
			 * 
			 * if (autoTime == 0) { startActivityForResult(new
			 * Intent(android.provider.Settings.ACTION_LOCALE_SETTINGS), 0);
			 * 
			 * } else {
			 */
			try {
				FragmentManager fm = getActivity().getSupportFragmentManager();

				Dialog_TransactionDate dialog = new Dialog_TransactionDate(getActivity(), sub_Items);
				dialog.show(fm, "");
			} catch (Exception e) {
				e.printStackTrace();
			}
			// }

		} else if (sub_Items.equals(AppStrings.memberloanrepayment)) {

			sSubMenu_Item = "2";
			try {
				FragmentManager fm = getActivity().getSupportFragmentManager();

				Dialog_TransactionDate dialog = new Dialog_TransactionDate(getActivity(), sub_Items);
				dialog.show(fm, "");
			} catch (Exception e) {
				e.printStackTrace();
			}

		} else if (sub_Items.equals(AppStrings.expenses)) {
			sSubMenu_Item = "3";
			try {
				FragmentManager fm = getActivity().getSupportFragmentManager();

				Dialog_TransactionDate dialog = new Dialog_TransactionDate(getActivity(), sub_Items);
				dialog.show(fm, "");
			} catch (Exception e) {
				e.printStackTrace();
			}

		} else if (sub_Items.equals(AppStrings.income)) {
			sSubMenu_Item = "4";
			try {
				FragmentManager fm = getActivity().getSupportFragmentManager();

				Dialog_TransactionDate dialog = new Dialog_TransactionDate(getActivity(), sub_Items);
				dialog.show(fm, "");
			} catch (Exception e) {
				e.printStackTrace();
			}

		} else if (sub_Items.equals(AppStrings.grouploanrepayment)) {
			int size = SelectedGroupsTask.loan_Name.size() - 1;
			System.out.println("Group Loan Size:>>>" + size);
			if (size != 0) {
				sSubMenu_Item = "5";
				try {
					FragmentManager fm = getActivity().getSupportFragmentManager();

					Dialog_TransactionDate dialog = new Dialog_TransactionDate(getActivity(), sub_Items);
					dialog.show(fm, "");
				} catch (Exception e) {
					e.printStackTrace();
				}

			} else {

				TastyToast.makeText(getActivity(), AppStrings.noGroupLoan_Alert, TastyToast.LENGTH_SHORT,
						TastyToast.WARNING);
			}
		} else if (sub_Items.equals(AppStrings.bankTransaction)) {
			sSubMenu_Item = "6";
			try {
				FragmentManager fm = getActivity().getSupportFragmentManager();

				Dialog_TransactionDate dialog = new Dialog_TransactionDate(getActivity(), sub_Items);
				dialog.show(fm, "");
			} catch (Exception e) {
				e.printStackTrace();
			}

		} else if (sub_Items.equals(AppStrings.InternalLoanDisbursement)) {

			sSubMenu_Item = "7";
			try {
				Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID = "0";

				System.out.println("PL LOAN ID : " + Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID);

				if (ConnectionUtils.isNetworkAvailable(getActivity())) {

					if (PrefUtils.getGroupMasterResValues() != null
							&& PrefUtils.getGroupMasterResValues().equals("1")) {

						EShaktiApplication.getInstance().getTransactionManager().startTransaction(
								DataType.GROUP_MASTER_PLDB,
								new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));

					} else {
						if (!isServiceCall) {
							isServiceCall = true;

							new Get_All_Mem_OutstandingTask(this).execute();

							isNavigate = true;
						}
					}

				} else {
					if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
						EShaktiApplication.getInstance().getTransactionManager().startTransaction(
								DataType.GROUP_MASTER_PLDB,
								new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));
					} else {

						TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
								TastyToast.ERROR);
					}
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();

				TastyToast.makeText(getActivity(), "Time Out Exception ", TastyToast.LENGTH_SHORT, TastyToast.ERROR);
				getActivity().finish();
			}

		} else if (sub_Items.equals(AppStrings.mDefault)) {
			try {
				sSubMenu_Item = "12";

				FragmentManager fm = getChildFragmentManager();

				Dialog_TransactionDate dialog = new Dialog_TransactionDate(getActivity(), AppStrings.mDefault);
				dialog.show(fm, "");
			} catch (Exception e) {
				e.printStackTrace();
			}

		} else if (sub_Items.equals(AppStrings.mCheckList)) {
			if (ConnectionUtils.isNetworkAvailable(getActivity())) {

				iSChecklist = true;
				new Get_Checklist_webservice(MainFragment_Dashboard.this).execute();
			}
		} else if (sub_Items.equals(AppStrings.agentProfile)) {

			Log.d(TAG, String.valueOf(ConnectionUtils.isNetworkAvailable(getActivity())));

			isAgentProfileNavigate = true;

			if (ConnectionUtils.isNetworkAvailable(getActivity())) {

				try {
					new Get_AgentProfileTask(MainFragment_Dashboard.this).execute();
					Thread.sleep(2000);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					TastyToast.makeText(getActivity(), "Time Out Exception ", TastyToast.LENGTH_SHORT,
							TastyToast.ERROR);
					getActivity().finish();
				}

			} else if (!ConnectionUtils.isNetworkAvailable(getActivity())) {
				Log.d(TAG, "Control goes DB to fetch Agent profile");
				if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
					EShaktiApplication.getInstance().getTransactionManager().startTransaction(
							DataType.GROUPPROFILE_GETMASTER,
							new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));
				} else {

					TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
							TastyToast.ERROR);
				}

			}

		} else if (sub_Items.equals(AppStrings.groupProfile)) {

			Log.d(TAG, String.valueOf(ConnectionUtils.isNetworkAvailable(getActivity())));

			isGroupProfileNavigate = true;

			if (ConnectionUtils.isNetworkAvailable(getActivity())) {

				try {
					new Get_GroupProfileTask(MainFragment_Dashboard.this).execute();
					Thread.sleep(2000);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					TastyToast.makeText(getActivity(), "Time Out Exception ", TastyToast.LENGTH_SHORT,
							TastyToast.ERROR);
					getActivity().finish();
				}

			} else if (!ConnectionUtils.isNetworkAvailable(getActivity())) {
				if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
					EShaktiApplication.getInstance().getTransactionManager().startTransaction(
							DataType.GROUPPROFILE_GETMASTER,
							new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));
				} else {

					TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
							TastyToast.ERROR);
				}

			}

		} else if (sub_Items.equals(AppStrings.uploadInfo)) {

			fragment = new MemberList_Fragment();
			setFragment(fragment);
		} else if (sub_Items.equals(AppStrings.mMobileNoUpdation)) {

			if (ConnectionUtils.isNetworkAvailable(getActivity())) {
				isGetMemberMobileNumber = true;
				new GetMemberMobileNumberWebservices(this).execute();

			} else {
				TastyToast.makeText(getActivity(), AppStrings.mMobileNoUpdationNetworkCheck, TastyToast.LENGTH_SHORT,
						TastyToast.ERROR);
			}

		} else if (sub_Items.equals(AppStrings.mAadhaarNoUpdation)) {

			if (ConnectionUtils.isNetworkAvailable(getActivity())) {
				isGetMemberAadhaarNumber = true;
				new Get_AadhaarNumber_Webservices(this).execute();

			} else {
				TastyToast.makeText(getActivity(), AppStrings.mAadhaarNoNetworkCheck, TastyToast.LENGTH_SHORT,
						TastyToast.ERROR);
			}

		} else if (sub_Items.equals(AppStrings.mSHGAccountNoUpdation)) {

			if (ConnectionUtils.isNetworkAvailable(getActivity())) {
				isGetShgAccountNumber = true;
				new Get_Shg_Account_Details_Webservices(this).execute();

			} else {
				TastyToast.makeText(getActivity(), AppStrings.mShgAccNoNetworkCheck, TastyToast.LENGTH_SHORT,
						TastyToast.ERROR);
			}

		} else if (sub_Items.equals(AppStrings.mAccountNoUpdation)) {

			if (ConnectionUtils.isNetworkAvailable(getActivity())) {
				isGetBankBranchName = true;
				new Get_BankBranchName_Webservices(this).execute();

			} else {
				TastyToast.makeText(getActivity(), AppStrings.mAccNoNetworkCheck, TastyToast.LENGTH_SHORT,
						TastyToast.ERROR);
			}

		} else if (sub_Items.equals(AppStrings.mCreditLinkageInfo)) {
			if (ConnectionUtils.isNetworkAvailable(getActivity())) {
				fragment = new Profile_CreditLinkageInfoFragment();
				setFragment(fragment);
			} else {

				TastyToast.makeText(getActivity(), AppStrings.mCreditLinkageAlert, TastyToast.LENGTH_SHORT,
						TastyToast.ERROR);
			}
		} else if (sub_Items.equals(AppStrings.Memberreports)) {
			fragment = new MemberList_Fragment();
			setFragment(fragment);
		} else if (sub_Items.equals(AppStrings.GroupReports)) {
			fragment = new Reports_GroupMenuReportFragment();
			setFragment(fragment);
		} else if (sub_Items.equals(AppStrings.transactionsummary)) {

			if (ConnectionUtils.isNetworkAvailable(getActivity())) {

				try {
					if (!isServiceCall_BankTransactionReport) {

						isServiceCall_BankTransactionReport = true;
						new Get_BankTransactionSummaryTask(MainFragment_Dashboard.this).execute();
						Thread.sleep(2000);

						isTransactionSummary = true;
					}

				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}

			} else if (!(ConnectionUtils.isNetworkAvailable(getActivity()))) {
				if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {

				} else {

					TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
							TastyToast.ERROR);
				}
			}

		} else if (sub_Items.equals(AppStrings.balanceSheet)) {
			/*
			 * isTrialBalanceReport = false; isBalanceSheetReport = true;
			 * Trial_BalanceSheetFragment.fromDate = ""; Trial_BalanceSheetFragment.toDate =
			 * ""; fragment = new Trial_BalanceSheetFragment();
			 * 
			 * setFragment(fragment);
			 */
		} else if (sub_Items.equals(AppStrings.trialBalance)) {
			/*
			 * isBalanceSheetReport = false; isTrialBalanceReport = true;
			 * Trial_BalanceSheetFragment.fromDate = ""; Trial_BalanceSheetFragment.toDate =
			 * ""; fragment = new Trial_BalanceSheetFragment();
			 * 
			 * setFragment(fragment);
			 */} else if (sub_Items.equals(AppStrings.bankBalance)) {
			isBankBalance = true;
			if (ConnectionUtils.isNetworkAvailable(getActivity())) {
				try {
					new Get_Bank_BalanceTask(MainFragment_Dashboard.this).execute();
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			} else if (!(ConnectionUtils.isNetworkAvailable(getActivity()))) {

				TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
						TastyToast.ERROR);
			}

		} else if (sub_Items.equals(AppStrings.Attendance)) {
			sSubMenu_Item = "8";
			try {
				FragmentManager fm = getActivity().getSupportFragmentManager();

				Dialog_TransactionDate dialog = new Dialog_TransactionDate(getActivity(), sub_Items);
				dialog.show(fm, "");
			} catch (Exception e) {
				e.printStackTrace();
			}

		} else if (sub_Items.equals(AppStrings.MinutesofMeeting)) {
			if (ConnectionUtils.isNetworkAvailable(getActivity())) {
				try {

					new Get_Group_Minutes_NewTask(this).execute();

				} catch (Exception e) {
					e.printStackTrace();
				}

				isMinutes = true;
			} else if (!ConnectionUtils.isNetworkAvailable(getActivity())) {
				// Do offline stuffs
				if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
					EShaktiApplication.getInstance().getTransactionManager().startTransaction(
							DataType.GET_MASTER_MINUTESOFMEETINGS,
							new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));
				} else {

					TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
							TastyToast.ERROR);
				}
			}

		} else if (sub_Items.equals(AppStrings.auditing)) {
			sSubMenu_Item = "10";
			try {

				Meeting_AuditingFragment.auditFromDate = Reset.reset(Meeting_AuditingFragment.auditFromDate);
				Meeting_AuditingFragment.auditToDate = Reset.reset(Meeting_AuditingFragment.auditToDate);
				Meeting_AuditingFragment.auditingDate = Reset.reset(Meeting_AuditingFragment.auditingDate);
				Meeting_AuditingFragment.auditorName = Reset.reset(Meeting_AuditingFragment.auditorName);
				Meeting_AuditingFragment.calFromDate = Reset.reset(Meeting_AuditingFragment.calFromDate);
				Meeting_AuditingFragment.calToDate = Reset.reset(Meeting_AuditingFragment.calToDate);

				FragmentManager fm = getActivity().getSupportFragmentManager();

				Dialog_TransactionDate dialog = new Dialog_TransactionDate(getActivity(), sub_Items);
				dialog.show(fm, "");
			} catch (Exception e) {
				e.printStackTrace();
			}

		} else if (sub_Items.equals(AppStrings.training)) {
			sSubMenu_Item = "11";
			Meeting_TrainingFragment.trainingDate = Reset.reset(Meeting_TrainingFragment.trainingDate);
			try {
				FragmentManager fm = getActivity().getSupportFragmentManager();

				Dialog_TransactionDate dialog = new Dialog_TransactionDate(getActivity(), sub_Items);
				dialog.show(fm, "");
			} catch (Exception e) {
				e.printStackTrace();
			}

		} else if (sub_Items.equals(AppStrings.passwordchange)) {
			if (ConnectionUtils.isNetworkAvailable(getActivity())) {

				fragment = new Settings_ChangePasswordFragment();
				setFragment(fragment);
			} else {
				if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {

					TastyToast.makeText(getActivity(), AppStrings.offline_ChangePwdAlert, TastyToast.LENGTH_SHORT,
							TastyToast.WARNING);
				} else {

					TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
							TastyToast.ERROR);
				}
			}
		} else if (sub_Items.equals(AppStrings.changeLanguage)) {

			try {

				if (ConnectionUtils.isNetworkAvailable(getActivity())) {

					ChangeLanguageDialog = new Dialog(getActivity());

					LayoutInflater li = (LayoutInflater) getActivity()
							.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					final View dialogView = li.inflate(R.layout.dialog_choose_language, null, false);

					TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.chooseLanguageHeader);
					confirmationHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.chooseLanguage));
					confirmationHeader.setTypeface(LoginActivity.sTypeface);
					final RadioGroup radioGroup = (RadioGroup) dialogView.findViewById(R.id.radioLanguage);
					RadioButton radioButton = (RadioButton) dialogView.findViewById(R.id.radioEnglish);
					RadioButton radioButton_reg = (RadioButton) dialogView.findViewById(R.id.radioRegional);
					radioButton.setText("English");
					radioButton.setTypeface(LoginActivity.sTypeface);

					// radioButton_reg.setText(RegionalConversion.getRegionalConversion(LoginTask.User_RegLanguage));

					if (LoginTask.User_RegLanguage.equals("Hindi")) {
						radioButton_reg.setText("हिंदी");
						Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "font/MANGAL.TTF");
						radioButton_reg.setTypeface(typeface);
					} else if (LoginTask.User_RegLanguage.equals("Marathi")) {
						radioButton_reg.setText("मराठी");
						Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),
                                "font/MANGALHindiMarathi.TTF");
						radioButton_reg.setTypeface(typeface);
					} else if (LoginTask.User_RegLanguage.equals("Kannada")) {
						radioButton_reg.setText("ಕನ್ನಡ");
						Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),
                                "font/tungaKannada.ttf");
						radioButton_reg.setTypeface(typeface);
					} else if (LoginTask.User_RegLanguage.equals("Malayalam")) {
						radioButton_reg.setText("മലയാളം");
						Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),
                                "font/MLKR0nttMalayalam.ttf");
						radioButton_reg.setTypeface(typeface);
					} else if (LoginTask.User_RegLanguage.equals("Punjabi")) {
						radioButton_reg.setText("ਪੰਜਾਬੀ ");
						Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),
                                "font/mangal-1361510185.ttf");
						radioButton_reg.setTypeface(typeface);
					} else if (LoginTask.User_RegLanguage.equals("Gujarathi")) {
						radioButton_reg.setText("ગુજરાતી");
						Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),
                                "font/shrutiGujarathi.ttf");
						radioButton_reg.setTypeface(typeface);
					} else if (LoginTask.User_RegLanguage.equals("Bengali")) {
						radioButton_reg.setText("বাঙ্গালী");
						Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),
                                "font/kalpurushBengali.ttf");
						radioButton_reg.setTypeface(typeface);
					} else if (LoginTask.User_RegLanguage.equals("Tamil")) {
						radioButton_reg.setText("தமிழ்");
						Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),
                                "font/TSCu_SaiIndira.ttf");
						radioButton_reg.setTypeface(typeface);
					} else if (LoginTask.User_RegLanguage.equals("Assamese")) {
						radioButton_reg.setText("Assamese");
						Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),
                                "font/KirtanUni_Assamese.ttf");
						radioButton_reg.setTypeface(typeface);
					} else {
						radioButton_reg.setText(RegionalConversion.getRegionalConversion(LoginTask.User_RegLanguage));
						Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "font/Exo-Medium.ttf");
						radioButton_reg.setTypeface(typeface);
					}
					// radioButton_reg.setTypeface(LoginActivity.sTypeface);

					ButtonFlat okButton = (ButtonFlat) dialogView.findViewById(R.id.dialog_yes_button);
					okButton.setText(RegionalConversion.getRegionalConversion(AppStrings.dialogOk));
					okButton.setTypeface(LoginActivity.sTypeface);
					okButton.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub

							mLanguageValue = true;
							mLanguageChangeValue = PrefUtils.getUserlangcode();
							int selectedId = radioGroup.getCheckedRadioButtonId();

							// find the radiobutton by returned id
							RadioButton radioLanguageButton = (RadioButton) dialogView.findViewById(selectedId);

							String mSelectedLang = radioLanguageButton.getText().toString();
							Log.v("On Selected language", mSelectedLang);
							if (mSelectedLang.equals("हिंदी")) {
								mSelectedLang = "Hindi";
							} else if (mSelectedLang.equals("मराठी")) {
								mSelectedLang = "Marathi";
							} else if (mSelectedLang.equals("ಕನ್ನಡ")) {
								mSelectedLang = "Kannada";
							} else if (mSelectedLang.equals("മലയാളം")) {
								mSelectedLang = "Malayalam";
							} else if (mSelectedLang.equals("ਪੰਜਾਬੀ ")) {
								mSelectedLang = "Punjabi";
							} else if (mSelectedLang.equals("ગુજરાતી")) {
								mSelectedLang = "Gujarathi";
							} else if (mSelectedLang.equals("বাঙ্গালী")) {
								mSelectedLang = "Bengali";
							} else if (mSelectedLang.equals("தமிழ்")) {
								mSelectedLang = "Tamil";
							} else if (mSelectedLang.equals("Assamese")) {
								mSelectedLang = "Assamese";
							}
							try {
								PrefUtils.setUserlangcode(mSelectedLang);
							} catch (Exception e) {
								e.printStackTrace();
							}

							LoginActivity.sTypeface = GetTypeface.getTypeface(getActivity(), mSelectedLang);

							RegionalserviceUtil.getRegionalService(getActivity(), mSelectedLang);
							try {

								SelectedGroupsTask.member_Id.clear();
								SelectedGroupsTask.member_Name.clear();
								SelectedGroupsTask.loan_EngName.clear();
								SelectedGroupsTask.loan_Id.clear();
								SelectedGroupsTask.loan_Name.clear();
								SelectedGroupsTask.sBankNames.clear();
								SelectedGroupsTask.sBankAmt.clear();
								SelectedGroupsTask.sEngBankNames.clear();
								SelectedGroupsTask.sCashatBank = "";
								SelectedGroupsTask.sCashinHand = "";

							} catch (Exception e) {
								e.printStackTrace();
							}

							try {
								PrefUtils.clearGroup_Offlinedata();
								PrefUtils.setLoginGroupService("1");
								EShaktiApplication.setIsChangeLanguage(true);

								EShaktiApplication.getInstance().getTransactionManager()
										.startTransaction(DataType.GROUPDETAILSDELETE, null);

								new Login_webserviceTask(MainFragment_Dashboard.this).execute();
								Thread.sleep(2000);
							} catch (InterruptedException e) {
								e.printStackTrace();

							}

							isLanguageSelection = true;
							ChangeLanguageDialog.dismiss();
						}
					});

					ChangeLanguageDialog.getWindow()
							.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
					ChangeLanguageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					ChangeLanguageDialog.setCanceledOnTouchOutside(false);
					ChangeLanguageDialog.setContentView(dialogView);
					ChangeLanguageDialog.setCancelable(true);
					ChangeLanguageDialog.show();
				} else {
					if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
						TastyToast.makeText(getActivity(), "Plz Go to Online", TastyToast.LENGTH_SHORT,
								TastyToast.ERROR);
					} else {

						TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
								TastyToast.ERROR);
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		} else if (sub_Items.equals(AppStrings.deactivateAccount)) {

			if (Boolean.valueOf(ConnectionUtils.isNetworkAvailable(getActivity()))) {

				fragment = new Settings_DeactivateAccountFragment();
				setFragment(fragment);
			} else if (Boolean.valueOf(!ConnectionUtils.isNetworkAvailable(getActivity()))) {
				if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {

					TastyToast.makeText(getActivity(), AppStrings.deactivateNetworkAlert, TastyToast.LENGTH_SHORT,
							TastyToast.WARNING);
				} else {

					TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
							TastyToast.ERROR);
				}
			}

		} else if (sub_Items.equals("ESHAKTI")) {
			fragment = new Helps_AboutYesbooksFragment();
			setFragment(fragment);

			/*
			 * ArrayList<DataModel> dataModelsList = new ArrayList<DataModel>(); if
			 * (PrefUtils.getDataValuesInit() == null ||
			 * !PrefUtils.getDataValuesInit().equals("1")) {
			 * PrefUtils.setDataValuesInit("1"); dataModelsList.clear(); for (int i = 0; i <
			 * 10; i++) { DataModel dataItem; dataItem = new DataModel("Content  "+i,
			 * false); dataModelsList.add(dataItem); } } else { dataModelsList =
			 * PrefUtils.getListValues(); }
			 * 
			 * FragmentManager fm = getActivity().getSupportFragmentManager();
			 * 
			 * Dialog_ListItemWithCheckboxFragment dialog = new
			 * Dialog_ListItemWithCheckboxFragment(getActivity(), dataModelsList);
			 * dialog.show(fm, "");
			 */
		} else if (sub_Items.equals(AppStrings.contacts)) {

			fragment = new Helps_ContactsFragment();
			setFragment(fragment);

		} else if (sub_Items.equals(AppStrings.mVideoManual)) {

			fragment = new Help_VideoManualFragment();
			setFragment(fragment);

		} else if (sub_Items.equals(AppStrings.mPdfManual)) {

			// CopyReadAssets();
			File file = new File(Environment.getExternalStorageDirectory(), "mobileapplication.pdf");

			if (file.exists()) {
				CopyReadAssets();
			} else {
				isPdfDownload = true;
				new DownloadPdfFileTask(MainFragment_Dashboard.this).execute();
			}

		} else if (sub_Items.equals(AppStrings.offlineReports)) {
			if (Boolean.valueOf(!ConnectionUtils.isNetworkAvailable(getActivity()))) {
				if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
					mOfflineNavigate = true;
					EShaktiApplication.setOfflineTransDate(true);
					EShaktiApplication.getInstance().getTransactionManager()
							.startTransaction(DataType.GET_TRANS_MASTERVALUES, null);
				}
			} else {
				mOfflineNavigate = true;
				EShaktiApplication.setOfflineTransDate(true);
				EShaktiApplication.getInstance().getTransactionManager()
						.startTransaction(DataType.GET_TRANS_MASTERVALUES, null);
			}
		} else if (sub_Items.equals("UPLOAD SCHEDULE VI")) {
			if (ConnectionUtils.isNetworkAvailable(getActivity())) {
				fragment = new Meeting_Upload_Schedule_form();
				setFragment(fragment);
			} else {
				TastyToast.makeText(getActivity(), AppStrings.mUploadScheduleAlert, TastyToast.LENGTH_SHORT,
						TastyToast.ERROR);
			}
		} else if (sub_Items.equals(AppStrings.mSendEmail)) {
			if (ConnectionUtils.isNetworkAvailable(getActivity())) {
				AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
				builder.setMessage("Do you want to send an email with database attachment?.");

				String positiveText = "YES";
				builder.setPositiveButton(positiveText, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// positive button logic
						// sendEmailWithDBAttachment(getActivity());
						sendFile(getActivity());
					}
				});

				String negativeText = "NO";
				builder.setNegativeButton(negativeText, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// negative button logic
						dialog.dismiss();
					}
				});

				AlertDialog dialog = builder.create();
				// display dialog
				dialog.show();

				TextView dialogMessage = (TextView) dialog.findViewById(android.R.id.message);
				dialogMessage.setTypeface(LoginActivity.sTypeface);

				Button yesButton = (Button) dialog.findViewById(android.R.id.button1);
				yesButton.setTypeface(LoginActivity.sTypeface);

				Button noButton = (Button) dialog.findViewById(android.R.id.button2);
				noButton.setTypeface(LoginActivity.sTypeface);
			} else {
				TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg, TastyToast.LENGTH_SHORT,
						TastyToast.WARNING);
			}
		} else if (sub_Items.equals("PAYMENTS")) {
			Payments_Internalloandisbursement_Fragment.isPaymentDisbursementBackPressed = false;
			Payments_Menu_Fragment payments_Menu_Fragment = new Payments_Menu_Fragment();
			setFragment(payments_Menu_Fragment);
		}
	}

	private void onSetCalendarValues() {
		// TODO Auto-generated method stub

		if (EShaktiApplication.getNextMonthLastDate() == null) {
			try {

				if (EShaktiApplication.isCalendarDateVisibleFlag()) {
					EShaktiApplication.setCalendarDateVisibleFlag(false);
				}
				Calendar calender = Calendar.getInstance();

				SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String formattedDate = df.format(calender.getTime());
				Log.e("Device Date  =  ", formattedDate + "");

				String mBalanceSheetDate = SelectedGroupsTask.sLastTransactionDate_Response; // dd/MM/yyyy
				String formatted_balancesheetDate = mBalanceSheetDate.replace("/", "-");
				Log.e("Balancesheet Date = ", formatted_balancesheetDate + "");

				Date balanceDate = df.parse(formatted_balancesheetDate);
				Date systemDate = df.parse(formattedDate);

				if (balanceDate.compareTo(systemDate) < 0 || balanceDate.compareTo(systemDate) == 0) {
					System.err.println(" balancesheet date is less than sys date");

					String mLastTransactionDate = SelectedGroupsTask.sLastTransactionDate_Response;

					String formattedDate_LastTransaction = mLastTransactionDate.replace("/", "-");

					String days = CalculateDateUtils.get_count_of_days(formattedDate_LastTransaction, formattedDate);

					int mDaycount = Integer.parseInt(days);
					Log.e("Total Days Count =====_____----", mDaycount + "");

					EShaktiApplication.setLastTransDate_GroupId(SelectedGroupsTask.Group_Id);

					if (mDaycount > 30) {

						SimpleDateFormat df_after = new SimpleDateFormat("dd-MM-yyyy");

						Calendar calendar = Calendar.getInstance();
						calendar.setTime(df_after.parse(formattedDate_LastTransaction));
						calendar.add(Calendar.MONTH, 1);
						calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
						Date nextMonthFirstDay = calendar.getTime();
						System.out.println(
								"------ Next month first date   =  " + df_after.format(nextMonthFirstDay) + "");

						Calendar cal_last_date = Calendar.getInstance();
						cal_last_date.setTime(df_after.parse(formattedDate_LastTransaction));
						cal_last_date.add(Calendar.MONTH, 1);
						cal_last_date.set(Calendar.DATE, cal_last_date.getActualMaximum(Calendar.DAY_OF_MONTH));
						Date nextMonthLastDay = cal_last_date.getTime();
						System.out
								.println("------ Next month Last date   =  " + df_after.format(nextMonthLastDay) + "");

						//
						String lastDateOfNextMonth = df_after.format(nextMonthLastDay);
						Calendar currentDate = Calendar.getInstance();
						int current_Day = currentDate.get(Calendar.DAY_OF_MONTH);
						int current_month = currentDate.get(Calendar.MONTH) + 1;
						int current_year = currentDate.get(Calendar.YEAR);
						String current_date = current_Day + "-" + current_month + "-" + current_year;

						Date lastdate = null, currentdate = null;
						String min_date_str = null;

						try {

							lastdate = df_after.parse(lastDateOfNextMonth);
							currentdate = df_after.parse(current_date);

						} catch (ParseException e) {
							// TODO: handle exception
							e.printStackTrace();
						}

						if (lastdate.compareTo(currentdate) <= 0) {
							min_date_str = lastDateOfNextMonth;
						} else {
							min_date_str = current_date;
						}

						Log.e("Minimum Date !!!!!	", min_date_str + "");

						EShaktiApplication.setNextMonthLastDate(min_date_str);

					} else {
						Calendar currentDate = Calendar.getInstance();
						SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
						EShaktiApplication.setNextMonthLastDate(sdf.format(currentDate.getTime()));

					}
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

	}

	private void SendEMailAlertDialog(final Activity context) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		final Dialog alertDialog = builder.create();
		// builder.setTitle("Location Services Not Active");
		builder.setMessage("Do you want to send an email with database attachment?.");
		builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialogInterface, int i) {
				// Show location settings when the user acknowledges the
				// alert dialog
				sendEmailWithDBAttachment(context);
			}
		});
		builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				alertDialog.dismiss();
			}
		});

		alertDialog.setCanceledOnTouchOutside(false);
		alertDialog.show();

	}

	public static void sendFile(Context context) {

		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("message/rfc822");// ("application/octet-stream");////("text/plain");
		intent.putExtra(Intent.EXTRA_EMAIL, new String[] { "prabhuraja@yesteam.in" });
		intent.putExtra(Intent.EXTRA_SUBJECT, "EShakti Application Database attached");
		intent.putExtra(Intent.EXTRA_TEXT, "");
		File root = Environment.getExternalStorageDirectory();
		String pathToMyAttachedFile = "backupname.db";
		File file = new File(root, pathToMyAttachedFile);
		Log.e("File Location  !!!!", file.getAbsolutePath().toString() + "");
		if (!file.exists() || !file.canRead()) {
			return;
		}

		Uri uri = FileProvider.getUriForFile(context, context.getPackageName() + ".provider", file);
		System.out.println("URI  Path %%%%%%%%%%%   =  " + uri);
		intent.putExtra(Intent.EXTRA_STREAM, uri);
		intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
		// context.startActivity(intent);
		final PackageManager pm = context.getPackageManager();
		final List<ResolveInfo> matches = pm.queryIntentActivities(intent, 0);
		ResolveInfo best = null;
		for (final ResolveInfo info : matches)
			if (info.activityInfo.packageName.endsWith(".gm") || info.activityInfo.name.toLowerCase().contains("gmail"))
				best = info;
		if (best != null)
			intent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
		context.startActivity(intent);
	}

	public static void sendEmailWithDBAttachment(Context context) {
		// TODO Auto-generated method stub
		try {
			Intent emailIntent = new Intent(Intent.ACTION_SEND);// works fine
			emailIntent.setType("message/rfc822");// ("application/octet-stream");////("text/plain");
			emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] { "prabhuraja@yesteam.in" });
			emailIntent.putExtra(Intent.EXTRA_SUBJECT, "EShakti Application Database attached");
			emailIntent.putExtra(Intent.EXTRA_TEXT, "");
			File root = Environment.getExternalStorageDirectory();
			String pathToMyAttachedFile = "backupname.db";
			File file = new File(root, pathToMyAttachedFile);
			Log.e("File Location  !!!!", file.getAbsolutePath().toString() + "");
			if (!file.exists() || !file.canRead()) {
				return;
			}
			Uri uri = Uri.fromFile(file);
			emailIntent.putExtra(Intent.EXTRA_STREAM, uri);
			final PackageManager pm = context.getPackageManager();
			final List<ResolveInfo> matches = pm.queryIntentActivities(emailIntent, 0);
			ResolveInfo best = null;
			for (final ResolveInfo info : matches)
				if (info.activityInfo.packageName.endsWith(".gm")
						|| info.activityInfo.name.toLowerCase().contains("gmail"))
					best = info;
			if (best != null)
				emailIntent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
			context.startActivity(emailIntent);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void CopyReadAssets() {
		File file = new File(Environment.getExternalStorageDirectory(), "mobileapplication.pdf");
		System.out.println("Pdf file Path  =  " + file + "");
		try {

			Intent intent = new Intent(Intent.ACTION_VIEW);
			Uri fileUri = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", file);
			intent.setDataAndType(fileUri, "application/pdf");
			intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
			intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

			Intent intent1 = Intent.createChooser(intent, "Open File");

			PackageManager pm = getActivity().getPackageManager();
			if (intent.resolveActivity(pm) != null) {
				startActivity(intent1);
			}

		} catch (ActivityNotFoundException e) {
			// TODO: handle exception
			TastyToast.makeText(getActivity(), "No pdf viewer installed, please install any pdf viewer.",
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
		}
	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub
		mProgressDialog = AppDialogUtils.createProgressDialog(getActivity());
		mProgressDialog.show();

	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub

		try {

			if (mProgressDialog != null) {
				mProgressDialog.dismiss();
				System.out.println("Check Exception Constant@@@@@@@@ :" + result);
				if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {
					System.out.println("--------Exception Executing------");
					getActivity().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							if (!result.equals("FAIL")) {

								TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg,
										TastyToast.LENGTH_SHORT, TastyToast.ERROR);

								Constants.NETWORKCOMMONFLAG = "SUCCESS";
							}
							if (mLanguageValue) {
								if (mLanguageChangeValue != null) {

									LoginActivity.sTypeface = GetTypeface.getTypeface(getActivity(),
											mLanguageChangeValue);
									mLanguageValue = false;

									TastyToast.makeText(getActivity(), AppStrings.changeLanguageNetworkException,
											TastyToast.LENGTH_SHORT, TastyToast.ERROR);
								}
							}
							isServiceCall = false;
						}
					});

				} else {

					if (isPdfDownload) {
						isPdfDownload = false;
						CopyReadAssets();
					} else {

						if (isGetMemberAccountNumber) {
							isGetMemberAccountNumber = false;

							if (publicValues.mGetBankBranchValues != null
									&& !publicValues.mGetBankBranchValues.equals("No")) {

								Profile_Member_Account_Number_UpdationFragment fragment = new Profile_Member_Account_Number_UpdationFragment();
								setFragment(fragment);
							} else {
								TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT,
										TastyToast.ERROR);
							}
						} else {

							if (isGetBankBranchName) {

								isGetMemberAccountNumber = true;

								new Get_MemberAccountNumber_Webservices(this).execute();

								isGetBankBranchName = false;

							} else {

								if (isGetShgAccountNumber) {
									isGetShgAccountNumber = false;
									if (publicValues.mGetShgAccountNumberValues != null
											&& !publicValues.mGetShgAccountNumberValues.equals("No")) {

										Profile_Shg_AccountNumberUpdation_Fragment fragment = new Profile_Shg_AccountNumberUpdation_Fragment();
										setFragment(fragment);
									} else {
										TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT,
												TastyToast.ERROR);
									}
								} else {
									if (isGetMemberAadhaarNumber) {
										isGetMemberAadhaarNumber = false;
										if (publicValues.mGetMemberAadhaarNumberValues != null
												&& !publicValues.mGetMemberAadhaarNumberValues.equals("No")) {

											Profile_Member_Aadhaar_UpdationFragment fragment = new Profile_Member_Aadhaar_UpdationFragment();
											setFragment(fragment);
										} else {
											TastyToast.makeText(getActivity(), AppStrings.tryLater,
													TastyToast.LENGTH_SHORT, TastyToast.ERROR);
										}
									} else {
										if (isGetMemberMobileNumber) {
											isGetMemberMobileNumber = false;
											if (publicValues.mGetMemberMobileNumberValues != null
													&& !publicValues.mGetMemberMobileNumberValues.equals("No")) {

												Profile_Member_Mobileno_Update_Fragment fragment = new Profile_Member_Mobileno_Update_Fragment();
												setFragment(fragment);
											} else {
												TastyToast.makeText(getActivity(), AppStrings.tryLater,
														TastyToast.LENGTH_SHORT, TastyToast.ERROR);
											}
										} else {

											if (isAnimatorValues) {
												isAnimatorValues = false;
												AppDialogUtils.showConfirmation_CheckBackLogDialog(getActivity());
											} else {

												if (iSChecklist) {
													iSChecklist = false;
													Transaction_CheckListFragment checkListFragment = new Transaction_CheckListFragment();
													setFragment(checkListFragment);
												} else {

													if (Boolean.valueOf(isLanguageSelection)) {
														if (Boolean.valueOf(EShaktiApplication.isAgent)) {

															mProgressDialog_thread.show();
															new Thread(new Runnable() {

																@Override
																public void run() {
																	// TODO Auto-generated method stub
																	EShaktiApplication.getInstance()
																			.getTransactionManager().startTransaction(
																					DataType.GROUPNAMEDELETE, null);
																	try {
																		Thread.sleep(300);
																	} catch (InterruptedException e1) {
																		// TODO Auto-generated catch
																		// block
																		e1.printStackTrace();
																	}
																	try {
																		Intent intentservice = new Intent(getActivity(),
																				GroupDetailsAddService.class);
																		getActivity().startService(intentservice);

																	} catch (Exception e) {
																		e.printStackTrace();
																	}

																	mProgressDialog_thread.dismiss();

																	isLanguageSelection = false;

																	Constants.BUTTON_CLICK_FLAG = "0";
																	EShaktiApplication.setSubmenuclicked(false);

																	PrefUtils.clearLoginValues();
																	PrefUtils.clearGrouplistValues();

																	startActivity(new Intent(getActivity(),
																			GroupListActivity.class));
																	getActivity().overridePendingTransition(
																			R.anim.right_to_left_in,
																			R.anim.right_to_left_out);
																	getActivity().finish();
																}
															}).start();
															/** DB **/

														} else if (Boolean.valueOf(!EShaktiApplication.isAgent)) {
															try {
																getActivity().startService(new Intent(getActivity(),
																		GroupDetailsService.class));

																isLanguageSelection = false;

																Constants.BUTTON_CLICK_FLAG = "0";
																EShaktiApplication.setSubmenuclicked(false);

																startActivity(
																		new Intent(getActivity(), MainActivity.class));
																getActivity().overridePendingTransition(
																		R.anim.right_to_left_in,
																		R.anim.right_to_left_out);
																getActivity().finish();
															} catch (Exception e) {
																e.printStackTrace();
															}
														}
													} else if (Boolean.valueOf(isAgentProfileNavigate)) {
														isAgentProfileNavigate = false;
														Profile_AgentProfileFragment fragment = new Profile_AgentProfileFragment();

														setFragment(fragment);
													} else if (Boolean.valueOf(isGroupProfileNavigate)) {
														isGroupProfileNavigate = false;
														Profile_GroupProfileFragment fragment = new Profile_GroupProfileFragment();
														setFragment(fragment);
													} else if (Boolean.valueOf(isBankBalance)) {
														isBankBalance = false;
														Reports_BankBalanceFragment fragment = new Reports_BankBalanceFragment();
														setFragment(fragment);
													} else if (Boolean.valueOf(isMinutes)) {
														sSubMenu_Item = "9";
														isMinutes = false;
														Log.e(TAG, "Dialog check onTask");
														try {
															FragmentManager fm = getActivity()
																	.getSupportFragmentManager();

															Dialog_TransactionDate dialog = new Dialog_TransactionDate(
																	getActivity(), sub_Items);
															dialog.show(fm, "");
														} catch (Exception e) {
															e.printStackTrace();
														}

													} else if (Boolean.valueOf(isTransactionSummary)) {
														isTransactionSummary = false;
														String responseArr[] = Get_BankTransactionSummaryTask.sBank_summary_Response
																.split("~");

														if (responseArr.length > 1) {

															fragment = new Reports_BankTransactionReportFragment();
															setFragment(fragment);
														} else {

															TastyToast.makeText(getActivity(),
																	AppStrings.nobanktransactionreportdatas,
																	TastyToast.LENGTH_SHORT, TastyToast.WARNING);
														}

													}

													if (Boolean.valueOf(isNavigate)) {

														if (Boolean.valueOf(
																ConnectionUtils.isNetworkAvailable(getActivity()))) {

															try {
																isServiceCall = false;
																FragmentManager fm = getActivity()
																		.getSupportFragmentManager();

																Dialog_TransactionDate dialog = new Dialog_TransactionDate(
																		getActivity(), sub_Items);
																dialog.show(fm, "");

															} catch (Exception e) {
																e.printStackTrace();
															}

														} else if (!Boolean.valueOf(
																ConnectionUtils.isNetworkAvailable(getActivity()))) {
															if (EShaktiApplication.getLoginFlag()
																	.equals(Constants.OFFLINEFLAG)) {

															} else {

																TastyToast.makeText(getActivity(),
																		AppStrings.mLoginAlert_ONOFF,
																		TastyToast.LENGTH_SHORT, TastyToast.ERROR);
															}

														}
													}
												}
											}
										}
									}
								}
							}
						}
					}

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Subscribe
	public void OnGroupMasterResponse(final Group_ProfileResponse groupProfileResponse) {
		switch (groupProfileResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), groupProfileResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), groupProfileResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			if (Boolean.valueOf(isAgentProfileNavigate)) {
				isAgentProfileNavigate = false;

				if (GetGroupMemberDetails.getAgentprofile() != null) {
					Get_AgentProfileTask.sGetAgentProfile_Response = GetGroupMemberDetails.getAgentprofile();

					fragment = new Profile_AgentProfileFragment();
					setFragment(fragment);
				} else {
					TastyToast.makeText(getActivity(), "Agent profile is empty", TastyToast.LENGTH_SHORT,
							TastyToast.ERROR);
				}

			} else if (Boolean.valueOf(isGroupProfileNavigate)) {
				isGroupProfileNavigate = false;

				if (GetGroupMemberDetails.getGroupprofile() != null) {

					Get_GroupProfileTask.sGetGroupProfile_Response = GetGroupMemberDetails.getGroupprofile();

					fragment = new Profile_GroupProfileFragment();
					setFragment(fragment);
				} else {
					TastyToast.makeText(getActivity(), "Group profile is empty", TastyToast.LENGTH_SHORT,
							TastyToast.ERROR);
				}
			}

			break;
		}

	}

	@Subscribe
	public void OnGroupMasterPLDBResponse(final GroupMasterPLDBResponse groupMasterPLDBResponse) {
		switch (groupMasterPLDBResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), groupMasterPLDBResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), groupMasterPLDBResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			mPLDBResponse = GetGroupMemberDetails.getPersonaloanOS();
			mPLDBPurposeofloan = GetGroupMemberDetails.getPurposeofloan();
			sSubMenu_Item = "7";
			try {
				FragmentManager fm = getActivity().getSupportFragmentManager();

				Dialog_TransactionDate dialog = new Dialog_TransactionDate(getActivity(), sub_Items);
				dialog.show(fm, "");
			} catch (Exception e) {
				e.printStackTrace();
			}

			break;
		}

	}

	@Subscribe
	public void OnGroupMasterMinutesofmeetingResponse(final GetMinutesofMeetingResponse getMinutesofMeetingResponse) {
		switch (getMinutesofMeetingResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), getMinutesofMeetingResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), getMinutesofMeetingResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			String mMasterMinutes = GetGroupMemberDetails.getMasterminutes();
			String mIndividualMinutes = GetGroupMemberDetails.getIndividualgroupminutes();
			String mMasterMinut[] = mMasterMinutes.split("~");
			String mIndi[] = mIndividualMinutes.split("~");
			int mSize = 0;

			mSize = mMasterMinut.length / 2;
			Log.i("Group Length", mSize + "");
			String mMasterValues[] = null, mMasterId[] = null;
			StringBuilder builder = new StringBuilder();
			String mMasterresponse, result;
			mMasterValues = new String[mSize];
			mMasterId = new String[mSize];
			int j = 0, k = 0;

			for (int i = 0; i < mMasterMinut.length; i++) {

				if (i % 2 == 0) {
					mMasterId[j] = mMasterMinut[i];
					j++;
				} else {

					mMasterValues[k] = mMasterMinut[i];
					k++;

				}
			}

			for (int i = 0; i < mIndi.length; i++) {
				String mIndiValues = mIndi[i];

				for (int m = 0; m < mMasterId.length; m++) {

					if (mIndiValues.equals(mMasterId[m])) {

						mMasterresponse = mIndiValues + "~" + mMasterValues[m] + "~";

						builder.append(mMasterresponse);
					}
				}

			}
			result = builder.toString();

			Get_Group_Minutes_NewTask.sGet_Group_Minutes__new_Response = result;

			sSubMenu_Item = "9";
			try {
				FragmentManager fm = getActivity().getSupportFragmentManager();

				Dialog_TransactionDate dialog = new Dialog_TransactionDate(getActivity(), sub_Items);
				dialog.show(fm, "");
			} catch (Exception e) {
				e.printStackTrace();
			}

			break;
		}

	}

	@Subscribe
	public void OnValues(final ArrayList<Transaction> arrayList) {
		if (mOfflineNavigate) {
			EShaktiApplication.setOfflineTransDate(false);
			mOfflineNavigate = false;
			if (arrayList.size() != 0 && arrayList.get(0).getUniqueId() != null) {
				try {
					for (int i = 0; i < arrayList.size(); i++) {
						// String mOfflineGroupresponse =
						// arrayList.get(i).getTransactionDate();

					}

					fragment = new Offline_ReportDateListFragment();
					setFragment(fragment);
				} catch (Exception e) {
					e.printStackTrace();
				}

			} else {
				TastyToast.makeText(getActivity(), AppStrings.noofflinedatas, TastyToast.LENGTH_SHORT,
						TastyToast.WARNING);
			}
		}
	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub
		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		SBus.INST.unRegister(this);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		System.out.println("---------------------- onActivityResult  ");
		switch (requestCode) {
		case 0:
			try {
				FragmentManager fm = getActivity().getSupportFragmentManager();

				Dialog_TransactionDate dialog = new Dialog_TransactionDate(getActivity(), sub_Items);
				dialog.show(fm, "");
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;

		default:
			break;
		}
	}
}
