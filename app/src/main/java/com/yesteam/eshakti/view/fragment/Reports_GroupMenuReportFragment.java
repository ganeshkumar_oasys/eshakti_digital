package com.yesteam.eshakti.view.fragment;

import java.util.ArrayList;
import java.util.List;

import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.adapter.CustomListAdapter;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.model.ListItem;
import com.yesteam.eshakti.model.RowItem;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.Get_GroupSavingsSummaryTask;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

public class Reports_GroupMenuReportFragment extends Fragment implements TaskListener, OnItemClickListener {

	private TextView mGroupName, mCashInHand, mCashAtBank;
	private Dialog mProgressDialog;
	public static List<RowItem> rowItems;
	public static String groupMenuReportArr[];;
	int size;
	public static boolean isNavigate;

	private ListView mListView;
	private List<ListItem> listItems;
	private CustomListAdapter mAdapter;
	int listImage;
	private TextView mHeader;

	public Reports_GroupMenuReportFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_GROUP_REPORTS_MENU;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		groupMenuReportArr = new String[] { RegionalConversion.getRegionalConversion(AppStrings.groupSavingssummary),
				RegionalConversion.getRegionalConversion(AppStrings.groupLoansummary) };
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_menulist, container, false);

		MainFragment_Dashboard.isBackpressed = false;

		try {

			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashInHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashInHand.setTypeface(LoginActivity.sTypeface);

			mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashAtBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashAtBank.setTypeface(LoginActivity.sTypeface);

			mHeader = (TextView) rootView.findViewById(R.id.submenuHeaderTextview);
			mHeader.setVisibility(View.VISIBLE);
			mHeader.setText(
					RegionalConversion.getRegionalConversion(AppStrings.GroupReports));
			mHeader.setTypeface(LoginActivity.sTypeface);
			
			size = groupMenuReportArr.length;
			System.out.println("Length of menu list:" + size);

			listItems = new ArrayList<ListItem>();
			mListView = (ListView) rootView.findViewById(R.id.fragment_List);
			listImage = R.drawable.ic_navigate_next_white_24dp;

			for (int i = 0; i < size; i++) {
				ListItem rowItem = new ListItem(RegionalConversion.getRegionalConversion(groupMenuReportArr[i]),
						listImage);
				listItems.add(rowItem);
			}

			mAdapter = new CustomListAdapter(getActivity(), listItems);
			mListView.setAdapter(mAdapter);
			mListView.setOnItemClickListener(this);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return rootView;
	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub
		mProgressDialog = AppDialogUtils.createProgressDialog(getActivity());
		mProgressDialog.show();
	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub
		if (mProgressDialog != null) {
			mProgressDialog.dismiss();
			if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {
				getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub

						if (!result.equals("FAIL")) {

							TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg, TastyToast.LENGTH_SHORT,
									TastyToast.ERROR);
							Constants.NETWORKCOMMONFLAG = "SUCESS";
						}

					}
				});
			} else {
				try {
					if (Boolean.valueOf(isNavigate)) {

						Reports_GroupSavingsSummaryFragment fragment = new Reports_GroupSavingsSummaryFragment();
						setFragment(fragment);
					}

				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		}

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		// TODO Auto-generated method stub

		TextView textColor_Change = (TextView) view.findViewById(R.id.dynamicText);
		textColor_Change.setText(String.valueOf(groupMenuReportArr[position]));
		textColor_Change.setTextColor(Color.rgb(251, 161, 108));

		String menuItem = groupMenuReportArr[position];
		System.out.println("Selected Menu Item :" + menuItem);
		try {
			if (menuItem.equals(RegionalConversion.getRegionalConversion(AppStrings.groupSavingssummary))) {
				if (EShaktiApplication.getLoginFlag().equals(Constants.ONLINEFLAG)) {
					try {
						new Get_GroupSavingsSummaryTask(this).execute();
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
				} else {
					
					TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
							TastyToast.ERROR);
				}
				isNavigate = true;

			} else if (menuItem.equals(RegionalConversion.getRegionalConversion(AppStrings.groupLoansummary))) {

				Reports_GroupLoanListReportFragment fragment = new Reports_GroupLoanListReportFragment();
				setFragment(fragment);
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub
		Log.e("Current Class Name!!!!!!!!!!!!!!", fragment.getClass().getName());
		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

	}
}
