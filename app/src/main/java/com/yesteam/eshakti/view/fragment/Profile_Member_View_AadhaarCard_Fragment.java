package com.yesteam.eshakti.view.fragment;


import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Profile_Member_View_AadhaarCard_Fragment extends Fragment {
	TableLayout confirmationTable;

	private static String mAadhaarcardValues[] = { "UID", "NAME", "GENDER", "CO", "HOUSE", "VILLAGE", "VC_POST", "POST",
			"DIST", "SUB DIST", "STATE", "PINCODE", "DOB" };
	private static String mQrcardValues[];
	private TextView mGroupName, mCashInHand, mCashAtBank;

	public Profile_Member_View_AadhaarCard_Fragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_AADHAAR_VIEW;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_memberview_aadhaarcard, container, false);

		confirmationTable = (TableLayout) rootView.findViewById(R.id.confirmationTable_view);

		mQrcardValues = Profile_Member_Aadhaar_Image_View_MenuFragment.mServiceResponse.split("~");
		
		setViewValues();

		try {
			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashInHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashInHand.setTypeface(LoginActivity.sTypeface);

			mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashAtBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashAtBank.setTypeface(LoginActivity.sTypeface);

		/*	mHeadertext = (TextView) rootView.findViewById(R.id.fragment_agent_group_profile_headertext);
			mHeadertext.setText(RegionalConversion.getRegionalConversion(AppStrings.groupProfile));
			mHeadertext.setTypeface(LoginActivity.sTypeface);*/
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return rootView;
	}

	private void setViewValues() {
		// TODO Auto-generated method stub
		
		if (mQrcardValues.length==1) {
		 

				TableRow indv_AadhardetailsRow = new TableRow(getActivity());

				TableRow.LayoutParams contentParams = new TableRow.LayoutParams(0, LayoutParams.WRAP_CONTENT, 1f);
				contentParams.setMargins(10, 5, 10, 5);

				TextView memberName_Text = new TextView(getActivity());
				 
				memberName_Text.setText(String.valueOf("AADHAAR NUMBER : "));
				memberName_Text.setPadding(5, 5, 5, 5);
				memberName_Text.setLayoutParams(contentParams);
				indv_AadhardetailsRow.addView(memberName_Text);

				TextView confirm_values = new TextView(getActivity());
				confirm_values.setText(String.valueOf(mQrcardValues[0]));
				confirm_values.setPadding(5, 5, 15, 5);
				confirm_values.setGravity(Gravity.RIGHT);
				confirm_values.setLayoutParams(contentParams);
				indv_AadhardetailsRow.addView(confirm_values);

				confirmationTable.addView(indv_AadhardetailsRow,
						new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

			
			View rullerView = new View(getActivity());
			rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
			rullerView.setBackgroundColor(Color.rgb(0, 199, 140));
			confirmationTable.addView(rullerView);
		}else{
			for (int i = 0; i < mAadhaarcardValues.length; i++) {

				TableRow indv_AadhardetailsRow = new TableRow(getActivity());

				TableRow.LayoutParams contentParams = new TableRow.LayoutParams(0, LayoutParams.WRAP_CONTENT, 1f);
				contentParams.setMargins(10, 5, 10, 5);

				TextView memberName_Text = new TextView(getActivity());
				if (mAadhaarcardValues[i].equals("") || mAadhaarcardValues[i].equals(null)) {
					memberName_Text.setText(String.valueOf("No Values"));
				}
				memberName_Text.setText(String.valueOf(mAadhaarcardValues[i]));
				memberName_Text.setPadding(5, 5, 5, 5);
				memberName_Text.setLayoutParams(contentParams);
				indv_AadhardetailsRow.addView(memberName_Text);

				TextView confirm_values = new TextView(getActivity());
				confirm_values.setText(String.valueOf(mQrcardValues[i]));
				confirm_values.setPadding(5, 5, 15, 5);
				confirm_values.setGravity(Gravity.RIGHT);
				confirm_values.setLayoutParams(contentParams);
				indv_AadhardetailsRow.addView(confirm_values);

				confirmationTable.addView(indv_AadhardetailsRow,
						new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

			}
			View rullerView = new View(getActivity());
			rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
			rullerView.setBackgroundColor(Color.rgb(0, 199, 140));
			confirmationTable.addView(rullerView);
		}
		

	}

}