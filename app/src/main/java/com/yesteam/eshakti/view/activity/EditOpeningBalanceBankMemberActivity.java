package com.yesteam.eshakti.view.activity;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.utils.CustomTypefaceSpan;
import com.yesteam.eshakti.utils.GetExit;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.utils.GetTypeface;
import com.yesteam.eshakti.utils.Get_EdiText_Filter;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.utils.Reset;
import com.yesteam.eshakti.utils.TextviewUtils;
import com.yesteam.eshakti.views.RaisedButton;
import com.yesteam.eshakti.webservices.Get_Edit_OpeningbalanceWebservice;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.InputType;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.TextAppearanceSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class EditOpeningBalanceBankMemberActivity extends AppCompatActivity implements OnClickListener {

	public static final String TAG = EditOpeningBalanceBankMemberActivity.class.getSimpleName();
	private TextView mGroupName, mHeaderTextView, mHeaderText;
	private TableLayout mHeaderTable, mContentTable;
	private RaisedButton mSubmit_RaisedButton;

	List<EditText> sSavingsFields;
	private EditText mSavings_values;
	int mSize;
	String[] mEditMasterValues;
	public static Vector<String> mInternalOSVectorMemberId, mInternalOSVectorLoanId, mInternalOSVectorOS;
	String mLoanId = null, mLoanName = null;

	Dialog confirmationDialog;
	private static String sOutstandingAmounts[];
	public static String sSendToServer_Outstanding;
	private Button mEdit_RaisedButton, mOk_RaisedButton;
	String[] confirmArr;
	private String mAmount_Values;
	public static int sOutstandingAmount_Total;
	String nullVlaue = "0";
	String mLanguageLocalae;
	boolean mIsNegativeValues = false;
	LinearLayout mMemberNameLayout;
	TextView mMemberName;

	public EditOpeningBalanceBankMemberActivity() {
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		mInternalOSVectorMemberId.clear();
		mInternalOSVectorLoanId.clear();
		mInternalOSVectorOS.clear();

		sOutstandingAmounts = null;

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_ob_internalloan);
		mInternalOSVectorMemberId = new Vector<String>();
		mInternalOSVectorLoanId = new Vector<String>();
		mInternalOSVectorOS = new Vector<String>();
		 
		sOutstandingAmount_Total = Integer.valueOf(nullVlaue);

		sSavingsFields = new ArrayList<EditText>();

		try {
			mGroupName = (TextView) findViewById(R.id.groupname_edit_Internal);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mHeaderTextView = (TextView) findViewById(R.id.fragmentHeader_edit_Internal);
			mHeaderTextView.setText(RegionalConversion.getRegionalConversion(AppStrings.mMemberLoanOutstanding));
			mHeaderTextView.setTypeface(LoginActivity.sTypeface);

			mHeaderText = (TextView) findViewById(R.id.header_edit_Internal);
			mHeaderText.setTypeface(LoginActivity.sTypeface);

			mHeaderTable = (TableLayout) findViewById(R.id.headerTable_edit_Internal);
			mContentTable = (TableLayout) findViewById(R.id.contentTableLayout_edit_Internal);
			
			mMemberNameLayout = (LinearLayout) findViewById(R.id.member_name_layout);
			mMemberName = (TextView) findViewById(R.id.member_name);

			if (publicValues.mMasterEditOpeningBalanceValues != null) {
				mEditMasterValues = publicValues.mMasterEditOpeningBalanceValues.split("!");
				String mEditInternaBanklOS;
				mEditInternaBanklOS = mEditMasterValues[2];
				String[] mEditInternalOS_indi = mEditInternaBanklOS.split("%");
				publicValues.mBankLoanSize_BankLoan = mEditInternalOS_indi.length;

				for (int i = 0; i < mEditInternalOS_indi.length; i++) {
					String mEditInternalOS = mEditInternalOS_indi[publicValues.mCurrentBankLoanSize_BankLoan];

					String[] mEditInternalBankLoan_indi = mEditInternalOS.split("#");

					for (int j = 0; j < mEditInternalBankLoan_indi.length; j++) {
						String[] mTempValues;
						mTempValues = mEditInternalBankLoan_indi[j].split("~");
						mInternalOSVectorOS.addElement(
								mTempValues[2].toString().toString().substring(0, mTempValues[2].indexOf(".")));
						mInternalOSVectorLoanId.addElement(mTempValues[1].toString());
						mLoanId = mTempValues[1];

					}
				}

				for (int i = 0; i < SelectedGroupsTask.loan_Id.size(); i++) {
					if (SelectedGroupsTask.loan_Id.elementAt(i).equals(mLoanId)) {
						mLoanName = SelectedGroupsTask.loan_Name.elementAt(i);
					}
				}

				mHeaderText.setText(mLoanName + "  " + AppStrings.mTermLoanOutstanding);
			}

			mSubmit_RaisedButton = (RaisedButton) findViewById(R.id.fragment_Submit_button_edit_Internal);
			mSubmit_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.mConfirm));
			mSubmit_RaisedButton.setTypeface(LoginActivity.sTypeface);
			mSubmit_RaisedButton.setOnClickListener(this);

			buildTableHeaderLayout();

			buildTableContentLayout();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	private void buildTableHeaderLayout() {
		// TODO Auto-generated method stub

		TableRow leftHeaderRow = new TableRow(this);

		TableRow.LayoutParams lHeaderParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT, 1f);

		TextView mMemberName_headerText = new TextView(this);
		mMemberName_headerText.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.memberName)));
		mMemberName_headerText.setTypeface(LoginActivity.sTypeface);
		mMemberName_headerText.setTextColor(Color.WHITE);
		mMemberName_headerText.setPadding(20, 5, 10, 5);
		mMemberName_headerText.setLayoutParams(lHeaderParams);
		leftHeaderRow.addView(mMemberName_headerText);

		TextView mSavingsAmount_HeaderText = new TextView(this);
		mSavingsAmount_HeaderText
				.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.OutstandingAmount)));
		mSavingsAmount_HeaderText.setTypeface(LoginActivity.sTypeface);
		mSavingsAmount_HeaderText.setTextColor(Color.WHITE);
		mSavingsAmount_HeaderText.setPadding(10, 5, 50, 5);
		mSavingsAmount_HeaderText.setLayoutParams(lHeaderParams);
		mSavingsAmount_HeaderText.setGravity(Gravity.RIGHT);
		mSavingsAmount_HeaderText.setSingleLine(true);
		leftHeaderRow.addView(mSavingsAmount_HeaderText);

		mHeaderTable.addView(leftHeaderRow);

	}

	@SuppressWarnings("deprecation")
	private void buildTableContentLayout() {
		// TODO Auto-generated method stub
		mSize = SelectedGroupsTask.member_Name.size();
		Log.d(TAG, String.valueOf(mSize));

		for (int i = 0; i < mSize; i++) {
			TableRow leftContentRow = new TableRow(this);

			TableRow.LayoutParams leftContentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1f);
			leftContentParams.setMargins(5, 5, 5, 5);

			final TextView memberName_Text = new TextView(this);
			memberName_Text.setText(
					GetSpanText.getSpanString(this, String.valueOf(SelectedGroupsTask.member_Name.elementAt(i))));
			memberName_Text.setTypeface(LoginActivity.sTypeface);
			memberName_Text.setTextColor(color.black);
			memberName_Text.setPadding(15, 5, 5, 5);
			memberName_Text.setLayoutParams(leftContentParams);
			memberName_Text.setWidth(200);
			memberName_Text.setSingleLine(true);
			memberName_Text.setEllipsize(TextUtils.TruncateAt.END);
			leftContentRow.addView(memberName_Text);

			TableRow.LayoutParams leftContentParams1 = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1f);
			leftContentParams1.setMargins(5, 5, 80, 5);

			mSavings_values = new EditText(this);

			mSavings_values.setId(i);
			sSavingsFields.add(mSavings_values);
			mSavings_values.setPadding(5, 5, 5, 5);
			mSavings_values.setBackgroundResource(R.drawable.edittext_background);
			mSavings_values.setLayoutParams(leftContentParams1);
			mSavings_values.setTextAppearance(this, R.style.MyMaterialTheme);
			mSavings_values.setFilters(Get_EdiText_Filter.editText_filter());
			mSavings_values.setInputType(InputType.TYPE_CLASS_NUMBER);
			mSavings_values.setTextColor(color.black);
			mSavings_values.setWidth(150);
			mSavings_values.setText(mInternalOSVectorOS.elementAt(i));
			mSavings_values.setGravity(Gravity.RIGHT);
			mSavings_values.setOnFocusChangeListener(new OnFocusChangeListener() {
				
				@Override
				public void onFocusChange(View v, boolean hasFocus) {
					// TODO Auto-generated method stub
					if (hasFocus) {
						mMemberNameLayout.setVisibility(View.VISIBLE);
						mMemberName.setText(memberName_Text.getText().toString().trim());
						mMemberName.setTypeface(LoginActivity.sTypeface);
						TextviewUtils.manageBlinkEffect(mMemberName, EditOpeningBalanceBankMemberActivity.this);
					} else {
						mMemberNameLayout.setVisibility(View.GONE);
						mMemberName.setText("");
					}
				}
			}); 
			leftContentRow.addView(mSavings_values);

			mContentTable.addView(leftContentRow);

		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {
		case R.id.fragment_Submit_button_edit_Internal:

			try {

				mMemberNameLayout.setVisibility(View.GONE);
				mMemberName.setText("");

				sOutstandingAmounts = new String[SelectedGroupsTask.member_Id.size()];
				// sIncome_Total = 0;

				confirmArr = new String[mSize];

				sSendToServer_Outstanding = "";
				sOutstandingAmount_Total = Integer.valueOf(nullVlaue);

				StringBuilder builder = new StringBuilder();

				for (int i = 0; i < SelectedGroupsTask.member_Id.size(); i++) {

					sOutstandingAmounts[i] = sSavingsFields.get(i).getText().toString();

					if ((sOutstandingAmounts[i].equals("")) || (sOutstandingAmounts[i] == null)) {
						sOutstandingAmounts[i] = nullVlaue;
					}

					if (sOutstandingAmounts[i].matches("\\d*\\.?\\d+")) { // match
																			// a
																			// decimal
																			// number

						int outStandingAmount = (int) Math.round(Double.parseDouble(sOutstandingAmounts[i]));
						sOutstandingAmounts[i] = String.valueOf(outStandingAmount);
					}

					if (Integer.parseInt(sOutstandingAmounts[i]) < 0) {
						mIsNegativeValues = true;
					}

					sSendToServer_Outstanding = sSendToServer_Outstanding
							+ String.valueOf(SelectedGroupsTask.member_Id.elementAt(i)) + "~" + mLoanId + "~"
							+ sOutstandingAmounts[i] + ".00" + "#";

					confirmArr[i] = String.valueOf(SelectedGroupsTask.member_Name.elementAt(i)) + "           "
							+ sOutstandingAmounts[i];

					sOutstandingAmount_Total = sOutstandingAmount_Total + Integer.parseInt(sOutstandingAmounts[i]);

					builder.append(sOutstandingAmounts[i]).append(",");
				}

				Log.d(TAG, sSendToServer_Outstanding);

				Log.d(TAG, "TOTAL " + String.valueOf(sOutstandingAmount_Total));

				// Do the SP insertion

				if (!mIsNegativeValues) {
					confirmationDialog = new Dialog(this);

					LayoutInflater inflater = this.getLayoutInflater();
					View dialogView = inflater.inflate(R.layout.dialog_confirmation, null);
					dialogView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
							LayoutParams.WRAP_CONTENT));

					TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
					confirmationHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.confirmation));
					confirmationHeader.setTypeface(LoginActivity.sTypeface);

					TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

					for (int i = 0; i < SelectedGroupsTask.member_Id.size(); i++) {

						TableRow indv_SavingsRow = new TableRow(this);

						@SuppressWarnings("deprecation")
						TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
								LayoutParams.WRAP_CONTENT, 1f);
						contentParams.setMargins(10, 5, 10, 5);

						TextView memberName_Text = new TextView(this);
						memberName_Text.setText(GetSpanText.getSpanString(this,
								String.valueOf(SelectedGroupsTask.member_Name.elementAt(i))));
						memberName_Text.setTypeface(LoginActivity.sTypeface);
						memberName_Text.setTextColor(color.black);
						memberName_Text.setPadding(5, 5, 5, 5);
						memberName_Text.setLayoutParams(contentParams);
						indv_SavingsRow.addView(memberName_Text);

						TextView confirm_values = new TextView(this);
						confirm_values.setText(GetSpanText.getSpanString(this, String.valueOf(sOutstandingAmounts[i])));
						confirm_values.setTextColor(color.black);
						confirm_values.setPadding(5, 5, 5, 5);
						confirm_values.setGravity(Gravity.RIGHT);
						confirm_values.setLayoutParams(contentParams);
						indv_SavingsRow.addView(confirm_values);

						confirmationTable.addView(indv_SavingsRow,
								new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

					}

					View rullerView = new View(this);
					rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
					rullerView.setBackgroundColor(Color.rgb(0, 199, 140));// rgb(255,
																			// 229,
																			// 242));
					confirmationTable.addView(rullerView);

					TableRow totalRow = new TableRow(this);

					@SuppressWarnings("deprecation")
					TableRow.LayoutParams totalParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
							LayoutParams.WRAP_CONTENT, 1f);
					totalParams.setMargins(10, 5, 10, 5);

					TextView totalText = new TextView(this);
					totalText.setText(GetSpanText.getSpanString(this, String.valueOf(AppStrings.total)));
					totalText.setTypeface(LoginActivity.sTypeface);
					totalText.setTextColor(color.black);
					totalText.setPadding(5, 5, 5, 5);// (5, 10, 5, 10);
					totalText.setLayoutParams(totalParams);
					totalRow.addView(totalText);

					TextView totalAmount = new TextView(this);
					totalAmount.setText(GetSpanText.getSpanString(this, String.valueOf(sOutstandingAmount_Total)));// SavingsFragment.sSavings_Total
					totalAmount.setTextColor(color.black);
					totalAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
					totalAmount.setGravity(Gravity.RIGHT);
					totalAmount.setLayoutParams(totalParams);
					totalRow.addView(totalAmount);

					confirmationTable.addView(totalRow,
							new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

					mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit_button);
					mEdit_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.edit));
					mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
					mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
																			// 205,
																			// 0));
					mEdit_RaisedButton.setOnClickListener(this);

					mOk_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Ok_button);
					mOk_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.mVerified));
					mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
					mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
					mOk_RaisedButton.setOnClickListener(this);

					confirmationDialog.getWindow()
							.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
					confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					confirmationDialog.setCanceledOnTouchOutside(false);
					confirmationDialog.setContentView(dialogView);
					confirmationDialog.setCancelable(true);
					confirmationDialog.show();

					MarginLayoutParams margin = (MarginLayoutParams) dialogView.getLayoutParams();
					margin.leftMargin = 10;
					margin.rightMargin = 10;
					margin.topMargin = 10;
					margin.bottomMargin = 10;
					margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);
				} else {

					TastyToast.makeText(getApplicationContext(), AppStrings.mIsNegativeOpeningBalance,
							TastyToast.LENGTH_SHORT, TastyToast.WARNING);

					sSendToServer_Outstanding = Reset.reset(sSendToServer_Outstanding);
					sOutstandingAmount_Total = Integer.valueOf(nullVlaue);

					mIsNegativeValues = false;

				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

			break;

		case R.id.fragment_Edit_button:

			mSubmit_RaisedButton.setClickable(true);

			sSendToServer_Outstanding = Reset.reset(sSendToServer_Outstanding);
			sOutstandingAmount_Total = Integer.valueOf(nullVlaue);

			mAmount_Values = Reset.reset(mAmount_Values);

			confirmationDialog.dismiss();
			break;

		case R.id.fragment_Ok_button:

			confirmationDialog.dismiss();
			mIsNegativeValues = false;

			if (!Get_Edit_OpeningbalanceWebservice.mEdit_OpeningBalance_Values_Check[3].equals("")) {
				if (publicValues.mBankLoanSize_BankLoan == publicValues.mCurrentBankLoanSize_BankLoan) {
					publicValues.mCurrentBankLoanSize_BankLoan = 0;
					publicValues.mBankLoanSize_BankLoan = 0;
					if (publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan == null) {
						publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan = "";
					}
					publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan = publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan
							+ sSendToServer_Outstanding + "%";
					Log.e("Current Bank Member Loan Values---->>>111",
							publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan);
					Intent intent = new Intent(EditOpeningBalanceBankMemberActivity.this,
							EditOpeningBalanceGroupLoanActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
					overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);
					finish();
				} else if (publicValues.mBankLoanSize_BankLoan == 1) {
					publicValues.mCurrentBankLoanSize_BankLoan = 0;
					publicValues.mBankLoanSize_BankLoan = 0;
					if (publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan == null) {
						publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan = "";
					}
					publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan = publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan
							+ sSendToServer_Outstanding + "%";
					Log.e("Current Bank Member Loan Values---->>>222",
							publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan);
					Intent intent = new Intent(EditOpeningBalanceBankMemberActivity.this,
							EditOpeningBalanceGroupLoanActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
					overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);
					finish();
				} else {
					if (publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan == null) {
						publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan = "";
					}

					publicValues.mCurrentBankLoanSize_BankLoan = publicValues.mCurrentBankLoanSize_BankLoan + 1;
					if (publicValues.mBankLoanSize_BankLoan == publicValues.mCurrentBankLoanSize_BankLoan) {

						publicValues.mCurrentBankLoanSize_BankLoan = 0;
						publicValues.mBankLoanSize_BankLoan = 0;
						if (publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan == null) {
							publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan = "";
						}
						publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan = publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan
								+ sSendToServer_Outstanding + "%";
						Log.e("CurrentMember Loan Values---->>>",
								publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan);
						Intent intent = new Intent(EditOpeningBalanceBankMemberActivity.this,
								EditOpeningBalanceGroupLoanActivity.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
						overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);
						finish();

					} else {
						if (publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan == null) {
							publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan = "";
						}
						publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan = publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan
								+ sSendToServer_Outstanding + "%";

						Intent intent = new Intent(EditOpeningBalanceBankMemberActivity.this,
								EditOpeningBalanceBankMemberActivity.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
						overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

						finish();
					}
				}
			} else if (!Get_Edit_OpeningbalanceWebservice.mEdit_OpeningBalance_Values_Check[4].equals("")) {

				if (publicValues.mBankLoanSize_BankLoan == publicValues.mCurrentBankLoanSize_BankLoan) {
					publicValues.mCurrentBankLoanSize_BankLoan = 0;
					publicValues.mBankLoanSize_BankLoan = 0;
					if (publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan == null) {
						publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan = "";
					}
					publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan = publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan
							+ sSendToServer_Outstanding + "%";
					Log.e("Current Bank Member Loan Values---->>>111",
							publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan);
					Intent intent = new Intent(EditOpeningBalanceBankMemberActivity.this,
							EditOpeningBalanceBankDetailsActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
					overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);
					finish();
				} else if (publicValues.mBankLoanSize_BankLoan == 1) {
					publicValues.mCurrentBankLoanSize_BankLoan = 0;
					publicValues.mBankLoanSize_BankLoan = 0;
					if (publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan == null) {
						publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan = "";
					}
					publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan = publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan
							+ sSendToServer_Outstanding + "%";
					Log.e("Current Bank Member Loan Values---->>>222",
							publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan);
					Intent intent = new Intent(EditOpeningBalanceBankMemberActivity.this,
							EditOpeningBalanceBankDetailsActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
					overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);
					finish();
				} else {
					if (publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan == null) {
						publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan = "";
					}

					publicValues.mCurrentBankLoanSize_BankLoan = publicValues.mCurrentBankLoanSize_BankLoan + 1;
					if (publicValues.mBankLoanSize_BankLoan == publicValues.mCurrentBankLoanSize_BankLoan) {

						publicValues.mCurrentBankLoanSize_BankLoan = 0;
						publicValues.mBankLoanSize_BankLoan = 0;
						if (publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan == null) {
							publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan = "";
						}
						publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan = publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan
								+ sSendToServer_Outstanding + "%";
						Log.e("Current Bank Member Loan Values---->>>3333",
								publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan);
						Intent intent = new Intent(EditOpeningBalanceBankMemberActivity.this,
								EditOpeningBalanceBankDetailsActivity.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
						overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);
						finish();

					} else {
						if (publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan == null) {
							publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan = "";
						}
						publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan = publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan
								+ sSendToServer_Outstanding + "%";

						Intent intent = new Intent(EditOpeningBalanceBankMemberActivity.this,
								EditOpeningBalanceBankMemberActivity.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
						overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

						finish();
					}
				}

			}
			break;

		}

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.

		if (Boolean.valueOf(EShaktiApplication.isAgent)) {
			getMenuInflater().inflate(R.menu.menu_edit_ob, menu);
			MenuItem item = menu.getItem(0);
			item.setVisible(true);
			MenuItem logOutItem = menu.getItem(1);
			logOutItem.setVisible(true);

			mLanguageLocalae = PrefUtils.getUserlangcode();

			LoginActivity.sTypeface = GetTypeface.getTypeface(getApplicationContext(), mLanguageLocalae);
			SpannableStringBuilder SS = new SpannableStringBuilder(
					RegionalConversion.getRegionalConversion(AppStrings.groupList));

			SS.setSpan(new CustomTypefaceSpan("", LoginActivity.sTypeface), 0,
					RegionalConversion.getRegionalConversion(AppStrings.groupList).length(),
					Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

			SS.setSpan(new TextAppearanceSpan(getApplicationContext(), R.style.text_menu), 0,
					RegionalConversion.getRegionalConversion(AppStrings.groupList).length(),
					Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

			SpannableStringBuilder logOutBuilder = new SpannableStringBuilder(
					RegionalConversion.getRegionalConversion(AppStrings.logOut));

			logOutBuilder.setSpan(new CustomTypefaceSpan("", LoginActivity.sTypeface), 0,
					RegionalConversion.getRegionalConversion(AppStrings.logOut).length(),
					Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

			logOutBuilder.setSpan(new TextAppearanceSpan(getApplicationContext(), R.style.text_menu), 0,
					RegionalConversion.getRegionalConversion(AppStrings.logOut).length(),
					Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

			if (item.getItemId() == R.id.action_grouplist_edit) {

				item.setTitle(SS);

			}
			if (logOutItem.getItemId() == R.id.menu_logout_edit) {

				logOutItem.setTitle(logOutBuilder);
			}

		} else {

			getMenuInflater().inflate(R.menu.menu_edit_ob_group, menu);

			MenuItem item = menu.getItem(0);
			item.setVisible(true);

			mLanguageLocalae = PrefUtils.getUserlangcode();
			LoginActivity.sTypeface = GetTypeface.getTypeface(getApplicationContext(), mLanguageLocalae);

			SpannableStringBuilder logOutBuilder = new SpannableStringBuilder(
					RegionalConversion.getRegionalConversion(AppStrings.logOut));

			logOutBuilder.setSpan(new CustomTypefaceSpan("", LoginActivity.sTypeface), 0,
					RegionalConversion.getRegionalConversion(AppStrings.logOut).length(),
					Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

			logOutBuilder.setSpan(new TextAppearanceSpan(getApplicationContext(), R.style.text_menu), 0,
					RegionalConversion.getRegionalConversion(AppStrings.logOut).length(),
					Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

			if (item.getItemId() == R.id.group_logout) {
				item.setTitle(logOutBuilder);
			}
		}

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (Boolean.valueOf(EShaktiApplication.isAgent)) {

			// noinspection SimplifiableIfStatement
			if (id == R.id.action_grouplist_edit) {

				try {

					SelectedGroupsTask.loan_Name.clear();
					SelectedGroupsTask.loan_Id.clear();
					SelectedGroupsTask.loan_EngName.clear();
					SelectedGroupsTask.member_Id.clear();
					SelectedGroupsTask.member_Name.clear();
					SelectedGroupsTask.sBankNames.clear();
					SelectedGroupsTask.sEngBankNames.clear();
					SelectedGroupsTask.sBankAmt.clear();
					SelectedGroupsTask.sCashatBank = " ";
					SelectedGroupsTask.sCashinHand = " ";
					EShaktiApplication.setSelectedgrouptask(false);
					EShaktiApplication.setPLOS(false);
					EShaktiApplication.setBankDeposit(false);
					EShaktiApplication.setOfflineTrans(false);
					EShaktiApplication.setOfflineTransDate(false);
					EShaktiApplication.setSubmenuclicked(false);
					EShaktiApplication.setLastTransId("");
					EShaktiApplication.setSetTransValues("");

					Constants.BUTTON_CLICK_FLAG = "0";
					EShaktiApplication.setSubmenuclicked(false);
					Constants.FRAG_INSTANCE_CONSTANT = "0";
					EShaktiApplication.setOnSavedFragment(false);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}

				startActivity(new Intent(EditOpeningBalanceBankMemberActivity.this, GroupListActivity.class));
				overridePendingTransition(R.anim.right_to_left_in, R.anim.right_to_left_out);
				finish();
				return true;

			} else if (id == R.id.menu_logout_edit) {
				Log.e(" Logout", "Logout Sucessfully");

				startActivity(new Intent(GetExit.getExitIntent(getApplicationContext())));
				this.finish();
				return true;

			}
		} else {
			if (id == R.id.group_logout_edit) {
				Log.e("Group Logout", "Logout Sucessfully");
				startActivity(new Intent(GetExit.getExitIntent(getApplicationContext())));
				this.finish();
				return true;

			}
		}
		return super.onOptionsItemSelected(item);
	}

}