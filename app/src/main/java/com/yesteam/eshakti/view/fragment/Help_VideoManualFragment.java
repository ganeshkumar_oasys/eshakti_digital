package com.yesteam.eshakti.view.fragment;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.OnInitializedListener;
import com.google.android.youtube.player.YouTubePlayer.Provider;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.oasys.eshakti.digitization.R;
import com.yesteam.eshakti.appConstants.Config;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class Help_VideoManualFragment extends Fragment {
	View rootView;

	private YouTubePlayer YPlayer;

	public Help_VideoManualFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		rootView = inflater.inflate(R.layout.fragment_help_video_manual, container, false);

		YouTubePlayerSupportFragment youTubePlayerFragment = YouTubePlayerSupportFragment.newInstance();
		FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
		transaction.add(R.id.youtube_fragment, youTubePlayerFragment).commit();

		youTubePlayerFragment.initialize(Config.DEVELOPER_KEY, new OnInitializedListener() {

			@Override
			public void onInitializationSuccess(Provider arg0, YouTubePlayer youTubePlayer, boolean b) {
				if (!b) {
					YPlayer = youTubePlayer;
					YPlayer.setFullscreen(false);
					YPlayer.loadVideo(Config.YOUTUBE_VIDEO_CODE);
					YPlayer.play();
				}
			}

			@Override
			public void onInitializationFailure(Provider arg0, YouTubeInitializationResult arg1) {
				// TODO Auto-generated method stub

			}
		});

		return rootView;
	}

}
