package com.yesteam.eshakti.view.activity;

import java.util.ArrayList;
import java.util.List;

import com.andexert.expandablelayout.library.ExpandableLayoutListView;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.R;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.adapter.GroupListAdapter;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.model.GroupListItem;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.GetExit;
import com.yesteam.eshakti.utils.GetTypeface;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.views.RaisedButton;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

public class Audit_Grouplist_Activity extends AppCompatActivity
		implements TaskListener, OnClickListener, OnItemClickListener {

	Toolbar mToolbar;
	Context context;

	private Dialog mProgressDialog;
	EditText mSearch_Groupid_editext;
	ImageView mSearch_groupid_imageview;
	Typeface sTypeface;
	String mLanguageLocalae = "";
	ExpandableLayoutListView expandableLayoutListView;
	private List<GroupListItem> listItems;
	private GroupListAdapter mAdapter;
	int listImage, leftImage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		super.onCreate(savedInstanceState);

		setContentView(R.layout.audit_activity_grouplist);

		try {

			if (PrefUtils.getUserlangcode() != null) {
				mLanguageLocalae = PrefUtils.getUserlangcode();
			} else {
				mLanguageLocalae = null;
			}

			sTypeface = GetTypeface.getTypeface(getApplicationContext(), mLanguageLocalae);

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		listImage = R.drawable.ic_navigate_next_white_24dp;
		leftImage = R.drawable.star;

		try {

			mToolbar = (Toolbar) findViewById(R.id.audit_toolbar_grouplist);
			TextView mTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);
			setSupportActionBar(mToolbar);
			getSupportActionBar().setDisplayShowHomeEnabled(true);

			getSupportActionBar().setTitle("");
			// mTitle.setText(Login_webserviceTask.mAnimatorName + " " +
			// LoginTask.UserName);
			mTitle.setText(PrefUtils.getAnimatorName() + "    " + PrefUtils.getAgentUserName());
			mTitle.setTypeface(sTypeface);
			mTitle.setGravity(Gravity.CENTER);

			mSearch_Groupid_editext = (EditText) findViewById(R.id.activity_group_id_search_edittext);
			mSearch_groupid_imageview = (ImageView) findViewById(R.id.activity_audit_search_button);
			mSearch_groupid_imageview.setOnClickListener(this);

			mSearch_Groupid_editext.setTypeface(sTypeface);
			mSearch_Groupid_editext.setHint("ENTER GROUP ID");

			expandableLayoutListView = (ExpandableLayoutListView) findViewById(R.id.audit_listview);

			expandableLayoutListView.setOnItemClickListener(this);
			
			

			context = this.getApplicationContext();

			listItems = new ArrayList<GroupListItem>();
			
			LoginActivity.sTypeface=sTypeface;
		 

			listItems = new ArrayList<GroupListItem>();
			for (int i = 0; i < 1; i++) {

				GroupListItem rowItem = new GroupListItem(leftImage, "GURU KRIPA AAVASS / ALAM BAI",
						listImage);

				listItems.add(rowItem);
			}
			
			mAdapter = new GroupListAdapter(Audit_Grouplist_Activity.this, listItems);
			expandableLayoutListView.setAdapter(mAdapter);

			

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_edit_ob_group, menu);
		MenuItem item = menu.getItem(0);
		item.setVisible(true);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.

		int id = item.getItemId();
		if (id == R.id.group_logout_edit) {

			startActivity(new Intent(GetExit.getExitIntent(getApplicationContext())));
			this.finish();

			return true;

		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onTaskStarted() {

		mProgressDialog = AppDialogUtils.createProgressDialog(this);
		mProgressDialog.show();
	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub

		if (mProgressDialog != null) {

			/**
			 * Inorder to Start Service for collecting the GroupDetails at background
			 **/
			System.out.println("Task Finished Values:::" + result);
			if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {

				mProgressDialog.dismiss();
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						if (!result.equals("FAIL")) {

							TastyToast.makeText(getApplicationContext(), AppStrings.mCommonNetworkErrorMsg,
									TastyToast.LENGTH_SHORT, TastyToast.ERROR);
							Constants.NETWORKCOMMONFLAG = "SUCCESS";
						}
					}
				});

			} else {
			}
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
		// TODO Auto-generated method stub

		RaisedButton button = (RaisedButton) view.findViewById(R.id.activity_next_button);
		TextView mShgName = (TextView) view.findViewById(R.id.shgcode);
		TextView mShgName_Values = (TextView) view.findViewById(R.id.shgcode_values);
		TextView mBlockname = (TextView) view.findViewById(R.id.blockname);
		TextView mBlockNameValues = (TextView) view.findViewById(R.id.blockname_values);
		TextView mPanchayatName = (TextView) view.findViewById(R.id.panchayat);
		TextView mPanchayatNameValues = (TextView) view.findViewById(R.id.panchayat_values);
		TextView mVillagename = (TextView) view.findViewById(R.id.villagename);
		TextView mVillagenameValues = (TextView) view.findViewById(R.id.villagename_vlaues);
		TextView mTransactionDate = (TextView) view.findViewById(R.id.transDate);
		TextView mTransactionDateValues = (TextView) view.findViewById(R.id.transDate_vlaues);

		mShgName.setTypeface(sTypeface);
		mShgName_Values.setTypeface(sTypeface);
		mBlockname.setTypeface(sTypeface);
		mBlockNameValues.setTypeface(sTypeface);

		mPanchayatName.setTypeface(sTypeface);
		mPanchayatNameValues.setTypeface(sTypeface);
		mVillagename.setTypeface(sTypeface);
		mVillagenameValues.setTypeface(sTypeface);

		mTransactionDate.setTypeface(sTypeface);
		mTransactionDateValues.setTypeface(sTypeface);

		mShgName.setTextColor(Color.BLACK);
		mShgName_Values.setTextColor(Color.BLACK);
		mBlockname.setTextColor(Color.BLACK);
		mBlockNameValues.setTextColor(Color.BLACK);

		mPanchayatName.setTextColor(Color.BLACK);
		mPanchayatNameValues.setTextColor(Color.BLACK);
		mVillagename.setTextColor(Color.BLACK);
		mVillagenameValues.setTextColor(Color.BLACK);

		mTransactionDate.setTextColor(Color.BLACK);
		mTransactionDateValues.setTextColor(Color.BLACK);

		button.setTypeface(sTypeface);
		button.setText(RegionalConversion.getRegionalConversion(AppStrings.mSelect));

		/*if (Login_webserviceTask.sSHG_Codes.elementAt(position) != null) {

			mShgName.setText(Login_webserviceTask.mTitleValues[0] + "  :  ");
			mShgName_Values.setText(Login_webserviceTask.sSHG_Codes.elementAt(position).toUpperCase());
			mBlockname.setText(Login_webserviceTask.mTitleValues[1] + "  :  ");
			mBlockNameValues.setText(Login_webserviceTask.sSHG_BlockNames.elementAt(position).toUpperCase());

			mPanchayatName.setText(Login_webserviceTask.mTitleValues[2] + "  :  ");
			mPanchayatNameValues.setText(Login_webserviceTask.sSHG_PanchayatNames.elementAt(position).toUpperCase());

			mVillagename.setText(Login_webserviceTask.mTitleValues[3] + "  :  ");
			mVillagenameValues.setText(Login_webserviceTask.sSHG_VillageNames.elementAt(position).toUpperCase());

			mTransactionDate.setText(Login_webserviceTask.mTitleValues[4] + "  :  ");
			mTransactionDateValues.setText(Login_webserviceTask.sSHG_TransactionDate.elementAt(position).toUpperCase());
			EShaktiApplication.setLastTransactionDate(mTransactionDateValues.getText().toString());

			Log.e("Selected Group Last Transaction date : ********** ", EShaktiApplication.getLastTransactionDate());

		}*/
		
		mShgName.setText("SHG CODE" + "  :  ");
		mShgName_Values.setText("12732005191018".toUpperCase());
		mBlockname.setText("BLOCK NAME"+ "  :  ");
		mBlockNameValues.setText("DHARMAPURI".toUpperCase());

		mPanchayatName.setText("PANCHAYAT NAME" + "  :  ");
		mPanchayatNameValues.setText("A. GOLLAHALLI".toUpperCase());

		mVillagename.setText("VILLAGE NAME" + "  :  ");
		mVillagenameValues.setText("AGOLLAHALLI".toUpperCase());

		mTransactionDate.setText("LAST TRANSACTION DATE" + "  :  ");
		mTransactionDateValues.setText("14/09/2017".toUpperCase());
	

		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				try { 
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {
		case R.id.activity_audit_search_button:
			
			Intent intent = new Intent(Audit_Grouplist_Activity.this, Audit_Date_selectionActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);
			finish();


			break;

		default:
			break;
		}

	}

}
