package com.yesteam.eshakti.view.fragment;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.Get_GroupLoanSummaryTask;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;


public class Reports_GroupLoanSummaryFragment extends Fragment {

	private TextView mGroupName, mCashInHand, mCashAtBank, mHeader;
	private String mLanguagelogale="";

	public Reports_GroupLoanSummaryFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_GROUP_REPORTS_LOAN_SUMM;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_grouploansummary,
				container, false);

		MainFragment_Dashboard.isBackpressed = false;

		try {
			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String
					.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashInHand.setText(RegionalConversion.getRegionalConversion(String
					.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashInHand.setTypeface(LoginActivity.sTypeface);

			mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashAtBank.setText(RegionalConversion.getRegionalConversion(String
					.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashAtBank.setTypeface(LoginActivity.sTypeface);

			mHeader = (TextView) rootView
					.findViewById(R.id.fragment_grouploansummary_headertext);
			mHeader.setText(RegionalConversion
					.getRegionalConversion(Reports_GroupLoanListReportFragment.loanName.toString()+" "+AppStrings.summary));
			mHeader.setTypeface(LoginActivity.sTypeface);

			mLanguagelogale = PrefUtils.getUserlangcode();
			
			String responseArr[] = Get_GroupLoanSummaryTask.sGroup_Loan_summary_response
					.split("~");

			System.out.println("Length of response :" + responseArr.length);

			TableLayout table1 = (TableLayout) rootView
					.findViewById(R.id.fragment_OpeningBalanceTable);
			TableRow groupLoan_row = new TableRow(getActivity());

			groupLoan_row.setGravity(Gravity.CENTER_HORIZONTAL);

			TextView head_groupLoan = new TextView(getActivity());
			head_groupLoan.setText(GetSpanText.getSpanString(getActivity(),
					String.valueOf(AppStrings.groupLoan)));
			head_groupLoan.setTextColor(color.black);
			head_groupLoan.setPadding(10, 5, 10, 5);
			head_groupLoan.setTypeface(LoginActivity.sTypeface);

			groupLoan_row.addView(head_groupLoan);

			TextView head_amount = new TextView(getActivity());
			head_amount.setText(GetSpanText.getSpanString(getActivity(),
					String.valueOf(responseArr[0])));
			head_amount.setTextColor(color.black);
			head_amount.setPadding(5, 5, 5, 5);
			groupLoan_row.addView(head_amount);

			table1.addView(groupLoan_row, new TableLayout.LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

			TableLayout table2 = (TableLayout) rootView
					.findViewById(R.id.fragment_ContentTable);

			TableRow balance_row = new TableRow(getActivity());

			balance_row.setGravity(Gravity.CENTER_HORIZONTAL);

			TextView head_balance = new TextView(getActivity());
			head_balance.setText(GetSpanText.getSpanString(getActivity(),
					String.valueOf(AppStrings.outstanding)));//(AppStrings.balance)));
			head_balance.setTypeface(LoginActivity.sTypeface);
			head_balance.setTextColor(color.black);
			head_balance.setPadding(10, 5, 10, 5);
			balance_row.addView(head_balance);

			TextView balance_amount = new TextView(getActivity());
			balance_amount.setText(GetSpanText.getSpanString(getActivity(),
					String.valueOf(responseArr[1])));
			balance_amount.setPadding(5, 5, 5, 5);
			balance_amount.setTextColor(color.black);
			balance_row.addView(balance_amount);

			table2.addView(balance_row, new TableLayout.LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

			TableLayout headertable = (TableLayout)rootView.findViewById(R.id.groupLoanSummary_headerTable);
			TableLayout table3 = (TableLayout) rootView
					.findViewById(R.id.fragment_OutstandingTable);

			TableRow head_row = new TableRow(getActivity());
			TableRow.LayoutParams headerParams = new TableRow.LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1f);

			TextView head_date = new TextView(getActivity());
			head_date.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.date)));
			head_date.setTypeface(LoginActivity.sTypeface);
			head_date.setTextColor(Color.WHITE);
			head_date.setPadding(50, 5, 10, 5);
			head_date.setBackgroundResource(color.tableHeader);
			head_date.setLayoutParams(headerParams);
			head_row.addView(head_date);

			TextView principleAmount = new TextView(getActivity());
			principleAmount.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.principleAmount)));
			principleAmount.setTypeface(LoginActivity.sTypeface);
			principleAmount.setTextColor(Color.WHITE);
			if (mLanguagelogale.equalsIgnoreCase("English")) {
				principleAmount.setPadding(25, 5, 10, 5);
			}else if (mLanguagelogale.equalsIgnoreCase("Tamil")) {
				principleAmount.setPadding(25, 5, 15, 5);
			}else if (mLanguagelogale.equalsIgnoreCase("Hindi")) {
				principleAmount.setPadding(25, 5, 15, 5);
			}else if (mLanguagelogale.equalsIgnoreCase("Marathi")) {
				principleAmount.setPadding(25, 5, 15, 5);
			}else{
				principleAmount.setPadding(25, 5, 15, 5);
			}
			principleAmount.setGravity(Gravity.RIGHT);
			principleAmount.setBackgroundResource(color.tableHeader);
			principleAmount.setLayoutParams(headerParams);
			head_row.addView(principleAmount);

			TextView head_interest = new TextView(getActivity());
			head_interest.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.interest)));
			head_interest.setTypeface(LoginActivity.sTypeface);
			head_interest.setTextColor(Color.WHITE);
			head_interest.setPadding(10, 5, 20, 5);
			head_interest.setGravity(Gravity.RIGHT);
			head_interest.setBackgroundResource(color.tableHeader);
			head_interest.setLayoutParams(headerParams);
			head_row.addView(head_interest);

			headertable.addView(head_row, new TableLayout.LayoutParams(
					TableLayout.LayoutParams.MATCH_PARENT,
					TableLayout.LayoutParams.WRAP_CONTENT));
			try {
				for (int i = 2; i < responseArr.length; i = i + 3) {
					TableRow contentRow = new TableRow(getActivity());
					TableRow.LayoutParams contentParams = new TableRow.LayoutParams(
							LayoutParams.MATCH_PARENT,
							LayoutParams.WRAP_CONTENT, 1f);

					TextView date = new TextView(getActivity());
					date.setText(GetSpanText.getSpanString(getActivity(),
							String.valueOf(responseArr[i])));
					date.setPadding(20, 5, 10, 5);
					date.setTextColor(color.black);
					date.setLayoutParams(contentParams);
					contentRow.addView(date);

					TextView amount = new TextView(getActivity());
					amount.setText(GetSpanText.getSpanString(getActivity(),
							String.valueOf(responseArr[i + 1])));
					amount.setPadding(10, 5, 20, 5);
					amount.setTextColor(color.black);
					amount.setGravity(Gravity.RIGHT);
					System.out.println("responseArr1:"
							+ responseArr[i + 1].toString());
					amount.setLayoutParams(contentParams);
					contentRow.addView(amount);

					TextView interest = new TextView(getActivity());
					interest.setText(GetSpanText.getSpanString(getActivity(),
							String.valueOf(responseArr[i + 2])));
					interest.setPadding(10, 5, 20, 5);
					interest.setTextColor(color.black);
					interest.setGravity(Gravity.RIGHT);
					interest.setLayoutParams(contentParams);
					System.out.println("responseArr2:"
							+ responseArr[i + 2].toString());
					contentRow.addView(interest);

					table3.addView(contentRow, new TableLayout.LayoutParams(
							LayoutParams.MATCH_PARENT,
							LayoutParams.WRAP_CONTENT));

					View rullerView = new View(getActivity());
					rullerView.setLayoutParams(new TableRow.LayoutParams(
							TableRow.LayoutParams.MATCH_PARENT, 2));
					rullerView.setBackgroundColor(Color.rgb(220, 220, 220));
					table3.addView(rullerView);

				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return rootView;
	}

}
