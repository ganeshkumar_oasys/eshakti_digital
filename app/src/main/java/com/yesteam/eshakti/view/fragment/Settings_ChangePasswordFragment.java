package com.yesteam.eshakti.view.fragment;

import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.Get_AgentChangePasswordTask;
import com.yesteam.eshakti.webservices.Get_ChangePasswordTask;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Settings_ChangePasswordFragment extends Fragment implements OnClickListener, TaskListener {

	private TextView mGroupName, mHeadertext;
	private Button mRaised_Submit_Button;
	private Dialog mprogressDialog;
	private TextView oldPwd, newPwd, confirmPwd;

	public static EditText oldPwdEditText, newPwdEditText, confirmPwdEditText;
	public static String pwd, OldPwd = null, NewPwd = null, ConfirmPwd = null;

	public Settings_ChangePasswordFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_SETTING_CHANGE_PASSWORD;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onAttach(Context context) {
		// TODO Auto-generated method stub
		super.onAttach(context);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_change_password, container, false);

		MainFragment_Dashboard.isBackpressed = false;

		try {

			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mHeadertext = (TextView) rootView.findViewById(R.id.fragment_changepassword_headertext);
			mHeadertext.setText(RegionalConversion.getRegionalConversion(AppStrings.passwordchange));
			mHeadertext.setTypeface(LoginActivity.sTypeface);
		//	mHeadertext.setTextColor(color.black);

			oldPwd = (TextView) rootView.findViewById(R.id.oldPwdtextView);
			oldPwd.setText(RegionalConversion.getRegionalConversion(AppStrings.OldPassword));
			oldPwd.setTypeface(LoginActivity.sTypeface);
			oldPwd.setTextColor(color.black);

			newPwd = (TextView) rootView.findViewById(R.id.NewPwdtextView);
			newPwd.setText(RegionalConversion.getRegionalConversion(AppStrings.NewPassword));
			newPwd.setTypeface(LoginActivity.sTypeface);
			newPwd.setTextColor(color.black);

			confirmPwd = (TextView) rootView.findViewById(R.id.ConfirmPwdtextView);
			confirmPwd.setText(RegionalConversion.getRegionalConversion(AppStrings.ConfirmPassword));
			confirmPwd.setTypeface(LoginActivity.sTypeface);
			confirmPwd.setTextColor(color.black);

			oldPwdEditText = (EditText) rootView.findViewById(R.id.OldPwdeditText);
			oldPwdEditText.setBackgroundResource(R.drawable.edittext_background);
			newPwdEditText = (EditText) rootView.findViewById(R.id.NewPwdeditText);
			newPwdEditText.setBackgroundResource(R.drawable.edittext_background);
			confirmPwdEditText = (EditText) rootView.findViewById(R.id.ConfirmPwdeditText);
			confirmPwdEditText.setBackgroundResource(R.drawable.edittext_background);

			mRaised_Submit_Button = (Button) rootView.findViewById(R.id.fragment_changepassword_Submitbutton);
			mRaised_Submit_Button.setText(RegionalConversion.getRegionalConversion(AppStrings.submit));
			mRaised_Submit_Button.setTypeface(LoginActivity.sTypeface);
			mRaised_Submit_Button.setOnClickListener(this);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return rootView;
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		try {
			mRaised_Submit_Button.setClickable(false);
			pwd = String.valueOf(LoginActivity.sPwd);
			OldPwd = oldPwdEditText.getText().toString();
			NewPwd = newPwdEditText.getText().toString();
			ConfirmPwd = confirmPwdEditText.getText().toString();

			System.out.println(
					"pwd:" + pwd + "" + "OldPwd" + OldPwd + "" + "NewPwd" + NewPwd + "" + "ConfirmPwd" + ConfirmPwd);
			if (OldPwd != null && NewPwd != null && ConfirmPwd != null && !OldPwd.isEmpty() && !NewPwd.isEmpty()
					&& !ConfirmPwd.isEmpty()) {

				if (!OldPwd.equals(NewPwd)) {
				if (NewPwd.equals(ConfirmPwd) && pwd.equals(OldPwd)) {

					if (ConnectionUtils.isNetworkAvailable(getActivity())) {
						// If Internet Connection is present

						if (Boolean.valueOf(EShaktiApplication.isAgent)) {

							try {
								new Get_AgentChangePasswordTask(this).execute();
							} catch (Exception e) { // TODO: handle exception
								System.out.println("Agent pwd:" + e.toString());
							}

						} else if (!Boolean.valueOf(EShaktiApplication.isAgent)) {
							try {
								new Get_ChangePasswordTask(this).execute();
							} catch (Exception e) { // TODO: handle exception
								System.out.println("Group pwd:" + e.toString());
							}
						}

					} else {
						// Show the dialog for No updates on OFFLINE
						if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
							
							TastyToast.makeText(getActivity(), AppStrings.offline_ChangePwdAlert, TastyToast.LENGTH_SHORT,
									TastyToast.WARNING);
						} else {
							
							TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
									TastyToast.ERROR);
						}
					}
				} else {
					
					TastyToast.makeText(getActivity(), AppStrings.pwdIncorrectAlert, TastyToast.LENGTH_SHORT,
							TastyToast.WARNING);
				}
				} else {
					TastyToast.makeText(getActivity(), AppStrings.mIsPasswordSame, TastyToast.LENGTH_SHORT, TastyToast.WARNING);
				}
			} else {
				
				TastyToast.makeText(getActivity(), AppStrings.loginUserAlert, TastyToast.LENGTH_SHORT,
						TastyToast.WARNING);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub
		mprogressDialog = AppDialogUtils.createProgressDialog(getActivity());
		mprogressDialog.show();
	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub
		try {
			if (mprogressDialog != null) {
				mprogressDialog.dismiss();

				if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {
					getActivity().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub

							if (!result.equals("FAIL")) {

								TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg, TastyToast.LENGTH_SHORT,
										TastyToast.ERROR);
								Constants.NETWORKCOMMONFLAG = "SUCCESS";
							}

						}
					});
				} else {
					TastyToast.makeText(getActivity(), AppStrings.mPasswordUpdate, TastyToast.LENGTH_SHORT,
							TastyToast.SUCCESS);
					
					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					getActivity().getSupportFragmentManager().beginTransaction()

							.replace(R.id.frame, fragment)
							.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
							.addToBackStack(fragment.getClass().getName()).commit();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
