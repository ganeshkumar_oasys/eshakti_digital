package com.yesteam.eshakti.view.fragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.squareup.otto.Subscribe;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.TransactionOffConstants;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;
import com.yesteam.eshakti.sqlite.database.GroupProvider;
import com.yesteam.eshakti.sqlite.database.TransactionProvider;
import com.yesteam.eshakti.sqlite.database.response.GetSingleTransResponse;
import com.yesteam.eshakti.sqlite.database.response.GroupFixedOSResponse;
import com.yesteam.eshakti.sqlite.database.response.TransactionResponse;
import com.yesteam.eshakti.sqlite.database.response.TransactionUpdateResponse;
import com.yesteam.eshakti.sqlite.db.model.GetTransactionSingle;
import com.yesteam.eshakti.sqlite.db.model.GetTransactionSinglevalues;
import com.yesteam.eshakti.sqlite.db.model.GroupFOSUpdate;
import com.yesteam.eshakti.sqlite.db.model.GroupResponseUpdate;
import com.yesteam.eshakti.sqlite.db.model.GroupTempDBLastTransDate;
import com.yesteam.eshakti.sqlite.db.model.Transaction;
import com.yesteam.eshakti.sqlite.db.model.TransactionUpdate;
import com.yesteam.eshakti.sqlite.db.model.Transaction_UniqueIdSingle;
import com.yesteam.eshakti.sqlite.db.transactions.TransactionManager.DataType;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.utils.GetFixedDepositBankValues;
import com.yesteam.eshakti.utils.GetOfflineTransactionValues;
import com.yesteam.eshakti.utils.GetResponseInfo;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.utils.Get_EdiText_Filter;
import com.yesteam.eshakti.utils.Get_Offline_MobileDate;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.utils.Put_DB_GroupNameDetail_Response;
import com.yesteam.eshakti.utils.Put_DB_GroupResponse;
import com.yesteam.eshakti.utils.Reset;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.utils.UpdateCalendarMinMaxDateUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.Fixed_Deposit_BalanceTask;
import com.yesteam.eshakti.webservices.Get_FixedDepositEntryTask;
import com.yesteam.eshakti.webservices.Get_LastTransactionID;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ParseException;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class Transaction_FixedDepositEntryFragment extends Fragment implements OnClickListener, TaskListener {

	public static String TAG = Transaction_FixedDepositEntryFragment.class.getSimpleName();
	private TextView mGroupName, mCashInHand, mCashAtBank, mHeadertext;
	private Button mRaised_Submit_Button, mEdit_RaisedButton, mOk_RaisedButton;
	public static EditText mEditAmount;

	public static String sSend_To_Server_FD;
	public static String[] sFixedDepositItem;

	public static List<EditText> sEditTextFields = new ArrayList<EditText>();
	public static String[] sFixedDepositAmount;
	boolean alert;
	public static Boolean isFixedDepositEntry = false;
	public static int sFixedDepositTotal;

	String toBeEditArr[];
	int size;
	public static int bank_deposit, bank_withdrawl, bank_expenses, bank_interest;

	private Dialog mProgressDilaog;
	Dialog confirmationDialog;

	String mDeposit_Fixed_offline, mInterest_Fixed_offline, mWithdrawl_Fixed_offline, mBankExpenses_Fixed_offline;
	public static String mCashatBank_Fixed_individual;
	int validateFixedOSAmount;

	String SelectedbankName;
	String amount;
	public static String fd_Response[];
	String mLastTrDate = null, mLastTr_ID = null;
	String mOfflineBankname;
	boolean isGetTrid = false;
	String mSelectedCashatBank = null;
	public static String mCashatBank_individual = null;
	boolean isServiceCall = false;

	String mSqliteDBStoredValues_FDValues = null;

	public Transaction_FixedDepositEntryFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_FIXEDDEPOSIT_ENTRY;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		SBus.INST.register(this);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Transaction_FixedDepositEntryFragment.sEditTextFields.clear();
		sSend_To_Server_FD = Reset.reset(sSend_To_Server_FD);
		sFixedDepositTotal = 0;

		mSelectedCashatBank = Transaction_BankDepositFragment.sSelected_BankDepositAmount;

		sFixedDepositItem = new String[] { RegionalConversion.getRegionalConversion(AppStrings.bankDeposit),
				RegionalConversion.getRegionalConversion(AppStrings.bankInterest),
				RegionalConversion.getRegionalConversion(AppStrings.withdrawl),
				RegionalConversion.getRegionalConversion(AppStrings.bankExpenses) };

		fd_Response = Fixed_Deposit_BalanceTask.sFD_BalanceTask_ServiceResponse.split("%");

		System.out.println("Length of fd_Response[]----------" + fd_Response.length);
		for (int i = 0; i < fd_Response.length; i++) {
			System.out.println("fd_Response[]  :" + fd_Response[i] + " i pos : " + i);
			String mTempFixedValues[] = fd_Response[i].split("~");
			if (mTempFixedValues[2].equals(Transaction_BankDepositFragment.sSendToServer_BankName.toString())) {
				SelectedbankName = mTempFixedValues[2];
				amount = mTempFixedValues[1];

			}

		}

		System.out.println("-----------SELECTED BANK NAME -----------" + SelectedbankName);

		System.out.println("-----------Amount---------------" + amount);

	}

	@SuppressWarnings("deprecation")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_deposit_entry, container, false);

		MainFragment_Dashboard.isBackpressed = false;

		mSqliteDBStoredValues_FDValues = null;

		mGroupName = (TextView) rootView.findViewById(R.id.groupname);
		mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
		mGroupName.setTypeface(LoginActivity.sTypeface);

		mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
		mCashInHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
				+ SelectedGroupsTask.sCashinHand);
		mCashInHand.setTypeface(LoginActivity.sTypeface);

		mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
		mCashAtBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
				+ SelectedGroupsTask.sCashatBank);
		mCashAtBank.setTypeface(LoginActivity.sTypeface);

		mHeadertext = (TextView) rootView.findViewById(R.id.fragment_deposit_entry_headertext);
		mHeadertext.setText(RegionalConversion.getRegionalConversion(AppStrings.fixedDeposit));
		mHeadertext.setTypeface(LoginActivity.sTypeface);

		mRaised_Submit_Button = (Button) rootView.findViewById(R.id.fragment_deposit_entry_Submitbutton);
		mRaised_Submit_Button.setText(RegionalConversion.getRegionalConversion(AppStrings.submit));
		mRaised_Submit_Button.setTypeface(LoginActivity.sTypeface);
		mRaised_Submit_Button.setOnClickListener(this);

		size = sFixedDepositItem.length;
		System.out.println("Size of values:>>>" + size);

		try {

			TableLayout tableLayout = (TableLayout) rootView.findViewById(R.id.fragmentDeposit_contentTable);
			TableRow outerRow = new TableRow(getActivity());

			TableRow.LayoutParams header_ContentParams = new TableRow.LayoutParams(250, LayoutParams.WRAP_CONTENT, 1f);
			header_ContentParams.setMargins(10, 5, 10, 5);

			TextView bankName = new TextView(getActivity());
			bankName.setText(GetSpanText.getSpanString(getActivity(),
					String.valueOf(Transaction_BankDepositFragment.sSelected_BankName)));
			bankName.setTypeface(LoginActivity.sTypeface);
			bankName.setPadding(15, 5, 5, 5);
			bankName.setTextColor(color.black);
			bankName.setLayoutParams(header_ContentParams);
			outerRow.addView(bankName);

			TableRow.LayoutParams amountParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);
			amountParams.setMargins(5, 5, 20, 5);

			TextView bankDeposit_Amount = new TextView(getActivity());
			bankDeposit_Amount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(amount)));
			bankDeposit_Amount.setTextColor(color.black);
			bankDeposit_Amount.setPadding(5, 5, 5, 5);
			bankDeposit_Amount.setGravity(Gravity.RIGHT);
			bankDeposit_Amount.setLayoutParams(amountParams);
			outerRow.addView(bankDeposit_Amount);

			tableLayout.addView(outerRow, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

			for (int i = 0; i < size; i++) {

				TableRow innerRow = new TableRow(getActivity());

				TextView memberName = new TextView(getActivity());
				memberName.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sFixedDepositItem[i])));
				memberName.setTypeface(LoginActivity.sTypeface);
				memberName.setPadding(15, 0, 5, 5);
				memberName.setLayoutParams(header_ContentParams);
				memberName.setTextColor(color.black);
				innerRow.addView(memberName);

				mEditAmount = new EditText(getActivity());
				sEditTextFields.add(mEditAmount);
				mEditAmount.setId(i);
				mEditAmount.setInputType(InputType.TYPE_CLASS_NUMBER);
				mEditAmount.setPadding(5, 5, 5, 5);
				mEditAmount.setFilters(Get_EdiText_Filter.editText_filter());
				mEditAmount.setGravity(Gravity.RIGHT);
				mEditAmount.setLayoutParams(amountParams);
				mEditAmount.setWidth(150);
				mEditAmount.setBackgroundResource(R.drawable.edittext_background);
				mEditAmount.setOnFocusChangeListener(new View.OnFocusChangeListener() {

					@Override
					public void onFocusChange(View v, boolean hasFocus) {
						// TODO Auto-generated method stub
						if (hasFocus) {
							((EditText) v).setGravity(Gravity.LEFT);
						} else {
							((EditText) v).setGravity(Gravity.RIGHT);
						}

					}
				});
				innerRow.addView(mEditAmount);

				tableLayout.addView(innerRow,
						new TableLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return rootView;
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		try {

			sFixedDepositAmount = new String[sEditTextFields.size()];

			switch (v.getId()) {
			case R.id.fragment_deposit_entry_Submitbutton:
				
				sFixedDepositTotal = Integer.valueOf("0");
				sSend_To_Server_FD = Reset.reset(sSend_To_Server_FD);
				
				for (int i = 0; i < sFixedDepositAmount.length; i++) {

					sFixedDepositAmount[i] = String.valueOf(sEditTextFields.get(i).getText());

					if (sFixedDepositAmount[i] == null || sFixedDepositAmount[i].equals("")) {
						sFixedDepositAmount[i] = "0";
					}

					if (sFixedDepositAmount[i].matches("\\d*\\.?\\d+")) { // match
																			// a
																			// decimal
																			// number

						Log.e("Double valuesssssss", sFixedDepositAmount[i]);
						int amount = (int) Math.round(Double.parseDouble(sFixedDepositAmount[i]));
						sFixedDepositAmount[i] = String.valueOf(amount);
					}

					Log.e("Integer valuesssssss", sFixedDepositAmount[i]);

					sFixedDepositTotal = sFixedDepositTotal + Integer.parseInt(sFixedDepositAmount[i]);

					sSend_To_Server_FD = sSend_To_Server_FD + sFixedDepositAmount[i] + "~";

				}
				System.out.println(" Total Value:>>>>" + sFixedDepositTotal);

				System.out.println("Send To Confirmation Value:>>>" + sSend_To_Server_FD);

				bank_deposit = Integer.parseInt(sFixedDepositAmount[0]);
				bank_interest = Integer.parseInt(sFixedDepositAmount[1]);
				bank_withdrawl = Integer.parseInt(sFixedDepositAmount[2]);
				bank_expenses = Integer.parseInt(sFixedDepositAmount[3]);

				System.out.println("Bank Deposit:>>>" + bank_deposit);
				System.out.println("Bank Withdrawl:>>>" + bank_withdrawl);

				int fdAvailableWithdrawlAmt = Integer.parseInt(amount.trim()) + Integer.parseInt(sFixedDepositAmount[0])
						+ Integer.parseInt(sFixedDepositAmount[1]) - Integer.parseInt(sFixedDepositAmount[3]);

				System.out.println("Fixed Deposit Available Withdrawl Amount:" + fdAvailableWithdrawlAmt);
				if (sFixedDepositTotal != 0 && bank_deposit <= Integer.parseInt(mSelectedCashatBank.trim())
						&& bank_withdrawl <= fdAvailableWithdrawlAmt
						&& bank_expenses <= Integer.parseInt(mSelectedCashatBank.trim())) {

					int tempNewCashInHand = Integer.parseInt(SelectedGroupsTask.sCashinHand.trim())
							- Integer.parseInt(sFixedDepositAmount[0]) + Integer.parseInt(sFixedDepositAmount[2]);
					int tempNewCashAtBank = Integer.parseInt(mSelectedCashatBank.trim())
							+ Integer.parseInt(sFixedDepositAmount[0]) + Integer.parseInt(sFixedDepositAmount[1])
							- Integer.parseInt(sFixedDepositAmount[2]) - Integer.parseInt(sFixedDepositAmount[3]);

					System.out.println("-----Checking Amounts-----");
					System.out.println("New cashINHand:>>>>>" + tempNewCashInHand);
					System.out.println("New cashAtBank:>>>>>" + tempNewCashAtBank);

					mDeposit_Fixed_offline = sFixedDepositAmount[0];
					mInterest_Fixed_offline = sFixedDepositAmount[1];
					mWithdrawl_Fixed_offline = sFixedDepositAmount[2];
					mBankExpenses_Fixed_offline = sFixedDepositAmount[3];

					confirmationDialog = new Dialog(getActivity());

					LayoutInflater inflater = getActivity().getLayoutInflater();
					View dialogView = inflater.inflate(R.layout.dialog_confirmation, null);
					dialogView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
							LayoutParams.WRAP_CONTENT));

					TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
					confirmationHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.confirmation));
					confirmationHeader.setTypeface(LoginActivity.sTypeface);

					TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

					for (int i = 0; i < size; i++) {

						TableRow indv_DepositEntryRow = new TableRow(getActivity());

						TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
								LayoutParams.WRAP_CONTENT, 1f);
						contentParams.setMargins(10, 5, 10, 5);

						TextView memberName_Text = new TextView(getActivity());
						memberName_Text.setText(
								GetSpanText.getSpanString(getActivity(), String.valueOf(sFixedDepositItem[i])));
						memberName_Text.setTypeface(LoginActivity.sTypeface);
						memberName_Text.setTextColor(color.black);
						memberName_Text.setPadding(5, 5, 5, 5);
						memberName_Text.setLayoutParams(contentParams);
						indv_DepositEntryRow.addView(memberName_Text);

						TextView confirm_values = new TextView(getActivity());
						confirm_values.setText(
								GetSpanText.getSpanString(getActivity(), String.valueOf(sFixedDepositAmount[i])));
						confirm_values.setTextColor(color.black);
						confirm_values.setPadding(5, 5, 5, 5);
						confirm_values.setGravity(Gravity.RIGHT);
						confirm_values.setLayoutParams(contentParams);
						indv_DepositEntryRow.addView(confirm_values);

						confirmationTable.addView(indv_DepositEntryRow,
								new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
					}
					mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit_button);
					mEdit_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.edit));
					mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
					mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
																			// 205,
																			// 0));
					mEdit_RaisedButton.setOnClickListener(this);

					mOk_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Ok_button);
					mOk_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.yes));
					mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
					mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
					mOk_RaisedButton.setOnClickListener(this);

					confirmationDialog.getWindow()
							.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
					confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					confirmationDialog.setCanceledOnTouchOutside(false);
					confirmationDialog.setContentView(dialogView);
					confirmationDialog.setCancelable(true);
					confirmationDialog.show();

					MarginLayoutParams margin = (MarginLayoutParams) dialogView.getLayoutParams();
					margin.leftMargin = 10;
					margin.rightMargin = 10;
					margin.topMargin = 10;
					margin.bottomMargin = 10;
					margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);

				} else if (bank_deposit > Integer.parseInt(mSelectedCashatBank.trim())) {

					TastyToast.makeText(getActivity(), AppStrings.cashatBankAlert, TastyToast.LENGTH_SHORT,
							TastyToast.WARNING);
					sSend_To_Server_FD = Reset.reset(sSend_To_Server_FD);
					sFixedDepositTotal = Integer.valueOf("0");

				} else if (bank_withdrawl > fdAvailableWithdrawlAmt
						|| bank_expenses > Integer.parseInt(mSelectedCashatBank.trim())) {

					 if (bank_expenses > Integer.parseInt(mSelectedCashatBank.trim())) {
							
							TastyToast.makeText(getActivity(), AppStrings.cashatBankAlert, TastyToast.LENGTH_SHORT,
									TastyToast.WARNING);
					} else if (bank_withdrawl > fdAvailableWithdrawlAmt) {
						
						TastyToast.makeText(getActivity(), AppStrings.mCheckFixedDepositAmount, TastyToast.LENGTH_SHORT,
								TastyToast.WARNING);
						
					}

					sSend_To_Server_FD = Reset.reset(sSend_To_Server_FD);
					sFixedDepositTotal = Integer.valueOf("0");

				} else {
					TastyToast.makeText(getActivity(), AppStrings.DepositnullAlert, TastyToast.LENGTH_SHORT,
							TastyToast.WARNING);

					sSend_To_Server_FD = Reset.reset(sSend_To_Server_FD);
					sFixedDepositTotal = Integer.valueOf("0");

				}

				break;
			case R.id.fragment_Edit_button:
				sSend_To_Server_FD = Reset.reset(sSend_To_Server_FD);
				sFixedDepositTotal = Integer.valueOf("0");
				mRaised_Submit_Button.setClickable(true);
				isServiceCall = false;
				confirmationDialog.dismiss();
				break;
			case R.id.fragment_Ok_button:

				if (ConnectionUtils.isNetworkAvailable(getActivity())) {

					try {

						if (EShaktiApplication.getLoginFlag().equals(Constants.ONLINEFLAG)) {

							if (!isServiceCall) {
								isServiceCall = true;

								new Get_FixedDepositEntryTask(Transaction_FixedDepositEntryFragment.this).execute();
							}
						} else {

							TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
									TastyToast.ERROR);
						}

					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
				} else {
					// Do offline Stuffs
					Log.v("Offline Ok", "Done button clicked");
					if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
						EShaktiApplication.getInstance().getTransactionManager().startTransaction(
								DataType.GET_TRANS_SINGLE, new GetTransactionSingle(PrefUtils.getOfflineUniqueID()));
					} else {

						TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
								TastyToast.ERROR);
					}
				}
				break;

			default:
				break;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub
		mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
		mProgressDilaog.show();
	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub

		if (mProgressDilaog != null) {
			mProgressDilaog.dismiss();
			if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {
				getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub

						if (!result.equals("FAIL")) {
							isServiceCall = false;
							TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg,
									TastyToast.LENGTH_SHORT, TastyToast.ERROR);
							Constants.NETWORKCOMMONFLAG = "SUCCESS";
						}

					}
				});

			} else {
				try {
					if (isGetTrid) {
						isGetTrid = false;
						callOfflineDataUpdate();
					} else {
						if (GetResponseInfo.sResponseUpdate[0].equals("Yes")) {

							TastyToast.makeText(getActivity(), AppStrings.transactionCompleted, TastyToast.LENGTH_SHORT,
									TastyToast.SUCCESS);

							new Get_LastTransactionID(Transaction_FixedDepositEntryFragment.this).execute();
							isGetTrid = true;

						} else {

							TastyToast.makeText(getActivity(), AppStrings.transactionFailAlert, TastyToast.LENGTH_SHORT,
									TastyToast.ERROR);
							MainFragment_Dashboard fragment = new MainFragment_Dashboard();
							setFragment(fragment);
						}

						confirmationDialog.dismiss();
					}

				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		}
	}

	private void callOfflineDataUpdate() {
		// TODO Auto-generated method stub
		String mCashinHand, mCashatBank;

		mCashatBank_Fixed_individual = Transaction_DepositMenuFragment.mFixedOS_Offlineresponse;
		mCashatBank = SelectedGroupsTask.sCashatBank;

		Log.e(TAG + "Deposit Amount", mDeposit_Fixed_offline);
		Log.e(TAG + "mInterest_offline", mInterest_Fixed_offline);
		Log.e(TAG + "mWithdrawl_offline", mWithdrawl_Fixed_offline);
		Log.e(TAG + "mBankExpenses_offline Amount", mBankExpenses_Fixed_offline);

		mCashinHand = SelectedGroupsTask.sCashinHand;

		mCashatBank_Fixed_individual = String.valueOf((Integer.parseInt(mCashatBank_Fixed_individual)
				+ Integer.parseInt(mDeposit_Fixed_offline) + Integer.parseInt(mInterest_Fixed_offline))
				- Integer.parseInt(mWithdrawl_Fixed_offline) - Integer.parseInt(mBankExpenses_Fixed_offline));

		mCashatBank_individual = Transaction_BankDepositFragment.sSelected_BankDepositAmount;

		mCashatBank_individual = String
				.valueOf((Integer.parseInt(mCashatBank_individual) + Integer.parseInt(mWithdrawl_Fixed_offline))
						- Integer.parseInt(mDeposit_Fixed_offline));

		EShaktiApplication.setBankFD(true);
		String mBankDetails = GetOfflineTransactionValues.getBankDetails();
		mLastTrDate = SelectedGroupsTask.sLastTransactionDate_Response;

		String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mLastTrDate, mCashinHand, mCashatBank,
				mBankDetails);
		String mFixedDepositOS = GetFixedDepositBankValues.GetFixedDepositBankValues();
		String mSelectedGroupId = SelectedGroupsTask.Group_Id;
		Log.e("Yessssssssssssssssssssssssssssss", mFixedDepositOS);
		isServiceCall = false;
		EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GROUP_MASTER_FDOS,
				new GroupFOSUpdate(mSelectedGroupId, mGroupMasterResponse, mFixedDepositOS));

	}

	@Subscribe
	public void OnGetSingleTransactionExpense(final GetSingleTransResponse getSingleTransResponse)
			throws ParseException {
		switch (getSingleTransResponse.getFetcherResult()) {
		case FAIL:
			Toast.makeText(getActivity(), getSingleTransResponse.getFetcherResult().getMessage(), Toast.LENGTH_SHORT)
					.show();
			break;
		case NO_NETWORK_CONNECTION:
			Toast.makeText(getActivity(), getSingleTransResponse.getFetcherResult().getMessage(), Toast.LENGTH_SHORT)
					.show();
			break;
		case SUCCESS:

			try {
				System.out.println("---------OnGetSingleTransactionExpense----------");

				String mUniqueId = GetTransactionSinglevalues.getUniqueId();
				String mBankFixedDepositValues_Offline = GetTransactionSinglevalues.getBankFixedDeposit();
				String mBankCheckValues = null;

				mLastTrDate = DatePickerDialog.sDashboardDate;
				mLastTr_ID = SelectedGroupsTask.sTr_ID.toString();
				String mCurrentTransDate = null;

				if (publicValues.mOffline_Trans_CurrentDate != null) {
					mCurrentTransDate = publicValues.mOffline_Trans_CurrentDate;
				}

				String mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
				String mMobileDate = Get_Offline_MobileDate.getOffline_MobileDate(getActivity());
				String mTransaction_UniqueId = PrefUtils.getOfflineUniqueID();

				mOfflineBankname = Transaction_BankDepositFragment.sSendToServer_BankName;

				String mSend_to_Offline_server_FD = sSend_To_Server_FD + "#" + mOfflineBankname + "#" + mTrasactiondate
						+ "#" + mMobileDate + "%";

				Log.e("Step 11111", "111111111");
				System.out.println("---------------->>>>>>>>>>>>" + mOfflineBankname);
				if (mBankFixedDepositValues_Offline == null) {

					publicValues.mFixedCheckLabel = new String[SelectedGroupsTask.sBankNames.size()];
					for (int i = 0; i < publicValues.mFixedCheckLabel.length; i++) {
						publicValues.mFixedCheckLabel[i] = "0";

					}
				}
				if (publicValues.mFixedCheckLabel != null) {
					for (int j = 0; j < publicValues.mFixedCheckLabel.length; j++) {
						String sSendToServer_BankName = SelectedGroupsTask.sEngBankNames.elementAt(j).toString();

						if (sSendToServer_BankName.equals(Transaction_BankDepositFragment.sSendToServer_BankName)) {
							mBankCheckValues = publicValues.mFixedCheckLabel[j];
						}

					}
				}
				Log.e("Bank Check Values::::", mBankCheckValues);

				Log.e("mSend_to_Offline_server_FD $$$ ", mSend_to_Offline_server_FD + "");

				if (!mSend_to_Offline_server_FD.contains("?") && !mCurrentTransDate.contains("?")
						&& !mTransaction_UniqueId.contains("?")) {

					if (mBankCheckValues.equals("0")) {

						if (PrefUtils.getOfflineBankFixedDeposit() == null
								&& PrefUtils.getOfflineBankFixedDepositName() == null) {
							PrefUtils.setOfflineBankFixedDeposit(mSend_to_Offline_server_FD);
							Log.e("Step 222222", "2222222");
						} else if (!PrefUtils.getOfflineBankFixedDepositName().equalsIgnoreCase(mOfflineBankname)) {
							Log.e("Step 333333333333333333333333", "33333333333333333");
							mSend_to_Offline_server_FD = PrefUtils.getOfflineBankFixedDeposit()
									+ mSend_to_Offline_server_FD;
							PrefUtils.setOfflineBankFixedDeposit(mSend_to_Offline_server_FD);
						}
						Log.e("Pref Utils Valuessssssssss", PrefUtils.getOfflineBankFixedDeposit());
						if (mUniqueId == null && mBankFixedDepositValues_Offline == null
								&& !PrefUtils.getOfflineBankFixedDeposit().equals(null)) {
							Log.e("Step 111111111113333333333", "1111333333333333333333");

							mSqliteDBStoredValues_FDValues = mSend_to_Offline_server_FD;

							if (mLastTrDate != null && mMobileDate != null && mTransaction_UniqueId != null
									&& mCurrentTransDate != null) {
								EShaktiApplication.getInstance().getTransactionManager().startTransaction(
										DataType.TRANSACTIONADD,
										new Transaction(0, PrefUtils.getUsernameKey(), SelectedGroupsTask.UserName,
												SelectedGroupsTask.Ngo_Id, SelectedGroupsTask.Group_Id,
												mTransaction_UniqueId, mLastTr_ID, mCurrentTransDate, null, null, null,
												null, null, null, null, null, null, null, null, null,
												mSend_to_Offline_server_FD, null, null, null, null, null, null, null,
												null, null));
							} else {

								TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT,
										TastyToast.ERROR);
							}

						} else if (mUniqueId.equals(PrefUtils.getOfflineUniqueID())
								&& mBankFixedDepositValues_Offline == null) {

							EShaktiApplication.setSetTransValues(TransactionOffConstants.TRANS_FIXD);

							EShaktiApplication.getInstance().getTransactionManager().startTransaction(
									DataType.TRANS_VALUES_UPDATE,
									new TransactionUpdate(mUniqueId, mSend_to_Offline_server_FD));

						} else if (mUniqueId.equals(PrefUtils.getOfflineUniqueID())
								&& mBankFixedDepositValues_Offline != null
								&& !PrefUtils.getOfflineBankFixedDepositName().equalsIgnoreCase(mOfflineBankname)) {
							Log.v("444444", "44444444");
							EShaktiApplication.setSetTransValues(TransactionOffConstants.TRANS_FIXD);

							EShaktiApplication.getInstance().getTransactionManager().startTransaction(
									DataType.TRANS_VALUES_UPDATE,
									new TransactionUpdate(mUniqueId, mSend_to_Offline_server_FD));

						} else if ((mBankFixedDepositValues_Offline != null)
								|| PrefUtils.getOfflineBankFixedDepositName().equals(mOfflineBankname)) {
							if (confirmationDialog.isShowing()) {

								confirmationDialog.dismiss();
							}

							TastyToast.makeText(getActivity(), AppStrings.offlineDataAvailable, TastyToast.LENGTH_SHORT,
									TastyToast.WARNING);

							DatePickerDialog.sDashboardDate = SelectedGroupsTask.sLastTransactionDate_Response;
							MainFragment_Dashboard fragment = new MainFragment_Dashboard();
							setFragment(fragment);
						}
					} else {
						if (confirmationDialog.isShowing()) {

							confirmationDialog.dismiss();
						}
						DatePickerDialog.sDashboardDate = SelectedGroupsTask.sLastTransactionDate_Response;
						TastyToast.makeText(getActivity(), AppStrings.offlineDataAvailable, TastyToast.LENGTH_SHORT,
								TastyToast.WARNING);

						MainFragment_Dashboard fragment = new MainFragment_Dashboard();
						setFragment(fragment);

					}
				} else {
					if (confirmationDialog.isShowing() && confirmationDialog != null) {
						confirmationDialog.dismiss();
					}

					TastyToast.makeText(getActivity(), "Please Try Again", TastyToast.LENGTH_SHORT, TastyToast.ERROR);

					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);

				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;

		}
	}

	@Subscribe
	public void onAddTransactionBankDeposit(final TransactionResponse transactionResponse) throws ParseException {
		switch (transactionResponse.getFetcherResult()) {
		case FAIL:
			Toast.makeText(getActivity(), transactionResponse.getFetcherResult().getMessage(), Toast.LENGTH_SHORT)
					.show();
			break;
		case NO_NETWORK_CONNECTION:
			Toast.makeText(getActivity(), transactionResponse.getFetcherResult().getMessage(), Toast.LENGTH_SHORT)
					.show();
			break;
		case SUCCESS:

			try {
				System.out.println("-----------onAddTransactionBankDeposit----------");

				TransactionProvider.getSinlgeGroupMaster_Transaction(
						new Transaction_UniqueIdSingle(PrefUtils.getOfflineUniqueID()));

				if (GetTransactionSinglevalues.getBankFixedDeposit() != null

						&& !GetTransactionSinglevalues.getBankFixedDeposit().equals("")) {

					String mCashinHand, mCashatBank;

					mCashatBank_Fixed_individual = Transaction_DepositMenuFragment.mFixedOS_Offlineresponse;
					mCashatBank = SelectedGroupsTask.sCashatBank;

					Log.e(TAG + "Deposit Amount", mDeposit_Fixed_offline);
					Log.e(TAG + "mInterest_offline", mInterest_Fixed_offline);
					Log.e(TAG + "mWithdrawl_offline", mWithdrawl_Fixed_offline);
					Log.e(TAG + "mBankExpenses_offline Amount", mBankExpenses_Fixed_offline);

					mCashatBank_individual = Transaction_BankDepositFragment.sSelected_BankDepositAmount;

					mCashatBank_individual = String.valueOf(
							(Integer.parseInt(mCashatBank_individual) + Integer.parseInt(mWithdrawl_Fixed_offline))
									- Integer.parseInt(mDeposit_Fixed_offline));

					mCashinHand = String.valueOf((Integer.parseInt(SelectedGroupsTask.sCashinHand)));

					mCashatBank_Fixed_individual = String.valueOf((Integer.parseInt(mCashatBank_Fixed_individual)
							+ Integer.parseInt(mDeposit_Fixed_offline) + Integer.parseInt(mInterest_Fixed_offline))
							- Integer.parseInt(mWithdrawl_Fixed_offline)
							- Integer.parseInt(mBankExpenses_Fixed_offline));

					mCashatBank = String
							.valueOf((Integer.parseInt(mCashatBank) - Integer.parseInt(mDeposit_Fixed_offline))
									+ Integer.parseInt(mWithdrawl_Fixed_offline));

					Log.i(TAG + "Deposit Amount", mDeposit_Fixed_offline);
					Log.i(TAG + "Cash in Hand", mCashinHand);
					Log.i(TAG + "Cash at Bank Individual", mCashatBank_Fixed_individual);
					Log.i(TAG + "Cash at bank", mCashatBank);

					EShaktiApplication.setBankFD(true);

					String mBankDetails = GetOfflineTransactionValues.getBankDetails();
					SelectedGroupsTask.sCashinHand = mCashinHand;
					SelectedGroupsTask.sCashatBank = mCashatBank;
					String mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
					SelectedGroupsTask.sLastTransactionDate_Response = mTrasactiondate;
					String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mTrasactiondate,
							mCashinHand, mCashatBank, mBankDetails);
					String mFixedDepositOS = GetFixedDepositBankValues.GetFixedDepositBankValues();
					String mSelectedGroupId = SelectedGroupsTask.Group_Id;
					Log.e(Transaction_FixedDepositEntryFragment.class.getName(),
							mCashatBank + "             2222                   " + mCashinHand
									+ "             2222                   " + mFixedDepositOS);

					Log.e("Yessssssssssssssssssssssssssssss", mFixedDepositOS);

					Log.e("mGroupMasterResponse $$$ ", mGroupMasterResponse + "");
					Log.e("mFixedDepositOS $$$ ", mFixedDepositOS + "");
					EShaktiApplication.getInstance().getTransactionManager().startTransaction(
							DataType.GROUP_MASTER_FDOS,
							new GroupFOSUpdate(mSelectedGroupId, mGroupMasterResponse, mFixedDepositOS));
				} else {
					if (confirmationDialog.isShowing() && confirmationDialog != null) {
						confirmationDialog.dismiss();
					}

					TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT, TastyToast.ERROR);

					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);

				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		}
	}

	@Subscribe
	public void OnTransUpdateBankDeposit(final TransactionUpdateResponse transactionUpdateResponse)
			throws ParseException {
		switch (transactionUpdateResponse.getFetcherResult()) {
		case FAIL:
			Toast.makeText(getActivity(), transactionUpdateResponse.getFetcherResult().getMessage(), Toast.LENGTH_SHORT)
					.show();
			break;
		case NO_NETWORK_CONNECTION:
			Toast.makeText(getActivity(), transactionUpdateResponse.getFetcherResult().getMessage(), Toast.LENGTH_SHORT)
					.show();
			break;
		case SUCCESS:
			try {
				System.out.println("--------OnTransUpdateBankDeposit--------");

				TransactionProvider.getSinlgeGroupMaster_Transaction(
						new Transaction_UniqueIdSingle(PrefUtils.getOfflineUniqueID()));

				if (GetTransactionSinglevalues.getBankFixedDeposit() != null

						&& !GetTransactionSinglevalues.getBankFixedDeposit().equals("")) {
					if (confirmationDialog.isShowing()) {
						// alertDialog.dismiss();
						confirmationDialog.dismiss();
					}
					String mCashinHand, mCashatBank;

					mCashatBank_Fixed_individual = Transaction_DepositMenuFragment.mFixedOS_Offlineresponse;
					mCashatBank = SelectedGroupsTask.sCashatBank;

					Log.e(TAG + "Deposit Amount", mDeposit_Fixed_offline);
					Log.e(TAG + "mInterest_offline", mInterest_Fixed_offline);
					Log.e(TAG + "mWithdrawl_offline", mWithdrawl_Fixed_offline);
					Log.e(TAG + "mBankExpenses_offline Amount", mBankExpenses_Fixed_offline);

					mCashinHand = String.valueOf((Integer.parseInt(SelectedGroupsTask.sCashinHand)));

					mCashatBank_Fixed_individual = String.valueOf((Integer.parseInt(mCashatBank_Fixed_individual)
							+ Integer.parseInt(mDeposit_Fixed_offline) + Integer.parseInt(mInterest_Fixed_offline))
							- Integer.parseInt(mWithdrawl_Fixed_offline)
							- Integer.parseInt(mBankExpenses_Fixed_offline));

					mCashatBank_individual = Transaction_BankDepositFragment.sSelected_BankDepositAmount;

					mCashatBank_individual = String.valueOf(
							(Integer.parseInt(mCashatBank_individual) + Integer.parseInt(mWithdrawl_Fixed_offline))
									- Integer.parseInt(mDeposit_Fixed_offline));

					mCashatBank = String
							.valueOf((Integer.parseInt(mCashatBank) - Integer.parseInt(mDeposit_Fixed_offline))
									+ Integer.parseInt(mWithdrawl_Fixed_offline));
					EShaktiApplication.setBankFD(true);
					String mBankDetails = GetOfflineTransactionValues.getBankDetails();
					SelectedGroupsTask.sCashinHand = mCashinHand;
					SelectedGroupsTask.sCashatBank = mCashatBank;
					String mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
					SelectedGroupsTask.sLastTransactionDate_Response = mTrasactiondate;
					String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mTrasactiondate,
							mCashinHand, mCashatBank, mBankDetails);
					String mFixedDepositOS = GetFixedDepositBankValues.GetFixedDepositBankValues();
					String mSelectedGroupId = SelectedGroupsTask.Group_Id;
					Log.e("Yessssssssssssssssssssssssssssss", mFixedDepositOS);
					Log.e("mGroupMasterResponse $$$ ", mGroupMasterResponse + "");
					Log.e("mFixedDepositOS $$$ ", mFixedDepositOS + "");
					EShaktiApplication.getInstance().getTransactionManager().startTransaction(
							DataType.GROUP_MASTER_FDOS,
							new GroupFOSUpdate(mSelectedGroupId, mGroupMasterResponse, mFixedDepositOS));
				} else {
					if (confirmationDialog.isShowing() && confirmationDialog != null) {
						confirmationDialog.dismiss();
					}

					TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT, TastyToast.ERROR);

					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);

				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		}
	}

	@Subscribe
	public void OnGroupMasBankFixedDepositUpdate(final GroupFixedOSResponse masterResponse) {
		switch (masterResponse.getFetcherResult()) {
		case FAIL:
			Toast.makeText(getActivity(), masterResponse.getFetcherResult().getMessage(), Toast.LENGTH_SHORT).show();
			break;
		case NO_NETWORK_CONNECTION:
			Toast.makeText(getActivity(), masterResponse.getFetcherResult().getMessage(), Toast.LENGTH_SHORT).show();
			break;
		case SUCCESS:
			try {
				System.out.println("--------OnGroupMasBankFixedDepositUpdate----------");
				Log.e("Group Master Update Sucessfully", "Updated !@#$#$#$");
				if (ConnectionUtils.isNetworkAvailable(getActivity())) {

					if (confirmationDialog != null) {

						confirmationDialog.dismiss();
					}
					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);
				} else {
					try {

						PrefUtils.setOfflineBankFixedDepositName(mOfflineBankname);
						for (int j = 0; j < publicValues.mFixedCheckLabel.length; j++) {
							String sSendToServer_BankName = SelectedGroupsTask.sEngBankNames.elementAt(j).toString();

							if (sSendToServer_BankName.equals(PrefUtils.getOfflineBankFixedDepositName())) {
								publicValues.mFixedCheckLabel[j] = "1";
							}
							Log.e("Fixed Check Values", publicValues.mFixedCheckLabel[j]);
						}

						confirmationDialog.dismiss();

						TastyToast.makeText(getActivity(), AppStrings.transactionCompleted, TastyToast.LENGTH_SHORT,
								TastyToast.SUCCESS);

						if (!ConnectionUtils.isNetworkAvailable(getActivity())) {

							String mValues = Put_DB_GroupNameDetail_Response
									.put_DB_GroupNameDetails_Response(EShaktiApplication.getGroupResponse());

							GroupProvider.updateGroupResponse(new GroupResponseUpdate(
									EShaktiApplication.getGroupId_GroupLastTransDate(), mValues));
						}

						MainFragment_Dashboard fragment = new MainFragment_Dashboard();
						setFragment(fragment);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		}
	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub
		Log.e("Current Class Name!!!!!!!!!!!!!!", fragment.getClass().getName());
		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

		FragmentDrawer.drawer_CashinHand.setText(RegionalConversion.getRegionalConversion(AppStrings.cashinhand)

				+ SelectedGroupsTask.sCashinHand);
		FragmentDrawer.drawer_CashinHand.setTypeface(LoginActivity.sTypeface);

		FragmentDrawer.drawer_CashatBank.setText(RegionalConversion.getRegionalConversion(AppStrings.cashatBank)

				+ SelectedGroupsTask.sCashatBank);
		FragmentDrawer.drawer_CashatBank.setTypeface(LoginActivity.sTypeface);
		
		FragmentDrawer.drawer_lastTR_Date.setText(RegionalConversion.getRegionalConversion(AppStrings.lastTransactionDate)
				+ " : " + SelectedGroupsTask.sLastTransactionDate_Response);
		FragmentDrawer.drawer_lastTR_Date.setTypeface(LoginActivity.sTypeface);

		UpdateCalendarMinMaxDateUtils.OnUpdateCalendarDate();
		
		Calendar calender = Calendar.getInstance();

		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String formattedDate = df.format(calender.getTime());
		EShaktiApplication.setLastTransDate_GroupId(SelectedGroupsTask.Group_Id);

		new GroupTempDBLastTransDate(EShaktiApplication.getLastTransDate_GroupId(),
				SelectedGroupsTask.sLastTransactionDate_Response, formattedDate);

		GroupProvider.updateGroupLastTransDateResponse();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		SBus.INST.unRegister(this);
	}

}
