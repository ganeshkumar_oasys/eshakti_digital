package com.yesteam.eshakti.view.fragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.squareup.otto.Subscribe;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.TransactionOffConstants;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;
import com.yesteam.eshakti.sqlite.database.GroupProvider;
import com.yesteam.eshakti.sqlite.database.TransactionProvider;
import com.yesteam.eshakti.sqlite.database.response.DefaultOffline_PL_DisResponse;
import com.yesteam.eshakti.sqlite.database.response.GetSingleTransResponse;
import com.yesteam.eshakti.sqlite.database.response.GroupMemberloanOSResponse;
import com.yesteam.eshakti.sqlite.database.response.GroupPLOS_Update_Response;
import com.yesteam.eshakti.sqlite.database.response.TransactionResponse;
import com.yesteam.eshakti.sqlite.database.response.TransactionUpdateResponse;
import com.yesteam.eshakti.sqlite.db.model.GetGroupMemberDetails;
import com.yesteam.eshakti.sqlite.db.model.GetTransactionSingle;
import com.yesteam.eshakti.sqlite.db.model.GetTransactionSinglevalues;
import com.yesteam.eshakti.sqlite.db.model.GroupMLUpdate;
import com.yesteam.eshakti.sqlite.db.model.GroupMasterSingle;
import com.yesteam.eshakti.sqlite.db.model.GroupResponseUpdate;
import com.yesteam.eshakti.sqlite.db.model.GroupTempDBLastTransDate;
import com.yesteam.eshakti.sqlite.db.model.Transaction;
import com.yesteam.eshakti.sqlite.db.model.TransactionUpdate;
import com.yesteam.eshakti.sqlite.db.model.Transaction_UniqueIdSingle;
import com.yesteam.eshakti.sqlite.db.transactions.TransactionManager.DataType;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.utils.GetOfflineTransactionValues;
import com.yesteam.eshakti.utils.GetResponseInfo;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.utils.Get_EdiText_Filter;
import com.yesteam.eshakti.utils.Get_Offline_MobileDate;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.utils.Put_DB_GroupNameDetail_Response;
import com.yesteam.eshakti.utils.Put_DB_GroupResponse;
import com.yesteam.eshakti.utils.Reset;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.utils.TextviewUtils;
import com.yesteam.eshakti.utils.UpdateCalendarMinMaxDateUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.views.CustomHorizontalScrollView;
import com.yesteam.eshakti.views.CustomHorizontalScrollView.onScrollChangedListener;
import com.yesteam.eshakti.webservices.GetMember_Loan_RepaidTask;
import com.yesteam.eshakti.webservices.Get_All_Mem_OutstandingTask;
import com.yesteam.eshakti.webservices.Get_LastTransactionID;
import com.yesteam.eshakti.webservices.Get_PurposeOfLoanTask;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Transaction_PersonalLoanRepaidFragment extends Fragment implements OnClickListener, TaskListener {

	public static final String TAG = Transaction_PersonalLoanRepaidFragment.class.getSimpleName();
	List<EditText> sPA_Fields;
	List<EditText> sInterest_Fields;
	public static String sPrincipleAmounts[];
	public static String sInterestAmounts[];
	public static String sSendToServer_MemLoanRepaid;
	public static int sPA_total = 0;
	public static int sInterest_total = 0;

	private Dialog mProgressDilaog;

	private TextView mGroupName, mCashinHand, mCashatBank;
	private TextView mHeader;
	private Button mSubmit_Raised_Button, mEdit_RaisedButton, mOk_RaisedButton;
	private EditText mPA_EditText;
	private EditText mInterest_EditText;
	private String[] mOutstanding, mRepaymentWithInterest;
	private boolean outstandingAlert;

	boolean isCheck = false;
	int mSize;
	String[] response, toBeEdit_PA, toBeEdit_Interest;
	String nullAmount = "0";
	String tempID = "", tempOuts = "", tempRepayment = "";
	String[] memID_Check;
	int[] validatedOutstanding;

	AlertDialog alertDialog;
	Dialog confirmationDialog;

	String mLastTrDate = null, mLastTr_ID = null;

	private TableLayout mLeftHeaderTable, mRightHeaderTable, mLeftContentTable, mRightContentTable;
	private CustomHorizontalScrollView mHSRightHeader, mHSRightContent;

	String width[] = { AppStrings.principleAmount, AppStrings.interest, AppStrings.OutsatndingAmt,
			AppStrings.interestRepayAmount };
	int[] rightHeaderWidth = new int[width.length];
	int[] rightContentWidth = new int[width.length];
	boolean isDefaultService = false;
	boolean isDefaultService_LoanPurpose = false;
	int mSavings_Values = 0;
	boolean isGetTrid = false;
	Button mPerviousButton, mNextButton;
	boolean isNextButton = false;
	public static int memLoanCount = 0;
	boolean isServiceCall = false;
	int mTotalOutstanding = 0;

	String mSqliteDBStoredValues_InternalLoanValues = null;

	LinearLayout mMemberNameLayout;
	TextView mMemberName;

	public Transaction_PersonalLoanRepaidFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_PERSONALLOANREPAID;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		SBus.INST.register(this);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		try {
			memLoanCount = 0;
			sPA_total = 0;
			sInterest_total = 0;
			sSendToServer_MemLoanRepaid = Reset.reset(sSendToServer_MemLoanRepaid);
			sPA_Fields = new ArrayList<EditText>();
			sInterest_Fields = new ArrayList<EditText>();

			mTotalOutstanding = 0;

			mSize = SelectedGroupsTask.member_Name.size();
			Log.d(TAG, "mSize " + String.valueOf(mSize));
			if (ConnectionUtils.isNetworkAvailable(getActivity())) {

				if (PrefUtils.getGroupMasterResValues() != null && PrefUtils.getGroupMasterResValues().equals("1")) {

					response = Transaction_MemLoanRepaid_MenuFragment.mPersonalOS_offlineresponse.split("~");
					System.out.println("RESPONSE : " + response.length);

				} else {

					response = Get_All_Mem_OutstandingTask.sGet_All_Mem_OutstandingTask_Response.split("~");
					System.out.println("RESPONSE : " + response.length);
					Log.v("Nabard Member loan Outstanding",
							Get_All_Mem_OutstandingTask.sGet_All_Mem_OutstandingTask_Response);
				}
			} else {
				response = Transaction_MemLoanRepaid_MenuFragment.mPersonalOS_offlineresponse.split("~");
				System.out.println("RESPONSE : " + response.length);
			}

			int count = 0;

			for (int i = 0; i < response.length; i++) {

				if (count == 0) {
					tempID = tempID + response[i] + "~";
					count = 1;
				} else if (count == 1) {
					tempOuts = tempOuts + response[i] + "~";
					count = 2;
				} else if (count == 2) {
					if (response[i].equals("")) {
						response[i] = nullAmount;
					}
					tempRepayment = tempRepayment + response[i] + "~";
					count = 0;
				}
			}

			validatedOutstanding = new int[mSize];

			// Storing the corresponding values in the corresponding array
			for (int i = 0; i < mSize; i++) {

				memID_Check = tempID.split("~");
				System.out.println("memID_Check " + memID_Check[i] + " POS " + i);

				mOutstanding = tempOuts.split("~");
				// TypeCasting the double values into integer
				validatedOutstanding[i] = Integer.parseInt(mOutstanding[i].substring(0, mOutstanding[i].indexOf(".")));
				// mOutstanding[i] = String.valueOf(validatedOutstanding[i]);
				System.out.println("mOutstanding " + validatedOutstanding[i] + " POS " + i);

				mRepaymentWithInterest = tempRepayment.split("~");
				System.out.println("mRepaymentWithInterest " + mRepaymentWithInterest[i] + " POS " + i);

				mTotalOutstanding = (mTotalOutstanding + validatedOutstanding[i]);
				Log.e("Current Values ----->>>>>", mTotalOutstanding + "");
			}

			Log.e("Total Out Standing  ------------->>>>", mTotalOutstanding + "");

			Log.d(TAG, "SIZE CHECK " + mOutstanding.length);
			if (EShaktiApplication.isDefault()) {
				Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID = "0";

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View rootView = inflater.inflate(R.layout.fragment_all_transaction, container, false);

		MainFragment_Dashboard.isBackpressed = false;

		mSqliteDBStoredValues_InternalLoanValues = null;

		try {

			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashinHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashinHand.setTypeface(LoginActivity.sTypeface);

			mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashatBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashatBank.setTypeface(LoginActivity.sTypeface);
			mHeader = (TextView) rootView.findViewById(R.id.fragmentHeader);

			mMemberNameLayout = (LinearLayout) rootView.findViewById(R.id.member_name_layout);
			mMemberName = (TextView) rootView.findViewById(R.id.member_name);

			if (!EShaktiApplication.isDefault()) {

				mHeader.setText(RegionalConversion
						.getRegionalConversion(Transaction_MemLoanRepaid_MenuFragment.sSelected_LoanName));

			} else {
				mHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.mInterloan));
			}
			mHeader.setTypeface(LoginActivity.sTypeface);
			mLeftHeaderTable = (TableLayout) rootView.findViewById(R.id.LeftHeaderTable);
			mRightHeaderTable = (TableLayout) rootView.findViewById(R.id.RightHeaderTable);
			mLeftContentTable = (TableLayout) rootView.findViewById(R.id.LeftContentTable);
			mRightContentTable = (TableLayout) rootView.findViewById(R.id.RightContentTable);

			mHSRightHeader = (CustomHorizontalScrollView) rootView.findViewById(R.id.rightHeaderHScrollView);
			mHSRightContent = (CustomHorizontalScrollView) rootView.findViewById(R.id.rightContentHScrollView);

			mHSRightHeader.setOnScrollChangedListener(new onScrollChangedListener() {

				public void onScrollChanged(int l, int t, int oldl, int oldt) {
					// TODO Auto-generated method stub

					mHSRightContent.scrollTo(l, 0);

				}
			});

			mHSRightContent.setOnScrollChangedListener(new onScrollChangedListener() {
				@Override
				public void onScrollChanged(int l, int t, int oldl, int oldt) {
					// TODO Auto-generated method stub
					mHSRightHeader.scrollTo(l, 0);
				}
			});

			TableRow leftHeaderRow = new TableRow(getActivity());

			TableRow.LayoutParams lHeaderParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);

			TextView mMemberName_Header = new TextView(getActivity());
			mMemberName_Header.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.memberName)));
			mMemberName_Header.setTypeface(LoginActivity.sTypeface);
			mMemberName_Header.setTextColor(Color.WHITE);
			mMemberName_Header.setPadding(10, 5, 10, 5);
			mMemberName_Header.setLayoutParams(lHeaderParams);
			leftHeaderRow.addView(mMemberName_Header);

			mLeftHeaderTable.addView(leftHeaderRow);

			TableRow rightHeaderRow = new TableRow(getActivity());
			TableRow.LayoutParams rHeaderParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);
			rHeaderParams.setMargins(10, 0, 10, 0);

			TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);
			contentParams.setMargins(10, 0, 10, 0);

			TextView mOutstanding_Header = new TextView(getActivity());
			mOutstanding_Header
					.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.OutsatndingAmt)));
			mOutstanding_Header.setTypeface(LoginActivity.sTypeface);
			mOutstanding_Header.setTextColor(Color.WHITE);
			mOutstanding_Header.setGravity(Gravity.RIGHT);
			mOutstanding_Header.setPadding(10, 5, 10, 5);
			mOutstanding_Header.setLayoutParams(contentParams);
			rightHeaderRow.addView(mOutstanding_Header);

			TextView mPA_Header = new TextView(getActivity());
			mPA_Header.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.principleAmount)));
			mPA_Header.setTypeface(LoginActivity.sTypeface);
			mPA_Header.setTextColor(Color.WHITE);
			mPA_Header.setPadding(10, 5, 10, 5);
			mPA_Header.setGravity(Gravity.CENTER);
			mPA_Header.setLayoutParams(rHeaderParams);
			rightHeaderRow.addView(mPA_Header);

			TextView mInterest_Header = new TextView(getActivity());
			mInterest_Header.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.interest)));
			mInterest_Header.setTypeface(LoginActivity.sTypeface);
			mInterest_Header.setTextColor(Color.WHITE);
			mInterest_Header.setGravity(Gravity.CENTER);
			mInterest_Header.setPadding(10, 5, 10, 5);
			mInterest_Header.setLayoutParams(rHeaderParams);
			rightHeaderRow.addView(mInterest_Header);

			TextView mRepayment_Header = new TextView(getActivity());
			mRepayment_Header
					.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.interestRepayAmount)));
			mRepayment_Header.setTypeface(LoginActivity.sTypeface);
			mRepayment_Header.setTextColor(Color.WHITE);
			mRepayment_Header.setGravity(Gravity.RIGHT);
			mRepayment_Header.setLayoutParams(contentParams);
			mRepayment_Header.setPadding(10, 5, 10, 5);
			rightHeaderRow.addView(mRepayment_Header);

			mRightHeaderTable.addView(rightHeaderRow);

			try {

				for (int j = 0; j < mSize; j++) {
					TableRow leftContentRow = new TableRow(getActivity());

					TableRow.LayoutParams leftContentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT, 60,
							1f);
					leftContentParams.setMargins(5, 5, 5, 5);

					final TextView memberName_Text = new TextView(getActivity());
					memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
							String.valueOf(SelectedGroupsTask.member_Name.elementAt(j))));
					memberName_Text.setTypeface(LoginActivity.sTypeface);
					memberName_Text.setTextColor(color.black);
					memberName_Text.setPadding(5, 0, 5, 5);
					memberName_Text.setLayoutParams(leftContentParams);
					memberName_Text.setWidth(200);
					memberName_Text.setSingleLine(true);
					memberName_Text.setEllipsize(TextUtils.TruncateAt.END);
					leftContentRow.addView(memberName_Text);

					mLeftContentTable.addView(leftContentRow);

					TableRow rightContentRow = new TableRow(getActivity());

					TableRow.LayoutParams rightContentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
							LayoutParams.WRAP_CONTENT, 1f);
					rightContentParams.setMargins(10, 5, 10, 5);

					TableRow.LayoutParams contentRow_Params = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT, 60,
							1f);
					contentRow_Params.setMargins(10, 5, 10, 5);

					TextView outstanding = new TextView(getActivity());
					outstanding
							.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(validatedOutstanding[j])));
					outstanding.setTextColor(color.black);
					outstanding.setGravity(Gravity.CENTER);
					outstanding.setLayoutParams(contentRow_Params);
					outstanding.setPadding(10, 0, 10, 5);
					rightContentRow.addView(outstanding);

					mPA_EditText = new EditText(getActivity());
					mPA_EditText.setId(j);
					sPA_Fields.add(mPA_EditText);
					mPA_EditText.setInputType(InputType.TYPE_CLASS_NUMBER);
					mPA_EditText.setPadding(5, 5, 5, 5);
					mPA_EditText.setFilters(Get_EdiText_Filter.editText_filter());
					mPA_EditText.setBackgroundResource(R.drawable.edittext_background);
					mPA_EditText.setLayoutParams(rightContentParams);
					mPA_EditText.setWidth(150);
					mPA_EditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {

						@Override
						public void onFocusChange(View v, boolean hasFocus) {
							// TODO Auto-generated method stub
							if (hasFocus) {
								((EditText) v).setGravity(Gravity.LEFT);

								mMemberNameLayout.setVisibility(View.VISIBLE);
								mMemberName.setText(memberName_Text.getText().toString().trim());
								mMemberName.setTypeface(LoginActivity.sTypeface);
								TextviewUtils.manageBlinkEffect(mMemberName, getActivity());

							} else {
								((EditText) v).setGravity(Gravity.RIGHT);

								mMemberNameLayout.setVisibility(View.GONE);
								mMemberName.setText("");
							}

						}
					});
					rightContentRow.addView(mPA_EditText);

					mInterest_EditText = new EditText(getActivity());
					mInterest_EditText.setId(j);
					sInterest_Fields.add(mInterest_EditText);
					mInterest_EditText.setInputType(InputType.TYPE_CLASS_NUMBER);// |InputType.TYPE_NUMBER_FLAG_DECIMAL);
					mInterest_EditText.setPadding(5, 5, 5, 5);
					mInterest_EditText.setFilters(Get_EdiText_Filter.editText_filter());
					mInterest_EditText.setBackgroundResource(R.drawable.edittext_background);
					mInterest_EditText.setLayoutParams(rightContentParams);
					mInterest_EditText.setWidth(150);
					mInterest_EditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {

						@Override
						public void onFocusChange(View v, boolean hasFocus) {
							// TODO Auto-generated method stub
							if (hasFocus) {
								((EditText) v).setGravity(Gravity.LEFT);

								mMemberNameLayout.setVisibility(View.VISIBLE);
								mMemberName.setText(memberName_Text.getText().toString().trim());
								mMemberName.setTypeface(LoginActivity.sTypeface);
								TextviewUtils.manageBlinkEffect(mMemberName, getActivity());
							} else {
								((EditText) v).setGravity(Gravity.RIGHT);

								mMemberNameLayout.setVisibility(View.GONE);
								mMemberName.setText("");
							}
						}
					});
					rightContentRow.addView(mInterest_EditText);

					TextView repaymentWithInterest = new TextView(getActivity());
					repaymentWithInterest.setText(
							GetSpanText.getSpanString(getActivity(), String.valueOf(mRepaymentWithInterest[j])));
					repaymentWithInterest.setTextColor(color.black);
					repaymentWithInterest.setGravity(Gravity.CENTER);
					repaymentWithInterest.setLayoutParams(contentRow_Params);
					repaymentWithInterest.setPadding(10, 0, 10, 5);
					rightContentRow.addView(repaymentWithInterest);

					mRightContentTable.addView(rightContentRow);
				}

				mSubmit_Raised_Button = (Button) rootView.findViewById(R.id.fragment_Submit_button);
				mSubmit_Raised_Button.setText(RegionalConversion.getRegionalConversion(AppStrings.submit));
				mSubmit_Raised_Button.setTypeface(LoginActivity.sTypeface);
				mSubmit_Raised_Button.setOnClickListener(this);

				mPerviousButton = (Button) rootView.findViewById(R.id.fragment_Previousbutton);
				mPerviousButton.setText(RegionalConversion.getRegionalConversion(AppStrings.mPervious));
				mPerviousButton.setTypeface(LoginActivity.sTypeface);
				mPerviousButton.setOnClickListener(this);

				mNextButton = (Button) rootView.findViewById(R.id.fragment_Nextbutton);
				mNextButton.setText(RegionalConversion.getRegionalConversion(AppStrings.mNext));
				mNextButton.setTypeface(LoginActivity.sTypeface);
				mNextButton.setOnClickListener(this);

				if (EShaktiApplication.isDefault()) {
					mPerviousButton.setVisibility(View.INVISIBLE);

					if (mTotalOutstanding == 0) {
						mNextButton.setVisibility(View.VISIBLE);
					} else {

						mNextButton.setVisibility(View.INVISIBLE);
					}

				}

				resizeMemberNameWidth();
				resizeRightSideTable();
				resizeBodyTableRowHeight();

			} catch (ArrayIndexOutOfBoundsException e) {
				e.printStackTrace();

				TastyToast.makeText(getActivity(), AppStrings.adminAlert, TastyToast.LENGTH_SHORT, TastyToast.WARNING);

				getActivity().finish();
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return rootView;
	}

	private void resizeRightSideTable() {
		// TODO Auto-generated method stub
		int rightHeaderCount = (((TableRow) mRightHeaderTable.getChildAt(0)).getChildCount());
		for (int i = 0; i < rightHeaderCount; i++) {
			rightHeaderWidth[i] = viewWidth(((TableRow) mRightHeaderTable.getChildAt(0)).getChildAt(i));
			rightContentWidth[i] = viewWidth(((TableRow) mRightContentTable.getChildAt(0)).getChildAt(i));
		}
		for (int i = 0; i < rightHeaderCount; i++) {
			if (rightHeaderWidth[i] < rightContentWidth[i]) {
				((TableRow) mRightHeaderTable.getChildAt(0)).getChildAt(i)
						.getLayoutParams().width = rightContentWidth[i];
			} else {
				((TableRow) mRightContentTable.getChildAt(0)).getChildAt(i)
						.getLayoutParams().width = rightHeaderWidth[i];
			}
		}

	}

	private void resizeMemberNameWidth() {
		// TODO Auto-generated method stub
		int leftHeadertWidth = viewWidth(mLeftHeaderTable);
		int leftContentWidth = viewWidth(mLeftContentTable);

		if (leftHeadertWidth < leftContentWidth) {
			mLeftHeaderTable.getLayoutParams().width = leftContentWidth;
		} else {
			mLeftContentTable.getLayoutParams().width = leftHeadertWidth;
		}
	}

	private void resizeBodyTableRowHeight() {

		int leftContentTable_ChildCount = mLeftContentTable.getChildCount();

		for (int x = 0; x < leftContentTable_ChildCount; x++) {

			TableRow leftContentTableRow = (TableRow) mLeftContentTable.getChildAt(x);
			TableRow rightContentTableRow = (TableRow) mRightContentTable.getChildAt(x);

			int rowLeftHeight = viewHeight(leftContentTableRow);
			int rowRightHeight = viewHeight(rightContentTableRow);

			TableRow tableRow = rowLeftHeight < rowRightHeight ? leftContentTableRow : rightContentTableRow;
			int finalHeight = rowLeftHeight > rowRightHeight ? rowLeftHeight : rowRightHeight;

			this.matchLayoutHeight(tableRow, finalHeight);
		}

	}

	private void matchLayoutHeight(TableRow tableRow, int height) {

		int tableRowChildCount = tableRow.getChildCount();

		// if a TableRow has only 1 child
		if (tableRow.getChildCount() == 1) {

			View view = tableRow.getChildAt(0);
			TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();
			params.height = height - (params.bottomMargin + params.topMargin);

			return;
		}

		// if a TableRow has more than 1 child
		for (int x = 0; x < tableRowChildCount; x++) {

			View view = tableRow.getChildAt(x);

			TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();

			if (!isTheHeighestLayout(tableRow, x)) {
				params.height = height - (params.bottomMargin + params.topMargin);
				return;
			}
		}

	}

	// check if the view has the highest height in a TableRow
	private boolean isTheHeighestLayout(TableRow tableRow, int layoutPosition) {

		int tableRowChildCount = tableRow.getChildCount();
		int heighestViewPosition = -1;
		int viewHeight = 0;

		for (int x = 0; x < tableRowChildCount; x++) {
			View view = tableRow.getChildAt(x);
			int height = this.viewHeight(view);

			if (viewHeight < height) {
				heighestViewPosition = x;
				viewHeight = height;
			}
		}

		return heighestViewPosition == layoutPosition;
	}

	// read a view's height
	private int viewHeight(View view) {
		view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
		return view.getMeasuredHeight();
	}

	// read a view's width
	private int viewWidth(View view) {
		view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
		return view.getMeasuredWidth();
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {
		case R.id.fragment_Submit_button:

			try {

				sPA_total = 0;
				sInterest_total = 0;
				sSendToServer_MemLoanRepaid = Reset.reset(sSendToServer_MemLoanRepaid);

				sPrincipleAmounts = new String[sPA_Fields.size()];
				sInterestAmounts = new String[sInterest_Fields.size()];

				for (int i = 0; i < sPrincipleAmounts.length; i++) {

					// Principle Amount
					sPrincipleAmounts[i] = String.valueOf(sPA_Fields.get(i).getText());
					if ((sPrincipleAmounts[i].equals("")) || (sPrincipleAmounts[i] == null)) {
						sPrincipleAmounts[i] = nullAmount;
					}

					Log.d(TAG, "PA " + sPrincipleAmounts[i] + " >> POS " + i);

					// Interest
					sInterestAmounts[i] = String.valueOf(sInterest_Fields.get(i).getText());
					if ((sInterestAmounts[i].equals("")) || (sInterestAmounts[i] == null)) {
						sInterestAmounts[i] = nullAmount;
					}

					if (sPrincipleAmounts[i].matches("\\d*\\.?\\d+")) { // match
																		// a
																		// decimal
																		// number

						int amount = (int) Math.round(Double.parseDouble(sPrincipleAmounts[i]));
						sPrincipleAmounts[i] = String.valueOf(amount);
					}

					if (sInterestAmounts[i].matches("\\d*\\.?\\d+")) { // match
																		// a
																		// decimal
																		// number

						int amount = (int) Math.round(Double.parseDouble(sInterestAmounts[i]));
						sInterestAmounts[i] = String.valueOf(amount);
					}

					/** Outstanding >= PrincipleAmount **/
					System.out.println("OUTS : " + validatedOutstanding[i] + " OUTS POS : " + i);

					if ((validatedOutstanding[i]) >= Integer.parseInt(sPrincipleAmounts[i].trim())) {

						sSendToServer_MemLoanRepaid = sSendToServer_MemLoanRepaid
								+ String.valueOf(SelectedGroupsTask.member_Id.elementAt(i)) + "~"
								+ String.valueOf(sPrincipleAmounts[i]) + "~" + String.valueOf(sInterestAmounts[i])
								+ "~";

						/** Calculating Total **/
						sPA_total = sPA_total + Integer.parseInt(sPrincipleAmounts[i].trim());

						sInterest_total = sInterest_total + Integer.parseInt(sInterestAmounts[i].trim());

						outstandingAlert = true;
					} else {

						outstandingAlert = false;

						TastyToast.makeText(getActivity(), AppStrings.groupRepaidAlert, TastyToast.LENGTH_SHORT,
								TastyToast.WARNING);

						sSendToServer_MemLoanRepaid = Reset.reset(sSendToServer_MemLoanRepaid);
						sInterest_total = Integer.parseInt(nullAmount);
						sPA_total = Integer.parseInt(nullAmount);

						break;
					}

				}

				Log.d(TAG, "PA TOATL " + Integer.valueOf(sPA_total));

				Log.d(TAG, "INTEREST TOTAL " + Integer.valueOf(sInterest_total));

				System.out.println("sSendToServer_MemLoanRepaid >> " + sSendToServer_MemLoanRepaid);

				if ((sPA_total != 0) || (sInterest_total != 0)) {

					confirmationDialog = new Dialog(getActivity());

					LayoutInflater inflater = getActivity().getLayoutInflater();
					View dialogView = inflater.inflate(R.layout.dialog_confirmation, null);
					dialogView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
							LayoutParams.WRAP_CONTENT));

					TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
					confirmationHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.confirmation));
					confirmationHeader.setTypeface(LoginActivity.sTypeface);

					TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

					for (int i = 0; i < sPrincipleAmounts.length; i++) {

						TableRow indv_SavingsRow = new TableRow(getActivity());

						@SuppressWarnings("deprecation")
						TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
								LayoutParams.WRAP_CONTENT, 1f);
						contentParams.setMargins(10, 5, 10, 5);

						TextView memberName_Text = new TextView(getActivity());
						memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
								String.valueOf(SelectedGroupsTask.member_Name.elementAt(i))));
						memberName_Text.setTypeface(LoginActivity.sTypeface);
						memberName_Text.setTextColor(color.black);
						memberName_Text.setPadding(5, 5, 5, 5);
						memberName_Text.setLayoutParams(contentParams);
						indv_SavingsRow.addView(memberName_Text);

						TextView confirm_PAvalues = new TextView(getActivity());
						confirm_PAvalues.setText(
								GetSpanText.getSpanString(getActivity(), String.valueOf(sPrincipleAmounts[i])));
						confirm_PAvalues.setTextColor(color.black);
						confirm_PAvalues.setPadding(5, 5, 5, 5);
						confirm_PAvalues.setGravity(Gravity.RIGHT);
						confirm_PAvalues.setLayoutParams(contentParams);
						indv_SavingsRow.addView(confirm_PAvalues);

						TextView confirm_Intrvalues = new TextView(getActivity());
						confirm_Intrvalues
								.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sInterestAmounts[i])));
						confirm_Intrvalues.setTextColor(color.black);
						confirm_Intrvalues.setPadding(5, 5, 5, 5);
						confirm_Intrvalues.setGravity(Gravity.RIGHT);
						confirm_Intrvalues.setLayoutParams(contentParams);
						indv_SavingsRow.addView(confirm_Intrvalues);

						confirmationTable.addView(indv_SavingsRow,
								new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

					}
					View rullerView = new View(getActivity());
					rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
					rullerView.setBackgroundColor(Color.rgb(0, 199, 140));// rgb(255,
																			// 229,
																			// 242));
					confirmationTable.addView(rullerView);

					TableRow totalRow = new TableRow(getActivity());

					@SuppressWarnings("deprecation")
					TableRow.LayoutParams totalParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
							LayoutParams.WRAP_CONTENT, 1f);
					totalParams.setMargins(10, 5, 10, 5);

					TextView totalText = new TextView(getActivity());
					totalText.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.total)));
					totalText.setTypeface(LoginActivity.sTypeface);
					totalText.setTextColor(color.black);
					totalText.setPadding(5, 5, 5, 5);// (5, 10, 5, 10);
					totalText.setLayoutParams(totalParams);
					totalRow.addView(totalText);

					TextView total_Principle_Amount = new TextView(getActivity());
					total_Principle_Amount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sPA_total)));
					total_Principle_Amount.setTextColor(color.black);
					total_Principle_Amount.setPadding(5, 5, 5, 5);// (5, 10,
																	// 100, 10);
					total_Principle_Amount.setGravity(Gravity.RIGHT);
					total_Principle_Amount.setLayoutParams(totalParams);
					totalRow.addView(total_Principle_Amount);

					TextView total_Interest_Amount = new TextView(getActivity());
					total_Interest_Amount
							.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sInterest_total)));
					total_Interest_Amount.setTextColor(color.black);
					total_Interest_Amount.setPadding(5, 5, 5, 5);// (5, 10,
																	// 100, 10);
					total_Interest_Amount.setGravity(Gravity.RIGHT);
					total_Interest_Amount.setLayoutParams(totalParams);
					totalRow.addView(total_Interest_Amount);

					confirmationTable.addView(totalRow,
							new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

					mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit_button);
					mEdit_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.edit));
					mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
					mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
																			// 205,
																			// 0));
					mEdit_RaisedButton.setOnClickListener(this);

					mOk_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Ok_button);
					mOk_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.yes));
					mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
					mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
					mOk_RaisedButton.setOnClickListener(this);

					confirmationDialog.getWindow()
							.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
					confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					confirmationDialog.setCanceledOnTouchOutside(false);
					confirmationDialog.setContentView(dialogView);
					confirmationDialog.setCancelable(true);
					confirmationDialog.show();

					MarginLayoutParams margin = (MarginLayoutParams) dialogView.getLayoutParams();
					margin.leftMargin = 10;
					margin.rightMargin = 10;
					margin.topMargin = 10;
					margin.bottomMargin = 10;
					margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);

				} else {

					if (outstandingAlert) {

						// Reset the values
						sSendToServer_MemLoanRepaid = Reset.reset(sSendToServer_MemLoanRepaid);
						sPA_total = Integer.parseInt(nullAmount);
						sInterest_total = Integer.parseInt(nullAmount);

						TastyToast.makeText(getActivity(), AppStrings.nullAlert, TastyToast.LENGTH_SHORT,
								TastyToast.WARNING);

					}

				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			break;

		case R.id.fragment_Edit_button:

			sSendToServer_MemLoanRepaid = Reset.reset(sSendToServer_MemLoanRepaid);
			sPA_total = Integer.parseInt(nullAmount);
			sInterest_total = Integer.parseInt(nullAmount);
			isServiceCall = false;
			confirmationDialog.dismiss();

			break;

		case R.id.fragment_Ok_button:

			if (ConnectionUtils.isNetworkAvailable(getActivity())) {

				try {

					if (EShaktiApplication.getLoginFlag().equals(Constants.ONLINEFLAG)) {
						if (!isServiceCall) {
							isServiceCall = true;
							new GetMember_Loan_RepaidTask(Transaction_PersonalLoanRepaidFragment.this).execute();
						}
					} else {

						TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
								TastyToast.ERROR);
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			} else {
				// Do offline Stuffs
				if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {

					EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GET_TRANS_SINGLE,
							new GetTransactionSingle(PrefUtils.getOfflineUniqueID()));
				} else {

					TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
							TastyToast.ERROR);
				}
			}
			break;
		case R.id.fragment_Previousbutton:
			Transaction_SavingsFragment savingsFragment = new Transaction_SavingsFragment();
			setFragment(savingsFragment);
			break;
		case R.id.fragment_Nextbutton:

			if (ConnectionUtils.isNetworkAvailable(getActivity())) {
				if (PrefUtils.getGroupMasterResValues() != null && PrefUtils.getGroupMasterResValues().equals("1")) {

					System.out.println(
							"-----Offline Id ---- " + Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID);

					for (int i = 0; i < SelectedGroupsTask.loan_Id.size(); i++) {
						System.out.println("------------Id   =  " + SelectedGroupsTask.loan_Id.elementAt(i));
					}

					for (int i = 0; i < SelectedGroupsTask.loan_Id.size(); i++) {

						if (!SelectedGroupsTask.loan_Id.elementAt(i).equals("0")) {

							Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID = SelectedGroupsTask.loan_Id
									.elementAt(i);
							Transaction_MemLoanRepaid_MenuFragment.sSelected_LoanName = SelectedGroupsTask.loan_Name
									.elementAt(i);
							Log.e("Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID",
									Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID);

							Log.e("Loan Name", SelectedGroupsTask.loan_Name.elementAt(i) + "");
							Log.e("Acc No", SelectedGroupsTask.loanAcc_loanAccNo.elementAt(i) + "");
							Log.e("Loan Bank Name", SelectedGroupsTask.loanAcc_bankName.elementAt(i) + "");
							Log.e("Date", SelectedGroupsTask.loanAcc_loanDisbursementDate.elementAt(i) + "");

							EShaktiApplication.setMemberLoanRepaymentLoanBankName(
									SelectedGroupsTask.loanAcc_bankName.elementAt(i));
							EShaktiApplication
									.setMemberLoanRepaymentLoanAccNo(SelectedGroupsTask.loanAcc_loanAccNo.elementAt(i));

							EShaktiApplication.getInstance().getTransactionManager().startTransaction(
									DataType.GROUP_MASTER_MEMBERLOANOS,
									new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));
							i = SelectedGroupsTask.loan_Id.size();
							Log.e("MemLoan Before inc memLoanCount size &&&&", memLoanCount + "");
							memLoanCount = memLoanCount + 1;
							Log.e("MemLoan After inc memLoanCount size &&&&", memLoanCount + "");
						} else if (SelectedGroupsTask.loan_Id.elementAt(i).equals("0")) {
							Log.e("PersonalLoan Before inc memLoanCount size &&&&", memLoanCount + "");
							memLoanCount = memLoanCount + 1;
							Log.e("PersonalLoan After inc memLoanCount size &&&&", memLoanCount + "");
							Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID = "0";
							EShaktiApplication.getInstance().getTransactionManager().startTransaction(
									DataType.DEFAULT_PLDISOS,
									new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));

							System.out.println("--------else part execution");
						}

					}

					EShaktiApplication.setLoanId(Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID);
					EShaktiApplication.setLoanName(Transaction_MemLoanRepaid_MenuFragment.sSelected_LoanName);

				} else {

					for (int i = 0; i < SelectedGroupsTask.loan_Id.size(); i++) {

						if (!SelectedGroupsTask.loan_Id.elementAt(i).equals("0")) {

							Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID = SelectedGroupsTask.loan_Id
									.elementAt(i);
							Transaction_MemLoanRepaid_MenuFragment.sSelected_LoanName = SelectedGroupsTask.loan_Name
									.elementAt(i);
							Log.e("Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID",
									Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID);

							Log.e("Loan Name", SelectedGroupsTask.loan_Name.elementAt(i) + "");
							Log.e("Acc No", SelectedGroupsTask.loanAcc_loanAccNo.elementAt(i) + "");
							Log.e("Loan Bank Name", SelectedGroupsTask.loanAcc_bankName.elementAt(i) + "");
							Log.e("Date", SelectedGroupsTask.loanAcc_loanDisbursementDate.elementAt(i) + "");

							EShaktiApplication.setMemberLoanRepaymentLoanBankName(
									SelectedGroupsTask.loanAcc_bankName.elementAt(i));
							EShaktiApplication
									.setMemberLoanRepaymentLoanAccNo(SelectedGroupsTask.loanAcc_loanAccNo.elementAt(i));

							new Get_All_Mem_OutstandingTask(this).execute();
							i = SelectedGroupsTask.loan_Id.size();
							Log.e("MemLoan Before inc memLoanCount size &&&&", memLoanCount + "");
							memLoanCount = memLoanCount + 1;
							Log.e("MemLoan After inc memLoanCount size &&&&", memLoanCount + "");
						} else if (SelectedGroupsTask.loan_Id.elementAt(i).equals("0")) {
							Log.e("PersonalLoan Before inc memLoanCount size &&&&", memLoanCount + "");
							memLoanCount = memLoanCount + 1;
							Log.e("PersonalLoan After inc memLoanCount size &&&&", memLoanCount + "");
							Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID = "0";
							new Get_All_Mem_OutstandingTask(this).execute();

						}

					}

					EShaktiApplication.setLoanId(Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID);
					EShaktiApplication.setLoanName(Transaction_MemLoanRepaid_MenuFragment.sSelected_LoanName);
				}

			} else {

				System.out.println(
						"-----Offline Id ---- " + Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID);

				for (int i = 0; i < SelectedGroupsTask.loan_Id.size(); i++) {
					System.out.println("------------Id   =  " + SelectedGroupsTask.loan_Id.elementAt(i));
				}

				for (int i = 0; i < SelectedGroupsTask.loan_Id.size(); i++) {

					if (!SelectedGroupsTask.loan_Id.elementAt(i).equals("0")) {

						Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID = SelectedGroupsTask.loan_Id
								.elementAt(i);
						Transaction_MemLoanRepaid_MenuFragment.sSelected_LoanName = SelectedGroupsTask.loan_Name
								.elementAt(i);
						Log.e("Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID",
								Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID);

						Log.e("Loan Name", SelectedGroupsTask.loan_Name.elementAt(i) + "");
						Log.e("Acc No", SelectedGroupsTask.loanAcc_loanAccNo.elementAt(i) + "");
						Log.e("Loan Bank Name", SelectedGroupsTask.loanAcc_bankName.elementAt(i) + "");
						Log.e("Date", SelectedGroupsTask.loanAcc_loanDisbursementDate.elementAt(i) + "");

						EShaktiApplication
								.setMemberLoanRepaymentLoanBankName(SelectedGroupsTask.loanAcc_bankName.elementAt(i));
						EShaktiApplication
								.setMemberLoanRepaymentLoanAccNo(SelectedGroupsTask.loanAcc_loanAccNo.elementAt(i));

						EShaktiApplication.getInstance().getTransactionManager().startTransaction(
								DataType.GROUP_MASTER_MEMBERLOANOS,
								new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));
						i = SelectedGroupsTask.loan_Id.size();
						Log.e("MemLoan Before inc memLoanCount size &&&&", memLoanCount + "");
						memLoanCount = memLoanCount + 1;
						Log.e("MemLoan After inc memLoanCount size &&&&", memLoanCount + "");
					} else if (SelectedGroupsTask.loan_Id.elementAt(i).equals("0")) {
						Log.e("PersonalLoan Before inc memLoanCount size &&&&", memLoanCount + "");
						memLoanCount = memLoanCount + 1;
						Log.e("PersonalLoan After inc memLoanCount size &&&&", memLoanCount + "");
						Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID = "0";
						EShaktiApplication.getInstance().getTransactionManager().startTransaction(
								DataType.DEFAULT_PLDISOS,
								new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));

						System.out.println("--------else part execution");
					}

				}

				EShaktiApplication.setLoanId(Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID);
				EShaktiApplication.setLoanName(Transaction_MemLoanRepaid_MenuFragment.sSelected_LoanName);
			}
			isDefaultService = true;

			// isNextButton = true;
			break;
		default:
			break;
		}

	}

	@Subscribe
	public void OnGetSingleTransactionPersonalLos(final GetSingleTransResponse getSingleTransResponse) {
		switch (getSingleTransResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), getSingleTransResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), getSingleTransResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			try {
				String mUniqueId = GetTransactionSinglevalues.getUniqueId();
				String mPersonalrepayValues = GetTransactionSinglevalues.getPersonalloanRepayment();

				mLastTrDate = DatePickerDialog.sDashboardDate;
				mLastTr_ID = SelectedGroupsTask.sTr_ID.toString();
				String mCurrentTransDate = null;

				if (publicValues.mOffline_Trans_CurrentDate != null) {
					mCurrentTransDate = publicValues.mOffline_Trans_CurrentDate;
				}

				String mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
				String mMobileDate = Get_Offline_MobileDate.getOffline_MobileDate(getActivity());
				String mTransaction_UniqueId = PrefUtils.getOfflineUniqueID();
				String mPersonalLoanrepaid = sSendToServer_MemLoanRepaid + "#"
						+ Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID + "#" + mTrasactiondate + "#"
						+ mMobileDate;

				Log.v(TAG, "Step 2");
				Log.e("Personal Loan Repaid ment&&&&&&", mPersonalLoanrepaid);
				if (!mPersonalLoanrepaid.contains("?") && !mCurrentTransDate.contains("?")
						&& !mTransaction_UniqueId.contains("?")) {

					if (mUniqueId == null && mPersonalrepayValues == null) {

						Log.v(TAG, "Step 3");
						mSqliteDBStoredValues_InternalLoanValues = mPersonalLoanrepaid;
						if (mLastTrDate != null && mMobileDate != null && mTransaction_UniqueId != null
								&& mCurrentTransDate != null) {
							EShaktiApplication.getInstance().getTransactionManager().startTransaction(
									DataType.TRANSACTIONADD,
									new Transaction(0, PrefUtils.getUsernameKey(), SelectedGroupsTask.UserName,
											SelectedGroupsTask.Ngo_Id, SelectedGroupsTask.Group_Id,
											mTransaction_UniqueId, mLastTr_ID, mCurrentTransDate, null, null,
											mPersonalLoanrepaid, null, null, null, null, null, null, null, null, null,
											null, null, null, null, null, null, null, null, null, null));
						}
					} else if (mUniqueId.equalsIgnoreCase(PrefUtils.getOfflineUniqueID())
							&& mPersonalrepayValues == null) {
						Log.v(TAG, "Step 4");
						EShaktiApplication.setSetTransValues(TransactionOffConstants.TRANS_PLRP);

						EShaktiApplication.getInstance().getTransactionManager().startTransaction(
								DataType.TRANS_VALUES_UPDATE, new TransactionUpdate(mUniqueId, mPersonalLoanrepaid));

					} else if (mPersonalrepayValues != null) {

						confirmationDialog.dismiss();

						TastyToast.makeText(getActivity(), AppStrings.offlineDataAvailable, TastyToast.LENGTH_SHORT,
								TastyToast.WARNING);

						DatePickerDialog.sDashboardDate = SelectedGroupsTask.sLastTransactionDate_Response;
						MainFragment_Dashboard fragment = new MainFragment_Dashboard();
						setFragment(fragment);

					}
				} else {
					if (confirmationDialog.isShowing() && confirmationDialog != null) {
						confirmationDialog.dismiss();
					}

					TastyToast.makeText(getActivity(), "Please Try Again", TastyToast.LENGTH_SHORT, TastyToast.ERROR);

					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		}
	}

	@Subscribe
	public void onAddTransactionPersonalLRepay(final TransactionResponse transactionResponse) {
		switch (transactionResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), transactionResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), transactionResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			try {
				TransactionProvider.getSinlgeGroupMaster_Transaction(
						new Transaction_UniqueIdSingle(PrefUtils.getOfflineUniqueID()));

				if (GetTransactionSinglevalues.getPersonalloanRepayment() != null

						&& !GetTransactionSinglevalues.getPersonalloanRepayment().equals("")) {
					StringBuilder builder = new StringBuilder();
					String mMasterresponse, mPLMemberId, mPLOS, result, mPLOSrepayment;
					for (int i = 0; i < sPrincipleAmounts.length; i++) {

						if (validatedOutstanding[i] >= Integer.parseInt(sPrincipleAmounts[i].trim())) {

							validatedOutstanding[i] = validatedOutstanding[i] - Integer.parseInt(sPrincipleAmounts[i]);
							System.out.println("Current OutStanding" + mOutstanding[i]);

							mPLMemberId = String.valueOf(SelectedGroupsTask.member_Id.elementAt(i));

							mPLOS = String.valueOf(validatedOutstanding[i] + ".00");
							mPLOSrepayment = mRepaymentWithInterest[i];
							mMasterresponse = mPLMemberId + "~" + mPLOS + "~" + mPLOSrepayment + "~";

							builder.append(mMasterresponse);

						}
					}
					result = builder.toString();
					sSendToServer_MemLoanRepaid = Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID + "#"
							+ result;

					Log.v("Total Bind Values", sSendToServer_MemLoanRepaid);
					String mCashinHand = String
							.valueOf(Integer.parseInt(SelectedGroupsTask.sCashinHand) + sPA_total + sInterest_total);

					String mCashatBank = SelectedGroupsTask.sCashatBank;
					String mBankDetails = GetOfflineTransactionValues.getBankDetails();
					SelectedGroupsTask.sCashinHand = mCashinHand;
					String mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
					SelectedGroupsTask.sLastTransactionDate_Response = mTrasactiondate;

					String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mTrasactiondate,
							mCashinHand, mCashatBank, mBankDetails);
					String mSelectedGroupId = SelectedGroupsTask.Group_Id;
					EShaktiApplication.setPLOS(true);
					EShaktiApplication.getInstance().getTransactionManager().startTransaction(
							DataType.GROUP_MASTER_PLOS,
							new GroupMLUpdate(mSelectedGroupId, mGroupMasterResponse, sSendToServer_MemLoanRepaid));

					Log.e(TAG, "Sucess" + SelectedGroupsTask.sCashinHand + "Cash In Hand are equals" + mCashinHand);

				} else {
					if (confirmationDialog.isShowing() && confirmationDialog != null) {
						confirmationDialog.dismiss();
					}

					TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT, TastyToast.ERROR);

					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);

				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		}
	}

	@Subscribe
	public void OnTransUpdatePersonalLos(final TransactionUpdateResponse transactionUpdateResponse) {
		switch (transactionUpdateResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), transactionUpdateResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), transactionUpdateResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:
			Log.e(TAG, "Updated !@#$#$#$");

			try {
				TransactionProvider.getSinlgeGroupMaster_Transaction(
						new Transaction_UniqueIdSingle(PrefUtils.getOfflineUniqueID()));

				if (GetTransactionSinglevalues.getPersonalloanRepayment() != null

						&& !GetTransactionSinglevalues.getPersonalloanRepayment().equals("")) {
					confirmationDialog.dismiss();

					StringBuilder builder = new StringBuilder();
					String mMasterresponse, mPLMemberId, mPLOS, result, mPLOSrepayment;
					for (int i = 0; i < sPrincipleAmounts.length; i++) {

						if (validatedOutstanding[i] >= Integer.parseInt(sPrincipleAmounts[i].trim())) {

							validatedOutstanding[i] = validatedOutstanding[i] - Integer.parseInt(sPrincipleAmounts[i]);
							System.out.println("Current OutStanding" + mOutstanding[i]);

							mPLMemberId = String.valueOf(SelectedGroupsTask.member_Id.elementAt(i));

							mPLOS = String.valueOf(validatedOutstanding[i] + ".00");
							mPLOSrepayment = mRepaymentWithInterest[i];
							mMasterresponse = mPLMemberId + "~" + mPLOS + "~" + mPLOSrepayment + "~";

							builder.append(mMasterresponse);

						}
					}
					result = builder.toString();
					sSendToServer_MemLoanRepaid = Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID + "#"
							+ result;

					String mCashinHand = String
							.valueOf(Integer.parseInt(SelectedGroupsTask.sCashinHand) + sPA_total + sInterest_total);

					String mCashatBank = SelectedGroupsTask.sCashatBank;
					String mBankDetails = GetOfflineTransactionValues.getBankDetails();
					SelectedGroupsTask.sCashinHand = mCashinHand;
					String mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
					SelectedGroupsTask.sLastTransactionDate_Response = mTrasactiondate;
					String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mTrasactiondate,
							mCashinHand, mCashatBank, mBankDetails);
					String mSelectedGroupId = SelectedGroupsTask.Group_Id;
					EShaktiApplication.setPLOS(true);

					EShaktiApplication.getInstance().getTransactionManager().startTransaction(
							DataType.GROUP_MASTER_PLOS,
							new GroupMLUpdate(mSelectedGroupId, mGroupMasterResponse, sSendToServer_MemLoanRepaid));

				} else {
					if (confirmationDialog.isShowing() && confirmationDialog != null) {
						confirmationDialog.dismiss();
					}

					TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT, TastyToast.ERROR);

					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);

				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		}
	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub
		mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
		mProgressDilaog.show();
	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub

		if (mProgressDilaog != null) {
			mProgressDilaog.dismiss();
			if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {
				getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub

						if (!result.equals("FAIL")) {
							isServiceCall = false;
							TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg,
									TastyToast.LENGTH_SHORT, TastyToast.ERROR);
							Constants.NETWORKCOMMONFLAG = "SUCCESS";
						}

					}
				});

			} else {
				try {
					if (isDefaultService) {
						isDefaultService = false;
						if (Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID.equals("0")) {
							isDefaultService_LoanPurpose = true;
							new Get_PurposeOfLoanTask(this).execute();

						} else {

							Transaction_MemLoanRepaidFragment memberLoanRepaidFragment = new Transaction_MemLoanRepaidFragment();
							setFragment(memberLoanRepaidFragment);

							int mFrag_InternalLoan = 0;
							mFrag_InternalLoan = sInterest_total + sPA_total;
							EShaktiApplication.setStepwiseInternalloan(mFrag_InternalLoan);

						}

					} else if (isDefaultService_LoanPurpose) {
						isDefaultService_LoanPurpose = false;
						Transaction_InternalLoan_DisbursementFragment savingsFragment = new Transaction_InternalLoan_DisbursementFragment();

						int mFrag_InternalLoan = 0;
						mFrag_InternalLoan = sInterest_total + sPA_total;
						EShaktiApplication.setStepwiseInternalloan(mFrag_InternalLoan);
						setFragment(savingsFragment);

					} else if (isGetTrid) {
						if (!isNextButton) {
							callOfflineDataUpdate();
							isNextButton = false;
						}
						isGetTrid = false;
					} else {
						if (GetResponseInfo.sResponseUpdate[0].equals("Yes")) {
						} else {
							TastyToast.makeText(getActivity(), AppStrings.transactionFailAlert, TastyToast.LENGTH_SHORT,
									TastyToast.ERROR);
							MainFragment_Dashboard fragment = new MainFragment_Dashboard();
							setFragment(fragment);
						}
						confirmationDialog.dismiss();
						new Get_LastTransactionID(Transaction_PersonalLoanRepaidFragment.this).execute();
						isGetTrid = true;

						// }
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Subscribe
	public void OnGroupPLOSUpdate(final GroupPLOS_Update_Response groupPLOSUpdateResponse) {
		switch (groupPLOSUpdateResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), groupPLOSUpdateResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), groupPLOSUpdateResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:
			System.out.println("---------OnGroupPLOSUpdate");

			try {
				confirmationDialog.dismiss();

				TastyToast.makeText(getActivity(), AppStrings.transactionCompleted, TastyToast.LENGTH_SHORT,
						TastyToast.SUCCESS);
				if (ConnectionUtils.isNetworkAvailable(getActivity())) {
					if (EShaktiApplication.isDefault()) {
						isDefaultService = true;
						for (int i = 0; i < SelectedGroupsTask.loan_Id.size(); i++) {

							if (!SelectedGroupsTask.loan_Id.elementAt(i).equals("0")) {
								Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID = SelectedGroupsTask.loan_Id
										.elementAt(i);
								Transaction_MemLoanRepaid_MenuFragment.sSelected_LoanName = SelectedGroupsTask.loan_Name
										.elementAt(i);
								Log.e("Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID",
										Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID);

								EShaktiApplication.setMemberLoanRepaymentLoanBankName(
										SelectedGroupsTask.loanAcc_bankName.elementAt(i));
								EShaktiApplication.setMemberLoanRepaymentLoanAccNo(
										SelectedGroupsTask.loanAcc_loanAccNo.elementAt(i));

								new Get_All_Mem_OutstandingTask(this).execute();
								i = SelectedGroupsTask.loan_Id.size();
								memLoanCount = memLoanCount + 1;
								Log.e("Count size &&&&", memLoanCount + "");
							} else if (SelectedGroupsTask.loan_Id.elementAt(i).equals("0")) {
								memLoanCount = memLoanCount + 1;
								Log.e("Count size", memLoanCount + "");
								Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID = "0";
								new Get_All_Mem_OutstandingTask(this).execute();

							}
						}

						EShaktiApplication.setLoanId(Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID);
						EShaktiApplication.setLoanName(Transaction_MemLoanRepaid_MenuFragment.sSelected_LoanName);

					} else {
						MainFragment_Dashboard fragment = new MainFragment_Dashboard();
						setFragment(fragment);
					}
				} else {

					if (EShaktiApplication.isDefault()) {

						for (int i = 0; i < SelectedGroupsTask.loan_Id.size(); i++) {

							if (!SelectedGroupsTask.loan_Id.elementAt(i).equals("0")) {
								Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID = SelectedGroupsTask.loan_Id
										.elementAt(i);
								Transaction_MemLoanRepaid_MenuFragment.sSelected_LoanName = SelectedGroupsTask.loan_Name
										.elementAt(i);
								Log.e("Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID",
										Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID);

								EShaktiApplication.setMemberLoanRepaymentLoanBankName(
										SelectedGroupsTask.loanAcc_bankName.elementAt(i));
								EShaktiApplication.setMemberLoanRepaymentLoanAccNo(
										SelectedGroupsTask.loanAcc_loanAccNo.elementAt(i));

								EShaktiApplication.getInstance().getTransactionManager().startTransaction(
										DataType.GROUP_MASTER_MEMBERLOANOS,
										new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));
								i = SelectedGroupsTask.loan_Id.size();
								memLoanCount = memLoanCount + 1;
								Log.e("Count size &&&&", memLoanCount + "");
							} else if (SelectedGroupsTask.loan_Id.elementAt(i).equals("0")) {
								memLoanCount = memLoanCount + 1;
								Log.e("Count size", memLoanCount + "");
								Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID = "0";
								EShaktiApplication.getInstance().getTransactionManager().startTransaction(
										DataType.DEFAULT_PLDISOS,
										new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));
							}
						}

						EShaktiApplication.setLoanId(Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID);
						EShaktiApplication.setLoanName(Transaction_MemLoanRepaid_MenuFragment.sSelected_LoanName);

					} else {

						if (!ConnectionUtils.isNetworkAvailable(getActivity())) {

							String mValues = Put_DB_GroupNameDetail_Response
									.put_DB_GroupNameDetails_Response(EShaktiApplication.getGroupResponse());

							GroupProvider.updateGroupResponse(new GroupResponseUpdate(
									EShaktiApplication.getGroupId_GroupLastTransDate(), mValues));
						}

						MainFragment_Dashboard fragment = new MainFragment_Dashboard();
						setFragment(fragment);
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		}
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		SBus.INST.unRegister(this);
	}

	private void setFragment(Fragment fragment) {

		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

		FragmentDrawer.drawer_CashinHand.setText(
				RegionalConversion.getRegionalConversion(AppStrings.cashinhand) + SelectedGroupsTask.sCashinHand);
		FragmentDrawer.drawer_CashinHand.setTypeface(LoginActivity.sTypeface);

		FragmentDrawer.drawer_CashatBank.setText(
				RegionalConversion.getRegionalConversion(AppStrings.cashatBank) + SelectedGroupsTask.sCashatBank);
		FragmentDrawer.drawer_CashatBank.setTypeface(LoginActivity.sTypeface);

		FragmentDrawer.drawer_lastTR_Date
				.setText(RegionalConversion.getRegionalConversion(AppStrings.lastTransactionDate) + " : "
						+ SelectedGroupsTask.sLastTransactionDate_Response);
		FragmentDrawer.drawer_lastTR_Date.setTypeface(LoginActivity.sTypeface);
		
		UpdateCalendarMinMaxDateUtils.OnUpdateCalendarDate();
		
		Calendar calender = Calendar.getInstance();

		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String formattedDate = df.format(calender.getTime());
		EShaktiApplication.setLastTransDate_GroupId(SelectedGroupsTask.Group_Id);

		new GroupTempDBLastTransDate(EShaktiApplication.getLastTransDate_GroupId(),
				SelectedGroupsTask.sLastTransactionDate_Response, formattedDate);

		GroupProvider.updateGroupLastTransDateResponse();
		
	}

	@Subscribe
	public void OnGroupMasterPLDBResponse(final DefaultOffline_PL_DisResponse groupMasterPLDBResponse) {
		switch (groupMasterPLDBResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), groupMasterPLDBResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), groupMasterPLDBResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			try {
				System.out.println("------------OnGroupMasterPLDBResponse");
				MainFragment_Dashboard.mPLDBResponse = GetGroupMemberDetails.getPersonaloanOS();
				MainFragment_Dashboard.mPLDBPurposeofloan = GetGroupMemberDetails.getPurposeofloan();

				Transaction_InternalLoan_DisbursementFragment savingsFragment = new Transaction_InternalLoan_DisbursementFragment();

				int mFrag_InternalLoan = 0;
				mFrag_InternalLoan = sInterest_total + sPA_total;
				EShaktiApplication.setStepwiseInternalloan(mFrag_InternalLoan);
				setFragment(savingsFragment);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		}

	}

	private void callOfflineDataUpdate() {
		// TODO Auto-generated method stub
		StringBuilder builder = new StringBuilder();
		String mMasterresponse, mPLMemberId, mPLOS, result, mPLOSrepayment;
		for (int i = 0; i < sPrincipleAmounts.length; i++) {

			if (validatedOutstanding[i] >= Integer.parseInt(sPrincipleAmounts[i].trim())) {

				validatedOutstanding[i] = validatedOutstanding[i] - Integer.parseInt(sPrincipleAmounts[i]);
				System.out.println("Current OutStanding" + mOutstanding[i]);

				mPLMemberId = String.valueOf(SelectedGroupsTask.member_Id.elementAt(i));

				mPLOS = String.valueOf(validatedOutstanding[i] + ".00");
				mPLOSrepayment = mRepaymentWithInterest[i];
				mMasterresponse = mPLMemberId + "~" + mPLOS + "~" + mPLOSrepayment + "~";

				builder.append(mMasterresponse);

			}
		}
		result = builder.toString();
		sSendToServer_MemLoanRepaid = Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID + "#" + result;

		Log.v("Total Bind Values", sSendToServer_MemLoanRepaid);
		String mCashinHand = SelectedGroupsTask.sCashinHand;

		String mCashatBank = SelectedGroupsTask.sCashatBank;
		String mBankDetails = GetOfflineTransactionValues.getBankDetails();
		mLastTrDate = SelectedGroupsTask.sLastTransactionDate_Response;

		String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mLastTrDate, mCashinHand, mCashatBank,
				mBankDetails);
		String mSelectedGroupId = SelectedGroupsTask.Group_Id;
		EShaktiApplication.setPLOS(true);
		isServiceCall = false;
		EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GROUP_MASTER_PLOS,
				new GroupMLUpdate(mSelectedGroupId, mGroupMasterResponse, sSendToServer_MemLoanRepaid));

	}

	@Subscribe
	public void OnGroupMasterResponse(final GroupMemberloanOSResponse groupMasterResponse) {
		switch (groupMasterResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), groupMasterResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), groupMasterResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:
			System.out.println("---------OnGroupMasterResponse");
			try {
				String mMemberLoanResponse = GetGroupMemberDetails.getMemberloanOS();

				Log.e("Valuessssssssssssssssssssssss", mMemberLoanResponse);
				System.out.println("--------------------------------->>>>>>>>" + mMemberLoanResponse);
				Log.e("OnGroupMasterResponse ===  ", mMemberLoanResponse.toString());
				if (mMemberLoanResponse != null && !mMemberLoanResponse.equals("")) {
					response = mMemberLoanResponse.split("%");

					for (int i = 0; i < response.length; i++) {
						System.out.println("Response  ****: " + response[i]);

						String secondResponse[] = response[i].split("#");
						System.out.println("LOAN ID : " + secondResponse[0]);
						if (Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID.equals(secondResponse[0])) {
							System.out.println("LOAN OUTS : " + secondResponse[1]);
							Transaction_MemLoanRepaid_MenuFragment.mMemberOS_Offlineresponse = secondResponse[1];
						}

					}

					int mFrag_InternalLoan = 0;
					mFrag_InternalLoan = sInterest_total + sPA_total;
					EShaktiApplication.setStepwiseInternalloan(mFrag_InternalLoan);

					Transaction_MemLoanRepaid_MenuFragment.response = response;

					PrefUtils.setStepWiseScreenCount("3");

					Transaction_MemLoanRepaidFragment fragment = new Transaction_MemLoanRepaidFragment();
					setFragment(fragment);
				} else {
					TastyToast.makeText(getActivity(), AppStrings.loginAgainAlert, TastyToast.LENGTH_LONG,
							TastyToast.WARNING);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		}

	}

}
