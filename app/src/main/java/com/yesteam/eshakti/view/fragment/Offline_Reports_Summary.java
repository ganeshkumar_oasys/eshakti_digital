package com.yesteam.eshakti.view.fragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.squareup.otto.Subscribe;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.adapter.CustomItemAdapter;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.model.RowItem;
import com.yesteam.eshakti.sqlite.db.model.GetGroupMemberDetails;
import com.yesteam.eshakti.sqlite.db.model.Transaction;
import com.yesteam.eshakti.sqlite.db.transactions.TransactionManager.DataType;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Offline_Reports_Summary extends Fragment {

	private TextView mGroupName, mCashInHand, mCashAtBank, mHeadertext;
	private TableLayout tableLayout, headerTable;
	private TableRow headerRow;
	ListView mListView;
	List<RowItem> rowItems;
	CustomItemAdapter mAdapter;

	String getResponse;
	String amount;
	String interest;
	private static String[] responseArr, response$Arr;
	private static String[] amountsArr;
	private static String[] interestArr;
	public static int[] sumAmount;
	public static int[] sumInterest;
	public static String[] minutesId, minutesName, responseId;
	String[] listItems;
	private String[] dateArr;
	private String transDate;
	private String mLanguagelocale = "";
	private String[] sDepositItems;
	private int otheIncomeAmount = 0;
	String selectedType;

	public Offline_Reports_Summary() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		sDepositItems = new String[]{
				RegionalConversion.getRegionalConversion(AppStrings.bankDeposit),
				RegionalConversion.getRegionalConversion(AppStrings.mCashDeposit),
				RegionalConversion.getRegionalConversion(AppStrings.withdrawl),
				RegionalConversion.getRegionalConversion(AppStrings.mBankCharges),
				RegionalConversion.getRegionalConversion(AppStrings.interestSubvention)
				
		};
		
		Transaction_ExpensesFragment.sExpensesName = new String[] { 
				/*RegionalConversion.getRegionalConversion(AppStrings.transport),
				RegionalConversion.getRegionalConversion(AppStrings.snacks),
				RegionalConversion.getRegionalConversion(AppStrings.telephone),
				RegionalConversion.getRegionalConversion(AppStrings.meeting),
				RegionalConversion.getRegionalConversion(AppStrings.stationary),
				RegionalConversion.getRegionalConversion(AppStrings.federationincome),
				RegionalConversion.getRegionalConversion(AppStrings.otherExpenses)*/
				
				RegionalConversion.getRegionalConversion(AppStrings.stationary),
				RegionalConversion.getRegionalConversion(AppStrings.federationincome),
				RegionalConversion.getRegionalConversion(AppStrings.mExpense_meeting),
				RegionalConversion.getRegionalConversion(AppStrings.otherExpenses)};

		Transaction_FixedDepositEntryFragment.sFixedDepositItem = new String[] {
				/*RegionalConversion.getRegionalConversion(AppStrings.bankTransaction),
				*/ 
				RegionalConversion.getRegionalConversion(AppStrings.bankDeposit),
				RegionalConversion.getRegionalConversion(AppStrings.bankInterest),
				RegionalConversion.getRegionalConversion(AppStrings.withdrawl),
				RegionalConversion.getRegionalConversion(AppStrings.bankExpenses)};

		Transaction_GroupLoanRepaidFragment.sGroupLoanName = new String[] {
				RegionalConversion.getRegionalConversion(AppStrings.bankInterest),
				RegionalConversion.getRegionalConversion(AppStrings.bankExpenses),
				RegionalConversion.getRegionalConversion(AppStrings.bankrepayment),
				RegionalConversion.getRegionalConversion(AppStrings.interestSubvention) };

		mLanguagelocale = PrefUtils.getUserlangcode();
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		SBus.INST.register(this);
	}

	@Override
	public void onAttach(Context context) {
		// TODO Auto-generated method stub
		super.onAttach(context);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.offline_summary, container, false);

		MainFragment_Dashboard.isBackpressed = false;

		try {
			EShaktiApplication.setOfflineTransDate(false);
			EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GET_TRANS_MASTERVALUES,
					null);
		/*	EShaktiApplication.getInstance().getTransactionManager().startTransaction(
					DataType.GET_OFFLINE_REPORTS_MINUTESOFMEETING,
					new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));*/

			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashInHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashInHand.setTypeface(LoginActivity.sTypeface);

			mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashAtBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashAtBank.setTypeface(LoginActivity.sTypeface);

			mHeadertext = (TextView) rootView.findViewById(R.id.offline_headertext);
			mHeadertext.setTypeface(LoginActivity.sTypeface);

			headerTable = (TableLayout) rootView.findViewById(R.id.summaryHeaderTable);
			tableLayout = (TableLayout) rootView.findViewById(R.id.summaryTable);

			headerRow = new TableRow(getActivity());
			headerRow.setBackgroundResource(R.color.tableHeader);
			TableRow.LayoutParams headerParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);
			headerRow.setLayoutParams(headerParams);

			mListView = (ListView) rootView.findViewById(R.id.offlineListView);
			rowItems = new ArrayList<RowItem>();

			sumAmount = new int[SelectedGroupsTask.member_Name.size()];
			sumInterest = new int[SelectedGroupsTask.member_Name.size()];

			String mMasterMinutes = GetGroupMemberDetails.getMasterminutes();
			System.out.println("-----------mMasterMinutes----------" + mMasterMinutes.toString());
			String[] masrterMinutesResponse = mMasterMinutes.split("~");
			// String[] minutesId,minutesName;
			String minutes_id = "", minutes_name = "";
			for (int i = 0; i < masrterMinutesResponse.length; i = i + 2) {
				minutes_id = minutes_id + masrterMinutesResponse[i] + "~";
				minutes_name = minutes_name + masrterMinutesResponse[i + 1] + "~";
			}

			minutesId = minutes_id.split("~");
			minutesName = minutes_name.split("~");

			responseId = new String[minutesId.length];
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return rootView;
	}

	@Subscribe
	public void OnValues(final ArrayList<Transaction> arrayList) {
		Log.e("CHECKING !23", "YES DONE4545");
		if (arrayList.size() != 0) {
			Log.v("Current Array List Size", arrayList.size() + "");
			try {
				TableRow.LayoutParams headerParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
						LayoutParams.WRAP_CONTENT, 1f);

				if (OfflineReports_TransactionList.selectedItem.equals(AppStrings.savings)) {
					for (int i = 0; i < arrayList.size(); i++) {
						getResponse = "";
						amount = "";
						if (Offline_ReportDateListFragment.sSelectedTransDate
								.equals(arrayList.get(i).getTransactionDate())) {

							if (Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.savings)) {
								if (arrayList.get(i).getSavings() != null) {
									getResponse = arrayList.get(i).getSavings();
									System.out.println("<<<<<getResponse>>>>>>>>>" + getResponse.toString());
									responseArr = getResponse.split("[~#]");

									dateArr = String.valueOf(responseArr[responseArr.length - 2]).split("/");
									transDate = dateArr[1] + "/" + dateArr[0] + "/" + dateArr[2];
									if (Offline_TransactionDateFragment.sSelectedTransDate.equals(transDate)) {
										for (int j = 0; j < responseArr.length - 3; j++) {
											if (j % 2 != 0) {
												amount = amount + responseArr[j] + "~";
											}
										}
										System.out.println("<<-----amt--------->>" + amount.toString());
										amountsArr = amount.split("~");

										for (int k = 0; k < amountsArr.length; k++) {

											System.out.println(" Amount :" + amountsArr[k] + " pos :" + k);
											sumAmount[k] += Integer.parseInt(amountsArr[k]);
											Log.v("SUM AMOUNT", sumAmount[k] + "");
										}
									}
								}

							} else if (Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.voluntarySavings)) {
								if (arrayList.get(i).getVolunteerSavings() != null) {
									getResponse = arrayList.get(i).getVolunteerSavings();
									System.out.println("<<<<<getResponse>>>>>>>>>" + getResponse.toString());
									responseArr = getResponse.split("[~#]");

									dateArr = String.valueOf(responseArr[responseArr.length - 2]).split("/");
									transDate = dateArr[1] + "/" + dateArr[0] + "/" + dateArr[2];
									if (Offline_TransactionDateFragment.sSelectedTransDate.equals(transDate)) {
										for (int j = 0; j < responseArr.length - 3; j++) {
											if (j % 2 != 0) {
												amount = amount + responseArr[j] + "~";
											}
										}
										System.out.println("<<-----amt--------->>" + amount.toString());
										amountsArr = amount.split("~");

										for (int k = 0; k < amountsArr.length; k++) {

											System.out.println(" Amount :" + amountsArr[k] + " pos :" + k);
											sumAmount[k] += Integer.parseInt(amountsArr[k]);
											Log.v("SUM AMOUNT", sumAmount[k] + "");
										}

									}
								}

							}
						}
					}
					if (mLanguagelocale.equalsIgnoreCase("English")) {
						mHeadertext.setText(RegionalConversion.getRegionalConversion(AppStrings.reports)
								+ RegionalConversion.getRegionalConversion(AppStrings.of)
								+ RegionalConversion.getRegionalConversion(Offline_SubMenuList.selectedSubMenuItem)
								+ " ON " + Offline_TransactionDateFragment.sSelectedTransDate);
					} else {
						mHeadertext.setText(Offline_TransactionDateFragment.sSelectedTransDate
								+ RegionalConversion.getRegionalConversion(AppStrings.of)
								+ RegionalConversion.getRegionalConversion(Offline_SubMenuList.selectedSubMenuItem)
								+ " " + RegionalConversion.getRegionalConversion(AppStrings.reports));
					}
					
					

					TextView memName = new TextView(getActivity());
					memName.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.memberName)));
					memName.setTypeface(LoginActivity.sTypeface);
					memName.setPadding(60, 5, 10, 5);
					memName.setTextColor(Color.WHITE);
					memName.setLayoutParams(headerParams);
					headerRow.addView(memName);

					TextView amount = new TextView(getActivity());
					amount.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.amount)));
					amount.setTypeface(LoginActivity.sTypeface);
					amount.setTextColor(Color.WHITE);
					amount.setPadding(10, 5, 60, 5);
					amount.setGravity(Gravity.RIGHT);
					amount.setLayoutParams(headerParams);
					headerRow.addView(amount);

					headerTable.addView(headerRow);
					/*
					 * new TableLayout.LayoutParams( LayoutParams.MATCH_PARENT,
					 * LayoutParams.WRAP_CONTENT,1f));
					 */

					for (int i1 = 0; i1 < SelectedGroupsTask.member_Name.size(); i1++) {

						TableRow indv_Row = new TableRow(getActivity());
						TableRow.LayoutParams contentParams = new TableRow.LayoutParams(
								TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);
						contentParams.setMargins(10, 5, 10, 5);

						TextView memberName_Text = new TextView(getActivity());
						memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
								String.valueOf(SelectedGroupsTask.member_Name.elementAt(i1))));
						memberName_Text.setTypeface(LoginActivity.sTypeface);
						memberName_Text.setTextColor(color.black);
						memberName_Text.setPadding(50, 5, 10, 5);
						// memberName_Text.setLayoutParams(contentParams);
						indv_Row.addView(memberName_Text, contentParams);

						TextView confirm_values = new TextView(getActivity());
						confirm_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sumAmount[i1])));
						confirm_values.setTextColor(color.black);
						confirm_values.setPadding(10, 5, 60, 5);
						confirm_values.setGravity(Gravity.RIGHT);
						// memberName_Text.setLayoutParams(contentParams);
						indv_Row.addView(confirm_values, contentParams);

						tableLayout.addView(indv_Row,
								new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

						View rullerView = new View(getActivity());
						rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
						rullerView.setBackgroundColor(Color.rgb(220, 220, 220));
						tableLayout.addView(rullerView);

					}

				} else if (OfflineReports_TransactionList.selectedItem.equals(AppStrings.memberloanrepayment)) {
					for (int i = 0; i < arrayList.size(); i++) {
						getResponse = "";
						amount = "";
						interest = "";
						if (Offline_ReportDateListFragment.sSelectedTransDate
								.equals(arrayList.get(i).getTransactionDate())) {

							/*
							 * if (Offline_SubMenuList.selectedSubMenuItem
							 * .equals(Offline_SubMenuList.memberLoanName
							 * .toString())) { if
							 * (arrayList.get(i).getMemberloanRepayment() !=
							 * null) { getResponse = arrayList.get(i)
							 * .getMemberloanRepayment(); System.out
							 * .println("<<<<<getResponse>>>>>>>>>" +
							 * getResponse.toString()); responseArr =
							 * getResponse.split("[~#]"); dateArr = String
							 * .valueOf( responseArr[responseArr.length - 2])
							 * .split("/"); transDate = dateArr[1] + "/" +
							 * dateArr[0] + "/" + dateArr[2]; if
							 * (Offline_TransactionDateFragment.
							 * sSelectedTransDate .equals(transDate)) { for (int
							 * j = 0; j < responseArr.length - 4; j = j + 3) {
							 * amount = amount + responseArr[j + 1] + "~";
							 * interest = interest + responseArr[j + 2] + "~"; }
							 * System.out
							 * .println("<<-----PRINCIPAL AMOUNT--------->>" +
							 * amount.toString()); System.out
							 * .println("<<----------INTEREST AMOUNT-------->>"
							 * + interest.toString()); amountsArr =
							 * amount.split("~"); interestArr =
							 * interest.split("~");
							 * 
							 * for (int k = 0; k < sumAmount.length; k++) {
							 * 
							 * System.out.println(" Amount :" + amountsArr[k] +
							 * " pos :" + k); sumAmount[k] += Integer
							 * .parseInt(amountsArr[k]); sumInterest[k] +=
							 * Integer .parseInt(interestArr[k]);
							 * Log.v("SUM AMOUNT", sumAmount[k] + "");
							 * Log.v("INTEREST AMOUNT", interestArr[k] + ""); }
							 * } } }
							 */
							if ((!Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.InternalLoan))) {
								if (arrayList.get(i).getMemberloanRepayment() != null) {
									getResponse = arrayList.get(i).getMemberloanRepayment();
									System.out.println("<<<<<getResponse>>>>>>>>>" + getResponse.toString());

									response$Arr = getResponse.split("[$]");

									for (int j1 = 0; j1 < response$Arr.length; j1++) {
										String string = response$Arr[j1];
										Log.v("!!!!!!!!!!!!!", string);
										String string2[] = string.split("#");
										for (int k = 0; k < string2.length; k++) {
											Log.v("string2[]", string2[k] + " pos " + k);
										}

										if (string2[1].equals(
												Offline_SubMenuList.LoanIdArr[Offline_SubMenuList.selectedItemPos])) {
											responseArr = string.split("[~#]");
											dateArr = String.valueOf(responseArr[responseArr.length - 2]).split("/");
											transDate = dateArr[1] + "/" + dateArr[0] + "/" + dateArr[2];

											if (Offline_TransactionDateFragment.sSelectedTransDate.equals(transDate)) {

												for (int j = 0; j < responseArr.length - 4; j = j + 3) {
													amount = amount + responseArr[j + 1] + "~";
													interest = interest + responseArr[j + 2] + "~";
												}
												System.out.println(
														"<<-----PRINCIPAL AMOUNT--------->>" + amount.toString());
												System.out.println(
														"<<----------INTEREST AMOUNT-------->>" + interest.toString());
												amountsArr = amount.split("~");
												interestArr = interest.split("~");

												for (int k = 0; k < sumAmount.length; k++) {

													System.out.println(" Amount :" + amountsArr[k] + " pos :" + k);
													sumAmount[k] += Integer.parseInt(amountsArr[k]);
													sumInterest[k] += Integer.parseInt(interestArr[k]);
													Log.v("SUM AMOUNT", sumAmount[k] + "");
													Log.v("INTEREST AMOUNT", interestArr[k] + "");
												}
											}
										}

									}
								}

							} else if (Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.InternalLoan)) {
								if (arrayList.get(i).getPersonalloanRepayment() != null) {
									getResponse = arrayList.get(i).getPersonalloanRepayment();
									System.out.println("<<<<<getResponse>>>>>>>>>" + getResponse.toString());
									responseArr = getResponse.split("[~#]");

									dateArr = String.valueOf(responseArr[responseArr.length - 2]).split("/");
									transDate = dateArr[1] + "/" + dateArr[0] + "/" + dateArr[2];
									if (Offline_TransactionDateFragment.sSelectedTransDate.equals(transDate)) {
										for (int j = 0; j < responseArr.length - 4; j = j + 3) {
											amount = amount + responseArr[j + 1] + "~";
											interest = interest + responseArr[j + 2] + "~";
										}
										System.out.println("<<-----PRINCIPAL AMOUNT--------->>" + amount.toString());
										System.out
												.println("<<----------INTEREST AMOUNT-------->>" + interest.toString());
										amountsArr = amount.split("~");
										interestArr = interest.split("~");

										for (int k = 0; k < sumAmount.length; k++) {

											System.out.println(" Amount :" + amountsArr[k] + " pos :" + k);
											sumAmount[k] += Integer.parseInt(amountsArr[k]);
											sumInterest[k] += Integer.parseInt(interestArr[k]);
											Log.v("SUM AMOUNT", sumAmount[k] + "");
											Log.v("INTEREST AMOUNT", interestArr[k] + "");
										}
									}
								}
							}
						}
					}

					if (mLanguagelocale.equalsIgnoreCase("English")) {
						mHeadertext.setText(RegionalConversion.getRegionalConversion(AppStrings.reports)
								+ RegionalConversion.getRegionalConversion(AppStrings.of)
								+ RegionalConversion.getRegionalConversion(Offline_SubMenuList.selectedSubMenuItem)
								+ " ON " + Offline_TransactionDateFragment.sSelectedTransDate);
					} else {
						mHeadertext.setText(Offline_TransactionDateFragment.sSelectedTransDate
								+ RegionalConversion.getRegionalConversion(AppStrings.of)
								+ RegionalConversion.getRegionalConversion(Offline_SubMenuList.selectedSubMenuItem)
								+ " " + RegionalConversion.getRegionalConversion(AppStrings.reports));
				
					}
					

					TextView mMemberName_Header = new TextView(getActivity());
					mMemberName_Header
							.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.memberName)));
					mMemberName_Header.setTypeface(LoginActivity.sTypeface);
					mMemberName_Header.setTextColor(Color.WHITE);
					mMemberName_Header.setPadding(10, 5, 5, 5);
					mMemberName_Header.setLayoutParams(headerParams);
					headerRow.addView(mMemberName_Header);

					TextView mPA_Header = new TextView(getActivity());
					mPA_Header.setText(
							RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.principleAmount)));
					mPA_Header.setTypeface(LoginActivity.sTypeface);
					mPA_Header.setTextColor(Color.WHITE);
					mPA_Header.setPadding(0, 5, 40, 5);// 50-40
					mPA_Header.setGravity(Gravity.RIGHT);
					mPA_Header.setLayoutParams(headerParams);
					headerRow.addView(mPA_Header);

					TextView mInterest_Header = new TextView(getActivity());
					mInterest_Header
							.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.interest)));
					mInterest_Header.setTypeface(LoginActivity.sTypeface);
					mInterest_Header.setTextColor(Color.WHITE);
					mInterest_Header.setPadding(5, 5, 10, 5);
					mInterest_Header.setGravity(Gravity.RIGHT);
					mInterest_Header.setLayoutParams(headerParams);
					headerRow.addView(mInterest_Header);

					headerTable.addView(headerRow);// ,
					/*
					 * new TableLayout.LayoutParams( LayoutParams.MATCH_PARENT,
					 * LayoutParams.WRAP_CONTENT,1f));
					 */

					for (int j = 0; j < SelectedGroupsTask.member_Name.size(); j++) {
						TableRow indv_SavingsRow = new TableRow(getActivity());

						TableRow.LayoutParams contentParams = new TableRow.LayoutParams(0, LayoutParams.WRAP_CONTENT,
								1f);
						contentParams.setMargins(5, 5, 5, 5);

						TextView memberName_Text = new TextView(getActivity());
						memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
								String.valueOf(SelectedGroupsTask.member_Name.elementAt(j))));
						memberName_Text.setTypeface(LoginActivity.sTypeface);
						memberName_Text.setTextColor(color.black);
						memberName_Text.setPadding(5, 5, 5, 5);
						memberName_Text.setLayoutParams(contentParams);
						indv_SavingsRow.addView(memberName_Text);

						TextView confirm_PAvalues = new TextView(getActivity());
						confirm_PAvalues
								.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sumAmount[j])));
						confirm_PAvalues.setTextColor(color.black);
						confirm_PAvalues.setPadding(5, 5, 15, 5);
						confirm_PAvalues.setGravity(Gravity.RIGHT);
						confirm_PAvalues.setLayoutParams(contentParams);
						indv_SavingsRow.addView(confirm_PAvalues);

						TextView confirm_Intrvalues = new TextView(getActivity());
						confirm_Intrvalues
								.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sumInterest[j])));
						confirm_Intrvalues.setTextColor(color.black);
						confirm_Intrvalues.setPadding(5, 5, 15, 5);
						confirm_Intrvalues.setGravity(Gravity.RIGHT);
						confirm_Intrvalues.setLayoutParams(contentParams);
						indv_SavingsRow.addView(confirm_Intrvalues);

						tableLayout.addView(indv_SavingsRow,
								new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

						View rullerView = new View(getActivity());
						rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
						rullerView.setBackgroundColor(Color.rgb(220, 220, 220));
						tableLayout.addView(rullerView);

					}

				} else if (OfflineReports_TransactionList.selectedItem.equals(AppStrings.expenses)) {
					sumAmount = new int[7];
					for (int i = 0; i < arrayList.size(); i++) {
						getResponse = "";
						amount = "";
						if (Offline_ReportDateListFragment.sSelectedTransDate
								.equals(arrayList.get(i).getTransactionDate())) {

							if (arrayList.get(i).getExpenses() != null) {
								getResponse = arrayList.get(i).getExpenses();
								System.out.println("<<<<<getResponse>>>>>>>>>" + getResponse.toString());
								responseArr = getResponse.split("[~#]");

								dateArr = String.valueOf(responseArr[responseArr.length - 2]).split("/");
								transDate = dateArr[1] + "/" + dateArr[0] + "/" + dateArr[2];
								if (Offline_TransactionDateFragment.sSelectedTransDate.equals(transDate)) {
									for (int j = 0; j < responseArr.length - 3; j++) {
										amount = amount + responseArr[j] + "~";
									}
									System.out.println("<<-----EXPENSES AMOUNT--------->>" + amount.toString());
									amountsArr = amount.split("~");

									for (int k = 0; k < Transaction_ExpensesFragment.sExpensesName.length; k++) {

										System.out.println(" Amount :" + amountsArr[k] + " pos :" + k);
										sumAmount[k] += Integer.parseInt(amountsArr[k]);
										Log.v("SUM AMOUNT", sumAmount[k] + "");
									}
								}
							}
						}
					}

					if (mLanguagelocale.equalsIgnoreCase("English")) {
						mHeadertext.setText(RegionalConversion.getRegionalConversion(AppStrings.reports)
								+ RegionalConversion.getRegionalConversion(AppStrings.of)
								+ RegionalConversion.getRegionalConversion(OfflineReports_TransactionList.selectedItem)
								+ " ON " + Offline_TransactionDateFragment.sSelectedTransDate);
					} else {
						mHeadertext.setText(Offline_TransactionDateFragment.sSelectedTransDate
								+ RegionalConversion.getRegionalConversion(AppStrings.of)
								+ RegionalConversion.getRegionalConversion(OfflineReports_TransactionList.selectedItem)
								+ " " + RegionalConversion.getRegionalConversion(AppStrings.reports));
					}
					

					TextView expensesText = new TextView(getActivity());
					expensesText.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.expenses)));
					expensesText.setTypeface(LoginActivity.sTypeface);
					expensesText.setTextColor(Color.WHITE);
					expensesText.setPadding(20, 5, 20, 5);
					expensesText.setLayoutParams(headerParams);
					headerRow.addView(expensesText);

					TextView amountText = new TextView(getActivity());
					amountText.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.amount)));
					amountText.setTypeface(LoginActivity.sTypeface);
					amountText.setTextColor(Color.WHITE);
					amountText.setGravity(Gravity.RIGHT);
					amountText.setPadding(20, 5, 50, 5);
					amountText.setLayoutParams(headerParams);
					headerRow.addView(amountText);

					headerTable.addView(headerRow);// ,
					/*
					 * new TableLayout.LayoutParams( LayoutParams.MATCH_PARENT,
					 * LayoutParams.WRAP_CONTENT));
					 */
					for (int i = 0; i < Transaction_ExpensesFragment.sExpensesName.length; i++) {

						TableRow indv_ExpensesRow = new TableRow(getActivity());

						TableRow.LayoutParams contentParams = new TableRow.LayoutParams(0, LayoutParams.WRAP_CONTENT,
								1f);
						contentParams.setMargins(10, 5, 10, 5);

						TextView memberName_Text = new TextView(getActivity());
						memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
								String.valueOf(Transaction_ExpensesFragment.sExpensesName[i])));
						memberName_Text.setTypeface(LoginActivity.sTypeface);
						memberName_Text.setTextColor(color.black);
						memberName_Text.setPadding(10, 5, 5, 5);
						memberName_Text.setLayoutParams(contentParams);
						indv_ExpensesRow.addView(memberName_Text);

						TextView confirm_values = new TextView(getActivity());
						confirm_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sumAmount[i])));
						confirm_values.setTextColor(color.black);
						confirm_values.setPadding(5, 5, 50, 5);
						confirm_values.setGravity(Gravity.RIGHT);
						confirm_values.setLayoutParams(contentParams);
						indv_ExpensesRow.addView(confirm_values);

						tableLayout.addView(indv_ExpensesRow,
								new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

						View rullerView = new View(getActivity());
						rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
						rullerView.setBackgroundColor(Color.rgb(220, 220, 220));
						tableLayout.addView(rullerView);
					}

				} else if (OfflineReports_TransactionList.selectedItem.equals(AppStrings.income)) {
					if (Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.otherincome)
							|| Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.subscriptioncharges)
							|| Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.penalty)) {
						otheIncomeAmount = 0;
						for (int i = 0; i < arrayList.size(); i++) {
							getResponse = "";
							amount = "";
							if (Offline_ReportDateListFragment.sSelectedTransDate
									.equals(arrayList.get(i).getTransactionDate())) {

								if (Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.otherincome)) {
									if (arrayList.get(i).getOtherIncome() != null) {
										getResponse = arrayList.get(i).getOtherIncome();
										System.out.println("<<<<<getResponse>>>>>>>>>" + getResponse.toString());
										responseArr = getResponse.split("[~#$]");
										otheIncomeAmount = otheIncomeAmount + Integer.parseInt(responseArr[responseArr.length-1]);
										Log.e("Other Income Amount", otheIncomeAmount+"");
										dateArr = String.valueOf(responseArr[responseArr.length - 3]).split("/");
										transDate = dateArr[1] + "/" + dateArr[0] + "/" + dateArr[2];
										if (Offline_TransactionDateFragment.sSelectedTransDate.equals(transDate)) {
											for (int j = 0; j < responseArr.length - 3; j++) {
												if (j % 2 != 0) {
													amount = amount + responseArr[j] + "~";
												}
											}
											System.out.println("<<-----amt--------->>" + amount.toString());
											amountsArr = amount.split("~");

											for (int k = 0; k < amountsArr.length; k++) {

												System.out.println(" Amount :" + amountsArr[k] + " pos :" + k);
												sumAmount[k] += Integer.parseInt(amountsArr[k]);
												Log.v("SUM AMOUNT", sumAmount[k] + "");
											}
											
										}
									}
								} else if (Offline_SubMenuList.selectedSubMenuItem
										.equals(AppStrings.subscriptioncharges)) {
									if (arrayList.get(i).getSubscriptionIncome() != null) {
										getResponse = arrayList.get(i).getSubscriptionIncome();
										System.out.println("<<<<<getResponse>>>>>>>>>" + getResponse.toString());
										responseArr = getResponse.split("[~#]");

										dateArr = String.valueOf(responseArr[responseArr.length - 2]).split("/");
										transDate = dateArr[1] + "/" + dateArr[0] + "/" + dateArr[2];
										if (Offline_TransactionDateFragment.sSelectedTransDate.equals(transDate)) {
											for (int j = 0; j < responseArr.length - 2; j++) {
												if (j % 2 != 0) {
													amount = amount + responseArr[j] + "~";
												}
											}
											System.out.println("<<-----amt--------->>" + amount.toString());
											amountsArr = amount.split("~");

											for (int k = 0; k < amountsArr.length; k++) {

												System.out.println(" Amount :" + amountsArr[k] + " pos :" + k);
												sumAmount[k] += Integer.parseInt(amountsArr[k]);
												Log.v("SUM AMOUNT", sumAmount[k] + "");
											}
										}
									}
								} else if (Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.penalty)) {
									if (arrayList.get(i).getPenaltyIncome() != null) {
										getResponse = arrayList.get(i).getPenaltyIncome();
										System.out.println("<<<<<getResponse>>>>>>>>>" + getResponse.toString());
										responseArr = getResponse.split("[~#]");

										dateArr = String.valueOf(responseArr[responseArr.length - 2]).split("/");
										transDate = dateArr[1] + "/" + dateArr[0] + "/" + dateArr[2];
										if (Offline_TransactionDateFragment.sSelectedTransDate.equals(transDate)) {
											for (int j = 0; j < responseArr.length - 2; j++) {
												if (j % 2 != 0) {
													amount = amount + responseArr[j] + "~";
												}
											}
											System.out.println("<<-----amt--------->>" + amount.toString());
											amountsArr = amount.split("~");

											for (int k = 0; k < amountsArr.length; k++) {

												System.out.println(" Amount :" + amountsArr[k] + " pos :" + k);
												sumAmount[k] += Integer.parseInt(amountsArr[k]);
												Log.v("SUM AMOUNT", sumAmount[k] + "");
											}
										}
									}
								}
							}
						}

						if (mLanguagelocale.equalsIgnoreCase("English")) {
							mHeadertext.setText(RegionalConversion.getRegionalConversion(AppStrings.reports)
									+ RegionalConversion.getRegionalConversion(AppStrings.of)
									+ RegionalConversion.getRegionalConversion(Offline_SubMenuList.selectedSubMenuItem)
									+ " ON " + Offline_TransactionDateFragment.sSelectedTransDate);
						} else {
							mHeadertext.setText(Offline_TransactionDateFragment.sSelectedTransDate
									+ RegionalConversion.getRegionalConversion(AppStrings.of)
									+ RegionalConversion.getRegionalConversion(Offline_SubMenuList.selectedSubMenuItem)
									+ " " + RegionalConversion.getRegionalConversion(AppStrings.reports));
						}
						

						TextView memName = new TextView(getActivity());
						memName.setText(
								RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.memberName)));
						memName.setTypeface(LoginActivity.sTypeface);
						memName.setPadding(60, 5, 10, 5);
						memName.setTextColor(Color.WHITE);
						memName.setLayoutParams(headerParams);
						headerRow.addView(memName);

						TextView amount = new TextView(getActivity());
						amount.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.amount)));
						amount.setTypeface(LoginActivity.sTypeface);
						amount.setTextColor(Color.WHITE);
						amount.setPadding(10, 5, 60, 5);
						amount.setGravity(Gravity.RIGHT);
						amount.setLayoutParams(headerParams);
						headerRow.addView(amount);

						headerTable.addView(headerRow);// ,
						/*
						 * new TableLayout.LayoutParams(
						 * LayoutParams.MATCH_PARENT,
						 * LayoutParams.WRAP_CONTENT));
						 */

						for (int i1 = 0; i1 < SelectedGroupsTask.member_Name.size(); i1++) {

							TableRow indv_Row = new TableRow(getActivity());
							TableRow.LayoutParams contentParams = new TableRow.LayoutParams(
									TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);
							contentParams.setMargins(10, 5, 10, 5);

							TextView memberName_Text = new TextView(getActivity());
							memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
									String.valueOf(SelectedGroupsTask.member_Name.elementAt(i1))));
							memberName_Text.setTypeface(LoginActivity.sTypeface);
							memberName_Text.setTextColor(color.black);
							memberName_Text.setPadding(50, 5, 10, 5);
							// memberName_Text.setLayoutParams(contentParams);
							indv_Row.addView(memberName_Text, contentParams);

							TextView confirm_values = new TextView(getActivity());
							confirm_values
									.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sumAmount[i1])));
							confirm_values.setTextColor(color.black);
							confirm_values.setPadding(10, 5, 60, 5);
							confirm_values.setGravity(Gravity.RIGHT);
							// memberName_Text.setLayoutParams(contentParams);
							indv_Row.addView(confirm_values, contentParams);

							tableLayout.addView(indv_Row,
									new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

							View rullerView = new View(getActivity());
							rullerView
									.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
							rullerView.setBackgroundColor(Color.rgb(220, 220, 220));
							tableLayout.addView(rullerView);

						}
						if (Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.otherincome)) {
						
							TableRow indv_Row = new TableRow(getActivity());
							TableRow.LayoutParams contentParams = new TableRow.LayoutParams(
									TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);
							contentParams.setMargins(10, 5, 10, 5);

							TextView memberName_Text = new TextView(getActivity());
							memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
									String.valueOf(RegionalConversion.getRegionalConversion(AppStrings.mOthers))));
							memberName_Text.setTypeface(LoginActivity.sTypeface);
							memberName_Text.setTextColor(color.black);
							memberName_Text.setPadding(50, 5, 10, 5);
							// memberName_Text.setLayoutParams(contentParams);
							indv_Row.addView(memberName_Text, contentParams);

							TextView confirm_values = new TextView(getActivity());
							confirm_values
									.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(otheIncomeAmount)));//String.valueOf(responseArr[responseArr.length-1])));
							confirm_values.setTextColor(color.black);
							confirm_values.setPadding(10, 5, 60, 5);
							confirm_values.setGravity(Gravity.RIGHT);
							// memberName_Text.setLayoutParams(contentParams);
							indv_Row.addView(confirm_values, contentParams);

							tableLayout.addView(indv_Row,
									new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

							View rullerView = new View(getActivity());
							rullerView
									.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
							rullerView.setBackgroundColor(Color.rgb(220, 220, 220));
							tableLayout.addView(rullerView);

						}
						
					} else if (Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.donation)
							|| Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.federationincome)
							|| Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.mSeedFund)) {
						System.out.println("-------------------------1");
						if (Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.donation)
							|| Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.federationincome)) {
							System.out.println("-------------------------2");
						sumAmount = new int[1];
						for (int i = 0; i < arrayList.size(); i++) {
							getResponse = "";
							amount = "";
							if (Offline_ReportDateListFragment.sSelectedTransDate
									.equals(arrayList.get(i).getTransactionDate())) {

								if (Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.donation)) {
									if (arrayList.get(i).getDonationIncome() != null) {
										getResponse = arrayList.get(i).getDonationIncome();
										System.out.println("<<<<<getResponse>>>>>>>>>" + getResponse.toString());
										responseArr = getResponse.split("#");
										dateArr = String.valueOf(responseArr[responseArr.length - 2]).split("/");
										transDate = dateArr[1] + "/" + dateArr[0] + "/" + dateArr[2];
										if (Offline_TransactionDateFragment.sSelectedTransDate.equals(transDate)) {
											sumAmount[0] = sumAmount[0] + Integer.parseInt(responseArr[0]);
											System.out.println("-------------AMOUNT-----------" + sumAmount[0]);
										}
									}
								} else if (Offline_SubMenuList.selectedSubMenuItem
										.equals(AppStrings.federationincome)) {
									if (arrayList.get(i).getFederationIncome() != null) {
										getResponse = arrayList.get(i).getFederationIncome();
										System.out.println("<<<<<getResponse>>>>>>>>>" + getResponse.toString());
										responseArr = getResponse.split("#");
										dateArr = String.valueOf(responseArr[responseArr.length - 2]).split("/");
										transDate = dateArr[1] + "/" + dateArr[0] + "/" + dateArr[2];
										if (Offline_TransactionDateFragment.sSelectedTransDate.equals(transDate)) {
											sumAmount[0] = sumAmount[0] + Integer.parseInt(responseArr[0]);
											System.out.println("-------------AMOUNT-----------" + sumAmount[0]);
										}
									}
								}
							}
						}
						if (mLanguagelocale.equalsIgnoreCase("English")) {
							mHeadertext.setText(RegionalConversion.getRegionalConversion(AppStrings.reports)
									+ RegionalConversion.getRegionalConversion(AppStrings.of)
									+ RegionalConversion.getRegionalConversion(Offline_SubMenuList.selectedSubMenuItem)
									+ " ON " + Offline_TransactionDateFragment.sSelectedTransDate);
						} else {
							mHeadertext.setText(Offline_TransactionDateFragment.sSelectedTransDate
									+ RegionalConversion.getRegionalConversion(AppStrings.of)
									+ RegionalConversion.getRegionalConversion(Offline_SubMenuList.selectedSubMenuItem)
									+ " " + RegionalConversion.getRegionalConversion(AppStrings.reports));
						}
						

						TableRow indv_row = new TableRow(getActivity());
						TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
								LayoutParams.WRAP_CONTENT, 1f);
						contentParams.setMargins(10, 5, 10, 5);

						TextView donation = new TextView(getActivity());
						donation.setText(GetSpanText.getSpanString(getActivity(),
								String.valueOf(Offline_SubMenuList.selectedSubMenuItem)));
						donation.setTypeface(LoginActivity.sTypeface);
						donation.setTextColor(color.black);
						donation.setPadding(10, 5, 10, 5);
						donation.setGravity(Gravity.CENTER_HORIZONTAL);
						donation.setLayoutParams(contentParams);
						indv_row.addView(donation);

						TextView confirm_values = new TextView(getActivity());
						confirm_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sumAmount[0])));
						confirm_values.setTextColor(color.black);
						confirm_values.setPadding(10, 5, 50, 5);
						confirm_values.setGravity(Gravity.CENTER_HORIZONTAL);
						donation.setLayoutParams(contentParams);
						indv_row.addView(confirm_values);

						tableLayout.addView(indv_row,
								new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

						View rullerView = new View(getActivity());
						rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
						rullerView.setBackgroundColor(Color.rgb(220, 220, 220));
						tableLayout.addView(rullerView);

					}else if (Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.mSeedFund)) {
						System.out.println("-------------------------3");
						String[] mAmountArr,mTypeArr,mBankNameArr;
						String amount = "",type = "",bankName = "";
						for (int i = 0; i < arrayList.size(); i++) {
							getResponse = "";
						//	amount = "";
							if (Offline_ReportDateListFragment.sSelectedTransDate
									.equals(arrayList.get(i).getTransactionDate())) {

								if (Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.mSeedFund)) {
									if (arrayList.get(i).getSeedFund() != null) {
										getResponse = arrayList.get(i).getSeedFund();
										System.out.println("<<<<<getResponse>>>>>>>>>" + getResponse.toString());
										responseArr = getResponse.split("#");
										dateArr = String.valueOf(responseArr[responseArr.length - 2]).split("/");
										transDate = dateArr[1] + "/" + dateArr[0] + "/" + dateArr[2];
										
										if (Offline_TransactionDateFragment.sSelectedTransDate.equals(transDate)) {
											amount = amount + responseArr[0] + "~";
											type = type + responseArr[1] + "~";
											if (!responseArr[2].equals("")) {
												bankName = bankName + responseArr[2] + "~";	
											} else {
												bankName = bankName + "0" + "~";
											}
											
										}
									}
								}
							}
						}
						if (mLanguagelocale.equalsIgnoreCase("English")) {
							mHeadertext.setText(RegionalConversion.getRegionalConversion(AppStrings.reports)
									+ RegionalConversion.getRegionalConversion(AppStrings.of)
									+ RegionalConversion.getRegionalConversion(Offline_SubMenuList.selectedSubMenuItem)
									+ " ON " + Offline_TransactionDateFragment.sSelectedTransDate);
						} else {
							mHeadertext.setText(Offline_TransactionDateFragment.sSelectedTransDate
									+ RegionalConversion.getRegionalConversion(AppStrings.of)
									+ RegionalConversion.getRegionalConversion(Offline_SubMenuList.selectedSubMenuItem)
									+ " " + RegionalConversion.getRegionalConversion(AppStrings.reports));
						}
						
						mAmountArr = amount.split("~");
						mTypeArr = type.split("~");
						mBankNameArr = bankName.split("~");
						Log.e("Type", type.toString()+"");
						Log.e("Amount", amount.toString()+"");
						System.out.println("--Bank Name list   =  "+bankName.toString()+"");

						for (int i = 0; i < mAmountArr.length; i++) {
							TableRow indv_Row = new TableRow(getActivity());
							TableRow.LayoutParams contentParams = new TableRow.LayoutParams(
									TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

							TextView type_Text = new TextView(getActivity());
							type_Text
									.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mLoanAccType)));
							type_Text.setTypeface(LoginActivity.sTypeface);
							type_Text.setPadding(10, 5, 10, 5);
							type_Text.setTextColor(color.black);
							type_Text.setLayoutParams(contentParams);
							indv_Row.addView(type_Text);

							TextView type_Text1 = new TextView(getActivity());
							type_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(mTypeArr[i])));
							type_Text1.setTypeface(LoginActivity.sTypeface);
							type_Text1.setPadding(10, 5, 10, 5);
							type_Text1.setTextColor(color.black);
							type_Text1.setLayoutParams(contentParams);
							indv_Row.addView(type_Text1);

							tableLayout.addView(indv_Row,
									new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

/*							View rullerView = new View(getActivity());
							rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
							rullerView.setBackgroundColor(Color.rgb(220, 220, 220));
							tableLayout.addView(rullerView);*/

							TableRow indv_Row1 = new TableRow(getActivity());
							TableRow.LayoutParams contentParams1 = new TableRow.LayoutParams(
									TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

							TextView amount_Text = new TextView(getActivity());
							amount_Text
									.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.amount)));
							amount_Text.setTypeface(LoginActivity.sTypeface);
							amount_Text.setTextColor(color.black);
							amount_Text.setLayoutParams(contentParams1);
							amount_Text.setPadding(10, 5, 10, 5);
							indv_Row1.addView(amount_Text);

							TextView amount_Text1 = new TextView(getActivity());
							amount_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(mAmountArr[i])));
							amount_Text1.setTypeface(LoginActivity.sTypeface);
							amount_Text1.setTextColor(color.black);
							amount_Text1.setPadding(10, 5, 10, 5);
							amount_Text1.setLayoutParams(contentParams1);
							indv_Row1.addView(amount_Text1);

							tableLayout.addView(indv_Row1,
									new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

							/*View rullerView1 = new View(getActivity());
							rullerView1.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
							rullerView1.setBackgroundColor(Color.rgb(220, 220, 220));
							tableLayout.addView(rullerView1);*/
							Log.e("Bank name size", mBankNameArr.length+"");
							if (!mBankNameArr[i].equals("0")) {
								
							TableRow indv_Row2 = new TableRow(getActivity());
							TableRow.LayoutParams contentParams2 = new TableRow.LayoutParams(
									TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

							TextView bankName_Text = new TextView(getActivity());
							bankName_Text.setText(
									GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.bankName)));
							bankName_Text.setTypeface(LoginActivity.sTypeface);
							bankName_Text.setTextColor(color.black);
							bankName_Text.setPadding(10, 5, 10, 5);
							bankName_Text.setLayoutParams(contentParams2);
							indv_Row2.addView(bankName_Text);

							TextView bankName_Text1 = new TextView(getActivity());
							bankName_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(mBankNameArr[i])));
							bankName_Text1.setTypeface(LoginActivity.sTypeface);
							bankName_Text1.setPadding(10, 5, 10, 5);
							bankName_Text1.setLayoutParams(contentParams2);
							bankName_Text1.setTextColor(color.black);
							indv_Row2.addView(bankName_Text1);

							tableLayout.addView(indv_Row2,
									new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

							}
							View rullerView2 = new View(getActivity());
							rullerView2.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
							rullerView2.setBackgroundColor(Color.rgb(220, 220, 220));
							tableLayout.addView(rullerView2);

						}


					}

				}
				}else if (OfflineReports_TransactionList.selectedItem
						.equals(String.valueOf(AppStrings.grouploanrepayment))) {
					sumAmount = new int[6];
					
					String type = "", interest = "", charge = "", repayment = "", subvention = "", bankName = "", bankCharge = "";
					String[] typeArr, interestArr, chargeArr, repaymentArr, subventionArr, bankNameArr, bankChargeArr;
					
					for (int j = 0; j < arrayList.size(); j++) {
						getResponse = "";
						amount = "";
						if (Offline_ReportDateListFragment.sSelectedTransDate
								.equals(arrayList.get(j).getTransactionDate())) {

							if (arrayList.get(j).getGrouploanrepayment() != null) {
								getResponse = arrayList.get(j).getGrouploanrepayment();
								System.out.println("<<<<<getResponse>>>>>>>>>" + getResponse.toString());
								
								selectedType = "";
							
								response$Arr = getResponse.split("[$]");

								for (int j1 = 0; j1 < response$Arr.length; j1++) {
									String string = response$Arr[j1];
									Log.v("!!!!!!!!!!!!!", string);
									String string2[] = string.split("#");
									if (string2[1].equals(
											Offline_SubMenuList.LoanIdArr[Offline_SubMenuList.selectedItemPos])) {
										responseArr = string.split("[~#]");
										dateArr = String.valueOf(responseArr[responseArr.length - 3]).split("/");
										transDate = dateArr[1] + "/" + dateArr[0] + "/" + dateArr[2];

										selectedType = responseArr[responseArr.length-1];
										
										if (Offline_TransactionDateFragment.sSelectedTransDate.equals(transDate)) {

											type = type + responseArr[responseArr.length-1]+"~";
											interest = interest + responseArr[0] + "~";
											charge = charge + responseArr[1] + "~";
											repayment = repayment + responseArr[2] + "~";
											subvention = subvention + responseArr[3] + "~";
											if (selectedType.equals("Cash")) {
												bankName = bankName + "0" + "~";
												bankCharge = bankCharge + "no" + "~";
											} else if (selectedType.equals("Bank")) {
												bankName = bankName + responseArr[4] + "~";
												bankCharge = bankCharge + responseArr[5] + "~";
											}
											
										}
									}
								}
							}
						}
					}
					
					typeArr = type.split("~");
					interestArr = interest.split("~");
					chargeArr = charge.split("~");
					repaymentArr = repayment.split("~");
					subventionArr = subvention.split("~");
					bankNameArr = bankName.split("~");
					bankChargeArr = bankCharge.split("~");
					
					//for testing
					for (int i = 0; i < typeArr.length; i++) {
						System.out.println("Type   		=  "+typeArr[i]+"");
						System.out.println("Interest    =  "+interestArr[i]+"");
						System.out.println("Charge   	=  "+chargeArr[i]+"");
						System.out.println("Repayment   =  "+repaymentArr[i]+"");
						System.out.println("Subvention  =  "+subventionArr[i]+"");
						System.out.println("Bank name   =  "+bankNameArr[i]+"");
						System.out.println("BankCharge  =  "+bankChargeArr[i]+"");
					}

					if (mLanguagelocale.equalsIgnoreCase("English")) {
						mHeadertext.setText(RegionalConversion.getRegionalConversion(AppStrings.reports)
								+ RegionalConversion.getRegionalConversion(AppStrings.of)
								+ RegionalConversion.getRegionalConversion(Offline_SubMenuList.selectedSubMenuItem)
								+ " ON " + Offline_TransactionDateFragment.sSelectedTransDate);
					} else {
						mHeadertext.setText(Offline_TransactionDateFragment.sSelectedTransDate
								+ RegionalConversion.getRegionalConversion(AppStrings.of)
								+ RegionalConversion.getRegionalConversion(Offline_SubMenuList.selectedSubMenuItem)
								+ " " + RegionalConversion.getRegionalConversion(AppStrings.reports));
					}
					
					for (int i = 0; i < typeArr.length; i++) {
					
						TableRow typeRow = new TableRow(getActivity());

						TableRow.LayoutParams typeParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
								LayoutParams.WRAP_CONTENT, 1f);
						typeParams.setMargins(10, 5, 10, 5);

						TextView type1 = new TextView(getActivity());
						type1.setText(GetSpanText.getSpanString(getActivity(),
								String.valueOf(AppStrings.mLoanAccType)));
						type1.setTypeface(LoginActivity.sTypeface);
						type1.setTextColor(color.black);
						type1.setPadding(30, 5, 10, 5);
						type1.setLayoutParams(typeParams);
						typeRow.addView(type1);

						TextView typeValue = new TextView(getActivity());
						typeValue.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(typeArr[i])));
						typeValue.setTextColor(color.black);
						typeValue.setPadding(10, 5, 30, 5);
						typeValue.setGravity(Gravity.CENTER_HORIZONTAL);
						typeValue.setLayoutParams(typeParams);
						typeRow.addView(typeValue);

						tableLayout.addView(typeRow,
								new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
						
						if (!bankNameArr[i].equals("0")) {
							TableRow bankNameRow = new TableRow(getActivity());
							
							TableRow.LayoutParams bankNameParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
									LayoutParams.WRAP_CONTENT, 1f);
							bankNameParams.setMargins(10, 5, 10, 5);

							TextView bankName1 = new TextView(getActivity());
							bankName1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.bankName)));
							bankName1.setTypeface(LoginActivity.sTypeface);
							bankName1.setTextColor(color.black);
							bankName1.setPadding(30, 5, 10, 5);
							bankName1.setLayoutParams(bankNameParams);
							bankNameRow.addView(bankName1);

							TextView bankNameValue = new TextView(getActivity());
							bankNameValue.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(bankNameArr[i])));
							bankNameValue.setTextColor(color.black);
							bankNameValue.setPadding(10, 5, 30, 5);
							bankNameValue.setGravity(Gravity.RIGHT);
							bankNameValue.setLayoutParams(bankNameParams);
							bankNameRow.addView(bankNameValue);

							tableLayout.addView(bankNameRow,
									new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

						}
						
						TableRow indvRow = new TableRow(getActivity());

						TableRow.LayoutParams params = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
								LayoutParams.WRAP_CONTENT, 1f);
						params.setMargins(10, 5, 10, 5);

						TextView gLoanRow = new TextView(getActivity());
						gLoanRow.setText(GetSpanText.getSpanString(getActivity(),
								String.valueOf(AppStrings.interest)));
						gLoanRow.setTypeface(LoginActivity.sTypeface);
						gLoanRow.setTextColor(color.black);
						gLoanRow.setPadding(30, 5, 10, 5);
						gLoanRow.setLayoutParams(params);
						indvRow.addView(gLoanRow);

						TextView confirm_values = new TextView(getActivity());
						confirm_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(interestArr[i])));
						confirm_values.setTextColor(color.black);
						confirm_values.setPadding(10, 5, 30, 5);
						confirm_values.setGravity(Gravity.CENTER_HORIZONTAL);
						confirm_values.setLayoutParams(params);
						indvRow.addView(confirm_values);

						tableLayout.addView(indvRow,
								new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

						TableRow indvRow1 = new TableRow(getActivity());

						TableRow.LayoutParams params1 = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
								LayoutParams.WRAP_CONTENT, 1f);
						params1.setMargins(10, 5, 10, 5);

						TextView gLoanRow1 = new TextView(getActivity());
						gLoanRow1.setText(GetSpanText.getSpanString(getActivity(),
								String.valueOf(AppStrings.mCharges)));
						gLoanRow1.setTypeface(LoginActivity.sTypeface);
						gLoanRow1.setTextColor(color.black);
						gLoanRow1.setPadding(30, 5, 10, 5);
						gLoanRow1.setLayoutParams(params1);
						indvRow1.addView(gLoanRow1);

						TextView confirm_values1 = new TextView(getActivity());
						confirm_values1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(chargeArr[i])));
						confirm_values1.setTextColor(color.black);
						confirm_values1.setPadding(10, 5, 30, 5);
						confirm_values1.setGravity(Gravity.CENTER_HORIZONTAL);
						confirm_values1.setLayoutParams(params1);
						indvRow1.addView(confirm_values1);

						tableLayout.addView(indvRow1,
								new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
						
						TableRow indvRow111 = new TableRow(getActivity());

						TableRow.LayoutParams params11 = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
								LayoutParams.WRAP_CONTENT, 1f);
						params11.setMargins(10, 5, 10, 5);

						TextView gLoanRow11 = new TextView(getActivity());
						gLoanRow11.setText(GetSpanText.getSpanString(getActivity(),
								String.valueOf(AppStrings.mRepaymentWithInterest)));
						gLoanRow11.setTypeface(LoginActivity.sTypeface);
						gLoanRow11.setTextColor(color.black);
						gLoanRow11.setPadding(30, 5, 10, 5);
						gLoanRow11.setLayoutParams(params11);
						indvRow111.addView(gLoanRow11);

						TextView confirm_values11 = new TextView(getActivity());
						confirm_values11.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(repaymentArr[i])));
						confirm_values11.setTextColor(color.black);
						confirm_values11.setPadding(10, 5, 30, 5);
						confirm_values11.setGravity(Gravity.CENTER_HORIZONTAL);
						confirm_values11.setLayoutParams(params11);
						indvRow111.addView(confirm_values11);

						tableLayout.addView(indvRow111,
								new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
						
						TableRow indvRow11 = new TableRow(getActivity());

						TableRow.LayoutParams params111 = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
								LayoutParams.WRAP_CONTENT, 1f);
						params111.setMargins(10, 5, 10, 5);

						TextView gLoanRow1111 = new TextView(getActivity());
						gLoanRow1111.setText(GetSpanText.getSpanString(getActivity(),
								String.valueOf(AppStrings.interestSubvention)));
						gLoanRow1111.setTypeface(LoginActivity.sTypeface);
						gLoanRow1111.setTextColor(color.black);
						gLoanRow1111.setPadding(30, 5, 10, 5);
						gLoanRow1111.setLayoutParams(params111);
						indvRow11.addView(gLoanRow1111);

						TextView confirm_values111 = new TextView(getActivity());
						confirm_values111.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(subventionArr[i])));
						confirm_values111.setTextColor(color.black);
						confirm_values111.setPadding(10, 5, 30, 5);
						confirm_values111.setGravity(Gravity.CENTER_HORIZONTAL);
						confirm_values111.setLayoutParams(params111);
						indvRow11.addView(confirm_values111);

						tableLayout.addView(indvRow11,
								new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
						
						if (!bankChargeArr[i].equals("no")) {
							TableRow indvRow1111 = new TableRow(getActivity());

							TableRow.LayoutParams params11111 = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
									LayoutParams.WRAP_CONTENT, 1f);
							params11111.setMargins(10, 5, 10, 5);

							TextView gLoanRow111 = new TextView(getActivity());
							gLoanRow111.setText(GetSpanText.getSpanString(getActivity(),
									String.valueOf(AppStrings.mBankCharges)));
							gLoanRow111.setTypeface(LoginActivity.sTypeface);
							gLoanRow111.setTextColor(color.black);
							gLoanRow111.setPadding(30, 5, 10, 5);
							gLoanRow111.setLayoutParams(params11111);
							indvRow1111.addView(gLoanRow111);

							TextView confirm_values1111 = new TextView(getActivity());
							confirm_values1111.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(bankChargeArr[i])));
							confirm_values1111.setTextColor(color.black);
							confirm_values1111.setPadding(10, 5, 30, 5);
							confirm_values1111.setGravity(Gravity.CENTER_HORIZONTAL);
							confirm_values1111.setLayoutParams(params11111);
							indvRow1111.addView(confirm_values1111);

							tableLayout.addView(indvRow1111,
									new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
						}
						
						View rullerView = new View(getActivity());
						rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
						rullerView.setBackgroundColor(Color.rgb(220, 220, 220));
						tableLayout.addView(rullerView);
					}
					

					
				} else if (OfflineReports_TransactionList.selectedItem.equals(AppStrings.bankTransaction)) {

					if (Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.bankTransaction)) {
						sumAmount = new int[5];
						for (int i = 0; i < arrayList.size(); i++) {
							getResponse = "";
							amount = "";
							if (Offline_ReportDateListFragment.sSelectedTransDate
									.equals(arrayList.get(i).getTransactionDate())) {

								if (arrayList.get(i).getBankDeposit() != null) {
									getResponse = arrayList.get(i).getBankDeposit();
									System.out.println("<<<<<getResponse>>>>>>>>>" + getResponse.toString());
									responseArr = getResponse.split("[~#]");

									dateArr = String.valueOf(responseArr[responseArr.length - 2]).split("/");
									transDate = dateArr[1] + "/" + dateArr[0] + "/" + dateArr[2];
									if (Offline_TransactionDateFragment.sSelectedTransDate.equals(transDate)) {
										for (int j = 0; j < responseArr.length - 3; j++) {
											amount = amount + responseArr[j] + "~";
										}
										System.out.println("<<-----amt--------->>" + amount.toString());
										amountsArr = amount.split("~");

										for (int k = 0; k < sumAmount.length; k++) {

											System.out.println(" Amount :" + amountsArr[k] + " pos :" + k);
											sumAmount[k] += Integer.parseInt(amountsArr[k]);
											Log.v("SUM AMOUNT", sumAmount[k] + "");
										}
									}
								}
							}
						}
						if (mLanguagelocale.equalsIgnoreCase("English")) {
							mHeadertext.setText(RegionalConversion.getRegionalConversion(AppStrings.reports)
									+ RegionalConversion.getRegionalConversion(AppStrings.of)
									+ RegionalConversion.getRegionalConversion(Offline_SubMenuList.selectedSubMenuItem)
									+ " ON " + Offline_TransactionDateFragment.sSelectedTransDate);
						} else {
							mHeadertext.setText(Offline_TransactionDateFragment.sSelectedTransDate
									+ RegionalConversion.getRegionalConversion(AppStrings.of)
									+ RegionalConversion.getRegionalConversion(Offline_SubMenuList.selectedSubMenuItem)
									+ " " + RegionalConversion.getRegionalConversion(AppStrings.reports));
						}
						

						for (int i = 0; i < sumAmount.length; i++) {

							TableRow indv_DepositEntryRow = new TableRow(getActivity());

							TableRow.LayoutParams contentParams = new TableRow.LayoutParams(0,
									LayoutParams.WRAP_CONTENT, 1f);
							contentParams.setMargins(10, 5, 40, 5);

							TextView depositText = new TextView(getActivity());
							depositText.setText(GetSpanText.getSpanString(getActivity(),
									String.valueOf(sDepositItems[i])));
							depositText.setTypeface(LoginActivity.sTypeface);
							depositText.setTextColor(color.black);
							depositText.setPadding(15, 5, 5, 5);
							depositText.setLayoutParams(contentParams);
							indv_DepositEntryRow.addView(depositText);

							TextView confirm_values = new TextView(getActivity());
							confirm_values
									.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sumAmount[i])));
							confirm_values.setTextColor(color.black);
							confirm_values.setPadding(5, 5, 50, 5);
							confirm_values.setGravity(Gravity.RIGHT);
							confirm_values.setLayoutParams(contentParams);
							indv_DepositEntryRow.addView(confirm_values);

							tableLayout.addView(indv_DepositEntryRow,
									new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

							View rullerView = new View(getActivity());
							rullerView
									.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
							rullerView.setBackgroundColor(Color.rgb(220, 220, 220));
							tableLayout.addView(rullerView);
						}

					} else if (Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.fixedDeposit)) {
						sumAmount = new int[4];
						for (int i = 0; i < arrayList.size(); i++) {
							getResponse = "";
							amount = "";

							if (Offline_ReportDateListFragment.sSelectedTransDate
									.equals(arrayList.get(i).getTransactionDate())) {

								if (arrayList.get(i).getBankFixedDeposit() != null) {
									getResponse = arrayList.get(i).getBankFixedDeposit();
									System.out.println("<<<<<getResponse>>>>>>>>>" + getResponse.toString());
									responseArr = getResponse.split("[~#]");

									dateArr = String.valueOf(responseArr[responseArr.length - 2]).split("/");
									transDate = dateArr[1] + "/" + dateArr[0] + "/" + dateArr[2];
									if (Offline_TransactionDateFragment.sSelectedTransDate.equals(transDate)) {
										for (int j = 0; j < responseArr.length - 3; j++) {
											amount = amount + responseArr[j] + "~";
										}
										System.out.println("<<-----amt--------->>" + amount.toString());
										amountsArr = amount.split("~");

										for (int k = 0; k < sumAmount.length; k++) {

											System.out.println(" Amount :" + amountsArr[k] + " pos :" + k);
											sumAmount[k] += Integer.parseInt(amountsArr[k]);
											Log.v("SUM AMOUNT", sumAmount[k] + "");
										}
									}
								}

							}
						}
						if (mLanguagelocale.equalsIgnoreCase("English")) {
							mHeadertext.setText(RegionalConversion.getRegionalConversion(AppStrings.reports)
									+ RegionalConversion.getRegionalConversion(AppStrings.of)
									+ RegionalConversion.getRegionalConversion(Offline_SubMenuList.selectedSubMenuItem)
									+ " ON " + Offline_TransactionDateFragment.sSelectedTransDate);
						} else {
							mHeadertext.setText(Offline_TransactionDateFragment.sSelectedTransDate
									+ RegionalConversion.getRegionalConversion(AppStrings.of)
									+ RegionalConversion.getRegionalConversion(Offline_SubMenuList.selectedSubMenuItem)
									+ " " + RegionalConversion.getRegionalConversion(AppStrings.reports));
						
						}
						

						for (int i = 0; i < sumAmount.length; i++) {

							TableRow indv_DepositEntryRow = new TableRow(getActivity());

							TableRow.LayoutParams contentParams = new TableRow.LayoutParams(0,
									LayoutParams.WRAP_CONTENT, 1f);
							contentParams.setMargins(10, 5, 40, 5);

							TextView depositText = new TextView(getActivity());
							depositText.setText(GetSpanText.getSpanString(getActivity(),
									String.valueOf(Transaction_FixedDepositEntryFragment.sFixedDepositItem[i])));
							depositText.setTypeface(LoginActivity.sTypeface);
							depositText.setTextColor(color.black);
							depositText.setPadding(15, 5, 5, 5);
							depositText.setLayoutParams(contentParams);
							indv_DepositEntryRow.addView(depositText);

							TextView confirm_values = new TextView(getActivity());
							confirm_values
									.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sumAmount[i])));
							confirm_values.setTextColor(color.black);
							confirm_values.setPadding(5, 5, 50, 5);
							confirm_values.setGravity(Gravity.RIGHT);
							confirm_values.setLayoutParams(contentParams);
							indv_DepositEntryRow.addView(confirm_values);

							tableLayout.addView(indv_DepositEntryRow,
									new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

							View rullerView = new View(getActivity());
							rullerView
									.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
							rullerView.setBackgroundColor(Color.rgb(220, 220, 220));
							tableLayout.addView(rullerView);
						}

					} else if (Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.mLoanaccHeader)) {

						if (mLanguagelocale.equalsIgnoreCase("English")) {
							mHeadertext.setText(RegionalConversion.getRegionalConversion(AppStrings.reports)
									+ RegionalConversion.getRegionalConversion(AppStrings.of)
									+ RegionalConversion.getRegionalConversion(Offline_SubMenuList.selectedSubMenuItem)
									+ " ON " + Offline_TransactionDateFragment.sSelectedTransDate);
						} else {
							mHeadertext.setText(Offline_TransactionDateFragment.sSelectedTransDate
									+ RegionalConversion.getRegionalConversion(AppStrings.of)
									+ RegionalConversion.getRegionalConversion(Offline_SubMenuList.selectedSubMenuItem)
									+ " " + RegionalConversion.getRegionalConversion(AppStrings.reports));
						}

						for (int i = 0; i < arrayList.size(); i++) {
							getResponse = "";
							amount = "";
							if (Offline_ReportDateListFragment.sSelectedTransDate
									.equals(arrayList.get(i).getTransactionDate())) {

								if (arrayList.get(i).getLoanWithdrawal() != null) {
									getResponse = arrayList.get(i).getLoanWithdrawal();
									System.out.println("<<<<<getResponse>>>>>>>>>" + getResponse.toString());
									responseArr = getResponse.split("[~#]");

									Log.e("Arr length", responseArr.length+"");
									dateArr = String.valueOf(responseArr[responseArr.length - 2]).split("/");
									transDate = dateArr[1] + "/" + dateArr[0] + "/" + dateArr[2];
									if (Offline_TransactionDateFragment.sSelectedTransDate.equals(transDate)) {
										
										TableRow indv_Row = new TableRow(getActivity());
										TableRow.LayoutParams contentParams = new TableRow.LayoutParams(
												TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

										TextView fromDate_Text = new TextView(getActivity());
										fromDate_Text
												.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mLoanAccType)));
										fromDate_Text.setTypeface(LoginActivity.sTypeface);
										fromDate_Text.setPadding(10, 5, 10, 5);
										fromDate_Text.setTextColor(color.black);
										fromDate_Text.setLayoutParams(contentParams);
										indv_Row.addView(fromDate_Text);

										TextView fromDate_Text1 = new TextView(getActivity());
										fromDate_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(responseArr[4])));
										fromDate_Text1.setTypeface(LoginActivity.sTypeface);
										fromDate_Text1.setPadding(10, 5, 10, 5);
										fromDate_Text1.setTextColor(color.black);
										fromDate_Text1.setLayoutParams(contentParams);
										indv_Row.addView(fromDate_Text1);

										tableLayout.addView(indv_Row,
												new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

										View rullerView = new View(getActivity());
										rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
										rullerView.setBackgroundColor(Color.rgb(220, 220, 220));
										tableLayout.addView(rullerView);

										TableRow indv_Row1 = new TableRow(getActivity());
										TableRow.LayoutParams contentParams1 = new TableRow.LayoutParams(
												TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

										TextView toDate_Text = new TextView(getActivity());
										toDate_Text
												.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mTransferFromBank)));
										toDate_Text.setTypeface(LoginActivity.sTypeface);
										toDate_Text.setTextColor(color.black);
										toDate_Text.setLayoutParams(contentParams1);
										toDate_Text.setPadding(10, 5, 10, 5);
										indv_Row1.addView(toDate_Text);

										TextView toDate_Text1 = new TextView(getActivity());
										toDate_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(responseArr[5]+" - "+responseArr[3])));
										toDate_Text1.setTypeface(LoginActivity.sTypeface);
										toDate_Text1.setTextColor(color.black);
										toDate_Text1.setPadding(10, 5, 10, 5);
										toDate_Text1.setLayoutParams(contentParams1);
										indv_Row1.addView(toDate_Text1);

										tableLayout.addView(indv_Row1,
												new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

										View rullerView1 = new View(getActivity());
										rullerView1.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
										rullerView1.setBackgroundColor(Color.rgb(220, 220, 220));
										tableLayout.addView(rullerView1);

										if (!responseArr[6].equals("")) {
											TableRow indv_Row2 = new TableRow(getActivity());
											TableRow.LayoutParams contentParams2 = new TableRow.LayoutParams(
													TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

											TextView auditDate_Text = new TextView(getActivity());
											auditDate_Text.setText(
													GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mLoanaccSpinnerFloating)));
											auditDate_Text.setTypeface(LoginActivity.sTypeface);
											auditDate_Text.setTextColor(color.black);
											auditDate_Text.setPadding(10, 5, 10, 5);
											auditDate_Text.setLayoutParams(contentParams2);
											indv_Row2.addView(auditDate_Text);

											TextView auditDate_Text1 = new TextView(getActivity());
											auditDate_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(responseArr[6])));
											auditDate_Text1.setTypeface(LoginActivity.sTypeface);
											auditDate_Text1.setPadding(10, 5, 10, 5);
											auditDate_Text1.setLayoutParams(contentParams2);
											auditDate_Text1.setTextColor(color.black);
											indv_Row2.addView(auditDate_Text1);

											tableLayout.addView(indv_Row2,
													new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

											View rullerView2 = new View(getActivity());
											rullerView2.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
											rullerView2.setBackgroundColor(Color.rgb(220, 220, 220));
											tableLayout.addView(rullerView2);

										}
										
										TableRow indv_Row3 = new TableRow(getActivity());
										TableRow.LayoutParams contentParams3 = new TableRow.LayoutParams(
												TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

										TextView name_Text = new TextView(getActivity());
										name_Text.setText(
												GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mLoanaccWithdrawal)));
										name_Text.setTypeface(LoginActivity.sTypeface);
										name_Text.setTextColor(color.black);
										name_Text.setPadding(10, 5, 10, 5);
										name_Text.setLayoutParams(contentParams3);
										indv_Row3.addView(name_Text);

										TextView nameText1 = new TextView(getActivity());
										nameText1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(responseArr[0])));
										nameText1.setTypeface(LoginActivity.sTypeface);
										nameText1.setTextColor(color.black);
										nameText1.setPadding(10, 5, 10, 5);
										nameText1.setLayoutParams(contentParams3);
										indv_Row3.addView(nameText1);

										tableLayout.addView(indv_Row3,
												new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

										View rullerView3 = new View(getActivity());
										rullerView3.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
										rullerView3.setBackgroundColor(Color.rgb(220, 220, 220));
										tableLayout.addView(rullerView3);

										TableRow indv_Row31 = new TableRow(getActivity());
										TableRow.LayoutParams contentParams31 = new TableRow.LayoutParams(
												TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

										TextView name_Text1 = new TextView(getActivity());
										name_Text1.setText(
												GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mLoanaccExapenses)));
										name_Text1.setTypeface(LoginActivity.sTypeface);
										name_Text1.setTextColor(color.black);
										name_Text1.setPadding(10, 5, 10, 5);
										name_Text1.setLayoutParams(contentParams31);
										indv_Row31.addView(name_Text1);

										TextView nameText11 = new TextView(getActivity());
										nameText11.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(responseArr[1])));
										nameText11.setTypeface(LoginActivity.sTypeface);
										nameText11.setTextColor(color.black);
										nameText11.setPadding(10, 5, 10, 5);
										nameText11.setLayoutParams(contentParams31);
										indv_Row31.addView(nameText11);

										tableLayout.addView(indv_Row31,
												new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

										View rullerView31 = new View(getActivity());
										rullerView31.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
										rullerView31.setBackgroundColor(Color.rgb(220, 220, 220));
										tableLayout.addView(rullerView31);

									}
								}
							}
						}
						
				
					}else if (Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.mAccountToAccountTransfer)) {

						String[] fromBankArr,tobankArr,withdrawArr,expensesArr;
						String  fromBank = "",toBank = "",withdraw = "",expenses = "";
						
						for (int i = 0; i < arrayList.size(); i++) {
							getResponse = "";
							if (Offline_ReportDateListFragment.sSelectedTransDate
									.equals(arrayList.get(i).getTransactionDate())) {

								if (arrayList.get(i).getAcctoAccTransfer() != null) {
									getResponse = arrayList.get(i).getAcctoAccTransfer();
									System.out.println("<<<<<getResponse>>>>>>>>>" + getResponse.toString());
									responseArr = getResponse.split("[~#]");

									Log.e("Arr length", responseArr.length+"");
									dateArr = String.valueOf(responseArr[responseArr.length - 2]).split("/");
									transDate = dateArr[1] + "/" + dateArr[0] + "/" + dateArr[2];
									if (Offline_TransactionDateFragment.sSelectedTransDate.equals(transDate)) {
										fromBank = fromBank +responseArr[2]+"~";
										toBank = toBank + responseArr[3] + "~";
										withdraw = withdraw + responseArr[0] +"~";
										expenses = expenses +responseArr[1]+ "~";
									}
								}
							}
						}
						
						fromBankArr = fromBank.split("~");
						tobankArr = toBank.split("~");
						withdrawArr = withdraw.split("~");
						expensesArr = expenses.split("~");
						
						for (int i = 0; i < fromBankArr.length; i++) {
							System.out.println("FromBank     :"+fromBankArr[i].toString());
							System.out.println("ToBank     :"+tobankArr[i].toString());
							System.out.println("Withdraw Amount     :"+withdrawArr[i].toString());
							System.out.println("Expenses Amount     :"+expensesArr[i].toString());
							
						}
						if (mLanguagelocale.equalsIgnoreCase("English")) {
							mHeadertext.setText(RegionalConversion.getRegionalConversion(AppStrings.reports)
									+ RegionalConversion.getRegionalConversion(AppStrings.of)
									+ RegionalConversion.getRegionalConversion(Offline_SubMenuList.selectedSubMenuItem)
									+ " ON " + Offline_TransactionDateFragment.sSelectedTransDate);
						} else {
							mHeadertext.setText(Offline_TransactionDateFragment.sSelectedTransDate
									+ RegionalConversion.getRegionalConversion(AppStrings.of)
									+ RegionalConversion.getRegionalConversion(Offline_SubMenuList.selectedSubMenuItem)
									+ " " + RegionalConversion.getRegionalConversion(AppStrings.reports));
						}
						

						for (int i = 0; i < fromBankArr.length; i++) {

							TableRow indv_Row1 = new TableRow(getActivity());
							TableRow.LayoutParams contentParams1 = new TableRow.LayoutParams(
									TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

							TextView toDate_Text = new TextView(getActivity());
							toDate_Text
									.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mTransferFromBank)));
							toDate_Text.setTypeface(LoginActivity.sTypeface);
							toDate_Text.setTextColor(color.black);
							toDate_Text.setLayoutParams(contentParams1);
							toDate_Text.setPadding(10, 5, 10, 5);
							indv_Row1.addView(toDate_Text);

							TextView toDate_Text1 = new TextView(getActivity());
							toDate_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(fromBankArr[i])));
							toDate_Text1.setTypeface(LoginActivity.sTypeface);
							toDate_Text1.setTextColor(color.black);
							toDate_Text1.setPadding(10, 5, 10, 5);
							toDate_Text1.setLayoutParams(contentParams1);
							indv_Row1.addView(toDate_Text1);

							tableLayout.addView(indv_Row1,
									new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

							View rullerView1 = new View(getActivity());
							rullerView1.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
							rullerView1.setBackgroundColor(Color.rgb(220, 220, 220));
							tableLayout.addView(rullerView1);

							TableRow indv_Row2 = new TableRow(getActivity());
							TableRow.LayoutParams contentParams2 = new TableRow.LayoutParams(
									TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

							TextView auditDate_Text = new TextView(getActivity());
							auditDate_Text.setText(
									GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mTransferToBank)));
							auditDate_Text.setTypeface(LoginActivity.sTypeface);
							auditDate_Text.setTextColor(color.black);
							auditDate_Text.setPadding(10, 5, 10, 5);
							auditDate_Text.setLayoutParams(contentParams2);
							indv_Row2.addView(auditDate_Text);

							TextView auditDate_Text1 = new TextView(getActivity());
							auditDate_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(tobankArr[i])));
							auditDate_Text1.setTypeface(LoginActivity.sTypeface);
							auditDate_Text1.setPadding(10, 5, 10, 5);
							auditDate_Text1.setLayoutParams(contentParams2);
							auditDate_Text1.setTextColor(color.black);
							indv_Row2.addView(auditDate_Text1);

							tableLayout.addView(indv_Row2,
									new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

							View rullerView2 = new View(getActivity());
							rullerView2.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
							rullerView2.setBackgroundColor(Color.rgb(220, 220, 220));
							tableLayout.addView(rullerView2);

							TableRow indv_Row3 = new TableRow(getActivity());
							TableRow.LayoutParams contentParams3 = new TableRow.LayoutParams(
									TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

							TextView name_Text = new TextView(getActivity());
							name_Text.setText(
									GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mTransferAmount)));
							name_Text.setTypeface(LoginActivity.sTypeface);
							name_Text.setTextColor(color.black);
							name_Text.setPadding(10, 5, 10, 5);
							name_Text.setLayoutParams(contentParams3);
							indv_Row3.addView(name_Text);

							TextView nameText1 = new TextView(getActivity());
							nameText1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(withdrawArr[i])));
							nameText1.setTypeface(LoginActivity.sTypeface);
							nameText1.setTextColor(color.black);
							nameText1.setPadding(10, 5, 10, 5);
							nameText1.setLayoutParams(contentParams3);
							indv_Row3.addView(nameText1);

							tableLayout.addView(indv_Row3,
									new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

							View rullerView3 = new View(getActivity());
							rullerView3.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
							rullerView3.setBackgroundColor(Color.rgb(220, 220, 220));
							tableLayout.addView(rullerView3);

							TableRow indv_Row31 = new TableRow(getActivity());
							TableRow.LayoutParams contentParams31 = new TableRow.LayoutParams(
									TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

							TextView name_Text1 = new TextView(getActivity());
							name_Text1.setText(
									GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mTransferCharges)));
							name_Text1.setTypeface(LoginActivity.sTypeface);
							name_Text1.setTextColor(color.black);
							name_Text1.setPadding(10, 5, 10, 5);
							name_Text1.setLayoutParams(contentParams31);
							indv_Row31.addView(name_Text1);

							TextView nameText11 = new TextView(getActivity());
							nameText11.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(expensesArr[i])));
							nameText11.setTypeface(LoginActivity.sTypeface);
							nameText11.setTextColor(color.black);
							nameText11.setPadding(10, 5, 10, 5);
							nameText11.setLayoutParams(contentParams31);
							indv_Row31.addView(nameText11);

							tableLayout.addView(indv_Row31,
									new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

							View rullerView31 = new View(getActivity());
							rullerView31.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
							rullerView31.setBackgroundColor(Color.rgb(220, 220, 220));
							tableLayout.addView(rullerView31);

						}
					
					} else if (Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.mLoanAccTransfer)) {


						String[] fromBankArr,loanNameArr,loanIdArr,withdrawArr,expensesArr;
						String  fromBank = "",loanName = "",loanId = "",withdraw = "",expenses = "";
						
						for (int i = 0; i < arrayList.size(); i++) {
							getResponse = "";
							if (Offline_ReportDateListFragment.sSelectedTransDate
									.equals(arrayList.get(i).getTransactionDate())) {

								if (arrayList.get(i).getAccToLoanAccTransfer() != null) {
									getResponse = arrayList.get(i).getAccToLoanAccTransfer();
									System.out.println("<<<<<getResponse>>>>>>>>>" + getResponse.toString());
									responseArr = getResponse.split("[~#]");

									Log.e("Arr length", responseArr.length+"");
									dateArr = String.valueOf(responseArr[responseArr.length - 2]).split("/");
									transDate = dateArr[1] + "/" + dateArr[0] + "/" + dateArr[2];
									if (Offline_TransactionDateFragment.sSelectedTransDate.equals(transDate)) {
										fromBank = fromBank +responseArr[2]+"~";
										loanName = loanName + responseArr[3] + "~";
										loanId = loanId + responseArr[4] + "~";
										withdraw = withdraw + responseArr[0] +"~";
										expenses = expenses +responseArr[1]+ "~";
									}
								}
							}
						}
						
						fromBankArr = fromBank.split("~");
						loanNameArr = loanName.split("~");
						loanIdArr = loanId.split("~");
						withdrawArr = withdraw.split("~");
						expensesArr = expenses.split("~");
						
						System.out.println("--------"+fromBank.toString());
						System.out.println("--------"+loanName.toString());
						System.out.println("--------"+loanId.toString());
						System.out.println("--------"+withdraw.toString());
						System.out.println("--------"+expenses.toString());
						
						for (int i = 0; i < fromBankArr.length; i++) {
							System.out.println("FromBank     :"+fromBankArr[i].toString());
							System.out.println("Loan Name     :"+loanNameArr[i].toString());
							System.out.println("Loan Id     :"+loanIdArr[i].toString());
							System.out.println("Withdraw Amount     :"+withdrawArr[i].toString());
							System.out.println("Expenses Amount     :"+expensesArr[i].toString());
							
						}
						if (mLanguagelocale.equalsIgnoreCase("English")) {
							mHeadertext.setText(RegionalConversion.getRegionalConversion(AppStrings.reports)
									+ RegionalConversion.getRegionalConversion(AppStrings.of)
									+ RegionalConversion.getRegionalConversion(Offline_SubMenuList.selectedSubMenuItem)
									+ " ON " + Offline_TransactionDateFragment.sSelectedTransDate);
						} else {
							mHeadertext.setText(Offline_TransactionDateFragment.sSelectedTransDate
									+ RegionalConversion.getRegionalConversion(AppStrings.of)
									+ RegionalConversion.getRegionalConversion(Offline_SubMenuList.selectedSubMenuItem)
									+ " " + RegionalConversion.getRegionalConversion(AppStrings.reports));
						}
						

						for (int i = 0; i < fromBankArr.length; i++) {

							TableRow indv_Row1 = new TableRow(getActivity());
							TableRow.LayoutParams contentParams1 = new TableRow.LayoutParams(
									TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

							TextView toDate_Text = new TextView(getActivity());
							toDate_Text
									.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mTransferFromBank)));
							toDate_Text.setTypeface(LoginActivity.sTypeface);
							toDate_Text.setTextColor(color.black);
							toDate_Text.setLayoutParams(contentParams1);
							toDate_Text.setPadding(10, 5, 10, 5);
							indv_Row1.addView(toDate_Text);

							TextView toDate_Text1 = new TextView(getActivity());
							toDate_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(fromBankArr[i])));
							toDate_Text1.setTypeface(LoginActivity.sTypeface);
							toDate_Text1.setTextColor(color.black);
							toDate_Text1.setPadding(10, 5, 10, 5);
							toDate_Text1.setLayoutParams(contentParams1);
							indv_Row1.addView(toDate_Text1);

							tableLayout.addView(indv_Row1,
									new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

							View rullerView1 = new View(getActivity());
							rullerView1.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
							rullerView1.setBackgroundColor(Color.rgb(220, 220, 220));
							tableLayout.addView(rullerView1);

							TableRow indv_Row2 = new TableRow(getActivity());
							TableRow.LayoutParams contentParams2 = new TableRow.LayoutParams(
									TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

							TextView auditDate_Text = new TextView(getActivity());
							auditDate_Text.setText(
									GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mLoanName)));
							auditDate_Text.setTypeface(LoginActivity.sTypeface);
							auditDate_Text.setTextColor(color.black);
							auditDate_Text.setPadding(10, 5, 10, 5);
							auditDate_Text.setLayoutParams(contentParams2);
							indv_Row2.addView(auditDate_Text);

							TextView auditDate_Text1 = new TextView(getActivity());
							auditDate_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(loanNameArr[i])));
							auditDate_Text1.setTypeface(LoginActivity.sTypeface);
							auditDate_Text1.setPadding(10, 5, 10, 5);
							auditDate_Text1.setLayoutParams(contentParams2);
							auditDate_Text1.setTextColor(color.black);
							indv_Row2.addView(auditDate_Text1);

							tableLayout.addView(indv_Row2,
									new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

							View rullerView2 = new View(getActivity());
							rullerView2.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
							rullerView2.setBackgroundColor(Color.rgb(220, 220, 220));
							tableLayout.addView(rullerView2);

							TableRow indv_Row21 = new TableRow(getActivity());
							TableRow.LayoutParams contentParams21 = new TableRow.LayoutParams(
									TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

							TextView auditDate_Text11 = new TextView(getActivity());
							auditDate_Text11.setText(
									GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mLoanId)));
							auditDate_Text11.setTypeface(LoginActivity.sTypeface);
							auditDate_Text11.setTextColor(color.black);
							auditDate_Text11.setPadding(10, 5, 10, 5);
							auditDate_Text11.setLayoutParams(contentParams21);
							indv_Row21.addView(auditDate_Text11);

							TextView auditDate_Text111 = new TextView(getActivity());
							auditDate_Text111.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(loanIdArr[i])));
							auditDate_Text111.setTypeface(LoginActivity.sTypeface);
							auditDate_Text111.setPadding(10, 5, 10, 5);
							auditDate_Text111.setLayoutParams(contentParams21);
							auditDate_Text111.setTextColor(color.black);
							indv_Row21.addView(auditDate_Text111);

							tableLayout.addView(indv_Row21,
									new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

							View rullerView21 = new View(getActivity());
							rullerView21.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
							rullerView21.setBackgroundColor(Color.rgb(220, 220, 220));
							tableLayout.addView(rullerView21);

							TableRow indv_Row3 = new TableRow(getActivity());
							TableRow.LayoutParams contentParams3 = new TableRow.LayoutParams(
									TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

							TextView name_Text = new TextView(getActivity());
							name_Text.setText(
									GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mTransferAmount)));
							name_Text.setTypeface(LoginActivity.sTypeface);
							name_Text.setTextColor(color.black);
							name_Text.setPadding(10, 5, 10, 5);
							name_Text.setLayoutParams(contentParams3);
							indv_Row3.addView(name_Text);

							TextView nameText1 = new TextView(getActivity());
							nameText1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(withdrawArr[i])));
							nameText1.setTypeface(LoginActivity.sTypeface);
							nameText1.setTextColor(color.black);
							nameText1.setPadding(10, 5, 10, 5);
							nameText1.setLayoutParams(contentParams3);
							indv_Row3.addView(nameText1);

							tableLayout.addView(indv_Row3,
									new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

							View rullerView3 = new View(getActivity());
							rullerView3.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
							rullerView3.setBackgroundColor(Color.rgb(220, 220, 220));
							tableLayout.addView(rullerView3);

							TableRow indv_Row31 = new TableRow(getActivity());
							TableRow.LayoutParams contentParams31 = new TableRow.LayoutParams(
									TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

							TextView name_Text1 = new TextView(getActivity());
							name_Text1.setText(
									GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mTransferCharges)));
							name_Text1.setTypeface(LoginActivity.sTypeface);
							name_Text1.setTextColor(color.black);
							name_Text1.setPadding(10, 5, 10, 5);
							name_Text1.setLayoutParams(contentParams31);
							indv_Row31.addView(name_Text1);

							TextView nameText11 = new TextView(getActivity());
							nameText11.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(expensesArr[i])));
							nameText11.setTypeface(LoginActivity.sTypeface);
							nameText11.setTextColor(color.black);
							nameText11.setPadding(10, 5, 10, 5);
							nameText11.setLayoutParams(contentParams31);
							indv_Row31.addView(nameText11);

							tableLayout.addView(indv_Row31,
									new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

							View rullerView31 = new View(getActivity());
							rullerView31.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
							rullerView31.setBackgroundColor(Color.rgb(220, 220, 220));
							tableLayout.addView(rullerView31);

						}
					
					
					}

				} else if (OfflineReports_TransactionList.selectedItem.equals(AppStrings.InternalLoanDisbursement)) {
					for (int i = 0; i < arrayList.size(); i++) {
						getResponse = "";
						amount = "";

						if (Offline_ReportDateListFragment.sSelectedTransDate
								.equals(arrayList.get(i).getTransactionDate())) {

							if (arrayList.get(i).getPersonalloandisbursement() != null) {
								getResponse = arrayList.get(i).getPersonalloandisbursement();
								System.out.println("<<<<<getResponse>>>>>>>>>" + getResponse.toString());
								responseArr = getResponse.split("[~#]");

								dateArr = String.valueOf(responseArr[responseArr.length - 2]).split("/");
								transDate = dateArr[1] + "/" + dateArr[0] + "/" + dateArr[2];
								if (Offline_TransactionDateFragment.sSelectedTransDate.equals(transDate)) {
									for (int j = 0; j < responseArr.length - 2; j = j + 4) {
										amount = amount + responseArr[j + 1] + "~";

									}
									amountsArr = amount.split("~");
									for (int k = 0; k < sumAmount.length; k++) {

										System.out.println(" Amount :" + amountsArr[k] + " pos :" + k);
										sumAmount[k] += Integer.parseInt(amountsArr[k]);
										Log.v("SUM AMOUNT", sumAmount[k] + "");
									}
								}
							}
						}
					}
					if (mLanguagelocale.equalsIgnoreCase("English")) {
						mHeadertext.setText(RegionalConversion.getRegionalConversion(AppStrings.reports)
								+ RegionalConversion.getRegionalConversion(AppStrings.of)
								+ RegionalConversion.getRegionalConversion(OfflineReports_TransactionList.selectedItem)
								+ " ON " + Offline_TransactionDateFragment.sSelectedTransDate);
					} else {
						mHeadertext.setText(Offline_TransactionDateFragment.sSelectedTransDate
								+ RegionalConversion.getRegionalConversion(AppStrings.of)
								+ RegionalConversion.getRegionalConversion(OfflineReports_TransactionList.selectedItem)
								+ " " + RegionalConversion.getRegionalConversion(AppStrings.reports));
					}
					

					TextView memName = new TextView(getActivity());
					memName.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.memberName)));
					memName.setTypeface(LoginActivity.sTypeface);
					memName.setPadding(60, 5, 10, 5);
					memName.setTextColor(Color.WHITE);
					memName.setLayoutParams(headerParams);
					headerRow.addView(memName);

					TextView amount = new TextView(getActivity());
					amount.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.amount)));
					amount.setTypeface(LoginActivity.sTypeface);
					amount.setTextColor(Color.WHITE);
					amount.setPadding(10, 5, 60, 5);
					amount.setGravity(Gravity.RIGHT);
					amount.setLayoutParams(headerParams);
					headerRow.addView(amount);

					headerTable.addView(headerRow);// ,
					/*
					 * new TableLayout.LayoutParams( LayoutParams.MATCH_PARENT,
					 * LayoutParams.WRAP_CONTENT));
					 */

					for (int i1 = 0; i1 < SelectedGroupsTask.member_Name.size(); i1++) {

						TableRow indv_Row = new TableRow(getActivity());
						TableRow.LayoutParams contentParams = new TableRow.LayoutParams(
								TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);
						contentParams.setMargins(10, 5, 10, 5);

						TextView memberName_Text = new TextView(getActivity());
						memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
								String.valueOf(SelectedGroupsTask.member_Name.elementAt(i1))));
						memberName_Text.setTypeface(LoginActivity.sTypeface);
						memberName_Text.setTextColor(color.black);
						memberName_Text.setPadding(50, 5, 10, 5);
						// memberName_Text.setLayoutParams(contentParams);
						indv_Row.addView(memberName_Text, contentParams);

						TextView confirm_values = new TextView(getActivity());
						confirm_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sumAmount[i1])));
						confirm_values.setTextColor(color.black);
						confirm_values.setPadding(10, 5, 70, 5);
						confirm_values.setGravity(Gravity.RIGHT);
						// memberName_Text.setLayoutParams(contentParams);
						indv_Row.addView(confirm_values, contentParams);

						tableLayout.addView(indv_Row,
								new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

						View rullerView = new View(getActivity());
						rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
						rullerView.setBackgroundColor(Color.rgb(220, 220, 220));
						tableLayout.addView(rullerView);

					}

				} else if (Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.Attendance)) {
					String name = "";
					String[] memberName;
					for (int i = 0; i < arrayList.size(); i++) {
						getResponse = "";
						amount = "";
						if (Offline_ReportDateListFragment.sSelectedTransDate
								.equals(arrayList.get(i).getTransactionDate())) {

							if (arrayList.get(i).getAttendance() != null) {
								getResponse = arrayList.get(i).getAttendance();
								System.out.println("<<<<<<<<<<<<getResponse>>>>>>>>>" + getResponse.toString());
								responseArr = getResponse.split("[~#]");
								dateArr = String.valueOf(responseArr[responseArr.length - 2]).split("/");
								transDate = dateArr[1] + "/" + dateArr[0] + "/" + dateArr[2];
								if (Offline_TransactionDateFragment.sSelectedTransDate.equals(transDate)) {
									for (int j = 0; j < responseArr.length - 2; j++) {
										if (j % 2 != 0) {
											amount = amount + responseArr[j] + "~";
										}
									}
									System.out.println("<<-----amt--------->>" + amount.toString());
									amountsArr = amount.split("~");

									for (int k = 0; k < amountsArr.length; k++) {
										if (!amountsArr[k].equals("0")) {
											name = name + SelectedGroupsTask.member_Name.elementAt(k).toString() + "~";
										}
									}
								}

							}
						}
					}
					System.out.println("-------------NAME------------" + name.toString());
					memberName = name.split("~");
					List<String> list = Arrays.asList(memberName);
					Set<String> set = new LinkedHashSet<String>(list);
					String[] memberNameList = new String[set.size()];
					set.toArray(memberNameList);

					for (int j = 0; j < memberNameList.length; j++) {
						System.out.println(
								"----------MEMBER NAME LIST---------" + memberNameList[j].toString() + " i pos " + j);
					}

					if (mLanguagelocale.equalsIgnoreCase("English")) {
						mHeadertext.setText(RegionalConversion.getRegionalConversion(AppStrings.reports)
								+ RegionalConversion.getRegionalConversion(AppStrings.of)
								+ RegionalConversion.getRegionalConversion(Offline_SubMenuList.selectedSubMenuItem)
								+ " ON " + Offline_TransactionDateFragment.sSelectedTransDate);
					} else {
						mHeadertext.setText(Offline_TransactionDateFragment.sSelectedTransDate
								+ RegionalConversion.getRegionalConversion(AppStrings.of)
								+ RegionalConversion.getRegionalConversion(Offline_SubMenuList.selectedSubMenuItem)
								+ " " + RegionalConversion.getRegionalConversion(AppStrings.reports));
					}
					

					CheckBox checkBox[] = new CheckBox[memberNameList.length];

					for (int i1 = 0; i1 < memberNameList.length; i1++) {

						TableRow indv_Row = new TableRow(getActivity());
						TableRow.LayoutParams contentParams = new TableRow.LayoutParams(
								TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);
						contentParams.setMargins(10, 5, 10, 5);

						checkBox[i1] = new CheckBox(getActivity());
						checkBox[i1]
								.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(memberNameList[i1])));
						checkBox[i1].setChecked(true);
						checkBox[i1].setClickable(false);
						checkBox[i1].setTextColor(color.black);
						checkBox[i1].setTypeface(LoginActivity.sTypeface);
						indv_Row.addView(checkBox[i1]);

						tableLayout.addView(indv_Row,
								new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

						View rullerView = new View(getActivity());
						rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
						rullerView.setBackgroundColor(Color.rgb(220, 220, 220));
						tableLayout.addView(rullerView);

					}

				} else if (Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.MinutesofMeeting)) {
					if (mLanguagelocale.equalsIgnoreCase("English")) {
						mHeadertext.setText(RegionalConversion.getRegionalConversion(AppStrings.reports)
								+ RegionalConversion.getRegionalConversion(AppStrings.of)
								+ RegionalConversion.getRegionalConversion(Offline_SubMenuList.selectedSubMenuItem)
								+ " ON " + Offline_TransactionDateFragment.sSelectedTransDate);
					} else {
						mHeadertext.setText(Offline_TransactionDateFragment.sSelectedTransDate
								+ RegionalConversion.getRegionalConversion(AppStrings.of)
								+ RegionalConversion.getRegionalConversion(Offline_SubMenuList.selectedSubMenuItem)
								+ " " + RegionalConversion.getRegionalConversion(AppStrings.reports));
					}
					

					String listItem = "";
					for (int i = 0; i < arrayList.size(); i++) {
						if (Offline_ReportDateListFragment.sSelectedTransDate
								.equals(arrayList.get(i).getTransactionDate())) {

							if (arrayList.get(i).getMinutesofmeeting() != null) {
								getResponse = "";

								getResponse = arrayList.get(i).getMinutesofmeeting();
								System.out.println("<<<<<<<<<<<<getResponse>>>>>>>>>>@@@@@@@@@@@@" + getResponse.toString());
								responseArr = getResponse.split("[~#]");
								dateArr = String.valueOf(responseArr[responseArr.length - 2]).split("/");
								transDate = dateArr[1] + "/" + dateArr[0] + "/" + dateArr[2];
								if (Offline_TransactionDateFragment.sSelectedTransDate.equals(transDate)) {
									System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++");
								//	for (int j = 0; j < responseArr.length - 3; j++) {
									for (int j = 0; j < responseArr.length - 2; j++) {
										responseId[j] = responseArr[j];
										System.out.println("----------responseId []------------" + responseId[j]);

									}

									for (int k = 0; k < minutesId.length; k++) {
										for (int j = 0; j < responseId.length; j++) {
											if (minutesId[k].equals(responseId[j])) {
												Log.v("MInitues list items :", minutesName[k]+"");
												listItem = listItem + minutesName[k] + "~";
											}
										}
									}
								}
							}
						}
					}

					System.out.println("--------------ITEMS-----------" + listItem);
					listItems = listItem.split("~");

					for (int j = 0; j < listItems.length; j++) {
						System.out.println("------------LIST ITEMS----------" + listItems[j]);
					}
					List<String> list = Arrays.asList(listItems);
					Set<String> set = new LinkedHashSet<String>(list);
					String[] minutesItems = new String[set.size()];
					set.toArray(minutesItems);

					for (int j = 0; j < minutesItems.length; j++) {
						System.out.println(
								"----------MINUTES LIST ITEMS---------" + minutesItems[j].toString() + " i pos " + j);
					}

					CheckBox checkBox[] = new CheckBox[minutesItems.length];

					for (int j = 0; j < minutesItems.length; j++) {
						TableRow indv_valueRow = new TableRow(getActivity());

						TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
								LayoutParams.WRAP_CONTENT, 1f);
						contentParams.setMargins(10, 5, 10, 5);

						checkBox[j] = new CheckBox(getActivity());
						checkBox[j].setText(GetSpanText.getSpanString(getActivity(), String.valueOf(minutesItems[j])));
						checkBox[j].setChecked(true);
						checkBox[j].setClickable(false);
						checkBox[j].setTextColor(color.black);
						checkBox[j].setTypeface(LoginActivity.sTypeface);
						indv_valueRow.addView(checkBox[j], contentParams);

						tableLayout.addView(indv_valueRow,
								new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
						View rullerView = new View(getActivity());
						rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
						rullerView.setBackgroundColor(Color.rgb(220, 220, 220));
						tableLayout.addView(rullerView);

					}

				} else if (Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.auditing)) {
					String[] fromDate, toDate, auditDate, auditorName;
					String fDate = "", tDate = "", auditingDate = "", name = "";
					for (int i = 0; i < arrayList.size(); i++) {
						getResponse = "";
						// amount = "";
						if (Offline_ReportDateListFragment.sSelectedTransDate
								.equals(arrayList.get(i).getTransactionDate())) {

							if (arrayList.get(i).getAuditting() != null) {
								getResponse = arrayList.get(i).getAuditting();
								System.out.println("<<<<<<<<<<<<getResponse>>>>>>>>>>" + getResponse.toString());
								responseArr = getResponse.split("[~#]");
								dateArr = String.valueOf(responseArr[responseArr.length - 2]).split("/");
								transDate = dateArr[1] + "/" + dateArr[0] + "/" + dateArr[2];
								if (Offline_TransactionDateFragment.sSelectedTransDate.equals(transDate)) {
									fDate = fDate + responseArr[0] + "~";
									tDate = tDate + responseArr[1] + "~";
									auditingDate = auditingDate + responseArr[2] + "~";
									name = name + responseArr[3] + "~";
								}
							}
						}
					}
					System.out.println("--------------fDate-----------" + fDate.toString());
					fromDate = fDate.split("~");
					toDate = tDate.split("~");
					auditDate = auditingDate.split("~");
					auditorName = name.split("~");
					for (int k = 0; k < fromDate.length; k++) {
						System.out.println("--------FROM DATE []----------" + fromDate[k].toString());
						System.out.println("--------TO DATE []----------" + toDate[k].toString());
						System.out.println("--------AUDITING DATE []----------" + auditDate[k].toString());
						System.out.println("--------AUDITOR NAME []----------" + auditorName[k].toString());
					}

					if (mLanguagelocale.equalsIgnoreCase("English")) {
						mHeadertext.setText(RegionalConversion.getRegionalConversion(AppStrings.reports)
								+ RegionalConversion.getRegionalConversion(AppStrings.of)
								+ RegionalConversion.getRegionalConversion(Offline_SubMenuList.selectedSubMenuItem)
								+ " ON " + Offline_TransactionDateFragment.sSelectedTransDate);
					} else {
						mHeadertext.setText(Offline_TransactionDateFragment.sSelectedTransDate
								+ RegionalConversion.getRegionalConversion(AppStrings.of)
								+ RegionalConversion.getRegionalConversion(Offline_SubMenuList.selectedSubMenuItem)
								+ " " + RegionalConversion.getRegionalConversion(AppStrings.reports));
					}
					

					for (int i = 0; i < fromDate.length; i++) {
						TableRow indv_Row = new TableRow(getActivity());
						TableRow.LayoutParams contentParams = new TableRow.LayoutParams(
								TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

						TextView fromDate_Text = new TextView(getActivity());
						fromDate_Text
								.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.FromDate)));
						fromDate_Text.setTypeface(LoginActivity.sTypeface);
						fromDate_Text.setPadding(10, 5, 10, 5);
						fromDate_Text.setTextColor(color.black);
						fromDate_Text.setLayoutParams(contentParams);
						indv_Row.addView(fromDate_Text);

						TextView fromDate_Text1 = new TextView(getActivity());
						fromDate_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(fromDate[i])));
						fromDate_Text1.setTypeface(LoginActivity.sTypeface);
						fromDate_Text1.setPadding(10, 5, 10, 5);
						fromDate_Text1.setTextColor(color.black);
						fromDate_Text1.setLayoutParams(contentParams);
						indv_Row.addView(fromDate_Text1);

						tableLayout.addView(indv_Row,
								new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

						View rullerView = new View(getActivity());
						rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
						rullerView.setBackgroundColor(Color.rgb(220, 220, 220));
						tableLayout.addView(rullerView);

						TableRow indv_Row1 = new TableRow(getActivity());
						TableRow.LayoutParams contentParams1 = new TableRow.LayoutParams(
								TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

						TextView toDate_Text = new TextView(getActivity());
						toDate_Text
								.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.ToDate)));
						toDate_Text.setTypeface(LoginActivity.sTypeface);
						toDate_Text.setTextColor(color.black);
						toDate_Text.setLayoutParams(contentParams1);
						toDate_Text.setPadding(10, 5, 10, 5);
						indv_Row1.addView(toDate_Text);

						TextView toDate_Text1 = new TextView(getActivity());
						toDate_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(toDate[i])));
						toDate_Text1.setTypeface(LoginActivity.sTypeface);
						toDate_Text1.setTextColor(color.black);
						toDate_Text1.setPadding(10, 5, 10, 5);
						toDate_Text1.setLayoutParams(contentParams1);
						indv_Row1.addView(toDate_Text1);

						tableLayout.addView(indv_Row1,
								new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

						View rullerView1 = new View(getActivity());
						rullerView1.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
						rullerView1.setBackgroundColor(Color.rgb(220, 220, 220));
						tableLayout.addView(rullerView1);

						TableRow indv_Row2 = new TableRow(getActivity());
						TableRow.LayoutParams contentParams2 = new TableRow.LayoutParams(
								TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

						TextView auditDate_Text = new TextView(getActivity());
						auditDate_Text.setText(
								GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.auditing_Date)));
						auditDate_Text.setTypeface(LoginActivity.sTypeface);
						auditDate_Text.setTextColor(color.black);
						auditDate_Text.setPadding(10, 5, 10, 5);
						auditDate_Text.setLayoutParams(contentParams2);
						indv_Row2.addView(auditDate_Text);

						TextView auditDate_Text1 = new TextView(getActivity());
						auditDate_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(auditDate[i])));
						auditDate_Text1.setTypeface(LoginActivity.sTypeface);
						auditDate_Text1.setPadding(10, 5, 10, 5);
						auditDate_Text1.setLayoutParams(contentParams2);
						auditDate_Text1.setTextColor(color.black);
						indv_Row2.addView(auditDate_Text1);

						tableLayout.addView(indv_Row2,
								new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

						View rullerView2 = new View(getActivity());
						rullerView2.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
						rullerView2.setBackgroundColor(Color.rgb(220, 220, 220));
						tableLayout.addView(rullerView2);

						TableRow indv_Row3 = new TableRow(getActivity());
						TableRow.LayoutParams contentParams3 = new TableRow.LayoutParams(
								TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

						TextView name_Text = new TextView(getActivity());
						name_Text.setText(
								GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.auditor_Name)));
						name_Text.setTypeface(LoginActivity.sTypeface);
						name_Text.setTextColor(color.black);
						name_Text.setPadding(10, 5, 10, 5);
						name_Text.setLayoutParams(contentParams3);
						indv_Row3.addView(name_Text);

						TextView nameText1 = new TextView(getActivity());
						nameText1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(auditorName[i])));
						nameText1.setTypeface(LoginActivity.sTypeface);
						nameText1.setTextColor(color.black);
						nameText1.setPadding(10, 5, 10, 5);
						nameText1.setLayoutParams(contentParams3);
						indv_Row3.addView(nameText1);

						tableLayout.addView(indv_Row3,
								new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

						View rullerView3 = new View(getActivity());
						rullerView3.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
						rullerView3.setBackgroundColor(Color.rgb(220, 220, 220));
						tableLayout.addView(rullerView3);

					}

				} else if (Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.training)) {
					String[] trainingDate, trainingType;
					String traindate = "", type, tempType = "";
					for (int i = 0; i < arrayList.size(); i++) {
						getResponse = "";
						type = "";
						if (Offline_ReportDateListFragment.sSelectedTransDate
								.equals(arrayList.get(i).getTransactionDate())) {

							if (arrayList.get(i).getTraining() != null) {
								getResponse = arrayList.get(i).getTraining();
								System.out.println("<<<<<<<<<<<<getResponse>>>>>>>>>>" + getResponse.toString());
								responseArr = getResponse.split("[~#]");
								dateArr = String.valueOf(responseArr[responseArr.length - 2]).split("/");
								transDate = dateArr[1] + "/" + dateArr[0] + "/" + dateArr[2];
								if (Offline_TransactionDateFragment.sSelectedTransDate.equals(transDate)) {
									for (int j = 0; j < responseArr.length; j++) {
										System.out.println("-----------" + responseArr[j]);
									}

									traindate = traindate + responseArr[0] + "~";
									for (int j = 1; j < responseArr.length - 3; j++) {// need
																						// to
																						// change
																						// length
																						// 3
										type = type + responseArr[j] + ",";
									}
									tempType = tempType + type + "#";
								}
							}
						}
					}
					System.out.println("----------Date----------" + traindate.toString());
					System.out.println("------------Type -----------------" + tempType.toString());
					trainingDate = traindate.split("~");
					trainingType = tempType.split("#");

					if (mLanguagelocale.equalsIgnoreCase("English")) {
						mHeadertext.setText(RegionalConversion.getRegionalConversion(AppStrings.reports)
								+ RegionalConversion.getRegionalConversion(AppStrings.of)
								+ RegionalConversion.getRegionalConversion(Offline_SubMenuList.selectedSubMenuItem)
								+ " ON " + Offline_TransactionDateFragment.sSelectedTransDate);
					} else {
						mHeadertext.setText(Offline_TransactionDateFragment.sSelectedTransDate
								+ RegionalConversion.getRegionalConversion(AppStrings.of)
								+ RegionalConversion.getRegionalConversion(Offline_SubMenuList.selectedSubMenuItem)
								+ " " + RegionalConversion.getRegionalConversion(AppStrings.reports));
					}
					

					for (int i1 = 0; i1 < trainingDate.length; i1++) {
						String training[] = trainingType[i1].split(",");

						TableRow indv_Row = new TableRow(getActivity());
						TableRow.LayoutParams contentParams = new TableRow.LayoutParams(
								TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);
						// contentParams.setMargins(10, 5, 10, 5);

						TextView DateText = new TextView(getActivity());
						DateText.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(trainingDate[i1])));
						DateText.setTypeface(LoginActivity.sTypeface);
						DateText.setPadding(10, 5, 10, 5);
						DateText.setTextColor(color.black);
						DateText.setLayoutParams(contentParams);
						indv_Row.addView(DateText);

						TextView typeText = new TextView(getActivity());
						typeText.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(training[0])));
						typeText.setTypeface(LoginActivity.sTypeface);
						typeText.setTextColor(color.black);
						typeText.setPadding(10, 5, 10, 5);
						typeText.setLayoutParams(contentParams);
						indv_Row.addView(typeText);

						tableLayout.addView(indv_Row,
								new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

						View rullerView = new View(getActivity());
						rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
						rullerView.setBackgroundColor(Color.rgb(220, 220, 220));
						tableLayout.addView(rullerView);

						for (int j = 1; j < training.length; j++) {

							TableRow indv_Row1 = new TableRow(getActivity());
							TableRow.LayoutParams contentParams1 = new TableRow.LayoutParams(
									TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);
							// contentParams1.setMargins(10, 5, 10, 5);

							TextView emptyText = new TextView(getActivity());
							emptyText.setPadding(20, 5, 10, 5);
							indv_Row1.addView(emptyText, contentParams1);

							TextView typeName = new TextView(getActivity());
							typeName.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(training[j])));
							typeName.setTypeface(LoginActivity.sTypeface);
							typeName.setTextColor(color.black);
							typeName.setPadding(10, 5, 20, 5);
							indv_Row1.addView(typeName, contentParams1);

							tableLayout.addView(indv_Row1,
									new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
							View rullerView1 = new View(getActivity());
							rullerView1
									.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
							rullerView1.setBackgroundColor(Color.rgb(220, 220, 220));
							tableLayout.addView(rullerView1);
						}

					}

				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		} else {
			/*Toast.makeText(getActivity(), "There is no Local DB Values", Toast.LENGTH_LONG).show();*/
			TastyToast.makeText(getActivity(), "There is no Local DB Values", TastyToast.LENGTH_SHORT, TastyToast.WARNING);
		}
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		SBus.INST.unRegister(this);
	}

}
