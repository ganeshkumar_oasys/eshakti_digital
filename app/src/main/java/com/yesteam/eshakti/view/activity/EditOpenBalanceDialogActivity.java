package com.yesteam.eshakti.view.activity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog.OnDateSetListener;
import com.yesteam.eshakti.utils.CalculateDateUtils;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.views.ButtonFlat;
import com.yesteam.eshakti.webservices.Get_Edit_OpeningbalanceWebservice;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class EditOpenBalanceDialogActivity extends AppCompatActivity implements OnDateSetListener {

	private String mLanguagelocale = "";
	Locale locale;
	String balsheetDate = "";
	public static String mCheck_EditOpening_BalncesheetChange = ""; 
	

	public EditOpenBalanceDialogActivity() {
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_openingbalance_dialog);
		try {
			if (Get_Edit_OpeningbalanceWebservice.mEdit_OpeningBalance_Values_Check[5] != null) {
				EShaktiApplication.setEditOBTransactionDate(
						Get_Edit_OpeningbalanceWebservice.mEdit_OpeningBalance_Values_Check[5]);

				String[] bal_date = EShaktiApplication.getEditOBTransactionDate().split("/");
				balsheetDate = bal_date[1] + "-" + bal_date[0] + "-" + bal_date[2];
			}
			TextView lastTr = (TextView) findViewById(R.id.edit_dialog_TransactionDate);

			lastTr.setText(
					RegionalConversion.getRegionalConversion(AppStrings.mBalanceSheetDate) + " : " + balsheetDate);
			lastTr.setTypeface(LoginActivity.sTypeface);

			TextView dialogMsg = (TextView) findViewById(R.id.edit_dialog_continueDate);
			dialogMsg.setText(// CalendarFragment.sDashboardDate
					RegionalConversion.getRegionalConversion(AppStrings.mContinueWithDate));
			dialogMsg.setTypeface(LoginActivity.sTypeface);

			ButtonFlat okButton = (ButtonFlat) findViewById(R.id.edit_fragment_Yes_button);
			okButton.setText(RegionalConversion.getRegionalConversion(AppStrings.dialogOk));
			okButton.setTypeface(LoginActivity.sTypeface);
			okButton.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					EShaktiApplication.setEditOBTransDate(true);
					EShaktiApplication.setEditOpeningScreen(true);
					
					publicValues.mEdit_OB_Sendtoserver_Savings = null;
					publicValues.mEdit_OB_Sendtoserver_InternalLoan = null;
					publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan = null;
					publicValues.mEdit_OB_Sendtoserver_Bank_Group_Loan = null;
					publicValues.mEdit_OB_Sendtiserver_Bank_CashinHandDetails = null;
					
					EShaktiApplication.setAuditFragment(true);
					
					onSetCalendarValues();
					try{
						Calendar calender = Calendar.getInstance();

						SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
						String formattedDate = df.format(calender.getTime());
						Log.e("Device Date  =  ", formattedDate+"");
						
						String mBalanceSheetDate = SelectedGroupsTask.sBalanceSheetDate_Response; //dd/MM/yyyy
						String formatted_balancesheetDate = mBalanceSheetDate.replace("/", "-");
						Log.e("Balancesheet Date = ", formatted_balancesheetDate+"");

						Date balanceDate = df.parse(formatted_balancesheetDate);
						Date systemDate = df.parse(formattedDate);
						

						if (balanceDate.compareTo(systemDate) < 0 || balanceDate.compareTo(systemDate) == 0) {
							mCheck_EditOpening_BalncesheetChange = "1"; 
							calendarDialogShow();
						} else {
							TastyToast.makeText(EditOpenBalanceDialogActivity.this, "PLEASE SET YOUR DEVICE DATE CORRECTLY", TastyToast.LENGTH_SHORT, TastyToast.ERROR);
							EShaktiApplication.setNextMonthLastDate(null);
							EShaktiApplication.setCalendarDateVisibleFlag(true);
						}
						}catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}

				}
			});

			ButtonFlat noButton = (ButtonFlat) findViewById(R.id.edit_fragment_No_button);
			noButton.setText(RegionalConversion.getRegionalConversion(AppStrings.dialogNo));
			noButton.setTypeface(LoginActivity.sTypeface);
			noButton.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					try {
						
						publicValues.mEdit_OB_Sendtoserver_Savings = null;
						publicValues.mEdit_OB_Sendtoserver_InternalLoan = null;
						publicValues.mEdit_OB_Sendtoserver_Bank_MemberLoan = null;
						publicValues.mEdit_OB_Sendtoserver_Bank_Group_Loan = null;
						publicValues.mEdit_OB_Sendtiserver_Bank_CashinHandDetails = null;

						Intent intent = new Intent(EditOpenBalanceDialogActivity.this,
								EditOpeningBalanceSavingsActivity.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
						overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

						finish();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			});

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	private void calendarDialogShow() {
		try {
			// TODO Auto-generated method stub
			mLanguagelocale = PrefUtils.getUserlangcode();

			if (mLanguagelocale.equalsIgnoreCase("English")) {
				locale = new Locale("en");
			} else if (mLanguagelocale.equalsIgnoreCase("Tamil")) {
				locale = new Locale("ta");
			} else if (mLanguagelocale.equalsIgnoreCase("Hindi")) {
				locale = new Locale("hi");
			} else if (mLanguagelocale.equalsIgnoreCase("Marathi")) {
				locale = new Locale("ma");
			} else if (mLanguagelocale.equalsIgnoreCase("Malayalam")) {
				locale = new Locale("ml");
			} else if (mLanguagelocale.equalsIgnoreCase("Kannada")) {
				locale = new Locale("kn");
			} else if (mLanguagelocale.equalsIgnoreCase("Bengali")) {
				locale = new Locale("bn");
			} else if (mLanguagelocale.equalsIgnoreCase("Gujarati")) {
				locale = new Locale("gu");
			} else if (mLanguagelocale.equalsIgnoreCase("Punjabi")) {
				locale = new Locale("pa");
			} else if (mLanguagelocale.equalsIgnoreCase("Assamese")) {
				locale = new Locale("as");
			} else {
				locale = new Locale("en");
			}
			locale = new Locale("en");

			Locale.setDefault(locale);
			Configuration config = new Configuration();
			config.locale = locale;
			this.getResources().updateConfiguration(config, this.getResources().getDisplayMetrics());

			OnDateSetListener datelistener = EditOpenBalanceDialogActivity.this;
			Calendar now = Calendar.getInstance();
			FragmentManager fm = getSupportFragmentManager();
			DatePickerDialog dialog = DatePickerDialog.newInstance(datelistener, now.get(Calendar.YEAR),
					now.get(Calendar.MONDAY), now.get(Calendar.DAY_OF_MONTH));
		//	SelectedGroupsTask.sBalanceSheetDate_Response = "01/01/2016";
			// SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
//			String balancesheet_date = String.valueOf(SelectedGroupsTask.sBalanceSheetDate_Response);
			String balancesheet_date = "01/01/2017";
			EShaktiApplication.setCalendarDialog_MinDate(balancesheet_date);
			if (SelectedGroupsTask.sBalanceSheetDate_Response!=null) {
				
			
			EShaktiApplication.setTempBalanceSheetDate(SelectedGroupsTask.sBalanceSheetDate_Response);
			
			}
			
			String dateArr[] = balancesheet_date.split("/");

			Calendar min_Cal = Calendar.getInstance();


			int day = Integer.parseInt(dateArr[0]);
			int month = Integer.parseInt(dateArr[1]);
			int year = Integer.parseInt(dateArr[2]);

			min_Cal.set(year, (month - 1), day);
			dialog.setMinDate(min_Cal);
			dialog.setMaxDate(now);

			dialog.setMaxDate(now);

			dialog.show(fm, "");
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void onSetCalendarValues() {
		// TODO Auto-generated method stub
		
		if (EShaktiApplication.getNextMonthLastDate() == null) {
			try {
				if (EShaktiApplication.isCalendarDateVisibleFlag()) {
					EShaktiApplication.setCalendarDateVisibleFlag(false);
				}

				Calendar calender = Calendar.getInstance();

				SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String formattedDate = df.format(calender.getTime());
				Log.e("Device Date  =  ", formattedDate+"");
				
				String mBalanceSheetDate = SelectedGroupsTask.sBalanceSheetDate_Response; //dd/MM/yyyy
				String formatted_balancesheetDate = mBalanceSheetDate.replace("/", "-");
				Log.e("Balancesheet Date = ", formatted_balancesheetDate+"");

				Date balanceDate = df.parse(formatted_balancesheetDate);
				Date systemDate = df.parse(formattedDate);
				

				if (balanceDate.compareTo(systemDate) < 0 || balanceDate.compareTo(systemDate) == 0) {
					System.err.println(" balancesheet date is less than sys date");

					String mLastTransactionDate = SelectedGroupsTask.sLastTransactionDate_Response;

				String formattedDate_LastTransaction = mLastTransactionDate.replace("/", "-");

				String days = CalculateDateUtils.get_count_of_days(formattedDate_LastTransaction, formattedDate);

				int mDaycount = Integer.parseInt(days);
				Log.e("Total Days Count =====_____----", mDaycount + "");

				EShaktiApplication.setLastTransDate_GroupId(SelectedGroupsTask.Group_Id);

				if (mDaycount > 30) {

						SimpleDateFormat df_after = new SimpleDateFormat("dd-MM-yyyy");

						Calendar calendar = Calendar.getInstance();
						calendar.setTime(df_after.parse(formattedDate_LastTransaction));
						calendar.add(Calendar.MONTH, 1);
						calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
						Date nextMonthFirstDay = calendar.getTime();
						System.out.println("------ Next month first date   =  " + df_after.format(nextMonthFirstDay) + "");

						Calendar cal_last_date = Calendar.getInstance();
						cal_last_date.setTime(df_after.parse(formattedDate_LastTransaction));
						cal_last_date.add(Calendar.MONTH, 1);
						cal_last_date.set(Calendar.DATE, cal_last_date.getActualMaximum(Calendar.DAY_OF_MONTH));
						Date nextMonthLastDay = cal_last_date.getTime();
						System.out.println("------ Next month Last date   =  " + df_after.format(nextMonthLastDay) + "");

						//
						String lastDateOfNextMonth = df_after.format(nextMonthLastDay);
						Calendar currentDate = Calendar.getInstance();
						int current_Day = currentDate.get(Calendar.DAY_OF_MONTH);
						int current_month = currentDate.get(Calendar.MONTH) + 1;
						int current_year = currentDate.get(Calendar.YEAR);
						String current_date = current_Day + "-" + current_month + "-" + current_year;

						Date lastdate = null, currentdate = null;
						String min_date_str = null;

						try {

							lastdate = df_after.parse(lastDateOfNextMonth);
							currentdate = df_after.parse(current_date);

						} catch (ParseException e) {
							// TODO: handle exception
							e.printStackTrace();
						}

						if (lastdate.compareTo(currentdate) <= 0) {
							min_date_str = lastDateOfNextMonth;
						} else {
							min_date_str = current_date;
						}

						Log.e("Minimum Date !!!!!	", min_date_str + "");

						EShaktiApplication.setNextMonthLastDate(min_date_str);

					} else {
						Calendar currentDate = Calendar.getInstance();
						SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
						EShaktiApplication.setNextMonthLastDate(sdf.format(currentDate.getTime()));

					}
				} 
			
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

	}

	@Override
	public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
		// TODO Auto-generated method stub

	}

}
