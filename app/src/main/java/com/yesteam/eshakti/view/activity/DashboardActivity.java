package com.yesteam.eshakti.view.activity;

import com.oasys.eshakti.digitization.R;
import com.yesteam.eshakti.intro.utils.AppIntroUtils;
import com.yesteam.eshakti.intro.utils.IntroFirstSlide;
import com.yesteam.eshakti.intro.utils.IntroFourthSlide;
import com.yesteam.eshakti.intro.utils.IntroSecondSlide;
import com.yesteam.eshakti.intro.utils.IntroThirdSlide;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class DashboardActivity extends AppIntroUtils {
	@Override
	public void init(Bundle savedInstanceState) {
		addSlide(new IntroFirstSlide(), getApplicationContext());
		addSlide(new IntroSecondSlide(), getApplicationContext());
		addSlide(new IntroThirdSlide(), getApplicationContext());
		addSlide(new IntroFourthSlide(), getApplicationContext());
	}

	private void loadMainActivity() {
		startActivity(new Intent(DashboardActivity.this, MainActivity.class));
		overridePendingTransition(R.anim.right_to_left_in, R.anim.right_to_left_out);
		finish();
	}

	@Override
	public void onSkipPressed() {
		loadMainActivity();
	}

	@Override
	public void onDonePressed() {
		loadMainActivity();
	}

	public void getStarted(View v) {
		loadMainActivity();
	}
}