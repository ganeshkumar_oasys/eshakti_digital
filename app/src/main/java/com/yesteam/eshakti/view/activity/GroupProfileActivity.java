package com.yesteam.eshakti.view.activity;

import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.views.RaisedButton;
import com.yesteam.eshakti.webservices.Get_Edit_OpeningbalanceWebservice;
import com.yesteam.eshakti.webservices.Get_GroupProfileTask;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class GroupProfileActivity extends AppCompatActivity implements TaskListener {
	String responseArr[];
	private TextView mGroupName, mHeadertext;
	RaisedButton mNextButton;
	boolean isNavigateEditOpeningBalanceActivity = false;
	private Dialog mProgressDialog;

	public GroupProfileActivity() {
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_group_profile);
		try {

			mGroupName = (TextView) findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mHeadertext = (TextView) findViewById(R.id.fragment_agent_group_profile_headertext);
			mHeadertext.setText(RegionalConversion.getRegionalConversion(AppStrings.groupProfile));
			mHeadertext.setTypeface(LoginActivity.sTypeface);

			mNextButton = (RaisedButton) findViewById(R.id.fragment_groupProfile_Submitbutton);
			mNextButton.setTypeface(LoginActivity.sTypeface);
			mNextButton.setText(RegionalConversion.getRegionalConversion(AppStrings.next));

			responseArr = Get_GroupProfileTask.sGetGroupProfile_Response.split("~");
			System.out.println("Length of response :" + responseArr.length);

			Log.d("Group Profile : ", Get_GroupProfileTask.sGetGroupProfile_Response);

			TableLayout tableLayout = (TableLayout) findViewById(R.id.fragment_agent_group_profile_tableLayout);

			for (int i = 0; i < responseArr.length; i = i + 2) {

				TableRow indv_row = new TableRow(this);
				TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
						LayoutParams.WRAP_CONTENT);
				contentParams.setMargins(10, 5, 10, 5);

				TextView agent_left_details = new TextView(this);
				agent_left_details.setText(GetSpanText.getSpanString(this, String.valueOf(responseArr[i])));
				agent_left_details.setTypeface(LoginActivity.sTypeface);
				agent_left_details.setTextColor(color.black);
				agent_left_details.setPadding(5, 5, 5, 5);
				agent_left_details.setLayoutParams(contentParams);
				indv_row.addView(agent_left_details);

				TextView agent_right_details = new TextView(this);
				agent_right_details.setText(GetSpanText.getSpanString(this, String.valueOf(responseArr[i + 1])));
				agent_right_details.setTypeface(LoginActivity.sTypeface);
				agent_right_details.setTextColor(color.black);
				agent_right_details.setPadding(5, 5, 5, 5);
				agent_right_details.setLayoutParams(contentParams);
				indv_row.addView(agent_right_details);

				tableLayout.addView(indv_row);

			}

			mNextButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					isNavigateEditOpeningBalanceActivity = true;
					new Get_Edit_OpeningbalanceWebservice(GroupProfileActivity.this).execute();
					}
			});

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub
		mProgressDialog = AppDialogUtils.createProgressDialog(this);
		mProgressDialog.show();
	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub
		
		if (mProgressDialog != null) {

			System.out.println("Task Finished Values:::" + result);
			if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {

				mProgressDialog.dismiss();
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						if (!result.equals("FAIL")) {
							if (isNavigateEditOpeningBalanceActivity) {

								isNavigateEditOpeningBalanceActivity = false;

							}

							TastyToast.makeText(getApplicationContext(), AppStrings.mCommonNetworkErrorMsg,
									TastyToast.LENGTH_SHORT, TastyToast.ERROR);
							Constants.NETWORKCOMMONFLAG = "SUCCESS";
						}
					}
				});

			} else {

				if (isNavigateEditOpeningBalanceActivity) {
					mProgressDialog.dismiss();
					isNavigateEditOpeningBalanceActivity = false;
					Intent intent = new Intent(GroupProfileActivity.this, EditOpenBalanceDialogActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
					overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

					finish();

				}
			}
		}
	}
}