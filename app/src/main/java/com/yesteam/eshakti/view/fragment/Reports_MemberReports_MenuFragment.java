package com.yesteam.eshakti.view.fragment;

import java.io.InterruptedIOException;
import java.util.ArrayList;
import java.util.List;

import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.adapter.CustomListAdapter;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.interfaces.RecyclerViewListener;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.model.ListItem;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.Member_savings_summaryTask;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

public class Reports_MemberReports_MenuFragment extends Fragment
		implements TaskListener, RecyclerViewListener, OnItemClickListener { // OnItemClickListener,

	public static String TAG = Reports_MemberReports_MenuFragment.class.getSimpleName();
	public static String sSlelectedReportsMenu = null;

	private TextView mGroupName, mCashinHand, mCashatBank,mMemberName;

	private Dialog mProgressDialog;

	String[] memberReports_MenuList;

	private ListView mListView;
	private List<ListItem> listItems;
	private CustomListAdapter mAdapter;
	int listImage;

	Fragment fragment;
	private TextView mHeader;

	public Reports_MemberReports_MenuFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_MEMBER_REPORTS_MENU;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub//fragment_menulist
		View rootView = inflater.inflate(R.layout.fragment_menulist, container, false);

		MainFragment_Dashboard.isBackpressed = false;

		try {

			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mMemberName = (TextView) rootView.findViewById(R.id.memberName);
			mMemberName.setText(EShaktiApplication.getSelectedMemberName());
			mMemberName.setTypeface(LoginActivity.sTypeface);
			mMemberName.setVisibility(View.VISIBLE);
			
			mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashinHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashinHand.setTypeface(LoginActivity.sTypeface);

			mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashatBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashatBank.setTypeface(LoginActivity.sTypeface);

			mHeader = (TextView) rootView.findViewById(R.id.submenuHeaderTextview);
			mHeader.setVisibility(View.VISIBLE);
			mHeader.setText(
					RegionalConversion.getRegionalConversion(AppStrings.Memberreports));
			mHeader.setTypeface(LoginActivity.sTypeface);
			
			listItems = new ArrayList<ListItem>();
			mListView = (ListView) rootView.findViewById(R.id.fragment_List);
			listImage = R.drawable.ic_navigate_next_white_24dp;
			
			memberReports_MenuList = new String[] { RegionalConversion.getRegionalConversion(AppStrings.savingsSummary),
					RegionalConversion.getRegionalConversion(AppStrings.loanSummary),
					RegionalConversion.getRegionalConversion(AppStrings.lastMonthReport) };

			for (int i = 0; i < memberReports_MenuList.length; i++) {
				ListItem rowItem = new ListItem(memberReports_MenuList[i], listImage);
				listItems.add(rowItem);
			}

			mAdapter = new CustomListAdapter(getActivity(), listItems);
			mListView.setAdapter(mAdapter);
			mListView.setOnItemClickListener(this);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return rootView;
	}

	@Override
	public void onAttach(Context context) {
		// TODO Auto-generated method stub
		super.onAttach(context);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub

		mProgressDialog = AppDialogUtils.createProgressDialog(getActivity());
		mProgressDialog.show();

	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub

		if (mProgressDialog != null) {
			mProgressDialog.dismiss();

			if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {
				getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub

						if (!result.equals("FAIL")) {

							TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg, TastyToast.LENGTH_SHORT,
									TastyToast.ERROR);
							Constants.NETWORKCOMMONFLAG = "SUCESS";
						}

					}
				});
			} else {

				Reports_MemberSavingsSummary_ReportsFragment fragment = new Reports_MemberSavingsSummary_ReportsFragment();
				setFragment(fragment);
			}
		}
	}

	@Override
	public void recyclerViewListClicked(View view, int position) throws InterruptedIOException {
		// TODO Auto-generated method stub

		sSlelectedReportsMenu = String.valueOf(memberReports_MenuList[position]);

		Log.d(TAG, "MEMBER MENU - LIST --> " + String.valueOf(memberReports_MenuList[position]));

		switch (position) {
		case 0:
			// Savings Summary

			if (ConnectionUtils.isNetworkAvailable(getActivity())) {

				try {

					new Member_savings_summaryTask(this).execute();

				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}

			} else {

				// Do Offline Stuffs
			}

			break;
		case 1:
			// Loan Summary

			Reports_MemberReports_LoanSummaryMenuFragment fragment = new Reports_MemberReports_LoanSummaryMenuFragment();
			setFragment(fragment);

			break;

		case 2:
			// Monthly Reports

			Reports_MonthYear_PickerReportsFragment fragment_month = new Reports_MonthYear_PickerReportsFragment();
			setFragment(fragment_month);

			break;
		default:
			break;
		}

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		// TODO Auto-generated method stub

		TextView textColor_Change = (TextView) view.findViewById(R.id.dynamicText);
		textColor_Change.setText(String.valueOf(memberReports_MenuList[position]));
		textColor_Change.setTextColor(Color.rgb(251, 161, 108));

		sSlelectedReportsMenu = String.valueOf(memberReports_MenuList[position]);

		Log.d(TAG, "MEMBER MENU - LIST --> " + String.valueOf(memberReports_MenuList[position]));

		switch (position) {
		case 0:
			// Savings Summary

			if (ConnectionUtils.isNetworkAvailable(getActivity())) {

				try {

					new Member_savings_summaryTask(this).execute();

				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}

			} else {

				// Do Offline Stuffs
			
				TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
						TastyToast.ERROR);
			}

			break;
		case 1:
			// Loan Summary

			Reports_MemberReports_LoanSummaryMenuFragment fragment = new Reports_MemberReports_LoanSummaryMenuFragment();
			setFragment(fragment);

			break;

		case 2:
			// Monthly Reports

			Reports_MonthYear_PickerReportsFragment fragment_month = new Reports_MonthYear_PickerReportsFragment();
			setFragment(fragment_month);

			break;
		default:
			break;
		}

	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub
		Log.e("Current Class Name!!!!!!!!!!!!!!", fragment.getClass().getName());
		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

	}

}
