package com.yesteam.eshakti.view.fragment;

import java.util.ArrayList;
import java.util.List;

import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;

import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.adapter.CustomItemAdapter;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.model.RowItem;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.views.MaterialSpinner;
import com.yesteam.eshakti.views.RaisedButton;
import com.yesteam.eshakti.webservices.Get_Update_Shg_Credit_Linkage_webservice;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Dialog;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.TextView;

public class Profile_CreditLinkageInfoFragment extends Fragment implements OnClickListener, TaskListener {

	public static final String TAG = Profile_CreditLinkageInfoFragment.class.getSimpleName();
	private TextView mGroupName, mCashInHand, mCashAtBank, mHeadertext, mContentTextview;
	private RaisedButton mRaised_Submit_Button;
	private String[] creditLinkageArr;
	private List<RowItem> creditLinkageItems;
	private MaterialSpinner spn_label_credit_linkageInfo;
	CustomItemAdapter creditLinkageInfoAdapter;
	public static String creditLinkageValue = "";
	private Dialog mProgressDialog;

	public Profile_CreditLinkageInfoFragment() {
		// TODO Auto-generated constructor stub
		creditLinkageValue = "";
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		// Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_TRAINING;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_credit_linkage_info, container, false);

		MainFragment_Dashboard.isBackpressed = false;

		try {
			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashInHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashInHand.setTypeface(LoginActivity.sTypeface);

			mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashAtBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashAtBank.setTypeface(LoginActivity.sTypeface);

			mHeadertext = (TextView) rootView.findViewById(R.id.fragment_credit_linkageInfo_headertext);
			mHeadertext.setText(RegionalConversion.getRegionalConversion(AppStrings.mCreditLinkageInfo));
			mHeadertext.setTypeface(LoginActivity.sTypeface);

			mContentTextview = (TextView) rootView.findViewById(R.id.credit_linkageInfo_content_textview);
			mContentTextview.setText(RegionalConversion.getRegionalConversion(AppStrings.mCreditLinkage_content));
			mContentTextview.setTypeface(LoginActivity.sTypeface);

			creditLinkageArr = new String[] { AppStrings.mCreditLinkage, "0", "1", "2", "3", "4", "5", "6", "7", "8",
					"9", "10" };

			spn_label_credit_linkageInfo = (MaterialSpinner) rootView.findViewById(R.id.spinner_credit_linkage_info);
			spn_label_credit_linkageInfo.setBaseColor(color.grey_400);
			spn_label_credit_linkageInfo.setFloatingLabelText(String.valueOf(AppStrings.mCreditLinkage));
			spn_label_credit_linkageInfo.setPaddingSafe(10, 0, 10, 0);

			creditLinkageItems = new ArrayList<RowItem>();
			for (int i = 0; i < creditLinkageArr.length; i++) {
				RowItem rowItem = new RowItem(creditLinkageArr[i]);
				creditLinkageItems.add(rowItem);
			}

			creditLinkageInfoAdapter = new CustomItemAdapter(getActivity(), creditLinkageItems);
			spn_label_credit_linkageInfo.setAdapter(creditLinkageInfoAdapter);
			spn_label_credit_linkageInfo.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
					// TODO Auto-generated method stub
					System.out.println("SPINNER CHECK : " + spn_label_credit_linkageInfo.getSelectedItem().toString());

					String selectedItem = creditLinkageArr[position];
					System.out.println("SELECTED ITEM :" + selectedItem);

					if (selectedItem.equals(AppStrings.mCreditLinkage)) {
						creditLinkageValue = "";
					} else {
						creditLinkageValue = selectedItem;
					}

					Log.e("Month value", creditLinkageValue);
				}

				@Override
				public void onNothingSelected(AdapterView<?> parent) {
					// TODO Auto-generated method stub

				}
			});

			mRaised_Submit_Button = (RaisedButton) rootView.findViewById(R.id.fragment_credit_linkageInfo_Submitbutton);
			mRaised_Submit_Button.setText(RegionalConversion.getRegionalConversion(AppStrings.submit));
			mRaised_Submit_Button.setTypeface(LoginActivity.sTypeface);
			mRaised_Submit_Button.setOnClickListener(this);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return rootView;
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		if (!creditLinkageValue.isEmpty()) {
			new Get_Update_Shg_Credit_Linkage_webservice(Profile_CreditLinkageInfoFragment.this).execute();
		} else {
			TastyToast.makeText(getActivity(), "PLEASE CHOOSE ANY ONE OF CREDIT LINKAGE", TastyToast.LENGTH_SHORT,
					TastyToast.WARNING);
		}
	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub

		mProgressDialog = AppDialogUtils.createProgressDialog(getActivity());
		mProgressDialog.show();

	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub

		try {

			if (mProgressDialog != null) {
				mProgressDialog.dismiss();
				if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {
					getActivity().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub

							if (!result.equals("FAIL")) {

								TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg,
										TastyToast.LENGTH_SHORT, TastyToast.ERROR);
								Constants.NETWORKCOMMONFLAG = "SUCCESS";
							}

						}
					});
				} else {
					if (Get_Update_Shg_Credit_Linkage_webservice._UpdateCreditLinkage_ServiceResponse != null) {

						if (Get_Update_Shg_Credit_Linkage_webservice._UpdateCreditLinkage_ServiceResponse
								.equals("Yes")) {

							TastyToast.makeText(getActivity(), AppStrings.transactionCompleted, TastyToast.LENGTH_SHORT,
									TastyToast.SUCCESS);
							MainFragment_Dashboard fragment = new MainFragment_Dashboard();
							setFragment(fragment);
						} else {
							TastyToast.makeText(getActivity(), AppStrings.transactionFailAlert, TastyToast.LENGTH_SHORT,
									TastyToast.ERROR);
							MainFragment_Dashboard fragment = new MainFragment_Dashboard();
							setFragment(fragment);
						}
					} else {
						TastyToast.makeText(getActivity(), AppStrings.transactionFailAlert, TastyToast.LENGTH_SHORT,
								TastyToast.ERROR);
						MainFragment_Dashboard fragment = new MainFragment_Dashboard();
						setFragment(fragment);
					}

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub

		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

	}
}
