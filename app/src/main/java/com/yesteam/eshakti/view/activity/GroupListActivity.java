package com.yesteam.eshakti.view.activity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import com.squareup.otto.Subscribe;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.adapter.CustomExpandableGroupListAdapter;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.interfaces.ExpandListItemClickListener;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;
import com.yesteam.eshakti.model.GroupListItem;
import com.yesteam.eshakti.service.GroupDetailsService;
import com.yesteam.eshakti.service.GroupWisePendingReportService;
import com.yesteam.eshakti.sqlite.database.GroupDetailsProvider;
import com.yesteam.eshakti.sqlite.database.TransactionProvider;
import com.yesteam.eshakti.sqlite.database.response.GroupMasterResponse;
import com.yesteam.eshakti.sqlite.database.response.GroupMemberResponse;
import com.yesteam.eshakti.sqlite.db.model.GetGroupMemberDetails;
import com.yesteam.eshakti.sqlite.db.model.GroupMasterSingle;
import com.yesteam.eshakti.sqlite.db.model.Groupdetails;
import com.yesteam.eshakti.sqlite.db.model.LoginCheck;
import com.yesteam.eshakti.sqlite.db.model.Transaction;
import com.yesteam.eshakti.sqlite.db.transactions.TransactionManager.DataType;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.utils.CustomTypefaceSpan;
import com.yesteam.eshakti.utils.GetExit;
import com.yesteam.eshakti.utils.GetTypeface;
import com.yesteam.eshakti.utils.Get_Cal_CurrentDate;
import com.yesteam.eshakti.utils.Get_UniqueID;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.views.MyExpandableListview;
import com.yesteam.eshakti.webservices.Get_GroupProfileTask;
import com.yesteam.eshakti.webservices.Get_Trans_Audit_Verified_Webservices;
import com.yesteam.eshakti.webservices.LoginTask;
import com.yesteam.eshakti.webservices.Login_webserviceTask;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.TextAppearanceSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.TextView;

public class GroupListActivity extends AppCompatActivity implements TaskListener, ExpandListItemClickListener {

    Toolbar mToolbar;
    Context context;

    private Dialog mProgressDialog;

    public static String sSelectedGroup = null;
    public static String sGroupName = null;

    boolean isNavigate = false;

    public static Vector<String> offline_sAgent_GIDs;
    public static Vector<String> offline_sAgent_Gname;

    public static Vector<String> offline_sAGent_GActiveStatus;

    public static String[] offline_groupList;

    private List<GroupListItem> listItems;
    // private GroupListAdapter mAdapter;
    private CustomExpandableGroupListAdapter mAdapter;
    int listImage, leftImage;

    String TAG = GroupListActivity.class.getSimpleName();

    boolean isCheckActiveStatus = false;
    String mGroupActiveStatus = null;
    // boolean isNavigateEditOpeningBalanceActivity = false;

    // String[] groupList;

    String[] groupList, shgcodeList, groupDetails;
    String[] mGroupListMaster;
    public static ArrayList<String> mGroupIdlist = new ArrayList<String>();
    boolean isNavigateGroupProfile = false;
    public static String mLanguageLocalae;
    // ExpandableLayoutListView expandableLayoutListView;
    MyExpandableListview expandableLayoutListView;
    public static String mGroupTransId = null;
    public static boolean mGroupListView = false;
    private MyWebRequestReceiver receiver;
    public static Vector<String> sGroupTransId;
    public static boolean isGetOfflineData = false;
    boolean isGrouplist = false;
    private ArrayList<HashMap<String, String>> childList;
    private int lastExpandedPosition = -1;
    private boolean _isAuditVerifiedService = false;
    public static String _NgoId = null;
    public static String _GroupId = null;
    public static String _GroupName = null;
    public static int mTransAuditMasterCount = 0;
    public static int mTransAuditChildCount = 0;
    public static int mTransAuditMasterSize = 0;
    public static int mTransAuditChildSize = 0;
    public static ArrayList<String> mTransAuditChildDetailList = new ArrayList<String>();
    boolean _IsOfflineDatasAvail = false;

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        SBus.INST.register(this);
    }

    @SuppressLint("LongLogTag")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_group_expandablelistview);
        isGetOfflineData = false;
        EShaktiApplication.setFragmentMenuListView(false);

        Log.e("Login Offline Data Values------------->>>>", LoginActivity.iSOfflineDataAvail + "");

        if (ConnectionUtils.isNetworkAvailable(getApplicationContext())) {

            publicValues.mGetAnimatorPendingGroupwiseValues = null;

            IntentFilter filter = new IntentFilter(MyWebRequestReceiver.PROCESS_RESPONSE);
            filter.addCategory(Intent.CATEGORY_DEFAULT);
            receiver = new MyWebRequestReceiver();
            registerReceiver(receiver, filter);

            Intent msgIntent = new Intent(GroupListActivity.this, GroupWisePendingReportService.class);
            startService(msgIntent);
        }

        expandableLayoutListView = (MyExpandableListview) findViewById(R.id.listview);
        Constants.BUTTON_CLICK_FLAG = "0";
        EShaktiApplication.setSubmenuclicked(false);
        mGroupListView = false;

        offline_sAgent_GIDs = new Vector<String>();
        offline_sAgent_Gname = new Vector<String>();
        offline_sAGent_GActiveStatus = new Vector<String>();
        sGroupTransId = new Vector<String>();
        mGroupIdlist.clear();

        _NgoId = null;
        _GroupId = null;
        _GroupName = null;

        try {

            mToolbar = (Toolbar) findViewById(R.id.toolbar_grouplist);
            TextView mTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

            getSupportActionBar().setTitle("");

            mTitle.setText(PrefUtils.getAnimatorName() + "    " + PrefUtils.getAgentUserName());
            mTitle.setTypeface(LoginActivity.sTypeface);
            mTitle.setGravity(Gravity.CENTER);

            Toolbar mToolBar_Target = (Toolbar) findViewById(R.id.toolbar_grouplist_target);

            TextView mTargetValues = (TextView) mToolBar_Target.findViewById(R.id.toolbar_title_target);

            if (PrefUtils.getAnimatorTargetValues() != null) {
                String[] mValues = PrefUtils.getAnimatorTargetValues().split("~");

                mTargetValues.setText(
                        "TARGET : " + mValues[0] + "  COMPLETED : " + mValues[1] + "  PENDING : " + mValues[2]);
                mTargetValues.setTypeface(LoginActivity.sTypeface);
                mTargetValues.setGravity(Gravity.CENTER);
            }

            context = this.getApplicationContext();

            listItems = new ArrayList<GroupListItem>();

            listImage = R.drawable.ic_navigate_next_white_24dp;
            leftImage = R.drawable.star;
            mGroupIdlist.clear();

            expandableLayoutListView.setOnGroupExpandListener(new OnGroupExpandListener() {

                @Override
                public void onGroupExpand(int groupPosition) {
                    if (lastExpandedPosition != -1 && groupPosition != lastExpandedPosition) {
                        expandableLayoutListView.collapseGroup(lastExpandedPosition);
                    }
                    lastExpandedPosition = groupPosition;

                }
            });

            if (ConnectionUtils.isNetworkAvailable(getApplicationContext())) {
                // this is data fro recycler view
                if (PrefUtils.getGrouplistValues() != null && PrefUtils.getGrouplistValues().equals("1")) {
                    isGetOfflineData = true;
                    isGrouplist = true;
                    EShaktiApplication.setCheckGroupListTextColor(true);
                    EShaktiApplication.setGroupListValues(true);
                    EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GROUPNAME_GET, null);

                    if (LoginCheck.getTrainerId() != null) {
                        LoginTask.Trainer_Id = LoginCheck.getTrainerId();
                        LoginTask.Ngo_Id = LoginCheck.getUserNgoId();
                        LoginTask.UserName = LoginActivity.sUName;
                    }

                } else {

				/*	if (Login_webserviceTask.sAgent_Gname != null) {

						String groupList[] = new String[Login_webserviceTask.sAgent_Gname.size()];

						for (int i = 0; i < groupList.length; i++) {
							GroupListItem item = new GroupListItem(leftImage,
									Login_webserviceTask.sAgent_Gname.elementAt(i).toString(), listImage);
							listItems.add(item);
							mGroupIdlist.add(Login_webserviceTask.sAgent_GIDs.elementAt(i).toString());

						}
						EShaktiApplication.setCheckGroupListTextColor(true);
						EShaktiApplication.setGroupListValues(true);
						mGroupListView = true;

						setCustomAdapter();
					} else {

						PrefUtils.clearUserId();
						PrefUtils.clearGroup_Offlinedata();
						PrefUtils.setUserlangcode(null);
						PrefUtils.clearLoginValues();
						PrefUtils.clearGrouplistValues();
						PrefUtils.clearGroupMasterResValues();
						PrefUtils.clearFirstLoginDashboard();

						PrefUtils.clearBankDepositName();
						PrefUtils.clearMemberLoanId();

						finish();

						TastyToast.makeText(getApplicationContext(), AppStrings.tryLater, TastyToast.LENGTH_SHORT,
								TastyToast.ERROR);

					}*/
                }

            } else {
                isGrouplist = true;
                EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GROUPNAME_GET, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setCustomAdapter() {
        // TODO Auto-generated method stub
        childList = new ArrayList<HashMap<String, String>>();

        for (int i = 0; i < Login_webserviceTask.sSHG_Codes.size(); i++) {
            if (Login_webserviceTask.sSHG_Codes.elementAt(i) != null) {
                HashMap<String, String> temp = new HashMap<String, String>();

                temp.put("SHGCode", Login_webserviceTask.sSHG_Codes.elementAt(i).toUpperCase());
                temp.put("BlockName", Login_webserviceTask.sSHG_BlockNames.elementAt(i).toUpperCase());
                temp.put("PanchayatName", Login_webserviceTask.sSHG_PanchayatNames.elementAt(i).toUpperCase());
                temp.put("VillageName", Login_webserviceTask.sSHG_VillageNames.elementAt(i).toUpperCase());
                temp.put("LastTransactionDate", Login_webserviceTask.sSHG_TransactionDate.elementAt(i).toUpperCase());

                temp.put("SHGCode_Label", Login_webserviceTask.sSHG_Codes_Label.elementAt(i));
                temp.put("BlockName_Label", Login_webserviceTask.sSHG_BlockNames_Label.elementAt(i));
                temp.put("PanchayatName_Label", Login_webserviceTask.sSHG_PanchayatNames_Label.elementAt(i));
                temp.put("VillageName_Label", Login_webserviceTask.sSHG_VillageNames_Label.elementAt(i));
                temp.put("LastTransactionDate_Label", Login_webserviceTask.sSHG_TransactionDate_Label.elementAt(i));

                childList.add(temp);
            }
        }

        mAdapter = new CustomExpandableGroupListAdapter(GroupListActivity.this, null, listItems, childList, this);
        expandableLayoutListView.setAdapter(mAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_ob_group, menu);
        MenuItem item = menu.getItem(0);
        item.setVisible(true);

        mLanguageLocalae = PrefUtils.getUserlangcode();
        LoginActivity.sTypeface = GetTypeface.getTypeface(getApplicationContext(), mLanguageLocalae);

        SpannableStringBuilder logOutBuilder = new SpannableStringBuilder(
                RegionalConversion.getRegionalConversion(AppStrings.logOut));

        logOutBuilder.setSpan(new CustomTypefaceSpan("", LoginActivity.sTypeface), 0,
                RegionalConversion.getRegionalConversion(AppStrings.logOut).length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

        logOutBuilder.setSpan(new TextAppearanceSpan(getApplicationContext(), R.style.text_menu), 0,
                RegionalConversion.getRegionalConversion(AppStrings.logOut).length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

        if (item.getItemId() == R.id.group_logout_edit) {

            item.setTitle(logOutBuilder);

        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();
        if (id == R.id.group_logout_edit) {
            Log.e("Group Logout", "Logout Sucessfully");
            startActivity(new Intent(GetExit.getExitIntent(getApplicationContext())));
            this.finish();

            return true;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTaskStarted() {
        // TODO Auto-generated method stub
        // mProgressDialog = ProgressDialog.show(this, "", "");
        mProgressDialog = AppDialogUtils.createProgressDialog(this);
        mProgressDialog.show();
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onTaskFinished(final String result) {
        // TODO Auto-generated method stub

        if (mProgressDialog != null) {

            /**
             * Inorder to Start Service for collecting the GroupDetails at background
             **/
            System.out.println("Task Finished Values:::" + result);
            if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {

                mProgressDialog.dismiss();
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        if (!result.equals("FAIL")) {

                            TastyToast.makeText(getApplicationContext(), AppStrings.mCommonNetworkErrorMsg,
                                    TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                            Constants.NETWORKCOMMONFLAG = "SUCCESS";
                        }
                    }
                });

            } else {

                if (_isAuditVerifiedService) {
                    mProgressDialog.dismiss();
                    _isAuditVerifiedService = false;
                    Log.e("Audit Trans Values ---->>", publicValues.mTransAuditVerifiedValues);

                    mTransAuditChildDetailList.clear();

                    String[] auditTransContentArr = publicValues.mTransAuditVerifiedValues.split("##");

                    String[] auditTransDateArr = auditTransContentArr[0].split("#");

                    if (auditTransDateArr.length != 0) {
                        mTransAuditMasterSize = auditTransDateArr.length;
                        mTransAuditMasterCount = 1;

                        for (int i = 1; i < auditTransContentArr.length; i++) {
                            mTransAuditChildDetailList.add(auditTransContentArr[i]);

                        }

                        String[] datewiseTransDetailArr = mTransAuditChildDetailList.get(0).split("!");
                        mTransAuditChildSize = datewiseTransDetailArr.length - 1;

                        mTransAuditChildCount = 1;

                        Intent intent = new Intent(GroupListActivity.this, Audit_Verified_Activity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

                        finish();
                    }
                } else {
                    if (isNavigateGroupProfile) {
                        mProgressDialog.dismiss();
                        isNavigateGroupProfile = false;
                        mGroupListView = false;
                        isGetOfflineData = false;
                        Intent intent = new Intent(GroupListActivity.this, GroupProfileActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

                        finish();

                    } else {

                        if (mGroupActiveStatus.equals("Yes")) {

                            mProgressDialog.dismiss();

                            EShaktiApplication.setGroupListValues(false);
                            if (PrefUtils.getLoginGroupService() != null) {
                                if (PrefUtils.getLoginGroupService().equals("1")) {
                                    Intent intentservice = new Intent(GroupListActivity.this,
                                            GroupDetailsService.class);
                                    startService(intentservice);
                                }

                            }

                            Constants.FRAG_INSTANCE_CONSTANT = "0";
                            EShaktiApplication.setOnSavedFragment(false);

                            EShaktiApplication.setCheckGroupListTextColor(false);
                            mGroupListView = false;
                            PrefUtils.setOfflineUniqueID(Get_UniqueID.get_UniqueID(this));

                            Intent intent_ = new Intent(GroupListActivity.this, MainActivity.class);
                            intent_.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent_.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent_);
                            overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);
                            finish();

                        } else {
                            if (isCheckActiveStatus) {
                                mProgressDialog.dismiss();

                                isCheckActiveStatus = false;
                                isNavigateGroupProfile = true;
                                EShaktiApplication.setGroupListValues(false);
                                new Get_GroupProfileTask(GroupListActivity.this).execute();

                            } else {

                                EShaktiApplication.setGroupListValues(false);

                                if (PrefUtils.getLoginGroupService() != null) {
                                    if (PrefUtils.getLoginGroupService().equals("1")) {
                                        Intent intentservice = new Intent(GroupListActivity.this,
                                                GroupDetailsService.class);
                                        startService(intentservice);
                                    }

                                }

                                Constants.FRAG_INSTANCE_CONSTANT = "0";
                                EShaktiApplication.setOnSavedFragment(false);

                                EShaktiApplication.setCheckGroupListTextColor(false);

                                mGroupListView = false;
                                PrefUtils.setOfflineUniqueID(Get_UniqueID.get_UniqueID(this));

                                Intent intent_ = new Intent(GroupListActivity.this, MainActivity.class);
                                intent_.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent_.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent_);
                                overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);
                                finish();
                            }

                        }
                    }
                }

            }
        }
    }

    public void InterruptedIOException(String message, Throwable throwable) {

        TastyToast.makeText(getApplicationContext(), message, TastyToast.LENGTH_SHORT, TastyToast.ERROR);

        finish();
    }

    @Subscribe
    public void onAddGroupMemberDetails(final GroupMemberResponse groupMemberResponse) {
        switch (groupMemberResponse.getFetcherResult()) {
            case FAIL:
                TastyToast.makeText(getApplicationContext(), groupMemberResponse.getFetcherResult().getMessage(),
                        TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                break;
            case NO_NETWORK_CONNECTION:
                TastyToast.makeText(getApplicationContext(), groupMemberResponse.getFetcherResult().getMessage(),
                        TastyToast.LENGTH_SHORT, TastyToast.ERROR);

                break;
            case SUCCESS:
                break;
        }
    }

    @SuppressLint("LongLogTag")
    @Subscribe
    public void OnGroupMasterResponse(final GroupMasterResponse groupMasterResponse) {
        switch (groupMasterResponse.getFetcherResult()) {
            case FAIL:
                TastyToast.makeText(getApplicationContext(), groupMasterResponse.getFetcherResult().getMessage(),
                        TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                break;
            case NO_NETWORK_CONNECTION:
                TastyToast.makeText(getApplicationContext(), groupMasterResponse.getFetcherResult().getMessage(),
                        TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                break;
            case SUCCESS:

                try {
                    if (GetGroupMemberDetails.getGroupMemberresponse() != null) {

                        if (ConnectionUtils.isNetworkAvailable(getApplicationContext())) {
                            mGroupIdlist.clear();
                            if (PrefUtils.getGrouplistValues() != null && PrefUtils.getGrouplistValues().equals("1")
                                    && isGetOfflineData) {

                                for (int i = 0; i < offline_sAgent_GIDs.size(); i++) {

                                    mGroupIdlist.add(offline_sAgent_GIDs.elementAt(i).toString());

                                }
                            }

                            if (PrefUtils.getGrouplistValues() != null && PrefUtils.getGrouplistValues().equals("1")
                                    && isGetOfflineData) {

                                if (PrefUtils.getLoginGroupService() != null) {
                                    if (PrefUtils.getLoginGroupService().equals("1")) {
                                        Intent intentservice = new Intent(GroupListActivity.this,
                                                GroupDetailsService.class);
                                        startService(intentservice);
                                    }

                                }

                            }
                        }

                        Log.e("Get group Offline Db values", GetGroupMemberDetails.getGroupMemberresponse() + "");
                        SelectedGroupsTask.getGroupDetails(GetGroupMemberDetails.getGroupMemberresponse());

                        PrefUtils.setOfflineUniqueID(Get_UniqueID.get_UniqueID(this));

                        PrefUtils.clearGroupLoanOSId();
                        PrefUtils.clearGroupLoanOsrepay();
                        PrefUtils.clearMemberLoanId();
                        PrefUtils.clearMemberLoanrepay();

                        PrefUtils.clearBankDeposit();
                        PrefUtils.clearBankDepositName();

                        PrefUtils.clearBankFixedDeposit();
                        PrefUtils.clearBankFixedDepositName();

                        Constants.FRAG_INSTANCE_CONSTANT = "0";
                        EShaktiApplication.setOnSavedFragment(false);
                        mGroupListView = false;

                        Intent intent = new Intent(GroupListActivity.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

                        finish();
                    } else {

                        TastyToast.makeText(getApplicationContext(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
                                TastyToast.ERROR);
                        finish();
                    }
                } catch (IndexOutOfBoundsException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (RuntimeException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                break;
        }

    }

    @SuppressLint("LongLogTag")
    @Subscribe
    public void onGroupNameResponse(final ArrayList<Groupdetails> groupdetails) {
        if (isGrouplist) {

            if (ConnectionUtils.isNetworkAvailable(getApplicationContext())) {
                isGrouplist = false;

                try {
                    String mOfflineGroupresponse = groupdetails.get(0).getGroupnameresponse();
                    if (mOfflineGroupresponse != null) {

                        EShaktiApplication.setGroupResponse(null);
                        EShaktiApplication.setGroupResponse(mOfflineGroupresponse);

                        Login_webserviceTask.sGroupTransId = new Vector<String>();
                        Login_webserviceTask.sSHG_Codes = new Vector<String>();
                        Login_webserviceTask.sSHG_BlockNames = new Vector<String>();
                        Login_webserviceTask.sSHG_PanchayatNames = new Vector<String>();
                        Login_webserviceTask.sSHG_VillageNames = new Vector<String>();
                        Login_webserviceTask.sSHG_TransactionDate = new Vector<String>();
                        Login_webserviceTask.mTitleValues = null;
                        Login_webserviceTask.mTitleValues = new String[5];
                        Login_webserviceTask.mFirstAddTitle = false;
                        Login_webserviceTask.sSHG_Codes_Label = new Vector<String>();
                        Login_webserviceTask.sSHG_BlockNames_Label = new Vector<String>();
                        Login_webserviceTask.sSHG_PanchayatNames_Label = new Vector<String>();
                        Login_webserviceTask.sSHG_VillageNames_Label = new Vector<String>();
                        Login_webserviceTask.sSHG_TransactionDate_Label = new Vector<String>();
                        Login_webserviceTask._VerifiedLastAuditDate = new Vector<String>();

                        String arr_viewGroups[] = mOfflineGroupresponse.split("#");

                        mGroupListMaster = arr_viewGroups[2].split("%");

                        try {
                            for (int i = 0; i < mGroupListMaster.length; i++) {

                                groupDetails = mGroupListMaster[i].split("[$]");

                                groupList = groupDetails[0].split("~");
                                shgcodeList = groupDetails[1].split("~");

                                offline_sAgent_GIDs.addElement(groupList[0].toString());

                                offline_sAgent_Gname.addElement(groupList[1].toString());
                                offline_sAGent_GActiveStatus.addElement(groupList[2].toString());
                                sGroupTransId.addElement(groupList[3].toString());
                                Login_webserviceTask._VerifiedLastAuditDate.add(groupList[5].toString());
                                if (!Login_webserviceTask.mFirstAddTitle) {
                                    Login_webserviceTask.mFirstAddTitle = true;
                                    Login_webserviceTask.mTitleValues[0] = shgcodeList[0].toString();
                                    Login_webserviceTask.mTitleValues[1] = shgcodeList[2].toString();
                                    Login_webserviceTask.mTitleValues[2] = shgcodeList[4].toString();
                                    Login_webserviceTask.mTitleValues[3] = shgcodeList[6].toString();
                                    Login_webserviceTask.mTitleValues[4] = shgcodeList[8].toString();
                                }

                                Login_webserviceTask.sGroupTransId.addElement(groupList[3].toString());

                                Login_webserviceTask.sSHG_Codes_Label.addElement(shgcodeList[0].toString());
                                Login_webserviceTask.sSHG_BlockNames_Label.addElement(shgcodeList[2].toString());
                                Login_webserviceTask.sSHG_PanchayatNames_Label.addElement(shgcodeList[4].toString());
                                Login_webserviceTask.sSHG_VillageNames_Label.addElement(shgcodeList[6].toString());
                                Login_webserviceTask.sSHG_TransactionDate_Label.addElement(shgcodeList[8].toString());

                                Login_webserviceTask.sSHG_Codes.addElement(shgcodeList[1].toString());
                                Login_webserviceTask.sSHG_BlockNames.addElement(shgcodeList[3].toString());
                                Login_webserviceTask.sSHG_PanchayatNames.addElement(shgcodeList[5].toString());
                                Login_webserviceTask.sSHG_VillageNames.addElement(shgcodeList[7].toString());
                                Login_webserviceTask.sSHG_TransactionDate.addElement(shgcodeList[9].toString());

                            }
                        } catch (Exception e) {
                            // TODO: handle exception
                        }

                        try {

                            // this is data fro recycler view
                            String groupList_offline[] = new String[offline_sAgent_Gname.size()];
                            CommonSetRecyclerview(groupList_offline);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {

                        TastyToast.makeText(getApplicationContext(), AppStrings.tryLater, TastyToast.LENGTH_SHORT,
                                TastyToast.ERROR);
                        finish();

                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } else {

                if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
                    isGrouplist = false;
                    try {
                        String mOfflineGroupresponse = groupdetails.get(0).getGroupnameresponse();
                        Log.e("Grouplist Activity -------->>>>>", mOfflineGroupresponse);
                        EShaktiApplication.setGroupResponse(null);
                        EShaktiApplication.setGroupResponse(mOfflineGroupresponse);

                        Login_webserviceTask.sGroupTransId = new Vector<String>();
                        Login_webserviceTask.sSHG_Codes = new Vector<String>();
                        Login_webserviceTask.sSHG_BlockNames = new Vector<String>();
                        Login_webserviceTask.sSHG_PanchayatNames = new Vector<String>();
                        Login_webserviceTask.sSHG_VillageNames = new Vector<String>();
                        Login_webserviceTask.sSHG_TransactionDate = new Vector<String>();
                        Login_webserviceTask.mTitleValues = null;
                        Login_webserviceTask.mTitleValues = new String[5];
                        Login_webserviceTask.mFirstAddTitle = false;
                        Login_webserviceTask.sSHG_Codes_Label = new Vector<String>();
                        Login_webserviceTask.sSHG_BlockNames_Label = new Vector<String>();
                        Login_webserviceTask.sSHG_PanchayatNames_Label = new Vector<String>();
                        Login_webserviceTask.sSHG_VillageNames_Label = new Vector<String>();
                        Login_webserviceTask.sSHG_TransactionDate_Label = new Vector<String>();
                        Login_webserviceTask._VerifiedLastAuditDate = new Vector<String>();

                        String arr_viewGroups[] = mOfflineGroupresponse.split("#");

                        mGroupListMaster = arr_viewGroups[2].split("%");

                        Log.e("Group Master Valuesssssss", arr_viewGroups[2]);

                        Log.e("0 Position Values---->>>>", mGroupListMaster[0]);

                        Log.e("Step 111111111111111111", "@@@@@@@@@@@@@@@@@@@@@@@@@@");
                        Log.e("Length_________------------------>>>", mGroupListMaster.length + "");

                        try {
                            for (int i = 0; i < mGroupListMaster.length; i++) {

                                groupDetails = mGroupListMaster[i].split("[$]");

                                groupList = groupDetails[0].split("~");
                                shgcodeList = groupDetails[1].split("~");

                                offline_sAgent_GIDs.addElement(groupList[0].toString());

                                offline_sAgent_Gname.addElement(groupList[1].toString());
                                offline_sAGent_GActiveStatus.addElement(groupList[2].toString());
                                sGroupTransId.addElement(groupList[3].toString());
                                Login_webserviceTask._VerifiedLastAuditDate.add(groupList[5].toString());
                                Log.e("SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS", "!!!!!!!!!!!!!!!!!!!!!!");
                                if (!Login_webserviceTask.mFirstAddTitle) {
                                    Log.e("First Add Title", "False");
                                    Login_webserviceTask.mFirstAddTitle = true;
                                    Login_webserviceTask.mTitleValues[0] = shgcodeList[0].toString();
                                    Login_webserviceTask.mTitleValues[1] = shgcodeList[2].toString();
                                    Login_webserviceTask.mTitleValues[2] = shgcodeList[4].toString();
                                    Login_webserviceTask.mTitleValues[3] = shgcodeList[6].toString();
                                    Login_webserviceTask.mTitleValues[4] = shgcodeList[8].toString();
                                }

                                Login_webserviceTask.sGroupTransId.addElement(groupList[3].toString());

                                Login_webserviceTask.sSHG_Codes_Label.addElement(shgcodeList[0].toString());
                                Login_webserviceTask.sSHG_BlockNames_Label.addElement(shgcodeList[2].toString());
                                Login_webserviceTask.sSHG_PanchayatNames_Label.addElement(shgcodeList[4].toString());
                                Login_webserviceTask.sSHG_VillageNames_Label.addElement(shgcodeList[6].toString());
                                Login_webserviceTask.sSHG_TransactionDate_Label.addElement(shgcodeList[8].toString());

                                Login_webserviceTask.sSHG_Codes.addElement(shgcodeList[1].toString());
                                Login_webserviceTask.sSHG_BlockNames.addElement(shgcodeList[3].toString());
                                Login_webserviceTask.sSHG_PanchayatNames.addElement(shgcodeList[5].toString());
                                Login_webserviceTask.sSHG_VillageNames.addElement(shgcodeList[7].toString());
                                Login_webserviceTask.sSHG_TransactionDate.addElement(shgcodeList[9].toString());

                                Log.e("checking!!!", "@@@@@@@");

                            }

                            Log.e("Step 22222222", "!!@@!@@");
                        } catch (Exception e) {
                            // TODO: handle exception
                        }
                        try {

                            // this is data fro recycler view

                            if (offline_sAgent_Gname != null) {

                                String groupList_offline[] = new String[offline_sAgent_Gname.size()];
                                CommonSetRecyclerview(groupList_offline);
                            } else {
                                TastyToast.makeText(getApplicationContext(), AppStrings.tryLater,
                                        TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                } else {
                    isGrouplist = false;
                    try {
                        String mOfflineGroupresponse = groupdetails.get(0).getGroupnameresponse();
                        Log.e("Grouplist Activity -------->>>>>", mOfflineGroupresponse);
                        EShaktiApplication.setGroupResponse(null);
                        EShaktiApplication.setGroupResponse(mOfflineGroupresponse);

                        Login_webserviceTask.sGroupTransId = new Vector<String>();
                        Login_webserviceTask.sSHG_Codes = new Vector<String>();
                        Login_webserviceTask.sSHG_BlockNames = new Vector<String>();
                        Login_webserviceTask.sSHG_PanchayatNames = new Vector<String>();
                        Login_webserviceTask.sSHG_VillageNames = new Vector<String>();
                        Login_webserviceTask.sSHG_TransactionDate = new Vector<String>();
                        Login_webserviceTask.mTitleValues = null;
                        Login_webserviceTask.mTitleValues = new String[5];
                        Login_webserviceTask.mFirstAddTitle = false;
                        Login_webserviceTask.sSHG_Codes_Label = new Vector<String>();
                        Login_webserviceTask.sSHG_BlockNames_Label = new Vector<String>();
                        Login_webserviceTask.sSHG_PanchayatNames_Label = new Vector<String>();
                        Login_webserviceTask.sSHG_VillageNames_Label = new Vector<String>();
                        Login_webserviceTask.sSHG_TransactionDate_Label = new Vector<String>();
                        Login_webserviceTask._VerifiedLastAuditDate = new Vector<String>();

                        String arr_viewGroups[] = mOfflineGroupresponse.split("#");

                        mGroupListMaster = arr_viewGroups[2].split("%");

                        try {
                            for (int i = 0; i < mGroupListMaster.length; i++) {

                                groupDetails = mGroupListMaster[i].split("[$]");

                                groupList = groupDetails[0].split("~");
                                shgcodeList = groupDetails[1].split("~");

                                offline_sAgent_GIDs.addElement(groupList[0].toString());

                                offline_sAgent_Gname.addElement(groupList[1].toString());
                                offline_sAGent_GActiveStatus.addElement(groupList[2].toString());
                                sGroupTransId.addElement(groupList[3].toString());
                                Login_webserviceTask._VerifiedLastAuditDate.add(groupList[5].toString());

                                if (!Login_webserviceTask.mFirstAddTitle) {
                                    Log.e("First Add Title", "False");
                                    Login_webserviceTask.mFirstAddTitle = true;
                                    Login_webserviceTask.mTitleValues[0] = shgcodeList[0].toString();
                                    Login_webserviceTask.mTitleValues[1] = shgcodeList[2].toString();
                                    Login_webserviceTask.mTitleValues[2] = shgcodeList[4].toString();
                                    Login_webserviceTask.mTitleValues[3] = shgcodeList[6].toString();
                                    Login_webserviceTask.mTitleValues[4] = shgcodeList[8].toString();
                                }

                                Login_webserviceTask.sGroupTransId.addElement(groupList[3].toString());

                                Login_webserviceTask.sSHG_Codes_Label.addElement(shgcodeList[0].toString());
                                Login_webserviceTask.sSHG_BlockNames_Label.addElement(shgcodeList[2].toString());
                                Login_webserviceTask.sSHG_PanchayatNames_Label.addElement(shgcodeList[4].toString());
                                Login_webserviceTask.sSHG_VillageNames_Label.addElement(shgcodeList[6].toString());
                                Login_webserviceTask.sSHG_TransactionDate_Label.addElement(shgcodeList[8].toString());

                                Login_webserviceTask.sSHG_Codes.addElement(shgcodeList[1].toString());
                                Login_webserviceTask.sSHG_BlockNames.addElement(shgcodeList[3].toString());
                                Login_webserviceTask.sSHG_PanchayatNames.addElement(shgcodeList[5].toString());
                                Login_webserviceTask.sSHG_VillageNames.addElement(shgcodeList[7].toString());
                                Login_webserviceTask.sSHG_TransactionDate.addElement(shgcodeList[9].toString());

                            }

                        } catch (Exception e) {
                            // TODO: handle exception
                        }
                        try {

                            // this is data fro recycler view

                            if (offline_sAgent_Gname != null) {

                                String groupList_offline[] = new String[offline_sAgent_Gname.size()];
                                CommonSetRecyclerview(groupList_offline);
                            } else {
                                TastyToast.makeText(getApplicationContext(), AppStrings.tryLater,
                                        TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }
            }
        }

    }

    private void CommonSetRecyclerview(String[] groupnames) {
        // TODO Auto-generated method stub
        try {

            // this is data fro recycler view
            String groupList[] = groupnames;

            listItems = new ArrayList<GroupListItem>();
            for (int i = 0; i < groupList.length; i++) {

                GroupListItem rowItem = new GroupListItem(leftImage, offline_sAgent_Gname.elementAt(i).toString(),
                        listImage);

                listItems.add(rowItem);
            }
            mGroupListView = true;

            setCustomAdapter();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();

        SBus.INST.unRegister(this);
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub

    }

    @Override
    public void onDestroy() {
        if (ConnectionUtils.isNetworkAvailable(getApplicationContext())) {

            if (receiver != null) {

                this.unregisterReceiver(receiver);
            }
        }
        super.onDestroy();
    }

    public class MyWebRequestReceiver extends BroadcastReceiver {

        public static final String PROCESS_RESPONSE = "com.as400samplecode.intent.action.PROCESS_RESPONSE";

        @SuppressWarnings("unused")
        @Override
        public void onReceive(Context context, Intent intent) {
            String responseString = intent.getStringExtra(GroupWisePendingReportService.RESPONSE_STRING);
            String reponseMessage = intent.getStringExtra(GroupWisePendingReportService.RESPONSE_MESSAGE);

            if (publicValues.mGetAnimatorPendingGroupwiseValues != null) {

                listItems = new ArrayList<GroupListItem>();

                listImage = R.drawable.ic_navigate_next_white_24dp;
                leftImage = R.drawable.star;
                mGroupIdlist.clear();

                if (ConnectionUtils.isNetworkAvailable(getApplicationContext())) {
                    // this is data fro recycler view

                    if (PrefUtils.getGrouplistValues() != null && PrefUtils.getGrouplistValues().equals("1")
                            && isGetOfflineData) {

                        String groupList[] = new String[offline_sAgent_Gname.size()];

                        for (int i = 0; i < groupList.length; i++) {
                            GroupListItem item = new GroupListItem(leftImage,
                                    offline_sAgent_Gname.elementAt(i).toString(), listImage);
                            listItems.add(item);
                            mGroupIdlist.add(offline_sAgent_GIDs.elementAt(i).toString());

                        }
                        EShaktiApplication.setCheckGroupListTextColor(true);
                        EShaktiApplication.setGroupListValues(true);
                        mGroupListView = true;

                        setCustomAdapter();

                    } else {

                        if (Login_webserviceTask.sAgent_Gname != null) {

                            String groupList[] = new String[Login_webserviceTask.sAgent_Gname.size()];

                            for (int i = 0; i < groupList.length; i++) {
                                GroupListItem item = new GroupListItem(leftImage,
                                        Login_webserviceTask.sAgent_Gname.elementAt(i).toString(), listImage);
                                listItems.add(item);
                                mGroupIdlist.add(Login_webserviceTask.sAgent_GIDs.elementAt(i).toString());

                            }
                            EShaktiApplication.setCheckGroupListTextColor(true);
                            EShaktiApplication.setGroupListValues(true);
                            mGroupListView = true;

                            setCustomAdapter();
                        }
                    }
                }
            }

        }

    }

    @SuppressLint("LongLogTag")
    @Override
    public void onItemClick(ViewGroup parent, View view, int position) {
        // TODO Auto-generated method stub

        String calDate[] = Get_Cal_CurrentDate.getCurrentDate();
        DatePickerDialog.sDashboardDate = calDate[0];
        DatePickerDialog.sSend_To_Server_Date = calDate[1];
        boolean _iSOfflineDataAvail = false;

        if (EShaktiApplication.isEditOBTransDate()) {
            EShaktiApplication.setEditOBTransDate(false);
        }

        EShaktiApplication
                .setLastTransactionDate(Login_webserviceTask.sSHG_TransactionDate.elementAt(position).toUpperCase());

        EShaktiApplication.setOfflineTrans(true);
        ArrayList<Transaction> transactionProviders_list = (ArrayList<Transaction>) TransactionProvider
                .getTransactionDetails();
        EShaktiApplication.setLastAuditDate(Login_webserviceTask._VerifiedLastAuditDate.elementAt(position).toString());
        Log.e("Last Audit Date ----------->>>",
                Login_webserviceTask._VerifiedLastAuditDate.elementAt(position).toString());
        if (!_IsOfflineDatasAvail) {

            if (transactionProviders_list != null && transactionProviders_list.size() > 0
                    && transactionProviders_list.get(0).getUsername() != null) {
                _iSOfflineDataAvail = true;
                LoginActivity.iSOfflineDataAvail = true;
                _IsOfflineDatasAvail = true;
            } else {
                _iSOfflineDataAvail = false;
                LoginActivity.iSOfflineDataAvail = false;
            }

            EShaktiApplication.setOfflineTrans(false);
        } else {
            _iSOfflineDataAvail = false;
        }

        try {
            if (ConnectionUtils.isNetworkAvailable(getApplicationContext())) {

                Log.e("Boolean Valuessss", isGetOfflineData + "");
                System.out.println(" --------------------------- --------------" + PrefUtils.getGrouplistValues());

                if (Login_webserviceTask._Audit_VerifiedValue.get(position).equals("Yes")) {

                    _isAuditVerifiedService = true;
                    _NgoId = LoginTask.Ngo_Id;
                    _GroupId = Login_webserviceTask.sAgent_GIDs.get(position);
                    _GroupName = Login_webserviceTask.sAgent_Gname.get(position);

                    new Get_Trans_Audit_Verified_Webservices(this).execute();

                } else {
                    if (_iSOfflineDataAvail) {
                        _iSOfflineDataAvail = false;

                        AppDialogUtils.showConfirmationOfflineAvailDialog(GroupListActivity.this);

                    } else {
                        if (PrefUtils.getGrouplistValues() != null && PrefUtils.getGrouplistValues().equals("1")
                                && isGetOfflineData) {
                            GroupDetailsProvider
                                    .getSinlgeGroupMaster(new GroupMasterSingle(offline_sAgent_GIDs.get(position)));

                            if (offline_sAGent_GActiveStatus.get(position).equals("Yes")
                                    && GetGroupMemberDetails.getGroupId() != null) {
                                EShaktiApplication.setGetsinglegrpId(offline_sAgent_GIDs.get(position));
                                SelectedGroupsTask.Group_Name = String.valueOf(offline_sAgent_Gname.get(position));
                                System.out.println(EShaktiApplication.getGetsinglegrpId());
                                mGroupTransId = sGroupTransId.get(position);
                                EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GROUPDETAILS_GETMASTER,
                                        new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));
                                PrefUtils.clearMemberLoanrepay();
                                offline_sAGent_GActiveStatus.clear();
                                PrefUtils.clearGroupMasterResValues();
                                GroupDetailsProvider
                                        .getSinlgeGroupMaster(new GroupMasterSingle(offline_sAgent_GIDs.get(position)));
                                if (GetGroupMemberDetails.getGroupId() != null) {
                                    PrefUtils.setGroupMasterResValues("1");
                                } else {
                                    PrefUtils.clearGroupMasterResValues();
                                }

                            } else {

                                EShaktiApplication.setGetsinglegrpId(offline_sAgent_GIDs.get(position));
                                SelectedGroupsTask.Group_Name = String.valueOf(offline_sAgent_Gname.get(position));
                                sSelectedGroup = String.valueOf(offline_sAgent_GIDs.get(position));
                                System.out.println(EShaktiApplication.getGetsinglegrpId());
                                mGroupActiveStatus = offline_sAGent_GActiveStatus.get(position);
                                if (mGroupActiveStatus.equals("No")) {
                                    isCheckActiveStatus = true;
                                }
                                mGroupTransId = sGroupTransId.get(position);

                                new SelectedGroupsTask(GroupListActivity.this).execute();

                                PrefUtils.clearMemberLoanrepay();
                                offline_sAGent_GActiveStatus.clear();
                                isGetOfflineData = false;

                            }

                        } else {

                            SelectedGroupsTask.Group_Name = String
                                    .valueOf(Login_webserviceTask.sAgent_Gname.get(position));

                            sSelectedGroup = String.valueOf(Login_webserviceTask.sAgent_GIDs.get(position));

                            mGroupActiveStatus = Login_webserviceTask.sAgent_GActiveStatus.get(position);

                            if (mGroupActiveStatus.equals("No")) {
                                isCheckActiveStatus = true;
                            }
                            mGroupTransId = Login_webserviceTask.sGroupTransId.get(position);

                            new SelectedGroupsTask(GroupListActivity.this).execute();

                            PrefUtils.clearMemberLoanrepay();
                            EShaktiApplication.setGetsinglegrpId(Login_webserviceTask.sAgent_GIDs.get(position));
                            PrefUtils.setGroupMasterResValues("1");

                            isGetOfflineData = false;

                        }
                    }
                }

            } else {

                if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {

                    try {

                        if (offline_sAGent_GActiveStatus.get(position).equals("Yes")) {

                            EShaktiApplication.setGetsinglegrpId(offline_sAgent_GIDs.get(position));
                            SelectedGroupsTask.Group_Name = String.valueOf(offline_sAgent_Gname.get(position));
                            System.out.println(EShaktiApplication.getGetsinglegrpId());

                            EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GROUPDETAILS_GETMASTER,
                                    new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));
                            PrefUtils.clearMemberLoanrepay();
                            offline_sAGent_GActiveStatus.clear();
                        } else {
                            TastyToast.makeText(getApplicationContext(), AppStrings.mVerifyGroupAlert,
                                    TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                        }
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                } else {

                    TastyToast.makeText(getApplicationContext(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
                            TastyToast.ERROR);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onItemClickVerification(ViewGroup parent, View view, int position) {

    }

}
