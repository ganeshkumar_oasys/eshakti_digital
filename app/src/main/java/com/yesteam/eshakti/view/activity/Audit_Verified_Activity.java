package com.yesteam.eshakti.view.activity;

import java.util.ArrayList;

import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.CustomTypefaceSpan;
import com.yesteam.eshakti.utils.GetExit;
import com.yesteam.eshakti.utils.GetTypeface;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.views.ButtonFlat;
import com.yesteam.eshakti.views.RaisedButton;
import com.yesteam.eshakti.webservices.Get_Trans_Audit_Update_Webservices;
import com.yesteam.eshakti.webservices.Login_webserviceTask;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.TextAppearanceSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Audit_Verified_Activity extends AppCompatActivity implements OnClickListener, TaskListener {

	private TextView mGroupName, mTransDateTextview, mTransHeader;
	private Toolbar mToolbar;
	Context context;
	Typeface sTypeface;
	String mLanguageLocalae = "";
	private TextView mHeader;
	String mTransactionAuditDate;
	String mTransactionName;
	ArrayList<String> mMemberNameList = new ArrayList<String>();
	ArrayList<String> mPreAmountList = new ArrayList<String>();
	ArrayList<String> mCurrentAmountList = new ArrayList<String>();
	int transactionCount = 1;
	ArrayList<String> column_1_list = new ArrayList<String>();
	ArrayList<String> column_2_list = new ArrayList<String>();
	ArrayList<String> column_3_list = new ArrayList<String>();
	ArrayList<String> column_4_list = new ArrayList<String>();
	ArrayList<String> column_5_list = new ArrayList<String>();
	ArrayList<String> column_6_list = new ArrayList<String>();
	int columnCount = 0;
	int headerColumnCount = 0;
	TableLayout headerTable, contentTable;
	private RaisedButton mSubmit_Raised_Button;
	String[] mSavingsHeader = new String[] { "MEMBER NAME", "PREVIOUS AMOUNT", "CURRENT AMOUNT" };
	String[] mSavingsDisbHeader = new String[] { "MEMBER NAME", "PREVIOUS AMOUNT", "CURRENT AMOUNT" };
	String[] mVSavingsHeader = new String[] { "MEMBER NAME", "PREVIOUS AMOUNT", "CURRENT AMOUNT" };
	String[] mVSavingsDisbHedaer = new String[] { "MEMBER NAME", "PREVIOUS AMOUNT", "CURRENT AMOUNT" };
	String[] mIncomeDisbHeader = new String[] { "MEMBER NAME", "PREVIOUS AMOUNT", "CURRENT AMOUNT" };
	String[] mPenaltyHeader = new String[] { "MEMBER NAME", "PREVIOUS AMOUNT", "CURRENT AMOUNT" };
	String[] mOtherIncomeHeader = new String[] { "MEMBER NAME", "PREVIOUS AMOUNT", "CURRENT AMOUNT" };
	String[] mOtherIncomeToGroupHeader = new String[] { "DETAIL", "PREVIOUS AMOUNT", "CURRENT AMOUNT" };
	String[] mDonationHeader = new String[] { "DETAIL", "PREVIOUS AMOUNT", "CURRENT AMOUNT" };
	String[] mSeedFundHeader = new String[] { "DETAIL", "PREVIOUS AMOUNT", "CURRENT AMOUNT" };
	String[] mSubscriptionHeader = new String[] { "MEMBER NAME", "PREVIOUS AMOUNT", "CURRENT AMOUNT" };
	String[] mExpensesHeader = new String[] { "EXPENSE DETAIL", "PREVIOUS AMOUNT", "CURRENT AMOUNT" };
	String[] mMemberInternalLoanRepayHedaer = new String[] { "MEMBER NAME", "PREVIOUS AMOUNT", "PREVIOUS INTREST",
			"CURRENT AMOUNT", "CURRENT INTREST" };
	String[] mMemberBankLoanRepayHeader = new String[] { "MEMBER NAME", "PREVIOUS AMOUNT", "PREVIOUS INTREST",
			"CURRENT AMOUNT", "CURRENT INTREST" };
	String[] mGroupLoanRepayHeader = new String[] { "DETAIL", "PREVIOUS AMOUNT", "CURRENT AMOUNT" };
	String[] mInternalLoanDisbHeader = new String[] { "MEMBER NAME", "PREVIOUS AMOUNT ", "CURRENT AMOUNT" };
	String[] mBankTransactionHeader = new String[] { "DETAIL", "PREVIOUS AMOUNT", "CURRENT AMOUNT" };// , "CURRENT
																										// AMOUNT"};
	String[] mFDHeader = new String[] { "DETAIL", "PREVIOUS AMOUNT", "CURRENT AMOUNT" };// , "CURRENT AMOUNT"};
	String[] mBankLoanDisbHeader = new String[] { "DETAIL", "PREVIOUS AMOUNT", "CURRENT AMOUNT" };
	String[] mBankLoanDisbToMemberHeader = new String[] { "MEMBER NAME", "PREVIOUS AMOUNT", "CURRENT AMOUNT" };
	String[] mBankLoanInstallmentHeader;
	String[] mBankLoanIncreaseLimitHeader = new String[] { "DETAIL", "PREVIOUS AMOUNT", "CURRENT AMOUNT" };
	int headerColumnWidth[], contentColumnWidth[];
	TableRow.LayoutParams headerParams, params;
	private Dialog mProgressDialog;
	boolean isGroupListServiceCall = false;
	boolean isAuditTransUpdateServiceCall = false;
	boolean isServiceCall = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_trans_audit);

		try {

			if (PrefUtils.getUserlangcode() != null) {
				mLanguageLocalae = PrefUtils.getUserlangcode();
			} else {
				mLanguageLocalae = null;
			}

			sTypeface = GetTypeface.getTypeface(getApplicationContext(), mLanguageLocalae);

		} catch (Exception e) {
			e.printStackTrace();
		}

		mToolbar = (Toolbar) findViewById(R.id.trans_audit_toolbar);
		TextView mTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);
		setSupportActionBar(mToolbar);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setTitle("");
		mTitle.setText(PrefUtils.getAnimatorName() + "    " + PrefUtils.getAgentUserName());
		mTitle.setTypeface(sTypeface);
		mTitle.setGravity(Gravity.CENTER);

		mGroupName = (TextView) findViewById(R.id.trans_audit_group_name_header);
		mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(GroupListActivity._GroupName)));
		mGroupName.setTypeface(LoginActivity.sTypeface);
		mGroupName.setTextColor(getResources().getColor(R.color.header_color));

		mTransDateTextview = (TextView) findViewById(R.id.trans_audit_date_header);
		mTransDateTextview.setTypeface(LoginActivity.sTypeface);

		mTransHeader = (TextView) findViewById(R.id.trans_audit_transaction_headertext);
		mTransHeader.setTypeface(LoginActivity.sTypeface);

		headerTable = (TableLayout) findViewById(R.id.trans_audi_headerTable);
		contentTable = (TableLayout) findViewById(R.id.trans_audi_contentTable);

		headerParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT, 60);

		mSubmit_Raised_Button = (RaisedButton) findViewById(R.id.trans_audit_Submit_button);
		if (GroupListActivity.mTransAuditMasterCount == GroupListActivity.mTransAuditMasterSize) {

			if (GroupListActivity.mTransAuditChildCount == GroupListActivity.mTransAuditChildSize) {

				mSubmit_Raised_Button.setText(RegionalConversion.getRegionalConversion(AppStrings.submit));
			} else {
				mSubmit_Raised_Button.setText(RegionalConversion.getRegionalConversion(AppStrings.next));
			}

		} else {
			mSubmit_Raised_Button.setText(RegionalConversion.getRegionalConversion(AppStrings.next));
		}

		mSubmit_Raised_Button.setTypeface(LoginActivity.sTypeface);
		mSubmit_Raised_Button.setOnClickListener(this);

		try {

			int i = GroupListActivity.mTransAuditMasterCount;

			String[] datewiseTransDetailArr = GroupListActivity.mTransAuditChildDetailList.get(i - 1).split("!");
			mTransactionAuditDate = datewiseTransDetailArr[0];
			mTransDateTextview.setText("Transaction Audit Date : " + mTransactionAuditDate);
			// Test

			for (int j = 0; j < datewiseTransDetailArr.length; j++) {
				System.out.println("-------------Single child detail  =  " + datewiseTransDetailArr[j]);
			}
			//

			GroupListActivity.mTransAuditChildSize = datewiseTransDetailArr.length - 1;

			int j = GroupListActivity.mTransAuditChildCount;

			String[] transactionlistArr = datewiseTransDetailArr[j].split("#");
			mTransactionName = transactionlistArr[0];
			mTransHeader.setText(mTransactionName);

			column_1_list.clear();
			column_2_list.clear();
			column_3_list.clear();
			column_4_list.clear();
			column_5_list.clear();
			column_6_list.clear();

			String[] details = null;
			int init = 0;

			if (mTransactionName.equals("MEMBER BANK LOAN REPAYMENT")) {
				init = 1;
			} else if (mTransactionName.equals("GROUP LOAN REPAYMENT") || mTransactionName.equals("BANK TRANSACTION")
					|| mTransactionName.equals("FD BANK TRANSACTION")) {
				init = 2;
			} else if (mTransactionName.equals("SAVINGS") || mTransactionName.equals("EXPENSES")
					|| mTransactionName.equals("INTERNAL LOAN DISBURSEMENT")
					|| mTransactionName.equals("VOLUNTARY SAVINGS") || mTransactionName.equals("SAVINGS DISBURSEMENT")
					|| mTransactionName.equals("VOLUNTARY SAVINGS DISBURSEMENT")
					|| mTransactionName.equals("INCOME DISBURSEMENT") || mTransactionName.equals("PENALTY")
					|| mTransactionName.equals("OTHER INCOME") || mTransactionName.equals("DONATION")
					|| mTransactionName.equals("SUBSCRIPTION")
					|| mTransactionName.equals("MEMBER INTERNAL LOAN REPAYMENT")
					|| mTransactionName.equals("OTHER INCOME TO GROUP") || mTransactionName.equals("SEED FUND")) {
				init = 1;
			}
			for (int k = init; k < transactionlistArr.length; k++) {

				details = transactionlistArr[k].split("~");

				int size = details.length;
				columnCount = size;
				headerColumnCount = size;

				if (size == 2) {
					column_1_list.add(details[0]);
					column_2_list.add(details[1]);
				} else if (size == 3) {
					column_1_list.add(details[0]);
					column_2_list.add(details[1]);
					column_3_list.add(details[2]);
				} else if (size == 4) {
					column_1_list.add(details[0]);
					column_2_list.add(details[1]);
					column_3_list.add(details[2]);
					column_4_list.add(details[3]);
				} else if (size == 5) {
					column_1_list.add(details[0]);
					column_2_list.add(details[1]);
					column_3_list.add(details[2]);
					column_4_list.add(details[3]);
					column_5_list.add(details[4]);
				} else if (size == 6) {
					column_1_list.add(details[0]);
					column_2_list.add(details[1]);
					column_3_list.add(details[2]);
					column_4_list.add(details[3]);
					column_5_list.add(details[4]);
					column_6_list.add(details[5]);
				}

			}

			if (mTransactionName.equals("MEMBER BANK LOAN REPAYMENT")) {

				mTransHeader.setText(mTransactionName + " - " + column_2_list.get(0).toString());
				headerColumnCount = mMemberBankLoanRepayHeader.length;
				columnCount = column_1_list.size();
				buildTableHeader(headerColumnCount, mMemberBankLoanRepayHeader);
			} else if (mTransactionName.equals("GROUP LOAN REPAYMENT")) {
				String[] loanName = transactionlistArr[1].split("~");
				mTransHeader.setText(mTransactionName + " - " + loanName[1]);
				headerColumnCount = mGroupLoanRepayHeader.length;
				columnCount = column_1_list.size();
				// buildTableHeader(headerColumnCount, mGroupLoanRepayHeader);
				buildGroupLoanTable(headerColumnCount, mGroupLoanRepayHeader);
			} else if (mTransactionName.equals("SAVINGS")) {
				mTransHeader.setText(mTransactionName);
				headerColumnCount = mSavingsHeader.length;
				columnCount = column_1_list.size();
				buildTableHeader(headerColumnCount, mSavingsHeader);
			} else if (mTransactionName.equals("EXPENSES")) {
				mTransHeader.setText(mTransactionName);
				headerColumnCount = mExpensesHeader.length;
				columnCount = column_1_list.size();
				buildTableHeader(headerColumnCount, mExpensesHeader);
			} else if (mTransactionName.equals("INTERNAL LOAN DISBURSEMENT")) {
				mTransHeader.setText(mTransactionName);
				headerColumnCount = mInternalLoanDisbHeader.length;
				columnCount = column_1_list.size();
				buildTableHeader(headerColumnCount, mInternalLoanDisbHeader);
			} else if (mTransactionName.equals("VOLUNTARY SAVINGS")) {
				mTransHeader.setText(mTransactionName);
				headerColumnCount = mVSavingsHeader.length;
				columnCount = column_1_list.size();
				buildTableHeader(headerColumnCount, mVSavingsHeader);
			} else if (mTransactionName.equals("SAVINGS DISBURSEMENT")) {
				mTransHeader.setText(mTransactionName);
				headerColumnCount = mSavingsDisbHeader.length;
				columnCount = column_1_list.size();
				buildTableHeader(headerColumnCount, mSavingsDisbHeader);
			} else if (mTransactionName.equals("VOLUNTARY SAVINGS DISBURSEMENT")) {
				mTransHeader.setText(mTransactionName);
				headerColumnCount = mVSavingsDisbHedaer.length;
				columnCount = column_1_list.size();
				buildTableHeader(headerColumnCount, mVSavingsDisbHedaer);
			} else if (mTransactionName.equals("INCOME DISBURSEMENT")) {
				mTransHeader.setText(mTransactionName);
				headerColumnCount = mIncomeDisbHeader.length;
				columnCount = column_1_list.size();
				buildTableHeader(headerColumnCount, mIncomeDisbHeader);
			} else if (mTransactionName.equals("PENALTY")) {
				mTransHeader.setText(mTransactionName);
				headerColumnCount = mSavingsHeader.length;
				columnCount = column_1_list.size();
				buildTableHeader(headerColumnCount, mSavingsHeader);
			} else if (mTransactionName.equals("OTHER INCOME")) {
				mTransHeader.setText(mTransactionName);
				headerColumnCount = mOtherIncomeHeader.length;
				columnCount = column_1_list.size();
				buildTableHeader(headerColumnCount, mOtherIncomeHeader);
			} else if (mTransactionName.equals("DONATION")) {
				mTransHeader.setText(mTransactionName);
				headerColumnCount = mDonationHeader.length;
				columnCount = column_1_list.size();
				buildTableHeader(headerColumnCount, mDonationHeader);
			} else if (mTransactionName.equals("SUBSCRIPTION")) {
				mTransHeader.setText(mTransactionName);
				headerColumnCount = mSubscriptionHeader.length;
				columnCount = column_1_list.size();
				buildTableHeader(headerColumnCount, mSubscriptionHeader);
			} else if (mTransactionName.equals("MEMBER INTERNAL LOAN REPAYMENT")) {
				mTransHeader.setText(mTransactionName);
				headerColumnCount = mMemberInternalLoanRepayHedaer.length;
				columnCount = column_1_list.size();
				buildTableHeader(headerColumnCount, mMemberInternalLoanRepayHedaer);
			} else if (mTransactionName.equals("BANK TRANSACTION")) {
				String[] bankName = transactionlistArr[1].split("~");
				mTransHeader.setText(mTransactionName + " - " + bankName[1]);
				headerColumnCount = mBankTransactionHeader.length;
				columnCount = column_1_list.size();
				// buildTableHeader(headerColumnCount, mBankTransactionHeader);
				buildGroupLoanTable(headerColumnCount, mBankTransactionHeader);
			} else if (mTransactionName.equals("FD BANK TRANSACTION")) {
				String[] bankName = transactionlistArr[1].split("~");
				mTransHeader.setText(mTransactionName + " - " + bankName[1]);
				headerColumnCount = mFDHeader.length;
				columnCount = column_1_list.size();
				buildTableHeader(headerColumnCount, mFDHeader);
			} else if (mTransactionName.equals("OTHER INCOME TO GROUP")) {
				mTransHeader.setText(mTransactionName);
				headerColumnCount = mOtherIncomeToGroupHeader.length;
				columnCount = column_1_list.size();
				buildTableHeader(headerColumnCount, mOtherIncomeToGroupHeader);
			} else if (mTransactionName.equals("SEED FUND")) {
				mTransHeader.setText(mTransactionName);
				headerColumnCount = mSeedFundHeader.length;
				columnCount = column_1_list.size();
				buildTableHeader(headerColumnCount, mSeedFundHeader);
			}

			if (!mTransactionName.equals("GROUP LOAN REPAYMENT") && !mTransactionName.equals("BANK TRANSACTION")) {
				buildContentTable(columnCount);
			}

			if (!mTransactionName.equals("GROUP LOAN REPAYMENT") && !mTransactionName.equals("BANK TRANSACTION")) {

				onChangeWidthOfColumn();
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	private void buildGroupLoanTable(int headerColumnCount2, String[] headerArr) {
		try {

			// TODO Auto-generated method stub

			TableRow head_row = new TableRow(Audit_Verified_Activity.this);
			TableRow.LayoutParams params = new TableRow.LayoutParams(0, LayoutParams.WRAP_CONTENT, 1f);

			TextView head_details = new TextView(Audit_Verified_Activity.this);
			head_details.setText(headerArr[0]);
			head_details.setTypeface(sTypeface);
			head_details.setTextColor(Color.WHITE);
			head_details.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER);
			head_details.setPadding(20, 5, 20, 5);
			head_details.setBackgroundResource(R.color.tableHeader);
			head_details.setLayoutParams(params);
			head_row.addView(head_details);

			TextView head_details1 = new TextView(Audit_Verified_Activity.this);
			head_details1.setText(headerArr[1]);
			head_details1.setTypeface(sTypeface);
			head_details1.setTextColor(Color.WHITE);
			head_details1.setGravity(Gravity.CENTER_VERTICAL | Gravity.RIGHT);
			head_details1.setPadding(20, 5, 20, 5);
			head_details1.setBackgroundResource(R.color.tableHeader);
			head_details1.setLayoutParams(params);
			head_row.addView(head_details1);

			TextView head_details11 = new TextView(Audit_Verified_Activity.this);
			head_details11.setText(headerArr[2]);
			head_details11.setTypeface(sTypeface);
			head_details11.setTextColor(Color.WHITE);
			head_details11.setGravity(Gravity.CENTER_VERTICAL | Gravity.RIGHT);
			head_details11.setPadding(20, 5, 20, 5);
			head_details11.setBackgroundResource(R.color.tableHeader);
			head_details11.setLayoutParams(params);
			head_row.addView(head_details11);

			headerTable.addView(head_row, new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
					TableLayout.LayoutParams.WRAP_CONTENT));

			for (int j = 0; j < columnCount; j++) {

				TableRow contentRow = new TableRow(Audit_Verified_Activity.this);
				// TableRow.LayoutParams contentParams = new
				// TableRow.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT,
				// 1f);

				TextView details = new TextView(Audit_Verified_Activity.this);
				details.setText(column_1_list.get(j).toString());
				details.setPadding(20, 5, 20, 5);
				details.setTextColor(color.black);
				details.setLayoutParams(params);
				details.setTypeface(sTypeface);
				contentRow.addView(details);

				TextView details1 = new TextView(Audit_Verified_Activity.this);
				details1.setText(column_2_list.get(j).toString());
				details1.setPadding(20, 5, 20, 5);
				details1.setTextColor(color.black);
				details1.setGravity(Gravity.RIGHT);
				details1.setTypeface(sTypeface);
				details1.setLayoutParams(params);// (contentParams);
				contentRow.addView(details1);

				TextView details11 = new TextView(Audit_Verified_Activity.this);
				details11.setText(column_3_list.get(j).toString());
				details11.setPadding(20, 5, 20, 5);
				details11.setTextColor(color.black);
				details11.setGravity(Gravity.RIGHT);
				details11.setTypeface(sTypeface);
				details11.setLayoutParams(params);// (contentParams);
				contentRow.addView(details11);

				contentTable.addView(contentRow,
						new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

			}

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private void buildContentTable(int rowCount) {
		// TODO Auto-generated method stub

		for (int j = 0; j < columnCount; j++) {

			TableRow contentRow = new TableRow(Audit_Verified_Activity.this);
			TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);

			if (headerColumnCount == 2) {

				TextView details = new TextView(Audit_Verified_Activity.this);
				details.setText(column_1_list.get(j).toString());
				details.setPadding(20, 5, 20, 5);
				details.setTextColor(color.black);
				details.setTypeface(sTypeface);
				details.setLayoutParams(headerParams);
				contentRow.addView(details);

				TextView details1 = new TextView(Audit_Verified_Activity.this);
				details1.setText(column_2_list.get(j).toString());
				details1.setPadding(20, 5, 20, 5);
				details1.setTextColor(color.black);
				details1.setTypeface(sTypeface);
				details1.setGravity(Gravity.RIGHT);
				details1.setLayoutParams(headerParams);// (contentParams);
				contentRow.addView(details1);

			} else if (headerColumnCount == 3) {

				TextView details = new TextView(Audit_Verified_Activity.this);
				details.setText(column_1_list.get(j).toString());
				details.setPadding(20, 5, 20, 5);
				details.setTextColor(color.black);
				details.setTypeface(sTypeface);
				details.setLayoutParams(headerParams);
				contentRow.addView(details);

				TextView details1 = new TextView(Audit_Verified_Activity.this);
				details1.setText(column_2_list.get(j).toString());
				details1.setPadding(20, 5, 20, 5);
				details1.setTextColor(color.black);
				details1.setTypeface(sTypeface);
				details1.setGravity(Gravity.RIGHT);
				details1.setLayoutParams(headerParams);// (contentParams);
				contentRow.addView(details1);

				TextView details11 = new TextView(Audit_Verified_Activity.this);
				details11.setText(column_3_list.get(j).toString());
				details11.setPadding(20, 5, 20, 5);
				details11.setTextColor(color.black);
				details11.setTypeface(sTypeface);
				details11.setGravity(Gravity.RIGHT);
				details11.setLayoutParams(headerParams);// (contentParams);
				contentRow.addView(details11);

			} else if (headerColumnCount == 4) {

				TextView details = new TextView(Audit_Verified_Activity.this);
				details.setText(column_1_list.get(j).toString());
				details.setPadding(20, 5, 20, 5);
				details.setTextColor(color.black);
				details.setTypeface(sTypeface);
				details.setLayoutParams(headerParams);// (contentParams);
				contentRow.addView(details);

				TextView details1 = new TextView(Audit_Verified_Activity.this);
				details1.setText(column_2_list.get(j).toString());
				details1.setPadding(20, 5, 20, 5);
				details1.setTextColor(color.black);
				details1.setGravity(Gravity.RIGHT);
				details1.setTypeface(sTypeface);
				details1.setLayoutParams(headerParams);// (contentParams);
				contentRow.addView(details1);

				TextView details11 = new TextView(Audit_Verified_Activity.this);
				details11.setText(column_3_list.get(j).toString());
				details11.setPadding(20, 5, 20, 5);
				details11.setTextColor(color.black);
				details11.setGravity(Gravity.RIGHT);
				details11.setTypeface(sTypeface);
				details11.setLayoutParams(headerParams);// (contentParams);
				contentRow.addView(details11);

				TextView details111 = new TextView(Audit_Verified_Activity.this);
				details111.setText(column_4_list.get(j).toString());
				details111.setPadding(20, 5, 20, 5);
				details111.setTextColor(color.black);
				details111.setTypeface(sTypeface);
				details111.setGravity(Gravity.RIGHT);
				details111.setLayoutParams(headerParams);// (contentParams);
				contentRow.addView(details111);

			} else if (headerColumnCount == 5) {

				TextView details = new TextView(Audit_Verified_Activity.this);
				details.setText(column_1_list.get(j).toString());
				details.setPadding(20, 5, 20, 5);
				details.setTextColor(color.black);
				details.setTypeface(sTypeface);
				details.setLayoutParams(headerParams);// (contentParams);
				contentRow.addView(details);

				if (!mTransactionName.equals("MEMBER BANK LOAN REPAYMENT")) {

					TextView details1 = new TextView(Audit_Verified_Activity.this);
					details1.setText(column_2_list.get(j).toString());
					details1.setPadding(20, 5, 20, 5);
					details1.setTextColor(color.black);
					details1.setGravity(Gravity.RIGHT);
					details1.setTypeface(sTypeface);
					details1.setLayoutParams(headerParams);// (contentParams);
					contentRow.addView(details1);

				}

				TextView details11 = new TextView(Audit_Verified_Activity.this);
				details11.setText(column_3_list.get(j).toString());
				details11.setPadding(20, 5, 20, 5);
				details11.setTextColor(color.black);
				details11.setGravity(Gravity.RIGHT);
				details11.setTypeface(sTypeface);
				details11.setLayoutParams(headerParams);// (contentParams);
				contentRow.addView(details11);

				TextView details111 = new TextView(Audit_Verified_Activity.this);
				details111.setText(column_4_list.get(j).toString());
				details111.setPadding(20, 5, 20, 5);
				details111.setTextColor(color.black);
				details111.setGravity(Gravity.RIGHT);
				details111.setTypeface(sTypeface);
				details111.setLayoutParams(headerParams);// (contentParams);
				contentRow.addView(details111);

				TextView details1111 = new TextView(Audit_Verified_Activity.this);
				details1111.setText(column_5_list.get(j).toString());
				details1111.setPadding(20, 5, 20, 5);
				details1111.setTextColor(color.black);
				details1111.setGravity(Gravity.RIGHT);
				details1111.setTypeface(sTypeface);
				details1111.setLayoutParams(headerParams);// (contentParams);
				contentRow.addView(details1111);

				if (!mTransactionName.equals("MEMBER INTERNAL LOAN REPAYMENT")) {

					TextView details11111 = new TextView(Audit_Verified_Activity.this);
					details11111.setText(column_6_list.get(j).toString());
					details11111.setPadding(20, 5, 20, 5);
					details11111.setTextColor(color.black);
					details11111.setGravity(Gravity.RIGHT);
					details11111.setTypeface(sTypeface);
					details11111.setLayoutParams(headerParams);// (contentParams);
					contentRow.addView(details11111);
				}

			}

			contentTable.addView(contentRow,
					new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

		}

		// }

	}

	private void buildTableHeader(int columnCount, String[] headerArr) {
		// TODO Auto-generated method stub

		TableRow head_row = new TableRow(Audit_Verified_Activity.this);
		// TableRow.LayoutParams headerParams = new
		// TableRow.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT,
		// 1f);

		for (int i = 0; i < columnCount; i++) {

			TextView head_details = new TextView(Audit_Verified_Activity.this);
			head_details.setText(headerArr[i]);
			head_details.setTypeface(sTypeface);
			head_details.setTextColor(Color.WHITE);
			if (i == 0) {
				head_details.setGravity(Gravity.CENTER_VERTICAL);
			} else {

				head_details.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
			}

			if (mTransactionName.equals("BANK TRANSACTION")) {
				if (i == 1) {
					head_details.setPadding(100, 5, 20, 5);
				}
			} else {
				head_details.setPadding(20, 5, 20, 5);
			}

			head_details.setBackgroundResource(R.color.tableHeader);
			head_details.setLayoutParams(headerParams);
			head_row.addView(head_details);

		}

		headerTable.addView(head_row, new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
				TableLayout.LayoutParams.WRAP_CONTENT));

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (GroupListActivity.mTransAuditMasterCount <= GroupListActivity.mTransAuditMasterSize) {

			if (GroupListActivity.mTransAuditChildCount < GroupListActivity.mTransAuditChildSize) {

				GroupListActivity.mTransAuditChildCount++;

				Intent intent = new Intent(Audit_Verified_Activity.this, Audit_Verified_Activity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

				finish();
			} else if (GroupListActivity.mTransAuditChildCount == GroupListActivity.mTransAuditChildSize) {
				if (GroupListActivity.mTransAuditMasterCount != GroupListActivity.mTransAuditMasterSize) {
					GroupListActivity.mTransAuditMasterCount++;
					GroupListActivity.mTransAuditChildCount = 1;
					Intent intent = new Intent(Audit_Verified_Activity.this, Audit_Verified_Activity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
					overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

					finish();
				} else {
					showConfirmationTransAuditDialog(Audit_Verified_Activity.this);
				}
			}
		}

	}

	private void onChangeWidthOfColumn() {
		// TODO Auto-generated method stub
		headerColumnWidth = new int[((TableRow) headerTable.getChildAt(0)).getChildCount()];

		TableRow headerRow = (TableRow) headerTable.getChildAt(0);

		contentColumnWidth = new int[((TableRow) contentTable.getChildAt(0)).getChildCount()];

		TableRow contentRow = (TableRow) contentTable.getChildAt(0);

		for (int i = 0; i < headerRow.getChildCount(); i++) {
			headerColumnWidth[i] = this.viewWidth(headerRow.getChildAt(i));

			contentColumnWidth[i] = this.viewWidth(contentRow.getChildAt(i));

		}

		for (int i = 0; i < headerColumnWidth.length; i++) {

			if (headerColumnWidth[i] > contentColumnWidth[i]) {
				TextView contentTextView = (TextView) contentRow.getChildAt(i);
				contentTextView
						.setLayoutParams(new TableRow.LayoutParams(headerColumnWidth[i], LayoutParams.MATCH_PARENT));
			} else {
				TextView headerTextView = (TextView) headerRow.getChildAt(i);
				headerTextView.setLayoutParams(
						new TableRow.LayoutParams(contentColumnWidth[i] + 10, LayoutParams.MATCH_PARENT));
			}
		}

	}

	// read a view's width
	private int viewWidth(View view) {
		view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
		return view.getMeasuredWidth();
	}

	// read a view's height
	private int viewHeight(View view) {
		view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
		return view.getMeasuredHeight();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.

		if (Boolean.valueOf(EShaktiApplication.isAgent)) {
			getMenuInflater().inflate(R.menu.menu_edit_ob, menu);
			MenuItem item = menu.getItem(0);
			item.setVisible(true);
			MenuItem logOutItem = menu.getItem(1);
			logOutItem.setVisible(true);

			mLanguageLocalae = PrefUtils.getUserlangcode();

			LoginActivity.sTypeface = GetTypeface.getTypeface(getApplicationContext(), mLanguageLocalae);
			SpannableStringBuilder SS = new SpannableStringBuilder(
					RegionalConversion.getRegionalConversion(AppStrings.groupList));

			SS.setSpan(new CustomTypefaceSpan("", LoginActivity.sTypeface), 0,
					RegionalConversion.getRegionalConversion(AppStrings.groupList).length(),
					Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

			SS.setSpan(new TextAppearanceSpan(getApplicationContext(), R.style.text_menu), 0,
					RegionalConversion.getRegionalConversion(AppStrings.groupList).length(),
					Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

			SpannableStringBuilder logOutBuilder = new SpannableStringBuilder(
					RegionalConversion.getRegionalConversion(AppStrings.logOut));

			logOutBuilder.setSpan(new CustomTypefaceSpan("", LoginActivity.sTypeface), 0,
					RegionalConversion.getRegionalConversion(AppStrings.logOut).length(),
					Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

			logOutBuilder.setSpan(new TextAppearanceSpan(getApplicationContext(), R.style.text_menu), 0,
					RegionalConversion.getRegionalConversion(AppStrings.logOut).length(),
					Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

			if (item.getItemId() == R.id.action_grouplist_edit) {

				item.setTitle(SS);

			}
			if (logOutItem.getItemId() == R.id.menu_logout_edit) {

				logOutItem.setTitle(logOutBuilder);
			}

		} else {

			getMenuInflater().inflate(R.menu.menu_edit_ob_group, menu);

			MenuItem item = menu.getItem(0);
			item.setVisible(true);

			mLanguageLocalae = PrefUtils.getUserlangcode();
			LoginActivity.sTypeface = GetTypeface.getTypeface(getApplicationContext(), mLanguageLocalae);

			SpannableStringBuilder logOutBuilder = new SpannableStringBuilder(
					RegionalConversion.getRegionalConversion(AppStrings.logOut));

			logOutBuilder.setSpan(new CustomTypefaceSpan("", LoginActivity.sTypeface), 0,
					RegionalConversion.getRegionalConversion(AppStrings.logOut).length(),
					Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

			logOutBuilder.setSpan(new TextAppearanceSpan(getApplicationContext(), R.style.text_menu), 0,
					RegionalConversion.getRegionalConversion(AppStrings.logOut).length(),
					Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

			if (item.getItemId() == R.id.group_logout) {
				item.setTitle(logOutBuilder);
			}
		}

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (Boolean.valueOf(EShaktiApplication.isAgent)) {

			// noinspection SimplifiableIfStatement
			if (id == R.id.action_grouplist_edit) {

				try {

					SelectedGroupsTask.loan_Name.clear();
					SelectedGroupsTask.loan_Id.clear();
					SelectedGroupsTask.loan_EngName.clear();
					SelectedGroupsTask.member_Id.clear();
					SelectedGroupsTask.member_Name.clear();
					SelectedGroupsTask.sBankNames.clear();
					SelectedGroupsTask.sEngBankNames.clear();
					SelectedGroupsTask.sBankAmt.clear();
					SelectedGroupsTask.sCashatBank = " ";
					SelectedGroupsTask.sCashinHand = " ";
					EShaktiApplication.setSelectedgrouptask(false);
					EShaktiApplication.setPLOS(false);
					EShaktiApplication.setBankDeposit(false);
					EShaktiApplication.setOfflineTrans(false);
					EShaktiApplication.setOfflineTransDate(false);
					EShaktiApplication.setSubmenuclicked(false);
					EShaktiApplication.setLastTransId("");
					EShaktiApplication.setSetTransValues("");

					Constants.BUTTON_CLICK_FLAG = "0";
					EShaktiApplication.setSubmenuclicked(false);
					Constants.FRAG_INSTANCE_CONSTANT = "0";
					EShaktiApplication.setOnSavedFragment(false);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}

				startActivity(new Intent(Audit_Verified_Activity.this, GroupListActivity.class));
				overridePendingTransition(R.anim.right_to_left_in, R.anim.right_to_left_out);
				finish();
				return true;

			} else if (id == R.id.menu_logout_edit) {
				Log.e(" Logout", "Logout Sucessfully");

				startActivity(new Intent(GetExit.getExitIntent(getApplicationContext())));
				this.finish();
				return true;

			}
		} else {
			if (id == R.id.group_logout_edit) {
				Log.e("Group Logout", "Logout Sucessfully");
				startActivity(new Intent(GetExit.getExitIntent(getApplicationContext())));
				this.finish();
				return true;

			}
		}
		return super.onOptionsItemSelected(item);
	}

	void showConfirmationTransAuditDialog(final Activity activity) {
		final Dialog confirmationDialog = new Dialog(activity);

		LayoutInflater li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View customView = li.inflate(R.layout.dialog_singin_diff, null, false);
		final ButtonFlat mConYesButton;
		final ButtonFlat mConNoButton;

		TextView mConfirmationHeadertextview;
		TextView mConfirmationTexttextview;

		mConfirmationHeadertextview = (TextView) customView.findViewById(R.id.dialog_Title);
		mConfirmationTexttextview = (TextView) customView.findViewById(R.id.dialog_Message);

		mConYesButton = (ButtonFlat) customView.findViewById(R.id.fragment_ok_button_alert);
		mConNoButton = (ButtonFlat) customView.findViewById(R.id.fragment_cancel_button_alert);
		mConYesButton.setText("Approve");
		mConYesButton.setTypeface(LoginActivity.sTypeface);

		mConNoButton.setText("Deny");
		mConNoButton.setTypeface(LoginActivity.sTypeface);

		mConfirmationHeadertextview.setText("CONFIRMATION");
		// Your offline data is still available at local db, So you must move
		// data from db to online server!!!
		mConfirmationTexttextview.setText("Please approve or reject the changes done by Auditor");
		mConfirmationHeadertextview.setTypeface(LoginActivity.sTypeface);
		mConfirmationTexttextview.setTypeface(LoginActivity.sTypeface);
		mConYesButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (confirmationDialog.isShowing() && confirmationDialog != null) {

					confirmationDialog.dismiss();
					EShaktiApplication.setTransAudit_UpdateValue("Approved");

					showConfirmationTransAuditDialog_WebServicesCall(Audit_Verified_Activity.this);
					try {
						Thread.sleep(200);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			}
		});

		mConNoButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (confirmationDialog.isShowing() && confirmationDialog != null) {
					EShaktiApplication.setTransAudit_UpdateValue("Denied");
					confirmationDialog.dismiss();

					showConfirmationTransAuditDialog_WebServicesCall(Audit_Verified_Activity.this);
					try {
						Thread.sleep(200);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});
		confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		confirmationDialog.setCanceledOnTouchOutside(false);
		confirmationDialog.setContentView(customView);
		confirmationDialog.setCancelable(false);
		confirmationDialog.show();

	}

	void showConfirmationTransAuditDialog_WebServicesCall(final Activity activity) {
		final Dialog confirmationDialog = new Dialog(activity);

		LayoutInflater li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View customView = li.inflate(R.layout.dialog_singin_diff, null, false);
		final ButtonFlat mConYesButton;
		final ButtonFlat mConNoButton;

		TextView mConfirmationHeadertextview;
		TextView mConfirmationTexttextview;

		mConfirmationHeadertextview = (TextView) customView.findViewById(R.id.dialog_Title);
		mConfirmationTexttextview = (TextView) customView.findViewById(R.id.dialog_Message);

		mConYesButton = (ButtonFlat) customView.findViewById(R.id.fragment_ok_button_alert);
		mConNoButton = (ButtonFlat) customView.findViewById(R.id.fragment_cancel_button_alert);
		mConYesButton.setText("Proceed");
		mConYesButton.setTypeface(LoginActivity.sTypeface);

		mConNoButton.setText("Cancel");
		mConNoButton.setTypeface(LoginActivity.sTypeface);

		mConfirmationHeadertextview.setText("CONFIRMATION");
		// Your offline data is still available at local db, So you must move
		// data from db to online server!!!
		if (EShaktiApplication.getTransAudit_UpdateValue().equals("Approved")) {
			mConfirmationTexttextview.setText("Changes has been approved and reflected in your accounts");
		} else if (EShaktiApplication.getTransAudit_UpdateValue().equals("Denied")) {
			mConfirmationTexttextview.setText("Changes has been rejected,the IA and DDM will be initimated");
		}

		mConfirmationHeadertextview.setTypeface(LoginActivity.sTypeface);
		mConfirmationTexttextview.setTypeface(LoginActivity.sTypeface);
		mConYesButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (confirmationDialog.isShowing() && confirmationDialog != null) {

					confirmationDialog.dismiss();

					if (!isServiceCall) {
						isServiceCall = true;
						isAuditTransUpdateServiceCall = true;
						new Get_Trans_Audit_Update_Webservices(Audit_Verified_Activity.this).execute();
					}

				}

			}
		});

		mConNoButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (confirmationDialog.isShowing() && confirmationDialog != null) {

					confirmationDialog.dismiss();

				}
			}
		});
		confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		confirmationDialog.setCanceledOnTouchOutside(false);
		confirmationDialog.setContentView(customView);
		confirmationDialog.setCancelable(false);
		confirmationDialog.show();

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub
		// mProgressDialog = ProgressDialog.show(this, "", "");
		mProgressDialog = AppDialogUtils.createProgressDialog(this);
		mProgressDialog.show();
	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub

		if (mProgressDialog != null) {

			/**
			 * Inorder to Start Service for collecting the GroupDetails at background
			 **/
			System.out.println("Task Finished Values:::" + result);
			if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {

				mProgressDialog.dismiss();
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						if (!result.equals("FAIL")) {

							TastyToast.makeText(getApplicationContext(), AppStrings.mCommonNetworkErrorMsg,
									TastyToast.LENGTH_SHORT, TastyToast.ERROR);
							Constants.NETWORKCOMMONFLAG = "SUCCESS";
						}
					}
				});

			} else {

				mProgressDialog.dismiss();
				if (isAuditTransUpdateServiceCall) {
					isAuditTransUpdateServiceCall = false;
					new Login_webserviceTask(Audit_Verified_Activity.this).execute();

				} else {
					try {

						SelectedGroupsTask.loan_Name.clear();
						SelectedGroupsTask.loan_Id.clear();
						SelectedGroupsTask.loan_EngName.clear();
						SelectedGroupsTask.member_Id.clear();
						SelectedGroupsTask.member_Name.clear();
						SelectedGroupsTask.sBankNames.clear();
						SelectedGroupsTask.sEngBankNames.clear();
						SelectedGroupsTask.sBankAmt.clear();
						SelectedGroupsTask.sCashatBank = " ";
						SelectedGroupsTask.sCashinHand = " ";
						EShaktiApplication.setSelectedgrouptask(false);
						EShaktiApplication.setPLOS(false);
						EShaktiApplication.setBankDeposit(false);
						EShaktiApplication.setOfflineTrans(false);
						EShaktiApplication.setOfflineTransDate(false);
						EShaktiApplication.setSubmenuclicked(false);
						EShaktiApplication.setLastTransId("");
						EShaktiApplication.setSetTransValues("");

						Constants.BUTTON_CLICK_FLAG = "0";
						EShaktiApplication.setSubmenuclicked(false);
						Constants.FRAG_INSTANCE_CONSTANT = "0";
						EShaktiApplication.setOnSavedFragment(false);
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}

					startActivity(new Intent(Audit_Verified_Activity.this, GroupListActivity.class));
					overridePendingTransition(R.anim.right_to_left_in, R.anim.right_to_left_out);
					finish();
				}
			}
		}
	}
}