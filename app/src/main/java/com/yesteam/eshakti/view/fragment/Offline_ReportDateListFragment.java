package com.yesteam.eshakti.view.fragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.squareup.otto.Subscribe;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.sqlite.db.model.Transaction;
import com.yesteam.eshakti.sqlite.db.transactions.TransactionManager.DataType;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Offline_ReportDateListFragment extends Fragment implements OnClickListener {

	private TextView mGroupName, mCashInHand, mCashAtBank, mHeadertext;
	int size;
	private LinearLayout linearLayout;
	private static Button mDateButton;

	public static String sSelectedTransDate;
	String response = "";

	public Offline_ReportDateListFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		SBus.INST.register(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.offline_datelist, container, false);

		MainFragment_Dashboard.isBackpressed = false;

		try {

			EShaktiApplication.setOfflineTransDate(true);
			EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GET_TRANS_MASTERVALUES,
					null);

			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashInHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashInHand.setTypeface(LoginActivity.sTypeface);

			mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashAtBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashAtBank.setTypeface(LoginActivity.sTypeface);

			mHeadertext = (TextView) rootView.findViewById(R.id.offline_headertext);
			mHeadertext.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.offlineReports)));
			mHeadertext.setTypeface(LoginActivity.sTypeface);

			linearLayout = (LinearLayout) rootView.findViewById(R.id.offline_button_linear);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return rootView;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		sSelectedTransDate = ((Button) v).getText().toString();

		Fragment fragment = new OfflineReports_TransactionList();
		getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame, fragment).addToBackStack(null)
				.commit();

	}

	@SuppressWarnings("deprecation")
	@Subscribe
	public void OnValues(final ArrayList<Transaction> arrayList) {

		if (arrayList.size() != 0 && arrayList.get(0).getUniqueId() != null) {
			Log.v("Current Array List Size", arrayList.size() + "");
			try {
				for (int i = 0; i < arrayList.size(); i++) {
					String mOfflineGroupresponse = arrayList.get(i).getTransactionDate();
					response = response + mOfflineGroupresponse + ",";
				}
				String[] withDupDateList = response.split(",");

				List<String> list = Arrays.asList(withDupDateList);
				Set<String> set = new LinkedHashSet<String>(list);

				String[] withoutDupDateList = new String[set.size()];
				set.toArray(withoutDupDateList);

				for (int i = 0; i < withoutDupDateList.length; i++) {
					System.out
							.println("----------withoutDupDateList[]---------" + withoutDupDateList[i] + " i pos " + i);

					if ((withDupDateList[i] != null) && (!withoutDupDateList[i].equals(""))) {

						LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,
								LayoutParams.WRAP_CONTENT);
						params.setMargins(0, 5, 0, 5);

						mDateButton = new Button(getActivity());
						mDateButton.setBackground(getResources().getDrawable(R.drawable.custom_button));
						mDateButton.setId(i);
						/*
						 * mDateButton.setText(GetSpanText.getSpanString(
						 * getActivity(),
						 * String.valueOf(withoutDupDateList[i])));
						 */
						mDateButton.setText(
								RegionalConversion.getRegionalConversion(String.valueOf(withoutDupDateList[i])));
						mDateButton.setTextColor(Color.WHITE);
						mDateButton.setTypeface(LoginActivity.sTypeface);
						mDateButton.setOnClickListener(this);

						linearLayout.addView(mDateButton, params);

					}

				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		} else {
			TastyToast.makeText(getActivity(), AppStrings.noofflinedatas, TastyToast.LENGTH_SHORT,
					TastyToast.WARNING);

		}

	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		SBus.INST.unRegister(this);
	}

}
