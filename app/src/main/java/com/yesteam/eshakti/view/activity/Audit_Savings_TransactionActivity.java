package com.yesteam.eshakti.view.activity;

import java.util.ArrayList;
import java.util.List;

import com.oasys.eshakti.digitization.R;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.utils.GetExit;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.utils.GetTypeface;
import com.yesteam.eshakti.utils.Get_EdiText_Filter;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.views.CustomHorizontalScrollView;
import com.yesteam.eshakti.views.RaisedButton;
import com.yesteam.eshakti.views.CustomHorizontalScrollView.onScrollChangedListener;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.InputType;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Audit_Savings_TransactionActivity extends AppCompatActivity implements OnClickListener{

	Toolbar mToolbar;
	Context context;
	Typeface sTypeface;
	String mLanguageLocalae = "";
	private TextView mHeader;
	private TableLayout mLeftHeaderTable, mRightHeaderTable, mLeftContentTable, mRightContentTable;
	private CustomHorizontalScrollView mHSRightHeader, mHSRightContent;
	int mSize;
	private EditText mCredit_EditText,mDebit_EditText;
	public static List<EditText> sCredit_Fields = new ArrayList<EditText>();
	public static List<EditText> sDebit_Fields = new ArrayList<EditText>();
	private RaisedButton mSubmit_Raised_Button;
	String width[] = { AppStrings.mCredit, AppStrings.mDebit, AppStrings.OutsatndingAmt };
	int[] rightHeaderWidth = new int[width.length];
	int[] rightContentWidth = new int[width.length];
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.audit_activity_savings_transaction);
		
	
			try {

				if (PrefUtils.getUserlangcode() != null) {
					mLanguageLocalae = PrefUtils.getUserlangcode();
				} else {
					mLanguageLocalae = null;
				}

				sTypeface = GetTypeface.getTypeface(getApplicationContext(), mLanguageLocalae);

			} catch (Exception e) {
				e.printStackTrace();
			}

			try {

			mToolbar = (Toolbar) findViewById(R.id.audit_savings_toolbar_grouplist);
			TextView mTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);
			setSupportActionBar(mToolbar);
			getSupportActionBar().setDisplayShowHomeEnabled(true);
			getSupportActionBar().setTitle("");
			mTitle.setText(PrefUtils.getAnimatorName() + "    " + PrefUtils.getAgentUserName());
			mTitle.setTypeface(sTypeface);
			mTitle.setGravity(Gravity.CENTER);

			mHeader = (TextView) findViewById(R.id.audit_savings_header);
			mHeader.setText("AUDIT SAVINGS TRANSACTION");
			mHeader.setTypeface(sTypeface);

			mLeftHeaderTable = (TableLayout) findViewById(R.id.audit_LeftHeaderTable);
			mRightHeaderTable = (TableLayout) findViewById(R.id.audit_RightHeaderTable);
			mLeftContentTable = (TableLayout) findViewById(R.id.audit_LeftContentTable);
			mRightContentTable = (TableLayout) findViewById(R.id.audit_RightContentTable);

			mHSRightHeader = (CustomHorizontalScrollView) findViewById(R.id.audit_rightHeaderHScrollView);
			mHSRightContent = (CustomHorizontalScrollView) findViewById(R.id.audit_rightContentHScrollView);

			mHSRightHeader.setOnScrollChangedListener(new onScrollChangedListener() {

				public void onScrollChanged(int l, int t, int oldl, int oldt) {
					// TODO Auto-generated method stub

					mHSRightContent.scrollTo(l, 0);

				}
			});

			mHSRightContent.setOnScrollChangedListener(new onScrollChangedListener() {
				@Override
				public void onScrollChanged(int l, int t, int oldl, int oldt) {
					// TODO Auto-generated method stub
					mHSRightHeader.scrollTo(l, 0);
				}
			});

			TableRow leftHeaderRow = new TableRow(this);

			TableRow.LayoutParams lHeaderParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);

			TextView mMemberName_Header = new TextView(this);
			mMemberName_Header.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.memberName)));
			mMemberName_Header.setTypeface(sTypeface);
			mMemberName_Header.setTextColor(Color.WHITE);
			mMemberName_Header.setPadding(10, 5, 10, 5);
			mMemberName_Header.setLayoutParams(lHeaderParams);
			leftHeaderRow.addView(mMemberName_Header);

			mLeftHeaderTable.addView(leftHeaderRow);

			TableRow rightHeaderRow = new TableRow(this);

			TableRow.LayoutParams rHeaderParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);
			rHeaderParams.setMargins(10, 0, 10, 0);

			TextView mCredit_Header = new TextView(this);
			mCredit_Header.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.mCredit)));
			mCredit_Header.setTypeface(sTypeface);
			mCredit_Header.setSingleLine(true);
			mCredit_Header.setTextColor(Color.WHITE);
			mCredit_Header.setGravity(Gravity.CENTER);
			mCredit_Header.setLayoutParams(rHeaderParams);
			mCredit_Header.setPadding(10, 5, 10, 5);
			rightHeaderRow.addView(mCredit_Header);

			TextView mDebit_Header = new TextView(this);
			mDebit_Header.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.mDebit)));
			mDebit_Header.setSingleLine(true);
			mDebit_Header.setTypeface(sTypeface);
			mDebit_Header.setTextColor(Color.WHITE);
			mDebit_Header.setGravity(Gravity.CENTER);
			mDebit_Header.setLayoutParams(rHeaderParams);
			mDebit_Header.setPadding(10, 5, 10, 5);
			rightHeaderRow.addView(mDebit_Header);

			TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);
			contentParams.setMargins(10, 0, 10, 0);

			TextView mOutstanding_Header = new TextView(this);
			mOutstanding_Header
					.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.OutsatndingAmt)));
			mOutstanding_Header.setTypeface(sTypeface);
			mOutstanding_Header.setSingleLine(true);
			mOutstanding_Header.setTextColor(Color.WHITE);
			mOutstanding_Header.setGravity(Gravity.RIGHT);
			mOutstanding_Header.setLayoutParams(contentParams);
			mOutstanding_Header.setPadding(10, 5, 10, 5);
			rightHeaderRow.addView(mOutstanding_Header);

			mRightHeaderTable.addView(rightHeaderRow);

			try {
				mSize = 10;
				for (int j = 0; j < mSize; j++) {
					TableRow leftContentRow = new TableRow(this);

					TableRow.LayoutParams leftContentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT, 60,
							1f);
					leftContentParams.setMargins(5, 5, 5, 5);

					TextView memberName_Text = new TextView(this);
					memberName_Text.setText(GetSpanText.getSpanString(this,
							String.valueOf("Member Name "+j)));
					memberName_Text.setTypeface(sTypeface);
					memberName_Text.setTextColor(color.black);
					memberName_Text.setPadding(5, 0, 5, 5);
					memberName_Text.setLayoutParams(leftContentParams);
					memberName_Text.setGravity(Gravity.CENTER_VERTICAL);
					leftContentRow.addView(memberName_Text);

					mLeftContentTable.addView(leftContentRow);

					TableRow rightContentRow = new TableRow(this);

					TableRow.LayoutParams rightContentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
							LayoutParams.WRAP_CONTENT, 1f);// (150,60,1f);
					rightContentParams.setMargins(10, 5, 10, 5);

					mCredit_EditText = new EditText(this);
					mCredit_EditText.setId(j);
					sCredit_Fields.add(mCredit_EditText);
					mCredit_EditText.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_DECIMAL);
					mCredit_EditText.setPadding(5, 5, 5, 5);
					mCredit_EditText.setFilters(Get_EdiText_Filter.editText_filter());
					mCredit_EditText.setBackgroundResource(R.drawable.edittext_background);
					mCredit_EditText.setGravity(Gravity.RIGHT);
					mCredit_EditText.setLayoutParams(rightContentParams);
					mCredit_EditText.setTag("EditTextView of Amount");
					mCredit_EditText.setWidth(150);
					mCredit_EditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {

						@Override
						public void onFocusChange(View v, boolean hasFocus) {
							// TODO Auto-generated method stub

							if (hasFocus) {
								((EditText) v).setGravity(Gravity.LEFT);
							} else {
								((EditText) v).setGravity(Gravity.RIGHT);
							}
						}
					});

					rightContentRow.addView(mCredit_EditText);

					mDebit_EditText = new EditText(this);
					mDebit_EditText.setId(j);
					sDebit_Fields.add(mDebit_EditText);
					mDebit_EditText.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_DECIMAL);
					mDebit_EditText.setPadding(5, 5, 5, 5);
					mDebit_EditText.setFilters(Get_EdiText_Filter.editText_filter());
					mDebit_EditText.setGravity(Gravity.RIGHT);
					mDebit_EditText.setBackgroundResource(R.drawable.edittext_background);
					mDebit_EditText.setLayoutParams(rightContentParams);
					mDebit_EditText.setWidth(150);
					mDebit_EditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {

						@Override
						public void onFocusChange(View v, boolean hasFocus) {
							// TODO Auto-generated method stub
							if (hasFocus) {
								((EditText) v).setGravity(Gravity.LEFT);
							} else {
								((EditText) v).setGravity(Gravity.RIGHT);
							}

						}
					});
					rightContentRow.addView(mDebit_EditText);

					TableRow.LayoutParams contentRow_Params = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT, 60,
							1f);

					contentRow_Params.setMargins(10, 5, 10, 5);
					TextView outstanding = new TextView(this);
					outstanding.setText(GetSpanText.getSpanString(this, String.valueOf(1000)));
					outstanding.setTextColor(color.black);
					outstanding.setGravity(Gravity.RIGHT);
					outstanding.setLayoutParams(contentRow_Params);
					outstanding.setPadding(10, 0, 10, 5);
					rightContentRow.addView(outstanding);

					mRightContentTable.addView(rightContentRow);
				}

			
			} catch (ArrayIndexOutOfBoundsException e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			
			mSubmit_Raised_Button = (RaisedButton) findViewById(R.id.audit_savings_Submit_button);
			mSubmit_Raised_Button.setText(RegionalConversion.getRegionalConversion(AppStrings.submit));
			mSubmit_Raised_Button.setTypeface(LoginActivity.sTypeface);
			mSubmit_Raised_Button.setOnClickListener(this);
			
			resizeMemberNameWidth();
			resizeRightSideTable();
			resizeBodyTableRowHeight();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		switch (v.getId()) {
		case R.id.audit_savings_Submit_button:
			
			break;

		default:
			break;
		}
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_edit_ob_group, menu);
		MenuItem item = menu.getItem(0);
		item.setVisible(true);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.

		int id = item.getItemId();
		if (id == R.id.group_logout_edit) {

			startActivity(new Intent(GetExit.getExitIntent(getApplicationContext())));
			this.finish();

			return true;

		}

		return super.onOptionsItemSelected(item);
	}
	
	private void resizeRightSideTable() {
		// TODO Auto-generated method stub
		int rightHeaderCount = (((TableRow) mRightHeaderTable.getChildAt(0)).getChildCount());
		System.out.println("------------header count---------" + rightHeaderCount);
		for (int i = 0; i < rightHeaderCount; i++) {
			rightHeaderWidth[i] = viewWidth(((TableRow) mRightHeaderTable.getChildAt(0)).getChildAt(i));
			rightContentWidth[i] = viewWidth(((TableRow) mRightContentTable.getChildAt(0)).getChildAt(i));
		}
		for (int i = 0; i < rightHeaderCount; i++) {
			if (rightHeaderWidth[i] < rightContentWidth[i]) {
				((TableRow) mRightHeaderTable.getChildAt(0)).getChildAt(i)
						.getLayoutParams().width = rightContentWidth[i];
			} else {
				((TableRow) mRightContentTable.getChildAt(0)).getChildAt(i)
						.getLayoutParams().width = rightHeaderWidth[i];
			}
		}
	}

	private void resizeMemberNameWidth() {
		// TODO Auto-generated method stub
		int leftHeadertWidth = viewWidth(mLeftHeaderTable);
		int leftContentWidth = viewWidth(mLeftContentTable);

		if (leftHeadertWidth < leftContentWidth) {
			mLeftHeaderTable.getLayoutParams().width = leftContentWidth;
		} else {
			mLeftContentTable.getLayoutParams().width = leftHeadertWidth;
		}
	}

	private void resizeBodyTableRowHeight() {

		int leftContentTable_ChildCount = mLeftContentTable.getChildCount();

		for (int x = 0; x < leftContentTable_ChildCount; x++) {

			TableRow leftContentTableRow = (TableRow) mLeftContentTable.getChildAt(x);
			TableRow rightContentTableRow = (TableRow) mRightContentTable.getChildAt(x);

			int rowLeftHeight = viewHeight(leftContentTableRow);
			int rowRightHeight = viewHeight(rightContentTableRow);

			TableRow tableRow = rowLeftHeight < rowRightHeight ? leftContentTableRow : rightContentTableRow;
			int finalHeight = rowLeftHeight > rowRightHeight ? rowLeftHeight : rowRightHeight;

			this.matchLayoutHeight(tableRow, finalHeight);
		}

	}

	private void matchLayoutHeight(TableRow tableRow, int height) {

		int tableRowChildCount = tableRow.getChildCount();

		// if a TableRow has only 1 child
		if (tableRow.getChildCount() == 1) {

			View view = tableRow.getChildAt(0);
			TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();
			params.height = height - (params.bottomMargin + params.topMargin);

			return;
		}

		// if a TableRow has more than 1 child
		for (int x = 0; x < tableRowChildCount; x++) {

			View view = tableRow.getChildAt(x);

			TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();

			if (!isTheHeighestLayout(tableRow, x)) {
				params.height = height - (params.bottomMargin + params.topMargin);
				return;
			}
		}

	}

	// check if the view has the highest height in a TableRow
	private boolean isTheHeighestLayout(TableRow tableRow, int layoutPosition) {

		int tableRowChildCount = tableRow.getChildCount();
		int heighestViewPosition = -1;
		int viewHeight = 0;

		for (int x = 0; x < tableRowChildCount; x++) {
			View view = tableRow.getChildAt(x);
			int height = this.viewHeight(view);

			if (viewHeight < height) {
				heighestViewPosition = x;
				viewHeight = height;
			}
		}

		return heighestViewPosition == layoutPosition;
	}

	// read a view's height
	private int viewHeight(View view) {
		view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
		return view.getMeasuredHeight();
	}

	// read a view's width
	private int viewWidth(View view) {
		view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
		return view.getMeasuredWidth();
	}


}
