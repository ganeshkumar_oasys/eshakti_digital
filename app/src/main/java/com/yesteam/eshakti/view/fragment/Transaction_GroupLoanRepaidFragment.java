package com.yesteam.eshakti.view.fragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.squareup.otto.Subscribe;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.adapter.CustomItemAdapter;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.TransactionOffConstants;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;
import com.yesteam.eshakti.model.RowItem;
import com.yesteam.eshakti.sqlite.database.GroupProvider;
import com.yesteam.eshakti.sqlite.database.TransactionProvider;
import com.yesteam.eshakti.sqlite.database.response.DefaultOffline_MinutesResponse;
import com.yesteam.eshakti.sqlite.database.response.GetGroupLoanOutstandingResponse;
import com.yesteam.eshakti.sqlite.database.response.GetLoanAccountNoResponse;
import com.yesteam.eshakti.sqlite.database.response.GetSingleTransResponse;
import com.yesteam.eshakti.sqlite.database.response.GroupLoanOSResponse;
import com.yesteam.eshakti.sqlite.database.response.LoanAccountNoUpdateResponse;
import com.yesteam.eshakti.sqlite.database.response.TransactionResponse;
import com.yesteam.eshakti.sqlite.database.response.TransactionUpdateResponse;
import com.yesteam.eshakti.sqlite.db.model.GetGroupMemberDetails;
import com.yesteam.eshakti.sqlite.db.model.GetTransactionSingle;
import com.yesteam.eshakti.sqlite.db.model.GetTransactionSinglevalues;
import com.yesteam.eshakti.sqlite.db.model.GroupLoanOSUpdate;
import com.yesteam.eshakti.sqlite.db.model.GroupMasterSingle;
import com.yesteam.eshakti.sqlite.db.model.GroupResponseUpdate;
import com.yesteam.eshakti.sqlite.db.model.GroupTempDBLastTransDate;
import com.yesteam.eshakti.sqlite.db.model.LoanAccountNoDetailsUpdate;
import com.yesteam.eshakti.sqlite.db.model.Transaction;
import com.yesteam.eshakti.sqlite.db.model.TransactionUpdate;
import com.yesteam.eshakti.sqlite.db.model.Transaction_UniqueIdSingle;
import com.yesteam.eshakti.sqlite.db.transactions.TransactionManager.DataType;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.utils.GetOfflineTransactionValues;
import com.yesteam.eshakti.utils.GetResponseInfo;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.utils.Get_EdiText_Filter;
import com.yesteam.eshakti.utils.Get_Offline_MobileDate;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.utils.Put_DB_GroupNameDetail_Response;
import com.yesteam.eshakti.utils.Put_DB_GroupResponse;
import com.yesteam.eshakti.utils.Reset;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.utils.UpdateCalendarMinMaxDateUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.views.MaterialSpinner;
import com.yesteam.eshakti.views.RaisedButton;
import com.yesteam.eshakti.webservices.Get_GroupLoanRepaidTask;
import com.yesteam.eshakti.webservices.Get_Group_Minutes_NewTask;
import com.yesteam.eshakti.webservices.Get_LastTransactionID;
import com.yesteam.eshakti.webservices.Get_LoanOutstandingTask;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Transaction_GroupLoanRepaidFragment extends Fragment implements OnClickListener, TaskListener {

	public static String TAG = Transaction_GroupLoanRepaidFragment.class.getSimpleName();
	private TextView mGroupName, mCashInHand, mCashAtBank, mHeadertext;
	private Button mRaised_Submit_Button, mNextButton;
	private Button mEdit_RaisedButton, mOk_RaisedButton;
	public static EditText mEditAmount;

	public static String sSend_To_Server_GLRepaid;
	public static String[] sGroupLoanName;
	public static List<EditText> sEditTextFields = new ArrayList<EditText>();
	public static String[] sLoanAmount;
	public static Boolean isGroupLoanRepaid = false;
	public static int sGroupLoanTotal;
	int size;
	String toBeEditArr[];

	private Dialog mProgressDilaog;
	Dialog confirmationDialog;

	String mLastTrDate = null, mLastTr_ID = null;
	String mOfflineGrouploanId;
	boolean isGetTrid = false;
	boolean isServiceCall = false;
	ArrayList<String> mLoanBankName = new ArrayList<String>();
	ArrayList<String> mLoanName = new ArrayList<String>();
	ArrayList<String> mLoanId = new ArrayList<String>();
	ArrayList<String> mLoanOutstanding = new ArrayList<String>();
	ArrayList<String> mLoanAccNo = new ArrayList<String>();

	String ToLoanAccNo = "", outstandingAmt = "";
	int mCount = 0;

	LinearLayout radioGroupLinearLayout, spinnerLayout;
	RadioButton mCashRadio, mBankRadio;
	MaterialSpinner materialSpinner_Bank;
	CustomItemAdapter bankNameAdapter;
	public static String selectedItemBank, selectedType;
	ArrayList<String> mBanknames_Array = new ArrayList<String>();
	ArrayList<String> mBanknamesId_Array = new ArrayList<String>();
	ArrayList<String> mEngSendtoServerBank_Array = new ArrayList<String>();
	ArrayList<String> mEngSendtoServerBankId_Array = new ArrayList<String>();
	private List<RowItem> stateNameItems;
	public static String mBankNameValue = null;

	TextView mBankAccountNo;
	TableLayout bankChargeTable;
	EditText editText_mBankCharges;
	public static String mRepaymentAmount = null, mBankChargesAmount = null;
	public static String mCashatBank_individual;
	String mCashatBank = null;

	String mSqliteDBStoredValues_GrouploanRepayValues = null;
	Date date_dashboard, date_loanDisb;
	boolean isMeetingValues = false;
	String mGroupOS_Offlineresponse = null;
	private TextView mLoanAccountNo;

	public Transaction_GroupLoanRepaidFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_GROUPLOAN_REPAID;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		SBus.INST.register(this);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		sGroupLoanName = new String[] { RegionalConversion.getRegionalConversion(AppStrings.interest), // (AppStrings.bankInterest),
				RegionalConversion.getRegionalConversion(AppStrings.mCharges), // (AppStrings.bankExpenses),
				RegionalConversion.getRegionalConversion(AppStrings.mRepaymentWithInterest), // (AppStrings.bankrepayment),
				RegionalConversion.getRegionalConversion(AppStrings.interestSubvention) };

		sSend_To_Server_GLRepaid = Reset.reset(sSend_To_Server_GLRepaid);
		sGroupLoanTotal = 0;
		Transaction_GroupLoanRepaidFragment.sEditTextFields.clear();
		selectedType = "Cash";
		selectedItemBank = "";
	}

	@SuppressWarnings("deprecation")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_deposit_entry, container, false);

		MainFragment_Dashboard.isBackpressed = false;
		mCount = 0;

		mSqliteDBStoredValues_GrouploanRepayValues = null;

		try {
			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashInHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashInHand.setTypeface(LoginActivity.sTypeface);

			mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashAtBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashAtBank.setTypeface(LoginActivity.sTypeface);

			mHeadertext = (TextView) rootView.findViewById(R.id.fragment_deposit_entry_headertext);
			mHeadertext.setText(
					RegionalConversion.getRegionalConversion(Transaction_GroupLoanRepaidMenuFragment.mloan_Name) + " - "
							+ EShaktiApplication.getGroupLoanRepaymentLoanBankName());
			mHeadertext.setTypeface(LoginActivity.sTypeface);

			mLoanAccountNo = (TextView) rootView.findViewById(R.id.LoanAccNo_GroupLoanRepayment);
			mLoanAccountNo.setVisibility(View.VISIBLE);
			mLoanAccountNo.setText(RegionalConversion.getRegionalConversion(AppStrings.Loan_Account_No) + " : "
					+ EShaktiApplication.getGroupLoanRepaymentAccNo());
			mLoanAccountNo.setTypeface(LoginActivity.sTypeface);
			
			mRaised_Submit_Button = (Button) rootView.findViewById(R.id.fragment_deposit_entry_Submitbutton);
			mRaised_Submit_Button.setText(RegionalConversion.getRegionalConversion(AppStrings.submit));
			mRaised_Submit_Button.setTypeface(LoginActivity.sTypeface);
			mRaised_Submit_Button.setOnClickListener(this);

			mNextButton = (RaisedButton) rootView.findViewById(R.id.fragment_deposit_entry_Nextbutton);
			mNextButton.setText(RegionalConversion.getRegionalConversion(AppStrings.mNext));
			mNextButton.setTypeface(LoginActivity.sTypeface);
			mNextButton.setOnClickListener(this);

			if (EShaktiApplication.isDefault()) {
				mNextButton.setVisibility(View.VISIBLE);
			} else {
				mNextButton.setVisibility(View.INVISIBLE);
			}

			size = sGroupLoanName.length;
			System.out.println("Size :" + size);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		try {

			radioGroupLinearLayout = (LinearLayout) rootView.findViewById(R.id.typeOfTransactionLayout);
			radioGroupLinearLayout.setVisibility(View.VISIBLE);

			mCashRadio = (RadioButton) rootView.findViewById(R.id.radio_group_cash);
			mBankRadio = (RadioButton) rootView.findViewById(R.id.radio_group_bank);
			mCashRadio.setText(AppStrings.mLoanaccCash);
			mBankRadio.setText(AppStrings.mLoanaccBank);
			mCashRadio.setTypeface(LoginActivity.sTypeface);
			mBankRadio.setTypeface(LoginActivity.sTypeface);

			spinnerLayout = (LinearLayout) rootView.findViewById(R.id.spinner_banklist_Layout);
			materialSpinner_Bank = (MaterialSpinner) rootView.findViewById(R.id.grouploan_bankName_spinner);

			RadioGroup radioGroup = (RadioGroup) rootView.findViewById(R.id.radio_grouploantrans_type);
			radioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(RadioGroup group, int checkedId) {
					// checkedId is the RadioButton selected
					switch (checkedId) {
					case R.id.radio_group_cash:
						spinnerLayout.setVisibility(View.GONE);
						selectedType = "Cash";
						mBankAccountNo.setVisibility(View.GONE);
						bankChargeTable.setVisibility(View.GONE);
						break;

					case R.id.radio_group_bank:
						selectedType = "Bank";
						spinnerLayout.setVisibility(View.VISIBLE);
						mBankAccountNo.setVisibility(View.VISIBLE);
						bankChargeTable.setVisibility(View.VISIBLE);
						break;
					}
				}
			});

			for (int i = 0; i < SelectedGroupsTask.sBankNames.size(); i++) {
				mBanknames_Array.add(SelectedGroupsTask.sBankNames.elementAt(i).toString());
				mBanknamesId_Array.add(String.valueOf(i));
			}

			for (int i = 0; i < SelectedGroupsTask.sEngBankNames.size(); i++) {
				mEngSendtoServerBank_Array.add(SelectedGroupsTask.sEngBankNames.elementAt(i).toString());
				mEngSendtoServerBankId_Array.add(String.valueOf(i));
			}

			materialSpinner_Bank.setBaseColor(color.grey_400);

			materialSpinner_Bank.setFloatingLabelText(AppStrings.mLoanaccSpinnerFloating);

			materialSpinner_Bank.setPaddingSafe(10, 0, 10, 0);

			mBankAccountNo = (TextView) rootView.findViewById(R.id.bankAccNo);

			final String[] bankNames = new String[SelectedGroupsTask.sBankNames.size() + 1];

			final String[] bankNames_BankID = new String[SelectedGroupsTask.sEngBankNames.size() + 1];

			bankNames[0] = String.valueOf("Select Bank Name");
			for (int i = 0; i < SelectedGroupsTask.sBankNames.size(); i++) {
				bankNames[i + 1] = SelectedGroupsTask.sBankNames.elementAt(i).toString();
			}

			bankNames_BankID[0] = String.valueOf("Select Bank Name");
			for (int i = 0; i < SelectedGroupsTask.sEngBankNames.size(); i++) {
				bankNames_BankID[i + 1] = SelectedGroupsTask.sEngBankNames.elementAt(i).toString();
			}

			int bank_size = bankNames.length;

			stateNameItems = new ArrayList<RowItem>();
			for (int i = 0; i < bank_size; i++) {
				RowItem rowItem = new RowItem(bankNames[i]);// SelectedGroupsTask.sBankNames.elementAt(i).toString());
				stateNameItems.add(rowItem);
			}
			bankNameAdapter = new CustomItemAdapter(getActivity(), stateNameItems);
			materialSpinner_Bank.setAdapter(bankNameAdapter);

			materialSpinner_Bank.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
					// TODO Auto-generated method stub

					if (position == 0) {
						selectedItemBank = bankNames_BankID[0];
						mBankNameValue = "0";

					} else {
						selectedItemBank = bankNames_BankID[position];
						System.out.println("SELECTED BANK NAME : " + selectedItemBank);
						mBankNameValue = selectedItemBank;
						String mBankname = null;
						for (int i = 0; i < mBanknames_Array.size(); i++) {
							if (selectedItemBank.equals(mEngSendtoServerBank_Array.get(i))) {
								mBankname = mEngSendtoServerBank_Array.get(i);
							}
						}

						mBankNameValue = mBankname;

					}
					Log.e("Selected Bank Name value", mBankNameValue);

				}

				@Override
				public void onNothingSelected(AdapterView<?> parent) {
					// TODO Auto-generated method stub

				}
			});

			TableLayout contentTable = (TableLayout) rootView.findViewById(R.id.fragmentDeposit_contentTable);

			TableRow outerRow = new TableRow(getActivity());

			TableRow.LayoutParams header_ContentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);
			header_ContentParams.setMargins(10, 5, 10, 5);// (0, 5, 5, 5);

			TextView loanName = new TextView(getActivity());
			loanName.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.outstanding)));
			loanName.setTypeface(LoginActivity.sTypeface);
			loanName.setPadding(15, 5, 5, 5);
			loanName.setTextColor(color.black);
			loanName.setLayoutParams(header_ContentParams);
			outerRow.addView(loanName);

			TableRow.LayoutParams amountParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);
			amountParams.setMargins(5, 5, 20, 5);

			TextView loanOutStanding_Amount = new TextView(getActivity());
			loanOutStanding_Amount.setText(GetSpanText.getSpanString(getActivity(),
					String.valueOf(Get_LoanOutstandingTask.sGet_Loan_outstanding_ServiceResponse)));
			loanOutStanding_Amount.setTextColor(color.black);
			loanOutStanding_Amount.setPadding(5, 5, 5, 5);
			loanOutStanding_Amount.setGravity(Gravity.RIGHT);
			loanOutStanding_Amount.setLayoutParams(amountParams);
			outerRow.addView(loanOutStanding_Amount);

			contentTable.addView(outerRow, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));

			TableRow addRow = new TableRow(getActivity());
			addRow.setBackgroundResource(R.drawable.heading_background);

			TableRow.LayoutParams addParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);
			addParams.setMargins(10, 5, 10, 5);// (0, 5, 5, 5);

			TextView addText = new TextView(getActivity());
			addText.setText(AppStrings.mAdd);
			addText.setTypeface(LoginActivity.sTypeface, Typeface.BOLD);
			addText.setPadding(15, 5, 5, 5);
			addText.setTextColor(Color.WHITE);
			addText.setLayoutParams(addParams);
			addRow.addView(addText);

			contentTable.addView(addRow, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
			for (int i = 0; i < size; i++) {
				if (i == 2) {

					TableRow lessRow = new TableRow(getActivity());
					lessRow.setBackgroundResource(R.drawable.heading_background);

					TableRow.LayoutParams lessParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
							LayoutParams.WRAP_CONTENT, 1f);
					lessParams.setMargins(10, 5, 10, 5);// (0, 5, 5, 5);

					TextView lessText = new TextView(getActivity());
					lessText.setText(AppStrings.mLess);
					lessText.setTypeface(LoginActivity.sTypeface, Typeface.BOLD);
					lessText.setPadding(15, 5, 5, 5);
					lessText.setTextColor(Color.WHITE);
					lessText.setLayoutParams(lessParams);
					lessRow.addView(lessText);

					contentTable.addView(lessRow,
							new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
				}

				TableRow innerRow = new TableRow(getActivity());

				TextView memberName = new TextView(getActivity());
				memberName.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sGroupLoanName[i])));
				memberName.setTypeface(LoginActivity.sTypeface);
				memberName.setPadding(15, 0, 5, 5);
				memberName.setTextColor(color.black);
				memberName.setLayoutParams(header_ContentParams);
				innerRow.addView(memberName);

				mEditAmount = new EditText(getActivity());
				sEditTextFields.add(mEditAmount);
				mEditAmount.setId(i);
				mEditAmount.setInputType(InputType.TYPE_CLASS_NUMBER);
				mEditAmount.setPadding(5, 5, 5, 5);
				mEditAmount.setFilters(Get_EdiText_Filter.editText_filter());
				mEditAmount.setLayoutParams(amountParams);
				mEditAmount.setBackgroundResource(R.drawable.edittext_background);
				mEditAmount.setOnFocusChangeListener(new View.OnFocusChangeListener() {

					@Override
					public void onFocusChange(View v, boolean hasFocus) {
						// TODO Auto-generated method stub
						if (hasFocus) {
							((EditText) v).setGravity(Gravity.LEFT);
						} else {
							((EditText) v).setGravity(Gravity.RIGHT);
						}

					}
				});
				innerRow.addView(mEditAmount);

				contentTable.addView(innerRow,
						new TableLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));

			}

			bankChargeTable = (TableLayout) rootView.findViewById(R.id.fragmentDeposit_bankChangeTable);

			TableRow bankChargeRow = new TableRow(getActivity());

			TextView bankCharge = new TextView(getActivity());
			bankCharge.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mBankCharges)));
			bankCharge.setTypeface(LoginActivity.sTypeface);
			bankCharge.setPadding(15, 0, 5, 5);
			bankCharge.setTextColor(color.black);
			bankCharge.setLayoutParams(header_ContentParams);
			bankChargeRow.addView(bankCharge);

			editText_mBankCharges = new EditText(getActivity());

			editText_mBankCharges.setInputType(InputType.TYPE_CLASS_NUMBER);
			editText_mBankCharges.setPadding(5, 5, 5, 5);
			editText_mBankCharges.setFilters(Get_EdiText_Filter.editText_filter());
			editText_mBankCharges.setLayoutParams(amountParams);
			editText_mBankCharges.setWidth(150);

			editText_mBankCharges.setBackgroundResource(R.drawable.edittext_background);
			editText_mBankCharges.setOnFocusChangeListener(new View.OnFocusChangeListener() {

				@Override
				public void onFocusChange(View v, boolean hasFocus) {
					// TODO Auto-generated method stub
					if (hasFocus) {
						((EditText) v).setGravity(Gravity.LEFT);
					} else {
						((EditText) v).setGravity(Gravity.RIGHT);
					}

				}
			});
			bankChargeRow.addView(editText_mBankCharges);

			bankChargeTable.addView(bankChargeRow,
					new TableLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return rootView;
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		try {

			switch (v.getId()) {

			case R.id.fragment_deposit_entry_Submitbutton:
				isGroupLoanRepaid = true;
				sLoanAmount = new String[sEditTextFields.size()];
				sSend_To_Server_GLRepaid = Reset.reset(sSend_To_Server_GLRepaid);
				sGroupLoanTotal = Integer.valueOf("0");
				mBankChargesAmount = editText_mBankCharges.getText().toString();

				if (mBankChargesAmount.equals("") || mBankChargesAmount == null) {
					mBankChargesAmount = "0";
				}
				for (int i = 0; i < sLoanAmount.length; i++) {

					sLoanAmount[i] = String.valueOf(sEditTextFields.get(i).getText());

					if (sLoanAmount[i] == null || sLoanAmount[i].equals("")) {
						sLoanAmount[i] = "0";
					}

					if (sLoanAmount[i].matches("\\d*\\.?\\d+")) { // match a
																	// decimal
																	// number

						int amount = (int) Math.round(Double.parseDouble(sLoanAmount[i]));
						sLoanAmount[i] = String.valueOf(amount);
					}

					if (mCashRadio.isChecked()) {

						sGroupLoanTotal = sGroupLoanTotal + Integer.parseInt(sLoanAmount[i]);
					} else if (mBankRadio.isChecked()) {
						sGroupLoanTotal = sGroupLoanTotal + Integer.parseInt(sLoanAmount[i])
								+ Integer.parseInt(mBankChargesAmount);
					}

					sSend_To_Server_GLRepaid = sSend_To_Server_GLRepaid + sLoanAmount[i] + "~";

				}

				/*
				 * if (mBankRadio.isChecked()) { sSend_To_Server_GLRepaid =
				 * sSend_To_Server_GLRepaid + mBankChargesAmount + "~"; }
				 */

				System.out.println(" Total Value:>>>>" + sGroupLoanTotal);

				System.out.println("Send To Confirmation Value:>>>" + sSend_To_Server_GLRepaid);

				if (Get_LoanOutstandingTask.sGet_Loan_outstanding_ServiceResponse.contains(".")) {

					double mValues = Double.parseDouble(Get_LoanOutstandingTask.sGet_Loan_outstanding_ServiceResponse);

					Get_LoanOutstandingTask.sGet_Loan_outstanding_ServiceResponse = String.valueOf(Math.round(mValues));

				}

				if (!EShaktiApplication.isDefault()) {

					String dashBoardDate = DatePickerDialog.sDashboardDate;
					String loanDisbArr[] = EShaktiApplication.getLoanDisbursementDate().split("/");
					String loanDisbDate = loanDisbArr[1] + "-" + loanDisbArr[0] + "-" + loanDisbArr[2];

					SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

					try {
						date_dashboard = sdf.parse(dashBoardDate);
						date_loanDisb = sdf.parse(loanDisbDate);
					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					if (date_dashboard.compareTo(date_loanDisb) >= 0) {
						onCallConfirmationDialog();
					} else {
						TastyToast.makeText(getActivity(), AppStrings.mCheckDisbursementDate, TastyToast.LENGTH_SHORT,
								TastyToast.WARNING);
					}

				} else {
					onCallConfirmationDialog();
				}

				break;
			case R.id.fragment_Edit_button:
				sSend_To_Server_GLRepaid = Reset.reset(sSend_To_Server_GLRepaid);
				sGroupLoanTotal = Integer.valueOf("0");
				mRaised_Submit_Button.setClickable(true);
				confirmationDialog.dismiss();

				isServiceCall = false;
				break;
			case R.id.fragment_Ok_button:

				
				if (EShaktiApplication.isDefault()) {
					
					int totalInterest = 0, totalCharges = 0, totalRepayment = 0, totalInterestSubRecevied = 0, totalBankcharges = 0;
					
					totalInterest = EShaktiApplication.getGroupLoanTotalInterest() + Integer.parseInt(sLoanAmount[0]);
					totalCharges = EShaktiApplication.getGroupLoanTotalCharges() + Integer.parseInt(sLoanAmount[1]);
					totalRepayment = EShaktiApplication.getGroupLoanTotalRepayment() + Integer.parseInt(sLoanAmount[2]);
					totalInterestSubRecevied = EShaktiApplication.getGroupLoanTotalInterestSubventionRecevied() + Integer.parseInt(sLoanAmount[3]);
					
					EShaktiApplication.setGroupLoanTotalInterest(totalInterest);
					EShaktiApplication.setGroupLoanTotalCharges(totalCharges);
					EShaktiApplication.setGroupLoanTotalRepayment(totalRepayment);
					EShaktiApplication.setGroupLoanTotalInterestSubventionRecevied(totalInterestSubRecevied);
					if (mBankRadio.isChecked()) {
						
						totalBankcharges = EShaktiApplication.getGroupLoanTotalBankcharges() + Integer.parseInt(mBankChargesAmount);
						EShaktiApplication.setGroupLoanTotalBankcharges(totalBankcharges);
					}
					
				}
				
				if (ConnectionUtils.isNetworkAvailable(getActivity())) {

					try {

						if (EShaktiApplication.getLoginFlag().equals(Constants.ONLINEFLAG)) {
							if (!isServiceCall) {
								isServiceCall = true;

								new Get_GroupLoanRepaidTask(Transaction_GroupLoanRepaidFragment.this).execute();
							}
						} else {
							TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
									TastyToast.ERROR);
						}

					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
				} else {
					// Do offline Stuffs
					if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
						EShaktiApplication.getInstance().getTransactionManager().startTransaction(
								DataType.GET_TRANS_SINGLE, new GetTransactionSingle(PrefUtils.getOfflineUniqueID()));
					} else {
						TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
								TastyToast.ERROR);
					}
				}

				break;

			case R.id.fragment_deposit_entry_Nextbutton:

				System.out.println("----------------- Next Button Clickkkkkkkkk----------");
				int loan_size = SelectedGroupsTask.loan_Id.size();
				Log.e("Loan Size !!!!!!!!", loan_size + "");
				Log.e("Before increment groupLoanCount   =   ",
						Transaction_InternalLoan_DisbursementFragment.groupLoanCount + "");
				int i = Transaction_InternalLoan_DisbursementFragment.groupLoanCount++;
				Log.e("After increment groupLoanCount   =   ", i + "");

				Log.e("PublicVariables.groupLoanCount   =   ",
						Transaction_InternalLoan_DisbursementFragment.groupLoanCount + "");
				// for (int j = 0; j < loan_size; j++) {
				if (Transaction_InternalLoan_DisbursementFragment.groupLoanCount <= loan_size) {
					if (!SelectedGroupsTask.loan_Id.elementAt(i).equals("0")) {

						try {
							Transaction_GroupLoanRepaidMenuFragment.mloan_Id = SelectedGroupsTask.loan_Id.elementAt(i)
									.toString();
							System.out.println("Loan Id" + Transaction_GroupLoanRepaidMenuFragment.mloan_Id);

							Transaction_GroupLoanRepaidMenuFragment.mloan_Name = SelectedGroupsTask.loan_Name
									.elementAt(i).toString();
							System.out.println("Loan Name:" + Transaction_GroupLoanRepaidMenuFragment.mloan_Name);

							String temp_loanName = SelectedGroupsTask.loan_Name.elementAt(i).toString();

							Transaction_GroupLoanRepaidMenuFragment.mSendTo_ServerLoan_Id = SelectedGroupsTask.loan_Id
									.elementAt(i).toString();

							EShaktiApplication
									.setLoanId(String.valueOf(Transaction_GroupLoanRepaidMenuFragment.mloan_Id));

							EShaktiApplication.setGroupLoanRepaymentLoanBankName(SelectedGroupsTask.loanAcc_bankName.elementAt(i));
							EShaktiApplication.setGroupLoanRepaymentAccNo(SelectedGroupsTask.loanAcc_loanAccNo.elementAt(i));

							if (Transaction_GroupLoanRepaidMenuFragment.mloan_Name.equals(temp_loanName)) {

								if (ConnectionUtils.isNetworkAvailable(getActivity())) {

									EShaktiApplication.getInstance().getTransactionManager().startTransaction(
											DataType.GET_GROUPLOAN_OUTSTANDING,
											new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));

								} else {
									// DO OFFLINE STUFFS

									if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {

										EShaktiApplication.getInstance().getTransactionManager().startTransaction(
												DataType.GET_GROUPLOAN_OUTSTANDING,
												new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));
									} else {
										TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF,
												TastyToast.LENGTH_SHORT, TastyToast.ERROR);
									}

								}

							}

						} catch (Exception e) {
							e.printStackTrace();
						}
					} else {

						if (ConnectionUtils.isNetworkAvailable(getActivity())) {
							System.out.println("----------- isMeetingValues true----");
							isMeetingValues = true;
							new Get_Group_Minutes_NewTask(this).execute();

						} else {
							if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
								EShaktiApplication.getInstance().getTransactionManager().startTransaction(
										DataType.DEFAULT_MINUTESOFMEETINGS,
										new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));
							} else {

								TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF,
										TastyToast.LENGTH_SHORT, TastyToast.ERROR);
							}
						}

					}
				}
				// }

				break;

			default:
				break;
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	private void onCallConfirmationDialog() {
		// TODO Auto-generated method stub
		int bank_repayment = Integer.parseInt(sLoanAmount[2].toString());

		int newGL_Outs = Integer.parseInt(Get_LoanOutstandingTask.sGet_Loan_outstanding_ServiceResponse.toString())
				- Integer.parseInt(sLoanAmount[3]);
		System.out.println("newGL_OUTS : ************** " + newGL_Outs);

		System.out.println("Bank_Repayment Amount:" + bank_repayment);

		if (selectedType.equals("Cash")) {

			if (sGroupLoanTotal != 0 && bank_repayment <= Integer.parseInt(SelectedGroupsTask.sCashinHand.trim())
					&& bank_repayment <= newGL_Outs) {

				showConfirmationDialog();

			} else if ((bank_repayment > newGL_Outs) // Integer.parseInt(Get_LoanOutstandingTask.sGet_Loan_outstanding_ServiceResponse.toString()
					|| bank_repayment > Integer.parseInt(SelectedGroupsTask.sCashinHand.trim())) {
				if (bank_repayment > newGL_Outs) {// Integer.parseInt(Get_LoanOutstandingTask.sGet_Loan_outstanding_ServiceResponse.toString())
					sSend_To_Server_GLRepaid = Reset.reset(sSend_To_Server_GLRepaid);
					sGroupLoanTotal = Integer.valueOf("0");
					TastyToast.makeText(getActivity(), AppStrings.groupRepaidAlert, TastyToast.LENGTH_SHORT,
							TastyToast.WARNING);
				} else {
					sSend_To_Server_GLRepaid = Reset.reset(sSend_To_Server_GLRepaid);
					sGroupLoanTotal = Integer.valueOf("0");
					TastyToast.makeText(getActivity(), AppStrings.cashinHandAlert, TastyToast.LENGTH_SHORT,
							TastyToast.WARNING);
				}
			} else {
				sSend_To_Server_GLRepaid = Reset.reset(sSend_To_Server_GLRepaid);
				sGroupLoanTotal = Integer.valueOf("0");
				TastyToast.makeText(getActivity(), AppStrings.nullAlert, TastyToast.LENGTH_SHORT, TastyToast.WARNING);
			}
		} else {

			if (!selectedItemBank.equals("Select Bank Name")) {

				for (int i = 0; i < SelectedGroupsTask.sBankNames.size(); i++) {

					if (selectedItemBank.equals(SelectedGroupsTask.sEngBankNames.elementAt(i))) {

						mCashatBank = SelectedGroupsTask.sBankAmt.elementAt(i);
					}
				}

				System.out.println("Selected Item Bank  =  " + selectedItemBank + "");
				System.out.println("mCashAtBank   =   " + mCashatBank + "");
				if (sGroupLoanTotal != 0 && bank_repayment <= Integer.parseInt(mCashatBank.trim())
						&& bank_repayment <= newGL_Outs) {

					if (!mBankNameValue.equals("0") && mBankNameValue != null) {
						showConfirmationDialog();
					} else {
						sSend_To_Server_GLRepaid = Reset.reset(sSend_To_Server_GLRepaid);
						sGroupLoanTotal = Integer.valueOf("0");
						TastyToast.makeText(getActivity(), AppStrings.mLoanaccBankNullToast, TastyToast.LENGTH_SHORT,
								TastyToast.WARNING);
					}

				} else if ((bank_repayment > newGL_Outs) // Integer.parseInt(Get_LoanOutstandingTask.sGet_Loan_outstanding_ServiceResponse.toString()
						|| bank_repayment > Integer.parseInt(mCashatBank.trim())) {
					if (bank_repayment > newGL_Outs) {// Integer.parseInt(Get_LoanOutstandingTask.sGet_Loan_outstanding_ServiceResponse.toString())
						sSend_To_Server_GLRepaid = Reset.reset(sSend_To_Server_GLRepaid);
						sGroupLoanTotal = Integer.valueOf("0");
						TastyToast.makeText(getActivity(), AppStrings.groupRepaidAlert, TastyToast.LENGTH_SHORT,
								TastyToast.WARNING);
					} else {
						sSend_To_Server_GLRepaid = Reset.reset(sSend_To_Server_GLRepaid);
						sGroupLoanTotal = Integer.valueOf("0");
						TastyToast.makeText(getActivity(), AppStrings.cashatBankAlert, TastyToast.LENGTH_SHORT,
								TastyToast.WARNING);
					}
				} else {
					sSend_To_Server_GLRepaid = Reset.reset(sSend_To_Server_GLRepaid);
					sGroupLoanTotal = Integer.valueOf("0");
					TastyToast.makeText(getActivity(), AppStrings.nullAlert, TastyToast.LENGTH_SHORT,
							TastyToast.WARNING);
				}
			} else {
				sSend_To_Server_GLRepaid = Reset.reset(sSend_To_Server_GLRepaid);
				sGroupLoanTotal = Integer.valueOf("0");
				TastyToast.makeText(getActivity(), AppStrings.mLoanaccBankNullToast, TastyToast.LENGTH_SHORT,
						TastyToast.WARNING);
			}

		}

	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub
		mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
		mProgressDilaog.show();
	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub

		if (mProgressDilaog != null) {
			mProgressDilaog.dismiss();
			if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {
				getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub

						if (!result.equals("FAIL")) {

							TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg,
									TastyToast.LENGTH_SHORT, TastyToast.ERROR);
							Constants.NETWORKCOMMONFLAG = "SUCCESS";
							if (isMeetingValues) {
								isMeetingValues = false;
								Transaction_InternalLoan_DisbursementFragment.groupLoanCount = --Transaction_InternalLoan_DisbursementFragment.groupLoanCount;
							}
							isServiceCall = false;
						}

					}
				});

			} else {
				try {
					if (isGetTrid) {
						isGetTrid = false;
						callOfflineDataUpdate();
						Log.e("Values---->>", "!!!!!");
					} else if (isMeetingValues) {
						isMeetingValues = false;
						if (EShaktiApplication.isDefault()) {
							PrefUtils.setStepWiseScreenCount("6");
						}
						Meeting_MinutesOfMeetingFragment meetingFragment = new Meeting_MinutesOfMeetingFragment();
						setFragment(meetingFragment);
					} else {
						if (GetResponseInfo.sResponseUpdate[0].equals("Yes")) {

							TastyToast.makeText(getActivity(), AppStrings.transactionCompleted, TastyToast.LENGTH_SHORT,
									TastyToast.SUCCESS);

							new Get_LastTransactionID(Transaction_GroupLoanRepaidFragment.this).execute();
							isGetTrid = true;
						} else {

							TastyToast.makeText(getActivity(), AppStrings.transactionFailAlert, TastyToast.LENGTH_SHORT,
									TastyToast.ERROR);

							MainFragment_Dashboard fragment = new MainFragment_Dashboard();
							setFragment(fragment);
						}
						confirmationDialog.dismiss();
					}

				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}

		}
	}

	private void callOfflineDataUpdate() {
		Log.e("222222222222", "333333333333333");
		System.out.println("------callOfflineDataUpdate-------");
		Log.e("Group Master Update Sucessfully", "Updated !@#$#$#$");
		StringBuilder builder = new StringBuilder();

		String mMasterresponse, mGroupLoanOSId = null, mGLOS = null, result;

		String mCashinHand = null, mGroupLoanOutStanding = null;
		String mInsaAmount = sLoanAmount[0];
		String mBankAmount = sLoanAmount[1];
		String mRepaidAmount = sLoanAmount[2];
		String mInterestSubvention = sLoanAmount[3];

		mRepaymentAmount = mRepaidAmount;

		mGroupLoanOutStanding = String
				.valueOf(Integer.parseInt(Get_LoanOutstandingTask.sGet_Loan_outstanding_ServiceResponse)
						+ (Integer.parseInt(mInsaAmount) + Integer.parseInt(mBankAmount)));
		Log.e("1", Integer.parseInt(Get_LoanOutstandingTask.sGet_Loan_outstanding_ServiceResponse) + "");
		Log.e("2", Integer.parseInt(sLoanAmount[0]) + "");
		Log.e("3", Integer.parseInt(sLoanAmount[1]) + "");
		Log.e("4", mGroupLoanOutStanding);
		for (int i = 0; i < Transaction_GroupLoanRepaidMenuFragment.response.length; i++) {
			System.out.println("Response : " + Transaction_GroupLoanRepaidMenuFragment.response[i]);

			String secondResponse[] = Transaction_GroupLoanRepaidMenuFragment.response[i].split("#");
			System.out.println("LOAN ID : " + secondResponse[0]);
			Log.v("Loan Id",
					Transaction_GroupLoanRepaidMenuFragment.mSendTo_ServerLoan_Id + "  :::" + secondResponse[0]);

			mGLOS = secondResponse[1];
			if (Transaction_GroupLoanRepaidMenuFragment.mSendTo_ServerLoan_Id.equals(secondResponse[0])) {
				System.out.println("LOAN OUTS : " + secondResponse[1]);
				mGLOS = String.valueOf(Integer.parseInt(mGroupLoanOutStanding) - Integer.parseInt(mRepaidAmount)
						- Integer.parseInt(mInterestSubvention));

			}
			mGroupLoanOSId = secondResponse[0];

			mMasterresponse = mGroupLoanOSId + "#" + mGLOS + "%";

			builder.append(mMasterresponse);

		}
		result = builder.toString();
		Log.v("Update Values", result);
		
		if (EShaktiApplication.isDefault()) {
			Transaction_GroupLoanRepaidMenuFragment.response = result.split("%");
		}

		String mCashatBank = null;

		mCashatBank = SelectedGroupsTask.sCashatBank;
		String mBankDetails = GetOfflineTransactionValues.getBankDetails();
		mCashinHand = SelectedGroupsTask.sCashinHand;
		String mTrasactiondate = SelectedGroupsTask.sLastTransactionDate_Response;// DatePickerDialog.sSend_To_Server_Date;
		// SelectedGroupsTask.sLastTransactionDate_Response = mTrasactiondate;
		String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mTrasactiondate, mCashinHand,
				mCashatBank, mBankDetails);
		String mSelectedGroupId = SelectedGroupsTask.Group_Id;

		Log.e("mGroupMasterResponse $$$ ", mGroupMasterResponse + "");
		Log.e("result $$$ ", result + "");
		Log.e("Values ---- >>>", SelectedGroupsTask.sCashinHand);
		EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GROUP_MASTER_GLOS,
				new GroupLoanOSUpdate(mSelectedGroupId, mGroupMasterResponse, result));
	}

	@Subscribe
	public void OnGetSingleTransactionGLOS(final GetSingleTransResponse getSingleTransResponse) {
		switch (getSingleTransResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), getSingleTransResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), getSingleTransResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			try {
				System.out.println("--------OnGetSingleTransactionGLOS---------");
				Log.v("ContentValues", "Step 2");
				String mUniqueId = GetTransactionSinglevalues.getUniqueId();
				String mGroupLOSValues = GetTransactionSinglevalues.getGrouploanrepayment();
				String mGroupLoanCheckValues = null;
				String mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
				String mMobileDate = Get_Offline_MobileDate.getOffline_MobileDate(getActivity());
				String mTransaction_UniqueId = PrefUtils.getOfflineUniqueID();
				String mCurrentTransDate = null;
				String mGroupLoanrepaid = null;
				mBankChargesAmount = editText_mBankCharges.getText().toString();

				if (mBankChargesAmount.equals("") || mBankChargesAmount == null) {
					mBankChargesAmount = "0";
				}
				if (publicValues.mOffline_Trans_CurrentDate != null) {
					mCurrentTransDate = publicValues.mOffline_Trans_CurrentDate;
				}
				mOfflineGrouploanId = Transaction_GroupLoanRepaidMenuFragment.mSendTo_ServerLoan_Id;
				if (selectedType.equals("Cash")) {
					mGroupLoanrepaid = sSend_To_Server_GLRepaid + "#" + mOfflineGrouploanId + "#" + mTrasactiondate
							+ "#" + mMobileDate + "#" + selectedType + "$";

				} else if (selectedType.equals("Bank")) {

					mGroupLoanrepaid = sSend_To_Server_GLRepaid + mBankNameValue + "~" + mBankChargesAmount + "#"
							+ mOfflineGrouploanId + "#" + mTrasactiondate + "#" + mMobileDate + "#" + selectedType
							+ "$";
				}

				System.out.println("DB val for ML repayment  : " + mGroupLoanrepaid);

				mLastTrDate = DatePickerDialog.sDashboardDate;
				mLastTr_ID = SelectedGroupsTask.sTr_ID.toString();

				Log.e("Current Offline Values", mOfflineGrouploanId + "     " + PrefUtils.getOfflineGroupOSLID());

				if (mGroupLOSValues == null) {
					publicValues.mGrouploanCheckLabel = new String[SelectedGroupsTask.loan_EngName.size()];
					for (int i = 0; i < publicValues.mGrouploanCheckLabel.length; i++) {
						publicValues.mGrouploanCheckLabel[i] = "0";

					}
				}
				if (publicValues.mGrouploanCheckLabel != null) {
					for (int j = 0; j < publicValues.mGrouploanCheckLabel.length; j++) {
						String sLoanID = SelectedGroupsTask.loan_Id.elementAt(j).toString();

						if (sLoanID.equals(mOfflineGrouploanId)) {
							mGroupLoanCheckValues = publicValues.mGrouploanCheckLabel[j];
						}

					}
				}
				Log.e("Loan Check Values::::", mGroupLoanCheckValues);
				if (!mGroupLoanrepaid.contains("?") && !mCurrentTransDate.contains("?")
						&& !mTransaction_UniqueId.contains("?")) {

					if (!mGroupLoanrepaid.contains("?") && !mCurrentTransDate.contains("?")
							&& !mTransaction_UniqueId.contains("?")) {

						if (mGroupLoanCheckValues.equals("0")) {
							if (PrefUtils.getOfflineGroupLosrepay() == null
									&& PrefUtils.getOfflineGroupOSLID() == null) {
								PrefUtils.setOfflineGroupLosrepay(mGroupLoanrepaid);
								Log.e("Step 1", "1111");
							} else if (PrefUtils.getOfflineGroupOSLID() != null
									&& !PrefUtils.getOfflineGroupOSLID().equalsIgnoreCase(mOfflineGrouploanId)) {

								mGroupLoanrepaid = PrefUtils.getOfflineGroupLosrepay() + mGroupLoanrepaid;
								PrefUtils.setOfflineGroupLosrepay(mGroupLoanrepaid);
								Log.e("Step 3", "33333");
							}

							Log.v("Group Loan Repaid Ment Values", PrefUtils.getOfflineGroupLosrepay());

							Log.e("mGroupLoanrepaid *** ", mGroupLoanrepaid + "");
							if (mUniqueId == null && mGroupLOSValues == null
									&& !PrefUtils.getOfflineGroupLosrepay().equals(null)) {

								mSqliteDBStoredValues_GrouploanRepayValues = mGroupLoanrepaid;

								if (mLastTrDate != null && mMobileDate != null && mTransaction_UniqueId != null
										&& mCurrentTransDate != null) {
									EShaktiApplication.getInstance().getTransactionManager().startTransaction(
											DataType.TRANSACTIONADD,
											new Transaction(0, PrefUtils.getUsernameKey(), SelectedGroupsTask.UserName,
													SelectedGroupsTask.Ngo_Id, SelectedGroupsTask.Group_Id,
													mTransaction_UniqueId, mLastTr_ID, mCurrentTransDate, null, null,
													null, null, null, null, null, null, null, null, mGroupLoanrepaid,
													null, null, null, null, null, null, null, null, null, null, null));
								} else {

									TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT,
											TastyToast.ERROR);
								}

							} else if (mUniqueId.equals(PrefUtils.getOfflineUniqueID()) && mGroupLOSValues == null) {

								EShaktiApplication.setSetTransValues(TransactionOffConstants.TRANS_GLRP);

								EShaktiApplication.getInstance().getTransactionManager().startTransaction(
										DataType.TRANS_VALUES_UPDATE,
										new TransactionUpdate(mUniqueId, mGroupLoanrepaid));

							} else if (mUniqueId.equals(PrefUtils.getOfflineUniqueID()) && mGroupLOSValues == null
									&& !PrefUtils.getOfflineGroupOSLID().equals(mOfflineGrouploanId)) {

								EShaktiApplication.setSetTransValues(TransactionOffConstants.TRANS_GLRP);

								EShaktiApplication.getInstance().getTransactionManager().startTransaction(
										DataType.TRANS_VALUES_UPDATE,
										new TransactionUpdate(mUniqueId, mGroupLoanrepaid));

							} else if (mUniqueId.equals(PrefUtils.getOfflineUniqueID()) && mGroupLOSValues != null
									&& !PrefUtils.getOfflineGroupOSLID().equals(mOfflineGrouploanId)) {

								EShaktiApplication.setSetTransValues(TransactionOffConstants.TRANS_GLRP);

								EShaktiApplication.getInstance().getTransactionManager().startTransaction(
										DataType.TRANS_VALUES_UPDATE,
										new TransactionUpdate(mUniqueId, mGroupLoanrepaid));

							} else if ((mGroupLOSValues != null)
									|| PrefUtils.getOfflineGroupOSLID().equals(mOfflineGrouploanId)) {

								confirmationDialog.dismiss();

								TastyToast.makeText(getActivity(), AppStrings.offlineDataAvailable,
										TastyToast.LENGTH_SHORT, TastyToast.WARNING);

								DatePickerDialog.sDashboardDate = SelectedGroupsTask.sLastTransactionDate_Response;
								MainFragment_Dashboard fragment = new MainFragment_Dashboard();
								setFragment(fragment);
							}
						} else {
							confirmationDialog.dismiss();

							TastyToast.makeText(getActivity(), AppStrings.offlineDataAvailable, TastyToast.LENGTH_SHORT,
									TastyToast.WARNING);

							DatePickerDialog.sDashboardDate = SelectedGroupsTask.sLastTransactionDate_Response;
							MainFragment_Dashboard fragment = new MainFragment_Dashboard();
							setFragment(fragment);
						}

					} else {

						if (confirmationDialog.isShowing() && confirmationDialog != null) {
							confirmationDialog.dismiss();
						}
						Log.v("Checking Values!!!!!!!!!!!!!!!!!!!!!!! Else", "!!!!!!!!@@@@@@@@@@@@@@@s");
						TastyToast.makeText(getActivity(), "Please Try Again", TastyToast.LENGTH_SHORT,
								TastyToast.ERROR);

						MainFragment_Dashboard fragment = new MainFragment_Dashboard();
						setFragment(fragment);

					}
				} else {

					if (confirmationDialog.isShowing() && confirmationDialog != null) {
						confirmationDialog.dismiss();
					}

					TastyToast.makeText(getActivity(), "Please Try Again", TastyToast.LENGTH_SHORT, TastyToast.ERROR);

					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);

				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		}
	}

	@Subscribe
	public void onAddTransactionGroupLRepay(final TransactionResponse transactionResponse) {
		switch (transactionResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), transactionResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), transactionResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:
			try {
				System.out.println("--------onAddTransactionGroupLRepay---------");
				Log.v("ContentValues", "Step 3");

				TransactionProvider.getSinlgeGroupMaster_Transaction(
						new Transaction_UniqueIdSingle(PrefUtils.getOfflineUniqueID()));

				if (GetTransactionSinglevalues.getGrouploanrepayment() != null

						&& !GetTransactionSinglevalues.getGrouploanrepayment().equals("")) {
					StringBuilder builder = new StringBuilder();

					String mMasterresponse, mGroupLoanOSId = null, mGLOS = null, result, mCashatBank_New;

					String mCashinHand = null, mGroupLoanOutStanding = null;
					String mInsaAmount = sLoanAmount[0];
					String mBankAmount = sLoanAmount[1];
					String mRepaidAmount = sLoanAmount[2];
					String mInterestSubvention = sLoanAmount[3];
					mRepaymentAmount = mRepaidAmount;

					mCashatBank_New = SelectedGroupsTask.sCashatBank;

					if (selectedType.equals("Cash")) {
						mCashinHand = String.valueOf(
								Integer.parseInt(SelectedGroupsTask.sCashinHand) - Integer.parseInt(mRepaidAmount));
						mGroupLoanOutStanding = String
								.valueOf(Integer.parseInt(Get_LoanOutstandingTask.sGet_Loan_outstanding_ServiceResponse)
										+ (Integer.parseInt(mInsaAmount) + Integer.parseInt(mBankAmount)));

					} else if (selectedType.equals("Bank")) {
						mCashinHand = SelectedGroupsTask.sCashinHand;

						System.out.println("Bank amount values _----->>>" + mBankChargesAmount);

						mCashatBank_New = String.valueOf(Integer.parseInt(SelectedGroupsTask.sCashatBank)
								- Integer.parseInt(mRepaidAmount) - Integer.parseInt(mBankChargesAmount));

						mGroupLoanOutStanding = String
								.valueOf(Integer.parseInt(Get_LoanOutstandingTask.sGet_Loan_outstanding_ServiceResponse)
										+ (Integer.parseInt(mInsaAmount) + Integer.parseInt(mBankAmount)));

					}

					Log.e("1", Integer.parseInt(Get_LoanOutstandingTask.sGet_Loan_outstanding_ServiceResponse) + "");
					Log.e("2", Integer.parseInt(sLoanAmount[0]) + "");
					Log.e("3", Integer.parseInt(sLoanAmount[1]) + "");
					Log.e("4", mGroupLoanOutStanding);
					for (int i = 0; i < Transaction_GroupLoanRepaidMenuFragment.response.length; i++) {
						System.out.println("Response : " + Transaction_GroupLoanRepaidMenuFragment.response[i]);

						String secondResponse[] = Transaction_GroupLoanRepaidMenuFragment.response[i].split("#");
						System.out.println("LOAN ID : " + secondResponse[0]);
						Log.v("Loan Id", Transaction_GroupLoanRepaidMenuFragment.mSendTo_ServerLoan_Id + "  :::"
								+ secondResponse[0]);

						mGLOS = secondResponse[1];
						if (Transaction_GroupLoanRepaidMenuFragment.mSendTo_ServerLoan_Id.equals(secondResponse[0])) {
							System.out.println("LOAN OUTS : " + secondResponse[1]);
							mGLOS = String.valueOf(Integer.parseInt(mGroupLoanOutStanding)
									- Integer.parseInt(mRepaidAmount) - Integer.parseInt(mInterestSubvention));

						}
						mGroupLoanOSId = secondResponse[0];

						mMasterresponse = mGroupLoanOSId + "#" + mGLOS + "%";

						builder.append(mMasterresponse);

					}
					result = builder.toString();
					Log.v("Update Values", result);
					String mCashatBank = null;
					if (selectedType.equals("Cash")) {
						mCashatBank = SelectedGroupsTask.sCashatBank;
					} else if (selectedType.equals("Bank")) {
						mCashatBank = mCashatBank_New;
					}
					if (selectedType.equals("Cash")) {
						EShaktiApplication.setGroupLoanRepayBank(false);
					} else if (selectedType.equals("Bank")) {
						EShaktiApplication.setGroupLoanRepayBank(true);
					}

					Log.e("Cash At Bank!!!!!!!!!!!", mCashatBank);

					SelectedGroupsTask.sCashatBank = mCashatBank;

					String mBankDetails = GetOfflineTransactionValues.getBankDetails();
					SelectedGroupsTask.sCashinHand = mCashinHand;
					String mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
					SelectedGroupsTask.sLastTransactionDate_Response = mTrasactiondate;
					String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mTrasactiondate,
							mCashinHand, mCashatBank, mBankDetails);
					String mSelectedGroupId = SelectedGroupsTask.Group_Id;

					Log.e("mGroupMasterResponse $$$", mGroupMasterResponse + "");
					Log.e("result $$$ ", result + "");

					EShaktiApplication.getInstance().getTransactionManager().startTransaction(
							DataType.GROUP_MASTER_GLOS,
							new GroupLoanOSUpdate(mSelectedGroupId, mGroupMasterResponse, result));
				} else {
					if (confirmationDialog.isShowing() && confirmationDialog != null) {
						confirmationDialog.dismiss();
					}

					TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT, TastyToast.ERROR);

					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);

				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		}
	}

	@Subscribe
	public void OnTransUpdateGLOS(final TransactionUpdateResponse transactionUpdateResponse) {
		switch (transactionUpdateResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), transactionUpdateResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), transactionUpdateResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:
			try {
				System.out.println("------OnTransUpdateGLOS-------");

				TransactionProvider.getSinlgeGroupMaster_Transaction(
						new Transaction_UniqueIdSingle(PrefUtils.getOfflineUniqueID()));

				if (GetTransactionSinglevalues.getGrouploanrepayment() != null

						&& !GetTransactionSinglevalues.getGrouploanrepayment().equals("")) {
					StringBuilder builder = new StringBuilder();

					String mMasterresponse, mGroupLoanOSId = null, mGLOS = null, result, mCashatBank_New;

					String mCashinHand = null, mGroupLoanOutStanding = null;
					String mInsaAmount = sLoanAmount[0];
					String mBankAmount = sLoanAmount[1];
					String mRepaidAmount = sLoanAmount[2];
					String mInterestSubvention = sLoanAmount[3];

					mRepaymentAmount = mRepaidAmount;

					mCashatBank_New = SelectedGroupsTask.sCashatBank;

					if (selectedType.equals("Cash")) {
						mCashinHand = String.valueOf(
								Integer.parseInt(SelectedGroupsTask.sCashinHand) - Integer.parseInt(mRepaidAmount));
						mGroupLoanOutStanding = String
								.valueOf(Integer.parseInt(Get_LoanOutstandingTask.sGet_Loan_outstanding_ServiceResponse)
										+ (Integer.parseInt(mInsaAmount) + Integer.parseInt(mBankAmount)));

					} else if (selectedType.equals("Bank")) {
						mCashinHand = SelectedGroupsTask.sCashinHand;
						mCashatBank_New = String.valueOf(Integer.parseInt(SelectedGroupsTask.sCashatBank)
								- Integer.parseInt(mRepaidAmount) - Integer.parseInt(mBankChargesAmount));
						mGroupLoanOutStanding = String
								.valueOf(Integer.parseInt(Get_LoanOutstandingTask.sGet_Loan_outstanding_ServiceResponse)
										+ (Integer.parseInt(mInsaAmount) + Integer.parseInt(mBankAmount)));

					}

					Log.e("1", Integer.parseInt(Get_LoanOutstandingTask.sGet_Loan_outstanding_ServiceResponse) + "");
					Log.e("2", Integer.parseInt(sLoanAmount[0]) + "");
					Log.e("3", Integer.parseInt(sLoanAmount[1]) + "");
					Log.e("4", mGroupLoanOutStanding);
					for (int i = 0; i < Transaction_GroupLoanRepaidMenuFragment.response.length; i++) {
						System.out.println("Response : " + Transaction_GroupLoanRepaidMenuFragment.response[i]);

						String secondResponse[] = Transaction_GroupLoanRepaidMenuFragment.response[i].split("#");
						System.out.println("LOAN ID : " + secondResponse[0]);
						Log.v("Loan Id", Transaction_GroupLoanRepaidMenuFragment.mSendTo_ServerLoan_Id + "  :::"
								+ secondResponse[0]);

						mGLOS = secondResponse[1];
						if (Transaction_GroupLoanRepaidMenuFragment.mSendTo_ServerLoan_Id.equals(secondResponse[0])) {
							System.out.println("LOAN OUTS : " + secondResponse[1]);
							mGLOS = String.valueOf(Integer.parseInt(mGroupLoanOutStanding)
									- Integer.parseInt(mRepaidAmount) - Integer.parseInt(mInterestSubvention));

						}
						mGroupLoanOSId = secondResponse[0];

						mMasterresponse = mGroupLoanOSId + "#" + mGLOS + "%";

						builder.append(mMasterresponse);

					}
					result = builder.toString();
					Log.v("Update Values", result);
					
					if (EShaktiApplication.isDefault()) {
						Transaction_GroupLoanRepaidMenuFragment.response = result.split("%");
					}

					String mCashatBank = null;
					if (selectedType.equals("Cash")) {
						mCashatBank = SelectedGroupsTask.sCashatBank;
					} else if (selectedType.equals("Bank")) {
						mCashatBank = mCashatBank_New;
					}

					if (selectedType.equals("Cash")) {
						EShaktiApplication.setGroupLoanRepayBank(false);
					} else if (selectedType.equals("Bank")) {
						EShaktiApplication.setGroupLoanRepayBank(true);
					}

					SelectedGroupsTask.sCashatBank = mCashatBank;
					String mBankDetails = GetOfflineTransactionValues.getBankDetails();
					SelectedGroupsTask.sCashinHand = mCashinHand;
					String mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
					SelectedGroupsTask.sLastTransactionDate_Response = mTrasactiondate;
					String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mTrasactiondate,
							mCashinHand, mCashatBank, mBankDetails);
					String mSelectedGroupId = SelectedGroupsTask.Group_Id;

					Log.e("mGroupMasterResponse $$$ ", mGroupMasterResponse + "");
					Log.e("result $$$ ", result + "");
					EShaktiApplication.getInstance().getTransactionManager().startTransaction(
							DataType.GROUP_MASTER_GLOS,
							new GroupLoanOSUpdate(mSelectedGroupId, mGroupMasterResponse, result));
				} else {
					if (confirmationDialog.isShowing() && confirmationDialog != null) {
						confirmationDialog.dismiss();
					}

					TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT, TastyToast.ERROR);

					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);

				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		}
	}

	@Subscribe
	public void OnGroupGLOSUpdate(final GroupLoanOSResponse groupLoanOSResponse) {
		switch (groupLoanOSResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), groupLoanOSResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), groupLoanOSResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:
			try {
				System.out.println("--------OnGroupGLOSUpdate--------");
				if (ConnectionUtils.isNetworkAvailable(getActivity())) {

					try {

						PrefUtils.setOfflineGroupOsLoanID(mOfflineGrouploanId);

						EShaktiApplication.getInstance().getTransactionManager().startTransaction(
								DataType.GET_LOANACCOUNT_NUMBER, new GroupMasterSingle(SelectedGroupsTask.Group_Id));

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} else {
					PrefUtils.setOfflineGroupOsLoanID(mOfflineGrouploanId);
					for (int j = 0; j < publicValues.mGrouploanCheckLabel.length; j++) {
						String sLoanID = SelectedGroupsTask.loan_Id.elementAt(j).toString();

						if (sLoanID.equals(PrefUtils.getOfflineGroupOSLID())) {
							publicValues.mGrouploanCheckLabel[j] = "1";
						}

					}

					try {

						EShaktiApplication.getInstance().getTransactionManager().startTransaction(
								DataType.GET_LOANACCOUNT_NUMBER, new GroupMasterSingle(SelectedGroupsTask.Group_Id));

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		}
	}

	@Subscribe
	public void OnGroupMasterLoanAccNoDetailsResponse(final GetLoanAccountNoResponse getLoanAccNoResponse) {
		switch (getLoanAccNoResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), getLoanAccNoResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), getLoanAccNoResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			System.out.println("-------------   OnGroupMasterLoanAccNoDetailsResponse  ---------");
			try {
				String mGetLoanAccNoResponse = GetGroupMemberDetails.getLoanOSDetails();
				Log.e("Offline get LoanAccountNo response ", mGetLoanAccNoResponse + "");

				if (mGetLoanAccNoResponse != null && !mGetLoanAccNoResponse.equals("")) {
					String[] arrValues = mGetLoanAccNoResponse.split("#");

					mLoanBankName.clear();
					mLoanName.clear();
					mLoanId.clear();
					mLoanOutstanding.clear();
					mLoanAccNo.clear();
					String mSelectedGroupId = SelectedGroupsTask.Group_Id;
					for (int i = 0; i < arrValues.length; i++) {
						String ValuesSplit = arrValues[i];

						String[] arrValuesSplit = ValuesSplit.split("~");

						if (arrValuesSplit.length != 0 && arrValuesSplit.length == 5) {
							mLoanBankName.add(arrValuesSplit[0]);
							mLoanName.add(arrValuesSplit[1]);
							mLoanId.add(arrValuesSplit[2]);
							mLoanOutstanding.add(arrValuesSplit[3]);
							mLoanAccNo.add(arrValuesSplit[4]);
						}

					}

					String mTempLoanBankName = null, mTempLoanname = null, mTempLoanId = null,
							mTempLoanOustanding = null, mTempLoanAccNo = null;
					String response = "";
					String mInsaAmount = sLoanAmount[0];
					String mBankAmount = sLoanAmount[1];
					String mRepaidAmount = sLoanAmount[2];
					String mInterestSubvention = sLoanAmount[3];

					for (int i = 0; i < mLoanName.size(); i++) {
						mTempLoanBankName = mLoanBankName.get(i).toString();
						mTempLoanname = mLoanName.get(i).toString();
						mTempLoanId = mLoanId.get(i).toString();
						mTempLoanOustanding = mLoanOutstanding.get(i).toString();
						mTempLoanAccNo = mLoanAccNo.get(i).toString();

						if (EShaktiApplication.getLoanId().equals(mLoanId.get(i))) {
							String mLoan_Outstanding = mLoanOutstanding.get(i).toString();
							String mLoan_AccNo = mLoanAccNo.get(i).toString();
							@SuppressWarnings("unused")
							String mLoan_BankName = mLoanBankName.get(i).toString();

							int Loan_Outstanding = Integer.parseInt(mLoan_Outstanding) + Integer.parseInt(mInsaAmount)
									+ Integer.parseInt(mBankAmount) - Integer.parseInt(mRepaidAmount)
									- Integer.parseInt(mInterestSubvention);
							mLoan_Outstanding = String.valueOf(Loan_Outstanding);
							mTempLoanOustanding = mLoan_Outstanding;

							ToLoanAccNo = mLoan_AccNo;
							outstandingAmt = mLoan_Outstanding;

						}
						response = response + mTempLoanBankName + "~" + mTempLoanname + "~" + mTempLoanId + "~"
								+ mTempLoanOustanding + "~" + mTempLoanAccNo + "#";

					}

					String result = response.toString();
					Log.e("Values------------------------>>>>>>", result);

					if (mCount == 0) {
						mCount++;

						EShaktiApplication.getInstance().getTransactionManager().startTransaction(
								DataType.GET_LOANACCNO_DETAILS_UPDATE,
								new LoanAccountNoDetailsUpdate(mSelectedGroupId, result));
					}

				} else {

					TastyToast.makeText(getActivity(), AppStrings.loginAgainAlert, TastyToast.LENGTH_LONG,
							TastyToast.WARNING);
				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		}

	}

	@Subscribe
	public void OnLoanAccNoUpdate(final LoanAccountNoUpdateResponse loanAccNoResponse) {
		switch (loanAccNoResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), loanAccNoResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), loanAccNoResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			System.out.println("-------------- OnLoanAccNoUpdate ----------------");
			try {
				confirmationDialog.dismiss();
				EShaktiApplication.setGroupLoanRepayBank(false);

				TastyToast.makeText(getActivity(), AppStrings.transactionCompleted, TastyToast.LENGTH_SHORT,
						TastyToast.SUCCESS);

				if (!ConnectionUtils.isNetworkAvailable(getActivity())) {

					String mValues = Put_DB_GroupNameDetail_Response
							.put_DB_GroupNameDetails_Response(EShaktiApplication.getGroupResponse());

					GroupProvider.updateGroupResponse(
							new GroupResponseUpdate(EShaktiApplication.getGroupId_GroupLastTransDate(), mValues));
				} else {

					String mValues = Put_DB_GroupNameDetail_Response
							.put_DB_GroupNameDetails_Response(EShaktiApplication.getGroupResponse());

					GroupProvider.updateGroupResponse(
							new GroupResponseUpdate(EShaktiApplication.getGroupId_GroupLastTransDate(), mValues));

				}

				if (EShaktiApplication.isDefault()) {
					int loan_size = SelectedGroupsTask.loan_Id.size();
					Log.e("Loan Size !!!!!!!!", loan_size + "");
					Log.e("Before increment groupLoanCount   =   ",
							Transaction_InternalLoan_DisbursementFragment.groupLoanCount + "");
					int i = Transaction_InternalLoan_DisbursementFragment.groupLoanCount++;
					Log.e("After increment groupLoanCount   =   ", i + "");

					Log.e("PublicVariables.groupLoanCount   =   ",
							Transaction_InternalLoan_DisbursementFragment.groupLoanCount + "");
					if (Transaction_InternalLoan_DisbursementFragment.groupLoanCount <= loan_size) {
						if (!SelectedGroupsTask.loan_Id.elementAt(i).equals("0")) {

							try {
								Transaction_GroupLoanRepaidMenuFragment.mloan_Id = SelectedGroupsTask.loan_Id
										.elementAt(i).toString();
								System.out.println("Loan Id" + Transaction_GroupLoanRepaidMenuFragment.mloan_Id);

								Transaction_GroupLoanRepaidMenuFragment.mloan_Name = SelectedGroupsTask.loan_Name
										.elementAt(i).toString();
								System.out.println("Loan Name:" + Transaction_GroupLoanRepaidMenuFragment.mloan_Name);

								String temp_loanName = SelectedGroupsTask.loan_Name.elementAt(i).toString();

								Transaction_GroupLoanRepaidMenuFragment.mSendTo_ServerLoan_Id = SelectedGroupsTask.loan_Id
										.elementAt(i).toString();

								EShaktiApplication
										.setLoanId(String.valueOf(Transaction_GroupLoanRepaidMenuFragment.mloan_Id));
								
								EShaktiApplication.setGroupLoanRepaymentLoanBankName(SelectedGroupsTask.loanAcc_bankName.elementAt(i));
								EShaktiApplication.setGroupLoanRepaymentAccNo(SelectedGroupsTask.loanAcc_loanAccNo.elementAt(i));

								if (Transaction_GroupLoanRepaidMenuFragment.mloan_Name.equals(temp_loanName)) {

									if (ConnectionUtils.isNetworkAvailable(getActivity())) {

										EShaktiApplication.getInstance().getTransactionManager().startTransaction(
												DataType.GET_GROUPLOAN_OUTSTANDING,
												new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));

									} else {
										// DO OFFLINE STUFFS

										if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {

											EShaktiApplication.getInstance().getTransactionManager().startTransaction(
													DataType.GET_GROUPLOAN_OUTSTANDING,
													new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));
										} else {
											TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF,
													TastyToast.LENGTH_SHORT, TastyToast.ERROR);
										}

									}

								}

							} catch (Exception e) {
								e.printStackTrace();
							}
						} else {

							if (ConnectionUtils.isNetworkAvailable(getActivity())) {
								System.out.println("----------- isMeetingValues true----");
								isMeetingValues = true;
								new Get_Group_Minutes_NewTask(this).execute();

							} else {
								if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
									EShaktiApplication.getInstance().getTransactionManager().startTransaction(
											DataType.DEFAULT_MINUTESOFMEETINGS,
											new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));
								} else {

									TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF,
											TastyToast.LENGTH_SHORT, TastyToast.ERROR);
								}
							}

						}
					}
				} else {

					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);

				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		}
	}

	@Subscribe
	public void OnGetGroupLoanOutstandingResponse(final GetGroupLoanOutstandingResponse groupLoanOutstandingResponse) {
		switch (groupLoanOutstandingResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), groupLoanOutstandingResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), groupLoanOutstandingResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:
			System.out.println("---------------- OnGroupLoanOSResponse ------------------");
			String mGroupLoanOSResponse = GetGroupMemberDetails.getGrouploanOS();
			Log.e("Group response@@@@@@@", mGroupLoanOSResponse + "");
			if (mGroupLoanOSResponse != null && !mGroupLoanOSResponse.equals("")) {

				String[] response_Grouploanrepaid = mGroupLoanOSResponse.split("%");

				for (int i = 0; i < response_Grouploanrepaid.length; i++) {
					System.out.println("Response : " + response_Grouploanrepaid[i]);

					String secondResponse[] = response_Grouploanrepaid[i].split("#");
					System.out.println("LOAN ID : " + secondResponse[0]);
					Log.e("LOAN ID : ", secondResponse[0]);

					if (Transaction_GroupLoanRepaidMenuFragment.mSendTo_ServerLoan_Id.equals(secondResponse[0])) {

						System.out.println("LOAN OUTS : " + secondResponse[1]);
						mGroupOS_Offlineresponse = secondResponse[1];
						Get_LoanOutstandingTask.sGet_Loan_outstanding_ServiceResponse = mGroupOS_Offlineresponse;
						Log.e("Group Os Values----->>>", mGroupOS_Offlineresponse);
						System.out.println("LOAN OUTS 121212121: "
								+ Get_LoanOutstandingTask.sGet_Loan_outstanding_ServiceResponse);
					}

				}
				if (EShaktiApplication.isDefault()) {
					PrefUtils.setStepWiseScreenCount("6");
				}
				Transaction_GroupLoanRepaidFragment fragment = new Transaction_GroupLoanRepaidFragment();
				setFragment(fragment);
			} else {
				TastyToast.makeText(getActivity(), AppStrings.loginAgainAlert, TastyToast.LENGTH_LONG,
						TastyToast.WARNING);
			}
			break;
		}

	}

	@Subscribe
	public void OnGroupMasterMinutesofmeetingResponse(
			final DefaultOffline_MinutesResponse getMinutesofMeetingResponse) {
		switch (getMinutesofMeetingResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), getMinutesofMeetingResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), getMinutesofMeetingResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:
			System.out.println("-------------------OnGroupMasterMinutesofmeetingResponse ---------------------");
			Log.v("Personal Loan outStandings FROM DB ", GetGroupMemberDetails.getMasterminutes() + "            "
					+ GetGroupMemberDetails.getIndividualgroupminutes());
			String mMasterMinutes = GetGroupMemberDetails.getMasterminutes();
			String mIndividualMinutes = GetGroupMemberDetails.getIndividualgroupminutes();
			String mMasterMinut[] = mMasterMinutes.split("~");
			String mIndi[] = mIndividualMinutes.split("~");
			int mSize = 0;

			mSize = mMasterMinut.length / 2;
			Log.i("Group Length", mSize + "");
			String mMasterValues[] = null, mMasterId[] = null;
			StringBuilder builder = new StringBuilder();
			String mMasterresponse, result;
			mMasterValues = new String[mSize];
			mMasterId = new String[mSize];
			int j = 0, k = 0;

			for (int i = 0; i < mMasterMinut.length; i++) {

				if (i % 2 == 0) {
					mMasterId[j] = mMasterMinut[i];

					j++;
				} else {

					mMasterValues[k] = mMasterMinut[i];
					k++;

				}
			}

			for (int i = 0; i < mIndi.length; i++) {
				String mIndiValues = mIndi[i];
				Log.e("mIndivalues", mIndiValues);
				Log.e("Master ID Length Values", mMasterId.length + "");

				for (int m = 0; m < mMasterId.length; m++) {

					if (mIndiValues.equals(mMasterId[m])) {

						mMasterresponse = mIndiValues + "~" + mMasterValues[m] + "~";

						builder.append(mMasterresponse);
					}
				}

			}
			result = builder.toString();
			System.out.println("mMasterValues " + result);

			Get_Group_Minutes_NewTask.sGet_Group_Minutes__new_Response = result;

			if (EShaktiApplication.isDefault()) {
				PrefUtils.setStepWiseScreenCount("6");
			}
			Meeting_MinutesOfMeetingFragment meetingFragment = new Meeting_MinutesOfMeetingFragment();
			setFragment(meetingFragment);

			break;
		}

	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		SBus.INST.unRegister(this);
	}

	@SuppressWarnings("deprecation")
	private void showConfirmationDialog() {
		// TODO Auto-generated method stub
		confirmationDialog = new Dialog(getActivity());

		LayoutInflater inflater = getActivity().getLayoutInflater();
		View dialogView = inflater.inflate(R.layout.dialog_confirmation, null);
		dialogView.setLayoutParams(
				new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

		TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
		confirmationHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.confirmation));
		confirmationHeader.setTypeface(LoginActivity.sTypeface);

		TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

		TableRow typeRow = new TableRow(getActivity());

		TableRow.LayoutParams typeParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT, 1f);
		typeParams.setMargins(10, 5, 10, 5);

		TextView type = new TextView(getActivity());
		type.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mLoanAccType)));
		type.setTypeface(LoginActivity.sTypeface);
		type.setTextColor(color.black);
		type.setPadding(5, 5, 5, 5);
		type.setLayoutParams(typeParams);
		typeRow.addView(type);

		TextView typeValue = new TextView(getActivity());
		typeValue.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(selectedType)));
		typeValue.setTextColor(color.black);
		typeValue.setPadding(5, 5, 5, 5);
		typeValue.setGravity(Gravity.RIGHT);
		typeValue.setLayoutParams(typeParams);
		typeRow.addView(typeValue);

		confirmationTable.addView(typeRow,
				new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

		if (mBankRadio.isChecked()) {
			TableRow bankNameRow = new TableRow(getActivity());

			TableRow.LayoutParams bankNameParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);
			bankNameParams.setMargins(10, 5, 10, 5);

			TextView bankName = new TextView(getActivity());
			bankName.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.bankName)));
			bankName.setTypeface(LoginActivity.sTypeface);
			bankName.setTextColor(color.black);
			bankName.setPadding(5, 5, 5, 5);
			bankName.setLayoutParams(bankNameParams);
			bankNameRow.addView(bankName);

			TextView bankNameValue = new TextView(getActivity());
			bankNameValue.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(mBankNameValue)));
			bankNameValue.setTextColor(color.black);
			bankNameValue.setPadding(5, 5, 5, 5);
			bankNameValue.setGravity(Gravity.RIGHT);
			bankNameValue.setLayoutParams(bankNameParams);
			bankNameRow.addView(bankNameValue);

			confirmationTable.addView(bankNameRow,
					new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

		}
		for (int i = 0; i < size; i++) {

			TableRow indv_DepositEntryRow = new TableRow(getActivity());

			TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);
			contentParams.setMargins(10, 5, 10, 5);

			TextView memberName_Text = new TextView(getActivity());
			memberName_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sGroupLoanName[i])));
			memberName_Text.setTypeface(LoginActivity.sTypeface);
			memberName_Text.setTextColor(color.black);
			memberName_Text.setPadding(5, 5, 5, 5);
			memberName_Text.setLayoutParams(contentParams);
			indv_DepositEntryRow.addView(memberName_Text);

			TextView confirm_values = new TextView(getActivity());
			confirm_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sLoanAmount[i])));
			confirm_values.setTextColor(color.black);
			confirm_values.setPadding(5, 5, 5, 5);
			confirm_values.setGravity(Gravity.RIGHT);
			confirm_values.setLayoutParams(contentParams);
			indv_DepositEntryRow.addView(confirm_values);

			confirmationTable.addView(indv_DepositEntryRow,
					new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		}

		if (mBankRadio.isChecked()) {
			TableRow bankChargeRow = new TableRow(getActivity());

			TableRow.LayoutParams bankChargeParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);
			bankChargeParams.setMargins(10, 5, 10, 5);

			TextView bankCharge = new TextView(getActivity());
			bankCharge.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mBankCharges)));
			bankCharge.setTypeface(LoginActivity.sTypeface);
			bankCharge.setTextColor(color.black);
			bankCharge.setPadding(5, 5, 5, 5);
			bankCharge.setLayoutParams(bankChargeParams);
			bankChargeRow.addView(bankCharge);

			TextView bankChargeValue = new TextView(getActivity());
			bankChargeValue.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(mBankChargesAmount)));
			bankChargeValue.setTextColor(color.black);
			bankChargeValue.setPadding(5, 5, 5, 5);
			bankChargeValue.setGravity(Gravity.RIGHT);
			bankChargeValue.setLayoutParams(bankChargeParams);
			bankChargeRow.addView(bankChargeValue);

			confirmationTable.addView(bankChargeRow,
					new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

		}
		mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit_button);
		mEdit_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.edit));
		mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
		mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
																// 205,
																// 0));
		mEdit_RaisedButton.setOnClickListener(this);

		mOk_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Ok_button);
		mOk_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.yes));
		mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
		mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
		mOk_RaisedButton.setOnClickListener(this);

		confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		confirmationDialog.setCanceledOnTouchOutside(false);
		confirmationDialog.setContentView(dialogView);
		confirmationDialog.setCancelable(true);
		confirmationDialog.show();

		MarginLayoutParams margin = (MarginLayoutParams) dialogView.getLayoutParams();
		margin.leftMargin = 10;
		margin.rightMargin = 10;
		margin.topMargin = 10;
		margin.bottomMargin = 10;
		margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);

	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub
		Log.e("Current Class Name!!!!!!!!!!!!!!", fragment.getClass().getName());
		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

		FragmentDrawer.drawer_CashinHand.setText(RegionalConversion.getRegionalConversion(AppStrings.cashinhand)

				+ SelectedGroupsTask.sCashinHand);
		FragmentDrawer.drawer_CashinHand.setTypeface(LoginActivity.sTypeface);

		FragmentDrawer.drawer_CashatBank.setText(RegionalConversion.getRegionalConversion(AppStrings.cashatBank)

				+ SelectedGroupsTask.sCashatBank);
		FragmentDrawer.drawer_CashatBank.setTypeface(LoginActivity.sTypeface);

		FragmentDrawer.drawer_lastTR_Date
				.setText(RegionalConversion.getRegionalConversion(AppStrings.lastTransactionDate) + " : "
						+ SelectedGroupsTask.sLastTransactionDate_Response);
		FragmentDrawer.drawer_lastTR_Date.setTypeface(LoginActivity.sTypeface);

		UpdateCalendarMinMaxDateUtils.OnUpdateCalendarDate();

		Calendar calender = Calendar.getInstance();

		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String formattedDate = df.format(calender.getTime());
		EShaktiApplication.setLastTransDate_GroupId(SelectedGroupsTask.Group_Id);

		new GroupTempDBLastTransDate(EShaktiApplication.getLastTransDate_GroupId(),
				SelectedGroupsTask.sLastTransactionDate_Response, formattedDate);

		GroupProvider.updateGroupLastTransDateResponse();
	}

}
