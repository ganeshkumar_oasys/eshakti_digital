package com.yesteam.eshakti.view.activity;

import java.util.Calendar;
import java.util.Locale;

import com.tutorialsee.lib.TastyToast;

import com.oasys.eshakti.digitization.R;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog.OnDateSetListener;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.GetExit;
import com.yesteam.eshakti.utils.GetTypeface;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.views.RaisedButton;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class Audit_Date_selectionActivity extends AppCompatActivity implements TaskListener, OnClickListener, OnDateSetListener {

	Toolbar mToolbar;
	Context context;

	private Dialog mProgressDialog;
	EditText mSearch_Groupid_editext;
	ImageView mSearch_groupid_imageview;
	Typeface sTypeface;
	String mLanguageLocalae = "";
	TextView mAuditName_textview, mAuditId_Textview, mAuditDate_textview;
	TextView mAuditFromDate_texview, mAuditTodate_Textview;
	public static TextView mAuditFromDate_Value_texview, mAuditTodate_Value_Textview;
	RaisedButton mSubmit_button;
	public static String audit_trial_balance_check = "";
	public static String fromDate = "", toDate = "";
	private String mLanguageLocale = "";
	Locale locale;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		super.onCreate(savedInstanceState);

		setContentView(R.layout.audit_activity_date_selection);

		try {

			if (PrefUtils.getUserlangcode() != null) {
				mLanguageLocalae = PrefUtils.getUserlangcode();
			} else {
				mLanguageLocalae = null;
			}

			sTypeface = GetTypeface.getTypeface(getApplicationContext(), mLanguageLocalae);

		} catch (Exception e) {
			e.printStackTrace();
		}

		try {

			mToolbar = (Toolbar) findViewById(R.id.audit_date_selection_toolbar_grouplist);
			TextView mTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);
			setSupportActionBar(mToolbar);
			getSupportActionBar().setDisplayShowHomeEnabled(true);

			getSupportActionBar().setTitle("");
			// mTitle.setText(Login_webserviceTask.mAnimatorName + " " +
			// LoginTask.UserName);
			mTitle.setText(PrefUtils.getAnimatorName() + "    " + PrefUtils.getAgentUserName());
			mTitle.setTypeface(sTypeface);
			mTitle.setGravity(Gravity.CENTER);

			audit_trial_balance_check = "";
			mAuditName_textview = (TextView) findViewById(R.id.audit_date_selection_auditName);
			mAuditId_Textview = (TextView) findViewById(R.id.audit_date_selection_audit_id);
			mAuditDate_textview = (TextView) findViewById(R.id.audit_date_selection_audit_last_auditdate);

			mAuditFromDate_texview = (TextView) findViewById(R.id.audit_date_selection_FromDateText);
			mAuditTodate_Textview = (TextView) findViewById(R.id.audit_date_selection_ToDateText);

			mAuditFromDate_Value_texview = (TextView) findViewById(R.id.audit_date_selection_fromDateEditText);
			mAuditTodate_Value_Textview = (TextView) findViewById(R.id.audit_date_selection_toDateEditText);
			
			mSubmit_button = (RaisedButton) findViewById(R.id.audit_date_selection_Submitbutton);
			mSubmit_button.setOnClickListener(this);

			mAuditName_textview.setTypeface(sTypeface);
			mAuditId_Textview.setTypeface(sTypeface);
			mAuditDate_textview.setTypeface(sTypeface);
			mAuditFromDate_texview.setTypeface(sTypeface);
			mAuditTodate_Textview.setTypeface(sTypeface);
			mAuditFromDate_Value_texview.setTypeface(sTypeface);
			mAuditTodate_Value_Textview.setTypeface(sTypeface);

			mAuditName_textview.setText("AUDITOR NAME : " + " PRABHURAJA");
			mAuditId_Textview.setText("AUDITOR ID : " + " 9677442028");
			mAuditDate_textview.setText("LAST TRANSACTION DATE : " + " 15/09/2017");

			mAuditFromDate_texview.setText("FROM DATE ");
			mAuditTodate_Textview.setText("TO DATE ");

			mAuditFromDate_Value_texview.setOnClickListener(this);
			mAuditTodate_Value_Textview.setOnClickListener(this);
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_edit_ob_group, menu);
		MenuItem item = menu.getItem(0);
		item.setVisible(true);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.

		int id = item.getItemId();
		if (id == R.id.group_logout_edit) {

			startActivity(new Intent(GetExit.getExitIntent(getApplicationContext())));
			this.finish();

			return true;

		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onTaskStarted() {

		mProgressDialog = AppDialogUtils.createProgressDialog(this);
		mProgressDialog.show();
	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub

		if (mProgressDialog != null) {

			/**
			 * Inorder to Start Service for collecting the GroupDetails at background
			 **/
			System.out.println("Task Finished Values:::" + result);
			if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {

				mProgressDialog.dismiss();
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						if (!result.equals("FAIL")) {

							TastyToast.makeText(getApplicationContext(), AppStrings.mCommonNetworkErrorMsg,
									TastyToast.LENGTH_SHORT, TastyToast.ERROR);
							Constants.NETWORKCOMMONFLAG = "SUCCESS";
						}
					}
				});

			} else {
			}
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {
		case R.id.audit_date_selection_Submitbutton:

			String fromDate_value = mAuditFromDate_Value_texview.getText().toString();
			String toDate_value = mAuditTodate_Value_Textview.getText().toString();
			
			if (fromDate_value != null && !fromDate_value.equals("") && toDate_value != null && !toDate_value.equals("")) {

				Intent intent = new Intent(Audit_Date_selectionActivity.this, Audit_TrialBalancesheetActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);
				finish();

			} else {
				TastyToast.makeText(getApplicationContext(), AppStrings.calFromToDateAlert, TastyToast.LENGTH_SHORT, TastyToast.WARNING);
			}
			
			break;

		case R.id.audit_date_selection_fromDateEditText:
			
			audit_trial_balance_check = "1";
			
			calendarDialogShow();
			break;
			
		case R.id.audit_date_selection_toDateEditText:
			
			audit_trial_balance_check = "2";
			
			if (mAuditFromDate_Value_texview.getText().toString().equals("")
					|| mAuditTodate_Value_Textview.getText().toString().equals(null)) {

				TastyToast.makeText(getApplicationContext(), AppStrings.previous_DateAlert, TastyToast.LENGTH_SHORT,
						TastyToast.WARNING);
			} else {
				calendarDialogShow();
			}
			break;
		default:
			break;
		}

	}
	
	private void calendarDialogShow() {
		// TODO Auto-generated method stub
		mLanguageLocale = PrefUtils.getUserlangcode();
		if (mLanguageLocale.equalsIgnoreCase("English")) {
			locale = new Locale("en");
		} else if (mLanguageLocale.equalsIgnoreCase("Tamil")) {
			locale = new Locale("ta");
		} else if (mLanguageLocale.equalsIgnoreCase("Hindi")) {
			locale = new Locale("hi");
		} else if (mLanguageLocale.equalsIgnoreCase("Marathi")) {
			locale = new Locale("ma");
		} else if (mLanguageLocale.equalsIgnoreCase("Malayalam")) {
			locale = new Locale("ml");
		} else if (mLanguageLocale.equalsIgnoreCase("Kannada")) {
			locale = new Locale("kn");
		} else if (mLanguageLocale.equalsIgnoreCase("Bengali")) {
			locale = new Locale("bn");
		} else if (mLanguageLocale.equalsIgnoreCase("Gujarati")) {
			locale = new Locale("gu");
		} else if (mLanguageLocale.equalsIgnoreCase("Punjabi")) {
			locale = new Locale("pa");
		} else if (mLanguageLocale.equalsIgnoreCase("Assamese")) {
			locale = new Locale("as");
		} else {
			locale = new Locale("en");
		}
		locale = new Locale("en");
		Locale.setDefault(locale);
		Configuration config = new Configuration();
		config.locale = locale;
		getApplication().getResources().updateConfiguration(config, getApplication().getResources().getDisplayMetrics());

		OnDateSetListener datelistener = Audit_Date_selectionActivity.this;
		Calendar now = Calendar.getInstance();
		FragmentManager fm = getSupportFragmentManager();
		@SuppressLint("WrongConstant") DatePickerDialog dialog = DatePickerDialog.newInstance(datelistener, now.get(Calendar.YEAR),
				now.get(Calendar.MONDAY), now.get(Calendar.DAY_OF_MONTH));

		SelectedGroupsTask.sBalanceSheetDate_Response = "01/01/2000";
		String balancesheet_date = String.valueOf(SelectedGroupsTask.sBalanceSheetDate_Response);
		String dateArr[] = balancesheet_date.split("/");

		Calendar min_Cal = Calendar.getInstance();

		int day = Integer.parseInt(dateArr[0]);
		int month = Integer.parseInt(dateArr[1]);
		int year = Integer.parseInt(dateArr[2]);

		min_Cal.set(year, (month - 1), day);
		dialog.setMinDate(min_Cal);
		dialog.setMaxDate(now);

		dialog.setMaxDate(now);

		dialog.show(fm, "");
	}

	@Override
	public void onDateSet(DatePickerDialog view, int year, int monthOfYear,
			int dayOfMonth) {
		// TODO Auto-generated method stub
		
	}


}
