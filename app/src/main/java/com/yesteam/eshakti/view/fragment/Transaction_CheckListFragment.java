package com.yesteam.eshakti.view.fragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.oasys.eshakti.digitization.EShaktiApplication;

import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.CheckBox;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Transaction_CheckListFragment extends Fragment implements OnClickListener {

	public static final String TAG = Transaction_CheckListFragment.class.getSimpleName();
	private TextView mGroupName, mCashInHand, mCashAtBank, mHeader;

	private int mSize;
	public static Vector<String> mCheckListVector, mCheckList_StatucVector;
	private static List<CheckBox> mCheckBoxFields = new ArrayList<CheckBox>();

	public Transaction_CheckListFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_CHECKLIST;

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		mCheckListVector = new Vector<String>();
		mCheckList_StatucVector = new Vector<String>();

		mCheckListVector.clear();
		mCheckList_StatucVector.clear();
		mCheckBoxFields.clear();

		if (publicValues.mCheckListWebserviceValues != null) {
			String[] mCheckListArray_values = publicValues.mCheckListWebserviceValues.split("#");

			for (int i = 0; i < mCheckListArray_values.length; i++) {
				String[] mTempValues = mCheckListArray_values[i].split("~");
				mCheckListVector.addElement(mTempValues[0]);
				mCheckList_StatucVector.addElement(mTempValues[1]);
			}

		}

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_check_list, container, false);

		try {

			try {
				mGroupName = (TextView) rootView.findViewById(R.id.groupname);
				mGroupName.setText(
						RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
				mGroupName.setTypeface(LoginActivity.sTypeface);

				mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
				mCashInHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
						+ SelectedGroupsTask.sCashinHand);
				mCashInHand.setTypeface(LoginActivity.sTypeface);

				mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
				mCashAtBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
						+ SelectedGroupsTask.sCashatBank);
				mCashAtBank.setTypeface(LoginActivity.sTypeface);

				mHeader = (TextView) rootView.findViewById(R.id.checkList_fragmentHeader);
				mHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.mCheckList));
				mHeader.setTypeface(LoginActivity.sTypeface);

				TableLayout checkListTable = (TableLayout) rootView.findViewById(R.id.checkListTable);

				mSize = mCheckListVector.size();

				for (int i = 0; i < mSize; i++) {

					TableRow checkListRow = new TableRow(getActivity());

					TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
							LayoutParams.WRAP_CONTENT, 1f);
					contentParams.setMargins(10, 5, 10, 5);

					TextView check_list_text = new TextView(getActivity());
					check_list_text.setText(GetSpanText.getSpanString(getActivity(),
							String.valueOf(mCheckListVector.elementAt(i).toString())));
					check_list_text.setTypeface(LoginActivity.sTypeface);
					check_list_text.setTextColor(color.black);
					check_list_text.setPadding(25, 0, 5, 0);
					check_list_text.setLayoutParams(contentParams);
					checkListRow.addView(check_list_text);

					TableRow.LayoutParams contentParams1 = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
							LayoutParams.WRAP_CONTENT, 1f);
					contentParams1.setMargins(30, 5, 10, 5);

					CheckBox sCheckBox = new CheckBox(getActivity());
					sCheckBox.setId(i);
					mCheckBoxFields.add(sCheckBox);
					sCheckBox.setBackgroundResource(R.drawable.btn_check_to_off_mtrl_013);
					sCheckBox.setClickable(false);
					sCheckBox.setLayoutParams(contentParams1);
					if (mCheckList_StatucVector.elementAt(i).equals("No")) {
						sCheckBox.setChecked(false);
					} else if (mCheckList_StatucVector.elementAt(i).equals("Yes")) {
						sCheckBox.setChecked(true);
					}
					sCheckBox.setGravity(Gravity.LEFT);
					checkListRow.addView(sCheckBox);

					checkListTable.addView(checkListRow,
							new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

				}
			//	publicValues.mCheckListWebserviceValues = null;

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return rootView;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}
}
