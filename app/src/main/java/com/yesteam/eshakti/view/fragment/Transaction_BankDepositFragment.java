package com.yesteam.eshakti.view.fragment;

import java.util.ArrayList;
import java.util.List;

import com.oasys.eshakti.digitization.R;
import com.squareup.otto.Subscribe;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.adapter.CustomListAdapter;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.model.ListItem;
import com.yesteam.eshakti.model.RowItem;
import com.yesteam.eshakti.sqlite.database.response.GetLoanAccountDataResponse;
import com.yesteam.eshakti.sqlite.db.model.GetGroupMemberDetails;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

public class Transaction_BankDepositFragment extends Fragment implements OnItemClickListener, TaskListener {

	private TextView mGroupName, mCashInHand, mCashAtBank;
	public static String sSendToServer_BankName, sSelected_BankName, sSelected_BankDepositAmount;
	public static String bankName;

	List<RowItem> rowItems;

	int size;

	private ListView mListView;
	private List<ListItem> listItems;
	private CustomListAdapter mAdapter;
	int listImage;

	List<RowItem> rowItems_loanacc;

	int size_loanacc;

	private ListView mListView_loanacc;
	private List<ListItem> listItems_loanacc;
	private CustomListAdapter mAdapter_loanacc;
	int listImage_loanacc;
	private Dialog mProgressDilaog;
	TextView mSavingaccTextview, mLoanaccTextview;

	ArrayList<String> mBankName = new ArrayList<String>();
	ArrayList<String> mBankNameSendtoServer = new ArrayList<String>();
	ArrayList<String> mLoanId = new ArrayList<String>();
	ArrayList<String> mLoanType = new ArrayList<String>();
	ArrayList<String> mFixedDeposit = new ArrayList<String>();

	public Transaction_BankDepositFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_BANKDEPOSIT;
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		SBus.INST.register(this);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_banktransaction_menulist, container, false);

		MainFragment_Dashboard.isBackpressed = false;
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_BANKDEPOSIT;

		try {
			/*if (ConnectionUtils.isNetworkAvailable(getActivity())) {

				if (PrefUtils.getGroupMasterResValues() != null && PrefUtils.getGroupMasterResValues().equals("1")) {

					EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GET_LOANACCOUNT,
							new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));

				} else {

//					new Get_GL_Fixed_Bank_Webservice(Transaction_BankDepositFragment.this).execute();
				}
			} else {
				EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GET_LOANACCOUNT,
						new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));
			}*/
			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashInHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashInHand.setTypeface(LoginActivity.sTypeface);

			mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashAtBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashAtBank.setTypeface(LoginActivity.sTypeface);

			System.out.println("BankNames length:>>>" + SelectedGroupsTask.sBankNames.size());

			String[] bankNames = new String[SelectedGroupsTask.sBankNames.size()];
			size = bankNames.length;

			listItems = new ArrayList<ListItem>();
			mListView = (ListView) rootView.findViewById(R.id.fragment_List);
			listImage = R.drawable.ic_navigate_next_white_24dp;
			/* for checking */

			listItems_loanacc = new ArrayList<ListItem>();
			mListView_loanacc = (ListView) rootView.findViewById(R.id.fragment_List_loanacc);
			listImage_loanacc = R.drawable.ic_navigate_next_white_24dp;

			mSavingaccTextview = (TextView) rootView.findViewById(R.id.savingsaccheader);
			mLoanaccTextview = (TextView) rootView.findViewById(R.id.loanaccheader);
			mSavingaccTextview.setTypeface(LoginActivity.sTypeface);
			mLoanaccTextview.setTypeface(LoginActivity.sTypeface);

			mSavingaccTextview.setText(
					RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.mBankTransSavingsAccount)));
			mLoanaccTextview.setText(
					RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.mBankTransLoanAccount)));
			//
			for (int i = 0; i < size; i++) {
				if (!SelectedGroupsTask.sBankNames.elementAt(i).toString().equals("")
						&& !SelectedGroupsTask.sBankNames.elementAt(i).toString().equals("-")) {
					ListItem rowItem = new ListItem(SelectedGroupsTask.sBankNames.elementAt(i).toString(), listImage);
					listItems.add(rowItem);
				}

			}

			mAdapter = new CustomListAdapter(getActivity(), listItems);
			mListView.setAdapter(mAdapter);
			mListView.setOnItemClickListener(this);

			mListView_loanacc.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					// TODO Auto-generated method stub
					TextView textColor_Change = (TextView) view.findViewById(R.id.dynamicText);
					textColor_Change.setText(String.valueOf(mBankName.get(position)));
					textColor_Change.setTextColor(Color.rgb(251, 161, 108));

					EShaktiApplication.setLoanaccBankName(mBankName.get(position));
					EShaktiApplication.setLoanaccBankNameSendtoServer(mBankNameSendtoServer.get(position));
					EShaktiApplication.setLoanaccLoanId(mLoanId.get(position));
					EShaktiApplication.setLoanaccLoanType(mLoanType.get(position));
					EShaktiApplication.setLoanaccFixedDeposit(mFixedDeposit.get(position));

					Transaction_LoanAccountFragment accountFragment = new Transaction_LoanAccountFragment();
					setFragment(accountFragment);
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}

		return rootView;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		// TODO Auto-generated method stub

		try {

			TextView textColor_Change = (TextView) view.findViewById(R.id.dynamicText);
			textColor_Change.setText(String.valueOf(SelectedGroupsTask.sBankNames.elementAt(position)));
			textColor_Change.setTextColor(Color.rgb(251, 161, 108));

			bankName = SelectedGroupsTask.sEngBankNames.elementAt(position).toString();
			System.out.println("Bank Name:>>>>" + bankName);

			for (int i = 0; i < size; i++) {

				if (bankName.equals(SelectedGroupsTask.sEngBankNames.elementAt(i).toString())) {

					sSendToServer_BankName = SelectedGroupsTask.sEngBankNames.elementAt(i).toString();

					sSelected_BankName = SelectedGroupsTask.sBankNames.elementAt(i).toString();

					sSelected_BankDepositAmount = SelectedGroupsTask.sBankAmt.elementAt(i).toString();

					Fragment fragment = new Transaction_DepositMenuFragment();
					setFragment(fragment);
					EShaktiApplication.setAcctoaccSelectBank(sSelected_BankName);
					EShaktiApplication.setAcctoaccSendtoserverBank(sSendToServer_BankName);

				}
			}

			System.out.println("Send_To_Server value:>>" + sSendToServer_BankName);
			System.out.println("Selected BankName:>>>" + sSelected_BankName);
			System.out.println("Selected BankDeposit:>>>" + sSelected_BankDepositAmount);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub
		mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
		mProgressDilaog.show();
	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub

		if (mProgressDilaog != null) {
			if (mProgressDilaog.isShowing()) {
				mProgressDilaog.dismiss();
			}

			if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {
				getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub

						if (!result.equals("FAIL")) {

							TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg,
									TastyToast.LENGTH_SHORT, TastyToast.ERROR);
							Constants.NETWORKCOMMONFLAG = "SUCCESS";
						}

					}
				});

			} else {
				try {
					if (publicValues.mLoanAccBankListWebservices != null
							&& !(publicValues.mLoanAccBankListWebservices.isEmpty())) {
						String[] arrValues = publicValues.mLoanAccBankListWebservices.split("#");

						mBankName.clear();
						mBankNameSendtoServer.clear();
						mLoanId.clear();
						mLoanType.clear();
						mFixedDeposit.clear();

						for (int i = 0; i < arrValues.length; i++) {
							String ValuesSplit = arrValues[i];

							String[] arrValuesSplit = ValuesSplit.split("~");
							if (arrValuesSplit.length != 0) {

								mBankName.add(arrValuesSplit[0]);
								mBankNameSendtoServer.add(arrValuesSplit[1]);
								mLoanId.add(arrValuesSplit[2]);
								mLoanType.add(arrValuesSplit[3]);
								mFixedDeposit.add(arrValuesSplit[4]);

								ListItem rowItem = new ListItem(mBankName.get(i).toString() + " - " + mLoanType.get(i),
										listImage_loanacc);
								listItems_loanacc.add(rowItem);

							}
						}

						mAdapter_loanacc = new CustomListAdapter(getActivity(), listItems_loanacc);
						mListView_loanacc.setAdapter(mAdapter_loanacc);

					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		}
	}

	@Subscribe
	public void OnGetLoanAccountResponse(final GetLoanAccountDataResponse accountDataResponse) {
		switch (accountDataResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), accountDataResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), accountDataResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			String mGetLoanBankResponse = GetGroupMemberDetails.getLoanaccbankdetails();

			if (mGetLoanBankResponse != null && !mGetLoanBankResponse.equals("")) {
				publicValues.mLoanAccBankListWebservices = mGetLoanBankResponse;
				String[] arrValues = publicValues.mLoanAccBankListWebservices.split("#");
				Log.e("Loan Acc Transfer-->>", publicValues.mLoanAccBankListWebservices);

				mBankName.clear();
				mBankNameSendtoServer.clear();
				mLoanId.clear();
				mLoanType.clear();
				mFixedDeposit.clear();

				for (int i = 0; i < arrValues.length; i++) {
					String ValuesSplit = arrValues[i];

					String[] arrValuesSplit = ValuesSplit.split("~");

					if (arrValuesSplit.length != 0 && arrValuesSplit.length == 5) {
						mBankName.add(arrValuesSplit[0]);
						mBankNameSendtoServer.add(arrValuesSplit[1]);
						mLoanId.add(arrValuesSplit[2]);
						mLoanType.add(arrValuesSplit[3]);

						mFixedDeposit.add(arrValuesSplit[4]);

						ListItem rowItem = new ListItem(mBankName.get(i).toString() + " - " + mLoanType.get(i),
								listImage_loanacc);
						listItems_loanacc.add(rowItem);
					}

				}

				mAdapter_loanacc = new CustomListAdapter(getActivity(), listItems_loanacc);
				mListView_loanacc.setAdapter(mAdapter_loanacc);

			} else {

				TastyToast.makeText(getActivity(), AppStrings.loginAgainAlert, TastyToast.LENGTH_LONG,
						TastyToast.WARNING);
			}
			break;
		}

	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		SBus.INST.unRegister(this);
	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub
		Log.e("Current Class Name!!!!!!!!!!!!!!", fragment.getClass().getName());
		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

	}

}
