package com.yesteam.eshakti.view.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.core.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.oasys.eshakti.digitization.Dto.LoginDto;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.squareup.otto.Subscribe;
import com.tutorialsee.lib.TastyToast;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.publicValues;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.service.GPSTracker;
import com.yesteam.eshakti.service.GroupDetailsAddService;
import com.yesteam.eshakti.service.GroupDetailsService;
import com.yesteam.eshakti.service.MemberAadharCardService;
import com.yesteam.eshakti.sqlite.database.response.GroupMasterResponse;
import com.yesteam.eshakti.sqlite.database.response.LoginCheckResponse;
import com.yesteam.eshakti.sqlite.database.response.LoginDeleteResponse;
import com.yesteam.eshakti.sqlite.database.response.LoginResponse;
import com.yesteam.eshakti.sqlite.database.response.OfflineTransDeleteResponse;
import com.yesteam.eshakti.sqlite.db.model.GetGroupMemberDetails;
import com.yesteam.eshakti.sqlite.db.model.GroupMasterSingle;
import com.yesteam.eshakti.sqlite.db.model.Login;
import com.yesteam.eshakti.sqlite.db.model.LoginCheck;
import com.yesteam.eshakti.sqlite.db.model.Transaction;
import com.yesteam.eshakti.sqlite.db.transactions.TransactionManager.DataType;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.CalculateDateUtils;
import com.yesteam.eshakti.utils.Configuration;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.utils.GetExit;
import com.yesteam.eshakti.utils.GetSqliteDBUtils;
import com.yesteam.eshakti.utils.GetTypeface;
import com.yesteam.eshakti.utils.Get_UniqueID;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.utils.RegionalserviceUtil;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.views.ButtonFlat;
import com.yesteam.eshakti.views.RaisedButton;
import com.yesteam.eshakti.webservices.GetMessageNotificationWebservice;
import com.yesteam.eshakti.webservices.Get_Edit_OpeningbalanceWebservice;
import com.yesteam.eshakti.webservices.LoginTask;
import com.yesteam.eshakti.webservices.Login_webserviceTask;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class LoginActivity extends BaseActivity implements OnClickListener, TaskListener,NewTaskListener {

	public static final String TAG = LoginActivity.class.getSimpleName();

	public static String sLatitude = "", sLongitude = "";

	public static Typeface sTypeface;

	public static String sUName = null, sPwd = null;
	public static String tempStrValue;

	RaisedButton mRaisedLoginButton;
	EditText mUsername, mPassword;

	Dialog mProgressDialog, mProgressDialog_thread;

	public static String mLanguageLocalae = "";

	boolean isNavigate;
	int userName_Length;

	Handler handler = new Handler();
	public static boolean iSOfflineDataAvail = false;

	TextView mCreateaccount, mSignUpdiffuser;

	int mDaycount = 0;
	boolean isStop = false;
	Intent intent_login;
	boolean isDialogChecking = false;
	TextView mNewUser;
	boolean isFirstTimeLogin_no = false;
	// GPSTracker class
	GPSTracker gps;
	boolean isNavigateEditOpeningBalanceActivity = false;
	public static int mVersion = 0;
	boolean iSServiceCall = false;
	boolean isGetLoginValues = false;
	boolean isGetMessageService = false;
	public static String mVersionName = null;
	static int REQUEST_CODE_SOME_FEATURES_PERMISSIONS = 100;
	public static String _VersionCode = null, _VersionName = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_login);
		_VersionCode = null;
		_VersionName = null;
		mVersion = 0;
		mVersionName = null;

		iSOfflineDataAvail = false;
		PrefUtils.setLoginValues(null);

		Log.e("LOGIN ACTIVITY", "DISPLAYS");
		PrefUtils.setLoginGroupService(null);

		EShaktiApplication.setLoginFlag(null);

		if (PrefUtils.getAnimatorOfflineDataValues() == null) {
			PrefUtils.clearGroup_Offlinedata();
		}

		GetSqliteDBUtils.copy_SqliteDB(getApplicationContext());
		mProgressDialog_thread = AppDialogUtils.createProgressDialog(this);
		try {

			if (PrefUtils.getUserlangcode() != null) {
				mLanguageLocalae = PrefUtils.getUserlangcode();
			} else {
				mLanguageLocalae = null;
			}

			sTypeface = GetTypeface.getTypeface(getApplicationContext(), mLanguageLocalae);

		} catch (Exception e) {
			e.printStackTrace();
		}

		initializeview();
		if (ConnectionUtils.isNetworkAvailable(getApplicationContext())) {
			EShaktiApplication.setOfflineTrans(true);
			EShaktiApplication.getInstance().getTransactionManager()
					.startTransaction(DataType.GET_OFFLINE_MASTER_TRANSACTION, null);
		}
		PackageManager manager = getPackageManager();
		PackageInfo info;
		try {
			info = manager.getPackageInfo(getPackageName(), 0);
			mVersion = info.versionCode;
			mVersionName = info.versionName;
			_VersionCode = String.valueOf(mVersion);
			_VersionName = String.valueOf(mVersionName);
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (ConnectionUtils.isNetworkAvailable(getApplicationContext())) {
			isGetMessageService = true;

			/** UNDER TESTING **/
			if (PrefUtils.getUserlangcode() != null) {
				String mStrLang = PrefUtils.getUserlangcode();
				RegionalserviceUtil.getRegionalService(getApplicationContext(), mStrLang);
			} else {

				RegionalserviceUtil.getRegionalService(getApplicationContext(), "English");
			}
			new GetMessageNotificationWebservice(LoginActivity.this).execute();

		}

	}

	@Subscribe
	public void onGetOfflineDataResponse(ArrayList<Transaction> mOfflineTransList_) {
		if (!isGetLoginValues) {
			if (!EShaktiApplication.isOfflineAadharcardPhot()) {

				if (ConnectionUtils.isNetworkAvailable(getApplicationContext())) {
					Log.e("!!!!!!!!!!", "Yesssssssss");
					EShaktiApplication.setOfflineTrans(false);
					if (mOfflineTransList_ != null && mOfflineTransList_.size() > 0
							&& mOfflineTransList_.get(0).getUsername() != null) {
						iSOfflineDataAvail = true;

					} else {
						iSOfflineDataAvail = false;

						Intent intentservice_aadharCard = new Intent(LoginActivity.this, MemberAadharCardService.class);
						startService(intentservice_aadharCard);
					}
				} else {
					if (!isStop) {

						EShaktiApplication.setOfflineTrans(false);

						if (mOfflineTransList_ != null && mOfflineTransList_.size() > 0
								&& mOfflineTransList_.size() != 0) {
							Log.v("Offline Data Size", mOfflineTransList_.size() + "");
							if (mOfflineTransList_.size() == 0) {
								Log.v("Offline Datas", "No Avail");

							} else {

								String mLastTransDate = mOfflineTransList_.get(mOfflineTransList_.size() - 1)
										.getTransactionDate();
								Calendar c = Calendar.getInstance();
								System.out.println("Current time => " + c.getTime());

								SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
								String formattedDate = df.format(c.getTime());

								Log.v("Current Date", formattedDate);
								Log.v("Last Trans Date", mLastTransDate);

								try {
									String days = CalculateDateUtils.get_count_of_days(mLastTransDate, formattedDate);
									Log.e("Total Days", days);
									mDaycount = Integer.parseInt(days);

								} catch (Exception e) {
								}
							}
						}

					}
				}
			}
		}
	}

	@SuppressLint("ClickableViewAccessibility")
	private void initializeview() {

		try {

			mUsername = (EditText) findViewById(R.id.activity_username_edittext);
			System.out.println("PREF USERNAME " + PrefUtils.getUsernameKey());
			if (PrefUtils.getUsernameKey() != null) {
				System.out.println("User name Pref");
				mUsername.setText(PrefUtils.getUsernameKey());
				mUsername.setEnabled(false);
				mUsername.setBackground(null);
			} else {
				System.out.println("User name No Pref");
				mUsername.setHint(RegionalConversion.getRegionalConversion(AppStrings.userName));
				mUsername.setTypeface(sTypeface);
			}
			mUsername.setHintTextColor(Color.LTGRAY);
			mUsername.addTextChangedListener(username_watcher);

			mPassword = (EditText) findViewById(R.id.activity_password_edittext);
			mPassword.setHint(RegionalConversion.getRegionalConversion(AppStrings.passWord));
			mPassword.setTypeface(sTypeface);
			mPassword.setHintTextColor(Color.LTGRAY);

			mPassword.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub

					mPassword.setHint(null);
					mPassword.setTypeface(null);
					return false;
				}
			});
			mPassword.addTextChangedListener(watch);

			mRaisedLoginButton = (RaisedButton) findViewById(R.id.activity_login_button);
			mRaisedLoginButton.setText(RegionalConversion.getRegionalConversion(AppStrings.submit));
			mRaisedLoginButton.setTypeface(sTypeface);

			mRaisedLoginButton.setOnClickListener(this);

			mSignUpdiffuser = (TextView) findViewById(R.id.activity_signin_diff);
			mSignUpdiffuser.setText(RegionalConversion.getRegionalConversion(AppStrings.signInAsDiffUser));
			mSignUpdiffuser.setTypeface(LoginActivity.sTypeface, Typeface.BOLD);

			mSignUpdiffuser.setOnClickListener(this);

			try {
				PackageManager manager = getPackageManager();
				PackageInfo info = manager.getPackageInfo(getPackageName(), 0);
				String version = info.versionName;

				mNewUser = (TextView) findViewById(R.id.activity_signup);
				mNewUser.setText(RegionalConversion.getRegionalConversion(AppStrings.mNewUserSignup));
				mNewUser.setTypeface(LoginActivity.sTypeface);
				mNewUser.setVisibility(View.VISIBLE);
				mNewUser.setText("VERSION   " + version);
				mNewUser.setTextSize(15);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			mUsername.setOnFocusChangeListener(new OnFocusChangeListener() {

				@Override
				public void onFocusChange(View v, boolean hasFocus) {
					// TODO Auto-generated method stub
					if (hasFocus && PrefUtils.getUsernameKey() == null) {
						mUsername.setCompoundDrawablesWithIntrinsicBounds(R.drawable.usericon_focus, 0, 0, 0);
						mPassword.setCompoundDrawablesWithIntrinsicBounds(R.drawable.passicon, 0, 0, 0);
					}

				}
			});
			mPassword.setOnFocusChangeListener(new OnFocusChangeListener() {

				@Override
				public void onFocusChange(View v, boolean hasFocus) {
					// TODO Auto-generated method stub
					if (hasFocus) {

						mPassword.setCompoundDrawablesWithIntrinsicBounds(R.drawable.passicon_focus, 0, 0, 0);
						mUsername.setCompoundDrawablesWithIntrinsicBounds(R.drawable.usericon, 0, 0, 0);
					}

				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	TextWatcher username_watcher = new TextWatcher() {

		@Override
		public void afterTextChanged(Editable edit) {

			if (edit.length() == 10) {
				mPassword.requestFocus();
				mPassword.setCursorVisible(true);
			} else {

			}
		}

		@Override
		public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onTextChanged(CharSequence s, int a, int b, int c) {
			// TODO Auto-generated method stub

		}
	};
	TextWatcher watch = new TextWatcher() {

		@Override
		public void afterTextChanged(Editable edit) {

			if (edit.length() == 4) {
				Configuration.hideSoftKeyboard(getApplicationContext(), mPassword);
				LoginValidation();

			} else {
				Log.v("Edit Text Length", edit.length() + "");
			}
		}

		@Override
		public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onTextChanged(CharSequence s, int a, int b, int c) {
			// TODO Auto-generated method stub

		}
	};

	private void LoginValidation() {
		// TODO Auto-generated method stub

		Log.d(TAG, String.valueOf(EShaktiApplication.isAgent));

		System.out.println("--------IMEI NO ------------" + EShaktiApplication.getIMEI_NO());

		sUName = mUsername.getText().toString();
		sPwd = mPassword.getText().toString();
		userName_Length = sUName.length();

		/** Hardcoded the userName length temproarily **/
		// userName_Length = 10;

		/* if (userName_Length == 10) { */

		Log.v(TAG, Boolean.valueOf(EShaktiApplication.isAgent) + "   Len : " + String.valueOf(userName_Length));

		if (ConnectionUtils.isNetworkAvailable(getApplicationContext())) {
			if (PrefUtils.getUsernameKey() != null && !(PrefUtils.getUsernameKey().equals(sUName))
					&& iSOfflineDataAvail) {
				TastyToast.makeText(getApplicationContext(),
						"Offline Transactions Available, so Login" + PrefUtils.getUsernameKey() + "  Username",
						TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			} else {
				Log.d(TAG, sUName + "   " + sPwd + "  CHECK  len " + String.valueOf(userName_Length));
				if (((sUName == null) || (sPwd == null) || (sUName.equals("")) || (sPwd.equals("")))
						&& (userName_Length != 10)) {
					Log.d(TAG, sUName + "   " + sPwd + "    len " + String.valueOf(userName_Length));
					TastyToast.makeText(getApplicationContext(), AppStrings.loginUserAlert, TastyToast.LENGTH_SHORT,
							TastyToast.WARNING);

				} else if ((sUName != null) && (sPwd != null) && (!sUName.isEmpty()) && (!sPwd.isEmpty())) { // (sUName.equals("1000002"))
					// &&
					PrefUtils.setUserNameKey(sUName);
					Log.d(TAG + "NOT NULL", sUName + "   " + sPwd);

					/** UNDER TESTING **/
					if (PrefUtils.getUserlangcode() != null) {
						String mStrLang = PrefUtils.getUserlangcode();
						RegionalserviceUtil.getRegionalService(getApplicationContext(), mStrLang);
					} else {

						RegionalserviceUtil.getRegionalService(getApplicationContext(), "English");
					}
					Log.d(TAG, LoginTask.NAMESPACE + "<<<< " + LoginTask.SOAP_ADDRESS);
					try {

						double latitude = gps.getLatitude();
						double longitude = gps.getLongitude();

						sLatitude = String.valueOf(latitude);
						sLongitude = String.valueOf(longitude);
						if (PrefUtils.getLoginValues() != null && PrefUtils.getLoginValues().equals("1")) {

							PrefUtils.setUserNameKey(sUName);

							if (PrefUtils.getAppUsertype() != null && PrefUtils.getAppUsertype().equals("ANIMATOR")) {
								PrefUtils.setAgentUserName(sUName);
								EShaktiApplication.isAgent = true;
								EShaktiApplication.setmUserType("AGENT");
							} else if (PrefUtils.getAppUsertype() != null && PrefUtils.getAppUsertype().equals("SHG")) {
								EShaktiApplication.setmUserType("GROUP");
								EShaktiApplication.isAgent = false;
								PrefUtils.setGroupUserName(sUName);

							}
							isGetLoginValues = true;
							EShaktiApplication.getInstance().getTransactionManager()
									.startTransaction(DataType.LOGIN_CHECK, new LoginCheck(sUName));
						} else {
							if (!iSServiceCall) {
								iSServiceCall = true;

//								new Login_webserviceTask(LoginActivity.this).execute();

								LoginDto dto = new LoginDto();
								dto.setUsername(String.valueOf(LoginActivity.sUName));
								//   dto.setUsername("testAnimator");
								dto.setPassword(String.valueOf(LoginActivity.sPwd));

								if (dto == null) {
									return;
								}

								String loginReqJson = new Gson().toJson(dto);
								RestClient.getRestClient(LoginActivity.this).callRestWebService(com.oasys.eshakti.digitization.OasysUtils.Constants.BASE_URL+ com.oasys.eshakti.digitization.OasysUtils.Constants.LOGIN_URL, loginReqJson, LoginActivity.this, ServiceType.LOGIN);
//													new Login_webserviceTask(LoginActivity.this).execute();
								isNavigate = false;
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}

				} else {

					TastyToast.makeText(getApplicationContext(), AppStrings.loginUserAlert, TastyToast.LENGTH_SHORT,
							TastyToast.WARNING);
				}
			}

		} else {

			// ********** OFFLINE SINPPETS ***********//

			if ((sUName == null) || (sPwd == null) || (sUName.equals("")) || (sPwd.equals(""))) {
				Log.d(TAG, sUName + "   " + sPwd);
				TastyToast.makeText(getApplicationContext(), AppStrings.loginUserAlert, TastyToast.LENGTH_SHORT,
						TastyToast.WARNING);
			} else if ((sUName != null) && (sPwd != null) && (!sUName.isEmpty()) && (!sPwd.isEmpty())) {
				PrefUtils.setUserNameKey(sUName);
				if (PrefUtils.getAppUsertype() != null && PrefUtils.getAppUsertype().equals("ANIMATOR")) {
					PrefUtils.setAgentUserName(sUName);
					EShaktiApplication.isAgent = true;
					EShaktiApplication.setmUserType("AGENT");
				} else if (PrefUtils.getAppUsertype() != null && PrefUtils.getAppUsertype().equals("SHG")) {
					EShaktiApplication.setmUserType("GROUP");
					EShaktiApplication.isAgent = false;
					PrefUtils.setGroupUserName(sUName);

				}
				// AgentLogin

				/**
				 * mDaycount - checks the day count of Offline transaction
				 **/
				if (mDaycount <= 30) {

					try {

						EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.LOGIN_CHECK,
								new LoginCheck(sUName));

					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			}

		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		try {
			switch (v.getId()) {
				case R.id.activity_login_button:

					Log.d(TAG, String.valueOf(EShaktiApplication.isAgent));

					System.out.println("--------IMEI NO ------------" + EShaktiApplication.getIMEI_NO());

					sUName = mUsername.getText().toString();
					sPwd = mPassword.getText().toString();
					userName_Length = sUName.length();

					/** Hardcoded the userName length temproarily **/
					// userName_Length = 10;

					/* if (userName_Length == 10) { */

					Log.v(TAG, Boolean.valueOf(EShaktiApplication.isAgent) + "   Len : " + String.valueOf(userName_Length));

					if (ConnectionUtils.isNetworkAvailable(getApplicationContext())) {

						if (PrefUtils.getUsernameKey() != null && !(PrefUtils.getUsernameKey().equals(sUName))
								&& iSOfflineDataAvail) {

							TastyToast.makeText(getApplicationContext(),
									"Offline Transactions Available, so Login" + PrefUtils.getUsernameKey() + "  Username",
									TastyToast.LENGTH_SHORT, TastyToast.ERROR);
						} else {

							Log.d(TAG, sUName + "   " + sPwd + "  CHECK  len " + String.valueOf(userName_Length));

							if (((sUName == null) || (sPwd == null) || (sUName.equals("")) || (sPwd.equals("")))
									&& (userName_Length != 10)) {

								Log.d(TAG, sUName + "   " + sPwd + "    len " + String.valueOf(userName_Length));

								TastyToast.makeText(getApplicationContext(), AppStrings.loginUserAlert,
										TastyToast.LENGTH_SHORT, TastyToast.WARNING);

							} else if ((sUName != null) && (sPwd != null) && (!sUName.isEmpty()) && (!sPwd.isEmpty())) { // (sUName.equals("1000002"))
								// &&

								PrefUtils.setUserNameKey(sUName);

								Log.d(TAG + "NOT NULL", sUName + "   " + sPwd);

								/** UNDER TESTING **/
								if (PrefUtils.getUserlangcode() != null) {
									String mStrLang = PrefUtils.getUserlangcode();
									RegionalserviceUtil.getRegionalService(getApplicationContext(), mStrLang);
								} else {

									RegionalserviceUtil.getRegionalService(getApplicationContext(), "English");
								}
								Log.d(TAG, LoginTask.NAMESPACE + "<<<< " + LoginTask.SOAP_ADDRESS);
								try {

									double latitude = gps.getLatitude();
									double longitude = gps.getLongitude();

									sLatitude = String.valueOf(latitude);
									sLongitude = String.valueOf(longitude);
									if (PrefUtils.getLoginValues() != null && PrefUtils.getLoginValues().equals("1")) {

										PrefUtils.setUserNameKey(sUName);

										if (PrefUtils.getAppUsertype() != null
												&& PrefUtils.getAppUsertype().equals("ANIMATOR")) {
											PrefUtils.setAgentUserName(sUName);
											EShaktiApplication.isAgent = true;
											EShaktiApplication.setmUserType("AGENT");
										} else if (PrefUtils.getAppUsertype() != null
												&& PrefUtils.getAppUsertype().equals("SHG")) {
											EShaktiApplication.setmUserType("GROUP");
											EShaktiApplication.isAgent = false;
											PrefUtils.setGroupUserName(sUName);

										}
										isGetLoginValues = true;
										EShaktiApplication.getInstance().getTransactionManager()
												.startTransaction(DataType.LOGIN_CHECK, new LoginCheck(sUName));
									} else {

											LoginDto dto = new LoginDto();
											dto.setUsername(String.valueOf(LoginActivity.sUName));
											//   dto.setUsername("testAnimator");
											dto.setPassword(String.valueOf(LoginActivity.sPwd));

											if (dto == null) {
												return;
											}

											String loginReqJson = new Gson().toJson(dto);
											RestClient.getRestClient(LoginActivity.this).callRestWebService(com.oasys.eshakti.digitization.OasysUtils.Constants.BASE_URL + com.oasys.eshakti.digitization.OasysUtils.Constants.LOGIN_URL, loginReqJson, LoginActivity.this, ServiceType.LOGIN);

//											new Login_webserviceTask(LoginActivity.this).execute();


									}
								} catch (Exception e) {
									e.printStackTrace();
								}

							} else {

								TastyToast.makeText(getApplicationContext(), AppStrings.loginUserAlert,
										TastyToast.LENGTH_SHORT, TastyToast.WARNING);
							}
						}

					} else {

						// ********** OFFLINE SINPPETS ***********//

						if ((sUName == null) || (sPwd == null) || (sUName.equals("")) || (sPwd.equals(""))) {

							Log.d(TAG, sUName + "   " + sPwd);

							TastyToast.makeText(getApplicationContext(), AppStrings.loginUserAlert, TastyToast.LENGTH_SHORT,
									TastyToast.WARNING);

						} else if ((sUName != null) && (sPwd != null) && (!sUName.isEmpty()) && (!sPwd.isEmpty())) {

							PrefUtils.setUserNameKey(sUName);

							if (PrefUtils.getAppUsertype() != null && PrefUtils.getAppUsertype().equals("ANIMATOR")) {
								PrefUtils.setAgentUserName(sUName);
								EShaktiApplication.isAgent = true;
								EShaktiApplication.setmUserType("AGENT");
							} else if (PrefUtils.getAppUsertype() != null && PrefUtils.getAppUsertype().equals("SHG")) {
								EShaktiApplication.setmUserType("GROUP");
								EShaktiApplication.isAgent = false;
								PrefUtils.setGroupUserName(sUName);

							}
							// AgentLogin

							/**
							 * mDaycount - checks the day count of Offline transaction
							 **/
							if (mDaycount <= 30) {

								try {

									EShaktiApplication.getInstance().getTransactionManager()
											.startTransaction(DataType.LOGIN_CHECK, new LoginCheck(sUName));

								} catch (Exception e) {
									e.printStackTrace();
								}

							}
						}

					}

					break;

				case R.id.activity_signin_diff:
					if (ConnectionUtils.isNetworkAvailable(getApplicationContext())) {

						PrefUtils.clearUserLanguageCode();
						PrefUtils.clearAddGroupFlag();

						if (iSOfflineDataAvail) {
							AppDialogUtils.showConfirmationDialog(this, "SIGNUPDIFF");
						} else {

							AppDialogUtils.showConfirmationDialog(this, "SIGNUPDIFF_NOENTRY");

						}
					} else {

						TastyToast.makeText(getApplicationContext(), AppStrings.mCommonNetworkErrorMsg,
								TastyToast.LENGTH_SHORT, TastyToast.ERROR);
					}
					break;
				case R.id.activity_signup:

					if (ConnectionUtils.isNetworkAvailable(getApplicationContext())) {
						Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
						overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);
						finish();
					} else {

					}
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	@Override
	protected void onResume() {

		super.onResume();
	}

	@Override
	protected void onPause() {

		super.onPause();
	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub

		try {
			if (!isDialogChecking) {
				mProgressDialog = AppDialogUtils.createProgressDialog(this);
				mProgressDialog.show();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onTaskFinished(final String respone, ServiceType serviceType)
	{
		switch (serviceType)
		{
			case LOGIN:
				Log.d("checkingData"," "+respone.toString());
				try {
					JSONObject jsonObject = new JSONObject(respone.toString());
					int statusCode = jsonObject.getInt("statusCode");
					if(statusCode == 200)
					{
						Intent  intent = new Intent(LoginActivity.this,GroupListActivity.class);
						startActivity(intent);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}

		}
	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub

		if (mProgressDialog != null) {
			mProgressDialog.dismiss();
			System.out.println("Task Finished Values:::" + result);
			if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {

				Log.v("Yesssssssssssss", "Progress Dialog Dismissss");
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						if (!result.equals("FAIL")) {
							iSServiceCall = false;

							TastyToast.makeText(getApplicationContext(), AppStrings.mCommonNetworkErrorMsg,
									TastyToast.LENGTH_SHORT, TastyToast.ERROR);

							Constants.NETWORKCOMMONFLAG = "SUCCESS";
						}
					}
				});

			} else {
				if (isGetMessageService) {
					isGetMessageService = false;
					if (GetMessageNotificationWebservice.sGetMessageNotificationWebservice != null) {
						if (!GetMessageNotificationWebservice.sGetMessageNotificationWebservice.equals("No")) {
							AppDialogUtils.showNotificationMessageDialog(this);
						}

					}

				} else {
					if (isNavigateEditOpeningBalanceActivity) {
						if (mVersion < Integer.parseInt(EShaktiApplication.getAppVersionCode())) {
							iSServiceCall = false;
							mUsername.setHint(RegionalConversion.getRegionalConversion(AppStrings.userName));
							mPassword.setHint(RegionalConversion.getRegionalConversion(AppStrings.passWord));
							AppDialogUtils.showAppVersionUpdateDialog(LoginActivity.this, mVersionName);
						} else {
							PrefUtils.setLoginValues("1");

							iSServiceCall = false;
							Intent intent = new Intent(LoginActivity.this, EditOpeningBalanceSavingsActivity.class);
							intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
							intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(intent);
							overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

							finish();
							isNavigateEditOpeningBalanceActivity = false;
						}
					} else {

						if (Login_webserviceTask.sLoginTask_Response.equals("LoginName or password incorrect.")) {

							mProgressDialog.dismiss();
							isFirstTimeLogin_no = false;

							TastyToast.makeText(getApplicationContext(), AppStrings.loginAlert, TastyToast.LENGTH_SHORT,
									TastyToast.ERROR);

							PrefUtils.clearUserId();

							Intent intent_login = new Intent(LoginActivity.this, LoginActivity.class);
							intent_login.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
							startActivity(intent_login);
							overridePendingTransition(R.anim.right_to_left_in, R.anim.right_to_left_out);
							finish();

						} else if (Login_webserviceTask.sLoginTask_Response.equals("No")) {
							String mValues = Login_webserviceTask.mMasterValues;
							String arr[] = mValues.split("!");
							TastyToast.makeText(getApplicationContext(), arr[1].toString(), TastyToast.LENGTH_SHORT,
									TastyToast.ERROR);

						} else {

							if (publicValues.mGetAlertMessageValues != null) {
								String[] mValues = publicValues.mGetAlertMessageValues.split("~");
								if (mValues[0].equals("1")) {
									AppDialogUtils.showAlertMessageDialog(this);
								} else if (mValues[0].equals("0") && !mValues[1].equals("0")
										&& PrefUtils.getUserlangcode() != null) {

									AppDialogUtils.showNotificationMessage_AlertDialog(this);

								} else {

									mProgressDialog.dismiss();
									isFirstTimeLogin_no = false;
									mLanguageLocalae = PrefUtils.getUserlangcode();
									if (EShaktiApplication.isAgent) {

										if (mLanguageLocalae == null) {
											/**
											 * IF THERE IS NO LANGUAGE ON PREFERENCE. (It happens only on the
											 * installation)
											 **/

											EShaktiApplication.getInstance().getTransactionManager()
													.startTransaction(DataType.LOGIN_ADD,
															new Login(0, LoginTask.UserName, LoginTask.Ngo_Id,
																	LoginTask.Trainer_Id, sPwd,
																	LoginTask.User_RegLanguage));

											// Navigates to Language selection

											final Dialog ChangeLanguageDialog = new Dialog(this);

											LayoutInflater li = (LayoutInflater) this
													.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
											final View dialogView = li.inflate(R.layout.dialog_choose_language, null,
													false);

											TextView confirmationHeader = (TextView) dialogView
													.findViewById(R.id.chooseLanguageHeader);
											confirmationHeader.setText(RegionalConversion
													.getRegionalConversion(AppStrings.chooseLanguage));
											confirmationHeader.setTypeface(LoginActivity.sTypeface);
											final RadioGroup radioGroup = (RadioGroup) dialogView
													.findViewById(R.id.radioLanguage);
											RadioButton radioButton = (RadioButton) dialogView
													.findViewById(R.id.radioEnglish);
											RadioButton radioButton_reg = (RadioButton) dialogView
													.findViewById(R.id.radioRegional);
											radioButton.setText("English");
											radioButton.setTypeface(LoginActivity.sTypeface);

											if (LoginTask.User_RegLanguage.equals("Hindi")) {
												radioButton_reg.setText("हिंदी");
												Typeface typeface = Typeface.createFromAsset(getAssets(),
                                                        "font/MANGAL.TTF");
												radioButton_reg.setTypeface(typeface);
											} else if (LoginTask.User_RegLanguage.equals("Marathi")) {
												radioButton_reg.setText("मराठी");
												Typeface typeface = Typeface.createFromAsset(getAssets(),
                                                        "font/MANGALHindiMarathi.TTF");
												radioButton_reg.setTypeface(typeface);
											} else if (LoginTask.User_RegLanguage.equals("Kannada")) {
												radioButton_reg.setText("ಕನ್ನಡ");
												Typeface typeface = Typeface.createFromAsset(getAssets(),
                                                        "font/tungaKannada.ttf");
												radioButton_reg.setTypeface(typeface);
											} else if (LoginTask.User_RegLanguage.equals("Malayalam")) {
												radioButton_reg.setText("മലയാളം");
												Typeface typeface = Typeface.createFromAsset(getAssets(),
                                                        "font/MLKR0nttMalayalam.ttf");
												radioButton_reg.setTypeface(typeface);
											} else if (LoginTask.User_RegLanguage.equals("Punjabi")) {
												radioButton_reg.setText("ਪੰਜਾਬੀ ");
												Typeface typeface = Typeface.createFromAsset(getAssets(),
                                                        "font/mangal-1361510185.ttf");
												radioButton_reg.setTypeface(typeface);
											} else if (LoginTask.User_RegLanguage.equals("Gujarathi")) {
												radioButton_reg.setText("ગુજરાતી");
												Typeface typeface = Typeface.createFromAsset(getAssets(),
                                                        "font/shrutiGujarathi.ttf");
												radioButton_reg.setTypeface(typeface);
											} else if (LoginTask.User_RegLanguage.equals("Bengali")) {
												radioButton_reg.setText("বাঙ্গালী");
												Typeface typeface = Typeface.createFromAsset(getAssets(),
                                                        "font/kalpurushBengali.ttf");
												radioButton_reg.setTypeface(typeface);
											} else if (LoginTask.User_RegLanguage.equals("Tamil")) {
												radioButton_reg.setText("தமிழ்");
												Typeface typeface = Typeface.createFromAsset(getAssets(),
                                                        "font/TSCu_SaiIndira.ttf");
												radioButton_reg.setTypeface(typeface);
											} else if (LoginTask.User_RegLanguage.equals("Assamese")) {
												radioButton_reg.setText("Assamese");
												Typeface typeface = Typeface.createFromAsset(getAssets(),
                                                        "font/KirtanUni_Assamese.ttf");
												radioButton_reg.setTypeface(typeface);
											} else {
												radioButton_reg.setText(RegionalConversion
														.getRegionalConversion(LoginTask.User_RegLanguage));
												Typeface typeface = Typeface.createFromAsset(getAssets(),
                                                        "font/Exo-Medium.ttf");
												radioButton_reg.setTypeface(typeface);
											}
											// radioButton_reg.setTypeface(LoginActivity.sTypeface);

											ButtonFlat okButton = (ButtonFlat) dialogView
													.findViewById(R.id.dialog_yes_button);
											okButton.setText(
													RegionalConversion.getRegionalConversion(AppStrings.dialogOk));
											okButton.setTypeface(LoginActivity.sTypeface);
											okButton.setOnClickListener(new OnClickListener() {

												@Override
												public void onClick(View v) {
													// TODO Auto-generated
													// method
													// stub

													int selectedId = radioGroup.getCheckedRadioButtonId();
													// find the radiobutton by
													// returned id
													RadioButton radioLanguageButton = (RadioButton) dialogView
															.findViewById(selectedId);

													String mSelectedLang = radioLanguageButton.getText().toString();
													Log.v("On Selected language", mSelectedLang);
													try {
														/**
														 * THE LANGUAGE VALUE INSERTS INTO PREFERENCE
														 **/
														if (mSelectedLang.equals("हिंदी")) {
															mSelectedLang = "Hindi";
														} else if (mSelectedLang.equals("मराठी")) {
															mSelectedLang = "Marathi";
														} else if (mSelectedLang.equals("ಕನ್ನಡ")) {
															mSelectedLang = "Kannada";
														} else if (mSelectedLang.equals("മലയാളം")) {
															mSelectedLang = "Malayalam";
														} else if (mSelectedLang.equals("ਪੰਜਾਬੀ")) {
															mSelectedLang = "Punjabi";
														} else if (mSelectedLang.equals("ગુજરાતી")) {
															mSelectedLang = "Gujarathi";
														} else if (mSelectedLang.equals("বাঙ্গালী")) {
															mSelectedLang = "Bengali";
														} else if (mSelectedLang.equals("தமிழ்")) {
															mSelectedLang = "Tamil";
														} else if (mSelectedLang.equals("Assamese")) {
															mSelectedLang = "Assamese";
														}
														PrefUtils.setUserlangcode(mSelectedLang);
													} catch (Exception e) {
														e.printStackTrace();
													}

													LoginActivity.sTypeface = GetTypeface
															.getTypeface(LoginActivity.this, mSelectedLang);

													RegionalserviceUtil.getRegionalService(LoginActivity.this,
															mSelectedLang);

													// isLanguageSelection =
													// true;

													LoginDto dto = new LoginDto();
													dto.setUsername(String.valueOf(LoginActivity.sUName));
													//   dto.setUsername("testAnimator");
													dto.setPassword(String.valueOf(LoginActivity.sPwd));

													if (dto == null) {
														return;
													}

													String loginReqJson = new Gson().toJson(dto);
													RestClient.getRestClient(LoginActivity.this).callRestWebService(com.oasys.eshakti.digitization.OasysUtils.Constants.BASE_URL + com.oasys.eshakti.digitization.OasysUtils.Constants.LOGIN_URL, loginReqJson, LoginActivity.this, ServiceType.LOGIN);
//													new Login_webserviceTask(LoginActivity.this).execute();

													isNavigate = true;
													ChangeLanguageDialog.dismiss();

												}
											});

											ChangeLanguageDialog.getWindow().setBackgroundDrawable(
													new ColorDrawable(Color.TRANSPARENT));
											ChangeLanguageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
											ChangeLanguageDialog.setCanceledOnTouchOutside(false);
											ChangeLanguageDialog.setContentView(dialogView);
											ChangeLanguageDialog.setCancelable(false);
											ChangeLanguageDialog.show();

										} else {

											RegionalserviceUtil.getRegionalService(getApplicationContext(),
													mLanguageLocalae);
											EShaktiApplication.getInstance().getTransactionManager()
													.startTransaction(DataType.LOGINDELETE, null);

											try {
												Thread.sleep(300);
											} catch (InterruptedException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}

											EShaktiApplication.getInstance().getTransactionManager().startTransaction(
													DataType.LOGIN_ADD,
													new Login(0, Login_webserviceTask.UserName,
															Login_webserviceTask.Ngo_Id,
															Login_webserviceTask.Trainer_Id, sPwd,
															Login_webserviceTask.User_RegLanguage));
											mProgressDialog_thread.show();

											Runnable runnable = new Runnable() {
												@Override
												public void run() {
													handler.post(new Runnable() {
														@Override
														public void run() {

															// TODO Auto-generated method
															// stub

															if (PrefUtils.getAddGroupFlag() == null) {
																EShaktiApplication.getInstance().getTransactionManager()
																		.startTransaction(DataType.GROUPNAMEDELETE,
																				null);

																try {
																	Thread.sleep(500);
																} catch (InterruptedException e) {
																	// TODO Auto-generated catch
																	// block
																	e.printStackTrace();
																}

																Intent intentservice = new Intent(LoginActivity.this,
																		GroupDetailsAddService.class);
																startService(intentservice);
															}

															mProgressDialog_thread.dismiss();

															EShaktiApplication.setLoginFlag(Constants.ONLINEFLAG);

															if (mVersion < Integer
																	.parseInt(EShaktiApplication.getAppVersionCode())) {
																iSServiceCall = false;
																mUsername.setHint(RegionalConversion
																		.getRegionalConversion(AppStrings.userName));
																mPassword.setHint(RegionalConversion
																		.getRegionalConversion(AppStrings.passWord));
																AppDialogUtils.showAppVersionUpdateDialog(
																		LoginActivity.this, mVersionName);
															} else {
																iSServiceCall = false;
																PrefUtils.setLoginGroupService("1");
																PrefUtils.setLoginValues("1");
																Intent intent_login = new Intent(LoginActivity.this,
																		GroupListActivity.class);
																intent_login.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
																		| Intent.FLAG_ACTIVITY_CLEAR_TASK);
																startActivity(intent_login);
																overridePendingTransition(R.anim.right_to_left_in,
																		R.anim.right_to_left_out);
																finish();

															}

														}
													});
												}
											};
											new Thread(runnable).start();

										}
									} else {
										if (mLanguageLocalae == null) {

											mProgressDialog.dismiss();

											EShaktiApplication.getInstance().getTransactionManager()
													.startTransaction(DataType.LOGIN_ADD,
															new Login(0, LoginTask.UserName, LoginTask.Ngo_Id,
																	LoginTask.Trainer_Id, sPwd,
																	LoginTask.User_RegLanguage));

											// Navigates to Language selection

											final Dialog ChangeLanguageDialog = new Dialog(this);

											LayoutInflater li = (LayoutInflater) this
													.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
											final View dialogView = li.inflate(R.layout.dialog_choose_language, null,
													false);

											TextView confirmationHeader = (TextView) dialogView
													.findViewById(R.id.chooseLanguageHeader);
											confirmationHeader.setText(RegionalConversion
													.getRegionalConversion(AppStrings.chooseLanguage));
											confirmationHeader.setTypeface(LoginActivity.sTypeface);
											final RadioGroup radioGroup = (RadioGroup) dialogView
													.findViewById(R.id.radioLanguage);
											RadioButton radioButton = (RadioButton) dialogView
													.findViewById(R.id.radioEnglish);
											RadioButton radioButton_reg = (RadioButton) dialogView
													.findViewById(R.id.radioRegional);
											radioButton.setText("English");
											radioButton.setTypeface(LoginActivity.sTypeface);

											if (LoginTask.User_RegLanguage.equals("Hindi")) {
												radioButton_reg.setText("हिंदी");
												Typeface typeface = Typeface.createFromAsset(getAssets(),
                                                        "font/MANGAL.TTF");
												radioButton_reg.setTypeface(typeface);
											} else if (LoginTask.User_RegLanguage.equals("Marathi")) {
												radioButton_reg.setText("मराठी");
												Typeface typeface = Typeface.createFromAsset(getAssets(),
                                                        "font/MANGALHindiMarathi.TTF");
												radioButton_reg.setTypeface(typeface);
											} else if (LoginTask.User_RegLanguage.equals("Kannada")) {
												radioButton_reg.setText("ಕನ್ನಡ");
												Typeface typeface = Typeface.createFromAsset(getAssets(),
                                                        "font/tungaKannada.ttf");
												radioButton_reg.setTypeface(typeface);
											} else if (LoginTask.User_RegLanguage.equals("Malayalam")) {
												radioButton_reg.setText("മലയാളം");
												Typeface typeface = Typeface.createFromAsset(getAssets(),
                                                        "font/MLKR0nttMalayalam.ttf");
												radioButton_reg.setTypeface(typeface);
											} else if (LoginTask.User_RegLanguage.equals("Punjabi")) {
												radioButton_reg.setText("ਪੰਜਾਬੀ ");
												Typeface typeface = Typeface.createFromAsset(getAssets(),
                                                        "font/mangal-1361510185.ttf");
												radioButton_reg.setTypeface(typeface);
											} else if (LoginTask.User_RegLanguage.equals("Gujarathi")) {
												radioButton_reg.setText("ગુજરાતી");
												Typeface typeface = Typeface.createFromAsset(getAssets(),
                                                        "font/shrutiGujarathi.ttf");
												radioButton_reg.setTypeface(typeface);
											} else if (LoginTask.User_RegLanguage.equals("Bengali")) {
												radioButton_reg.setText("বাঙ্গালী");
												Typeface typeface = Typeface.createFromAsset(getAssets(),
                                                        "font/kalpurushBengali.ttf");
												radioButton_reg.setTypeface(typeface);
											} else if (LoginTask.User_RegLanguage.equals("Tamil")) {
												radioButton_reg.setText("தமிழ்");
												Typeface typeface = Typeface.createFromAsset(getAssets(),
                                                        "font/TSCu_SaiIndira.ttf");
												radioButton_reg.setTypeface(typeface);
											} else if (LoginTask.User_RegLanguage.equals("Assamese")) {
												radioButton_reg.setText("Assamese");
												Typeface typeface = Typeface.createFromAsset(getAssets(),
                                                        "font/KirtanUni_Assamese.ttf");
												radioButton_reg.setTypeface(typeface);
											} else {
												radioButton_reg.setText(RegionalConversion
														.getRegionalConversion(LoginTask.User_RegLanguage));
												Typeface typeface = Typeface.createFromAsset(getAssets(),
                                                        "font/Exo-Medium.ttf");
												radioButton_reg.setTypeface(typeface);
											}

											// radioButton_reg.setTypeface(LoginActivity.sTypeface);

											ButtonFlat okButton = (ButtonFlat) dialogView
													.findViewById(R.id.dialog_yes_button);
											okButton.setText(
													RegionalConversion.getRegionalConversion(AppStrings.dialogOk));
											okButton.setTypeface(LoginActivity.sTypeface);
											okButton.setOnClickListener(new OnClickListener() {

												@Override
												public void onClick(View v) {
													// TODO Auto-generated method
													// stub

													int selectedId = radioGroup.getCheckedRadioButtonId();

													// find the radiobutton by
													// returned
													// id
													RadioButton radioLanguageButton = (RadioButton) dialogView
															.findViewById(selectedId);

													String mSelectedLang = radioLanguageButton.getText().toString();
													Log.v("On Selected language", mSelectedLang);
													if (mSelectedLang.equals("हिंदी")) {
														mSelectedLang = "Hindi";
													} else if (mSelectedLang.equals("मराठी")) {
														mSelectedLang = "Marathi";
													} else if (mSelectedLang.equals("ಕನ್ನಡ")) {
														mSelectedLang = "Kannada";
													} else if (mSelectedLang.equals("മലയാളം")) {
														mSelectedLang = "Malayalam";
													} else if (mSelectedLang.equals("ਪੰਜਾਬੀ ")) {
														mSelectedLang = "Punjabi";
													} else if (mSelectedLang.equals("ગુજરાતી")) {
														mSelectedLang = "Gujarathi";
													} else if (mSelectedLang.equals("বাঙ্গালী")) {
														mSelectedLang = "Bengali";
													} else if (mSelectedLang.equals("தமிழ்")) {
														mSelectedLang = "Tamil";
													} else if (mSelectedLang.equals("Assamese")) {
														mSelectedLang = "Assamese";
													}
													try {
														PrefUtils.setUserlangcode(mSelectedLang);
													} catch (Exception e) {
														e.printStackTrace();
													}

													LoginActivity.sTypeface = GetTypeface
															.getTypeface(LoginActivity.this, mSelectedLang);

													RegionalserviceUtil.getRegionalService(LoginActivity.this,
															mSelectedLang);
													try {
//														new Login_webserviceTask(LoginActivity.this).execute();
														LoginDto dto = new LoginDto();
														dto.setUsername(String.valueOf(LoginActivity.sUName));
														//   dto.setUsername("testAnimator");
														dto.setPassword(String.valueOf(LoginActivity.sPwd));

														if (dto == null) {
															return;
														}

														String loginReqJson = new Gson().toJson(dto);
														RestClient.getRestClient(LoginActivity.this).callRestWebService(com.oasys.eshakti.digitization.OasysUtils.Constants.BASE_URL + com.oasys.eshakti.digitization.OasysUtils.Constants.LOGIN_URL, loginReqJson, LoginActivity.this, ServiceType.LOGIN);
//													new Login_webserviceTask(LoginActivity.this).execute();
														Thread.sleep(2000);
													} catch (InterruptedException e) {
														e.printStackTrace();
													}
													isNavigate = true;
													ChangeLanguageDialog.dismiss();

												}
											});

											ChangeLanguageDialog.getWindow().setBackgroundDrawable(
													new ColorDrawable(Color.TRANSPARENT));
											ChangeLanguageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
											ChangeLanguageDialog.setCanceledOnTouchOutside(false);
											ChangeLanguageDialog.setContentView(dialogView);
											ChangeLanguageDialog.setCancelable(false);
											ChangeLanguageDialog.show();

										} else {

											RegionalserviceUtil.getRegionalService(getApplicationContext(),
													mLanguageLocalae);
											EShaktiApplication.getInstance().getTransactionManager()
													.startTransaction(DataType.LOGINDELETE, null);
											try {
												Thread.sleep(300);
											} catch (InterruptedException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
											EShaktiApplication.getInstance().getTransactionManager().startTransaction(
													DataType.LOGIN_ADD,
													new Login(0, Login_webserviceTask.UserName,
															Login_webserviceTask.Ngo_Id,
															Login_webserviceTask.Trainer_Id, sPwd,
															Login_webserviceTask.User_RegLanguage));

											EShaktiApplication.setLoginFlag(Constants.ONLINEFLAG);
											if (Login_webserviceTask.mGroupActiveStatus.equals("No")) {

												new Get_Edit_OpeningbalanceWebservice(LoginActivity.this).execute();
												isNavigateEditOpeningBalanceActivity = true;

											} else {

												try {
													// ** CHECK **/

													Intent intentservice = new Intent(LoginActivity.this,
															GroupDetailsService.class);
													startService(intentservice);

												} catch (Exception e) {
													e.printStackTrace();
												}

												if (mVersion < Integer
														.parseInt(EShaktiApplication.getAppVersionCode())) {
													iSServiceCall = false;
													mUsername.setHint(RegionalConversion
															.getRegionalConversion(AppStrings.userName));
													mPassword.setHint(RegionalConversion
															.getRegionalConversion(AppStrings.passWord));
													AppDialogUtils.showAppVersionUpdateDialog(LoginActivity.this,
															mVersionName);
												} else {
													iSServiceCall = false;
													PrefUtils.setLoginGroupService("1");
													PrefUtils.setLoginValues("1");
													Intent intent_login = new Intent(LoginActivity.this,
															MainActivity.class);
													intent_login.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
															| Intent.FLAG_ACTIVITY_CLEAR_TASK);
													startActivity(intent_login);
													overridePendingTransition(R.anim.right_to_left_in,
															R.anim.right_to_left_out);
													finish();
												}
											}

										}
									}

								}

							} else {
								TastyToast.makeText(getApplicationContext(), AppStrings.tryLater,
										TastyToast.LENGTH_SHORT, TastyToast.ERROR);
							}
						}
					}
				}
			}
		} // ProgressDialog

	}

	/* Initiating Menu XML file (menu.xml) */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		return true;
	}

	/**
	 * Event Handling for Individual menu item selected Identify single menu item by
	 * it's id
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
			case R.id.menu_exit:

				try {
					Log.d("EXIT INTENT ", "Controls");
					// startActivity(getExitIntent(getApplicationContext()));
					startActivity(GetExit.getExitIntent(getApplicationContext()));

				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}

				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Subscribe
	public void onAddLoginDetails(final LoginResponse loginresponse) {
		switch (loginresponse.getFetcherResult()) {
			case FAIL:
				TastyToast.makeText(getApplicationContext(), loginresponse.getFetcherResult().getMessage(),
						TastyToast.LENGTH_SHORT, TastyToast.ERROR);
				break;
			case NO_NETWORK_CONNECTION:
				TastyToast.makeText(getApplicationContext(), loginresponse.getFetcherResult().getMessage(),
						TastyToast.LENGTH_SHORT, TastyToast.ERROR);
				break;
			case SUCCESS:
				// LoginProvider.getLoginDetails();
				Log.v("On Login", "s");
				break;
		}
	}

	@Subscribe
	public void onDeleteLoginDetails(final LoginDeleteResponse loginDeleteResponse) {
		switch (loginDeleteResponse.getFetcherResult()) {
			case FAIL:
				TastyToast.makeText(getApplicationContext(), loginDeleteResponse.getFetcherResult().getMessage(),
						TastyToast.LENGTH_SHORT, TastyToast.ERROR);
				break;
			case NO_NETWORK_CONNECTION:
				TastyToast.makeText(getApplicationContext(), loginDeleteResponse.getFetcherResult().getMessage(),
						TastyToast.LENGTH_SHORT, TastyToast.ERROR);
				break;
			case SUCCESS:
				Log.v("Delete Sucessfully", "12121");
				break;
		}
	}

	@Subscribe
	public void onCheckLoginDetails(final LoginCheckResponse logincheckresponse) {

		switch (logincheckresponse.getFetcherResult()) {
			case FAIL:
				TastyToast.makeText(getApplicationContext(), logincheckresponse.getFetcherResult().getMessage(),
						TastyToast.LENGTH_SHORT, TastyToast.ERROR);
				break;
			case NO_NETWORK_CONNECTION:
				TastyToast.makeText(getApplicationContext(), logincheckresponse.getFetcherResult().getMessage(),
						TastyToast.LENGTH_SHORT, TastyToast.ERROR);
				break;
			case SUCCESS:
				if (sPwd != null) {

					if (sPwd.equalsIgnoreCase(LoginCheck.getUsername()) && LoginCheck.isUserAvabl()) {
						Log.e("Yesssssssssssss", "Yessssssssssssssssss");
						Log.v("Agent Valuessssssssss", EShaktiApplication.isAgent + "");
						if (Boolean.valueOf(EShaktiApplication.isAgent)) {

							EShaktiApplication.setLoginFlag(Constants.OFFLINEFLAG);
							if (ConnectionUtils.isNetworkAvailable(getApplicationContext())) {
								PrefUtils.setLoginGroupService("1");
								EShaktiApplication.setLoginFlag(Constants.ONLINEFLAG);
							}

							intent_login = new Intent(LoginActivity.this, GroupListActivity.class);
							intent_login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

							startActivity(intent_login);
							overridePendingTransition(R.anim.right_to_left_in, R.anim.right_to_left_out);

							isStop = true;
							finish();
						} else if (!Boolean.valueOf(EShaktiApplication.isAgent)) {
							Log.v("Is Not Agent", "!!!!!!!!!!!!!!!!!!!!!!!!!");
							try {
								if (LoginCheck.getTrainerId() != null) {
									EShaktiApplication.getInstance().getTransactionManager().startTransaction(
											DataType.GROUPDETAILS_GETMASTER,
											new GroupMasterSingle(LoginCheck.getTrainerId()));
								} else {
									TastyToast.makeText(getApplicationContext(), "Please Go to Online",
											TastyToast.LENGTH_SHORT, TastyToast.WARNING);
								}
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						}

					} else if (LoginCheck.isUserAvabl() == false) {

						TastyToast.makeText(getApplicationContext(), AppStrings.userNotExist, TastyToast.LENGTH_SHORT,
								TastyToast.WARNING);

					} else {

						TastyToast.makeText(getApplicationContext(), AppStrings.pwdIncorrectAlert, TastyToast.LENGTH_SHORT,
								TastyToast.WARNING);

					}

				}
				break;
		}
	}

	@Subscribe
	public void onDeleteOffline(final OfflineTransDeleteResponse offlineTransDeleteResponse) {
		switch (offlineTransDeleteResponse.getFetcherResult()) {
			case FAIL:
				TastyToast.makeText(getApplicationContext(), offlineTransDeleteResponse.getFetcherResult().getMessage(),
						TastyToast.LENGTH_SHORT, TastyToast.ERROR);
				break;
			case NO_NETWORK_CONNECTION:
				TastyToast.makeText(getApplicationContext(), offlineTransDeleteResponse.getFetcherResult().getMessage(),
						TastyToast.LENGTH_SHORT, TastyToast.ERROR);
				break;
			case SUCCESS:

				Intent intent = new Intent(LoginActivity.this, LoginActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);
				finish();

				break;
		}
	}

	@Subscribe
	public void OnGroupMasterResponse_GorupLogin(final GroupMasterResponse groupMasterResponse) {
		switch (groupMasterResponse.getFetcherResult()) {
			case FAIL:
				TastyToast.makeText(getApplicationContext(), groupMasterResponse.getFetcherResult().getMessage(),
						TastyToast.LENGTH_SHORT, TastyToast.ERROR);
				break;
			case NO_NETWORK_CONNECTION:
				TastyToast.makeText(getApplicationContext(), groupMasterResponse.getFetcherResult().getMessage(),
						TastyToast.LENGTH_SHORT, TastyToast.ERROR);
				break;
			case SUCCESS:

				try {

					if (GetGroupMemberDetails.getGroupMemberresponse() != null) {
						SelectedGroupsTask.getGroupDetails(GetGroupMemberDetails.getGroupMemberresponse());

						System.out.println("GroupActivity.UserName : " + SelectedGroupsTask.sLastTransactionDate_Response);
						EShaktiApplication.setGetsinglegrpId(SelectedGroupsTask.Group_Id);
						PrefUtils.setOfflineUniqueID(Get_UniqueID.get_UniqueID(this));

						Log.v("Offline Unique ID ", PrefUtils.getOfflineUniqueID());

						PrefUtils.clearGroupLoanOSId();
						PrefUtils.clearGroupLoanOsrepay();
						PrefUtils.clearMemberLoanId();
						PrefUtils.clearMemberLoanrepay();

						PrefUtils.clearBankDeposit();
						PrefUtils.clearBankDepositName();

						PrefUtils.clearBankFixedDeposit();
						PrefUtils.clearBankFixedDepositName();

						Constants.FRAG_INSTANCE_CONSTANT = "0";
						EShaktiApplication.setOnSavedFragment(false);
						EShaktiApplication.setLoginFlag(Constants.OFFLINEFLAG);
						if (ConnectionUtils.isNetworkAvailable(getApplicationContext())) {
							EShaktiApplication.setLoginFlag(Constants.ONLINEFLAG);
						}
						Intent intent = new Intent(LoginActivity.this, MainActivity.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
						overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

						finish();

					} else {
						TastyToast.makeText(getApplicationContext(), "Please Go to Online", TastyToast.LENGTH_SHORT,
								TastyToast.ERROR);
					}
				} catch (IndexOutOfBoundsException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (RuntimeException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				break;
		}

	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		SBus.INST.register(this);

		if ((int) Build.VERSION.SDK_INT < 23) {

			TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
			if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
				// TODO: Consider calling
				//    ActivityCompat#requestPermissions
				// here to request the missing permissions, and then overriding
				//   public void onRequestPermissionsResult(int requestCode, String[] permissions,
				//                                          int[] grantResults)
				// to handle the case where the user grants the permission. See the documentation
				// for ActivityCompat#requestPermissions for more details.
				return;
			}
			String mIMEINo = mngr.getDeviceId();
			EShaktiApplication.setIMEI_NO(mIMEINo);

			/** Gets the Location **/
			gps = new GPSTracker(LoginActivity.this);

			// check if GPS enabled
			if (gps.canGetLocation()) {

				double latitude = gps.getLatitude();
				double longitude = gps.getLongitude();

				sLatitude = String.valueOf(latitude);
				sLongitude = String.valueOf(longitude);

			} else {
				// can't get location
				// GPS or Network is not enabled
				// Ask user to enable GPS/network in settings
				gps.showSettingsAlert();
			}

		} else if ((int) Build.VERSION.SDK_INT >= 23) {

			int hasLocationPermission = checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);
			int hasLocationCOARSEPermission = checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION);
			int hasReadPhoto = checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
			int haswritePhoto = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
			int hasReadPhonePermission = checkSelfPermission(Manifest.permission.READ_PHONE_STATE);
			int haswriteCemera = checkSelfPermission(Manifest.permission.CAMERA);
			List<String> permissions = new ArrayList<String>();
			if (hasLocationPermission != PackageManager.PERMISSION_GRANTED) {
				permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
			}

			if (hasLocationCOARSEPermission != PackageManager.PERMISSION_GRANTED) {
				permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
			}

			if (hasReadPhoto != PackageManager.PERMISSION_GRANTED) {
				permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
			}

			if (haswritePhoto != PackageManager.PERMISSION_GRANTED) {
				permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
			}
			if (hasReadPhonePermission != PackageManager.PERMISSION_GRANTED) {
				permissions.add(Manifest.permission.READ_PHONE_STATE);
			}
			if (haswriteCemera != PackageManager.PERMISSION_GRANTED) {
				permissions.add(Manifest.permission.CAMERA);
			}

			if (!permissions.isEmpty()) {
				requestPermissions(permissions.toArray(new String[permissions.size()]),
						REQUEST_CODE_SOME_FEATURES_PERMISSIONS);
			} else {

				TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

				String mIMEINo = mngr.getDeviceId();

				EShaktiApplication.setIMEI_NO(mIMEINo);
				/** Gets the Location **/
				gps = new GPSTracker(LoginActivity.this);

				// check if GPS enabled
				if (gps.canGetLocation()) {

					double latitude = gps.getLatitude();
					double longitude = gps.getLongitude();

					sLatitude = String.valueOf(latitude);
					sLongitude = String.valueOf(longitude);

				} else {
					// can't get location
					// GPS or Network is not enabled
					// Ask user to enable GPS/network in settings
					gps.showSettingsAlert();
				}

			}

		}

	}

	@Override
	public void onStop() {
		super.onStop();
		SBus.INST.unRegister(this);

	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();

	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		switch (requestCode) {
			case 100: {
				for (int i = 0; i < permissions.length; i++) {
					if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
						Log.d("Permissions", "Permission Granted: " + permissions[i]);
						if (permissions[i].equals("android.permission.ACCESS_FINE_LOCATION")) {

							PrefUtils.setLocationPermission("true");
						} else if (permissions[i].equals("android.permission.ACCESS_COARSE_LOCATION")) {
							PrefUtils.setLocationPermission("true");
						} else if (permissions[i].equals("android.permission.READ_EXTERNAL_STORAGE")) {

							PrefUtils.setStoragePermission("true");
						} else if (permissions[i].equals("android.permission.WRITE_EXTERNAL_STORAGE")) {
							PrefUtils.setStoragePermission("true");
						} else if (permissions[i].equals("android.permission.READ_PHONE_STATE")) {

							PrefUtils.setPhonePermission("true");
						} else if (permissions[i].equals("android.permission.CAMERA")) {

							PrefUtils.setCameraPermission("true");
						}
					} else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
						Log.d("Permissions", "Permission Denied: " + permissions[i]);
						if (permissions[i].equals("android.permission.ACCESS_FINE_LOCATION")) {
							PrefUtils.setLocationPermission("false");
						} else if (permissions[i].equals("android.permission.ACCESS_COARSE_LOCATION")) {
							PrefUtils.setLocationPermission("false");
						} else if (permissions[i].equals("android.permission.READ_EXTERNAL_STORAGE")) {
							PrefUtils.setStoragePermission("false");
						} else if (permissions[i].equals("android.permission.WRITE_EXTERNAL_STORAGE")) {
							PrefUtils.setStoragePermission("false");
						} else if (permissions[i].equals("android.permission.READ_PHONE_STATE")) {
							PrefUtils.setPhonePermission("false");
						} else if (permissions[i].equals("android.permission.CAMERA")) {
							PrefUtils.setCameraPermission("false");
						}
					}

				}

				if (PrefUtils.getLocationPermission() != null) {
					if (PrefUtils.getLocationPermission().equals("true")) {

						TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

						if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
							// TODO: Consider calling
							//    ActivityCompat#requestPermissions
							// here to request the missing permissions, and then overriding
							//   public void onRequestPermissionsResult(int requestCode, String[] permissions,
							//                                          int[] grantResults)
							// to handle the case where the user grants the permission. See the documentation
							// for ActivityCompat#requestPermissions for more details.
							return;
						}
						String mIMEINo = mngr.getDeviceId();

					EShaktiApplication.setIMEI_NO(mIMEINo);
					/** Gets the Location **/
					gps = new GPSTracker(LoginActivity.this);

					// check if GPS enabled
					if (gps.canGetLocation()) {

						double latitude = gps.getLatitude();
						double longitude = gps.getLongitude();

						sLatitude = String.valueOf(latitude);
						sLongitude = String.valueOf(longitude);

					} else {
						// can't get location
						// GPS or Network is not enabled
						// Ask user to enable GPS/network in settings
						gps.showSettingsAlert();
					}

				} else {
					Toast.makeText(getApplicationContext(), "PLEASE PROVIDE A PERMISSION", Toast.LENGTH_LONG).show();
					finish();
				}
			}
		}
			break;
		default: {
			super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		}
		}
	}

}
