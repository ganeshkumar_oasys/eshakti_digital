package com.yesteam.eshakti.view.fragment;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.view.activity.LoginActivity;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class Profile_Member_View_Image_Fragment extends Fragment {

	private TextView mMemberName;

	public Profile_Member_View_Image_Fragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_IMAGE_VIEW;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_memberview_image, container, false);

		mMemberName = (TextView) rootView.findViewById(R.id.memberName);
		mMemberName.setText(String.valueOf(EShaktiApplication.getSelectedMemberName()));
		mMemberName.setTypeface(LoginActivity.sTypeface);

		ImageView imageView = (ImageView) rootView.findViewById(R.id.member_imageview);

		byte[] imageAsBytes = Base64.decode(Profile_Member_Aadhaar_Image_View_MenuFragment.mServiceResponse.getBytes(),
				Base64.DEFAULT);
		imageView.setImageBitmap(BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length));

		return rootView;
	}

}