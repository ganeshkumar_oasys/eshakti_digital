package com.yesteam.eshakti.view.fragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.squareup.otto.Subscribe;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.adapter.CustomItemAdapter;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;
import com.yesteam.eshakti.model.RowItem;
import com.yesteam.eshakti.sqlite.database.GroupProvider;
import com.yesteam.eshakti.sqlite.database.response.GroupLoanOSResponse;
import com.yesteam.eshakti.sqlite.db.model.GetGroupMemberDetails;
import com.yesteam.eshakti.sqlite.db.model.GroupTempDBLastTransDate;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.utils.GetOfflineTransactionValues;
import com.yesteam.eshakti.utils.GetResponseInfo;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.utils.Get_EdiText_Filter;
import com.yesteam.eshakti.utils.Put_DB_GroupResponse;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.utils.UpdateCalendarMinMaxDateUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.views.MaterialSpinner;
import com.yesteam.eshakti.views.RaisedButton;
import com.yesteam.eshakti.webservices.GetAddLoanLimitWebservice;
import com.yesteam.eshakti.webservices.Get_LastTransactionID;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Transaction_IncreaseLimit_Fragment extends Fragment implements OnClickListener, TaskListener {

	private TextView mGroupName, mCashInHand, mCashAtBank, mHeader;
	private TextView mSanctionAmountText, mDisbursementAmountText, mSanctionAmount_value, mDisbursementAmount_value,
			mIncreaseLimitText, mBankChargesText, mBalance_Text, mBalance_value;
	private EditText mIncreaseLimit_editText, mBankCharges_editText;
	private RaisedButton mSubmitButton;
	View rootView;
	public static String increaseLimit = "", bankCharges = "";
	private MaterialSpinner mTenureSpinner;
	private String[] mTenureArr, mTenureArray;
	private List<RowItem> tenureItems;
	private CustomItemAdapter tenureAdapter;
	public static String mTenure_value = "";
	private Dialog mProgressDialog;
	Dialog confirmationDialog;
	private Button mEdit_RaisedButton, mOk_RaisedButton;
	public static String[] response;
	public static String mGroupOS_Offlineresponse = null;
	String mLastTrDate = null;
	boolean isLastTransactionValues = false;
	Date date_dashboard, date_loanDisb;

	public Transaction_IncreaseLimit_Fragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		increaseLimit = "";
		bankCharges = "";
		mTenure_value = "";
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		SBus.INST.register(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		rootView = inflater.inflate(R.layout.fragment_increase_limit, container, false);

		MainFragment_Dashboard.isBackpressed = false;

		try {

			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashInHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashInHand.setTypeface(LoginActivity.sTypeface);

			mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashAtBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashAtBank.setTypeface(LoginActivity.sTypeface);

			init();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return rootView;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_LOAN_LIMIT;
	}

	private void init() {
		// TODO Auto-generated method stub
		mHeader = (TextView) rootView.findViewById(R.id.increaseLimitheader);
		mSanctionAmountText = (TextView) rootView.findViewById(R.id.sanctionAmountTextView);
		mSanctionAmount_value = (TextView) rootView.findViewById(R.id.sanctionAmount_values);
		mDisbursementAmountText = (TextView) rootView.findViewById(R.id.disbursementAmountTextView);
		mDisbursementAmount_value = (TextView) rootView.findViewById(R.id.disbursementAmount_values);
		mIncreaseLimitText = (TextView) rootView.findViewById(R.id.increase_limitTextView);
		mBankChargesText = (TextView) rootView.findViewById(R.id.bankChargesTextView);

		mBalance_Text = (TextView) rootView.findViewById(R.id.balanceAmountTextView);
		mBalance_value = (TextView) rootView.findViewById(R.id.balanceAmount_values);

		mHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.mIncreaseLimit));
		mSanctionAmountText.setText(RegionalConversion.getRegionalConversion(AppStrings.mSanctionAmount));
		mDisbursementAmountText.setText(RegionalConversion.getRegionalConversion(AppStrings.mBankCharges));
		mIncreaseLimitText.setText(RegionalConversion.getRegionalConversion(AppStrings.mIncreaseLimit));
		mBankChargesText.setText(RegionalConversion.getRegionalConversion(AppStrings.mBankCharges));
		mBalance_Text.setText(RegionalConversion.getRegionalConversion(AppStrings.tenure));

		mHeader.setTypeface(LoginActivity.sTypeface);
		mSanctionAmountText.setTypeface(LoginActivity.sTypeface);
		mSanctionAmount_value.setTypeface(LoginActivity.sTypeface);
		mDisbursementAmountText.setTypeface(LoginActivity.sTypeface);
		mDisbursementAmount_value.setTypeface(LoginActivity.sTypeface);
		mIncreaseLimitText.setTypeface(LoginActivity.sTypeface);
		mBankChargesText.setTypeface(LoginActivity.sTypeface);
		mBalance_Text.setTypeface(LoginActivity.sTypeface);
		mBalance_value.setTypeface(LoginActivity.sTypeface);
		Log.e("publicValues.mGetLoanBalnceValues", publicValues.mGetLoanBalnceValues + "");
		if (publicValues.mGetLoanBalnceValues != null) {
			String[] mValues = publicValues.mGetLoanBalnceValues.split("#");
			String mValues_limit = mValues[0];
			String[] mLimitValues = mValues_limit.split("~");
			mSanctionAmount_value.setText(mLimitValues[0]);
			mDisbursementAmount_value.setText(mLimitValues[1]);
			mBalance_value.setText(mLimitValues[2]);

		}

		mIncreaseLimit_editText = (EditText) rootView.findViewById(R.id.increase_limit_value);
		mIncreaseLimit_editText.setTypeface(LoginActivity.sTypeface);
		mIncreaseLimit_editText.setInputType(InputType.TYPE_CLASS_NUMBER);
		mIncreaseLimit_editText.setPadding(5, 5, 5, 5);
		mIncreaseLimit_editText.setFilters(Get_EdiText_Filter.editText_filter());
		mIncreaseLimit_editText.setBackgroundResource(R.drawable.edittext_background);

		mBankCharges_editText = (EditText) rootView.findViewById(R.id.bankCharges_value);
		mBankCharges_editText.setTypeface(LoginActivity.sTypeface);
		mBankCharges_editText.setInputType(InputType.TYPE_CLASS_NUMBER);
		mBankCharges_editText.setPadding(5, 5, 5, 5);
		mBankCharges_editText.setFilters(Get_EdiText_Filter.editText_filter());
		mBankCharges_editText.setBackgroundResource(R.drawable.edittext_background);

		// Tenure spinner
		mTenureSpinner = (MaterialSpinner) rootView.findViewById(R.id.increaselimit_tenure_spinner);
		mTenureSpinner.setBaseColor(color.grey_400);
		mTenureSpinner.setFloatingLabelText(AppStrings.tenure);
		mTenureSpinner.setPaddingSafe(10, 0, 10, 0);

		mTenureArr = new String[] { "6", "10", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23",
				"24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40",
				"41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57",
				"58", "59", "60" };
		mTenureArray = new String[mTenureArr.length + 1];
		mTenureArray[0] = String.valueOf(AppStrings.tenure);
		for (int i = 0; i < mTenureArr.length; i++) {
			mTenureArray[i + 1] = mTenureArr[i].toString();
		}

		tenureItems = new ArrayList<RowItem>();
		for (int i = 0; i < mTenureArray.length; i++) {
			RowItem rowItem = new RowItem(mTenureArray[i]);
			tenureItems.add(rowItem);
		}
		tenureAdapter = new CustomItemAdapter(getActivity(), tenureItems);
		mTenureSpinner.setAdapter(tenureAdapter);
		mTenureSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				Log.d("TENURE POSITION", String.valueOf(position));

				String selectedItem;
				if (position == 0) {
					selectedItem = mTenureArray[0];
					mTenure_value = "0";
				} else {
					selectedItem = mTenureArray[position];
					System.out.println("SELECTED TENURE:" + selectedItem);

					mTenure_value = selectedItem;

				}
				Log.e("Tenure value", mTenure_value);

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});

		mSubmitButton = (RaisedButton) rootView.findViewById(R.id.increaseLimit_submit);
		mSubmitButton.setText(RegionalConversion.getRegionalConversion(AppStrings.submit));
		mSubmitButton.setTypeface(LoginActivity.sTypeface);
		mSubmitButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {
		case R.id.increaseLimit_submit:

			increaseLimit = "";
			bankCharges = "";

			increaseLimit = mIncreaseLimit_editText.getText().toString();
			bankCharges = mBankCharges_editText.getText().toString();

			if (increaseLimit.equals("") || increaseLimit == null) {
				increaseLimit = "0";
			}

			if (bankCharges.equals("") || bankCharges == null) {
				bankCharges = "0";
			}

			if (!increaseLimit.equals("0")) {
				// String dashBoardDate = DatePickerDialog.sDashboardDate;
				String dashBoardDate = null;
				if (DatePickerDialog.sDashboardDate.contains("-")) {
					dashBoardDate = DatePickerDialog.sDashboardDate;
				} else if (DatePickerDialog.sDashboardDate.contains("/")) {
					String[] dateArr = DatePickerDialog.sDashboardDate.split("/");
					dashBoardDate = dateArr[0] + "-" + dateArr[1] + "-" + dateArr[2];
					;
				}
				String loanDisbArr[] = EShaktiApplication.getLoanAcc_LoanDisbursementDate().split("/");
				String loanDisbDate = loanDisbArr[1] + "-" + loanDisbArr[0] + "-" + loanDisbArr[2];

				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

				try {
					date_dashboard = sdf.parse(dashBoardDate);
					date_loanDisb = sdf.parse(loanDisbDate);
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				if (date_dashboard.compareTo(date_loanDisb) >= 0) {
					confirmationDialog = new Dialog(getActivity());

					LayoutInflater inflater = getActivity().getLayoutInflater();
					View dialogView = inflater.inflate(R.layout.dialog_confirmation, null);
					dialogView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
							LayoutParams.WRAP_CONTENT));

					TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
					confirmationHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.confirmation));
					confirmationHeader.setTypeface(LoginActivity.sTypeface);

					TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

					TableRow increaseLimitRow = new TableRow(getActivity());

					@SuppressWarnings("deprecation")
					TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
							LayoutParams.WRAP_CONTENT, 1f);
					contentParams.setMargins(10, 5, 10, 5);

					TextView increaseLimitText = new TextView(getActivity());
					increaseLimitText.setText(
							GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mIncreaseLimit)));
					increaseLimitText.setTypeface(LoginActivity.sTypeface);
					increaseLimitText.setTextColor(color.white);
					increaseLimitText.setPadding(5, 5, 5, 5);
					increaseLimitText.setLayoutParams(contentParams);
					increaseLimitRow.addView(increaseLimitText);

					TextView increaseLimit_values = new TextView(getActivity());
					increaseLimit_values
							.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(increaseLimit)));
					increaseLimit_values.setTextColor(color.white);
					increaseLimit_values.setPadding(5, 5, 5, 5);
					increaseLimit_values.setGravity(Gravity.RIGHT);
					increaseLimit_values.setLayoutParams(contentParams);
					increaseLimitRow.addView(increaseLimit_values);

					confirmationTable.addView(increaseLimitRow,
							new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

					TableRow bankChargeRow = new TableRow(getActivity());

					TextView bankChargeText = new TextView(getActivity());
					bankChargeText
							.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mBankCharges)));
					bankChargeText.setTypeface(LoginActivity.sTypeface);
					bankChargeText.setTextColor(color.white);
					bankChargeText.setPadding(5, 5, 5, 5);
					bankChargeText.setLayoutParams(contentParams);
					bankChargeRow.addView(bankChargeText);

					TextView bankCharge_values = new TextView(getActivity());
					bankCharge_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(bankCharges)));
					bankCharge_values.setTextColor(color.white);
					bankCharge_values.setPadding(5, 5, 5, 5);
					bankCharge_values.setGravity(Gravity.RIGHT);
					bankCharge_values.setLayoutParams(contentParams);
					bankChargeRow.addView(bankCharge_values);

					confirmationTable.addView(bankChargeRow,
							new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

					TableRow tenureRow = new TableRow(getActivity());

					TextView tenureText = new TextView(getActivity());
					tenureText.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.tenure)));
					tenureText.setTypeface(LoginActivity.sTypeface);
					tenureText.setTextColor(color.white);
					tenureText.setPadding(5, 5, 5, 5);
					tenureText.setLayoutParams(contentParams);
					tenureRow.addView(tenureText);

					TextView tenure_values = new TextView(getActivity());
					tenure_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(mTenure_value)));
					tenure_values.setTextColor(color.white);
					tenure_values.setPadding(5, 5, 5, 5);
					tenure_values.setGravity(Gravity.RIGHT);
					tenure_values.setLayoutParams(contentParams);
					tenureRow.addView(tenure_values);

					confirmationTable.addView(tenureRow,
							new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

					mEdit_RaisedButton = (RaisedButton) dialogView.findViewById(R.id.fragment_Edit_button);
					mEdit_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.edit));
					mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
					mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
																			// 205,
																			// 0));
					mEdit_RaisedButton.setOnClickListener(this);

					mOk_RaisedButton = (RaisedButton) dialogView.findViewById(R.id.fragment_Ok_button);
					mOk_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.yes));
					mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
					mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
					mOk_RaisedButton.setOnClickListener(this);

					confirmationDialog.getWindow()
							.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
					confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					confirmationDialog.setCanceledOnTouchOutside(false);
					confirmationDialog.setContentView(dialogView);
					confirmationDialog.setCancelable(true);
					confirmationDialog.show();

					MarginLayoutParams margin = (MarginLayoutParams) dialogView.getLayoutParams();
					margin.leftMargin = 10;
					margin.rightMargin = 10;
					margin.topMargin = 10;
					margin.bottomMargin = 10;
					margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);
				} else {
					TastyToast.makeText(getActivity(), AppStrings.mCheckDisbursementDate, TastyToast.LENGTH_SHORT,
							TastyToast.WARNING);
				}
			} else {
				TastyToast.makeText(getActivity(), AppStrings.nullDetailsAlert, TastyToast.LENGTH_SHORT,
						TastyToast.WARNING);
			}

			break;

		case R.id.fragment_Edit_button:
			increaseLimit = "";
			bankCharges = "";
			// mTenure_value = "0";
			mSubmitButton.setClickable(true);
			confirmationDialog.dismiss();

			break;
		case R.id.fragment_Ok_button:

			if (EShaktiApplication.getLoginFlag().equals(Constants.ONLINEFLAG)) {
				if (ConnectionUtils.isNetworkAvailable(getActivity())) {
					new GetAddLoanLimitWebservice(Transaction_IncreaseLimit_Fragment.this).execute();
				}
			}

			break;

		}
	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub

		mProgressDialog = AppDialogUtils.createProgressDialog(getActivity());
		mProgressDialog.show();

	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub

		try {

			if (mProgressDialog != null) {
				mProgressDialog.dismiss();
				if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {
					getActivity().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							confirmationDialog.dismiss();
							if (!result.equals("FAIL")) {

								TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg,
										TastyToast.LENGTH_SHORT, TastyToast.ERROR);
								Constants.NETWORKCOMMONFLAG = "SUCCESS";
							}

						}
					});
				} else {

					if (isLastTransactionValues) {

						confirmationDialog.dismiss();

						TastyToast.makeText(getActivity(), AppStrings.transactionCompleted, TastyToast.LENGTH_SHORT,
								TastyToast.SUCCESS);

						MainFragment_Dashboard fragment = new MainFragment_Dashboard();
						setFragment(fragment);
						/*
						 * EShaktiApplication.getInstance().getTransactionManager().startTransaction(
						 * DataType.GET_GROUPMASTER_GLOS, new
						 * GroupMasterSingle(SelectedGroupsTask.Group_Id));
						 */ } else {

						if (GetResponseInfo.sResponseUpdate[0].equals("Yes")) {

							new Get_LastTransactionID(Transaction_IncreaseLimit_Fragment.this).execute();
							isLastTransactionValues = true;

						} else {

							confirmationDialog.dismiss();

							TastyToast.makeText(getActivity(), AppStrings.transactionFailAlert, TastyToast.LENGTH_SHORT,
									TastyToast.ERROR);
							MainFragment_Dashboard fragment = new MainFragment_Dashboard();
							setFragment(fragment);

						}

					}

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Subscribe
	public void OnGroupLoanOSResponse(final GroupLoanOSResponse groupLoanOSResponse) {
		switch (groupLoanOSResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), groupLoanOSResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), groupLoanOSResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:
			String mGroupLoanOSResponse = GetGroupMemberDetails.getGrouploanOS();
			StringBuilder builder = new StringBuilder();

			String mMasterresponse, mGroupLoanOSId = null, mGLOS = null, result;

			String mCashinHand, mGroupLoanOutStanding = null;
			Log.e("Group response@@@@@@@", mGroupLoanOSResponse + "");
			if (mGroupLoanOSResponse != null && !mGroupLoanOSResponse.equals("")) {

				response = mGroupLoanOSResponse.split("%");

				for (int i = 0; i < response.length; i++) {
					System.out.println("Response : " + response[i]);

					String secondResponse[] = response[i].split("#");
					System.out.println("LOAN ID : " + secondResponse[0]);

					mGLOS = secondResponse[1];
					if (Transaction_LoanDisbursement_LoansMenuFragment.mSendTo_ServerLoan_Id
							.equals(secondResponse[0])) {
						System.out.println("LOAN OUTS : " + secondResponse[1]);
						mGroupLoanOutStanding = secondResponse[1];
						mGroupLoanOSId = secondResponse[0];
						mGLOS = String
								.valueOf(Integer.parseInt(mGroupLoanOutStanding) + Integer.parseInt(increaseLimit));

					} else {
						mGroupLoanOSId = secondResponse[0];
						mGLOS = secondResponse[1];
					}

					mMasterresponse = mGroupLoanOSId + "#" + mGLOS + "%";

					builder.append(mMasterresponse);

				}
				result = builder.toString();
				Log.v("Update Values", result);

				String mCashatBank = SelectedGroupsTask.sCashatBank;
				mCashinHand = SelectedGroupsTask.sCashinHand;
				String mBankDetails = GetOfflineTransactionValues.getBankDetails();
				mLastTrDate = SelectedGroupsTask.sLastTransactionDate_Response;

				String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mLastTrDate, mCashinHand,
						mCashatBank, mBankDetails);
				String mSelectedGroupId = SelectedGroupsTask.Group_Id;

			/*	EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GROUP_MASTER_GLOS,
						new GroupLoanOSUpdate(mSelectedGroupId, mGroupMasterResponse, result));*/
			} else {
				TastyToast.makeText(getActivity(), AppStrings.loginAgainAlert, TastyToast.LENGTH_LONG,
						TastyToast.WARNING);
			}
			break;
		}

	}

	@Subscribe
	public void OnGroupGLOSUpdate(final GroupLoanOSResponse groupLoanOSResponse) {
		switch (groupLoanOSResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), groupLoanOSResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), groupLoanOSResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			if (ConnectionUtils.isNetworkAvailable(getActivity())) {
				confirmationDialog.dismiss();

				TastyToast.makeText(getActivity(), AppStrings.transactionCompleted, TastyToast.LENGTH_SHORT,
						TastyToast.SUCCESS);

				MainFragment_Dashboard fragment = new MainFragment_Dashboard();
				setFragment(fragment);
			}
			break;
		}
	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub

		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

		FragmentDrawer.drawer_CashinHand.setText(RegionalConversion.getRegionalConversion(AppStrings.cashinhand)

				+ SelectedGroupsTask.sCashinHand);
		FragmentDrawer.drawer_CashinHand.setTypeface(LoginActivity.sTypeface);

		FragmentDrawer.drawer_CashatBank.setText(RegionalConversion.getRegionalConversion(AppStrings.cashatBank)

				+ SelectedGroupsTask.sCashatBank);
		FragmentDrawer.drawer_CashatBank.setTypeface(LoginActivity.sTypeface);

		FragmentDrawer.drawer_lastTR_Date
				.setText(RegionalConversion.getRegionalConversion(AppStrings.lastTransactionDate) + " : "
						+ SelectedGroupsTask.sLastTransactionDate_Response);
		FragmentDrawer.drawer_lastTR_Date.setTypeface(LoginActivity.sTypeface);

		UpdateCalendarMinMaxDateUtils.OnUpdateCalendarDate();

		Calendar calender = Calendar.getInstance();

		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String formattedDate = df.format(calender.getTime());
		EShaktiApplication.setLastTransDate_GroupId(SelectedGroupsTask.Group_Id);

		String lastTrDateArr[] = DatePickerDialog.sDashboardDate.split("-");
		String lastTrDate = lastTrDateArr[0] + "/" + lastTrDateArr[1] + "/" + lastTrDateArr[2];

		new GroupTempDBLastTransDate(EShaktiApplication.getLastTransDate_GroupId(), lastTrDate, formattedDate);

		GroupProvider.updateGroupLastTransDateResponse();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		SBus.INST.unRegister(this);
	}

}
