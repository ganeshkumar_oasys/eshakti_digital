package com.yesteam.eshakti.view.fragment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.FileUtil;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.views.RaisedButton;
import com.yesteam.eshakti.webservices.GetResolution_Webservices;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.fragment.app.Fragment;
import androidx.core.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import id.zelory.compressor.Compressor;


public class Meeting_Upload_Schedule_form extends Fragment implements OnClickListener, TaskListener {
	private TextView mGroupName, mCashInHand, mCashAtBank;

	ImageView mImageView;
	String mCurrentPhotoPath;
	RaisedButton mTakePhoto, mSubmitPhoto;

	static final int REQUEST_TAKE_PHOTO = 1;
	File photoFile = null;
	private File actualImage;
	private File compressedImage;
	private Dialog mProgressDilaog;
	Bitmap bitmap_ = null;

	public Meeting_Upload_Schedule_form() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		SBus.INST.register(this);
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		SBus.INST.unRegister(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_upload_schedule_form, container, false);
		try {

			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashInHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashInHand.setTypeface(LoginActivity.sTypeface);

			mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashAtBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashAtBank.setTypeface(LoginActivity.sTypeface);

			mTakePhoto = (RaisedButton) rootView.findViewById(R.id.take_photo);
			mTakePhoto.setOnClickListener(this);
			mTakePhoto.setTypeface(LoginActivity.sTypeface);
			mTakePhoto.setText("TAKE PHOTO");

			mSubmitPhoto = (RaisedButton) rootView.findViewById(R.id.submit_photo);
			mSubmitPhoto.setOnClickListener(this);
			mSubmitPhoto.setTypeface(LoginActivity.sTypeface);
			mSubmitPhoto.setText(AppStrings.submit);
			mSubmitPhoto.setVisibility(View.INVISIBLE);

			mImageView = (ImageView) rootView.findViewById(R.id.imageview);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return rootView;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {
		case R.id.take_photo:

			takePhoto();
			break;

		case R.id.submit_photo:

			if (bitmap_ != null) {

				new GetResolution_Webservices(Meeting_Upload_Schedule_form.this, bitmap_).execute();
			} else {
				TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT, TastyToast.WARNING);
			}
			break;

		}

	}

	private void takePhoto() {

		dispatchTakePictureIntent();
	}

	private void dispatchTakePictureIntent() {
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		// Ensure that there's a camera activity to handle the intent
		if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
			// Create the File where the photo should go
			File photoFile = null;
			try {
				photoFile = createImageFile();
			} catch (IOException ex) {
				// Error occurred while creating the File

			}
			// Continue only if the File was successfully created
			if (photoFile != null) {

				Uri fileUri = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider",
						photoFile);

				takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
				startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
			}
		}
	}

	private File createImageFile() throws IOException {
		// Create an image file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		String imageFileName = "JPEG_" + timeStamp + "_";
		String storageDir = Environment.getExternalStorageDirectory() + "/picupload";
		File dir = new File(storageDir);
		if (!dir.exists())
			dir.mkdir();

		File image = new File(storageDir + "/" + imageFileName + ".jpg");

		// Save a file: path for use with ACTION_VIEW intents
		mCurrentPhotoPath = image.getAbsolutePath();
		Log.i("photo path = ", mCurrentPhotoPath);
		return image;
	}

	@SuppressWarnings("deprecation")
	private void setPic() {
		// Get the dimensions of the View
		int targetW = mImageView.getWidth();
		int targetH = mImageView.getHeight();

		// Get the dimensions of the bitmap
		BitmapFactory.Options bmOptions = new BitmapFactory.Options();
		bmOptions.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
		int photoW = bmOptions.outWidth;
		int photoH = bmOptions.outHeight;

		// Determine how much to scale down the image
		int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

		// Decode the image file into a Bitmap sized to fill the View
		bmOptions.inJustDecodeBounds = false;
		bmOptions.inSampleSize = scaleFactor << 1;
		bmOptions.inPurgeable = true;

		Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);

		try {

			actualImage = FileUtil.from(getActivity(), Uri.fromFile(new File(mCurrentPhotoPath)));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			String storageDir = Environment.getExternalStorageDirectory() + "/Files/Compressed";

			compressedImage = new Compressor(getActivity()).setMaxWidth(640).setMaxHeight(480).setQuality(35)
					.setCompressFormat(Bitmap.CompressFormat.WEBP)

					.setDestinationDirectoryPath(storageDir).compressToFile(actualImage);

		} catch (IOException e) {
			e.printStackTrace();
			showError(e.getMessage());
		}

		Matrix mtx = new Matrix();
		// mtx.postRotate(90);
		// Rotating Bitmap
		Bitmap rotatedBMP = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), mtx, true);

		if (rotatedBMP != bitmap)
			bitmap.recycle();

		mImageView.setImageBitmap(rotatedBMP);

		File f = new File(compressedImage.getPath());
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inPreferredConfig = Bitmap.Config.ARGB_8888;

		try {
			mTakePhoto.setEnabled(false);
			mSubmitPhoto.setVisibility(View.VISIBLE);
			bitmap_ = BitmapFactory.decodeStream(new FileInputStream(f), null, options);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == REQUEST_TAKE_PHOTO && resultCode == Activity.RESULT_OK) {

			setPic();
		}

	}

	public void showError(String errorMessage) {
		Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub
		mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
		mProgressDilaog.show();
	}

	@SuppressLint("LongLogTag")
	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub

		if (mProgressDilaog != null) {
			mProgressDilaog.dismiss();
			if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {
				getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub

						if (!result.equals("FAIL")) {

							TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg,
									TastyToast.LENGTH_SHORT, TastyToast.ERROR);
							Constants.NETWORKCOMMONFLAG = "SUCCESS";
						}

					}
				});

			} else {
				Log.e("Current Resution Values --->>", publicValues.mGetSHGResultionValues);

				if (publicValues.mGetSHGResultionValues != null && publicValues.mGetSHGResultionValues.equals("Yes")) {

					TastyToast.makeText(getActivity(), "IMAGE UPLOADED SUCCESSFULLY !!", TastyToast.LENGTH_SHORT,
							TastyToast.SUCCESS);
					MainFragment_Dashboard fragment_Dashboard = new MainFragment_Dashboard();
					setFragment(fragment_Dashboard);

				}

			}
		}

	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub

		if (EShaktiApplication.isDefault()) {
			PrefUtils.setStepWiseScreenCount("2");

		}

		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

	}
}