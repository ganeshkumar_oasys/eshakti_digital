package com.yesteam.eshakti.view.fragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.squareup.otto.Subscribe;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.adapter.CustomExpandableMenuListAdapter;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.interfaces.ExpandListItemClickListener;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;
import com.yesteam.eshakti.model.ListItem;
import com.yesteam.eshakti.model.RowItem;
import com.yesteam.eshakti.sqlite.database.response.GroupLoanOSResponse;
import com.yesteam.eshakti.sqlite.db.model.GetGroupMemberDetails;
import com.yesteam.eshakti.sqlite.db.model.GroupMasterSingle;
import com.yesteam.eshakti.sqlite.db.transactions.TransactionManager.DataType;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.views.MyExpandableListview;
import com.yesteam.eshakti.views.RaisedButton;
import com.yesteam.eshakti.webservices.Get_LoanOutstandingTask;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.TextView;

public class Transaction_GroupLoanRepaidMenuFragment extends Fragment implements TaskListener, OnItemClickListener, ExpandListItemClickListener {

	public static final String TAG = Transaction_GroupLoanRepaidMenuFragment.class.getSimpleName();
	private TextView mGroupName, mCashInHand, mCashAtBank;
	private Dialog mProgressDialog;
	public static List<RowItem> rowItems;
	public static String[] mGroupLoanNames;
	public static String mloan_Id;
	public static String mloan_Name;
	public static String mSendTo_ServerLoan_Id;

	public static String[] response;
	public static String mGroupOS_Offlineresponse = null;

	int size;

	//private ExpandableLayoutListView mListView;
	private MyExpandableListview mListView;
	private List<ListItem> listItems;
	//private CustomMenuListAdapter mAdapter;
	private CustomExpandableMenuListAdapter mAdapter;
	int listImage;
	private TextView mHeader;
	private ArrayList<HashMap<String, String>> childList;
	private int lastExpandedPosition = -1;
	Date date_dashboard, date_loanDisb;

	public Transaction_GroupLoanRepaidMenuFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_GROUPLOAN_REPAID_MENU;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		SBus.INST.register(this);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_menulist_expandable, container, false);

		MainFragment_Dashboard.isBackpressed = false;
		EShaktiApplication.setFragmentMenuListView(false);

		try {

			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashInHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashInHand.setTypeface(LoginActivity.sTypeface);

			mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashAtBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashAtBank.setTypeface(LoginActivity.sTypeface);

			mHeader = (TextView) rootView.findViewById(R.id.expandableSubmenuHeaderTextview);
			mHeader.setVisibility(View.VISIBLE);
			mHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.grouploanrepayment));
			mHeader.setTypeface(LoginActivity.sTypeface);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		try {

			size = SelectedGroupsTask.loan_Name.size() - 1;
			System.out.println("Loan_Name Size:" + size);

			mGroupLoanNames = new String[size];

			for (int i = 0; i < mGroupLoanNames.length; i++) {

				String loanName = SelectedGroupsTask.loan_Name.elementAt(i).toString();
				Log.d(TAG, "Loan Name:" + loanName);
				Log.d(TAG, "Loan Name:" + RegionalConversion
						.getRegionalConversion(SelectedGroupsTask.loan_Name.elementAt(i).toString()));
				mGroupLoanNames[i] = loanName;
				Log.d(TAG, "GroupLoanName:>>>" + mGroupLoanNames[i]);
			}

			listItems = new ArrayList<ListItem>();
			/*mListView = (ExpandableLayoutListView) rootView.findViewById(R.id.fragment_List_loanrepaid);
			mListView.setOnItemClickListener(this);*/
			mListView = (MyExpandableListview) rootView.findViewById(R.id.fragment_List_loanrepaid);
			
			listImage = R.drawable.ic_navigate_next_white_24dp;

			// rowItems = new ArrayList<RowItem>();
			for (int i = 0; i < mGroupLoanNames.length; i++) {
				ListItem rowItem = new ListItem(mGroupLoanNames[i].toString(), listImage);
				listItems.add(rowItem);
			}
			
			childList = new ArrayList<HashMap<String,String>>();
			
			for (int i = 0; i < mGroupLoanNames.length; i++) {
					HashMap<String, String> temp = new HashMap<String, String>();
					
					temp.put("AccNo", SelectedGroupsTask.loanAcc_loanAccNo.elementAt(i));
					temp.put("BankName", SelectedGroupsTask.loanAcc_bankName.elementAt(i));
					String[] disbursementDateArr = SelectedGroupsTask.loanAcc_loanDisbursementDate.elementAt(i).split("/");
					temp.put("DisbursementDate", disbursementDateArr[1] + "/" + disbursementDateArr[0] + "/" + disbursementDateArr[2]);
					childList.add(temp);
			}

			//mAdapter = new CustomMenuListAdapter(getActivity(), listItems);
			mAdapter = new CustomExpandableMenuListAdapter(getActivity(), listItems, childList, this);
			mListView.setAdapter(mAdapter);
			
			mListView.setOnGroupExpandListener(new OnGroupExpandListener() {
				
				@Override
				public void onGroupExpand(int groupPosition) {
					// TODO Auto-generated method stub
					if (lastExpandedPosition != -1 && groupPosition != lastExpandedPosition) {
						mListView.collapseGroup(lastExpandedPosition);
					}
					lastExpandedPosition = groupPosition;
				}
			});

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return rootView;
	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub

		mProgressDialog = AppDialogUtils.createProgressDialog(getActivity());
		mProgressDialog.show();

	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub

		try {

			if (mProgressDialog != null) {
				mProgressDialog.dismiss();
				if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {
					getActivity().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub

							if (!result.equals("FAIL")) {

								TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg,
										TastyToast.LENGTH_SHORT, TastyToast.ERROR);
								Constants.NETWORKCOMMONFLAG = "SUCCESS";
							}

						}
					});
				} else {
					EShaktiApplication.getInstance().getTransactionManager().startTransaction(
							DataType.GET_GROUPMASTER_GLOS, new GroupMasterSingle(SelectedGroupsTask.Group_Id));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Subscribe
	public void OnGroupLoanOSResponse(final GroupLoanOSResponse groupLoanOSResponse) {
		switch (groupLoanOSResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), groupLoanOSResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), groupLoanOSResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:
			String mGroupLoanOSResponse = GetGroupMemberDetails.getGrouploanOS();
			Log.e("Group response@@@@@@@", mGroupLoanOSResponse + "");
			if (mGroupLoanOSResponse != null && !mGroupLoanOSResponse.equals("")) {

				response = mGroupLoanOSResponse.split("%");
				for (int i = 0; i < response.length; i++) {
					System.out.println("Response : " + response[i]);

					String secondResponse[] = response[i].split("#");
					System.out.println("LOAN ID : " + secondResponse[0]);
					Log.e("LOAN ID : ", secondResponse[0]);

					if (mSendTo_ServerLoan_Id.equals(secondResponse[0])) {

						System.out.println("LOAN OUTS : " + secondResponse[1]);
						mGroupOS_Offlineresponse = secondResponse[1];
						Get_LoanOutstandingTask.sGet_Loan_outstanding_ServiceResponse = mGroupOS_Offlineresponse;
						Log.e("Group Os Values----->>>", mGroupOS_Offlineresponse);
						System.out.println("LOAN OUTS 121212121: "
								+ Get_LoanOutstandingTask.sGet_Loan_outstanding_ServiceResponse);
					}

				}

				Transaction_GroupLoanRepaidFragment fragment = new Transaction_GroupLoanRepaidFragment();
				setFragment(fragment);
			} else {
				TastyToast.makeText(getActivity(), AppStrings.loginAgainAlert, TastyToast.LENGTH_LONG,
						TastyToast.WARNING);
			}
			break;
		}

	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		SBus.INST.unRegister(this);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
		// TODO Auto-generated method stub

		try {

			RaisedButton button = (RaisedButton) view.findViewById(R.id.fragment_menulist_next_button);
			TextView mAccNo = (TextView) view.findViewById(R.id.accNumber);
			TextView mAccNo_Values = (TextView) view.findViewById(R.id.AccNumber_values);
			TextView mBankName = (TextView) view.findViewById(R.id.bankName);
			TextView mBankNameValues = (TextView) view.findViewById(R.id.bankName_values);
			TextView mdisbursementTime = (TextView) view.findViewById(R.id.disbursementTime);
			TextView mdisbursementTimeValues = (TextView) view.findViewById(R.id.disbursementTime_values);

			mAccNo.setText(AppStrings.mAccountNumber + " :  ");
			mAccNo.setPadding(5, 5, 5, 5);
			mAccNo_Values.setText(SelectedGroupsTask.loanAcc_loanAccNo.get(position));
			mAccNo_Values.setPadding(5, 5, 5, 5);
			mAccNo.setTypeface(LoginActivity.sTypeface);
			mAccNo_Values.setTypeface(LoginActivity.sTypeface);

			mBankName.setText(AppStrings.bankName + " :  ");
			mBankName.setPadding(5, 5, 5, 5);
			mBankNameValues.setText(SelectedGroupsTask.loanAcc_bankName.get(position));
			mBankNameValues.setPadding(5, 5, 5, 5);
			mBankName.setTypeface(LoginActivity.sTypeface);
			mBankNameValues.setTypeface(LoginActivity.sTypeface);

			mdisbursementTime.setText(AppStrings.mLoanDisbursementDate + " :  ");
			mdisbursementTime.setPadding(5, 5, 5, 5);
			mdisbursementTimeValues.setText(SelectedGroupsTask.loanAcc_loanDisbursementDate.get(position));
			mdisbursementTimeValues.setPadding(5, 5, 5, 5);
			mdisbursementTime.setTypeface(LoginActivity.sTypeface);
			mdisbursementTimeValues.setTypeface(LoginActivity.sTypeface);

			mAccNo.setTextColor(Color.BLACK);
			mAccNo_Values.setTextColor(Color.BLACK);
			mBankName.setTextColor(Color.BLACK);
			mBankNameValues.setTextColor(Color.BLACK);

			mdisbursementTime.setTextColor(Color.BLACK);
			mdisbursementTimeValues.setTextColor(Color.BLACK);

			button.setTypeface(LoginActivity.sTypeface);
			button.setText(RegionalConversion.getRegionalConversion(AppStrings.mSelect));

			button.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					try {
						mloan_Id = SelectedGroupsTask.loan_Id.elementAt(position).toString();
						System.out.println("Loan Id" + mloan_Id);

						mloan_Name = SelectedGroupsTask.loan_Name.elementAt(position).toString();
						System.out.println("Loan Name:" + mloan_Name);

						String temp_loanName = SelectedGroupsTask.loan_Name.elementAt(position).toString();

						mSendTo_ServerLoan_Id = SelectedGroupsTask.loan_Id.elementAt(position).toString();

						EShaktiApplication.setLoanId(String.valueOf(mloan_Id));

						if (mloan_Name.equals(temp_loanName)) {

							if (ConnectionUtils.isNetworkAvailable(getActivity())) {

								if (PrefUtils.getGroupMasterResValues() != null
										&& PrefUtils.getGroupMasterResValues().equals("1")) {

									EShaktiApplication.getInstance().getTransactionManager().startTransaction(
											DataType.GET_GROUPMASTER_GLOS,
											new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));

								} else {

									new Get_LoanOutstandingTask(Transaction_GroupLoanRepaidMenuFragment.this).execute();
								}
							} else {
								// DO OFFLINE STUFFS

								if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
									EShaktiApplication.getInstance().getTransactionManager().startTransaction(
											DataType.GET_GROUPMASTER_GLOS,
											new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));
								} else {
									TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF,
											TastyToast.LENGTH_SHORT, TastyToast.ERROR);
								}

							}

						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub
		Log.e("Current Class Name!!!!!!!!!!!!!!", fragment.getClass().getName());
		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();
		
		FragmentDrawer.drawer_CashinHand.setText(RegionalConversion.getRegionalConversion(AppStrings.cashinhand)

				+ SelectedGroupsTask.sCashinHand);
		FragmentDrawer.drawer_CashinHand.setTypeface(LoginActivity.sTypeface);

		 
		FragmentDrawer.drawer_CashatBank.setText(RegionalConversion.getRegionalConversion(AppStrings.cashatBank)

				+ SelectedGroupsTask.sCashatBank);
		FragmentDrawer.drawer_CashatBank.setTypeface(LoginActivity.sTypeface);
		
		FragmentDrawer.drawer_lastTR_Date.setText(RegionalConversion.getRegionalConversion(AppStrings.lastTransactionDate)
				+ " : " + SelectedGroupsTask.sLastTransactionDate_Response);
		FragmentDrawer.drawer_lastTR_Date.setTypeface(LoginActivity.sTypeface);

	}

	@Override
	public void onItemClick(ViewGroup parent, View view, int position) {
		// TODO Auto-generated method stub

	//	String dashBoardDate = DatePickerDialog.sDashboardDate;
		
		String dashBoardDate = null;
		if (DatePickerDialog.sDashboardDate.contains("-")) {
			dashBoardDate = DatePickerDialog.sDashboardDate;
		} else if (DatePickerDialog.sDashboardDate.contains("/")) {
			String[] dateArr = DatePickerDialog.sDashboardDate.split("/");
			dashBoardDate = dateArr[0] + "-" + dateArr[1] + "-" + dateArr[2];;
		}
		
		String loanDisbArr[] = SelectedGroupsTask.loanAcc_loanDisbursementDate.elementAt(position).split("/");
		EShaktiApplication.setLoanDisbursementDate(SelectedGroupsTask.loanAcc_loanDisbursementDate.elementAt(position));
		String loanDisbDate = loanDisbArr[1]+"-"+loanDisbArr[0]+"-"+loanDisbArr[2];
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		
		try {
			date_dashboard = sdf.parse(dashBoardDate);
			date_loanDisb = sdf.parse(loanDisbDate);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if (date_dashboard.compareTo(date_loanDisb) >= 0) {
		try {
			mloan_Id = SelectedGroupsTask.loan_Id.elementAt(position).toString();
			System.out.println("Loan Id" + mloan_Id);

			mloan_Name = SelectedGroupsTask.loan_Name.elementAt(position).toString();
			System.out.println("Loan Name:" + mloan_Name);

			String temp_loanName = SelectedGroupsTask.loan_Name.elementAt(position).toString();

			mSendTo_ServerLoan_Id = SelectedGroupsTask.loan_Id.elementAt(position).toString();

			EShaktiApplication.setLoanId(String.valueOf(mloan_Id));

			if (mloan_Name.equals(temp_loanName)) {

				if (ConnectionUtils.isNetworkAvailable(getActivity())) {

					if (PrefUtils.getGroupMasterResValues() != null
							&& PrefUtils.getGroupMasterResValues().equals("1")) {

						EShaktiApplication.getInstance().getTransactionManager().startTransaction(
								DataType.GET_GROUPMASTER_GLOS,
								new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));
						EShaktiApplication.setGroupLoanRepaymentLoanBankName(SelectedGroupsTask.loanAcc_bankName.elementAt(position));
						EShaktiApplication.setGroupLoanRepaymentAccNo(SelectedGroupsTask.loanAcc_loanAccNo.elementAt(position));

					} else {

						new Get_LoanOutstandingTask(Transaction_GroupLoanRepaidMenuFragment.this).execute();
						EShaktiApplication.setGroupLoanRepaymentLoanBankName(SelectedGroupsTask.loanAcc_bankName.elementAt(position));
						EShaktiApplication.setGroupLoanRepaymentAccNo(SelectedGroupsTask.loanAcc_loanAccNo.elementAt(position));
					}
				} else {
					// DO OFFLINE STUFFS

					if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
						EShaktiApplication.getInstance().getTransactionManager().startTransaction(
								DataType.GET_GROUPMASTER_GLOS,
								new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));
						EShaktiApplication.setGroupLoanRepaymentLoanBankName(SelectedGroupsTask.loanAcc_bankName.elementAt(position));
						EShaktiApplication.setGroupLoanRepaymentAccNo(SelectedGroupsTask.loanAcc_loanAccNo.elementAt(position));
					} else {
						TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF,
								TastyToast.LENGTH_SHORT, TastyToast.ERROR);
					}

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		} else {
			TastyToast.makeText(getActivity(), AppStrings.mCheckDisbursementDate, TastyToast.LENGTH_SHORT, TastyToast.WARNING);
		}
	}

	@Override
	public void onItemClickVerification(ViewGroup parent, View view, int position) {

	}
}
