package com.yesteam.eshakti.view.fragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.squareup.otto.Subscribe;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.sqlite.db.model.Transaction;
import com.yesteam.eshakti.sqlite.db.transactions.TransactionManager.DataType;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Offline_TransactionDateFragment extends Fragment implements OnClickListener {

	private TextView mGroupName, mCashInHand, mCashAtBank, mHeadertext;
	int size;
	private LinearLayout linearLayout;
	private static Button mDateButton;

	public static String sSelectedTransDate;
	String getResponse;
	private static String[] responseArr, response$Arr;
	String dateArr[];
	public static String transDate;
	String date;
	String type;

	public Offline_TransactionDateFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		SBus.INST.register(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.offline_datelist, container, false);

		MainFragment_Dashboard.isBackpressed = false;

		try {

			EShaktiApplication.setOfflineTransDate(true);
			EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GET_TRANS_MASTERVALUES,
					null);

			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashInHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashInHand.setTypeface(LoginActivity.sTypeface);

			mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashAtBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashAtBank.setTypeface(LoginActivity.sTypeface);

			mHeadertext = (TextView) rootView.findViewById(R.id.offline_headertext);
			mHeadertext.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.offlineReports)));
			mHeadertext.setTypeface(LoginActivity.sTypeface);

			linearLayout = (LinearLayout) rootView.findViewById(R.id.offline_button_linear);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return rootView;

	}

	@SuppressWarnings("deprecation")
	@Subscribe
	public void OnValues(final ArrayList<Transaction> arrayList) {
		Log.e("CHECKING !23", "YES DONE4545");

		if (arrayList.size() != 0) {
			Log.v("Current Array List Size", arrayList.size() + "");
			try {

				if (OfflineReports_TransactionList.selectedItem.equals(AppStrings.savings)) {
					date = "";
					for (int i = 0; i < arrayList.size(); i++) {
						getResponse = "";
						if (Offline_ReportDateListFragment.sSelectedTransDate
								.equals(arrayList.get(i).getTransactionDate())) {

							if (Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.savings)) {
								if (arrayList.get(i).getSavings() != null) {
									getResponse = arrayList.get(i).getSavings();
									System.out.println("<<<<<getResponse>>>>>>>>>" + getResponse.toString());
									responseArr = getResponse.split("[~#]");
									getDate();
								}

							} else if (Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.voluntarySavings)) {
								if (arrayList.get(i).getVolunteerSavings() != null) {
									getResponse = arrayList.get(i).getVolunteerSavings();
									System.out.println("<<<<<getResponse>>>>>>>>>" + getResponse.toString());
									responseArr = getResponse.split("[~#]");
									getDate();
								}

							}
						}
					}

				} else if (OfflineReports_TransactionList.selectedItem.equals(AppStrings.memberloanrepayment)) {
					date = "";
					for (int i = 0; i < arrayList.size(); i++) {
						getResponse = "";
						if (Offline_ReportDateListFragment.sSelectedTransDate
								.equals(arrayList.get(i).getTransactionDate())) {

							/*
							 * if (Offline_SubMenuList.selectedSubMenuItem
							 * .equals(Offline_SubMenuList.memberLoanName.
							 * toString())) { if
							 * (arrayList.get(i).getMemberloanRepayment() !=
							 * null) { getResponse = arrayList.get(i)
							 * .getMemberloanRepayment();
							 * System.out.println("<<<<<getResponse>>>>>>>>>" +
							 * getResponse.toString()); responseArr =
							 * getResponse.split("[~#]"); getDate(); }
							 */
							if (!(Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.InternalLoan))) {
								if (arrayList.get(i).getMemberloanRepayment() != null) {
									getResponse = arrayList.get(i).getMemberloanRepayment();
									System.out.println("<<<<<getResponse>>>>>>>>>" + getResponse.toString());
									response$Arr = getResponse.split("[$]");

									for (int j = 0; j < response$Arr.length; j++) {
										String string = response$Arr[j];
										Log.v("!!!!!!!!!!!!!", string);
										String string2[] = string.split("#");
										/*
										 * for (int k = 0; k < string2.length;
										 * k++) { Log.v("string2[]",
										 * string2[k]+" pos "+k); }
										 */
										if (Offline_SubMenuList.selectedItemPos != Offline_SubMenuList.subMenuItemList.length) {
											if (string2[1].equals(
													Offline_SubMenuList.LoanIdArr[Offline_SubMenuList.selectedItemPos])) {
												responseArr = string.split("[~#]");
												getDate();
											}
										}

									}
								}

							} else if (Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.InternalLoan)) {
								if (arrayList.get(i).getPersonalloanRepayment() != null) {
									getResponse = arrayList.get(i).getPersonalloanRepayment();
									System.out.println("<<<<<getResponse>>>>>>>>>" + getResponse.toString());
									responseArr = getResponse.split("[~#]");
									getDate();
								}
							}
						} else if (Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.InternalLoan)) {
							if (arrayList.get(i).getPersonalloanRepayment() != null) {
								getResponse = arrayList.get(i).getPersonalloanRepayment();
								System.out.println("<<<<<getResponse>>>>>>>>>" + getResponse.toString());
								responseArr = getResponse.split("[~#]");
								getDate();
							}
						}
					}

				} else if (OfflineReports_TransactionList.selectedItem.equals(AppStrings.expenses)) {
					date = "";
					for (int i = 0; i < arrayList.size(); i++) {
						getResponse = "";
						if (Offline_ReportDateListFragment.sSelectedTransDate
								.equals(arrayList.get(i).getTransactionDate())) {

							if (arrayList.get(i).getExpenses() != null) {
								getResponse = arrayList.get(i).getExpenses();
								System.out.println("<<<<<getResponse>>>>>>>>>" + getResponse.toString());
								responseArr = getResponse.split("[~#]");
								getDate();
							}
						}
					}

				} else if (OfflineReports_TransactionList.selectedItem.equals(AppStrings.income)) {
					date = "";
					if (Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.otherincome)
							|| Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.subscriptioncharges)
							|| Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.penalty)) {
						for (int i = 0; i < arrayList.size(); i++) {
							getResponse = "";
							if (Offline_ReportDateListFragment.sSelectedTransDate
									.equals(arrayList.get(i).getTransactionDate())) {

								if (Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.otherincome)) {
									if (arrayList.get(i).getOtherIncome() != null) {
										getResponse = arrayList.get(i).getOtherIncome();
										System.out.println("<<<<<getResponse>>>>>>>>>" + getResponse.toString());
										responseArr = getResponse.split("[~#]");
										// getDate();
										dateArr = String.valueOf(responseArr[responseArr.length - 3]).split("/");
										transDate = dateArr[1] + "/" + dateArr[0] + "/" + dateArr[2];
										date = date + transDate + "~";

									}
								} else if (Offline_SubMenuList.selectedSubMenuItem
										.equals(AppStrings.subscriptioncharges)) {
									if (arrayList.get(i).getSubscriptionIncome() != null) {
										getResponse = arrayList.get(i).getSubscriptionIncome();
										System.out.println("<<<<<getResponse>>>>>>>>>" + getResponse.toString());
										responseArr = getResponse.split("[~#]");
										getDate();
									}
								} else if (Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.penalty)) {
									if (arrayList.get(i).getPenaltyIncome() != null) {
										getResponse = arrayList.get(i).getPenaltyIncome();
										System.out.println("<<<<<getResponse>>>>>>>>>" + getResponse.toString());
										responseArr = getResponse.split("[~#]");
										getDate();
									}
								}
							}

						}
					} else if (Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.donation)
							|| Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.federationincome)
							|| Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.mSeedFund)) {
						for (int i = 0; i < arrayList.size(); i++) {
							getResponse = "";
							if (Offline_ReportDateListFragment.sSelectedTransDate
									.equals(arrayList.get(i).getTransactionDate())) {

								if (Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.donation)) {
									if (arrayList.get(i).getDonationIncome() != null) {
										getResponse = arrayList.get(i).getDonationIncome();
										System.out.println("<<<<<getResponse>>>>>>>>>" + getResponse.toString());
										responseArr = getResponse.split("#");
										getDate();
									}
								} else if (Offline_SubMenuList.selectedSubMenuItem
										.equals(AppStrings.federationincome)) {
									if (arrayList.get(i).getFederationIncome() != null) {
										getResponse = arrayList.get(i).getFederationIncome();
										System.out.println("<<<<<getResponse>>>>>>>>>" + getResponse.toString());
										responseArr = getResponse.split("#");
										getDate();
									}
								} else if (Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.mSeedFund)) {
									if (arrayList.get(i).getSeedFund() != null) {
										getResponse = arrayList.get(i).getSeedFund();
										System.out.println("<<<<<getResponse>>>>>>>>>" + getResponse.toString());
										responseArr = getResponse.split("#");
										getDate();
									}
								}
							}

						}
					}

				} /*
					 * else if (Offline_SubMenuList.selectedSubMenuItem
					 * .equals(String.valueOf(Offline_SubMenuList.groupLoanName)
					 * )) { date = ""; for (int j = 0; j < arrayList.size();
					 * j++) { getResponse = ""; if
					 * (arrayList.get(j).getGrouploanrepayment() != null) {
					 * getResponse = arrayList.get(j) .getGrouploanrepayment();
					 * System.out.println("<<<<<getResponse>>>>>>>>>" +
					 * getResponse.toString()); responseArr =
					 * getResponse.split("[~#]"); getDate(); } } }
					 */ else if (OfflineReports_TransactionList.selectedItem
						.equals(String.valueOf(AppStrings.grouploanrepayment))) {
					date = "";
					for (int i = 0; i < arrayList.size(); i++) {
						getResponse = "";
						type = "";
						if (Offline_ReportDateListFragment.sSelectedTransDate
								.equals(arrayList.get(i).getTransactionDate())) {

							if (arrayList.get(i).getGrouploanrepayment() != null) {
								getResponse = arrayList.get(i).getGrouploanrepayment();
								System.out.println("<<<<<getResponse>>>>>>>>>" + getResponse.toString());
								response$Arr = getResponse.split("[$]");

								for (int j = 0; j < response$Arr.length; j++) {
									String string = response$Arr[j];
									Log.v("!!!!!!!!!!!!!", string);
									String string2[] = string.split("#");
									/*
									 * for (int k = 0; k < string2.length; k++)
									 * { Log.v("string2[]",
									 * string2[k]+" pos "+k); }
									 */
									if (string2[1].equals(
											Offline_SubMenuList.LoanIdArr[Offline_SubMenuList.selectedItemPos])) {
										responseArr = string.split("[~#]");
										
										type = responseArr[responseArr.length-1];
										getDate();
									}
								}

							}
						}

					}
				} else if (OfflineReports_TransactionList.selectedItem.equals(AppStrings.bankTransaction)) {
					date = "";
					if (Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.bankTransaction)) {
						for (int i = 0; i < arrayList.size(); i++) {
							getResponse = "";
							if (Offline_ReportDateListFragment.sSelectedTransDate
									.equals(arrayList.get(i).getTransactionDate())) {

								if (arrayList.get(i).getBankDeposit() != null) {
									getResponse = arrayList.get(i).getBankDeposit();
									System.out.println("<<<<<getResponse>>>>>>>>>" + getResponse.toString());
									responseArr = getResponse.split("[~#]");
									getDate();
								}
							}

						}
					} else if (Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.fixedDeposit)) {
						for (int i = 0; i < arrayList.size(); i++) {
							getResponse = "";
							if (Offline_ReportDateListFragment.sSelectedTransDate
									.equals(arrayList.get(i).getTransactionDate())) {

								if (arrayList.get(i).getBankFixedDeposit() != null) {
									getResponse = arrayList.get(i).getBankFixedDeposit();
									System.out.println("<<<<<getResponse>>>>>>>>>" + getResponse.toString());
									responseArr = getResponse.split("[~#]");
									getDate();
								}
							}
						}
					} else if (Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.mLoanaccHeader)) {
						for (int i = 0; i < arrayList.size(); i++) {
							getResponse = "";
							if (Offline_ReportDateListFragment.sSelectedTransDate
									.equals(arrayList.get(i).getTransactionDate())) {

								if (arrayList.get(i).getLoanWithdrawal() != null) {
									getResponse = arrayList.get(i).getLoanWithdrawal();
									System.out.println("<<<<<getResponse>>>>>>>>>" + getResponse.toString());
									responseArr = getResponse.split("[~#]");
									getDate();
								}
							}
						}
					} else if (Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.mAccountToAccountTransfer)) {
						for (int i = 0; i < arrayList.size(); i++) {
							getResponse = "";
							if (Offline_ReportDateListFragment.sSelectedTransDate
									.equals(arrayList.get(i).getTransactionDate())) {

								if (arrayList.get(i).getAcctoAccTransfer() != null) {
									getResponse = arrayList.get(i).getAcctoAccTransfer();
									System.out.println("<<<<<getResponse>>>>>>>>>" + getResponse.toString());
									responseArr = getResponse.split("[~#]");
									getDate();
								}
							}
						}

					} else if (Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.mLoanAccTransfer)) {
						for (int i = 0; i < arrayList.size(); i++) {
							getResponse = "";
							
							if (Offline_ReportDateListFragment.sSelectedTransDate.equals(arrayList.get(i).getTransactionDate())) {
								if (arrayList.get(i).getAccToLoanAccTransfer() != null) {
									getResponse = arrayList.get(i).getAccToLoanAccTransfer();
									System.out.println("<<<<<getResponse>>>>>>>>>" + getResponse.toString());
									responseArr = getResponse.split("[~#]");
									getDate();
								}
							}
						}
					}

				} else if (OfflineReports_TransactionList.selectedItem.equals(AppStrings.InternalLoanDisbursement)) {
					date = "";
					for (int i = 0; i < arrayList.size(); i++) {
						getResponse = "";
						if (Offline_ReportDateListFragment.sSelectedTransDate
								.equals(arrayList.get(i).getTransactionDate())) {

							if (arrayList.get(i).getPersonalloandisbursement() != null) {
								getResponse = arrayList.get(i).getPersonalloandisbursement();
								System.out.println("<<<<<getResponse>>>>>>>>>" + getResponse.toString());
								responseArr = getResponse.split("[~#]");
								getDate();
							}
						}
					}
				} else if (Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.Attendance)) {
					date = "";
					for (int i = 0; i < arrayList.size(); i++) {
						getResponse = "";
						if (Offline_ReportDateListFragment.sSelectedTransDate
								.equals(arrayList.get(i).getTransactionDate())) {

							if (arrayList.get(i).getAttendance() != null) {
								getResponse = arrayList.get(i).getAttendance();
								System.out.println("<<<<<<<<<<<<getResponse>>>>>>>>>" + getResponse.toString());
								responseArr = getResponse.split("[~#]");
								getDate();
							}
						}
					}
				} else if (Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.MinutesofMeeting)) {
					date = "";
					for (int i = 0; i < arrayList.size(); i++) {
						if (Offline_ReportDateListFragment.sSelectedTransDate
								.equals(arrayList.get(i).getTransactionDate())) {

							if (arrayList.get(i).getMinutesofmeeting() != null) {
								getResponse = "";

								getResponse = arrayList.get(i).getMinutesofmeeting();
								System.out.println("<<<<<<<<<<<<getResponse>>>>>>>>>>" + getResponse.toString());
								responseArr = getResponse.split("[~#]");
								getDate();
							}
						}
					}
				} else if (Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.auditing)) {
					date = "";
					for (int i = 0; i < arrayList.size(); i++) {
						getResponse = "";
						if (Offline_ReportDateListFragment.sSelectedTransDate
								.equals(arrayList.get(i).getTransactionDate())) {

							if (arrayList.get(i).getAuditting() != null) {
								getResponse = arrayList.get(i).getAuditting();
								System.out.println("<<<<<<<<<<<<getResponse>>>>>>>>>>" + getResponse.toString());
								responseArr = getResponse.split("[~#]");
								getDate();
							}
						}
					}
				} else if (Offline_SubMenuList.selectedSubMenuItem.equals(AppStrings.training)) {
					date = "";
					for (int i = 0; i < arrayList.size(); i++) {
						getResponse = "";
						if (Offline_ReportDateListFragment.sSelectedTransDate
								.equals(arrayList.get(i).getTransactionDate())) {

							if (arrayList.get(i).getTraining() != null) {
								getResponse = arrayList.get(i).getTraining();
								System.out.println("<<<<<<<<<<<<getResponse>>>>>>>>>>" + getResponse.toString());
								responseArr = getResponse.split("[~#]");
								getDate();
							}
						}
					}
				}

				String[] withDupDateList = date.split("~");
				for (int i = 0; i < withDupDateList.length; i++) {
					System.out.println("----------withDupDateList[]---------" + withDupDateList[i] + " i pos " + i);
				}

				List<String> list = Arrays.asList(withDupDateList);
				Set<String> set = new LinkedHashSet<String>(list);

				String[] withoutDupDateList = new String[set.size()];
				set.toArray(withoutDupDateList);

				for (int i = 0; i < withoutDupDateList.length; i++) {
					System.out
							.println("----------withoutDupDateList[]---------" + withoutDupDateList[i] + " i pos " + i);
					if ((withoutDupDateList[i] != null) && (!withoutDupDateList[i].equals(""))) {

						LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,
								LayoutParams.WRAP_CONTENT);
						params.setMargins(0, 5, 0, 5);

						mDateButton = new Button(getActivity());
						mDateButton.setBackground(getResources().getDrawable(R.drawable.custom_button));
						mDateButton.setId(i);
						/*
						 * mDateButton.setText(GetSpanText.getSpanString(
						 * getActivity(),
						 * String.valueOf(withoutDupDateList[i])));
						 */
						mDateButton.setText(
								RegionalConversion.getRegionalConversion(String.valueOf(withoutDupDateList[i])));
						mDateButton.setTextColor(Color.WHITE);
						mDateButton.setTypeface(LoginActivity.sTypeface);
						mDateButton.setOnClickListener(this);

						linearLayout.addView(mDateButton, params);

					}

				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		} else {
			/*Toast.makeText(getActivity(), "There is no Local DB Values", Toast.LENGTH_LONG).show();*/
			TastyToast.makeText(getActivity(), "There is no Local DB Values", TastyToast.LENGTH_SHORT, TastyToast.ERROR);
		}
	}

	private void getDate() {
		// TODO Auto-generated method stub
		
		if (OfflineReports_TransactionList.selectedItem
						.equals(String.valueOf(AppStrings.grouploanrepayment))) {
			
			if (type.equals("Cash")) {
				dateArr = String.valueOf(responseArr[responseArr.length - 3]).split("/");
				transDate = dateArr[1] + "/" + dateArr[0] + "/" + dateArr[2];
				date = date + transDate + "~";
			} else if (type.equals("Bank")) {
				dateArr = String.valueOf(responseArr[responseArr.length - 3]).split("/");
				transDate = dateArr[1] + "/" + dateArr[0] + "/" + dateArr[2];
				date = date + transDate + "~";
			}
			
		} else {
		
			dateArr = String.valueOf(responseArr[responseArr.length - 2]).split("/");
			transDate = dateArr[1] + "/" + dateArr[0] + "/" + dateArr[2];
			date = date + transDate + "~";
		}

	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		SBus.INST.unRegister(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		sSelectedTransDate = ((Button) v).getText().toString();

		System.out.println("---------------------SELECTED TRANS DATE ----------------" + sSelectedTransDate);
		Fragment fragment = new Offline_Reports_Summary();
		getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame, fragment).addToBackStack(null)
				.commit();

	}
}
