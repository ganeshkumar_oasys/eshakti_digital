package com.yesteam.eshakti.view.activity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import com.oasys.eshakti.digitization.R;
import com.yesteam.eshakti.appConstants.Constants;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.media.AudioManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;

@SuppressWarnings("deprecation")
public class CapturingImageActivity extends Activity implements
		SurfaceHolder.Callback, AutoFocusCallback {
	private Camera mCamera;
	private SurfaceView mSurfaceView;
	private SurfaceHolder mSurfaceHolder;
	private ShutterCallback mShutterCallback;
	private PictureCallback mJpegCallback;
	private LinearLayout mCaptureImageLayout, mSaveImageLayout;
	private byte[] mImageData;
	private String mFileName;
	private ImageView mCapturePictureIV;
	private boolean isAutoFocusSupported = false;
	@SuppressWarnings("unused")
	private Context mContext;
	
//	private ScheduledExecutorService myScheduledExecutorService;
	@SuppressWarnings("unused")
	private AutoFocusCallback mAutoFocus;

	/** Called when the activity is first created. */

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		System.out.println("Inside the cameraCapture activity!!!!!!");
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_capture_image);
		mContext = this;
		if (getIntent() != null) {
			mFileName = getIntent().getStringExtra(Constants.FILE_PATH);
		}

		mSurfaceView = (SurfaceView) findViewById(R.id.surfaceView);
		mSurfaceHolder = mSurfaceView.getHolder();

		mCaptureImageLayout = (LinearLayout) findViewById(R.id.capture);
		mSaveImageLayout = (LinearLayout) findViewById(R.id.save_picture_layout);
		mCapturePictureIV = (ImageView) findViewById(R.id.capture_image_iv);
		// Install a SurfaceHolder.Callback so we get notified when the
		// underlying surface is created and destroyed.
		mSurfaceHolder.addCallback(this);

		// deprecated setting, but required on Android versions prior to 3.0
		mSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

		mJpegCallback = new PictureCallback() {
			@SuppressLint({ "SdCardPath", "ShowToast" })
			public void onPictureTaken(byte[] data, Camera camera) {
				mCaptureImageLayout.setVisibility(View.GONE);
				mSaveImageLayout.setVisibility(View.VISIBLE);
				mImageData = data;
			}
		};

		mShutterCallback = new ShutterCallback() {

			@Override
			public void onShutter() {
				AudioManager mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
				mAudioManager.playSoundEffect(AudioManager.FLAG_PLAY_SOUND);
			}
		};

		mCapturePictureIV.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					if (isAutoFocusSupported) {
						setCameraAutoFocus();
					} else {
						try {
							mCamera.takePicture(mShutterCallback, null,
									mJpegCallback);
						} catch (Exception e) {

						}
					}
				} catch (Exception e) {
					System.err.println("Error Message :" + e.getMessage());
				}
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		SurfaceHolder surfaceHolder = mSurfaceView.getHolder();
		surfaceHolder.addCallback(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		System.out.println("Inside the onPause()!!!!");
	}

	public void discardImage(View view) {
		System.out.println("Inside the discardImage()!!!!!");
		mCaptureImageLayout.setVisibility(View.VISIBLE);
		mSaveImageLayout.setVisibility(View.GONE);
		refreshCamera();
	}

	private void setCameraAutoFocus() {
		AutoFocusCallback mAutoFocus = new AutoFocusCallback() {

			@Override
			public void onAutoFocus(boolean success, final Camera camera) {
				try {
					System.out.println("Camera Focus : " + success);
					// if(success){
					// camera.cancelAutoFocus();
					// runOnUiThread(new Runnable() {
					//
					// @Override
					// public void run() {
					// mCamera.takePicture(mShutterCallback, null,
					// mJpegCallback);
					// }
					// });
					//
					// }
					camera.cancelAutoFocus();
					takePic();
				} catch (Exception e) {
					// Configuration.warningAlertDialog(mContext, "",
					// "Error Occuredd"+e);
					takePic();
				}
			}
		};
		mCamera.autoFocus(mAutoFocus);
	}

	private void takePic() {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				mCamera.takePicture(mShutterCallback, null, mJpegCallback);
			}
		});
	}

	@SuppressLint("SdCardPath")
	public void saveImage(View view) {
		System.out.println("Inside the saveImage()!!!!!");
		FileOutputStream outStream = null;
		try {
			File file = new File(mFileName);
			outStream = new FileOutputStream(file);
			outStream.write(mImageData);
			outStream.close();
			Intent data = new Intent().putExtra(Constants.FILE_PATH, mFileName);
			data.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			setResult(Constants.RESULT_OK, data);
			finish();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("SaveImage block :" + e);
		} finally {
		}
	}

	public void refreshCamera() {
		System.out.println("Inside the refreshCamera()!!!");
		if (mSurfaceHolder.getSurface() == null) {
			// preview surface does not exist
			return;
		}

		// stop preview before making changes
		try {
			mCamera.stopPreview();
		} catch (Exception e) {
			// ignore: tried to stop a non-existent preview
		}

		// set preview size and make any resize, rotate or
		// reformatting changes here
		// start preview with new settings
		try {
			mCamera.setPreviewDisplay(mSurfaceHolder);
			mCamera.startPreview();
		} catch (Exception e) {
			System.err.println("Refresh Camera block :" + e);
		}
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
		System.out.println("Inside the surface changed method!!!!!!!!!");
		// Now that the size is known, set up the camera parameters and begin
		// the preview.

		refreshCamera();
	}

	@SuppressLint("InlinedApi")
	public void surfaceCreated(SurfaceHolder holder) {
		System.out.println("Inside the surface crearted()!!!!");
		try {
			mCamera = Camera.open();
			mCamera.setDisplayOrientation(90);
			Camera.Parameters param;
			param = mCamera.getParameters();
			List<Camera.Size> pictureSizes = param
					.getSupportedPictureSizes();
			if (pictureSizes.get(0).width > pictureSizes.get(pictureSizes
					.size() - 1).width) {
				Collections.reverse(pictureSizes);
			}
			int mWidth = 0, mHeight = 0;
			float mPicSize = 0;
			for (int i = 0; i < pictureSizes.size(); i++) {
				mWidth = pictureSizes.get(i).width;
				mHeight = pictureSizes.get(i).height;
				mPicSize = ((mWidth * mHeight));
				mPicSize = ((mPicSize) / 1000000);
				System.out.println("Width :" + mWidth);
				System.out.println("Height :" + mHeight);
				System.out.println("Pic size :" + mPicSize);
				if (mPicSize >= 0.9f) {
					param.setPictureSize(mWidth, mHeight);
					break;
				}
			}
			// modify parameter
			// param.setPreviewSize(mWidth, mHeight);
			// param.setRotation(90);
			boolean isPreviewSizeEnabled = false;
			List<Camera.Size> previewSizes = param.getSupportedPreviewSizes();
			if (previewSizes.get(0).width > previewSizes.get(previewSizes
					.size() - 1).width) {
				Collections.reverse(previewSizes);
			}
			mWidth = 0;
			mHeight = 0;
			mPicSize = 0;

			for (int i = 0; i < previewSizes.size(); i++) {
				mWidth = previewSizes.get(i).width;
				mHeight = previewSizes.get(i).height;
				mPicSize = ((mWidth * mHeight));
				mPicSize = ((mPicSize) / 1000000);
				if (mPicSize >= 0.9f) {
					param.setPreviewSize(mWidth, mHeight);
					isPreviewSizeEnabled = true;
					break;
				}
			}
			if (!isPreviewSizeEnabled) {
				param.setPreviewSize(
						previewSizes.get(previewSizes.size() - 1).width,
						previewSizes.get(previewSizes.size() - 1).height);
			}

			param.setPreviewSize(640, 480);

			isAutoFocusSupported = false;
			List<String> supportedFocusModes = param.getSupportedFocusModes();
			for (int i = 0; i < supportedFocusModes.size(); i++) {
				System.out.println("Focus mode :"
						+ supportedFocusModes.get(i).toString());
				if (TextUtils.equals(supportedFocusModes.get(i).toString(),
						Camera.Parameters.FOCUS_MODE_AUTO)) {
					// param.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
					isAutoFocusSupported = true;
					break;
				}

			}

			System.out.println("Selected PreView Image Width :"
					+ param.getPreviewSize().width);
			System.out.println("Selected PreView Image Height :"
					+ param.getPreviewSize().height);

			mCamera.setParameters(param);
		} catch (RuntimeException e) {
			if (mCamera != null) {
				mCamera.stopPreview();
				mCamera.release();
				mCamera = null;
			}
			System.err.println("surface Created1 :" + e);
			return;
		}

		try {
			// The Surface has been created, now tell the camera where to draw
			// the preview.
			mCamera.setPreviewDisplay(mSurfaceHolder);
			mCamera.startPreview();
		} catch (Exception e) {
			// check for exceptions
			System.err
					.println("surface Created2 :" + e.getMessage().toString());
			return;
		}
	}

	
	public void surfaceDestroyed(SurfaceHolder holder) {
		System.out.println("Inside the surfaceDestroyed!!!");
		// stop preview and release camera
		if (mCamera != null) {
			mCamera.stopPreview();
			mCamera.release();
			mCamera = null;
		}
	}

	@Override
	public void onBackPressed() {
		System.out.println("Inside the onBackPressed()!!!!!!!!!!!");
		CapturingImageActivity.this.finish();
		setResult(Constants.RESULT_CANCEL);
	}

	@Override
	public void onAutoFocus(boolean success, Camera camera) {
		// System.out.println("Auto Focus:"+success);
	}

}