package com.yesteam.eshakti.view.fragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.squareup.otto.Subscribe;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;

import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.TransactionOffConstants;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;
import com.yesteam.eshakti.sqlite.database.GroupProvider;
import com.yesteam.eshakti.sqlite.database.TransactionProvider;
import com.yesteam.eshakti.sqlite.database.response.GetSingleTransResponse;
import com.yesteam.eshakti.sqlite.database.response.GroupMasUpdateResponse;
import com.yesteam.eshakti.sqlite.database.response.TransactionResponse;
import com.yesteam.eshakti.sqlite.database.response.TransactionUpdateResponse;
import com.yesteam.eshakti.sqlite.db.model.GetTransactionSingle;
import com.yesteam.eshakti.sqlite.db.model.GetTransactionSinglevalues;
import com.yesteam.eshakti.sqlite.db.model.GroupDetailsUpdate;
import com.yesteam.eshakti.sqlite.db.model.GroupResponseUpdate;
import com.yesteam.eshakti.sqlite.db.model.GroupTempDBLastTransDate;
import com.yesteam.eshakti.sqlite.db.model.Transaction;
import com.yesteam.eshakti.sqlite.db.model.TransactionUpdate;
import com.yesteam.eshakti.sqlite.db.model.Transaction_UniqueIdSingle;
import com.yesteam.eshakti.sqlite.db.transactions.TransactionManager.DataType;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.utils.GetOfflineTransactionValues;
import com.yesteam.eshakti.utils.GetResponseInfo;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.utils.Get_EdiText_Filter;
import com.yesteam.eshakti.utils.Get_Offline_MobileDate;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.utils.Put_DB_GroupNameDetail_Response;
import com.yesteam.eshakti.utils.Put_DB_GroupResponse;
import com.yesteam.eshakti.utils.Reset;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.utils.UpdateCalendarMinMaxDateUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.Get_DepositEntryTask;
import com.yesteam.eshakti.webservices.Get_LastTransactionID;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ParseException;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

@SuppressLint({ "RtlHardcoded", "InlinedApi" })
public class Transaction_DepositEntryFragment extends Fragment implements OnClickListener, TaskListener {

	public static final String TAG = Transaction_DepositEntryFragment.class.getSimpleName();

	private TextView mGroupName, mCashInHand, mCashAtBank, mHeadertext;
	private Button mRaised_Submit_Button, mEdit_RaisedButton, mOk_RaisedButton;
	public static EditText mEditAmount;

	private Dialog mProgressDilaog;
	public static String sSend_To_Server_Deposit;
	public static String[] sDepositItems;
	public static String[] sDepositAmount;
	int size;
	public static List<EditText> sEditTextFields = new ArrayList<EditText>();

	public static int sBank_deposit, sBank_withdrawl, sBank_expenses, sBank_Interest, sBank_IntrSubvention,
			cashAtWithdrawl;

	String toBeEditArr[];
	public static int sDepositTotal;
	String mDeposit_offline, mInterest_offline, mWithdrawl_offline, mBankExpenses_offline, mInterestSubvention_Offline;
	public static String mCashatBank_individual;
	String mLastTrDate = null, mLastTr_ID = null;

	Dialog confirmationDialog;
	String mOfflineBankname;
	boolean isGetTrid = false;
	boolean isServiceCall = false;
	String mSqliteDBStoredValues_BankTransactionValues = null;

	public Transaction_DepositEntryFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_DEPOSIT_ENTRY;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		SBus.INST.register(this);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Transaction_DepositEntryFragment.sEditTextFields.clear();
		sSend_To_Server_Deposit = Reset.reset(sSend_To_Server_Deposit);
		sDepositTotal = 0;

		sDepositItems = new String[] { RegionalConversion.getRegionalConversion(AppStrings.mCashDeposit), // (AppStrings.bankDeposit),
				RegionalConversion.getRegionalConversion(AppStrings.bankInterest), // (AppStrings.mCashDeposit),
																					// //
																					// (AppStrings.bankInterest),
				RegionalConversion.getRegionalConversion(AppStrings.interestSubvention),
				RegionalConversion.getRegionalConversion(AppStrings.withdrawl),
				RegionalConversion.getRegionalConversion(AppStrings.mBankCharges) };// (AppStrings.bankExpenses)
																					// };
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_deposit_entry, container, false);

		MainFragment_Dashboard.isBackpressed = false;

		mSqliteDBStoredValues_BankTransactionValues = null;

		try {

			System.out.println("LENGTH " + sDepositItems.length);

			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashInHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashInHand.setTypeface(LoginActivity.sTypeface);

			mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashAtBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashAtBank.setTypeface(LoginActivity.sTypeface);

			mHeadertext = (TextView) rootView.findViewById(R.id.fragment_deposit_entry_headertext);
			mHeadertext.setText(RegionalConversion.getRegionalConversion(AppStrings.mSavings_Banktransaction));//(AppStrings.mSavingsTransaction));// (AppStrings.bankTransaction));
			mHeadertext.setTypeface(LoginActivity.sTypeface);

			mRaised_Submit_Button = (Button) rootView.findViewById(R.id.fragment_deposit_entry_Submitbutton);
			mRaised_Submit_Button.setText(RegionalConversion.getRegionalConversion(AppStrings.submit));
			mRaised_Submit_Button.setTypeface(LoginActivity.sTypeface);
			mRaised_Submit_Button.setOnClickListener(this);

			size = sDepositItems.length;
			System.out.println("Size of values:>>>" + size);

			try {

				TableLayout tableLayout = (TableLayout) rootView.findViewById(R.id.fragmentDeposit_contentTable);
				TableRow outerRow = new TableRow(getActivity());
				TableRow.LayoutParams header_ContentParams = new TableRow.LayoutParams(250, LayoutParams.WRAP_CONTENT,
						1f);
				header_ContentParams.setMargins(10, 5, 10, 5);

				TextView bankName = new TextView(getActivity());
				bankName.setText(GetSpanText.getSpanString(getActivity(),
						String.valueOf(Transaction_BankDepositFragment.sSelected_BankName)));
				bankName.setTypeface(LoginActivity.sTypeface);
				bankName.setPadding(15, 5, 5, 5);
				bankName.setLayoutParams(header_ContentParams);
				bankName.setTextColor(color.black);
				outerRow.addView(bankName);

				TableRow.LayoutParams amountParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
						LayoutParams.WRAP_CONTENT, 1f);
				amountParams.setMargins(5, 5, 20, 5);

				TextView bankDeposit_Amount = new TextView(getActivity());
				bankDeposit_Amount.setText(GetSpanText.getSpanString(getActivity(),
						String.valueOf(Transaction_BankDepositFragment.sSelected_BankDepositAmount)));
				bankDeposit_Amount.setTextColor(color.black);
				bankDeposit_Amount.setGravity(Gravity.RIGHT);
				bankDeposit_Amount.setLayoutParams(amountParams);
				bankDeposit_Amount.setPadding(5, 5, 5, 5);
				outerRow.addView(bankDeposit_Amount);

				tableLayout.addView(outerRow, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

				for (int i = 0; i < size; i++) {

					TableRow innerRow = new TableRow(getActivity());

					TextView memberName = new TextView(getActivity());
					memberName.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sDepositItems[i])));
					memberName.setTypeface(LoginActivity.sTypeface);
					memberName.setPadding(15, 0, 5, 5);
					memberName.setTextColor(color.black);

					memberName.setLayoutParams(header_ContentParams);
					innerRow.addView(memberName);

					mEditAmount = new EditText(getActivity());
					sEditTextFields.add(mEditAmount);
					mEditAmount.setId(i);
					mEditAmount.setInputType(InputType.TYPE_CLASS_NUMBER);
					mEditAmount.setPadding(5, 5, 5, 5);
					mEditAmount.setFilters(Get_EdiText_Filter.editText_filter());
					mEditAmount.setLayoutParams(amountParams);
					mEditAmount.setBackgroundResource(R.drawable.edittext_background);
					mEditAmount.setWidth(150);
					mEditAmount.setOnFocusChangeListener(new View.OnFocusChangeListener() {

						@Override
						public void onFocusChange(View v, boolean hasFocus) {
							// TODO Auto-generated method stub
							if (hasFocus) {
								((EditText) v).setGravity(Gravity.LEFT);
							} else {
								((EditText) v).setGravity(Gravity.RIGHT);
							}
						}
					});
					innerRow.addView(mEditAmount);

					tableLayout.addView(innerRow,
							new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return rootView;
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		try {

			sDepositAmount = new String[sEditTextFields.size()];

			switch (v.getId()) {

			case R.id.fragment_deposit_entry_Submitbutton:
				
				sDepositTotal = Integer.valueOf("0");
				sSend_To_Server_Deposit = Reset.reset(sSend_To_Server_Deposit);

				for (int i = 0; i < sDepositAmount.length; i++) {

					sDepositAmount[i] = String.valueOf(sEditTextFields.get(i).getText());

					if ((sDepositAmount[i].equals("")) || (sDepositAmount[i] == null)) {
						sDepositAmount[i] = "0";
					}

					if (sDepositAmount[i].matches("\\d*\\.?\\d+")) { // match a
																		// decimal
																		// number

						int amount = (int) Math.round(Double.parseDouble(sDepositAmount[i]));
						sDepositAmount[i] = String.valueOf(amount);
					}

					sDepositTotal = sDepositTotal + Integer.parseInt(sDepositAmount[i]);

					sSend_To_Server_Deposit = sSend_To_Server_Deposit + sDepositAmount[i] + "~";

				}

				System.out.println("Total value:>>>" + sDepositTotal);

				System.out.println("Sennd To Confirmation value:>>>" + sSend_To_Server_Deposit);

				sBank_deposit = Integer.parseInt(sDepositAmount[0]);
				sBank_Interest = Integer.parseInt(sDepositAmount[1]);
				sBank_withdrawl = Integer.parseInt(sDepositAmount[3]);
				sBank_expenses = Integer.parseInt(sDepositAmount[4]);
				sBank_IntrSubvention = Integer.parseInt(sDepositAmount[2]);

				sSend_To_Server_Deposit = sBank_deposit + "~" + sBank_Interest + "~" + sBank_withdrawl + "~"
						+ sBank_expenses + "~" + sBank_IntrSubvention + "~";

				System.out.println("Bank Repayment:>>>" + sBank_deposit);
				System.out.println("Bank Withdrawl:>>>" + sBank_withdrawl);

				double mValues = Double.parseDouble(Transaction_BankDepositFragment.sSelected_BankDepositAmount);
				double mRoundedValues = Math.round(mValues);
				Transaction_BankDepositFragment.sSelected_BankDepositAmount = String.valueOf(mRoundedValues);

				Double mDoubleValues = Double.parseDouble(Transaction_BankDepositFragment.sSelected_BankDepositAmount);
				String mFinalDepositAmount = mDoubleValues.longValue() == mDoubleValues ? "" + mDoubleValues.longValue()
						: "" + mDoubleValues;

				Transaction_BankDepositFragment.sSelected_BankDepositAmount = mFinalDepositAmount;

				cashAtWithdrawl = Integer.parseInt(Transaction_BankDepositFragment.sSelected_BankDepositAmount.trim())
						+ Integer.parseInt(sDepositAmount[0]) + Integer.parseInt(sDepositAmount[1])
						+ Integer.parseInt(sDepositAmount[2]) - Integer.parseInt(sDepositAmount[4]);

				System.out.println("Deposit Available Withdrawl Amount:" + cashAtWithdrawl);

				if (sDepositTotal != 0 && sBank_deposit <= Integer.parseInt(SelectedGroupsTask.sCashinHand.trim())
						&& sBank_withdrawl <= cashAtWithdrawl && sBank_expenses <= Integer
								.parseInt(Transaction_BankDepositFragment.sSelected_BankDepositAmount.trim())) {

					int tempNewCashInHand = Integer.parseInt(SelectedGroupsTask.sCashinHand.trim())
							- Integer.parseInt(sDepositAmount[0]) + Integer.parseInt(sDepositAmount[3]);
					int tempNewCashAtBank = Integer
							.parseInt(Transaction_BankDepositFragment.sSelected_BankDepositAmount.trim())
							+ Integer.parseInt(sDepositAmount[0]) + Integer.parseInt(sDepositAmount[1])
							- Integer.parseInt(sDepositAmount[3])
							- Integer.parseInt(sDepositAmount[4] + Integer.parseInt(sDepositAmount[2]));// Make
																										// sure

					System.out.println("New cashINHand:>>>>>" + tempNewCashInHand);
					System.out.println("New cashAtBank:>>>>>" + tempNewCashAtBank);

					mDeposit_offline = sDepositAmount[0];
					mInterest_offline = sDepositAmount[1];
					mWithdrawl_offline = sDepositAmount[3];
					mBankExpenses_offline = sDepositAmount[4];
					mInterestSubvention_Offline = sDepositAmount[2];

					confirmationDialog = new Dialog(getActivity());

					LayoutInflater inflater = getActivity().getLayoutInflater();
					View dialogView = inflater.inflate(R.layout.dialog_confirmation, null);
					dialogView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
							LayoutParams.WRAP_CONTENT));

					TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
					confirmationHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.confirmation));
					confirmationHeader.setTypeface(LoginActivity.sTypeface);

					TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

					for (int i = 0; i < size; i++) {

						TableRow indv_DepositEntryRow = new TableRow(getActivity());

						TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
								LayoutParams.WRAP_CONTENT, 1f);
						contentParams.setMargins(10, 5, 10, 5);

						TextView memberName_Text = new TextView(getActivity());
						memberName_Text
								.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sDepositItems[i])));
						memberName_Text.setTypeface(LoginActivity.sTypeface);
						memberName_Text.setTextColor(color.black);
						memberName_Text.setPadding(5, 5, 5, 5);
						memberName_Text.setLayoutParams(contentParams);
						indv_DepositEntryRow.addView(memberName_Text);

						TextView confirm_values = new TextView(getActivity());
						confirm_values
								.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sDepositAmount[i])));
						confirm_values.setTextColor(color.black);
						confirm_values.setPadding(5, 5, 5, 5);
						confirm_values.setGravity(Gravity.RIGHT);
						confirm_values.setLayoutParams(contentParams);
						indv_DepositEntryRow.addView(confirm_values);

						confirmationTable.addView(indv_DepositEntryRow,
								new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
					}
					mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit_button);
					mEdit_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.edit));
					mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
					mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
																			// 205,
																			// 0));
					mEdit_RaisedButton.setOnClickListener(this);

					mOk_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Ok_button);
					mOk_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.yes));
					mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
					mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
					mOk_RaisedButton.setOnClickListener(this);

					confirmationDialog.getWindow()
							.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
					confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					confirmationDialog.setCanceledOnTouchOutside(false);
					confirmationDialog.setContentView(dialogView);
					confirmationDialog.setCancelable(true);
					confirmationDialog.show();

					MarginLayoutParams margin = (MarginLayoutParams) dialogView.getLayoutParams();
					margin.leftMargin = 10;
					margin.rightMargin = 10;
					margin.topMargin = 10;
					margin.bottomMargin = 10;
					margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);

				} else if (sBank_deposit > Integer.parseInt(SelectedGroupsTask.sCashinHand.trim())) {

					TastyToast.makeText(getActivity(), AppStrings.cashinHandAlert, TastyToast.LENGTH_SHORT,
							TastyToast.WARNING);

					sSend_To_Server_Deposit = Reset.reset(sSend_To_Server_Deposit);
					sDepositTotal = Integer.valueOf("0");

				} else if (sBank_withdrawl > cashAtWithdrawl || sBank_expenses > Integer
						.parseInt(Transaction_BankDepositFragment.sSelected_BankDepositAmount.trim())) {

					TastyToast.makeText(getActivity(), AppStrings.cashatBankAlert, TastyToast.LENGTH_SHORT,
							TastyToast.WARNING);

					sSend_To_Server_Deposit = Reset.reset(sSend_To_Server_Deposit);
					sDepositTotal = Integer.valueOf("0");

				}

				else {

					TastyToast.makeText(getActivity(), AppStrings.DepositnullAlert, TastyToast.LENGTH_SHORT,
							TastyToast.WARNING);

					sSend_To_Server_Deposit = Reset.reset(sSend_To_Server_Deposit);
					sDepositTotal = Integer.valueOf("0");

				}

				break;
			case R.id.fragment_Edit_button:
				sSend_To_Server_Deposit = Reset.reset(sSend_To_Server_Deposit);
				sDepositTotal = Integer.valueOf("0");
				mRaised_Submit_Button.setClickable(true);
				// alertDialog.dismiss();
				isServiceCall = false;
				confirmationDialog.dismiss();
				break;
			case R.id.fragment_Ok_button:

				if (ConnectionUtils.isNetworkAvailable(getActivity())) {

					try {

						if (EShaktiApplication.getLoginFlag().equals(Constants.ONLINEFLAG)) {
							if (!isServiceCall) {
								isServiceCall = true;

								new Get_DepositEntryTask(Transaction_DepositEntryFragment.this).execute();
							}
						} else {

							TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
									TastyToast.ERROR);
						}
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
				} else {
					// Do offline Stuffs
					if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
						EShaktiApplication.getInstance().getTransactionManager().startTransaction(
								DataType.GET_TRANS_SINGLE, new GetTransactionSingle(PrefUtils.getOfflineUniqueID()));
					} else {

						TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
								TastyToast.ERROR);
					}
				}
				break;
			default:
				break;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Subscribe
	public void OnGetSingleTransactionExpense(final GetSingleTransResponse getSingleTransResponse)
			throws ParseException {
		switch (getSingleTransResponse.getFetcherResult()) {
		case FAIL:

			TastyToast.makeText(getActivity(), getSingleTransResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), getSingleTransResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			try {
				System.out.println("---------OnGetSingleTransactionExpense----------");
				String mUniqueId = GetTransactionSinglevalues.getUniqueId();
				String mBankDepositValues_Offline = GetTransactionSinglevalues.getBankDeposit();
				Log.e("mBankDepositValues_Offline $$$ ", mBankDepositValues_Offline + "");
				String mBankCheckValues = null;

				String mCurrentTransDate = null;

				if (publicValues.mOffline_Trans_CurrentDate != null) {
					mCurrentTransDate = publicValues.mOffline_Trans_CurrentDate;
				}

				mLastTrDate = DatePickerDialog.sDashboardDate;
				mLastTr_ID = SelectedGroupsTask.sTr_ID.toString();

				String mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
				String mMobileDate = Get_Offline_MobileDate.getOffline_MobileDate(getActivity());
				String mTransaction_UniqueId = PrefUtils.getOfflineUniqueID();

				mOfflineBankname = Transaction_BankDepositFragment.sSendToServer_BankName;

				System.out.println(TAG + "sDeposit Amount::::              " + mDeposit_offline);
				String mSend_to_Offline_server_BD = sSend_To_Server_Deposit + "#" + mOfflineBankname + "#" + mTrasactiondate
						+ "#" + mMobileDate + "%";
				Log.v("Offline Bank Name::", mOfflineBankname);

				if (mBankDepositValues_Offline == null) {
					publicValues.mBankCheckLabel = new String[SelectedGroupsTask.sBankNames.size()];
					for (int i = 0; i < publicValues.mBankCheckLabel.length; i++) {
						publicValues.mBankCheckLabel[i] = "0";

					}
				}
				if (publicValues.mBankCheckLabel != null) {
					for (int j = 0; j < publicValues.mBankCheckLabel.length; j++) {
						String sSendToServer_BankName = SelectedGroupsTask.sEngBankNames.elementAt(j).toString();

						if (sSendToServer_BankName.equals(Transaction_BankDepositFragment.sSendToServer_BankName)) {
							mBankCheckValues = publicValues.mBankCheckLabel[j];
						}

					}
				}
				Log.e("Bank Check Values::::", mBankCheckValues);

				if (!mSend_to_Offline_server_BD.contains("?") && !mCurrentTransDate.contains("?")
						&& !mTransaction_UniqueId.contains("?")) {

					mSqliteDBStoredValues_BankTransactionValues = mSend_to_Offline_server_BD;

					if (mBankCheckValues.equals("0")) {
						if (PrefUtils.getOfflineBankDeposit() == null && PrefUtils.getOfflineBankDepositName() == null) {
							PrefUtils.setOfflineBankDeposit(mSend_to_Offline_server_BD);

						} else if (!PrefUtils.getOfflineBankDepositName().equalsIgnoreCase(mOfflineBankname)) {
							Log.v("22222222222222222222222", "22222222222222222222222");
							mSend_to_Offline_server_BD = PrefUtils.getOfflineBankDeposit() + mSend_to_Offline_server_BD;
							PrefUtils.setOfflineBankDeposit(mSend_to_Offline_server_BD);
						}

						Log.e("Deposit Valuessss::::::::::::::", PrefUtils.getOfflineBankDeposit());
						Log.e("mSend_to_Offline_server_BD $$$ ", mSend_to_Offline_server_BD + "");

						if (mUniqueId == null && mBankDepositValues_Offline == null
								&& !PrefUtils.getOfflineBankDeposit().equals(null)) {
							Log.e("Step 11111111111", "1111");

							if (mLastTrDate != null && mMobileDate != null && mTransaction_UniqueId != null
									&& mCurrentTransDate != null) {
								EShaktiApplication.getInstance().getTransactionManager().startTransaction(
										DataType.TRANSACTIONADD,
										new Transaction(0, PrefUtils.getUsernameKey(), SelectedGroupsTask.UserName,
												SelectedGroupsTask.Ngo_Id, SelectedGroupsTask.Group_Id,
												mTransaction_UniqueId, mLastTr_ID, mCurrentTransDate, null, null, null,
												null, null, null, null, null, null, null, null, mSend_to_Offline_server_BD,
												null, null, null, null, null, null, null, null, null, null));

							} else {

								TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT,
										TastyToast.ERROR);
							}

						} else if (mUniqueId.equals(PrefUtils.getOfflineUniqueID()) && mBankDepositValues_Offline == null) {
							Log.v("3333333333333333333333333333333333333333", "33333333333333333333333333333333333333");
							EShaktiApplication.setSetTransValues(TransactionOffConstants.TRANS_BDEP);

							EShaktiApplication.getInstance().getTransactionManager().startTransaction(
									DataType.TRANS_VALUES_UPDATE,
									new TransactionUpdate(mUniqueId, mSend_to_Offline_server_BD));

						} else if (mUniqueId.equals(PrefUtils.getOfflineUniqueID()) && mBankDepositValues_Offline != null
								&& !PrefUtils.getOfflineBankDepositName().equalsIgnoreCase(mOfflineBankname)) {
							Log.v("444444", "44444444");
							EShaktiApplication.setSetTransValues(TransactionOffConstants.TRANS_BDEP);

							EShaktiApplication.getInstance().getTransactionManager().startTransaction(
									DataType.TRANS_VALUES_UPDATE,
									new TransactionUpdate(mUniqueId, mSend_to_Offline_server_BD));

						} else if ((mBankDepositValues_Offline != null)
								|| PrefUtils.getOfflineBankDepositName().equals(mOfflineBankname)) {

							Log.v("55555555555555555555555555555555555555", "5555555555555555555555555");

							confirmationDialog.dismiss();

							TastyToast.makeText(getActivity(), AppStrings.offlineDataAvailable, TastyToast.LENGTH_SHORT,
									TastyToast.WARNING);

							DatePickerDialog.sDashboardDate = SelectedGroupsTask.sLastTransactionDate_Response;
							MainFragment_Dashboard fragment = new MainFragment_Dashboard();
							setFragment(fragment);

						}
					} else {
						confirmationDialog.dismiss();

						TastyToast.makeText(getActivity(), AppStrings.offlineDataAvailable, TastyToast.LENGTH_SHORT,
								TastyToast.WARNING);

						MainFragment_Dashboard fragment = new MainFragment_Dashboard();
						setFragment(fragment);
					}
				} else {
					if (confirmationDialog.isShowing() && confirmationDialog != null) {
						confirmationDialog.dismiss();
					}

					TastyToast.makeText(getActivity(), "Please Try Again", TastyToast.LENGTH_SHORT, TastyToast.ERROR);

					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;

		}
	}

	@Subscribe
	public void onAddTransactionBankDeposit(final TransactionResponse transactionResponse) {
		switch (transactionResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), transactionResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), transactionResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			try {
				System.out.println("----------onAddTransactionBankDeposit-------------");

				TransactionProvider
						.getSinlgeGroupMaster_Transaction(new Transaction_UniqueIdSingle(PrefUtils.getOfflineUniqueID()));

				if (GetTransactionSinglevalues.getBankDeposit() != null

						&& !GetTransactionSinglevalues.getBankDeposit().equals("")) {

					String mCashinHand, mCashatBank;

					double mValues = Double.parseDouble(Transaction_BankDepositFragment.sSelected_BankDepositAmount);
					double mRoundedValues = Math.round(mValues);
					Transaction_BankDepositFragment.sSelected_BankDepositAmount = String.valueOf(mRoundedValues);

					Double mDoubleValues = Double.parseDouble(Transaction_BankDepositFragment.sSelected_BankDepositAmount);
					String mFinalDepositAmount = mDoubleValues.longValue() == mDoubleValues ? "" + mDoubleValues.longValue()
							: "" + mDoubleValues;

					Transaction_BankDepositFragment.sSelected_BankDepositAmount = mFinalDepositAmount;

					double mValues_CashatBank = Double.parseDouble(SelectedGroupsTask.sCashatBank);
					double mRoundedValues_CashatBank = Math.round(mValues_CashatBank);
					SelectedGroupsTask.sCashatBank = String.valueOf(mRoundedValues_CashatBank);

					Double mDoubleValues_CashatBank = Double.parseDouble(SelectedGroupsTask.sCashatBank);
					String mFinalDepositAmount_CashatBank = mDoubleValues_CashatBank.longValue() == mDoubleValues_CashatBank
							? "" + mDoubleValues_CashatBank.longValue()
							: "" + mDoubleValues_CashatBank;

					SelectedGroupsTask.sCashatBank = mFinalDepositAmount_CashatBank;

					mCashatBank_individual = Transaction_BankDepositFragment.sSelected_BankDepositAmount;
					mCashatBank = SelectedGroupsTask.sCashatBank;

					Log.e(TAG + "Deposit Amount", mDeposit_offline);
					Log.e(TAG + "mInterest_offline", mInterest_offline);
					Log.e(TAG + "mWithdrawl_offline", mWithdrawl_offline);
					Log.e(TAG + "mBankExpenses_offline Amount", mBankExpenses_offline);

					mCashinHand = String.valueOf(
							(Integer.parseInt(SelectedGroupsTask.sCashinHand) + Integer.parseInt(mWithdrawl_offline))
									- Integer.parseInt(mDeposit_offline));

					mCashatBank_individual = String
							.valueOf((Integer.parseInt(mCashatBank_individual) + Integer.parseInt(mDeposit_offline)
									+ Integer.parseInt(mInterest_offline) + Integer.parseInt(mInterestSubvention_Offline))
									- Integer.parseInt(mWithdrawl_offline) - Integer.parseInt(mBankExpenses_offline));

					mCashatBank = String.valueOf((Integer.parseInt(mCashatBank) + Integer.parseInt(mDeposit_offline)
							+ Integer.parseInt(mInterest_offline) + Integer.parseInt(mInterestSubvention_Offline))
							- Integer.parseInt(mWithdrawl_offline) - Integer.parseInt(mBankExpenses_offline));

					Log.i(TAG + "Deposit Amount", mDeposit_offline);
					Log.i(TAG + "Cash In Hand", mCashinHand);
					Log.i(TAG + "cash at Bank Individual", mCashatBank_individual);
					Log.i(TAG + "Cash At Bank", mCashatBank);

					EShaktiApplication.setBankDeposit(true);
					String mBankDetails = GetOfflineTransactionValues.getBankDetails();
					SelectedGroupsTask.sCashinHand = mCashinHand;
					SelectedGroupsTask.sCashatBank = mCashatBank;
					String mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
					SelectedGroupsTask.sLastTransactionDate_Response = mTrasactiondate;
					String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mTrasactiondate, mCashinHand,
							mCashatBank, mBankDetails);
					String mSelectedGroupId = SelectedGroupsTask.Group_Id;

					Log.e("mGroupMasterResponse $$$ ", mGroupMasterResponse + "");

					EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GROUPMASTERUPDATE,
							new GroupDetailsUpdate(mSelectedGroupId, mGroupMasterResponse));
				} else {
					if (confirmationDialog.isShowing() && confirmationDialog != null) {
						confirmationDialog.dismiss();
					}

					TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT, TastyToast.ERROR);

					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);

				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		}
	}

	@Subscribe
	public void OnTransUpdateBankDeposit(final TransactionUpdateResponse transactionUpdateResponse) {
		switch (transactionUpdateResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), transactionUpdateResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), transactionUpdateResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			try {
				System.out.println("-----------OnTransUpdateBankDeposit-------------");

				TransactionProvider
						.getSinlgeGroupMaster_Transaction(new Transaction_UniqueIdSingle(PrefUtils.getOfflineUniqueID()));

				if (GetTransactionSinglevalues.getBankDeposit() != null

						&& !GetTransactionSinglevalues.getBankDeposit().equals("")) {
					String mCashinHand, mCashatBank;

					double mValues = Double.parseDouble(Transaction_BankDepositFragment.sSelected_BankDepositAmount);
					double mRoundedValues = Math.round(mValues);
					Transaction_BankDepositFragment.sSelected_BankDepositAmount = String.valueOf(mRoundedValues);

					Double mDoubleValues = Double.parseDouble(Transaction_BankDepositFragment.sSelected_BankDepositAmount);
					String mFinalDepositAmount = mDoubleValues.longValue() == mDoubleValues ? "" + mDoubleValues.longValue()
							: "" + mDoubleValues;

					Transaction_BankDepositFragment.sSelected_BankDepositAmount = mFinalDepositAmount;

					double mValues_CashatBank = Double.parseDouble(SelectedGroupsTask.sCashatBank);
					double mRoundedValues_CashatBank = Math.round(mValues_CashatBank);
					SelectedGroupsTask.sCashatBank = String.valueOf(mRoundedValues_CashatBank);

					Double mDoubleValues_CashatBank = Double.parseDouble(SelectedGroupsTask.sCashatBank);
					String mFinalDepositAmount_CashatBank = mDoubleValues_CashatBank.longValue() == mDoubleValues_CashatBank
							? "" + mDoubleValues_CashatBank.longValue()
							: "" + mDoubleValues_CashatBank;

					SelectedGroupsTask.sCashatBank = mFinalDepositAmount_CashatBank;

					mCashatBank_individual = Transaction_BankDepositFragment.sSelected_BankDepositAmount;
					mCashatBank = SelectedGroupsTask.sCashatBank;

					mCashinHand = String
							.valueOf(Integer.parseInt(SelectedGroupsTask.sCashinHand) - Integer.parseInt(mDeposit_offline));

					Log.e(TAG + "Deposit Amount", mDeposit_offline);
					Log.e(TAG + "mInterest_offline", mInterest_offline);
					Log.e(TAG + "mWithdrawl_offline", mWithdrawl_offline);
					Log.e(TAG + "mBankExpenses_offline Amount", mBankExpenses_offline);

					mCashinHand = String.valueOf(
							(Integer.parseInt(SelectedGroupsTask.sCashinHand) + Integer.parseInt(mWithdrawl_offline))
									- Integer.parseInt(mDeposit_offline));

					mCashatBank_individual = String
							.valueOf((Integer.parseInt(mCashatBank_individual) + Integer.parseInt(mDeposit_offline)
									+ Integer.parseInt(mInterest_offline) + Integer.parseInt(mInterestSubvention_Offline))
									- Integer.parseInt(mWithdrawl_offline) - Integer.parseInt(mBankExpenses_offline));

					mCashatBank = String.valueOf((Integer.parseInt(mCashatBank) + Integer.parseInt(mDeposit_offline)
							+ Integer.parseInt(mInterest_offline) + Integer.parseInt(mInterestSubvention_Offline))
							- Integer.parseInt(mWithdrawl_offline) - Integer.parseInt(mBankExpenses_offline));

					EShaktiApplication.setBankDeposit(true);
					String mBankDetails = GetOfflineTransactionValues.getBankDetails();
					SelectedGroupsTask.sCashinHand = mCashinHand;
					SelectedGroupsTask.sCashatBank = mCashatBank;
					String mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
					SelectedGroupsTask.sLastTransactionDate_Response = mTrasactiondate;
					String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mTrasactiondate, mCashinHand,
							mCashatBank, mBankDetails);
					String mSelectedGroupId = SelectedGroupsTask.Group_Id;

					Log.e("mGroupMasterResponse $$$ ", mGroupMasterResponse + "");
					EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GROUPMASTERUPDATE,
							new GroupDetailsUpdate(mSelectedGroupId, mGroupMasterResponse));

				} else {
					if (confirmationDialog.isShowing() && confirmationDialog != null) {
						confirmationDialog.dismiss();
					}

					TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT, TastyToast.ERROR);

					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);

				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		}
	}

	@Subscribe
	public void OnGroupMasBankDepositUpdate(final GroupMasUpdateResponse masterResponse) {
		switch (masterResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), masterResponse.getFetcherResult().getMessage(), TastyToast.LENGTH_SHORT,
					TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), masterResponse.getFetcherResult().getMessage(), TastyToast.LENGTH_SHORT,
					TastyToast.ERROR);
			break;
		case SUCCESS:

			System.out.println("--------OnGroupMasBankDepositUpdate-----------");
			if (ConnectionUtils.isNetworkAvailable(getActivity())) {
				confirmationDialog.dismiss();
				MainFragment_Dashboard fragment = new MainFragment_Dashboard();
				setFragment(fragment);
			} else {
				PrefUtils.setOfflineBankDepositName(mOfflineBankname);
				for (int j = 0; j < publicValues.mBankCheckLabel.length; j++) {
					String sSendToServer_BankName = SelectedGroupsTask.sEngBankNames.elementAt(j).toString();

					if (sSendToServer_BankName.equals(PrefUtils.getOfflineBankDepositName())) {
						publicValues.mBankCheckLabel[j] = "1";
					}

				}
				confirmationDialog.dismiss();
				try {

					TastyToast.makeText(getActivity(), AppStrings.transactionCompleted, TastyToast.LENGTH_SHORT,
							TastyToast.SUCCESS);

					if (!ConnectionUtils.isNetworkAvailable(getActivity())) {

						String mValues = Put_DB_GroupNameDetail_Response
								.put_DB_GroupNameDetails_Response(EShaktiApplication.getGroupResponse());

						GroupProvider.updateGroupResponse(
								new GroupResponseUpdate(EShaktiApplication.getGroupId_GroupLastTransDate(), mValues));
					}

					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			break;
		}
	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub
		mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
		mProgressDilaog.show();
	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub

		if (mProgressDilaog != null) {
			mProgressDilaog.dismiss();
			if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {
				getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub

						if (!result.equals("FAIL")) {
							isServiceCall = false;
							TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg,
									TastyToast.LENGTH_SHORT, TastyToast.ERROR);
							Constants.NETWORKCOMMONFLAG = "SUCCESS";
						}

					}
				});

			} else {
				try {
					if (isGetTrid) {
						isGetTrid = false;
						callOfflineDataUpdate();
					} else {
						if (GetResponseInfo.sResponseUpdate[0].equals("Yes")) {

							TastyToast.makeText(getActivity(), AppStrings.transactionCompleted, TastyToast.LENGTH_SHORT,
									TastyToast.SUCCESS);
							new Get_LastTransactionID(Transaction_DepositEntryFragment.this).execute();
							isGetTrid = true;

						} else {

							TastyToast.makeText(getActivity(), AppStrings.transactionFailAlert, TastyToast.LENGTH_SHORT,
									TastyToast.ERROR);

							MainFragment_Dashboard fragment = new MainFragment_Dashboard();
							setFragment(fragment);
						}

						// alertDialog.dismiss();
						confirmationDialog.dismiss();

					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		}
	}

	private void callOfflineDataUpdate() {
		// TODO Auto-generated method stub
		String mCashinHand, mCashatBank;

		double mValues = Double.parseDouble(Transaction_BankDepositFragment.sSelected_BankDepositAmount);
		double mRoundedValues = Math.round(mValues);
		Transaction_BankDepositFragment.sSelected_BankDepositAmount = String.valueOf(mRoundedValues);

		Double mDoubleValues = Double.parseDouble(Transaction_BankDepositFragment.sSelected_BankDepositAmount);
		String mFinalDepositAmount = mDoubleValues.longValue() == mDoubleValues ? "" + mDoubleValues.longValue()
				: "" + mDoubleValues;

		Transaction_BankDepositFragment.sSelected_BankDepositAmount = mFinalDepositAmount;

		double mValues_CashatBank = Double.parseDouble(SelectedGroupsTask.sCashatBank);
		double mRoundedValues_CashatBank = Math.round(mValues_CashatBank);
		SelectedGroupsTask.sCashatBank = String.valueOf(mRoundedValues_CashatBank);

		Double mDoubleValues_CashatBank = Double.parseDouble(SelectedGroupsTask.sCashatBank);
		String mFinalDepositAmount_CashatBank = mDoubleValues_CashatBank.longValue() == mDoubleValues_CashatBank
				? "" + mDoubleValues_CashatBank.longValue()
				: "" + mDoubleValues_CashatBank;

		SelectedGroupsTask.sCashatBank = mFinalDepositAmount_CashatBank;

		mCashatBank_individual = Transaction_BankDepositFragment.sSelected_BankDepositAmount;
		mCashatBank = SelectedGroupsTask.sCashatBank;

		mCashinHand = SelectedGroupsTask.sCashinHand;

		mCashatBank_individual = String
				.valueOf((Integer.parseInt(mCashatBank_individual) + Integer.parseInt(mDeposit_offline)
						+ Integer.parseInt(mInterest_offline) + Integer.parseInt(mInterestSubvention_Offline))
						- Integer.parseInt(mWithdrawl_offline) - Integer.parseInt(mBankExpenses_offline));

		EShaktiApplication.setBankDeposit(true);
		String mBankDetails = GetOfflineTransactionValues.getBankDetails();

		mLastTrDate = SelectedGroupsTask.sLastTransactionDate_Response;
		String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mLastTrDate, mCashinHand, mCashatBank,
				mBankDetails);
		String mSelectedGroupId = SelectedGroupsTask.Group_Id;
		isServiceCall = false;
		EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GROUPMASTERUPDATE,
				new GroupDetailsUpdate(mSelectedGroupId, mGroupMasterResponse));
	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub

		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();
		
		FragmentDrawer.drawer_CashinHand.setText(RegionalConversion.getRegionalConversion(AppStrings.cashinhand)

				+ SelectedGroupsTask.sCashinHand);
		FragmentDrawer.drawer_CashinHand.setTypeface(LoginActivity.sTypeface);

		 
		FragmentDrawer.drawer_CashatBank.setText(RegionalConversion.getRegionalConversion(AppStrings.cashatBank)

				+ SelectedGroupsTask.sCashatBank);
		FragmentDrawer.drawer_CashatBank.setTypeface(LoginActivity.sTypeface);
		
		FragmentDrawer.drawer_lastTR_Date.setText(RegionalConversion.getRegionalConversion(AppStrings.lastTransactionDate)
				+ " : " + SelectedGroupsTask.sLastTransactionDate_Response);
		FragmentDrawer.drawer_lastTR_Date.setTypeface(LoginActivity.sTypeface);
		
		UpdateCalendarMinMaxDateUtils.OnUpdateCalendarDate();
		
		Calendar calender = Calendar.getInstance();

		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String formattedDate = df.format(calender.getTime());
		EShaktiApplication.setLastTransDate_GroupId(SelectedGroupsTask.Group_Id);

		new GroupTempDBLastTransDate(EShaktiApplication.getLastTransDate_GroupId(),
				SelectedGroupsTask.sLastTransactionDate_Response, formattedDate);

		GroupProvider.updateGroupLastTransDateResponse();
		
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		SBus.INST.unRegister(this);
	}

}
