package com.yesteam.eshakti.view.fragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.squareup.otto.Subscribe;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.adapter.CustomAdapter;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.TransactionOffConstants;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;
import com.yesteam.eshakti.model.RowItem;
import com.yesteam.eshakti.sqlite.database.GroupProvider;
import com.yesteam.eshakti.sqlite.database.TransactionProvider;
import com.yesteam.eshakti.sqlite.database.response.DefaultOffline_MinutesResponse;
import com.yesteam.eshakti.sqlite.database.response.DefaultOffline_PLResponse;
import com.yesteam.eshakti.sqlite.database.response.GetSingleTransResponse;
import com.yesteam.eshakti.sqlite.database.response.GroupLoanOSResponse;
import com.yesteam.eshakti.sqlite.database.response.GroupMemberloanOSResponse;
import com.yesteam.eshakti.sqlite.database.response.GroupPLOSUpdateResponse;
import com.yesteam.eshakti.sqlite.database.response.TransactionResponse;
import com.yesteam.eshakti.sqlite.database.response.TransactionUpdateResponse;
import com.yesteam.eshakti.sqlite.db.model.GetGroupMemberDetails;
import com.yesteam.eshakti.sqlite.db.model.GetTransactionSingle;
import com.yesteam.eshakti.sqlite.db.model.GetTransactionSinglevalues;
import com.yesteam.eshakti.sqlite.db.model.GroupMLUpdate;
import com.yesteam.eshakti.sqlite.db.model.GroupMasterSingle;
import com.yesteam.eshakti.sqlite.db.model.GroupResponseUpdate;
import com.yesteam.eshakti.sqlite.db.model.GroupTempDBLastTransDate;
import com.yesteam.eshakti.sqlite.db.model.Transaction;
import com.yesteam.eshakti.sqlite.db.model.TransactionUpdate;
import com.yesteam.eshakti.sqlite.db.model.Transaction_UniqueIdSingle;
import com.yesteam.eshakti.sqlite.db.transactions.TransactionManager.DataType;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.utils.GetOfflineTransactionValues;
import com.yesteam.eshakti.utils.GetResponseInfo;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.utils.Get_EdiText_Filter;
import com.yesteam.eshakti.utils.Get_Offline_MobileDate;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.utils.Put_DB_GroupNameDetail_Response;
import com.yesteam.eshakti.utils.Put_DB_GroupResponse;
import com.yesteam.eshakti.utils.Reset;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.utils.TextviewUtils;
import com.yesteam.eshakti.utils.UpdateCalendarMinMaxDateUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.views.ButtonFlat;
import com.yesteam.eshakti.views.CustomHorizontalScrollView;
import com.yesteam.eshakti.views.CustomHorizontalScrollView.onScrollChangedListener;
import com.yesteam.eshakti.webservices.GetPL_DisbursementTask;
import com.yesteam.eshakti.webservices.Get_All_Mem_OutstandingTask;
import com.yesteam.eshakti.webservices.Get_Group_Minutes_NewTask;
import com.yesteam.eshakti.webservices.Get_LastTransactionID;
import com.yesteam.eshakti.webservices.Get_LoanOutstandingTask;
import com.yesteam.eshakti.webservices.Get_PurposeOfLoanTask;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Activity;

import android.app.Dialog;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Transaction_InternalLoan_DisbursementFragment extends Fragment implements OnClickListener, TaskListener {

	public static final String TAG = Transaction_InternalLoan_DisbursementFragment.class.getSimpleName();

	public static List<EditText> sPL_Fields = new ArrayList<EditText>();
	public static List<EditText> sTenure_Fields = new ArrayList<EditText>();
	public static List<TextView> sPOL_Fields = new ArrayList<TextView>();
	public static String[] sPOLvalues;
	public static String sPL_Amounts[];
	public static String sTenurePeriod[];
	public static String sSelected_POL[], sSelected_POL_Id[];
	public static String sSendToServer_PLdisburse;
	public static int sPL_total;
	public static boolean isPL_Disburse_Submit;
	@SuppressWarnings("unused")
	private String[] mPol_IdValues, mRadio_Pol_id;
	private TextView mGroupName, mCashinHand, mCashatBank, mHeader, mLoanType;

	private Button mSubmit_Raised_Button, mEdit_RaisedButton, mOk_RaisedButton;
	private EditText mPL_values, mTenure;
	private String mOutstanding[];
	int mSize;
	String response[], toBeEdit_PLdisburse[], mPOL_Response[], mPOL_Values[], mPOL_ID[];
	String memOuts = "";
	String nullAmount = "0";

	Dialog confirmationDialog;
	AlertDialog alertDialog;
	CustomAdapter custAdapter;

	private Dialog mProgressDilaog;
	private String mPldbrepayamount[];
	public static List<RowItem> sRowItems;

	int[] validatedOutstanding;
	boolean isError, isnullAmountError;
	String mLastTrDate = null, mLastTr_ID = null;
	String mRepayment;

	String mSelectedPOL_Values[], mPOLText_Values[];
	String mSelected_POL_Values = "", mPOL_Text = "";

	String loanType;
	RadioButton radioButton;
	int selectedId = 100, mPOL_Size;
	String mSelectedLoanType;
	String check;

	private TableLayout mLeftHeaderTable, mRightHeaderTable, mLeftContentTable, mRightContentTable;
	private CustomHorizontalScrollView mHSRightHeader, mHSRightContent;

	String width[] = { AppStrings.amount, AppStrings.OutsatndingAmt, AppStrings.purposeOfLoan, AppStrings.tenure };
	int[] rightHeaderWidth = new int[width.length];
	int[] rightContentWidth = new int[width.length];
	int mSavings_Values = 0;
	int InternalLoan = 0;
	public static int mTotalCollection = 0;
	public static int mTotalDisbursement = 0;
	boolean isGetTrid = false;
	Button mPerviousButton, mNextButton;
	boolean isMeetingValues = false;
	boolean isPrevious = false;
	boolean isServiceCall = false;
	String mSqliteDBStoredValues_InternalLoanDisburseValues = null;
	LinearLayout mMemberNameLayout;
	TextView mMemberName;
	public static int groupLoanCount = 0;
	private boolean isGroupLoanOutstanding = false;
	String mGroupOS_Offlineresponse = null;

	public Transaction_InternalLoan_DisbursementFragment() {
		// TODO Auto-generated constructor stub

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_PL_DISBURSE;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		SBus.INST.register(this);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Transaction_InternalLoan_DisbursementFragment.groupLoanCount = 0;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_all_transaction, container, false);

		MainFragment_Dashboard.isBackpressed = false;

		try {

			mSqliteDBStoredValues_InternalLoanDisburseValues = null;

			OnCallInternalloanValues();

			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashinHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashinHand.setTypeface(LoginActivity.sTypeface);

			mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashatBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashatBank.setTypeface(LoginActivity.sTypeface);

			/**
			 * 
			 */
			mMemberNameLayout = (LinearLayout) rootView.findViewById(R.id.member_name_layout);
			mMemberName = (TextView) rootView.findViewById(R.id.member_name);

			mHeader = (TextView) rootView.findViewById(R.id.fragmentHeader);
			if (EShaktiApplication.isStepWiseFragment()) {

				mHeader.setText(
						RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.mStepWise_LoanDibursement)));
			} else {

				mHeader.setText(
						RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.InternalLoanDisbursement)));
			}

			mHeader.setTypeface(LoginActivity.sTypeface);

			mSize = SelectedGroupsTask.member_Name.size();
			Log.d(TAG, String.valueOf(mSize));

			mLeftHeaderTable = (TableLayout) rootView.findViewById(R.id.LeftHeaderTable);
			mRightHeaderTable = (TableLayout) rootView.findViewById(R.id.RightHeaderTable);
			mLeftContentTable = (TableLayout) rootView.findViewById(R.id.LeftContentTable);
			mRightContentTable = (TableLayout) rootView.findViewById(R.id.RightContentTable);

			mHSRightHeader = (CustomHorizontalScrollView) rootView.findViewById(R.id.rightHeaderHScrollView);
			mHSRightContent = (CustomHorizontalScrollView) rootView.findViewById(R.id.rightContentHScrollView);

			mHSRightHeader.setOnScrollChangedListener(new onScrollChangedListener() {

				public void onScrollChanged(int l, int t, int oldl, int oldt) {
					// TODO Auto-generated method stub

					mHSRightContent.scrollTo(l, 0);

				}
			});

			mHSRightContent.setOnScrollChangedListener(new onScrollChangedListener() {
				@Override
				public void onScrollChanged(int l, int t, int oldl, int oldt) {
					// TODO Auto-generated method stub
					mHSRightHeader.scrollTo(l, 0);
				}
			});
			TableRow leftHeaderRow = new TableRow(getActivity());

			TableRow.LayoutParams lHeaderParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);

			TextView mMemberName_Header = new TextView(getActivity());
			mMemberName_Header.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.memberName)));
			mMemberName_Header.setTypeface(LoginActivity.sTypeface);
			mMemberName_Header.setTextColor(Color.WHITE);
			mMemberName_Header.setPadding(10, 5, 10, 5);
			mMemberName_Header.setLayoutParams(lHeaderParams);
			leftHeaderRow.addView(mMemberName_Header);

			mLeftHeaderTable.addView(leftHeaderRow);

			TableRow rightHeaderRow = new TableRow(getActivity());
			TableRow.LayoutParams rHeaderParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);
			rHeaderParams.setMargins(10, 0, 10, 0);

			TextView mPL_Header = new TextView(getActivity());
			mPL_Header.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.amount)));
			mPL_Header.setTypeface(LoginActivity.sTypeface);
			mPL_Header.setTextColor(Color.WHITE);
			mPL_Header.setPadding(10, 5, 10, 5);
			mPL_Header.setGravity(Gravity.CENTER);
			mPL_Header.setLayoutParams(rHeaderParams);
			mPL_Header.setBackgroundResource(R.color.tableHeader);
			rightHeaderRow.addView(mPL_Header);

			TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);
			contentParams.setMargins(10, 0, 10, 0);

			TextView mOutstanding_Header = new TextView(getActivity());
			mOutstanding_Header
					.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.OutsatndingAmt)));
			mOutstanding_Header.setTypeface(LoginActivity.sTypeface);
			mOutstanding_Header.setTextColor(Color.WHITE);
			mOutstanding_Header.setPadding(25, 5, 10, 5);
			mOutstanding_Header.setGravity(Gravity.RIGHT);
			mOutstanding_Header.setLayoutParams(contentParams);
			mOutstanding_Header.setBackgroundResource(R.color.tableHeader);
			rightHeaderRow.addView(mOutstanding_Header);

			TableRow.LayoutParams POLParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);
			POLParams.setMargins(10, 0, 10, 0);

			TextView mPurposeOfLoan_Header = new TextView(getActivity());
			mPurposeOfLoan_Header
					.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.purposeOfLoan)));
			mPurposeOfLoan_Header.setTypeface(LoginActivity.sTypeface);
			mPurposeOfLoan_Header.setTextColor(Color.WHITE);
			mPurposeOfLoan_Header.setGravity(Gravity.CENTER);
			mPurposeOfLoan_Header.setLayoutParams(POLParams);
			mPurposeOfLoan_Header.setPadding(10, 5, 10, 5);
			mPurposeOfLoan_Header.setBackgroundResource(R.color.tableHeader);
			rightHeaderRow.addView(mPurposeOfLoan_Header);

			TableRow.LayoutParams tenureParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);
			tenureParams.setMargins(10, 0, 20, 0);

			TextView mTenure_Header = new TextView(getActivity());
			mTenure_Header.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.tenure)));
			mTenure_Header.setTypeface(LoginActivity.sTypeface);
			mTenure_Header.setTextColor(Color.WHITE);
			mTenure_Header.setSingleLine(true);
			mTenure_Header.setGravity(Gravity.CENTER);
			mTenure_Header.setLayoutParams(tenureParams);// (rHeaderParams);
			mTenure_Header.setBackgroundResource(R.color.tableHeader);
			mTenure_Header.setPadding(10, 5, 10, 5);
			rightHeaderRow.addView(mTenure_Header);

			mRightHeaderTable.addView(rightHeaderRow);

			try {

				for (int j = 0; j < mSize; j++) {

					TableRow leftContentRow = new TableRow(getActivity());

					TableRow.LayoutParams leftContentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
							LayoutParams.WRAP_CONTENT, 1f);
					leftContentParams.setMargins(5, 5, 5, 15);

					final TextView memberName_Text = new TextView(getActivity());
					memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
							String.valueOf(SelectedGroupsTask.member_Name.elementAt(j))));
					memberName_Text.setTypeface(LoginActivity.sTypeface);
					memberName_Text.setTextColor(color.black);
					memberName_Text.setPadding(5, 5, 5, 5);
					memberName_Text.setLayoutParams(leftContentParams);
					leftContentRow.addView(memberName_Text);

					mLeftContentTable.addView(leftContentRow);

					TableRow rightContentRow = new TableRow(getActivity());

					TableRow.LayoutParams rightContentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
							LayoutParams.WRAP_CONTENT, 1f);
					rightContentParams.setMargins(10, 5, 10, 5);

					mPL_values = new EditText(getActivity());
					mPL_values.setId(j);
					sPL_Fields.add(mPL_values);
					mPL_values.setInputType(InputType.TYPE_CLASS_NUMBER);
					mPL_values.setPadding(5, 5, 5, 5);
					mPL_values.setFilters(Get_EdiText_Filter.editText_filter());
					mPL_values.setBackgroundResource(R.drawable.edittext_background);
					mPL_values.setLayoutParams(rightContentParams);
					mPL_values.setWidth(150);
					mPL_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

						@Override
						public void onFocusChange(View v, boolean hasFocus) {
							// TODO Auto-generated method stub
							if (hasFocus) {
								((EditText) v).setGravity(Gravity.LEFT);

								mMemberNameLayout.setVisibility(View.VISIBLE);
								mMemberName.setText(memberName_Text.getText().toString().trim());
								mMemberName.setTypeface(LoginActivity.sTypeface);
								TextviewUtils.manageBlinkEffect(mMemberName, getActivity());
							} else {
								((EditText) v).setGravity(Gravity.RIGHT);
								mMemberNameLayout.setVisibility(View.GONE);
								mMemberName.setText("");
							}
						}
					});
					rightContentRow.addView(mPL_values);

					TableRow.LayoutParams contentRow_Params = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
							LayoutParams.WRAP_CONTENT, 1f);
					contentRow_Params.setMargins(10, 5, 10, 5);

					TextView outstanding = new TextView(getActivity());
					outstanding.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(mOutstanding[j])));
					outstanding.setTextColor(color.black);
					outstanding.setGravity(Gravity.RIGHT);
					outstanding.setLayoutParams(contentRow_Params);// (rightContentParams);
					outstanding.setPadding(10, 0, 10, 5);
					rightContentRow.addView(outstanding);

					TableRow.LayoutParams contentRowParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
							LayoutParams.WRAP_CONTENT, 1f);
					contentRowParams.setMargins(10, 5, 10, 5);

					mLoanType = new TextView(getActivity());
					mLoanType.setText(loanType);

					mLoanType.setTypeface(LoginActivity.sTypeface);
					mLoanType.setId(j);
					mLoanType.setLayoutParams(contentRowParams);// (rightContentParams);
					mLoanType.setPadding(10, 0, 10, 5);
					sPOL_Fields.add(mLoanType);
					mLoanType.setOnClickListener(new OnClickListener() {

						@SuppressWarnings("deprecation")
						@Override
						public void onClick(final View v) {
							// TODO Auto-generated method stub

							try {

								mMemberNameLayout.setVisibility(View.VISIBLE);
								mMemberName.setText(memberName_Text.getText().toString().trim());
								mMemberName.setTypeface(LoginActivity.sTypeface);
								TextviewUtils.manageBlinkEffect(mMemberName, getActivity());
								final Dialog ChooseLoanType;
								final View dialogView;
								final RadioGroup radioGroup;
								ChooseLoanType = new Dialog(getActivity());

								LayoutInflater li = (LayoutInflater) getActivity()
										.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
								dialogView = li.inflate(R.layout.dialog_loantype, null, false);

								TextView confirmationHeader = (TextView) dialogView
										.findViewById(R.id.dialog_ChooseLabel);
								confirmationHeader.setText(AppStrings.chooseLabel);
								confirmationHeader.setTypeface(LoginActivity.sTypeface);

								int radioColor = getResources().getColor(R.color.pink);
								radioGroup = (RadioGroup) dialogView.findViewById(R.id.dialog_RadioGroup);
								radioGroup.removeAllViews();

								for (int j = 0; j < mPOL_Values.length; j++) {

									radioButton = new RadioButton(getActivity());
									radioButton.setText(mPOL_Values[j]);
									radioButton.setId(j);
									radioButton.setTypeface(LoginActivity.sTypeface);
									int currentapiVersion = android.os.Build.VERSION.SDK_INT;
									if (currentapiVersion >= android.os.Build.VERSION_CODES.LOLLIPOP) {

										radioButton.setButtonTintList(ColorStateList.valueOf(radioColor));
									}

									radioGroup.addView(radioButton);

								}

								ButtonFlat okButton = (ButtonFlat) dialogView.findViewById(R.id.dialog_yes_button);
								okButton.setText(RegionalConversion.getRegionalConversion(AppStrings.dialogOk));
								okButton.setTypeface(LoginActivity.sTypeface);
								okButton.setOnClickListener(new OnClickListener() {

									@Override
									public void onClick(View view) {
										// TODO Auto-generated method
										// stub

										selectedId = radioGroup.getCheckedRadioButtonId();
										Log.e(TAG, String.valueOf(selectedId));
										if ((selectedId != 100) && (selectedId != (-1))) {

											// find the radiobutton by
											// returned id
											RadioButton radioLoanButton = (RadioButton) dialogView
													.findViewById(selectedId);

											mSelectedLoanType = radioLoanButton.getText().toString();
											Log.v("On Selected LOAN TYPE", mSelectedLoanType);

											Log.v("view ID check : ", String.valueOf(v.getId()));

											TextView selectedTextView = (TextView) v.findViewById(v.getId());

											if (selectedId == 0) {
												selectedTextView.setText(AppStrings.chooseLabel);
											} else {
												selectedTextView.setText(mSelectedLoanType);
											}

											selectedTextView.setTypeface(LoginActivity.sTypeface);

											System.out.println("------idssssssssssss-----" + v.getId());

											ChooseLoanType.dismiss();

										} else {
											TastyToast.makeText(getActivity(), AppStrings.choosePOLAlert,
													TastyToast.LENGTH_SHORT, TastyToast.WARNING);

										}

									}

								});

								ChooseLoanType.getWindow()
										.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
								ChooseLoanType.requestWindowFeature(Window.FEATURE_NO_TITLE);
								ChooseLoanType.setCanceledOnTouchOutside(false);
								ChooseLoanType.setContentView(dialogView);
								ChooseLoanType.setCancelable(true);
								ChooseLoanType.show();

							} catch (Exception e) {
								e.printStackTrace();
							}

						}
					});
					rightContentRow.addView(mLoanType);

					TableRow.LayoutParams contentRowParams1 = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
							LayoutParams.WRAP_CONTENT, 1f);
					contentRowParams1.setMargins(10, 5, 10, 5);

					mTenure = new EditText(getActivity());
					mTenure.setId(j);
					sTenure_Fields.add(mTenure);
					mTenure.setInputType(InputType.TYPE_CLASS_NUMBER);
					mTenure.setPadding(5, 5, 5, 5);
					mTenure.setFilters(Get_EdiText_Filter.editText_tenure_filter());
					mTenure.setBackgroundResource(R.drawable.edittext_background);
					mTenure.setLayoutParams(contentRowParams1);// (rightContentParams);
					mTenure.setWidth(150);
					mTenure.setOnFocusChangeListener(new View.OnFocusChangeListener() {

						@Override
						public void onFocusChange(View v, boolean hasFocus) {
							// TODO Auto-generated method stub
							if (hasFocus) {
								((EditText) v).setGravity(Gravity.LEFT);

								mMemberNameLayout.setVisibility(View.VISIBLE);
								mMemberName.setText(memberName_Text.getText().toString().trim());
								mMemberName.setTypeface(LoginActivity.sTypeface);
								TextviewUtils.manageBlinkEffect(mMemberName, getActivity());

							} else {

								mMemberNameLayout.setVisibility(View.GONE);
								mMemberName.setText("");

								((EditText) v).setGravity(Gravity.RIGHT);
							}

						}
					});
					rightContentRow.addView(mTenure);

					mRightContentTable.addView(rightContentRow);

				}

			} catch (ArrayIndexOutOfBoundsException e) {
				e.printStackTrace();

				TastyToast.makeText(getActivity(), AppStrings.adminAlert, TastyToast.LENGTH_SHORT, TastyToast.WARNING);

				getActivity().finish();
			}

			mSubmit_Raised_Button = (Button) rootView.findViewById(R.id.fragment_Submit_button);
			mSubmit_Raised_Button.setText(RegionalConversion.getRegionalConversion(AppStrings.submit));
			mSubmit_Raised_Button.setTypeface(LoginActivity.sTypeface);
			mSubmit_Raised_Button.setOnClickListener(this);

			mPerviousButton = (Button) rootView.findViewById(R.id.fragment_Previousbutton);
			mPerviousButton.setText(RegionalConversion.getRegionalConversion(AppStrings.mPervious));
			mPerviousButton.setTypeface(LoginActivity.sTypeface);
			mPerviousButton.setOnClickListener(this);

			mNextButton = (Button) rootView.findViewById(R.id.fragment_Nextbutton);
			mNextButton.setText(RegionalConversion.getRegionalConversion(AppStrings.mNext));
			mNextButton.setTypeface(LoginActivity.sTypeface);
			mNextButton.setOnClickListener(this);

			if (EShaktiApplication.isDefault()) {
				mPerviousButton.setVisibility(View.INVISIBLE);
				mNextButton.setVisibility(View.VISIBLE);
			}
			resizeMemberNameWidth();
			resizeRightSideTable();
			resizeBodyTableRowHeight();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return rootView;
	}

	private void resizeRightSideTable() {
		// TODO Auto-generated method stub
		int rightHeaderCount = (((TableRow) mRightHeaderTable.getChildAt(0)).getChildCount());
		for (int i = 0; i < rightHeaderCount; i++) {
			rightHeaderWidth[i] = viewWidth(((TableRow) mRightHeaderTable.getChildAt(0)).getChildAt(i));
			rightContentWidth[i] = viewWidth(((TableRow) mRightContentTable.getChildAt(0)).getChildAt(i));
		}
		for (int i = 0; i < rightHeaderCount; i++) {
			if (rightHeaderWidth[i] < rightContentWidth[i]) {
				((TableRow) mRightHeaderTable.getChildAt(0)).getChildAt(i)
						.getLayoutParams().width = rightContentWidth[i];
			} else {
				((TableRow) mRightContentTable.getChildAt(0)).getChildAt(i)
						.getLayoutParams().width = rightHeaderWidth[i];
			}
		}
	}

	private void resizeMemberNameWidth() {
		// TODO Auto-generated method stub
		int leftHeadertWidth = viewWidth(mLeftHeaderTable);
		int leftContentWidth = viewWidth(mLeftContentTable);

		if (leftHeadertWidth < leftContentWidth) {
			mLeftHeaderTable.getLayoutParams().width = leftContentWidth;
		} else {
			mLeftContentTable.getLayoutParams().width = leftHeadertWidth;
		}
	}

	private void resizeBodyTableRowHeight() {

		int leftContentTable_ChildCount = mLeftContentTable.getChildCount();

		for (int x = 0; x < leftContentTable_ChildCount; x++) {

			TableRow leftContentTableRow = (TableRow) mLeftContentTable.getChildAt(x);
			TableRow rightContentTableRow = (TableRow) mRightContentTable.getChildAt(x);

			int rowLeftHeight = viewHeight(leftContentTableRow);
			int rowRightHeight = viewHeight(rightContentTableRow);

			TableRow tableRow = rowLeftHeight < rowRightHeight ? leftContentTableRow : rightContentTableRow;
			int finalHeight = rowLeftHeight > rowRightHeight ? rowLeftHeight : rowRightHeight;

			this.matchLayoutHeight(tableRow, finalHeight);
		}

	}

	private void matchLayoutHeight(TableRow tableRow, int height) {

		int tableRowChildCount = tableRow.getChildCount();

		// if a TableRow has only 1 child
		if (tableRow.getChildCount() == 1) {

			View view = tableRow.getChildAt(0);
			TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();
			params.height = height - (params.bottomMargin + params.topMargin);

			return;
		}

		// if a TableRow has more than 1 child
		for (int x = 0; x < tableRowChildCount; x++) {

			View view = tableRow.getChildAt(x);

			TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();

			if (!isTheHeighestLayout(tableRow, x)) {
				params.height = height - (params.bottomMargin + params.topMargin);
				return;
			}
		}

	}

	// check if the view has the highest height in a TableRow
	private boolean isTheHeighestLayout(TableRow tableRow, int layoutPosition) {

		int tableRowChildCount = tableRow.getChildCount();
		int heighestViewPosition = -1;
		int viewHeight = 0;

		for (int x = 0; x < tableRowChildCount; x++) {
			View view = tableRow.getChildAt(x);
			int height = this.viewHeight(view);

			if (viewHeight < height) {
				heighestViewPosition = x;
				viewHeight = height;
			}
		}

		return heighestViewPosition == layoutPosition;
	}

	// read a view's height
	private int viewHeight(View view) {
		view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
		return view.getMeasuredHeight();
	}

	// read a view's width
	private int viewWidth(View view) {
		view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
		return view.getMeasuredWidth();
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {

		case R.id.fragment_Submit_button:

			mMemberNameLayout.setVisibility(View.GONE);
			mMemberName.setText("");

			// To avoid Double click
			mSubmit_Raised_Button.setClickable(false);

			isPL_Disburse_Submit = true;
			sPL_total = 0;
			mTotalDisbursement = 0;

			try {

				mSelectedPOL_Values = new String[sPL_Fields.size()];

				sPL_Amounts = new String[sPL_Fields.size()];
				sTenurePeriod = new String[sTenure_Fields.size()];
				sPOLvalues = new String[sPOL_Fields.size()];
				mPol_IdValues = new String[sPOL_Fields.size()];
				mSelected_POL_Values = "";
				sSendToServer_PLdisburse = "";

				sSelected_POL_Id = new String[SelectedGroupsTask.member_Id.size()];

				for (int i = 0; i < sPL_Amounts.length; i++) {

					sPL_Amounts[i] = String.valueOf(sPL_Fields.get(i).getText());

					if ((sPL_Amounts[i].equals("")) || (sPL_Amounts[i] == null)) {
						sPL_Amounts[i] = nullAmount;
					}

					if (sPL_Amounts[i].matches("\\d*\\.?\\d+")) { // match a
																	// decimal
																	// number

						int amount = (int) Math.round(Double.parseDouble(sPL_Amounts[i]));
						sPL_Amounts[i] = String.valueOf(amount);
					}

					sTenurePeriod[i] = String.valueOf(sTenure_Fields.get(i).getText());
					if ((sTenurePeriod[i].equals("")) || (sTenurePeriod[i] == null)) {
						sTenurePeriod[i] = nullAmount;
					}

					sPOLvalues[i] = String.valueOf(sPOL_Fields.get(i).getText());
					Log.v("sPOLvalues", sPOLvalues[i]);

					if (sPOLvalues[i].equals(loanType)) {
						sSelected_POL[i] = nullAmount;
						sSelected_POL_Id[i] = "0";
					} else if (!sPOLvalues[i].equals(loanType)) {
						sSelected_POL[i] = sPOLvalues[i]; // sSelected_POL[i];
						for (int j = 0; j < mPOL_Values.length; j++) {

							if (mPOL_Values[j].equals(sPOLvalues[i])) {
								Log.e("mPOL_ID", mPOL_ID[j]);
								sSelected_POL_Id[i] = mPOL_ID[j];
								Log.e("Selected pol id$$$$$$$$$$$", sSelected_POL_Id[i] + "");
							}

						}
						Log.e("cccccccccccccccc", String.valueOf(sSelected_POL[i]));

					}
					Log.v("Selected pol id", sSelected_POL_Id[i] + "");

					if (!sPL_Amounts[i].equals(nullAmount)) {
						if ((sSelected_POL_Id[i].equals(nullAmount)) || (sTenurePeriod[i].equals(nullAmount))) {

							isError = true;
						}
					}
					if (sPL_Amounts[i].equals(nullAmount)) {
						if ((!sSelected_POL_Id[i].equals(nullAmount)) || (!sTenurePeriod[i].equals(nullAmount))) {

							isnullAmountError = true;
						}
					}

					if (sSelected_POL_Id[i] != null) {
						if (sSelected_POL_Id[i].equals(nullAmount)) {
							mSelected_POL_Values = mSelected_POL_Values + AppStrings.dialogNo + ",";
						} else if (!sSelected_POL_Id[i].equals(nullAmount)) {
							mSelected_POL_Values = mSelected_POL_Values + sSelected_POL[i] + ",";
						}

						Log.v("kkkkkkkkkkkkkkk ", mSelected_POL_Values);

						sPL_total = sPL_total + Integer.parseInt(sPL_Amounts[i]);

						sSendToServer_PLdisburse = sSendToServer_PLdisburse
								+ String.valueOf(SelectedGroupsTask.member_Id.elementAt(i)) + "~"
								+ String.valueOf(sPL_Amounts[i]).trim() + "~" + String.valueOf(sSelected_POL_Id[i])// String.valueOf(sSelected_POL[i].trim())
								+ "~" + String.valueOf(sTenurePeriod[i]) + "~";
					}

				}

				mSelectedPOL_Values = mSelected_POL_Values.split(",");

				Log.d(TAG, "Total " + Integer.toString(sPL_total));

				Log.d(TAG, "Vals" + sSendToServer_PLdisburse);

				if ((sPL_total <= Integer.parseInt(SelectedGroupsTask.sCashinHand)) && (sPL_total != 0)
						&& (!Boolean.valueOf(isError)) && (!Boolean.valueOf(isnullAmountError))) {
					System.out.println("Do Navigate");

					confirmationDialog = new Dialog(getActivity());

					LayoutInflater inflater = getActivity().getLayoutInflater();
					View dialogView = inflater.inflate(R.layout.dialog_confirmation, null);
					dialogView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
							LayoutParams.WRAP_CONTENT));

					TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
					confirmationHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.confirmation));
					confirmationHeader.setTypeface(LoginActivity.sTypeface);

					TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

					TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
							LayoutParams.WRAP_CONTENT, 1f);
					contentParams.setMargins(10, 5, 10, 5);

					for (int i = 0; i < sPL_Amounts.length; i++) {

						TableRow indv_SavingsRow = new TableRow(getActivity());

						TextView memberName_Text = new TextView(getActivity());
						memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
								String.valueOf(SelectedGroupsTask.member_Name.elementAt(i))));
						memberName_Text.setTypeface(LoginActivity.sTypeface);
						memberName_Text.setTextColor(color.black);
						memberName_Text.setPadding(5, 5, 5, 5);
						memberName_Text.setLayoutParams(contentParams);
						indv_SavingsRow.addView(memberName_Text);

						TextView confirm_values = new TextView(getActivity());
						confirm_values
								.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sPL_Amounts[i])));
						confirm_values.setTextColor(color.black);
						confirm_values.setPadding(5, 5, 5, 5);
						confirm_values.setGravity(Gravity.RIGHT);
						confirm_values.setLayoutParams(contentParams);
						indv_SavingsRow.addView(confirm_values);

						TextView POL_values = new TextView(getActivity());
						POL_values.setText(
								GetSpanText.getSpanString(getActivity(), String.valueOf(mSelectedPOL_Values[i])));
						POL_values.setTextColor(color.black);
						POL_values.setPadding(5, 5, 5, 5);
						POL_values.setGravity(Gravity.RIGHT);
						POL_values.setLayoutParams(contentParams);
						indv_SavingsRow.addView(POL_values);

						TextView tenure_values = new TextView(getActivity());
						tenure_values
								.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sTenurePeriod[i])));
						tenure_values.setTextColor(color.black);
						tenure_values.setPadding(5, 5, 5, 5);
						tenure_values.setGravity(Gravity.RIGHT);
						tenure_values.setLayoutParams(contentParams);
						indv_SavingsRow.addView(tenure_values);

						confirmationTable.addView(indv_SavingsRow,
								new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

					}
					View rullerView = new View(getActivity());
					rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
					rullerView.setBackgroundColor(Color.rgb(0, 199, 140));// rgb(255,
																			// 229,
																			// 242));
					confirmationTable.addView(rullerView);

					TableRow totalRow = new TableRow(getActivity());

					TextView totalText = new TextView(getActivity());
					totalText.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.total)));
					totalText.setTypeface(LoginActivity.sTypeface);
					totalText.setTextColor(color.black);
					totalText.setPadding(5, 5, 5, 5);// (5, 10, 5, 10);
					totalText.setLayoutParams(contentParams);
					totalRow.addView(totalText);

					TextView totalAmount = new TextView(getActivity());
					totalAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sPL_total)));
					totalAmount.setTextColor(color.black);
					totalAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
					totalAmount.setGravity(Gravity.RIGHT);
					totalAmount.setLayoutParams(contentParams);
					totalRow.addView(totalAmount);

					TextView emptyAmount = new TextView(getActivity());
					emptyAmount.setText("");
					emptyAmount.setTextColor(color.black);
					emptyAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
					emptyAmount.setGravity(Gravity.RIGHT);
					emptyAmount.setLayoutParams(contentParams);
					totalRow.addView(emptyAmount);

					TextView total_Amount = new TextView(getActivity());
					total_Amount.setText("");
					total_Amount.setTextColor(color.black);
					total_Amount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
					total_Amount.setGravity(Gravity.RIGHT);
					total_Amount.setLayoutParams(contentParams);
					totalRow.addView(total_Amount);

					confirmationTable.addView(totalRow,
							new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

					mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit_button);
					mEdit_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.edit));
					mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
					mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
																			// 205,
																			// 0));
					mEdit_RaisedButton.setOnClickListener(this);

					mOk_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Ok_button);
					mOk_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.yes));
					mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
					mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
					mOk_RaisedButton.setOnClickListener(this);

					mTotalDisbursement = sPL_total;
					Log.e("PL Total Valuesssss", mTotalDisbursement + "");

					confirmationDialog.getWindow()
							.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
					confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					confirmationDialog.setCanceledOnTouchOutside(false);
					confirmationDialog.setContentView(dialogView);
					confirmationDialog.setCancelable(true);
					confirmationDialog.show();

					MarginLayoutParams margin = (MarginLayoutParams) dialogView.getLayoutParams();
					margin.leftMargin = 10;
					margin.rightMargin = 10;
					margin.topMargin = 10;
					margin.bottomMargin = 10;
					margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);

				} else if (sPL_total > Integer.parseInt(SelectedGroupsTask.sCashinHand)) {

					TastyToast.makeText(getActivity(), AppStrings.cashinHandAlert, TastyToast.LENGTH_SHORT,
							TastyToast.WARNING);

					sSendToServer_PLdisburse = Reset.reset(sSendToServer_PLdisburse);
					sPL_total = Integer.parseInt(nullAmount);

				} else if (Boolean.valueOf(isnullAmountError) || (sPL_total == Integer.parseInt(nullAmount))) {

					isnullAmountError = false;
					TastyToast.makeText(getActivity(), AppStrings.nullAlert, TastyToast.LENGTH_SHORT,
							TastyToast.WARNING);

					sSendToServer_PLdisburse = Reset.reset(sSendToServer_PLdisburse);
					sPL_total = Integer.parseInt(nullAmount);

				} else if (Boolean.valueOf(isError)) {

					isError = false;

					TastyToast.makeText(getActivity(), AppStrings.Tenure_POL_Alert, TastyToast.LENGTH_SHORT,
							TastyToast.WARNING);
					sSendToServer_PLdisburse = Reset.reset(sSendToServer_PLdisburse);
					sPL_total = Integer.parseInt(nullAmount);

				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			// }
			break;

		case R.id.fragment_Edit_button:

			mTotalDisbursement = 0;

			sSendToServer_PLdisburse = Reset.reset(sSendToServer_PLdisburse);
			sPL_total = Integer.parseInt(nullAmount);
			mSelected_POL_Values = Reset.reset(mSelected_POL_Values);
			confirmationDialog.dismiss();
			isServiceCall = false;
			mMemberNameLayout.setVisibility(View.GONE);
			mMemberName.setText("");

			break;

		case R.id.fragment_Ok_button:

			if (ConnectionUtils.isNetworkAvailable(getActivity())) {

				try {
					if (EShaktiApplication.getLoginFlag().equals(Constants.ONLINEFLAG)) {

						if (!isServiceCall) {
							isServiceCall = true;

							new GetPL_DisbursementTask(Transaction_InternalLoan_DisbursementFragment.this).execute();
						}
					} else {
						TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
								TastyToast.ERROR);
					}

				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			} else {
				// Do offline Stuffs
				if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
					EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GET_TRANS_SINGLE,
							new GetTransactionSingle(PrefUtils.getOfflineUniqueID()));
				} else {
					TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
							TastyToast.ERROR);
				}
			}

			break;
		case R.id.fragment_Previousbutton:

			if (SelectedGroupsTask.loan_Id.size() > 1) {
				Log.e("----Previous loan id------",
						SelectedGroupsTask.loan_Id.elementAt(SelectedGroupsTask.loan_Id.size() - 2) + "");

				Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID = SelectedGroupsTask.loan_Id
						.elementAt(SelectedGroupsTask.loan_Id.size() - 2);
				Transaction_MemLoanRepaid_MenuFragment.sSelected_LoanName = SelectedGroupsTask.loan_Name
						.elementAt(SelectedGroupsTask.loan_Name.size() - 2);
				EShaktiApplication.setLoanId(Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID);
				EShaktiApplication.setLoanName(Transaction_MemLoanRepaid_MenuFragment.sSelected_LoanName);
			} else {
				Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID = "0";
			}
			if (ConnectionUtils.isNetworkAvailable(getActivity())) {

				if (PrefUtils.getGroupMasterResValues() != null && PrefUtils.getGroupMasterResValues().equals("1")) {

					if (Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID.equals("0")) {
						EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.DEFAULT_PLOS,
								new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));
					} else {

						EShaktiApplication.getInstance().getTransactionManager().startTransaction(
								DataType.GROUP_MASTER_MEMBERLOANOS,
								new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));

					}

				} else {
					new Get_All_Mem_OutstandingTask(this).execute();
				}

			} else {
				if (Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID.equals("0")) {
					EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.DEFAULT_PLOS,
							new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));
				} else {

					EShaktiApplication.getInstance().getTransactionManager().startTransaction(
							DataType.GROUP_MASTER_MEMBERLOANOS,
							new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));

				}

			}

			isPrevious = true;
			break;
		case R.id.fragment_Nextbutton:

			for (int i = 0; i < SelectedGroupsTask.loan_Id.size(); i++) {
				if (!SelectedGroupsTask.loan_Id.elementAt(i).equals("0")) {

					try {
						Transaction_GroupLoanRepaidMenuFragment.mloan_Id = SelectedGroupsTask.loan_Id.elementAt(i)
								.toString();
						System.out.println("Loan Id" + Transaction_GroupLoanRepaidMenuFragment.mloan_Id);

						Transaction_GroupLoanRepaidMenuFragment.mloan_Name = SelectedGroupsTask.loan_Name.elementAt(i)
								.toString();
						System.out.println("Loan Name:" + Transaction_GroupLoanRepaidMenuFragment.mloan_Name);

						String temp_loanName = SelectedGroupsTask.loan_Name.elementAt(i).toString();

						Transaction_GroupLoanRepaidMenuFragment.mSendTo_ServerLoan_Id = SelectedGroupsTask.loan_Id
								.elementAt(i).toString();

						EShaktiApplication.setLoanId(String.valueOf(Transaction_GroupLoanRepaidMenuFragment.mloan_Id));

						EShaktiApplication
								.setGroupLoanRepaymentLoanBankName(SelectedGroupsTask.loanAcc_bankName.elementAt(i));
						EShaktiApplication
								.setGroupLoanRepaymentAccNo(SelectedGroupsTask.loanAcc_loanAccNo.elementAt(i));

						if (Transaction_GroupLoanRepaidMenuFragment.mloan_Name.equals(temp_loanName)) {

							if (ConnectionUtils.isNetworkAvailable(getActivity())) {

								if (PrefUtils.getGroupMasterResValues() != null
										&& PrefUtils.getGroupMasterResValues().equals("1")) {

									EShaktiApplication.getInstance().getTransactionManager().startTransaction(
											DataType.GET_GROUPMASTER_GLOS,
											new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));

								} else {
									isGroupLoanOutstanding = true;
									new Get_LoanOutstandingTask(Transaction_InternalLoan_DisbursementFragment.this)
											.execute();
								}
							} else {
								// DO OFFLINE STUFFS

								if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
									EShaktiApplication.getInstance().getTransactionManager().startTransaction(
											DataType.GET_GROUPMASTER_GLOS,
											new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));
								} else {
									TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF,
											TastyToast.LENGTH_SHORT, TastyToast.ERROR);
								}

							}

						}
						i = SelectedGroupsTask.loan_Id.size();
						Log.e("GroupLoan Before inc groupLoanCount size &&&& -------------",
								Transaction_InternalLoan_DisbursementFragment.groupLoanCount + "");
						Transaction_InternalLoan_DisbursementFragment.groupLoanCount = Transaction_InternalLoan_DisbursementFragment.groupLoanCount
								+ 1;
						Log.e("GroupLoan After inc groupLoanCount size &&&& ========",
								Transaction_InternalLoan_DisbursementFragment.groupLoanCount + "");

					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					if (ConnectionUtils.isNetworkAvailable(getActivity())) {
						System.out.println("----------- isMeetingValues true----");
						isMeetingValues = true;
						new Get_Group_Minutes_NewTask(this).execute();

					} else {
						if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
							EShaktiApplication.getInstance().getTransactionManager().startTransaction(
									DataType.DEFAULT_MINUTESOFMEETINGS,
									new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));
						} else {

							TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
									TastyToast.ERROR);
						}
					}
				}
			}

			break;
		default:
			break;
		}

	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub
		mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
		mProgressDilaog.show();
	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub

		if (mProgressDilaog != null) {
			mProgressDilaog.dismiss();
			if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {
				getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub

						if (!result.equals("FAIL")) {
							isServiceCall = false;
							TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg,
									TastyToast.LENGTH_SHORT, TastyToast.ERROR);

							Constants.NETWORKCOMMONFLAG = "SUCCESS";
						}

					}
				});

			} else {
				try {
					if (isPrevious) {
						isPrevious = false;
						if (Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID.equals("0")) {
							Transaction_PersonalLoanRepaidFragment savingsFragment = new Transaction_PersonalLoanRepaidFragment();
							setFragment(savingsFragment);
						} else {
							Transaction_MemLoanRepaidFragment savingsFragment = new Transaction_MemLoanRepaidFragment();
							setFragment(savingsFragment);
						}
					} else {

						if (!isMeetingValues) {

							if (isGetTrid) {
								isGetTrid = false;
								callOfflineDataUpdate();
							} else if (isGroupLoanOutstanding) {
								isGroupLoanOutstanding = false;
								EShaktiApplication.getInstance().getTransactionManager().startTransaction(
										DataType.GET_GROUPMASTER_GLOS,
										new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));
							} else {
								if (GetResponseInfo.sResponseUpdate[0].equals("Yes")) {

								} else {

									TastyToast.makeText(getActivity(), AppStrings.transactionFailAlert,
											TastyToast.LENGTH_SHORT, TastyToast.ERROR);

									MainFragment_Dashboard defaultReportsFragment = new MainFragment_Dashboard();
									setFragment(defaultReportsFragment);
								}

								confirmationDialog.dismiss();

								new Get_LastTransactionID(Transaction_InternalLoan_DisbursementFragment.this).execute();
								isGetTrid = true;
							}
						} else {
							isMeetingValues = false;
							Meeting_MinutesOfMeetingFragment meetingFragment = new Meeting_MinutesOfMeetingFragment();
							setFragment(meetingFragment);
						}
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		}
	}

	private void callOfflineDataUpdate() {
		// TODO Auto-generated method stub
		StringBuilder builder = new StringBuilder();
		String mMasterresponse, mPLMemberId, mPLDB, result, mPLDBrepayment = null;
		for (int i = 0; i < sPL_Amounts.length; i++) {

			validatedOutstanding[i] = validatedOutstanding[i] + Integer.parseInt(sPL_Amounts[i]);

			mPLMemberId = String.valueOf(SelectedGroupsTask.member_Id.elementAt(i));

			mPLDB = String.valueOf(validatedOutstanding[i] + ".00");
			mPLDBrepayment = mPldbrepayamount[i];
			Log.v("PLDB Repay Amount", mPLDBrepayment);
			mMasterresponse = mPLMemberId + "~" + mPLDB + "~" + mPLDBrepayment + "~";

			builder.append(mMasterresponse);

		}
		result = builder.toString();
		String sSendToServer_PLDB = "0" + "#" + result;
		Log.e("Send to Server Insert Values $$$$$", sSendToServer_PLDB);
		String mCashinHand = SelectedGroupsTask.sCashinHand;

		String mCashatBank = SelectedGroupsTask.sCashatBank;
		String mBankDetails = GetOfflineTransactionValues.getBankDetails();

		mLastTrDate = SelectedGroupsTask.sLastTransactionDate_Response;
		String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mLastTrDate, mCashinHand, mCashatBank,
				mBankDetails);
		String mSelectedGroupId = SelectedGroupsTask.Group_Id;
		EShaktiApplication.setPLOS(true);
		isServiceCall = false;
		EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GROUP_UPDATE_PLDB,
				new GroupMLUpdate(mSelectedGroupId, mGroupMasterResponse, sSendToServer_PLDB));

	}

	@Subscribe
	public void OnGetSingleTransactionPersonalLos(final GetSingleTransResponse getSingleTransResponse) {
		switch (getSingleTransResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), getSingleTransResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), getSingleTransResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			try {
				String mUniqueId = GetTransactionSinglevalues.getUniqueId();
				String mPersonalLDBValues = GetTransactionSinglevalues.getPersonalloandisbursement();
				String mCurrentTransDate = null;

				if (publicValues.mOffline_Trans_CurrentDate != null) {
					mCurrentTransDate = publicValues.mOffline_Trans_CurrentDate;
				}
				mLastTrDate = DatePickerDialog.sDashboardDate;
				mLastTr_ID = SelectedGroupsTask.sTr_ID.toString();

				String mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
				String mMobileDate = Get_Offline_MobileDate.getOffline_MobileDate(getActivity());
				String mTransaction_UniqueId = PrefUtils.getOfflineUniqueID();
				String mPersonalLoanDisburse = sSendToServer_PLdisburse + "#" + mTrasactiondate + "#" + mMobileDate;

				Log.e("PLDB @@@@#####", mPersonalLoanDisburse);
				if (!mPersonalLoanDisburse.contains("?") && !mCurrentTransDate.contains("?")
						&& !mTransaction_UniqueId.contains("?")) {

					if (mUniqueId == null && mPersonalLDBValues == null) {

						mSqliteDBStoredValues_InternalLoanDisburseValues = mPersonalLoanDisburse;

						if (mLastTrDate != null && mMobileDate != null && mTransaction_UniqueId != null
								&& mCurrentTransDate != null) {
							Log.i(TAG, "Checking");
							EShaktiApplication.getInstance().getTransactionManager().startTransaction(
									DataType.TRANSACTIONADD,
									new Transaction(0, PrefUtils.getUsernameKey(), SelectedGroupsTask.UserName,
											SelectedGroupsTask.Ngo_Id, SelectedGroupsTask.Group_Id,
											mTransaction_UniqueId, mLastTr_ID, mCurrentTransDate, null, null, null,
											null, null, null, null, null, null, null, null, null, null,
											mPersonalLoanDisburse, null, null, null, null, null, null, null, null));
						} else {

							TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT,
									TastyToast.ERROR);

						}

					} else if (mUniqueId.equals(PrefUtils.getOfflineUniqueID()) && mPersonalLDBValues == null) {
						EShaktiApplication.setSetTransValues(TransactionOffConstants.TRANS_PDB);

						EShaktiApplication.getInstance().getTransactionManager().startTransaction(
								DataType.TRANS_VALUES_UPDATE, new TransactionUpdate(mUniqueId, mPersonalLoanDisburse));

					} else if (mPersonalLDBValues != null) {
						confirmationDialog.dismiss();

						TastyToast.makeText(getActivity(), AppStrings.offlineDataAvailable, TastyToast.LENGTH_SHORT,
								TastyToast.WARNING);

						DatePickerDialog.sDashboardDate = SelectedGroupsTask.sLastTransactionDate_Response;
						MainFragment_Dashboard fragment = new MainFragment_Dashboard();
						setFragment(fragment);
					}
				} else {

					if (confirmationDialog.isShowing() && confirmationDialog != null) {
						confirmationDialog.dismiss();
					}
					Log.v("Checking Values!!!!!!!!!!!!!!!!!!!!!!! Else", "!!!!!!!!@@@@@@@@@@@@@@@s");
					TastyToast.makeText(getActivity(), "Please Try Again", TastyToast.LENGTH_SHORT, TastyToast.ERROR);

					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);

				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		}
	}

	@Subscribe
	public void onAddTransactionPersonalLDB(final TransactionResponse transactionResponse) {
		switch (transactionResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), transactionResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), transactionResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			try {
				TransactionProvider.getSinlgeGroupMaster_Transaction(
						new Transaction_UniqueIdSingle(PrefUtils.getOfflineUniqueID()));

				if (GetTransactionSinglevalues.getPersonalloandisbursement() != null

						&& !GetTransactionSinglevalues.getPersonalloandisbursement().equals("")) {
					StringBuilder builder = new StringBuilder();
					String mMasterresponse, mPLMemberId, mPLDB, result, mPLDBrepayment = null;
					for (int i = 0; i < sPL_Amounts.length; i++) {
						Log.e("Validated Values IS@@@@@@@@", validatedOutstanding[i] + "");
						validatedOutstanding[i] = validatedOutstanding[i] + Integer.parseInt(sPL_Amounts[i]);
						Log.e("Prin Amount is::::", sPL_Amounts[i]);
						Log.e("Validated Values IS::::", validatedOutstanding[i] + "");

						mPLMemberId = String.valueOf(SelectedGroupsTask.member_Id.elementAt(i));

						mPLDB = String.valueOf(validatedOutstanding[i] + ".00");
						mPLDBrepayment = mPldbrepayamount[i];
						Log.v("PLDB Repay Amount", mPLDBrepayment);
						mMasterresponse = mPLMemberId + "~" + mPLDB + "~" + mPLDBrepayment + "~";

						builder.append(mMasterresponse);

					}
					result = builder.toString();
					String sSendToServer_PLDB = "0" + "#" + result;
					Log.e("Send to Server Insert Values $$$$$", sSendToServer_PLDB);
					String mCashinHand = String.valueOf(Integer.parseInt(SelectedGroupsTask.sCashinHand) - sPL_total);

					String mCashatBank = SelectedGroupsTask.sCashatBank;
					String mBankDetails = GetOfflineTransactionValues.getBankDetails();
					SelectedGroupsTask.sCashinHand = mCashinHand;

					Log.e("Trans Date In PLDB", mLastTrDate);
					Log.e("Trans Date In PLDB Dashboard Date", DatePickerDialog.sDashboardDate);
					String mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
					SelectedGroupsTask.sLastTransactionDate_Response = mTrasactiondate;
					String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mTrasactiondate,
							mCashinHand, mCashatBank, mBankDetails);
					String mSelectedGroupId = SelectedGroupsTask.Group_Id;
					EShaktiApplication.setPLOS(true);
					EShaktiApplication.getInstance().getTransactionManager().startTransaction(
							DataType.GROUP_UPDATE_PLDB,
							new GroupMLUpdate(mSelectedGroupId, mGroupMasterResponse, sSendToServer_PLDB));

					Log.e("Saving Insert Sucessfully",
							"Sucess" + SelectedGroupsTask.sCashinHand + "Cash In Hand are equals" + mCashinHand);

				} else {
					if (confirmationDialog.isShowing() && confirmationDialog != null) {
						confirmationDialog.dismiss();
					}

					TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT, TastyToast.ERROR);

					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);

				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		}
	}

	@Subscribe
	public void OnTransUpdatePersonalLos(final TransactionUpdateResponse transactionUpdateResponse) {
		switch (transactionUpdateResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), transactionUpdateResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), transactionUpdateResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:
			try {
				TransactionProvider.getSinlgeGroupMaster_Transaction(
						new Transaction_UniqueIdSingle(PrefUtils.getOfflineUniqueID()));

				if (GetTransactionSinglevalues.getPersonalloandisbursement() != null

						&& !GetTransactionSinglevalues.getPersonalloandisbursement().equals("")) {

					StringBuilder builder = new StringBuilder();
					String mMasterresponse, mPLMemberId, mPLDB, result, mPLDBrepayment = null;
					for (int i = 0; i < sPL_Amounts.length; i++) {

						validatedOutstanding[i] = validatedOutstanding[i] + Integer.parseInt(sPL_Amounts[i]);

						mPLMemberId = String.valueOf(SelectedGroupsTask.member_Id.elementAt(i));

						mPLDB = String.valueOf(validatedOutstanding[i] + ".00");
						mPLDBrepayment = mPldbrepayamount[i];
						Log.v("PLDB Repay Amount", mPLDBrepayment);
						mMasterresponse = mPLMemberId + "~" + mPLDB + "~" + mPLDBrepayment + "~";

						builder.append(mMasterresponse);

					}
					result = builder.toString();
					String sSendToServer_PLDB = "0" + "#" + result;
					Log.e("Send to Server Insert Values $$$$$", sSendToServer_PLDB);
					String mCashinHand = String.valueOf(Integer.parseInt(SelectedGroupsTask.sCashinHand) - sPL_total);

					String mCashatBank = SelectedGroupsTask.sCashatBank;
					String mBankDetails = GetOfflineTransactionValues.getBankDetails();
					SelectedGroupsTask.sCashinHand = mCashinHand;
					String mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
					SelectedGroupsTask.sLastTransactionDate_Response = mTrasactiondate;
					String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mTrasactiondate,
							mCashinHand, mCashatBank, mBankDetails);
					String mSelectedGroupId = SelectedGroupsTask.Group_Id;
					EShaktiApplication.setPLOS(true);
					EShaktiApplication.getInstance().getTransactionManager().startTransaction(
							DataType.GROUP_UPDATE_PLDB,
							new GroupMLUpdate(mSelectedGroupId, mGroupMasterResponse, sSendToServer_PLDB));

					Log.e("Saving Insert Sucessfully",
							"Sucess" + SelectedGroupsTask.sCashinHand + "Cash In Hand are equals" + mCashinHand);

				} else {
					if (confirmationDialog.isShowing() && confirmationDialog != null) {
						confirmationDialog.dismiss();
					}

					TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT, TastyToast.ERROR);

					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);

				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		}
	}

	@Subscribe
	public void OnGroupPLDBUpdate(final GroupPLOSUpdateResponse groupPLOSUpdateResponse) {
		switch (groupPLOSUpdateResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), groupPLOSUpdateResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), groupPLOSUpdateResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			confirmationDialog.dismiss();
			try {
				TastyToast.makeText(getActivity(), AppStrings.transactionCompleted, TastyToast.LENGTH_SHORT,
						TastyToast.SUCCESS);

				System.out.println("-------------OnGroupPLDBUpdate ---------");
				if (EShaktiApplication.isDefault()) {

					for (int i = 0; i < SelectedGroupsTask.loan_Id.size(); i++) {
						if (!SelectedGroupsTask.loan_Id.elementAt(i).equals("0")) {

							try {
								Transaction_GroupLoanRepaidMenuFragment.mloan_Id = SelectedGroupsTask.loan_Id
										.elementAt(i).toString();
								System.out.println("Loan Id" + Transaction_GroupLoanRepaidMenuFragment.mloan_Id);

								Transaction_GroupLoanRepaidMenuFragment.mloan_Name = SelectedGroupsTask.loan_Name
										.elementAt(i).toString();
								System.out.println("Loan Name:" + Transaction_GroupLoanRepaidMenuFragment.mloan_Name);

								String temp_loanName = SelectedGroupsTask.loan_Name.elementAt(i).toString();

								Transaction_GroupLoanRepaidMenuFragment.mSendTo_ServerLoan_Id = SelectedGroupsTask.loan_Id
										.elementAt(i).toString();

								EShaktiApplication
										.setLoanId(String.valueOf(Transaction_GroupLoanRepaidMenuFragment.mloan_Id));

								EShaktiApplication.setGroupLoanRepaymentLoanBankName(
										SelectedGroupsTask.loanAcc_bankName.elementAt(i));
								EShaktiApplication
										.setGroupLoanRepaymentAccNo(SelectedGroupsTask.loanAcc_loanAccNo.elementAt(i));

								if (Transaction_GroupLoanRepaidMenuFragment.mloan_Name.equals(temp_loanName)) {

									if (ConnectionUtils.isNetworkAvailable(getActivity())) {

										if (PrefUtils.getGroupMasterResValues() != null
												&& PrefUtils.getGroupMasterResValues().equals("1")) {

											EShaktiApplication.getInstance().getTransactionManager().startTransaction(
													DataType.GET_GROUPMASTER_GLOS,
													new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));

										} else {
											isGroupLoanOutstanding = true;
											new Get_LoanOutstandingTask(
													Transaction_InternalLoan_DisbursementFragment.this).execute();
										}
									} else {
										// DO OFFLINE STUFFS

										if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
											EShaktiApplication.getInstance().getTransactionManager().startTransaction(
													DataType.GET_GROUPMASTER_GLOS,
													new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));
										} else {
											TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF,
													TastyToast.LENGTH_SHORT, TastyToast.ERROR);
										}

									}

								}

								i = SelectedGroupsTask.loan_Id.size();
								Log.e("GroupLoan Before inc groupLoanCount size &&&&",
										Transaction_InternalLoan_DisbursementFragment.groupLoanCount + "");
								Transaction_InternalLoan_DisbursementFragment.groupLoanCount = Transaction_InternalLoan_DisbursementFragment.groupLoanCount
										+ 1;
								Log.e("GroupLoan After inc groupLoanCount size &&&&",
										Transaction_InternalLoan_DisbursementFragment.groupLoanCount + "");

							} catch (Exception e) {
								e.printStackTrace();
							}
						} else {
							if (ConnectionUtils.isNetworkAvailable(getActivity())) {
								System.out.println("----------- isMeetingValues true----");
								isMeetingValues = true;
								new Get_Group_Minutes_NewTask(this).execute();

							} else {
								if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
									EShaktiApplication.getInstance().getTransactionManager().startTransaction(
											DataType.DEFAULT_MINUTESOFMEETINGS,
											new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));
								} else {

									TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF,
											TastyToast.LENGTH_SHORT, TastyToast.ERROR);
								}
							}
						}
					}

				} else {

					if (!ConnectionUtils.isNetworkAvailable(getActivity())) {

						String mValues = Put_DB_GroupNameDetail_Response
								.put_DB_GroupNameDetails_Response(EShaktiApplication.getGroupResponse());

						GroupProvider.updateGroupResponse(
								new GroupResponseUpdate(EShaktiApplication.getGroupId_GroupLastTransDate(), mValues));
					}

					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		}
	}

	@Subscribe
	public void OnGroupLoanOSResponse(final GroupLoanOSResponse groupLoanOSResponse) {
		switch (groupLoanOSResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), groupLoanOSResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), groupLoanOSResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:
			String mGroupLoanOSResponse = GetGroupMemberDetails.getGrouploanOS();

			Transaction_GroupLoanRepaidMenuFragment.response = mGroupLoanOSResponse.split("%");
			if (mGroupLoanOSResponse != null && !mGroupLoanOSResponse.equals("")) {
				String[] response_Grouploanrepaid = mGroupLoanOSResponse.split("%");
				for (int i = 0; i < response_Grouploanrepaid.length; i++) {

					String secondResponse[] = response_Grouploanrepaid[i].split("#");

					if (Transaction_GroupLoanRepaidMenuFragment.mSendTo_ServerLoan_Id.equals(secondResponse[0])) {

						mGroupOS_Offlineresponse = secondResponse[1];
						Get_LoanOutstandingTask.sGet_Loan_outstanding_ServiceResponse = mGroupOS_Offlineresponse;

					}

				}

				Transaction_GroupLoanRepaidFragment fragment = new Transaction_GroupLoanRepaidFragment();
				setFragment(fragment);

			} else {
				TastyToast.makeText(getActivity(), AppStrings.loginAgainAlert, TastyToast.LENGTH_LONG,
						TastyToast.WARNING);
			}
			break;
		}

	}

	private void OnCallInternalloanValues() {

		try {

			mPOLText_Values = new String[SelectedGroupsTask.member_Id.size()];
			mRepayment = "";

			sSendToServer_PLdisburse = Reset.reset(sSendToServer_PLdisburse);
			sPL_total = 0;
			sPL_Fields.clear();
			sTenure_Fields.clear();
			sPOL_Fields.clear();
			mTotalDisbursement = 0;

			sSelected_POL = new String[SelectedGroupsTask.member_Id.size()];
			mRadio_Pol_id = new String[SelectedGroupsTask.member_Id.size()];

			/** PL Outstanding **/
			if (Boolean.valueOf(ConnectionUtils.isNetworkAvailable(getActivity()))) {
				if (PrefUtils.getGroupMasterResValues() != null && PrefUtils.getGroupMasterResValues().equals("1")) {
					response = MainFragment_Dashboard.mPLDBResponse.split("~");
				} else {
					response = Get_All_Mem_OutstandingTask.sGet_All_Mem_OutstandingTask_Response.split("~");
				}

			} else {
				response = MainFragment_Dashboard.mPLDBResponse.split("~");
				Log.v("Personal Loan OutStanding@@@@@", response + "");

			}

			System.out.println("PL DISBURSE OUTS : " + response);

			mOutstanding = new String[SelectedGroupsTask.member_Name.size()];
			mSize = SelectedGroupsTask.member_Name.size();
			validatedOutstanding = new int[mSize];

			int count = 0;
			for (int i = 0; i < response.length; i++) {

				if (count == 0) {
					count = 1;
				} else if (count == 1) {
					memOuts = memOuts + response[i] + "~";
					count = 2;
				} else if (count == 2) {
					if (response[i].equals("")) {
						response[i] = nullAmount;
					}

					mRepayment = mRepayment + response[i] + "~";
					count = 0;
				}
			}

			/** Purpose of Loan **/
			if (Boolean.valueOf(ConnectionUtils.isNetworkAvailable(getActivity()))) {

				if (PrefUtils.getGroupMasterResValues() != null && PrefUtils.getGroupMasterResValues().equals("1")) {
					mPOL_Response = MainFragment_Dashboard.mPLDBPurposeofloan.split("~");
				} else {

					Log.i("POL RESPONSEllllllllllllllllllll ", Get_PurposeOfLoanTask.sGet_PurposeOfLoan_Response);

					mPOL_Response = Get_PurposeOfLoanTask.sGet_PurposeOfLoan_Response.split("~");
				}
			} else {
				mPOL_Response = MainFragment_Dashboard.mPLDBPurposeofloan.split("~");
			}

			Log.d(TAG, " PURPOSE OF LOAN " + mPOL_Response);

			mOutstanding = memOuts.split("~");
			mPldbrepayamount = mRepayment.split("~");
			Log.e("Size values Is ", mSize + "");

			for (int i = 0; i < mSize; i++) {

				mOutstanding = memOuts.split("~");
				Log.v("I Values Is ", i + "");
				// TypeCasting the double values into integer
				validatedOutstanding[i] = Integer.parseInt(mOutstanding[i].substring(0, mOutstanding[i].indexOf(".")));
				System.out.println("mOutstanding " + validatedOutstanding[i] + " POS " + i);

			}

			String loadPOL = "";
			String mPOLID = "";

			for (int i = 0; i < mPOL_Response.length; i++) {

				if (i % 2 != 0) {
					loadPOL = loadPOL + mPOL_Response[i] + ",";
					mPOL_Text = mPOL_Text + mPOL_Response[i] + ",";
				} else {
					mPOLID = mPOLID + mPOL_Response[i] + ",";
				}
			}

			Log.d("LOAD POL --------------------", String.valueOf(loadPOL));
			/*
			 * mPOL_Values = loadPOL.split(","); mPOL_ID = mPOLID.split(",");
			 */
			String[] mTemp_POL_Values = loadPOL.split(",");
			String[] mTemp_POL_Id = mPOLID.split(",");
			mPOL_ID = new String[mTemp_POL_Id.length + 1];
			mPOL_ID[0] = "0";

			mPOL_Values = new String[mTemp_POL_Values.length + 1];
			mPOL_Values[0] = String.valueOf(AppStrings.mNone);

			for (int i = 0; i < mTemp_POL_Values.length; i++) {
				mPOL_Values[i + 1] = mTemp_POL_Values[i];
				mPOL_ID[i + 1] = mTemp_POL_Id[i];
			}
			// end
			mPOLText_Values = mPOL_Text.split(",");

			// For testing
			for (int i = 0; i < mPOL_Values.length; i++) {
				System.out.println("---------mPOL_Values--------" + mPOL_Values[i] + "    i   :" + i);
				System.out.println("---------mPOL_ID--------" + mPOL_ID[i] + "    i   :" + i);
			}

			sRowItems = new ArrayList<RowItem>();

			mPOL_Size = mPOL_Values.length;
			Log.e("LENGTH of POL ", String.valueOf(mPOL_Size));

			for (int i = 0; i < mPOL_Values.length; i++) {
				// RowItem item = new RowItem(mPOL_Values[i]);
				RowItem item = new RowItem(mPOL_Values[i]);

				sRowItems.add(item);
			}
			custAdapter = new CustomAdapter(getActivity(), sRowItems);

			loanType = AppStrings.chooseLabel;

			System.out.println(" Loan type =  " + loanType);

			if (EShaktiApplication.isDefault()) {

				mSavings_Values = EShaktiApplication.getStepwiseSavings();
				InternalLoan = EShaktiApplication.getStepwiseInternalloan();
				mTotalCollection = mSavings_Values + InternalLoan;

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Subscribe
	public void OnGroupMasterMinutesofmeetingResponse(
			final DefaultOffline_MinutesResponse getMinutesofMeetingResponse) {
		switch (getMinutesofMeetingResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), getMinutesofMeetingResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), getMinutesofMeetingResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			Log.v("Personal Loan outStandings FROM DB ", GetGroupMemberDetails.getMasterminutes() + "            "
					+ GetGroupMemberDetails.getIndividualgroupminutes());
			String mMasterMinutes = GetGroupMemberDetails.getMasterminutes();
			String mIndividualMinutes = GetGroupMemberDetails.getIndividualgroupminutes();
			String mMasterMinut[] = mMasterMinutes.split("~");
			String mIndi[] = mIndividualMinutes.split("~");
			int mSize = 0;

			mSize = mMasterMinut.length / 2;
			Log.i("Group Length", mSize + "");
			String mMasterValues[] = null, mMasterId[] = null;
			StringBuilder builder = new StringBuilder();
			String mMasterresponse, result;
			mMasterValues = new String[mSize];
			mMasterId = new String[mSize];
			int j = 0, k = 0;

			for (int i = 0; i < mMasterMinut.length; i++) {

				if (i % 2 == 0) {
					mMasterId[j] = mMasterMinut[i];

					j++;
				} else {

					mMasterValues[k] = mMasterMinut[i];
					k++;

				}
			}

			for (int i = 0; i < mIndi.length; i++) {
				String mIndiValues = mIndi[i];
				Log.e("mIndivalues", mIndiValues);
				Log.e("Master ID Length Values", mMasterId.length + "");

				for (int m = 0; m < mMasterId.length; m++) {

					if (mIndiValues.equals(mMasterId[m])) {

						mMasterresponse = mIndiValues + "~" + mMasterValues[m] + "~";

						builder.append(mMasterresponse);
					}
				}

			}
			result = builder.toString();
			System.out.println("mMasterValues " + result);

			Get_Group_Minutes_NewTask.sGet_Group_Minutes__new_Response = result;

			Meeting_MinutesOfMeetingFragment meetingFragment = new Meeting_MinutesOfMeetingFragment();
			setFragment(meetingFragment);

			break;
		}

	}

	@Subscribe
	public void OnGroupMasterPLOSResponse(final DefaultOffline_PLResponse groupPersonalloanOSResponse) {
		switch (groupPersonalloanOSResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), groupPersonalloanOSResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), groupPersonalloanOSResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			String mMemberLoanResponse = GetGroupMemberDetails.getPersonaloanOS();
			response = mMemberLoanResponse.split("#");

			for (int i = 0; i < response.length; i++) {
				System.out.println("Response : " + response[i]);

				System.out.println("LOAN ID : " + response[0]);
				System.out.println("LOAN ID : " + response[1]);
				Transaction_MemLoanRepaid_MenuFragment.mPersonalOS_offlineresponse = response[1];
			}

			if (isPrevious) {
				isPrevious = false;
				Transaction_PersonalLoanRepaidFragment savingsFragment = new Transaction_PersonalLoanRepaidFragment();
				setFragment(savingsFragment);
			}
			break;
		}

	}

	@Subscribe
	public void OnGroupMasterResponse(final GroupMemberloanOSResponse groupMasterResponse) {
		switch (groupMasterResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), groupMasterResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), groupMasterResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			String mMemberLoanResponse = GetGroupMemberDetails.getMemberloanOS();

			Log.e("OnGroupMasterResponse ===  ", mMemberLoanResponse.toString());
			if (mMemberLoanResponse != null && !mMemberLoanResponse.equals("")) {
				response = mMemberLoanResponse.split("%");

				for (int i = 0; i < response.length; i++) {
					System.out.println("Response  ****: " + response[i]);

					String secondResponse[] = response[i].split("#");
					System.out.println("LOAN ID : " + secondResponse[0]);
					if (Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID.equals(secondResponse[0])) {
						System.out.println("LOAN OUTS : " + secondResponse[1]);
						Transaction_MemLoanRepaid_MenuFragment.mMemberOS_Offlineresponse = secondResponse[1];
					}

				}

				if (isPrevious) {
					isPrevious = false;
					Transaction_MemLoanRepaidFragment fragment = new Transaction_MemLoanRepaidFragment();
					setFragment(fragment);
				}

			} else {
				TastyToast.makeText(getActivity(), AppStrings.loginAgainAlert, TastyToast.LENGTH_LONG,
						TastyToast.WARNING);
			}

			break;
		}

	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		SBus.INST.unRegister(this);
	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub
		Log.e("Current Class Name!!!!!!!!!!!!!!", fragment.getClass().getName());
		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

		FragmentDrawer.drawer_CashinHand.setText(
				RegionalConversion.getRegionalConversion(AppStrings.cashinhand) + SelectedGroupsTask.sCashinHand);
		FragmentDrawer.drawer_CashinHand.setTypeface(LoginActivity.sTypeface);

		FragmentDrawer.drawer_CashatBank.setText(
				RegionalConversion.getRegionalConversion(AppStrings.cashatBank) + SelectedGroupsTask.sCashatBank);
		FragmentDrawer.drawer_CashatBank.setTypeface(LoginActivity.sTypeface);

		FragmentDrawer.drawer_lastTR_Date
				.setText(RegionalConversion.getRegionalConversion(AppStrings.lastTransactionDate) + " : "
						+ SelectedGroupsTask.sLastTransactionDate_Response);
		FragmentDrawer.drawer_lastTR_Date.setTypeface(LoginActivity.sTypeface);

		UpdateCalendarMinMaxDateUtils.OnUpdateCalendarDate();

		Calendar calender = Calendar.getInstance();

		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String formattedDate = df.format(calender.getTime());
		EShaktiApplication.setLastTransDate_GroupId(SelectedGroupsTask.Group_Id);

		new GroupTempDBLastTransDate(EShaktiApplication.getLastTransDate_GroupId(),
				SelectedGroupsTask.sLastTransactionDate_Response, formattedDate);

		GroupProvider.updateGroupLastTransDateResponse();
	}

}
