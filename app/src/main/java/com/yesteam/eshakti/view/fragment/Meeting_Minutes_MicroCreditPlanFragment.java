package com.yesteam.eshakti.view.fragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.adapter.CustomAdapter;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.model.RowItem;
import com.yesteam.eshakti.sqlite.database.GroupProvider;
import com.yesteam.eshakti.sqlite.db.model.GroupTempDBLastTransDate;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.utils.Get_EdiText_Filter;
import com.yesteam.eshakti.utils.Reset;
import com.yesteam.eshakti.utils.TextviewUtils;
import com.yesteam.eshakti.utils.UpdateCalendarMinMaxDateUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.views.ButtonFlat;
import com.yesteam.eshakti.views.CustomHorizontalScrollView;
import com.yesteam.eshakti.views.CustomHorizontalScrollView.onScrollChangedListener;
import com.yesteam.eshakti.webservices.Get_MicroCreditPlanWebservice;
import com.yesteam.eshakti.webservices.Get_PurposeOfLoanTask;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Dialog;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Meeting_Minutes_MicroCreditPlanFragment extends Fragment implements OnClickListener, TaskListener {

	public static final String TAG = Meeting_Minutes_MicroCreditPlanFragment.class.getSimpleName();
	public static List<EditText> sPL_Fields = new ArrayList<EditText>();
	public static List<TextView> sPOL_Fields = new ArrayList<TextView>();
	public static String[] sPOLvalues;
	public static String[] sPL_Amounts, sIncomeA_Amounts, sExpense_Amounts, sBeforeLoanPay_Amounts, sLoans_Amounts,
			sSurplus_Amounts;
	public static String sSelected_POL[], sSelected_POL_Id[];
	public static String sSendToServer_EditInternalLoan;
	public static int sPL_total, sIncome_total, sExpense_total, sBeforeLoanPay_total, sLoans_total, sSurplus_total;

	private Dialog mProgressDilaog;

	private TextView mGroupName, mCashinHand, mCashatBank, mHeader, mLoanType;

	private Button mSubmit_Raised_Button, mEdit_RaisedButton, mOk_RaisedButton;
	private EditText mPL_values, mIncome_values, mExpense_values, mBeforeLoanPay_values, mLoans_values, mSurplus_values;
	int mSize;
	String response[], toBeEdit_PLdisburse[], mPOL_Response[], mPOL_Values[], mPOL_ID[];

	private TableLayout mLeftHeaderTable, mRightHeaderTable, mLeftContentTable, mRightContentTable;
	private CustomHorizontalScrollView mHSRightHeader, mHSRightContent;

	String width[] = { AppStrings.amount, AppStrings.purposeOfLoan, AppStrings.income, AppStrings.mExpense,
			AppStrings.mBeforeLoanPay, AppStrings.mLoans, AppStrings.mSurplus };
	int[] rightHeaderWidth = new int[width.length];
	int[] rightContentWidth = new int[width.length];

	String mSelectedPOL_Values[], mPOLText_Values[];
	String mSelected_POL_Values = "", mPOL_Text = "";
	public static List<RowItem> sRowItems;
	int selectedId = 100, mPOL_Size;
	CustomAdapter custAdapter;
	String loanType;
	RadioButton radioButton;
	String mSelectedLoanType;
	String nullAmount = "0";
	boolean isError, isnullAmountError;
	Dialog confirmationDialog;
	boolean iSPurposeLoanTask = false;

	public static List<EditText> sIncome_Fields = new ArrayList<EditText>();
	public static List<EditText> sExpense_Fields = new ArrayList<EditText>();
	public static List<EditText> sBefoeLoanPay_Fields = new ArrayList<EditText>();
	public static List<EditText> sLoans_Fields = new ArrayList<EditText>();
	public static List<EditText> sSurplus_Fields = new ArrayList<EditText>();

	TableRow.LayoutParams incomeHeaderParams, expenseHeaderParams, blpHeaderParams, loansHeaderParams,
			surplusHeaderParams;
	int leftHeaderColumnWidth[], leftContentColumnWidth[], rightHeaderColumnWidth[], rightContentColumnWidth[];
	LinearLayout mMemberNameLayout;
	TextView mMemberName;

	public Meeting_Minutes_MicroCreditPlanFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_all_transaction, container, false);

		MainFragment_Dashboard.isBackpressed = false;

		try {
			OnCallInternalloanValues();

			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashinHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashinHand.setTypeface(LoginActivity.sTypeface);

			mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashatBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashatBank.setTypeface(LoginActivity.sTypeface);

			mMemberNameLayout = (LinearLayout) rootView.findViewById(R.id.member_name_layout);
			mMemberName = (TextView) rootView.findViewById(R.id.member_name);
			
			mHeader = (TextView) rootView.findViewById(R.id.fragmentHeader);

			mHeader.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.mMicro_Credit_Plan)));

			mHeader.setTypeface(LoginActivity.sTypeface);

			mSize = SelectedGroupsTask.member_Name.size();
			Log.d(TAG, String.valueOf(mSize));

			mLeftHeaderTable = (TableLayout) rootView.findViewById(R.id.LeftHeaderTable);
			mRightHeaderTable = (TableLayout) rootView.findViewById(R.id.RightHeaderTable);
			mLeftContentTable = (TableLayout) rootView.findViewById(R.id.LeftContentTable);
			mRightContentTable = (TableLayout) rootView.findViewById(R.id.RightContentTable);

			mHSRightHeader = (CustomHorizontalScrollView) rootView.findViewById(R.id.rightHeaderHScrollView);
			mHSRightContent = (CustomHorizontalScrollView) rootView.findViewById(R.id.rightContentHScrollView);

			mHSRightHeader.setOnScrollChangedListener(new onScrollChangedListener() {

				public void onScrollChanged(int l, int t, int oldl, int oldt) {
					// TODO Auto-generated method stub

					mHSRightContent.scrollTo(l, 0);

				}
			});

			mHSRightContent.setOnScrollChangedListener(new onScrollChangedListener() {
				@Override
				public void onScrollChanged(int l, int t, int oldl, int oldt) {
					// TODO Auto-generated method stub
					mHSRightHeader.scrollTo(l, 0);
				}
			});
			TableRow leftHeaderRow = new TableRow(getActivity());

			TableRow.LayoutParams lHeaderParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);

			TextView mMemberName_Header = new TextView(getActivity());
			mMemberName_Header.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.memberName)));
			mMemberName_Header.setTypeface(LoginActivity.sTypeface);
			mMemberName_Header.setTextColor(Color.WHITE);
			mMemberName_Header.setPadding(10, 5, 10, 5);
			mMemberName_Header.setLayoutParams(lHeaderParams);
			leftHeaderRow.addView(mMemberName_Header);

			mLeftHeaderTable.addView(leftHeaderRow);

			TableRow rightHeaderRow = new TableRow(getActivity());
			TableRow.LayoutParams rHeaderParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);
			rHeaderParams.setMargins(10, 0, 10, 0);

			TextView mPL_Header = new TextView(getActivity());
			mPL_Header.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.amount)));
			mPL_Header.setTypeface(LoginActivity.sTypeface);
			mPL_Header.setTextColor(Color.WHITE);
			mPL_Header.setPadding(10, 5, 10, 5);
			mPL_Header.setGravity(Gravity.CENTER);
			mPL_Header.setLayoutParams(rHeaderParams);
			mPL_Header.setBackgroundResource(R.color.tableHeader);
			rightHeaderRow.addView(mPL_Header);

			TableRow.LayoutParams POLParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);
			POLParams.setMargins(10, 0, 10, 0);

			TextView mPurposeOfLoan_Header = new TextView(getActivity());
			mPurposeOfLoan_Header
					.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.purposeOfLoan)));
			mPurposeOfLoan_Header.setTypeface(LoginActivity.sTypeface);
			mPurposeOfLoan_Header.setTextColor(Color.WHITE);
			mPurposeOfLoan_Header.setGravity(Gravity.RIGHT);
			mPurposeOfLoan_Header.setLayoutParams(rHeaderParams);
			mPurposeOfLoan_Header.setPadding(10, 5, 10, 5);
			mPurposeOfLoan_Header.setBackgroundResource(R.color.tableHeader);
			rightHeaderRow.addView(mPurposeOfLoan_Header);

			incomeHeaderParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1f);
			rHeaderParams.setMargins(10, 0, 10, 0);

			TextView mIncome_Header = new TextView(getActivity());
			mIncome_Header.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.income)));
			mIncome_Header.setTypeface(LoginActivity.sTypeface);
			mIncome_Header.setTextColor(Color.WHITE);
			mIncome_Header.setPadding(10, 5, 10, 5);
			mIncome_Header.setGravity(Gravity.RIGHT);
			mIncome_Header.setLayoutParams(rHeaderParams);
			mIncome_Header.setBackgroundResource(R.color.tableHeader);
			rightHeaderRow.addView(mIncome_Header);

			expenseHeaderParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1f);
			rHeaderParams.setMargins(10, 0, 10, 0);

			TextView mExpense_Header = new TextView(getActivity());
			mExpense_Header.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.mExpense)));
			mExpense_Header.setTypeface(LoginActivity.sTypeface);
			mExpense_Header.setTextColor(Color.WHITE);
			mExpense_Header.setPadding(10, 5, 10, 5);
			mExpense_Header.setGravity(Gravity.RIGHT);
			mExpense_Header.setLayoutParams(rHeaderParams);
			mExpense_Header.setBackgroundResource(R.color.tableHeader);
			rightHeaderRow.addView(mExpense_Header);

			blpHeaderParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1f);
			rHeaderParams.setMargins(10, 0, 10, 0);

			TextView mBeforeLoanPay_Header = new TextView(getActivity());
			mBeforeLoanPay_Header
					.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.mBeforeLoanPay)));
			mBeforeLoanPay_Header.setTypeface(LoginActivity.sTypeface);
			mBeforeLoanPay_Header.setTextColor(Color.WHITE);
			mBeforeLoanPay_Header.setPadding(10, 5, 10, 5);
			mBeforeLoanPay_Header.setSingleLine(true);
			mBeforeLoanPay_Header.setGravity(Gravity.RIGHT);
			mBeforeLoanPay_Header.setLayoutParams(rHeaderParams);
			mBeforeLoanPay_Header.setBackgroundResource(R.color.tableHeader);
			rightHeaderRow.addView(mBeforeLoanPay_Header);

			loansHeaderParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1f);
			rHeaderParams.setMargins(10, 0, 10, 0);

			TextView mLoans_Header = new TextView(getActivity());
			mLoans_Header.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.mLoans)));
			mLoans_Header.setTypeface(LoginActivity.sTypeface);
			mLoans_Header.setTextColor(Color.WHITE);
			mLoans_Header.setPadding(10, 5, 10, 5);
			mLoans_Header.setGravity(Gravity.CENTER);
			mLoans_Header.setLayoutParams(rHeaderParams);
			mLoans_Header.setBackgroundResource(R.color.tableHeader);
			rightHeaderRow.addView(mLoans_Header);

			surplusHeaderParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1f);
			rHeaderParams.setMargins(10, 0, 10, 0);

			TextView mSurplus_Header = new TextView(getActivity());
			mSurplus_Header.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.mSurplus)));
			mSurplus_Header.setTypeface(LoginActivity.sTypeface);
			mSurplus_Header.setTextColor(Color.WHITE);
			mSurplus_Header.setPadding(10, 5, 10, 5);
			mSurplus_Header.setGravity(Gravity.RIGHT);
			mSurplus_Header.setLayoutParams(rHeaderParams);
			mSurplus_Header.setBackgroundResource(R.color.tableHeader);
			rightHeaderRow.addView(mSurplus_Header);

			mRightHeaderTable.addView(rightHeaderRow);

			try {

				for (int j = 0; j < mSize; j++) {

					TableRow leftContentRow = new TableRow(getActivity());

					TableRow.LayoutParams leftContentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
							LayoutParams.WRAP_CONTENT, 1f);
					leftContentParams.setMargins(5, 5, 5, 15);

					final TextView memberName_Text = new TextView(getActivity());
					memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
							String.valueOf(SelectedGroupsTask.member_Name.elementAt(j))));
					memberName_Text.setTypeface(LoginActivity.sTypeface);
					memberName_Text.setTextColor(color.black);
					memberName_Text.setPadding(5, 5, 5, 5);
					memberName_Text.setLayoutParams(leftContentParams);
					memberName_Text.setWidth(200);
					memberName_Text.setSingleLine(true);
					memberName_Text.setEllipsize(TextUtils.TruncateAt.END);
					leftContentRow.addView(memberName_Text);

					mLeftContentTable.addView(leftContentRow);

					TableRow rightContentRow = new TableRow(getActivity());

					TableRow.LayoutParams rightContentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
							LayoutParams.WRAP_CONTENT, 1f);
					rightContentParams.setMargins(10, 5, 10, 5);

					mPL_values = new EditText(getActivity());
					mPL_values.setId(j);
					sPL_Fields.add(mPL_values);
					mPL_values.setInputType(InputType.TYPE_CLASS_NUMBER);
					mPL_values.setPadding(5, 5, 5, 5);
					mPL_values.setFilters(Get_EdiText_Filter.editText_filter());
					mPL_values.setBackgroundResource(R.drawable.edittext_background);
					mPL_values.setLayoutParams(rightContentParams);
					mPL_values.setWidth(150);
					mPL_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

						@Override
						public void onFocusChange(View v, boolean hasFocus) {
							// TODO Auto-generated method stub
							if (hasFocus) {
								((EditText) v).setGravity(Gravity.LEFT);
								mMemberNameLayout.setVisibility(View.VISIBLE);
								mMemberName.setText(memberName_Text.getText().toString().trim());
								mMemberName.setTypeface(LoginActivity.sTypeface);
								TextviewUtils.manageBlinkEffect(mMemberName, getActivity());
							} else {
								((EditText) v).setGravity(Gravity.RIGHT);
								mMemberNameLayout.setVisibility(View.GONE);
								mMemberName.setText("");
							}
						}
					});
					rightContentRow.addView(mPL_values);

					TableRow.LayoutParams contentRowParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
							LayoutParams.WRAP_CONTENT, 1f);
					contentRowParams.setMargins(10, 5, 10, 5);

					mLoanType = new TextView(getActivity());
					Log.e("Loan Typeeeeeeeeeeeeeeeeeee", loanType);
					mLoanType.setText(loanType);

					mLoanType.setTypeface(LoginActivity.sTypeface);
					mLoanType.setId(j);
					mLoanType.setLayoutParams(rightContentParams);// (rightContentParams);
					mLoanType.setPadding(10, 0, 10, 5);
					sPOL_Fields.add(mLoanType);
					mLoanType.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(final View v) {
							// TODO Auto-generated method stub

							try {
								mMemberNameLayout.setVisibility(View.VISIBLE);
								mMemberName.setText(memberName_Text.getText().toString().trim());
								mMemberName.setTypeface(LoginActivity.sTypeface);
								TextviewUtils.manageBlinkEffect(mMemberName, getActivity());
								final Dialog ChooseLoanType = new Dialog(getActivity());

								LayoutInflater li = (LayoutInflater) getActivity()
										.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
								final View dialogView = li.inflate(R.layout.dialog_loantype, null, false);

								TextView confirmationHeader = (TextView) dialogView
										.findViewById(R.id.dialog_ChooseLabel);
								confirmationHeader
										.setText(RegionalConversion.getRegionalConversion(AppStrings.chooseLabel));
								confirmationHeader.setTypeface(LoginActivity.sTypeface);

								int radioColor = getResources().getColor(R.color.pink);
								final RadioGroup radioGroup = (RadioGroup) dialogView
										.findViewById(R.id.dialog_RadioGroup);
								radioGroup.removeAllViews();

								for (int j = 0; j < mPOL_Values.length; j++) {

									radioButton = new RadioButton(getActivity());
									radioButton.setText(RegionalConversion.getRegionalConversion(mPOL_Values[j]));
									radioButton.setId(j);
									radioButton.setTypeface(LoginActivity.sTypeface);
									int currentapiVersion = android.os.Build.VERSION.SDK_INT;
									if (currentapiVersion >= android.os.Build.VERSION_CODES.LOLLIPOP) {

										radioButton.setButtonTintList(ColorStateList.valueOf(radioColor));
									}
									radioGroup.addView(radioButton);

								}

								ButtonFlat okButton = (ButtonFlat) dialogView.findViewById(R.id.dialog_yes_button);
								okButton.setText(RegionalConversion.getRegionalConversion(AppStrings.dialogOk));
								okButton.setTypeface(LoginActivity.sTypeface);
								okButton.setOnClickListener(new OnClickListener() {

									@Override
									public void onClick(View view) {
										// TODO Auto-generated method
										// stub

										System.out.println(">>>>>>>>>>>>  : " + radioButton.getId());

										selectedId = radioGroup.getCheckedRadioButtonId();
										Log.e(TAG, String.valueOf(selectedId));

										if ((selectedId != 100) && (selectedId != (-1))) {

											// find the radiobutton by
											// returned id
											RadioButton radioLoanButton = (RadioButton) dialogView
													.findViewById(selectedId);

											mSelectedLoanType = radioLoanButton.getText().toString();
											Log.v("On Selected LOAN TYPE", mSelectedLoanType);

											Log.v("view ID check : ", String.valueOf(v.getId()));

											TextView selectedTextView = (TextView) v.findViewById(v.getId());
											selectedTextView.setText(mSelectedLoanType);
											selectedTextView.setTypeface(LoginActivity.sTypeface);

											int id = radioButton.getId();
											Log.d(TAG, String.valueOf(id));

											// for (int i = 0; i <
											// sSelected_POL.length; i++) {
											for (int i = 0; i < sSelected_POL.length; i++) {

												if (i == v.getId()) {
													sSelected_POL[i] = String.valueOf(mPOL_ID[selectedId]);
													Log.e("000000000000", String.valueOf(mPOL_ID[selectedId])
															+ " POL_ID POSITION >>>> " + selectedId);
												} else if (sSelected_POL[i] != null) {
													sSelected_POL[i] = sSelected_POL[i];
												} else {
													sSelected_POL[i] = "0";
												}

											}

											ChooseLoanType.dismiss();

										} else {
											TastyToast.makeText(getActivity(), AppStrings.choosePOLAlert,
													TastyToast.LENGTH_SHORT, TastyToast.ERROR);

										}

									}

								});

								ChooseLoanType.getWindow()
										.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
								ChooseLoanType.requestWindowFeature(Window.FEATURE_NO_TITLE);
								ChooseLoanType.setCanceledOnTouchOutside(false);
								ChooseLoanType.setContentView(dialogView);
								ChooseLoanType.setCancelable(true);
								ChooseLoanType.show();

							} catch (Exception e) {
								e.printStackTrace();
							}

						}
					});
					rightContentRow.addView(mLoanType);

					mIncome_values = new EditText(getActivity());
					mIncome_values.setId(j);
					sIncome_Fields.add(mIncome_values);
					mIncome_values.setInputType(InputType.TYPE_CLASS_NUMBER);
					mIncome_values.setPadding(5, 5, 5, 5);
					mIncome_values.setFilters(Get_EdiText_Filter.editText_filter());
					mIncome_values.setBackgroundResource(R.drawable.edittext_background);
					mIncome_values.setLayoutParams(rightContentParams);
					mIncome_values.setWidth(150);
					mIncome_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

						@Override
						public void onFocusChange(View v, boolean hasFocus) {
							// TODO Auto-generated method stub
							if (hasFocus) {
								((EditText) v).setGravity(Gravity.LEFT);
								mMemberNameLayout.setVisibility(View.VISIBLE);
								mMemberName.setText(memberName_Text.getText().toString().trim());
								mMemberName.setTypeface(LoginActivity.sTypeface);
								TextviewUtils.manageBlinkEffect(mMemberName, getActivity());
							} else {
								((EditText) v).setGravity(Gravity.RIGHT);
								mMemberNameLayout.setVisibility(View.GONE);
								mMemberName.setText("");
							}
						}
					});
					rightContentRow.addView(mIncome_values);

					mExpense_values = new EditText(getActivity());
					mExpense_values.setId(j);
					sExpense_Fields.add(mExpense_values);
					mExpense_values.setInputType(InputType.TYPE_CLASS_NUMBER);
					mExpense_values.setPadding(5, 5, 5, 5);
					mExpense_values.setFilters(Get_EdiText_Filter.editText_filter());
					mExpense_values.setBackgroundResource(R.drawable.edittext_background);
					mExpense_values.setLayoutParams(rightContentParams);
					mExpense_values.setWidth(150);
					mExpense_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

						@Override
						public void onFocusChange(View v, boolean hasFocus) {
							// TODO Auto-generated method stub
							if (hasFocus) {
								((EditText) v).setGravity(Gravity.LEFT);
								mMemberNameLayout.setVisibility(View.VISIBLE);
								mMemberName.setText(memberName_Text.getText().toString().trim());
								mMemberName.setTypeface(LoginActivity.sTypeface);
								TextviewUtils.manageBlinkEffect(mMemberName, getActivity());
							} else {
								((EditText) v).setGravity(Gravity.RIGHT);
								mMemberNameLayout.setVisibility(View.GONE);
								mMemberName.setText("");
							}
						}
					});

					rightContentRow.addView(mExpense_values);

					mBeforeLoanPay_values = new EditText(getActivity());
					mBeforeLoanPay_values.setId(j);
					sBefoeLoanPay_Fields.add(mBeforeLoanPay_values);
					mBeforeLoanPay_values.setInputType(InputType.TYPE_CLASS_NUMBER);
					mBeforeLoanPay_values.setPadding(5, 5, 5, 5);
					mBeforeLoanPay_values.setFilters(Get_EdiText_Filter.editText_filter());
					mBeforeLoanPay_values.setBackgroundResource(R.drawable.edittext_background);
					mBeforeLoanPay_values.setLayoutParams(rightContentParams);
					mBeforeLoanPay_values.setWidth(150);
					mBeforeLoanPay_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

						@Override
						public void onFocusChange(View v, boolean hasFocus) {
							// TODO Auto-generated method stub
							if (hasFocus) {
								((EditText) v).setGravity(Gravity.LEFT);
								mMemberNameLayout.setVisibility(View.VISIBLE);
								mMemberName.setText(memberName_Text.getText().toString().trim());
								mMemberName.setTypeface(LoginActivity.sTypeface);
								TextviewUtils.manageBlinkEffect(mMemberName, getActivity());
							} else {
								((EditText) v).setGravity(Gravity.RIGHT);
								mMemberNameLayout.setVisibility(View.GONE);
								mMemberName.setText("");
							}
						}
					});

					rightContentRow.addView(mBeforeLoanPay_values);

					mLoans_values = new EditText(getActivity());
					mLoans_values.setId(j);
					sLoans_Fields.add(mLoans_values);
					mLoans_values.setInputType(InputType.TYPE_CLASS_NUMBER);
					mLoans_values.setPadding(5, 5, 5, 5);
					mLoans_values.setFilters(Get_EdiText_Filter.editText_filter());
					mLoans_values.setBackgroundResource(R.drawable.edittext_background);
					mLoans_values.setLayoutParams(rightContentParams);
					mLoans_values.setWidth(150);
					mLoans_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

						@Override
						public void onFocusChange(View v, boolean hasFocus) {
							// TODO Auto-generated method stub
							if (hasFocus) {
								((EditText) v).setGravity(Gravity.LEFT);
								mMemberNameLayout.setVisibility(View.VISIBLE);
								mMemberName.setText(memberName_Text.getText().toString().trim());
								mMemberName.setTypeface(LoginActivity.sTypeface);
								TextviewUtils.manageBlinkEffect(mMemberName, getActivity());
							} else {
								((EditText) v).setGravity(Gravity.RIGHT);
								mMemberNameLayout.setVisibility(View.GONE);
								mMemberName.setText("");
							}
						}
					});

					rightContentRow.addView(mLoans_values);

					mSurplus_values = new EditText(getActivity());
					mSurplus_values.setId(j);
					sSurplus_Fields.add(mSurplus_values);
					mSurplus_values.setInputType(InputType.TYPE_CLASS_NUMBER);
					mSurplus_values.setPadding(5, 5, 5, 5);
					mSurplus_values.setFilters(Get_EdiText_Filter.editText_filter());
					mSurplus_values.setBackgroundResource(R.drawable.edittext_background);
					mSurplus_values.setLayoutParams(rightContentParams);
					mSurplus_values.setWidth(150);
					mSurplus_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

						@Override
						public void onFocusChange(View v, boolean hasFocus) {
							// TODO Auto-generated method stub
							if (hasFocus) {
								((EditText) v).setGravity(Gravity.LEFT);
								mMemberNameLayout.setVisibility(View.VISIBLE);
								mMemberName.setText(memberName_Text.getText().toString().trim());
								mMemberName.setTypeface(LoginActivity.sTypeface);
								TextviewUtils.manageBlinkEffect(mMemberName, getActivity());
							} else {
								((EditText) v).setGravity(Gravity.RIGHT);
								mMemberNameLayout.setVisibility(View.GONE);
								mMemberName.setText("");
							}
						}
					});

					rightContentRow.addView(mSurplus_values);

					mRightContentTable.addView(rightContentRow);

				}

			} catch (ArrayIndexOutOfBoundsException e) {
				e.printStackTrace();

				TastyToast.makeText(getActivity(), AppStrings.adminAlert, TastyToast.LENGTH_SHORT, TastyToast.WARNING);

				getActivity().finish();
			}

			mSubmit_Raised_Button = (Button) rootView.findViewById(R.id.fragment_Submit_button);
			mSubmit_Raised_Button.setText(RegionalConversion.getRegionalConversion(AppStrings.submit));
			mSubmit_Raised_Button.setTypeface(LoginActivity.sTypeface);
			mSubmit_Raised_Button.setOnClickListener(this);

			onChangeWidthOfColumn();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return rootView;
	}

	private void OnCallInternalloanValues() {

		try {

			mPOLText_Values = new String[SelectedGroupsTask.member_Id.size()];

			sSendToServer_EditInternalLoan = Reset.reset(sSendToServer_EditInternalLoan);
			sPL_total = 0;
			sIncome_total = 0;
			sExpense_total = 0;
			sBeforeLoanPay_total = 0;
			sLoans_total = 0;
			sSurplus_total = 0;

			sPL_Fields.clear();
			sPOL_Fields.clear();
			sIncome_Fields.clear();
			sExpense_Fields.clear();
			sBefoeLoanPay_Fields.clear();
			sLoans_Fields.clear();
			sSurplus_Fields.clear();

			sSelected_POL = new String[SelectedGroupsTask.member_Id.size()];

			/** Purpose of Loan **/
			if (Boolean.valueOf(ConnectionUtils.isNetworkAvailable(getActivity()))) {

				mPOL_Response = Get_PurposeOfLoanTask.sGet_PurposeOfLoan_Response.split("~");
			} else {
				mPOL_Response = MainFragment_Dashboard.mPLDBPurposeofloan.split("~");
			}

			String loadPOL = "";
			String mPOLID = "";

			for (int i = 0; i < mPOL_Response.length; i++) {

				if (i % 2 != 0) {
					loadPOL = loadPOL + mPOL_Response[i] + ",";
					mPOL_Text = mPOL_Text + mPOL_Response[i] + ",";
				} else {
					mPOLID = mPOLID + mPOL_Response[i] + ",";
				}
			}

			mPOL_Values = loadPOL.split(",");
			mPOLText_Values = mPOL_Text.split(",");

			mPOL_ID = mPOLID.split(",");

			/*
			 * for (int i = 0; i < mPOL_Values.length; i++) {
			 * System.out.println("---------mPOL_Values--------" + mPOL_Values[i] +
			 * "    i   :" + i); System.out.println("---------mPOLText_Values--------" +
			 * mPOLText_Values[i] + "    i   :" + i);
			 * System.out.println("---------mPOL_ID--------" + mPOL_ID[i] + "    i   :" +
			 * i); } Log.d("LOAD POL --------------------", String.valueOf(loadPOL));
			 */
			sRowItems = new ArrayList<RowItem>();

			mPOL_Size = mPOL_Values.length;
			Log.e("LENGTH of POL ", String.valueOf(mPOL_Size));

			for (int i = 0; i < mPOL_Values.length; i++) {
				RowItem item = new RowItem(mPOL_Values[i]);

				sRowItems.add(item);
			}
			custAdapter = new CustomAdapter(getActivity(), sRowItems);

			loanType = RegionalConversion.getRegionalConversion(AppStrings.chooseLabel);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {

		case R.id.fragment_Submit_button:

			// To avoid Double click
			mSubmit_Raised_Button.setClickable(false);

			mMemberNameLayout.setVisibility(View.GONE);
			mMemberName.setText("");

			sPL_total = 0;
			sIncome_total = 0;
			sExpense_total = 0;
			sBeforeLoanPay_total = 0;
			sLoans_total = 0;
			sSurplus_total = 0;

			try {

				mSelectedPOL_Values = new String[sPL_Fields.size()];

				sPL_Amounts = new String[sPL_Fields.size()];
				sPOLvalues = new String[sPOL_Fields.size()];
				sIncomeA_Amounts = new String[sPOL_Fields.size()];
				sExpense_Amounts = new String[sPOL_Fields.size()];
				sBeforeLoanPay_Amounts = new String[sPOL_Fields.size()];
				sLoans_Amounts = new String[sPOL_Fields.size()];
				sSurplus_Amounts = new String[sPOL_Fields.size()];

				mSelected_POL_Values = "";
				sSendToServer_EditInternalLoan = "";

				sSelected_POL_Id = new String[SelectedGroupsTask.member_Id.size()];

				for (int i = 0; i < sPL_Amounts.length; i++) {

					sPL_Amounts[i] = String.valueOf(sPL_Fields.get(i).getText());

					sIncomeA_Amounts[i] = String.valueOf(sIncome_Fields.get(i).getText());
					sExpense_Amounts[i] = String.valueOf(sExpense_Fields.get(i).getText());
					sBeforeLoanPay_Amounts[i] = String.valueOf(sBefoeLoanPay_Fields.get(i).getText());
					sLoans_Amounts[i] = String.valueOf(sLoans_Fields.get(i).getText());
					sSurplus_Amounts[i] = String.valueOf(sSurplus_Fields.get(i).getText());

					if ((sPL_Amounts[i].equals("")) || (sPL_Amounts[i] == null)) {
						sPL_Amounts[i] = nullAmount;
					}

					if ((sIncomeA_Amounts[i].equals("")) || (sIncomeA_Amounts[i] == null)) {
						sIncomeA_Amounts[i] = nullAmount;
					}

					if ((sExpense_Amounts[i].equals("")) || (sExpense_Amounts[i] == null)) {
						sExpense_Amounts[i] = nullAmount;
					}

					if ((sBeforeLoanPay_Amounts[i].equals("")) || (sBeforeLoanPay_Amounts[i] == null)) {
						sBeforeLoanPay_Amounts[i] = nullAmount;
					}

					if ((sLoans_Amounts[i].equals("")) || (sLoans_Amounts[i] == null)) {
						sLoans_Amounts[i] = nullAmount;
					}
					if ((sSurplus_Amounts[i].equals("")) || (sSurplus_Amounts[i] == null)) {
						sSurplus_Amounts[i] = nullAmount;
					}

					sPOLvalues[i] = String.valueOf(sPOL_Fields.get(i).getText());

					if (sPOLvalues[i].equals(loanType)) {
						sSelected_POL[i] = nullAmount;
						sSelected_POL_Id[i] = "0";
					} else if (!sPOLvalues[i].equals(loanType)) {
						sSelected_POL[i] = sPOLvalues[i]; // sSelected_POL[i];
						for (int j = 0; j < mPOL_Values.length; j++) {
							if (mPOL_Values[j].equals(sPOLvalues[i])) {
								Log.e("mPOL_ID", mPOL_ID[j]);
								sSelected_POL_Id[i] = mPOL_ID[j];
								Log.e("Selected pol id$$$$$$$$$$$", sSelected_POL_Id[i] + "");
							}
						}
					}

					if (!sPL_Amounts[i].equals(nullAmount)) {

						if ((sSelected_POL[i].equals(nullAmount))) {
							isError = true;
						}
					}
					if (sPL_Amounts[i].equals(nullAmount)) {
						if ((!sSelected_POL[i].equals(nullAmount))) {
							isnullAmountError = true;
						}

						if ((!sIncomeA_Amounts[i].equals(nullAmount))) {
							isnullAmountError = true;
						}

						if ((!sExpense_Amounts[i].equals(nullAmount))) {
							isnullAmountError = true;
						}

						if ((!sBeforeLoanPay_Amounts[i].equals(nullAmount))) {
							isnullAmountError = true;
						}

						if ((!sLoans_Amounts[i].equals(nullAmount))) {
							isnullAmountError = true;
						}

						if ((!sSurplus_Amounts[i].equals(nullAmount))) {
							isnullAmountError = true;
						}
					}

					if (sSelected_POL[i] != null) {
						if (sSelected_POL[i].equals(nullAmount)) {
							mSelected_POL_Values = mSelected_POL_Values + AppStrings.dialogNo + ",";
						} else if (!sSelected_POL[i].equals(nullAmount)) {
							mSelected_POL_Values = mSelected_POL_Values + sSelected_POL[i] + ",";
						}

						sPL_total = sPL_total + Integer.parseInt(sPL_Amounts[i]);

						sIncome_total = sIncome_total + Integer.parseInt(sIncomeA_Amounts[i]);
						sExpense_total = sExpense_total + Integer.parseInt(sExpense_Amounts[i]);
						sBeforeLoanPay_total = sBeforeLoanPay_total + Integer.parseInt(sBeforeLoanPay_Amounts[i]);
						sLoans_total = sLoans_total + Integer.parseInt(sLoans_Amounts[i]);
						sSurplus_total = sSurplus_total + Integer.parseInt(sSurplus_Amounts[i]);

						sSendToServer_EditInternalLoan = sSendToServer_EditInternalLoan
								+ String.valueOf(SelectedGroupsTask.member_Id.elementAt(i)) + "~"
								+ String.valueOf(sPL_Amounts[i]).trim() + "~" + String.valueOf(sSelected_POL_Id[i])
								+ "~" + String.valueOf(sIncomeA_Amounts[i].trim()) + "~"
								+ String.valueOf(sExpense_Amounts[i].trim()) + "~"
								+ String.valueOf(sBeforeLoanPay_Amounts[i].trim()) + "~"
								+ String.valueOf(sLoans_Amounts[i].trim()) + "~"
								+ String.valueOf(sSurplus_Amounts[i].trim()) + "~";

					}

				}

				mSelectedPOL_Values = mSelected_POL_Values.split(",");

				Log.d(TAG, "Total " + Integer.toString(sPL_total));

				Log.d(TAG, "Vals---------------------------" + sSendToServer_EditInternalLoan);

				if ((sPL_total != 0) && (!Boolean.valueOf(isError)) && (!Boolean.valueOf(isnullAmountError))) {
					System.out.println("Do Navigate");

					confirmationDialog = new Dialog(getActivity());

					LayoutInflater inflater = getActivity().getLayoutInflater();
					View dialogView = inflater.inflate(R.layout.dialog_confirmation, null);
					dialogView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
							LayoutParams.WRAP_CONTENT));

					TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
					confirmationHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.confirmation));
					confirmationHeader.setTypeface(LoginActivity.sTypeface);

					TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

					TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
							LayoutParams.WRAP_CONTENT, 1f);
					contentParams.setMargins(10, 5, 10, 5);

					for (int i = 0; i < sPL_Amounts.length; i++) {

						TableRow indv_SavingsRow = new TableRow(getActivity());

						TextView memberName_Text = new TextView(getActivity());
						memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
								String.valueOf(SelectedGroupsTask.member_Name.elementAt(i))));
						memberName_Text.setTypeface(LoginActivity.sTypeface);
						memberName_Text.setTextColor(color.black);
						memberName_Text.setPadding(5, 5, 5, 5);
						memberName_Text.setLayoutParams(contentParams);
						indv_SavingsRow.addView(memberName_Text);

						TextView confirm_values = new TextView(getActivity());
						confirm_values
								.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sPL_Amounts[i])));
						confirm_values.setTextColor(color.black);
						confirm_values.setPadding(5, 5, 5, 5);
						confirm_values.setGravity(Gravity.RIGHT);
						confirm_values.setLayoutParams(contentParams);
						indv_SavingsRow.addView(confirm_values);

						TextView POL_values = new TextView(getActivity());
						POL_values.setText(
								GetSpanText.getSpanString(getActivity(), String.valueOf(mSelectedPOL_Values[i])));
						POL_values.setTextColor(color.black);
						POL_values.setPadding(5, 5, 5, 5);
						POL_values.setGravity(Gravity.RIGHT);
						POL_values.setLayoutParams(contentParams);
						indv_SavingsRow.addView(POL_values);

						TextView income_values = new TextView(getActivity());
						income_values
								.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sIncomeA_Amounts[i])));
						income_values.setTextColor(color.black);
						income_values.setPadding(5, 5, 5, 5);
						income_values.setGravity(Gravity.RIGHT);
						income_values.setLayoutParams(contentParams);
						indv_SavingsRow.addView(income_values);

						TextView expense_values = new TextView(getActivity());
						expense_values
								.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sExpense_Amounts[i])));
						expense_values.setTextColor(color.black);
						expense_values.setPadding(5, 5, 5, 5);
						expense_values.setGravity(Gravity.RIGHT);
						expense_values.setLayoutParams(contentParams);
						indv_SavingsRow.addView(expense_values);

						TextView beforeLoanPay_values = new TextView(getActivity());
						beforeLoanPay_values.setText(
								GetSpanText.getSpanString(getActivity(), String.valueOf(sBeforeLoanPay_Amounts[i])));
						beforeLoanPay_values.setTextColor(color.black);
						beforeLoanPay_values.setPadding(5, 5, 5, 5);
						beforeLoanPay_values.setGravity(Gravity.RIGHT);
						beforeLoanPay_values.setLayoutParams(contentParams);
						indv_SavingsRow.addView(beforeLoanPay_values);

						TextView loans_values = new TextView(getActivity());
						loans_values
								.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sLoans_Amounts[i])));
						loans_values.setTextColor(color.black);
						loans_values.setPadding(5, 5, 5, 5);
						loans_values.setGravity(Gravity.RIGHT);
						loans_values.setLayoutParams(contentParams);
						indv_SavingsRow.addView(loans_values);

						TextView surplus_values = new TextView(getActivity());
						surplus_values
								.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sSurplus_Amounts[i])));
						surplus_values.setTextColor(color.black);
						surplus_values.setPadding(5, 5, 5, 5);
						surplus_values.setGravity(Gravity.RIGHT);
						surplus_values.setLayoutParams(contentParams);
						indv_SavingsRow.addView(surplus_values);

						confirmationTable.addView(indv_SavingsRow,
								new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

					}
					View rullerView = new View(getActivity());
					rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
					rullerView.setBackgroundColor(Color.rgb(0, 199, 140));// rgb(255,
																			// 229,
																			// 242));
					confirmationTable.addView(rullerView);

					TableRow totalRow = new TableRow(getActivity());

					TextView totalText = new TextView(getActivity());
					totalText.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.total)));
					totalText.setTypeface(LoginActivity.sTypeface);
					totalText.setTextColor(color.black);
					totalText.setPadding(5, 5, 5, 5);// (5, 10, 5, 10);
					totalText.setLayoutParams(contentParams);
					totalRow.addView(totalText);

					TextView totalAmount = new TextView(getActivity());
					totalAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sPL_total)));
					totalAmount.setTextColor(color.black);
					totalAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
					totalAmount.setGravity(Gravity.RIGHT);
					totalAmount.setLayoutParams(contentParams);
					totalRow.addView(totalAmount);

					TextView emptyAmount = new TextView(getActivity());
					emptyAmount.setText("");
					emptyAmount.setTextColor(color.black);
					emptyAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
					emptyAmount.setGravity(Gravity.RIGHT);
					emptyAmount.setLayoutParams(contentParams);
					totalRow.addView(emptyAmount);

					TextView incomeAmount = new TextView(getActivity());
					incomeAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sIncome_total)));
					incomeAmount.setTextColor(color.black);
					incomeAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
					incomeAmount.setGravity(Gravity.RIGHT);
					incomeAmount.setLayoutParams(contentParams);
					totalRow.addView(incomeAmount);

					TextView expenseAmount = new TextView(getActivity());
					expenseAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sExpense_total)));
					expenseAmount.setTextColor(color.black);
					expenseAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
					expenseAmount.setGravity(Gravity.RIGHT);
					expenseAmount.setLayoutParams(contentParams);
					totalRow.addView(expenseAmount);

					TextView blpAmount = new TextView(getActivity());
					blpAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sBeforeLoanPay_total)));
					blpAmount.setTextColor(color.black);
					blpAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
					blpAmount.setGravity(Gravity.RIGHT);
					blpAmount.setLayoutParams(contentParams);
					totalRow.addView(blpAmount);

					TextView loansAmount = new TextView(getActivity());
					loansAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sLoans_total)));
					loansAmount.setTextColor(color.black);
					loansAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
					loansAmount.setGravity(Gravity.RIGHT);
					loansAmount.setLayoutParams(contentParams);
					totalRow.addView(loansAmount);

					TextView surplusAmount = new TextView(getActivity());
					surplusAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sSurplus_total)));
					surplusAmount.setTextColor(color.black);
					surplusAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
					surplusAmount.setGravity(Gravity.RIGHT);
					surplusAmount.setLayoutParams(contentParams);
					totalRow.addView(surplusAmount);

					confirmationTable.addView(totalRow,
							new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

					mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit_button);
					mEdit_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.edit));
					mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
					mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
																			// 205,
																			// 0));
					mEdit_RaisedButton.setOnClickListener(this);

					mOk_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Ok_button);
					mOk_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.yes));
					mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
					mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
					mOk_RaisedButton.setOnClickListener(this);

					confirmationDialog.getWindow()
							.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
					confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					confirmationDialog.setCanceledOnTouchOutside(false);
					confirmationDialog.setContentView(dialogView);
					confirmationDialog.setCancelable(true);
					confirmationDialog.show();

					MarginLayoutParams margin = (MarginLayoutParams) dialogView.getLayoutParams();
					margin.leftMargin = 10;
					margin.rightMargin = 10;
					margin.topMargin = 10;
					margin.bottomMargin = 10;
					margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);

				} /*
					 * else if (sPL_total > Integer.parseInt(SelectedGroupsTask.sCashinHand)) {
					 * 
					 * TastyToast.makeText(getActivity(), AppStrings.cashinHandAlert,
					 * TastyToast.LENGTH_SHORT, TastyToast.WARNING);
					 * 
					 * sSendToServer_EditInternalLoan = Reset.reset(sSendToServer_EditInternalLoan);
					 * sPL_total = Integer.parseInt(nullAmount);
					 * 
					 * }
					 */else if (Boolean.valueOf(isnullAmountError) || (sPL_total == Integer.parseInt(nullAmount))) {

					isnullAmountError = false;
					TastyToast.makeText(getActivity(), AppStrings.nullAlert, TastyToast.LENGTH_SHORT,
							TastyToast.WARNING);

					sSendToServer_EditInternalLoan = Reset.reset(sSendToServer_EditInternalLoan);
					sPL_total = Integer.parseInt(nullAmount);
					sIncome_total = Integer.parseInt(nullAmount);
					sExpense_total = Integer.parseInt(nullAmount);
					sBeforeLoanPay_total = Integer.parseInt(nullAmount);
					sLoans_total = Integer.parseInt(nullAmount);
					sSurplus_total = Integer.parseInt(nullAmount);

				} else if (Boolean.valueOf(isError)) {

					isError = false;

					TastyToast.makeText(getActivity(), AppStrings.mPOLAlert, TastyToast.LENGTH_SHORT,
							TastyToast.WARNING);
					sSendToServer_EditInternalLoan = Reset.reset(sSendToServer_EditInternalLoan);
					sPL_total = Integer.parseInt(nullAmount);
					sIncome_total = Integer.parseInt(nullAmount);
					sExpense_total = Integer.parseInt(nullAmount);
					sBeforeLoanPay_total = Integer.parseInt(nullAmount);
					sLoans_total = Integer.parseInt(nullAmount);
					sSurplus_total = Integer.parseInt(nullAmount);

				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			// }
			break;

		case R.id.fragment_Edit_button:

			mMemberNameLayout.setVisibility(View.GONE);
			mMemberName.setText("");
			sSendToServer_EditInternalLoan = Reset.reset(sSendToServer_EditInternalLoan);
			sPL_total = Integer.parseInt(nullAmount);
			mSelected_POL_Values = Reset.reset(mSelected_POL_Values);
			confirmationDialog.dismiss();
			break;

		case R.id.fragment_Ok_button:

			if (ConnectionUtils.isNetworkAvailable(getActivity())) {
				new Get_MicroCreditPlanWebservice(Meeting_Minutes_MicroCreditPlanFragment.this).execute();
			} else {
				// Do offline Stuffs

			}

			break;

		}

	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub
		mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
		mProgressDilaog.show();
	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub

		try {

			if (mProgressDilaog != null) {
				mProgressDilaog.dismiss();

				if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {
					getActivity().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub

							if (!result.equals("FAIL")) {
								confirmationDialog.dismiss();

								TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg,
										TastyToast.LENGTH_SHORT, TastyToast.ERROR);
								Constants.NETWORKCOMMONFLAG = "SUCESS";
							}

						}
					});
				} else {
					confirmationDialog.dismiss();
					if (publicValues.mGetMinutesLanguageValues.equals("Yes")) {
						publicValues.mGetMinutesLanguageValues = null;
						if (EShaktiApplication.isDefault()) {

							TastyToast.makeText(getActivity(), AppStrings.transactionCompleted, TastyToast.LENGTH_SHORT,
									TastyToast.SUCCESS);

							DefaultReportsFragment fragment_Dashboard = new DefaultReportsFragment();
							setFragment(fragment_Dashboard);
						} else {

							TastyToast.makeText(getActivity(), AppStrings.transactionCompleted, TastyToast.LENGTH_SHORT,
									TastyToast.SUCCESS);

							MainFragment_Dashboard fragment = new MainFragment_Dashboard();
							setFragment(fragment);

						}
					} else {

						TastyToast.makeText(getActivity(), AppStrings.transactionFailAlert, TastyToast.LENGTH_SHORT,
								TastyToast.ERROR);
						MainFragment_Dashboard fragment = new MainFragment_Dashboard();
						setFragment(fragment);
					}

				}
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	private void onChangeWidthOfColumn() {
		// TODO Auto-generated method stub

		leftHeaderColumnWidth = new int[((TableRow) mLeftHeaderTable.getChildAt(0)).getChildCount()];

		leftContentColumnWidth = new int[((TableRow) mLeftContentTable.getChildAt(0)).getChildCount()];

		TableRow leftHeaderRow = (TableRow) mLeftHeaderTable.getChildAt(0);
		TableRow leftContentRow = (TableRow) mLeftContentTable.getChildAt(0);

		for (int i = 0; i < leftHeaderRow.getChildCount(); i++) {
			leftHeaderColumnWidth[i] = this.viewWidth(leftHeaderRow.getChildAt(i));
			leftContentColumnWidth[i] = this.viewWidth(leftContentRow.getChildAt(i));
		}

		for (int i = 0; i < leftHeaderColumnWidth.length; i++) {

			if (leftHeaderColumnWidth[i] > leftContentColumnWidth[i]) {
				TextView leftContentTextView = (TextView) leftContentRow.getChildAt(i);
				leftContentTextView.setLayoutParams(
						new TableRow.LayoutParams(leftHeaderColumnWidth[i], LayoutParams.MATCH_PARENT));
			} else {
				TextView leftHeaderTextView = (TextView) leftHeaderRow.getChildAt(i);
				leftHeaderTextView.setLayoutParams(
						new TableRow.LayoutParams(leftContentColumnWidth[i] + 10, LayoutParams.MATCH_PARENT));
			}
		}

		rightHeaderColumnWidth = new int[((TableRow) mRightHeaderTable.getChildAt(0)).getChildCount()];
		TableRow headerRow = (TableRow) mRightHeaderTable.getChildAt(0);

		rightContentColumnWidth = new int[((TableRow) mRightContentTable.getChildAt(0)).getChildCount()];

		TableRow contentRow = (TableRow) mRightContentTable.getChildAt(0);

		for (int i = 0; i < headerRow.getChildCount(); i++) {
			rightHeaderColumnWidth[i] = this.viewWidth(headerRow.getChildAt(i));

			rightContentColumnWidth[i] = this.viewWidth(contentRow.getChildAt(i));

		}

		for (int i = 0; i < rightHeaderColumnWidth.length; i++) {

			if (rightHeaderColumnWidth[i] > rightContentColumnWidth[i]) {
				if ((i != 1)) {
					EditText editTextView = (EditText) contentRow.getChildAt(i);
					TableRow.LayoutParams editTextParams = new TableRow.LayoutParams(rightHeaderColumnWidth[i],
							LayoutParams.MATCH_PARENT);
					editTextParams.setMargins(10, 5, 10, 5);
					editTextView.setLayoutParams(editTextParams);

				} else {
					TextView contentTextView = (TextView) contentRow.getChildAt(i);
					contentTextView.setLayoutParams(
							new TableRow.LayoutParams(rightHeaderColumnWidth[i], LayoutParams.MATCH_PARENT));
				}

			} else {
				TextView headerTextView = (TextView) headerRow.getChildAt(i);
				TableRow.LayoutParams params = new TableRow.LayoutParams(rightContentColumnWidth[i],
						LayoutParams.MATCH_PARENT);
				params.setMargins(10, 0, 10, 0);
				headerTextView.setLayoutParams(params);

			}
		}

	}

	// read a view's width
	private int viewWidth(View view) {
		view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
		return view.getMeasuredWidth();
	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub

		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

		FragmentDrawer.drawer_lastTR_Date.setText(RegionalConversion.getRegionalConversion(AppStrings.lastTransactionDate) + " : "
				+ SelectedGroupsTask.sLastTransactionDate_Response);
		FragmentDrawer.drawer_lastTR_Date.setTypeface(LoginActivity.sTypeface);
		
		UpdateCalendarMinMaxDateUtils.OnUpdateCalendarDate();
		
		Calendar calender = Calendar.getInstance();

		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String formattedDate = df.format(calender.getTime());
		EShaktiApplication.setLastTransDate_GroupId(SelectedGroupsTask.Group_Id);

		new GroupTempDBLastTransDate(EShaktiApplication.getLastTransDate_GroupId(),
				SelectedGroupsTask.sLastTransactionDate_Response, formattedDate);

		GroupProvider.updateGroupLastTransDateResponse();
	}

}
