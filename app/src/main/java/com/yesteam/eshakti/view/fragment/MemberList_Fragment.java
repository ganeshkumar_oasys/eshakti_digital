package com.yesteam.eshakti.view.fragment;

import java.util.ArrayList;
import java.util.List;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.adapter.CustomListAdapter;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.model.ListItem;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

public class MemberList_Fragment extends Fragment implements OnItemClickListener {

	public static String TAG = MemberList_Fragment.class.getSimpleName();
	public static String sSelected_MemberId = null;

	private TextView mGroupName, mCashinHand, mCashatBank;
	private List<ListItem> listItems;
	private ListView mListView;
	
	private CustomListAdapter mAdapter;
	String[] memList;
	int listImage;
	private TextView mHeader;

	public MemberList_Fragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_MEMBER_REPORTS_MEMBERLIST;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_menulist, container, false);

		MainFragment_Dashboard.isBackpressed = false;

		try {

			EShaktiApplication.setCheckGroupListTextColor(false);
			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashinHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashinHand.setTypeface(LoginActivity.sTypeface);

			mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashatBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashatBank.setTypeface(LoginActivity.sTypeface);

			if (!Boolean.valueOf(MainFragment_Dashboard.isProfile)) {
			mHeader = (TextView) rootView.findViewById(R.id.submenuHeaderTextview);
			mHeader.setVisibility(View.VISIBLE);
			mHeader.setText(
					RegionalConversion.getRegionalConversion(AppStrings.Memberreports));
			mHeader.setTypeface(LoginActivity.sTypeface);
			} else {
			
				mHeader = (TextView) rootView.findViewById(R.id.submenuHeaderTextview);
				mHeader.setVisibility(View.VISIBLE);
				mHeader.setText(
						RegionalConversion.getRegionalConversion(AppStrings.uploadInfo));
				mHeader.setTypeface(LoginActivity.sTypeface);
			}
			listItems = new ArrayList<ListItem>();
			mListView = (ListView) rootView.findViewById(R.id.fragment_List);

			memList = new String[SelectedGroupsTask.member_Name.size()];
			listImage = R.drawable.ic_navigate_next_white_24dp;

			for (int i = 0; i < memList.length; i++) {
				ListItem item = new ListItem(SelectedGroupsTask.member_Name.elementAt(i).toString(), listImage);
				listItems.add(item);
			}

			mAdapter = new CustomListAdapter(getActivity(), listItems);
			mListView.setAdapter(mAdapter);
			mListView.setOnItemClickListener(this);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return rootView;
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		// TODO Auto-generated method stub

		sSelected_MemberId = String.valueOf(SelectedGroupsTask.member_Id.elementAt(position));
		Log.d(TAG, "MEMBER NAME - LIST --> " + String.valueOf(SelectedGroupsTask.member_Name.elementAt(position)));

		EShaktiApplication.setSelectedMemberName(String.valueOf(SelectedGroupsTask.member_Name.elementAt(position)));
		Log.v("Member Name", EShaktiApplication.getSelectedMemberName());
		
		TextView change_TextView = (TextView) view.findViewById(R.id.dynamicText);
		change_TextView.setTextColor(Color.rgb(251, 161, 108));

		if (!Boolean.valueOf(MainFragment_Dashboard.isProfile)) {

			Reports_MemberReports_MenuFragment fragment = new Reports_MemberReports_MenuFragment();
			setFragment(fragment);

		} else if (Boolean.valueOf(MainFragment_Dashboard.isProfile)) {

			if (ConnectionUtils.isNetworkAvailable(getActivity())) {

				Profile_Member_Aadhaar_Image_MenuFragment fragment = new Profile_Member_Aadhaar_Image_MenuFragment();
				setFragment(fragment);

			} else if (!ConnectionUtils.isNetworkAvailable(getActivity())) {

				Profile_Member_Aadhaar_Image_MenuFragment fragment = new Profile_Member_Aadhaar_Image_MenuFragment();
				setFragment(fragment);
				mAdapter.notifyDataSetChanged();

			}
		}

	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub
		Log.e("Current Class Name!!!!!!!!!!!!!!", fragment.getClass().getName());
		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

	}

}
