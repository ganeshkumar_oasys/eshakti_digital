package com.yesteam.eshakti.view.fragment;

import java.util.ArrayList;
import java.util.List;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.adapter.CustomItemAdapter;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.model.RowItem;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.views.MaterialSpinner;
import com.yesteam.eshakti.views.RaisedButton;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

public class Payments_Cashdeposit_Fragment extends Fragment {

	public static String TAG = MemberList_Fragment.class.getSimpleName();
	public static String sSelected_MemberId = null;

	private TextView mGroupName, mCashinHand, mCashatBank;

	MaterialSpinner mSpinner_MemberName;
	CustomItemAdapter mMemberNameAdapter;
	private List<RowItem> mMemberNameItems;
	public static String selectedMemberName;
	public static String mMemberNameValue = null;
	View rootView;
	private String[] mMemberNameArray;

	TextView mFundTransfer_text;
	EditText mFundtransferAmount_edittext;
	RaisedButton mSubmit_Button;

	public Payments_Cashdeposit_Fragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_MEMBER_REPORTS_MEMBERLIST;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		rootView = inflater.inflate(R.layout.fragment_payments_cashdeposit, container, false);

		MainFragment_Dashboard.isBackpressed = false;

		try {

			EShaktiApplication.setCheckGroupListTextColor(false);
			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashinHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashinHand.setTypeface(LoginActivity.sTypeface);

			mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashatBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashatBank.setTypeface(LoginActivity.sTypeface);

			init();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return rootView;
	}

	private void init() {
		// TODO Auto-generated method stub

		mSpinner_MemberName = (MaterialSpinner) rootView.findViewById(R.id.memberlist_cashdeposit_spinner);
		mSpinner_MemberName.setBaseColor(color.grey_400);
		mSpinner_MemberName.setFloatingLabelText("Member Name");
		mSpinner_MemberName.setPaddingSafe(10, 0, 10, 0);

		mFundTransfer_text = (TextView) rootView.findViewById(R.id.payments_cashdeposit_TextView);
		mFundtransferAmount_edittext = (EditText) rootView.findViewById(R.id.payments_cashdeposit);
		mSubmit_Button = (RaisedButton) rootView.findViewById(R.id.payments_cashdeposit_submit);

		mFundTransfer_text.setTypeface(LoginActivity.sTypeface);
		mFundtransferAmount_edittext.setTypeface(LoginActivity.sTypeface);
		mSubmit_Button.setTypeface(LoginActivity.sTypeface);

		mMemberNameItems = new ArrayList<RowItem>();

		mMemberNameArray = new String[SelectedGroupsTask.member_Name.size() + 1];
		mMemberNameArray[0] = String.valueOf("Member Name");
		for (int i = 0; i < SelectedGroupsTask.member_Name.size(); i++) {
			mMemberNameArray[i + 1] = SelectedGroupsTask.member_Name.elementAt(i).toString();
		}

		for (int i = 0; i < mMemberNameArray.length; i++) {
			RowItem rowItem = new RowItem(mMemberNameArray[i]);
			mMemberNameItems.add(rowItem);
		}
		mMemberNameAdapter = new CustomItemAdapter(getActivity(), mMemberNameItems);
		mSpinner_MemberName.setAdapter(mMemberNameAdapter);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub
		Log.e("Current Class Name!!!!!!!!!!!!!!", fragment.getClass().getName());
		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

	}

}
