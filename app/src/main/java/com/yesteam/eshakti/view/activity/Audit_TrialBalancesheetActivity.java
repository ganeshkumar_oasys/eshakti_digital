package com.yesteam.eshakti.view.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.PopupWindow;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.R;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.utils.GetExit;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.utils.GetTypeface;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.views.RaisedButton;

public class Audit_TrialBalancesheetActivity extends AppCompatActivity implements OnClickListener {

	Toolbar mToolbar;
	Context context;
	Typeface sTypeface;
	String mLanguageLocalae = "", mCheckedTransactionList = "";
	private TextView mCheckListCount;
	private TableLayout mTableLayout;
	private RaisedButton mSubmit_button;
	int mCount = 0;
	private CheckBox[] mCheckbox;
	private String[] mTransactionArr, mCreditAmountArr, mDebitAmountArr;
	Dialog confirmationDialog;
	private Button mEdit_RaisedButton, mOk_RaisedButton;
	PopupWindow popupwindow;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.audit_activity_trialbalancesheet);

		try {

			if (PrefUtils.getUserlangcode() != null) {
				mLanguageLocalae = PrefUtils.getUserlangcode();
			} else {
				mLanguageLocalae = null;
			}

			sTypeface = GetTypeface.getTypeface(getApplicationContext(), mLanguageLocalae);

		} catch (Exception e) {
			e.printStackTrace();
		}

		try {

			mToolbar = (Toolbar) findViewById(R.id.audit_date_selection_toolbar_grouplist);
			TextView mTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);
			setSupportActionBar(mToolbar);
			getSupportActionBar().setDisplayShowHomeEnabled(true);

			getSupportActionBar().setTitle("");
			mTitle.setText(PrefUtils.getAnimatorName() + "    " + PrefUtils.getAgentUserName());
			mTitle.setTypeface(sTypeface);
			mTitle.setGravity(Gravity.CENTER);

			mCount = 0;
			mCheckListCount = (TextView) findViewById(R.id.auditCoutIndicator_Textview);
			mCheckListCount.setText(String.valueOf(mCount));
			mCheckListCount.setTypeface(sTypeface);

			mSubmit_button = (RaisedButton) findViewById(R.id.audit_trialbalancesheet_Submitbutton);
			mSubmit_button.setText(RegionalConversion.getRegionalConversion(AppStrings.submit));
			mSubmit_button.setTypeface(sTypeface);
			mSubmit_button.setOnClickListener(this);

			mTransactionArr = new String[10];
			mCreditAmountArr = new String[10];
			mDebitAmountArr = new String[10];

			for (int i = 0; i < mTransactionArr.length; i++) {
				mTransactionArr[i] = "Transaction " + i;
				mCreditAmountArr[i] = "1000";
				mDebitAmountArr[i] = "1000";
			}
			mTableLayout = (TableLayout) findViewById(R.id.auditTrialBalancesheet_Tablelayout);

			TableRow head_row = new TableRow(Audit_TrialBalancesheetActivity.this);
			TableRow.LayoutParams headerParams = new TableRow.LayoutParams(0, LayoutParams.WRAP_CONTENT, 1f);

			/*
			 * CheckBox checkBox_header = new
			 * CheckBox(Audit_TrialBalancesheetActivity.this);
			 * checkBox_header.setChecked(false);
			 * checkBox_header.setBackgroundResource(R.drawable.btn_check_to_off_mtrl_013);
			 * checkBox_header.setVisibility(View.INVISIBLE);
			 * head_row.addView(checkBox_header);
			 */

			TextView head_details = new TextView(Audit_TrialBalancesheetActivity.this);
			head_details.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.details)));
			head_details.setTypeface(LoginActivity.sTypeface);
			head_details.setTextColor(Color.WHITE);
			head_details.setPadding(20, 5, 10, 5);
			head_details.setBackgroundResource(R.color.tableHeader);
			head_details.setLayoutParams(headerParams);
			head_details.setGravity(Gravity.CENTER);
			head_row.addView(head_details);

			TextView creditAmount = new TextView(Audit_TrialBalancesheetActivity.this);
			creditAmount.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.credit)));
			creditAmount.setTypeface(LoginActivity.sTypeface);
			creditAmount.setTextColor(Color.WHITE);
			creditAmount.setPadding(10, 5, 10, 5);
			creditAmount.setGravity(Gravity.RIGHT);
			creditAmount.setBackgroundResource(R.color.tableHeader);
			creditAmount.setLayoutParams(headerParams);
			head_row.addView(creditAmount);

			TextView debitAmount = new TextView(Audit_TrialBalancesheetActivity.this);
			debitAmount.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.debit)));
			debitAmount.setTypeface(LoginActivity.sTypeface);
			debitAmount.setTextColor(Color.WHITE);
			debitAmount.setPadding(10, 5, 10, 5);
			debitAmount.setGravity(Gravity.RIGHT);
			debitAmount.setBackgroundResource(R.color.tableHeader);
			debitAmount.setLayoutParams(headerParams);
			head_row.addView(debitAmount);

			mTableLayout.addView(head_row, new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
					TableLayout.LayoutParams.WRAP_CONTENT));

			mCheckbox = new CheckBox[10];

			for (int i = 0; i < 10; i++) {
				TableRow contentRow = new TableRow(Audit_TrialBalancesheetActivity.this);
				/*
				 * TableRow.LayoutParams contentParams = new TableRow.LayoutParams(
				 * LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1f);
				 */
				mCheckbox[i] = new CheckBox(Audit_TrialBalancesheetActivity.this);
				mCheckbox[i].setChecked(false);
				mCheckbox[i].setBackgroundResource(R.drawable.btn_check_to_off_mtrl_013);
				mCheckbox[i].setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
						// TODO Auto-generated method stub
						if (isChecked) {
							mCount = mCount + 1;
							System.out.println("   Check  count  =  " + mCount);
							mCheckListCount.setText(String.valueOf(mCount));
						} else {
							mCount = mCount - 1;
							System.out.println("   Check  count  =  " + mCount);
							mCheckListCount.setText(String.valueOf(mCount));
						}
					}
				});

				contentRow.addView(mCheckbox[i]);

				TextView details = new TextView(Audit_TrialBalancesheetActivity.this);
				details.setText(GetSpanText.getSpanString(this, String.valueOf(mTransactionArr[i])));
				details.setPadding(10, 5, 10, 5);
				details.setTextColor(color.black);
				details.setLayoutParams(headerParams);// (contentParams);
				contentRow.addView(details);

				TextView credit_amount = new TextView(Audit_TrialBalancesheetActivity.this);
				credit_amount.setText(GetSpanText.getSpanString(this, String.valueOf(mCreditAmountArr[i])));
				credit_amount.setPadding(50, 5, 10, 5);
				credit_amount.setTextColor(color.black);
				credit_amount.setGravity(Gravity.RIGHT);
				credit_amount.setLayoutParams(headerParams);// (contentParams);
				contentRow.addView(credit_amount);

				TextView debit_amount = new TextView(Audit_TrialBalancesheetActivity.this);
				debit_amount.setText(GetSpanText.getSpanString(this, String.valueOf(mDebitAmountArr[i])));
				debit_amount.setPadding(10, 5, 10, 5);
				debit_amount.setTextColor(color.black);
				debit_amount.setGravity(Gravity.RIGHT);
				debit_amount.setLayoutParams(headerParams);// (contentParams);
				contentRow.addView(debit_amount);

				mTableLayout.addView(contentRow,
						new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

				View rullerView = new View(Audit_TrialBalancesheetActivity.this);
				rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
				rullerView.setBackgroundColor(Color.rgb(220, 220, 220));

				mTableLayout.addView(rullerView);

				mCheckListCount.setOnClickListener(this);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_edit_ob_group, menu);
		MenuItem item = menu.getItem(0);
		item.setVisible(true);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.

		int id = item.getItemId();
		if (id == R.id.group_logout_edit) {

			startActivity(new Intent(GetExit.getExitIntent(getApplicationContext())));
			this.finish();

			return true;

		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {
		case R.id.auditCoutIndicator_Textview:

			mCheckedTransactionList = "";
			for (int i = 0; i < mTransactionArr.length; i++) {
				if (mCheckbox[i].isChecked()) {
					mCheckedTransactionList = mCheckedTransactionList + mTransactionArr[i] + "~";
				}
			}

			System.out.println("mCheckedTransactionList  =  " + mCheckedTransactionList + "");
			String[] listItems = mCheckedTransactionList.split("~");

			for (int i = 0; i < listItems.length; i++) {
				System.out.println("List items = " + listItems[i].toString() + "");
			}

			if (!mCheckedTransactionList.equals("")) {

				// PopupMenu

				PopupMenu popupmenu = new PopupMenu(Audit_TrialBalancesheetActivity.this, mCheckListCount);
				popupmenu.setGravity(Gravity.END);

				for (int i = 0; i < listItems.length; i++) {
					popupmenu.getMenu().add(listItems[i]);
				}
				popupmenu.show();
			} else {
				TastyToast.makeText(getApplicationContext(), "Please select any item", TastyToast.LENGTH_SHORT,
						TastyToast.WARNING);
			}

			break;

		case R.id.audit_trialbalancesheet_Submitbutton:

			Intent intent = new Intent(Audit_TrialBalancesheetActivity.this, Audit_Savings_TransactionActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);
			finish();

			break;
		default:
			break;

		}
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		if (popupwindow != null) {
			popupwindow.dismiss();
			popupwindow = null;
		}
	}

}
