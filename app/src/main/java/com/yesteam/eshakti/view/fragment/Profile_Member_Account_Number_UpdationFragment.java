package com.yesteam.eshakti.view.fragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.sqlite.database.BankDetailsProvider;
import com.yesteam.eshakti.sqlite.db.model.BankDetails;
import com.yesteam.eshakti.utils.AccountNumberValidationUtils;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.utils.Get_EdiText_Filter;
import com.yesteam.eshakti.utils.Reset;
import com.yesteam.eshakti.utils.TextviewUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.views.CustomHorizontalScrollView;
import com.yesteam.eshakti.views.CustomHorizontalScrollView.onScrollChangedListener;
import com.yesteam.eshakti.views.RaisedButton;
import com.yesteam.eshakti.webservices.Get_Update_MemberAccountNumber_Webservices;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;
import com.yesteam.eshakti.widget.Dialog_AccountNoUpdationFragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Profile_Member_Account_Number_UpdationFragment extends Fragment implements OnClickListener, TaskListener {

	private TextView mGroupName, mCashinHand, mCashatBank, mHeader, mLoanType;
	private RaisedButton mSubmit_Raised_Button, mEdit_RaisedButton, mOk_RaisedButton;
	private TableLayout mLeftHeaderTable, mRightHeaderTable, mLeftContentTable, mRightContentTable;
	private CustomHorizontalScrollView mHSRightHeader, mHSRightContent;
	int mSize;
	String width[] = { RegionalConversion.getRegionalConversion(AppStrings.bankName),
			RegionalConversion.getRegionalConversion(AppStrings.mBranchName),
			RegionalConversion.getRegionalConversion(AppStrings.mAccountNumber) };
	int[] rightHeaderWidth = new int[width.length];
	int[] rightContentWidth = new int[width.length];
	LinearLayout mMemberNameLayout;
	TextView mMemberName;
	private EditText mAccountNumber_values;
	private TextView mBankName_values, mBranchName_values;
	public static List<EditText> sAccountNumber_Fields = new ArrayList<EditText>();
	public static List<TextView> sBankName_Fields = new ArrayList<TextView>();
	public static List<TextView> sBranch_Fields = new ArrayList<TextView>();
	String[] accNoArr_value, bankNameArr_value, branchNameArr_value;
	String[] accNoArr_server, bankName_server, branchName_server;

	ArrayList<String> mBankName = new ArrayList<String>();
	ArrayList<String> mBranchName = new ArrayList<String>();
	ArrayList<String> mIfscCode = new ArrayList<String>();

	ArrayList<String> mBankName_member = new ArrayList<String>();
	ArrayList<String> mBranchName_member = new ArrayList<String>();
	ArrayList<String> mAccountNumber_member = new ArrayList<String>();
	List<String> mBankNameWithoutDupList;
	Dialog confirmationDialog;
	public static String mSendToServer_AccNo = null;
	boolean isServiceCall = false;
	private Dialog mProgressDialog;
	boolean isError = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		mBankName = new ArrayList<String>();
		mBranchName = new ArrayList<String>();
		mIfscCode = new ArrayList<String>();

		mBankName.clear();
		mBranchName.clear();
		mIfscCode.clear();

		mBankName_member = new ArrayList<String>();
		mBranchName_member = new ArrayList<String>();
		mAccountNumber_member = new ArrayList<String>();

		mBankName_member.clear();
		mBranchName_member.clear();
		mAccountNumber_member.clear();

		sAccountNumber_Fields.clear();
		sBankName_Fields.clear();
		sBranch_Fields.clear();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_all_transaction, container, false);

		try {

			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashinHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashinHand.setTypeface(LoginActivity.sTypeface);

			mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashatBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashatBank.setTypeface(LoginActivity.sTypeface);

			mHeader = (TextView) rootView.findViewById(R.id.fragmentHeader);
			mHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.mAccountNoUpdation));
			mHeader.setTypeface(LoginActivity.sTypeface);

			mMemberNameLayout = (LinearLayout) rootView.findViewById(R.id.member_name_layout);
			mMemberName = (TextView) rootView.findViewById(R.id.member_name);

			mSize = SelectedGroupsTask.member_Name.size();

			Log.e("Bank Names---BranchName----->>", publicValues.mGetBankBranchValues);

			if (publicValues.mGetBankBranchValues != null) {

				String mMasterArray[] = publicValues.mGetBankBranchValues.split("#");

				for (int i = 0; i < mMasterArray.length; i++) {
					String _indiArray[] = mMasterArray[i].split("~");
					mBankName.add(_indiArray[0]);
					mBranchName.add(_indiArray[1]);
					mIfscCode.add(_indiArray[2]);
				}

				Log.e("Master Array", mMasterArray.length + "");

				BankDetailsProvider.deleteBankDetails();
				for (int i = 0; i < mBankName.size(); i++) {

					BankDetailsProvider.addBankMasterDetails(
							new BankDetails(0, mBankName.get(i), mBranchName.get(i), mIfscCode.get(i)));

				}

				System.out.println("------------Bank Name with dub size  =  " + mBankName.size() + "");

				Set<String> set = new LinkedHashSet<String>(mBankName);

				String[] withoutDupBankNameList = new String[set.size()];
				set.toArray(withoutDupBankNameList);

				mBankNameWithoutDupList = new ArrayList<String>(Arrays.asList(withoutDupBankNameList));// Arrays.asList(withoutDupBankNameList);
				mBankNameWithoutDupList.add("NONE");

				System.out.println("******  Bank Name size without dup  =  " + mBankNameWithoutDupList.size());

			}

			if (publicValues.mGetMemberAccountNumberValues != null) {

				String mMemberMasterArray[] = publicValues.mGetMemberAccountNumberValues.split("#");

				for (int i = 0; i < mMemberMasterArray.length; i++) {
					String mMember_indiArray[] = mMemberMasterArray[i].split("~");

					mBankName_member.add(mMember_indiArray[1]);
					mBranchName_member.add(mMember_indiArray[2]);
					mAccountNumber_member.add(mMember_indiArray[3]);

					if (mMember_indiArray[3].equals("No")) {
						mAccountNumber_member.set(i, "");
					}

					if (mMember_indiArray[1].equals("No")) {
						mBankName_member.set(i, "SELECT A BANK");
					}

					if (mMember_indiArray[2].equals("No")) {
						mBranchName_member.set(i, "SELECT A BRANCH");
					}

				}

			}

			mLeftHeaderTable = (TableLayout) rootView.findViewById(R.id.LeftHeaderTable);
			mRightHeaderTable = (TableLayout) rootView.findViewById(R.id.RightHeaderTable);
			mLeftContentTable = (TableLayout) rootView.findViewById(R.id.LeftContentTable);
			mRightContentTable = (TableLayout) rootView.findViewById(R.id.RightContentTable);

			mHSRightHeader = (CustomHorizontalScrollView) rootView.findViewById(R.id.rightHeaderHScrollView);
			mHSRightContent = (CustomHorizontalScrollView) rootView.findViewById(R.id.rightContentHScrollView);

			mHSRightHeader.setOnScrollChangedListener(new onScrollChangedListener() {

				public void onScrollChanged(int l, int t, int oldl, int oldt) {
					// TODO Auto-generated method stub

					mHSRightContent.scrollTo(l, 0);

				}
			});

			mHSRightContent.setOnScrollChangedListener(new onScrollChangedListener() {
				@Override
				public void onScrollChanged(int l, int t, int oldl, int oldt) {
					// TODO Auto-generated method stub
					mHSRightHeader.scrollTo(l, 0);
				}
			});

			TableRow leftHeaderRow = new TableRow(getActivity());

			TableRow.LayoutParams lHeaderParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);

			TextView mMemberName_Header = new TextView(getActivity());
			mMemberName_Header.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.memberName)));
			mMemberName_Header.setTypeface(LoginActivity.sTypeface);
			mMemberName_Header.setTextColor(Color.WHITE);
			mMemberName_Header.setPadding(10, 5, 10, 5);
			mMemberName_Header.setLayoutParams(lHeaderParams);
			leftHeaderRow.addView(mMemberName_Header);

			mLeftHeaderTable.addView(leftHeaderRow);

			TableRow rightHeaderRow = new TableRow(getActivity());

			TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);
			contentParams.setMargins(10, 0, 10, 0);

			TextView mOutstanding_Header = new TextView(getActivity());
			mOutstanding_Header.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.bankName)));
			mOutstanding_Header.setTypeface(LoginActivity.sTypeface);
			mOutstanding_Header.setTextColor(Color.WHITE);
			mOutstanding_Header.setPadding(10, 5, 10, 5);
			mOutstanding_Header.setGravity(Gravity.LEFT);
			mOutstanding_Header.setLayoutParams(contentParams);
			mOutstanding_Header.setBackgroundResource(R.color.tableHeader);
			rightHeaderRow.addView(mOutstanding_Header);

			TableRow.LayoutParams POLParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);
			POLParams.setMargins(10, 0, 10, 0);

			TextView mPurposeOfLoan_Header = new TextView(getActivity());
			mPurposeOfLoan_Header
					.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.mBranchName)));
			mPurposeOfLoan_Header.setTypeface(LoginActivity.sTypeface);
			mPurposeOfLoan_Header.setTextColor(Color.WHITE);
			mPurposeOfLoan_Header.setGravity(Gravity.RIGHT);
			mPurposeOfLoan_Header.setLayoutParams(POLParams);
			mPurposeOfLoan_Header.setPadding(25, 5, 10, 5);
			mPurposeOfLoan_Header.setBackgroundResource(R.color.tableHeader);
			rightHeaderRow.addView(mPurposeOfLoan_Header);

			TableRow.LayoutParams rHeaderParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);
			rHeaderParams.setMargins(10, 0, 10, 0);

			TextView mPL_Header = new TextView(getActivity());
			mPL_Header.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.mAccountNumber)));
			mPL_Header.setTypeface(LoginActivity.sTypeface);
			mPL_Header.setTextColor(Color.WHITE);
			mPL_Header.setPadding(10, 5, 10, 5);
			mPL_Header.setGravity(Gravity.CENTER);
			mPL_Header.setLayoutParams(rHeaderParams);
			mPL_Header.setBackgroundResource(R.color.tableHeader);
			rightHeaderRow.addView(mPL_Header);
			mRightHeaderTable.addView(rightHeaderRow);

			for (int j = 0; j < mSize; j++) {

				final int pos = j;
				TableRow leftContentRow = new TableRow(getActivity());

				TableRow.LayoutParams leftContentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
						LayoutParams.WRAP_CONTENT, 1f);
				leftContentParams.setMargins(5, 5, 5, 15);

				final TextView memberName_Text = new TextView(getActivity());
				memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
						String.valueOf(SelectedGroupsTask.member_Name.elementAt(j))));
				memberName_Text.setTypeface(LoginActivity.sTypeface);
				memberName_Text.setTextColor(color.black);
				memberName_Text.setPadding(5, 5, 5, 5);
				memberName_Text.setLayoutParams(leftContentParams);
				memberName_Text.setWidth(200);
				memberName_Text.setSingleLine(true);
				memberName_Text.setEllipsize(TextUtils.TruncateAt.END);
				leftContentRow.addView(memberName_Text);

				mLeftContentTable.addView(leftContentRow);

				TableRow rightContentRow = new TableRow(getActivity());

				TableRow.LayoutParams rightContentTextviewParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
						LayoutParams.MATCH_PARENT, 1f);
				rightContentTextviewParams.setMargins(20, 5, 20, 5);
				mBankName_values = new TextView(getActivity());
				mBankName_values.setId(j);
				sBankName_Fields.add(mBankName_values);
				mBankName_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf("SELECT A BANK")));
				mBankName_values.setTypeface(LoginActivity.sTypeface);
				mBankName_values.setTextColor(color.black);
				mBankName_values.setPadding(5, 5, 5, 5);
				mBankName_values.setWidth(230);
				mBankName_values.setLayoutParams(rightContentTextviewParams);
				mBankName_values.setSingleLine(true);
				mBankName_values.setEllipsize(TextUtils.TruncateAt.END);
				mBankName_values.setText(mBankName_member.get(j));
				mBankName_values.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						EShaktiApplication.setAccountNumberBankName(true);
						FragmentManager fm = getActivity().getSupportFragmentManager();

						Dialog_AccountNoUpdationFragment dialog = new Dialog_AccountNoUpdationFragment(getActivity(),
								mBankNameWithoutDupList, pos, 0, mRightContentTable);
						dialog.show(fm, "");
					}

				});

				rightContentRow.addView(mBankName_values);

				TableRow.LayoutParams rightContentTextviewParams1 = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
						LayoutParams.WRAP_CONTENT, 1f);
				rightContentTextviewParams1.setMargins(20, 5, 20, 5);

				mBranchName_values = new TextView(getActivity());
				mBranchName_values.setId(j);
				sBranch_Fields.add(mBranchName_values);
				mBranchName_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf("SELECT A BRANCH")));
				mBranchName_values.setTypeface(LoginActivity.sTypeface);
				mBranchName_values.setTextColor(color.black);
				mBranchName_values.setPadding(5, 5, 5, 5);
				mBranchName_values.setLayoutParams(rightContentTextviewParams1);
				mBranchName_values.setText(mBranchName_member.get(j));
				mBranchName_values.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						EShaktiApplication.setAccountNumberBankName(false);

						View view = mRightContentTable.getChildAt(pos);
						TableRow r = (TableRow) view;

						TextView textView = (TextView) r.getChildAt(0);
						String contentValue_ = textView.getText().toString();
						if (contentValue_.equals("SELECT A BANK") || contentValue_.equals("NONE")) {
							EShaktiApplication.setShg_selected_bankName("");
						} else {
							EShaktiApplication.setShg_selected_bankName(contentValue_);
						}
						Log.e("####  Selected Bank name  =  ", EShaktiApplication.getShg_selected_bankName() + "");
						List<BankDetails> branchList = new ArrayList<BankDetails>();

						branchList = BankDetailsProvider.getBranchMasterDetails();
						List<String> branchNameList = new ArrayList<String>();

						for (int i = 0; i < branchList.size(); i++) {
							System.out.println("-----Branch name   =  " + branchList.get(i).getBranchName().toString());
							branchNameList.add(branchList.get(i).getBranchName().toString());
						}

						branchNameList.add("NONE");

						FragmentManager fm = getActivity().getSupportFragmentManager();

						Dialog_AccountNoUpdationFragment dialog = new Dialog_AccountNoUpdationFragment(getActivity(),
								branchNameList, pos, 1, mRightContentTable);
						dialog.show(fm, "");
					}
				});
				rightContentRow.addView(mBranchName_values);

				TableRow.LayoutParams rightContentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
						LayoutParams.WRAP_CONTENT, 1f);
				rightContentParams.setMargins(10, 5, 10, 5);

				mAccountNumber_values = new EditText(getActivity());
				mAccountNumber_values.setId(j);
				sAccountNumber_Fields.add(mAccountNumber_values);
				mAccountNumber_values.setPadding(5, 5, 5, 5);
				mAccountNumber_values.setFilters(Get_EdiText_Filter.editText_AccNo_filter());
				mAccountNumber_values.setBackgroundResource(R.drawable.edittext_background);
				mAccountNumber_values.setLayoutParams(rightContentParams);
				mAccountNumber_values.setWidth(400);
				mAccountNumber_values.setText(mAccountNumber_member.get(j));

				mAccountNumber_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

					@Override
					public void onFocusChange(View v, boolean hasFocus) {
						// TODO Auto-generated method stub
						if (hasFocus) {
							((EditText) v).setGravity(Gravity.LEFT);

							mMemberNameLayout.setVisibility(View.VISIBLE);
							mMemberName.setText(memberName_Text.getText().toString().trim());
							mMemberName.setTypeface(LoginActivity.sTypeface);
							TextviewUtils.manageBlinkEffect(mMemberName, getActivity());
						} else {
							((EditText) v).setGravity(Gravity.RIGHT);
							mMemberNameLayout.setVisibility(View.GONE);
							mMemberName.setText("");
						}
					}
				});

				rightContentRow.addView(mAccountNumber_values);

				mRightContentTable.addView(rightContentRow);
			}

			mSubmit_Raised_Button = (RaisedButton) rootView.findViewById(R.id.fragment_Submit_button);
			mSubmit_Raised_Button.setText(RegionalConversion.getRegionalConversion(AppStrings.submit));
			mSubmit_Raised_Button.setTypeface(LoginActivity.sTypeface);
			mSubmit_Raised_Button.setOnClickListener(this);

			resizeMemberNameWidth();
			resizeRightSideTable();
			resizeBodyTableRowHeight();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return rootView;
	}

	private void resizeRightSideTable() {
		// TODO Auto-generated method stub
		int rightHeaderCount = (((TableRow) mRightHeaderTable.getChildAt(0)).getChildCount());
		for (int i = 0; i < rightHeaderCount; i++) {
			rightHeaderWidth[i] = viewWidth(((TableRow) mRightHeaderTable.getChildAt(0)).getChildAt(i));
			rightContentWidth[i] = viewWidth(((TableRow) mRightContentTable.getChildAt(0)).getChildAt(i));
		}
		for (int i = 0; i < rightHeaderCount; i++) {
			if (rightHeaderWidth[i] < rightContentWidth[i]) {
				((TableRow) mRightHeaderTable.getChildAt(0)).getChildAt(i)
						.getLayoutParams().width = rightContentWidth[i];
			} else {
				((TableRow) mRightContentTable.getChildAt(0)).getChildAt(i)
						.getLayoutParams().width = rightHeaderWidth[i];
			}
		}
	}

	private void resizeMemberNameWidth() {
		// TODO Auto-generated method stub
		int leftHeadertWidth = viewWidth(mLeftHeaderTable);
		int leftContentWidth = viewWidth(mLeftContentTable);

		if (leftHeadertWidth < leftContentWidth) {
			mLeftHeaderTable.getLayoutParams().width = leftContentWidth;
		} else {
			mLeftContentTable.getLayoutParams().width = leftHeadertWidth;
		}
	}

	private void resizeBodyTableRowHeight() {

		int leftContentTable_ChildCount = mLeftContentTable.getChildCount();

		for (int x = 0; x < leftContentTable_ChildCount; x++) {

			TableRow leftContentTableRow = (TableRow) mLeftContentTable.getChildAt(x);
			TableRow rightContentTableRow = (TableRow) mRightContentTable.getChildAt(x);

			int rowLeftHeight = viewHeight(leftContentTableRow);
			int rowRightHeight = viewHeight(rightContentTableRow);

			TableRow tableRow = rowLeftHeight < rowRightHeight ? leftContentTableRow : rightContentTableRow;
			int finalHeight = rowLeftHeight > rowRightHeight ? rowLeftHeight : rowRightHeight;

			this.matchLayoutHeight(tableRow, finalHeight);
		}

	}

	private void matchLayoutHeight(TableRow tableRow, int height) {

		int tableRowChildCount = tableRow.getChildCount();

		// if a TableRow has only 1 child
		if (tableRow.getChildCount() == 1) {

			View view = tableRow.getChildAt(0);
			TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();
			params.height = height - (params.bottomMargin + params.topMargin);

			return;
		}

		// if a TableRow has more than 1 child
		for (int x = 0; x < tableRowChildCount; x++) {

			View view = tableRow.getChildAt(x);

			TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();

			if (!isTheHeighestLayout(tableRow, x)) {
				params.height = height - (params.bottomMargin + params.topMargin);
				return;
			}
		}

	}

	// check if the view has the highest height in a TableRow
	private boolean isTheHeighestLayout(TableRow tableRow, int layoutPosition) {

		int tableRowChildCount = tableRow.getChildCount();
		int heighestViewPosition = -1;
		int viewHeight = 0;

		for (int x = 0; x < tableRowChildCount; x++) {
			View view = tableRow.getChildAt(x);
			int height = this.viewHeight(view);

			if (viewHeight < height) {
				heighestViewPosition = x;
				viewHeight = height;
			}
		}

		return heighestViewPosition == layoutPosition;
	}

	// read a view's height
	private int viewHeight(View view) {
		view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
		return view.getMeasuredHeight();
	}

	// read a view's width
	private int viewWidth(View view) {
		view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
		return view.getMeasuredWidth();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {
		case R.id.fragment_Submit_button:

			boolean _MasterisValid = true;
			boolean _IsEmpty = true;

			boolean _IsRepeatedAccountNumberNo = false;
			accNoArr_value = new String[sAccountNumber_Fields.size()];
			bankNameArr_value = new String[sAccountNumber_Fields.size()];
			branchNameArr_value = new String[sAccountNumber_Fields.size()];

			accNoArr_server = new String[sAccountNumber_Fields.size()];
			bankName_server = new String[sAccountNumber_Fields.size()];
			branchName_server = new String[sAccountNumber_Fields.size()];

			for (int i = 0; i < SelectedGroupsTask.member_Id.size(); i++) {

				accNoArr_value[i] = sAccountNumber_Fields.get(i).getText().toString();
				accNoArr_server[i] = sAccountNumber_Fields.get(i).getText().toString();

				bankNameArr_value[i] = sBankName_Fields.get(i).getText().toString();
				bankName_server[i] = sBankName_Fields.get(i).getText().toString();

				branchNameArr_value[i] = sBranch_Fields.get(i).getText().toString();
				branchName_server[i] = sBranch_Fields.get(i).getText().toString();

				if (bankNameArr_value[i].equals("SELECT A BANK") || bankNameArr_value[i].equals("NONE")) {
					bankNameArr_value[i] = "No";
				}

				if (branchNameArr_value[i].equals("SELECT A BRANCH") || branchNameArr_value[i].equals("NONE")) {
					branchNameArr_value[i] = "No";
				}

				if (accNoArr_value[i].equals("")) {
					accNoArr_value[i] = "No";
				}

			}

			mSendToServer_AccNo = Reset.reset(mSendToServer_AccNo);

			for (int i = 0; i < SelectedGroupsTask.member_Id.size(); i++) {
				boolean isValid = false;

				if (!sAccountNumber_Fields.get(i).getText().toString().isEmpty()) {
					_IsEmpty = false;
				}

				isValid = AccountNumberValidationUtils.validateAccountNumber(accNoArr_server[i]);

				Log.e("Valid Position------------------" + i + "     Pos", isValid + "");
				if (!isValid) {
					sAccountNumber_Fields.get(i)
							.setError(RegionalConversion.getRegionalConversion(AppStrings.mInvalidAccountNo));
					_MasterisValid = false;
				}
			}
			if (_MasterisValid && !_IsEmpty) {

				for (int i = 0; i < SelectedGroupsTask.member_Id.size(); i++) {

					if (mAccountNumber_member.get(i).equals("")) {
						mAccountNumber_member.set(i, "No");
					}

					if (mBankName_member.get(i).equals("SELECT A BANK") || mBankName_member.get(i).equals("NONE")) {
						mBankName_member.set(i, "No");
					}

					if (mBranchName_member.get(i).equals("SELECT A BRANCH")
							|| mBranchName_member.get(i).equals("NONE")) {
						mBranchName_member.set(i, "No");
					}

					if (bankName_server[i].equals("SELECT A BANK") || bankName_server[i].equals("NONE")) {
						bankName_server[i] = "";
					}

					if (branchName_server[i].equals("SELECT A BRANCH") || branchName_server[i].equals("NONE")) {
						branchName_server[i] = "";
					}

					if (!bankName_server[i].equals("") && branchName_server[i].equals("")
							&& accNoArr_server[i].equals("")) {
						// isError = true;//

					} else if (bankName_server[i].equals("") && !branchName_server[i].equals("")
							&& accNoArr_server[i].equals("")) {
						// isError = true;//

					} else if (bankName_server[i].equals("") && branchName_server[i].equals("")
							&& !accNoArr_server[i].equals("")) {
						isError = true;

					} else if (!bankName_server[i].equals("") && !branchName_server[i].equals("")
							&& accNoArr_server[i].equals("")) {
						// isError = true;//

					} else if (bankName_server[i].equals("") && !branchName_server[i].equals("")
							&& !accNoArr_server[i].equals("")) {
						isError = true;

					} else if (!bankName_server[i].equals("") && branchName_server[i].equals("")
							&& !accNoArr_server[i].equals("")) {
						isError = true;

					}

				}

				Log.e("SendToServer value  = ", mSendToServer_AccNo + "");

				for (int j = 0; j < SelectedGroupsTask.member_Id.size(); j++) {
					String _AccountNumber = accNoArr_server[j];
					String _BranchName = branchName_server[j];
					String _BankName = bankName_server[j];
					for (int k = 0; k < SelectedGroupsTask.member_Id.size(); k++) {
						if (j != k) {
							if (!accNoArr_server[k].isEmpty()) {
								if (accNoArr_server[k].equals(_AccountNumber)
										&& branchName_server[k].equals(_BranchName)
										&& bankName_server[k].equals(_BankName)) {
									_IsRepeatedAccountNumberNo = true;
								}
							}

						}

					}

				}

				System.out.println("-------------------- Flag  =  " + isError + "");
				if (!isError) {
					if (!_IsRepeatedAccountNumberNo) {
						for (int i = 0; i < SelectedGroupsTask.member_Id.size(); i++) {
							mSendToServer_AccNo = mSendToServer_AccNo
									+ String.valueOf(SelectedGroupsTask.member_Id.elementAt(i)) + "~"
									+ mBankName_member.get(i).toString() + "~" + mBranchName_member.get(i).toString()
									+ "~" + mAccountNumber_member.get(i).toString() + "~" + bankName_server[i] + "~"
									+ branchName_server[i] + "~" + accNoArr_server[i] + "#";

						}

						callConfirmationDialog();
					} else {
						TastyToast.makeText(getActivity(), "ACCOUNT NUMBERS REPEATED", TastyToast.LENGTH_SHORT,
								TastyToast.ERROR);
					}

				} else {

					isError = false;
					TastyToast.makeText(getActivity(), AppStrings.mCheckBankAndBranchNameSelected,
							TastyToast.LENGTH_SHORT, TastyToast.ERROR);
				}
			} else {

				TastyToast.makeText(getActivity(), AppStrings.mCheckAccountNumber, TastyToast.LENGTH_SHORT,
						TastyToast.ERROR);

			}

			break;

		case R.id.fragment_Edit_button:

			mSendToServer_AccNo = Reset.reset(mSendToServer_AccNo);
			mSubmit_Raised_Button.setClickable(true);
			confirmationDialog.dismiss();
			isServiceCall = false;
			break;

		case R.id.fragment_Ok_button:

			if (ConnectionUtils.isNetworkAvailable(getActivity())) {
				new Get_Update_MemberAccountNumber_Webservices(this).execute();
			} else {
				// Do offline Stuffs
				TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			}
			break;
		default:
			break;
		}
	}

	private void callConfirmationDialog() {
		// TODO Auto-generated method stub

		confirmationDialog = new Dialog(getActivity());

		LayoutInflater inflater = getActivity().getLayoutInflater();
		View dialogView = inflater.inflate(R.layout.dialog_confirmation, null);
		dialogView.setLayoutParams(
				new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

		TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
		confirmationHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.confirmation));
		confirmationHeader.setTypeface(LoginActivity.sTypeface);

		TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

		TableRow header_row = new TableRow(getActivity());

		TableRow.LayoutParams headerParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT, 1f);
		headerParams.setMargins(10, 5, 10, 5);

		TextView bankName_header = new TextView(getActivity());
		bankName_header.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.bankName)));
		bankName_header.setTypeface(LoginActivity.sTypeface);
		bankName_header.setTextColor(color.black);
		bankName_header.setPadding(5, 5, 5, 5);
		bankName_header.setLayoutParams(headerParams);
		header_row.addView(bankName_header);

		TextView branchName_header = new TextView(getActivity());
		branchName_header.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mBranchName)));
		branchName_header.setTypeface(LoginActivity.sTypeface);
		branchName_header.setTextColor(color.black);
		branchName_header.setPadding(5, 5, 5, 5);
		branchName_header.setLayoutParams(headerParams);
		header_row.addView(branchName_header);

		TextView accNo_header = new TextView(getActivity());
		accNo_header.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mAccountNumber)));
		accNo_header.setTypeface(LoginActivity.sTypeface);
		accNo_header.setTextColor(color.black);
		accNo_header.setPadding(5, 5, 5, 5);
		accNo_header.setLayoutParams(headerParams);
		header_row.addView(accNo_header);

		confirmationTable.addView(header_row,
				new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

		for (int i = 0; i < SelectedGroupsTask.member_Name.size(); i++) {

			TableRow indv_SavingsRow = new TableRow(getActivity());

			@SuppressWarnings("deprecation")
			TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);
			contentParams.setMargins(10, 5, 10, 5);

			TextView bankName_Text = new TextView(getActivity());
			bankName_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(bankNameArr_value[i])));
			bankName_Text.setTypeface(LoginActivity.sTypeface);
			bankName_Text.setTextColor(color.black);
			bankName_Text.setPadding(5, 5, 5, 5);
			bankName_Text.setLayoutParams(contentParams);
			indv_SavingsRow.addView(bankName_Text);

			TextView branchName_Text = new TextView(getActivity());
			branchName_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(branchNameArr_value[i])));
			branchName_Text.setTextColor(color.black);
			branchName_Text.setPadding(5, 5, 5, 5);
			branchName_Text.setGravity(Gravity.RIGHT);
			branchName_Text.setLayoutParams(contentParams);
			indv_SavingsRow.addView(branchName_Text);

			TextView accNo_Text = new TextView(getActivity());
			accNo_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(accNoArr_value[i])));
			accNo_Text.setTextColor(color.black);
			accNo_Text.setPadding(5, 5, 5, 5);
			accNo_Text.setGravity(Gravity.RIGHT);
			accNo_Text.setLayoutParams(contentParams);
			indv_SavingsRow.addView(accNo_Text);

			confirmationTable.addView(indv_SavingsRow,
					new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

		}

		mEdit_RaisedButton = (RaisedButton) dialogView.findViewById(R.id.fragment_Edit_button);
		mEdit_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.edit));
		mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
		mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
		mEdit_RaisedButton.setOnClickListener(this);

		mOk_RaisedButton = (RaisedButton) dialogView.findViewById(R.id.fragment_Ok_button);
		mOk_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.yes));
		mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
		mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
		mOk_RaisedButton.setOnClickListener(this);

		confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		confirmationDialog.setCanceledOnTouchOutside(false);
		confirmationDialog.setContentView(dialogView);
		confirmationDialog.setCancelable(true);
		confirmationDialog.show();

		MarginLayoutParams margin = (MarginLayoutParams) dialogView.getLayoutParams();
		margin.leftMargin = 10;
		margin.rightMargin = 10;
		margin.topMargin = 10;
		margin.bottomMargin = 10;
		margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);

	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub
		mProgressDialog = AppDialogUtils.createProgressDialog(getActivity());
		mProgressDialog.show();
	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub

		try {

			if (mProgressDialog != null) {
				mProgressDialog.dismiss();

				if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {
					getActivity().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub

							if (!result.equals("FAIL")) {
								isServiceCall = false;
								TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg,
										TastyToast.LENGTH_SHORT, TastyToast.ERROR);
								Constants.NETWORKCOMMONFLAG = "SUCESS";
							}

						}
					});
				} else {
					confirmationDialog.dismiss();
					TastyToast.makeText(getActivity(), AppStrings.mMemberAccNoUpdateSuccessAlert,
							TastyToast.LENGTH_SHORT, TastyToast.SUCCESS);

					MainFragment_Dashboard dashboard = new MainFragment_Dashboard();
					setFragment(dashboard);
				}
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub
		Log.e("Current Class Name!!!!!!!!!!!!!!", fragment.getClass().getName());
		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

	}

}
