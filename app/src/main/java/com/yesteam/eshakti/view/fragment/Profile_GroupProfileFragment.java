package com.yesteam.eshakti.view.fragment;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.Get_GroupProfileTask;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Profile_GroupProfileFragment extends Fragment {

	private TextView mGroupName, mCashInHand, mCashAtBank, mHeadertext;
	String responseArr[];

	public Profile_GroupProfileFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_GROUP_PROFILE;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_agent_group_profile, container, false);

		MainFragment_Dashboard.isBackpressed = false;

		try {

			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashInHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashInHand.setTypeface(LoginActivity.sTypeface);

			mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashAtBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashAtBank.setTypeface(LoginActivity.sTypeface);

			mHeadertext = (TextView) rootView.findViewById(R.id.fragment_agent_group_profile_headertext);
			mHeadertext.setText(RegionalConversion.getRegionalConversion(AppStrings.groupProfile));
			mHeadertext.setTypeface(LoginActivity.sTypeface);

			responseArr = Get_GroupProfileTask.sGetGroupProfile_Response.split("~");
			System.out.println("Length of response :" + responseArr.length);

			Log.d("Group Profile : ", Get_GroupProfileTask.sGetGroupProfile_Response);

			TableLayout tableLayout = (TableLayout) rootView
					.findViewById(R.id.fragment_agent_group_profile_tableLayout);

			for (int i = 0; i < responseArr.length; i = i + 2) {

				TableRow indv_row = new TableRow(getActivity());
				TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
						LayoutParams.WRAP_CONTENT);
				contentParams.setMargins(10, 5, 10, 5);

				TextView agent_left_details = new TextView(getActivity());
				agent_left_details.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(responseArr[i])));
				agent_left_details.setTypeface(LoginActivity.sTypeface);
				agent_left_details.setTextColor(color.black);
				agent_left_details.setPadding(5, 5, 5, 5);
				agent_left_details.setLayoutParams(contentParams);
				indv_row.addView(agent_left_details);

				TextView agent_right_details = new TextView(getActivity());
				agent_right_details
						.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(responseArr[i + 1])));
				agent_right_details.setTypeface(LoginActivity.sTypeface);
				agent_right_details.setTextColor(color.black);
				agent_right_details.setPadding(5, 5, 5, 5);
				agent_right_details.setLayoutParams(contentParams);
				indv_row.addView(agent_right_details);

				tableLayout
						.addView(indv_row);/*
											 * , new TableLayout.LayoutParams(
											 * LayoutParams.MATCH_PARENT,
											 * LayoutParams.WRAP_CONTENT));
											 */

			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return rootView;
	}

}
