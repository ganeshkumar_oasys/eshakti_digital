package com.yesteam.eshakti.view.fragment;


import com.google.zxing.client.android.IntentIntegrator;
import com.google.zxing.client.android.IntentResult;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.view.activity.MainActivity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class Profile_AadhaarCardFragment extends Fragment {

    String mQRcodeValues;
    Dialog dialog;

    public Profile_AadhaarCardFragment() {
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        // TODO Auto-generated method stub
        super.onSaveInstanceState(outState);
        EShaktiApplication.setOnSavedFragment(true);
        Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_AADHAAR_UPLOAD;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        View rootView = inflater.inflate(R.layout.fragment_aadhaarcard, container, false);
        MainActivity.isQRCode_Back_Pressed = true;
        MainFragment_Dashboard.isBackpressed = false;
        // Constants.FRAG_BACKPRESS_CONSTANT = Constants.FRAG_INSTANCE_SAVINGS;

        try {
            IntentIntegrator.forSupportFragment(this).initiateScan();


        } catch (Exception e) {
            e.printStackTrace();
        }

        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onStop() {
        // TODO Auto-generated method stub
        super.onStop();


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        if (result != null) {
            if (result.getContents() == null) {

                Profile_MemberPhoto_AadharInfoMenuFragment fragment = new Profile_MemberPhoto_AadharInfoMenuFragment();

                setFragment(fragment);
            } else {
                Log.d("MainActivity", "Scanned");
                Log.e("Scanned Content", result.getContents() + "");

                mQRcodeValues = result.getContents();
                Log.e("QR Code Values----->>>>", mQRcodeValues);
                Profile_AadhaarCardDialogFragment fragment = new Profile_AadhaarCardDialogFragment();
                Bundle bundle = new Bundle();
                bundle.putString("QRVALUES", mQRcodeValues);
                fragment.setArguments(bundle);
                setFragment(fragment);
            }
        } else {
            // This is important, otherwise the result will not be passed to the
            // fragment
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @SuppressLint("LongLogTag")
    private void setFragment(Fragment fragment) {
        // TODO Auto-generated method stub
        Log.e("Current Class Name!!!!!!!!!!!!!!", fragment.getClass().getName());
        getActivity().getSupportFragmentManager().beginTransaction()

                .replace(R.id.frame, fragment)
                .setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
                .addToBackStack(fragment.getClass().getName()).commit();

    }

}
