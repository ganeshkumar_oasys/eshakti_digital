package com.yesteam.eshakti.view.fragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.squareup.otto.Subscribe;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;

import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.adapter.CustomItemAdapter;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.TransactionOffConstants;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;
import com.yesteam.eshakti.model.RowItem;
import com.yesteam.eshakti.sqlite.database.GroupProvider;
import com.yesteam.eshakti.sqlite.database.response.GetSingleTransResponse;
import com.yesteam.eshakti.sqlite.database.response.GroupMasUpdateResponse;
import com.yesteam.eshakti.sqlite.database.response.LoanAccountDataUpdateResponse;
import com.yesteam.eshakti.sqlite.database.response.TransactionResponse;
import com.yesteam.eshakti.sqlite.database.response.TransactionUpdateResponse;
import com.yesteam.eshakti.sqlite.db.model.GetTransactionSingle;
import com.yesteam.eshakti.sqlite.db.model.GetTransactionSinglevalues;
import com.yesteam.eshakti.sqlite.db.model.GroupDetailsUpdate;
import com.yesteam.eshakti.sqlite.db.model.GroupResponseUpdate;
import com.yesteam.eshakti.sqlite.db.model.GroupTempDBLastTransDate;
import com.yesteam.eshakti.sqlite.db.model.LoanAccountUpdate;
import com.yesteam.eshakti.sqlite.db.model.Transaction;
import com.yesteam.eshakti.sqlite.db.model.TransactionUpdate;
import com.yesteam.eshakti.sqlite.db.transactions.TransactionManager.DataType;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.utils.GetOfflineTransactionValues;
import com.yesteam.eshakti.utils.GetResponseInfo;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.utils.Get_Offline_MobileDate;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.utils.Put_DB_GroupNameDetail_Response;
import com.yesteam.eshakti.utils.Put_DB_GroupResponse;
import com.yesteam.eshakti.utils.Reset;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.utils.UpdateCalendarMinMaxDateUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.views.MaterialSpinner;
import com.yesteam.eshakti.views.RaisedButton;
import com.yesteam.eshakti.webservices.Get_GL_Fixed_Deposit_WithdrawalWebservice;
import com.yesteam.eshakti.webservices.Get_LastTransactionID;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Transaction_LoanAccountFragment extends Fragment implements TaskListener, OnClickListener {
	private TextView mGroupName, mCashInHand, mCashAtBank;
	TextView mHeader, mLoanaccFixeddeposit, mLoanaccFixedDepositeText, mLoanaccWithdrawText, mLoanaccExpensesText;
	RadioButton mCashRadio, mBankRadio;
	EditText mWithdrawal, mExpenses;
	MaterialSpinner materialSpinner_Bank;
	RaisedButton mSubmitButton;
	View rootView;
	CustomItemAdapter bankNameAdapter;

	private List<RowItem> stateNameItems;
	public static String mWithdrawalValue, mExpensesValue, mSelectedTypeValue;
	private Dialog mProgressDilaog;
	public static String mBankNameValue = null;
	public static String selectedItemBank, selectedType;
	private Dialog confirmationDialog;
	private Button mEdit_RaisedButton, mOk_RaisedButton;
	String mLastTrDate = null, mLastTr_ID = null;

	ArrayList<String> mBanknames_Array = new ArrayList<String>();
	ArrayList<String> mBanknamesId_Array = new ArrayList<String>();

	ArrayList<String> mEngSendtoServerBank_Array = new ArrayList<String>();
	ArrayList<String> mEngSendtoServerBankId_Array = new ArrayList<String>();
	boolean isGetTrid = false;

	ArrayList<String> mBankName = new ArrayList<String>();
	ArrayList<String> mBankNameSendtoServer = new ArrayList<String>();
	ArrayList<String> mLoanId = new ArrayList<String>();
	ArrayList<String> mLoanType = new ArrayList<String>();
	ArrayList<String> mFixedDeposit = new ArrayList<String>();
	boolean isServiceCall = false;

	public Transaction_LoanAccountFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		SBus.INST.register(this);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_LOANACCWITHDRAWAL;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		try {
			mWithdrawalValue = Reset.reset(mWithdrawalValue);
			mExpensesValue = Reset.reset(mExpensesValue);
			mBankNameValue = Reset.reset(mBankNameValue);
			mSelectedTypeValue = Reset.reset(mSelectedTypeValue);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		rootView = inflater.inflate(R.layout.fragment_loanaccount, container, false);

		MainFragment_Dashboard.isBackpressed = false;
		Constants.FRAG_BACKPRESS_CONSTANT = Constants.FRAG_INSTANCE_LOANACCWITHDRAWAL;

		try {
			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashInHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashInHand.setTypeface(LoginActivity.sTypeface);

			mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashAtBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashAtBank.setTypeface(LoginActivity.sTypeface);
			init();
			RadioGroup radioGroup = (RadioGroup) rootView.findViewById(R.id.radio);
			radioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(RadioGroup group, int checkedId) {
					// checkedId is the RadioButton selected
					switch (checkedId) {
					case R.id.radioloanaccCash:
						materialSpinner_Bank.setVisibility(View.GONE);
						selectedType = "CASH";
						break;

					case R.id.radioLoanaccBank:
						selectedType = "BANK";
						materialSpinner_Bank.setVisibility(View.VISIBLE);
						break;
					}
				}
			});

			for (int i = 0; i < SelectedGroupsTask.sBankNames.size(); i++) {
				mBanknames_Array.add(SelectedGroupsTask.sBankNames.elementAt(i).toString());
				mBanknamesId_Array.add(String.valueOf(i));
			}

			for (int i = 0; i < SelectedGroupsTask.sEngBankNames.size(); i++) {
				mEngSendtoServerBank_Array.add(SelectedGroupsTask.sEngBankNames.elementAt(i).toString());
				mEngSendtoServerBankId_Array.add(String.valueOf(i));
			}

			materialSpinner_Bank.setBaseColor(color.grey_400);

			materialSpinner_Bank.setFloatingLabelText(AppStrings.mLoanaccSpinnerFloating);

			materialSpinner_Bank.setPaddingSafe(10, 0, 10, 0);

			mHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.mLoanaccHeader));
			mLoanaccFixedDepositeText.setText(EShaktiApplication.getLoanaccBankName());
			mLoanaccFixedDepositeText.setTypeface(LoginActivity.sTypeface);
			mLoanaccFixeddeposit.setText(EShaktiApplication.getLoanaccFixedDeposit());
			mHeader.setTypeface(LoginActivity.sTypeface);
			mLoanaccFixeddeposit.setTypeface(LoginActivity.sTypeface);
			mLoanaccWithdrawText.setText(AppStrings.mLoanaccWithdrawal);
			mLoanaccExpensesText.setText(AppStrings.mLoanaccExapenses);

			mWithdrawal.setTypeface(LoginActivity.sTypeface);
			mExpenses.setTypeface(LoginActivity.sTypeface);
			mCashRadio.setTypeface(LoginActivity.sTypeface);
			mBankRadio.setTypeface(LoginActivity.sTypeface);
			mSubmitButton.setTypeface(LoginActivity.sTypeface);
			mLoanaccWithdrawText.setTypeface(LoginActivity.sTypeface);
			mLoanaccExpensesText.setTypeface(LoginActivity.sTypeface);
			mSubmitButton.setText(RegionalConversion.getRegionalConversion(AppStrings.submit));

			// mWithdrawal.setHint(AppStrings.mLoanaccWithdrawal);
			// mExpenses.setHint(AppStrings.mLoanaccExapenses);
			final String[] bankNames = new String[SelectedGroupsTask.sBankNames.size() + 1];

			final String[] bankNames_BankID = new String[SelectedGroupsTask.sEngBankNames.size() + 1];

			bankNames[0] = String.valueOf("Select Bank Name");
			for (int i = 0; i < SelectedGroupsTask.sBankNames.size(); i++) {
				bankNames[i + 1] = SelectedGroupsTask.sBankNames.elementAt(i).toString();
			}

			bankNames_BankID[0] = String.valueOf("Select Bank Name");
			for (int i = 0; i < SelectedGroupsTask.sEngBankNames.size(); i++) {
				bankNames_BankID[i + 1] = SelectedGroupsTask.sEngBankNames.elementAt(i).toString();
			}

			int size = bankNames.length;

			stateNameItems = new ArrayList<RowItem>();
			for (int i = 0; i < size; i++) {
				RowItem rowItem = new RowItem(bankNames[i]);// SelectedGroupsTask.sBankNames.elementAt(i).toString());
				stateNameItems.add(rowItem);
			}
			bankNameAdapter = new CustomItemAdapter(getActivity(), stateNameItems);
			materialSpinner_Bank.setAdapter(bankNameAdapter);

			materialSpinner_Bank.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
					// TODO Auto-generated method stub

					if (position == 0) {
						selectedItemBank = bankNames_BankID[0];
						mBankNameValue = "0";

					} else {
						selectedItemBank = bankNames_BankID[position];
						System.out.println("SELECTED BANK NAME : " + selectedItemBank);
						mBankNameValue = selectedItemBank;
						String mBankname = null;
						for (int i = 0; i < mBanknames_Array.size(); i++) {
							if (selectedItemBank.equals(mEngSendtoServerBank_Array.get(i))) {
								mBankname = mEngSendtoServerBank_Array.get(i);
							}
						}

						mBankNameValue = mBankname;

					}
					Log.e("Selected Bank Name value", mBankNameValue);

				}

				@Override
				public void onNothingSelected(AdapterView<?> parent) {
					// TODO Auto-generated method stub

				}
			});
			mSubmitButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					mWithdrawalValue = Reset.reset(mWithdrawalValue);
					mExpensesValue = Reset.reset(mExpensesValue);
					
					mWithdrawalValue = mWithdrawal.getText().toString().trim();
					mExpensesValue = mExpenses.getText().toString().trim();
					mSelectedTypeValue = "";
					int bankBalance = (int) Double.parseDouble(EShaktiApplication.getLoanaccFixedDeposit());
					if (mBankRadio.isChecked() || mCashRadio.isChecked()) {

						if (!mWithdrawalValue.isEmpty() && !mExpensesValue.isEmpty()) {
							if (mBankRadio.isChecked()) {
								mSelectedTypeValue = mBankRadio.getText().toString();
								if (!mBankNameValue.equals("0") && mBankNameValue != null) {

									if (Integer.parseInt(mWithdrawalValue) <= bankBalance) {

										onShowConfirmationDialog();
									} else {
										TastyToast.makeText(getActivity(), AppStrings.mLoanAccBankAmountAlert,
												TastyToast.LENGTH_SHORT, TastyToast.WARNING);
									}

								} else {
									TastyToast.makeText(getActivity(), AppStrings.mLoanaccBankNullToast,
											TastyToast.LENGTH_SHORT, TastyToast.WARNING);

								}
							} else {
								mSelectedTypeValue = mCashRadio.getText().toString();

								selectedItemBank = "-";

								if (Integer.parseInt(mWithdrawalValue) <= bankBalance) {

									onShowConfirmationDialog();
								} else {
									TastyToast.makeText(getActivity(), AppStrings.mLoanAccBankAmountAlert,
											TastyToast.LENGTH_SHORT, TastyToast.WARNING);
								}

							}

						} else {

							TastyToast.makeText(getActivity(), AppStrings.mLoanaccNullToast, TastyToast.LENGTH_SHORT,
									TastyToast.WARNING);
						}
					} else {
						TastyToast.makeText(getActivity(), AppStrings.mLoanaccCash_BankToast, TastyToast.LENGTH_SHORT,
								TastyToast.WARNING);
					}
				}

			});

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return rootView;
	}

	private void onShowConfirmationDialog() {
		// TODO Auto-generated method stub
		confirmationDialog = new Dialog(getActivity());

		LayoutInflater inflater = getActivity().getLayoutInflater();
		View dialogView = inflater.inflate(R.layout.dialog_confirmation, null);
		dialogView.setLayoutParams(
				new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

		TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
		confirmationHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.confirmation));
		confirmationHeader.setTypeface(LoginActivity.sTypeface);

		TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

		TableRow typeRow = new TableRow(getActivity());

		@SuppressWarnings("deprecation")
		TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT, 1f);
		contentParams.setMargins(10, 5, 10, 5);

		TextView memberName_Text = new TextView(getActivity());
		memberName_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mLoanAccType)));
		memberName_Text.setTypeface(LoginActivity.sTypeface);
		memberName_Text.setTextColor(color.white);
		memberName_Text.setPadding(5, 5, 5, 5);
		memberName_Text.setLayoutParams(contentParams);
		typeRow.addView(memberName_Text);

		TextView confirm_values = new TextView(getActivity());
		confirm_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(mSelectedTypeValue)));
		confirm_values.setTextColor(color.white);
		confirm_values.setPadding(5, 5, 5, 5);
		confirm_values.setGravity(Gravity.RIGHT);
		confirm_values.setLayoutParams(contentParams);
		typeRow.addView(confirm_values);

		confirmationTable.addView(typeRow,
				new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

		if (mBankRadio.isChecked()) {
			TableRow bankNameRow = new TableRow(getActivity());

			@SuppressWarnings("deprecation")
			TableRow.LayoutParams bankNameParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);
			bankNameParams.setMargins(10, 5, 10, 5);

			TextView bankName_Text = new TextView(getActivity());
			bankName_Text.setText(
					GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mLoanaccSpinnerFloating)));
			bankName_Text.setTypeface(LoginActivity.sTypeface);
			bankName_Text.setTextColor(color.white);
			bankName_Text.setPadding(5, 5, 5, 5);
			bankName_Text.setLayoutParams(bankNameParams);
			bankNameRow.addView(bankName_Text);

			TextView bankName_values = new TextView(getActivity());
			bankName_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(mBankNameValue)));
			bankName_values.setTextColor(color.white);
			bankName_values.setPadding(5, 5, 5, 5);
			bankName_values.setGravity(Gravity.RIGHT);
			bankName_values.setLayoutParams(bankNameParams);
			bankNameRow.addView(bankName_values);

			confirmationTable.addView(bankNameRow,
					new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

		}

		TableRow withdrawRow = new TableRow(getActivity());

		@SuppressWarnings("deprecation")
		TableRow.LayoutParams withdrawParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT, 1f);
		withdrawParams.setMargins(10, 5, 10, 5);

		TextView withdraw = new TextView(getActivity());
		withdraw.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mLoanaccWithdrawal)));
		withdraw.setTypeface(LoginActivity.sTypeface);
		withdraw.setTextColor(color.white);
		withdraw.setPadding(5, 5, 5, 5);
		withdraw.setLayoutParams(withdrawParams);
		withdrawRow.addView(withdraw);

		TextView withdraw_values = new TextView(getActivity());
		withdraw_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(mWithdrawalValue)));
		withdraw_values.setTextColor(color.white);
		withdraw_values.setPadding(5, 5, 5, 5);
		withdraw_values.setGravity(Gravity.RIGHT);
		withdraw_values.setLayoutParams(withdrawParams);
		withdrawRow.addView(withdraw_values);

		confirmationTable.addView(withdrawRow,
				new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

		TableRow expenseRow = new TableRow(getActivity());

		@SuppressWarnings("deprecation")
		TableRow.LayoutParams expensesParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT, 1f);
		expensesParams.setMargins(10, 5, 10, 5);

		TextView expense = new TextView(getActivity());
		expense.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mLoanaccExapenses)));
		expense.setTypeface(LoginActivity.sTypeface);
		expense.setTextColor(color.white);
		expense.setPadding(5, 5, 5, 5);
		expense.setLayoutParams(expensesParams);
		expenseRow.addView(expense);

		TextView expense_values = new TextView(getActivity());
		expense_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(mExpensesValue)));
		expense_values.setTextColor(color.white);
		expense_values.setPadding(5, 5, 5, 5);
		expense_values.setGravity(Gravity.RIGHT);
		expense_values.setLayoutParams(expensesParams);
		expenseRow.addView(expense_values);

		confirmationTable.addView(expenseRow,
				new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

		mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit_button);
		mEdit_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.edit));
		mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
		mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
																// 205,
																// 0));
		mEdit_RaisedButton.setOnClickListener(this);

		mOk_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Ok_button);
		mOk_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.yes));
		mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
		mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
		mOk_RaisedButton.setOnClickListener(this);

		confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		confirmationDialog.setCanceledOnTouchOutside(false);
		confirmationDialog.setContentView(dialogView);
		confirmationDialog.setCancelable(true);
		confirmationDialog.show();

		MarginLayoutParams margin = (MarginLayoutParams) dialogView.getLayoutParams();
		margin.leftMargin = 10;
		margin.rightMargin = 10;
		margin.topMargin = 10;
		margin.bottomMargin = 10;
		margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);

	}

	private void init() {
		// TODO Auto-generated method stub
		mHeader = (TextView) rootView.findViewById(R.id.loanaccheader);
		mLoanaccFixeddeposit = (TextView) rootView.findViewById(R.id.loanaccfixeddeposit);
		mSubmitButton = (RaisedButton) rootView.findViewById(R.id.loanacc_submit);
		materialSpinner_Bank = (MaterialSpinner) rootView.findViewById(R.id.loanaccbankspinner);
		mWithdrawal = (EditText) rootView.findViewById(R.id.loanaccwithdrawal);
		mExpenses = (EditText) rootView.findViewById(R.id.loanaccexpenses);
		mCashRadio = (RadioButton) rootView.findViewById(R.id.radioloanaccCash);
		mBankRadio = (RadioButton) rootView.findViewById(R.id.radioLoanaccBank);

		mCashRadio.setText(AppStrings.mLoanaccCash);
		mBankRadio.setText(AppStrings.mLoanaccBank);

		mLoanaccFixedDepositeText = (TextView) rootView.findViewById(R.id.loanaccfixeddepositText);
		mLoanaccWithdrawText = (TextView) rootView.findViewById(R.id.loanaccWithdrawalTextView);
		mLoanaccExpensesText = (TextView) rootView.findViewById(R.id.loanaccexpenseslTextView);

	}

	@Subscribe
	public void OnGetSingleTransactionLoanWithdrawal(final GetSingleTransResponse getSingleTransResponse) {
		switch (getSingleTransResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), getSingleTransResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), getSingleTransResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			String mUniqueId = GetTransactionSinglevalues.getUniqueId();
			String mTrainingValues_Offline = GetTransactionSinglevalues.getLoanwithdrawal();

			String mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
			String mMobileDate = Get_Offline_MobileDate.getOffline_MobileDate(getActivity());
			String mTransaction_UniqueId = PrefUtils.getOfflineUniqueID();

			String Send_to_server_values_Offline = mWithdrawalValue + "~" + mExpensesValue + "~"
					+ EShaktiApplication.getLoanaccLoanId() + "~" + EShaktiApplication.getLoanaccLoanType() + "#"
					+ selectedType + "#" + EShaktiApplication.getLoanaccBankNameSendtoServer() + "#" + mBankNameValue;

			mLastTrDate = DatePickerDialog.sDashboardDate;
			mLastTr_ID = SelectedGroupsTask.sTr_ID.toString();
			String mCurrentTransDate = null;

			if (publicValues.mOffline_Trans_CurrentDate != null) {
				mCurrentTransDate = publicValues.mOffline_Trans_CurrentDate;
			}
			String mLoanAccWithdrawalvalues = Send_to_server_values_Offline + "#" + mTrasactiondate + "#" + mMobileDate;

			if (mUniqueId == null && mTrainingValues_Offline == null) {

				if (mTrasactiondate != null && mMobileDate != null && mTransaction_UniqueId != null
						&& mCurrentTransDate != null) {
					EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.TRANSACTIONADD,
							new Transaction(0, PrefUtils.getUsernameKey(), SelectedGroupsTask.UserName,
									SelectedGroupsTask.Ngo_Id, SelectedGroupsTask.Group_Id, mTransaction_UniqueId,
									mLastTr_ID, mCurrentTransDate, null, null, null, null, null, null, null, null, null,
									null, null, null, null, null, null, null, null, null, mLoanAccWithdrawalvalues,
									null, null, null));
				} else {

					TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT, TastyToast.ERROR);
				}

			} else if (mUniqueId.equalsIgnoreCase(PrefUtils.getOfflineUniqueID()) && mTrainingValues_Offline == null) {
				EShaktiApplication.setSetTransValues(TransactionOffConstants.TRANS_LOANWITHD);

				EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.TRANS_VALUES_UPDATE,
						new TransactionUpdate(mUniqueId, mLoanAccWithdrawalvalues));

			} else if (mTrainingValues_Offline != null) {
				confirmationDialog.dismiss();
				TastyToast.makeText(getActivity(), AppStrings.offlineDataAvailable, TastyToast.LENGTH_SHORT,
						TastyToast.WARNING);

				DatePickerDialog.sDashboardDate = SelectedGroupsTask.sLastTransactionDate_Response;
				MainFragment_Dashboard fragment = new MainFragment_Dashboard();
				setFragment(fragment);
			}
			break;
		}
	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub

		if (mProgressDilaog != null) {
			mProgressDilaog.dismiss();
			if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {
				getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub

						confirmationDialog.dismiss();

						if (!result.equals("FAIL")) {
							isServiceCall = false;
							TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg,
									TastyToast.LENGTH_SHORT, TastyToast.ERROR);
							Constants.NETWORKCOMMONFLAG = "SUCCESS";
						}

					}
				});

			} else {
				if (isGetTrid) {
					isGetTrid = false;
					callOfflineDataUpdate();
				} else {

					try {
						// confirmationDialog.dismiss();
						if (GetResponseInfo.sResponseUpdate[0].equals("Yes")) {

							new Get_LastTransactionID(Transaction_LoanAccountFragment.this).execute();
							isGetTrid = true;
						} else {
							confirmationDialog.dismiss();

							TastyToast.makeText(getActivity(), AppStrings.transactionFailAlert, TastyToast.LENGTH_SHORT,
									TastyToast.ERROR);
							MainFragment_Dashboard fragment = new MainFragment_Dashboard();
							setFragment(fragment);
						}

					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
				}
			}
		}
	}

	private void callOfflineDataUpdate() {
		// TODO Auto-generated method stub
		String mCashinHand = SelectedGroupsTask.sCashinHand;
		String mTransactionDate = SelectedGroupsTask.sLastTransactionDate_Response;
		String mCashatBank = SelectedGroupsTask.sCashatBank;

		if (selectedType.equals("BANK")) {

			EShaktiApplication.setLoanAccToBank(true);
		}
		String mBankDetails = GetOfflineTransactionValues.getBankDetails();

		String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mTransactionDate, mCashinHand,
				mCashatBank, mBankDetails);

		Log.e("LOANACCOUNTFRAGMENT", mGroupMasterResponse);
		Log.i("LOANACCOUNTFRAGMENT" + "Bank Detailssss", mBankDetails);

		String mSelectedGroupId = SelectedGroupsTask.Group_Id;
		EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GROUPMASTERUPDATE,
				new GroupDetailsUpdate(mSelectedGroupId, mGroupMasterResponse));

		String[] arrValues = publicValues.mLoanAccBankListWebservices.split("#");

		mBankName.clear();
		mBankNameSendtoServer.clear();
		mLoanId.clear();
		mLoanType.clear();
		mFixedDeposit.clear();
		String[] arrValuesSplit = null;
		String mMasterresponse = null, result = null;
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < arrValues.length; i++) {
			String ValuesSplit = arrValues[i];

			arrValuesSplit = ValuesSplit.split("~");

			mBankName.add(arrValuesSplit[0]);
			mBankNameSendtoServer.add(arrValuesSplit[1]);

			mLoanId.add(arrValuesSplit[2]);
			mLoanType.add(arrValuesSplit[3]);
			mFixedDeposit.add(arrValuesSplit[4]);

		}
		String mTempbankname = null, mTempBankNameSendtoServer = null, mTempLoanId = null, mTempLoanType = null,
				mTempFixedDeposit = null;
		for (int i = 0; i < arrValues.length; i++) {

			mTempbankname = mBankName.get(i);
			mTempBankNameSendtoServer = mBankNameSendtoServer.get(i);
			mTempLoanId = mLoanId.get(i);
			mTempLoanType = mLoanType.get(i);
			mTempFixedDeposit = mFixedDeposit.get(i);
			if (EShaktiApplication.getLoanaccBankNameSendtoServer().equals(mBankNameSendtoServer.get(i))) {
				int mFixedDepositValues = Integer
						.parseInt(mTempFixedDeposit.substring(0, mTempFixedDeposit.indexOf(".")));

				mTempFixedDeposit = String.valueOf(mFixedDepositValues - Integer.parseInt(mWithdrawalValue));

				mTempFixedDeposit = mTempFixedDeposit + ".00";
			}
			mMasterresponse = mTempbankname + "~" + mTempBankNameSendtoServer + "~" + mTempLoanId + "~" + mTempLoanType
					+ "~" + mTempFixedDeposit + "#";
			builder.append(mMasterresponse);

		}
		result = builder.toString();

		Log.e("Values-------------->>>> Current Loan Account Bank Values", result);

		isServiceCall = false;

		EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GET_LOANACC_UPDATE,
				new LoanAccountUpdate(mSelectedGroupId, result));
	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub

		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();
		
		FragmentDrawer.drawer_CashinHand.setText(RegionalConversion.getRegionalConversion(AppStrings.cashinhand)

				+ SelectedGroupsTask.sCashinHand);
		FragmentDrawer.drawer_CashinHand.setTypeface(LoginActivity.sTypeface);

		 
		FragmentDrawer.drawer_CashatBank.setText(RegionalConversion.getRegionalConversion(AppStrings.cashatBank)

				+ SelectedGroupsTask.sCashatBank);
		FragmentDrawer.drawer_CashatBank.setTypeface(LoginActivity.sTypeface);
		
		FragmentDrawer.drawer_lastTR_Date.setText(RegionalConversion.getRegionalConversion(AppStrings.lastTransactionDate)
				+ " : " + SelectedGroupsTask.sLastTransactionDate_Response);
		FragmentDrawer.drawer_lastTR_Date.setTypeface(LoginActivity.sTypeface);
		
		UpdateCalendarMinMaxDateUtils.OnUpdateCalendarDate();
		
		Calendar calender = Calendar.getInstance();

		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String formattedDate = df.format(calender.getTime());
		EShaktiApplication.setLastTransDate_GroupId(SelectedGroupsTask.Group_Id);

		new GroupTempDBLastTransDate(EShaktiApplication.getLastTransDate_GroupId(),
				SelectedGroupsTask.sLastTransactionDate_Response, formattedDate);

		GroupProvider.updateGroupLastTransDateResponse();

	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		SBus.INST.unRegister(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.fragment_Edit_button:
			mWithdrawalValue = Reset.reset(mWithdrawalValue);
			mExpensesValue = Reset.reset(mExpensesValue);
			// mBankNameValue = Reset.reset(mBankNameValue);
			mSelectedTypeValue = Reset.reset(mSelectedTypeValue);
			mSubmitButton.setClickable(true);
			isServiceCall = false;
			confirmationDialog.dismiss();

			break;
		case R.id.fragment_Ok_button:

			if (ConnectionUtils.isNetworkAvailable(getActivity())) {
				if (PrefUtils.getGroupMasterResValues() != null && PrefUtils.getGroupMasterResValues().equals("1")) {

					EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GET_TRANS_SINGLE,
							new GetTransactionSingle(PrefUtils.getOfflineUniqueID()));

				} else {

					try {
						if (!isServiceCall) {
							isServiceCall = true;

							new Get_GL_Fixed_Deposit_WithdrawalWebservice(Transaction_LoanAccountFragment.this)
									.execute();
						}
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
				}

			} else {
				// Do offline Stuffs

				if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {

					EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GET_TRANS_SINGLE,
							new GetTransactionSingle(PrefUtils.getOfflineUniqueID()));
				} else {
					TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
							TastyToast.ERROR);
				}

			}

			break;
		}
	}

	@Subscribe
	public void OnTransUpdateLoanWithdrawal(final TransactionUpdateResponse transactionUpdateResponse) {
		switch (transactionUpdateResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), transactionUpdateResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), transactionUpdateResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			confirmationDialog.dismiss();
			String mCashinHand = SelectedGroupsTask.sCashinHand;

			int mCashatBankValue = Integer.parseInt(SelectedGroupsTask.sCashatBank);
			String mCashatBank = String.valueOf((mCashatBankValue));

			SelectedGroupsTask.sCashatBank = mCashatBank;
			if (selectedType.equals("CASH")) {
				mCashinHand = String.valueOf((Integer.parseInt(mCashinHand) + Integer.parseInt(mWithdrawalValue))
						- (Integer.parseInt(mExpensesValue)));
			} else if (selectedType.equals("BANK")) {

				mCashatBank = String.valueOf((Integer.parseInt(mCashatBank) + Integer.parseInt(mWithdrawalValue)));

			}
			if (selectedType.equals("BANK")) {

				EShaktiApplication.setLoanAccToBank(true);
			}
			String mBankDetails = GetOfflineTransactionValues.getBankDetails();
			String mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
			SelectedGroupsTask.sLastTransactionDate_Response = mTrasactiondate;
			String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mTrasactiondate, mCashinHand,
					mCashatBank, mBankDetails);
			String mSelectedGroupId = SelectedGroupsTask.Group_Id;
			EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GROUPMASTERUPDATE,
					new GroupDetailsUpdate(mSelectedGroupId, mGroupMasterResponse));

			SelectedGroupsTask.sCashatBank = mCashatBank;

			SelectedGroupsTask.sCashinHand = mCashinHand;
			String[] arrValues = publicValues.mLoanAccBankListWebservices.split("#");

			mBankName.clear();
			mBankNameSendtoServer.clear();
			mLoanId.clear();
			mLoanType.clear();
			mFixedDeposit.clear();
			String[] arrValuesSplit = null;
			String mMasterresponse = null, result = null;
			StringBuilder builder = new StringBuilder();
			for (int i = 0; i < arrValues.length; i++) {
				String ValuesSplit = arrValues[i];

				arrValuesSplit = ValuesSplit.split("~");

				mBankName.add(arrValuesSplit[0]);
				mBankNameSendtoServer.add(arrValuesSplit[1]);

				mLoanId.add(arrValuesSplit[2]);
				mLoanType.add(arrValuesSplit[3]);
				mFixedDeposit.add(arrValuesSplit[4]);

			}
			String mTempbankname = null, mTempBankNameSendtoServer = null, mTempLoanId = null, mTempLoanType = null,
					mTempFixedDeposit = null;
			for (int i = 0; i < arrValues.length; i++) {

				mTempbankname = mBankName.get(i);
				mTempBankNameSendtoServer = mBankNameSendtoServer.get(i);
				mTempLoanId = mLoanId.get(i);
				mTempLoanType = mLoanType.get(i);
				mTempFixedDeposit = mFixedDeposit.get(i);
				if (EShaktiApplication.getLoanaccBankNameSendtoServer().equals(mBankNameSendtoServer.get(i))) {
					int mFixedDepositValues = Integer
							.parseInt(mTempFixedDeposit.substring(0, mTempFixedDeposit.indexOf(".")));

					mTempFixedDeposit = String.valueOf(mFixedDepositValues - Integer.parseInt(mWithdrawalValue));

					mTempFixedDeposit = mTempFixedDeposit + ".00";
				}
				mMasterresponse = mTempbankname + "~" + mTempBankNameSendtoServer + "~" + mTempLoanId + "~"
						+ mTempLoanType + "~" + mTempFixedDeposit + "#";
				builder.append(mMasterresponse);

			}
			result = builder.toString();

			Log.e("Values-------------->>>> Current Loan Account Bank Values", result);

			EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GET_LOANACC_UPDATE,
					new LoanAccountUpdate(mSelectedGroupId, result));

			break;
		}
	}

	@Subscribe
	public void onAddTransactionLoanWithdrawal(final TransactionResponse transactionResponse) {
		switch (transactionResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), transactionResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), transactionResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:
			confirmationDialog.dismiss();

			String mCashinHand = SelectedGroupsTask.sCashinHand;

			int mCashatBankValue = Integer.parseInt(SelectedGroupsTask.sCashatBank);
			String mCashatBank = String.valueOf((mCashatBankValue));

			SelectedGroupsTask.sCashatBank = mCashatBank;
			if (selectedType.equals("CASH")) {
				mCashinHand = String.valueOf((Integer.parseInt(mCashinHand) + Integer.parseInt(mWithdrawalValue))
						- (Integer.parseInt(mExpensesValue)));
			} else if (selectedType.equals("BANK")) {
				mCashatBank = String.valueOf((Integer.parseInt(mCashatBank) + Integer.parseInt(mWithdrawalValue)
						- (Integer.parseInt(mExpensesValue))));
			}
			if (selectedType.equals("BANK")) {

				EShaktiApplication.setLoanAccToBank(true);
			}
			String mBankDetails = GetOfflineTransactionValues.getBankDetails();
			String mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
			SelectedGroupsTask.sLastTransactionDate_Response = mTrasactiondate;
			String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mTrasactiondate, mCashinHand,
					mCashatBank, mBankDetails);
			String mSelectedGroupId = SelectedGroupsTask.Group_Id;
			EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GROUPMASTERUPDATE,
					new GroupDetailsUpdate(mSelectedGroupId, mGroupMasterResponse));
			SelectedGroupsTask.sCashatBank = mCashatBank;

			SelectedGroupsTask.sCashinHand = mCashinHand;

			String[] arrValues = publicValues.mLoanAccBankListWebservices.split("#");

			mBankName.clear();
			mBankNameSendtoServer.clear();
			mLoanId.clear();
			mLoanType.clear();
			mFixedDeposit.clear();
			String[] arrValuesSplit = null;
			String mMasterresponse = null, result = null;
			StringBuilder builder = new StringBuilder();
			for (int i = 0; i < arrValues.length; i++) {
				String ValuesSplit = arrValues[i];

				arrValuesSplit = ValuesSplit.split("~");

				mBankName.add(arrValuesSplit[0]);
				mBankNameSendtoServer.add(arrValuesSplit[1]);

				mLoanId.add(arrValuesSplit[2]);
				mLoanType.add(arrValuesSplit[3]);
				mFixedDeposit.add(arrValuesSplit[4]);

			}
			String mTempbankname = null, mTempBankNameSendtoServer = null, mTempLoanId = null, mTempLoanType = null,
					mTempFixedDeposit = null;
			for (int i = 0; i < arrValues.length; i++) {

				mTempbankname = mBankName.get(i);
				mTempBankNameSendtoServer = mBankNameSendtoServer.get(i);
				mTempLoanId = mLoanId.get(i);
				mTempLoanType = mLoanType.get(i);
				mTempFixedDeposit = mFixedDeposit.get(i);
				if (EShaktiApplication.getLoanaccBankNameSendtoServer().equals(mBankNameSendtoServer.get(i))) {
					int mFixedDepositValues = Integer
							.parseInt(mTempFixedDeposit.substring(0, mTempFixedDeposit.indexOf(".")));

					mTempFixedDeposit = String.valueOf(mFixedDepositValues - Integer.parseInt(mWithdrawalValue));

					mTempFixedDeposit = mTempFixedDeposit + ".00";
				}

				mMasterresponse = mTempbankname + "~" + mTempBankNameSendtoServer + "~" + mTempLoanId + "~"
						+ mTempLoanType + "~" + mTempFixedDeposit + "#";
				builder.append(mMasterresponse);

			}
			result = builder.toString();

			EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GET_LOANACC_UPDATE,
					new LoanAccountUpdate(mSelectedGroupId, result));

			break;
		}
	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub
		mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
		mProgressDilaog.show();
	}

	@Subscribe
	public void OnGetLoanAccountResponse(final LoanAccountDataUpdateResponse loanAccountDataUpdateResponse) {
		switch (loanAccountDataUpdateResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), loanAccountDataUpdateResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), loanAccountDataUpdateResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			TastyToast.makeText(getActivity(), AppStrings.transactionCompleted, TastyToast.LENGTH_SHORT,
					TastyToast.SUCCESS);
			if (!ConnectionUtils.isNetworkAvailable(getActivity())) {

				String mValues = Put_DB_GroupNameDetail_Response
						.put_DB_GroupNameDetails_Response(EShaktiApplication.getGroupResponse());

				GroupProvider.updateGroupResponse(
						new GroupResponseUpdate(EShaktiApplication.getGroupId_GroupLastTransDate(), mValues));
			}

			MainFragment_Dashboard fragment = new MainFragment_Dashboard();
			setFragment(fragment);
			break;
		}

	}

	@Subscribe
	public void OnGroupMasUpdate(final GroupMasUpdateResponse masterResponse) {
		switch (masterResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), masterResponse.getFetcherResult().getMessage(), TastyToast.LENGTH_SHORT,
					TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), masterResponse.getFetcherResult().getMessage(), TastyToast.LENGTH_SHORT,
					TastyToast.ERROR);
			break;
		case SUCCESS:

			confirmationDialog.dismiss();

			TastyToast.makeText(getActivity(), AppStrings.transactionCompleted, TastyToast.LENGTH_SHORT,
					TastyToast.SUCCESS);

			if (!ConnectionUtils.isNetworkAvailable(getActivity())) {

				String mValues = Put_DB_GroupNameDetail_Response
						.put_DB_GroupNameDetails_Response(EShaktiApplication.getGroupResponse());

				GroupProvider.updateGroupResponse(
						new GroupResponseUpdate(EShaktiApplication.getGroupId_GroupLastTransDate(), mValues));
			}

			MainFragment_Dashboard fragment = new MainFragment_Dashboard();
			setFragment(fragment);

			break;
		}
	}
}
