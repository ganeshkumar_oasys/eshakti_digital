package com.yesteam.eshakti.view.fragment;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.Member_savings_summaryTask;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Reports_MemberSavingsSummary_ReportsFragment extends Fragment {

	public static String TAG = Reports_MemberSavingsSummary_ReportsFragment.class.getSimpleName();
	private TextView mGroupName, mCashinHand, mCashatBank, mHeader,mMemberName;
	private TableLayout mReportsTable;

	String responseArr[];
	int mSize;
	String[] trDate, trAmount;

	public Reports_MemberSavingsSummary_ReportsFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_MEMBER_REPORTS_MEMBER_SAVING_SUMM;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		try {

			String response = Member_savings_summaryTask.sMem_Savings_Summary_ServiceResponse;
			Log.d(TAG, response);

			responseArr = Member_savings_summaryTask.sMem_Savings_Summary_ServiceResponse.split("~");
			System.out.println("RESPONSE ARR : " + responseArr.length);

			mSize = responseArr.length;

			String transactionDate = "", transactionAmount = "";

			for (int i = 0; i < (mSize - 2); i++) {
				if (i % 2 == 0) {
					transactionAmount = transactionAmount + responseArr[i] + "~";
				} else if (i % 2 != 0) {
					transactionDate = transactionDate + responseArr[i] + "~";
				}
			}

			trDate = transactionDate.split("~");
			trAmount = transactionAmount.split("~");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.frgament_reports_summary, container, false);

		MainFragment_Dashboard.isBackpressed = false;

		try {

			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mMemberName = (TextView) rootView.findViewById(R.id.memberName);
			mMemberName.setText(EShaktiApplication.getSelectedMemberName());
			mMemberName.setTypeface(LoginActivity.sTypeface);
			mMemberName.setVisibility(View.VISIBLE);
			
			mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashinHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashinHand.setTypeface(LoginActivity.sTypeface);

			mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashatBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashatBank.setTypeface(LoginActivity.sTypeface);

			mHeader = (TextView) rootView.findViewById(R.id.fragmentHeader);
			mHeader.setText(RegionalConversion
					.getRegionalConversion(String.valueOf(Reports_MemberReports_MenuFragment.sSlelectedReportsMenu)));
			mHeader.setTypeface(LoginActivity.sTypeface);

			TableLayout headerTableLayout = (TableLayout) rootView.findViewById(R.id.memberReports_headerTable);
			mReportsTable = (TableLayout) rootView.findViewById(R.id.fragment_ReportsTable);

			TableRow reportsHeader = new TableRow(getActivity());

			TableRow.LayoutParams headerParams = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
					TableRow.LayoutParams.WRAP_CONTENT, 1f);

			TextView date_headerText = new TextView(getActivity());
			date_headerText.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.date)));
			date_headerText.setTypeface(LoginActivity.sTypeface);
			date_headerText.setTextColor(Color.WHITE);
			date_headerText.setPadding(110, 5, 10, 5);
			date_headerText.setLayoutParams(headerParams);
			date_headerText.setBackgroundResource(color.tableHeader);
			reportsHeader.addView(date_headerText);

			TextView amountHeader = new TextView(getActivity());
			amountHeader.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.amount)));
			amountHeader.setTypeface(LoginActivity.sTypeface);
			amountHeader.setTextColor(Color.WHITE);
			amountHeader.setPadding(10, 5, 80, 5);
			amountHeader.setLayoutParams(headerParams);
			amountHeader.setGravity(Gravity.RIGHT);
			amountHeader.setBackgroundResource(color.tableHeader);
			reportsHeader.addView(amountHeader);

			headerTableLayout.addView(reportsHeader,
					new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

			for (int i = 0; i < trDate.length; i++) {

				TableRow indv_ReportsRow = new TableRow(getActivity());

				TableRow.LayoutParams contentParams = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
						TableRow.LayoutParams.WRAP_CONTENT, 1f);

				TextView trDate_Text = new TextView(getActivity());
				trDate_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(trDate[i])));
				trDate_Text.setTextColor(color.black);
				trDate_Text.setPadding(80, 5, 10, 5);
				trDate_Text.setLayoutParams(contentParams);
				indv_ReportsRow.addView(trDate_Text);

				TextView trAmount_Text = new TextView(getActivity());
				trAmount_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(trAmount[i])));
				trAmount_Text.setTextColor(color.black);
				trAmount_Text.setPadding(10, 5, 80, 5);
				trAmount_Text.setLayoutParams(contentParams);// contentParams
																// lParams
				trAmount_Text.setGravity(Gravity.RIGHT);
				trAmount_Text.setTextColor(color.black);
				indv_ReportsRow.addView(trAmount_Text);

				mReportsTable.addView(indv_ReportsRow,
						new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

				View rullerView = new View(getActivity());
				rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
				rullerView.setBackgroundColor(Color.rgb(220, 220, 220));

				mReportsTable.addView(rullerView);
			}

			TableLayout outstandingTable = (TableLayout) rootView.findViewById(R.id.fragment_OutstandingTable);

			TableRow totalRow = new TableRow(getActivity());
			totalRow.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
			totalRow.setGravity(Gravity.CENTER_HORIZONTAL);

			totalRow.setPadding(15, 10, 5, 10);

			TextView total_Text = new TextView(getActivity());
			total_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.total)));
			total_Text.setTypeface(LoginActivity.sTypeface, Typeface.BOLD);
			total_Text.setTextColor(Color.BLACK);
			total_Text.setPadding(5, 5, 5, 5);
			totalRow.addView(total_Text);

			TextView totalAmount = new TextView(getActivity());
			totalAmount.setText(
					GetSpanText.getSpanString(getActivity(), String.valueOf(responseArr[responseArr.length - 1])));
			totalAmount.setTextColor(Color.BLACK);
			totalAmount.setTypeface(null, Typeface.BOLD);
			totalAmount.setPadding(5, 5, 5, 5);
			totalRow.addView(totalAmount);

			outstandingTable.addView(totalRow, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

			View rullerView = new View(getActivity());
			rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
			rullerView.setBackgroundColor(Color.rgb(220, 220, 220));

			outstandingTable.addView(rullerView);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return rootView;
	}

	@Override
	public void onAttach(Context context) {
		// TODO Auto-generated method stub
		super.onAttach(context);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}
}
