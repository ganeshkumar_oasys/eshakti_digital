package com.yesteam.eshakti.view.fragment;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.Get_GroupLoanSummaryTask;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Reports_GroupPersonalLoanSummaryFragment extends Fragment {

	private TextView mGroupName, mCashInHand, mCashAtBank, mHeader;
	public static String tempmem_Ids = "", tempprinciple_Amount = "", temprepayment_Amount = "";
	public static String mem_Ids[];
	public static String principle_Amount[];
	public static String repayment_Amount[];

	int size;

	private String mLanguagelocale = "";
	int headerColumnWidth[], contentColumnWidth[];
	private TableLayout headerTable, table2;

	public Reports_GroupPersonalLoanSummaryFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_GROUP_REPORTS_PERSONAL_LOAN_SUMM;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_membersloansummary, container, false);

		MainFragment_Dashboard.isBackpressed = false;

		try {
			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashInHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashInHand.setTypeface(LoginActivity.sTypeface);

			mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashAtBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashAtBank.setTypeface(LoginActivity.sTypeface);

			mHeader = (TextView) rootView.findViewById(R.id.fragment_membersloansummary_headertext);
			mHeader.setText(RegionalConversion.getRegionalConversion(
					Reports_GroupLoanListReportFragment.loanName.toString() + " " + AppStrings.summary));
			mHeader.setTypeface(LoginActivity.sTypeface);

			mLanguagelocale = PrefUtils.getUserlangcode();

			String responseArr[] = Get_GroupLoanSummaryTask.sGroup_Loan_summary_response.split("~");

			System.out.println("Length of response :" + responseArr.length);
			
			tempprinciple_Amount = "";
			temprepayment_Amount = "";

			for (int i = 6; i < responseArr.length; i = i + 3) {
				tempmem_Ids = tempmem_Ids + responseArr[i].toString() + "~";

				tempprinciple_Amount = tempprinciple_Amount + responseArr[i + 1].toString() + "~";

				temprepayment_Amount = temprepayment_Amount + responseArr[i + 2].toString() + "~";

			}

			size = SelectedGroupsTask.member_Name.size();
			System.out.println("Length of members name :" + size);

			for (int i = 0; i < size; i++) {

				mem_Ids = tempmem_Ids.split("~");

				principle_Amount = tempprinciple_Amount.split("~");

				repayment_Amount = temprepayment_Amount.split("~");

			}

			TableLayout table1 = (TableLayout) rootView.findViewById(R.id.fragment_balanceTable);
			table1.setGravity(Gravity.CENTER_HORIZONTAL);

			TableRow loan_row = new TableRow(getActivity());

			TextView head_groupLoan = new TextView(getActivity());
			head_groupLoan.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.groupLoan)));
			head_groupLoan.setTypeface(LoginActivity.sTypeface);
			head_groupLoan.setTextColor(color.black);
			head_groupLoan.setPadding(10, 5, 10, 5);
			loan_row.addView(head_groupLoan);

			TextView head_amount = new TextView(getActivity());
			head_amount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(responseArr[3])));
			head_amount.setTextColor(color.black);
			head_amount.setPadding(5, 5, 5, 5);
			loan_row.addView(head_amount);

			table1.addView(loan_row,
					new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
			TableRow balance_row = new TableRow(getActivity());

			TextView head_balance = new TextView(getActivity());
			head_balance.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.outstanding)));// (AppStrings.balance)));
			head_balance.setTypeface(LoginActivity.sTypeface);
			head_balance.setTextColor(color.black);
			head_balance.setPadding(10, 5, 10, 5);
			balance_row.addView(head_balance);

			TextView balance_amount = new TextView(getActivity());
			balance_amount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(responseArr[5])));
			balance_amount.setTextColor(color.black);
			balance_amount.setPadding(5, 5, 5, 5);
			balance_row.addView(balance_amount);

			table1.addView(balance_row,
					new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

			headerTable = (TableLayout) rootView.findViewById(R.id.groupPersonalLoanSummary_headerTable);

			table2 = (TableLayout) rootView.findViewById(R.id.fragment_contentTable);

			TableRow head_row = new TableRow(getActivity());

			TableRow.LayoutParams headerParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);

			TextView head_date = new TextView(getActivity());
			head_date.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.Name)));
			head_date.setTypeface(LoginActivity.sTypeface);
			head_date.setTextColor(Color.WHITE);
			head_date.setPadding(20, 5, 10, 5);
			head_date.setBackgroundResource(color.tableHeader);
			head_date.setLayoutParams(headerParams);
			head_row.addView(head_date);

			TextView principleAmount = new TextView(getActivity());
			principleAmount
					.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.principleAmount)));
			principleAmount.setTypeface(LoginActivity.sTypeface);
			principleAmount.setTextColor(Color.WHITE);
			if (mLanguagelocale.equalsIgnoreCase("English")) {
				principleAmount.setPadding(90, 5, 10, 5);
			} else if (mLanguagelocale.equalsIgnoreCase("Tamil")) {
				principleAmount.setPadding(80, 5, 10, 5);
			} else if (mLanguagelocale.equalsIgnoreCase("Hindi")) {
				principleAmount.setPadding(80, 5, 10, 5);
			} else if (mLanguagelocale.equalsIgnoreCase("Marathi")) {
				principleAmount.setPadding(80, 5, 10, 5);
			} else {
				principleAmount.setPadding(90, 5, 10, 5);
			}

			principleAmount.setLayoutParams(headerParams);
			principleAmount.setGravity(Gravity.RIGHT);
			principleAmount.setBackgroundResource(color.tableHeader);
			head_row.addView(principleAmount);

			TextView head_interest = new TextView(getActivity());
			head_interest.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.mRepaid)));// (AppStrings.Repaymenybalance)));
			head_interest.setTypeface(LoginActivity.sTypeface);
			head_interest.setTextColor(Color.WHITE);
			head_interest.setPadding(10, 5, 20, 5);
			head_interest.setLayoutParams(headerParams);
			head_interest.setGravity(Gravity.RIGHT);
			head_interest.setBackgroundResource(color.tableHeader);
			head_row.addView(head_interest);

			headerTable.addView(head_row, new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
					TableLayout.LayoutParams.WRAP_CONTENT));

			try {

				for (int i = 0; i < size; i++) {

					TableRow contentRow = new TableRow(getActivity());
					TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
							LayoutParams.WRAP_CONTENT, 1f);

					TextView member_Name = new TextView(getActivity());
					String memberName = SelectedGroupsTask.member_Name.elementAt(i).toString();
					member_Name.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(memberName)));
					member_Name.setTypeface(LoginActivity.sTypeface);
					member_Name.setTextColor(color.black);
					member_Name.setPadding(20, 5, 10, 5);
					member_Name.setLayoutParams(contentParams);
					contentRow.addView(member_Name);

					TextView amount = new TextView(getActivity());
					amount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(principle_Amount[i])));
					amount.setTextColor(color.black);
					amount.setGravity(Gravity.RIGHT);
					if (mLanguagelocale.equalsIgnoreCase("English")) {
						amount.setPadding(10, 5, 30, 5);
					} else if (mLanguagelocale.equalsIgnoreCase("Tamil")) {
						amount.setPadding(10, 5, 35, 5);
					} else if (mLanguagelocale.equalsIgnoreCase("Hindi")) {
						amount.setPadding(10, 5, 35, 5);
					} else if (mLanguagelocale.equalsIgnoreCase("Marathi")) {
						amount.setPadding(10, 5, 35, 5);
					} else {
						amount.setPadding(10, 5, 30, 5);
					}
					amount.setLayoutParams(contentParams);
					contentRow.addView(amount);

					TextView repaymentAmount = new TextView(getActivity());
					repaymentAmount
							.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(repayment_Amount[i])));
					repaymentAmount.setPadding(10, 5, 20, 5);
					repaymentAmount.setGravity(Gravity.RIGHT);
					repaymentAmount.setLayoutParams(contentParams);
					repaymentAmount.setTextColor(color.black);
					contentRow.addView(repaymentAmount);

					table2.addView(contentRow,
							new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

					View rullerView = new View(getActivity());
					rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
					rullerView.setBackgroundColor(Color.rgb(220, 220, 220));
					table2.addView(rullerView);

				}

				// }
			} catch (Exception e) {
			}

			onChangeWidthOfColumn();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return rootView;

	}

	private void onChangeWidthOfColumn() {
		// TODO Auto-generated method stub
		headerColumnWidth = new int[((TableRow) headerTable.getChildAt(0)).getChildCount()];

		TableRow headerRow = (TableRow) headerTable.getChildAt(0);

		contentColumnWidth = new int[((TableRow) table2.getChildAt(0)).getChildCount()];
		TableRow contentRow = (TableRow) table2.getChildAt(0);

		for (int i = 0; i < headerRow.getChildCount(); i++) {
			headerColumnWidth[i] = this.viewWidth(headerRow.getChildAt(i));
			contentColumnWidth[i] = this.viewWidth(contentRow.getChildAt(i));
		}

		for (int i = 0; i < headerColumnWidth.length; i++) {

			if (headerColumnWidth[i] > contentColumnWidth[i]) {
				TextView contentTextView = (TextView) contentRow.getChildAt(i);
				contentTextView
						.setLayoutParams(new TableRow.LayoutParams(headerColumnWidth[i], LayoutParams.MATCH_PARENT));
			} else {
				TextView headerTextView = (TextView) headerRow.getChildAt(i);
				headerTextView.setLayoutParams(
						new TableRow.LayoutParams(contentColumnWidth[i] + 10, LayoutParams.MATCH_PARENT));
			}
		}

	}

	// read a view's width
	private int viewWidth(View view) {
		view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
		return view.getMeasuredWidth();
	}

}
