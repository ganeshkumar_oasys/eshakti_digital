package com.yesteam.eshakti.view.fragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.adapter.CustomExpandableMenuListAdapter;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;

import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.interfaces.ExpandListItemClickListener;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;
import com.yesteam.eshakti.model.ListItem;
import com.yesteam.eshakti.model.RowItem;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.views.MyExpandableListview;
import com.yesteam.eshakti.views.RaisedButton;
import com.yesteam.eshakti.webservices.GetAddLoanInstallment_BalanceWebservice;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.TextView;

public class Transaction_LoanDisbursement_LoansMenuFragment extends Fragment
		implements TaskListener, OnItemClickListener, ExpandListItemClickListener {

	public static final String TAG = Transaction_GroupLoanRepaidMenuFragment.class.getSimpleName();
	private TextView mGroupName, mCashInHand, mCashAtBank;
	private Dialog mProgressDialog;
	public static List<RowItem> rowItems;
	public static String[] mGroupLoanNames;
	public static String mloan_Id;
	public static String mloan_Name;
	public static String mSendTo_ServerLoan_Id;

	public static String[] response;
	public static String mGroupOS_Offlineresponse = null;

	int size;

	// private ExpandableLayoutListView mListView;
	private MyExpandableListview mListView;
	private List<ListItem> listItems;
	// private CustomMenuListAdapter mAdapter;
	private CustomExpandableMenuListAdapter mAdapter;
	int listImage;
	private TextView mHeader;
	private ArrayList<HashMap<String, String>> childList;
	private int lastExpandedPosition = -1;
	Date date_dashboard, date_loanDisb;

	public Transaction_LoanDisbursement_LoansMenuFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_LOAN_DISBURSE_LOANS;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		SBus.INST.register(this);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_menulist_expandable, container, false);

		MainFragment_Dashboard.isBackpressed = false;
		EShaktiApplication.setFragmentMenuListView(false);

		try {

			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashInHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashInHand.setTypeface(LoginActivity.sTypeface);

			mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashAtBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashAtBank.setTypeface(LoginActivity.sTypeface);

			mHeader = (TextView) rootView.findViewById(R.id.expandableSubmenuHeaderTextview);
			mHeader.setVisibility(View.VISIBLE);
			mHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.mExistingLoan));
			mHeader.setTypeface(LoginActivity.sTypeface);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		try {

			size = SelectedGroupsTask.loan_Name.size() - 1;
			System.out.println("Loan_Name Size:" + size);

			mGroupLoanNames = new String[size];

			for (int i = 0; i < mGroupLoanNames.length; i++) {

				String loanName = SelectedGroupsTask.loan_Name.elementAt(i).toString();
				Log.d(TAG, "Loan Name:" + loanName);
				Log.d(TAG, "Loan Name:" + RegionalConversion
						.getRegionalConversion(SelectedGroupsTask.loan_Name.elementAt(i).toString()));
				mGroupLoanNames[i] = loanName;
				Log.d(TAG, "GroupLoanName:>>>" + mGroupLoanNames[i]);
			}

			listItems = new ArrayList<ListItem>();
			/*
			 * mListView = (ExpandableLayoutListView)
			 * rootView.findViewById(R.id.fragment_List_loanrepaid);
			 * mListView.setOnItemClickListener(this);
			 */
			mListView = (MyExpandableListview) rootView.findViewById(R.id.fragment_List_loanrepaid);
			listImage = R.drawable.ic_navigate_next_white_24dp;

			// rowItems = new ArrayList<RowItem>();
			for (int i = 0; i < mGroupLoanNames.length; i++) {
				ListItem rowItem = new ListItem(mGroupLoanNames[i].toString(), listImage);
				listItems.add(rowItem);
			}

			childList = new ArrayList<HashMap<String, String>>();

			for (int i = 0; i < mGroupLoanNames.length; i++) {
				HashMap<String, String> temp = new HashMap<String, String>();

				temp.put("AccNo", SelectedGroupsTask.loanAcc_loanAccNo.elementAt(i));
				temp.put("BankName", SelectedGroupsTask.loanAcc_bankName.elementAt(i));
				String[] disbursementDateArr = SelectedGroupsTask.loanAcc_loanDisbursementDate.elementAt(i).split("/");
				temp.put("DisbursementDate",
						disbursementDateArr[1] + "/" + disbursementDateArr[0] + "/" + disbursementDateArr[2]);
				childList.add(temp);
			}

			// mAdapter = new CustomMenuListAdapter(getActivity(), listItems);
			mAdapter = new CustomExpandableMenuListAdapter(getActivity(), listItems, childList, this);
			mListView.setAdapter(mAdapter);

			mListView.setOnGroupExpandListener(new OnGroupExpandListener() {

				@Override
				public void onGroupExpand(int groupPosition) {
					// TODO Auto-generated method stub
					if (lastExpandedPosition != -1 && groupPosition != lastExpandedPosition) {
						mListView.collapseGroup(lastExpandedPosition);
					}
					lastExpandedPosition = groupPosition;
				}
			});

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return rootView;
	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub

		mProgressDialog = AppDialogUtils.createProgressDialog(getActivity());
		mProgressDialog.show();

	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub

		try {

			if (mProgressDialog != null) {
				mProgressDialog.dismiss();
				if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {
					getActivity().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub

							if (!result.equals("FAIL")) {

								TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg,
										TastyToast.LENGTH_SHORT, TastyToast.ERROR);
								Constants.NETWORKCOMMONFLAG = "SUCCESS";
							}

						}
					});
				} else {

					if (publicValues.mGetLoanBalnceValues != null) {
						Transaction_Limit_Loan_SB_MenuFragment limit_Loan_SB_MenuFragment = new Transaction_Limit_Loan_SB_MenuFragment();
						setFragment(limit_Loan_SB_MenuFragment);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		SBus.INST.unRegister(this);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
		// TODO Auto-generated method stub

		try {

			RaisedButton button = (RaisedButton) view.findViewById(R.id.fragment_menulist_next_button);
			TextView mAccNo = (TextView) view.findViewById(R.id.accNumber);
			TextView mAccNo_Values = (TextView) view.findViewById(R.id.AccNumber_values);
			TextView mBankName = (TextView) view.findViewById(R.id.bankName);
			TextView mBankNameValues = (TextView) view.findViewById(R.id.bankName_values);
			TextView mdisbursementTime = (TextView) view.findViewById(R.id.disbursementTime);
			TextView mdisbursementTimeValues = (TextView) view.findViewById(R.id.disbursementTime_values);

			mAccNo.setText(AppStrings.mAccountNumber + " :  ");
			mAccNo.setPadding(5, 5, 5, 5);
			mAccNo_Values.setText(SelectedGroupsTask.loanAcc_loanAccNo.get(position));
			mAccNo_Values.setPadding(5, 5, 5, 5);
			mAccNo.setTypeface(LoginActivity.sTypeface);
			mAccNo_Values.setTypeface(LoginActivity.sTypeface);

			mBankName.setText(AppStrings.bankName + " :  ");
			mBankName.setPadding(5, 5, 5, 5);
			mBankNameValues.setText(SelectedGroupsTask.loanAcc_bankName.get(position));
			mBankNameValues.setPadding(5, 5, 5, 5);
			mBankName.setTypeface(LoginActivity.sTypeface);
			mBankNameValues.setTypeface(LoginActivity.sTypeface);

			mdisbursementTime.setText(AppStrings.mLoanDisbursementDate + " :  ");
			mdisbursementTime.setPadding(5, 5, 5, 5);
			mdisbursementTimeValues.setText(SelectedGroupsTask.loanAcc_loanDisbursementDate.get(position));
			mdisbursementTimeValues.setPadding(5, 5, 5, 5);
			mdisbursementTime.setTypeface(LoginActivity.sTypeface);
			mdisbursementTimeValues.setTypeface(LoginActivity.sTypeface);
			EShaktiApplication
					.setLoanAcc_LoanDisbursementDate(SelectedGroupsTask.loanAcc_loanDisbursementDate.get(position));

			mAccNo.setTextColor(Color.BLACK);
			mAccNo_Values.setTextColor(Color.BLACK);
			mBankName.setTextColor(Color.BLACK);
			mBankNameValues.setTextColor(Color.BLACK);

			mdisbursementTime.setTextColor(Color.BLACK);
			mdisbursementTimeValues.setTextColor(Color.BLACK);

			button.setTypeface(LoginActivity.sTypeface);
			button.setText(RegionalConversion.getRegionalConversion(AppStrings.mSelect));

			button.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					try {
						mloan_Id = SelectedGroupsTask.loan_Id.elementAt(position).toString();
						System.out.println("Loan Id" + mloan_Id);

						EShaktiApplication.setLoanId(mloan_Id);
						EShaktiApplication.setLoanName(SelectedGroupsTask.loan_Name.elementAt(position).toString());
						mloan_Name = SelectedGroupsTask.loan_Name.elementAt(position).toString();
						System.out.println("Loan Name:" + mloan_Name);

						String temp_loanName = SelectedGroupsTask.loan_Name.elementAt(position).toString();

						mSendTo_ServerLoan_Id = SelectedGroupsTask.loan_Id.elementAt(position).toString();

						Log.e("Current Loan Name", mloan_Name);
						EShaktiApplication.setIsLoanDisBurseRepaid(false);
						if (mloan_Name.equals(temp_loanName)) {

							if (ConnectionUtils.isNetworkAvailable(getActivity())) {
								new GetAddLoanInstallment_BalanceWebservice(
										Transaction_LoanDisbursement_LoansMenuFragment.this).execute();
							}

						}
						if (PrefUtils.getUserlangcode().equals("English")) {
							if (mloan_Name.equals("CASH CREDIT")) {
								EShaktiApplication.setIsLoanDisBurseRepaid(true);
							}
						} else if (PrefUtils.getUserlangcode().equals("Tamil")) {
							if (mloan_Name.equals("பண கடன்")) {
								EShaktiApplication.setIsLoanDisBurseRepaid(true);
							}
						} else if (PrefUtils.getUserlangcode().equals("Hindi")) {
							if (mloan_Name.equals("कैश क्रेडिट")) {
								EShaktiApplication.setIsLoanDisBurseRepaid(true);
							}
						} else {
							if (mloan_Name.equals("CASH CREDIT")) {
								EShaktiApplication.setIsLoanDisBurseRepaid(true);
							}
						}

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub
		Log.e("Current Class Name!!!!!!!!!!!!!!", fragment.getClass().getName());
		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

	}

	@Override
	public void onItemClick(ViewGroup parent, View view, int position) {
		// TODO Auto-generated method stub

		try {
			// String dashBoardDate = DatePickerDialog.sDashboardDate;

			String dashBoardDate = null;
			if (DatePickerDialog.sDashboardDate.contains("-")) {
				dashBoardDate = DatePickerDialog.sDashboardDate;
			} else if (DatePickerDialog.sDashboardDate.contains("/")) {
				String[] dateArr = DatePickerDialog.sDashboardDate.split("/");
				dashBoardDate = dateArr[0] + "-" + dateArr[1] + "-" + dateArr[2];
				;
			}

			String loanDisbArr[] = SelectedGroupsTask.loanAcc_loanDisbursementDate.elementAt(position).split("/");
			EShaktiApplication
					.setLoanDisbursementDate(SelectedGroupsTask.loanAcc_loanDisbursementDate.elementAt(position));
			String loanDisbDate = loanDisbArr[1] + "-" + loanDisbArr[0] + "-" + loanDisbArr[2];

			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

			try {
				date_dashboard = sdf.parse(dashBoardDate);
				date_loanDisb = sdf.parse(loanDisbDate);
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			if (date_dashboard.compareTo(date_loanDisb) >= 0) {
				EShaktiApplication
						.setLoanAcc_LoanDisbursementDate(SelectedGroupsTask.loanAcc_loanDisbursementDate.get(position));
				mloan_Id = SelectedGroupsTask.loan_Id.elementAt(position).toString();
				System.out.println("Loan Id" + mloan_Id);

				EShaktiApplication.setLoanId(mloan_Id);
				EShaktiApplication.setLoanName(SelectedGroupsTask.loan_Name.elementAt(position).toString());
				mloan_Name = SelectedGroupsTask.loan_Name.elementAt(position).toString();
				System.out.println("Loan Name:" + mloan_Name);

				String temp_loanName = SelectedGroupsTask.loan_Name.elementAt(position).toString();

				mSendTo_ServerLoan_Id = SelectedGroupsTask.loan_Id.elementAt(position).toString();

				Log.e("Current Loan Name", mloan_Name);
				EShaktiApplication.setIsLoanDisBurseRepaid(false);
				if (mloan_Name.equals(temp_loanName)) {

					if (ConnectionUtils.isNetworkAvailable(getActivity())) {
						new GetAddLoanInstallment_BalanceWebservice(Transaction_LoanDisbursement_LoansMenuFragment.this)
								.execute();
					}

				}

				if (PrefUtils.getUserlangcode().equals("English")) {
					if (mloan_Name.equals("CASH CREDIT")) {
						EShaktiApplication.setIsLoanDisBurseRepaid(true);
					}
				} else if (PrefUtils.getUserlangcode().equals("Tamil")) {
					if (mloan_Name.equals("பண கடன்")) {
						EShaktiApplication.setIsLoanDisBurseRepaid(true);
					}
				} else if (PrefUtils.getUserlangcode().equals("Hindi")) {
					if (mloan_Name.equals("कैश क्रेडिट")) {
						EShaktiApplication.setIsLoanDisBurseRepaid(true);
					}
				} else {
					if (mloan_Name.equals("CASH CREDIT")) {
						EShaktiApplication.setIsLoanDisBurseRepaid(true);
					}
				}

			} else {
				TastyToast.makeText(getActivity(), AppStrings.mCheckDisbursementDate, TastyToast.LENGTH_SHORT,
						TastyToast.WARNING);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onItemClickVerification(ViewGroup parent, View view, int position) {

	}
}
