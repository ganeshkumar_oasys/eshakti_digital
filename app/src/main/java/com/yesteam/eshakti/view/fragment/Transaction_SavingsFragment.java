package com.yesteam.eshakti.view.fragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.squareup.otto.Subscribe;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.TransactionOffConstants;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;
import com.yesteam.eshakti.model.RowItem;
import com.yesteam.eshakti.sqlite.database.GroupProvider;
import com.yesteam.eshakti.sqlite.database.TransactionProvider;
import com.yesteam.eshakti.sqlite.database.response.DefaultOffline_PLResponse;
import com.yesteam.eshakti.sqlite.database.response.GetSingleTransResponse;
import com.yesteam.eshakti.sqlite.database.response.GroupMasUpdateResponse;
import com.yesteam.eshakti.sqlite.database.response.TransactionResponse;
import com.yesteam.eshakti.sqlite.database.response.TransactionUpdateResponse;
import com.yesteam.eshakti.sqlite.db.model.GetGroupMemberDetails;
import com.yesteam.eshakti.sqlite.db.model.GetTransactionSingle;
import com.yesteam.eshakti.sqlite.db.model.GetTransactionSinglevalues;
import com.yesteam.eshakti.sqlite.db.model.GroupDetailsUpdate;
import com.yesteam.eshakti.sqlite.db.model.GroupMasterSingle;
import com.yesteam.eshakti.sqlite.db.model.GroupResponseUpdate;
import com.yesteam.eshakti.sqlite.db.model.GroupTempDBLastTransDate;
import com.yesteam.eshakti.sqlite.db.model.Transaction;
import com.yesteam.eshakti.sqlite.db.model.TransactionSav_VSavUpdate;
import com.yesteam.eshakti.sqlite.db.model.TransactionUpdate;
import com.yesteam.eshakti.sqlite.db.model.Transaction_UniqueIdSingle;
import com.yesteam.eshakti.sqlite.db.transactions.TransactionManager.DataType;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.utils.GetOfflineTransactionValues;
import com.yesteam.eshakti.utils.GetResponseInfo;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.utils.Get_EdiText_Filter;
import com.yesteam.eshakti.utils.Get_Offline_MobileDate;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.utils.Put_DB_GroupNameDetail_Response;
import com.yesteam.eshakti.utils.Put_DB_GroupResponse;
import com.yesteam.eshakti.utils.Reset;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.utils.TextviewUtils;
import com.yesteam.eshakti.utils.UpdateCalendarMinMaxDateUtils;
import com.yesteam.eshakti.view.activity.ExitActivity;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.views.CustomHorizontalScrollView;
import com.yesteam.eshakti.views.CustomHorizontalScrollView.onScrollChangedListener;
import com.yesteam.eshakti.webservices.GetMember_SavingsTask;
import com.yesteam.eshakti.webservices.GetMember_VoluntarySavingsTask;
import com.yesteam.eshakti.webservices.Get_All_Mem_OutstandingTask;
import com.yesteam.eshakti.webservices.Get_LastTransactionID;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Transaction_SavingsFragment extends Fragment implements OnClickListener, TaskListener {

	public static final String TAG = Transaction_SavingsFragment.class.getSimpleName();
	List<EditText> sSavingsFields;
	List<EditText> sVSavingsFields;
	public static String sSavingsAmounts[], sVSavingsAmount[];
	public static String sSendToServer_Savings, sSendToServer_VSavings;

	private TextView mGroupName, mCashinHand, mCashatBank, mHeader, mAutoFill_label;
	private CheckBox mAutoFill;
	private Button mSubmit_Raised_Button;
	private Button mEdit_RaisedButton, mOk_RaisedButton;
	private EditText mSavings_values, mVSavings_values;

	public static List<RowItem> sRowItems;
	private Dialog mProgressDilaog;

	String send_To_Server_SavingsType;
	String savingsType[];
	Boolean isValues;
	String nullVlaue = "0";
	String toBeEditArr[];

	String[] confirmArr;
	List<RowItem> rowItems;
	int mSize;
	public static int sSavings_Total, sVSavings_Total;
	public static Boolean isSavings = false;
	boolean isService = false;
	AlertDialog alertDialog;
	Dialog confirmationDialog;

	String mLastTrDate = null, mLastTr_ID = null;

	View rootView;

	private TableLayout mLeftHeaderTable, mRightHeaderTable, mLeftContentTable, mRightContentTable;
	private CustomHorizontalScrollView mHSRightHeader, mHSRightContent;

	String width[] = { AppStrings.memberName, AppStrings.savingsAmount, AppStrings.voluntarySavings };
	int[] rightHeaderWidth = new int[width.length];
	int[] rightContentWidth = new int[width.length];
	boolean isDefaultService = false;
	public static String[] response;
	boolean isGetTrid = false;
	private String mLanguagelocale = "";
	Button mPerviousButton, mNextButton;
	boolean isNextButton = false;
	boolean isServiceCall = false;
	String mSqliteDBStoredValues_Savings = null, mSqliteDBStoredValues_VSavings = null;
	LinearLayout mMemberNameLayout;
	TextView mMemberName;

	public Transaction_SavingsFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		sSavings_Total = 0;
		sVSavings_Total = 0;
		sSendToServer_Savings = Reset.reset(sSendToServer_Savings);
		sSendToServer_VSavings = Reset.reset(sSendToServer_VSavings);

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_SAVINGS;

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		SBus.INST.register(this);
	}

	@SuppressWarnings("deprecation")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		// fragment_savings
		rootView = inflater.inflate(R.layout.fragment_all_transaction, container, false);

		MainFragment_Dashboard.isBackpressed = false;

		sSavingsFields = new ArrayList<EditText>();
		sVSavingsFields = new ArrayList<EditText>();
		mSqliteDBStoredValues_Savings = null;
		mSqliteDBStoredValues_VSavings = null;

		try {

			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashinHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashinHand.setTypeface(LoginActivity.sTypeface);

			mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashatBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashatBank.setTypeface(LoginActivity.sTypeface);

			/** UI Mapping **/

			mHeader = (TextView) rootView.findViewById(R.id.fragmentHeader);
			mHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.savings));
			mHeader.setTypeface(LoginActivity.sTypeface);

			mAutoFill_label = (TextView) rootView.findViewById(R.id.autofillLabel);
			mAutoFill_label.setText(RegionalConversion.getRegionalConversion(AppStrings.autoFill));
			mAutoFill_label.setTypeface(LoginActivity.sTypeface);
			mAutoFill_label.setVisibility(View.VISIBLE);

			mAutoFill = (CheckBox) rootView.findViewById(R.id.autoFill);
			mAutoFill.setVisibility(View.VISIBLE);
			mAutoFill.setOnClickListener(this);

			mMemberNameLayout = (LinearLayout) rootView.findViewById(R.id.member_name_layout);
			mMemberName = (TextView) rootView.findViewById(R.id.member_name);

			mLanguagelocale = PrefUtils.getUserlangcode();

			mSize = SelectedGroupsTask.member_Name.size();
			Log.d(TAG, String.valueOf(mSize));

			mLeftHeaderTable = (TableLayout) rootView.findViewById(R.id.LeftHeaderTable);
			mRightHeaderTable = (TableLayout) rootView.findViewById(R.id.RightHeaderTable);
			mLeftContentTable = (TableLayout) rootView.findViewById(R.id.LeftContentTable);
			mRightContentTable = (TableLayout) rootView.findViewById(R.id.RightContentTable);

			mHSRightHeader = (CustomHorizontalScrollView) rootView.findViewById(R.id.rightHeaderHScrollView);
			mHSRightContent = (CustomHorizontalScrollView) rootView.findViewById(R.id.rightContentHScrollView);

			mHSRightHeader.setOnScrollChangedListener(new onScrollChangedListener() {

				public void onScrollChanged(int l, int t, int oldl, int oldt) {
					// TODO Auto-generated method stub

					mHSRightContent.scrollTo(l, 0);

				}
			});

			mHSRightContent.setOnScrollChangedListener(new onScrollChangedListener() {
				@Override
				public void onScrollChanged(int l, int t, int oldl, int oldt) {
					// TODO Auto-generated method stub
					mHSRightHeader.scrollTo(l, 0);
				}
			});

			TableRow leftHeaderRow = new TableRow(getActivity());

			TableRow.LayoutParams lHeaderParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);

			TextView mMemberName_headerText = new TextView(getActivity());
			mMemberName_headerText
					.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.memberName)));
			mMemberName_headerText.setTypeface(LoginActivity.sTypeface);
			mMemberName_headerText.setTextColor(Color.WHITE);
			mMemberName_headerText.setPadding(10, 5, 10, 5);
			mMemberName_headerText.setLayoutParams(lHeaderParams);
			leftHeaderRow.addView(mMemberName_headerText);

			mLeftHeaderTable.addView(leftHeaderRow);

			TableRow rightHeaderRow = new TableRow(getActivity());
			TableRow.LayoutParams rHeaderParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);
			rHeaderParams.setMargins(10, 0, 20, 0);
			TextView mSavingsAmount_HeaderText = new TextView(getActivity());
			mSavingsAmount_HeaderText
					.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.savingsAmount)));
			mSavingsAmount_HeaderText.setTypeface(LoginActivity.sTypeface);
			mSavingsAmount_HeaderText.setTextColor(Color.WHITE);
			mSavingsAmount_HeaderText.setPadding(5, 5, 5, 5);
			mSavingsAmount_HeaderText.setLayoutParams(rHeaderParams);
			mSavingsAmount_HeaderText.setGravity(Gravity.CENTER);
			mSavingsAmount_HeaderText.setSingleLine(true);
			rightHeaderRow.addView(mSavingsAmount_HeaderText);

			TextView mVSavingsAmount_HeaderText = new TextView(getActivity());
			mVSavingsAmount_HeaderText
					.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.voluntarySavings)));
			mVSavingsAmount_HeaderText.setTypeface(LoginActivity.sTypeface);
			mVSavingsAmount_HeaderText.setTextColor(Color.WHITE);
			if (mLanguagelocale.equalsIgnoreCase("English")) {
				mVSavingsAmount_HeaderText.setPadding(25, 5, 5, 5);
			} else if (mLanguagelocale.equalsIgnoreCase("Tamil")) {
				mVSavingsAmount_HeaderText.setPadding(5, 5, 25, 5);
			} else if (mLanguagelocale.equalsIgnoreCase("Hindi")) {
				mVSavingsAmount_HeaderText.setPadding(5, 5, 25, 5);
			} else if (mLanguagelocale.equalsIgnoreCase("Marathi")) {
				mVSavingsAmount_HeaderText.setPadding(5, 5, 25, 5);
			} else {
				mVSavingsAmount_HeaderText.setPadding(25, 5, 5, 5);
			}

			mVSavingsAmount_HeaderText.setLayoutParams(rHeaderParams);
			mVSavingsAmount_HeaderText.setSingleLine(true);
			rightHeaderRow.addView(mVSavingsAmount_HeaderText);

			mRightHeaderTable.addView(rightHeaderRow);

			getTableRowHeaderCellWidth();

			for (int i = 0; i < mSize; i++) {
				TableRow leftContentRow = new TableRow(getActivity());

				TableRow.LayoutParams leftContentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT, 60, 1f);
				leftContentParams.setMargins(5, 5, 5, 5);

				final TextView memberName_Text = new TextView(getActivity());
				memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
						String.valueOf(SelectedGroupsTask.member_Name.elementAt(i))));
				memberName_Text.setTypeface(LoginActivity.sTypeface);
				memberName_Text.setTextColor(color.black);
				memberName_Text.setPadding(5, 5, 5, 5);
				memberName_Text.setLayoutParams(leftContentParams);
				memberName_Text.setWidth(200);
				memberName_Text.setSingleLine(true);
				memberName_Text.setEllipsize(TextUtils.TruncateAt.END);
				leftContentRow.addView(memberName_Text);

				mLeftContentTable.addView(leftContentRow);

				TableRow rightContentRow = new TableRow(getActivity());

				TableRow.LayoutParams rightContentParams = new TableRow.LayoutParams(rightHeaderWidth[1],
						LayoutParams.WRAP_CONTENT, 1f);
				rightContentParams.setMargins(20, 5, 20, 5);

				mSavings_values = new EditText(getActivity());

				mSavings_values.setId(i);
				sSavingsFields.add(mSavings_values);
				mSavings_values.setPadding(5, 5, 5, 5);
				mSavings_values.setBackgroundResource(R.drawable.edittext_background);
				mSavings_values.setLayoutParams(rightContentParams);
				mSavings_values.setTextAppearance(getActivity(), R.style.MyMaterialTheme);
				mSavings_values.setFilters(Get_EdiText_Filter.editText_filter());
				mSavings_values.setInputType(InputType.TYPE_CLASS_NUMBER);
				mSavings_values.setTextColor(color.black);
				// mSavings_values.setWidth(150);
				mSavings_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

					@Override
					public void onFocusChange(View v, boolean hasFocus) {
						// TODO Auto-generated method stub
						if (hasFocus) {
							((EditText) v).setGravity(Gravity.LEFT);

							mMemberNameLayout.setVisibility(View.VISIBLE);
							mMemberName.setText(memberName_Text.getText().toString().trim());
							mMemberName.setTypeface(LoginActivity.sTypeface);
							TextviewUtils.manageBlinkEffect(mMemberName, getActivity());

						} else {

							((EditText) v).setGravity(Gravity.RIGHT);
							mMemberNameLayout.setVisibility(View.GONE);
							mMemberName.setText("");
						}

					}
				});
				rightContentRow.addView(mSavings_values);

				mVSavings_values = new EditText(getActivity());
				mVSavings_values.setId(i);
				sVSavingsFields.add(mVSavings_values);
				mVSavings_values.setPadding(5, 5, 5, 5);
				mVSavings_values.setBackgroundResource(R.drawable.edittext_background);
				mVSavings_values.setLayoutParams(rightContentParams);
				mVSavings_values.setTextAppearance(getActivity(), R.style.MyMaterialTheme);
				mVSavings_values.setFilters(Get_EdiText_Filter.editText_filter());
				mVSavings_values.setInputType(InputType.TYPE_CLASS_NUMBER);
				mVSavings_values.setTextColor(color.black);
				// mVSavings_values.setWidth(150);
				mVSavings_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

					@Override
					public void onFocusChange(View v, boolean hasFocus) {
						// TODO Auto-generated method stub
						if (hasFocus) {
							((EditText) v).setGravity(Gravity.LEFT);

							mMemberNameLayout.setVisibility(View.VISIBLE);
							mMemberName.setText(memberName_Text.getText().toString().trim());
							mMemberName.setTypeface(LoginActivity.sTypeface);
							TextviewUtils.manageBlinkEffect(mMemberName, getActivity());

						} else {

							((EditText) v).setGravity(Gravity.RIGHT);
							mMemberNameLayout.setVisibility(View.GONE);
							mMemberName.setText("");
						}
					}
				});
				rightContentRow.addView(mVSavings_values);

				mRightContentTable.addView(rightContentRow);

			}

			resizeMemberNameWidth();
			// resizeRightSideTable();

			resizeBodyTableRowHeight();

			mSubmit_Raised_Button = (Button) rootView.findViewById(R.id.fragment_Submit_button);
			mSubmit_Raised_Button.setText(RegionalConversion.getRegionalConversion(AppStrings.submit));
			mSubmit_Raised_Button.setTypeface(LoginActivity.sTypeface);
			mSubmit_Raised_Button.setOnClickListener(this);

			mPerviousButton = (Button) rootView.findViewById(R.id.fragment_Previousbutton);
			mPerviousButton.setText(RegionalConversion.getRegionalConversion(AppStrings.mPervious));
			mPerviousButton.setTypeface(LoginActivity.sTypeface);
			mPerviousButton.setOnClickListener(this);

			mNextButton = (Button) rootView.findViewById(R.id.fragment_Nextbutton);
			mNextButton.setText(RegionalConversion.getRegionalConversion(AppStrings.mNext));
			mNextButton.setTypeface(LoginActivity.sTypeface);
			mNextButton.setOnClickListener(this);

			if (EShaktiApplication.isDefault()) {
				mPerviousButton.setVisibility(View.INVISIBLE);
				mNextButton.setVisibility(View.INVISIBLE);
			}
		} catch (Exception e) {
			e.printStackTrace();

			TastyToast.makeText(getActivity(), AppStrings.adminAlert, TastyToast.LENGTH_SHORT, TastyToast.WARNING);

			Intent intent = new Intent(getActivity(), ExitActivity.class);
			startActivity(intent);
		}

		return rootView;
	}

	private void getTableRowHeaderCellWidth() {

		int lefHeaderChildCount = ((TableRow) mLeftHeaderTable.getChildAt(0)).getChildCount();
		int rightHeaderChildCount = ((TableRow) mRightHeaderTable.getChildAt(0)).getChildCount();

		for (int x = 0; x < (lefHeaderChildCount + rightHeaderChildCount); x++) {

			if (x == 0) {
				rightHeaderWidth[x] = viewWidth(((TableRow) mLeftHeaderTable.getChildAt(0)).getChildAt(x));
			} else {
				rightHeaderWidth[x] = viewWidth(((TableRow) mRightHeaderTable.getChildAt(0)).getChildAt(x - 1));
			}

		}
	}

	private void resizeMemberNameWidth() {
		// TODO Auto-generated method stub
		int leftHeadertWidth = viewWidth(mLeftHeaderTable);
		int leftContentWidth = viewWidth(mLeftContentTable);

		if (leftHeadertWidth < leftContentWidth) {
			mLeftHeaderTable.getLayoutParams().width = leftContentWidth;
		} else {
			mLeftContentTable.getLayoutParams().width = leftHeadertWidth;
		}
	}

	private void resizeBodyTableRowHeight() {

		int leftContentTable_ChildCount = mLeftContentTable.getChildCount();

		for (int x = 0; x < leftContentTable_ChildCount; x++) {

			TableRow leftContentTableRow = (TableRow) mLeftContentTable.getChildAt(x);
			TableRow rightContentTableRow = (TableRow) mRightContentTable.getChildAt(x);

			int rowLeftHeight = viewHeight(leftContentTableRow);
			int rowRightHeight = viewHeight(rightContentTableRow);

			TableRow tableRow = rowLeftHeight < rowRightHeight ? leftContentTableRow : rightContentTableRow;
			int finalHeight = rowLeftHeight > rowRightHeight ? rowLeftHeight : rowRightHeight;

			this.matchLayoutHeight(tableRow, finalHeight);
		}

	}

	private void matchLayoutHeight(TableRow tableRow, int height) {

		int tableRowChildCount = tableRow.getChildCount();

		// if a TableRow has only 1 child
		if (tableRow.getChildCount() == 1) {

			View view = tableRow.getChildAt(0);
			TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();
			params.height = height - (params.bottomMargin + params.topMargin);

			return;
		}

		// if a TableRow has more than 1 child
		for (int x = 0; x < tableRowChildCount; x++) {

			View view = tableRow.getChildAt(x);

			TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();

			if (!isTheHeighestLayout(tableRow, x)) {
				params.height = height - (params.bottomMargin + params.topMargin);
				return;
			}
		}

	}

	// check if the view has the highest height in a TableRow
	private boolean isTheHeighestLayout(TableRow tableRow, int layoutPosition) {

		int tableRowChildCount = tableRow.getChildCount();
		int heighestViewPosition = -1;
		int viewHeight = 0;

		for (int x = 0; x < tableRowChildCount; x++) {
			View view = tableRow.getChildAt(x);
			int height = this.viewHeight(view);

			if (viewHeight < height) {
				heighestViewPosition = x;
				viewHeight = height;
			}
		}

		return heighestViewPosition == layoutPosition;
	}

	// read a view's height
	private int viewHeight(View view) {
		view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
		return view.getMeasuredHeight();
	}

	// read a view's width
	private int viewWidth(View view) {
		view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
		return view.getMeasuredWidth();
	}

	@Override
	public void onAttach(Context context) {
		// TODO Auto-generated method stub
		super.onAttach(context);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		sSavingsAmounts = new String[sSavingsFields.size()];
		sVSavingsAmount = new String[sVSavingsFields.size()];
		Log.d(TAG, "sSavingsAmounts size : " + sSavingsAmounts.length);

		switch (v.getId()) {

		case R.id.fragment_Submit_button:

			try {

				sSavings_Total = 0;
				sVSavings_Total = 0;
				sSendToServer_Savings = Reset.reset(sSendToServer_Savings);
				sSendToServer_VSavings = Reset.reset(sSendToServer_VSavings);

				confirmArr = new String[mSize];

				// Do edit values here

				StringBuilder builder = new StringBuilder();

				for (int i = 0; i < sSavingsAmounts.length; i++) {

					sSavingsAmounts[i] = String.valueOf(sSavingsFields.get(i).getText());

					sVSavingsAmount[i] = String.valueOf(sVSavingsFields.get(i).getText());

					if ((sSavingsAmounts[i].equals("")) || (sSavingsAmounts[i] == null)) {
						sSavingsAmounts[i] = nullVlaue;
					}

					if ((sVSavingsAmount[i].equals("")) || (sVSavingsAmount[i] == null)) {
						sVSavingsAmount[i] = nullVlaue;
					}

					if (sSavingsAmounts[i].matches("\\d*\\.?\\d+")) { // match a
																		// decimal
																		// number

						int savingsAmount = (int) Math.round(Double.parseDouble(sSavingsAmounts[i]));
						sSavingsAmounts[i] = String.valueOf(savingsAmount);
					}

					if (sVSavingsAmount[i].matches("\\d*\\.?\\d+")) { // match a
																		// decimal
																		// number

						int vSavingsamount = (int) Math.round(Double.parseDouble(sVSavingsAmount[i]));
						sVSavingsAmount[i] = String.valueOf(vSavingsamount);
					}

					sSendToServer_Savings = sSendToServer_Savings
							+ String.valueOf(SelectedGroupsTask.member_Id.elementAt(i)) + "~" + sSavingsAmounts[i]
							+ "~";

					sSendToServer_VSavings = sSendToServer_VSavings
							+ String.valueOf(SelectedGroupsTask.member_Id.elementAt(i)) + "~" + sVSavingsAmount[i]
							+ "~";

					confirmArr[i] = String.valueOf(SelectedGroupsTask.member_Name.elementAt(i)) + "           "
							+ sSavingsAmounts[i] + "~" + sVSavingsAmount[i];

					sSavings_Total = sSavings_Total + Integer.parseInt(sSavingsAmounts[i]);

					sVSavings_Total = sVSavings_Total + Integer.parseInt(sVSavingsAmount[i]);

					builder.append(sSavingsAmounts[i]).append(",");

				}

				PrefUtils.setSIPSCValuesKey(builder.toString());

				Log.d(TAG, sSendToServer_Savings);

				Log.d(TAG, sSendToServer_VSavings);

				Log.d(TAG, "TOTAL " + String.valueOf(sSavings_Total));

				Log.d(TAG, "VS TOTAL " + String.valueOf(sVSavings_Total));

				// Do the SP insertion

				if ((sSavings_Total != 0) || (sVSavings_Total != 0)) {

					confirmationDialog = new Dialog(getActivity());

					LayoutInflater inflater = getActivity().getLayoutInflater();
					View dialogView = inflater.inflate(R.layout.dialog_confirmation, null);

					LayoutParams lParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
					dialogView.setLayoutParams(lParams);

					TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
					confirmationHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.confirmation));
					confirmationHeader.setTypeface(LoginActivity.sTypeface);

					TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

					for (int i = 0; i < confirmArr.length; i++) {

						TableRow indv_SavingsRow = new TableRow(getActivity());

						TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
								LayoutParams.WRAP_CONTENT, 1f);
						contentParams.setMargins(10, 5, 10, 5);

						TextView memberName_Text = new TextView(getActivity());
						memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
								String.valueOf(SelectedGroupsTask.member_Name.elementAt(i))));
						memberName_Text.setTypeface(LoginActivity.sTypeface);
						memberName_Text.setTextColor(color.black);
						memberName_Text.setPadding(5, 5, 5, 5);
						memberName_Text.setSingleLine(true);
						memberName_Text.setLayoutParams(contentParams);
						indv_SavingsRow.addView(memberName_Text);

						TextView confirm_values = new TextView(getActivity());
						confirm_values
								.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sSavingsAmounts[i])));
						confirm_values.setTextColor(color.black);
						confirm_values.setPadding(5, 5, 5, 5);
						confirm_values.setGravity(Gravity.RIGHT);
						confirm_values.setLayoutParams(contentParams);
						indv_SavingsRow.addView(confirm_values);

						TextView confirm_VSvalues = new TextView(getActivity());
						confirm_VSvalues
								.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sVSavingsAmount[i])));
						confirm_VSvalues.setTextColor(color.black);
						confirm_VSvalues.setPadding(5, 5, 5, 5);
						confirm_VSvalues.setGravity(Gravity.RIGHT);
						confirm_VSvalues.setLayoutParams(contentParams);
						indv_SavingsRow.addView(confirm_VSvalues);

						confirmationTable.addView(indv_SavingsRow,
								new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

					}
					View rullerView = new View(getActivity());
					rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
					rullerView.setBackgroundColor(Color.rgb(0, 199, 140));// rgb(255,
																			// 229,
																			// 242));
					confirmationTable.addView(rullerView);

					TableRow totalRow = new TableRow(getActivity());

					TableRow.LayoutParams totalParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
							LayoutParams.WRAP_CONTENT, 1f);
					totalParams.setMargins(10, 5, 10, 5);

					TextView totalText = new TextView(getActivity());
					totalText.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.total)));
					totalText.setTypeface(LoginActivity.sTypeface);
					totalText.setTextColor(color.black);
					totalText.setPadding(5, 5, 5, 5);// (5, 10, 5, 10);
					totalText.setLayoutParams(totalParams);
					totalRow.addView(totalText);

					TextView totalAmount = new TextView(getActivity());
					totalAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sSavings_Total)));
					totalAmount.setTextColor(color.black);
					totalAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
					totalAmount.setGravity(Gravity.RIGHT);
					totalAmount.setLayoutParams(totalParams);
					totalRow.addView(totalAmount);

					TextView totalVSAmount = new TextView(getActivity());
					totalVSAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sVSavings_Total)));
					totalVSAmount.setTextColor(color.black);
					totalVSAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
					totalVSAmount.setGravity(Gravity.RIGHT);
					totalVSAmount.setLayoutParams(totalParams);
					totalRow.addView(totalVSAmount);

					confirmationTable.addView(totalRow,
							new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

					mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit_button);
					mEdit_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.edit));
					mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
					mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
																			// 205,
																			// 0));
					mEdit_RaisedButton.setOnClickListener(this);

					mOk_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Ok_button);
					mOk_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.yes));
					mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
					mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
					mOk_RaisedButton.setOnClickListener(this);

					confirmationDialog.getWindow()
							.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
					confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					confirmationDialog.setCanceledOnTouchOutside(false);
					confirmationDialog.setContentView(dialogView);
					confirmationDialog.setCancelable(true);
					confirmationDialog.show();

					MarginLayoutParams margin = (MarginLayoutParams) dialogView.getLayoutParams();
					margin.leftMargin = 10;
					margin.rightMargin = 10;
					margin.topMargin = 10;
					margin.bottomMargin = 10;
					margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);

				} else {

					TastyToast.makeText(getActivity(), AppStrings.nullAlert, TastyToast.LENGTH_SHORT,
							TastyToast.WARNING);

					sSendToServer_Savings = Reset.reset(sSendToServer_Savings);
					sSendToServer_VSavings = Reset.reset(sSendToServer_VSavings);
					sSavings_Total = 0;
					sVSavings_Total = Integer.valueOf(nullVlaue);

				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

			break;

		case R.id.autoFill:

			String similiar_Savings;

			if (mAutoFill.isChecked()) {

				try {

					// Makes all edit fields holds the same savings
					if (!String.valueOf(sSavingsFields.get(0).getText()).equals("")) {

						Log.d(TAG, "SAVINGS");

						similiar_Savings = sSavingsFields.get(0).getText().toString();

						send_To_Server_SavingsType = AppStrings.savings;

						System.out.println("CB Amount : " + similiar_Savings);

						for (int i = 0; i < sSavingsAmounts.length; i++) {
							sSavingsFields.get(i).setText(similiar_Savings);
							sSavingsFields.get(i).setGravity(Gravity.RIGHT);
							sSavingsFields.get(i).clearFocus();
							sSavingsAmounts[i] = similiar_Savings;
						}

					}

					/** To clear the values of EditFields in case of uncheck **/

				} catch (ArrayIndexOutOfBoundsException e) {
					e.printStackTrace();

					TastyToast.makeText(getActivity(), AppStrings.adminAlert, TastyToast.LENGTH_SHORT,
							TastyToast.WARNING);

				}
			} else {
				// Do check box unCheck
			}

			break;

		case R.id.fragment_Edit_button:

			sSendToServer_Savings = Reset.reset(sSendToServer_Savings);
			sSavings_Total = Integer.valueOf(nullVlaue);
			sSendToServer_VSavings = Reset.reset(sSendToServer_VSavings);
			sVSavings_Total = Integer.valueOf(nullVlaue);
			mSubmit_Raised_Button.setClickable(true);

			confirmationDialog.dismiss();

			isServiceCall = false;
			break;

		case R.id.fragment_Ok_button:

			if (ConnectionUtils.isNetworkAvailable(getActivity())) {

				isService = false;

				try {

					if (EShaktiApplication.getLoginFlag().equals(Constants.ONLINEFLAG)) {

						if (!isServiceCall) {
							isServiceCall = true;
							new GetMember_SavingsTask(Transaction_SavingsFragment.this).execute();
						}

					} else {
						TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
								TastyToast.ERROR);
					}

				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}

			} else {

				if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {

					EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GET_TRANS_SINGLE,
							new GetTransactionSingle(PrefUtils.getOfflineUniqueID()));

				} else {
					TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
							TastyToast.ERROR);
				}
			}

			break;
		case R.id.fragment_Previousbutton:
			Meeting_AttendanceFragment attendanceFragment = new Meeting_AttendanceFragment();
			setFragment(attendanceFragment);
			break;
		case R.id.fragment_Nextbutton:
			if (ConnectionUtils.isNetworkAvailable(getActivity())) {
				if (PrefUtils.getGroupMasterResValues() != null && PrefUtils.getGroupMasterResValues().equals("1")) {

					EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.DEFAULT_PLOS,
							new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));

				} else {

					Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID = "0";
					new Get_All_Mem_OutstandingTask(this).execute();
				}
			} else {
				EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.DEFAULT_PLOS,
						new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));

			}
			isDefaultService = true;
			isNextButton = true;
			break;
		default:
			break;
		}

	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub
		mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
		mProgressDilaog.show();
	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub

		if (mProgressDilaog != null) {

			if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
				mProgressDilaog.dismiss();
				mProgressDilaog = null;

			}
			if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {
				getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub

						if (!result.equals("FAIL")) {

							TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg,
									TastyToast.LENGTH_SHORT, TastyToast.ERROR);
							Constants.NETWORKCOMMONFLAG = "SUCCESS";

							isServiceCall = false;
						}

					}
				});

			} else {

				if (isDefaultService) {
					if (!isNextButton) {

						if (GetResponseInfo.sResponseUpdate[0].equals("Yes")) {
							TastyToast.makeText(getActivity(), AppStrings.transactionCompleted, TastyToast.LENGTH_SHORT,
									TastyToast.SUCCESS);
						}
					}
					Transaction_PersonalLoanRepaidFragment savingsFragment = new Transaction_PersonalLoanRepaidFragment();

					int mFragSavingTotal = 0;
					mFragSavingTotal = sSavings_Total + sVSavings_Total;
					EShaktiApplication.setStepwiseSavings(mFragSavingTotal);

					isDefaultService = false;
					Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID = "";
					new Get_LastTransactionID(Transaction_SavingsFragment.this).execute();
					isGetTrid = true;
					isNextButton = false;

					setFragment(savingsFragment);
				} else if (isGetTrid) {
					String mCashinHand = SelectedGroupsTask.sCashinHand;
					String mTransactionDate = SelectedGroupsTask.sLastTransactionDate_Response;
					String mCashatBank = SelectedGroupsTask.sCashatBank;

					String mBankDetails = GetOfflineTransactionValues.getBankDetails();

					String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mTransactionDate,
							mCashinHand, mCashatBank, mBankDetails);
					Log.e(TAG, mGroupMasterResponse);
					Log.i(TAG + "Bank Detailssss", mBankDetails);

					String mSelectedGroupId = SelectedGroupsTask.Group_Id;
					EShaktiApplication.getInstance().getTransactionManager().startTransaction(
							DataType.GROUPMASTERUPDATE, new GroupDetailsUpdate(mSelectedGroupId, mGroupMasterResponse));
					isGetTrid = false;
				} else {

					if (Boolean.valueOf(isService)) {

						try {

							if (GetResponseInfo.sResponseUpdate[0].equals("Yes")) {
							} else {

								TastyToast.makeText(getActivity(), AppStrings.transactionFailAlert,
										TastyToast.LENGTH_SHORT, TastyToast.ERROR);
								MainFragment_Dashboard fragment = new MainFragment_Dashboard();
								setFragment(fragment);

							}
							confirmationDialog.dismiss();
							if (EShaktiApplication.isDefault()) {
								Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID = "0";
								new Get_All_Mem_OutstandingTask(this).execute();
								isDefaultService = true;
							} else {

								new Get_LastTransactionID(Transaction_SavingsFragment.this).execute();
								isGetTrid = true;

							}
						} catch (Exception e) {
							e.printStackTrace();
						}

					} else if (Boolean.valueOf(!isService)) {
						Log.v(TAG, "VS Service calls");
						if (EShaktiApplication.getLoginFlag().equals(Constants.ONLINEFLAG)) {

							new GetMember_VoluntarySavingsTask(Transaction_SavingsFragment.this).execute();

							isService = true;

						} else {

							TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
									TastyToast.ERROR);
						}

					}
				}
			}
		}
	}

	@Subscribe
	public void OnGetSingleTransaction(final GetSingleTransResponse getSingleTransResponse) {
		switch (getSingleTransResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), getSingleTransResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), getSingleTransResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			try {
				String mUniqueId = GetTransactionSinglevalues.getUniqueId();
				String mTrasactiondate = null, mMobileDate = null, mTransaction_UniqueId = null, mSavingsvalues = null,
						mVSavingsvalues = null, mSavingValues_offline = null, mVSavings_offline = null;
				String mCurrentTransDate = null;

				if (publicValues.mOffline_Trans_CurrentDate != null) {
					mCurrentTransDate = publicValues.mOffline_Trans_CurrentDate;
				}
				mSavingValues_offline = GetTransactionSinglevalues.getSavings();
				mVSavings_offline = GetTransactionSinglevalues.getVolunteerSavings();

				mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
				mMobileDate = Get_Offline_MobileDate.getOffline_MobileDate(getActivity());
				mTransaction_UniqueId = PrefUtils.getOfflineUniqueID();
				mSavingsvalues = sSendToServer_Savings + "#" + mTrasactiondate + "#" + mMobileDate;
				mVSavingsvalues = sSendToServer_VSavings + "#" + mTrasactiondate + "#" + mMobileDate;

				System.out.println("---------OnGetSingleTransaction---------");
				Log.e("Savings sendToServer vale  = ", mSavingsvalues);
				Log.e("VoluntarySavings sendToServer vale  = ", mSavingsvalues);
				Log.e("Offline Transaction date", mCurrentTransDate);

				mLastTrDate = DatePickerDialog.sDashboardDate;
				mLastTr_ID = SelectedGroupsTask.sTr_ID.toString();

				if (!mSavingsvalues.contains("?") && !mVSavingsvalues.contains("?") && !mCurrentTransDate.contains("?")
						&& !mTransaction_UniqueId.contains("?")) {

					mSqliteDBStoredValues_Savings = mSavingsvalues;

					mSqliteDBStoredValues_VSavings = mVSavingsvalues;

					if (!ConnectionUtils.isNetworkAvailable(getActivity())) {
						PrefUtils.setStatus_GroupId(SelectedGroupsTask.Group_Id);
						PrefUtils.setStatus_UniqueID(PrefUtils.getOfflineUniqueID());
						PrefUtils.setStatus_Trans_Type(TransactionOffConstants.TRANS_SAV);
					}

					if (mUniqueId == null && mSavingValues_offline == null && mVSavings_offline == null) {

						Log.e(TAG, mSavingsvalues);

						if (mLastTrDate != null && mMobileDate != null && mTransaction_UniqueId != null
								&& mCurrentTransDate != null) {

							EShaktiApplication.getInstance().getTransactionManager().startTransaction(
									DataType.TRANSACTIONADD,
									new Transaction(0, PrefUtils.getUsernameKey(), SelectedGroupsTask.UserName,
											SelectedGroupsTask.Ngo_Id, SelectedGroupsTask.Group_Id,
											mTransaction_UniqueId, mLastTr_ID, mCurrentTransDate, mSavingsvalues,
											mVSavingsvalues, null, null, null, null, null, null, null, null, null, null,
											null, null, null, null, null, null, null, null, null, null));

						}

					} else if (mUniqueId.equalsIgnoreCase(PrefUtils.getOfflineUniqueID())
							&& mSavingValues_offline == null && mVSavings_offline == null) {

						EShaktiApplication.setSetTransValues(TransactionOffConstants.TRANS_SAV);

						EShaktiApplication.getInstance().getTransactionManager().startTransaction(
								DataType.TRANS_VALUES_UPDATE, new TransactionUpdate(mUniqueId, mSavingsvalues));

						new TransactionSav_VSavUpdate(mUniqueId, mSavingsvalues, mVSavingsvalues);

					} else if (mSavingValues_offline != null && mVSavings_offline != null) {

						if (confirmationDialog.isShowing() && confirmationDialog != null) {
							confirmationDialog.dismiss();
						}

						TastyToast.makeText(getActivity(), AppStrings.offlineDataAvailable, TastyToast.LENGTH_SHORT,
								TastyToast.WARNING);

						DatePickerDialog.sDashboardDate = SelectedGroupsTask.sLastTransactionDate_Response;
						MainFragment_Dashboard fragment = new MainFragment_Dashboard();
						setFragment(fragment);

					} else {

						TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT,
								TastyToast.ERROR);

						MainFragment_Dashboard fragment = new MainFragment_Dashboard();
						setFragment(fragment);

					}
				} else {
					if (confirmationDialog.isShowing() && confirmationDialog != null) {
						confirmationDialog.dismiss();
					}

					TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT, TastyToast.ERROR);

					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);

				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		}
	}

	@Subscribe
	public void onAddTransactionSavings(final TransactionResponse transactionResponse) {
		switch (transactionResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), transactionResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), transactionResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			try {
				TransactionProvider.getSinlgeGroupMaster_Transaction(
						new Transaction_UniqueIdSingle(PrefUtils.getOfflineUniqueID()));

				if (GetTransactionSinglevalues.getSavings() != null
						&& GetTransactionSinglevalues.getSavings().equals(mSqliteDBStoredValues_Savings)
						&& !GetTransactionSinglevalues.getSavings().equals("")
						&& GetTransactionSinglevalues.getVolunteerSavings() != null
						&& GetTransactionSinglevalues.getVolunteerSavings().equals(mSqliteDBStoredValues_VSavings)
						&& !GetTransactionSinglevalues.getVolunteerSavings().equals("")) {
					String mCashinHand = String.valueOf(
							Integer.parseInt(SelectedGroupsTask.sCashinHand) + sSavings_Total + sVSavings_Total);
					String mTransactionDate = DatePickerDialog.sSend_To_Server_Date;
					String mCashatBank = SelectedGroupsTask.sCashatBank;
					String mBankDetails = GetOfflineTransactionValues.getBankDetails();
					SelectedGroupsTask.sCashinHand = mCashinHand;
					SelectedGroupsTask.sLastTransactionDate_Response = mTransactionDate;
					String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mTransactionDate,
							mCashinHand, mCashatBank, mBankDetails);
					String mSelectedGroupId = SelectedGroupsTask.Group_Id;

					System.out.println("-------onAddTransactionSavings--------");
					Log.e("mSelectedGroupId", mSelectedGroupId);
					Log.e("mGroupMasterResponse", mGroupMasterResponse);
					System.out.println("------------------");
					EShaktiApplication.getInstance().getTransactionManager().startTransaction(
							DataType.GROUPMASTERUPDATE, new GroupDetailsUpdate(mSelectedGroupId, mGroupMasterResponse));

				} else {
					if (confirmationDialog.isShowing() && confirmationDialog != null) {
						confirmationDialog.dismiss();
					}

					TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT, TastyToast.ERROR);

					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);

				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		}
	}

	@Subscribe
	public void OnTransUpdate(final TransactionUpdateResponse transactionUpdateResponse) {
		switch (transactionUpdateResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), transactionUpdateResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), transactionUpdateResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			try {
				TransactionProvider.getSinlgeGroupMaster_Transaction(
						new Transaction_UniqueIdSingle(PrefUtils.getOfflineUniqueID()));

				if (GetTransactionSinglevalues.getSavings() != null
						&& GetTransactionSinglevalues.getSavings().equals(mSqliteDBStoredValues_Savings)
						&& !GetTransactionSinglevalues.getSavings().equals("")
						&& GetTransactionSinglevalues.getVolunteerSavings() != null
						&& GetTransactionSinglevalues.getVolunteerSavings().equals(mSqliteDBStoredValues_VSavings)
						&& !GetTransactionSinglevalues.getVolunteerSavings().equals("")) {

					confirmationDialog.dismiss();

					String mCashinHand = String.valueOf(
							Integer.parseInt(SelectedGroupsTask.sCashinHand) + sSavings_Total + sVSavings_Total);
					String mTransactionDate = DatePickerDialog.sSend_To_Server_Date;
					String mCashatBank = SelectedGroupsTask.sCashatBank;
					String mBankDetails = GetOfflineTransactionValues.getBankDetails();
					SelectedGroupsTask.sCashinHand = mCashinHand;
					SelectedGroupsTask.sLastTransactionDate_Response = mTransactionDate;
					String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mTransactionDate,
							mCashinHand, mCashatBank, mBankDetails);
					Log.e(TAG, mGroupMasterResponse);
					Log.i(TAG + "Bank Detailssss", mBankDetails);

					System.out.println("---------OnTransUpdate---------");
					Log.e("mGroupMasterResponse", mGroupMasterResponse);
					System.out.println("----------------------");
					String mSelectedGroupId = SelectedGroupsTask.Group_Id;
					EShaktiApplication.getInstance().getTransactionManager().startTransaction(
							DataType.GROUPMASTERUPDATE, new GroupDetailsUpdate(mSelectedGroupId, mGroupMasterResponse));

					PrefUtils.setStatus_Calculation_Success("true");

				} else {
					if (confirmationDialog.isShowing() && confirmationDialog != null) {
						confirmationDialog.dismiss();
					}

					TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT, TastyToast.ERROR);

					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);

				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		}
	}

	@Subscribe
	public void OnGroupMasUpdate(final GroupMasUpdateResponse masterResponse) {
		switch (masterResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), masterResponse.getFetcherResult().getMessage(), TastyToast.LENGTH_SHORT,
					TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), masterResponse.getFetcherResult().getMessage(), TastyToast.LENGTH_SHORT,
					TastyToast.ERROR);
			break;
		case SUCCESS:

			try {
				confirmationDialog.dismiss();

				System.out.println("------OnGroupMasUpdate--------");

				TastyToast.makeText(getActivity(), AppStrings.transactionCompleted, TastyToast.LENGTH_SHORT,
						TastyToast.SUCCESS);

				if (EShaktiApplication.isDefault()) {

					EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.DEFAULT_PLOS,
							new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));

				} else {

					isServiceCall = false;

					if (!ConnectionUtils.isNetworkAvailable(getActivity())) {

						String mValues = Put_DB_GroupNameDetail_Response
								.put_DB_GroupNameDetails_Response(EShaktiApplication.getGroupResponse());

						GroupProvider.updateGroupResponse(
								new GroupResponseUpdate(EShaktiApplication.getGroupId_GroupLastTransDate(), mValues));
					}

					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		}
	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub

		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

		FragmentDrawer.drawer_CashinHand.setText(RegionalConversion.getRegionalConversion(AppStrings.cashinhand)

				+ SelectedGroupsTask.sCashinHand);
		FragmentDrawer.drawer_CashinHand.setTypeface(LoginActivity.sTypeface);

		FragmentDrawer.drawer_CashatBank.setText(RegionalConversion.getRegionalConversion(AppStrings.cashatBank)

				+ SelectedGroupsTask.sCashatBank);
		FragmentDrawer.drawer_CashatBank.setTypeface(LoginActivity.sTypeface);

		FragmentDrawer.drawer_lastTR_Date
				.setText(RegionalConversion.getRegionalConversion(AppStrings.lastTransactionDate) + " : "
						+ SelectedGroupsTask.sLastTransactionDate_Response);
		FragmentDrawer.drawer_lastTR_Date.setTypeface(LoginActivity.sTypeface);

		UpdateCalendarMinMaxDateUtils.OnUpdateCalendarDate();

		Calendar calender = Calendar.getInstance();

		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String formattedDate = df.format(calender.getTime());
		EShaktiApplication.setLastTransDate_GroupId(SelectedGroupsTask.Group_Id);

		new GroupTempDBLastTransDate(EShaktiApplication.getLastTransDate_GroupId(),
				SelectedGroupsTask.sLastTransactionDate_Response, formattedDate);

		GroupProvider.updateGroupLastTransDateResponse();

	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		SBus.INST.unRegister(this);
	}

	@Subscribe
	public void OnGroupMasterPLOSResponse(final DefaultOffline_PLResponse groupPersonalloanOSResponse) {
		switch (groupPersonalloanOSResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), groupPersonalloanOSResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), groupPersonalloanOSResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			try {
				String mMemberLoanResponse = GetGroupMemberDetails.getPersonaloanOS();
				response = mMemberLoanResponse.split("#");

				for (int i = 0; i < response.length; i++) {
					System.out.println("Response : " + response[i]);

					System.out.println("LOAN ID : " + response[0]);
					System.out.println("LOAN ID : " + response[1]);
					Transaction_MemLoanRepaid_MenuFragment.mPersonalOS_offlineresponse = response[1];
				}

				isServiceCall = false;

				PrefUtils.setStepWiseScreenCount("2");
				Transaction_PersonalLoanRepaidFragment savingsFragment = new Transaction_PersonalLoanRepaidFragment();

				int mFragSavingTotal = 0;
				mFragSavingTotal = sSavings_Total + sVSavings_Total;
				EShaktiApplication.setStepwiseSavings(mFragSavingTotal);

				setFragment(savingsFragment);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		}

	}

}
