package com.yesteam.eshakti.view.fragment;


import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.R;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.DeactivateTask;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class Settings_DeactivateAccountFragment extends Fragment implements
		OnClickListener, TaskListener {

	private TextView mGroupName;
	Button mButton;

	private Dialog mProgressDialog;

	public Settings_DeactivateAccountFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}

	@Override
	public void onAttach(Context context) {
		// TODO Auto-generated method stub
		super.onAttach(context);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_deactivate,
				container, false);

		try {
			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String
					.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			/*
			 * mHeadertext = (TextView) rootView
			 * .findViewById(R.id.fragment_changepassword_headertext);
			 * mHeadertext.setText("DEACTIVATE ACCOUNT");
			 * mHeadertext.setTypeface(LoginActivity.sTypeface);
			 * mHeadertext.setTextColor(color.header_color);
			 */
			mButton = (Button) rootView
					.findViewById(R.id.fragment_deactivate_button);
			mButton.setText(RegionalConversion.getRegionalConversion(AppStrings.deactivateAccount));
			mButton.setTypeface(LoginActivity.sTypeface, Typeface.BOLD);
			mButton.setOnClickListener(this);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return rootView;
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		if (ConnectionUtils.isNetworkAvailable(getActivity())) {

			try {

				new DeactivateTask(Settings_DeactivateAccountFragment.this).execute();

			} catch (Exception e) {
				e.printStackTrace();
			}

		} else if (!ConnectionUtils.isNetworkAvailable(getActivity())) {
		/*	if (YesBooksApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
			Toast toast = new Toast(getActivity());
			toast.setView(ToastUtil.getToast(getActivity(),
					AppStrings.deactivateNetworkAlert, Color.RED));
			toast.setDuration(Toast.LENGTH_SHORT);
			toast.show();
			} else {
				Toast toast = new Toast(getActivity());
				toast.setView(ToastUtil.getToast(getActivity(), AppStrings.mLoginAlert_ONOFF, Color.RED));
				toast.setDuration(Toast.LENGTH_SHORT);
				toast.show();
			}*/
		}
	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub
		mProgressDialog = AppDialogUtils.createProgressDialog(getActivity());
		mProgressDialog.show();
	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub
		if (mProgressDialog != null) {
			mProgressDialog.dismiss();

			if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {
				getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub

						if (!result.equals("FAIL")) {

							TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg, TastyToast.LENGTH_SHORT,
									TastyToast.ERROR);
							Constants.NETWORKCOMMONFLAG = "SUCCESS";
						}

					}
				});
			}else {
			if (DeactivateTask.sDeactivate_Response.equals("Yes")) {

				TastyToast.makeText(getActivity(), AppStrings.deactivateAccount_SuccessAlert, TastyToast.LENGTH_SHORT,
						TastyToast.SUCCESS);
				
			} else {

				TastyToast.makeText(getActivity(), DeactivateTask.sDeactivate_Response, TastyToast.LENGTH_SHORT,
						TastyToast.ERROR);
			}

			MainFragment_Dashboard fragment = new MainFragment_Dashboard();
			setFragment(fragment);
			}

		}
	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub
		Log.e("Current Class Name!!!!!!!!!!!!!!", fragment.getClass().getName());
		getActivity()
				.getSupportFragmentManager()
				.beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0,
						R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

	}

}
