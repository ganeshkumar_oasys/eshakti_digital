package com.yesteam.eshakti.view.fragment;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.squareup.otto.Subscribe;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.TransactionOffConstants;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog.OnDateSetListener;
import com.yesteam.eshakti.sqlite.database.GroupProvider;
import com.yesteam.eshakti.sqlite.database.response.GetSingleTransResponse;
import com.yesteam.eshakti.sqlite.database.response.TransactionResponse;
import com.yesteam.eshakti.sqlite.database.response.TransactionUpdateResponse;
import com.yesteam.eshakti.sqlite.db.model.GetTransactionSingle;
import com.yesteam.eshakti.sqlite.db.model.GetTransactionSinglevalues;
import com.yesteam.eshakti.sqlite.db.model.GroupTempDBLastTransDate;
import com.yesteam.eshakti.sqlite.db.model.Transaction;
import com.yesteam.eshakti.sqlite.db.model.TransactionUpdate;
import com.yesteam.eshakti.sqlite.db.transactions.TransactionManager.DataType;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.CalculateDateUtils;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.utils.Get_Offline_MobileDate;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.utils.Reset;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.utils.UpdateCalendarMinMaxDateUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.Get_TrainingTask;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Meeting_TrainingFragment extends Fragment implements OnClickListener, TaskListener, OnDateSetListener {

	public static final String TAG = Meeting_AuditingFragment.class.getSimpleName();
	private TextView mGroupName, mCashInHand, mCashAtBank, mHeadertext;
	private Button mRaised_Submit_Button, mEdit_RaisedButton, mOk_RaisedButton;
	private Dialog mProgressDialog;
	public static String training_check = "";
	private TextView mTrainingDate_Text;
	public static TextView mTrainingDate_editText;
	private CheckBox mcheckBox[];
	private LinearLayout mLayout;
	public static String trainingDate = "", trainingType = "";
	public static String checkedTrainingType[];
	int size;
	String trainingEditTextValue;
	String mLastTrDate = null, mLastTr_ID = null;
	Dialog confirmationDialog;
	private String mLanguageLocale = "";
	Locale locale;
	boolean isServiceCall = false;

	public Meeting_TrainingFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_TRAINING;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		SBus.INST.register(this);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		trainingType = "";
		training_check = "";
		trainingEditTextValue = "";
		checkedTrainingType = new String[] { "" };

	}

	@Override
	public void onAttach(Context context) {
		// TODO Auto-generated method stub
		super.onAttach(context);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_training, container, false);

		MainFragment_Dashboard.isBackpressed = false;

		try {
			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashInHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashInHand.setTypeface(LoginActivity.sTypeface);

			mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashAtBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashAtBank.setTypeface(LoginActivity.sTypeface);

			mHeadertext = (TextView) rootView.findViewById(R.id.fragment_train_headertext);
			mHeadertext.setText(RegionalConversion.getRegionalConversion(AppStrings.training));
			mHeadertext.setTypeface(LoginActivity.sTypeface);

			mRaised_Submit_Button = (Button) rootView.findViewById(R.id.fragment_training_Submitbutton);
			mRaised_Submit_Button.setText(RegionalConversion.getRegionalConversion(AppStrings.submit));
			mRaised_Submit_Button.setTypeface(LoginActivity.sTypeface);
			mRaised_Submit_Button.setOnClickListener(this);

			mTrainingDate_Text = (TextView) rootView.findViewById(R.id.trainingDateText);
			mTrainingDate_Text.setText(RegionalConversion.getRegionalConversion(AppStrings.training_Date));
			mTrainingDate_Text.setTypeface(LoginActivity.sTypeface);
			mTrainingDate_Text.setTextColor(color.black);

			mTrainingDate_editText = (TextView) rootView.findViewById(R.id.trainingeditText);
			mTrainingDate_editText.setText(Meeting_TrainingFragment.trainingDate);
			mTrainingDate_editText.setGravity(Gravity.CENTER);
			mTrainingDate_editText.setOnClickListener(this);

			mLayout = (LinearLayout) rootView.findViewById(R.id.training_linear);

			size = AppStrings.training_types.length;
			System.out.println("Size:" + size);

			mcheckBox = new CheckBox[size];

			for (int i = 0; i < size; i++) {

				LinearLayout linearLayout = new LinearLayout(getActivity());
				linearLayout.setOrientation(LinearLayout.HORIZONTAL);
				mcheckBox[i] = new CheckBox(getActivity());
				mcheckBox[i].setChecked(false);
				mcheckBox[i].setBackgroundResource(R.drawable.btn_check_to_off_mtrl_013);
				linearLayout.addView(mcheckBox[i]);

				LayoutParams lParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

				TextView trainingTypes = new TextView(getActivity());
				trainingTypes.setText(
						GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.training_types[i])));
				trainingTypes.setTypeface(LoginActivity.sTypeface);
				trainingTypes.setPadding(0, 5, 5, 5);
				trainingTypes.setLayoutParams(lParams);
				linearLayout.addView(trainingTypes);

				mLayout.addView(linearLayout);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rootView;
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		try {

			// trainingType = "";

			if (v.getId() == R.id.trainingeditText) {

				training_check = "1";

				/*
				 * CalendarFragment fragment = new CalendarFragment(); setFragment(fragment);
				 */

				EShaktiApplication.setAudit_TrainingFragment(true);

				onSetCalendarValues();

				try {
					Calendar calender = Calendar.getInstance();

					SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
					String formattedDate = df.format(calender.getTime());
					Log.e("Device Date  =  ", formattedDate + "");

					String mBalanceSheetDate = SelectedGroupsTask.sLastTransactionDate_Response; // dd/MM/yyyy
					String formatted_balancesheetDate = mBalanceSheetDate.replace("/", "-");
					Log.e("Balancesheet Date = ", formatted_balancesheetDate + "");

					Date balanceDate = df.parse(formatted_balancesheetDate);
					Date systemDate = df.parse(formattedDate);

					if (balanceDate.compareTo(systemDate) < 0 || balanceDate.compareTo(systemDate) == 0) {

						calendarDialogShow();
					} else {
						TastyToast.makeText(getActivity(), "PLEASE SET YOUR DEVICE DATE CORRECTLY",
								TastyToast.LENGTH_SHORT, TastyToast.ERROR);
						EShaktiApplication.setNextMonthLastDate(null);
						EShaktiApplication.setCalendarDateVisibleFlag(true);
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}

			} else if (v.getId() == R.id.fragment_training_Submitbutton) {
				trainingType = "";

				String trainingDateArr[] = mTrainingDate_editText.getText().toString().split("/");
				trainingEditTextValue = trainingDateArr[1] + "/" + trainingDateArr[0] + "/" + trainingDateArr[2];
				System.out.println("training Date:" + trainingEditTextValue);

				if (trainingEditTextValue.equals("")) {
					trainingEditTextValue = null;
				}

				for (int i = 0; i < size; i++) {
					if (mcheckBox[i].isChecked()) {
						trainingType = trainingType + AppStrings.training_types[i] + "~";
					}
				}

				System.out.println("Checked Training types:" + trainingType.toString());

				if (trainingEditTextValue != null && (!trainingType.equals(""))) {

					confirmationDialog = new Dialog(getActivity());

					LayoutInflater inflater = getActivity().getLayoutInflater();
					View dialogView = inflater.inflate(R.layout.dialog_confirmation, null);
					dialogView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
							LayoutParams.WRAP_CONTENT));

					TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
					confirmationHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.confirmation));
					confirmationHeader.setTypeface(LoginActivity.sTypeface);

					TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

					TableRow trainingRow = new TableRow(getActivity());

					@SuppressWarnings("deprecation")
					TableRow.LayoutParams contentTrainingParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
							LayoutParams.WRAP_CONTENT, 1f);
					contentTrainingParams.setMargins(10, 5, 10, 5);

					TextView trainingDateText = new TextView(getActivity());
					trainingDateText.setText(
							GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.training_Date)));
					trainingDateText.setTypeface(LoginActivity.sTypeface);
					trainingDateText.setTextColor(color.black);
					trainingDateText.setLayoutParams(contentTrainingParams);
					trainingRow.addView(trainingDateText);

					TextView trainingDate = new TextView(getActivity());
					trainingDate.setText(
							GetSpanText.getSpanString(getActivity(), String.valueOf(mTrainingDate_editText.getText())));
					trainingDate.setTextColor(color.black);
					trainingDate.setPadding(10, 5, 5, 5);
					trainingDate.setLayoutParams(contentTrainingParams);
					trainingRow.addView(trainingDate);

					confirmationTable.addView(trainingRow,
							new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

					try {
						checkedTrainingType = trainingType.split("~");
						CheckBox checkBox[] = new CheckBox[checkedTrainingType.length];
						for (int j = 0; j < checkedTrainingType.length; j++) {
							TableRow indv_memberNameRow = new TableRow(getActivity());

							TableRow.LayoutParams contentParams = new TableRow.LayoutParams(0,
									LayoutParams.WRAP_CONTENT, 1f);
							contentParams.setMargins(10, 5, 10, 5);

							checkBox[j] = new CheckBox(getActivity());
							checkBox[j].setText(
									GetSpanText.getSpanString(getActivity(), String.valueOf(checkedTrainingType[j])));
							checkBox[j].setChecked(true);
							checkBox[j].setClickable(false);
							checkBox[j].setTextColor(color.black);
							checkBox[j].setTypeface(LoginActivity.sTypeface);
							indv_memberNameRow.addView(checkBox[j]);

							confirmationTable.addView(indv_memberNameRow,
									new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
						}

					} catch (Exception e) {
						// TODO: handle exception
						System.out.println("checkbox layout error:" + e.toString());
					}

					mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit_button);
					mEdit_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.edit));
					mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
					mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
																			// 205,
																			// 0));
					mEdit_RaisedButton.setOnClickListener(this);

					mOk_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Ok_button);
					mOk_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.yes));
					mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
					mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
					mOk_RaisedButton.setOnClickListener(this);

					confirmationDialog.getWindow()
							.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
					confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					confirmationDialog.setCanceledOnTouchOutside(false);
					confirmationDialog.setContentView(dialogView);
					confirmationDialog.setCancelable(true);
					confirmationDialog.show();

					MarginLayoutParams margin = (MarginLayoutParams) dialogView.getLayoutParams();
					margin.leftMargin = 10;
					margin.rightMargin = 10;
					margin.topMargin = 10;
					margin.bottomMargin = 10;
					margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);

				} else {
					if (trainingEditTextValue == null) {

						TastyToast.makeText(getActivity(), AppStrings.trainingDateAlert, TastyToast.LENGTH_SHORT,
								TastyToast.WARNING);

						// trainingType = "";

					} else if (trainingType != null) {

						TastyToast.makeText(getActivity(), AppStrings.trainingTypeAlert, TastyToast.LENGTH_SHORT,
								TastyToast.WARNING);
					}
				}
			} else if (v.getId() == R.id.fragment_Edit_button) {
				trainingType = "";
				mRaised_Submit_Button.setClickable(true);
				confirmationDialog.dismiss();
				isServiceCall = false;
			} else if (v.getId() == R.id.fragment_Ok_button) {
				// call service

				if (ConnectionUtils.isNetworkAvailable(getActivity())) {
					try {
						if (EShaktiApplication.getLoginFlag().equals(Constants.ONLINEFLAG)) {
							if (!isServiceCall) {
								isServiceCall = true;
								new Get_TrainingTask(Meeting_TrainingFragment.this).execute();
							}
						} else {

							TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
									TastyToast.ERROR);
						}
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
				} else {

					// Do offline

					if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
						EShaktiApplication.getInstance().getTransactionManager().startTransaction(
								DataType.GET_TRANS_SINGLE, new GetTransactionSingle(PrefUtils.getOfflineUniqueID()));
					} else {

						TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
								TastyToast.ERROR);
					}
				}
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	private void calendarDialogShow() {
		// TODO Auto-generated method stub

		mLanguageLocale = PrefUtils.getUserlangcode();
		if (mLanguageLocale.equalsIgnoreCase("English")) {
			locale = new Locale("en");
		} else if (mLanguageLocale.equalsIgnoreCase("Tamil")) {
			locale = new Locale("ta");
		} else if (mLanguageLocale.equalsIgnoreCase("Hindi")) {
			locale = new Locale("hi");
		} else if (mLanguageLocale.equalsIgnoreCase("Marathi")) {
			locale = new Locale("ma");
		} else if (mLanguageLocale.equalsIgnoreCase("Malayalam")) {
			locale = new Locale("ml");
		} else if (mLanguageLocale.equalsIgnoreCase("Kannada")) {
			locale = new Locale("kn");
		} else if (mLanguageLocale.equalsIgnoreCase("Bengali")) {
			locale = new Locale("bn");
		} else if (mLanguageLocale.equalsIgnoreCase("Gujarati")) {
			locale = new Locale("gu");
		} else if (mLanguageLocale.equalsIgnoreCase("Punjabi")) {
			locale = new Locale("pa");
		} else if (mLanguageLocale.equalsIgnoreCase("Assamese")) {
			locale = new Locale("as");
		} else {
			locale = new Locale("en");
		}
		locale = new Locale("en");
		Locale.setDefault(locale);
		Configuration config = new Configuration();
		config.locale = locale;
		getActivity().getResources().updateConfiguration(config, getActivity().getResources().getDisplayMetrics());

		OnDateSetListener datelistener = Meeting_TrainingFragment.this;
		Calendar now = Calendar.getInstance();
		FragmentManager fm = getFragmentManager();
		DatePickerDialog dialog = DatePickerDialog.newInstance(datelistener, now.get(Calendar.YEAR),
				now.get(Calendar.MONDAY), now.get(Calendar.DAY_OF_MONTH));

		/*
		 * String balancesheet_date =
		 * String.valueOf(SelectedGroupsTask.sBalanceSheetDate_Response); String
		 * dateArr[] = balancesheet_date.split("/");
		 */
		Calendar min_Cal = Calendar.getInstance();

		/*
		 * int day = Integer.parseInt(dateArr[0]); int month =
		 * Integer.parseInt(dateArr[1]); int year = Integer.parseInt(dateArr[2]);
		 * 
		 * min_Cal.set(year, (month - 1), day); dialog.setMinDate(min_Cal);
		 */ // dialog.setMaxDate(now);

		String lastTransactionDateArr[] = SelectedGroupsTask.sLastTransactionDate_Response.split("/");
		min_Cal.set(Integer.parseInt(lastTransactionDateArr[2]), Integer.parseInt(lastTransactionDateArr[1]) - 1,
				Integer.parseInt(lastTransactionDateArr[0]));
		dialog.setMinDate(min_Cal);
		EShaktiApplication.setCalendarDialog_MinDate(SelectedGroupsTask.sLastTransactionDate_Response);

		Log.e("Calendar Last Month Date !!!!!!!!!! ", EShaktiApplication.getNextMonthLastDate() + "");
		String formattedDate_LastTransaction = EShaktiApplication.getNextMonthLastDate();
		DateFormat df_ = new SimpleDateFormat("dd-MM-yyyy");
		Calendar calendar = Calendar.getInstance();
		try {
			calendar.setTime(df_.parse(formattedDate_LastTransaction));
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Date nextMonthLastDay = calendar.getTime();
		System.out.println("------ Next month Last date   =  " + df_.format(nextMonthLastDay) + "");

		String nextMonthLastDay_str = df_.format(nextMonthLastDay);
		String dateArray[] = nextMonthLastDay_str.split("-");

		Calendar lastDate = Calendar.getInstance();
		lastDate.set(Integer.parseInt(dateArray[2]), Integer.parseInt(dateArray[1]) - 1,
				Integer.parseInt(dateArray[0]));

		dialog.setMaxDate(lastDate);
		dialog.show(fm, "");

	}

	@Subscribe
	public void OnGetSingleTransactionTraining(final GetSingleTransResponse getSingleTransResponse) {
		switch (getSingleTransResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), getSingleTransResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), getSingleTransResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			String mUniqueId = GetTransactionSinglevalues.getUniqueId();
			String mTrainingValues_Offline = GetTransactionSinglevalues.getTraining();

			String mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
			String mMobileDate = Get_Offline_MobileDate.getOffline_MobileDate(getActivity());
			String mTransaction_UniqueId = PrefUtils.getOfflineUniqueID();
			String Send_to_server_values_Offline = trainingEditTextValue + "~" + trainingType;

			mLastTrDate = DatePickerDialog.sDashboardDate;
			mLastTr_ID = SelectedGroupsTask.sTr_ID.toString();
			String mCurrentTransDate = null;

			if (publicValues.mOffline_Trans_CurrentDate != null) {
				mCurrentTransDate = publicValues.mOffline_Trans_CurrentDate;
			}
			String mTrainingvalues = Send_to_server_values_Offline + "#" + mTrasactiondate + "#" + mMobileDate;

			if (mUniqueId == null && mTrainingValues_Offline == null) {

				if (mTrasactiondate != null && mMobileDate != null && mTransaction_UniqueId != null
						&& mCurrentTransDate != null) {
					EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.TRANSACTIONADD,
							new Transaction(0, PrefUtils.getUsernameKey(), SelectedGroupsTask.UserName,
									SelectedGroupsTask.Ngo_Id, SelectedGroupsTask.Group_Id, mTransaction_UniqueId,
									mLastTr_ID, mCurrentTransDate, null, null, null, null, null, null, null, null, null,
									null, null, null, null, null, null, null, null, mTrainingvalues, null, null, null,
									null));
				} else {

					TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT, TastyToast.ERROR);
				}

			} else if (mUniqueId.equalsIgnoreCase(PrefUtils.getOfflineUniqueID()) && mTrainingValues_Offline == null) {
				EShaktiApplication.setSetTransValues(TransactionOffConstants.TRANS_TRAN);

				EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.TRANS_VALUES_UPDATE,
						new TransactionUpdate(mUniqueId, mTrainingvalues));

			} else if (mTrainingValues_Offline != null) {
				confirmationDialog.dismiss();
				isServiceCall = false;
				TastyToast.makeText(getActivity(), AppStrings.offlineDataAvailable, TastyToast.LENGTH_SHORT,
						TastyToast.WARNING);

				DatePickerDialog.sDashboardDate = SelectedGroupsTask.sLastTransactionDate_Response;
				MainFragment_Dashboard fragment = new MainFragment_Dashboard();
				setFragment(fragment);
			}
			break;
		}
	}

	@Subscribe
	public void OnTransUpdateTraining(final TransactionUpdateResponse transactionUpdateResponse) {
		switch (transactionUpdateResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), transactionUpdateResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), transactionUpdateResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			confirmationDialog.dismiss();
			isServiceCall = false;

			TastyToast.makeText(getActivity(), AppStrings.transactionCompleted, TastyToast.LENGTH_SHORT,
					TastyToast.SUCCESS);

			/*
			 * String[] lastTrDate = DatePickerDialog.sSend_To_Server_Date.split("/");
			 * String mTrasactiondate = lastTrDate[1] + "/" + lastTrDate[0] + "/" +
			 * lastTrDate[2]; SelectedGroupsTask.sLastTransactionDate_Response =
			 * mTrasactiondate;
			 * FragmentDrawer.drawer_lastTR_Date.setText(RegionalConversion.
			 * getRegionalConversion(AppStrings.lastTransactionDate) + " : " +
			 * SelectedGroupsTask.sLastTransactionDate_Response);
			 * FragmentDrawer.drawer_lastTR_Date.setTypeface(LoginActivity.sTypeface);
			 */

			MainFragment_Dashboard fragment = new MainFragment_Dashboard();
			setFragment(fragment);

			trainingDate = Reset.reset(trainingDate);

			break;
		}
	}

	@Subscribe
	public void onAddTransactionTraining(final TransactionResponse transactionResponse) {
		switch (transactionResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), transactionResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), transactionResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:
			confirmationDialog.dismiss();
			isServiceCall = false;

			TastyToast.makeText(getActivity(), AppStrings.transactionCompleted, TastyToast.LENGTH_SHORT,
					TastyToast.SUCCESS);

			/*
			 * String[] lastTrDate = DatePickerDialog.sSend_To_Server_Date.split("/");
			 * String mTrasactiondate = lastTrDate[1] + "/" + lastTrDate[0] + "/" +
			 * lastTrDate[2]; SelectedGroupsTask.sLastTransactionDate_Response =
			 * mTrasactiondate;
			 * FragmentDrawer.drawer_lastTR_Date.setText(RegionalConversion.
			 * getRegionalConversion(AppStrings.lastTransactionDate) + " : " +
			 * SelectedGroupsTask.sLastTransactionDate_Response);
			 * FragmentDrawer.drawer_lastTR_Date.setTypeface(LoginActivity.sTypeface);
			 */

			MainFragment_Dashboard fragment = new MainFragment_Dashboard();
			setFragment(fragment);
			trainingDate = Reset.reset(trainingDate);
			break;
		}
	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub
		mProgressDialog = AppDialogUtils.createProgressDialog(getActivity());
		mProgressDialog.show();
	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub
		try {
			if (mProgressDialog != null) {
				mProgressDialog.dismiss();

				if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {
					getActivity().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub

							if (!result.equals("FAIL")) {
								isServiceCall = false;
								TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg,
										TastyToast.LENGTH_SHORT, TastyToast.ERROR);
								Constants.NETWORKCOMMONFLAG = "SUCESS";
							}

						}
					});

				} else {

					if (Get_TrainingTask.sTraining_Response.equals("Transaction Successful")) {

						TastyToast.makeText(getActivity(), AppStrings.transactionCompleted, TastyToast.LENGTH_SHORT,
								TastyToast.SUCCESS);
					} else {

						TastyToast.makeText(getActivity(), AppStrings.transactionFailAlert, TastyToast.LENGTH_SHORT,
								TastyToast.ERROR);
					}
					confirmationDialog.dismiss();
					isServiceCall = false;
					trainingDate = Reset.reset(trainingDate);
					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		SBus.INST.unRegister(this);
		EShaktiApplication.setAudit_TrainingFragment(false);
	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub

		EShaktiApplication.setAudit_TrainingFragment(false);
		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

		FragmentDrawer.drawer_lastTR_Date
				.setText(RegionalConversion.getRegionalConversion(AppStrings.lastTransactionDate) + " : "
						+ SelectedGroupsTask.sLastTransactionDate_Response);
		FragmentDrawer.drawer_lastTR_Date.setTypeface(LoginActivity.sTypeface);

		UpdateCalendarMinMaxDateUtils.OnUpdateCalendarDate();

		Calendar calender = Calendar.getInstance();

		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String formattedDate = df.format(calender.getTime());
		EShaktiApplication.setLastTransDate_GroupId(SelectedGroupsTask.Group_Id);

		String lastTrDateArr[] = DatePickerDialog.sDashboardDate.split("-");
		String lastTrDate = lastTrDateArr[0] + "/" + lastTrDateArr[1] + "/" + lastTrDateArr[2];

		new GroupTempDBLastTransDate(EShaktiApplication.getLastTransDate_GroupId(), lastTrDate, formattedDate);

		GroupProvider.updateGroupLastTransDateResponse();

	}

	@Override
	public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
		// TODO Auto-generated method stub

	}

	private void onSetCalendarValues() {
		// TODO Auto-generated method stub

		if (EShaktiApplication.getNextMonthLastDate() == null) {
			try {
				if (EShaktiApplication.isCalendarDateVisibleFlag()) {
					EShaktiApplication.setCalendarDateVisibleFlag(false);
				}

				Calendar calender = Calendar.getInstance();

				SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String formattedDate = df.format(calender.getTime());
				Log.e("Device Date  =  ", formattedDate + "");

				String mBalanceSheetDate = SelectedGroupsTask.sLastTransactionDate_Response; // dd/MM/yyyy
				String formatted_balancesheetDate = mBalanceSheetDate.replace("/", "-");
				Log.e("Balancesheet Date = ", formatted_balancesheetDate + "");

				Date balanceDate = df.parse(formatted_balancesheetDate);
				Date systemDate = df.parse(formattedDate);

				if (balanceDate.compareTo(systemDate) < 0 || balanceDate.compareTo(systemDate) == 0) {
					System.err.println(" balancesheet date is less than sys date");

					String mLastTransactionDate = SelectedGroupsTask.sLastTransactionDate_Response;

					String formattedDate_LastTransaction = mLastTransactionDate.replace("/", "-");

					String days = CalculateDateUtils.get_count_of_days(formattedDate_LastTransaction, formattedDate);

					int mDaycount = Integer.parseInt(days);
					Log.e("Total Days Count =====_____----", mDaycount + "");

					EShaktiApplication.setLastTransDate_GroupId(SelectedGroupsTask.Group_Id);

					if (mDaycount > 30) {

						SimpleDateFormat df_after = new SimpleDateFormat("dd-MM-yyyy");

						Calendar calendar = Calendar.getInstance();
						calendar.setTime(df_after.parse(formattedDate_LastTransaction));
						calendar.add(Calendar.MONTH, 1);
						calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
						Date nextMonthFirstDay = calendar.getTime();
						System.out.println(
								"------ Next month first date   =  " + df_after.format(nextMonthFirstDay) + "");

						Calendar cal_last_date = Calendar.getInstance();
						cal_last_date.setTime(df_after.parse(formattedDate_LastTransaction));
						cal_last_date.add(Calendar.MONTH, 1);
						cal_last_date.set(Calendar.DATE, cal_last_date.getActualMaximum(Calendar.DAY_OF_MONTH));
						Date nextMonthLastDay = cal_last_date.getTime();
						System.out
								.println("------ Next month Last date   =  " + df_after.format(nextMonthLastDay) + "");

						//
						String lastDateOfNextMonth = df_after.format(nextMonthLastDay);
						Calendar currentDate = Calendar.getInstance();
						int current_Day = currentDate.get(Calendar.DAY_OF_MONTH);
						int current_month = currentDate.get(Calendar.MONTH) + 1;
						int current_year = currentDate.get(Calendar.YEAR);
						String current_date = current_Day + "-" + current_month + "-" + current_year;

						Date lastdate = null, currentdate = null;
						String min_date_str = null;

						try {

							lastdate = df_after.parse(lastDateOfNextMonth);
							currentdate = df_after.parse(current_date);

						} catch (ParseException e) {
							// TODO: handle exception
							e.printStackTrace();
						}

						if (lastdate.compareTo(currentdate) <= 0) {
							min_date_str = lastDateOfNextMonth;
						} else {
							min_date_str = current_date;
						}

						Log.e("Minimum Date !!!!!	", min_date_str + "");

						EShaktiApplication.setNextMonthLastDate(min_date_str);

					} else {
						Calendar currentDate = Calendar.getInstance();
						SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
						EShaktiApplication.setNextMonthLastDate(sdf.format(currentDate.getTime()));

					}
				}

			} catch (Exception e) {
				// TODO: handle exception
			}
		}

	}

}
