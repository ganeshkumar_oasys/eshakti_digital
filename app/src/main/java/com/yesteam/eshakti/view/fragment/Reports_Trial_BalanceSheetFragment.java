package com.yesteam.eshakti.view.fragment;

import java.util.Calendar;
import java.util.Locale;

import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog.OnDateSetListener;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.Get_BalanceSheetTask;
import com.yesteam.eshakti.webservices.Get_TrialBalanceTask;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Dialog;
import android.content.res.Configuration;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class Reports_Trial_BalanceSheetFragment extends Fragment
		implements OnClickListener, TaskListener, OnDateSetListener {

	private TextView mGroupName, mCashInHand, mCashAtBank, mFromDate, mToDate;
	public static TextView mFromDateEdit_Text, mToDateEdit_Text;
	private Button mRaised_Submit_Button;
	private Dialog mProgressDialog;

	public static String fromDate = "", toDate = "";
	private boolean isBalanceSheetReport = false;
	private boolean isTrialBalanceReport = false;
	String offlineServiceResponse;

	public static String trial_balance_check = "";
	private String mLanguageLocale = "";
	Locale locale;

	public Reports_Trial_BalanceSheetFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_BALANCE_SHEET_TRAIL;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		MainFragment_Dashboard.sSubMenu_Item = "";
		FragmentDrawer.sSubMenu_Item = "";
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_trialdate, container, false);

		MainFragment_Dashboard.isBackpressed = false;

		try {
			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashInHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashInHand.setTypeface(LoginActivity.sTypeface);

			mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashAtBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashAtBank.setTypeface(LoginActivity.sTypeface);

			mFromDate = (TextView) rootView.findViewById(R.id.FromDateText);
			mFromDate.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.FromDate)));
			mFromDate.setTypeface(LoginActivity.sTypeface);
			mFromDate.setTextColor(color.black);

			mToDate = (TextView) rootView.findViewById(R.id.ToDateText);
			mToDate.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.ToDate)));
			mToDate.setTypeface(LoginActivity.sTypeface);
			mToDate.setTextColor(color.black);

			mFromDateEdit_Text = (TextView) rootView.findViewById(R.id.fromDateEditText);
			mFromDateEdit_Text.setText(Reports_Trial_BalanceSheetFragment.fromDate);
			mFromDateEdit_Text.setGravity(Gravity.CENTER);
			mFromDateEdit_Text.setPadding(10, 0, 10, 0);
			mFromDateEdit_Text.setOnClickListener(this);

			mToDateEdit_Text = (TextView) rootView.findViewById(R.id.toDateEditText);
			mToDateEdit_Text.setText(Reports_Trial_BalanceSheetFragment.toDate);
			mToDateEdit_Text.setGravity(Gravity.CENTER);
			mToDateEdit_Text.setPadding(10, 0, 10, 0);
			mToDateEdit_Text.setOnClickListener(this);

			mRaised_Submit_Button = (Button) rootView.findViewById(R.id.fragment_trialbalance_Submitbutton);
			mRaised_Submit_Button.setText(RegionalConversion.getRegionalConversion(AppStrings.submit));
			mRaised_Submit_Button.setTypeface(LoginActivity.sTypeface);
			mRaised_Submit_Button.setOnClickListener(this);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return rootView;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		try {
			if (v.getId() == R.id.fromDateEditText) {
				trial_balance_check = "1";

				calendarDialogShow();

			} else if (v.getId() == R.id.toDateEditText) {
				trial_balance_check = "2";

				if (mFromDateEdit_Text.getText().toString().equals("")
						|| mFromDateEdit_Text.getText().toString().equals(null)) {

					TastyToast.makeText(getActivity(), AppStrings.previous_DateAlert, TastyToast.LENGTH_SHORT,
							TastyToast.WARNING);
				} else {
					calendarDialogShow();
				}
			} else if (v.getId() == R.id.fragment_trialbalance_Submitbutton) {
				if (mFromDateEdit_Text.getText().toString().equals("")
						|| mFromDateEdit_Text.getText().toString().equals(null)
						|| mToDateEdit_Text.getText().toString().equals("")
						|| mToDateEdit_Text.getText().toString().equals(null)) {

					TastyToast.makeText(getActivity(), AppStrings.calFromToDateAlert, TastyToast.LENGTH_SHORT,
							TastyToast.WARNING);
				} else {
					if (Boolean.valueOf(ConnectionUtils.isNetworkAvailable(getActivity()))) {
						if (Boolean.valueOf(FragmentDrawer.isBalanceSheetReport)
								|| Boolean.valueOf(MainFragment_Dashboard.isBalanceSheetReport)) {

							MainFragment_Dashboard.isBalanceSheetReport = false;
							FragmentDrawer.isBalanceSheetReport = false;

							try {

								new Get_BalanceSheetTask(Reports_Trial_BalanceSheetFragment.this).execute();
							} catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();
								TastyToast.makeText(getActivity(), "Time Out Exception ", TastyToast.LENGTH_SHORT,
										TastyToast.ERROR);
								getActivity().finish();
							}
							isBalanceSheetReport = true;
						} else if (Boolean.valueOf(FragmentDrawer.isTrialBalanceReport)
								|| Boolean.valueOf(MainFragment_Dashboard.isTrialBalanceReport)) {

							MainFragment_Dashboard.isTrialBalanceReport = false;
							FragmentDrawer.isTrialBalanceReport = false;

							try {
								new Get_TrialBalanceTask(this).execute();
							} catch (Exception e) {
								e.printStackTrace();
								TastyToast.makeText(getActivity(), "Time Out Exception ", TastyToast.LENGTH_SHORT,
										TastyToast.ERROR);
								getActivity().finish();
							}
							isTrialBalanceReport = true;
						}
					} else {

						TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
								TastyToast.ERROR);
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	private void calendarDialogShow() {
		// TODO Auto-generated method stub
		mLanguageLocale = PrefUtils.getUserlangcode();
		if (mLanguageLocale.equalsIgnoreCase("English")) {
			locale = new Locale("en");
		} else if (mLanguageLocale.equalsIgnoreCase("Tamil")) {
			locale = new Locale("ta");
		} else if (mLanguageLocale.equalsIgnoreCase("Hindi")) {
			locale = new Locale("hi");
		} else if (mLanguageLocale.equalsIgnoreCase("Marathi")) {
			locale = new Locale("ma");
		} else if (mLanguageLocale.equalsIgnoreCase("Malayalam")) {
			locale = new Locale("ml");
		} else if (mLanguageLocale.equalsIgnoreCase("Kannada")) {
			locale = new Locale("kn");
		} else if (mLanguageLocale.equalsIgnoreCase("Bengali")) {
			locale = new Locale("bn");
		} else if (mLanguageLocale.equalsIgnoreCase("Gujarati")) {
			locale = new Locale("gu");
		} else if (mLanguageLocale.equalsIgnoreCase("Punjabi")) {
			locale = new Locale("pa");
		} else if (mLanguageLocale.equalsIgnoreCase("Assamese")) {
			locale = new Locale("as");
		} else {
			locale = new Locale("en");
		}
		locale = new Locale("en");
		Locale.setDefault(locale);
		Configuration config = new Configuration();
		config.locale = locale;
		getActivity().getResources().updateConfiguration(config, getActivity().getResources().getDisplayMetrics());

		OnDateSetListener datelistener = Reports_Trial_BalanceSheetFragment.this;
		Calendar now = Calendar.getInstance();
		FragmentManager fm = getFragmentManager();
		DatePickerDialog dialog = DatePickerDialog.newInstance(datelistener, now.get(Calendar.YEAR),
				now.get(Calendar.MONDAY), now.get(Calendar.DAY_OF_MONTH));

		String balancesheet_date = String.valueOf(SelectedGroupsTask.sBalanceSheetDate_Response);
		String dateArr[] = balancesheet_date.split("/");

		Calendar min_Cal = Calendar.getInstance();

		int day = Integer.parseInt(dateArr[0]);
		int month = Integer.parseInt(dateArr[1]);
		int year = Integer.parseInt(dateArr[2]);

		min_Cal.set(year, (month - 1), day);
		dialog.setMinDate(min_Cal);
		dialog.setMaxDate(now);

		dialog.setMaxDate(now);

		dialog.show(fm, "");
	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub
		mProgressDialog = AppDialogUtils.createProgressDialog(getActivity());
		mProgressDialog.show();
	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub
		if (mProgressDialog != null) {
			mProgressDialog.dismiss();

			if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {

				getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub

						if (!result.equals("FAIL")) {

							TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg,
									TastyToast.LENGTH_SHORT, TastyToast.ERROR);
							Constants.NETWORKCOMMONFLAG = "SUCESS";
						}

						if (isBalanceSheetReport) {
							MainFragment_Dashboard.isBalanceSheetReport = true;
							FragmentDrawer.isBalanceSheetReport = true;
						} else if (isTrialBalanceReport) {
							MainFragment_Dashboard.isTrialBalanceReport = true;
							FragmentDrawer.isTrialBalanceReport = true;
						}

					}
				});
			} else {

				try {
					if (Boolean.valueOf(isBalanceSheetReport)) {
						try {
							/*
							 * MainFragment_Dashboard.isBalanceSheetReport =
							 * false; FragmentDrawer.isBalanceSheetReport =
							 * false;
							 */

							isBalanceSheetReport = false;
							offlineServiceResponse = SelectedGroupsTask.sBalanceSheetDate_Response;
							System.out.println("offlineServiceResponse :" + offlineServiceResponse);
							Reports_BalanceSheetReportFragment fragment = new Reports_BalanceSheetReportFragment();
							setFragment(fragment);

						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}
					} else if (Boolean.valueOf(isTrialBalanceReport)) {
						try {
							/*
							 * MainFragment_Dashboard.isTrialBalanceReport =
							 * false; FragmentDrawer.isTrialBalanceReport =
							 * false;
							 */
							isTrialBalanceReport = false;
							offlineServiceResponse = Get_TrialBalanceTask.sTrailBalance_Response;

							System.out.println("offlineServiceResponse :" + offlineServiceResponse);
							Reports_TrialBalanceReportFragment fragment = new Reports_TrialBalanceReportFragment();
							setFragment(fragment);

						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		}

	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub
		Log.e("Current Class Name!!!!!!!!!!!!!!", fragment.getClass().getName());
		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

	}

	@Override
	public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
		// TODO Auto-generated method stub

	}
}
