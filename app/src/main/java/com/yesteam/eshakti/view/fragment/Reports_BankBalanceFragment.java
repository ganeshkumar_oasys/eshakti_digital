package com.yesteam.eshakti.view.fragment;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.Get_Bank_BalanceTask;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Reports_BankBalanceFragment extends Fragment {

	private TextView mGroupName, mCashInHand, mCashAtBank, mHeadertext;

	String response[];
	String mBalanceOnDate = "";
	String msavingsBook = "";
	String mSavingsBank = "";
	int mSavingsDifference;
	String mloanBook = "";
	String mloanBank = "";
	int mloanDifference;
	View rullerView;

	public Reports_BankBalanceFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_BANK_BALANCE;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		try {

			response = Get_Bank_BalanceTask.sGet_Bank_Balance_ServiceResponse.split("#");

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_bankbalance, container, false);

		MainFragment_Dashboard.isBackpressed = false;
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_BANK_BALANCE;

		try {

			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashInHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashInHand.setTypeface(LoginActivity.sTypeface);

			mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashAtBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashAtBank.setTypeface(LoginActivity.sTypeface);

			mHeadertext = (TextView) rootView.findViewById(R.id.fragment_Header);
			mHeadertext.setText(RegionalConversion.getRegionalConversion(AppStrings.bankBalance));
			mHeadertext.setTypeface(LoginActivity.sTypeface);

			String bankBalnce[] = response[0].split("~");

			mBalanceOnDate = bankBalnce[0];
			msavingsBook = bankBalnce[1];
			mSavingsBank = bankBalnce[2];

			TableLayout bankBalanceTable = (TableLayout) rootView.findViewById(R.id.fragment_BankBalance);

			TableRow row = new TableRow(getActivity());
			row.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
			row.setPadding(5, 5, 5, 5);

			TextView savingsBook = new TextView(getActivity());
			savingsBook.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.savingsBook)));
			savingsBook.setTypeface(LoginActivity.sTypeface);
			savingsBook.setTextColor(color.black);
			row.addView(savingsBook);

			TextView savingsBookValue = new TextView(getActivity());
			savingsBookValue.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(msavingsBook)));
			savingsBookValue.setTypeface(LoginActivity.sTypeface);
			savingsBookValue.setTextColor(color.black);
			savingsBookValue.setGravity(Gravity.RIGHT);
			row.addView(savingsBookValue);

			bankBalanceTable.addView(row,
					new TableLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));

			rullerView = new View(getActivity());
			rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
			rullerView.setBackgroundColor(Color.rgb(220, 220, 220));
			bankBalanceTable.addView(rullerView);

			/*
			 * TableRow secondRow = new TableRow(getActivity());
			 * secondRow.setLayoutParams(new
			 * LayoutParams(LayoutParams.FILL_PARENT,
			 * LayoutParams.WRAP_CONTENT)); secondRow.setPadding(5, 5, 5, 5);
			 * 
			 * TextView savingsBank = new TextView(getActivity());
			 * savingsBank.setText(GetSpanText.getSpanString(getActivity(),
			 * String.valueOf(AppStrings.savingsBank)));
			 * savingsBank.setTypeface(LoginActivity.sTypeface);
			 * savingsBank.setTextColor(color.black);
			 * secondRow.addView(savingsBank);
			 * 
			 * TextView savingsBankValue = new TextView(getActivity());
			 * savingsBankValue.setText(GetSpanText.getSpanString(getActivity(),
			 * String.valueOf(mSavingsBank)));
			 * savingsBankValue.setTypeface(LoginActivity.sTypeface);
			 * savingsBankValue.setTextColor(color.black);
			 * savingsBankValue.setGravity(Gravity.RIGHT);
			 * secondRow.addView(savingsBankValue);
			 * 
			 * bankBalanceTable.addView(secondRow, new
			 * TableLayout.LayoutParams(LayoutParams.FILL_PARENT,
			 * LayoutParams.WRAP_CONTENT));
			 * 
			 * rullerView = new View(getActivity());
			 * rullerView.setLayoutParams(new
			 * TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
			 * rullerView.setBackgroundColor(Color.rgb(220, 220, 220));
			 * bankBalanceTable.addView(rullerView);
			 */
			/*
			 * TableRow thirdRow = new TableRow(getActivity());
			 * thirdRow.setLayoutParams(new
			 * LayoutParams(LayoutParams.FILL_PARENT,
			 * LayoutParams.WRAP_CONTENT)); thirdRow.setPadding(5, 5, 5, 5);
			 * 
			 * TextView savingsDifference = new TextView(getActivity());
			 * savingsDifference.setText(GetSpanText.getSpanString(getActivity()
			 * , String.valueOf(AppStrings.difference)));
			 * savingsDifference.setTypeface(LoginActivity.sTypeface,
			 * Typeface.BOLD); savingsDifference.setTextColor(Color.BLACK);
			 * savingsDifference.setGravity(Gravity.RIGHT);
			 * thirdRow.addView(savingsDifference);
			 * 
			 * TextView savingsDifferenceValue = new TextView(getActivity());
			 * mSavingsDifference = Integer.parseInt(msavingsBook) -
			 * Integer.parseInt(mSavingsBank); savingsDifferenceValue.setText(
			 * GetSpanText.getSpanString(getActivity(),
			 * String.valueOf(String.valueOf(mSavingsDifference))));
			 * savingsDifferenceValue.setTypeface(LoginActivity.sTypeface,
			 * Typeface.BOLD); savingsDifferenceValue.setTextColor(Color.BLACK);
			 * savingsDifferenceValue.setGravity(Gravity.RIGHT);
			 * thirdRow.addView(savingsDifferenceValue);
			 * 
			 * bankBalanceTable.addView(thirdRow, new
			 * TableLayout.LayoutParams(LayoutParams.FILL_PARENT,
			 * LayoutParams.WRAP_CONTENT));
			 * 
			 * rullerView = new View(getActivity());
			 * rullerView.setLayoutParams(new
			 * TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
			 * rullerView.setBackgroundColor(Color.rgb(220, 220, 220));
			 * bankBalanceTable.addView(rullerView);
			 */
			TableLayout loanTable = (TableLayout) rootView.findViewById(R.id.fragment_LoanBalnce);

			String loanArr[] = Get_Bank_BalanceTask.sGet_Bank_Balance_ServiceResponse.split("~#");
			for (int i = 0; i < loanArr.length; i++) {
				System.out.println("loanArr[]" + loanArr[i] + " pos :" + i);
			}
			String loanArr1[] = null;
			try {
				loanArr1 = loanArr[1].split("~");
			} catch (ArrayIndexOutOfBoundsException e) {
				// TODO Auto-generated catch block
				System.out.println("ArrayIndexOutOfBoundsException" + e);
			}

			try {
				for (int i = 0; i < loanArr1.length; i = (i + 3)) {

					TableRow loanFirstRow = new TableRow(getActivity());
					loanFirstRow.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
					loanFirstRow.setPadding(10, 5, 10, 5);

					TextView loanBook = new TextView(getActivity());
					loanBook.setText(GetSpanText.getSpanString(getActivity(),
							String.valueOf(loanArr1[i] + " " + AppStrings.loanOutstandingBook)));
					loanBook.setTypeface(LoginActivity.sTypeface);
					loanBook.setTextColor(color.black);
					loanFirstRow.addView(loanBook);

					TextView loanBookValue = new TextView(getActivity());
					mloanBook = loanArr1[i + 1];
					loanBookValue.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(mloanBook)));
					loanBookValue.setTypeface(LoginActivity.sTypeface);
					loanBookValue.setTextColor(color.black);
					loanBookValue.setGravity(Gravity.RIGHT);
					loanFirstRow.addView(loanBookValue);

					loanTable.addView(loanFirstRow,
							new TableLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));

					rullerView = new View(getActivity());
					rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
					rullerView.setBackgroundColor(Color.rgb(220, 220, 220));
					loanTable.addView(rullerView);

					/*
					 * TableRow loanSecondRow = new TableRow(getActivity());
					 * loanSecondRow .setLayoutParams(new
					 * LayoutParams(LayoutParams.FILL_PARENT,
					 * LayoutParams.WRAP_CONTENT)); loanSecondRow.setPadding(10,
					 * 5, 10, 5);
					 * 
					 * TextView loanBank = new TextView(getActivity());
					 * loanBank.setText(GetSpanText.getSpanString(getActivity(),
					 * String.valueOf(loanArr1[i] + " " +
					 * AppStrings.loanOutstandingBank)));
					 * loanBank.setTypeface(LoginActivity.sTypeface);
					 * loanBank.setTextColor(color.black);
					 * loanSecondRow.addView(loanBank);
					 * 
					 * TextView loanBankValue = new TextView(getActivity());
					 * mloanBank = loanArr1[i + 2];
					 * loanBankValue.setText(GetSpanText.getSpanString(
					 * getActivity(), String.valueOf(mloanBank)));
					 * loanBankValue.setTypeface(LoginActivity.sTypeface);
					 * loanBankValue.setTextColor(color.black);
					 * loanBankValue.setGravity(Gravity.RIGHT);
					 * loanSecondRow.addView(loanBankValue);
					 * 
					 * loanTable.addView(loanSecondRow, new
					 * TableLayout.LayoutParams(LayoutParams.FILL_PARENT,
					 * LayoutParams.WRAP_CONTENT));
					 * 
					 * rullerView = new View(getActivity());
					 * rullerView.setLayoutParams(new
					 * TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
					 * 1)); rullerView.setBackgroundColor(Color.rgb(220, 220,
					 * 220)); loanTable.addView(rullerView);
					 * 
					 * TableRow loanThirdRow = new TableRow(getActivity());
					 * loanThirdRow.setLayoutParams(new
					 * LayoutParams(LayoutParams.FILL_PARENT,
					 * LayoutParams.WRAP_CONTENT)); loanThirdRow.setPadding(10,
					 * 5, 10, 5);
					 * 
					 * TextView loanDifference = new TextView(getActivity());
					 * loanDifference
					 * .setText(GetSpanText.getSpanString(getActivity(),
					 * String.valueOf(AppStrings.difference)));
					 * loanDifference.setTypeface(LoginActivity.sTypeface,
					 * Typeface.BOLD); loanDifference.setTextColor(Color.BLACK);
					 * loanDifference.setGravity(Gravity.RIGHT);
					 * loanThirdRow.addView(loanDifference);
					 * 
					 * TextView loanDifferenceValue = new
					 * TextView(getActivity()); mloanDifference =
					 * Integer.parseInt(mloanBook) -
					 * Integer.parseInt(mloanBank); // mloanDifference =
					 * Integer.toString(loanInt);
					 * System.out.println("mloanDifference : " +
					 * mloanDifference); loanDifferenceValue
					 * .setText(GetSpanText.getSpanString(getActivity(),
					 * String.valueOf(mloanDifference)));
					 * loanDifferenceValue.setTypeface(LoginActivity.sTypeface,
					 * Typeface.BOLD);
					 * loanDifferenceValue.setTextColor(Color.BLACK);
					 * loanDifferenceValue.setGravity(Gravity.RIGHT);
					 * loanThirdRow.addView(loanDifferenceValue);
					 * 
					 * loanTable.addView(loanThirdRow, new
					 * TableLayout.LayoutParams(LayoutParams.FILL_PARENT,
					 * LayoutParams.WRAP_CONTENT));
					 * 
					 * rullerView = new View(getActivity());
					 * rullerView.setLayoutParams(new
					 * TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
					 * 1)); rullerView.setBackgroundColor(Color.rgb(220, 220,
					 * 220)); loanTable.addView(rullerView);
					 */
				}
			} catch (NullPointerException e) {
				// TODO Auto-generated catch block
				System.out.println("NullPointerException" + e);
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return rootView;
	}

	@Override
	public void onAttach(Context context) {
		// TODO Auto-generated method stub
		super.onAttach(context);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

}
