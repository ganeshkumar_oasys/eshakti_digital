package com.yesteam.eshakti.view.activity;

import com.oasys.eshakti.digitization.EShaktiApplication;

import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;

public abstract class BaseActivity extends FragmentActivity {

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
	}

	@Override
	protected void onStart() {
		super.onStart();

	}

	protected EShaktiApplication getApplciation() {
		return (EShaktiApplication) getApplication();
	}

}
