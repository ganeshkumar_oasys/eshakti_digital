package com.yesteam.eshakti.view.fragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.adapter.CustomItemAdapter;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog.OnDateSetListener;
import com.yesteam.eshakti.model.RowItem;
import com.yesteam.eshakti.sqlite.database.GroupProvider;
import com.yesteam.eshakti.sqlite.db.model.GroupTempDBLastTransDate;
import com.yesteam.eshakti.utils.CalculateDateUtils;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.utils.Get_EdiText_Filter;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.utils.Reset;
import com.yesteam.eshakti.utils.UpdateCalendarMinMaxDateUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.views.MaterialSpinner;
import com.yesteam.eshakti.views.RaisedButton;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Dialog;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Transaction_Loan_disburse_LoanAccFragment extends Fragment implements OnClickListener, OnDateSetListener {

	private TextView mGroupName, mCashInHand, mCashAtBank, mHeader;
	private TextView mSanctionAmountText, mDisbursementAmountText, mBalanceAmountText, mSanctionAmount_value,
			mDisbursementAmount_value, mBalanceAmount_value, mDis_AmountText;
	public static TextView mDis_DateText;
	private EditText mDis_Amount_editText;
	RadioButton mCashRadio, mBankRadio;
	private RaisedButton mSubmitButton;
	View rootView;
	public static String disbursementDate = "";
	public static String selectedType, selectedItemBank;
	public static String disbursementAmount;
	public static String disbursementDate_check = "";
	private String mLanguageLocale = "";
	Locale locale;
	MaterialSpinner materialSpinner_Bank;
	CustomItemAdapter bankNameAdapter;
	private List<RowItem> bankNameItems;
	public static String mBankNameValue = null;
	LinearLayout mSpinnerLayout;
	ArrayList<String> mBanknames_Array = new ArrayList<String>();
	ArrayList<String> mBanknamesId_Array = new ArrayList<String>();
	ArrayList<String> mEngSendtoServerBank_Array = new ArrayList<String>();
	ArrayList<String> mEngSendtoServerBankId_Array = new ArrayList<String>();
	Dialog confirmationDialog;
	private Button mEdit_RaisedButton, mOk_RaisedButton;
	Date date_dashboard, date_loanDisb;

	public Transaction_Loan_disburse_LoanAccFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		disbursementDate = "";
		disbursementAmount = "";
		selectedType = "";
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_LOAN_GROUP_INSTALLMENT;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		rootView = inflater.inflate(R.layout.fragment_loan_from_loanacc_disburse, container, false);

		MainFragment_Dashboard.isBackpressed = false;

		try {

			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashInHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashInHand.setTypeface(LoginActivity.sTypeface);

			mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashAtBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashAtBank.setTypeface(LoginActivity.sTypeface);

			init();

			for (int i = 0; i < SelectedGroupsTask.sBankNames.size(); i++) {
				mBanknames_Array.add(SelectedGroupsTask.sBankNames.elementAt(i).toString());
				mBanknamesId_Array.add(String.valueOf(i));
			}

			for (int i = 0; i < SelectedGroupsTask.sEngBankNames.size(); i++) {
				mEngSendtoServerBank_Array.add(SelectedGroupsTask.sEngBankNames.elementAt(i).toString());
				mEngSendtoServerBankId_Array.add(String.valueOf(i));
			}

			materialSpinner_Bank.setBaseColor(color.grey_400);

			materialSpinner_Bank.setFloatingLabelText(AppStrings.bankName);

			materialSpinner_Bank.setPaddingSafe(10, 0, 10, 0);

			final String[] bankNames = new String[SelectedGroupsTask.sBankNames.size() + 1];

			final String[] bankNames_BankID = new String[SelectedGroupsTask.sEngBankNames.size() + 1];

			bankNames[0] = String.valueOf(RegionalConversion.getRegionalConversion(AppStrings.bankName));
			for (int i = 0; i < SelectedGroupsTask.sBankNames.size(); i++) {
				bankNames[i + 1] = SelectedGroupsTask.sBankNames.elementAt(i).toString();
			}

			bankNames_BankID[0] = String.valueOf(RegionalConversion.getRegionalConversion(AppStrings.bankName));
			for (int i = 0; i < SelectedGroupsTask.sEngBankNames.size(); i++) {
				bankNames_BankID[i + 1] = SelectedGroupsTask.sEngBankNames.elementAt(i).toString();
			}

			int size = bankNames.length;

			bankNameItems = new ArrayList<RowItem>();
			for (int i = 0; i < size; i++) {
				RowItem rowItem = new RowItem(bankNames[i]);// SelectedGroupsTask.sBankNames.elementAt(i).toString());
				bankNameItems.add(rowItem);
			}
			bankNameAdapter = new CustomItemAdapter(getActivity(), bankNameItems);
			materialSpinner_Bank.setAdapter(bankNameAdapter);

			materialSpinner_Bank.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
					// TODO Auto-generated method stub

					if (position == 0) {
						selectedItemBank = bankNames_BankID[0];
						mBankNameValue = "0";

					} else {
						selectedItemBank = bankNames_BankID[position];
						System.out.println("SELECTED BANK NAME : " + selectedItemBank);
						mBankNameValue = selectedItemBank;
						String mBankname = null;
						for (int i = 0; i < mBanknames_Array.size(); i++) {
							if (selectedItemBank.equals(mEngSendtoServerBank_Array.get(i))) {
								mBankname = mEngSendtoServerBank_Array.get(i);
							}
						}

						mBankNameValue = mBankname;

					}
					Log.e("Selected Bank Name value", mBankNameValue);

				}

				@Override
				public void onNothingSelected(AdapterView<?> parent) {
					// TODO Auto-generated method stub

				}
			});

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return rootView;
	}

	private void init() {
		// TODO Auto-generated method stub

		mHeader = (TextView) rootView.findViewById(R.id.loanDisbursementheader);
		mSanctionAmountText = (TextView) rootView.findViewById(R.id.loan_dis_sanctionAmountTextView);
		mSanctionAmount_value = (TextView) rootView.findViewById(R.id.loan_dis_sanctionAmount_values);
		mDisbursementAmountText = (TextView) rootView.findViewById(R.id.loan_dis_disbursementAmountTextView);
		mDisbursementAmount_value = (TextView) rootView.findViewById(R.id.loan_dis_disbursementAmount_values);
		mBalanceAmountText = (TextView) rootView.findViewById(R.id.loan_dis_balanceTextView);
		mBalanceAmount_value = (TextView) rootView.findViewById(R.id.loan_dis_balance_values);
		mDis_AmountText = (TextView) rootView.findViewById(R.id.loanDisb_disbursementAmountTextView);
		mDis_DateText = (TextView) rootView.findViewById(R.id.loan_dis_disDateTV);
		mCashRadio = (RadioButton) rootView.findViewById(R.id.radioDisbursementLimitCash);
		mBankRadio = (RadioButton) rootView.findViewById(R.id.radioDisbursementLimitBank);

		mHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.mLoanDisbursementFromLoanAcc));
		mSanctionAmountText.setText(RegionalConversion.getRegionalConversion(AppStrings.mSanctionAmount));
		mDisbursementAmountText.setText(RegionalConversion.getRegionalConversion(AppStrings.mDisbursementAmount));
		mBalanceAmountText.setText(RegionalConversion.getRegionalConversion(AppStrings.mBalanceAmount));
		mDis_AmountText.setText(RegionalConversion.getRegionalConversion(AppStrings.mDisbursementAmount));
		mCashRadio.setText(RegionalConversion.getRegionalConversion(AppStrings.mLoanaccCash));
		mBankRadio.setText(RegionalConversion.getRegionalConversion(AppStrings.mLoanaccBank));

		mDis_DateText.setHint(GetSpanText.getSpanString(getActivity(), AppStrings.mDisbursementDate));
		mDis_DateText.setText(Transaction_Loan_disburse_LoanAccFragment.disbursementDate);
		mDis_DateText.setOnClickListener(this);

		mHeader.setTypeface(LoginActivity.sTypeface);
		mSanctionAmountText.setTypeface(LoginActivity.sTypeface);
		mSanctionAmount_value.setTypeface(LoginActivity.sTypeface);
		mDisbursementAmountText.setTypeface(LoginActivity.sTypeface);
		mDisbursementAmount_value.setTypeface(LoginActivity.sTypeface);
		mBalanceAmountText.setTypeface(LoginActivity.sTypeface);
		mBalanceAmount_value.setTypeface(LoginActivity.sTypeface);
		mDis_AmountText.setTypeface(LoginActivity.sTypeface);
		mDis_DateText.setTypeface(LoginActivity.sTypeface);
		mCashRadio.setTypeface(LoginActivity.sTypeface);
		mBankRadio.setTypeface(LoginActivity.sTypeface);

		if (publicValues.mGetLoanBalnceValues != null) {

			String[] mValues = publicValues.mGetLoanBalnceValues.split("#");
			String mValues_limit = mValues[1];
			String[] mLimitValues = mValues_limit.split("~");
			mSanctionAmount_value.setText(mLimitValues[0]);
			mDisbursementAmount_value.setText(mLimitValues[1]);
			mBalanceAmount_value.setText(mLimitValues[2]);

		}

		mDis_Amount_editText = (EditText) rootView.findViewById(R.id.loanDisb_disbursementAmount_value);
		mDis_Amount_editText.setTypeface(LoginActivity.sTypeface);
		mDis_Amount_editText.setInputType(InputType.TYPE_CLASS_NUMBER);
		mDis_Amount_editText.setPadding(5, 5, 5, 5);
		mDis_Amount_editText.setFilters(Get_EdiText_Filter.editText_filter());
		mDis_Amount_editText.setBackgroundResource(R.drawable.edittext_background);

		mSubmitButton = (RaisedButton) rootView.findViewById(R.id.loanDisbursement_submit);
		mSubmitButton.setText(RegionalConversion.getRegionalConversion(AppStrings.next));
		mSubmitButton.setTypeface(LoginActivity.sTypeface);
		mSubmitButton.setOnClickListener(this);

		mSpinnerLayout = (LinearLayout) rootView.findViewById(R.id.loan_dis_bankSpinnerlayout);
		mSpinnerLayout.setVisibility(View.GONE);

		materialSpinner_Bank = (MaterialSpinner) rootView.findViewById(R.id.loan_dis_bankspinner);

		RadioGroup radioGroup = (RadioGroup) rootView.findViewById(R.id.radioDisbursementLimit);
		radioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// checkedId is the RadioButton selected
				switch (checkedId) {
				case R.id.radioDisbursementLimitCash:
					selectedType = "Cash";
					mSpinnerLayout.setVisibility(View.GONE);
					break;

				case R.id.radioDisbursementLimitBank:
					selectedType = "Bank";
					mSpinnerLayout.setVisibility(View.VISIBLE);
					break;
				}
			}
		});

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {
		case R.id.loan_dis_disDateTV:

			disbursementDate_check = "1";
			onSetCalendarValues();
			try {
				Calendar calender = Calendar.getInstance();

				SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String formattedDate = df.format(calender.getTime());
				Log.e("Device Date  =  ", formattedDate + "");

				String mBalanceSheetDate = SelectedGroupsTask.sLastTransactionDate_Response; // dd/MM/yyyy
				String formatted_balancesheetDate = mBalanceSheetDate.replace("/", "-");
				Log.e("Balancesheet Date = ", formatted_balancesheetDate + "");

				Date balanceDate = df.parse(formatted_balancesheetDate);
				Date systemDate = df.parse(formattedDate);

				if (balanceDate.compareTo(systemDate) < 0 || balanceDate.compareTo(systemDate) == 0) {

					calendarDialogShow();
				} else {
					TastyToast.makeText(getActivity(), "PLEASE SET YOUR DEVICE DATE CORRECTLY", TastyToast.LENGTH_SHORT,
							TastyToast.ERROR);
					EShaktiApplication.setNextMonthLastDate(null);
					EShaktiApplication.setCalendarDateVisibleFlag(true);
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

			break;

		case R.id.loanDisbursement_submit:

			disbursementAmount = Reset.reset(disbursementAmount);
			disbursementAmount = mDis_Amount_editText.getText().toString();
			String balanceAmount = mBalanceAmount_value.getText().toString();
			if (disbursementAmount.equals("") || disbursementAmount == null) {
				disbursementAmount = "0";
			}

			if (!disbursementAmount.equals("0") && !selectedType.equals("") && !disbursementDate.equals("")) {

				// String dashBoardDate = DatePickerDialog.sDashboardDate;
				String dashBoardDate = null;
				if (DatePickerDialog.sDashboardDate.contains("-")) {
					dashBoardDate = DatePickerDialog.sDashboardDate;
				} else if (DatePickerDialog.sDashboardDate.contains("/")) {
					String[] dateArr = DatePickerDialog.sDashboardDate.split("/");
					dashBoardDate = dateArr[0] + "-" + dateArr[1] + "-" + dateArr[2];
					;
				}
				String loanDisbArr[] = EShaktiApplication.getLoanAcc_LoanDisbursementDate().split("/");
				String loanDisbDate = loanDisbArr[1] + "-" + loanDisbArr[0] + "-" + loanDisbArr[2];

				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

				try {
					date_dashboard = sdf.parse(dashBoardDate);
					date_loanDisb = sdf.parse(loanDisbDate);
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				if (date_dashboard.compareTo(date_loanDisb) >= 0) {
					if (Integer.parseInt(balanceAmount) != 0) {

						if (Integer.parseInt(disbursementAmount) <= Integer.parseInt(balanceAmount)) {

							EShaktiApplication.setLoanAccBalanceAmount(disbursementAmount);
							if (mBankRadio.isChecked()) {
								if (!mBankNameValue.equals("0") && mBankNameValue != null) {

									onShowConfirmationDialog();

								} else {
									TastyToast.makeText(getActivity(), AppStrings.mLoanaccBankNullToast,
											TastyToast.LENGTH_SHORT, TastyToast.WARNING);
								}

							} else {
								onShowConfirmationDialog();
							}

						} else {
							TastyToast.makeText(getActivity(), AppStrings.mCheckDisbursementAlert,
									TastyToast.LENGTH_SHORT, TastyToast.WARNING);
						}

					} else {
						TastyToast.makeText(getActivity(), AppStrings.mCheckbalanceAlert, TastyToast.LENGTH_SHORT,
								TastyToast.WARNING);
					}

				} else {
					TastyToast.makeText(getActivity(), AppStrings.mCheckDisbursementDate, TastyToast.LENGTH_SHORT,
							TastyToast.WARNING);
				}
			} else {
				TastyToast.makeText(getActivity(), AppStrings.nullDetailsAlert, TastyToast.LENGTH_SHORT,
						TastyToast.WARNING);
			}
			break;

		case R.id.fragment_Edit_button:
			disbursementAmount = Reset.reset(disbursementAmount);
			mSubmitButton.setClickable(true);
			confirmationDialog.dismiss();

			break;
		case R.id.fragment_Ok_button:
			confirmationDialog.dismiss();
			Transaction_Loan_SB_disbursementFragment loan_SB_disbursementFragment = new Transaction_Loan_SB_disbursementFragment();
			setFragment(loan_SB_disbursementFragment);

			break;

		}

	}

	private void onShowConfirmationDialog() {
		// TODO Auto-generated method stub
		confirmationDialog = new Dialog(getActivity());

		LayoutInflater inflater = getActivity().getLayoutInflater();
		View dialogView = inflater.inflate(R.layout.dialog_confirmation, null);
		dialogView.setLayoutParams(
				new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

		TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
		confirmationHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.confirmation));
		confirmationHeader.setTypeface(LoginActivity.sTypeface);

		TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

		TableRow increaseLimitRow = new TableRow(getActivity());

		@SuppressWarnings("deprecation")
		TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT, 1f);
		contentParams.setMargins(10, 5, 10, 5);

		TextView increaseLimitText = new TextView(getActivity());
		increaseLimitText.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mLoanAccType)));
		increaseLimitText.setTypeface(LoginActivity.sTypeface);
		increaseLimitText.setTextColor(color.white);
		increaseLimitText.setPadding(5, 5, 5, 5);
		increaseLimitText.setLayoutParams(contentParams);
		increaseLimitRow.addView(increaseLimitText);

		TextView increaseLimit_values = new TextView(getActivity());
		increaseLimit_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(selectedType)));
		increaseLimit_values.setTextColor(color.white);
		increaseLimit_values.setPadding(5, 5, 5, 5);
		increaseLimit_values.setGravity(Gravity.RIGHT);
		increaseLimit_values.setLayoutParams(contentParams);
		increaseLimitRow.addView(increaseLimit_values);

		confirmationTable.addView(increaseLimitRow,
				new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

		if (selectedType.equals("Bank")) {

			TableRow increaseLimitRow1 = new TableRow(getActivity());

			TextView increaseLimitText1 = new TextView(getActivity());
			increaseLimitText1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.bankName)));
			increaseLimitText1.setTypeface(LoginActivity.sTypeface);
			increaseLimitText1.setTextColor(color.white);
			increaseLimitText1.setPadding(5, 5, 5, 5);
			increaseLimitText1.setLayoutParams(contentParams);
			increaseLimitRow1.addView(increaseLimitText1);

			TextView increaseLimit_values1 = new TextView(getActivity());
			increaseLimit_values1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(mBankNameValue)));
			increaseLimit_values1.setTextColor(color.white);
			increaseLimit_values1.setPadding(5, 5, 5, 5);
			increaseLimit_values1.setGravity(Gravity.RIGHT);
			increaseLimit_values1.setLayoutParams(contentParams);
			increaseLimitRow1.addView(increaseLimit_values1);

			confirmationTable.addView(increaseLimitRow1,
					new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

		}

		TableRow bankChargeRow = new TableRow(getActivity());

		TextView bankChargeText = new TextView(getActivity());
		bankChargeText
				.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mDisbursementAmount)));
		bankChargeText.setTypeface(LoginActivity.sTypeface);
		bankChargeText.setTextColor(color.white);
		bankChargeText.setPadding(5, 5, 5, 5);
		bankChargeText.setLayoutParams(contentParams);
		bankChargeRow.addView(bankChargeText);

		TextView bankCharge_values = new TextView(getActivity());
		bankCharge_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(disbursementAmount)));
		bankCharge_values.setTextColor(color.white);
		bankCharge_values.setPadding(5, 5, 5, 5);
		bankCharge_values.setGravity(Gravity.RIGHT);
		bankCharge_values.setLayoutParams(contentParams);
		bankChargeRow.addView(bankCharge_values);

		confirmationTable.addView(bankChargeRow,
				new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

		TableRow tenureRow = new TableRow(getActivity());

		TextView tenureText = new TextView(getActivity());
		tenureText.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mDisbursementDate)));
		tenureText.setTypeface(LoginActivity.sTypeface);
		tenureText.setTextColor(color.white);
		tenureText.setPadding(5, 5, 5, 5);
		tenureText.setLayoutParams(contentParams);
		tenureRow.addView(tenureText);

		TextView tenure_values = new TextView(getActivity());
		tenure_values.setText(GetSpanText.getSpanString(getActivity(),
				String.valueOf(Transaction_Loan_disburse_LoanAccFragment.disbursementDate)));
		tenure_values.setTextColor(color.white);
		tenure_values.setPadding(5, 5, 5, 5);
		tenure_values.setGravity(Gravity.RIGHT);
		tenure_values.setLayoutParams(contentParams);
		tenureRow.addView(tenure_values);

		confirmationTable.addView(tenureRow,
				new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

		mEdit_RaisedButton = (RaisedButton) dialogView.findViewById(R.id.fragment_Edit_button);
		mEdit_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.edit));
		mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
		mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
																// 205,
																// 0));
		mEdit_RaisedButton.setOnClickListener(this);

		mOk_RaisedButton = (RaisedButton) dialogView.findViewById(R.id.fragment_Ok_button);
		mOk_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.yes));
		mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
		mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
		mOk_RaisedButton.setOnClickListener(this);

		confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		confirmationDialog.setCanceledOnTouchOutside(false);
		confirmationDialog.setContentView(dialogView);
		confirmationDialog.setCancelable(true);
		confirmationDialog.show();

		MarginLayoutParams margin = (MarginLayoutParams) dialogView.getLayoutParams();
		margin.leftMargin = 10;
		margin.rightMargin = 10;
		margin.topMargin = 10;
		margin.bottomMargin = 10;
		margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);
	}

	private void calendarDialogShow() {
		// TODO Auto-generated method stub
		mLanguageLocale = PrefUtils.getUserlangcode();
		if (mLanguageLocale.equalsIgnoreCase("English")) {
			locale = new Locale("en");
		} else if (mLanguageLocale.equalsIgnoreCase("Tamil")) {
			locale = new Locale("ta");
		} else if (mLanguageLocale.equalsIgnoreCase("Hindi")) {
			locale = new Locale("hi");
		} else if (mLanguageLocale.equalsIgnoreCase("Marathi")) {
			locale = new Locale("ma");
		} else if (mLanguageLocale.equalsIgnoreCase("Malayalam")) {
			locale = new Locale("ml");
		} else if (mLanguageLocale.equalsIgnoreCase("Kannada")) {
			locale = new Locale("kn");
		} else if (mLanguageLocale.equalsIgnoreCase("Bengali")) {
			locale = new Locale("bn");
		} else if (mLanguageLocale.equalsIgnoreCase("Gujarati")) {
			locale = new Locale("gu");
		} else if (mLanguageLocale.equalsIgnoreCase("Punjabi")) {
			locale = new Locale("pa");
		} else if (mLanguageLocale.equalsIgnoreCase("Assamese")) {
			locale = new Locale("as");
		} else {
			locale = new Locale("en");
		}
		locale = new Locale("en");
		Locale.setDefault(locale);
		Configuration config = new Configuration();
		config.locale = locale;
		getActivity().getResources().updateConfiguration(config, getActivity().getResources().getDisplayMetrics());

		OnDateSetListener datelistener = Transaction_Loan_disburse_LoanAccFragment.this;
		Calendar now = Calendar.getInstance();
		FragmentManager fm = getFragmentManager();
		DatePickerDialog dialog = DatePickerDialog.newInstance(datelistener, now.get(Calendar.YEAR),
				now.get(Calendar.MONDAY), now.get(Calendar.DAY_OF_MONTH));

		Calendar min_Cal = Calendar.getInstance();

		String loanDisbDateArr[] = EShaktiApplication.getLoanAcc_LoanDisbursementDate().split("/");// mm/dd/yyyy
		min_Cal.set(Integer.parseInt(loanDisbDateArr[2]), Integer.parseInt(loanDisbDateArr[0]) - 1,
				Integer.parseInt(loanDisbDateArr[1]));
		dialog.setMinDate(min_Cal);

		String[] calMinDate = EShaktiApplication.getLoanAcc_LoanDisbursementDate().split("/");
		String minDate = calMinDate[1] + "/" + calMinDate[0] + "/" + calMinDate[2];
		EShaktiApplication.setCalendarDialog_MinDate(minDate);

		Calendar max_cal = Calendar.getInstance();
		String lastTransactionDateArr[] = DatePickerDialog.sDashboardDate.split("-");// SelectedGroupsTask.sLastTransactionDate_Response.split("/");
		max_cal.set(Integer.parseInt(lastTransactionDateArr[2]), Integer.parseInt(lastTransactionDateArr[1]) - 1,
				Integer.parseInt(lastTransactionDateArr[0]));
		dialog.setMaxDate(max_cal);

		dialog.show(fm, "");

	}

	@Override
	public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
		// TODO Auto-generated method stub

	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub
		Log.e("Current Class Name!!!!!!!!!!!!!!", fragment.getClass().getName());
		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

		FragmentDrawer.drawer_CashinHand.setText(
				RegionalConversion.getRegionalConversion(AppStrings.cashinhand) + SelectedGroupsTask.sCashinHand);
		FragmentDrawer.drawer_CashinHand.setTypeface(LoginActivity.sTypeface);

		FragmentDrawer.drawer_CashatBank.setText(
				RegionalConversion.getRegionalConversion(AppStrings.cashatBank) + SelectedGroupsTask.sCashatBank);
		FragmentDrawer.drawer_CashatBank.setTypeface(LoginActivity.sTypeface);

		FragmentDrawer.drawer_lastTR_Date
				.setText(RegionalConversion.getRegionalConversion(AppStrings.lastTransactionDate) + " : "
						+ SelectedGroupsTask.sLastTransactionDate_Response);
		FragmentDrawer.drawer_lastTR_Date.setTypeface(LoginActivity.sTypeface);

		UpdateCalendarMinMaxDateUtils.OnUpdateCalendarDate();

		Calendar calender = Calendar.getInstance();

		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String formattedDate = df.format(calender.getTime());
		EShaktiApplication.setLastTransDate_GroupId(SelectedGroupsTask.Group_Id);

		new GroupTempDBLastTransDate(EShaktiApplication.getLastTransDate_GroupId(),
				SelectedGroupsTask.sLastTransactionDate_Response, formattedDate);

		GroupProvider.updateGroupLastTransDateResponse();
	}

	private void onSetCalendarValues() {
		// TODO Auto-generated method stub

		if (EShaktiApplication.getNextMonthLastDate() == null) {
			try {
				if (EShaktiApplication.isCalendarDateVisibleFlag()) {
					EShaktiApplication.setCalendarDateVisibleFlag(false);
				}

				Calendar calender = Calendar.getInstance();

				SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String formattedDate = df.format(calender.getTime());
				Log.e("Device Date  =  ", formattedDate + "");

				String mBalanceSheetDate = SelectedGroupsTask.sLastTransactionDate_Response; // dd/MM/yyyy
				String formatted_balancesheetDate = mBalanceSheetDate.replace("/", "-");
				Log.e("Balancesheet Date = ", formatted_balancesheetDate + "");

				Date balanceDate = df.parse(formatted_balancesheetDate);
				Date systemDate = df.parse(formattedDate);

				if (balanceDate.compareTo(systemDate) < 0 || balanceDate.compareTo(systemDate) == 0) {
					System.err.println(" balancesheet date is less than sys date");

					String mLastTransactionDate = SelectedGroupsTask.sLastTransactionDate_Response;

					String formattedDate_LastTransaction = mLastTransactionDate.replace("/", "-");

					String days = CalculateDateUtils.get_count_of_days(formattedDate_LastTransaction, formattedDate);

					int mDaycount = Integer.parseInt(days);
					Log.e("Total Days Count =====_____----", mDaycount + "");

					EShaktiApplication.setLastTransDate_GroupId(SelectedGroupsTask.Group_Id);

					if (mDaycount > 30) {

						SimpleDateFormat df_after = new SimpleDateFormat("dd-MM-yyyy");

						Calendar calendar = Calendar.getInstance();
						calendar.setTime(df_after.parse(formattedDate_LastTransaction));
						calendar.add(Calendar.MONTH, 1);
						calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
						Date nextMonthFirstDay = calendar.getTime();
						System.out.println(
								"------ Next month first date   =  " + df_after.format(nextMonthFirstDay) + "");

						Calendar cal_last_date = Calendar.getInstance();
						cal_last_date.setTime(df_after.parse(formattedDate_LastTransaction));
						cal_last_date.add(Calendar.MONTH, 1);
						cal_last_date.set(Calendar.DATE, cal_last_date.getActualMaximum(Calendar.DAY_OF_MONTH));
						Date nextMonthLastDay = cal_last_date.getTime();
						System.out
								.println("------ Next month Last date   =  " + df_after.format(nextMonthLastDay) + "");

						//
						String lastDateOfNextMonth = df_after.format(nextMonthLastDay);
						Calendar currentDate = Calendar.getInstance();
						int current_Day = currentDate.get(Calendar.DAY_OF_MONTH);
						int current_month = currentDate.get(Calendar.MONTH) + 1;
						int current_year = currentDate.get(Calendar.YEAR);
						String current_date = current_Day + "-" + current_month + "-" + current_year;

						Date lastdate = null, currentdate = null;
						String min_date_str = null;

						try {

							lastdate = df_after.parse(lastDateOfNextMonth);
							currentdate = df_after.parse(current_date);

						} catch (ParseException e) {
							// TODO: handle exception
							e.printStackTrace();
						}

						if (lastdate.compareTo(currentdate) <= 0) {
							min_date_str = lastDateOfNextMonth;
						} else {
							min_date_str = current_date;
						}

						Log.e("Minimum Date !!!!!	", min_date_str + "");

						EShaktiApplication.setNextMonthLastDate(min_date_str);

					} else {
						Calendar currentDate = Calendar.getInstance();
						SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
						EShaktiApplication.setNextMonthLastDate(sdf.format(currentDate.getTime()));

					}
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

	}

}
