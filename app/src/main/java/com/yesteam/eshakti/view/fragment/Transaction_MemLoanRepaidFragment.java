package com.yesteam.eshakti.view.fragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.squareup.otto.Subscribe;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.TransactionOffConstants;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;
import com.yesteam.eshakti.sqlite.database.GroupDetailsProvider;
import com.yesteam.eshakti.sqlite.database.GroupProvider;
import com.yesteam.eshakti.sqlite.database.TransactionProvider;
import com.yesteam.eshakti.sqlite.database.response.DefaultOffline_PLResponse;
import com.yesteam.eshakti.sqlite.database.response.DefaultOffline_PL_DisResponse;
import com.yesteam.eshakti.sqlite.database.response.GetSingleTransResponse;
import com.yesteam.eshakti.sqlite.database.response.GroupMLOSUpdateResponse;
import com.yesteam.eshakti.sqlite.database.response.GroupMemberloanOSResponse;
import com.yesteam.eshakti.sqlite.database.response.TransactionResponse;
import com.yesteam.eshakti.sqlite.database.response.TransactionUpdateResponse;
import com.yesteam.eshakti.sqlite.db.model.GetGroupMemberDetails;
import com.yesteam.eshakti.sqlite.db.model.GetTransactionSingle;
import com.yesteam.eshakti.sqlite.db.model.GetTransactionSinglevalues;
import com.yesteam.eshakti.sqlite.db.model.GroupMLUpdate;
import com.yesteam.eshakti.sqlite.db.model.GroupMasterSingle;
import com.yesteam.eshakti.sqlite.db.model.GroupResponseUpdate;
import com.yesteam.eshakti.sqlite.db.model.GroupTempDBLastTransDate;
import com.yesteam.eshakti.sqlite.db.model.Transaction;
import com.yesteam.eshakti.sqlite.db.model.TransactionUpdate;
import com.yesteam.eshakti.sqlite.db.model.Transaction_UniqueIdSingle;
import com.yesteam.eshakti.sqlite.db.transactions.TransactionManager.DataType;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.utils.GetOfflineTransactionValues;
import com.yesteam.eshakti.utils.GetResponseInfo;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.utils.Get_EdiText_Filter;
import com.yesteam.eshakti.utils.Get_Offline_MobileDate;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.utils.Put_DB_GroupNameDetail_Response;
import com.yesteam.eshakti.utils.Put_DB_GroupResponse;
import com.yesteam.eshakti.utils.Reset;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.utils.TextviewUtils;
import com.yesteam.eshakti.utils.UpdateCalendarMinMaxDateUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.views.CustomHorizontalScrollView;
import com.yesteam.eshakti.views.CustomHorizontalScrollView.onScrollChangedListener;
import com.yesteam.eshakti.webservices.GetMember_Loan_RepaidTask;
import com.yesteam.eshakti.webservices.Get_All_Mem_OutstandingTask;
import com.yesteam.eshakti.webservices.Get_LastTransactionID;
import com.yesteam.eshakti.webservices.Get_PurposeOfLoanTask;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Transaction_MemLoanRepaidFragment extends Fragment implements OnClickListener, TaskListener {

	public static final String TAG = Transaction_MemLoanRepaidFragment.class.getSimpleName();
	public static List<EditText> sPA_Fields = new ArrayList<EditText>();
	public static List<EditText> sInterest_Fields = new ArrayList<EditText>();
	public static String sPrincipleAmounts[];
	public static String sInterestAmounts[];
	public static String sSendToServer_MemLoanRepaid;
	public static int sPA_total;
	public static int sInterest_total;

	private TextView mGroupName, mCashinHand, mCashatBank;
	private TextView mHeader;
	private Button mSubmit_Raised_Button, mEdit_RaisedButton, mOk_RaisedButton;
	private EditText mPA_EditText;
	private EditText mInterest_EditText;
	private String[] mOutstanding;
	private boolean outstandingAlert;
	private Dialog mProgressDilaog;

	int mSize;
	String[] response, toBeEdit_PA, toBeEdit_Interest;
	String nullAmount = "0";
	String tempID = "", tempOuts = "";
	AlertDialog alertDialog;
	Dialog confirmationDialog;

	String mLastTrDate = null, mLastTr_ID = null;
	String mOfflineMemberloanId;

	private TableLayout mLeftHeaderTable, mRightHeaderTable, mLeftContentTable, mRightContentTable;
	private CustomHorizontalScrollView mHSRightHeader, mHSRightContent;

	String width[] = { AppStrings.principleAmount, AppStrings.interestRate, AppStrings.OutsatndingAmt };
	int[] rightHeaderWidth = new int[width.length];
	int[] rightContentWidth = new int[width.length];
	public static boolean mServerService = false;
	boolean isGetTrid = false;
	Button mPerviousButton, mNextButton;
	boolean isNextButton = false;
	boolean isDefaultService = false;
	boolean isDefaultService_LoanPurpose = false;
	int count = 0;
	boolean isPrevious = false;
	boolean isServiceCall = false;
	private TextView mLoanAccountNo;
	int mTotalOutstanding = 0;

	String mSqliteDBStoredValues_memberloanrepayValues = null;
	Date date_dashboard, date_loanDisb;
	LinearLayout mMemberNameLayout;
	TextView mMemberName;

	public Transaction_MemLoanRepaidFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_MEMBERLOANREPAID;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		SBus.INST.register(this);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		try {
			sPA_total = 0;
			sInterest_total = 0;
			sSendToServer_MemLoanRepaid = Reset.reset(sSendToServer_MemLoanRepaid);
			sInterest_Fields.clear();
			sPA_Fields.clear();
			mTotalOutstanding = 0;
			mSize = SelectedGroupsTask.member_Name.size();
			Log.d(TAG, "mSize " + String.valueOf(mSize));

			if (ConnectionUtils.isNetworkAvailable(getActivity())) {

				response = Get_All_Mem_OutstandingTask.sGet_All_Mem_OutstandingTask_Response.split("~");

			} else {
				response = Transaction_MemLoanRepaid_MenuFragment.mMemberOS_Offlineresponse.split("~");
			}
			int count = 0;

			System.out.println("Id   __________------------" + EShaktiApplication.getLoanId());
			System.out.println("------Name -" + EShaktiApplication.getLoanName());
			for (int i = 0; i < response.length; i++) {

				if ((count == 0) && (i % 2 == 0)) {
					tempID = tempID + response[i] + "~";
					count = 1;
				} else if ((count == 1) && (i % 2 != 0)) {
					tempOuts = tempOuts + response[i] + "~";

					mTotalOutstanding = mTotalOutstanding + Integer.parseInt(response[i]);
					count = 0;
				}
			}

			mOutstanding = tempOuts.split("~");

			Log.e("Total Member Loan Outstanding---->>>", mTotalOutstanding + "");

		} catch (Exception e) {
			e.printStackTrace();

			TastyToast.makeText(getActivity(), AppStrings.adminAlert, TastyToast.LENGTH_SHORT, TastyToast.WARNING);

			getActivity().finish();

		}

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View rootView = inflater.inflate(R.layout.fragment_all_transaction, container, false);

		MainFragment_Dashboard.isBackpressed = false;

		mSqliteDBStoredValues_memberloanrepayValues = null;

		try {
			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashinHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashinHand.setTypeface(LoginActivity.sTypeface);

			mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashatBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashatBank.setTypeface(LoginActivity.sTypeface);

			mHeader = (TextView) rootView.findViewById(R.id.fragmentHeader);

			Log.e("bank Name", EShaktiApplication.getMemberLoanRepaymentLoanBankName() + "");
			Log.e("Loan Id", EShaktiApplication.getMemberLoanRepaymentLoanAccNo() + "");
			mHeader.setText(
					RegionalConversion.getRegionalConversion(Transaction_MemLoanRepaid_MenuFragment.sSelected_LoanName)
							+ " - " + EShaktiApplication.getMemberLoanRepaymentLoanBankName());
			mLoanAccountNo = (TextView) rootView.findViewById(R.id.loanAccNo_MembeLoanRepayment);
			mLoanAccountNo.setVisibility(View.VISIBLE);
			mLoanAccountNo.setText(RegionalConversion.getRegionalConversion(AppStrings.Loan_Account_No) + " : "
					+ EShaktiApplication.getMemberLoanRepaymentLoanAccNo());
			mLoanAccountNo.setTypeface(LoginActivity.sTypeface);
			mHeader.setTypeface(LoginActivity.sTypeface);

			mLeftHeaderTable = (TableLayout) rootView.findViewById(R.id.LeftHeaderTable);
			mRightHeaderTable = (TableLayout) rootView.findViewById(R.id.RightHeaderTable);
			mLeftContentTable = (TableLayout) rootView.findViewById(R.id.LeftContentTable);
			mRightContentTable = (TableLayout) rootView.findViewById(R.id.RightContentTable);

			mHSRightHeader = (CustomHorizontalScrollView) rootView.findViewById(R.id.rightHeaderHScrollView);
			mHSRightContent = (CustomHorizontalScrollView) rootView.findViewById(R.id.rightContentHScrollView);

			/**
			 * 
			 */
			mMemberNameLayout = (LinearLayout) rootView.findViewById(R.id.member_name_layout);
			mMemberName = (TextView) rootView.findViewById(R.id.member_name);

			mHSRightHeader.setOnScrollChangedListener(new onScrollChangedListener() {

				public void onScrollChanged(int l, int t, int oldl, int oldt) {
					// TODO Auto-generated method stub

					mHSRightContent.scrollTo(l, 0);

				}
			});

			mHSRightContent.setOnScrollChangedListener(new onScrollChangedListener() {
				@Override
				public void onScrollChanged(int l, int t, int oldl, int oldt) {
					// TODO Auto-generated method stub
					mHSRightHeader.scrollTo(l, 0);
				}
			});

			TableRow leftHeaderRow = new TableRow(getActivity());

			TableRow.LayoutParams lHeaderParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);

			TextView mMemberName_Header = new TextView(getActivity());
			mMemberName_Header.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.memberName)));
			mMemberName_Header.setTypeface(LoginActivity.sTypeface);
			mMemberName_Header.setTextColor(Color.WHITE);
			mMemberName_Header.setPadding(10, 5, 10, 5);
			mMemberName_Header.setLayoutParams(lHeaderParams);
			leftHeaderRow.addView(mMemberName_Header);

			mLeftHeaderTable.addView(leftHeaderRow);

			TableRow rightHeaderRow = new TableRow(getActivity());

			TableRow.LayoutParams rHeaderParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);
			rHeaderParams.setMargins(10, 0, 10, 0);

			TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);
			contentParams.setMargins(10, 0, 10, 0);

			TextView mOutstanding_Header = new TextView(getActivity());
			mOutstanding_Header
					.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.OutsatndingAmt)));
			mOutstanding_Header.setTypeface(LoginActivity.sTypeface);
			mOutstanding_Header.setSingleLine(true);
			mOutstanding_Header.setTextColor(Color.WHITE);
			mOutstanding_Header.setGravity(Gravity.CENTER);
			mOutstanding_Header.setLayoutParams(contentParams);
			mOutstanding_Header.setPadding(10, 5, 10, 5);
			rightHeaderRow.addView(mOutstanding_Header);

			TextView mPA_Header = new TextView(getActivity());
			mPA_Header.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.principleAmount)));
			mPA_Header.setTypeface(LoginActivity.sTypeface);
			mPA_Header.setSingleLine(true);
			mPA_Header.setTextColor(Color.WHITE);
			mPA_Header.setGravity(Gravity.CENTER);
			mPA_Header.setLayoutParams(rHeaderParams);
			mPA_Header.setPadding(10, 5, 10, 5);
			rightHeaderRow.addView(mPA_Header);

			TextView mInterest_Header = new TextView(getActivity());
			mInterest_Header.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.interest)));
			mInterest_Header.setSingleLine(true);
			mInterest_Header.setTypeface(LoginActivity.sTypeface);
			mInterest_Header.setTextColor(Color.WHITE);
			mInterest_Header.setGravity(Gravity.CENTER);
			mInterest_Header.setLayoutParams(rHeaderParams);
			mInterest_Header.setPadding(10, 5, 10, 5);
			rightHeaderRow.addView(mInterest_Header);

			mRightHeaderTable.addView(rightHeaderRow);

			try {

				for (int j = 0; j < mSize; j++) {
					TableRow leftContentRow = new TableRow(getActivity());

					TableRow.LayoutParams leftContentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT, 60,
							1f);
					leftContentParams.setMargins(5, 5, 5, 5);

					final TextView memberName_Text = new TextView(getActivity());
					memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
							String.valueOf(SelectedGroupsTask.member_Name.elementAt(j))));
					memberName_Text.setTypeface(LoginActivity.sTypeface);
					memberName_Text.setTextColor(color.black);
					memberName_Text.setPadding(5, 0, 5, 5);
					memberName_Text.setLayoutParams(leftContentParams);
					leftContentRow.addView(memberName_Text);

					mLeftContentTable.addView(leftContentRow);

					TableRow rightContentRow = new TableRow(getActivity());

					TableRow.LayoutParams rightContentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
							LayoutParams.WRAP_CONTENT, 1f);// (150,60,1f);
					rightContentParams.setMargins(10, 5, 10, 5);

					TableRow.LayoutParams contentRow_Params = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT, 60,
							1f);

					contentRow_Params.setMargins(10, 5, 10, 5);
					TextView outstanding = new TextView(getActivity());
					outstanding.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(mOutstanding[j])));
					outstanding.setTextColor(color.black);
					outstanding.setGravity(Gravity.CENTER);
					outstanding.setLayoutParams(contentRow_Params);
					outstanding.setPadding(10, 0, 10, 5);
					rightContentRow.addView(outstanding);

					mPA_EditText = new EditText(getActivity());
					mPA_EditText.setId(j);
					sPA_Fields.add(mPA_EditText);
					mPA_EditText.setInputType(InputType.TYPE_CLASS_NUMBER);
					mPA_EditText.setPadding(5, 5, 5, 5);
					mPA_EditText.setFilters(Get_EdiText_Filter.editText_filter());
					mPA_EditText.setBackgroundResource(R.drawable.edittext_background);
					mPA_EditText.setGravity(Gravity.RIGHT);
					mPA_EditText.setLayoutParams(rightContentParams);
					mPA_EditText.setTag("EditTextView of Amount");
					mPA_EditText.setWidth(150);
					mPA_EditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {

						@Override
						public void onFocusChange(View v, boolean hasFocus) {
							// TODO Auto-generated method stub

							if (hasFocus) {

								mMemberNameLayout.setVisibility(View.VISIBLE);
								mMemberName.setText(memberName_Text.getText().toString().trim());
								mMemberName.setTypeface(LoginActivity.sTypeface);
								TextviewUtils.manageBlinkEffect(mMemberName, getActivity());
								((EditText) v).setGravity(Gravity.LEFT);
							} else {
								((EditText) v).setGravity(Gravity.RIGHT);
								mMemberNameLayout.setVisibility(View.GONE);
								mMemberName.setText("");
							}
						}
					});

					rightContentRow.addView(mPA_EditText);

					mInterest_EditText = new EditText(getActivity());
					mInterest_EditText.setId(j);
					sInterest_Fields.add(mInterest_EditText);
					mInterest_EditText.setInputType(InputType.TYPE_CLASS_NUMBER);// |
																					// InputType.TYPE_NUMBER_FLAG_DECIMAL);
					mInterest_EditText.setPadding(5, 5, 5, 5);
					mInterest_EditText.setFilters(Get_EdiText_Filter.editText_filter());
					mInterest_EditText.setGravity(Gravity.RIGHT);
					mInterest_EditText.setBackgroundResource(R.drawable.edittext_background);
					mInterest_EditText.setLayoutParams(rightContentParams);
					mInterest_EditText.setWidth(150);
					mInterest_EditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {

						@Override
						public void onFocusChange(View v, boolean hasFocus) {
							// TODO Auto-generated method stub
							if (hasFocus) {
								((EditText) v).setGravity(Gravity.LEFT);
								mMemberNameLayout.setVisibility(View.VISIBLE);
								mMemberName.setText(memberName_Text.getText().toString().trim());
								mMemberName.setTypeface(LoginActivity.sTypeface);
								TextviewUtils.manageBlinkEffect(mMemberName, getActivity());
								((EditText) v).setGravity(Gravity.LEFT);
							} else {
								((EditText) v).setGravity(Gravity.RIGHT);
								mMemberNameLayout.setVisibility(View.GONE);
								mMemberName.setText("");
							}

						}
					});
					rightContentRow.addView(mInterest_EditText);

					mRightContentTable.addView(rightContentRow);
				}

			} catch (ArrayIndexOutOfBoundsException e) {
				e.printStackTrace();
				TastyToast.makeText(getActivity(), AppStrings.adminAlert, TastyToast.LENGTH_SHORT, TastyToast.WARNING);

				getActivity().finish();

			}

			mSubmit_Raised_Button = (Button) rootView.findViewById(R.id.fragment_Submit_button);
			mSubmit_Raised_Button.setText(RegionalConversion.getRegionalConversion(AppStrings.submit));
			mSubmit_Raised_Button.setTypeface(LoginActivity.sTypeface);
			mSubmit_Raised_Button.setOnClickListener(this);

			mPerviousButton = (Button) rootView.findViewById(R.id.fragment_Previousbutton);
			mPerviousButton.setText(RegionalConversion.getRegionalConversion(AppStrings.mPervious));
			mPerviousButton.setTypeface(LoginActivity.sTypeface);
			mPerviousButton.setOnClickListener(this);

			mNextButton = (Button) rootView.findViewById(R.id.fragment_Nextbutton);
			mNextButton.setText(RegionalConversion.getRegionalConversion(AppStrings.mNext));
			mNextButton.setTypeface(LoginActivity.sTypeface);
			mNextButton.setOnClickListener(this);

			if (EShaktiApplication.isDefault()) {
				mPerviousButton.setVisibility(View.INVISIBLE);
				if (mTotalOutstanding == 0) {
					mNextButton.setVisibility(View.VISIBLE);
				} else {
					mNextButton.setVisibility(View.INVISIBLE);
				}

			}

			resizeMemberNameWidth();
			resizeRightSideTable();
			resizeBodyTableRowHeight();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return rootView;
	}

	private void resizeRightSideTable() {
		// TODO Auto-generated method stub
		int rightHeaderCount = (((TableRow) mRightHeaderTable.getChildAt(0)).getChildCount());
		System.out.println("------------header count---------" + rightHeaderCount);
		for (int i = 0; i < rightHeaderCount; i++) {
			rightHeaderWidth[i] = viewWidth(((TableRow) mRightHeaderTable.getChildAt(0)).getChildAt(i));
			rightContentWidth[i] = viewWidth(((TableRow) mRightContentTable.getChildAt(0)).getChildAt(i));
		}
		for (int i = 0; i < rightHeaderCount; i++) {
			if (rightHeaderWidth[i] < rightContentWidth[i]) {
				((TableRow) mRightHeaderTable.getChildAt(0)).getChildAt(i)
						.getLayoutParams().width = rightContentWidth[i];
			} else {
				((TableRow) mRightContentTable.getChildAt(0)).getChildAt(i)
						.getLayoutParams().width = rightHeaderWidth[i];
			}
		}
	}

	private void resizeMemberNameWidth() {
		// TODO Auto-generated method stub
		int leftHeadertWidth = viewWidth(mLeftHeaderTable);
		int leftContentWidth = viewWidth(mLeftContentTable);

		if (leftHeadertWidth < leftContentWidth) {
			mLeftHeaderTable.getLayoutParams().width = leftContentWidth;
		} else {
			mLeftContentTable.getLayoutParams().width = leftHeadertWidth;
		}
	}

	private void resizeBodyTableRowHeight() {

		int leftContentTable_ChildCount = mLeftContentTable.getChildCount();

		for (int x = 0; x < leftContentTable_ChildCount; x++) {

			TableRow leftContentTableRow = (TableRow) mLeftContentTable.getChildAt(x);
			TableRow rightContentTableRow = (TableRow) mRightContentTable.getChildAt(x);

			int rowLeftHeight = viewHeight(leftContentTableRow);
			int rowRightHeight = viewHeight(rightContentTableRow);

			TableRow tableRow = rowLeftHeight < rowRightHeight ? leftContentTableRow : rightContentTableRow;
			int finalHeight = rowLeftHeight > rowRightHeight ? rowLeftHeight : rowRightHeight;

			this.matchLayoutHeight(tableRow, finalHeight);
		}

	}

	private void matchLayoutHeight(TableRow tableRow, int height) {

		int tableRowChildCount = tableRow.getChildCount();

		// if a TableRow has only 1 child
		if (tableRow.getChildCount() == 1) {

			View view = tableRow.getChildAt(0);
			TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();
			params.height = height - (params.bottomMargin + params.topMargin);

			return;
		}

		// if a TableRow has more than 1 child
		for (int x = 0; x < tableRowChildCount; x++) {

			View view = tableRow.getChildAt(x);

			TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();

			if (!isTheHeighestLayout(tableRow, x)) {
				params.height = height - (params.bottomMargin + params.topMargin);
				return;
			}
		}

	}

	// check if the view has the highest height in a TableRow
	private boolean isTheHeighestLayout(TableRow tableRow, int layoutPosition) {

		int tableRowChildCount = tableRow.getChildCount();
		int heighestViewPosition = -1;
		int viewHeight = 0;

		for (int x = 0; x < tableRowChildCount; x++) {
			View view = tableRow.getChildAt(x);
			int height = this.viewHeight(view);

			if (viewHeight < height) {
				heighestViewPosition = x;
				viewHeight = height;
			}
		}

		return heighestViewPosition == layoutPosition;
	}

	// read a view's height
	private int viewHeight(View view) {
		view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
		return view.getMeasuredHeight();
	}

	// read a view's width
	private int viewWidth(View view) {
		view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
		return view.getMeasuredWidth();
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {

		case R.id.fragment_Submit_button:

			try {

				sPA_total = 0;
				sInterest_total = 0;
				sSendToServer_MemLoanRepaid = Reset.reset(sSendToServer_MemLoanRepaid);

				sPrincipleAmounts = new String[sPA_Fields.size()];
				sInterestAmounts = new String[sInterest_Fields.size()];

				for (int i = 0; i < sPrincipleAmounts.length; i++) {

					// Principle Amount
					sPrincipleAmounts[i] = String.valueOf(sPA_Fields.get(i).getText());
					if ((sPrincipleAmounts[i].equals("")) || (sPrincipleAmounts[i] == null)) {
						sPrincipleAmounts[i] = nullAmount;
					}

					// Interest
					sInterestAmounts[i] = String.valueOf(sInterest_Fields.get(i).getText());
					if ((sInterestAmounts[i].equals("")) || (sInterestAmounts[i] == null)) {
						sInterestAmounts[i] = nullAmount;
					}

					if (sPrincipleAmounts[i].matches("\\d*\\.?\\d+")) { // match
																		// a
																		// decimal
																		// number

						int amount = (int) Math.round(Double.parseDouble(sPrincipleAmounts[i]));
						sPrincipleAmounts[i] = String.valueOf(amount);
					}

					if (sInterestAmounts[i].matches("\\d*\\.?\\d+")) { // match
																		// a
																		// decimal
																		// number

						int amount = (int) Math.round(Double.parseDouble(sInterestAmounts[i]));
						sInterestAmounts[i] = String.valueOf(amount);
					}

					if (Integer.parseInt(mOutstanding[i]) >= Integer.parseInt(sPrincipleAmounts[i].trim())) {

						sSendToServer_MemLoanRepaid = sSendToServer_MemLoanRepaid
								+ String.valueOf(SelectedGroupsTask.member_Id.elementAt(i)) + "~"
								+ String.valueOf(sPrincipleAmounts[i]) + "~" + String.valueOf(sInterestAmounts[i])
								+ "~";

						/** Calculating Total **/
						sPA_total = sPA_total + Integer.parseInt(sPrincipleAmounts[i].trim());

						sInterest_total = sInterest_total + Integer.parseInt(sInterestAmounts[i].trim());

						outstandingAlert = true;
					} else {

						outstandingAlert = false;

						TastyToast.makeText(getActivity(), AppStrings.groupRepaidAlert, TastyToast.LENGTH_SHORT,
								TastyToast.WARNING);

						sSendToServer_MemLoanRepaid = Reset.reset(sSendToServer_MemLoanRepaid);
						sInterest_total = Integer.parseInt(nullAmount);
						sPA_total = Integer.parseInt(nullAmount);

						break;
					}
				}

				Log.d(TAG, "PA TOATL " + Integer.valueOf(sPA_total));

				Log.d(TAG, "INTEREST TOTAL " + Integer.valueOf(sInterest_total));

				System.out.println("sSendToServer_MemLoanRepaid >> " + sSendToServer_MemLoanRepaid);

				if (!EShaktiApplication.isDefault()) {

					System.out.println(
							"-----------  DatePickerDialog.sDashboardDate  =  " + DatePickerDialog.sDashboardDate + "");
					// String dashBoardDate = DatePickerDialog.sDashboardDate;
					String dashBoardDate = null;
					if (DatePickerDialog.sDashboardDate.contains("-")) {
						dashBoardDate = DatePickerDialog.sDashboardDate;
					} else if (DatePickerDialog.sDashboardDate.contains("/")) {
						String[] dateArr = DatePickerDialog.sDashboardDate.split("/");
						dashBoardDate = dateArr[0] + "-" + dateArr[1] + "-" + dateArr[2];
						;
					}
					String loanDisbArr[] = EShaktiApplication.getLoanDisbursementDate().split("/");
					String loanDisbDate = loanDisbArr[1] + "-" + loanDisbArr[0] + "-" + loanDisbArr[2];

					SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

					try {
						date_dashboard = sdf.parse(dashBoardDate);
						date_loanDisb = sdf.parse(loanDisbDate);
					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					if (date_dashboard.compareTo(date_loanDisb) >= 0) {
						onCallConfirmationDialog();
					} else {
						TastyToast.makeText(getActivity(), AppStrings.mCheckDisbursementDate, TastyToast.LENGTH_SHORT,
								TastyToast.WARNING);
					}
				} else {
					onCallConfirmationDialog();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			break;

		case R.id.fragment_Edit_button:

			sSendToServer_MemLoanRepaid = Reset.reset(sSendToServer_MemLoanRepaid);
			sPA_total = Integer.parseInt(nullAmount);
			sInterest_total = Integer.parseInt(nullAmount);
			confirmationDialog.dismiss();

			isServiceCall = false;
			break;

		case R.id.fragment_Ok_button:
			// Do service call

			if (ConnectionUtils.isNetworkAvailable(getActivity())) {

				try {
					mServerService = true;
					if (EShaktiApplication.getLoginFlag().equals(Constants.ONLINEFLAG)) {

						if (!isServiceCall) {
							isServiceCall = true;

							new GetMember_Loan_RepaidTask(Transaction_MemLoanRepaidFragment.this).execute();
						}
					} else {
						TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
								TastyToast.ERROR);
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			} else {
				// Do offline Stuffs
				Log.v("ContentValues", "Step 1");
				if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
					EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GET_TRANS_SINGLE,
							new GetTransactionSingle(PrefUtils.getOfflineUniqueID()));
				} else {
					TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
							TastyToast.ERROR);
				}
			}

			break;

		case R.id.fragment_Previousbutton:

			System.out.println("------MemLoan Previous button pressed-----------");
			Log.e("Loan id size = ", SelectedGroupsTask.loan_Id.size() + "");
			Log.v("Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID",
					Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID);
			int id = 0;

			for (int i = 0; i < SelectedGroupsTask.loan_Id.size() - 1; i++) {

				if (SelectedGroupsTask.loan_Id.elementAt(i)
						.equals(Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID)) {

					if (i == 0) {
						Log.e("i == 0", " So navigate to PersonalLoan");
						i = 0;
						Log.e("previous loan id if part",
								i + " Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID = " + i);
						Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID = "0";
						Transaction_PersonalLoanRepaidFragment.memLoanCount = 0;

					} else if (i != 0) {
						Log.e("i == 0", " So navigate to MemberLoan");
						Log.v("Before dec i = ", i + "");
						id = --i;
						Log.v("After dec id = ", id + "");
						Log.e(" --Transaction_PersonalLoanRepaidFragment.memLoanCount	 ",
								Transaction_PersonalLoanRepaidFragment.memLoanCount + "");
						int count = --Transaction_PersonalLoanRepaidFragment.memLoanCount;
						Log.v("After dec count = ", count + "");
						Transaction_PersonalLoanRepaidFragment.memLoanCount = count;
						Log.e("previous loan idssssssss ", id + "");
						Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID = SelectedGroupsTask.loan_Id
								.elementAt(id);
						Transaction_MemLoanRepaid_MenuFragment.sSelected_LoanName = SelectedGroupsTask.loan_Name
								.elementAt(id);
						Log.e("Loan Name", SelectedGroupsTask.loan_Name.elementAt(i) + "");
						Log.e("Acc No", SelectedGroupsTask.loanAcc_loanAccNo.elementAt(i) + "");
						Log.e("Loan Bank Name", SelectedGroupsTask.loanAcc_bankName.elementAt(i) + "");
						Log.e("Date", SelectedGroupsTask.loanAcc_loanDisbursementDate.elementAt(i) + "");

						EShaktiApplication
								.setMemberLoanRepaymentLoanBankName(SelectedGroupsTask.loanAcc_bankName.elementAt(i));
						EShaktiApplication
								.setMemberLoanRepaymentLoanAccNo(SelectedGroupsTask.loanAcc_loanAccNo.elementAt(i));

					}
				}
			}
			if (ConnectionUtils.isNetworkAvailable(getActivity())) {

				if (PrefUtils.getGroupMasterResValues() != null && PrefUtils.getGroupMasterResValues().equals("1")) {

					if (Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID.equals("0")) {
						EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.DEFAULT_PLOS,
								new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));
					} else {

						EShaktiApplication.getInstance().getTransactionManager().startTransaction(
								DataType.GROUP_MASTER_MEMBERLOANOS,
								new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));

					}

				} else {
					new Get_All_Mem_OutstandingTask(this).execute();
				}
			} else {
				System.out.println("-----Offline---");
				if (Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID.equals("0")) {
					EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.DEFAULT_PLOS,
							new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));
				} else {

					EShaktiApplication.getInstance().getTransactionManager().startTransaction(
							DataType.GROUP_MASTER_MEMBERLOANOS,
							new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));

				}
			}

			isPrevious = true;

			break;
		case R.id.fragment_Nextbutton:

			System.out.println("-Next Button pressed-------");
			if (ConnectionUtils.isNetworkAvailable(getActivity())) {

				if (PrefUtils.getGroupMasterResValues() != null && PrefUtils.getGroupMasterResValues().equals("1")) {

					int loan_size = SelectedGroupsTask.loan_Id.size();
					Log.e("Before inc Transaction_PersonalLoanRepaidFragment.memLoanCount  =  ",
							Transaction_PersonalLoanRepaidFragment.memLoanCount + "");
					int i = Transaction_PersonalLoanRepaidFragment.memLoanCount++;
					Log.v("After inc Transaction_PersonalLoanRepaidFragment.memLoanCount  =  ",
							Transaction_PersonalLoanRepaidFragment.memLoanCount + "  " + "  i = " + i);
					if (Transaction_PersonalLoanRepaidFragment.memLoanCount <= loan_size) {
						if (!SelectedGroupsTask.loan_Id.elementAt(i).equals("0")) {

							Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID = SelectedGroupsTask.loan_Id
									.elementAt(i);
							Transaction_MemLoanRepaid_MenuFragment.sSelected_LoanName = SelectedGroupsTask.loan_Name
									.elementAt(i);
							Log.e("&&&&&&&&&  Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID",
									Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID);

							Log.e("Loan Name", SelectedGroupsTask.loan_Name.elementAt(i) + "");
							Log.e("Acc No", SelectedGroupsTask.loanAcc_loanAccNo.elementAt(i) + "");
							Log.e("Loan Bank Name", SelectedGroupsTask.loanAcc_bankName.elementAt(i) + "");
							Log.e("Date", SelectedGroupsTask.loanAcc_loanDisbursementDate.elementAt(i) + "");

							EShaktiApplication.setMemberLoanRepaymentLoanBankName(
									SelectedGroupsTask.loanAcc_bankName.elementAt(i));
							EShaktiApplication
									.setMemberLoanRepaymentLoanAccNo(SelectedGroupsTask.loanAcc_loanAccNo.elementAt(i));

							EShaktiApplication.getInstance().getTransactionManager().startTransaction(
									DataType.GROUP_MASTER_MEMBERLOANOS,
									new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));

							EShaktiApplication.setLoanId(Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID);
							EShaktiApplication.setLoanName(Transaction_MemLoanRepaid_MenuFragment.sSelected_LoanName);
						} else {
							Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID = "0";
							Transaction_MemLoanRepaid_MenuFragment.sSelected_LoanName = SelectedGroupsTask.loan_Name
									.elementAt(i);
							EShaktiApplication.getInstance().getTransactionManager().startTransaction(
									DataType.DEFAULT_PLDISOS,
									new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));
							EShaktiApplication.setLoanId(Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID);
							EShaktiApplication.setLoanName(Transaction_MemLoanRepaid_MenuFragment.sSelected_LoanName);
						}
					}

				} else {
					// Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID
					// = "0";
					int loan_size = SelectedGroupsTask.loan_Id.size();
					Log.e("Before inc Transaction_PersonalLoanRepaidFragment.memLoanCount  =  ",
							Transaction_PersonalLoanRepaidFragment.memLoanCount + "");
					int i = Transaction_PersonalLoanRepaidFragment.memLoanCount++;
					Log.v("After inc Transaction_PersonalLoanRepaidFragment.memLoanCount  =  ",
							Transaction_PersonalLoanRepaidFragment.memLoanCount + "  " + "  i = " + i);
					if (Transaction_PersonalLoanRepaidFragment.memLoanCount <= loan_size) {
						if (!SelectedGroupsTask.loan_Id.elementAt(i).equals("0")) {

							Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID = SelectedGroupsTask.loan_Id
									.elementAt(i);
							Transaction_MemLoanRepaid_MenuFragment.sSelected_LoanName = SelectedGroupsTask.loan_Name
									.elementAt(i);
							Log.e("&&&&&&&&&  Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID",
									Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID);

							Log.e("Loan Name", SelectedGroupsTask.loan_Name.elementAt(i) + "");
							Log.e("Acc No", SelectedGroupsTask.loanAcc_loanAccNo.elementAt(i) + "");
							Log.e("Loan Bank Name", SelectedGroupsTask.loanAcc_bankName.elementAt(i) + "");
							Log.e("Date", SelectedGroupsTask.loanAcc_loanDisbursementDate.elementAt(i) + "");

							EShaktiApplication.setMemberLoanRepaymentLoanBankName(
									SelectedGroupsTask.loanAcc_bankName.elementAt(i));
							EShaktiApplication
									.setMemberLoanRepaymentLoanAccNo(SelectedGroupsTask.loanAcc_loanAccNo.elementAt(i));

							new Get_All_Mem_OutstandingTask(this).execute();

							EShaktiApplication.setLoanId(Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID);
							EShaktiApplication.setLoanName(Transaction_MemLoanRepaid_MenuFragment.sSelected_LoanName);
						} else {
							Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID = "0";
							Transaction_MemLoanRepaid_MenuFragment.sSelected_LoanName = SelectedGroupsTask.loan_Name
									.elementAt(i);
							new Get_All_Mem_OutstandingTask(this).execute();
							EShaktiApplication.setLoanId(Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID);
							EShaktiApplication.setLoanName(Transaction_MemLoanRepaid_MenuFragment.sSelected_LoanName);
						}
					}
				}

			} else {
				// Offline

				int loan_size = SelectedGroupsTask.loan_Id.size();
				Log.e("Before inc Transaction_PersonalLoanRepaidFragment.memLoanCount  =  ",
						Transaction_PersonalLoanRepaidFragment.memLoanCount + "");
				int i = Transaction_PersonalLoanRepaidFragment.memLoanCount++;
				Log.v("After inc Transaction_PersonalLoanRepaidFragment.memLoanCount  =  ",
						Transaction_PersonalLoanRepaidFragment.memLoanCount + "  " + "  i = " + i);
				if (Transaction_PersonalLoanRepaidFragment.memLoanCount <= loan_size) {
					if (!SelectedGroupsTask.loan_Id.elementAt(i).equals("0")) {

						Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID = SelectedGroupsTask.loan_Id
								.elementAt(i);
						Transaction_MemLoanRepaid_MenuFragment.sSelected_LoanName = SelectedGroupsTask.loan_Name
								.elementAt(i);
						Log.e("&&&&&&&&&  Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID",
								Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID);

						Log.e("Loan Name", SelectedGroupsTask.loan_Name.elementAt(i) + "");
						Log.e("Acc No", SelectedGroupsTask.loanAcc_loanAccNo.elementAt(i) + "");
						Log.e("Loan Bank Name", SelectedGroupsTask.loanAcc_bankName.elementAt(i) + "");
						Log.e("Date", SelectedGroupsTask.loanAcc_loanDisbursementDate.elementAt(i) + "");

						EShaktiApplication
								.setMemberLoanRepaymentLoanBankName(SelectedGroupsTask.loanAcc_bankName.elementAt(i));
						EShaktiApplication
								.setMemberLoanRepaymentLoanAccNo(SelectedGroupsTask.loanAcc_loanAccNo.elementAt(i));

						EShaktiApplication.getInstance().getTransactionManager().startTransaction(
								DataType.GROUP_MASTER_MEMBERLOANOS,
								new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));

						EShaktiApplication.setLoanId(Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID);
						EShaktiApplication.setLoanName(Transaction_MemLoanRepaid_MenuFragment.sSelected_LoanName);
					} else {
						Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID = "0";
						Transaction_MemLoanRepaid_MenuFragment.sSelected_LoanName = SelectedGroupsTask.loan_Name
								.elementAt(i);
						EShaktiApplication.getInstance().getTransactionManager().startTransaction(
								DataType.DEFAULT_PLDISOS,
								new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));
						EShaktiApplication.setLoanId(Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID);
						EShaktiApplication.setLoanName(Transaction_MemLoanRepaid_MenuFragment.sSelected_LoanName);
					}
				}

			}
			isDefaultService = true;

			// isNextButton = true;

			break;

		default:
			break;
		}

	}

	private void onCallConfirmationDialog() {
		// TODO Auto-generated method stub

		if ((sPA_total != 0) || (sInterest_total != 0)) {

			confirmationDialog = new Dialog(getActivity());

			LayoutInflater inflater = getActivity().getLayoutInflater();
			View dialogView = inflater.inflate(R.layout.dialog_confirmation, null);
			dialogView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
					LayoutParams.WRAP_CONTENT));

			TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
			confirmationHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.confirmation));
			confirmationHeader.setTypeface(LoginActivity.sTypeface);

			TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

			for (int i = 0; i < sPrincipleAmounts.length; i++) {

				TableRow indv_SavingsRow = new TableRow(getActivity());

				@SuppressWarnings("deprecation")
				TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
						LayoutParams.WRAP_CONTENT, 1f);
				contentParams.setMargins(10, 5, 10, 5);

				TextView memberName_Text = new TextView(getActivity());
				memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
						String.valueOf(SelectedGroupsTask.member_Name.elementAt(i))));
				memberName_Text.setTypeface(LoginActivity.sTypeface);
				memberName_Text.setTextColor(color.black);
				memberName_Text.setPadding(5, 5, 5, 5);
				memberName_Text.setSingleLine(true);
				memberName_Text.setLayoutParams(contentParams);
				indv_SavingsRow.addView(memberName_Text);

				TextView confirm_PAvalues = new TextView(getActivity());
				confirm_PAvalues
						.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sPrincipleAmounts[i])));
				confirm_PAvalues.setTextColor(color.black);
				confirm_PAvalues.setPadding(5, 5, 5, 5);
				confirm_PAvalues.setGravity(Gravity.RIGHT);
				confirm_PAvalues.setLayoutParams(contentParams);
				indv_SavingsRow.addView(confirm_PAvalues);

				TextView confirm_Intrvalues = new TextView(getActivity());
				confirm_Intrvalues
						.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sInterestAmounts[i])));
				confirm_Intrvalues.setTextColor(color.black);
				confirm_Intrvalues.setPadding(5, 5, 5, 5);
				confirm_Intrvalues.setGravity(Gravity.RIGHT);
				confirm_Intrvalues.setLayoutParams(contentParams);
				indv_SavingsRow.addView(confirm_Intrvalues);

				confirmationTable.addView(indv_SavingsRow,
						new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

			}
			View rullerView = new View(getActivity());
			rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
			rullerView.setBackgroundColor(Color.rgb(0, 199, 140));// rgb(255,
																	// 229,
																	// 242));
			confirmationTable.addView(rullerView);

			TableRow totalRow = new TableRow(getActivity());

			TableRow.LayoutParams totalParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);
			totalParams.setMargins(10, 5, 10, 5);

			TextView totalText = new TextView(getActivity());
			totalText.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.total)));
			totalText.setTypeface(LoginActivity.sTypeface);
			totalText.setTextColor(color.black);
			totalText.setPadding(5, 5, 5, 5);// (5, 10, 5, 10);
			totalText.setLayoutParams(totalParams);
			totalRow.addView(totalText);

			TextView total_Principle_Amount = new TextView(getActivity());
			total_Principle_Amount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sPA_total)));
			total_Principle_Amount.setTextColor(color.black);
			total_Principle_Amount.setPadding(5, 5, 5, 5);// (5, 10,
															// 100, 10);
			total_Principle_Amount.setGravity(Gravity.RIGHT);
			total_Principle_Amount.setLayoutParams(totalParams);
			totalRow.addView(total_Principle_Amount);

			TextView total_Interest_Amount = new TextView(getActivity());
			total_Interest_Amount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sInterest_total)));
			total_Interest_Amount.setTextColor(color.black);
			total_Interest_Amount.setPadding(5, 5, 5, 5);// (5, 10,
															// 100, 10);
			total_Interest_Amount.setGravity(Gravity.RIGHT);
			total_Interest_Amount.setLayoutParams(totalParams);
			totalRow.addView(total_Interest_Amount);

			confirmationTable.addView(totalRow,
					new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

			mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit_button);
			mEdit_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.edit));
			mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
			mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
																	// 205,
																	// 0));
			mEdit_RaisedButton.setOnClickListener(this);

			mOk_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Ok_button);
			mOk_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.yes));
			mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
			mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
			mOk_RaisedButton.setOnClickListener(this);

			confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			confirmationDialog.setCanceledOnTouchOutside(false);
			confirmationDialog.setContentView(dialogView);
			confirmationDialog.setCancelable(true);
			confirmationDialog.show();

			MarginLayoutParams margin = (MarginLayoutParams) dialogView.getLayoutParams();
			margin.leftMargin = 10;
			margin.rightMargin = 10;
			margin.topMargin = 10;
			margin.bottomMargin = 10;
			margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);

		} else {

			if (outstandingAlert) {

				// Reset the values
				sSendToServer_MemLoanRepaid = Reset.reset(sSendToServer_MemLoanRepaid);
				sPA_total = Integer.parseInt(nullAmount);
				sInterest_total = Integer.parseInt(nullAmount);

				TastyToast.makeText(getActivity(), AppStrings.nullAlert, TastyToast.LENGTH_SHORT, TastyToast.WARNING);

			}

		}

	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub
		mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
		mProgressDilaog.show();
	}



	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub

		if (mProgressDilaog != null) {
			mProgressDilaog.dismiss();
			if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {

				if (confirmationDialog != null && confirmationDialog.isShowing()) {

					confirmationDialog.dismiss();
				}
				getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub

						if (!result.equals("FAIL")) {
							isServiceCall = false;

							TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg,
									TastyToast.LENGTH_SHORT, TastyToast.ERROR);
							Constants.NETWORKCOMMONFLAG = "SUCCESS";
						}

					}
				});

			} else {

				try {
					if (isDefaultService) {

						isDefaultService = false;
						if (Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID.equals("0")) {
							isDefaultService_LoanPurpose = true;
							new Get_PurposeOfLoanTask(this).execute();
							isDefaultService = false;
						} else {

							Transaction_MemLoanRepaidFragment memberLoanRepaidFragment = new Transaction_MemLoanRepaidFragment();
							setFragment(memberLoanRepaidFragment);

							int mFrag_InternalLoan = 0;
							mFrag_InternalLoan = sInterest_total + sPA_total
									+ EShaktiApplication.getStepwiseInternalloan();
							EShaktiApplication.setStepwiseInternalloan(mFrag_InternalLoan);

						}

					} else if (isDefaultService_LoanPurpose) {
						isDefaultService_LoanPurpose = false;

						Transaction_InternalLoan_DisbursementFragment savingsFragment = new Transaction_InternalLoan_DisbursementFragment();

						int mFrag_InternalLoan = 0;
						mFrag_InternalLoan = sInterest_total + sPA_total + EShaktiApplication.getStepwiseInternalloan();
						EShaktiApplication.setStepwiseInternalloan(mFrag_InternalLoan);
						setFragment(savingsFragment);

					} else if (isPrevious) {
						isPrevious = false;
						if (Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID.equals("0")) {
							Transaction_PersonalLoanRepaidFragment savingsFragment = new Transaction_PersonalLoanRepaidFragment();
							setFragment(savingsFragment);
						} else {
							Transaction_MemLoanRepaidFragment savingsFragment = new Transaction_MemLoanRepaidFragment();
							setFragment(savingsFragment);
						}
					} else if (isGetTrid) {
						if (!isNextButton) {
							callOfflineDataUpdate();
							isNextButton = false;
						}
						isGetTrid = false;

					}

					else {
						mServerService = false;
						if (GetResponseInfo.sResponseUpdate[0].equals("Yes")) {
						} else {
							TastyToast.makeText(getActivity(), AppStrings.transactionFailAlert, TastyToast.LENGTH_SHORT,
									TastyToast.ERROR);
							MainFragment_Dashboard fragment = new MainFragment_Dashboard();
							setFragment(fragment);
						}
						confirmationDialog.dismiss();
						new Get_LastTransactionID(Transaction_MemLoanRepaidFragment.this).execute();
						isGetTrid = true;

						// }
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Subscribe
	public void OnGetSingleTransaction(final GetSingleTransResponse getSingleTransResponse) {
		switch (getSingleTransResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), getSingleTransResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), getSingleTransResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			try {
				System.out.println("--------------OnGetSingleTransaction");
				Log.v("ContentValues", "Step 2");
				String mUniqueId = GetTransactionSinglevalues.getUniqueId();
				String mMemberLOSValues = GetTransactionSinglevalues.getMemberloanRepayment();
				String mLoanCheckValues = null;
				Log.e("Offline Unique ID Values is@@@@@", PrefUtils.getOfflineUniqueID());

				mLastTrDate = DatePickerDialog.sDashboardDate;
				mLastTr_ID = SelectedGroupsTask.sTr_ID.toString();
				String mCurrentTransDate = null;

				if (publicValues.mOffline_Trans_CurrentDate != null) {
					mCurrentTransDate = publicValues.mOffline_Trans_CurrentDate;
				}
				String mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
				String mMobileDate = Get_Offline_MobileDate.getOffline_MobileDate(getActivity());
				String mTransaction_UniqueId = PrefUtils.getOfflineUniqueID();

				System.out.println(
						"Offline Mem Loan ID : " + Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID);

				mOfflineMemberloanId = Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID;
				String mMemberLoanrepaid = sSendToServer_MemLoanRepaid + "#"
						+ Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID + "#" + mTrasactiondate + "#"
						+ mMobileDate + "$";

				System.out.println("DB val for ML repayment  : " + mMemberLoanrepaid);
				if (mMemberLOSValues == null) {
					publicValues.mMemberloanCheckLabel = new String[SelectedGroupsTask.loan_EngName.size()];
					for (int i = 0; i < publicValues.mMemberloanCheckLabel.length; i++) {
						publicValues.mMemberloanCheckLabel[i] = "0";

					}
				}
				if (publicValues.mMemberloanCheckLabel != null) {
					for (int j = 0; j < publicValues.mMemberloanCheckLabel.length; j++) {
						String sLoanID = SelectedGroupsTask.loan_Id.elementAt(j).toString();

						if (sLoanID.equals(mOfflineMemberloanId)) {
							mLoanCheckValues = publicValues.mMemberloanCheckLabel[j];
						}

					}
				}
				Log.e("Loan Check Values::::", mLoanCheckValues);
				Log.v("Current Member Loan Id", mOfflineMemberloanId);
				System.out.println("<<<<<<<<<<<<<<<_______>>>>>>>>>>" + PrefUtils.getOfflineMemberloanID());

				if (!mMemberLoanrepaid.contains("?") && !mCurrentTransDate.contains("?")
						&& !mTransaction_UniqueId.contains("?")) {

					if (mLoanCheckValues.equals("0")) {
						if (PrefUtils.getOfflineMLrepay() == null && PrefUtils.getOfflineMemberloanID() == null) {
							PrefUtils.setOfflineMLrepay(mMemberLoanrepaid);
						} else if (PrefUtils.getOfflineMemberloanID() != null
								&& !PrefUtils.getOfflineMemberloanID().equalsIgnoreCase(mOfflineMemberloanId)) {

							mMemberLoanrepaid = PrefUtils.getOfflineMLrepay() + mMemberLoanrepaid;
							PrefUtils.setOfflineMLrepay(mMemberLoanrepaid);
						}

						Log.v("Member Loan Repaid Values", PrefUtils.getOfflineMLrepay() + "");

						if (mUniqueId == null && mMemberLOSValues == null
								&& !PrefUtils.getOfflineMLrepay().equals(null)) {
							Log.e(TAG, " If 1");

							mSqliteDBStoredValues_memberloanrepayValues = mMemberLoanrepaid;
							if (mLastTrDate != null && mMobileDate != null && mTransaction_UniqueId != null
									&& mCurrentTransDate != null) {
								EShaktiApplication.getInstance().getTransactionManager().startTransaction(
										DataType.TRANSACTIONADD,
										new Transaction(0, PrefUtils.getUsernameKey(), SelectedGroupsTask.UserName,
												SelectedGroupsTask.Ngo_Id, SelectedGroupsTask.Group_Id,
												mTransaction_UniqueId, mLastTr_ID, mCurrentTransDate, null, null, null,
												mMemberLoanrepaid, null, null, null, null, null, null, null, null, null,
												null, null, null, null, null, null, null, null, null));
							}
						} else if (mUniqueId.equalsIgnoreCase(PrefUtils.getOfflineUniqueID())
								&& mMemberLOSValues == null) {
							Log.e("Member Loan Repayment!!!!!", mMemberLoanrepaid);
							Log.e(TAG, "Else If 2");
							EShaktiApplication.setSetTransValues(TransactionOffConstants.TRANS_MLRP);

							EShaktiApplication.getInstance().getTransactionManager().startTransaction(
									DataType.TRANS_VALUES_UPDATE, new TransactionUpdate(mUniqueId, mMemberLoanrepaid));

						} else if (mUniqueId.equalsIgnoreCase(PrefUtils.getOfflineUniqueID())
								&& mMemberLOSValues == null
								&& !PrefUtils.getOfflineMemberloanID().equalsIgnoreCase(mOfflineMemberloanId)) {
							Log.e("Member Loan Repayment!!!!!", mMemberLoanrepaid);
							Log.e(TAG, "Else If 2");
							EShaktiApplication.setSetTransValues(TransactionOffConstants.TRANS_MLRP);

							EShaktiApplication.getInstance().getTransactionManager().startTransaction(
									DataType.TRANS_VALUES_UPDATE, new TransactionUpdate(mUniqueId, mMemberLoanrepaid));

						} else if (mUniqueId.equalsIgnoreCase(PrefUtils.getOfflineUniqueID())
								&& mMemberLOSValues != null
								&& !PrefUtils.getOfflineMemberloanID().equalsIgnoreCase(mOfflineMemberloanId)) {
							Log.e("Member Loan Repayment!!!!!", mMemberLoanrepaid);
							Log.e(TAG, "Else If 2");
							EShaktiApplication.setSetTransValues(TransactionOffConstants.TRANS_MLRP);

							EShaktiApplication.getInstance().getTransactionManager().startTransaction(
									DataType.TRANS_VALUES_UPDATE, new TransactionUpdate(mUniqueId, mMemberLoanrepaid));

						} else if ((mMemberLOSValues != null)
								|| PrefUtils.getOfflineMemberloanID().equals(mOfflineMemberloanId)) {
							Log.e(TAG, "Else If 3");

							confirmationDialog.dismiss();

							TastyToast.makeText(getActivity(), AppStrings.offlineDataAvailable, TastyToast.LENGTH_SHORT,
									TastyToast.WARNING);

							DatePickerDialog.sDashboardDate = SelectedGroupsTask.sLastTransactionDate_Response;
							MainFragment_Dashboard fragment = new MainFragment_Dashboard();
							setFragment(fragment);

						}
					} else {
						confirmationDialog.dismiss();

						TastyToast.makeText(getActivity(), AppStrings.offlineDataAvailable, TastyToast.LENGTH_SHORT,
								TastyToast.WARNING);

						DatePickerDialog.sDashboardDate = SelectedGroupsTask.sLastTransactionDate_Response;
						MainFragment_Dashboard fragment = new MainFragment_Dashboard();
						setFragment(fragment);

					}
				} else {
					if (confirmationDialog.isShowing() && confirmationDialog != null) {
						confirmationDialog.dismiss();
					}
					Log.v("Checking Values!!!!!!!!!!!!!!!!!!!!!!! Else", "!!!!!!!!@@@@@@@@@@@@@@@s");
					TastyToast.makeText(getActivity(), "Please Try Again", TastyToast.LENGTH_SHORT, TastyToast.ERROR);

					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		}
	}

	@Subscribe
	public void onAddTransactionMemberLRepay(final TransactionResponse transactionResponse) {
		switch (transactionResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), transactionResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), transactionResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			try {
				System.out.println("--------------onAddTransactionMemberLRepay");

				TransactionProvider.getSinlgeGroupMaster_Transaction(
						new Transaction_UniqueIdSingle(PrefUtils.getOfflineUniqueID()));

				if (GetTransactionSinglevalues.getMemberloanRepayment() != null

						&& !GetTransactionSinglevalues.getMemberloanRepayment().equals("")) {
					StringBuilder builder = new StringBuilder();
					StringBuilder builder_ML = new StringBuilder();
					String mMasterresponse, mMLMemberId, mMLOS, result, mMLID, mMLOSResponse, mMLMasterresponse,
							mMLresult;
					for (int i = 0; i < sPrincipleAmounts.length; i++) {

						if (Integer.parseInt(mOutstanding[i]) >= Integer.parseInt(sPrincipleAmounts[i].trim())) {

							mOutstanding[i] = String.valueOf(
									Integer.parseInt(mOutstanding[i]) - Integer.parseInt(sPrincipleAmounts[i]));
							System.out.println("Current OutStanding" + mOutstanding[i]);

							mMLMemberId = String.valueOf(SelectedGroupsTask.member_Id.elementAt(i));

							mMLOS = mOutstanding[i];

							mMasterresponse = mMLMemberId + "~" + mMLOS + "~";

							builder.append(mMasterresponse);

						}
					}

					result = builder.toString();
					if (EShaktiApplication.isDefault()) {
						Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID = EShaktiApplication.getLoanId();
						Log.e("Step wise Loan id $$$$$",
								Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID + "");
					}
					sSendToServer_MemLoanRepaid = Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID + "#"
							+ result;

					Log.v("Total Bind Values", sSendToServer_MemLoanRepaid);

					String mCashinHand = String
							.valueOf(Integer.parseInt(SelectedGroupsTask.sCashinHand) + sPA_total + sInterest_total);
					if (EShaktiApplication.isDefault()) {
						for (int i = 0; i < Transaction_MemLoanRepaid_MenuFragment.response.length; i++) {
							String secondResponse[] = Transaction_MemLoanRepaid_MenuFragment.response[i].split("#");// sSendToServer_MemLoanRepaid.split("#");
							System.out.println("LOAN ID : " + secondResponse[0]);
							if (Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID
									.equals(secondResponse[0])) {

								System.out.println("LOAN OUTS : " + secondResponse[1]);
								secondResponse[1] = result;

							}
							mMLID = secondResponse[0];
							mMLOSResponse = secondResponse[1];
							mMLMasterresponse = mMLID + "#" + mMLOSResponse + "%";
							builder_ML.append(mMLMasterresponse);

						}
					} else {
						for (int i = 0; i < Transaction_MemLoanRepaid_MenuFragment.response.length; i++) {

							String secondResponse[] = Transaction_MemLoanRepaid_MenuFragment.response[i].split("#");
							System.out.println("LOAN ID : " + secondResponse[0]);
							if (Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID
									.equals(secondResponse[0])) {

								System.out.println("LOAN OUTS : " + secondResponse[1]);
								secondResponse[1] = result;

							}
							mMLID = secondResponse[0];
							mMLOSResponse = secondResponse[1];
							mMLMasterresponse = mMLID + "#" + mMLOSResponse + "%";
							builder_ML.append(mMLMasterresponse);

						}
					}
					mMLresult = builder_ML.toString();
					Log.e(TAG, mMLresult);
					String mCashatBank = SelectedGroupsTask.sCashatBank;
					String mBankDetails = GetOfflineTransactionValues.getBankDetails();
					SelectedGroupsTask.sCashinHand = mCashinHand;
					String mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
					SelectedGroupsTask.sLastTransactionDate_Response = mTrasactiondate;
					String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mTrasactiondate,
							mCashinHand, mCashatBank, mBankDetails);
					String mSelectedGroupId = SelectedGroupsTask.Group_Id;
					EShaktiApplication.setPLOS(false);
					System.out.println("Master Group Member Outstanding!!!!!" + mMLresult);
					EShaktiApplication.getInstance().getTransactionManager().startTransaction(
							DataType.GROUP_MASTER_MLOS,
							new GroupMLUpdate(mSelectedGroupId, mGroupMasterResponse, mMLresult));

					Log.e(TAG, "Sucess" + SelectedGroupsTask.sCashinHand + "Cash In Hand are equals" + mCashinHand);

				} else {
					if (confirmationDialog.isShowing() && confirmationDialog != null) {
						confirmationDialog.dismiss();
					}

					TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT, TastyToast.ERROR);

					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);

				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		}
	}

	@Subscribe
	public void OnTransUpdateMLRepay(final TransactionUpdateResponse transactionUpdateResponse) {
		switch (transactionUpdateResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), transactionUpdateResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), transactionUpdateResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			try {
				System.out.println("--------------OnTransUpdateMLRepay");

				TransactionProvider.getSinlgeGroupMaster_Transaction(
						new Transaction_UniqueIdSingle(PrefUtils.getOfflineUniqueID()));

				if (GetTransactionSinglevalues.getMemberloanRepayment() != null

						&& !GetTransactionSinglevalues.getMemberloanRepayment().equals("")) {
					confirmationDialog.dismiss();
					Log.e("Saving Insert Sucessfully", "Sucess");
					Log.v("ContentValues", "Step 4");
					StringBuilder builder = new StringBuilder();
					StringBuilder builder_ML = new StringBuilder();
					String mMasterresponse, mMLMemberId, mMLOS, result, mMLID, mMLOSResponse, mMLMasterresponse,
							mMLresult;
					for (int i = 0; i < sPrincipleAmounts.length; i++) {

						if (Integer.parseInt(mOutstanding[i]) >= Integer.parseInt(sPrincipleAmounts[i].trim())) {

							mOutstanding[i] = String.valueOf(
									Integer.parseInt(mOutstanding[i]) - Integer.parseInt(sPrincipleAmounts[i]));
							System.out.println("Current OutStanding" + mOutstanding[i]);

							mMLMemberId = String.valueOf(SelectedGroupsTask.member_Id.elementAt(i));

							mMLOS = mOutstanding[i];

							mMasterresponse = mMLMemberId + "~" + mMLOS + "~";

							builder.append(mMasterresponse);

						}
					}
					result = builder.toString();
					if (EShaktiApplication.isDefault()) {
						Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID = EShaktiApplication.getLoanId();
						Log.e("Step wise Loan id $$$$$",
								Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID + "");
					}
					sSendToServer_MemLoanRepaid = Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID + "#"
							+ result;

					Log.v("Total Bind Values", sSendToServer_MemLoanRepaid);
					String mCashinHand = String
							.valueOf(Integer.parseInt(SelectedGroupsTask.sCashinHand) + sPA_total + sInterest_total);

					if (EShaktiApplication.isDefault()) {

						if (Transaction_MemLoanRepaid_MenuFragment.response != null) {

							for (int i = 0; i < Transaction_MemLoanRepaid_MenuFragment.response.length; i++) {
								String secondResponse[] = Transaction_MemLoanRepaid_MenuFragment.response[i].split("#");// sSendToServer_MemLoanRepaid.split("#");
								System.out.println("LOAN ID : " + secondResponse[0]);
								Log.e("Step wise Member loan entry Values--------->>>>", secondResponse[1] + "");
								if (Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID
										.equals(secondResponse[0])) {

									secondResponse[1] = result;

								}
								mMLID = secondResponse[0];
								mMLOSResponse = secondResponse[1];
								mMLMasterresponse = mMLID + "#" + mMLOSResponse + "%";
								builder_ML.append(mMLMasterresponse);

							}
						} else {

							TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT,
									TastyToast.ERROR);
						}
					} else {
						if (Transaction_MemLoanRepaid_MenuFragment.response != null) {

							for (int i = 0; i < Transaction_MemLoanRepaid_MenuFragment.response.length; i++) {

								String secondResponse[] = Transaction_MemLoanRepaid_MenuFragment.response[i].split("#");
								System.out.println("LOAN ID : " + secondResponse[0]);
								if (Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID
										.equals(secondResponse[0])) {

									secondResponse[1] = result;

								}
								mMLID = secondResponse[0];
								mMLOSResponse = secondResponse[1];
								mMLMasterresponse = mMLID + "#" + mMLOSResponse + "%";
								builder_ML.append(mMLMasterresponse);

							}
						} else {
							TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT,
									TastyToast.ERROR);
						}
					}
					mMLresult = builder_ML.toString();

					Log.e("Current Member loan Outstanding", mMLresult);

					String mCashatBank = SelectedGroupsTask.sCashatBank;
					String mBankDetails = GetOfflineTransactionValues.getBankDetails();
					SelectedGroupsTask.sCashinHand = mCashinHand;
					String mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
					SelectedGroupsTask.sLastTransactionDate_Response = mTrasactiondate;
					String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mTrasactiondate,
							mCashinHand, mCashatBank, mBankDetails);
					String mSelectedGroupId = SelectedGroupsTask.Group_Id;
					EShaktiApplication.setPLOS(false);
					Log.e("Get GroupResponse121221", mGroupMasterResponse);
					System.out.println("Master Group Member Outstanding!!!!!" + mMLresult);
					EShaktiApplication.getInstance().getTransactionManager().startTransaction(
							DataType.GROUP_MASTER_MLOS,
							new GroupMLUpdate(mSelectedGroupId, mGroupMasterResponse, mMLresult));

				} else {
					if (confirmationDialog.isShowing() && confirmationDialog != null) {
						confirmationDialog.dismiss();
					}

					TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT, TastyToast.ERROR);

					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);

				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		}
	}

	@Subscribe
	public void OnGroupMLOSUpdate(final GroupMLOSUpdateResponse masterResponse) {
		switch (masterResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), masterResponse.getFetcherResult().getMessage(), TastyToast.LENGTH_SHORT,
					TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), masterResponse.getFetcherResult().getMessage(), TastyToast.LENGTH_SHORT,
					TastyToast.ERROR);
			break;
		case SUCCESS:

			try {
				System.out.println("--------------OnGroupMLOSUpdate");
				TastyToast.makeText(getActivity(), AppStrings.transactionCompleted, TastyToast.LENGTH_SHORT,
						TastyToast.SUCCESS);
				confirmationDialog.dismiss();

				if (ConnectionUtils.isNetworkAvailable(getActivity())) {
					if (EShaktiApplication.isDefault()) {
						isDefaultService = true;

						int loan_size = SelectedGroupsTask.loan_Id.size();
						Log.e("Before inc Transaction_PersonalLoanRepaidFragment.memLoanCount  =  ",
								Transaction_PersonalLoanRepaidFragment.memLoanCount + "");
						int i = Transaction_PersonalLoanRepaidFragment.memLoanCount++;
						Log.v("After inc Transaction_PersonalLoanRepaidFragment.memLoanCount  =  ",
								Transaction_PersonalLoanRepaidFragment.memLoanCount + "  " + "  i = " + i);
						if (Transaction_PersonalLoanRepaidFragment.memLoanCount <= loan_size) {
							if (!SelectedGroupsTask.loan_Id.elementAt(i).equals("0")) {
								Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID = SelectedGroupsTask.loan_Id
										.elementAt(i);
								Transaction_MemLoanRepaid_MenuFragment.sSelected_LoanName = SelectedGroupsTask.loan_Name
										.elementAt(i);
								Log.e("&&&&&&&&&  Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID",
										Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID);
								new Get_All_Mem_OutstandingTask(this).execute();

								EShaktiApplication
										.setLoanId(Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID);
								EShaktiApplication
										.setLoanName(Transaction_MemLoanRepaid_MenuFragment.sSelected_LoanName);

								EShaktiApplication.setMemberLoanRepaymentLoanBankName(
										SelectedGroupsTask.loanAcc_bankName.elementAt(i));
								EShaktiApplication.setMemberLoanRepaymentLoanAccNo(
										SelectedGroupsTask.loanAcc_loanAccNo.elementAt(i));
							} else {
								Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID = "0";
								Transaction_MemLoanRepaid_MenuFragment.sSelected_LoanName = SelectedGroupsTask.loan_Name
										.elementAt(i);
								new Get_All_Mem_OutstandingTask(this).execute();

								EShaktiApplication
										.setLoanId(Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID);
								EShaktiApplication
										.setLoanName(Transaction_MemLoanRepaid_MenuFragment.sSelected_LoanName);
							}
						}
					} else {

						MainFragment_Dashboard fragment = new MainFragment_Dashboard();
						setFragment(fragment);
					}
				} else {
					PrefUtils.setOfflineMemberLoanID(mOfflineMemberloanId);
					for (int j = 0; j < publicValues.mMemberloanCheckLabel.length; j++) {
						String sLoanID = SelectedGroupsTask.loan_Id.elementAt(j).toString();

						if (sLoanID.equals(PrefUtils.getOfflineMemberloanID())) {
							publicValues.mMemberloanCheckLabel[j] = "1";
						}

					}

					try {

						if (EShaktiApplication.isDefault()) {

							int loan_size = SelectedGroupsTask.loan_Id.size();
							Log.e("Before inc Transaction_PersonalLoanRepaidFragment.memLoanCount  =  ",
									Transaction_PersonalLoanRepaidFragment.memLoanCount + "");
							int i = Transaction_PersonalLoanRepaidFragment.memLoanCount++;
							Log.v("After inc Transaction_PersonalLoanRepaidFragment.memLoanCount  =  ",
									Transaction_PersonalLoanRepaidFragment.memLoanCount + "  " + "  i = " + i);
							Log.e("----i value == ", i + "");
							if (Transaction_PersonalLoanRepaidFragment.memLoanCount <= loan_size) {
								if (!SelectedGroupsTask.loan_Id.elementAt(i).equals("0")) {
									Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID = SelectedGroupsTask.loan_Id
											.elementAt(i);
									Transaction_MemLoanRepaid_MenuFragment.sSelected_LoanName = SelectedGroupsTask.loan_Name
											.elementAt(i);
									Log.e("&&&&&&&&&  Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID",
											Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID);

									EShaktiApplication.setMemberLoanRepaymentLoanBankName(
											SelectedGroupsTask.loanAcc_bankName.elementAt(i));
									EShaktiApplication.setMemberLoanRepaymentLoanAccNo(
											SelectedGroupsTask.loanAcc_loanAccNo.elementAt(i));

									EShaktiApplication
											.setLoanId(Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID);
									EShaktiApplication
											.setLoanName(Transaction_MemLoanRepaid_MenuFragment.sSelected_LoanName);

									EShaktiApplication.getInstance().getTransactionManager().startTransaction(
											DataType.GROUP_MASTER_MEMBERLOANOS,
											new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));

								} else {
									Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID = "0";
									Transaction_MemLoanRepaid_MenuFragment.sSelected_LoanName = SelectedGroupsTask.loan_Name
											.elementAt(i);

									EShaktiApplication
											.setLoanId(Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID);
									EShaktiApplication
											.setLoanName(Transaction_MemLoanRepaid_MenuFragment.sSelected_LoanName);
									EShaktiApplication.getInstance().getTransactionManager().startTransaction(
											DataType.DEFAULT_PLDISOS, // (DataType.DEFAULT_PLOS,
											new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));

								}
							}
						} else {

							if (!ConnectionUtils.isNetworkAvailable(getActivity())) {

								String mValues = Put_DB_GroupNameDetail_Response
										.put_DB_GroupNameDetails_Response(EShaktiApplication.getGroupResponse());

								GroupProvider.updateGroupResponse(new GroupResponseUpdate(
										EShaktiApplication.getGroupId_GroupLastTransDate(), mValues));
							}
							MainFragment_Dashboard fragment = new MainFragment_Dashboard();
							setFragment(fragment);
						}

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		}
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		SBus.INST.unRegister(this);
	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub
		Log.e("Current Class Name!!!!!!!!!!!!!!", fragment.getClass().getName());
		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

		FragmentDrawer.drawer_CashinHand.setText(
				RegionalConversion.getRegionalConversion(AppStrings.cashinhand) + SelectedGroupsTask.sCashinHand);
		FragmentDrawer.drawer_CashinHand.setTypeface(LoginActivity.sTypeface);

		FragmentDrawer.drawer_CashatBank.setText(
				RegionalConversion.getRegionalConversion(AppStrings.cashatBank) + SelectedGroupsTask.sCashatBank);
		FragmentDrawer.drawer_CashatBank.setTypeface(LoginActivity.sTypeface);

		FragmentDrawer.drawer_lastTR_Date
				.setText(RegionalConversion.getRegionalConversion(AppStrings.lastTransactionDate) + " : "
						+ SelectedGroupsTask.sLastTransactionDate_Response);
		FragmentDrawer.drawer_lastTR_Date.setTypeface(LoginActivity.sTypeface);

		UpdateCalendarMinMaxDateUtils.OnUpdateCalendarDate();

		Calendar calender = Calendar.getInstance();

		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String formattedDate = df.format(calender.getTime());
		EShaktiApplication.setLastTransDate_GroupId(SelectedGroupsTask.Group_Id);

		new GroupTempDBLastTransDate(EShaktiApplication.getLastTransDate_GroupId(),
				SelectedGroupsTask.sLastTransactionDate_Response, formattedDate);

		GroupProvider.updateGroupLastTransDateResponse();

	}

	private void callOfflineDataUpdate() {
		// TODO Auto-generated method stub
		StringBuilder builder = new StringBuilder();
		StringBuilder builder_ML = new StringBuilder();
		String mMasterresponse, mMLMemberId, mMLOS, result, mMLID, mMLOSResponse, mMLMasterresponse, mMLresult;
		for (int i = 0; i < sPrincipleAmounts.length; i++) {

			if (Integer.parseInt(mOutstanding[i]) >= Integer.parseInt(sPrincipleAmounts[i].trim())) {

				mOutstanding[i] = String
						.valueOf(Integer.parseInt(mOutstanding[i]) - Integer.parseInt(sPrincipleAmounts[i]));
				System.out.println("Current OutStanding" + mOutstanding[i]);

				mMLMemberId = String.valueOf(SelectedGroupsTask.member_Id.elementAt(i));

				mMLOS = mOutstanding[i];

				mMasterresponse = mMLMemberId + "~" + mMLOS + "~";

				builder.append(mMasterresponse);

			}
		}
		result = builder.toString();
		if (EShaktiApplication.isDefault()) {
			Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID = EShaktiApplication.getLoanId();
			Log.e("Step wise Loan id $$$$$", Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID + "");
		}
		sSendToServer_MemLoanRepaid = Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID + "#" + result;

		Log.v("Total Bind Values", sSendToServer_MemLoanRepaid);

		if (EShaktiApplication.isDefault()) {

			GroupDetailsProvider.getSinlgeGroupMaster(new GroupMasterSingle(SelectedGroupsTask.Group_Id));

			String mMemberLoanResponse = GetGroupMemberDetails.getMemberloanOS();
			Transaction_MemLoanRepaid_MenuFragment.response = mMemberLoanResponse.split("%");
			Log.e("Length Values--->>>>>", Transaction_MemLoanRepaid_MenuFragment.response.length + "");

			for (int i = 0; i < Transaction_MemLoanRepaid_MenuFragment.response.length; i++) {

				String secondResponse[] = Transaction_MemLoanRepaid_MenuFragment.response[i].split("#");
				System.out.println("LOAN ID : " + secondResponse[0]);
				if (Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID.equals(secondResponse[0])) {

					secondResponse[1] = result;

				}
				Log.e("Member Loan------->>>>>>", Transaction_MemLoanRepaid_MenuFragment.response[i]);
				mMLID = secondResponse[0];
				mMLOSResponse = secondResponse[1];
				mMLMasterresponse = mMLID + "#" + mMLOSResponse + "%";
				builder_ML.append(mMLMasterresponse);

			}

			Log.e("Member Loan repaid ment---------------->>>>>>", result);

		} else {

			for (int i = 0; i < Transaction_MemLoanRepaid_MenuFragment.response.length; i++) {

				String secondResponse[] = Transaction_MemLoanRepaid_MenuFragment.response[i].split("#");
				System.out.println("LOAN ID : " + secondResponse[0]);
				if (Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID.equals(secondResponse[0])) {

					secondResponse[1] = result;

				}
				mMLID = secondResponse[0];
				mMLOSResponse = secondResponse[1];
				mMLMasterresponse = mMLID + "#" + mMLOSResponse + "%";
				builder_ML.append(mMLMasterresponse);

			}
		}
		mMLresult = builder_ML.toString();

		String mCashatBank = SelectedGroupsTask.sCashatBank;
		String mBankDetails = GetOfflineTransactionValues.getBankDetails();
		String mCashinHand = SelectedGroupsTask.sCashinHand;
		mLastTrDate = SelectedGroupsTask.sLastTransactionDate_Response;

		String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mLastTrDate, mCashinHand, mCashatBank,
				mBankDetails);
		String mSelectedGroupId = SelectedGroupsTask.Group_Id;
		EShaktiApplication.setPLOS(false);
		Log.e("Get GroupResponse121221", mGroupMasterResponse);
		isServiceCall = false;
		EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GROUP_MASTER_MLOS,
				new GroupMLUpdate(mSelectedGroupId, mGroupMasterResponse, mMLresult));

	}

	@Subscribe
	public void OnGroupMasterResponse(final GroupMemberloanOSResponse groupMasterResponse) {
		switch (groupMasterResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), groupMasterResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), groupMasterResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			try {
				String mMemberLoanResponse = GetGroupMemberDetails.getMemberloanOS();

				Log.e("OnGroupMasterResponse ===  ", mMemberLoanResponse.toString());
				if (mMemberLoanResponse != null && !mMemberLoanResponse.equals("")) {
					response = mMemberLoanResponse.split("%");

					for (int i = 0; i < response.length; i++) {
						System.out.println("Response  ****: " + response[i]);

						String secondResponse[] = response[i].split("#");
						System.out.println("LOAN ID : " + secondResponse[0]);
						if (Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID.equals(secondResponse[0])) {
							System.out.println("LOAN OUTS : " + secondResponse[1]);
							Transaction_MemLoanRepaid_MenuFragment.mMemberOS_Offlineresponse = secondResponse[1];
						}

					}

					int mFrag_InternalLoan = 0;
					mFrag_InternalLoan = sInterest_total + sPA_total + EShaktiApplication.getStepwiseInternalloan();
					EShaktiApplication.setStepwiseInternalloan(mFrag_InternalLoan);

					if (isPrevious) {
						isPrevious = false;
					}
					Transaction_MemLoanRepaid_MenuFragment.response = response;
					Transaction_MemLoanRepaidFragment fragment = new Transaction_MemLoanRepaidFragment();
					setFragment(fragment);
				} else {
					TastyToast.makeText(getActivity(), AppStrings.loginAgainAlert, TastyToast.LENGTH_LONG,
							TastyToast.WARNING);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		}

	}

	@Subscribe
	public void OnGroupMasterPLDBResponse(final DefaultOffline_PL_DisResponse groupMasterPLDBResponse) {
		switch (groupMasterPLDBResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), groupMasterPLDBResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), groupMasterPLDBResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			try {
				System.out.println("------------OnGroupMasterPLDBResponse");
				MainFragment_Dashboard.mPLDBResponse = GetGroupMemberDetails.getPersonaloanOS();
				MainFragment_Dashboard.mPLDBPurposeofloan = GetGroupMemberDetails.getPurposeofloan();

				PrefUtils.setStepWiseScreenCount("4");
				Transaction_InternalLoan_DisbursementFragment savingsFragment = new Transaction_InternalLoan_DisbursementFragment();

				int mFrag_InternalLoan = 0;
				mFrag_InternalLoan = sInterest_total + sPA_total + EShaktiApplication.getStepwiseInternalloan();
				EShaktiApplication.setStepwiseInternalloan(mFrag_InternalLoan);
				setFragment(savingsFragment);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		}

	}

	@Subscribe
	public void OnGroupMasterPLOSResponse(final DefaultOffline_PLResponse groupPersonalloanOSResponse) {
		switch (groupPersonalloanOSResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), groupPersonalloanOSResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), groupPersonalloanOSResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:
			try {
				System.out.println("--------------OnGroupMasterPLOSResponse");
				String mMemberLoanResponse = GetGroupMemberDetails.getPersonaloanOS();
				response = mMemberLoanResponse.split("#");

				for (int i = 0; i < response.length; i++) {
					System.out.println("Response : " + response[i]);

					System.out.println("LOAN ID : " + response[0]);
					System.out.println("LOAN ID : " + response[1]);
					Transaction_MemLoanRepaid_MenuFragment.mPersonalOS_offlineresponse = response[1];
				}

				if (isPrevious) {
					Transaction_PersonalLoanRepaidFragment savingsFragment = new Transaction_PersonalLoanRepaidFragment();
					setFragment(savingsFragment);
					isPrevious = false;
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		}

	}

}
