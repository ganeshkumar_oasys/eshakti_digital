
package com.yesteam.eshakti.view.fragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.adapter.CustomItemAdapter;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;
import com.yesteam.eshakti.model.RowItem;
import com.yesteam.eshakti.sqlite.database.GroupProvider;
import com.yesteam.eshakti.sqlite.db.model.GroupTempDBLastTransDate;
import com.yesteam.eshakti.utils.UpdateCalendarMinMaxDateUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.views.MaterialSpinner;
import com.yesteam.eshakti.views.RaisedButton;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Dialog;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

public class Transaction_Sb_Loan_acc_disburseFragment extends Fragment implements OnClickListener {

	private TextView mGroupName, mCashInHand, mCashAtBank, mHeader;
	private TextView mDisbursementAmountText, mBalanceAmountText, mMemberDisbursementAmountText,
			mDisbursementAmount_value, mBalanceAmount_value, mMemberDisbursementAmount_value;
	private RaisedButton mSubmitButton;
	RadioButton mCashRadio, mBankRadio;
	public static String selectedType, selectedItemBank;

	MaterialSpinner materialSpinner_Bank;
	CustomItemAdapter bankNameAdapter;
	private List<RowItem> bankNameItems;
	public static String mBankNameValue = null;
	LinearLayout mSpinnerLayout;
	ArrayList<String> mBanknames_Array = new ArrayList<String>();
	ArrayList<String> mBanknamesId_Array = new ArrayList<String>();
	ArrayList<String> mEngSendtoServerBank_Array = new ArrayList<String>();
	ArrayList<String> mEngSendtoServerBankId_Array = new ArrayList<String>();
	Dialog confirmationDialog;
	View rootView;
	Date date_dashboard, date_loanDisb;

	public Transaction_Sb_Loan_acc_disburseFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_LOAN_SB_INSTALLMENT;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		rootView = inflater.inflate(R.layout.fragment_sb_acc_loandisburse, container, false);

		MainFragment_Dashboard.isBackpressed = false;
		// Constants.FRAG_BACKPRESS_CONSTANT =
		// Constants.FRAG_INSTANCE_LOANACCWITHDRAWAL;

		try {

			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashInHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashInHand.setTypeface(LoginActivity.sTypeface);

			mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashAtBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashAtBank.setTypeface(LoginActivity.sTypeface);

			init();
			for (int i = 0; i < SelectedGroupsTask.sBankNames.size(); i++) {
				mBanknames_Array.add(SelectedGroupsTask.sBankNames.elementAt(i).toString());
				mBanknamesId_Array.add(String.valueOf(i));
			}

			for (int i = 0; i < SelectedGroupsTask.sEngBankNames.size(); i++) {
				mEngSendtoServerBank_Array.add(SelectedGroupsTask.sEngBankNames.elementAt(i).toString());
				mEngSendtoServerBankId_Array.add(String.valueOf(i));
			}

			materialSpinner_Bank.setBaseColor(color.grey_400);

			materialSpinner_Bank.setFloatingLabelText(AppStrings.bankName);

			materialSpinner_Bank.setPaddingSafe(10, 0, 10, 0);

			final String[] bankNames = new String[SelectedGroupsTask.sBankNames.size() + 1];

			final String[] bankNames_BankID = new String[SelectedGroupsTask.sEngBankNames.size() + 1];
			
			final String[] bankAmount = new String[SelectedGroupsTask.sBankAmt.size() + 1];

			bankNames[0] = String.valueOf(RegionalConversion.getRegionalConversion(AppStrings.bankName));
			for (int i = 0; i < SelectedGroupsTask.sBankNames.size(); i++) {
				bankNames[i + 1] = SelectedGroupsTask.sBankNames.elementAt(i).toString();
			}

			bankNames_BankID[0] = String.valueOf(RegionalConversion.getRegionalConversion(AppStrings.bankName));
			for (int i = 0; i < SelectedGroupsTask.sEngBankNames.size(); i++) {
				bankNames_BankID[i + 1] = SelectedGroupsTask.sEngBankNames.elementAt(i).toString();
			}

			bankAmount[0] = String.valueOf("Bank Amount");
			for (int i = 0; i < SelectedGroupsTask.sBankAmt.size(); i++) {
				bankAmount[i + 1] = SelectedGroupsTask.sBankAmt.elementAt(i).toString();
			}

			int size = bankNames.length;

			bankNameItems = new ArrayList<RowItem>();
			for (int i = 0; i < size; i++) {
				RowItem rowItem = new RowItem(bankNames[i]);// SelectedGroupsTask.sBankNames.elementAt(i).toString());
				bankNameItems.add(rowItem);
			}
			bankNameAdapter = new CustomItemAdapter(getActivity(), bankNameItems);
			materialSpinner_Bank.setAdapter(bankNameAdapter);

			materialSpinner_Bank.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
					// TODO Auto-generated method stub

					if (position == 0) {
						selectedItemBank = bankNames_BankID[0];
						mBankNameValue = "0";
						EShaktiApplication.setSelectedBankAmount("");
					} else {
						selectedItemBank = bankNames_BankID[position];
						System.out.println("SELECTED BANK NAME : " + selectedItemBank);
						mBankNameValue = selectedItemBank;
						String mBankname = null;
						for (int i = 0; i < mBanknames_Array.size(); i++) {
							if (selectedItemBank.equals(mEngSendtoServerBank_Array.get(i))) {
								mBankname = mEngSendtoServerBank_Array.get(i);
								EShaktiApplication.setSelectedBankAmount(bankAmount[i+1]);
							}
						}

						mBankNameValue = mBankname;

					}
					Log.e("Selected Bank Name value", mBankNameValue.toString()+"");
					Log.e("Selected Bank Amount", EShaktiApplication.getSelectedBankAmount().toString()+"");

				}

				@Override
				public void onNothingSelected(AdapterView<?> parent) {
					// TODO Auto-generated method stub

				}
			});

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return rootView;
	}

	private void init() {
		// TODO Auto-generated method stub

		mHeader = (TextView) rootView.findViewById(R.id.sbAccDisbursementheader);
		mDisbursementAmountText = (TextView) rootView.findViewById(R.id.sbAcc_disbursementAmountTextView);
		mDisbursementAmount_value = (TextView) rootView.findViewById(R.id.sbAcc_disbursementAmount_values);
		mBalanceAmountText = (TextView) rootView.findViewById(R.id.sbAcc_balanceAmountTextView);
		mBalanceAmount_value = (TextView) rootView.findViewById(R.id.sbAcc_balanceAmount_values);
		mMemberDisbursementAmountText = (TextView) rootView.findViewById(R.id.sbAcc_memberDisbursementAmountTextView);
		mMemberDisbursementAmount_value = (TextView) rootView.findViewById(R.id.sbAcc_memberDisbursementAmount_values);

		mCashRadio = (RadioButton) rootView.findViewById(R.id.radioDisbursementLimitCash_sb);
		mBankRadio = (RadioButton) rootView.findViewById(R.id.radioDisbursementLimitBank_sb);

		mHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.mLoanDisbursementFromSbAcc));
		mDisbursementAmountText.setText(RegionalConversion.getRegionalConversion(AppStrings.mDisbursementAmount));
		mBalanceAmountText.setText(RegionalConversion.getRegionalConversion(AppStrings.mBalanceAmount));
		mMemberDisbursementAmountText
				.setText(RegionalConversion.getRegionalConversion(AppStrings.mMemberDisbursementAmount));

		mHeader.setTypeface(LoginActivity.sTypeface);
		mDisbursementAmountText.setTypeface(LoginActivity.sTypeface);
		mDisbursementAmount_value.setTypeface(LoginActivity.sTypeface);
		mBalanceAmountText.setTypeface(LoginActivity.sTypeface);
		mBalanceAmount_value.setTypeface(LoginActivity.sTypeface);
		mMemberDisbursementAmountText.setTypeface(LoginActivity.sTypeface);
		mMemberDisbursementAmount_value.setTypeface(LoginActivity.sTypeface);

		if (publicValues.mGetLoanBalnceValues != null) {
			String[] mValues = publicValues.mGetLoanBalnceValues.split("#");
			String mValues_limit = mValues[2];
			String[] mLimitValues = mValues_limit.split("~");
			mDisbursementAmount_value.setText(mLimitValues[0]);
			mMemberDisbursementAmount_value.setText(mLimitValues[1]);
			mBalanceAmount_value.setText(mLimitValues[2]);

		}

		mSubmitButton = (RaisedButton) rootView.findViewById(R.id.sbAccDisbursement_submit);
		mSubmitButton.setText(RegionalConversion.getRegionalConversion(AppStrings.next));
		mSubmitButton.setTypeface(LoginActivity.sTypeface);
		mSubmitButton.setOnClickListener(this);

		mSpinnerLayout = (LinearLayout) rootView.findViewById(R.id.loan_dis_bankSpinnerlayout_sb);
		mSpinnerLayout.setVisibility(View.GONE);

		materialSpinner_Bank = (MaterialSpinner) rootView.findViewById(R.id.loan_dis_bankspinner_sb);
		RadioGroup radioGroup = (RadioGroup) rootView.findViewById(R.id.radioDisbursementLimit_sb);
		radioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// checkedId is the RadioButton selected
				switch (checkedId) {
				case R.id.radioDisbursementLimitCash_sb:
					selectedType = "Cash";
					mSpinnerLayout.setVisibility(View.GONE);
					break;

				case R.id.radioDisbursementLimitBank_sb:
					selectedType = "Bank";
					mSpinnerLayout.setVisibility(View.VISIBLE);

					break;
				}
			}
		});
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.sbAccDisbursement_submit:
			
			mBanknames_Array.clear();
			String sbAcc_balanceAmount = mBalanceAmount_value.getText().toString();
			
			Log.e("SB ACCOUNT BALANCE AMOUNT", sbAcc_balanceAmount+"");
			if (Integer.parseInt(sbAcc_balanceAmount) != 0) {
				
				if (mBankRadio.isChecked() || mCashRadio.isChecked()) {
				//	String dashBoardDate = DatePickerDialog.sDashboardDate;
					String dashBoardDate = null;
					if (DatePickerDialog.sDashboardDate.contains("-")) {
						dashBoardDate = DatePickerDialog.sDashboardDate;
					} else if (DatePickerDialog.sDashboardDate.contains("/")) {
						String[] dateArr = DatePickerDialog.sDashboardDate.split("/");
						dashBoardDate = dateArr[0] + "-" + dateArr[1] + "-" + dateArr[2];;
					}
					
					String loanDisbArr[] = EShaktiApplication.getLoanAcc_LoanDisbursementDate().split("/");
					String loanDisbDate = loanDisbArr[1]+"-"+loanDisbArr[0]+"-"+loanDisbArr[2];
					
					SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
					
					try {
						date_dashboard = sdf.parse(dashBoardDate);
						date_loanDisb = sdf.parse(loanDisbDate);
					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					if (date_dashboard.compareTo(date_loanDisb) >= 0) {
					
					if (mBankRadio.isChecked()) {

						if (!mBankNameValue.equals("0")) {
							EShaktiApplication.setSelectedType("BANK");
							EShaktiApplication.setSBAccBalanceAmount(sbAcc_balanceAmount);
							Transaction_Loan_SB_disbursementFragment disbursementFragment = new Transaction_Loan_SB_disbursementFragment();
							setFragment(disbursementFragment);

						} else {
							TastyToast.makeText(getActivity(),AppStrings.nullDetailsAlert,
											TastyToast.LENGTH_SHORT,
											TastyToast.WARNING);
						}
					} else {
						EShaktiApplication.setSelectedType("CASH");
						EShaktiApplication
								.setSBAccBalanceAmount(sbAcc_balanceAmount);
						Transaction_Loan_SB_disbursementFragment disbursementFragment = new Transaction_Loan_SB_disbursementFragment();
						setFragment(disbursementFragment);
						
					}
					
					} else {
						TastyToast.makeText(getActivity(), AppStrings.mCheckDisbursementDate, TastyToast.LENGTH_SHORT, TastyToast.WARNING);
					}

				} else {
					TastyToast.makeText(getActivity(), AppStrings.mLoanaccCash_BankToast, TastyToast.LENGTH_SHORT,
							TastyToast.WARNING);
				}
			
			} else {
				TastyToast.makeText(getActivity(), AppStrings.mCheckbalanceAlert, TastyToast.LENGTH_SHORT, TastyToast.WARNING);
			}
			break;

		}

	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub
		Log.e("Current Class Name!!!!!!!!!!!!!!", fragment.getClass().getName());
		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

		
		FragmentDrawer.drawer_CashinHand.setText(RegionalConversion.getRegionalConversion(AppStrings.cashinhand)

				+ SelectedGroupsTask.sCashinHand);
		FragmentDrawer.drawer_CashinHand.setTypeface(LoginActivity.sTypeface);

		 
		FragmentDrawer.drawer_CashatBank.setText(RegionalConversion.getRegionalConversion(AppStrings.cashatBank)

				+ SelectedGroupsTask.sCashatBank);
		FragmentDrawer.drawer_CashatBank.setTypeface(LoginActivity.sTypeface);
		
		FragmentDrawer.drawer_lastTR_Date.setText(RegionalConversion.getRegionalConversion(AppStrings.lastTransactionDate)
				+ " : " + SelectedGroupsTask.sLastTransactionDate_Response);
		FragmentDrawer.drawer_lastTR_Date.setTypeface(LoginActivity.sTypeface);
		
		UpdateCalendarMinMaxDateUtils.OnUpdateCalendarDate();
		
		Calendar calender = Calendar.getInstance();

		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String formattedDate = df.format(calender.getTime());
		EShaktiApplication.setLastTransDate_GroupId(SelectedGroupsTask.Group_Id);

		new GroupTempDBLastTransDate(EShaktiApplication.getLastTransDate_GroupId(),
				SelectedGroupsTask.sLastTransactionDate_Response, formattedDate);

		GroupProvider.updateGroupLastTransDateResponse();
	}

}
