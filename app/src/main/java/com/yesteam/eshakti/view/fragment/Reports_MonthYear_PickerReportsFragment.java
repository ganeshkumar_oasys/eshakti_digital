package com.yesteam.eshakti.view.fragment;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.adapter.CustomItemAdapter;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.model.RowItem;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.utils.Get_Cal_CurrentDate;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.views.MaterialSpinner;
import com.yesteam.eshakti.webservices.MonthReportTask;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.TextView;

public class Reports_MonthYear_PickerReportsFragment extends Fragment implements OnClickListener, TaskListener {

	private TextView mGroupName, mCashInHand, mCashAtBank,mMemberName;
	private Button mRaised_SubmitButton;
	private Dialog mProgressDialog;
	private String[] monthArr;
	private List<RowItem> monthItems, yearItems;
	private MaterialSpinner spn_label_month, spn_label_year;
	CustomItemAdapter monthAdapter, yearAdapter;
	public static String monthValue = "", yearValue = "";

	public Reports_MonthYear_PickerReportsFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_MEMBER_REPORTS_MONTH_YEAR_PICK;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		monthValue = "";
		yearValue = "";
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_monthyear_picker, container, false);

		MainFragment_Dashboard.isBackpressed = false;

		mGroupName = (TextView) rootView.findViewById(R.id.groupname);
		mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
		mGroupName.setTypeface(LoginActivity.sTypeface);

		mMemberName = (TextView) rootView.findViewById(R.id.memberName);
		mMemberName.setText(EShaktiApplication.getSelectedMemberName());
		mMemberName.setTypeface(LoginActivity.sTypeface);
		mMemberName.setVisibility(View.VISIBLE);
		
		mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
		mCashInHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
				+ SelectedGroupsTask.sCashinHand);
		mCashInHand.setTypeface(LoginActivity.sTypeface);

		mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
		mCashAtBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
				+ SelectedGroupsTask.sCashatBank);
		mCashAtBank.setTypeface(LoginActivity.sTypeface);

		mRaised_SubmitButton = (Button) rootView.findViewById(R.id.fragment_monthyear_picker_Submitbutton);
		mRaised_SubmitButton.setText(RegionalConversion.getRegionalConversion(AppStrings.submit));
		mRaised_SubmitButton.setTypeface(LoginActivity.sTypeface);
		mRaised_SubmitButton.setOnClickListener(this);

		monthArr = new String[] { AppStrings.month, AppStrings.January, AppStrings.February, AppStrings.March,
				AppStrings.April, AppStrings.May, AppStrings.June, AppStrings.July, AppStrings.August,
				AppStrings.September, AppStrings.October, AppStrings.November, AppStrings.December };
		try {

			spn_label_month = (MaterialSpinner) rootView.findViewById(R.id.spinner_label_month);
			spn_label_year = (MaterialSpinner) rootView.findViewById(R.id.spinner_label_year);

			spn_label_month.setBaseColor(color.grey_400);
			spn_label_year.setBaseColor(color.grey_400);

			spn_label_month.setFloatingLabelText(String.valueOf(AppStrings.month));
			spn_label_year.setFloatingLabelText(String.valueOf(AppStrings.year));
			spn_label_month.setPaddingSafe(10, 0, 10, 0);
			spn_label_year.setPaddingSafe(10, 0, 10, 0);

			monthItems = new ArrayList<RowItem>();
			for (int i = 0; i < monthArr.length; i++) {
				RowItem rowItem = new RowItem(monthArr[i]);
				monthItems.add(rowItem);
			}

			monthAdapter = new CustomItemAdapter(getActivity(), monthItems);
			spn_label_month.setAdapter(monthAdapter);
			spn_label_month.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
					// TODO Auto-generated method stub
					System.out.println("SPINNER CHECK : " + spn_label_month.getSelectedItem().toString());

					String selectedItem = monthArr[position];
					System.out.println("SELECTED MONTH :" + selectedItem);

					if (selectedItem.equals(AppStrings.month)) {
						monthValue = "0";
					} else if (selectedItem.equals(AppStrings.January)) {
						monthValue = "1";
					} else if (selectedItem.equals(AppStrings.February)) {
						monthValue = "2";
					} else if (selectedItem.equals(AppStrings.March)) {
						monthValue = "3";
					} else if (selectedItem.equals(AppStrings.April)) {
						monthValue = "4";
					} else if (selectedItem.equals(AppStrings.May)) {
						monthValue = "5";
					} else if (selectedItem.equals(AppStrings.June)) {
						monthValue = "6";
					} else if (selectedItem.equals(AppStrings.July)) {
						monthValue = "7";
					} else if (selectedItem.equals(AppStrings.August)) {
						monthValue = "8";
					} else if (selectedItem.equals(AppStrings.September)) {
						monthValue = "9";
					} else if (selectedItem.equals(AppStrings.October)) {
						monthValue = "10";
					} else if (selectedItem.equals(AppStrings.November)) {
						monthValue = "11";
					} else if (selectedItem.equals(AppStrings.December)) {
						monthValue = "12";
					}
					Log.e("Month value", monthValue);
				}

				@Override
				public void onNothingSelected(AdapterView<?> parent) {
					// TODO Auto-generated method stub

				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}

		try {

			yearItems = new ArrayList<RowItem>();
			int thisyear = Calendar.getInstance().get(Calendar.YEAR);
			/*final String yearArr[] = { AppStrings.year, Integer.toString(thisyear), Integer.toString(thisyear - 1),
					Integer.toString(thisyear - 2) };*/
			String balancesheetDateArr[] = SelectedGroupsTask.sBalanceSheetDate_Response.split("/");
			Log.e("Balancesheet date", SelectedGroupsTask.sBalanceSheetDate_Response.toString());
			String bal_year = balancesheetDateArr[2];
			Log.e("Balancesheet year ", bal_year.toString());
			
			String year = AppStrings.year + "~" + bal_year, temp_year = "";
			String result;

			temp_year = bal_year;
			StringBuilder builder = new StringBuilder();

			int i_tempYearValues = thisyear - Integer.parseInt(bal_year);

			if (i_tempYearValues > 0) {

				for (int i = 0; i <= i_tempYearValues; i++) {
					int year_value = Integer.parseInt(temp_year) + i;
					year = year_value + "~";

					builder.append(year);

				}

				result = AppStrings.year + "~" + builder.toString();

				year = result;
			} else {
				year = AppStrings.year + "~" + bal_year;
			}

			String[] tempYearArr = year.split("~");
			
			final String yearArr[] = new String[tempYearArr.length];
			
		//	yearArr[0] = AppStrings.year; 
			
			for (int i = 0; i < yearArr.length; i++) {
				yearArr[i] = tempYearArr[i].toString();
			}
						
			for (int i = 0; i < yearArr.length; i++) {
				// year = year + Integer.toString(i)+"~";
				RowItem item = new RowItem(yearArr[i]);
				yearItems.add(item);
			}

			yearAdapter = new CustomItemAdapter(getActivity(), yearItems);
			spn_label_year.setAdapter(yearAdapter);
			spn_label_year.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
					// TODO Auto-generated method stub
					Log.d("YEAR POSITION", String.valueOf(position));

					String selectedItem;
					if (position == 0) {
						selectedItem = yearArr[0];
						yearValue = "0";
					} else {
						selectedItem = yearArr[position];
						System.out.println("SELECTED YEAR : " + selectedItem);
						yearValue = selectedItem;
					}
					Log.e("Year value", yearValue);
				}

				@Override
				public void onNothingSelected(AdapterView<?> parent) {
					// TODO Auto-generated method stub

				}
			});

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return rootView;
	}

	@Override
	public void onAttach(Context context) {
		// TODO Auto-generated method stub
		super.onAttach(context);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		if ((!monthValue.equals("")) && (!yearValue.equals("")) && (!monthValue.equals("0"))
				&& (!yearValue.equals("0"))) {

			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			DateFormat format = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
			Date date1 = null;
			String mCurrentDate = Get_Cal_CurrentDate.getCurrentDate()[1];

			try {
				date1 = format.parse(mCurrentDate);
			} catch (ParseException e3) {
				// TODO Auto-generated catch block
				e3.printStackTrace();
			}

			Date date2 = null;
			int mMonthValues = Integer.parseInt(monthValue);

			try {
				date2 = sdf.parse(mMonthValues + "/" + "01" + "/" + yearValue);
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			if (date2.after(date1)) {

				TastyToast.makeText(getActivity(), AppStrings.monthYear_Warning, TastyToast.LENGTH_SHORT,
						TastyToast.WARNING);

			} else {
				if (Boolean.valueOf(ConnectionUtils.isNetworkAvailable(getActivity()))) {
					try {
						new MonthReportTask(this).execute();
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
				} else {
					
					TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
							TastyToast.ERROR);
				}
			}

		} else {
			
			TastyToast.makeText(getActivity(), AppStrings.monthYear_Alert, TastyToast.LENGTH_SHORT,
					TastyToast.WARNING);
		}

	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub
		mProgressDialog = AppDialogUtils.createProgressDialog(getActivity());
		mProgressDialog.show();

	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub
		if (mProgressDialog != null) {
			mProgressDialog.dismiss();
			if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {
				getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub

						if (!result.equals("FAIL")) {

							TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg, TastyToast.LENGTH_SHORT,
									TastyToast.ERROR);
							Constants.NETWORKCOMMONFLAG = "SUCESS";
						}

					}
				});

			} else {

				String offLineService = MonthReportTask.sMonthSummary_Response.toString();
				System.out.println("OFFLINE SERVICE :" + offLineService);
				Reports_MonthSummary_ReportFragment fragment = new Reports_MonthSummary_ReportFragment();
				setFragment(fragment);
			}
		}

	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub
		Log.e("Current Class Name!!!!!!!!!!!!!!", fragment.getClass().getName());
		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

	}

}
