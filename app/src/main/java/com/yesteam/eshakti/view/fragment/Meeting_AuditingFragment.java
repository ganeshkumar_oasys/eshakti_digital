package com.yesteam.eshakti.view.fragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.squareup.otto.Subscribe;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.TransactionOffConstants;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog.OnDateSetListener;
import com.yesteam.eshakti.sqlite.database.GroupProvider;
import com.yesteam.eshakti.sqlite.database.response.GetSingleTransResponse;
import com.yesteam.eshakti.sqlite.database.response.TransactionResponse;
import com.yesteam.eshakti.sqlite.database.response.TransactionUpdateResponse;
import com.yesteam.eshakti.sqlite.db.model.GetTransactionSingle;
import com.yesteam.eshakti.sqlite.db.model.GetTransactionSinglevalues;
import com.yesteam.eshakti.sqlite.db.model.GroupTempDBLastTransDate;
import com.yesteam.eshakti.sqlite.db.model.Transaction;
import com.yesteam.eshakti.sqlite.db.model.TransactionUpdate;
import com.yesteam.eshakti.sqlite.db.transactions.TransactionManager.DataType;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.CalculateDateUtils;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.utils.Get_Offline_MobileDate;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.utils.Reset;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.utils.UpdateCalendarMinMaxDateUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.Get_AuditTask;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Activity;
import android.app.Dialog;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ParseException;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Meeting_AuditingFragment extends Fragment implements OnClickListener, TaskListener, OnDateSetListener {

	public static final String TAG = Meeting_AuditingFragment.class.getSimpleName();
	private TextView mGroupName, mCashInHand, mCashAtBank, mHeadertext;
	private Button mRaised_Submit_Button, mEdit_RaisedButton, mOk_RaisedButton;
	private Dialog mProgressDialog;

	public static String audit_check = "";

	private TextView mAuditFromDate_Text, mAuditToDate_Text, mAuditDate_Text, mAuditorName_Text;
	public static TextView mAuditFromDate_editText, mAuditToDate_editText, mAuditDate_editText;
	private EditText mAuditorName_editText;

	public static String Send_to_server_values = "";
	public static String auditFromDate = "", auditToDate = "", auditingDate = "", auditorName = "", calToDate = "",
			calFromDate = "";

	public static String[] sAuditTextItems;
	public static String[] sAuditEditValues = { "" };
	public static int sAuditNullEditTextCheck;
	int size;

	String mLastTrDate = null, mLastTr_ID = null;
	Dialog confirmationDialog;
	private String mLanguageLocale = "";
	Locale locale;
	boolean isServiceCall = false;

	public Meeting_AuditingFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_AUDITING;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Send_to_server_values = Reset.reset(Send_to_server_values);

		EShaktiApplication.setAuditFromDate(null);
		EShaktiApplication.setAuditToDate(null);

		sAuditTextItems = new String[] { RegionalConversion.getRegionalConversion(AppStrings.FromDate),
				RegionalConversion.getRegionalConversion(AppStrings.ToDate),
				RegionalConversion.getRegionalConversion(AppStrings.auditing_Date),
				RegionalConversion.getRegionalConversion(AppStrings.auditor_Name) };
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		SBus.INST.register(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_audit, container, false);

		MainFragment_Dashboard.isBackpressed = false;
		Constants.FRAG_BACKPRESS_CONSTANT = Constants.FRAG_INSTANCE_AUDITING;

		try {
			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashInHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashInHand.setTypeface(LoginActivity.sTypeface);

			mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashAtBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashAtBank.setTypeface(LoginActivity.sTypeface);

			mHeadertext = (TextView) rootView.findViewById(R.id.fragment_auditing_headertext);
			mHeadertext.setText(RegionalConversion.getRegionalConversion(AppStrings.auditing));
			mHeadertext.setTypeface(LoginActivity.sTypeface);

			mRaised_Submit_Button = (Button) rootView.findViewById(R.id.fragment_auditing_Submitbutton);
			mRaised_Submit_Button.setText(RegionalConversion.getRegionalConversion(AppStrings.submit));
			mRaised_Submit_Button.setTypeface(LoginActivity.sTypeface);
			mRaised_Submit_Button.setOnClickListener(this);

			mAuditFromDate_Text = (TextView) rootView.findViewById(R.id.auditFromDateText);
			mAuditFromDate_Text.setText(GetSpanText.getSpanString(getActivity(), AppStrings.FromDate));
			mAuditFromDate_Text.setTypeface(LoginActivity.sTypeface);
			mAuditFromDate_Text.setTextColor(color.black);

			mAuditToDate_Text = (TextView) rootView.findViewById(R.id.auditToDateText);
			mAuditToDate_Text.setText(GetSpanText.getSpanString(getActivity(), AppStrings.ToDate));
			mAuditToDate_Text.setTypeface(LoginActivity.sTypeface);
			mAuditToDate_Text.setTextColor(color.black);

			mAuditDate_Text = (TextView) rootView.findViewById(R.id.auditDateText);
			mAuditDate_Text.setText(GetSpanText.getSpanString(getActivity(), AppStrings.auditing_Date));
			mAuditDate_Text.setTypeface(LoginActivity.sTypeface);
			mAuditDate_Text.setTextColor(color.black);

			mAuditorName_Text = (TextView) rootView.findViewById(R.id.auditorText);
			mAuditorName_Text.setText(GetSpanText.getSpanString(getActivity(), AppStrings.auditor_Name));
			mAuditorName_Text.setTypeface(LoginActivity.sTypeface);
			mAuditorName_Text.setTextColor(color.black);

			mAuditFromDate_editText = (TextView) rootView.findViewById(R.id.auditFromeditText);
			mAuditFromDate_editText.setText(Meeting_AuditingFragment.auditFromDate);
			mAuditFromDate_editText.setOnClickListener(this);

			mAuditToDate_editText = (TextView) rootView.findViewById(R.id.auditToeditText);
			mAuditToDate_editText.setText(Meeting_AuditingFragment.auditToDate);
			mAuditToDate_editText.setOnClickListener(this);

			mAuditDate_editText = (TextView) rootView.findViewById(R.id.auditeditText);
			mAuditDate_editText.setText(Meeting_AuditingFragment.auditingDate);
			mAuditDate_editText.setOnClickListener(this);

			mAuditorName_editText = (EditText) rootView.findViewById(R.id.auditoreditText);
			mAuditorName_editText.setText(Meeting_AuditingFragment.auditorName);
			mAuditorName_editText.setBackgroundResource(R.drawable.edittext_background);
			mAuditorName_editText.setOnClickListener(this);

			size = sAuditTextItems.length;
			System.out.println("Size:" + size);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return rootView;
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		// if (ConnectionUtils.isNetworkAvailable(getActivity())) {
		try {
			Send_to_server_values = Reset.reset(Send_to_server_values);
			sAuditEditValues = new String[size];

			if (v.getId() == R.id.auditFromeditText) {

				// Do call calendar activity

				audit_check = "1";
				EShaktiApplication.setAudit_TrainingFragment(true);
				EShaktiApplication.setAuditFragment(true);

				onSetCalendarValues();
				try {
					Calendar calender = Calendar.getInstance();

					SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
					String formattedDate = df.format(calender.getTime());
					Log.e("Device Date  =  ", formattedDate + "");

					String mBalanceSheetDate = SelectedGroupsTask.sLastTransactionDate_Response; // dd/MM/yyyy
					String formatted_balancesheetDate = mBalanceSheetDate.replace("/", "-");
					Log.e("Balancesheet Date = ", formatted_balancesheetDate + "");

					Date balanceDate = df.parse(formatted_balancesheetDate);
					Date systemDate = df.parse(formattedDate);

					if (balanceDate.compareTo(systemDate) < 0 || balanceDate.compareTo(systemDate) == 0) {

						calendarDialogShow();
					} else {
						TastyToast.makeText(getActivity(), "PLEASE SET YOUR DEVICE DATE CORRECTLY",
								TastyToast.LENGTH_SHORT, TastyToast.ERROR);
						EShaktiApplication.setNextMonthLastDate(null);
						EShaktiApplication.setCalendarDateVisibleFlag(true);
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}

			} else if (v.getId() == R.id.auditToeditText) {
				// Do call calendar activity
				audit_check = "2";
				auditFromDate = mAuditFromDate_editText.getText().toString();
				EShaktiApplication.setAuditFragment(true);
				if (auditFromDate.equals("")) {
					auditFromDate = null;
				}
				if (auditFromDate == null) {

					TastyToast.makeText(getActivity(), AppStrings.previous_DateAlert, TastyToast.LENGTH_SHORT,
							TastyToast.WARNING);

				} else if (auditFromDate != null) {
					EShaktiApplication.setAudit_TrainingFragment(true);

					onSetCalendarValues();
					try {
						Calendar calender = Calendar.getInstance();

						SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
						String formattedDate = df.format(calender.getTime());
						Log.e("Device Date  =  ", formattedDate + "");

						String mBalanceSheetDate = SelectedGroupsTask.sLastTransactionDate_Response; // dd/MM/yyyy
						String formatted_balancesheetDate = mBalanceSheetDate.replace("/", "-");
						Log.e("Balancesheet Date = ", formatted_balancesheetDate + "");

						Date balanceDate = df.parse(formatted_balancesheetDate);
						Date systemDate = df.parse(formattedDate);

						if (balanceDate.compareTo(systemDate) < 0 || balanceDate.compareTo(systemDate) == 0) {

							calendarDialogShow();
						} else {
							TastyToast.makeText(getActivity(), "PLEASE SET YOUR DEVICE DATE CORRECTLY",
									TastyToast.LENGTH_SHORT, TastyToast.ERROR);
							EShaktiApplication.setNextMonthLastDate(null);
						}
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
				}

			} else if (v.getId() == R.id.auditeditText) {
				audit_check = "3";
				EShaktiApplication.setAuditFragment(true);
				if (auditToDate.equals("")) {
					auditToDate = null;
				}
				if (auditToDate == null) {
					TastyToast.makeText(getActivity(), AppStrings.previous_DateAlert, TastyToast.LENGTH_SHORT,
							TastyToast.WARNING);
				} else if (auditFromDate != null) {
					EShaktiApplication.setAudit_TrainingFragment(true);

					onSetCalendarValues();
					try {
						Calendar calender = Calendar.getInstance();

						SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
						String formattedDate = df.format(calender.getTime());
						Log.e("Device Date  =  ", formattedDate + "");

						String mBalanceSheetDate = SelectedGroupsTask.sLastTransactionDate_Response; // dd/MM/yyyy
						String formatted_balancesheetDate = mBalanceSheetDate.replace("/", "-");
						Log.e("Balancesheet Date = ", formatted_balancesheetDate + "");

						Date balanceDate = df.parse(formatted_balancesheetDate);
						Date systemDate = df.parse(formattedDate);

						if (balanceDate.compareTo(systemDate) < 0 || balanceDate.compareTo(systemDate) == 0) {

							calendarDialogShow();
						} else {
							TastyToast.makeText(getActivity(), "PLEASE SET YOUR DEVICE DATE CORRECTLY",
									TastyToast.LENGTH_SHORT, TastyToast.ERROR);
							EShaktiApplication.setNextMonthLastDate(null);
						}
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
				}
			} /*
				 * else if (v.getId() == R.id.auditoreditText) { AuditingFragment.auditorName =
				 * ""; }
				 */
			else if (v.getId() == R.id.fragment_auditing_Submitbutton) {

				String[] fromDateArr = mAuditFromDate_editText.getText().toString().split("/");
				String[] toDateArr = mAuditToDate_editText.getText().toString().split("/");
				String[] auditDateArr = mAuditDate_editText.getText().toString().split("/");

				auditFromDate = fromDateArr[1] + "/" + fromDateArr[0] + "/" + fromDateArr[2];
				auditToDate = toDateArr[1] + "/" + toDateArr[0] + "/" + toDateArr[2];
				auditingDate = auditDateArr[1] + "/" + auditDateArr[0] + "/" + auditDateArr[2];
				auditorName = mAuditorName_editText.getText().toString();

				if (auditFromDate.equals("")) {
					auditFromDate = null;
				}
				if (auditToDate.equals("")) {
					auditToDate = null;
				}
				if (auditingDate.equals("")) {
					auditingDate = null;
				}
				if (auditorName.equals("") || auditorName.startsWith(" ")) {
					auditorName = null;
				}

				if ((auditFromDate != null) && (auditToDate != null) && (auditingDate != null) && (auditorName != null)
						&& (!auditorName.startsWith(" "))) {

					System.out.println("FROM DATE : " + auditFromDate + "\n" + "TO DATE : " + auditToDate + "\n"
							+ "AUDITING DATE : " + auditingDate + "\n" + "AUDITOR NAME : " + auditorName);

					String toBeEditArr = auditFromDate + "~" + auditToDate + "~" + auditingDate + "~" + auditorName
							+ "~";

					System.out.println("Check sp value:>>>" + toBeEditArr);

					for (int i = 0; i < size; i++) {
						sAuditEditValues = toBeEditArr.split("~");

						Send_to_server_values = Send_to_server_values + sAuditEditValues[i] + "~";
					}

					Log.d(TAG, "Send to server value:" + Send_to_server_values);

					confirmationDialog = new Dialog(getActivity());

					LayoutInflater inflater = getActivity().getLayoutInflater();
					View dialogView = inflater.inflate(R.layout.dialog_confirmation, null);
					dialogView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
							LayoutParams.WRAP_CONTENT));

					TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
					confirmationHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.confirmation));
					confirmationHeader.setTypeface(LoginActivity.sTypeface);

					TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

					for (int i = 0; i < size; i++) {

						TableRow indv_DepositEntryRow = new TableRow(getActivity());

						TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
								LayoutParams.WRAP_CONTENT, 1f);
						contentParams.setMargins(10, 5, 10, 5);

						TextView memberName_Text = new TextView(getActivity());
						memberName_Text
								.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sAuditTextItems[i])));
						memberName_Text.setTypeface(LoginActivity.sTypeface);
						memberName_Text.setTextColor(color.black);
						memberName_Text.setPadding(5, 5, 5, 5);
						memberName_Text.setLayoutParams(contentParams);
						indv_DepositEntryRow.addView(memberName_Text);

						TextView confirm_values = new TextView(getActivity());
						confirm_values
								.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sAuditEditValues[i])));
						confirm_values.setTextColor(color.black);
						confirm_values.setPadding(5, 5, 5, 5);
						confirm_values.setGravity(Gravity.RIGHT);
						confirm_values.setLayoutParams(contentParams);
						indv_DepositEntryRow.addView(confirm_values);

						confirmationTable.addView(indv_DepositEntryRow,
								new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
					}

					mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit_button);
					mEdit_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.edit));
					mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
					mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
																			// 205,
																			// 0));
					mEdit_RaisedButton.setOnClickListener(this);

					mOk_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Ok_button);
					mOk_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.yes));
					mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
					mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
					mOk_RaisedButton.setOnClickListener(this);

					confirmationDialog.getWindow()
							.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
					confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					confirmationDialog.setCanceledOnTouchOutside(false);
					confirmationDialog.setContentView(dialogView);
					confirmationDialog.setCancelable(true);
					confirmationDialog.show();

					MarginLayoutParams margin = (MarginLayoutParams) dialogView.getLayoutParams();
					margin.leftMargin = 10;
					margin.rightMargin = 10;
					margin.topMargin = 10;
					margin.bottomMargin = 10;
					margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);

				} else {
					if ((auditFromDate == null && auditToDate == null && auditingDate == null && auditorName == null)) {
						TastyToast.makeText(getActivity(), AppStrings.nullDetailsAlert, TastyToast.LENGTH_SHORT,
								TastyToast.WARNING);

						Send_to_server_values = Reset.reset(Send_to_server_values);
					}

					else if (auditFromDate == null) {
						TastyToast.makeText(getActivity(), AppStrings.fromDateAlert, TastyToast.LENGTH_SHORT,
								TastyToast.WARNING);

						Send_to_server_values = Reset.reset(Send_to_server_values);

					} else if (auditToDate == null) {
						TastyToast.makeText(getActivity(), AppStrings.toDateAlert, TastyToast.LENGTH_SHORT,
								TastyToast.WARNING);

						Send_to_server_values = Reset.reset(Send_to_server_values);

					} else if (auditingDate == null) {
						TastyToast.makeText(getActivity(), AppStrings.auditingDateAlert, TastyToast.LENGTH_SHORT,
								TastyToast.WARNING);

						Send_to_server_values = Reset.reset(Send_to_server_values);

					} else if (auditorName == null || auditorName.startsWith(" ")) {
						TastyToast.makeText(getActivity(), AppStrings.auditorAlert, TastyToast.LENGTH_SHORT,
								TastyToast.WARNING);

						Send_to_server_values = Reset.reset(Send_to_server_values);
						auditorName = "";
						mAuditorName_editText.setText(auditorName);
					}

				}
			} else if (v.getId() == R.id.fragment_Edit_button) {
				Send_to_server_values = Reset.reset(Send_to_server_values);
				mRaised_Submit_Button.setClickable(true);
				confirmationDialog.dismiss();
				isServiceCall = false;
			} else if (v.getId() == R.id.fragment_Ok_button) {
				// Call Get_AuditTask here

				if (ConnectionUtils.isNetworkAvailable(getActivity())) {
					try {
						if (EShaktiApplication.getLoginFlag().equals(Constants.ONLINEFLAG)) {
							if (!isServiceCall) {
								isServiceCall = true;
								new Get_AuditTask(Meeting_AuditingFragment.this).execute();
							}
						} else {
							TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
									TastyToast.ERROR);
						}

					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();

					}
				} else {

					// Do offline
					if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
						EShaktiApplication.getInstance().getTransactionManager().startTransaction(
								DataType.GET_TRANS_SINGLE, new GetTransactionSingle(PrefUtils.getOfflineUniqueID()));
					} else {
						TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
								TastyToast.ERROR);
					}

				}
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		/*
		 * } else { // Internet not present // Create Dialog }
		 */

	}

	private void calendarDialogShow() {
		// TODO Auto-generated method stub
		mLanguageLocale = PrefUtils.getUserlangcode();
		if (mLanguageLocale.equalsIgnoreCase("English")) {
			locale = new Locale("en");
		} else if (mLanguageLocale.equalsIgnoreCase("Tamil")) {
			locale = new Locale("ta");
		} else if (mLanguageLocale.equalsIgnoreCase("Hindi")) {
			locale = new Locale("hi");
		} else if (mLanguageLocale.equalsIgnoreCase("Marathi")) {
			locale = new Locale("ma");
		} else if (mLanguageLocale.equalsIgnoreCase("Malayalam")) {
			locale = new Locale("ml");
		} else if (mLanguageLocale.equalsIgnoreCase("Kannada")) {
			locale = new Locale("kn");
		} else if (mLanguageLocale.equalsIgnoreCase("Bengali")) {
			locale = new Locale("bn");
		} else if (mLanguageLocale.equalsIgnoreCase("Gujarati")) {
			locale = new Locale("gu");
		} else if (mLanguageLocale.equalsIgnoreCase("Punjabi")) {
			locale = new Locale("pa");
		} else if (mLanguageLocale.equalsIgnoreCase("Assamese")) {
			locale = new Locale("as");
		} else {
			locale = new Locale("en");
		}
		locale = new Locale("en");
		Locale.setDefault(locale);
		Configuration config = new Configuration();
		config.locale = locale;
		getActivity().getResources().updateConfiguration(config, getActivity().getResources().getDisplayMetrics());

		OnDateSetListener datelistener = Meeting_AuditingFragment.this;
		Calendar now = Calendar.getInstance();
		FragmentManager fm = getFragmentManager();
		DatePickerDialog dialog = DatePickerDialog.newInstance(datelistener, now.get(Calendar.YEAR),
				now.get(Calendar.MONDAY), now.get(Calendar.DAY_OF_MONTH));

		if (audit_check.equals("1")) {

			String dateArr[] = null;

			if (EShaktiApplication.getLastAuditDate() != null) {
				if (!EShaktiApplication.getLastAuditDate().equals("No")) {
					String lastAudit_date = String.valueOf(EShaktiApplication.getLastAuditDate());
					dateArr = lastAudit_date.split("/");
					EShaktiApplication.setCalendarDialog_MinDate(EShaktiApplication.getLastAuditDate());
				} else {
					String lastAudit_date = String.valueOf(SelectedGroupsTask.sBalanceSheetDate_Response);
					dateArr = lastAudit_date.split("/");
					EShaktiApplication.setCalendarDialog_MinDate(SelectedGroupsTask.sBalanceSheetDate_Response);
				}

				Calendar min_Cal = Calendar.getInstance();

				int day = Integer.parseInt(dateArr[0]);
				int month = Integer.parseInt(dateArr[1]);
				int year = Integer.parseInt(dateArr[2]);

				min_Cal.set(year, (month - 1), day);
				dialog.setMinDate(min_Cal);

				String tr_date = String.valueOf(DatePickerDialog.sDashboardDate);
				String trDateArr[] = tr_date.split("-");

				Calendar max_Cal = Calendar.getInstance();

				int day_ = Integer.parseInt(trDateArr[0]);
				int month_ = Integer.parseInt(trDateArr[1]);
				int year_ = Integer.parseInt(trDateArr[2]);

				max_Cal.set(year_, (month_ - 1), day_);
				dialog.setMaxDate(max_Cal);
				dialog.show(fm, "");

			}

		} else if (audit_check.equals("2")) {
			if (EShaktiApplication.getAuditFromDate() != null) {

				String sanc_date = String.valueOf(EShaktiApplication.getAuditFromDate());
				String dateArr[] = sanc_date.split("/");

				Calendar min_Cal = Calendar.getInstance();

				int day = Integer.parseInt(dateArr[0]);
				int month = Integer.parseInt(dateArr[1]);
				int year = Integer.parseInt(dateArr[2]);

				min_Cal.set(year, (month - 1), day);
				dialog.setMinDate(min_Cal);
				EShaktiApplication.setCalendarDialog_MinDate(EShaktiApplication.getAuditFromDate());
				dialog.setMaxDate(now);
				dialog.show(fm, "");

			}

		} else if (audit_check.equals("3")) {
			dialog.setMaxDate(now);
			EShaktiApplication.setCalendarDialog_MinDate(null);
			dialog.show(fm, "");

			// }
		}

	}

	@Subscribe
	public void OnGetSingleTransactionAuditing(final GetSingleTransResponse getSingleTransResponse)
			throws ParseException {
		switch (getSingleTransResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), getSingleTransResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), getSingleTransResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			String mUniqueId = GetTransactionSinglevalues.getUniqueId();
			String mAuditingValues_Offline = GetTransactionSinglevalues.getAuditting();

			String mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
			String mMobileDate = Get_Offline_MobileDate.getOffline_MobileDate(getActivity());
			String mTransaction_UniqueId = PrefUtils.getOfflineUniqueID();
			String Send_to_server_values_Offline = auditFromDate + "~" + auditToDate + "~" + auditingDate + "~"
					+ auditorName + "~";

			String mAuditingvalues = Send_to_server_values_Offline + "#" + mTrasactiondate + "#" + mMobileDate;

			mLastTrDate = DatePickerDialog.sDashboardDate;
			mLastTr_ID = SelectedGroupsTask.sTr_ID.toString();

			String mCurrentTransDate = null;

			if (publicValues.mOffline_Trans_CurrentDate != null) {
				mCurrentTransDate = publicValues.mOffline_Trans_CurrentDate;
			}

			if (mUniqueId == null && mAuditingValues_Offline == null) {

				if (mLastTrDate != null && mMobileDate != null && mTransaction_UniqueId != null
						&& mCurrentTransDate != null) {
					EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.TRANSACTIONADD,
							new Transaction(0, PrefUtils.getUsernameKey(), SelectedGroupsTask.UserName,
									SelectedGroupsTask.Ngo_Id, SelectedGroupsTask.Group_Id, mTransaction_UniqueId,
									mLastTr_ID, mCurrentTransDate, null, null, null, null, null, null, null, null, null,
									null, null, null, null, null, null, null, mAuditingvalues, null, null, null, null,
									null));
				} else {

					TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT, TastyToast.ERROR);
				}

			} else if (mUniqueId.equalsIgnoreCase(PrefUtils.getOfflineUniqueID()) && mAuditingValues_Offline == null) {
				EShaktiApplication.setSetTransValues(TransactionOffConstants.TRANS_AUD);

				EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.TRANS_VALUES_UPDATE,
						new TransactionUpdate(mUniqueId, mAuditingvalues));

			} else if ((mAuditingValues_Offline != null)) {
				confirmationDialog.dismiss();
				isServiceCall = false;

				TastyToast.makeText(getActivity(), AppStrings.offlineDataAvailable, TastyToast.LENGTH_SHORT,
						TastyToast.WARNING);

				DatePickerDialog.sDashboardDate = SelectedGroupsTask.sLastTransactionDate_Response;
				MainFragment_Dashboard fragment = new MainFragment_Dashboard();
				setFragment(fragment);
			}
			break;
		}
	}

	@Subscribe
	public void OnTransUpdateAuditing(final TransactionUpdateResponse transactionUpdateResponse) {
		switch (transactionUpdateResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), transactionUpdateResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), transactionUpdateResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			confirmationDialog.dismiss();
			isServiceCall = false;

			try {

				TastyToast.makeText(getActivity(), AppStrings.transactionCompleted, TastyToast.LENGTH_SHORT,
						TastyToast.SUCCESS);

				/*
				 * String[] lastTrDate = DatePickerDialog.sSend_To_Server_Date.split("/");
				 * String mTrasactiondate = lastTrDate[1] + "/" + lastTrDate[0] + "/" +
				 * lastTrDate[2]; SelectedGroupsTask.sLastTransactionDate_Response =
				 * mTrasactiondate;
				 * FragmentDrawer.drawer_lastTR_Date.setText(RegionalConversion.
				 * getRegionalConversion(AppStrings.lastTransactionDate) + " : " +
				 * SelectedGroupsTask.sLastTransactionDate_Response);
				 * FragmentDrawer.drawer_lastTR_Date.setTypeface(LoginActivity.sTypeface);
				 */

				MainFragment_Dashboard fragment = new MainFragment_Dashboard();
				setFragment(fragment);

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		}
	}

	@Subscribe
	public void onAddTransactionAuditing(final TransactionResponse transactionResponse) {
		switch (transactionResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), transactionResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), transactionResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:
			confirmationDialog.dismiss();
			isServiceCall = false;
			try {

				TastyToast.makeText(getActivity(), AppStrings.transactionCompleted, TastyToast.LENGTH_SHORT,
						TastyToast.SUCCESS);

				/*
				 * String[] lastTrDate = DatePickerDialog.sSend_To_Server_Date.split("/");
				 * String mTrasactiondate = lastTrDate[1] + "/" + lastTrDate[0] + "/" +
				 * lastTrDate[2]; SelectedGroupsTask.sLastTransactionDate_Response =
				 * mTrasactiondate;
				 * FragmentDrawer.drawer_lastTR_Date.setText(RegionalConversion.
				 * getRegionalConversion(AppStrings.lastTransactionDate) + " : " +
				 * SelectedGroupsTask.sLastTransactionDate_Response);
				 * FragmentDrawer.drawer_lastTR_Date.setTypeface(LoginActivity.sTypeface);
				 */

				MainFragment_Dashboard fragment = new MainFragment_Dashboard();
				setFragment(fragment);

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		}
	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub
		mProgressDialog = AppDialogUtils.createProgressDialog(getActivity());
		mProgressDialog.show();
	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub

		try {

			if (mProgressDialog != null) {
				mProgressDialog.dismiss();

				if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {
					getActivity().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub

							if (!result.equals("FAIL")) {
								isServiceCall = false;

								TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg,
										TastyToast.LENGTH_SHORT, TastyToast.ERROR);
								Constants.NETWORKCOMMONFLAG = "SUCESS";
							}

						}
					});

				} else {

					if (Get_AuditTask.get_Audit_Response.equals("Transaction Successful")) {

						TastyToast.makeText(getActivity(), AppStrings.transactionCompleted, TastyToast.LENGTH_SHORT,
								TastyToast.SUCCESS);
					} else {

						TastyToast.makeText(getActivity(), AppStrings.transactionFailAlert, TastyToast.LENGTH_SHORT,
								TastyToast.ERROR);
					}
					confirmationDialog.dismiss();
					isServiceCall = false;
					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);
				}
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub

		EShaktiApplication.setAudit_TrainingFragment(false);
		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

		FragmentDrawer.drawer_lastTR_Date
				.setText(RegionalConversion.getRegionalConversion(AppStrings.lastTransactionDate) + " : "
						+ SelectedGroupsTask.sLastTransactionDate_Response);
		FragmentDrawer.drawer_lastTR_Date.setTypeface(LoginActivity.sTypeface);

		UpdateCalendarMinMaxDateUtils.OnUpdateCalendarDate();

		Calendar calender = Calendar.getInstance();

		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String formattedDate = df.format(calender.getTime());
		EShaktiApplication.setLastTransDate_GroupId(SelectedGroupsTask.Group_Id);

		String lastTrDateArr[] = DatePickerDialog.sDashboardDate.split("-");
		String lastTrDate = lastTrDateArr[0] + "/" + lastTrDateArr[1] + "/" + lastTrDateArr[2];

		new GroupTempDBLastTransDate(EShaktiApplication.getLastTransDate_GroupId(), lastTrDate, formattedDate);

		GroupProvider.updateGroupLastTransDateResponse();

	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		SBus.INST.unRegister(this);
		EShaktiApplication.setAudit_TrainingFragment(false);
	}

	@Override
	public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
		// TODO Auto-generated method stub

	}

	private void onSetCalendarValues() {
		// TODO Auto-generated method stub

		if (EShaktiApplication.getNextMonthLastDate() == null) {
			try {

				if (EShaktiApplication.isCalendarDateVisibleFlag()) {
					EShaktiApplication.setCalendarDateVisibleFlag(false);
				}
				Calendar calender = Calendar.getInstance();

				SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String formattedDate = df.format(calender.getTime());
				Log.e("Device Date  =  ", formattedDate + "");

				String mBalanceSheetDate = SelectedGroupsTask.sLastTransactionDate_Response; // dd/MM/yyyy
				String formatted_balancesheetDate = mBalanceSheetDate.replace("/", "-");
				Log.e("Balancesheet Date = ", formatted_balancesheetDate + "");

				Date balanceDate = df.parse(formatted_balancesheetDate);
				Date systemDate = df.parse(formattedDate);

				if (balanceDate.compareTo(systemDate) < 0 || balanceDate.compareTo(systemDate) == 0) {
					System.err.println(" balancesheet date is less than sys date");

					String mLastTransactionDate = SelectedGroupsTask.sLastTransactionDate_Response;

					String formattedDate_LastTransaction = mLastTransactionDate.replace("/", "-");

					String days = CalculateDateUtils.get_count_of_days(formattedDate_LastTransaction, formattedDate);

					int mDaycount = Integer.parseInt(days);
					Log.e("Total Days Count =====_____----", mDaycount + "");

					EShaktiApplication.setLastTransDate_GroupId(SelectedGroupsTask.Group_Id);

					if (mDaycount > 30) {

						SimpleDateFormat df_after = new SimpleDateFormat("dd-MM-yyyy");

						Calendar calendar = Calendar.getInstance();
						calendar.setTime(df_after.parse(formattedDate_LastTransaction));
						calendar.add(Calendar.MONTH, 1);
						calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
						Date nextMonthFirstDay = calendar.getTime();
						System.out.println(
								"------ Next month first date   =  " + df_after.format(nextMonthFirstDay) + "");

						Calendar cal_last_date = Calendar.getInstance();
						cal_last_date.setTime(df_after.parse(formattedDate_LastTransaction));
						cal_last_date.add(Calendar.MONTH, 1);
						cal_last_date.set(Calendar.DATE, cal_last_date.getActualMaximum(Calendar.DAY_OF_MONTH));
						Date nextMonthLastDay = cal_last_date.getTime();
						System.out
								.println("------ Next month Last date   =  " + df_after.format(nextMonthLastDay) + "");

						//
						String lastDateOfNextMonth = df_after.format(nextMonthLastDay);
						Calendar currentDate = Calendar.getInstance();
						int current_Day = currentDate.get(Calendar.DAY_OF_MONTH);
						int current_month = currentDate.get(Calendar.MONTH) + 1;
						int current_year = currentDate.get(Calendar.YEAR);
						String current_date = current_Day + "-" + current_month + "-" + current_year;

						Date lastdate = null, currentdate = null;
						String min_date_str = null;

						try {

							lastdate = df_after.parse(lastDateOfNextMonth);
							currentdate = df_after.parse(current_date);

						} catch (ParseException e) {
							// TODO: handle exception
							e.printStackTrace();
						}

						if (lastdate.compareTo(currentdate) <= 0) {
							min_date_str = lastDateOfNextMonth;
						} else {
							min_date_str = current_date;
						}

						Log.e("Minimum Date !!!!!	", min_date_str + "");

						EShaktiApplication.setNextMonthLastDate(min_date_str);

					} else {
						Calendar currentDate = Calendar.getInstance();
						SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
						EShaktiApplication.setNextMonthLastDate(sdf.format(currentDate.getTime()));

					}
				}

			} catch (Exception e) {
				// TODO: handle exception
			}
		}

	}

}
