package com.yesteam.eshakti.view.fragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.squareup.otto.Subscribe;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.TransactionOffConstants;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;
import com.yesteam.eshakti.sqlite.database.GroupDetailsProvider;
import com.yesteam.eshakti.sqlite.database.GroupProvider;
import com.yesteam.eshakti.sqlite.database.response.GetSingleTransResponse;
import com.yesteam.eshakti.sqlite.database.response.TransactionResponse;
import com.yesteam.eshakti.sqlite.database.response.TransactionUpdateResponse;
import com.yesteam.eshakti.sqlite.db.model.GetTransactionSingle;
import com.yesteam.eshakti.sqlite.db.model.GetTransactionSinglevalues;
import com.yesteam.eshakti.sqlite.db.model.GroupDetailsUpdate;
import com.yesteam.eshakti.sqlite.db.model.GroupResponseUpdate;
import com.yesteam.eshakti.sqlite.db.model.GroupTempDBLastTransDate;
import com.yesteam.eshakti.sqlite.db.model.Transaction;
import com.yesteam.eshakti.sqlite.db.model.TransactionUpdate;
import com.yesteam.eshakti.sqlite.db.transactions.TransactionManager.DataType;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.utils.GetOfflineTransactionValues;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.utils.Get_Offline_MobileDate;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.utils.Put_DB_GroupNameDetail_Response;
import com.yesteam.eshakti.utils.Put_DB_GroupResponse;
import com.yesteam.eshakti.utils.Reset;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.utils.UpdateCalendarMinMaxDateUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.Get_AttendanceTask;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Meeting_AttendanceFragment extends Fragment implements OnClickListener, TaskListener {

	public static final String TAG = Meeting_AttendanceFragment.class.getSimpleName();
	private TextView mGroupName, mCashInHand, mCashAtBank, mHeadertext;
	private Button mRaised_Submit_Button;
	private Button mEdit_RaisedButton, mOk_RaisedButton;
	private Dialog mprogressDialog;
	private LinearLayout mLayout;

	int size;
	public static CheckBox sCheckBox[];
	public static List<CheckBox> sAllCbs = new ArrayList<CheckBox>();
	public static String memberName;
	public static String Send_to_server_values = null;

	String mLastTrDate = null, mLastTr_ID = null;
	Dialog confirmationDialog;
	private Button mPreviousButton, mNextButton;
	boolean isServiceCall = false;

	public Meeting_AttendanceFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_ATTENDANCE;

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		memberName = "";
		Send_to_server_values = Reset.reset(Send_to_server_values);
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();

		SBus.INST.register(this);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

	}

	@Override
	public void onAttach(Context context) {
		// TODO Auto-generated method stub
		super.onAttach(context);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View rootView = inflater.inflate(R.layout.fragment_attendance_minutes, container, false);

		MainFragment_Dashboard.isBackpressed = false;
		Constants.FRAG_BACKPRESS_CONSTANT = Constants.FRAG_INSTANCE_ATTENDANCE;

		try {

			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashInHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashInHand.setTypeface(LoginActivity.sTypeface);

			mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashAtBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashAtBank.setTypeface(LoginActivity.sTypeface);

			mHeadertext = (TextView) rootView.findViewById(R.id.fragment_attendance_minutes_headertext);
			mHeadertext.setText(RegionalConversion.getRegionalConversion(AppStrings.Attendance));
			mHeadertext.setTypeface(LoginActivity.sTypeface);

			mRaised_Submit_Button = (Button) rootView.findViewById(R.id.fragment_attendance_minutes_Submitbutton);
			mRaised_Submit_Button.setText(RegionalConversion.getRegionalConversion(AppStrings.submit));
			mRaised_Submit_Button.setTypeface(LoginActivity.sTypeface);
			mRaised_Submit_Button.setOnClickListener(this);

			mPreviousButton = (Button) rootView.findViewById(R.id.fragment_attendance_minutes_Previousbutton);
			mPreviousButton.setText(RegionalConversion.getRegionalConversion(AppStrings.mPervious));
			mPreviousButton.setTypeface(LoginActivity.sTypeface);
			mPreviousButton.setOnClickListener(this);

			mNextButton = (Button) rootView.findViewById(R.id.fragment_attendance_minutes_Nextbutton);
			mNextButton.setText(RegionalConversion.getRegionalConversion(AppStrings.mNext));
			mNextButton.setTypeface(LoginActivity.sTypeface);
			mNextButton.setOnClickListener(this);

			if (EShaktiApplication.isDefault()) {

				PrefUtils.setStepWise_GroupId(SelectedGroupsTask.Group_Id);

				mNextButton.setVisibility(View.INVISIBLE);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		size = SelectedGroupsTask.member_Name.size();
		System.out.println("Member Name Size:" + size);

		try {
			mLayout = (LinearLayout) rootView.findViewById(R.id.linearLayout);
			sCheckBox = new CheckBox[size];

			for (int i = 0; i < size; i++) {

				LinearLayout linearLayout = new LinearLayout(getActivity());
				linearLayout.setOrientation(LinearLayout.HORIZONTAL);
				sCheckBox[i] = new CheckBox(getActivity());
				sCheckBox[i].setChecked(true);

				sCheckBox[i].setBackgroundResource(R.drawable.btn_check_to_off_mtrl_013);

				linearLayout.addView(sCheckBox[i]);

				TextView memberName = new TextView(getActivity());
				memberName.setText(GetSpanText.getSpanString(getActivity(),
						String.valueOf(SelectedGroupsTask.member_Name.elementAt(i))));
				memberName.setTypeface(LoginActivity.sTypeface);
				memberName.setPadding(20, 10, 0, 10);
				linearLayout.addView(memberName);

				mLayout.addView(linearLayout);
			}

		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("checkbox layout error:" + e.toString());
		}

		return rootView;
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.fragment_attendance_minutes_Submitbutton:
			try {

				String Yes_No = "0";
				memberName = "";
				Send_to_server_values = Reset.reset(Send_to_server_values);

				for (int i = 0; i < size; i++) {

					if (sCheckBox[i].isChecked()) {
						Yes_No = "1";
						System.out.println("CheckBox:" + SelectedGroupsTask.member_Id.elementAt(i).toString());
						memberName = memberName + SelectedGroupsTask.member_Name.elementAt(i).toString() + "~";
					} else {
						Yes_No = "0";
					}

					Send_to_server_values = Send_to_server_values.trim()
							+ SelectedGroupsTask.member_Id.elementAt(i).toString().trim() + "~" + Yes_No + "~";
				}
				System.out.println("Send to Server values:" + Send_to_server_values);

				String memberNameArr[] = memberName.split("~");

				if (!memberName.equals("")) {

					confirmationDialog = new Dialog(getActivity());

					LayoutInflater inflater = getActivity().getLayoutInflater();
					View dialogView = inflater.inflate(R.layout.dialog_confirmation, null);
					dialogView.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
							LayoutParams.WRAP_CONTENT));

					TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
					confirmationHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.confirmation));// GetSpanText.getSpanString(getActivity(),
																													// AppStrings.confirmation));
					confirmationHeader.setTypeface(LoginActivity.sTypeface);

					TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

					try {

						CheckBox checkBox[] = new CheckBox[memberNameArr.length];

						for (int j = 0; j < memberNameArr.length; j++) {
							TableRow indv_memberNameRow = new TableRow(getActivity());

							TableRow.LayoutParams contentParams = new TableRow.LayoutParams(
									TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);
							contentParams.setMargins(10, 5, 10, 5);

							checkBox[j] = new CheckBox(getActivity());
							checkBox[j].setText(GetSpanText.getSpanString(getActivity(), memberNameArr[j]));
							checkBox[j].setChecked(true);
							checkBox[j].setClickable(false);
							checkBox[j].setTextColor(color.black);
							checkBox[j].setTypeface(LoginActivity.sTypeface);
							indv_memberNameRow.addView(checkBox[j]);

							confirmationTable.addView(indv_memberNameRow,
									new TableLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
						}

					} catch (Exception e) {
						// TODO: handle exception
						System.out.println("checkbox layout error:" + e.toString());
					}

					mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit_button);
					mEdit_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.edit));
					mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
					mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
																			// 205,
																			// 0));
					mEdit_RaisedButton.setOnClickListener(this);

					mOk_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Ok_button);
					mOk_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.yes));
					mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
					mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
					mOk_RaisedButton.setOnClickListener(this);

					confirmationDialog.getWindow()
							.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
					confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					confirmationDialog.setCanceledOnTouchOutside(false);
					confirmationDialog.setContentView(dialogView);
					confirmationDialog.setCancelable(true);
					confirmationDialog.show();

					MarginLayoutParams margin = (MarginLayoutParams) dialogView.getLayoutParams();
					margin.leftMargin = 10;
					margin.rightMargin = 10;
					margin.topMargin = 10;
					margin.bottomMargin = 10;
					margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);
				} else {
					TastyToast.makeText(getActivity(), AppStrings.MinutesAlert, TastyToast.LENGTH_SHORT,
							TastyToast.WARNING);

					Send_to_server_values = Reset.reset(Send_to_server_values);
					memberName = "";
				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			break;
		case R.id.fragment_Edit_button:

			Send_to_server_values = Reset.reset(Send_to_server_values);
			memberName = "";

			mRaised_Submit_Button.setClickable(true);
			confirmationDialog.dismiss();
			isServiceCall = false;
			break;
		case R.id.fragment_Ok_button:

			if (ConnectionUtils.isNetworkAvailable(getActivity())) {
				// Internet is present
				try {
					if (EShaktiApplication.getLoginFlag().equals(Constants.ONLINEFLAG)) {
						if (!isServiceCall) {
							isServiceCall = true;
						new Get_AttendanceTask(Meeting_AttendanceFragment.this).execute();
						}
					} else {
						TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
								TastyToast.ERROR);
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			} else {
				// Internet not present,dialog creation
				if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
					EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GET_TRANS_SINGLE,
							new GetTransactionSingle(PrefUtils.getOfflineUniqueID()));
				} else {
					TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
							TastyToast.ERROR);
				}

			}

			break;
		case R.id.fragment_attendance_minutes_Previousbutton:

			break;

		case R.id.fragment_attendance_minutes_Nextbutton:

			Transaction_SavingsFragment fragment = new Transaction_SavingsFragment();
			setFragment(fragment);
			break;
		default:
			break;
		}

	}

	@Subscribe
	public void OnGetSingleTransactionAttendance(final GetSingleTransResponse getSingleTransResponse) {
		switch (getSingleTransResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), getSingleTransResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), getSingleTransResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			String mUniqueId = GetTransactionSinglevalues.getUniqueId();
			String mAttendanceValues_Offline = GetTransactionSinglevalues.getAttendance();

			String mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
			String mMobileDate = Get_Offline_MobileDate.getOffline_MobileDate(getActivity());
			String mTransaction_UniqueId = PrefUtils.getOfflineUniqueID();
			String mAttendancevalues = Send_to_server_values + "#" + mTrasactiondate + "#" + mMobileDate;

			mLastTrDate = DatePickerDialog.sDashboardDate;
			mLastTr_ID = SelectedGroupsTask.sTr_ID.toString();

			String mCurrentTransDate = null;

			if (publicValues.mOffline_Trans_CurrentDate != null) {
				mCurrentTransDate = publicValues.mOffline_Trans_CurrentDate;
			}

			if (mUniqueId == null && mAttendanceValues_Offline == null) {

				if (mLastTrDate != null && mMobileDate != null && mTransaction_UniqueId != null
						&& mCurrentTransDate != null) {
					EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.TRANSACTIONADD,
							new Transaction(0, PrefUtils.getUsernameKey(), SelectedGroupsTask.UserName,
									SelectedGroupsTask.Ngo_Id, SelectedGroupsTask.Group_Id, mTransaction_UniqueId,
									mLastTr_ID, mCurrentTransDate, null, null, null, null, null, null, null, null, null,
									null, null, null, null, null, mAttendancevalues, null, null, null, null, null, null,
									null));
				} else {
					TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT, TastyToast.ERROR);
				}

			} else if (mUniqueId.equalsIgnoreCase(PrefUtils.getOfflineUniqueID())
					&& mAttendanceValues_Offline == null) {
				EShaktiApplication.setSetTransValues(TransactionOffConstants.TRANS_ATT);

				EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.TRANS_VALUES_UPDATE,
						new TransactionUpdate(mUniqueId, mAttendancevalues));

			} else if (mAttendanceValues_Offline != null) {

				confirmationDialog.dismiss();
				isServiceCall = false;

				TastyToast.makeText(getActivity(), AppStrings.offlineDataAvailable, TastyToast.LENGTH_SHORT,
						TastyToast.WARNING);

				DatePickerDialog.sDashboardDate = SelectedGroupsTask.sLastTransactionDate_Response;
				MainFragment_Dashboard fragment = new MainFragment_Dashboard();
				setFragment(fragment);

			}
			break;
		}
	}

	@Subscribe
	public void OnTransUpdateAttendance(final TransactionUpdateResponse transactionUpdateResponse) {
		switch (transactionUpdateResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), transactionUpdateResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), transactionUpdateResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			confirmationDialog.dismiss();
			isServiceCall = false;

			try {

				TastyToast.makeText(getActivity(), AppStrings.transactionCompleted, TastyToast.LENGTH_SHORT,
						TastyToast.SUCCESS);

				String mTransactionDate = DatePickerDialog.sSend_To_Server_Date;
				String mCashatBank = SelectedGroupsTask.sCashatBank;
				String mBankDetails = GetOfflineTransactionValues.getBankDetails();

				SelectedGroupsTask.sLastTransactionDate_Response = mTransactionDate;
				String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mTransactionDate,
						SelectedGroupsTask.sCashinHand, mCashatBank, mBankDetails);
				String mSelectedGroupId = SelectedGroupsTask.Group_Id;

				GroupDetailsProvider
						.updateGroupDetailsEntry(new GroupDetailsUpdate(mSelectedGroupId, mGroupMasterResponse));

				String mValues = Put_DB_GroupNameDetail_Response
						.put_DB_GroupNameDetails_Response(EShaktiApplication.getGroupResponse());

				GroupProvider.updateGroupResponse(
						new GroupResponseUpdate(EShaktiApplication.getGroupId_GroupLastTransDate(), mValues));

				if (EShaktiApplication.isDefault()) {
					PrefUtils.setStepWiseScreenCount("1");
					Transaction_SavingsFragment savingsFragment = new Transaction_SavingsFragment();
					setFragment(savingsFragment);
				} else {
					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		}
	}

	@Subscribe
	public void onAddTransactionAttendance(final TransactionResponse transactionResponse) {
		switch (transactionResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), transactionResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), transactionResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:
			confirmationDialog.dismiss();
			isServiceCall = false;

			try {

				TastyToast.makeText(getActivity(), AppStrings.transactionCompleted, TastyToast.LENGTH_SHORT,
						TastyToast.SUCCESS);

				String mTransactionDate = DatePickerDialog.sSend_To_Server_Date;
				String mCashatBank = SelectedGroupsTask.sCashatBank;
				String mBankDetails = GetOfflineTransactionValues.getBankDetails();

				SelectedGroupsTask.sLastTransactionDate_Response = mTransactionDate;
				String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mTransactionDate,
						SelectedGroupsTask.sCashinHand, mCashatBank, mBankDetails);
				String mSelectedGroupId = SelectedGroupsTask.Group_Id;

				GroupDetailsProvider
						.updateGroupDetailsEntry(new GroupDetailsUpdate(mSelectedGroupId, mGroupMasterResponse));

				String mValues = Put_DB_GroupNameDetail_Response
						.put_DB_GroupNameDetails_Response(EShaktiApplication.getGroupResponse());

				GroupProvider.updateGroupResponse(
						new GroupResponseUpdate(EShaktiApplication.getGroupId_GroupLastTransDate(), mValues));

				if (EShaktiApplication.isDefault()) {
					PrefUtils.setStepWiseScreenCount("1");
					Transaction_SavingsFragment savingsFragment = new Transaction_SavingsFragment();
					setFragment(savingsFragment);
				} else {
					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		}
	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub
		mprogressDialog = AppDialogUtils.createProgressDialog(getActivity());
		mprogressDialog.show();

	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub\
		try {
			if (mprogressDialog != null) {
				mprogressDialog.dismiss();

				if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {
					getActivity().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub

							if (!result.equals("FAIL")) {
								isServiceCall = false;
								TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg,
										TastyToast.LENGTH_SHORT, TastyToast.ERROR);
								Constants.NETWORKCOMMONFLAG = "SUCCESS";
							}

						}
					});

				} else {

					try {
						if (Get_AttendanceTask.get_Attendance_Response.equals("Yes")) {

							String mValues = Put_DB_GroupNameDetail_Response
									.put_DB_GroupNameDetails_Response(EShaktiApplication.getGroupResponse());

							GroupProvider.updateGroupResponse(new GroupResponseUpdate(
									EShaktiApplication.getGroupId_GroupLastTransDate(), mValues));

							String mCashinHand = SelectedGroupsTask.sCashinHand;
							String mTransactionDate = SelectedGroupsTask.sLastTransactionDate_Response;
							String mCashatBank = SelectedGroupsTask.sCashatBank;

							String mBankDetails = GetOfflineTransactionValues.getBankDetails();

							String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mTransactionDate,
									mCashinHand, mCashatBank, mBankDetails);

							String mSelectedGroupId = SelectedGroupsTask.Group_Id;

							GroupDetailsProvider.updateGroupDetailsEntry(
									new GroupDetailsUpdate(mSelectedGroupId, mGroupMasterResponse));

							TastyToast.makeText(getActivity(), AppStrings.transactionCompleted, TastyToast.LENGTH_SHORT,
									TastyToast.SUCCESS);

						} else {

							TastyToast.makeText(getActivity(), AppStrings.transactionFailAlert, TastyToast.LENGTH_SHORT,
									TastyToast.ERROR);
						}
						confirmationDialog.dismiss();
						isServiceCall = false;
						if (EShaktiApplication.isDefault()) {
							Transaction_SavingsFragment savingsFragment = new Transaction_SavingsFragment();
							setFragment(savingsFragment);
						} else {
							MainFragment_Dashboard fragment = new MainFragment_Dashboard();
							setFragment(fragment);
						}

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub

		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

		FragmentDrawer.drawer_lastTR_Date
				.setText(RegionalConversion.getRegionalConversion(AppStrings.lastTransactionDate) + " : "
						+ SelectedGroupsTask.sLastTransactionDate_Response);
		FragmentDrawer.drawer_lastTR_Date.setTypeface(LoginActivity.sTypeface);

		UpdateCalendarMinMaxDateUtils.OnUpdateCalendarDate();
		
		Calendar calender = Calendar.getInstance();

		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String formattedDate = df.format(calender.getTime());
		EShaktiApplication.setLastTransDate_GroupId(SelectedGroupsTask.Group_Id);

		new GroupTempDBLastTransDate(EShaktiApplication.getLastTransDate_GroupId(),
				SelectedGroupsTask.sLastTransactionDate_Response, formattedDate);

		GroupProvider.updateGroupLastTransDateResponse();

	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();

		SBus.INST.unRegister(this);
	}
}
