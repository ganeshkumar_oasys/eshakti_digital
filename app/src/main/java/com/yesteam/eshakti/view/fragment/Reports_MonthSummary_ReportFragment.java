package com.yesteam.eshakti.view.fragment;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.MonthReportTask;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;


public class Reports_MonthSummary_ReportFragment extends Fragment {

	private TextView mGroupName, mCashInHand, mCashAtBank, mHeadertext,mMemberName;
	String responseArr[];

	public Reports_MonthSummary_ReportFragment() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_MEMBER_REPORTS_MONTH_SUMM;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_monthlyreport,
				container, false);

		MainFragment_Dashboard.isBackpressed = false;

		try {
			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String
					.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);
			
			mMemberName = (TextView) rootView.findViewById(R.id.memberName);
			mMemberName.setText(EShaktiApplication.getSelectedMemberName());
			mMemberName.setTypeface(LoginActivity.sTypeface);
			mMemberName.setVisibility(View.VISIBLE);

			mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashInHand.setText(RegionalConversion.getRegionalConversion(String
					.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashInHand.setTypeface(LoginActivity.sTypeface);

			mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashAtBank.setText(RegionalConversion.getRegionalConversion(String
					.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashAtBank.setTypeface(LoginActivity.sTypeface);

			mHeadertext = (TextView) rootView
					.findViewById(R.id.fragment_monthlyreport_headertext);
			mHeadertext.setText(RegionalConversion
					.getRegionalConversion(AppStrings.lastMonthReport));
			mHeadertext.setTypeface(LoginActivity.sTypeface);


			responseArr = MonthReportTask.sMonthSummary_Response.split("#");
			for (int i = 0; i < responseArr.length; i++) {
				System.out.println("responseArr[] :"
						+ responseArr[i].toString());
			}

			String total = responseArr[0].toString();
			System.out.println("Total : " + total);

			String total_savings[] = total.split("~");

			TableLayout totalTable = (TableLayout) rootView
					.findViewById(R.id.fragment_totalTable);

			try {
				TableRow totalHeader = new TableRow(getActivity());

				TableRow.LayoutParams totalParams = new TableRow.LayoutParams(
						LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT,
						1f);

				TextView totalTxt = new TextView(getActivity());
				totalTxt.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.totalSavings)));
				totalTxt.setTypeface(LoginActivity.sTypeface);
			//	totalTxt.setTextSize(15);
				totalTxt.setPadding(20, 5, 10, 5);
				totalTxt.setTextColor(Color.BLACK);
				totalTxt.setLayoutParams(totalParams);
				totalHeader.addView(totalTxt);

				TextView totalAmount = new TextView(getActivity());
				totalAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(total_savings[1])));
				totalAmount.setTextSize(15);
				totalAmount.setPadding(5, 5, 5, 5);
				totalAmount.setTextColor(Color.BLACK);
				totalAmount.setTypeface(null, Typeface.BOLD);
				totalAmount.setLayoutParams(totalParams);
				totalHeader.addView(totalAmount);

				totalTable.addView(totalHeader, new TableLayout.LayoutParams(
						LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
			} catch (Exception e) {
				e.printStackTrace();
			}

			TableLayout loanSummaryTable = (TableLayout) rootView
					.findViewById(R.id.fragment_loanSummaryTable);
			try {
				TableRow loanSummaryHeader = new TableRow(getActivity());

				TableRow.LayoutParams loanSummaryParams = new TableRow.LayoutParams(
						LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT,
						1f);

				TextView loanSummary = new TextView(getActivity());
				loanSummary.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.loanSummary)));
				loanSummary.setTypeface(LoginActivity.sTypeface);
				loanSummary.setTextSize(15);
				loanSummary.setPadding(20, 5, 5, 5);
				loanSummary.setBackgroundResource(R.color.tableHeader);
				loanSummary.setTextColor(Color.WHITE);
				loanSummary.setLayoutParams(loanSummaryParams);
				loanSummaryHeader.addView(loanSummary);

				TextView paymentAmount = new TextView(getActivity());
				paymentAmount.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.mLimit)));
				paymentAmount.setTypeface(LoginActivity.sTypeface);
				paymentAmount.setBackgroundResource(R.color.tableHeader);
				paymentAmount.setGravity(Gravity.RIGHT);
				paymentAmount.setTextSize(15);
				paymentAmount.setPadding(10, 5, 5, 5);
				paymentAmount.setTextColor(Color.WHITE);
				paymentAmount.setLayoutParams(loanSummaryParams);
				loanSummaryHeader.addView(paymentAmount);

				TextView receipt = new TextView(getActivity());
				receipt.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.outstanding)));
				receipt.setTypeface(LoginActivity.sTypeface);
				receipt.setBackgroundResource(R.color.tableHeader);
				receipt.setGravity(Gravity.RIGHT);
				receipt.setTextSize(15);
				receipt.setPadding(10, 5, 5, 5);
				receipt.setTextColor(Color.WHITE);
				receipt.setLayoutParams(loanSummaryParams);
				loanSummaryHeader.addView(receipt);


		/*		TextView limit = new TextView(getActivity());
				limit.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.mLimit)));
				limit.setTypeface(LoginActivity.sTypeface);
				limit.setTextSize(15);
				limit.setPadding(10, 5, 10, 5);
				limit.setBackgroundResource(R.color.tableHeader);
				limit.setTextColor(Color.WHITE);
				limit.setLayoutParams(loanSummaryParams);
				loanSummaryHeader.addView(loanSummary);


				TextView outstanding = new TextView(getActivity());
				outstanding.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.outstanding)));
				outstanding.setTypeface(LoginActivity.sTypeface);
				outstanding.setTextSize(15);
				outstanding.setPadding(10, 5, 10, 5);
				outstanding.setBackgroundResource(R.color.tableHeader);
				outstanding.setTextColor(Color.WHITE);
				outstanding.setLayoutParams(loanSummaryParams);
				loanSummaryHeader.addView(loanSummary);
*/

				loanSummaryTable.addView(loanSummaryHeader,
						new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT,
								LayoutParams.WRAP_CONTENT));

				String loan_details[] = responseArr[2].split("~");
				for (int i = 1; i < loan_details.length; i = i + 3) {
					TableRow loanSummaryContent = new TableRow(getActivity());
					TableRow.LayoutParams loanContentParams = new TableRow.LayoutParams(
							LayoutParams.MATCH_PARENT,
							LayoutParams.WRAP_CONTENT, 1f);

					TextView loanName = new TextView(getActivity());
					loanName.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(loan_details[i])));
					loanName.setTypeface(LoginActivity.sTypeface);
					loanName.setTextSize(14);
					loanName.setPadding(20, 5, 10, 5);
					loanName.setTextColor(color.black);
					loanName.setLayoutParams(loanContentParams);
					loanSummaryContent.addView(loanName);

					TextView amount = new TextView(getActivity());
					amount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(loan_details[i + 1])));
					amount.setTextSize(14);
					amount.setGravity(Gravity.RIGHT);
					amount.setPadding(10, 5, 10, 5);
					amount.setTextColor(color.black);
					amount.setLayoutParams(loanContentParams);
					loanSummaryContent.addView(amount);

					TextView outstanding_amount = new TextView(getActivity());
					outstanding_amount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(loan_details[i + 2])));
					outstanding_amount.setTextSize(14);
					outstanding_amount.setGravity(Gravity.RIGHT);
					outstanding_amount.setPadding(10, 5, 10, 5);
					outstanding_amount.setTextColor(color.black);
					outstanding_amount.setLayoutParams(loanContentParams);
					loanSummaryContent.addView(outstanding_amount);

					loanSummaryTable.addView(loanSummaryContent,
							new TableLayout.LayoutParams(
									LayoutParams.MATCH_PARENT,
									LayoutParams.WRAP_CONTENT));

					View rullerView = new View(getActivity());
					rullerView.setLayoutParams(new TableRow.LayoutParams(
							TableRow.LayoutParams.MATCH_PARENT, 1));
					rullerView.setBackgroundColor(Color.rgb(245, 245, 245));

					loanSummaryTable.addView(rullerView);

				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

			TableLayout monthlyReportTable = (TableLayout) rootView
					.findViewById(R.id.fragment_monthlyReportTable);
			try {
				TableRow monthlyReportHeader = new TableRow(getActivity());

				TableRow.LayoutParams monthlyReportParams = new TableRow.LayoutParams(
						LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT,
						1f);

				TextView monthlyReport = new TextView(getActivity());
				monthlyReport.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.lastMonthReport)));
				monthlyReport.setTypeface(LoginActivity.sTypeface);
				monthlyReport.setTextSize(15);
				monthlyReport.setPadding(20, 5, 20, 5);
				monthlyReport.setBackgroundResource(R.color.tableHeader);
				monthlyReport.setTextColor(Color.WHITE);
				monthlyReport.setLayoutParams(monthlyReportParams);
				monthlyReportHeader.addView(monthlyReport);

				monthlyReportTable.addView(monthlyReportHeader,
						new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT,
								LayoutParams.WRAP_CONTENT));

				String lastmonth_Report[] = responseArr[4].split("~");
				System.out
						.println("Report length : " + lastmonth_Report.length);
				for (int j = 1; j < lastmonth_Report.length; j = j + 2) {
					TableRow monthReportContent = new TableRow(getActivity());
					TableRow.LayoutParams monthContentParams = new TableRow.LayoutParams(
							LayoutParams.MATCH_PARENT,
							LayoutParams.WRAP_CONTENT, 1f);

					TextView loanName = new TextView(getActivity());
					loanName.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(lastmonth_Report[j]
									)));
					loanName.setTypeface(LoginActivity.sTypeface);
					loanName.setTextSize(14);
					loanName.setPadding(20, 5, 10, 5);
					loanName.setTextColor(color.black);
					loanName.setLayoutParams(monthContentParams);
					monthReportContent.addView(loanName);

					TextView amount = new TextView(getActivity());
					amount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(lastmonth_Report[j + 1])));
					amount.setTextSize(14);
					amount.setGravity(Gravity.RIGHT);
					amount.setPadding(10, 5, 10, 5);
					amount.setTextColor(color.black);
					amount.setLayoutParams(monthContentParams);
					monthReportContent.addView(amount);

					monthlyReportTable.addView(monthReportContent,
							new TableLayout.LayoutParams(
									LayoutParams.MATCH_PARENT,
									LayoutParams.WRAP_CONTENT));

					View rullerView = new View(getActivity());
					rullerView.setLayoutParams(new TableRow.LayoutParams(
							TableRow.LayoutParams.MATCH_PARENT, 1));
					rullerView.setBackgroundColor(Color.rgb(245, 245, 245));

					monthlyReportTable.addView(rullerView);

				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return rootView;
	}
}
