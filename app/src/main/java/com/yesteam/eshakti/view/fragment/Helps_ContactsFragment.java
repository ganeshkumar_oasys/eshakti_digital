package com.yesteam.eshakti.view.fragment;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;

public class Helps_ContactsFragment extends Fragment {

	private TextView mGroupName, mCashInHand, mCashAtBank, mHeadertext;
	private TableLayout mTableLayout;

	public Helps_ContactsFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_CONTACTS;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_help_contacts, container, false);

		MainFragment_Dashboard.isBackpressed = false;

		try {
			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashInHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashInHand.setTypeface(LoginActivity.sTypeface);

			mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashAtBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashAtBank.setTypeface(LoginActivity.sTypeface);

			mHeadertext = (TextView) rootView.findViewById(R.id.contacts_titleHead);
			mHeadertext.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.contacts)));
			mHeadertext.setTypeface(LoginActivity.sTypeface);

			mTableLayout = (TableLayout) rootView.findViewById(R.id.contactTableLayout);
			LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
			params.setMargins(10, 5, 10, 5);

			// final String contact_No = "04342 - 261337";

			// final String contact_No = "+919994250580";
			 
			String[] mContacts = null;

			if (PrefUtils.getOfflineContacts() != null) {
				if (EShaktiApplication.getEshaktiContacts() == null) {
					EShaktiApplication.setEshaktiContacts(PrefUtils.getOfflineContacts());
				}
			}
			if (EShaktiApplication.getEshaktiContacts() != null) {
				mContacts = EShaktiApplication.getEshaktiContacts().split("%");
				for (int i = 0; i < mContacts.length; i++) {
					String mContactSplit_temp = mContacts[i];
					// mContactSplit = mContactSplit_temp.split("~");

					// final String contact_No = mContactSplit[0] + " " +
					// mContactSplit[1];
					final String contact_No = mContactSplit_temp;
					TableRow row = new TableRow(getActivity());

					TextView contactText = new TextView(getActivity());
					contactText.setText(
							RegionalConversion.getRegionalConversion(AppStrings.contactNo + " :  " + contact_No));
					contactText.setTypeface(LoginActivity.sTypeface);
					contactText.setTextColor(color.black);
					contactText.setPadding(5, 10, 5, 5);

					row.addView(contactText);

					TextView emptyTextView = new TextView(getActivity());
					emptyTextView.setText("   ");
					emptyTextView.setLayoutParams(params);
					row.addView(emptyTextView);

					ImageView imageView = new ImageView(getActivity());
					imageView.setBackgroundResource(android.R.drawable.ic_menu_call);
					imageView.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							Intent callIntent = new Intent(Intent.ACTION_CALL);
							callIntent.setData(Uri.parse("tel:" + contact_No));
							startActivity(callIntent);

						}
					});
					row.addView(imageView);

					mTableLayout.addView(row);

				}
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return rootView;
	}
}
