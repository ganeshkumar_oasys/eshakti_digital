package com.yesteam.eshakti.view.fragment;

import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.utils.Reset;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.views.RaisedButton;
import com.yesteam.eshakti.webservices.Get_Shg_Account_Update_Webservices;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Profile_Shg_AccountNumberUpdation_Fragment extends Fragment implements TaskListener, OnClickListener {

	public static String TAG = Transaction_InternalLoanDisburseMentFragment.class.getSimpleName();
	private TextView mGroupName, mCashinHand, mCashatBank, mHeader;

	TextView _AccountNumberText, _AccountNumberText_two;
	EditText _AccountNumberValue, _AccountNumberValue_two;

	private Button mSubmit_Raised_Button;
	private Dialog mProgressDialog;
	boolean isServiceCall = false;

	TextView _BankHeader_one, _BankHeader_two;
	TextView _BankName_one, _BankName_Text_one, _BankName_two, _BankName_Text_two, BranchName_one, _BranchName_Text_one,
			_BranchName_two, _BranchName_Text_two;
	LinearLayout _LinearLayout_secondary;

	String _bankNameValue, _BranchNameValue, _AccountNumberValue_string, _bankNameValue_two, _BranchNameValue_two,
			_AccountNumberValue_two_string;
	public static String mSendtoServerValues = null;
	String mAccountNumber_GetEdit = null;
	String mAccountNumber_GetEdit_two = null;
	Dialog confirmationDialog;
	private RaisedButton mEdit_RaisedButton, mOk_RaisedButton;

	public Profile_Shg_AccountNumberUpdation_Fragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_INTERLAONMENU;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		SBus.INST.register(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_shg_account_number_updation, container, false);

		MainFragment_Dashboard.isBackpressed = false;

		try {

			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashinHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashinHand.setTypeface(LoginActivity.sTypeface);

			mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashatBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashatBank.setTypeface(LoginActivity.sTypeface);

			mHeader = (TextView) rootView.findViewById(R.id.fragmentHeader_shg_accountNumber);

			mSubmit_Raised_Button = (RaisedButton) rootView.findViewById(R.id.fragment_Submit_button_shg_accountNumber);

			_BankHeader_one = (TextView) rootView.findViewById(R.id.fragmentHeader_shg_account_bank);
			_BankHeader_two = (TextView) rootView.findViewById(R.id.fragmentHeader_shg_account_bank_secondary);

			_BankName_one = (TextView) rootView.findViewById(R.id._shg_bankname_textview);
			_BankName_Text_one = (TextView) rootView.findViewById(R.id._shg_bankname_label);
			BranchName_one = (TextView) rootView.findViewById(R.id._shg_branchname_textview);
			_BranchName_Text_one = (TextView) rootView.findViewById(R.id._shg_branchname_label);
			_AccountNumberText = (TextView) rootView.findViewById(R.id._shg_accountNumber_textview);
			_AccountNumberValue = (EditText) rootView.findViewById(R.id._shg_accountNumber);

			_BankName_two = (TextView) rootView.findViewById(R.id._shg_bankname_textview_secondary);
			_BankName_Text_two = (TextView) rootView.findViewById(R.id._shg_bankname_label_secondary);
			_BranchName_two = (TextView) rootView.findViewById(R.id._shg_branchname_textview_secondary);
			_BranchName_Text_two = (TextView) rootView.findViewById(R.id._shg_branchname_label_secondary);
			_AccountNumberText_two = (TextView) rootView.findViewById(R.id._shg_accountNumber_textview_secondary);
			_AccountNumberValue_two = (EditText) rootView.findViewById(R.id._shg_accountNumber_secondary);

			_LinearLayout_secondary = (LinearLayout) rootView.findViewById(R.id.linear_layout_secondary);

			mHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.mSHGAccountNoUpdation));

			if (publicValues.mGetShgAccountNumberValues != null
					&& !publicValues.mGetShgAccountNumberValues.equals("No")) {

				String _StringArray[] = publicValues.mGetShgAccountNumberValues.split("#");

				String _BankNameArray_One[] = _StringArray[0].split("~");

				String _BankNameArray_two[] = _StringArray[1].split("~");

				_bankNameValue = _BankNameArray_One[0];
				_BranchNameValue = _BankNameArray_One[1];
				_AccountNumberValue_string = _BankNameArray_One[2];
				_bankNameValue_two = _BankNameArray_two[0];
				_BranchNameValue_two = _BankNameArray_two[1];
				_AccountNumberValue_two_string = _BankNameArray_two[2];

			}

			mHeader.setTypeface(LoginActivity.sTypeface);
			_AccountNumberText.setTypeface(LoginActivity.sTypeface);
			_AccountNumberValue.setTypeface(LoginActivity.sTypeface);
			mSubmit_Raised_Button.setTypeface(LoginActivity.sTypeface);

			_BankHeader_one.setTypeface(LoginActivity.sTypeface);
			_BankHeader_two.setTypeface(LoginActivity.sTypeface);

			_BankName_one.setTypeface(LoginActivity.sTypeface);
			_BankName_Text_one.setTypeface(LoginActivity.sTypeface);
			BranchName_one.setTypeface(LoginActivity.sTypeface);
			_BranchName_Text_one.setTypeface(LoginActivity.sTypeface);

			_BankName_two.setTypeface(LoginActivity.sTypeface);
			_BankName_Text_two.setTypeface(LoginActivity.sTypeface);
			_BranchName_two.setTypeface(LoginActivity.sTypeface);
			_BranchName_Text_two.setTypeface(LoginActivity.sTypeface);
			_AccountNumberText_two.setTypeface(LoginActivity.sTypeface);
			_AccountNumberValue_two.setTypeface(LoginActivity.sTypeface);

			if (_bankNameValue_two.equals("No") && _BranchNameValue_two.equals("No")) {

				_LinearLayout_secondary.setVisibility(View.GONE);
			} else {
				_LinearLayout_secondary.setVisibility(View.VISIBLE);
			}

			_BankHeader_one.setTypeface(LoginActivity.sTypeface);
			_BankHeader_two.setTypeface(LoginActivity.sTypeface);

			_BankName_one.setTypeface(LoginActivity.sTypeface);
			_BankName_Text_one.setTypeface(LoginActivity.sTypeface);
			BranchName_one.setTypeface(LoginActivity.sTypeface);
			_BranchName_Text_one.setTypeface(LoginActivity.sTypeface);

			_BankName_two.setTypeface(LoginActivity.sTypeface);
			_BankName_Text_two.setTypeface(LoginActivity.sTypeface);
			_BranchName_two.setTypeface(LoginActivity.sTypeface);
			_BranchName_Text_two.setTypeface(LoginActivity.sTypeface);
			_AccountNumberText_two.setTypeface(LoginActivity.sTypeface);
			_AccountNumberValue_two.setTypeface(LoginActivity.sTypeface);

			_BankHeader_one.setText("FIRST BANK");
			_BankName_one.setText(RegionalConversion.getRegionalConversion(AppStrings.bankName));
			_BankName_Text_one.setText(_bankNameValue);
			BranchName_one.setText(RegionalConversion.getRegionalConversion(AppStrings.mBranchName));
			_BranchName_Text_one.setText(_BranchNameValue);
			_AccountNumberText.setText(RegionalConversion.getRegionalConversion(AppStrings.mAccountNumber));
			_AccountNumberValue.setText(_AccountNumberValue_string);

			_BankHeader_two.setText("SECOND BANK");
			_BankName_two.setText(RegionalConversion.getRegionalConversion(AppStrings.bankName));
			_BankName_Text_two.setText(_bankNameValue_two);
			_BranchName_two.setText(RegionalConversion.getRegionalConversion(AppStrings.mBranchName));
			_BranchName_Text_two.setText(_BranchNameValue_two);
			_AccountNumberText_two.setText(RegionalConversion.getRegionalConversion(AppStrings.mAccountNumber));
			_AccountNumberValue_two.setText(_AccountNumberValue_two_string);

			mSubmit_Raised_Button.setOnClickListener(this);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return rootView;
	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub
		mProgressDialog = AppDialogUtils.createProgressDialog(getActivity());
		mProgressDialog.show();
	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub

		try {

			if (mProgressDialog != null) {
				mProgressDialog.dismiss();

				if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {
					getActivity().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub

							if (!result.equals("FAIL")) {
								isServiceCall = false;
								TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg,
										TastyToast.LENGTH_SHORT, TastyToast.ERROR);
								Constants.NETWORKCOMMONFLAG = "SUCESS";
							}

						}
					});
				} else {

					TastyToast.makeText(getActivity(), AppStrings.mSHGAccNoUpdateSuccessAlert, TastyToast.LENGTH_SHORT,
							TastyToast.SUCCESS);

					confirmationDialog.dismiss();
					MainFragment_Dashboard dashboard = new MainFragment_Dashboard();
					setFragment(dashboard);
				}
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub
		Log.e("Current Class Name!!!!!!!!!!!!!!", fragment.getClass().getName());
		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		SBus.INST.unRegister(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.fragment_Submit_button_shg_accountNumber:

			mAccountNumber_GetEdit = _AccountNumberValue.getText().toString();
			mAccountNumber_GetEdit_two = _AccountNumberValue_two.getText().toString();

			mSendtoServerValues = Reset.reset(mSendtoServerValues);
			boolean isError = false;

			if (!_bankNameValue.equals("No") && !_BranchNameValue.equals("No") && !_bankNameValue_two.equals("No")
					&& !_BranchNameValue_two.equals("No")) {

				if (!mAccountNumber_GetEdit.equals("") && !mAccountNumber_GetEdit_two.equals("")) {

					if (_bankNameValue.equals(_bankNameValue_two) && _BranchNameValue.equals(_bankNameValue_two)
							&& mAccountNumber_GetEdit.equals(mAccountNumber_GetEdit_two)) {
						isError = true;
					}

					if (!isError) {

						mSendtoServerValues = _bankNameValue + "~" + _BranchNameValue + "~" + _AccountNumberValue_string
								+ "#" + _bankNameValue_two + "~" + _BranchNameValue_two + "~"
								+ _AccountNumberValue_two_string + "#" + _bankNameValue + "~" + _BranchNameValue + "~"
								+ mAccountNumber_GetEdit + "#" + _bankNameValue_two + "~" + _BranchNameValue_two + "~"
								+ mAccountNumber_GetEdit_two;

						Log.e("Yes Doing!!!!", mSendtoServerValues);

						callConfirmationDialog();

					} else {
						TastyToast.makeText(getActivity(), "PLEASE CHECK YOUR BANK DETAILS", TastyToast.LENGTH_SHORT,
								TastyToast.ERROR);
					}

				} else {

					TastyToast.makeText(getActivity(), AppStrings.nullDetailsAlert, TastyToast.LENGTH_SHORT,
							TastyToast.ERROR);
				}
			} else if (_bankNameValue_two.equals("No")) {

				if (!_bankNameValue.equals("No") && !_BranchNameValue.equals("No")) {
					if (!mAccountNumber_GetEdit.equals("")) {

						mSendtoServerValues = _bankNameValue + "~" + _BranchNameValue + "~" + _AccountNumberValue_string
								+ "#" + _bankNameValue_two + "~" + _BranchNameValue_two + "~"
								+ _AccountNumberValue_two_string + "#" + _bankNameValue + "~" + _BranchNameValue + "~"
								+ mAccountNumber_GetEdit + "#" + _bankNameValue_two + "~" + _BranchNameValue_two + "~"
								+ mAccountNumber_GetEdit_two;

						Log.e("Yes Doing!!!!", mSendtoServerValues);

						callConfirmationDialog();

					} else {

						TastyToast.makeText(getActivity(), AppStrings.nullDetailsAlert, TastyToast.LENGTH_SHORT,
								TastyToast.ERROR);
					}
				}
			}

			break;

		case R.id.fragment_Edit_button:

			mSendtoServerValues = Reset.reset(mSendtoServerValues);
			mSubmit_Raised_Button.setClickable(true);
			confirmationDialog.dismiss();
			break;

		case R.id.fragment_Ok_button:

			if (ConnectionUtils.isNetworkAvailable(getActivity())) {

				new Get_Shg_Account_Update_Webservices(this).execute();

			} else {
				// Do offline Stuffs

				TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT, TastyToast.ERROR);

			}
			break;

		}
	}

	@SuppressWarnings("deprecation")
	private void callConfirmationDialog() {

		try {

			// TODO Auto-generated method stub

			confirmationDialog = new Dialog(getActivity());

			LayoutInflater inflater = getActivity().getLayoutInflater();
			View dialogView = inflater.inflate(R.layout.dialog_confirmation, null);
			dialogView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT));

			TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
			confirmationHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.confirmation));
			confirmationHeader.setTypeface(LoginActivity.sTypeface);

			TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

			String[] bankDetailsLeft = { RegionalConversion.getRegionalConversion(AppStrings.bankName),
					RegionalConversion.getRegionalConversion(AppStrings.mBranchName),
					RegionalConversion.getRegionalConversion(AppStrings.mAccountNumber) };
			String[] bankOneValue = { _bankNameValue, _BranchNameValue, mAccountNumber_GetEdit };
			String[] bankTwoValue = { _bankNameValue_two, _BranchNameValue_two, mAccountNumber_GetEdit_two };

			TableRow firstBankHeader_row = new TableRow(getActivity());

			TableRow.LayoutParams firstBankParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);
			firstBankParams.setMargins(10, 5, 10, 5);

			TextView firstBank_Text = new TextView(getActivity());
			firstBank_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf("FIRST BANK  :  ")));
			firstBank_Text.setTypeface(LoginActivity.sTypeface);
			firstBank_Text.setTextColor(getResources().getColor(R.color.colorPrimary));
			firstBank_Text.setPadding(5, 5, 5, 5);
			firstBank_Text.setTextSize(20);
			firstBank_Text.setLayoutParams(firstBankParams);
			firstBankHeader_row.addView(firstBank_Text);

			confirmationTable.addView(firstBankHeader_row,
					new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

			for (int i = 0; i < bankDetailsLeft.length; i++) {
				TableRow indv_SavingsRow = new TableRow(getActivity());

				TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
						LayoutParams.WRAP_CONTENT, 1f);
				contentParams.setMargins(10, 5, 10, 5);

				TextView memberName_Text = new TextView(getActivity());
				memberName_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(bankDetailsLeft[i])));
				memberName_Text.setTypeface(LoginActivity.sTypeface);
				memberName_Text.setTextColor(color.black);
				memberName_Text.setPadding(5, 5, 5, 5);
				memberName_Text.setLayoutParams(contentParams);
				indv_SavingsRow.addView(memberName_Text);

				TextView confirm_values = new TextView(getActivity());
				confirm_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(bankOneValue[i])));
				confirm_values.setTextColor(color.black);
				confirm_values.setPadding(5, 5, 5, 5);
				confirm_values.setGravity(Gravity.RIGHT);
				confirm_values.setLayoutParams(contentParams);
				indv_SavingsRow.addView(confirm_values);

				confirmationTable.addView(indv_SavingsRow,
						new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

			}

			if (!_bankNameValue_two.equals("No") && !_BranchNameValue_two.equals("No")) {
				System.out.println("-----------------------------");
				TableRow secondBankHeader_row = new TableRow(getActivity());

				TableRow.LayoutParams secondBankParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
						LayoutParams.WRAP_CONTENT, 1f);
				secondBankParams.setMargins(10, 5, 10, 5);

				TextView secondBank_Text = new TextView(getActivity());
				secondBank_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf("SECOND BANK  :  ")));
				secondBank_Text.setTypeface(LoginActivity.sTypeface);
				secondBank_Text.setTextColor(color.colorPrimary);
				secondBank_Text.setPadding(5, 5, 5, 5);
				secondBank_Text.setTextSize(18);
				secondBank_Text.setLayoutParams(secondBankParams);
				secondBankHeader_row.addView(secondBank_Text);

				confirmationTable.addView(secondBankHeader_row,
						new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

				for (int i = 0; i < bankDetailsLeft.length; i++) {
					TableRow indv_SavingsRow = new TableRow(getActivity());

					TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
							LayoutParams.WRAP_CONTENT, 1f);
					contentParams.setMargins(10, 5, 10, 5);

					TextView memberName_Text = new TextView(getActivity());
					memberName_Text
							.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(bankDetailsLeft[i])));
					memberName_Text.setTypeface(LoginActivity.sTypeface);
					memberName_Text.setTextColor(color.black);
					memberName_Text.setPadding(5, 5, 5, 5);
					memberName_Text.setLayoutParams(contentParams);
					indv_SavingsRow.addView(memberName_Text);

					TextView confirm_values = new TextView(getActivity());
					confirm_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(bankTwoValue[i])));
					confirm_values.setTextColor(color.black);
					confirm_values.setPadding(5, 5, 5, 5);
					confirm_values.setGravity(Gravity.RIGHT);
					confirm_values.setLayoutParams(contentParams);
					indv_SavingsRow.addView(confirm_values);

					confirmationTable.addView(indv_SavingsRow,
							new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

				}

			}

			mEdit_RaisedButton = (RaisedButton) dialogView.findViewById(R.id.fragment_Edit_button);
			mEdit_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.edit));
			mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
			mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
			mEdit_RaisedButton.setOnClickListener(this);

			mOk_RaisedButton = (RaisedButton) dialogView.findViewById(R.id.fragment_Ok_button);
			mOk_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.yes));
			mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
			mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
			mOk_RaisedButton.setOnClickListener(this);

			confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			confirmationDialog.setCanceledOnTouchOutside(false);
			confirmationDialog.setContentView(dialogView);
			confirmationDialog.setCancelable(true);
			confirmationDialog.show();

			MarginLayoutParams margin = (MarginLayoutParams) dialogView.getLayoutParams();
			margin.leftMargin = 10;
			margin.rightMargin = 10;
			margin.topMargin = 10;
			margin.bottomMargin = 10;
			margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}