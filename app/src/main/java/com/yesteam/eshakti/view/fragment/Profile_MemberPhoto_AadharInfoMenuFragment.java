package com.yesteam.eshakti.view.fragment;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.oasys.eshakti.digitization.R;
import com.squareup.otto.Subscribe;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.adapter.CustomListAdapter;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.model.ListItem;
import com.yesteam.eshakti.sqlite.database.response.MemberPhotoResponse;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.views.ButtonFlat;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

public class Profile_MemberPhoto_AadharInfoMenuFragment extends Fragment implements OnItemClickListener, TaskListener {

	private TextView mGroupName, mCashinHand, mCashatBank, mMemberName;

	private List<ListItem> listItems;
	private ListView mListView;
	private CustomListAdapter mAdapter;
	int listImage;

	String memberDeatilsMenu[];
	final static int cameracapture = 9;
	String fileName;
	private Dialog mProgressDilaog;
	public static String mServiceResponse;
	static File file;
	public static final int MEDIA_TYPE_IMAGE = 1;

	File destination;

	public Profile_MemberPhoto_AadharInfoMenuFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_PHOTO_AADHAAR_INFOMENU;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		memberDeatilsMenu = new String[] { RegionalConversion.getRegionalConversion(AppStrings.uploadAadhar),
				RegionalConversion.getRegionalConversion(AppStrings.uploadPhoto) };
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		SBus.INST.register(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View rootView = inflater.inflate(R.layout.fragment_menulist, container, false);

		MainFragment_Dashboard.isBackpressed = false;

		try {

			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mMemberName = (TextView) rootView.findViewById(R.id.memberName);
			mMemberName.setText(EShaktiApplication.getSelectedMemberName());
			mMemberName.setTypeface(LoginActivity.sTypeface);
			mMemberName.setVisibility(View.VISIBLE);

			mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashinHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashinHand.setTypeface(LoginActivity.sTypeface);

			mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashatBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashatBank.setTypeface(LoginActivity.sTypeface);

			int mSize = 2;

			listItems = new ArrayList<ListItem>();
			mListView = (ListView) rootView.findViewById(R.id.fragment_List);
			listImage = R.drawable.ic_navigate_next_white_24dp;

			for (int i = 0; i < mSize; i++) {
				ListItem item = new ListItem(memberDeatilsMenu[i], listImage);
				listItems.add(item);
			}

			System.out.println("ROW ITEM " + listItems.size());

			mAdapter = new CustomListAdapter(getActivity(), listItems);
			mListView.setAdapter(mAdapter);
			mListView.setOnItemClickListener(this);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return rootView;
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		// TODO Auto-generated method stub
		TextView textColor_Change = (TextView) view.findViewById(R.id.dynamicText);
		textColor_Change.setText(String.valueOf(memberDeatilsMenu[position]));
		textColor_Change.setTextColor(Color.rgb(251, 161, 108));

		Log.d("MENU", "Menu Item " + memberDeatilsMenu[position]);
		switch (position) {
		case 0:
			showConfirmationDialog("AADHAARCARD");
			break;
		case 1:
			// CameraCapture();
			// cameraIntent();
			Profile_Member_TakePhotoFragment fragment = new Profile_Member_TakePhotoFragment();
			getActivity().getSupportFragmentManager().beginTransaction()

					.replace(R.id.frame, fragment)
					.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
					.addToBackStack(fragment.getClass().getName()).commit();
			break;
		default:
			break;
		}
	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub
		mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
		mProgressDilaog.show();
	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub
		if (mProgressDilaog != null) {

			mProgressDilaog.dismiss();
			if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {
				getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub

						if (!result.equals("FAIL")) {

							TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg,
									TastyToast.LENGTH_SHORT, TastyToast.ERROR);
							Constants.NETWORKCOMMONFLAG = "SUCCESS";
						}

					}
				});
			} else {
				if (destination.exists()) {
					destination.delete();
				}

				TastyToast.makeText(getActivity(), AppStrings.photouploadmsg, TastyToast.LENGTH_SHORT,
						TastyToast.SUCCESS);

				MemberList_Fragment fragment = new MemberList_Fragment();
				getActivity().getSupportFragmentManager().beginTransaction()

						.replace(R.id.frame, fragment)
						.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
						.addToBackStack(fragment.getClass().getName()).commit();
			}

		}
	}

	private void showConfirmationDialog(final String string) {
		final Dialog confirmationDialog = new Dialog(getActivity());

		LayoutInflater li = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View customView = li.inflate(R.layout.dialog_alert, null, false);
		final ButtonFlat mConYesButton;

		TextView mConfirmationHeadertextview;
		TextView mConfirmationTexttextview;

		mConfirmationHeadertextview = (TextView) customView.findViewById(R.id.dialog_TransactionDate_alert);
		mConfirmationTexttextview = (TextView) customView.findViewById(R.id.dialog_continueDate_alert);

		mConYesButton = (ButtonFlat) customView.findViewById(R.id.fragment_No_button_alert);

		mConfirmationHeadertextview.setText(RegionalConversion.getRegionalConversion(AppStrings.confirmation));
		mConfirmationHeadertextview.setTypeface(LoginActivity.sTypeface);

		mConfirmationTexttextview.setText(RegionalConversion.getRegionalConversion(AppStrings.showAadharAlert));
		mConfirmationTexttextview.setTypeface(LoginActivity.sTypeface);

		mConYesButton.setText(RegionalConversion.getRegionalConversion(AppStrings.yes));
		mConYesButton.setTypeface(LoginActivity.sTypeface);
		mConYesButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (confirmationDialog.isShowing() && confirmationDialog != null) {

					confirmationDialog.dismiss();
					Profile_AadhaarCardFragment fragment = new Profile_AadhaarCardFragment();
					getActivity().getSupportFragmentManager().beginTransaction()

							.replace(R.id.frame, fragment)
							.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
							.addToBackStack(fragment.getClass().getName()).commit();

				}

			}
		});

		confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		confirmationDialog.setCanceledOnTouchOutside(false);
		confirmationDialog.setContentView(customView);
		confirmationDialog.setCancelable(true);
		confirmationDialog.show();

	}

	@Subscribe
	public void OnMemberUploadPhoto(final MemberPhotoResponse memberPhotoResponse) {
		switch (memberPhotoResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), memberPhotoResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), memberPhotoResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			TastyToast.makeText(getActivity(), AppStrings.transactionCompleted, TastyToast.LENGTH_SHORT,
					TastyToast.SUCCESS);

			MemberList_Fragment fragment = new MemberList_Fragment();
			getActivity().getSupportFragmentManager().beginTransaction()

					.replace(R.id.frame, fragment)
					.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
					.addToBackStack(fragment.getClass().getName()).commit();
			break;
		}
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		SBus.INST.unRegister(this);
	}

}
