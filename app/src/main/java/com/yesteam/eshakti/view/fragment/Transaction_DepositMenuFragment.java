package com.yesteam.eshakti.view.fragment;

import java.util.ArrayList;
import java.util.List;

import com.squareup.otto.Subscribe;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.adapter.CustomListAdapter;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.model.ListItem;
import com.yesteam.eshakti.model.RowItem;
import com.yesteam.eshakti.sqlite.database.response.GetBankAccountNoResponse;
import com.yesteam.eshakti.sqlite.database.response.GroupFixedOSResponse;
import com.yesteam.eshakti.sqlite.db.model.GetGroupMemberDetails;
import com.yesteam.eshakti.sqlite.db.model.GroupMasterSingle;
import com.yesteam.eshakti.sqlite.db.transactions.TransactionManager.DataType;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.Fixed_Deposit_BalanceTask;
import com.yesteam.eshakti.webservices.Get_sb_account_number_webservices;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

public class Transaction_DepositMenuFragment extends Fragment implements TaskListener, OnItemClickListener {

	private TextView mGroupName, mCashInHand, mCashAtBank;
	private Dialog mProgressDialog;

	public static String[] depositMenu;

	ListView listView;
	List<RowItem> rowItems;
	Intent intent;
	int size;
	FragmentTransaction frTransaction;
	public static boolean isFixedDepositEntry = false;

	public static String[] response;
	public static String mFixedOS_Offlineresponse;
	public static String mFD_BankId_Offline = null;

	private ListView mListView;
	private List<ListItem> listItems;
	private CustomListAdapter mAdapter;
	int listImage;
	public static boolean isAccToAccTransfer = false;
	ArrayList<String> mBankName = new ArrayList<String>();
	ArrayList<String> mBankNameSendtoServer = new ArrayList<String>();
	ArrayList<String> mBankAccNo = new ArrayList<String>();
	private TextView mHeader;

	public Transaction_DepositMenuFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_DEPOSIT_MENU;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		SBus.INST.register(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View rootView = inflater.inflate(R.layout.fragment_menulist, container, false);

		MainFragment_Dashboard.isBackpressed = false;

		try {

			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashInHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashInHand.setTypeface(LoginActivity.sTypeface);

			mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashAtBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashAtBank.setTypeface(LoginActivity.sTypeface);

			mHeader = (TextView) rootView.findViewById(R.id.submenuHeaderTextview);
			mHeader.setVisibility(View.VISIBLE);
			mHeader.setText(
					RegionalConversion.getRegionalConversion(AppStrings.mSavings_Banktransaction));
			mHeader.setTypeface(LoginActivity.sTypeface);
			
			if (ConnectionUtils.isNetworkAvailable(getActivity())) {
				depositMenu = new String[] { RegionalConversion.getRegionalConversion(AppStrings.bankTransaction),
						RegionalConversion.getRegionalConversion(AppStrings.fixedDeposit),
						RegionalConversion.getRegionalConversion(AppStrings.mAccountToAccountTransfer) };
			} else {
				depositMenu = new String[] { RegionalConversion.getRegionalConversion(AppStrings.bankTransaction),
						RegionalConversion.getRegionalConversion(AppStrings.fixedDeposit),
						RegionalConversion.getRegionalConversion(AppStrings.mAccountToAccountTransfer) };
			}
			System.out.println("Size of deposit menu :" + depositMenu.length);
			size = depositMenu.length;

			listItems = new ArrayList<ListItem>();
			mListView = (ListView) rootView.findViewById(R.id.fragment_List);
			listImage = R.drawable.ic_navigate_next_white_24dp;

			for (int i = 0; i < size; i++) {
				ListItem rowItem = new ListItem(depositMenu[i], listImage);
				listItems.add(rowItem);
			}

			System.out.println("List if deposit Menus :" + listItems.toString());

			mAdapter = new CustomListAdapter(getActivity(), listItems);
			mListView.setAdapter(mAdapter);
			mListView.setOnItemClickListener(this);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return rootView;
	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub

		mProgressDialog = AppDialogUtils.createProgressDialog(getActivity());
		mProgressDialog.show();

	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub
		if (mProgressDialog != null) {

			mProgressDialog.dismiss();

			if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {
				getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub

						if (!result.equals("FAIL")) {

							TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg,
									TastyToast.LENGTH_SHORT, TastyToast.ERROR);
							Constants.NETWORKCOMMONFLAG = "SUCCESS";
						}

					}
				});
			} else {

				if (Boolean.valueOf(isFixedDepositEntry)) {

					EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GET_FIXED_OS,
							new GroupMasterSingle(SelectedGroupsTask.Group_Id));
					isFixedDepositEntry = false;
				} else if (Boolean.valueOf(isAccToAccTransfer)) {
					Transaction_AcctoAccTransferFragment acctoAccTransferFragment = new Transaction_AcctoAccTransferFragment();
					setFragment(acctoAccTransferFragment);
					isAccToAccTransfer = false;
				}
			}
		}

	}

	@Subscribe
	public void OnGroupMasterFixedResponse(final GroupFixedOSResponse groupFixedOSResponse) {
		switch (groupFixedOSResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), groupFixedOSResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), groupFixedOSResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:
			if (GetGroupMemberDetails.getFixeddepositOS() != null
					&& !GetGroupMemberDetails.getFixeddepositOS().equals("")) {

				Log.e("Fixed Deposit Values---->>>>", GetGroupMemberDetails.getFixeddepositOS());

				String mFixedLoanOSResponse = GetGroupMemberDetails.getFixeddepositOS();
				String tempSelectedBankName = Transaction_BankDepositFragment.sSendToServer_BankName;
				response = mFixedLoanOSResponse.split("%");

				for (int i = 0; i < response.length; i++) {
					System.out.println("Response : " + response[i]);

					String secondResponse[] = response[i].split("~");
					System.out.println("LOAN BANK NAME : " + secondResponse[0]);
					System.out.println("BankDepositFragment BANK NAME : " + tempSelectedBankName);

					if (tempSelectedBankName.equals(secondResponse[2])) {

						mFixedOS_Offlineresponse = secondResponse[1];
						mFD_BankId_Offline = secondResponse[2];
						Fixed_Deposit_BalanceTask.sFD_BalanceTask_ServiceResponse = response[i];

					}

				}

				Transaction_FixedDepositEntryFragment fragment = new Transaction_FixedDepositEntryFragment();
				setFragment(fragment);

			} else {

				TastyToast.makeText(getActivity(), AppStrings.loginAgainAlert, TastyToast.LENGTH_LONG,
						TastyToast.WARNING);
			}
			break;
		}

	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		SBus.INST.unRegister(this);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		// TODO Auto-generated method stub

		TextView textColor_Change = (TextView) view.findViewById(R.id.dynamicText);
		textColor_Change.setText(String.valueOf(depositMenu[position]));
		textColor_Change.setTextColor(Color.rgb(251, 161, 108));

		if (String.valueOf(RegionalConversion.getRegionalConversion(AppStrings.bankTransaction))
				.equals(depositMenu[position])) {

			Transaction_FixedDepositEntryFragment.isFixedDepositEntry = false;

			Transaction_DepositEntryFragment fragment = new Transaction_DepositEntryFragment();
			setFragment(fragment);

		} else if (String.valueOf(RegionalConversion.getRegionalConversion(AppStrings.fixedDeposit))
				.equals(depositMenu[position])) {
			if (ConnectionUtils.isNetworkAvailable(getActivity())) {

				try {
					new Fixed_Deposit_BalanceTask(this).execute();
				} catch (Exception e) {
					// TODO: handle exception
					System.out.println("Get_FixedDeposit_BalanceTask Exception" + e.toString());
				}
				isFixedDepositEntry = true;
			} else {
				Log.i(Transaction_DepositEntryFragment.class.getName(), "NetWork Is not available");
				if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
					EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GET_FIXED_OS,
							new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));
				} else {

					TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
							TastyToast.ERROR);
				}
			}
			isFixedDepositEntry = true;
		} else if (String.valueOf(RegionalConversion.getRegionalConversion(AppStrings.mAccountToAccountTransfer))
				.equals(depositMenu[position])) {

			if (ConnectionUtils.isNetworkAvailable(getActivity())) {

				if (EShaktiApplication.getLoginFlag().equals(Constants.ONLINEFLAG)) {

					if (PrefUtils.getGroupMasterResValues() != null
							&& PrefUtils.getGroupMasterResValues().equals("1")) {
						EShaktiApplication.getInstance().getTransactionManager().startTransaction(
								DataType.GET_BANKACCOUNT_NUMBER,
								new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));
					} else {
						try {
							new Get_sb_account_number_webservices(Transaction_DepositMenuFragment.this).execute();

							isAccToAccTransfer = true;
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}

					}

				} else {

					TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
							TastyToast.ERROR);
				}

			} else {
				if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {

					EShaktiApplication.getInstance().getTransactionManager().startTransaction(
							DataType.GET_BANKACCOUNT_NUMBER,
							new GroupMasterSingle(EShaktiApplication.getGetsinglegrpId()));
				} else {

					TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
							TastyToast.ERROR);
				}

			}
		}

	}

	@Subscribe
	public void OnGroupMasterBankAccNoDetailsResponse(final GetBankAccountNoResponse getBanlAccNoResponse) {
		switch (getBanlAccNoResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), getBanlAccNoResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), getBanlAccNoResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:
			String mGetBankAccNoResponse = GetGroupMemberDetails.getBankAccDetails();
			Log.e("Offline get BankAccountNo response ", mGetBankAccNoResponse + "");

			if (mGetBankAccNoResponse != null && !mGetBankAccNoResponse.equals("")) {
				String[] arrValues = mGetBankAccNoResponse.split("#");

				mBankName.clear();
				mBankNameSendtoServer.clear();
				mBankAccNo.clear();

				for (int i = 0; i < arrValues.length; i++) {
					String ValuesSplit = arrValues[i];

					String[] arrValuesSplit = ValuesSplit.split("~");

					if (arrValuesSplit.length != 0 && arrValuesSplit.length == 3) {
						mBankName.add(arrValuesSplit[0]);
						mBankNameSendtoServer.add(arrValuesSplit[1]);
						mBankAccNo.add(arrValuesSplit[2]);

					}

				}

				for (int i = 0; i < mBankName.size(); i++) {
					if (EShaktiApplication.getAcctoaccSendtoserverBank().equals(mBankNameSendtoServer.get(i))) {
						publicValues.mGetSbAccountNumber = mBankAccNo.get(i).toString();
					}
				}

				Transaction_AcctoAccTransferFragment acctoAccTransferFragment = new Transaction_AcctoAccTransferFragment();
				setFragment(acctoAccTransferFragment);
				isAccToAccTransfer = false;
			} else {

				TastyToast.makeText(getActivity(), AppStrings.loginAgainAlert, TastyToast.LENGTH_LONG,
						TastyToast.WARNING);
			}
			break;
		}

	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub
		Log.e("Current Class Name!!!!!!!!!!!!!!", fragment.getClass().getName());
		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

	}

}
