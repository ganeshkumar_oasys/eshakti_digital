package com.yesteam.eshakti.view.fragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.squareup.otto.Subscribe;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.adapter.CustomListAdapter;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.model.ListItem;
import com.yesteam.eshakti.model.RowItem;
import com.yesteam.eshakti.sqlite.db.model.Transaction;
import com.yesteam.eshakti.sqlite.db.transactions.TransactionManager.DataType;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

public class OfflineReports_TransactionList extends Fragment implements
		OnItemClickListener {

	private TextView mGroupName, mCashInHand, mCashAtBank, mHeadertext;
	List<RowItem> rowItems;

	int size;
	String transactionItems[];
	String[] listItem;

	public static String selectedItem;

	public static int[] sumAmount;
	String amt;

	public static String[] principal, interest;
	public static int[] sumPrincipal, sumInterest;

	private ListView mListView;
	private List<ListItem> listItems;
	private CustomListAdapter mAdapter;
	int listImage;

	public OfflineReports_TransactionList() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		SBus.INST.register(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.offline_transactionlist,
				container, false);

		MainFragment_Dashboard.isBackpressed = false;

		try {

			EShaktiApplication.setOfflineTransDate(false);
			EShaktiApplication.getInstance().getTransactionManager()
					.startTransaction(DataType.GET_TRANS_MASTERVALUES, null);

			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String
					.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashInHand.setText(RegionalConversion.getRegionalConversion(String
					.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashInHand.setTypeface(LoginActivity.sTypeface);

			mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashAtBank.setText(RegionalConversion.getRegionalConversion(String
					.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashAtBank.setTypeface(LoginActivity.sTypeface);

			mHeadertext = (TextView) rootView
					.findViewById(R.id.offline_headertext);
			mHeadertext.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.offlineReports)));
			mHeadertext.setTypeface(LoginActivity.sTypeface);

			listItems = new ArrayList<ListItem>();
			mListView = (ListView) rootView.findViewById(R.id.fragment_List);
			listImage = R.drawable.ic_navigate_next_white_24dp;

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return rootView;
	}

	@Subscribe
	public void OnValues(final ArrayList<Transaction> arrayList) {
		Log.e("CHECKING !23", "YES DONE4545");
		String itemList = "";
		if (arrayList.size() != 0) {
			Log.v("Current Array List Size", arrayList.size() + "");
			try {
				for (int i = 0; i < arrayList.size(); i++) {

					if (Offline_ReportDateListFragment.sSelectedTransDate
							.equals(arrayList.get(i).getTransactionDate())) {
						if (arrayList.get(i).getSavings() != null
								|| arrayList.get(i).getVolunteerSavings() != null) {
							itemList = itemList + AppStrings.savings + "~";
						}
						if (arrayList.get(i).getMemberloanRepayment() != null
								|| arrayList.get(i).getPersonalloanRepayment() != null) {
							itemList = itemList
									+ AppStrings.memberloanrepayment + "~";
						}
						if (arrayList.get(i).getExpenses() != null) {
							itemList = itemList + AppStrings.expenses + "~";
						}
						if (arrayList.get(i).getOtherIncome() != null
								|| arrayList.get(i).getSubscriptionIncome() != null
								|| arrayList.get(i).getPenaltyIncome() != null
								|| arrayList.get(i).getDonationIncome() != null
								|| arrayList.get(i).getFederationIncome() != null
								|| arrayList.get(i).getSeedFund() != null) {
							itemList = itemList + AppStrings.income + "~";
						}
						if (arrayList.get(i).getGrouploanrepayment() != null) {
							itemList = itemList + AppStrings.grouploanrepayment
									+ "~";
						}
						if (arrayList.get(i).getBankDeposit() != null
								|| arrayList.get(i).getBankFixedDeposit() != null
								|| arrayList.get(i).getLoanWithdrawal() != null
								|| arrayList.get(i).getAcctoAccTransfer() != null
								|| arrayList.get(i).getAccToLoanAccTransfer() != null) {
							itemList = itemList + AppStrings.bankTransaction
									+ "~";
						}
						if (arrayList.get(i).getPersonalloandisbursement() != null) {
							itemList = itemList
									+ AppStrings.InternalLoanDisbursement + "~";
						}
						if (arrayList.get(i).getAttendance() != null
								|| arrayList.get(i).getMinutesofmeeting() != null
								|| arrayList.get(i).getAuditting() != null
								|| arrayList.get(i).getTraining() != null) {
							itemList = itemList + AppStrings.meeting + "~";
						}

					}
				}

				listItem = itemList.split("~");

				List<String> list = Arrays.asList(listItem);
				Set<String> set = new LinkedHashSet<String>(list);
				transactionItems = new String[set.size()];
				set.toArray(transactionItems);

				for (int j = 0; j < transactionItems.length; j++) {
					System.out
							.println("----------TRANSACTION LIST ITEMS---------"
									+ transactionItems[j].toString()
									+ " i pos " + j);
				}

				size = transactionItems.length;

				for (int i = 0; i < size; i++) {
					ListItem rowItem = new ListItem(
							RegionalConversion.getRegionalConversion(transactionItems[i]
									.toString()), listImage);
					listItems.add(rowItem);
				}
				System.out.println("ROW ITEM " + listItems.size());

				mAdapter = new CustomListAdapter(getActivity(), listItems);
				mListView.setAdapter(mAdapter);
				mListView.setOnItemClickListener(this);

			} catch (Exception e) {
				e.printStackTrace();
			}

		} else {
			TastyToast.makeText(getActivity(), "There is no Local DB Values", TastyToast.LENGTH_SHORT, TastyToast.WARNING);
		}
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		SBus.INST.unRegister(this);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub

		TextView textColor_Change = (TextView) view
				.findViewById(R.id.dynamicText);
		textColor_Change.setText(String.valueOf(transactionItems[position]));
		textColor_Change.setTextColor(Color.rgb(251, 161, 108));

		selectedItem = transactionItems[position].toString();

		System.out.println("SELECTED ITEM  :" + selectedItem);
		if (selectedItem.equals(AppStrings.savings)
				|| selectedItem.equals(AppStrings.memberloanrepayment)
				|| selectedItem.equals(AppStrings.income)
				|| selectedItem.equals(AppStrings.bankTransaction)
				|| selectedItem.equals(AppStrings.meeting)
				|| selectedItem.equals(AppStrings.grouploanrepayment)) {
			Fragment fragment = new Offline_SubMenuList();
			setFragment(fragment);
		} else if (selectedItem.equals(AppStrings.expenses)
				|| selectedItem.equals(AppStrings.InternalLoanDisbursement)) {
			Fragment fragment = new Offline_TransactionDateFragment();
			setFragment(fragment);
		}
	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub
		Log.e("Current Class Name!!!!!!!!!!!!!!", fragment.getClass().getName());
		getActivity()
				.getSupportFragmentManager()
				.beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0,
						R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

	}
}
