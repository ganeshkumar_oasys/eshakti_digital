package com.yesteam.eshakti.view.fragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.squareup.otto.Subscribe;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.TransactionOffConstants;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;
import com.yesteam.eshakti.sqlite.database.GroupProvider;
import com.yesteam.eshakti.sqlite.database.TransactionProvider;
import com.yesteam.eshakti.sqlite.database.response.GetSingleTransResponse;
import com.yesteam.eshakti.sqlite.database.response.GroupMasUpdateResponse;
import com.yesteam.eshakti.sqlite.database.response.TransactionResponse;
import com.yesteam.eshakti.sqlite.database.response.TransactionUpdateResponse;
import com.yesteam.eshakti.sqlite.db.model.GetTransactionSingle;
import com.yesteam.eshakti.sqlite.db.model.GetTransactionSinglevalues;
import com.yesteam.eshakti.sqlite.db.model.GroupDetailsUpdate;
import com.yesteam.eshakti.sqlite.db.model.GroupResponseUpdate;
import com.yesteam.eshakti.sqlite.db.model.GroupTempDBLastTransDate;
import com.yesteam.eshakti.sqlite.db.model.Transaction;
import com.yesteam.eshakti.sqlite.db.model.TransactionUpdate;
import com.yesteam.eshakti.sqlite.db.model.Transaction_UniqueIdSingle;
import com.yesteam.eshakti.sqlite.db.transactions.TransactionManager.DataType;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.utils.GetOfflineTransactionValues;
import com.yesteam.eshakti.utils.GetResponseInfo;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.utils.Get_EdiText_Filter;
import com.yesteam.eshakti.utils.Get_Offline_MobileDate;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.utils.Put_DB_GroupNameDetail_Response;
import com.yesteam.eshakti.utils.Put_DB_GroupResponse;
import com.yesteam.eshakti.utils.Reset;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.utils.UpdateCalendarMinMaxDateUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.Get_DonationTask;
import com.yesteam.eshakti.webservices.Get_FederationIncomeTask;
import com.yesteam.eshakti.webservices.Get_LastTransactionID;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Transaction_Donation_FIncomeFragment extends Fragment implements OnClickListener, TaskListener {

	public static String TAG = Transaction_Donation_FIncomeFragment.class.getSimpleName();

	private TextView mGroupName, mCashinHand, mCashatBank, mHeader;
	private Button mSubmit_Raised_Button, mEdit_RaisedButton, mOk_RaisedButton;

	public static String sSend_To_Server_Donation_FIncome;
	private Dialog mProgressDilaog;

	EditText amountEdit;
	Dialog confirmationDialog;

	String mLastTrDate = null, mLastTr_ID = null;
	boolean isGetTrid = false;
	boolean isServiceCall = false;
	String mSqliteDBStoredValues_Donation = null;

	public Transaction_Donation_FIncomeFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_DONATION_FINCOME;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		SBus.INST.register(this);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		sSend_To_Server_Donation_FIncome = Reset.reset(sSend_To_Server_Donation_FIncome);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_doantion_fincome, container, false);

		MainFragment_Dashboard.isBackpressed = false;
		Constants.FRAG_BACKPRESS_CONSTANT = Constants.FRAG_INSTANCE_DONATION_FINCOME;

		
		mSqliteDBStoredValues_Donation = null;
		try {

			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashinHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashinHand.setTypeface(LoginActivity.sTypeface);

			mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashatBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashatBank.setTypeface(LoginActivity.sTypeface);

			mHeader = (TextView) rootView.findViewById(R.id.fragment_header);
			mHeader.setText(
					RegionalConversion.getRegionalConversion(Transaction_IncomeMenuFragment.sSelectedIncomeMenu));
			mHeader.setTypeface(LoginActivity.sTypeface);

			TextView labelText = (TextView) rootView.findViewById(R.id.fragment_label_text);
			labelText.setText(
					RegionalConversion.getRegionalConversion(Transaction_IncomeMenuFragment.sSelectedIncomeMenu));
			labelText.setTypeface(LoginActivity.sTypeface);
			labelText.setTextColor(color.black);

			amountEdit = (EditText) rootView.findViewById(R.id.fragment_amount_editText);
			amountEdit.setInputType(InputType.TYPE_CLASS_NUMBER);
			amountEdit.setPadding(5, 5, 5, 5);
			amountEdit.setWidth(150);
			amountEdit.setHeight(60);
			amountEdit.setFilters(Get_EdiText_Filter.editText_filter());
			amountEdit.setGravity(Gravity.RIGHT);

			mSubmit_Raised_Button = (Button) rootView.findViewById(R.id.fragment_Submit_button);
			mSubmit_Raised_Button.setText(RegionalConversion.getRegionalConversion(AppStrings.submit));
			mSubmit_Raised_Button.setTypeface(LoginActivity.sTypeface);
			mSubmit_Raised_Button.setOnClickListener(this);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return rootView;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {
		case R.id.fragment_Submit_button:

			sSend_To_Server_Donation_FIncome = Reset.reset(sSend_To_Server_Donation_FIncome);
			sSend_To_Server_Donation_FIncome = String.valueOf(amountEdit.getText());
			System.out.println("Donation & Income Value : " + sSend_To_Server_Donation_FIncome);

			if ((sSend_To_Server_Donation_FIncome.equals("")) || (sSend_To_Server_Donation_FIncome == null)) {
				sSend_To_Server_Donation_FIncome = "0";
			}

			if (sSend_To_Server_Donation_FIncome.matches("\\d*\\.?\\d+")) { // match
																			// a
																			// decimal
																			// number

				int amount = (int) Math.round(Double.parseDouble(sSend_To_Server_Donation_FIncome));
				sSend_To_Server_Donation_FIncome = String.valueOf(amount);
			}

			Log.e(TAG, sSend_To_Server_Donation_FIncome);
			// ((sSend_To_Server_Donation_FIncome.equals(" "))
			// || (sSend_To_Server_Donation_FIncome != null)) &&
			if ((!sSend_To_Server_Donation_FIncome.equals("0"))) {

				confirmationDialog = new Dialog(getActivity());

				LayoutInflater inflater = getActivity().getLayoutInflater();
				View dialogView = inflater.inflate(R.layout.dialog_confirmation, null);
				dialogView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
						LayoutParams.WRAP_CONTENT));

				TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
				confirmationHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.confirmation));
				confirmationHeader.setTypeface(LoginActivity.sTypeface);

				TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

				TableRow indv_SavingsRow = new TableRow(getActivity());

				TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
						LayoutParams.WRAP_CONTENT, 1f);
				contentParams.setMargins(10, 5, 10, 5);

				TextView lableItem = new TextView(getActivity());
				lableItem.setText(GetSpanText.getSpanString(getActivity(),
						String.valueOf(Transaction_IncomeMenuFragment.sSelectedIncomeMenu)));
				lableItem.setTypeface(LoginActivity.sTypeface);
				lableItem.setTextColor(color.black);
				lableItem.setPadding(5, 5, 5, 5);
				lableItem.setLayoutParams(contentParams);
				indv_SavingsRow.addView(lableItem);

				TextView confirm_values = new TextView(getActivity());
				confirm_values.setText(
						GetSpanText.getSpanString(getActivity(), String.valueOf(sSend_To_Server_Donation_FIncome)));
				confirm_values.setTextColor(color.black);
				confirm_values.setPadding(5, 5, 15, 5);
				confirm_values.setGravity(Gravity.RIGHT);
				confirm_values.setLayoutParams(contentParams);
				indv_SavingsRow.addView(confirm_values);

				confirmationTable.addView(indv_SavingsRow,
						new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

				mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit_button);
				mEdit_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.edit));
				mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
				mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
																		// 205,
																		// 0));
				mEdit_RaisedButton.setOnClickListener(this);

				mOk_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Ok_button);
				mOk_RaisedButton.setText(RegionalConversion.getRegionalConversion(AppStrings.yes));
				mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
				mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
				mOk_RaisedButton.setOnClickListener(this);

				confirmationDialog.getWindow()
						.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
				confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				confirmationDialog.setCanceledOnTouchOutside(false);
				confirmationDialog.setContentView(dialogView);
				confirmationDialog.setCancelable(true);
				confirmationDialog.show();

				MarginLayoutParams margin = (MarginLayoutParams) dialogView.getLayoutParams();
				margin.leftMargin = 10;
				margin.rightMargin = 10;
				margin.topMargin = 10;
				margin.bottomMargin = 10;
				margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);

			} else {
				sSend_To_Server_Donation_FIncome = Reset.reset(sSend_To_Server_Donation_FIncome);
				TastyToast.makeText(getActivity(), AppStrings.nullAlert, TastyToast.LENGTH_SHORT, TastyToast.WARNING);
			}

			break;

		case R.id.fragment_Edit_button:
			sSend_To_Server_Donation_FIncome = Reset.reset(sSend_To_Server_Donation_FIncome);
			isServiceCall = false;
			confirmationDialog.dismiss();
			break;

		case R.id.fragment_Ok_button:

			if (ConnectionUtils.isNetworkAvailable(getActivity())) {

				if (Transaction_IncomeMenuFragment.sSelectedIncomeMenu.equals(AppStrings.donation)) {

					try {
						if (!isServiceCall) {
							isServiceCall = true;

							new Get_DonationTask(Transaction_Donation_FIncomeFragment.this).execute();
						}
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
				} else if (Transaction_IncomeMenuFragment.sSelectedIncomeMenu.equals(AppStrings.federationincome)) {

					try {

						new Get_FederationIncomeTask(Transaction_Donation_FIncomeFragment.this).execute();

					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			} else {
				// Do offline Stuffs
				if (EShaktiApplication.getLoginFlag().equals(Constants.OFFLINEFLAG)) {
					if (Transaction_IncomeMenuFragment.sSelectedIncomeMenu.equals(AppStrings.donation)) {
						EShaktiApplication.getInstance().getTransactionManager().startTransaction(
								DataType.GET_TRANS_SINGLE, new GetTransactionSingle(PrefUtils.getOfflineUniqueID()));

					} else if (Transaction_IncomeMenuFragment.sSelectedIncomeMenu.equals(AppStrings.federationincome)) {
						EShaktiApplication.getInstance().getTransactionManager().startTransaction(
								DataType.GET_TRANS_SINGLE, new GetTransactionSingle(PrefUtils.getOfflineUniqueID()));

					}
				} else {
					TastyToast.makeText(getActivity(), AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT,
							TastyToast.ERROR);
				}

			}
			break;

		default:
			break;
		}

	}

	@Subscribe
	public void OnGetSingleTransaction(final GetSingleTransResponse getSingleTransResponse) {
		switch (getSingleTransResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), getSingleTransResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), getSingleTransResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			try {
				String mUniqueId = GetTransactionSinglevalues.getUniqueId();
				String mDFIncomeValues = null, mTrasactiondate = null, mMobileDate = null, mTransaction_UniqueId = null,
						mDonationincomevalues = null, mFederationincomevalues = null;

				mLastTrDate = DatePickerDialog.sDashboardDate;
				mLastTr_ID = SelectedGroupsTask.sTr_ID.toString();

				String mCurrentTransDate = null;

				if (publicValues.mOffline_Trans_CurrentDate != null) {
					mCurrentTransDate = publicValues.mOffline_Trans_CurrentDate;
				}

				if (Transaction_IncomeMenuFragment.sSelectedIncomeMenu.equals(AppStrings.donation)) {
					mDFIncomeValues = GetTransactionSinglevalues.getDonationIncome();

					mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
					mMobileDate = Get_Offline_MobileDate.getOffline_MobileDate(getActivity());
					mTransaction_UniqueId = PrefUtils.getOfflineUniqueID();
					mDonationincomevalues = sSend_To_Server_Donation_FIncome + "#" + mTrasactiondate + "#" + mMobileDate
							+ "$";
				} else if (Transaction_IncomeMenuFragment.sSelectedIncomeMenu.equals(AppStrings.federationincome)) {
					mDFIncomeValues = GetTransactionSinglevalues.getFederationIncome();
					mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
					mMobileDate = Get_Offline_MobileDate.getOffline_MobileDate(getActivity());
					mTransaction_UniqueId = PrefUtils.getOfflineUniqueID();
					mFederationincomevalues = sSend_To_Server_Donation_FIncome + "#" + mTrasactiondate + "#" + mMobileDate;

					Log.e("Fragment Saving Values", mFederationincomevalues);
				}
				System.out.println("Donation Values:::::::::" + mDFIncomeValues);

				if (Transaction_IncomeMenuFragment.sSelectedIncomeMenu.equals(AppStrings.donation)) {
					mFederationincomevalues = "null";
				} else if (Transaction_IncomeMenuFragment.sSelectedIncomeMenu.equals(AppStrings.federationincome)) {
					mDonationincomevalues = "null";
				}

				if (!mDonationincomevalues.contains("?") && !mFederationincomevalues.contains("?")
						&& !mCurrentTransDate.contains("?") && !mTransaction_UniqueId.contains("?")) {

					mSqliteDBStoredValues_Donation = mDonationincomevalues;

					if (mUniqueId == null && mDFIncomeValues == null) {

						Log.v("ContentValues", "Step 3");
						if (mLastTrDate != null && mMobileDate != null && mTransaction_UniqueId != null
								&& Transaction_IncomeMenuFragment.sSelectedIncomeMenu.equals(AppStrings.donation)
								&& mCurrentTransDate != null) {
							EShaktiApplication.getInstance().getTransactionManager().startTransaction(
									DataType.TRANSACTIONADD,
									new Transaction(0, PrefUtils.getUsernameKey(), SelectedGroupsTask.UserName,
											SelectedGroupsTask.Ngo_Id, SelectedGroupsTask.Group_Id, mTransaction_UniqueId,
											mLastTr_ID, mCurrentTransDate, null, null, null, null, null, null, null, null,
											mDonationincomevalues, null, null, null, null, null, null, null, null, null,
											null, null, null, null));
						} else if (mLastTrDate != null && mMobileDate != null && mTransaction_UniqueId != null
								&& Transaction_IncomeMenuFragment.sSelectedIncomeMenu.equals(AppStrings.federationincome)
								&& mCurrentTransDate != null) {

							EShaktiApplication.getInstance().getTransactionManager().startTransaction(
									DataType.TRANSACTIONADD,
									new Transaction(0, PrefUtils.getUsernameKey(), SelectedGroupsTask.UserName,
											SelectedGroupsTask.Ngo_Id, SelectedGroupsTask.Group_Id, mTransaction_UniqueId,
											mLastTr_ID, mCurrentTransDate, null, null, null, null, null, null, null, null,
											null, mFederationincomevalues, null, null, null, null, null, null, null, null,
											null, null, null, null));

						}

					} else if (mUniqueId.equalsIgnoreCase(PrefUtils.getOfflineUniqueID()) && mDFIncomeValues == null) {
						Log.v("ContentValues", "Step 4");

						if (Transaction_IncomeMenuFragment.sSelectedIncomeMenu.equals(AppStrings.donation)) {
							EShaktiApplication.setSetTransValues(TransactionOffConstants.TRANS_DON);

							EShaktiApplication.getInstance().getTransactionManager().startTransaction(
									DataType.TRANS_VALUES_UPDATE, new TransactionUpdate(mUniqueId, mDonationincomevalues));

						} else if (Transaction_IncomeMenuFragment.sSelectedIncomeMenu.equals(AppStrings.federationincome)
								&& mDFIncomeValues == null) {
							EShaktiApplication.setSetTransValues(TransactionOffConstants.TRANS_FED);

							EShaktiApplication.getInstance().getTransactionManager().startTransaction(
									DataType.TRANS_VALUES_UPDATE,
									new TransactionUpdate(mUniqueId, mFederationincomevalues));
						}

					} else if ((mDFIncomeValues != null)) {
						confirmationDialog.dismiss();
						TastyToast.makeText(getActivity(), AppStrings.offlineDataAvailable, TastyToast.LENGTH_SHORT,
								TastyToast.WARNING);

						DatePickerDialog.sDashboardDate = SelectedGroupsTask.sLastTransactionDate_Response;
						MainFragment_Dashboard fragment = new MainFragment_Dashboard();
						setFragment(fragment);
					}
				} else {

					if (confirmationDialog.isShowing() && confirmationDialog != null) {
						confirmationDialog.dismiss();
					}

					TastyToast.makeText(getActivity(), "Please Try Again", TastyToast.LENGTH_SHORT, TastyToast.ERROR);

					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		}
	}

	@Subscribe
	public void OnTransUpdate(final TransactionUpdateResponse transactionUpdateResponse) {
		switch (transactionUpdateResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), transactionUpdateResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), transactionUpdateResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			try {
				TransactionProvider
						.getSinlgeGroupMaster_Transaction(new Transaction_UniqueIdSingle(PrefUtils.getOfflineUniqueID()));

				if (GetTransactionSinglevalues.getDonationIncome() != null
						&& GetTransactionSinglevalues.getDonationIncome().equals(mSqliteDBStoredValues_Donation)
						&& !GetTransactionSinglevalues.getDonationIncome().equals("")) {

					confirmationDialog.dismiss();

					String mCashinHand = String.valueOf(Integer.parseInt(SelectedGroupsTask.sCashinHand)
							+ Integer.parseInt(sSend_To_Server_Donation_FIncome));
					String mCashatBank = SelectedGroupsTask.sCashatBank;
					String mBankDetails = GetOfflineTransactionValues.getBankDetails();
					SelectedGroupsTask.sCashinHand = mCashinHand;
					String mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
					SelectedGroupsTask.sLastTransactionDate_Response = mTrasactiondate;
					String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mTrasactiondate, mCashinHand,
							mCashatBank, mBankDetails);
					String mSelectedGroupId = SelectedGroupsTask.Group_Id;
					EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GROUPMASTERUPDATE,
							new GroupDetailsUpdate(mSelectedGroupId, mGroupMasterResponse));

				} else {
					if (confirmationDialog.isShowing() && confirmationDialog != null) {
						confirmationDialog.dismiss();
					}

					TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT, TastyToast.ERROR);

					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);

				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		}
	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub
		mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
		mProgressDilaog.show();
	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub

		if (mProgressDilaog != null) {
			mProgressDilaog.dismiss();
			if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {
				getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub

						if (!result.equals("FAIL")) {
							isServiceCall = false;
							TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg,
									TastyToast.LENGTH_SHORT, TastyToast.ERROR);
							Constants.NETWORKCOMMONFLAG = "SUCCESS";
						}

					}
				});

			} else {
				try {
					if (isGetTrid) {
						isGetTrid = false;

						callOfflineDataUpdate();
					} else {

						if (GetResponseInfo.sResponseUpdate[0].equals("Yes")) {

							new Get_LastTransactionID(Transaction_Donation_FIncomeFragment.this).execute();
							isGetTrid = true;
						} else {

							TastyToast.makeText(getActivity(), AppStrings.transactionFailAlert, TastyToast.LENGTH_SHORT,
									TastyToast.ERROR);
							MainFragment_Dashboard fragment = new MainFragment_Dashboard();
							setFragment(fragment);
						}

						confirmationDialog.dismiss();

					}

				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		}
	}

	private void callOfflineDataUpdate() {
		// TODO Auto-generated method stub
		String mCashinHand = SelectedGroupsTask.sCashinHand;
		String mCashatBank = SelectedGroupsTask.sCashatBank;
		String mBankDetails = GetOfflineTransactionValues.getBankDetails();

		mLastTrDate = SelectedGroupsTask.sLastTransactionDate_Response;

		String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mLastTrDate, mCashinHand, mCashatBank,
				mBankDetails);
		String mSelectedGroupId = SelectedGroupsTask.Group_Id;
		isServiceCall = false;
		EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GROUPMASTERUPDATE,
				new GroupDetailsUpdate(mSelectedGroupId, mGroupMasterResponse));
	}

	@Subscribe
	public void onAddTransactionDFIncome(final TransactionResponse transactionResponse) {
		switch (transactionResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), transactionResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), transactionResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.ERROR);
			break;
		case SUCCESS:

			try {
				TransactionProvider
						.getSinlgeGroupMaster_Transaction(new Transaction_UniqueIdSingle(PrefUtils.getOfflineUniqueID()));

				if (GetTransactionSinglevalues.getDonationIncome() != null
						&& GetTransactionSinglevalues.getDonationIncome().equals(mSqliteDBStoredValues_Donation)
						&& !GetTransactionSinglevalues.getDonationIncome().equals("")) {

					String mCashinHand = String.valueOf(Integer.parseInt(SelectedGroupsTask.sCashinHand)
							+ Integer.parseInt(sSend_To_Server_Donation_FIncome));
					String mCashatBank = SelectedGroupsTask.sCashatBank;
					String mBankDetails = GetOfflineTransactionValues.getBankDetails();
					SelectedGroupsTask.sCashinHand = mCashinHand;
					String mTrasactiondate = DatePickerDialog.sSend_To_Server_Date;
					SelectedGroupsTask.sLastTransactionDate_Response = mTrasactiondate;
					String mGroupMasterResponse = Put_DB_GroupResponse.put_DB_GroupResponse(mTrasactiondate, mCashinHand,
							mCashatBank, mBankDetails);
					String mSelectedGroupId = SelectedGroupsTask.Group_Id;
					EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GROUPMASTERUPDATE,
							new GroupDetailsUpdate(mSelectedGroupId, mGroupMasterResponse));

				} else {
					if (confirmationDialog.isShowing() && confirmationDialog != null) {
						confirmationDialog.dismiss();
					}

					TastyToast.makeText(getActivity(), AppStrings.tryLater, TastyToast.LENGTH_SHORT, TastyToast.ERROR);

					MainFragment_Dashboard fragment = new MainFragment_Dashboard();
					setFragment(fragment);

				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		}
	}

	@Subscribe
	public void OnGroupMasDFIncomeUpdate(final GroupMasUpdateResponse masterResponse) {
		switch (masterResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(getActivity(), masterResponse.getFetcherResult().getMessage(), TastyToast.LENGTH_SHORT,
					TastyToast.ERROR);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(getActivity(), masterResponse.getFetcherResult().getMessage(), TastyToast.LENGTH_SHORT,
					TastyToast.ERROR);
			break;
		case SUCCESS:

			confirmationDialog.dismiss();
			try {

				TastyToast.makeText(getActivity(), AppStrings.transactionCompleted, TastyToast.LENGTH_SHORT,
						TastyToast.SUCCESS);

				if (!ConnectionUtils.isNetworkAvailable(getActivity())) {

					String mValues = Put_DB_GroupNameDetail_Response
							.put_DB_GroupNameDetails_Response(EShaktiApplication.getGroupResponse());

					GroupProvider.updateGroupResponse(
							new GroupResponseUpdate(EShaktiApplication.getGroupId_GroupLastTransDate(), mValues));
				}

				MainFragment_Dashboard fragment = new MainFragment_Dashboard();
				setFragment(fragment);

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		}
	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub

		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();
		
		FragmentDrawer.drawer_CashinHand.setText(RegionalConversion.getRegionalConversion(AppStrings.cashinhand)

				+ SelectedGroupsTask.sCashinHand);
		FragmentDrawer.drawer_CashinHand.setTypeface(LoginActivity.sTypeface);

		 
		FragmentDrawer.drawer_CashatBank.setText(RegionalConversion.getRegionalConversion(AppStrings.cashatBank)

				+ SelectedGroupsTask.sCashatBank);
		FragmentDrawer.drawer_CashatBank.setTypeface(LoginActivity.sTypeface);
		
		FragmentDrawer.drawer_lastTR_Date.setText(RegionalConversion.getRegionalConversion(AppStrings.lastTransactionDate)
				+ " : " + SelectedGroupsTask.sLastTransactionDate_Response);
		FragmentDrawer.drawer_lastTR_Date.setTypeface(LoginActivity.sTypeface);
		
		UpdateCalendarMinMaxDateUtils.OnUpdateCalendarDate();
		
		Calendar calender = Calendar.getInstance();

		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String formattedDate = df.format(calender.getTime());
		EShaktiApplication.setLastTransDate_GroupId(SelectedGroupsTask.Group_Id);

		new GroupTempDBLastTransDate(EShaktiApplication.getLastTransDate_GroupId(),
				SelectedGroupsTask.sLastTransactionDate_Response, formattedDate);

		GroupProvider.updateGroupLastTransDateResponse();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		SBus.INST.unRegister(this);
	}
}
