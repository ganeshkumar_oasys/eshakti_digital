package com.yesteam.eshakti.view.fragment;

import java.util.ArrayList;
import java.util.List;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.adapter.CustomListAdapter;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.model.ListItem;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

public class Transaction_Limit_Loan_SB_MenuFragment extends Fragment implements OnItemClickListener {

	public static String TAG = Transaction_IncomeMenuFragment.class.getSimpleName();

	private TextView mGroupName, mCashinHand, mCashatBank;
	public static String sSelectedIncomeMenu = null;

	String[] mLoanMenu;

	private ListView mListView;
	private List<ListItem> listItems;
	private CustomListAdapter mAdapter;
	int listImage;
	private TextView mHeader;
	boolean isCashCredit = false;

	public Transaction_Limit_Loan_SB_MenuFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_LOAN_DISBURSE_LIMIT_LOAN_SB;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		isCashCredit = false;
		if (EShaktiApplication.isIsLoanDisBurseRepaid()) {
			mLoanMenu = new String[] { AppStrings.mIncreaseLimit, AppStrings.mLoanDisbursementFromLoanAcc,
					AppStrings.mLoanDisbursementFromSbAcc, AppStrings.mLoanDisbursementFromRepaid };
			EShaktiApplication.setIsLoanDisBurseRepaid(false);
			isCashCredit = true;
		} else {
			mLoanMenu = new String[] { AppStrings.mLoanDisbursementFromLoanAcc, AppStrings.mLoanDisbursementFromSbAcc };
		}

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_menulist, container, false);

		MainFragment_Dashboard.isBackpressed = false;

		try {

			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashinHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashinHand.setTypeface(LoginActivity.sTypeface);

			mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashatBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashatBank.setTypeface(LoginActivity.sTypeface);

			mHeader = (TextView) rootView.findViewById(R.id.submenuHeaderTextview);
			mHeader.setVisibility(View.VISIBLE);
			mHeader.setText(RegionalConversion.getRegionalConversion(EShaktiApplication.getLoanName()));
			mHeader.setTypeface(LoginActivity.sTypeface);

			listItems = new ArrayList<ListItem>();
			mListView = (ListView) rootView.findViewById(R.id.fragment_List);
			listImage = R.drawable.ic_navigate_next_white_24dp;

			for (int i = 0; i < mLoanMenu.length; i++) {
				ListItem rowItem = new ListItem(mLoanMenu[i].toString(), listImage);
				listItems.add(rowItem);
			}

			mAdapter = new CustomListAdapter(getActivity(), listItems);
			mListView.setAdapter(mAdapter);
			mListView.setOnItemClickListener(this);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return rootView;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		// TODO Auto-generated method stub

		TextView textColor_Change = (TextView) view.findViewById(R.id.dynamicText);
		textColor_Change.setText(String.valueOf(mLoanMenu[position]));
		textColor_Change.setTextColor(Color.rgb(251, 161, 108));

		sSelectedIncomeMenu = String.valueOf(mLoanMenu[position]);

		if (isCashCredit) {

			if (position == 0) {
				Transaction_IncreaseLimit_Fragment increaseLimit_Fragment = new Transaction_IncreaseLimit_Fragment();
				setFragment(increaseLimit_Fragment);
			} else if (position == 1) {
				EShaktiApplication.setLoanDisburseValues("LOANACC");
				Transaction_Loan_disburse_LoanAccFragment disburse_LoanAccFragment = new Transaction_Loan_disburse_LoanAccFragment();
				setFragment(disburse_LoanAccFragment);
			} else if (position == 2) {
				EShaktiApplication.setLoanDisburseValues("SBACC");
				Transaction_Sb_Loan_acc_disburseFragment loan_acc_disburseFragment = new Transaction_Sb_Loan_acc_disburseFragment();
				setFragment(loan_acc_disburseFragment);
			} else if (position == 3) {
				EShaktiApplication.setLoanDisburseValues("REPAID");
				Transaction_LoanDisbursementFromRepaidFragment transaction_LoanDisbursementFromRepaidFragment = new Transaction_LoanDisbursementFromRepaidFragment();
				setFragment(transaction_LoanDisbursementFromRepaidFragment);

			}
		} else {
			if (position == 0) {
				EShaktiApplication.setLoanDisburseValues("LOANACC");
				Transaction_Loan_disburse_LoanAccFragment disburse_LoanAccFragment = new Transaction_Loan_disburse_LoanAccFragment();
				setFragment(disburse_LoanAccFragment);
			} else if (position == 1) {
				EShaktiApplication.setLoanDisburseValues("SBACC");
				Transaction_Sb_Loan_acc_disburseFragment loan_acc_disburseFragment = new Transaction_Sb_Loan_acc_disburseFragment();
				setFragment(loan_acc_disburseFragment);
			}

		}
	}

	private void setFragment(Fragment fragment) {
		// TODO Auto-generated method stub

		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();

	}

}