package com.yesteam.eshakti.view.activity;

import com.oasys.eshakti.digitization.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;

public class ExitActivity extends Activity {

	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.activity_exit);

		try {

			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub

					/*
					 * MediaPlayer mediaPlayer =
					 * MediaPlayer.create(ExitActivity.this, R.raw.thankyou);
					 * mediaPlayer.start();
					 */

				}
			}, 100);

			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub

					Intent intent = new Intent(ExitActivity.this, SplashScreenActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					//intent.putExtra("EXIT", true);
					startActivity(intent);

				}
			}, 1500);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
