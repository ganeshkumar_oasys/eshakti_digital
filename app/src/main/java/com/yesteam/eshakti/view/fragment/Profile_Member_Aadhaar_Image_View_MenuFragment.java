package com.yesteam.eshakti.view.fragment;

import java.util.ArrayList;
import java.util.List;

import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.adapter.CustomListAdapter;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.model.ListItem;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.Member_AadhaarCard_View_Task;
import com.yesteam.eshakti.webservices.Member_View_Image_Task;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

public class Profile_Member_Aadhaar_Image_View_MenuFragment extends Fragment implements OnItemClickListener, TaskListener {

	private TextView mGroupName, mCashinHand, mCashatBank,mMemberName;

	private List<ListItem> listItems;
	private ListView mListView;
	private CustomListAdapter mAdapter;
	int listImage;

	String memberDeatilsMenu[];
	Fragment fragment;
	private Dialog mProgressDialog;
	public static String mServiceResponse;
	boolean isNavigateAadhaarCard = false;

	public Profile_Member_Aadhaar_Image_View_MenuFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_AADHAAR_IMAGE_VIEW_MENU;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		memberDeatilsMenu = new String[] { RegionalConversion.getRegionalConversion(AppStrings.viewAadhar),
				RegionalConversion.getRegionalConversion(AppStrings.viewPhoto) };
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View rootView = inflater.inflate(R.layout.fragment_aadhaar_image_view_menu, container, false);
		try {

			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mMemberName = (TextView) rootView.findViewById(R.id.memberName);
			mMemberName.setText(EShaktiApplication.getSelectedMemberName());
			mMemberName.setTypeface(LoginActivity.sTypeface);
			mMemberName.setVisibility(View.VISIBLE);
			
			mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashinHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashinHand.setTypeface(LoginActivity.sTypeface);

			mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashatBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashatBank.setTypeface(LoginActivity.sTypeface);

			int mSize = 2;

			listItems = new ArrayList<ListItem>();
			mListView = (ListView) rootView.findViewById(R.id.fragment_view_menu_List);
			listImage = R.drawable.ic_navigate_next_white_24dp;

			for (int i = 0; i < mSize; i++) {
				ListItem item = new ListItem(memberDeatilsMenu[i], listImage);
				listItems.add(item);
			}

			System.out.println("ROW ITEM " + listItems.size());

			mAdapter = new CustomListAdapter(getActivity(), listItems);
			mListView.setAdapter(mAdapter);
			mListView.setOnItemClickListener(this);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return rootView;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		TextView textColor_Change = (TextView) view.findViewById(R.id.dynamicText);
		textColor_Change.setText(String.valueOf(memberDeatilsMenu[position]));
		textColor_Change.setTextColor(Color.rgb(251, 161, 108));

		Log.d("MENU", "Menu Item " + memberDeatilsMenu[position]);
		switch (position) {
		case 0:
			new Member_AadhaarCard_View_Task(this).execute();
			isNavigateAadhaarCard = true;

			break;
		case 1:
			isNavigateAadhaarCard = false;
			new Member_View_Image_Task(this).execute();

			break;
		default:
			break;

		}

	}

	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub
		mProgressDialog = AppDialogUtils.createProgressDialog(getActivity());
		mProgressDialog.show();
	}

	@Override
	public void onTaskFinished(final String result) {
		// TODO Auto-generated method stub
		mProgressDialog.dismiss();

		if (result.equals(Constants.EXCEPTION) || result.equals("FAIL")) {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub

					if (!result.equals("FAIL")) {

						TastyToast.makeText(getActivity(), AppStrings.mCommonNetworkErrorMsg, TastyToast.LENGTH_SHORT,
								TastyToast.ERROR);
						Constants.NETWORKCOMMONFLAG = "SUCCESS";
					}

				}
			});
		} else {
			try {
				if (mServiceResponse != null) {

					if (!isNavigateAadhaarCard) {

						Profile_Member_View_Image_Fragment fragment = new Profile_Member_View_Image_Fragment();
						setFragment(fragment);
					} else if (isNavigateAadhaarCard) {
						if (mServiceResponse.equals("Not available aadhaar information.")) {
							TastyToast.makeText(getActivity(), AppStrings.mIsAadharAvailable, TastyToast.LENGTH_SHORT,
									TastyToast.ERROR);
							MemberList_Fragment fragment = new MemberList_Fragment();
							setFragment(fragment);
						} else {
							Profile_Member_View_AadhaarCard_Fragment fragment = new Profile_Member_View_AadhaarCard_Fragment();
							setFragment(fragment);
						}
					}
				} else {
					TastyToast.makeText(getActivity(), AppStrings.uploadprofilealert, TastyToast.LENGTH_SHORT,
							TastyToast.WARNING);
					MemberList_Fragment fragment = new MemberList_Fragment();
					setFragment(fragment);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public void setFragment(Fragment fragment) {

		getActivity().getSupportFragmentManager().beginTransaction()

				.replace(R.id.frame, fragment)
				.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out).show(fragment)
				.addToBackStack(fragment.getClass().getName()).commit();
	}
}
