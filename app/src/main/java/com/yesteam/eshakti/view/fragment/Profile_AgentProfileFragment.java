package com.yesteam.eshakti.view.fragment;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.Get_AgentProfileTask;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;


public class Profile_AgentProfileFragment extends Fragment {
	private TextView mGroupName, mCashInHand, mCashAtBank, mHeadertext;
	String responseArr[];

	public Profile_AgentProfileFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_GROUP_AGENT;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		try {

			responseArr = Get_AgentProfileTask.sGetAgentProfile_Response
					.split("~");
			for (int i = 0; i < responseArr.length; i = i + 2) {
				System.out.println("ODD : " + responseArr[i] + "pos:" + i);

				System.out.println("EVEN " + responseArr[i + 1] + "pos:"
						+ (i + 1));
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_agent_group_profile,
				container, false);

		MainFragment_Dashboard.isBackpressed = false;
		Constants.FRAG_BACKPRESS_CONSTANT = Constants.FRAG_INSTANCE_GROUP_AGENT;

		try {
			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String
					.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashInHand.setText(RegionalConversion.getRegionalConversion(String
					.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashInHand.setTypeface(LoginActivity.sTypeface);

			mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashAtBank.setText(RegionalConversion.getRegionalConversion(String
					.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashAtBank.setTypeface(LoginActivity.sTypeface);

			mHeadertext = (TextView) rootView
					.findViewById(R.id.fragment_agent_group_profile_headertext);
			mHeadertext.setText(RegionalConversion
					.getRegionalConversion(AppStrings.agentProfile));
			mHeadertext.setTypeface(LoginActivity.sTypeface);

			// System.out.println("Length of response:"+Get_AgentProfileTask.sAgentProfile_Response.toString());

			// String
			// responseArr[]=Get_AgentProfileTask.sAgentProfile_Response.split("~");

			System.out.println("Length of response:" + responseArr.length);

			TableLayout tableLayout = (TableLayout) rootView
					.findViewById(R.id.fragment_agent_group_profile_tableLayout);

			for (int i = 0; i < responseArr.length; i = i + 2) {

				TableRow indv_row = new TableRow(getActivity());
				TableRow.LayoutParams contentParams = new TableRow.LayoutParams(
						LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1f);
				contentParams.setMargins(10, 5, 10, 5);

				TextView agent_left_details = new TextView(getActivity());
				agent_left_details.setText(GetSpanText.getSpanString(
						getActivity(), responseArr[i]));
				agent_left_details.setTypeface(LoginActivity.sTypeface);
				agent_left_details.setTextColor(color.black);
				agent_left_details.setPadding(5, 5, 5, 5);
				agent_left_details.setLayoutParams(contentParams);
				indv_row.addView(agent_left_details);

				TextView agent_right_details = new TextView(getActivity());
				agent_right_details.setText(GetSpanText.getSpanString(
						getActivity(), responseArr[i + 1]));
				agent_right_details.setTypeface(LoginActivity.sTypeface);
				agent_right_details.setTextColor(color.black);
				agent_right_details.setPadding(5, 5, 5, 5);
				agent_right_details.setLayoutParams(contentParams);
				indv_row.addView(agent_right_details);

				tableLayout.addView(indv_row);/*, new TableLayout.LayoutParams(
						LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));*/

			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return rootView;
	}

}
