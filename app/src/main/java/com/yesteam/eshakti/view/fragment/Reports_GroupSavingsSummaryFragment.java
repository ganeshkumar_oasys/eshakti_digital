package com.yesteam.eshakti.view.fragment;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.utils.GetSpanText;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.views.CustomHorizontalScrollView;
import com.yesteam.eshakti.views.CustomHorizontalScrollView.onScrollChangedListener;
import com.yesteam.eshakti.webservices.Get_GroupSavingsSummaryTask;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Reports_GroupSavingsSummaryFragment extends Fragment {

	private TextView mGroupName, mCashinHand, mCashatBank, mHeader;
	private TableLayout mGroupSavingsTable;
	int size;
	String amountsArr[], vSavingsAmountArr[];
	private TableLayout mLeftHeaderTable, mRightHeaderTable, mLeftContentTable, mRightContentTable;
	private CustomHorizontalScrollView mHSRightHeader, mHSRightContent;
	private String mLanguagelocale = "";
	String width[] = { AppStrings.memberName, AppStrings.savingsAmount, AppStrings.voluntarySavings };
	int[] rightHeaderWidth = new int[width.length];
	int[] rightContentWidth = new int[width.length];

	public Reports_GroupSavingsSummaryFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		EShaktiApplication.setOnSavedFragment(true);
		Constants.FRAG_INSTANCE_CONSTANT = Constants.FRAG_INSTANCE_GROUP_REPORTS_SAVING_SUMM;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_groupsavingssummary, container, false);

		MainFragment_Dashboard.isBackpressed = false;

		try {

			mGroupName = (TextView) rootView.findViewById(R.id.groupname);
			mGroupName.setText(RegionalConversion.getRegionalConversion(String.valueOf(SelectedGroupsTask.Group_Name)));
			mGroupName.setTypeface(LoginActivity.sTypeface);

			mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
			mCashinHand.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashinhand))
					+ SelectedGroupsTask.sCashinHand);
			mCashinHand.setTypeface(LoginActivity.sTypeface);

			mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
			mCashatBank.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.cashatBank))
					+ SelectedGroupsTask.sCashatBank);
			mCashatBank.setTypeface(LoginActivity.sTypeface);

			mHeader = (TextView) rootView.findViewById(R.id.fragment_groupsavingssummary_headertext);
			mHeader.setText(RegionalConversion.getRegionalConversion(AppStrings.groupSavingssummary));
			mHeader.setTypeface(LoginActivity.sTypeface);
			
			mLanguagelocale = PrefUtils.getUserlangcode();
			String responseArr[] = Get_GroupSavingsSummaryTask.sGroupSaving_details_response.split("~");

			System.out.println("Length of response :" + responseArr.length);
			String amt = "", vSavingsAmt = "";
			/*for (int i = 0; i < responseArr.length; i++) {
				System.out.println("responseArr[] :" + responseArr[i] + " pos :" + i);
				if (i % 2 != 0) {
					amt = amt + responseArr[i] + "~";
				}
			}*/
			
			for (int i = 0; i < responseArr.length; i=i+3) {
				amt = amt + responseArr[i+1] + "~";
				vSavingsAmt = vSavingsAmt + responseArr[i+2] + "~";
			}
			amountsArr = amt.split("~");
			vSavingsAmountArr = vSavingsAmt.split("~");

			mLeftHeaderTable = (TableLayout) rootView.findViewById(R.id.LeftHeaderTable);
			mRightHeaderTable = (TableLayout) rootView.findViewById(R.id.RightHeaderTable);
			mLeftContentTable = (TableLayout) rootView.findViewById(R.id.LeftContentTable);
			mRightContentTable = (TableLayout) rootView.findViewById(R.id.RightContentTable);

			mHSRightHeader = (CustomHorizontalScrollView) rootView.findViewById(R.id.rightHeaderHScrollView);
			mHSRightContent = (CustomHorizontalScrollView) rootView.findViewById(R.id.rightContentHScrollView);

			mHSRightHeader.setOnScrollChangedListener(new onScrollChangedListener() {

				public void onScrollChanged(int l, int t, int oldl, int oldt) {
					// TODO Auto-generated method stub

					mHSRightContent.scrollTo(l, 0);

				}
			});

			mHSRightContent.setOnScrollChangedListener(new onScrollChangedListener() {
				@Override
				public void onScrollChanged(int l, int t, int oldl, int oldt) {
					// TODO Auto-generated method stub
					mHSRightHeader.scrollTo(l, 0);
				}
			});
			
			
			TableRow leftHeaderRow = new TableRow(getActivity());

			TableRow.LayoutParams lHeaderParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);

			TextView mMemberName_headerText = new TextView(getActivity());
			mMemberName_headerText
					.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.memberName)));
			mMemberName_headerText.setTypeface(LoginActivity.sTypeface);
			mMemberName_headerText.setTextColor(Color.WHITE);
			mMemberName_headerText.setPadding(10, 5, 10, 5);
			mMemberName_headerText.setLayoutParams(lHeaderParams);
			leftHeaderRow.addView(mMemberName_headerText);

			mLeftHeaderTable.addView(leftHeaderRow);

			TableRow rightHeaderRow = new TableRow(getActivity());
			TableRow.LayoutParams rHeaderParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT, 1f);
			rHeaderParams.setMargins(10, 0, 20, 0);
			TextView mSavingsAmount_HeaderText = new TextView(getActivity());
			mSavingsAmount_HeaderText
					.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.savingsAmount)));
			mSavingsAmount_HeaderText.setTypeface(LoginActivity.sTypeface);
			mSavingsAmount_HeaderText.setTextColor(Color.WHITE);
			mSavingsAmount_HeaderText.setPadding(50, 5, 5, 5);
			mSavingsAmount_HeaderText.setLayoutParams(rHeaderParams);
			mSavingsAmount_HeaderText.setGravity(Gravity.CENTER);
			mSavingsAmount_HeaderText.setSingleLine(true);
			rightHeaderRow.addView(mSavingsAmount_HeaderText);

			TextView mVSavingsAmount_HeaderText = new TextView(getActivity());
			mVSavingsAmount_HeaderText
					.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.voluntarySavings)));
			mVSavingsAmount_HeaderText.setTypeface(LoginActivity.sTypeface);
			mVSavingsAmount_HeaderText.setTextColor(Color.WHITE);
			if (mLanguagelocale.equalsIgnoreCase("English")) {
				mVSavingsAmount_HeaderText.setPadding(25, 5, 5, 5);
			} else if (mLanguagelocale.equalsIgnoreCase("Tamil")) {
				mVSavingsAmount_HeaderText.setPadding(5, 5, 25, 5);
			} else if (mLanguagelocale.equalsIgnoreCase("Hindi")) {
				mVSavingsAmount_HeaderText.setPadding(5, 5, 25, 5);
			} else if (mLanguagelocale.equalsIgnoreCase("Marathi")) {
				mVSavingsAmount_HeaderText.setPadding(5, 5, 25, 5);
			} else {
				mVSavingsAmount_HeaderText.setPadding(25, 5, 5, 5);
			}

			mVSavingsAmount_HeaderText.setLayoutParams(rHeaderParams);
			mVSavingsAmount_HeaderText.setSingleLine(true);
			rightHeaderRow.addView(mVSavingsAmount_HeaderText);

			mRightHeaderTable.addView(rightHeaderRow);

			getTableRowHeaderCellWidth();

			for (int i = 0; i < amountsArr.length-1; i++) {
				TableRow leftContentRow = new TableRow(getActivity());

				TableRow.LayoutParams leftContentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT, 40, 1f);
				leftContentParams.setMargins(5, 5, 5, 5);

				final TextView memberName_Text = new TextView(getActivity());
				memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
						String.valueOf(SelectedGroupsTask.member_Name.elementAt(i))));
				memberName_Text.setTypeface(LoginActivity.sTypeface);
				memberName_Text.setTextColor(color.black);
				memberName_Text.setPadding(5, 5, 5, 5);
				memberName_Text.setLayoutParams(leftContentParams);
				leftContentRow.addView(memberName_Text);

				mLeftContentTable.addView(leftContentRow);

				TableRow rightContentRow = new TableRow(getActivity());

				TableRow.LayoutParams rightContentParams = new TableRow.LayoutParams(rightHeaderWidth[1],
						LayoutParams.WRAP_CONTENT, 1f);
				rightContentParams.setMargins(20, 5, 20, 5);

				TextView savingsAmount = new TextView(getActivity());
				savingsAmount.setText(GetSpanText.getSpanString(getActivity(),
						String.valueOf(amountsArr[i])));
				savingsAmount.setTypeface(LoginActivity.sTypeface);
				savingsAmount.setTextColor(color.black);
				savingsAmount.setPadding(5, 5, 50, 5);
				savingsAmount.setGravity(Gravity.RIGHT);
				savingsAmount.setLayoutParams(rightContentParams);
				rightContentRow.addView(savingsAmount);
				
				TextView vSavingsAmount = new TextView(getActivity());
				vSavingsAmount.setText(GetSpanText.getSpanString(getActivity(),
						String.valueOf(vSavingsAmountArr[i])));
				vSavingsAmount.setTypeface(LoginActivity.sTypeface);
				vSavingsAmount.setTextColor(color.black);
				vSavingsAmount.setPadding(5, 5, 30, 5);
				vSavingsAmount.setGravity(Gravity.RIGHT);
				vSavingsAmount.setLayoutParams(rightContentParams);
				rightContentRow.addView(vSavingsAmount);
				
				mRightContentTable.addView(rightContentRow);
				
			}
			
			
			TableRow totalRow = new TableRow(getActivity());

			TableRow.LayoutParams leftContentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT, 40, 1f);
			leftContentParams.setMargins(5, 5, 5, 5);

			final TextView memberName_Text = new TextView(getActivity());
			memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
					String.valueOf(AppStrings.total)));
			memberName_Text.setTypeface(LoginActivity.sTypeface, Typeface.BOLD);
			memberName_Text.setTextColor(Color.BLACK);
			memberName_Text.setPadding(5, 5, 5, 5);
			memberName_Text.setLayoutParams(leftContentParams);
			totalRow.addView(memberName_Text);

			mLeftContentTable.addView(totalRow);

			TableRow totalContentRow = new TableRow(getActivity());

			TableRow.LayoutParams rightContentParams = new TableRow.LayoutParams(rightHeaderWidth[1],
					LayoutParams.WRAP_CONTENT, 1f);
			rightContentParams.setMargins(20, 5, 20, 5);

			TextView savingsAmount_total = new TextView(getActivity());
			savingsAmount_total.setText(GetSpanText.getSpanString(getActivity(),
					String.valueOf(amountsArr[amountsArr.length-1])));
			savingsAmount_total.setTypeface(null, Typeface.BOLD);
			savingsAmount_total.setTextColor(Color.BLACK);
			savingsAmount_total.setPadding(5, 5, 50, 5);
			savingsAmount_total.setGravity(Gravity.RIGHT);
			savingsAmount_total.setLayoutParams(rightContentParams);
			totalContentRow.addView(savingsAmount_total);
			
			TextView vSavingsAmount_total = new TextView(getActivity());
			vSavingsAmount_total.setText(GetSpanText.getSpanString(getActivity(),
					String.valueOf(vSavingsAmountArr[vSavingsAmountArr.length-1])));
			vSavingsAmount_total.setTypeface(null, Typeface.BOLD);
			vSavingsAmount_total.setTextColor(Color.BLACK);
			vSavingsAmount_total.setPadding(5, 5, 30, 5);
			vSavingsAmount_total.setGravity(Gravity.RIGHT);
			vSavingsAmount_total.setLayoutParams(rightContentParams);
			totalContentRow.addView(vSavingsAmount_total);
			
			mRightContentTable.addView(totalContentRow);

			resizeMemberNameWidth();

			resizeBodyTableRowHeight();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return rootView;
	}
	
	private void getTableRowHeaderCellWidth() {

		int lefHeaderChildCount = ((TableRow) mLeftHeaderTable.getChildAt(0)).getChildCount();
		int rightHeaderChildCount = ((TableRow) mRightHeaderTable.getChildAt(0)).getChildCount();

		for (int x = 0; x < (lefHeaderChildCount + rightHeaderChildCount); x++) {

			if (x == 0) {
				rightHeaderWidth[x] = viewWidth(((TableRow) mLeftHeaderTable.getChildAt(0)).getChildAt(x));
			} else {
				rightHeaderWidth[x] = viewWidth(((TableRow) mRightHeaderTable.getChildAt(0)).getChildAt(x - 1));
			}

		}
	}

	private void resizeMemberNameWidth() {
		// TODO Auto-generated method stub
		int leftHeadertWidth = viewWidth(mLeftHeaderTable);
		int leftContentWidth = viewWidth(mLeftContentTable);

		if (leftHeadertWidth < leftContentWidth) {
			mLeftHeaderTable.getLayoutParams().width = leftContentWidth;
		} else {
			mLeftContentTable.getLayoutParams().width = leftHeadertWidth;
		}
	}

	private void resizeBodyTableRowHeight() {

		int leftContentTable_ChildCount = mLeftContentTable.getChildCount();

		for (int x = 0; x < leftContentTable_ChildCount; x++) {

			TableRow leftContentTableRow = (TableRow) mLeftContentTable.getChildAt(x);
			TableRow rightContentTableRow = (TableRow) mRightContentTable.getChildAt(x);

			int rowLeftHeight = viewHeight(leftContentTableRow);
			int rowRightHeight = viewHeight(rightContentTableRow);

			TableRow tableRow = rowLeftHeight < rowRightHeight ? leftContentTableRow : rightContentTableRow;
			int finalHeight = rowLeftHeight > rowRightHeight ? rowLeftHeight : rowRightHeight;

			this.matchLayoutHeight(tableRow, finalHeight);
		}

	}

	private void matchLayoutHeight(TableRow tableRow, int height) {

		int tableRowChildCount = tableRow.getChildCount();

		// if a TableRow has only 1 child
		if (tableRow.getChildCount() == 1) {

			View view = tableRow.getChildAt(0);
			TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();
			params.height = height - (params.bottomMargin + params.topMargin);

			return;
		}

		// if a TableRow has more than 1 child
		for (int x = 0; x < tableRowChildCount; x++) {

			View view = tableRow.getChildAt(x);

			TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();

			if (!isTheHeighestLayout(tableRow, x)) {
				params.height = height - (params.bottomMargin + params.topMargin);
				return;
			}
		}

	}

	// check if the view has the highest height in a TableRow
	private boolean isTheHeighestLayout(TableRow tableRow, int layoutPosition) {

		int tableRowChildCount = tableRow.getChildCount();
		int heighestViewPosition = -1;
		int viewHeight = 0;

		for (int x = 0; x < tableRowChildCount; x++) {
			View view = tableRow.getChildAt(x);
			int height = this.viewHeight(view);

			if (viewHeight < height) {
				heighestViewPosition = x;
				viewHeight = height;
			}
		}

		return heighestViewPosition == layoutPosition;
	}

	// read a view's height
	private int viewHeight(View view) {
		view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
		return view.getMeasuredHeight();
	}

	// read a view's width
	private int viewWidth(View view) {
		view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
		return view.getMeasuredWidth();
	}


}
