package com.yesteam.eshakti.service;

import java.io.IOException;
import java.util.ArrayList;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import com.squareup.otto.Subscribe;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.yesteam.eshakti.aadharcard.AadhaarCard;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.sqlite.database.AadharcardMasterProvider;
import com.yesteam.eshakti.sqlite.database.response.AadharCardGetMasterResponse;
import com.yesteam.eshakti.sqlite.db.model.AadharCardMaster;
import com.yesteam.eshakti.sqlite.db.model.AadharCardMasterUpdate;
import com.yesteam.eshakti.sqlite.db.model.AadharcardDelete;
import com.yesteam.eshakti.sqlite.db.transactions.TransactionManager.DataType;
import com.yesteam.eshakti.utils.CryptographyUtils;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.view.activity.SplashScreenActivity;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

public class MemberAadharCardService extends Service {
	ArrayList<AadharCardMaster> mOfflineTransList = new ArrayList<AadharCardMaster>();
	boolean isStart = false;
	String mUsername;
	String mNgoId;
	String mGroupId;
	String mUniqueId;
	int mNotifiSucessCount = 0;
	int mNotifiFailCount = 0;
	int mTotalOfflineCount = 0;
	String mServiceNamespace, mServiceSoapaddress;

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onStart(Intent intent, int startId) {
		// TODO Auto-generated method stub
		super.onStart(intent, startId);
		SBus.INST.register(this);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		Log.e("Aadhar Card Servicesssssssssss", "-------------------------->>>>>>");
		if (!isStart) {

			if (PrefUtils.getServicenamespace() != null) {
				mServiceNamespace = PrefUtils.getServicenamespace();
				mServiceSoapaddress = mServiceNamespace + "service.asmx";

			}
			EShaktiApplication.setOfflineAadharcardPhot(true);
			EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.AADHARCARDMASTERGET,
					null);
		}
		return super.onStartCommand(intent, flags, startId);
	}

	@Subscribe
	public void OnGetAadharCardResponse(final AadharCardGetMasterResponse aadharCardGetMasterResponse) {
		switch (aadharCardGetMasterResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(this, aadharCardGetMasterResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.WARNING);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(this, aadharCardGetMasterResponse.getFetcherResult().getMessage(),
					TastyToast.LENGTH_SHORT, TastyToast.WARNING);
			break;
		case SUCCESS:

			if (EShaktiApplication.isOfflineAadharcardPhot()) {
				mOfflineTransList = (ArrayList<AadharCardMaster>) AadharcardMasterProvider.aadharCardMasters;

				Log.e("Aadhar Card Valuessssssssssssssss-------->>", mOfflineTransList.size() + "");

				if (mOfflineTransList != null && mOfflineTransList.size() > 0 && mOfflineTransList.size() != 0) {

					if (mOfflineTransList.get(0).getUsername() != null) {

						mUsername = mOfflineTransList.get(0).getUsername();
						mNgoId = mOfflineTransList.get(0).getNgoId();
						new OfflineAadharCardAysncTask().execute();
						isStart = true;
						mTotalOfflineCount = mOfflineTransList.size();
					} else {
						mTotalOfflineCount = 0;

						isStart = false;
						Intent intentservice = new Intent(MemberAadharCardService.this, MemberAadharCardService.class);
						stopService(intentservice);

						Intent intentservice_aadharCard = new Intent(MemberAadharCardService.this,
								MemberPhotoUploadService.class);
						startService(intentservice_aadharCard);
						stopSelf();

					}

				} else {

					mTotalOfflineCount = 0;

					isStart = false;
					Intent intentservice = new Intent(MemberAadharCardService.this, MemberAadharCardService.class);
					stopService(intentservice);

					Intent intentservice_aadharCard = new Intent(MemberAadharCardService.this,
							MemberPhotoUploadService.class);
					startService(intentservice_aadharCard);
					stopSelf();

				}
			}

			break;
		}

	}

	public class OfflineAadharCardAysncTask extends AsyncTask<String, String, String> {

		String sMember_Aadhar_Info_Response = null;
		String mAadhaarCardValues;
		AadhaarCard newCard;

		String mAadhaarCardDetails;

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub

			SoapObject request = new SoapObject(mServiceNamespace,
					SOAP_OperationConstants.OPERATION_NAME_MEMBER_AADHAR_INFO);
			Log.i("Yes Checking ", "Do in back Ground");
			int i;
			for (i = 0; i < mOfflineTransList.size(); i++) {

				if (mOfflineTransList.get(i).getUpdateValues().equals("0")) {

					try {
						request.addProperty("User_Name",
								CryptographyUtils.Encrypt(mOfflineTransList.get(i).getUsername()));
						request.addProperty("Ngo_id", CryptographyUtils.Encrypt(mOfflineTransList.get(i).getNgoId()));
						request.addProperty("Group_id",
								CryptographyUtils.Encrypt(mOfflineTransList.get(i).getGroupId()));
						request.addProperty("Mem_id",
								CryptographyUtils.Encrypt(mOfflineTransList.get(i).getMemberId()));
						request.addProperty("Values",
								CryptographyUtils.Encrypt(mOfflineTransList.get(i).getMasterValues()));

					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
					envelope.setOutputSoapObject(request);
					envelope.dotNet = true;

					HttpTransportSE httpSe = new HttpTransportSE(mServiceSoapaddress);

					try {
						httpSe.call(mServiceNamespace + SOAP_OperationConstants.OPERATION_NAME_MEMBER_AADHAR_INFO,
								envelope);

						Object result = envelope.getResponse();

						sMember_Aadhar_Info_Response = CryptographyUtils.Decrypt(String.valueOf(result));
						Log.e("Valuessssssssssssssss" + i, sMember_Aadhar_Info_Response);

						if (sMember_Aadhar_Info_Response.equals("Yes")) {
							mNotifiSucessCount++;
							AadharCardMasterUpdate.setMasterUniqueId(mOfflineTransList.get(i).getUniqueid());
							AadharCardMasterUpdate.setMasterUpdateValues("1");
							EShaktiApplication.getInstance().getTransactionManager()
									.startTransaction(DataType.MEMBERAADHARCARDUPDATE, null);
							Thread.sleep(1000);
							EShaktiApplication.getInstance().getTransactionManager().startTransaction(
									DataType.AADHARCARDMASTERDELETE,
									new AadharcardDelete(mOfflineTransList.get(i).getUniqueid()));

							Thread.sleep(500);
						} else {
							mNotifiFailCount++;
						}

						Constants.NETWORKCOMMONFLAG = "SUCCESS";

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();

						Constants.NETWORKCOMMONFLAG = "FAIL";
					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();

						Constants.NETWORKCOMMONFLAG = "FAIL";
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();

						Constants.NETWORKCOMMONFLAG = "FAIL";
					}
				} else {
					Log.e("AadharCard Upload Sucess", "-------------->>>>");
				}
			}
			return Constants.NETWORKCOMMONFLAG;// sMember_Aadhar_Info_Response;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub

		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			isStart = false;
			Intent intentservice = new Intent(MemberAadharCardService.this, MemberAadharCardService.class);
			stopService(intentservice);

			stopSelf();

			Intent intentservice_aadharCard = new Intent(MemberAadharCardService.this, MemberPhotoUploadService.class);
			startService(intentservice_aadharCard);
			Log.v("Service Stopped", "Yes Service Stopped");
			try {
				createNotification();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();

		SBus.INST.unRegister(this);
	}

	public void createNotification() {
		try {
			// Prepare intent which is triggered if the
			// notification is selected
			Intent intent = new Intent(this, SplashScreenActivity.class);
			PendingIntent pIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, 0);
			String mSucessCount, mFailCount, mTotalCount;
			mSucessCount = String.valueOf(mNotifiSucessCount);
			mFailCount = String.valueOf(mNotifiFailCount);
			mTotalCount = String.valueOf(mTotalOfflineCount);

			// Build notification
			// Actions are just fake
			Notification noti = new Notification.Builder(this).setContentTitle(" MEMBER AADHAR CARD UPLOADED ")
					.setContentText(
							"TOTAL : " + mTotalCount + " SUCESS : " + mSucessCount + "   PENDING :" + mFailCount)
					.setSmallIcon(R.drawable.lauchericon)

					.setContentIntent(pIntent).build();
			noti.sound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.welcometone);
			noti.defaults = Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE;
			NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
			// hide the notification after its selected
			noti.flags |= Notification.FLAG_AUTO_CANCEL;

			notificationManager.notify(0, noti);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
