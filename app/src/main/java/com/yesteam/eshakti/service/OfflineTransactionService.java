package com.yesteam.eshakti.service;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.ArrayList;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import com.squareup.otto.Subscribe;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.sqlite.db.model.GroupMasterSingle;
import com.yesteam.eshakti.sqlite.db.model.TransDelete;
import com.yesteam.eshakti.sqlite.db.model.TransIdUpdate;
import com.yesteam.eshakti.sqlite.db.model.Transaction;
import com.yesteam.eshakti.sqlite.db.transactions.TransactionManager.DataType;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.yesteam.eshakti.utils.GPSTracker;
import com.yesteam.eshakti.utils.Get_IMEIandModel;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.view.activity.SplashScreenActivity;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

public class OfflineTransactionService extends Service {
	ArrayList<Transaction> mOfflineTransList = new ArrayList<Transaction>();
	String mGroupTransId;
	String mUsername;
	String mNgoId;
	String mGroupId;
	String mUniqueId;
	String mTransactionId;
	String mTransactionDate;
	String mSavings;
	String mVolunteerSavings;
	String mPersonalloanRepayment;
	String mMemberloanRepayment;
	String mExpenses;
	String mOtherIncome;
	String mSubscriptionIncome;
	String mPenaltyIncome;
	String mDonationIncome;
	String mFederationIncome;
	String mGrouploanrepayment;
	String mBankDeposit;
	String mBankFixedDeposit;
	String mPersonalloandisbursement;
	String mAttendance;
	String mMinutesofmeeting;
	String mAuditting;
	String mTraining;
	String mLoanaccTransfer;
	String mAccount_to_AccountTransfer;
	String mAccount_to_LoanAccountTransfer;
	String mSeedFund;

	String mNullValue = "##";
	String mNullValueSelected = "###";

	String mBankDepositValues, mIncomeValues;
	String mValuesCon = "!";
	GPSTracker gps;

	double latitude = 0.0;
	double longitude = 0.0;

	String mUpdateTransId, mUniqueId_Online, mUpdateGroupId, mUpdateSucessStatus;
	boolean isStart = false;
	String mServiceNamespace, mServiceSoapaddress;
	int i;
	int mNotifiSucessCount = 0;
	int mNotifiFailCount = 0;
	int mTotalOfflineCount = 0;

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onStart(Intent intent, int startId) {
		// TODO Auto-generated method stub
		super.onStart(intent, startId);
		SBus.INST.register(this);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.e("Service Started Create", "Started");
		gps = new GPSTracker(OfflineTransactionService.this);
		if (!isStart) {
			EShaktiApplication.setOfflineTrans(true);
			EShaktiApplication.setOfflineServiceRunning(true);
			EShaktiApplication.setOfflineServiceCall(true);
			EShaktiApplication.getInstance().getTransactionManager()
					.startTransaction(DataType.GET_OFFLINE_MASTER_TRANSACTION, null);
		}

		return super.onStartCommand(intent, flags, startId);
	}

	@Subscribe
	public void onGroupNameResponse(ArrayList<Transaction> mOfflineTransList_) {
		Log.e("Offline Transaction Valuessssssssss", mOfflineTransList_.size() + "");
		if (EShaktiApplication.isOfflineTrans() && EShaktiApplication.isOfflineServiceCall()) {

			if (mOfflineTransList_ != null && mOfflineTransList_.size() > 0 && mOfflineTransList_.size() != 0) {
				mOfflineTransList = mOfflineTransList_;
				if (mOfflineTransList.get(0).getUsername() != null) {
					mUsername = mOfflineTransList.get(0).getUsername();
					mNgoId = mOfflineTransList.get(0).getNgoId();
					new OfflineTransactionAsyncTask().execute();
					isStart = true;
					mTotalOfflineCount = mOfflineTransList.size();
				} else {
					mTotalOfflineCount = 0;
					System.out.println("Offline UserName___>>>" + mOfflineTransList.get(0).getUsername());
					EShaktiApplication.setOfflineTrans(false);
					EShaktiApplication.setOfflineServiceCall(false);
					isStart = false;
					if (AppDialogUtils.mProgressDialog != null && AppDialogUtils.mProgressDialog.isShowing()) {

						AppDialogUtils.mProgressDialog.cancel();
					}
					Intent intentservice = new Intent(OfflineTransactionService.this, OfflineTransactionService.class);
					stopService(intentservice);

					stopSelf();

					Intent intentservice_aadharCard = new Intent(OfflineTransactionService.this,
							MemberAadharCardService.class);
					startService(intentservice_aadharCard);
					Log.v("Service Stopped", "Yes Service Stopped");
				}

			} else {

				mTotalOfflineCount = 0;

				EShaktiApplication.setOfflineTrans(false);
				EShaktiApplication.setOfflineServiceCall(false);
				isStart = false;
				if (AppDialogUtils.mProgressDialog != null && AppDialogUtils.mProgressDialog.isShowing()) {

					AppDialogUtils.mProgressDialog.cancel();
				}
				Intent intentservice = new Intent(OfflineTransactionService.this, OfflineTransactionService.class);
				stopService(intentservice);

				stopSelf();

				Intent intentservice_aadharCard = new Intent(OfflineTransactionService.this,
						MemberAadharCardService.class);
				startService(intentservice_aadharCard);

			}
		}
		EShaktiApplication.setOfflineServiceRunning(false);
	}

	public class OfflineTransactionAsyncTask extends AsyncTask<Void, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

		}

		@Override
		protected String doInBackground(Void... params) {
			String response = "";
			Log.v("Offline Arrayb size", mOfflineTransList.size() + "");
			for (i = 0; i < mOfflineTransList.size(); i++) {

				mGroupId = mOfflineTransList.get(i).getGroupId();

				try {
					EShaktiApplication.getInstance().getTransactionManager()
							.startTransaction(DataType.GET_MASTER_TRANSID, new GroupMasterSingle(mGroupId));
					Thread.sleep(300);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				mUniqueId = mOfflineTransList.get(i).getUniqueId();

				mSavings = mOfflineTransList.get(i).getSavings();
				mVolunteerSavings = mOfflineTransList.get(i).getVolunteerSavings();
				mMemberloanRepayment = mOfflineTransList.get(i).getMemberloanRepayment();
				mPersonalloanRepayment = mOfflineTransList.get(i).getPersonalloanRepayment();
				mExpenses = mOfflineTransList.get(i).getExpenses();
				mOtherIncome = mOfflineTransList.get(i).getOtherIncome();
				mSubscriptionIncome = mOfflineTransList.get(i).getSubscriptionIncome();
				mPenaltyIncome = mOfflineTransList.get(i).getPenaltyIncome();
				mDonationIncome = mOfflineTransList.get(i).getDonationIncome();
				mFederationIncome = mOfflineTransList.get(i).getFederationIncome();
				mGrouploanrepayment = mOfflineTransList.get(i).getGrouploanrepayment();
				mPersonalloandisbursement = mOfflineTransList.get(i).getPersonalloandisbursement();
				mBankDeposit = mOfflineTransList.get(i).getBankDeposit();
				mBankFixedDeposit = mOfflineTransList.get(i).getBankFixedDeposit();
				mMinutesofmeeting = mOfflineTransList.get(i).getMinutesofmeeting();
				mAttendance = mOfflineTransList.get(i).getAttendance();
				mAuditting = mOfflineTransList.get(i).getAuditting();
				mTraining = mOfflineTransList.get(i).getTraining();
				mLoanaccTransfer = mOfflineTransList.get(i).getLoanWithdrawal();
				mAccount_to_AccountTransfer = mOfflineTransList.get(i).getAcctoAccTransfer();
				mAccount_to_LoanAccountTransfer = mOfflineTransList.get(i).getAccToLoanAccTransfer();
				mSeedFund = mOfflineTransList.get(i).getSeedFund();

				if (mSavings == null) {
					mSavings = mNullValue;

				}
				if (mVolunteerSavings == null) {
					mVolunteerSavings = mNullValue;

				}
				if (mPersonalloanRepayment == null) {
					mPersonalloanRepayment = mNullValueSelected;

				}

				if (mMemberloanRepayment == null) {
					mMemberloanRepayment = mNullValueSelected;

				}

				if (mExpenses == null) {
					mExpenses = mNullValue;

				}
				if (mOtherIncome == null) {
					mOtherIncome = mNullValue + "$";

				}
				if (mSubscriptionIncome == null) {
					mSubscriptionIncome = mNullValue + "$";

				}
				if (mPenaltyIncome == null) {
					mPenaltyIncome = mNullValue + "$";

				}
				if (mDonationIncome == null) {
					mDonationIncome = mNullValue + "$";

				}
				if (mFederationIncome == null) {
					mFederationIncome = mNullValue + "$";

				}

				if (mSeedFund == null) {
					mSeedFund = mNullValue + "$";
				}

				if (mGrouploanrepayment == null) {
					mGrouploanrepayment = mNullValueSelected;

				}
				if (mPersonalloandisbursement == null) {

					mPersonalloandisbursement = mNullValue;

				}
				if (mAttendance == null) {
					mAttendance = mNullValue;

				}
				if (mAuditting == null) {
					mAuditting = mNullValue;

				}
				if (mMinutesofmeeting == null) {
					mMinutesofmeeting = mNullValue;

				}
				if (mTraining == null) {
					mTraining = mNullValue;

				}
				if (mLoanaccTransfer == null) {
					mLoanaccTransfer = mNullValue;
				}
				if (mAccount_to_AccountTransfer == null) {
					mAccount_to_AccountTransfer = mNullValue;
				}
				if (mAccount_to_LoanAccountTransfer == null) {
					mAccount_to_LoanAccountTransfer = mNullValue;
				}
				if (mBankDeposit == null && mBankFixedDeposit == null) {
					mBankDepositValues = mNullValueSelected;

				} else if (mBankDeposit == null) {
					mBankDepositValues = "$" + mBankFixedDeposit;

				} else if (mBankFixedDeposit == null) {
					mBankDepositValues = mBankDeposit + "$";

				} else if (mBankDeposit != null && mBankFixedDeposit != null) {
					mBankDepositValues = mBankDeposit + "$" + mBankFixedDeposit;
				}

				System.out.println("Bank Deposit" + mBankDeposit);
				System.out.println("Bank Fixed Deposit" + mBankFixedDeposit);
				System.out.println("Total Bank Values" + mBankDepositValues);

				mIncomeValues = mOtherIncome + mSubscriptionIncome + mPenaltyIncome + mDonationIncome
						+ mFederationIncome + mSeedFund;
				uploadOfflineTransaction(mGroupId, mUniqueId, mSavings, mVolunteerSavings, mMemberloanRepayment,
						mPersonalloanRepayment, mIncomeValues, mExpenses, mGrouploanrepayment,
						mPersonalloandisbursement, mBankDepositValues, mMinutesofmeeting, mAttendance, mAuditting,
						mTraining, mLoanaccTransfer, mAccount_to_AccountTransfer, mAccount_to_LoanAccountTransfer);

			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			EShaktiApplication.setOfflineTrans(false);
			EShaktiApplication.setOfflineServiceCall(false);
			isStart = false;
			Intent intentservice = new Intent(OfflineTransactionService.this, OfflineTransactionService.class);
			stopService(intentservice);

			stopSelf();

			Intent intentservice_aadharCard = new Intent(OfflineTransactionService.this, MemberAadharCardService.class);
			startService(intentservice_aadharCard);
			Log.v("Service Stopped", "Yes Service Stopped");

			try {
				createNotification();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@SuppressWarnings("null")
	public String uploadOfflineTransaction(String mGroupid1, String muniqueid1, String mSavings1,
			String mVolunteerSavings2, String mMemberloanRepayment2, String mPersonalloanRepayment2, String mIncome2,
			String mExpenses2, String mGrouploanrepayment2, String mPersonalloandisbursement2, String mBankDeposit2,
			String mMinutesofmeeting2, String mAttendance2, String mAuditting2, String mTraining2, String mLoanAccTrans,
			String mAccountTrans, String mAcc_to_LoanAccTrans) {
		if (PrefUtils.getServicenamespace() != null) {
			mServiceNamespace = PrefUtils.getServicenamespace();
			mServiceSoapaddress = mServiceNamespace + "service.asmx";

		}
		SoapObject request = new SoapObject(mServiceNamespace, SOAP_OperationConstants.OPERATION_NAME_OFFLINE_MODE_NEW);
		System.out.println("Bank DDDDDDDDDDDDDDDD----->>>" + mBankDeposit2);
		Log.e("Personalloan Repaidment", mPersonalloanRepayment2);
		String mValues = mSavings1 + mValuesCon + mPersonalloanRepayment2 + mValuesCon + mMemberloanRepayment2
				+ mValuesCon + mExpenses2 + mValuesCon + mIncome2 + mValuesCon + mGrouploanrepayment2 + mValuesCon
				+ mBankDeposit2 + mValuesCon + mPersonalloandisbursement2 + mValuesCon + mAttendance2 + mValuesCon
				+ mMinutesofmeeting2 + mValuesCon + mAuditting2 + mValuesCon + mTraining2 + mValuesCon
				+ mVolunteerSavings2 + mValuesCon + mLoanAccTrans + mValuesCon + mAccountTrans + mValuesCon
				+ mAcc_to_LoanAccTrans;

		System.out.println("****************************");
		Log.e("Offline service values", mValues.toString() + "");
		System.out.println("****************************");
		String tempTransId = EShaktiApplication.getLastTransId();

		Log.i("Temp IDDDDDDDDD", tempTransId + "::::::  ::: ");
		String mModel_imei = Get_IMEIandModel.getIMEIandModel(getApplicationContext());
		if (EShaktiApplication.getLastTransId() != null) {
			mGroupTransId = EShaktiApplication.getLastTransId();

		}
		if (gps.canGetLocation()) {

			latitude = gps.getLatitude();
			longitude = gps.getLongitude();

		}

		request.addProperty("UserName", mUsername);
		request.addProperty("Ngo_Id", mNgoId);
		request.addProperty("Group_Id", mGroupid1);
		request.addProperty("U_Id", muniqueid1);
		request.addProperty("Tr_Id", mGroupTransId);

		request.addProperty("Values", mValues);
		request.addProperty("Mobile_Model", mModel_imei);
		request.addProperty("ValueFrom", "Android");
		request.addProperty("Latitude", String.valueOf(latitude));
		request.addProperty("Longitude", String.valueOf(longitude));

		System.out.println("Offline request   :" + request.toString() + "");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.setOutputSoapObject(request);
		envelope.dotNet = true;

		HttpTransportSE httpSe = new HttpTransportSE(mServiceSoapaddress);
		Log.v("Soap NameSpace", mServiceNamespace);
		Log.v("Soap Address", mServiceSoapaddress);
		Log.e("Offline Values", mValues);
		Log.e("UserName", mUsername);

		Log.e("NGO Id", mNgoId);

		Log.e("Group Id", mGroupid1);
		Log.e("Unique Id", muniqueid1);
		Log.e("Transaction Id", mGroupTransId);
		Log.e("ValueFrom", "Android");
		Log.e("IMEI Model", mModel_imei);
		try {
			httpSe.call(mServiceNamespace + SOAP_OperationConstants.OPERATION_NAME_OFFLINE_MODE_NEW, envelope);

			Object result = envelope.getResponse();

			String sOffline_ModeTask_Response = String.valueOf(result);

			Log.i("Offline Service Response", sOffline_ModeTask_Response);
			String mArr_Successfull[] = null;

			if (sOffline_ModeTask_Response != null) {
				mArr_Successfull = sOffline_ModeTask_Response.split("~");

				if (mArr_Successfull[0].equals("Transaction Successful")) {

					String mArr[] = sOffline_ModeTask_Response.split("~");
					mUpdateSucessStatus = mArr[0];
					mUniqueId_Online = mArr[1];
					mUpdateTransId = mArr[2];
					mUpdateGroupId = mArr[3];

					Log.e("Transaction ID && Response", sOffline_ModeTask_Response);
					Log.v("Update Unique ID", mUniqueId_Online);
					Log.v("Update Trans Id", mUpdateTransId);
					Log.i("Update Group ID", mUpdateGroupId);
					mNotifiSucessCount++;
					if (mGroupid1.equals(mUpdateGroupId) && muniqueid1.equals(mUniqueId_Online)
							&& mUpdateSucessStatus.equals("Transaction Successful")) {

						EShaktiApplication.getInstance().getTransactionManager().startTransaction(
								DataType.TRANS_ID_UPDATE, new TransIdUpdate(mUpdateGroupId, mUpdateTransId));
						try {
							Thread.sleep(500);
						} catch (Exception e) {
							// TODO: handle exception
						}

					}
					if (muniqueid1.equals(mUniqueId_Online) && mUpdateSucessStatus.equals("Transaction Successful")) {

						EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.TRANS_DELETE,
								new TransDelete(mUniqueId_Online));
						try {
							Thread.sleep(500);
						} catch (Exception e) {
							// TODO: handle exception
						}
					} else if (mUpdateSucessStatus.equals("Transaction Unsuccessful")) {

						String mArr_UnsucessStatus[] = sOffline_ModeTask_Response.split("~");
						mUpdateSucessStatus = mArr_UnsucessStatus[0];
						mUniqueId_Online = mArr_UnsucessStatus[1];
						mUpdateTransId = mArr_UnsucessStatus[2];
						mUpdateGroupId = mArr_UnsucessStatus[3];

					}

				} else if (mArr_Successfull[0].equals("Transaction Unsuccessful")) {
					mNotifiFailCount++;

					String mArr[] = sOffline_ModeTask_Response.split("~");
					mUpdateSucessStatus = mArr[0];
					mUniqueId_Online = mArr[1];
					mUpdateTransId = mArr[2];
					mUpdateGroupId = mArr[3];

					if (mGroupid1.equals(mUpdateGroupId) && muniqueid1.equals(mUniqueId_Online)
							&& mUpdateSucessStatus.equals("Transaction Unsuccessful")) {

						EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.TRANS_ID_UPDATE,
								new TransIdUpdate(mUpdateGroupId, mUpdateTransId));
						try {
							Thread.sleep(500);
						} catch (Exception e) {
							// TODO: handle exception
						}

					}

				}

			} 

		} catch (InterruptedIOException e) {
			// handle timeout here...
			TastyToast.makeText(this, "PLEASE RESTART YOUR NETWORK CONNECTION", TastyToast.LENGTH_SHORT,
					TastyToast.WARNING);
		} catch (IOException e) {
			e.printStackTrace();
			EShaktiApplication.setOfflineTrans(false);
			EShaktiApplication.setOfflineServiceCall(false);
			Intent intentservice = new Intent(this, OfflineTransactionService.class);
			startService(intentservice);
			mNotifiFailCount++;
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "";
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		EShaktiApplication.setOfflineTrans(false);
		EShaktiApplication.setOfflineServiceCall(false);
		SBus.INST.unRegister(this);
	}

	public void createNotification() {
		try {
			// Prepare intent which is triggered if the
			// notification is selected
			Intent intent = new Intent(this, SplashScreenActivity.class);
			PendingIntent pIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, 0);
			String mSucessCount, mFailCount, mTotalCount;
			mSucessCount = String.valueOf(mNotifiSucessCount);
			mFailCount = String.valueOf(mNotifiFailCount);
			mTotalCount = String.valueOf(mTotalOfflineCount);

			mNotifiSucessCount = 0;
			mNotifiFailCount = 0;
			mTotalOfflineCount = 0;
			if (AppDialogUtils.mProgressDialog != null && AppDialogUtils.mProgressDialog.isShowing()) {

				AppDialogUtils.mProgressDialog.cancel();
			}
			// Build notification
			// Actions are just fake
			Notification noti = new Notification.Builder(this).setContentTitle("Your Offline Data")
					.setContentText(
							"TOTAL : " + mTotalCount + " SUCESS : " + mSucessCount + "   PENDING :" + mFailCount)
					.setSmallIcon(R.drawable.lauchericon)

					.setContentIntent(pIntent).build();
			noti.sound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.welcometone);
			noti.defaults = Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE;
			NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
			// hide the notification after its selected
			noti.flags |= Notification.FLAG_AUTO_CANCEL;

			notificationManager.notify(0, noti);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
