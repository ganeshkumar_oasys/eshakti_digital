package com.yesteam.eshakti.service;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.yesteam.eshakti.sqlite.db.model.Groupdetails;
import com.yesteam.eshakti.sqlite.db.transactions.TransactionManager.DataType;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.webservices.Login_webserviceTask;

import android.app.IntentService;
import android.content.Intent;
import android.os.IBinder;

public class GroupDetailsAddService extends IntentService {

	public GroupDetailsAddService() {
		super(GroupDetailsAddService.class.getName());

	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onStart(Intent intent, int startId) {
		// TODO Auto-generated method stub
		super.onStart(intent, startId);
	}

	@Override
	protected void onHandleIntent(Intent intent) {

		if (EShaktiApplication.isAgent) {

			try {

				try {
					Thread.sleep(500);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch
					// block
					e1.printStackTrace();
				}
				final String mGroupDetails = Login_webserviceTask.mViewGroupValues;
				String arr[] = Login_webserviceTask.mViewGroupValues.split("#");

				final String mTrainerId = arr[0];
				final String mNgoId = arr[1];

				try {

					for (int i = 0; i < Login_webserviceTask.sAgent_Gname.size(); i++) {

						String mGroupName = Login_webserviceTask.sAgent_Gname.elementAt(i).toString();
						String mGroupId = Login_webserviceTask.sAgent_GIDs.elementAt(i).toString();
						String mLastTransDate = Login_webserviceTask.sSHG_TransactionDate.elementAt(i).toString();

						EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GROUPNAMEADD,
								new Groupdetails(0, mTrainerId, mNgoId, mGroupId, mGroupName, mGroupDetails, null,
										null));
						Thread.sleep(200);

					}
					if (EShaktiApplication.isIsChangeLanguage()) {
						EShaktiApplication.setIsChangeLanguage(false);
					} else {
						PrefUtils.setGrouplistValues(null);
					}
					Intent intentservice = new Intent(GroupDetailsAddService.this, GroupDetailsAddService.class);
					stopService(intentservice);

					PrefUtils.setAddGroupFlag("1");

					stopSelf();

				} catch (Exception e) {
					// TODO Auto-generated catch
					// block
					e.printStackTrace();
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

			}

		} else {

		}
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
}
