package com.yesteam.eshakti.service;

import java.util.ArrayList;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import com.squareup.otto.Subscribe;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.sqlite.database.MemberPhotoProvider;
import com.yesteam.eshakti.sqlite.database.response.MemberGetPhotoMasterResponse;
import com.yesteam.eshakti.sqlite.db.model.MemberPhotoDelete;
import com.yesteam.eshakti.sqlite.db.model.MemberPhotoMaster;
import com.yesteam.eshakti.sqlite.db.model.MemberPhotoMasterUpdate;
import com.yesteam.eshakti.sqlite.db.transactions.TransactionManager.DataType;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.utils.SBus;
import com.yesteam.eshakti.view.activity.SplashScreenActivity;
import com.yesteam.eshakti.view.fragment.Profile_MemberPhoto_AadharInfoMenuFragment;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Base64;
import android.util.Log;

public class MemberPhotoUploadService extends Service {

	ArrayList<MemberPhotoMaster> mOfflineTransList = new ArrayList<MemberPhotoMaster>();
	boolean isStart = false;
	String mUsername;
	String mNgoId;
	String mGroupId;
	String mUniqueId;
	int mNotifiSucessCount = 0;
	int mNotifiFailCount = 0;
	int mTotalOfflineCount = 0;
	String mServiceNamespace, mServiceSoapaddress;

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onStart(Intent intent, int startId) {
		// TODO Auto-generated method stub
		super.onStart(intent, startId);
		SBus.INST.register(this);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.e("Image------------->>>", "Service@@@@@@@@@@@@@@@@");
		if (!isStart) {

			if (PrefUtils.getServicenamespace() != null) {
				mServiceNamespace = PrefUtils.getServicenamespace();
				mServiceSoapaddress = mServiceNamespace + "service.asmx";

			}
			EShaktiApplication.setOfflineAadharcardPhot(true);

			EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.MEMBERPHOTOUPLOADGET,
					null);
		}
		return super.onStartCommand(intent, flags, startId);
	}

	@Subscribe
	public void OnGetAadharCardResponse(final MemberGetPhotoMasterResponse getPhotoMasterResponse) {
		switch (getPhotoMasterResponse.getFetcherResult()) {
		case FAIL:
			TastyToast.makeText(this, getPhotoMasterResponse.getFetcherResult().getMessage(), TastyToast.LENGTH_SHORT,
					TastyToast.WARNING);
			break;
		case NO_NETWORK_CONNECTION:
			TastyToast.makeText(this, getPhotoMasterResponse.getFetcherResult().getMessage(), TastyToast.LENGTH_SHORT,
					TastyToast.WARNING);
			break;
		case SUCCESS:

			if (EShaktiApplication.isOfflineAadharcardPhot()) {
				mOfflineTransList = (ArrayList<MemberPhotoMaster>) MemberPhotoProvider.memberPhotoMasters;
				Log.e("Offline size valuesssssssssss", mOfflineTransList.size() + "");
				if (mOfflineTransList != null && mOfflineTransList.size() > 0 && mOfflineTransList.size() != 0) {

					if (mOfflineTransList.get(0).getUsername() != null) {
						mUsername = mOfflineTransList.get(0).getUsername();
						mNgoId = mOfflineTransList.get(0).getNgoId();
						new OfflineMemberPhotoAysncTask().execute();
						isStart = true;
						mTotalOfflineCount = mOfflineTransList.size();
					} else {
						mTotalOfflineCount = 0;

						isStart = false;
						Intent intentservice = new Intent(MemberPhotoUploadService.this,
								MemberPhotoUploadService.class);
						stopService(intentservice);

						stopSelf();

					}

				}
			}

			break;
		}

	}

	public class OfflineMemberPhotoAysncTask extends AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(Void... params) {
			uploadImage();
			return Constants.NETWORKCOMMONFLAG;// response;
		}

		@Override
		protected void onPreExecute() {

		}

		@Override
		protected void onPostExecute(String result) {

			super.onPostExecute(result);
			try {
				createNotification();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			isStart = false;
			Intent intentservice = new Intent(MemberPhotoUploadService.this, MemberPhotoUploadService.class);
			stopService(intentservice);

			stopSelf();

		}

		public void uploadImage() {

			int i = 0;

			for (i = 0; i < mOfflineTransList.size(); i++) {
				if (mOfflineTransList.get(i).getUpdateValues().equals("0")) {

					String encodedString = Base64.encodeToString(mOfflineTransList.get(i).getImage(), Base64.NO_WRAP);
					Log.e("-------------------->>>>", mOfflineTransList.get(i).getMemberId());
					String mUsername, mNgoId, mGroupId, mMemberId;
					mUsername = mOfflineTransList.get(i).getUsername();
					mNgoId = mOfflineTransList.get(i).getNgoId();
					mGroupId = mOfflineTransList.get(i).getGroupId();
					mMemberId = mOfflineTransList.get(i).getMemberId();

					SoapObject request = new SoapObject(mServiceNamespace,
							SOAP_OperationConstants.OPERATION_NAME_GET_IMAGE);

					try {
						request.addProperty("UserName", mUsername);
						request.addProperty("Ngo_Id", mNgoId);
						request.addProperty("Group_Id", mGroupId);
						request.addProperty("Mem_Id", mMemberId);
						request.addProperty("image", encodedString);

					} catch (Exception e) {
						e.getStackTrace();
					}
					SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
					envelope.setOutputSoapObject(request);
					envelope.dotNet = true;
					// LoginTask.SOAP_ADDRESS =
					// "http://yshpiaws.yesbooks.in/service.asmx";
					HttpTransportSE androidHttpTransport = new HttpTransportSE(mServiceSoapaddress);

					try {

						androidHttpTransport.call(mServiceNamespace + SOAP_OperationConstants.OPERATION_NAME_GET_IMAGE,
								envelope);

						Object result = envelope.getResponse();

						String sGroupLoginTask_Response = String.valueOf(result);
						Log.i("Image Output String", sGroupLoginTask_Response);

						Profile_MemberPhoto_AadharInfoMenuFragment.mServiceResponse = sGroupLoginTask_Response;
						if (Profile_MemberPhoto_AadharInfoMenuFragment.mServiceResponse
								.equals("UPLOADED SUCCESSFULLY")) {
							mNotifiSucessCount++;
							MemberPhotoMasterUpdate.setMasterUniqueId(mOfflineTransList.get(i).getUniqueid());
							MemberPhotoMasterUpdate.setMasterUpdateValues("1");
							EShaktiApplication.getInstance().getTransactionManager()
									.startTransaction(DataType.MEMBERPHOTOUPDATE, null);
							Thread.sleep(1000);

							EShaktiApplication.getInstance().getTransactionManager().startTransaction(
									DataType.MEMBERPHOTOUPLOADDELETE,
									new MemberPhotoDelete(mOfflineTransList.get(i).getUniqueid()));

							try {
								Thread.sleep(500);
							} catch (Exception e) {
								// TODO: handle exception
							}

						} else {
							mNotifiFailCount++;
						}
						Constants.NETWORKCOMMONFLAG = "SUCCESS";

					} catch (Exception e) {

						e.printStackTrace();

						Constants.NETWORKCOMMONFLAG = "FAIL";
					}
				} else {
					Log.e("Photo Already Upload", "----------------------------->>>>>");
				}
			}

		}

	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();

		SBus.INST.unRegister(this);
	}

	public void createNotification() {
		try {
			// Prepare intent which is triggered if the
			// notification is selected
			Intent intent = new Intent(this, SplashScreenActivity.class);
			PendingIntent pIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, 0);
			String mSucessCount, mFailCount, mTotalCount;
			mSucessCount = String.valueOf(mNotifiSucessCount);
			mFailCount = String.valueOf(mNotifiFailCount);
			mTotalCount = String.valueOf(mTotalOfflineCount);

			// Build notification
			// Actions are just fake
			Notification noti = new Notification.Builder(this).setContentTitle(" MEMBER PHOTO UPLOADED ")
					.setContentText(
							"TOTAL : " + mTotalCount + " SUCESS : " + mSucessCount + "   PENDING :" + mFailCount)
					.setSmallIcon(R.drawable.ic_launcher)

					.setContentIntent(pIntent).build();
			noti.sound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.welcometone);
			noti.defaults = Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE;
			NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
			// hide the notification after its selected
			noti.flags |= Notification.FLAG_AUTO_CANCEL;

			notificationManager.notify(0, noti);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
