package com.yesteam.eshakti.service;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.sqlite.db.model.LoginCheck;
import com.yesteam.eshakti.utils.CryptographyUtils;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.view.activity.GroupListActivity.MyWebRequestReceiver;
import com.yesteam.eshakti.webservices.LoginTask;

import android.app.IntentService;
import android.content.Intent;
import android.text.format.DateFormat;
import android.util.Log;

public class GroupWisePendingReportService extends IntentService {
	public static final String REQUEST_STRING = "myRequest";
	public static final String RESPONSE_STRING = "myResponse";
	public static final String RESPONSE_MESSAGE = "myResponseMessage";
	String requestString = "";
	String responseString = "";
	String responseMessage = "";

	public GroupWisePendingReportService() {
		super(GroupWisePendingReportService.class.getName());

	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub

		String requestString = intent.getStringExtra(REQUEST_STRING);
		String responseString = requestString + " " + DateFormat.format("MM/dd/yy h:mmaa", System.currentTimeMillis());
		String responseMessage = "";
		try {
			callWebServiceMethod();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	SoapObject callWebServiceMethod() throws Exception {
		int TimeOutInSeconds = 1000;
		String url = LoginTask.SOAP_ADDRESS;
		URL myurl = new URL(LoginTask.SOAP_ADDRESS);
		URLConnection connection = myurl.openConnection();
		connection.setConnectTimeout(20 * 1000);
		HttpURLConnection httpConnection = (HttpURLConnection) connection;
		int responseCode = httpConnection.getResponseCode();
		if (responseCode == HttpURLConnection.HTTP_OK) {
			httpConnection.disconnect();
			SoapObject request = new SoapObject(LoginTask.NAMESPACE,
					SOAP_OperationConstants.OPERATION_NAME_GET_ANIMATOR_PENDING_GROUPWISE);

			try {
				if (PrefUtils.getGrouplistValues() != null && PrefUtils.getGrouplistValues().equals("1")) {
					request.addProperty("ngo_id", CryptographyUtils.Encrypt(String.valueOf(LoginCheck.getUserNgoId())));
					request.addProperty("Trainer_Id",
							CryptographyUtils.Encrypt(String.valueOf(LoginCheck.getTrainerId())));
				} else {
					request.addProperty("ngo_id", CryptographyUtils.Encrypt(String.valueOf(LoginTask.Ngo_Id)));
					request.addProperty("Trainer_Id", CryptographyUtils.Encrypt(String.valueOf(LoginTask.Trainer_Id)));

				}

			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;

			Log.e("Input Valuesssssssssss", request.toString().trim());
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(url, TimeOutInSeconds * 1000);

			androidHttpTransport.call(
					LoginTask.NAMESPACE + SOAP_OperationConstants.OPERATION_NAME_GET_ANIMATOR_PENDING_GROUPWISE,
					envelope);

			Object result = envelope.getResponse();
			Log.e("Output String Values--------->", result.toString());
			String Get_All_Info_ServiceResponse = CryptographyUtils.Decrypt(String.valueOf(result));

			publicValues.mGetAnimatorPendingGroupwiseValues = Get_All_Info_ServiceResponse;

			try {
				Intent broadcastIntent = new Intent();
				broadcastIntent.setAction(MyWebRequestReceiver.PROCESS_RESPONSE);
				broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
				broadcastIntent.putExtra(RESPONSE_STRING, responseString);
				broadcastIntent.putExtra(RESPONSE_MESSAGE, responseMessage);
				sendBroadcast(broadcastIntent);

			} catch (Exception e) {
				e.printStackTrace();

			}

		} else {
			httpConnection.disconnect();

		}
		return null;

	}

}
