package com.yesteam.eshakti.widget;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.oasys.eshakti.digitization.R;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog.OnDateSetListener;
import com.yesteam.eshakti.sqlite.database.GroupProvider;
import com.yesteam.eshakti.utils.CalculateDateUtils;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.utils.Get_Cal_CurrentDate;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.view.activity.MainActivity;
import com.yesteam.eshakti.view.fragment.FragmentDrawer;
import com.yesteam.eshakti.view.fragment.MainFragment_Dashboard;
import com.yesteam.eshakti.view.fragment.Meeting_AttendanceFragment;
import com.yesteam.eshakti.view.fragment.Meeting_AuditingFragment;
import com.yesteam.eshakti.view.fragment.Meeting_MinutesOfMeetingFragment;
import com.yesteam.eshakti.view.fragment.Meeting_TrainingFragment;
import com.yesteam.eshakti.view.fragment.Transaction_BankDepositFragment;
import com.yesteam.eshakti.view.fragment.Transaction_ExpensesFragment;
import com.yesteam.eshakti.view.fragment.Transaction_GroupLoanRepaidMenuFragment;
import com.yesteam.eshakti.view.fragment.Transaction_IncomeMenuFragment;
import com.yesteam.eshakti.view.fragment.Transaction_InternalloanMenuFragment;
import com.yesteam.eshakti.view.fragment.Transaction_Loandisbursement_MentFragment;
import com.yesteam.eshakti.view.fragment.Transaction_MemLoanRepaid_MenuFragment;
import com.yesteam.eshakti.view.fragment.Transaction_SavingsFragment;
import com.yesteam.eshakti.views.ButtonFlat;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

public class Dialog_TransactionDate extends DialogFragment implements OnDateSetListener {
	static String sItem = null;
	static Fragment sFragment;
	static Context mContext;

	boolean isNavigate = false;
	private String mLanguagelocale = "";
	Locale locale;
	int mMonthCount = 0;
	int mYearCount = 0;
	boolean isStepwiseYes = false;

	public Dialog_TransactionDate() {
		// TODO Auto-generated constructor stub
	}

	public Dialog_TransactionDate(Context context, String item) {
		// TODO Auto-generated constructor stub
		mContext = context;
		sItem = item;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		// setRetainInstance(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View rootView = inflater.inflate(R.layout.dialog_transactiondate, container, false);
		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

		System.out.println("---------------SUB ITEM ---------------  : " + sItem);

		try {

			EShaktiApplication.setAuditFragment(false);

			TextView lastTr = (TextView) rootView.findViewById(R.id.dialog_TransactionDate);
			if (EShaktiApplication.isEditOBTransDate()) {
				lastTr.setText(RegionalConversion.getRegionalConversion(AppStrings.mBalanceSheetDate) + " : "
						+ EShaktiApplication.getEditOBTransactionDate());
				lastTr.setTypeface(LoginActivity.sTypeface);

			} else {
				lastTr.setText(RegionalConversion.getRegionalConversion(AppStrings.lastTransactionDate) + " : "
						+ SelectedGroupsTask.sLastTransactionDate_Response);
				lastTr.setTypeface(LoginActivity.sTypeface);
			}

			publicValues.mOffline_Trans_CurrentDate = null;
			String calDate[] = Get_Cal_CurrentDate.getCurrentDate();

			onGetTrasactionDate();

			publicValues.mOffline_Trans_CurrentDate = calDate[0];
			TextView dialogMsg = (TextView) rootView.findViewById(R.id.dialog_continueDate);
			dialogMsg.setText(EShaktiApplication.getNextMonthFirstDate()// DatePickerDialog.sDashboardDate
					+ " : " + RegionalConversion.getRegionalConversion(AppStrings.dialogMsg));
			dialogMsg.setTypeface(LoginActivity.sTypeface);

			ButtonFlat okButton = (ButtonFlat) rootView.findViewById(R.id.fragment_Yes_button);
			okButton.setText(RegionalConversion.getRegionalConversion(AppStrings.dialogOk));
			okButton.setTypeface(LoginActivity.sTypeface);
			okButton.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					String mLastTransDate = SelectedGroupsTask.sLastTransactionDate_Response;

					Log.e("Last Date", mLastTransDate);

					Calendar c = Calendar.getInstance();
					System.out.println("Current time => " + c.getTime());

					SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
					String formattedDate = df.format(c.getTime());
					Log.v("formattedDate", formattedDate + "");
					String[] mLastTransDateArr;
					if (mLastTransDate.contains("/")) {

						mLastTransDateArr = mLastTransDate.split("/");
					} else {
						mLastTransDateArr = mLastTransDate.split("-");
					}

					String[] mformattedDateArr = formattedDate.split("-");

					mMonthCount = Integer.parseInt(mformattedDateArr[1]) - Integer.parseInt(mLastTransDateArr[1]);
					mYearCount = Integer.parseInt(mformattedDateArr[2]) - Integer.parseInt(mLastTransDateArr[2]);

					Log.e("mMonthCount", mMonthCount + "");
					Log.e("mYearCount", mYearCount + "");

					String serverDateArr[] = EShaktiApplication.getNextMonthFirstDate().split("-");

					DatePickerDialog.sDashboardDate = EShaktiApplication.getNextMonthFirstDate();
					DatePickerDialog.sSend_To_Server_Date = serverDateArr[1] + "/" + serverDateArr[0] + "/"
							+ serverDateArr[2];

					if (!EShaktiApplication.IsNewLoanDisburseDate()) {
						EShaktiApplication.setTransactionDate(DatePickerDialog.sSend_To_Server_Date);
					}

					if (EShaktiApplication.isEditOBTransDate()) {

						EShaktiApplication.setEditOBTransDate(false);

					} else {

						EShaktiApplication.setDefault(false);
						if (sItem.equals(AppStrings.savings)) {
							sFragment = new Transaction_SavingsFragment();

						} else if (sItem.equals(AppStrings.memberloanrepayment)) {
							sFragment = new Transaction_MemLoanRepaid_MenuFragment();

						} else if (sItem.equals(AppStrings.expenses)) {
							sFragment = new Transaction_ExpensesFragment();

						} else if (sItem.equals(AppStrings.income)) {
							sFragment = new Transaction_IncomeMenuFragment();

						} else if (sItem.equals(AppStrings.grouploanrepayment)) {
							sFragment = new Transaction_GroupLoanRepaidMenuFragment();

						} else if (sItem.equals(AppStrings.bankTransaction)) {
							sFragment = new Transaction_BankDepositFragment();

						} else if (sItem.equals(AppStrings.InternalLoanDisbursement)) {

							// sFragment = new
							// Transaction_InternalloanMenuFragment();
							if (ConnectionUtils.isNetworkAvailable(getActivity())) {

								EShaktiApplication.setTransactionDate(DatePickerDialog.sSend_To_Server_Date);
								sFragment = new Transaction_Loandisbursement_MentFragment();
							} else {
								sFragment = new Transaction_InternalloanMenuFragment();
							}

						} else if (sItem.equals(AppStrings.Attendance)) {
							sFragment = new Meeting_AttendanceFragment();

						} else if (sItem.equals(AppStrings.MinutesofMeeting)) {
							sFragment = new Meeting_MinutesOfMeetingFragment();

						} else if (sItem.equals(AppStrings.auditing)) {
							sFragment = new Meeting_AuditingFragment();

						} else if (sItem.equals(AppStrings.training)) {
							sFragment = new Meeting_TrainingFragment();
						} else if (sItem.equals(AppStrings.mDefault)) {
							EShaktiApplication.setDefault(true);
							EShaktiApplication.setStepwiseSavings(0);
							EShaktiApplication.setStepwiseInternalloan(0);
							EShaktiApplication.setStepWiseFragment(true);

							EShaktiApplication.setGroupLoanTotalInterest(0);
							EShaktiApplication.setGroupLoanTotalCharges(0);
							EShaktiApplication.setGroupLoanTotalRepayment(0);
							EShaktiApplication.setGroupLoanTotalInterestSubventionRecevied(0);
							EShaktiApplication.setGroupLoanTotalBankcharges(0);

							if (SelectedGroupsTask.loanAcc_loanDisbursementDate.size() != 0) {

								SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
								Log.e("First Loan Disbursement Date",
										SelectedGroupsTask.loanAcc_loanDisbursementDate.elementAt(0).toString() + "");
								String[] loanDisbDateArr = SelectedGroupsTask.loanAcc_loanDisbursementDate.elementAt(0)
										.toString().split("/");
								String temp_maxDate = loanDisbDateArr[1] + "-" + loanDisbDateArr[0] + "-"
										+ loanDisbDateArr[2];
								Date maxDate = null, nextDate = null;
								String maxDate_str = null;

								try {
									maxDate = sdf.parse(temp_maxDate);
								} catch (ParseException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								if (SelectedGroupsTask.loanAcc_loanDisbursementDate.size() > 1) {
									for (int i = 1; i < SelectedGroupsTask.loanAcc_loanDisbursementDate.size(); i++) {
										String[] temp_date = SelectedGroupsTask.loanAcc_loanDisbursementDate
												.elementAt(i).toString().split("/");
										String date = temp_date[1] + "-" + temp_date[0] + "-" + temp_date[2];
										try {
											maxDate = sdf.parse(temp_maxDate);
											nextDate = sdf.parse(date);
										} catch (ParseException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}

										if (maxDate.compareTo(nextDate) >= 0) {
											maxDate_str = temp_maxDate;
										} else {
											maxDate_str = date;
										}
									}
								} else if (SelectedGroupsTask.loanAcc_loanDisbursementDate.size() == 1) {
									maxDate_str = temp_maxDate;
								}

								Log.e("Max Loan Disbursement Date !!!!!!", maxDate_str + "");

								// String lastTrDate_str = DatePickerDialog.sDashboardDate;

								String lastTrDate_str = null;
								if (DatePickerDialog.sDashboardDate.contains("-")) {
									lastTrDate_str = DatePickerDialog.sDashboardDate;
								} else if (DatePickerDialog.sDashboardDate.contains("/")) {
									String[] dateArr = DatePickerDialog.sDashboardDate.split("/");
									lastTrDate_str = dateArr[0] + "-" + dateArr[1] + "-" + dateArr[2];
									;
								}

								Date lastTrDate = null, maxLoanDisbDate = null;
								String minDate_str = null;

								try {
									lastTrDate = sdf.parse(lastTrDate_str);
									maxLoanDisbDate = sdf.parse(maxDate_str);
								} catch (ParseException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								if (maxLoanDisbDate.compareTo(lastTrDate) >= 0) {
									minDate_str = maxDate_str;
									isStepwiseYes = true;
									TastyToast.makeText(getActivity(), AppStrings.mCheckDisbursementDate,
											TastyToast.LENGTH_SHORT, TastyToast.WARNING);
									// calendarDialogShow();
								} else {
									minDate_str = lastTrDate_str;
									isStepwiseYes = false;
									sFragment = new Meeting_AttendanceFragment();
								}

								Log.e("STEPWISE Final Min Date    ---------", minDate_str.toString() + "");

							} else if (SelectedGroupsTask.loanAcc_loanDisbursementDate.size() == 0) {
								isStepwiseYes = false;
								sFragment = new Meeting_AttendanceFragment();
							}

							MainActivity.mStepwiseBackKey = true;
						} else if (sItem.equals("EditOBTransDate")) {

						}
						if (EShaktiApplication.isEditOBTransDate()) {
							EShaktiApplication.setEditOBTransDate(false);

						} else {
							if (!isStepwiseYes) {

								if (EShaktiApplication.isDefault()) {
									MainActivity.mDateView.setClickable(false);
								} else {
									MainActivity.mDateView.setClickable(true);
								}

								MainFragment_Dashboard.sSubMenu_Item = "";
								FragmentDrawer.sSubMenu_Item = "";
								getActivity().getSupportFragmentManager().beginTransaction()
										.replace(R.id.frame, sFragment)
										.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out)
										.addToBackStack(null).show(sFragment).commit();
							}
						}
						// }
					}
					dismiss();
				}
			});

			ButtonFlat noButton = (ButtonFlat) rootView.findViewById(R.id.fragment_No_button);
			noButton.setText(RegionalConversion.getRegionalConversion(AppStrings.dialogNo));
			noButton.setTypeface(LoginActivity.sTypeface);
			noButton.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					// Do CalendarFragment Navigation

					// DatePickerDialog show
					try {
						dismiss();

						if (sItem.equals(AppStrings.mDefault)) {
							EShaktiApplication.setDefault(true);
						} else {
							EShaktiApplication.setDefault(false);
						}

						Calendar calender = Calendar.getInstance();

						SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
						String formattedDate = df.format(calender.getTime());
						Log.e("Device Date  =  ", formattedDate + "");

						String mBalanceSheetDate = SelectedGroupsTask.sLastTransactionDate_Response; // dd/MM/yyyy
						String formatted_balancesheetDate = mBalanceSheetDate.replace("/", "-");
						Log.e("Balancesheet Date = ", formatted_balancesheetDate + "");

						Date balanceDate = df.parse(formatted_balancesheetDate);
						Date systemDate = df.parse(formattedDate);

						if (balanceDate.compareTo(systemDate) < 0 || balanceDate.compareTo(systemDate) == 0) {

							calendarDialogShow();
						} else {
							TastyToast.makeText(getActivity(), "PLEASE SET YOUR DEVICE DATE CORRECTLY",
									TastyToast.LENGTH_SHORT, TastyToast.ERROR);
							EShaktiApplication.setNextMonthLastDate(null);
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			});

			ImageView image = (ImageView) rootView.findViewById(R.id.dialog_imageView);
			image.setBackgroundResource(R.drawable.lauchericon);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return rootView;
	}

	private void onGetTrasactionDate() {
		// TODO Auto-generated method stub

		try {

			GroupProvider.getSinlgeGroupMaster();

			Calendar calender = Calendar.getInstance();

			SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			String formattedDate = df.format(calender.getTime());

			String mLastTransactionDate = SelectedGroupsTask.sLastTransactionDate_Response;

			String formattedDate_LastTransaction = mLastTransactionDate.replace("/", "-");

			String days = CalculateDateUtils.get_count_of_days(formattedDate_LastTransaction, formattedDate);

			int mDaycount = Integer.parseInt(days);
			Log.e("Total Days Count =====_____----", mDaycount + "");

			Calendar calender_today = Calendar.getInstance();

			SimpleDateFormat df_today = new SimpleDateFormat("dd-MM-yyyy");
			String formattedDate_today = df_today.format(calender_today.getTime());

			if (EShaktiApplication.getSystemEntryDate() == null) {
				if (mDaycount > 30) {

					SimpleDateFormat df_after = new SimpleDateFormat("dd-MM-yyyy");

					Calendar calendar = Calendar.getInstance();
					calendar.setTime(df_after.parse(formattedDate_LastTransaction));
					calendar.add(Calendar.MONTH, 1);
					calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
					Date nextMonthFirstDay = calendar.getTime();
					System.out.println("------ Next month first date   =  " + df_after.format(nextMonthFirstDay) + "");

					Calendar cal_last_date = Calendar.getInstance();
					cal_last_date.setTime(df_after.parse(formattedDate_LastTransaction));
					cal_last_date.add(Calendar.MONTH, 1);
					cal_last_date.set(Calendar.DATE, cal_last_date.getActualMaximum(Calendar.DAY_OF_MONTH));
					Date nextMonthLastDay = cal_last_date.getTime();
					System.out.println("------ Next month Last date   =  " + df_after.format(nextMonthLastDay) + "");

					//
					String lastDateOfNextMonth = df_after.format(nextMonthLastDay);
					Calendar currentDate = Calendar.getInstance();
					int current_Day = currentDate.get(Calendar.DAY_OF_MONTH);
					int current_month = currentDate.get(Calendar.MONTH) + 1;
					int current_year = currentDate.get(Calendar.YEAR);
					String current_date = current_Day + "-" + current_month + "-" + current_year;

					Date lastdate = null, currentdate = null;
					String min_date_str = null;

					try {

						lastdate = df_after.parse(lastDateOfNextMonth);
						currentdate = df_after.parse(current_date);

					} catch (ParseException e) {
						// TODO: handle exception
						e.printStackTrace();
					}

					if (lastdate.compareTo(currentdate) <= 0) {
						min_date_str = lastDateOfNextMonth;
					} else {
						min_date_str = current_date;
					}

					Log.e("Minimum Date !!!!!	", min_date_str + "");

					EShaktiApplication.setNextMonthFirstDate(df_after.format(nextMonthFirstDay));

					EShaktiApplication.setNextMonthLastDate(min_date_str);

				} else {

					Calendar currentDate = Calendar.getInstance();
					SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
					EShaktiApplication.setNextMonthLastDate(sdf.format(currentDate.getTime()));
					EShaktiApplication.setNextMonthFirstDate(sdf.format(currentDate.getTime()));

				}
			} else {

				if (!EShaktiApplication.getSystemEntryDate().equals(formattedDate_today)) {

					if (mDaycount > 30) {

						SimpleDateFormat df_after = new SimpleDateFormat("dd-MM-yyyy");

						Calendar calendar = Calendar.getInstance();
						calendar.setTime(df_after.parse(formattedDate_LastTransaction));
						calendar.add(Calendar.MONTH, 1);
						calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
						Date nextMonthFirstDay = calendar.getTime();
						System.out.println(
								"------ Next month first date   =  " + df_after.format(nextMonthFirstDay) + "");

						Calendar cal_last_date = Calendar.getInstance();
						cal_last_date.setTime(df_after.parse(formattedDate_LastTransaction));
						cal_last_date.add(Calendar.MONTH, 1);
						cal_last_date.set(Calendar.DATE, cal_last_date.getActualMaximum(Calendar.DAY_OF_MONTH));
						Date nextMonthLastDay = cal_last_date.getTime();
						System.out
								.println("------ Next month Last date   =  " + df_after.format(nextMonthLastDay) + "");

						//
						String lastDateOfNextMonth = df_after.format(nextMonthLastDay);
						Calendar currentDate = Calendar.getInstance();
						int current_Day = currentDate.get(Calendar.DAY_OF_MONTH);
						int current_month = currentDate.get(Calendar.MONTH) + 1;
						int current_year = currentDate.get(Calendar.YEAR);
						String current_date = current_Day + "-" + current_month + "-" + current_year;

						Date lastdate = null, currentdate = null;
						String min_date_str = null;

						try {

							lastdate = df_after.parse(lastDateOfNextMonth);
							currentdate = df_after.parse(current_date);

						} catch (ParseException e) {
							// TODO: handle exception
							e.printStackTrace();
						}

						if (lastdate.compareTo(currentdate) <= 0) {
							min_date_str = lastDateOfNextMonth;
						} else {
							min_date_str = current_date;
						}

						Log.e("Minimum Date !!!!!	", min_date_str + "");

						EShaktiApplication.setNextMonthFirstDate(df_after.format(nextMonthFirstDay));

						EShaktiApplication.setNextMonthLastDate(min_date_str);

					} else {
						Calendar currentDate = Calendar.getInstance();
						SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
						EShaktiApplication.setNextMonthLastDate(sdf.format(currentDate.getTime()));
						EShaktiApplication.setNextMonthFirstDate(sdf.format(currentDate.getTime()));

					}

				} else {
					String transactionDate = EShaktiApplication.getLastTransDate_DB();
					System.out.println("----------- DB Transaction Date  =  " + transactionDate + "");
					String[] transactionDateArr = transactionDate.split("/");

					String trDate = transactionDateArr[0] + "-" + transactionDateArr[1] + "-" + transactionDateArr[2];
					EShaktiApplication.setNextMonthFirstDate(trDate);
				}
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	@Override
	public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
		// TODO Auto-generated method stub
		Log.e("onDateSet() call", dayOfMonth + "-" + monthOfYear + "-" + year);
	}

	private void calendarDialogShow() {
		try {
			// TODO Auto-generated method stub
			mLanguagelocale = PrefUtils.getUserlangcode();

			if (mLanguagelocale.equalsIgnoreCase("English")) {
				locale = new Locale("en");
			} else if (mLanguagelocale.equalsIgnoreCase("Tamil")) {
				locale = new Locale("ta");
			} else if (mLanguagelocale.equalsIgnoreCase("Hindi")) {
				locale = new Locale("hi");
			} else if (mLanguagelocale.equalsIgnoreCase("Marathi")) {
				locale = new Locale("ma");
			} else if (mLanguagelocale.equalsIgnoreCase("Malayalam")) {
				locale = new Locale("ml");
			} else if (mLanguagelocale.equalsIgnoreCase("Kannada")) {
				locale = new Locale("kn");
			} else if (mLanguagelocale.equalsIgnoreCase("Bengali")) {
				locale = new Locale("bn");
			} else if (mLanguagelocale.equalsIgnoreCase("Gujarati")) {
				locale = new Locale("gu");
			} else if (mLanguagelocale.equalsIgnoreCase("Punjabi")) {
				locale = new Locale("pa");
			} else if (mLanguagelocale.equalsIgnoreCase("Assamese")) {
				locale = new Locale("as");
			} else {
				locale = new Locale("en");
			}
			locale = new Locale("en");

			Locale.setDefault(locale);
			Configuration config = new Configuration();
			config.locale = locale;
			getActivity().getResources().updateConfiguration(config, getActivity().getResources().getDisplayMetrics());

			OnDateSetListener datelistener = Dialog_TransactionDate.this;
			Calendar now = Calendar.getInstance();
			FragmentManager fm = getFragmentManager();
			DatePickerDialog dialog = DatePickerDialog.newInstance(datelistener, now.get(Calendar.YEAR),
					now.get(Calendar.MONDAY), now.get(Calendar.DAY_OF_MONTH));

			onSetCalendarValues();
			// SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
			String balancesheet_date = String.valueOf(SelectedGroupsTask.sBalanceSheetDate_Response);
			String dateArr[] = balancesheet_date.split("/");

			Calendar min_Cal = Calendar.getInstance();

			/*
			 * int day = Integer.parseInt(dateArr[0]); int month =
			 * Integer.parseInt(dateArr[1]); int year = Integer.parseInt(dateArr[2]);
			 * 
			 * min_Cal.set(year, (month - 1), day);
			 */

			System.out.println("-------Checking   isDefault   =   " + EShaktiApplication.isDefault() + "");
			Log.e("Calendar Last Month Date !!!!!!!!!! ", EShaktiApplication.getNextMonthLastDate() + "");
			String formattedDate_LastTransaction = EShaktiApplication.getNextMonthLastDate();// SelectedGroupsTask.sLastTransactionDate_Response.replace("/",
																								// "-");

			DateFormat df_ = new SimpleDateFormat("dd-MM-yyyy");
			Calendar calendar = Calendar.getInstance();
			try {
				calendar.setTime(df_.parse(formattedDate_LastTransaction));
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			Date nextMonthLastDay = calendar.getTime();
			System.out.println("------ Next month Last date   =  " + df_.format(nextMonthLastDay) + "");

			String nextMonthLastDay_str = df_.format(nextMonthLastDay);
			String dateArray[] = nextMonthLastDay_str.split("-");

			Calendar lastDate = Calendar.getInstance();
			lastDate.set(Integer.parseInt(dateArray[2]), Integer.parseInt(dateArray[1]) - 1,
					Integer.parseInt(dateArray[0]));

			Log.e("Last Transaction Date ------", SelectedGroupsTask.sLastTransactionDate_Response + "");
			String lastTrasactiondateArr[] = SelectedGroupsTask.sLastTransactionDate_Response.split("/");
			min_Cal.set(Integer.parseInt(lastTrasactiondateArr[2]), Integer.parseInt(lastTrasactiondateArr[1]) - 1,
					Integer.parseInt(lastTrasactiondateArr[0]));

			dialog.setMinDate(min_Cal);
			EShaktiApplication.setCalendarDialog_MinDate(SelectedGroupsTask.sLastTransactionDate_Response);
			dialog.setMaxDate(lastDate);

			dialog.show(fm, "");

		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void onSetCalendarValues() {
		// TODO Auto-generated method stub

		try {

			if (EShaktiApplication.isCalendarDateVisibleFlag()) {
				EShaktiApplication.setCalendarDateVisibleFlag(false);
			}
			Calendar calender = Calendar.getInstance();

			SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			String formattedDate = df.format(calender.getTime());
			Log.e("Device Date  =  ", formattedDate + "");

			String mBalanceSheetDate = SelectedGroupsTask.sLastTransactionDate_Response; // dd/MM/yyyy
			String formatted_balancesheetDate = mBalanceSheetDate.replace("/", "-");
			Log.e("Balancesheet Date = ", formatted_balancesheetDate + "");

			Date balanceDate = df.parse(formatted_balancesheetDate);
			Date systemDate = df.parse(formattedDate);

			if (balanceDate.compareTo(systemDate) < 0 || balanceDate.compareTo(systemDate) == 0) {
				System.err.println(" balancesheet date is less than sys date");

				String mLastTransactionDate = SelectedGroupsTask.sLastTransactionDate_Response;

				String formattedDate_LastTransaction = mLastTransactionDate.replace("/", "-");

				String days = CalculateDateUtils.get_count_of_days(formattedDate_LastTransaction, formattedDate);

				int mDaycount = Integer.parseInt(days);
				Log.e("Total Days Count =====_____----", mDaycount + "");

				EShaktiApplication.setLastTransDate_GroupId(SelectedGroupsTask.Group_Id);

				if (mDaycount > 30) {

					SimpleDateFormat df_after = new SimpleDateFormat("dd-MM-yyyy");

					Calendar calendar = Calendar.getInstance();
					calendar.setTime(df_after.parse(formattedDate_LastTransaction));
					calendar.add(Calendar.MONTH, 1);
					calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
					Date nextMonthFirstDay = calendar.getTime();
					System.out.println("------ Next month first date   =  " + df_after.format(nextMonthFirstDay) + "");

					Calendar cal_last_date = Calendar.getInstance();
					cal_last_date.setTime(df_after.parse(formattedDate_LastTransaction));
					cal_last_date.add(Calendar.MONTH, 1);
					cal_last_date.set(Calendar.DATE, cal_last_date.getActualMaximum(Calendar.DAY_OF_MONTH));
					Date nextMonthLastDay = cal_last_date.getTime();
					System.out.println("------ Next month Last date   =  " + df_after.format(nextMonthLastDay) + "");

					//
					String lastDateOfNextMonth = df_after.format(nextMonthLastDay);
					Calendar currentDate = Calendar.getInstance();
					int current_Day = currentDate.get(Calendar.DAY_OF_MONTH);
					int current_month = currentDate.get(Calendar.MONTH) + 1;
					int current_year = currentDate.get(Calendar.YEAR);
					String current_date = current_Day + "-" + current_month + "-" + current_year;

					Date lastdate = null, currentdate = null;
					String min_date_str = null;

					try {

						lastdate = df_after.parse(lastDateOfNextMonth);
						currentdate = df_after.parse(current_date);

					} catch (ParseException e) {
						// TODO: handle exception
						e.printStackTrace();
					}

					if (lastdate.compareTo(currentdate) <= 0) {
						min_date_str = lastDateOfNextMonth;
					} else {
						min_date_str = current_date;
					}

					Log.e("Minimum Date !!!!!	", min_date_str + "");

					EShaktiApplication.setNextMonthLastDate(min_date_str);

				} else {
					Calendar currentDate = Calendar.getInstance();
					SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
					EShaktiApplication.setNextMonthLastDate(sdf.format(currentDate.getTime()));

				}
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}
