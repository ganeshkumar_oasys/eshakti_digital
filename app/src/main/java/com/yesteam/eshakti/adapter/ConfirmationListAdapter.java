package com.yesteam.eshakti.adapter;

import java.util.List;

import com.oasys.eshakti.digitization.R;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.model.RowItem;
import com.yesteam.eshakti.view.activity.LoginActivity;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ConfirmationListAdapter extends BaseAdapter {

	Context context;
	List<RowItem> listItems;

	public ConfirmationListAdapter(Context context, List<RowItem> items) {
		this.context = context;
		this.listItems = items;
	}

	/* private view holder class */
	private class ViewHolder {
		TextView listTitle;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		try {

			ViewHolder holder = null;

			LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.adapter_confirmation_list, null);
				holder = new ViewHolder();

				holder.listTitle = (TextView) convertView.findViewById(R.id.confirmText);

				convertView.setTag(holder);

			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			RowItem listItem = (RowItem) getItem(position);

			holder.listTitle.setText(RegionalConversion.getRegionalConversion(String.valueOf(listItem.getTitle())));
			holder.listTitle.setTypeface(LoginActivity.sTypeface);
			holder.listTitle.setTextColor(Color.WHITE);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return convertView;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return listItems.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return listItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return listItems.indexOf(getItem(position));
	}
}
