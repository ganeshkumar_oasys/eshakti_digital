package com.yesteam.eshakti.adapter;

import java.util.List;

import com.oasys.eshakti.digitization.R;
import com.yesteam.eshakti.model.RowItem;
import com.yesteam.eshakti.view.activity.LoginActivity;

import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class CustomItemAdapter extends BaseAdapter {

	Context context;
	List<RowItem> rowItems;

	public CustomItemAdapter(Context context, List<RowItem> items) {
		this.context = context;
		this.rowItems = items;
	}

	/* private view holder class */
	private class ViewHolder {
		TextView txtTitle;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder = null;

		try {
			LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			if (convertView == null || !(convertView.getTag() instanceof ViewHolder)) {
				convertView = mInflater.inflate(R.layout.spinner_row_items, null);
				holder = new ViewHolder();

				holder.txtTitle = (TextView) convertView.findViewById(R.id.row_spn_tv);

				convertView.setTag(holder);

			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			RowItem rowItem = (RowItem) getItem(position);

			holder.txtTitle.setText(rowItem.getTitle());
			holder.txtTitle.setTypeface(LoginActivity.sTypeface);
			holder.txtTitle.setGravity(Gravity.CENTER_VERTICAL);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return convertView;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return rowItems.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return rowItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return rowItems.indexOf(getItem(position));
	}
}
