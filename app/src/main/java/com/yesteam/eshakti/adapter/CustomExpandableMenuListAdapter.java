package com.yesteam.eshakti.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.oasys.eshakti.digitization.R;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.interfaces.ExpandListItemClickListener;
import com.yesteam.eshakti.model.ListItem;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.views.RaisedButton;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomExpandableMenuListAdapter extends BaseExpandableListAdapter{

	Context context;
	private List<ListItem> _listDataHeader;
	private ArrayList<HashMap<String, String>> listDataChild;
	private ExpandListItemClickListener mCallBack;
	
	public CustomExpandableMenuListAdapter(Context context,List<ListItem> listDataHeader,
			ArrayList<HashMap<String, String>> listChildData, ExpandListItemClickListener callback) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this._listDataHeader = listDataHeader;
		this.listDataChild = listChildData;
		this.mCallBack = callback;
	}
	
	@Override
	public int getGroupCount() {
		// TODO Auto-generated method stub
		return this._listDataHeader.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public Object getGroup(int groupPosition) {
		// TODO Auto-generated method stub
		return this._listDataHeader.get(groupPosition);
	}

	@Override
	public HashMap<String, String> getChild(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return this.listDataChild.get(groupPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {
		// TODO Auto-generated method stub
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		TextView listTitle;
		ImageView listImage;
		try {

			LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.view_row_menulist, null);
			} 

			listTitle = (TextView) convertView.findViewById(R.id.dynamicText);
			listImage = (ImageView) convertView.findViewById(R.id.dynamicImage);

			ListItem listItem = (ListItem) getGroup(groupPosition);

			listTitle.setText(RegionalConversion.getRegionalConversion(String.valueOf(listItem.getTitle())));
			listTitle.setTypeface(LoginActivity.sTypeface);

			listTitle.setTextColor(Color.WHITE);
			listImage.setImageResource(listItem.getImageId());

		} catch (Exception e) {
			e.printStackTrace();
		}

		return convertView;
	}

	@Override
	public View getChildView(final int groupPosition, int childPosition,
			boolean isLastChild, View convertView, final ViewGroup parent) {
		// TODO Auto-generated method stub
		HashMap<String, String> childMenu = getChild(groupPosition, childPosition);
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.fragment_menulist_view_content, null);
		}
		
		RaisedButton button = (RaisedButton) convertView.findViewById(R.id.fragment_menulist_next_button);
		TextView mAccNo = (TextView) convertView.findViewById(R.id.accNumber);
		TextView mAccNo_Values = (TextView) convertView.findViewById(R.id.AccNumber_values);
		TextView mBankName = (TextView) convertView.findViewById(R.id.bankName);
		TextView mBankNameValues = (TextView) convertView.findViewById(R.id.bankName_values);
		TextView mdisbursementTime = (TextView) convertView.findViewById(R.id.disbursementTime);
		TextView mdisbursementTimeValues = (TextView) convertView.findViewById(R.id.disbursementTime_values);

		try {
			if (childMenu.get("AccNo").equals("") ) {
				mAccNo.setVisibility(View.GONE);
				mAccNo_Values.setVisibility(View.GONE);

				mBankName.setVisibility(View.GONE);
				mBankNameValues.setVisibility(View.GONE);

				mdisbursementTime.setVisibility(View.GONE);
				mdisbursementTimeValues.setVisibility(View.GONE);
			} else {

				mAccNo.setVisibility(View.VISIBLE);
				mAccNo_Values.setVisibility(View.VISIBLE);

				mBankName.setVisibility(View.VISIBLE);
				mBankNameValues.setVisibility(View.VISIBLE);

				mdisbursementTime.setVisibility(View.VISIBLE);
				mdisbursementTimeValues.setVisibility(View.VISIBLE);
				
				mAccNo.setText(AppStrings.mAccountNumber + " :  ");
				mAccNo_Values.setText(childMenu.get("AccNo"));

				mBankName.setText(AppStrings.bankName + " :  ");
				mBankName.setPadding(5, 5, 5, 5);
				mBankNameValues.setText(childMenu.get("BankName"));
				mBankNameValues.setPadding(5, 5, 5, 5);
				mBankName.setTypeface(LoginActivity.sTypeface);
				mBankNameValues.setTypeface(LoginActivity.sTypeface);

				mdisbursementTime.setText(AppStrings.mLoanDisbursementDate + " :  ");
				mdisbursementTime.setPadding(5, 5, 5, 5);
				mdisbursementTimeValues.setText(childMenu.get("DisbursementDate"));
				mdisbursementTimeValues.setPadding(5, 5, 5, 5);
				mdisbursementTime.setTypeface(LoginActivity.sTypeface);
				mdisbursementTimeValues.setTypeface(LoginActivity.sTypeface);
				
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			e.printStackTrace();
		}
		mAccNo.setPadding(5, 5, 5, 5);
		mAccNo_Values.setPadding(5, 5, 5, 5);
		mAccNo.setTypeface(LoginActivity.sTypeface);
		mAccNo_Values.setTypeface(LoginActivity.sTypeface);

		mAccNo.setTextColor(Color.BLACK);
		mAccNo_Values.setTextColor(Color.BLACK);
		mBankName.setTextColor(Color.BLACK);
		mBankNameValues.setTextColor(Color.BLACK);
		mdisbursementTime.setTextColor(Color.BLACK);
		mdisbursementTimeValues.setTextColor(Color.BLACK);

		button.setTypeface(LoginActivity.sTypeface);
		button.setText(RegionalConversion.getRegionalConversion(AppStrings.mSelect));
		button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mCallBack.onItemClick(parent, v, groupPosition);
			}
		});
		return convertView;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return false;
	}

}
