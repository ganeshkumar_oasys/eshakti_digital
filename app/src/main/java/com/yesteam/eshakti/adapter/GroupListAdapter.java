package com.yesteam.eshakti.adapter;

import java.util.List;
import java.util.Vector;

import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.model.GroupListItem;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.view.activity.GroupListActivity;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.Login_webserviceTask;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class GroupListAdapter extends BaseAdapter {

	Context context;
	List<GroupListItem> listItems;
	Vector<String> mGroupId;
	Vector<String> mGroupValues;

	public GroupListAdapter(Context context, List<GroupListItem> items) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.listItems = items;
	}

	/* private view holder class */
	private class ViewHolder {
		ImageView listLeftImage;
		TextView listTitle;
		ImageView listImage;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return listItems.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return listItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub

		return listItems.indexOf(getItem(position));
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		try {

			ViewHolder holder = null;

			LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.view_row, null);
				holder = new ViewHolder();

				holder.listLeftImage = (ImageView) convertView.findViewById(R.id.dynamicImage_left);
				holder.listTitle = (TextView) convertView.findViewById(R.id.dynamicText);
				holder.listImage = (ImageView) convertView.findViewById(R.id.dynamicImage);

				convertView.setTag(holder);

			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			holder.listLeftImage.setVisibility(View.VISIBLE);

			GroupListItem group_listItem = (GroupListItem) getItem(position);

			holder.listTitle
					.setText(RegionalConversion.getRegionalConversion(String.valueOf(group_listItem.getTitle())));
			holder.listTitle.setTypeface(LoginActivity.sTypeface);
			holder.listTitle.setSelected(true);

			if (EShaktiApplication.isCheckGroupListTextColor()) {
				if (PrefUtils.getGrouplistValues() != null && PrefUtils.getGrouplistValues().equals("1")
						&& GroupListActivity.isGetOfflineData) {

					if (GroupListActivity.offline_sAGent_GActiveStatus.elementAt(position).equals("Yes")) {
						holder.listTitle.setTextColor(Color.WHITE);
					} else {
						holder.listTitle.setTextColor(Color.YELLOW);
					}

				} else {
					if (Login_webserviceTask.sAgent_GActiveStatus.elementAt(position).equals("Yes")) {
						holder.listTitle.setTextColor(Color.WHITE);
					} else {
						holder.listTitle.setTextColor(Color.YELLOW);
					}
				}
			}

			if (EShaktiApplication.isGroupListValues()) {
				if (PrefUtils.getGrouplistValues() != null && PrefUtils.getGrouplistValues().equals("1")
						&& GroupListActivity.isGetOfflineData) {

					if (GroupListActivity.offline_sAGent_GActiveStatus.elementAt(position).equals("No")) {
						holder.listTitle.setTextColor(Color.YELLOW);
					}
				} else {

					if (Login_webserviceTask.sAgent_GActiveStatus.elementAt(position).equals("No")) {
						holder.listTitle.setTextColor(Color.YELLOW);
					}
				}

			} else {
				holder.listTitle.setTextColor(Color.WHITE);
			}

			if (!ConnectionUtils.isNetworkAvailable(context)) {
				if (GroupListActivity.mGroupListView) {
					if (GroupListActivity.offline_sAGent_GActiveStatus.elementAt(position).equals("Yes")) {
						holder.listTitle.setTextColor(Color.WHITE);
					} else {
						holder.listTitle.setTextColor(Color.YELLOW);
					}
				}
			}
			if (publicValues.mGetAnimatorPendingGroupwiseValues != null) {
				mGroupId = new Vector<String>();
				mGroupValues = new Vector<String>();
				String mValues = null;

				String[] mGroupwiseArray = publicValues.mGetAnimatorPendingGroupwiseValues.split("#");

				for (int i = 0; i < mGroupwiseArray.length; i++) {
					String mStrinValues = mGroupwiseArray[i];
					String[] mStringArray = mStrinValues.split("~");
					mGroupId.addElement(mStringArray[0].toString());
					mGroupValues.addElement(mStringArray[1].toString());

				}
				for (int i = 0; i < mGroupId.size(); i++) {
					if (PrefUtils.getGrouplistValues() != null && PrefUtils.getGrouplistValues().equals("1")
							&& GroupListActivity.isGetOfflineData) {
						if (GroupListActivity.offline_sAgent_GIDs.get(position).equals(mGroupId.get(i))) {
							mValues = mGroupValues.get(i);

						}
					} else {
						if (Login_webserviceTask.sAgent_GIDs.get(position).equals(mGroupId.get(i))) {
							mValues = mGroupValues.get(i);

						}
					}

				}
				if (mValues.equals("Yes")) {
					holder.listLeftImage.setImageResource(R.drawable.stars);
				} else {

					holder.listLeftImage.setVisibility(View.INVISIBLE);
				}

			} else {

				holder.listImage.setImageResource(group_listItem.getImageId());
				holder.listLeftImage.setVisibility(View.INVISIBLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return convertView;
	}

}
