package com.yesteam.eshakti.adapter;

import java.util.List;

import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.model.ListItem;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.view.activity.GroupListActivity;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.Login_webserviceTask;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomListAdapter extends BaseAdapter {

	Context context;
	List<ListItem> listItems;

	public CustomListAdapter(Context context, List<ListItem> items) {
		this.context = context;
		this.listItems = items;
	}

	/* private view holder class */
	private class ViewHolder {
		ImageView listLeftImage;
		TextView listTitle;
		ImageView listImage;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		try {

			ViewHolder holder = null;

			LayoutInflater mInflater = (LayoutInflater) context
					.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.view_row, null);
				holder = new ViewHolder();

				holder.listLeftImage = (ImageView) convertView.findViewById(R.id.dynamicImage_left);
				holder.listTitle = (TextView) convertView
						.findViewById(R.id.dynamicText);
				holder.listImage = (ImageView) convertView
						.findViewById(R.id.dynamicImage);

				convertView.setTag(holder);

			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			holder.listLeftImage.setVisibility(View.GONE);
			ListItem listItem = (ListItem) getItem(position);

			holder.listTitle
					.setText(RegionalConversion.getRegionalConversion(String
							.valueOf(listItem.getTitle())));
			holder.listTitle.setTypeface(LoginActivity.sTypeface);
			holder.listTitle.setSelected(true);

			if (EShaktiApplication.isCheckGroupListTextColor()) {

				if (Login_webserviceTask.sAgent_GActiveStatus.elementAt(
						position).equals("Yes")) {
					holder.listTitle.setTextColor(Color.WHITE);
				} else {
					holder.listTitle.setTextColor(Color.YELLOW);
				}
			}

			if (EShaktiApplication.isGroupListValues()) {
				if (Login_webserviceTask.sAgent_GActiveStatus.elementAt(
						position).equals("No")) {
					holder.listTitle.setTextColor(Color.YELLOW);
				}

			} else {
				holder.listTitle.setTextColor(Color.WHITE);
			}

			if (!ConnectionUtils.isNetworkAvailable(context)) {
				if (GroupListActivity.mGroupListView) {
					if (GroupListActivity.offline_sAGent_GActiveStatus
							.elementAt(position).equals("Yes")) {
						holder.listTitle.setTextColor(Color.WHITE);
					} else {
						holder.listTitle.setTextColor(Color.YELLOW);
					}
				}
			}

			holder.listImage.setImageResource(listItem.getImageId());

		} catch (Exception e) {
			e.printStackTrace();
		}

		return convertView;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return listItems.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return listItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub

		return listItems.indexOf(getItem(position));
	}

}
