package com.yesteam.eshakti.adapter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.R;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.interfaces.ExpandListItemClickListener;
import com.yesteam.eshakti.model.GroupListItem;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.views.RaisedButton;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomExpandableGroupListAdapter extends BaseExpandableListAdapter {

    public ArrayList<ListOfShg> shgList;
    Context context;
    List<GroupListItem> _listDataHeader;
    private ArrayList<HashMap<String, String>> listDataChild;
    private ExpandListItemClickListener mCallBack;
    private NetworkConnection networkConnection;
    boolean flag = false;
    Vector<String> mGroupId;
    Vector<String> mGroupValues;
    private boolean flag_verificaton = false;

    public CustomExpandableGroupListAdapter(Context context, ArrayList<ListOfShg> shgDto, List<GroupListItem> listDataHeader,
                                            ArrayList<HashMap<String, String>> listChildData, ExpandListItemClickListener callback) {
        // TODO Auto-generated constructor stub
        networkConnection = NetworkConnection.getNetworkConnection(context);
        this.context = context;
        this._listDataHeader = listDataHeader;
        this.listDataChild = listChildData;
        this.shgList = shgDto;
        this.mCallBack = callback;
    }

    @Override
    public int getGroupCount() {
        // TODO Auto-generated method stub
        return this._listDataHeader.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        // TODO Auto-generated method stub
        return 1;
    }


    @Override
    public Object getGroup(int groupPosition) {
        // TODO Auto-generated method stub
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public HashMap<String, String> getChild(int groupPosition, int childPosition) {
        // TODO Auto-generated method stub
        return this.listDataChild.get(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        // TODO Auto-generated method stub
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        // TODO Auto-generated method stub
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        try {

            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.view_new_row, null);
            }

            ImageView listLeftImage = (ImageView) convertView.findViewById(R.id.dynamicImage_left);
            TextView listTitle = (TextView) convertView.findViewById(R.id.dynamicText);
            ImageView listImage = (ImageView) convertView.findViewById(R.id.dynamicImage);

            listLeftImage.setVisibility(View.VISIBLE);

            GroupListItem group_listItem = (GroupListItem) getGroup(groupPosition);
            ListOfShg dto = this.shgList.get(groupPosition);

            listTitle.setText(RegionalConversion.getRegionalConversion(String.valueOf(group_listItem.getTitle())));
            listTitle.setTypeface(LoginActivity.sTypeface);
            listTitle.setSelected(true);

            if (networkConnection.isNetworkAvailable()) {

                if (dto.isVerified()) {
                    flag_verificaton = false;
                    listTitle.setTextColor(Color.WHITE);
                } else if (!dto.isVerified()) {
                    flag_verificaton = true;
                    listTitle.setTextColor(Color.YELLOW);
                }


                if (dto.getLastTransactionDate().length() > 0) {
                    listLeftImage.setImageResource(R.drawable.stars);
                    Calendar cal = Calendar.getInstance();
                    // DateFormat simple = new SimpleDateFormat("dd MMM yyyy HH:mm:ss:SSS Z");
                    Date result = new Date(Long.parseLong(dto.getLastTransactionDate()));
                    cal.setTime(result);
                    int month = cal.get(Calendar.MONTH);
                    long yr = cal.get(Calendar.YEAR);
                    //  String dateStr = simple.format(result);
                    Date d = new Date();
                    cal.setTime(d);
                    int month1 = cal.get(Calendar.MONTH);
                    int yr1 = cal.get(Calendar.YEAR);
                    if (month == month1 && yr == yr1) {
                        listLeftImage.setVisibility(View.INVISIBLE);
                    } else {
                        listLeftImage.setVisibility(View.VISIBLE);
                    }
                }
            }else{
                listTitle.setTextColor(Color.WHITE);
                listLeftImage.setVisibility(View.INVISIBLE);
            }
            //  listLeftImage.setVisibility(View.INVISIBLE);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, int childPosition, boolean isLastChild, View convertView,
                             final ViewGroup parent) {
        // TODO Auto-generated method stub
        HashMap<String, String> childMenu = getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.view_content, null);
        }

        RaisedButton button = (RaisedButton) convertView.findViewById(R.id.activity_next_button);
        TextView mShgName = (TextView) convertView.findViewById(R.id.shgcode);
        TextView mShgName_Values = (TextView) convertView.findViewById(R.id.shgcode_values);
        TextView mBlockname = (TextView) convertView.findViewById(R.id.blockname);
        TextView mBlockNameValues = (TextView) convertView.findViewById(R.id.blockname_values);
        TextView mPanchayatName = (TextView) convertView.findViewById(R.id.panchayat);
        TextView mPanchayatNameValues = (TextView) convertView.findViewById(R.id.panchayat_values);
        TextView mVillagename = (TextView) convertView.findViewById(R.id.villagename);
        TextView mVillagenameValues = (TextView) convertView.findViewById(R.id.villagename_vlaues);
        TextView mTransactionDate = (TextView) convertView.findViewById(R.id.transDate);
        TextView mTransactionDateValues = (TextView) convertView.findViewById(R.id.transDate_vlaues);

        mShgName.setTypeface(LoginActivity.sTypeface);
        mShgName_Values.setTypeface(LoginActivity.sTypeface);
        mBlockname.setTypeface(LoginActivity.sTypeface);
        mBlockNameValues.setTypeface(LoginActivity.sTypeface);

        mPanchayatName.setTypeface(LoginActivity.sTypeface);
        mPanchayatNameValues.setTypeface(LoginActivity.sTypeface);
        mVillagename.setTypeface(LoginActivity.sTypeface);
        mVillagenameValues.setTypeface(LoginActivity.sTypeface);

        mTransactionDate.setTypeface(LoginActivity.sTypeface);
        mTransactionDateValues.setTypeface(LoginActivity.sTypeface);

        mShgName.setTextColor(Color.BLACK);
        mShgName_Values.setTextColor(Color.BLACK);
        mBlockname.setTextColor(Color.BLACK);
        mBlockNameValues.setTextColor(Color.BLACK);

        mPanchayatName.setTextColor(Color.BLACK);
        mPanchayatNameValues.setTextColor(Color.BLACK);
        mVillagename.setTextColor(Color.BLACK);
        mVillagenameValues.setTextColor(Color.BLACK);

        mTransactionDate.setTextColor(Color.BLACK);
        mTransactionDateValues.setTextColor(Color.BLACK);

        button.setText(AppStrings.mSelect);

        mShgName.setText(childMenu.get("SHGCode_Label") + "  :  ");
        mShgName_Values.setText(childMenu.get("SHGCode"));


        mBlockname.setText(childMenu.get("BlockName_Label") + "  :  ");
        mBlockNameValues.setText(childMenu.get("BlockName"));

        if (childMenu.get("BlockName_Label").equals("Municipality Name")) {
            mVillagename.setVisibility(View.GONE);
            mVillagenameValues.setVisibility(View.GONE);
            mPanchayatName.setVisibility(View.VISIBLE);
            mPanchayatNameValues.setVisibility(View.VISIBLE);
            mPanchayatName.setText(childMenu.get("PanchayatName_Label") + "  :  ");
            mPanchayatNameValues.setText(childMenu.get("PanchayatName"));

        } else if (childMenu.get("BlockName_Label").equals("Panchayat Name")) {
            mVillagename.setVisibility(View.GONE);
            mVillagenameValues.setVisibility(View.GONE);
            mPanchayatName.setVisibility(View.GONE);
            mPanchayatNameValues.setVisibility(View.GONE);

        } else {
            mVillagename.setVisibility(View.VISIBLE);
            mVillagenameValues.setVisibility(View.VISIBLE);
            mPanchayatName.setVisibility(View.VISIBLE);
            mPanchayatNameValues.setVisibility(View.VISIBLE);
            mVillagename.setText(childMenu.get("VillageName_Label") + "  :  ");
            mVillagenameValues.setText(childMenu.get("VillageName"));
            mPanchayatName.setText(childMenu.get("PanchayatName_Label") + "  :  ");
            mPanchayatNameValues.setText(childMenu.get("PanchayatName"));
        }

        mTransactionDate.setText(childMenu.get("LastTransactionDate_Label") + "  :  ");

        DateFormat simple = new SimpleDateFormat("dd/MM/yyyy");
        Date d = new Date(Long.parseLong(childMenu.get("LastTransactionDate")));
        String dateStr = simple.format(d);
        mTransactionDateValues.setText(dateStr);


        button.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                ListOfShg dto = shgList.get(groupPosition);

                if (dto.isVerified()) {
                    flag_verificaton = false;
                } else if (!dto.isVerified()) {
                    flag_verificaton = true;
                }

                if (!flag_verificaton)
                    mCallBack.onItemClick(parent, v, groupPosition);
                else
                    mCallBack.onItemClickVerification(parent, v, groupPosition);
            }
        });
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        // TODO Auto-generated method stub
        return false;
    }

}
