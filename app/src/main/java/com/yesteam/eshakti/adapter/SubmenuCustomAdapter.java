package com.yesteam.eshakti.adapter;

import java.util.List;

import com.oasys.eshakti.digitization.R;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.model.RowItem_submenu;
import com.yesteam.eshakti.view.activity.LoginActivity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class SubmenuCustomAdapter extends BaseAdapter {

	Context context;
	List<RowItem_submenu> rowItems;

	public SubmenuCustomAdapter(Context context, List<RowItem_submenu> rowItems) {
		this.context = context;
		this.rowItems = rowItems;
	}

	@Override
	public int getCount() {
		return rowItems.size();
	}

	@Override
	public Object getItem(int position) {
		return rowItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return rowItems.indexOf(getItem(position));
	}

	/* private view holder class */
	private class ViewHolder {
		ImageView mSubmenu_img;
		TextView mSubmenu_name;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder holder = null;

		if (convertView == null) {
			holder = new ViewHolder();
			convertView = LayoutInflater.from(context).inflate(R.layout.adapter_listrow, null);
			holder.mSubmenu_name = (TextView) convertView.findViewById(R.id.submenu_textView);
			holder.mSubmenu_img = (ImageView) convertView.findViewById(R.id.submenu_imageView);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		RowItem_submenu row_pos = rowItems.get(position);
		if (row_pos != null) {
			holder.mSubmenu_img.setImageResource(row_pos.getSubmenu_img_id());
			holder.mSubmenu_name.setText(RegionalConversion.getRegionalConversion(row_pos.getSubmenu_name()));
			holder.mSubmenu_name.setTypeface(LoginActivity.sTypeface);
		}

		return convertView;
	}
}