package com.yesteam.eshakti.utils;

import com.yesteam.eshakti.view.fragment.Transaction_BankDepositFragment;
import com.yesteam.eshakti.view.fragment.Transaction_DepositMenuFragment;
import com.yesteam.eshakti.view.fragment.Transaction_FixedDepositEntryFragment;

import android.util.Log;

public class GetFixedDepositBankValues {

	public GetFixedDepositBankValues() {
	}

	public static String GetFixedDepositBankValues() {

		StringBuilder builder = new StringBuilder();
		String mMasterresponse = null, mFixedBankName = null, mFOSAmount = null, mFOSBANKID = null, result = null,
				mPLOSrepayment, tempSelectedBankName;

		tempSelectedBankName = Transaction_BankDepositFragment.sSendToServer_BankName;
		for (int i = 0; i < Transaction_DepositMenuFragment.response.length; i++) {
			System.out.println("Response : " + Transaction_DepositMenuFragment.response[i]);

			String secondResponse[] = Transaction_DepositMenuFragment.response[i].split("~");
			System.out.println("LOAN BANK NAME : " + secondResponse[0]);

			mFixedBankName = secondResponse[0];
			mFOSAmount = secondResponse[1];
			mFOSBANKID = secondResponse[2];
			System.out.println("BankDepositFragment BANK NAME : " + mFixedBankName
					+ "                  :SelectedBank Are Equals:: " + tempSelectedBankName);
			if (tempSelectedBankName.equals(secondResponse[2])) {
				Log.v("Yes", "If Condition True");
				Log.e("Bank Name 1", tempSelectedBankName);

				Log.e("Bank Name 2", secondResponse[0]);
				mFOSAmount = Transaction_FixedDepositEntryFragment.mCashatBank_Fixed_individual;

			}
			mMasterresponse = mFixedBankName + "~" + mFOSAmount + "~" + mFOSBANKID + "%";

			builder.append(mMasterresponse);

		}
		result = builder.toString();
		return result;
	}

}
