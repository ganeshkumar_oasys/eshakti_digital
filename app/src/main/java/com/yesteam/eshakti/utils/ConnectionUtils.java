package com.yesteam.eshakti.utils;

import com.yesteam.eshakti.receiver.ConnectivityReceiver;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public final class ConnectionUtils {

	public static final boolean isNetworkAvailable(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		boolean isConnected = ConnectivityReceiver.isConnected();
//		return netInfo != null && netInfo.isConnected();
		return isConnected; 
	}
}