package com.yesteam.eshakti.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

public class CalendarUtils {

	public static ArrayList<String> sNameOfEvent = new ArrayList<String>();
	public static ArrayList<String> sStartDates = new ArrayList<String>();
	public static ArrayList<String> sEndDates = new ArrayList<String>();
	public static ArrayList<String> sDescriptions = new ArrayList<String>();

	public static ArrayList<String> readCalendarEvent(Context context) {
		Cursor cursor = context.getContentResolver()
				.query(Uri.parse("content://com.android.calendar/events"),
						new String[] { "calendar_id", "title", "description",
								"dtstart", "dtend", "eventLocation" }, null,
						null, null);
		cursor.moveToFirst();
		// fetching calendars name
		String CNames[] = new String[cursor.getCount()];

		// fetching calendars id
		sNameOfEvent.clear();
		sStartDates.clear();
		sEndDates.clear();
		sDescriptions.clear();
		
		for (int i = 0; i < CNames.length; i++) {

			sNameOfEvent.add(cursor.getString(1));
			sStartDates.add(getDate(Long.parseLong(cursor.getString(3))));
			sEndDates.add(getDate(Long.parseLong(cursor.getString(4))));
			sDescriptions.add(cursor.getString(2));
			CNames[i] = cursor.getString(1);
			cursor.moveToNext();

		}
		return sNameOfEvent;
	}

	public static String getDate(long milliSeconds) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(milliSeconds);
		return formatter.format(calendar.getTime());
	}

}
