package com.yesteam.eshakti.utils;

import java.io.ByteArrayOutputStream;

import com.yesteam.eshakti.appConstants.Constants;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.util.Base64;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class Configuration {
	public static String toByteCode(Bitmap fileName) {
		String imageString = Constants.EXCEPTION;

		try {

			Bitmap bitmap = fileName;
			int newWidth = 150, newHeight = 200;
			Bitmap scaledBitmap = Bitmap.createBitmap(newWidth, newHeight, Config.ARGB_8888);
			float ratioX = newWidth / (float) bitmap.getWidth();
			float ratioY = newHeight / (float) bitmap.getHeight();
			float middleX = newWidth / 2.0f;
			float middleY = newHeight / 2.0f;

			Matrix scaleMatrix = new Matrix();
			scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);
			Canvas canvas = new Canvas(scaledBitmap);
			canvas.setMatrix(scaleMatrix);
			canvas.drawBitmap(bitmap, middleX - bitmap.getWidth() / 2, middleY - bitmap.getHeight() / 2,
					new Paint(Paint.FILTER_BITMAP_FLAG));

			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bos);
			byte[] imageArray = bos.toByteArray();
			imageString = Base64.encodeToString(imageArray, Base64.NO_WRAP);

		} catch (Exception e) {
			imageString = Constants.EXCEPTION;
		}
		return imageString;
	}

	public static void hideSoftKeyboard(Context mContext, View view) {
		if (view != null) {
			int heightDiff = view.getRootView().getHeight() - view.getHeight();
			if (heightDiff > 100) {

				InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
			}
		}
	}

}
