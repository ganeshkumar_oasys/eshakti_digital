package com.yesteam.eshakti.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.util.Log;

public class CalculateDateUtils {

	public static String get_count_of_days(String mLastTransactionDate,
			String mCurrentDate) {
		String dayDifference = null;
		try {
			String CurrentDate = mLastTransactionDate;
			String FinalDate = mCurrentDate;

			Date date1;
			Date date2;

			SimpleDateFormat dates = new SimpleDateFormat("dd-MM-yyyy");

			// Setting dates
			date1 = dates.parse(CurrentDate);
			date2 = dates.parse(FinalDate);

			// Comparing dates
			long difference = Math.abs(date1.getTime() - date2.getTime());
			long differenceDates = difference / (24 * 60 * 60 * 1000);

			// Convert long to String
			dayDifference = Long.toString(differenceDates);

			Log.e("HERE", "HERE: " + dayDifference);

		} catch (Exception exception) {
			Log.e("DIDN'T WORK", "exception " + exception);
		}
		return dayDifference;

	}
}
