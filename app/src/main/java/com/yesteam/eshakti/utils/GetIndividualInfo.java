package com.yesteam.eshakti.utils;

import com.yesteam.eshakti.webservices.SelectedGroupsTask;

public class GetIndividualInfo {

	private static String SEND_LOAN_INFO = "";

	private static String SEND_MEMBERID_INFO = "";

	private static String SEND_MEMBERNAME_INFO = "";

	public static String getLoanInfo() {
		
		SEND_LOAN_INFO = "";
		
		int size = SelectedGroupsTask.loan_Id.size();
		System.out.println("Size :" + size);

		for (int i = 0; i < size; i++) {
			SEND_LOAN_INFO = SEND_LOAN_INFO
					+ SelectedGroupsTask.loan_Id.elementAt(i).toString() + "~";
			System.out.println("sendloaninfo :" + SEND_LOAN_INFO);
		}

		return SEND_LOAN_INFO;
	}

	public static String getMemberID_Info() {
		
		 SEND_MEMBERID_INFO = "";

		int size = SelectedGroupsTask.member_Id.size();

		for (int i = 0; i < size; i++) {
			SEND_MEMBERID_INFO = SEND_MEMBERID_INFO
					+ SelectedGroupsTask.member_Id.elementAt(i).toString()
					+ "~";
		}

		return SEND_MEMBERID_INFO;

	}

	public static String getMemberName_Info() {
		
		SEND_MEMBERNAME_INFO = "";

		int size = SelectedGroupsTask.member_Name.size();

		for (int i = 0; i < size; i++) {
			SEND_MEMBERNAME_INFO = SEND_MEMBERNAME_INFO
					+ SelectedGroupsTask.member_Name.elementAt(i).toString()
					+ "~";
		}

		return SEND_MEMBERNAME_INFO;

	}
}
