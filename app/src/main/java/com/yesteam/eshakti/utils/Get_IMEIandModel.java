package com.yesteam.eshakti.utils;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;

public class Get_IMEIandModel {
	
	static String sIMEI_Model = null;
	static String sManufacturer,sBrand,sProduct,sModel;

	public static String getIMEIandModel(Context context){
		
		try{
			
			sManufacturer = Build.MANUFACTURER;
			sBrand = Build.BRAND;
			sProduct = Build.PRODUCT;
			sModel = Build.MODEL;
			
			System.out.println(" MANUFACTURE : " +sManufacturer+ "  BRAND : " +sBrand+ " PRODUCT : " +sProduct+ " MODEL : "+sModel);
			
			TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
			String mIMEI = telephony.getDeviceId();
			
			System.out.println("IMEI : " +mIMEI);
			
			sIMEI_Model = sBrand +" "+sModel+"~"+mIMEI;
			System.out.println(" IMEI & MODEL : " +sIMEI_Model);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return sIMEI_Model;
	}
	
	/*try{
		
		String manufacturer = Build.MANUFACTURER;
		String brand        = Build.BRAND;
		String product      = Build.PRODUCT;
		String model        = Build.MODEL;
		
		System.out.println("MANUFACTURER : "+manufacturer+ "  BRAND : " +brand+ "  PRODUCT : " +product+ "  MODEL : " +model);
		
		TelephonyManager telephony = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		String imeiNo = telephony.getDeviceId();
		
		System.out.println("IMEI NO : " +imeiNo);
		
		//Toast.makeText(this, "IMEI : "+imeiNo+ "  BRAND : " +brand+ "  PRODUCT : " +product+ "  MODEL : " +model, Toast.LENGTH_SHORT).show();
		
		System.out.println("IMEI & MODEL NO : " +brand+" "+model+"~"+imeiNo);
		
		IMEIModel = brand+" "+model+"~"+imeiNo;
		System.out.println("IMEI & MODEL : " +IMEIModel);
		
	//	Toast.makeText(this, IMEIModel, Toast.LENGTH_SHORT).show();
		
		SharedPreferences sp = this.getSharedPreferences("mOnOffPrfs", Context.MODE_PRIVATE);
		Editor editor = sp.edit();
		
		editor.putString("mOnOffIMEIModel", IMEIModel);
		editor.commit();
		
		System.out.println(" CONTRY ISO : " +telephony.getNetworkCountryIso()+ " VOICE MAIL NUMBER : "+telephony.getVoiceMailNumber()+ "  PHONE TYPE : "+telephony.getPhoneType());
		
	}catch(Exception e){
		System.out.println("DEVICE FEATURES EXCEP :  " +e.toString());
	}*/
}
