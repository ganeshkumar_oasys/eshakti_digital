package com.yesteam.eshakti.utils;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.yesteam.eshakti.view.activity.ExitActivity;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.content.Context;
import android.content.Intent;


public class GetExit {

	/**
	 * Call to Exit
	 * 
	 * @param context
	 *            context of the view
	 * @return intent to exit
	 **/

	public static Intent getExitIntent(Context context) {

		Intent intent = null;

		try {

			SelectedGroupsTask.loan_Name.clear();
			SelectedGroupsTask.loan_Id.clear();
			SelectedGroupsTask.loan_EngName.clear();
			SelectedGroupsTask.member_Id.clear();
			SelectedGroupsTask.member_Name.clear();
			SelectedGroupsTask.sBankNames.clear();
			SelectedGroupsTask.sEngBankNames.clear();
			SelectedGroupsTask.sBankAmt.clear();
			SelectedGroupsTask.sCashatBank = " ";
			SelectedGroupsTask.sCashinHand = " ";
			SelectedGroupsTask.Group_Id = "";
			SelectedGroupsTask.Group_Name = "";
			EShaktiApplication.setSelectedgrouptask(false);
			EShaktiApplication.setPLOS(false);
			EShaktiApplication.setBankDeposit(false);
			EShaktiApplication.setOfflineTrans(false);
			EShaktiApplication.setOfflineTransDate(false);
			EShaktiApplication.setSubmenuclicked(false);
			EShaktiApplication.setLastTransId("");
			EShaktiApplication.setSetTransValues("");

			try {

				Intent exitIntent = new Intent(context, ExitActivity.class);
				intent = exitIntent;

			} catch (Exception e) {
				e.printStackTrace();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return intent;

	}

}
