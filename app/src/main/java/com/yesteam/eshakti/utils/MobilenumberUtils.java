package com.yesteam.eshakti.utils;

public class MobilenumberUtils {
	public static boolean isValidMobile(String phone) {
		boolean check = false;
		boolean _SameNumberCheck = false;

		if (phone.length() == 10) {
			String desiredString = phone.substring(0, 1);

			if (desiredString.equals("9") || desiredString.equals("8") || desiredString.equals("7")
					|| desiredString.equals("6")) {
				check = true;
			} else {
				check = false;
			}

			if (phone.equals("0000000000")) {
				_SameNumberCheck = false;
			} else if (phone.equals("1111111111")) {
				_SameNumberCheck = false;
			} else if (phone.equals("2222222222")) {
				_SameNumberCheck = false;
			} else if (phone.equals("3333333333")) {
				_SameNumberCheck = false;
			} else if (phone.equals("4444444444")) {
				_SameNumberCheck = false;
			} else if (phone.equals("5555555555")) {
				_SameNumberCheck = false;
			} else if (phone.equals("6666666666")) {
				_SameNumberCheck = false;
			} else if (phone.equals("7777777777")) {
				_SameNumberCheck = false;
			} else if (phone.equals("8888888888")) {
				_SameNumberCheck = false;
			} else if (phone.equals("9999999999")) {
				_SameNumberCheck = false;
			} else {
				_SameNumberCheck = true;
			}

			if (!_SameNumberCheck) {
				check = false;
			}

		} else {
			check = false;
		}
		if (phone.equals("")) {
			check = true;
		}

		return check;
	}

}
