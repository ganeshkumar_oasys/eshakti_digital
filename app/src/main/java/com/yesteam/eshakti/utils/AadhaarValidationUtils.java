package com.yesteam.eshakti.utils;

public class AadhaarValidationUtils {
	public static boolean validateAadharNumber(String aadharNumber) {

		boolean isValidAadhar = false;

		if (aadharNumber.length() == 12) {

			isValidAadhar = true;

			if (aadharNumber.equals("000000000000")) {
				isValidAadhar = false;
			} else if (aadharNumber.equals("111111111111")) {
				isValidAadhar = false;
			} else if (aadharNumber.equals("222222222222")) {
				isValidAadhar = false;
			} else if (aadharNumber.equals("333333333333")) {
				isValidAadhar = false;
			} else if (aadharNumber.equals("444444444444")) {
				isValidAadhar = false;
			} else if (aadharNumber.equals("555555555555")) {
				isValidAadhar = false;
			} else if (aadharNumber.equals("666666666666")) {
				isValidAadhar = false;
			} else if (aadharNumber.equals("777777777777")) {
				isValidAadhar = false;
			} else if (aadharNumber.equals("888888888888")) {
				isValidAadhar = false;
			} else if (aadharNumber.equals("999999999999")) {
				isValidAadhar = false;
			} else {
				isValidAadhar = true;
			}

		} else {
			isValidAadhar = false;
		}
		if (aadharNumber.equals("")) {
			isValidAadhar = true;
		}

		/*
		 * Pattern aadharPattern = Pattern.compile("\\d{12}"); boolean isValidAadhar =
		 * aadharPattern.matcher(aadharNumber).matches(); if (isValidAadhar) {
		 * isValidAadhar = VerhoeffAlgorithmUtils.validateVerhoeff(aadharNumber); }
		 */
		return isValidAadhar;
	}
}
