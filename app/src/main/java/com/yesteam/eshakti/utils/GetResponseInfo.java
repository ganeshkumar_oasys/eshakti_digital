package com.yesteam.eshakti.utils;

import com.yesteam.eshakti.view.fragment.Transaction_Loan_SB_disbursementFragment;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.util.Log;

public class GetResponseInfo {

	public static String sResponseUpdate[];

	public static void getResponseInfo(String service_values) {

		try {

			sResponseUpdate = service_values.split("#");

			if (sResponseUpdate[0].equals("Yes")) {

				SelectedGroupsTask.sLastTransactionDate_Response = String.valueOf(sResponseUpdate[1]);
				System.out.println("Last Transaction Date : " + SelectedGroupsTask.sLastTransactionDate_Response);

				String bankInfo[] = sResponseUpdate[2].split("~");

				SelectedGroupsTask.sCashinHand = String.valueOf(bankInfo[1]);
				System.out.println("CASH IN HAND : " + SelectedGroupsTask.sCashinHand);

				SelectedGroupsTask.sCashatBank = String.valueOf(bankInfo[3]);
				System.out.println("CASH AT BANK : " + SelectedGroupsTask.sCashatBank);

				// Clearing the info to avoid the replica's
				SelectedGroupsTask.sBankAmt.clear();
				SelectedGroupsTask.sBankNames.clear();
				SelectedGroupsTask.sEngBankNames.clear();

				Log.e("IIIIIIIII", String.valueOf(bankInfo[4]));

				try {

					int j = 0;
					for (int i = 4; i < bankInfo.length; i++) {
						if (j == 2) {
							if (!bankInfo[i].toString().equals("-")) {
								SelectedGroupsTask.sBankAmt.addElement(bankInfo[i].toString());
								System.out.println("Amount : " + i + " " + bankInfo[i].toString());
							}
						}
						if (j == 1) {
							if (!bankInfo[i].toString().equals("-")) {
								SelectedGroupsTask.sBankNames.addElement(bankInfo[i].toString());
								System.out.println("Tamil bankName : " + i + " " + bankInfo[i].toString());
							}

						}
						if (j == 0) {
							SelectedGroupsTask.sEngBankNames.addElement(bankInfo[i].toString());
							System.out.println("English bankName : " + i + " " + bankInfo[i].toString());
						}
						j = j + 1;
						if (j > 2) {
							j = 0;
						}
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
				
				
				Log.e("Current Bank Amount Values-------", SelectedGroupsTask.sBankAmt.size()+"");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void getResponseInfo_ExistingLoan(String service_values) {

		try {

			sResponseUpdate = service_values.split("#");

			if (sResponseUpdate[0].equals("Yes")) {

				SelectedGroupsTask.sLastTransactionDate_Response = String.valueOf(sResponseUpdate[1]);
				System.out.println("Last Transaction Date : " + SelectedGroupsTask.sLastTransactionDate_Response);

				String bankInfo[] = sResponseUpdate[2].split("~");

				SelectedGroupsTask.sCashinHand = String.valueOf(bankInfo[1]);
				System.out.println("CASH IN HAND : " + SelectedGroupsTask.sCashinHand);

				SelectedGroupsTask.sCashatBank = String.valueOf(bankInfo[3]);
				System.out.println("CASH AT BANK : " + SelectedGroupsTask.sCashatBank);

				// Clearing the info to avoid the replica's
				SelectedGroupsTask.sBankAmt.clear();
				SelectedGroupsTask.sBankNames.clear();
				SelectedGroupsTask.sEngBankNames.clear();

				Log.e("IIIIIIIII", String.valueOf(bankInfo[4]));

				try {

					int j = 0;
					for (int i = 4; i < bankInfo.length; i++) {
						if (j == 2) {
							if (!bankInfo[i].toString().equals("-")) {
								SelectedGroupsTask.sBankAmt.addElement(bankInfo[i].toString());
								System.out.println("Amount : " + i + " " + bankInfo[i].toString());
							}
						}
						if (j == 1) {
							if (!bankInfo[i].toString().equals("-")) {
								SelectedGroupsTask.sBankNames.addElement(bankInfo[i].toString());
								System.out.println("Tamil bankName : " + i + " " + bankInfo[i].toString());
							}

						}
						if (j == 0) {
							SelectedGroupsTask.sEngBankNames.addElement(bankInfo[i].toString());
							System.out.println("English bankName : " + i + " " + bankInfo[i].toString());
						}
						j = j + 1;
						if (j > 2) {
							j = 0;
						}
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

				@SuppressWarnings("unused")
				String mLoanId, mLoanOs;

				String tempID = "", tempOuts = "";
				String[] memID_Check, mOutstanding;
				int[] validatedOutstanding;
				int mSize;
				mLoanId = sResponseUpdate[3];

				mLoanOs = sResponseUpdate[4];
				Log.e("Loan Ouss", mLoanOs);
				String[] mArrayValues = mLoanOs.split("~");

				mSize = SelectedGroupsTask.member_Name.size();
				int count = 0;

				for (int i = 0; i < mArrayValues.length; i++) {

					if ((count == 0) && (i % 2 == 0)) {
						tempID = tempID + mArrayValues[i] + "~";
						count = 1;
					} else if ((count == 1) && (i % 2 != 0)) {
						tempOuts = tempOuts + mArrayValues[i] + "~";
						count = 0;
					}
				}
				Log.e("Temp Id----->>>>", tempID);
				Log.e("Current Outs Values_____------>>>>", tempOuts);

				validatedOutstanding = new int[mSize];

				// Storing the corresponding values in the corresponding array
				for (int i = 0; i < mSize; i++) {

					memID_Check = tempID.split("~");
					System.out.println("memID_Check " + memID_Check[i] + " POS " + i);

					mOutstanding = tempOuts.split("~");
					// TypeCasting the double values into integer
					validatedOutstanding[i] = Integer
							.parseInt(mOutstanding[i].substring(0, mOutstanding[i].indexOf(".")));
					// mOutstanding[i] =
					// String.valueOf(validatedOutstanding[i]);
					System.out.println("mOutstanding " + validatedOutstanding[i] + " POS " + i);

				}

				StringBuilder builder = new StringBuilder();
				String mMasterresponse, mPLMemberId, mPLOS, result;
				for (int i = 0; i < mSize; i++) {

					validatedOutstanding[i] = validatedOutstanding[i];
					mPLMemberId = String.valueOf(SelectedGroupsTask.member_Id.elementAt(i));

					mPLOS = String.valueOf(validatedOutstanding[i]);

					mMasterresponse = mPLMemberId + "~" + mPLOS + "~";

					builder.append(mMasterresponse);

				}
				result = builder.toString();
				Transaction_Loan_SB_disbursementFragment.mMemberOsValues = result;
				Log.e("Final Result Values-------->>>>", Transaction_Loan_SB_disbursementFragment.mMemberOsValues);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
