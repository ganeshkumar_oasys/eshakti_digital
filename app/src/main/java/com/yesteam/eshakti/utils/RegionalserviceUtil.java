package com.yesteam.eshakti.utils;

import com.yesteam.eshakti.appConstants.PublicServiceValues;
import com.yesteam.eshakti.webservices.LoginTask;

import android.content.Context;
import android.util.Log;

public class RegionalserviceUtil {

	static String mTag = RegionalserviceUtil.class.getSimpleName();

	public RegionalserviceUtil() {
		// TODO Auto-generated constructor stub
	}

	public static void getRegionalService(Context context, String string) {

		try {

			if (string.equals("English")) {

				Log.d(mTag, string);
				LoginTask.NAMESPACE = PublicServiceValues.BASE_NAMESPACE_ENGLISH;

				LoginTask.SOAP_ADDRESS = PublicServiceValues.BASE_SOAP_ADDRESS_ENGLISH;

				PrefUtils.setServicenamespace(LoginTask.NAMESPACE);

			} else if ((string.equals("Tamil")) || (string.equals("Hindi")) || (string.equals("Marathi"))
					|| (string.equals("Kannada")) || (string.equals("Malayalam")) || (string.equals("Gujarathi"))
					|| (string.equals("Bengali")) || (string.equals("Punjabi")) || (string.equals("Assamese"))) {

				Log.d(mTag, string);
				LoginTask.NAMESPACE = PublicServiceValues.BASE_NAMESPACE;

				LoginTask.SOAP_ADDRESS = PublicServiceValues.BASE_SOAP_ADDRESS;

				PrefUtils.setServicenamespace(LoginTask.NAMESPACE);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
