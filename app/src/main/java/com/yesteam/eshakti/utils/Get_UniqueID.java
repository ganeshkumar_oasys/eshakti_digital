package com.yesteam.eshakti.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.content.Context;

public class Get_UniqueID {

	static String sOffline_UniqueId;

	/**Generates unique ID for offline transaction
	 * @param Context application context
	 * @see returns the string of unique ID **/
	
	public static String get_UniqueID(Context context) {

		try {

			Calendar cal = Calendar.getInstance();

			SimpleDateFormat df = new SimpleDateFormat("ddMMyyyyHHmmss");
			String Date = df.format(cal.getTime());

			/**Unique ID format --> GroupIdDateHoursMinsSecs**/
			sOffline_UniqueId = SelectedGroupsTask.Group_Id.toString() + Date;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return sOffline_UniqueId;
	}

}
