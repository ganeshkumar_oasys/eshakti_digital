package com.yesteam.eshakti.utils;

import com.oasys.eshakti.digitization.R;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.view.activity.LoginActivity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;


public class ToastUtil {

	private static View mLayout;

	public static View getToast(Context context, String alertString,int color) {

		try {

			LayoutInflater inflater = LayoutInflater.from(context);
			mLayout = inflater.inflate(R.layout.activity_toast, null);

			TextView txt = (TextView) mLayout
					.findViewById(R.id.activity_toast_text);
			txt.setText(RegionalConversion.getRegionalConversion(alertString));
			txt.setTextColor(color);
			txt.setTypeface(LoginActivity.sTypeface);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return mLayout;

	}
}
