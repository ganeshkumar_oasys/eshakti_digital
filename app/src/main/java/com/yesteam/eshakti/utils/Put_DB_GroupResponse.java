package com.yesteam.eshakti.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.util.Log;

public class Put_DB_GroupResponse {

	static String sPut_GroupResponse;
	static int size, loanSize;
	static String memberInfo, loanInfo;
	static Date lastTr_date, tr_date;

	public Put_DB_GroupResponse() {
		// TODO Auto-generated constructor stub
		memberInfo = "";
		loanInfo = "";
		System.out.println("Member INFO " + memberInfo);

		System.out.println("Loan INFO " + loanInfo);
	}

	public static String put_DB_GroupResponse(String trDate, String cashInHand, String cashAtBank, String bankInfo) {

		try {
			memberInfo = "";
			loanInfo = "";
			size = SelectedGroupsTask.member_Id.size();
			Log.d("SIZE ", String.valueOf(size));

			System.out.println("Member INFO " + memberInfo);

			System.out.println("Loan INFO " + loanInfo);

			for (int i = 0; i < size; i++) {
				memberInfo = memberInfo + SelectedGroupsTask.member_Id.elementAt(i).toString() + "~"
						+ SelectedGroupsTask.member_Name.elementAt(i).toString() + "~";
			}
			Log.d("MemberInfo ", memberInfo);

			loanSize = SelectedGroupsTask.loan_Id.size();
			Log.d("LOAN SIZE ", String.valueOf(loanSize));

			for (int i = 0; i < loanSize; i++) {
				loanInfo = loanInfo + SelectedGroupsTask.loan_Id.elementAt(i).toString() + "~"
						+ SelectedGroupsTask.loan_EngName.elementAt(i).toString() + "~"
						+ SelectedGroupsTask.loan_Name.elementAt(i).toString() + "~";
			}
			Log.d("LOAN INFO ", loanInfo);
			Log.v("Agent::::::::::::::", EShaktiApplication.isAgent + "");
			Log.e("Group Name", SelectedGroupsTask.Group_Name);

			// Check max last transaction date
			if (!ConnectionUtils.isNetworkAvailable(EShaktiApplication.getInstance())) {

				Log.v("Last Transaction date", EShaktiApplication.getLastTransactionDate().toString() + "");
				Log.v("Tr Date", trDate + "");
				String[] lastTrDateArr = EShaktiApplication.getLastTransactionDate().split("/");
				String[] trDateArr = trDate.split("/");

				String lastTrDate_str = lastTrDateArr[0] + "-" + lastTrDateArr[1] + "-" + lastTrDateArr[2];
				String trDate_str = trDateArr[1] + "-" + trDateArr[0] + "-" + trDateArr[2];

				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

				try {
					lastTr_date = sdf.parse(lastTrDate_str);
					tr_date = sdf.parse(trDate_str);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (lastTr_date.compareTo(tr_date) >= 0) {
					trDate = EShaktiApplication.getLastTransactionDate();
				} else {
					trDate = trDateArr[1] + "/" + trDateArr[0] + "/" + trDateArr[2];
				}

				SelectedGroupsTask.sLastTransactionDate_Response = trDate;

			}

			if (EShaktiApplication.isAgent) {
				sPut_GroupResponse = SelectedGroupsTask.UserName.toString() + "#" + SelectedGroupsTask.Ngo_Id.toString()
						+ "#" + SelectedGroupsTask.Group_Id.toString() + "#" + SelectedGroupsTask.Group_Name.toString()
						+ "#" + memberInfo + "#" + loanInfo + "#" + trDate + "#"
						+ SelectedGroupsTask.sBalanceSheetDate_Response + "#" + "CashinHand~" + cashInHand
						+ "~CashatBank~" + cashAtBank + "~" + bankInfo + "#" + SelectedGroupsTask.sTr_ID + "#"
						+ SelectedGroupsTask.sLoanAccDetails;
			} else if (!EShaktiApplication.isAgent) {
				sPut_GroupResponse = SelectedGroupsTask.UserName.toString() + "#" + SelectedGroupsTask.Ngo_Id.toString()
						+ "#" + SelectedGroupsTask.Group_Id.toString() + "#" + SelectedGroupsTask.Group_Name.toString()
						+ "#" + memberInfo + "#" + loanInfo + "#" + trDate + "#"
						+ SelectedGroupsTask.sBalanceSheetDate_Response + "#" + "CashinHand~" + cashInHand
						+ "~CashatBank~" + cashAtBank + "~" + bankInfo + "#" + SelectedGroupsTask.sTr_ID + "#"
						+ SelectedGroupsTask.sLoanAccDetails;
			}

			// 0000034#1#34#786~K.தெய�?வமணி~787~செந�?தாமரை~788~பான�?மதி~789~மீனா~790~ச�?க�?ணா~791~ஜெயலக�?ஷ�?மி~792~பிரேமா~793~தமிழ�?செல�?வி~794~ப�?ஷ�?பா~795~V.பச�?சையம�?மாள�?~796~ச�?கன�?யா~797~பத�?மினி~798~சின�?னதாய�?~799~உமாமகேஸ�?வரி~800~மாதம�?மாள�?~801~சாம�?ண�?டேஸ�?வரி~802~நிர�?மலா~803~சாந�?தி~804~ச�?லோச�?சனா~805~சக�?ந�?தலா~#0~PL~தனிநபர�?
			// கடன�?~#17/08/2015#01/04/2014#CashinHand~560~CashatBank~70571~Primaryagriculturecooperativecreditsociety~தொ
			// வே கூட�?ட�?றவ�? வங�?கி~70571~-~-~~-~-
			Log.d("BINDED GROUP RESPONSE ", sPut_GroupResponse);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return sPut_GroupResponse;
	}
}