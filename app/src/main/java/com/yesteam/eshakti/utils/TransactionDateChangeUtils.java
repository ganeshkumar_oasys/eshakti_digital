package com.yesteam.eshakti.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class TransactionDateChangeUtils {

	public static void getTransactionDateChangeUtils(Context context, Activity activity) {

		Calendar calender = Calendar.getInstance();

		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String formattedDate = df.format(calender.getTime());

		if (EShaktiApplication.getLastTransDate_DB() != null) {

			String mLastTransactionDate = EShaktiApplication.getLastTransDate_DB();

			String formattedDate_LastTransaction = mLastTransactionDate.replace("/", "-");

			String days = CalculateDateUtils.get_count_of_days(formattedDate_LastTransaction, formattedDate);

			int mDaycount = Integer.parseInt(days);

			System.out.println("EShaktiApplication.getSystemEntryDate()  " + EShaktiApplication.getSystemEntryDate());

			if (EShaktiApplication.getSystemEntryDate() == null) {

				Calendar c = Calendar.getInstance();
				SimpleDateFormat df_currentDate = new SimpleDateFormat("dd-MM-yyyy");
				String formattedDate_currentDate = df_currentDate.format(c.getTime());

				PrefUtils.setCurrentDate(formattedDate_currentDate);
				System.out.println(
						"EShaktiApplication.getSystemEntryDate() 111111 " + EShaktiApplication.getSystemEntryDate());
				if (mDaycount > 30) {

					DateFormat df_ = new SimpleDateFormat("dd-MM-yyyy");
					Calendar cal = Calendar.getInstance();
					try {
						cal.setTime(df_.parse(formattedDate_LastTransaction));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					cal.add(Calendar.DATE, 30);
					SimpleDateFormat df_after = new SimpleDateFormat("dd-MM-yyyy");
					String formattedDate_after = df_after.format(cal.getTime());

					DatePickerDialog.sDashboardDate = formattedDate_after;

					SimpleDateFormat df_after_SeverDate = new SimpleDateFormat("MM/dd/yyyy");
					String formattedDate_after_SeverDate = df_after_SeverDate.format(cal.getTime());

					DatePickerDialog.sSend_To_Server_Date = formattedDate_after_SeverDate;

					EShaktiApplication.setTempLastTransDate(formattedDate_after);

				}

			} else {
				Calendar c = Calendar.getInstance();
				SimpleDateFormat mDfCurrentdate = new SimpleDateFormat("dd-MM-yyyy");
				String mFormattedCurrentDate = mDfCurrentdate.format(c.getTime());
				System.out.println(
						"EShaktiApplication.getSystemEntryDate() 2222222 " + EShaktiApplication.getSystemEntryDate());
				if (!EShaktiApplication.getSystemEntryDate().equals(mFormattedCurrentDate)) {
					System.out.println(
							"EShaktiApplication.getSystemEntryDate() 33333 " + EShaktiApplication.getSystemEntryDate());
					if (mDaycount > 30) {

						DateFormat df_ = new SimpleDateFormat("dd-MM-yyyy");
						Calendar cal = Calendar.getInstance();
						try {
							cal.setTime(df_.parse(formattedDate_LastTransaction));
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						cal.add(Calendar.DATE, 30);
						SimpleDateFormat df_after = new SimpleDateFormat("dd-MM-yyyy");
						String formattedDate_after = df_after.format(cal.getTime());

						DatePickerDialog.sDashboardDate = formattedDate_after;

						SimpleDateFormat df_after_SeverDate = new SimpleDateFormat("MM/dd/yyyy");
						String formattedDate_after_SeverDate = df_after_SeverDate.format(cal.getTime());

						DatePickerDialog.sSend_To_Server_Date = formattedDate_after_SeverDate;
						PrefUtils.setCurrentFormatDate(formattedDate_after);

					}
				} else {
					System.out.println(
							"EShaktiApplication.getSystemEntryDate() 44444 " + EShaktiApplication.getSystemEntryDate());
					if (mDaycount > 30) {
						System.out.println("EShaktiApplication.getSystemEntryDate() 555555 "
								+ EShaktiApplication.getSystemEntryDate());

						DatePickerDialog.sDashboardDate = EShaktiApplication.getTempLastTansDate();

						String[] mSendtoserverDate = EShaktiApplication.getTempLastTansDate().split("-");

						DatePickerDialog.sSend_To_Server_Date = mSendtoserverDate[1] + "/" + mSendtoserverDate[0] + "/"
								+ mSendtoserverDate[2];
					}
				}

			}
		} else {

			PrefUtils.clearAddGroupFlag();

			TastyToast.makeText(activity, AppStrings.mLoginAlert_ONOFF, TastyToast.LENGTH_SHORT, TastyToast.ERROR);

			activity.startActivity(new Intent(GetExit.getExitIntent(activity)));
			activity.finish();

		}
	}

}
