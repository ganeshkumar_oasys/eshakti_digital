package com.yesteam.eshakti.utils;

import com.oasys.eshakti.digitization.R;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.view.activity.LoginActivity;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.TextAppearanceSpan;


public class GetSpanText {

	public GetSpanText() {
		// TODO Auto-generated constructor stub
	}
	
	public static SpannableStringBuilder getSpanString(Context context, String str){
		SpannableStringBuilder SS = new SpannableStringBuilder(
				RegionalConversion
						.getRegionalConversion(str));
		
		SS.setSpan(
				new CustomTypefaceSpan("", LoginActivity.sTypeface),
				0,
				RegionalConversion.getRegionalConversion(
						str).length(),
				Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
		
		SS.setSpan(new TextAppearanceSpan(context, R.style.text), 0, RegionalConversion.getRegionalConversion(str).length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
		
		return SS;
	}
}
