package com.yesteam.eshakti.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.adapter.CalendarAdapter;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class CalendarDialog extends Dialog {

	public static GregorianCalendar month, itemMonth, test;

	static CalendarAdapter cal_Adapter;

	static LinearLayout rlayout;
	static RelativeLayout calendarTop_Relative;
	static ArrayList<String> date;
	static ArrayList<String> desc;

	public static String sSelectedFromDate = null, sSelectedToDate = null, sSelectedDate = null;
	public static String sSend_To_Server_Date = null;
	public static String sCalendarDate = "";

	static String[] separatedTime;
	static String regional_MonthName, balanceSheetDate;

	static String sCurrentDate;
	static Date date1, date2, date3;
	static TextView balnceSheet_DateText, monthTitle;

	static Button mRaised_Submit_Button;

	public CalendarDialog(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public static void calendarDialog(final Context context, View view) {

		try {

			final Dialog dialogView = new Dialog(context);
			dialogView.setContentView(R.layout.fragment_calendar);

			Locale.setDefault(Locale.US);

			calendarTop_Relative = (RelativeLayout) dialogView.findViewById(R.id.calendarHeader);
			calendarTop_Relative.setBackgroundResource(R.drawable.calendar_top);

			String calResponse[] = String.valueOf(SelectedGroupsTask.sBalanceSheetDate_Response).split("/");
			balanceSheetDate = calResponse[0] + "-" + calResponse[1] + "-" + calResponse[2];

			balnceSheet_DateText = (TextView) dialogView.findViewById(R.id.fragment_Calendar_bsDate);
			balnceSheet_DateText.setText(String.valueOf(SelectedGroupsTask.sBalanceSheetDate_Response) + "  "
					+ RegionalConversion.getRegionalConversion(AppStrings.afterDate));
			balnceSheet_DateText.setTypeface(LoginActivity.sTypeface);
			balnceSheet_DateText.setTextColor(color.black);

			final TextView selectedDateText = (TextView) dialogView.findViewById(R.id.fragment_UserselectedDate);
			selectedDateText
					.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.selectedDate)) + " : ");
			selectedDateText.setTypeface(LoginActivity.sTypeface);

			mRaised_Submit_Button = (Button) dialogView.findViewById(R.id.fragment_Raised_Submitbutton_calender_);
			mRaised_Submit_Button.setText(RegionalConversion.getRegionalConversion(AppStrings.submit));
			mRaised_Submit_Button.setTypeface(LoginActivity.sTypeface);
			mRaised_Submit_Button.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialogView.dismiss();
				}
			});

			rlayout = (LinearLayout) dialogView.findViewById(R.id.itemLinear);

			month = (GregorianCalendar) GregorianCalendar.getInstance();

			itemMonth = (GregorianCalendar) month.clone();

			cal_Adapter = new CalendarAdapter(context, month);

			GridView gridView = (GridView) dialogView.findViewById(R.id.calendarGrid);
			gridView.setAdapter(cal_Adapter);

			monthTitle = (TextView) dialogView.findViewById(R.id.monthTitle);

			// Do function call for refreshCalendar
			refreshCalendar(view);

			RelativeLayout previousMonth = (RelativeLayout) dialogView.findViewById(R.id.previous_MonthSelection);
			previousMonth.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					setPreviousMonth();
					refreshCalendar(v);
				}
			});

			RelativeLayout nextMonth = (RelativeLayout) dialogView.findViewById(R.id.next_MonthSelection);
			nextMonth.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					setPreviousMonth();
					refreshCalendar(v);
				}
			});

			gridView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					// TODO Auto-generated method stub

					// remove the previous view if exists
					if (((LinearLayout) rlayout).getChildCount() > 0) {
						((LinearLayout) rlayout).removeAllViews();
					}

					desc = new ArrayList<String>();
					date = new ArrayList<String>();

					((CalendarAdapter) parent.getAdapter()).setSelected(view);

					String selectedGridDate = CalendarAdapter.dayString.get(position);

					String[] separatedTime = selectedGridDate.split("-");

					// To be removed on Testing
					for (int i = 0; i < separatedTime.length; i++) {
						Log.d("SEPARATED TIME ", separatedTime[i]);
					}

					/** dd/mm/yyyy ***/
					sSelectedDate = separatedTime[2] + "-" + separatedTime[1] + "-" + separatedTime[0];
					Log.d("dd/mm/yyyy SELDATE ", sSelectedDate);

					/** mm/dd/yyyy **/
					sSend_To_Server_Date = separatedTime[1] + "/" + separatedTime[2] + "/" + separatedTime[0];
					Log.d("mm/dd/yyyy TO_SER_DATE ", sSend_To_Server_Date);

					balnceSheet_DateText.setTextColor(color.black);

					selectedDateText
							.setText(RegionalConversion.getRegionalConversion(AppStrings.selectedDate) + sSelectedDate);
					selectedDateText.setTypeface(LoginActivity.sTypeface);
					selectedDateText.setTextColor(Color.rgb(34, 139, 0));

					SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

					/** User Selected Date **/
					try {

						date1 = sdf.parse(sSelectedDate);

					} catch (ParseException e) {
						// TODO: handle exception
						e.printStackTrace();
					}

					/** BalanceSheetDate **/
					try {
						date2 = sdf.parse(balanceSheetDate);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					/** Current Date **/
					try {
						String calCurrentDate[] = CalendarAdapter.curentDateString.split("-");

						sCurrentDate = calCurrentDate[2] + "-" + calCurrentDate[1] + "-" + calCurrentDate[0];

						date3 = sdf.parse(sCurrentDate);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					/**
					 * Compares the date of SelectedDate with BalanceSheetDate &
					 * CurrentDate
					 **/

					if ((date1.compareTo(date2)) >= 0 && (date1.compareTo(date3) <= 0)) {

					} else {

						mRaised_Submit_Button.setVisibility(View.GONE);
						balnceSheet_DateText.setTextColor(Color.RED);

						/*Toast toast = new Toast(context);
						toast.setView(ToastUtil.getToast(context, AppStrings.calAlert, Color.RED));
						toast.setDuration(Toast.LENGTH_SHORT);
						toast.show();*/
						TastyToast.makeText(context, AppStrings.calAlert, TastyToast.LENGTH_SHORT,
								TastyToast.WARNING);

						dialogView.create();
					}

					String gridValueStr = separatedTime[2].replaceFirst("^0*", ""); // Considering
																					// only
																					// the
																					// dd
																					// from
																					// yyyy/mm/dd

					int gridValue = Integer.parseInt(gridValueStr);// navigate
																	// to next
																	// or
																	// previous
																	// month by
																	// clicking
																	// the
																	// offDays.
					if ((gridValue > 10) && (position < 8)) {
						setPreviousMonth();
						refreshCalendar(view);
					} else if ((gridValue < 7) && (position > 28)) {
						setNextMonth();
						refreshCalendar(view);
					}

					((CalendarAdapter) parent.getAdapter()).setSelected(view);
					desc = null;
				}

			});

			dialogView.show();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return;
	}

	protected static void setNextMonth() {
		// TODO Auto-generated method stub
		if (month.get(GregorianCalendar.MONTH) == month.getActualMaximum(GregorianCalendar.MONTH)) {
			month.set((month.get(GregorianCalendar.YEAR) + 1), month.getActualMinimum(GregorianCalendar.MONTH), 1);
		} else {
			month.set(GregorianCalendar.MONTH, month.get(GregorianCalendar.MONTH) + 1);
		}
	}

	protected static void setPreviousMonth() {
		// TODO Auto-generated method stub

		if (month.get(GregorianCalendar.MONTH) == month.getActualMinimum(GregorianCalendar.MONTH)) {
			month.set(month.get(GregorianCalendar.YEAR) - 1, month.getActualMaximum(GregorianCalendar.MONTH), 1);
		} else {
			month.set(GregorianCalendar.MONTH, month.get(GregorianCalendar.MONTH) - 1);
		}

	}

	public static void refreshCalendar(View view) {

		// String MonthItem = "";

		try {

			cal_Adapter.refreshDays();
			cal_Adapter.notifyDataSetChanged();

			String monthText = (String) android.text.format.DateFormat.format("MMMM yyyy", month);
			Log.d("REFRESHED MONTH ITEM ", monthText);

			String[] items = monthText.split(" ");
			Log.d("Items of Month Name ", items[0]);

			if (items[0].equals("January")) {
				regional_MonthName = AppStrings.January;
			} else if (items[0].equals("February")) {
				regional_MonthName = AppStrings.February;
			} else if (items[0].equals("March")) {
				regional_MonthName = AppStrings.March;
			} else if (items[0].equals("April")) {
				regional_MonthName = AppStrings.April;
			} else if (items[0].equals("May")) {
				regional_MonthName = AppStrings.May;
			} else if (items[0].equals("June")) {
				regional_MonthName = AppStrings.June;
			} else if (items[0].equals("July")) {
				regional_MonthName = AppStrings.July;
			} else if (items[0].equals("August")) {
				regional_MonthName = AppStrings.August;
			} else if (items[0].equals("September")) {
				regional_MonthName = AppStrings.September;
			} else if (items[0].equals("October")) {
				regional_MonthName = AppStrings.October;
			} else if (items[0].equals("November")) {
				regional_MonthName = AppStrings.November;
			} else if (items[0].equals("December")) {
				regional_MonthName = AppStrings.December;
			}

			Log.d("Moth & Year ", regional_MonthName + "  " + items[1]);

			monthTitle.setText(RegionalConversion.getRegionalConversion(regional_MonthName + " " + items[1]));
			monthTitle.setTypeface(LoginActivity.sTypeface);
			monthTitle.setTextColor(color.black);

		} catch (Exception e) {

		}
	}

}
