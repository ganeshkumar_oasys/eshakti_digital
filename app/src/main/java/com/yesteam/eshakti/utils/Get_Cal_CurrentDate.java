package com.yesteam.eshakti.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Get_Cal_CurrentDate {

	static int sYear;
	static int sMonth;
	static int sDay;

	static Calendar sCalendar;

	static String sFormattedDate;

	/**Gets the current date of the calendar 
	 * @see returns the array which holds the date**/
	
	public static String[] getCurrentDate() {

		String calendarArr[] = new String[2];

		try {

			// sets the Current date

			sCalendar = Calendar.getInstance();
			sYear = sCalendar.get(Calendar.YEAR);
			sMonth = sCalendar.get(Calendar.MONTH);
			sDay = sCalendar.get(Calendar.DAY_OF_MONTH);

			SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			sFormattedDate = df.format(sCalendar.getTime());

			String cal_arr[] = sFormattedDate.split("-");
			String sendToformattedDate = cal_arr[1] + "/" + cal_arr[0] + "/"
					+ cal_arr[2];

			System.out.println("Dashboard Date : " + sCalendar.getTime());

			System.out.println("formattedDate :" + sFormattedDate.toString());

			calendarArr[0] = sFormattedDate.toString();
			calendarArr[1] = sendToformattedDate.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return calendarArr;
	}

}
