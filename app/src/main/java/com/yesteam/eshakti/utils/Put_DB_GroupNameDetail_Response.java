package com.yesteam.eshakti.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.content.Context;
import android.util.Log;

public class Put_DB_GroupNameDetail_Response {

	static String sPut_GroupResponse;

	Context context;

	static String[] groupList;
	static String[] shgcodeList;
	static String[] groupDetails;
	static String[] mGroupListMaster;
	static String mViewGroupValues = null;
	static String mAnimatorName = null;

	static boolean mFirstAddTitle = false;
	static String mLoginValues = null;
	static String mAnimatorValues = null;
	static String mTempValues = null;
	static String mGroupValues_Username = null;
	static String mGroupValues_NgoId = null;
	static String mMasterresponse = null, result = null;
	static StringBuilder builder;
	static Date trDate, lastTransactionResponseDate;

	public Put_DB_GroupNameDetail_Response() {
		// TODO Auto-generated constructor stub

	}

	public static String put_DB_GroupNameDetails_Response(String mLoginResponse) {

		try {
			String mMasterresponse = null, result = null;
			Log.e("Orginal Values Of Login Response!!!!!@@@@@", mLoginResponse);
			if (ConnectionUtils.isNetworkAvailable(EShaktiApplication
					.getInstance())) {

				builder = new StringBuilder();
				String arr_Master[] = mLoginResponse.split("!");
				mLoginValues = arr_Master[0];
				mViewGroupValues = arr_Master[1];
				mAnimatorValues = arr_Master[2];
				mTempValues = arr_Master[3];

			} else {

				builder = new StringBuilder();
				mViewGroupValues = mLoginResponse;

			}

			Log.e("View Groups Values----->>>>", mViewGroupValues);

			String arr_viewGroups[] = mViewGroupValues.split("#");
			mGroupValues_Username = arr_viewGroups[0];
			mGroupValues_NgoId = arr_viewGroups[1];
			mGroupListMaster = arr_viewGroups[2].split("%");
			Log.e("Arrray Values__++___+__+_+_______", arr_viewGroups[2]);

			try {
				Log.e("Group List Length Values_-------____----->>>>",
						mGroupListMaster.length + "");
				for (int i = 0; i < mGroupListMaster.length; i++) {

					groupDetails = mGroupListMaster[i].split("[$]");

					String mGroupList = groupDetails[0];
					shgcodeList = groupDetails[1].split("~");

					String mGroupId[] = mGroupList.split("~");

					if (i == 0) {
						EShaktiApplication
								.setGroupId_GroupLastTransDate(mGroupId[0]);
					}

					String shgcodeName = shgcodeList[0];
					String shgcodeValues = shgcodeList[1];
					String blocknameName = shgcodeList[2];
					String blocknameValues = shgcodeList[3];
					String panchayatName = shgcodeList[4];
					String panchayatValues = shgcodeList[5];
					String villagenameName = shgcodeList[6];
					String villagenameValues = shgcodeList[7];
					String transactionDate = shgcodeList[8];
					String transactionDateValues = null;
					transactionDateValues = shgcodeList[9];

					String transactionDateValuesArr[] = transactionDateValues
							.split("/");
					String responseDateArr[] = SelectedGroupsTask.sLastTransactionDate_Response
							.split("/");

					String transDate = transactionDateValuesArr[0] + "-"
							+ transactionDateValuesArr[1] + "-"
							+ transactionDateValuesArr[2];
					String responseDate = responseDateArr[0] + "-"
							+ responseDateArr[1] + "-" + responseDateArr[2];

					SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

					try {
						trDate = sdf.parse(transDate);
						lastTransactionResponseDate = sdf.parse(responseDate);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					if (mGroupId[0].equals(SelectedGroupsTask.Group_Id)) {
						if (lastTransactionResponseDate.compareTo(trDate) >= 0) {
						transactionDateValues = SelectedGroupsTask.sLastTransactionDate_Response;
						}
					} else {
						transactionDateValues = shgcodeList[9];
					}

					String mValues = shgcodeName + "~" + shgcodeValues + "~"
							+ blocknameName + "~" + blocknameValues + "~"
							+ panchayatName + "~" + panchayatValues + "~"
							+ villagenameName + "~" + villagenameValues + "~"
							+ transactionDate + "~" + transactionDateValues;

					mMasterresponse = mGroupList + "$" + mValues + "%";
					Log.e("Master i Values--->>>" + i, mMasterresponse);
					builder.append(mMasterresponse);
				}
				result = builder.toString();

			} catch (Exception e) {
				// TODO: handle exception
			}
			Log.e("MasterResponse", result);

			sPut_GroupResponse = mGroupValues_Username + "#"
					+ mGroupValues_NgoId + "#" + result;
			Log.d("BINDED GROUP RESPONSE ", sPut_GroupResponse);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return sPut_GroupResponse;
	}
}
