package com.yesteam.eshakti.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.net.ParseException;

public class DateformatUtils {
	public static String parseDateToddMMyyyy(String time)
			throws java.text.ParseException {

		String inputPattern = "dd-MM-yyyy";
		String outputPattern = "MM/dd/yyyy";
		SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
		SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

		Date date = null;
		String str = null;

		try {
			date = inputFormat.parse(time);
			str = outputFormat.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return str;
	}
}
