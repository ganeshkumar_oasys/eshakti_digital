package com.yesteam.eshakti.utils;

import com.oasys.eshakti.digitization.R;

import android.content.Context;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

public class AnimationList {

	Context mContext;

	public AnimationList(Context context) {
		mContext = context;
	}

	public Animation materialShow() {
		return AnimationUtils.loadAnimation(mContext, R.anim.anim_meterial_show);
	}

}
