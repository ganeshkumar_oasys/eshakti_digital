package com.yesteam.eshakti.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.content.Context;
import android.util.Log;

public class Get_Offline_MobileDate {

	static String mTag = Get_Offline_MobileDate.class.getSimpleName();

	static String sMobileDate_AtTransaction = null;

	/**Generates Mobile Date for offline transaction during submission
	 * @param Context application context
	 * @see returns the mobile date **/
	public static String getOffline_MobileDate(Context context) {

		try {

			Calendar cal = Calendar.getInstance();
			long milliSecs = cal.getTimeInMillis();
			Log.d(mTag, String.valueOf(milliSecs));

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String mobileDate = sdf.format(cal.getTime());

			String milli_Secs = String.valueOf(milliSecs);

			sMobileDate_AtTransaction = mobileDate + "."
					+ milli_Secs.substring(0, 3);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return sMobileDate_AtTransaction;
	}

}
