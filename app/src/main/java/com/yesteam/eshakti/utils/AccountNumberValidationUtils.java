package com.yesteam.eshakti.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AccountNumberValidationUtils {

	public static boolean validateAccountNumber(String accountNumber) {

		boolean isValidAccountNumber = false;

		Pattern pattern = Pattern.compile("[a-zA-Z0-9]*");

		Matcher matcher = pattern.matcher(accountNumber);

		if (!matcher.matches()) {
			isValidAccountNumber = false;

		} else {
			isValidAccountNumber = true;

		}

		if (accountNumber.equals("")) {
			isValidAccountNumber = true;
		}

		return isValidAccountNumber;
	}

}
