package com.yesteam.eshakti.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Handler;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.R;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.service.GroupDetailsAddService;
import com.yesteam.eshakti.service.OfflineTransactionService;
import com.yesteam.eshakti.sqlite.db.model.Login;
import com.yesteam.eshakti.sqlite.db.transactions.TransactionManager.DataType;
import com.yesteam.eshakti.view.activity.Audit_Verified_Activity;
import com.yesteam.eshakti.view.activity.GroupListActivity;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.view.fragment.MainFragment_Dashboard;
import com.yesteam.eshakti.views.ButtonFlat;
import com.yesteam.eshakti.views.RaisedButton;
import com.yesteam.eshakti.webservices.GetMessageNotificationWebservice;
import com.yesteam.eshakti.webservices.Login_webserviceTask;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;
import com.yesteam.eshakti.widget.ProgressBar;

public class AppDialogUtils {
	// public static Dialog mProgressDialog;

	public static ProgressDialog mProgressDialog;

	public static Dialog createProgressDialog(Context context) {

		return new ProgressBar(context);
	}

	public static void showConfirmationDialog(final Activity activity, final String string) {
		final Dialog confirmationDialog = new Dialog(activity);

		LayoutInflater li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View customView = li.inflate(R.layout.dialog_singin_diff, null, false);
		final ButtonFlat mConYesButton;
		final ButtonFlat mConNoButton;

		TextView mConfirmationHeadertextview;
		TextView mConfirmationTexttextview;

		mConfirmationHeadertextview = (TextView) customView.findViewById(R.id.dialog_Title);
		mConfirmationTexttextview = (TextView) customView.findViewById(R.id.dialog_Message);

		mConYesButton = (ButtonFlat) customView.findViewById(R.id.fragment_ok_button_alert);
		mConNoButton = (ButtonFlat) customView.findViewById(R.id.fragment_cancel_button_alert);

		mConYesButton.setText(AppStrings.dialogOk);
		mConYesButton.setTypeface(LoginActivity.sTypeface);

		mConNoButton.setText(AppStrings.dialogNo);
		mConNoButton.setTypeface(LoginActivity.sTypeface);
		mConfirmationTexttextview.setTypeface(LoginActivity.sTypeface);

		if (string.equals("SIGNUPDIFF")) {
			mConfirmationHeadertextview.setText("CONFIRMATION");
			mConfirmationHeadertextview.setTypeface(LoginActivity.sTypeface);

			mConfirmationTexttextview.setText("Are You want to delete Offline Data?");
			mConYesButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (confirmationDialog.isShowing() && confirmationDialog != null) {

						confirmationDialog.dismiss();

					}
					PrefUtils.clearUserId();

					PrefUtils.clearGroup_Offlinedata();
					PrefUtils.clearLoginValues();
					PrefUtils.clearGrouplistValues();
					PrefUtils.clearGroupMasterResValues();
					PrefUtils.clearFirstLoginDashboard();

					EShaktiApplication.getInstance().getTransactionManager()
							.startTransaction(DataType.OFFLINE_TRANS_DELETE, null);

					try {
						Thread.sleep(200);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					EShaktiApplication.getInstance().getTransactionManager()
							.startTransaction(DataType.GROUPDETAILSDELETE, null);

					try {
						Thread.sleep(200);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.LOGINDELETE,
							null);

					try {
						Thread.sleep(200);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.GROUPNAMEDELETE,
							null);

				}
			});

			mConNoButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (confirmationDialog.isShowing() && confirmationDialog != null) {
						confirmationDialog.dismiss();
					}
				}
			});
		} else if (string.equals("AADHAARCARD")) {
			mConNoButton.setVisibility(View.INVISIBLE);
			mConfirmationHeadertextview.setText("CONFIRMATION");
			mConfirmationHeadertextview.setTypeface(LoginActivity.sTypeface);

			mConfirmationTexttextview.setText("Can You Show you Aadhaar Card");
			mConYesButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (confirmationDialog.isShowing() && confirmationDialog != null) {

						confirmationDialog.dismiss();

					}

				}
			});

		} else if (string.equals("SIGNUPDIFF_NOENTRY")) {
			mConfirmationHeadertextview.setText("CONFIRMATION");
			mConfirmationHeadertextview.setTypeface(LoginActivity.sTypeface);

			mConfirmationTexttextview.setText(AppStrings.mLoginDetailsDelete);
			mConYesButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (confirmationDialog.isShowing() && confirmationDialog != null) {

						confirmationDialog.dismiss();
						PrefUtils.clearUserId();
						PrefUtils.clearGroup_Offlinedata();
						PrefUtils.setUserlangcode(null);
						PrefUtils.clearLoginValues();
						PrefUtils.clearGrouplistValues();
						PrefUtils.clearGroupMasterResValues();
						PrefUtils.clearFirstLoginDashboard();

						EShaktiApplication.getInstance().getTransactionManager()
								.startTransaction(DataType.OFFLINE_TRANS_DELETE, null);

						try {
							Thread.sleep(200);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						EShaktiApplication.getInstance().getTransactionManager()
								.startTransaction(DataType.GROUPDETAILSDELETE, null);

						try {
							Thread.sleep(200);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.LOGINDELETE,
								null);

						try {
							Thread.sleep(200);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						EShaktiApplication.getInstance().getTransactionManager()
								.startTransaction(DataType.GROUPNAMEDELETE, null);

						Intent intent = new Intent(activity, LoginActivity.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						activity.startActivity(intent);
						activity.overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);
						activity.finish();
					}

				}
			});

			mConNoButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (confirmationDialog.isShowing() && confirmationDialog != null) {
						confirmationDialog.dismiss();
					}
				}

			});
		}

		confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		confirmationDialog.setCanceledOnTouchOutside(false);
		confirmationDialog.setContentView(customView);
		confirmationDialog.setCancelable(true);
		confirmationDialog.show();

	}

	public static void showGpsWarningDialog(final Activity activity) {
		final Dialog confirmationDialog = new Dialog(activity);

		LayoutInflater li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View customView = li.inflate(R.layout.dialog_singin_diff, null, false);
		final ButtonFlat mConYesButton;
		final ButtonFlat mConNoButton;

		TextView mConfirmationHeadertextview;
		TextView mConfirmationTexttextview;

		mConfirmationHeadertextview = (TextView) customView.findViewById(R.id.dialog_Title);
		mConfirmationTexttextview = (TextView) customView.findViewById(R.id.dialog_Message);

		mConYesButton = (ButtonFlat) customView.findViewById(R.id.fragment_ok_button_alert);
		mConNoButton = (ButtonFlat) customView.findViewById(R.id.fragment_cancel_button_alert);
		mConYesButton.setText(AppStrings.dialogOk);
		mConYesButton.setTypeface(LoginActivity.sTypeface);

		mConNoButton.setText(AppStrings.dialogNo);
		mConNoButton.setTypeface(LoginActivity.sTypeface);

		mConfirmationHeadertextview.setText("GPS settings");
		mConfirmationHeadertextview.setTypeface(LoginActivity.sTypeface);

		mConfirmationTexttextview.setText("GPS is not enabled.To Turn on the GPS click th settings.");
		mConYesButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (confirmationDialog.isShowing() && confirmationDialog != null) {

					confirmationDialog.dismiss();
					Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
					activity.startActivity(intent);
					activity.finish();
				}

			}
		});

		mConNoButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (confirmationDialog.isShowing() && confirmationDialog != null) {
					confirmationDialog.dismiss();
				}
			}
		});

		confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		confirmationDialog.setCanceledOnTouchOutside(false);
		confirmationDialog.setContentView(customView);
		confirmationDialog.setCancelable(true);
		confirmationDialog.show();

	}

	public static void showReportsDialog(final Activity activity, final int totalcoll, final int totaldisburse) {
		final Dialog confirmationDialog = new Dialog(activity);

		LayoutInflater li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View customView = li.inflate(R.layout.default_reports, null, false);

		TextView header, totalAmountLabel, plDisbursementLabel, cashInHandLabel, cashAtBankLabel;
		TextView totalAmount, plDisbursementAmount, cashInHandAmount, cashAtBankAmount;
		Button fragment_Submitbutton;
		fragment_Submitbutton = (Button) customView.findViewById(R.id.fragment_Submitbutton);
		header = (TextView) customView.findViewById(R.id.header);
		header.setText(RegionalConversion.getRegionalConversion(AppStrings.reports));
		header.setTypeface(LoginActivity.sTypeface);

		totalAmountLabel = (TextView) customView.findViewById(R.id.lefttotalAmount);
		totalAmountLabel.setText(RegionalConversion.getRegionalConversion(AppStrings.totalSavings));
		totalAmountLabel.setTypeface(LoginActivity.sTypeface);
		totalAmountLabel.setTextColor(Color.BLACK);

		totalAmount = (TextView) customView.findViewById(R.id.righttotalAmount);
		totalAmount.setText(totalcoll);
		totalAmount.setTypeface(LoginActivity.sTypeface);
		totalAmount.setTextColor(Color.BLACK);

		plDisbursementLabel = (TextView) customView.findViewById(R.id.leftPLAmount);
		plDisbursementLabel.setText(RegionalConversion.getRegionalConversion(AppStrings.InternalLoanDisbursement) + " "
				+ AppStrings.amount);
		plDisbursementLabel.setTypeface(LoginActivity.sTypeface);
		plDisbursementLabel.setTextColor(Color.BLACK);

		plDisbursementAmount = (TextView) customView.findViewById(R.id.rightPLAmount);
		plDisbursementAmount.setText(totaldisburse);
		plDisbursementAmount.setTypeface(LoginActivity.sTypeface);
		plDisbursementAmount.setTextColor(Color.BLACK);

		cashInHandLabel = (TextView) customView.findViewById(R.id.leftCIHAmount);
		cashInHandLabel.setText(RegionalConversion.getRegionalConversion(AppStrings.cashinhand));
		cashInHandLabel.setTypeface(LoginActivity.sTypeface);
		cashInHandLabel.setTextColor(Color.BLACK);

		cashInHandAmount = (TextView) customView.findViewById(R.id.rightCIHAmount);
		cashInHandAmount.setText(SelectedGroupsTask.sCashinHand);
		cashInHandAmount.setTypeface(LoginActivity.sTypeface);
		cashInHandAmount.setTextColor(Color.BLACK);

		cashAtBankLabel = (TextView) customView.findViewById(R.id.leftCABAmount);
		cashAtBankLabel.setText(RegionalConversion.getRegionalConversion(AppStrings.cashatBank));
		cashAtBankLabel.setTypeface(LoginActivity.sTypeface);
		cashAtBankLabel.setTextColor(Color.BLACK);

		cashAtBankAmount = (TextView) customView.findViewById(R.id.rightCABAmount);
		cashAtBankAmount.setText(SelectedGroupsTask.sCashatBank);
		cashAtBankAmount.setTypeface(LoginActivity.sTypeface);
		cashAtBankAmount.setTextColor(Color.BLACK);

		fragment_Submitbutton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

			}
		});

		confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		confirmationDialog.setCanceledOnTouchOutside(false);
		confirmationDialog.setContentView(customView);
		confirmationDialog.setCancelable(true);
		confirmationDialog.show();

	}

	public static void showAppVersionUpdateDialog(final Activity activity, String mVersion) {
		final Dialog confirmationDialog = new Dialog(activity);

		LayoutInflater li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View customView = li.inflate(R.layout.dialog_appversionupdatealert, null, false);

		TextView header;

		RaisedButton fragment_Submitbutton;
		fragment_Submitbutton = (RaisedButton) customView.findViewById(R.id.fragment_Ok_button_dialog_appVersion);
		header = (TextView) customView.findViewById(R.id.confirmationHeader_dialog_appVersion);
		header.setText(
				"  YOUR APP VERSION CODE IS :  " + String.valueOf(mVersion) + "\n  UPDATED APP VERSION CODE IS :  "
						+ EShaktiApplication.getmAppVersionName() + "\n  SO PLEASE UPDATE EShakti APP. ");
		header.setTypeface(LoginActivity.sTypeface);
		fragment_Submitbutton.setTypeface(LoginActivity.sTypeface);

		fragment_Submitbutton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				confirmationDialog.dismiss();

				final String appPackageName = activity.getPackageName();
				try {
					activity.startActivity(
							new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
				} catch (android.content.ActivityNotFoundException anfe) {
					activity.startActivity(new Intent(Intent.ACTION_VIEW,
							Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
				}
			}
		});

		confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		confirmationDialog.setCanceledOnTouchOutside(false);
		confirmationDialog.setContentView(customView);
		confirmationDialog.setCancelable(false);
		confirmationDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
				WindowManager.LayoutParams.WRAP_CONTENT);
		confirmationDialog.show();

	}

	public static void LocationAlertDialog(final Activity context) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		final Dialog alertDialog = builder.create();
		builder.setTitle("Location Services Not Active");
		builder.setMessage("Please enable Location Services and GPS");
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialogInterface, int i) {
				// Show location settings when the user acknowledges the
				// alert dialog
				Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
				context.startActivity(intent);
			}
		});
		builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				alertDialog.dismiss();
			}
		});

		alertDialog.setCanceledOnTouchOutside(false);
		alertDialog.show();

	}

	public static void showConfirmationOfflineAvailDialog(final Activity activity) {
		final Dialog confirmationDialog = new Dialog(activity);

		LayoutInflater li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View customView = li.inflate(R.layout.dialog_singin_diff, null, false);
		final ButtonFlat mConYesButton;
		final ButtonFlat mConNoButton;

		TextView mConfirmationHeadertextview;
		TextView mConfirmationTexttextview;

		mConfirmationHeadertextview = (TextView) customView.findViewById(R.id.dialog_Title);
		mConfirmationTexttextview = (TextView) customView.findViewById(R.id.dialog_Message);

		mConYesButton = (ButtonFlat) customView.findViewById(R.id.fragment_ok_button_alert);
		mConNoButton = (ButtonFlat) customView.findViewById(R.id.fragment_cancel_button_alert);
		mConYesButton.setText(AppStrings.dialogOk);
		mConYesButton.setTypeface(LoginActivity.sTypeface);

		mConNoButton.setText(AppStrings.dialogNo);
		mConNoButton.setTypeface(LoginActivity.sTypeface);

		mConNoButton.setVisibility(View.GONE);
		mConfirmationHeadertextview.setText("INFORMATION");
		// Your offline data is still available at local db, So you must move
		// data from db to online server!!!
		mConfirmationTexttextview.setText("Click yes to upload offline data.");
		mConfirmationHeadertextview.setTypeface(LoginActivity.sTypeface);
		mConfirmationTexttextview.setTypeface(LoginActivity.sTypeface);
		mConYesButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (confirmationDialog.isShowing() && confirmationDialog != null) {

					confirmationDialog.dismiss();
					LoginActivity.iSOfflineDataAvail = false;
					mProgressDialog = new ProgressDialog(activity);
					mProgressDialog.setTitle("Offline data is uploading.. ");
					mProgressDialog.setMessage("Please Wait....");
					mProgressDialog.show();
					mProgressDialog.setCancelable(false);
					mProgressDialog.setCanceledOnTouchOutside(false);

					Intent intentservice = new Intent(activity, OfflineTransactionService.class);
					activity.startService(intentservice);

				}

			}
		});

		confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		confirmationDialog.setCanceledOnTouchOutside(false);
		confirmationDialog.setContentView(customView);
		confirmationDialog.setCancelable(true);
		confirmationDialog.show();

	}

	public static void showConfirmation_CheckBackLogDialog(final Activity activity) {
		final Dialog confirmationDialog = new Dialog(activity);

		LayoutInflater li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View customView = li.inflate(R.layout.dialog_check_backlog, null, false);

		final com.oasys.eshakti.digitization.views.RaisedButton mConNoButton;

		TextView mConfirmationHeadertextview;

		TextView mTarget_textview, mCompleted_textview, mPending_textview;
		TextView mTarget_values, mCompleted_values, mPending_values;

		mConfirmationHeadertextview = (TextView) customView.findViewById(R.id.confirmationHeader_CheckBackLog);
		mConfirmationHeadertextview.setTypeface(LoginActivity.sTypeface);
		mConfirmationHeadertextview.setText(AppStrings.mCheckBackLog);
		mConNoButton = (com.oasys.eshakti.digitization.views.RaisedButton) customView.findViewById(R.id.fragment_Ok_button_checkBacklog);
		mConNoButton.setText(AppStrings.yes);
		mConNoButton.setTypeface(LoginActivity.sTypeface);

		mTarget_textview = (TextView) customView.findViewById(R.id.target_text);
		mTarget_values = (TextView) customView.findViewById(R.id.target_values);
		mTarget_textview.setTypeface(LoginActivity.sTypeface);
		mTarget_values.setTypeface(LoginActivity.sTypeface);

		mCompleted_textview = (TextView) customView.findViewById(R.id.completed_text);
		mCompleted_values = (TextView) customView.findViewById(R.id.completed_values);
		mCompleted_textview.setTypeface(LoginActivity.sTypeface);
		mCompleted_values.setTypeface(LoginActivity.sTypeface);

		mPending_textview = (TextView) customView.findViewById(R.id.pending_text);
		mPending_values = (TextView) customView.findViewById(R.id.pending_values);
		mPending_textview.setTypeface(LoginActivity.sTypeface);
		mPending_values.setTypeface(LoginActivity.sTypeface);

		mTarget_textview.setText(AppStrings.mTarget);
		mCompleted_textview.setText(AppStrings.mCompleted);
		mPending_textview.setText(AppStrings.mPending);
		if (PrefUtils.getAnimatorTargetValues() != null) {

			String[] mValues = PrefUtils.getAnimatorTargetValues().split("~");

			mTarget_values.setText(mValues[0]);

			mCompleted_values.setText(mValues[1]);
			mPending_values.setText(mValues[2]);

		}

		mConNoButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				confirmationDialog.dismiss();
			}
		});

		confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		confirmationDialog.setCanceledOnTouchOutside(false);
		confirmationDialog.setContentView(customView);
		confirmationDialog.setCancelable(true);
		confirmationDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
				WindowManager.LayoutParams.WRAP_CONTENT);

		confirmationDialog.show();

	}

	public static void showConfirmation_LogoutDialog(final Activity activity) {
		final Dialog confirmationDialog = new Dialog(activity);

		LayoutInflater li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View customView = li.inflate(R.layout.dialog_new_logout, null, false);
		TextView mHeaderView, mMessageView;

		mHeaderView = (TextView) customView.findViewById(R.id.dialog_Title_logout);
		mMessageView = (TextView) customView.findViewById(R.id.dialog_Message_logout);
		mHeaderView.setTypeface(LoginActivity.sTypeface);
		mMessageView.setTypeface(LoginActivity.sTypeface);

		final ButtonFlat mConYesButton, mConNoButton;

		mConYesButton = (ButtonFlat) customView.findViewById(R.id.fragment_ok_button_alert_logout);
		mConNoButton = (ButtonFlat) customView.findViewById(R.id.fragment_cancel_button_alert_logout);
		mConYesButton.setTypeface(LoginActivity.sTypeface);
		mConNoButton.setTypeface(LoginActivity.sTypeface);
		mConYesButton.setText(AppStrings.dialogOk);

		mConNoButton.setText(AppStrings.dialogNo);

		mHeaderView.setText(AppStrings.logOut);
		mHeaderView.setVisibility(View.INVISIBLE);

		mMessageView.setText(AppStrings.mAskLogout);
		mConYesButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				confirmationDialog.dismiss();

				activity.startActivity(new Intent(GetExit.getExitIntent(activity)));
				activity.finish();
			}
		});

		mConNoButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				confirmationDialog.dismiss();
			}
		});

		confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		confirmationDialog.setCanceledOnTouchOutside(false);
		confirmationDialog.setContentView(customView);
		confirmationDialog.setCancelable(false);
		confirmationDialog.show();

	}

	public static void showNotificationMessageDialog(final Activity activity) {
		final Dialog confirmationDialog = new Dialog(activity);

		LayoutInflater li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View customView = li.inflate(R.layout.dialog_notification, null, false);
		final Button mConYesButton;

		TextView mConfirmationHeadertextview;

		mConfirmationHeadertextview = (TextView) customView.findViewById(R.id.dialogTextView_notification);

		mConYesButton = (Button) customView.findViewById(R.id.dialog_Yes_button_notification);

		mConYesButton.setText(AppStrings.yes);
		mConYesButton.setTypeface(LoginActivity.sTypeface);
		if (GetMessageNotificationWebservice.sGetMessageNotificationWebservice != null) {
			if (!GetMessageNotificationWebservice.sGetMessageNotificationWebservice.equals("No")) {
				mConfirmationHeadertextview.setText(GetMessageNotificationWebservice.sGetMessageNotificationWebservice);
			}

		}

		mConfirmationHeadertextview.setTypeface(LoginActivity.sTypeface);

		mConYesButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (confirmationDialog.isShowing() && confirmationDialog != null) {

					confirmationDialog.dismiss();

				}

			}
		});

		confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		confirmationDialog.setCanceledOnTouchOutside(false);
		confirmationDialog.setContentView(customView);
		confirmationDialog.setCancelable(false);
		confirmationDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
				WindowManager.LayoutParams.WRAP_CONTENT);
		confirmationDialog.show();

	}

	public static void showAlertMessageDialog(final Activity activity) {
		final Dialog confirmationDialog = new Dialog(activity);

		LayoutInflater li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View customView = li.inflate(R.layout.dialog_alert_message, null, false);
		final Button mConYesButton;

		TextView mConfirmationHeadertextview;

		mConfirmationHeadertextview = (TextView) customView.findViewById(R.id.dialogTextView_alert);

		mConYesButton = (Button) customView.findViewById(R.id.dialog_Yes_button_alert);

		mConYesButton.setText("EXIT");
		mConYesButton.setTypeface(LoginActivity.sTypeface);
		if (publicValues.mGetAlertMessageValues != null) {
			String[] mValues = publicValues.mGetAlertMessageValues.split("~");
			if (mValues[0].equals("1")) {

				mConfirmationHeadertextview.setText(mValues[1]);
			}

		}

		mConfirmationHeadertextview.setTypeface(LoginActivity.sTypeface);
		mConfirmationHeadertextview.setTextSize(18);

		mConYesButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (confirmationDialog.isShowing() && confirmationDialog != null) {

					confirmationDialog.dismiss();
					activity.finish();

				}

			}
		});

		confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		confirmationDialog.setCanceledOnTouchOutside(false);
		confirmationDialog.setContentView(customView);
		confirmationDialog.setCancelable(false);
		confirmationDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
				WindowManager.LayoutParams.WRAP_CONTENT);
		confirmationDialog.show();

	}

	public static void showNotificationMessage_AlertDialog(final Activity activity) {
		final Dialog confirmationDialog = new Dialog(activity);

		LayoutInflater li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View customView = li.inflate(R.layout.dialog_notification, null, false);
		final Button mConYesButton;

		TextView mConfirmationHeadertextview;
		final Handler handler = new Handler();

		mConfirmationHeadertextview = (TextView) customView.findViewById(R.id.dialogTextView_notification);

		mConYesButton = (Button) customView.findViewById(R.id.dialog_Yes_button_notification);

		mConYesButton.setText(AppStrings.yes);
		mConYesButton.setTypeface(LoginActivity.sTypeface);

		if (publicValues.mGetAlertMessageValues != null) {
			String[] mValues = publicValues.mGetAlertMessageValues.split("~");
			if (mValues[0].equals("0")) {
				System.out.println("Current Values ---->>>" + mValues[1]);
				mConfirmationHeadertextview.setText(mValues[1]);

			}

		}

		mConfirmationHeadertextview.setTypeface(LoginActivity.sTypeface);

		mConYesButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (confirmationDialog.isShowing() && confirmationDialog != null) {

					confirmationDialog.dismiss();

					EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.LOGINDELETE,
							null);

					try {
						Thread.sleep(300);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.LOGIN_ADD,
							new Login(0, Login_webserviceTask.UserName, Login_webserviceTask.Ngo_Id,
									Login_webserviceTask.Trainer_Id, LoginActivity.sPwd,
									Login_webserviceTask.User_RegLanguage));

					Runnable runnable = new Runnable() {
						@Override
						public void run() {
							handler.post(new Runnable() { // This
								// thread
								// runs
								// in
								// the
								// UI
								@Override
								public void run() {

									// TODO Auto-generated method
									// stub

									if (PrefUtils.getAddGroupFlag() == null) {

										EShaktiApplication.getInstance().getTransactionManager()
												.startTransaction(DataType.GROUPNAMEDELETE, null);

										try {
											Thread.sleep(500);
										} catch (InterruptedException e) {
											// TODO Auto-generated catch
											// block
											e.printStackTrace();
										}

										Intent intentservice = new Intent(activity, GroupDetailsAddService.class);
										activity.startService(intentservice);
									}
									EShaktiApplication.setLoginFlag(Constants.ONLINEFLAG);

									if (LoginActivity.mVersion < Integer
											.parseInt(EShaktiApplication.getAppVersionCode())) {

										AppDialogUtils.showAppVersionUpdateDialog(activity, LoginActivity.mVersionName);
									} else {

										PrefUtils.setLoginGroupService("1");
										PrefUtils.setLoginValues("1");
										Intent intent_login = new Intent(activity, GroupListActivity.class);
										intent_login.setFlags(
												Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
										activity.startActivity(intent_login);
										activity.overridePendingTransition(R.anim.right_to_left_in,
												R.anim.right_to_left_out);
										activity.finish();

									}

								}
							});
						}
					};
					new Thread(runnable).start();

				}

			}
		});

		confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		confirmationDialog.setCanceledOnTouchOutside(false);
		confirmationDialog.setContentView(customView);
		confirmationDialog.setCancelable(false);
		confirmationDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
				WindowManager.LayoutParams.WRAP_CONTENT);
		confirmationDialog.show();

	}

	public static void showSendEMailAlertDialog(final Activity activity) {
		final Dialog confirmationDialog = new Dialog(activity);

		LayoutInflater li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View customView = li.inflate(R.layout.dialog_singin_diff, null, false);
		final ButtonFlat mConYesButton;
		final ButtonFlat mConNoButton;

		TextView mConfirmationHeadertextview;
		TextView mConfirmationTexttextview;

		mConfirmationHeadertextview = (TextView) customView.findViewById(R.id.dialog_Title);
		mConfirmationTexttextview = (TextView) customView.findViewById(R.id.dialog_Message);

		mConYesButton = (ButtonFlat) customView.findViewById(R.id.fragment_ok_button_alert);
		mConNoButton = (ButtonFlat) customView.findViewById(R.id.fragment_cancel_button_alert);
		mConYesButton.setText(AppStrings.dialogOk);
		mConYesButton.setTypeface(LoginActivity.sTypeface);

		mConNoButton.setText(AppStrings.dialogNo);
		mConNoButton.setTypeface(LoginActivity.sTypeface);

		// mConfirmationHeadertextview.setText("GPS settings");
		// mConfirmationHeadertextview.setTypeface(LoginActivity.sTypeface);
		mConfirmationHeadertextview.setVisibility(View.INVISIBLE);

		mConfirmationTexttextview.setText("Do you want to send an email with database attachment?.");
		mConfirmationTexttextview.setTextSize(18);
		mConfirmationTexttextview.setTypeface(LoginActivity.sTypeface);
		mConYesButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (confirmationDialog.isShowing() && confirmationDialog != null) {

					confirmationDialog.dismiss();
					MainFragment_Dashboard.sendEmailWithDBAttachment(activity);
				}

			}
		});

		mConNoButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (confirmationDialog.isShowing() && confirmationDialog != null) {
					confirmationDialog.dismiss();
				}
			}
		});

		confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		confirmationDialog.setCanceledOnTouchOutside(false);
		confirmationDialog.setContentView(customView);
		confirmationDialog.setCancelable(true);
		confirmationDialog.show();

	}

	public static void showAlertTransAuditDialog(final Activity activity) {
		final Dialog confirmationDialog = new Dialog(activity);

		LayoutInflater li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View customView = li.inflate(R.layout.dialog_singin_diff, null, false);
		final ButtonFlat mConYesButton;
		final ButtonFlat mConNoButton;

		TextView mConfirmationHeadertextview;
		TextView mConfirmationTexttextview;

		mConfirmationHeadertextview = (TextView) customView.findViewById(R.id.dialog_Title);
		mConfirmationTexttextview = (TextView) customView.findViewById(R.id.dialog_Message);

		mConYesButton = (ButtonFlat) customView.findViewById(R.id.fragment_ok_button_alert);
		mConNoButton = (ButtonFlat) customView.findViewById(R.id.fragment_cancel_button_alert);
		mConYesButton.setText("YES");
		mConYesButton.setTypeface(LoginActivity.sTypeface);

		mConNoButton.setText("NO");
		mConNoButton.setTypeface(LoginActivity.sTypeface);

		mConfirmationHeadertextview.setText("CONFIRMATION");
		// Your offline data is still available at local db, So you must move
		// data from db to online server!!!
		mConfirmationTexttextview
				.setText("Transaction audit has been completed and changes waiting for your approval.");
		mConfirmationHeadertextview.setTypeface(LoginActivity.sTypeface);
		mConfirmationTexttextview.setTypeface(LoginActivity.sTypeface);
		mConYesButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (confirmationDialog.isShowing() && confirmationDialog != null) {

					confirmationDialog.dismiss();

					Intent intent = new Intent(activity, Audit_Verified_Activity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					activity.startActivity(intent);
					activity.overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

					activity.finish();

				}

			}
		});

		mConNoButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (confirmationDialog.isShowing() && confirmationDialog != null) {

					confirmationDialog.dismiss();

				}
			}
		});
		confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		confirmationDialog.setCanceledOnTouchOutside(false);
		confirmationDialog.setContentView(customView);
		confirmationDialog.setCancelable(false);
		confirmationDialog.show();

	}

}