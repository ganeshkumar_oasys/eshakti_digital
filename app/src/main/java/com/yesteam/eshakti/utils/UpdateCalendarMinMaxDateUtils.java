package com.yesteam.eshakti.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.util.Log;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.view.activity.MainActivity;
import com.yesteam.eshakti.view.fragment.FragmentDrawer;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

public class UpdateCalendarMinMaxDateUtils {

	public static void OnUpdateCalendarDate(){
		
		Calendar calender = Calendar.getInstance();

		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String formattedDate = df.format(calender.getTime());

		String mLastTransactionDate = SelectedGroupsTask.sLastTransactionDate_Response;

		String formattedDate_LastTransaction = mLastTransactionDate.replace("/", "-");

		String days = CalculateDateUtils.get_count_of_days(formattedDate_LastTransaction, formattedDate);

		int mDaycount = Integer.parseInt(days);
		Log.e("Total Days Count =====_____----", mDaycount + "");
		
		if (mDaycount > 30) {
			
			SimpleDateFormat df_after = new SimpleDateFormat("dd-MM-yyyy");

			Calendar calendar = Calendar.getInstance();
			try {
				calendar.setTime(df_after.parse(formattedDate_LastTransaction));
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			calendar.add(Calendar.MONTH, 1);
			calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
			Date nextMonthFirstDay = calendar.getTime();
			System.out.println("------ Next month first date   =  "	+ df_after.format(nextMonthFirstDay) + "");
			/*if (!EShaktiApplication.IsNewLoanDisburseDate()) {
				EShaktiApplication.setNextMonthFirstDate(df_after.format(nextMonthFirstDay));	
			}*/

			Calendar cal_last_date = Calendar.getInstance();
			try {
				cal_last_date.setTime(df_after.parse(formattedDate_LastTransaction));
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			cal_last_date.add(Calendar.MONTH, 1);
			cal_last_date.set(Calendar.DATE, cal_last_date.getActualMaximum(Calendar.DAY_OF_MONTH));
			Date nextMonthLastDay = cal_last_date.getTime();
			System.out.println("------ Next month Last date   =  " + df_after.format(nextMonthLastDay) + "");
			String lastDateOfNextMonth = df_after.format(nextMonthLastDay);
			
			Calendar currentDate = Calendar.getInstance();
			int current_Day = currentDate.get(Calendar.DAY_OF_MONTH);
			int current_month = currentDate.get(Calendar.MONTH) + 1;
			int current_year = currentDate.get(Calendar.YEAR);
			String current_date = current_Day + "-" + current_month + "-" + current_year;
			
			Date lastdate = null, currentdate = null;
			String min_date_str = null;

			try {

				lastdate = df_after.parse(lastDateOfNextMonth);
				currentdate = df_after.parse(current_date);

			} catch (ParseException e) {
				// TODO: handle exception
				e.printStackTrace();
			}

			if (lastdate.compareTo(currentdate) <= 0) {
				min_date_str = lastDateOfNextMonth;
			} else {
				min_date_str = current_date;
			}

			Log.e("Minimum Date !!!!!	", min_date_str + "");
			EShaktiApplication.setNextMonthLastDate(min_date_str);

		} else {
			Calendar currentDate = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			EShaktiApplication.setNextMonthLastDate(sdf.format(currentDate.getTime()));
			if (!EShaktiApplication.IsNewLoanDisburseDate()) {
			EShaktiApplication.setNextMonthFirstDate(sdf.format(currentDate.getTime()));
			}

		}
		
		String lastTrArr[] = SelectedGroupsTask.sLastTransactionDate_Response.split("/");
		String toolbarDate = lastTrArr[0] + "-" + lastTrArr[1] + "-" + lastTrArr[2];
		MainActivity.mDateView.setText(toolbarDate);
		FragmentDrawer.drawer_lastTR_Date.setText(
				RegionalConversion.getRegionalConversion(AppStrings.lastTransactionDate) + " : "
						+ SelectedGroupsTask.sLastTransactionDate_Response);
		FragmentDrawer.drawer_lastTR_Date.setTypeface(LoginActivity.sTypeface);
		
	}
}
