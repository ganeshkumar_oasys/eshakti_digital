package com.yesteam.eshakti.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

public class GetSqliteDBUtils {
	public static void copyDatabase(Context c, String DATABASE_NAME) {
		String databasePath = c.getDatabasePath(DATABASE_NAME).getPath();
		File f = new File(databasePath);
		OutputStream myOutput = null;
		InputStream myInput = null;
		Log.d("testing", " testing db path " + databasePath);
		Log.d("testing", " testing db exist " + f.exists());

		if (f.exists()) {
			try {

				File directory = new File("/mnt/sdcard/YESBOOKS/BACKUP");
				if (!directory.exists())
					directory.mkdir();

				myOutput = new FileOutputStream(directory.getAbsolutePath() + "/" + DATABASE_NAME);
				myInput = new FileInputStream(databasePath);

				byte[] buffer = new byte[1024];
				int length;
				while ((length = myInput.read(buffer)) > 0) {
					myOutput.write(buffer, 0, length);
				}

				myOutput.flush();
			} catch (Exception e) {
			} finally {
				try {
					if (myOutput != null) {
						myOutput.close();
						myOutput = null;
					}
					if (myInput != null) {
						myInput.close();
						myInput = null;
					}
				} catch (Exception e) {
				}
			}
		}
	}

	@SuppressWarnings({ "resource", "unused" })
	public static void copy_SqliteDB(Context c) {
		try {
			File sd = Environment.getExternalStorageDirectory();
			// File sd =new File("/sdcard/YESBOOKS/BACKUP");
			File data = Environment.getDataDirectory();
			String root = Environment.getExternalStorageDirectory().toString();
			
			if (sd.canWrite()) {
				String currentDBPath = "/data/data/" + c.getPackageName() + "/databases/yesteam_eshakti.db";
				String backupDBPath = "backupname.db";
				File currentDB = new File(currentDBPath);
				File backupDB = new File(sd, backupDBPath);

				if (currentDB.exists()) {
					FileChannel src = new FileInputStream(currentDB).getChannel();
					FileChannel dst = new FileOutputStream(backupDB).getChannel();
					dst.transferFrom(src, 0, src.size());
					src.close();
					dst.close();
				}
			}
		} catch (Exception e) {

		}
	}
}
