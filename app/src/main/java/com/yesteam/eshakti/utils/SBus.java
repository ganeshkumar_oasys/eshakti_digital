package com.yesteam.eshakti.utils;

import com.squareup.otto.Bus;

public enum SBus {

	INST;

	private Bus mBus = new Bus();

	public void register(final Object obj) {
		mBus.register(obj);
	}

	public void post(final Object obj) {
		mBus.post(obj);
	}

	public void unRegister(final Object obj) {
		mBus.unregister(obj);
	}
}
