package com.yesteam.eshakti.utils;

import java.util.Vector;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.yesteam.eshakti.view.fragment.Transaction_AcctoAccTransferFragment;
import com.yesteam.eshakti.view.fragment.Transaction_BankDepositFragment;
import com.yesteam.eshakti.view.fragment.Transaction_DepositEntryFragment;
import com.yesteam.eshakti.view.fragment.Transaction_FixedDepositEntryFragment;
import com.yesteam.eshakti.view.fragment.Transaction_GroupLoanRepaidFragment;
import com.yesteam.eshakti.view.fragment.Transaction_LoanAccountFragment;
import com.yesteam.eshakti.view.fragment.Transaction_SeedFundFragment;
import com.yesteam.eshakti.webservices.SelectedGroupsTask;

import android.util.Log;

public class GetOfflineTransactionValues {
	static int size;
	public static Vector<String> sBankNames = new Vector<String>();
	public static Vector<String> sEngBankNames = new Vector<String>();
	public static Vector<String> sBankAmt = new Vector<String>();

	public static String getBankDetails() {
		String[] bankNames = new String[SelectedGroupsTask.sBankNames.size()];

		size = bankNames.length;
		int i;
		@SuppressWarnings("unused")
		String bankName = Transaction_BankDepositFragment.bankName, sSendToServer_BankName = null,
				sSelected_BankName = null, sSelected_BankDepositAmount = null;
		String mMasterresponse = null, result = null;
		StringBuilder builder = new StringBuilder();
		try {

			for (i = 0; i < size; i++) {
				Log.e("Bank Size Values----->>>>", SelectedGroupsTask.sBankAmt.size()+"");
				sSendToServer_BankName = SelectedGroupsTask.sEngBankNames.elementAt(i).toString();

				sSelected_BankName = SelectedGroupsTask.sBankNames.elementAt(i).toString();
				sSelected_BankDepositAmount = SelectedGroupsTask.sBankAmt.elementAt(i).toString();

				if (EShaktiApplication.isBankDeposit()) {

					if (sSendToServer_BankName.equals(Transaction_BankDepositFragment.sSendToServer_BankName)) {

						sSelected_BankDepositAmount = Transaction_DepositEntryFragment.mCashatBank_individual;
						EShaktiApplication.setBankDeposit(false);
					}
				}

				if (EShaktiApplication.isBankFD()) {

					if (sSendToServer_BankName.equals(Transaction_BankDepositFragment.sSendToServer_BankName)) {

						sSelected_BankDepositAmount = Transaction_FixedDepositEntryFragment.mCashatBank_individual;
						EShaktiApplication.setBankFD(false);
					}

				}

				if (EShaktiApplication.isAcctoaccTransferBank()) {
					if (sSendToServer_BankName.equals(EShaktiApplication.getAcctoaccSendtoserverBank())) {

						sSelected_BankDepositAmount = String
								.valueOf(Transaction_AcctoAccTransferFragment.mFromBankAmount);

					} else if (sSendToServer_BankName.equals(Transaction_AcctoAccTransferFragment.mBankNameValue)) {

						sSelected_BankDepositAmount = String
								.valueOf(Transaction_AcctoAccTransferFragment.mToBankAmount);

					}

				}

				if (EShaktiApplication.isLoanAccToBank()) {

					if (sSendToServer_BankName.equals(Transaction_LoanAccountFragment.mBankNameValue)) {

						int sSelected_BankDepositAmount_int;
						sSelected_BankDepositAmount_int = Integer.parseInt(sSelected_BankDepositAmount);

						sSelected_BankDepositAmount_int = sSelected_BankDepositAmount_int
								+ Integer.parseInt(Transaction_LoanAccountFragment.mWithdrawalValue)
								- Integer.parseInt(Transaction_LoanAccountFragment.mExpensesValue);
						sSelected_BankDepositAmount = String.valueOf(sSelected_BankDepositAmount_int);
						EShaktiApplication.setLoanAccToBank(false);
					}
				}

				if (EShaktiApplication.isSeedFund()) {
					Log.e("Seed fund Bank Name----------________>>>>>>", sSendToServer_BankName);
					Log.e("Seed fund Selected Bank Name---------______________>>>>",
							Transaction_SeedFundFragment.mBankNameValue);
					if (sSendToServer_BankName.equals(Transaction_SeedFundFragment.mBankNameValue)) {
						int sSelected_BankDepositAmount_int;
						sSelected_BankDepositAmount_int = Integer.parseInt(sSelected_BankDepositAmount);
						if (!ConnectionUtils.isNetworkAvailable(EShaktiApplication.getAppContext())) {

							sSelected_BankDepositAmount_int = sSelected_BankDepositAmount_int
									+ Integer.parseInt(Transaction_SeedFundFragment.mWithdrawalValue);
							sSelected_BankDepositAmount = String.valueOf(sSelected_BankDepositAmount_int);
						}

					}

					Log.e("Values ------>>>> Offline", sSelected_BankDepositAmount);

				}

				if (EShaktiApplication.isGroupLoanRepayBank()) {

					if (sSendToServer_BankName.equals(Transaction_GroupLoanRepaidFragment.mBankNameValue)) {
						int sSelected_BankDepositAmount_int;
						sSelected_BankDepositAmount_int = Integer.parseInt(sSelected_BankDepositAmount);

						sSelected_BankDepositAmount_int = sSelected_BankDepositAmount_int
								- (Integer.parseInt(Transaction_GroupLoanRepaidFragment.mRepaymentAmount))
										- (Integer.parseInt(Transaction_GroupLoanRepaidFragment.mBankChargesAmount));

						sSelected_BankDepositAmount = String.valueOf(sSelected_BankDepositAmount_int);
					}

				}

				mMasterresponse = sSendToServer_BankName + "~" + sSelected_BankName + "~" + sSelected_BankDepositAmount
						+ "~";
				builder.append(mMasterresponse);

			}
			EShaktiApplication.setIsSeedFund(false);
			EShaktiApplication.setAcctoaccTransferBank(false);
			result = builder.toString();

			Log.e("Bank Detailsssssssssssss", result);

			String bankInfo[] = result.split("~");
			SelectedGroupsTask.sBankAmt.clear();
			SelectedGroupsTask.sBankNames.clear();
			SelectedGroupsTask.sEngBankNames.clear();
			try {
				/* Here bankinfo length starts from k=0 not as k=4 */
				int j = 0;
				for (int k = 0; k < bankInfo.length; k++) {
					if (j == 2) {
						if (!bankInfo[k].toString().equals("-")) { // "0"
							SelectedGroupsTask.sBankAmt.addElement(bankInfo[k].toString());
							System.out.println("Amount : " + k + " " + bankInfo[k].toString());
						}
					}
					if (j == 1) {
						if (!bankInfo[k].toString().equals("-")) {
							SelectedGroupsTask.sBankNames.addElement(bankInfo[k].toString());
							System.out.println("Regional bankName : " + k + " " + bankInfo[k].toString());
						}

					}
					if (j == 0) {
						if (!bankInfo[k].toString().equals("-")) {
							SelectedGroupsTask.sEngBankNames.addElement(bankInfo[k].toString());
							System.out.println("English bankName : " + k + " " + bankInfo[k].toString());
						}
					}
					j = j + 1;
					if (j > 2) {
						j = 0;
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}
}
