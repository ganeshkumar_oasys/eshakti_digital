package com.yesteam.eshakti.model;

public class NavDrawerItem {
	
	private String mTitle;
	private int mIcon;
	
	public NavDrawerItem() {

	}

	public NavDrawerItem(String title, int icon){
		super();
		this.mTitle = title;
		this.mIcon = icon;
	}
	
	public String getTitle(){
		return mTitle;
	}
	
	public void setTitle(String title){
		this.mTitle = title;
	}
	
	public int getIcon(){
		return mIcon;
	}
	
	public void setIcon(int icon){
		this.mIcon = icon;
	}
}
