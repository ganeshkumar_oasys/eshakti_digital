package com.yesteam.eshakti.model;

public class ListItem {

	private String title;
	private int imageId;
	private boolean isSelected;
	
	public ListItem( String title ,int imageId) {
		this.title = title;
		this.imageId = imageId;  
	}
	
	public int getImageId() {
		return imageId;
	}
	public void setImageId(int imageId) {
		this.imageId = imageId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
