package com.yesteam.eshakti.Config.utils;


import com.yesteam.eshakti.utils.TamilUtil;

public class RegionalConversion {

	static String sRegionalString;

	public static String getRegionalConversion(String string) {

	
			String convertRegional = string;
			sRegionalString = TamilUtil.convertToTamil(TamilUtil.TSCII,
					convertRegional);

		return sRegionalString;

	}
}
