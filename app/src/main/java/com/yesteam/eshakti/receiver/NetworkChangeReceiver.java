package com.yesteam.eshakti.receiver;

import com.yesteam.eshakti.service.OfflineTransactionService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class NetworkChangeReceiver extends BroadcastReceiver {

	@SuppressWarnings("deprecation")
	@Override
	public void onReceive(Context context, Intent intent) {
		ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

		boolean isConnected = wifi != null && wifi.isConnectedOrConnecting()
				|| mobile != null && mobile.isConnectedOrConnecting();

		if (isConnected) {
			Log.e("Yes Network ", "is on");
			Intent intentservice = new Intent(context, OfflineTransactionService.class);
			context.startService(intentservice);
		} else {
			Log.e("No Network", "Is Off");
		}
	}

}