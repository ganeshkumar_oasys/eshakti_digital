package com.yesteam.eshakti.views;

import com.oasys.eshakti.digitization.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.TextView;

public class VerticalTextView extends TextView {

	public final static int ORIENTATION_UP_TO_DOWN = 0;

	Rect text_bounds = new Rect();
	private int direction;

	public VerticalTextView(Context context) {
		super(context);
	}

	public VerticalTextView(Context context, AttributeSet attrs) {
		super(context, attrs);

		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.verticaltextview);
		direction = a.getInt(R.styleable.verticaltextview_direction, 0);
		a.recycle();

		requestLayout();
		invalidate();

	}

	public void setDirection(int direction) {
		this.direction = direction;

		requestLayout();
		invalidate();
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		getPaint().getTextBounds(getText().toString(), 0, getText().length(), text_bounds);
		if (direction == ORIENTATION_UP_TO_DOWN) {
			setMeasuredDimension(measureWidth(widthMeasureSpec), measureHeight(heightMeasureSpec));
		}

	}

	private int measureWidth(int measureSpec) {
		int result = 0;
		int specMode = MeasureSpec.getMode(measureSpec);
		int specSize = MeasureSpec.getSize(measureSpec);

		if (specMode == MeasureSpec.EXACTLY) {
			result = specSize;
		} else {
			result = text_bounds.height() + getPaddingTop() + getPaddingBottom();
			// result = text_bounds.height();
			if (specMode == MeasureSpec.AT_MOST) {
				result = Math.min(result, specSize);
			}
		}
		return result;
	}

	private int measureHeight(int measureSpec) {
		int result = 0;
		int specMode = MeasureSpec.getMode(measureSpec);
		int specSize = MeasureSpec.getSize(measureSpec);

		if (specMode == MeasureSpec.EXACTLY) {
			result = specSize;
		} else {
			result = text_bounds.width() + getPaddingLeft() + getPaddingRight();
			// result = text_bounds.width();
			if (specMode == MeasureSpec.AT_MOST) {
				result = Math.min(result, specSize);
			}
		}
		return result;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		// super.onDraw(canvas);

		canvas.save();

		int startX = 0;
		int startY = 0;
		int stopX = 0;
		int stopY = 0;
		Path path = new Path();
		if (direction == ORIENTATION_UP_TO_DOWN) {
			startX = (getWidth() - text_bounds.height() >> 1);
			startY = (getHeight() - text_bounds.width() >> 1);
			stopX = (getWidth() - text_bounds.height() >> 1);
			stopY = (getHeight() + text_bounds.width() >> 1);
			path.moveTo(startX, startY);
			path.lineTo(stopX, stopY);
		}
		this.getPaint().setColor(this.getCurrentTextColor());
		// canvas.drawLine(startX, startY, stopX, stopY, this.getPaint());
		canvas.drawTextOnPath(getText().toString(), path, 0, 0, this.getPaint());

		canvas.restore();
	}

	@Override
	public void setText(CharSequence text, BufferType type) {
		super.setText(text, type);
		invalidate();
	}

	@Override
	public void setBackground(Drawable background) {
		// TODO Auto-generated method stub
		super.setBackground(background);
		invalidate();

	}
}
