package com.yesteam.eshakti.transaction.response;

import com.yesteam.eshakti.sqlite.database.response.TransactionResult;

public class BaseResponse implements ITransactionResponse {

	private final TransactionResult mFetcherResult;

	protected BaseResponse(final TransactionResult mFetcherResult) {
		this.mFetcherResult = mFetcherResult;
	}

	public TransactionResult getFetcherResult() {
		return mFetcherResult;
	}
}
