package com.yesteam.eshakti.webservices;

import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;
import com.yesteam.eshakti.utils.CryptographyUtils;
import com.yesteam.eshakti.utils.GetResponseInfo;
import com.yesteam.eshakti.view.fragment.Transaction_LoanDisbursementFromRepaidFragment;
import com.yesteam.eshakti.view.fragment.Transaction_LoanDisbursement_LoansMenuFragment;
import com.yesteam.eshakti.view.fragment.Transaction_Loan_SB_disbursementFragment;

import android.os.AsyncTask;
import android.util.Log;

public class GetAddLoan_Installment_CCWebservice extends AsyncTask<String, String, String>{

	private TaskListener mListener;

	public static String mGetLoanLimit = null;

	public GetAddLoan_Installment_CCWebservice(TaskListener listener) {
		// TODO Auto-generated constructor stub
		mListener = listener;
	}
	
	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub

		SoapObject request = new SoapObject(LoginTask.NAMESPACE,
				SOAP_OperationConstants.OPERATION_NAME_GET_ADDLOAN_INSTALLMENT_CC);

		try {

			request.addProperty("Ngo_Id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Ngo_Id)));
			request.addProperty("Group_Id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Group_Id)));
			request.addProperty("Loan_Id",
					CryptographyUtils.Encrypt(Transaction_LoanDisbursement_LoansMenuFragment.mSendTo_ServerLoan_Id));
			request.addProperty("Loan_Disbursement_Date",
					CryptographyUtils.Encrypt(Transaction_LoanDisbursementFromRepaidFragment.disbursement_Date));
			request.addProperty("Loan_Disbursement_Amount",
					CryptographyUtils.Encrypt(Transaction_LoanDisbursementFromRepaidFragment.disbursementAmount));
			request.addProperty("Values", CryptographyUtils
					.Encrypt(Transaction_Loan_SB_disbursementFragment.sSendToServer_InternalLoanDisbursement));
			request.addProperty("Tr_Date", CryptographyUtils.Encrypt(DatePickerDialog.sSend_To_Server_Date));
			request.addProperty("Trans_Type",
					CryptographyUtils.Encrypt(Transaction_LoanDisbursementFromRepaidFragment.selectedType));
			if (Transaction_LoanDisbursementFromRepaidFragment.selectedType != null
					&& Transaction_LoanDisbursementFromRepaidFragment.selectedType.equals("Bank")) {
				request.addProperty("SB_BankName",
						CryptographyUtils.Encrypt(Transaction_LoanDisbursementFromRepaidFragment.mBankNameValue));

			} else {
				request.addProperty("SB_BankName", CryptographyUtils.Encrypt(""));

			}

			Log.e("Request---------------->", request.toString());
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

		envelope.setOutputSoapObject(request);

		envelope.dotNet = true;

		try {

			HttpTransportSE http_se = new HttpTransportSE(LoginTask.SOAP_ADDRESS);

			http_se.call(LoginTask.NAMESPACE + SOAP_OperationConstants.OPERATION_NAME_GET_ADDLOAN_INSTALLMENT_CC,
					envelope);

			Object result = envelope.getResponse();

			mGetLoanLimit = CryptographyUtils.Decrypt(result.toString());

			String[] sResponseUpdate = mGetLoanLimit.split("#");

			if (sResponseUpdate[0].equals("Yes")) {

				GetResponseInfo.getResponseInfo_ExistingLoan(mGetLoanLimit);

			}

			System.out.println("Service Response:" + mGetLoanLimit);
			Constants.NETWORKCOMMONFLAG = "SUCCESS";
		} catch (IOException e) {
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (XmlPullParserException e) {
			// TODO: handle exception
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		}

		return Constants.NETWORKCOMMONFLAG;

	}
	
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		mListener.onTaskStarted();
	}
	
	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		if (result == null) {
			result = Constants.EXCEPTION;
			mListener.onTaskFinished(result);
		} else {
			mListener.onTaskFinished(result);
		}
	}

}
