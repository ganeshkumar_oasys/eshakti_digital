package com.yesteam.eshakti.webservices;

import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;
import com.yesteam.eshakti.utils.CryptographyUtils;
import com.yesteam.eshakti.utils.GetIndividualInfo;
import com.yesteam.eshakti.utils.GetResponseInfo;
import com.yesteam.eshakti.view.fragment.Transaction_MemLoanRepaidFragment;
import com.yesteam.eshakti.view.fragment.Transaction_MemLoanRepaid_MenuFragment;
import com.yesteam.eshakti.view.fragment.Transaction_PersonalLoanRepaidFragment;

import android.os.AsyncTask;
import android.util.Log;

public class GetMember_Loan_RepaidTask extends AsyncTask<String, String, String> {

	public static String sGetMember_Loan_RepaidTask_Response = null;

	private TaskListener mListener;

	String mTag = GetMember_Loan_RepaidTask.class.getSimpleName();

	public GetMember_Loan_RepaidTask(TaskListener listener) {
		// TODO Auto-generated constructor stub
		this.mListener = listener;
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub

		String sendLoanInfo = GetIndividualInfo.getLoanInfo();

		String memberID = GetIndividualInfo.getMemberID_Info();

		SoapObject request = new SoapObject(LoginTask.NAMESPACE,
				SOAP_OperationConstants.OPERATION_NAME_GET_MEMBER_LOAN_REPAID);

		try {
			request.addProperty("User_Name", CryptographyUtils.Encrypt(String.valueOf(LoginTask.UserName)));
			request.addProperty("Ngo_id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Ngo_Id)));
			request.addProperty("Group_id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Group_Id)));
			request.addProperty("Loan_Type",
					CryptographyUtils.Encrypt(String.valueOf(Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID))); // LoanId
			// of
			// the
			// Selected

			// loan
			if (Transaction_MemLoanRepaidFragment.mServerService) {
				request.addProperty("Values",
						CryptographyUtils.Encrypt(String.valueOf(Transaction_MemLoanRepaidFragment.sSendToServer_MemLoanRepaid)));
				Log.e("Values", CryptographyUtils.Encrypt(Transaction_MemLoanRepaidFragment.sSendToServer_MemLoanRepaid));
			} else {
				request.addProperty("Values", CryptographyUtils
						.Encrypt(String.valueOf(Transaction_PersonalLoanRepaidFragment.sSendToServer_MemLoanRepaid)));
				Log.e("Values", CryptographyUtils.Encrypt(Transaction_PersonalLoanRepaidFragment.sSendToServer_MemLoanRepaid));

			}
			request.addProperty("Member_id", CryptographyUtils.Encrypt(memberID));
			request.addProperty("Loanid", CryptographyUtils.Encrypt(sendLoanInfo));
			request.addProperty("Trdate", CryptographyUtils.Encrypt(DatePickerDialog.sSend_To_Server_Date)); // CalendarDate
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.setOutputSoapObject(request);
		envelope.dotNet = true;

		HttpTransportSE httpSe = new HttpTransportSE(LoginTask.SOAP_ADDRESS);
		try {
			httpSe.call(LoginTask.NAMESPACE + SOAP_OperationConstants.OPERATION_NAME_GET_MEMBER_LOAN_REPAID, envelope);

			Object result = envelope.getResponse();

			sGetMember_Loan_RepaidTask_Response = CryptographyUtils.Decrypt(result.toString());

			GetResponseInfo.getResponseInfo(sGetMember_Loan_RepaidTask_Response);
			Log.e("PLREPAID", sGetMember_Loan_RepaidTask_Response);

			Constants.NETWORKCOMMONFLAG = "SUCCESS";

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		}

		return Constants.NETWORKCOMMONFLAG;
	}

	@Override
	protected void onPreExecute() {
		mListener.onTaskStarted();
	}

	@Override
	protected void onPostExecute(String result) {
		if (result == null) {
			result = Constants.EXCEPTION;
			mListener.onTaskFinished(result);
		} else {
			mListener.onTaskFinished(result);
		}

	}
}
