package com.yesteam.eshakti.webservices;

import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;
import com.yesteam.eshakti.utils.CryptographyUtils;
import com.yesteam.eshakti.view.fragment.Meeting_AuditingFragment;

import android.os.AsyncTask;
import android.util.Log;

public class Get_AuditTask extends AsyncTask<String, String, String> {
	private TaskListener mListener;

	String mTag = Get_AuditTask.class.getSimpleName();

	public static String get_Audit_Response = null;

	public Get_AuditTask(TaskListener listener) {
		// TODO Auto-generated method stub
		this.mListener = listener;
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub

		try {
			SoapObject request = new SoapObject(LoginTask.NAMESPACE, SOAP_OperationConstants.OPERATION_NAME_GET_AUDIT);

			request.addProperty("Ngo_id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Ngo_Id)));
			request.addProperty("Group_id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Group_Id)));
			request.addProperty("From_Date", CryptographyUtils.Encrypt(String.valueOf(Meeting_AuditingFragment.auditFromDate)));
			request.addProperty("To_Date", CryptographyUtils.Encrypt(String.valueOf(Meeting_AuditingFragment.auditToDate)));
			request.addProperty("Audit_Date", CryptographyUtils.Encrypt(String.valueOf(Meeting_AuditingFragment.auditingDate)));
			request.addProperty("Audit_Name", CryptographyUtils.Encrypt(String.valueOf(Meeting_AuditingFragment.auditorName)));
			request.addProperty("Trdate",
					CryptographyUtils.Encrypt(String.valueOf(DatePickerDialog.sSend_To_Server_Date)));
			Log.e("SERVICE REQUEST - Get_AuditTask:", request + "");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

			envelope.setOutputSoapObject(request);

			envelope.dotNet = true;

			try {

				HttpTransportSE http_se = new HttpTransportSE(LoginTask.SOAP_ADDRESS);

				http_se.call(LoginTask.NAMESPACE + SOAP_OperationConstants.OPERATION_NAME_GET_AUDIT, envelope);

				Object result = envelope.getResponse();

				String getResponse = CryptographyUtils.Decrypt(result.toString());
				String[] responseArray = getResponse.split("[#]");
				
				get_Audit_Response = responseArray[0];
				SelectedGroupsTask.sLastTransactionDate_Response = responseArray[1];

				System.out.println("Service Response:" + get_Audit_Response);

				Constants.NETWORKCOMMONFLAG = "SUCCESS";

			} catch (IOException e) {
				e.printStackTrace();
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			} catch (XmlPullParserException e) {
				// TODO: handle exception
				e.printStackTrace();
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return Constants.NETWORKCOMMONFLAG;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		mListener.onTaskStarted();
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		if (result == null) {
			result = Constants.EXCEPTION;
			mListener.onTaskFinished(result);
		} else {
			mListener.onTaskFinished(result);
		}
	}
}
