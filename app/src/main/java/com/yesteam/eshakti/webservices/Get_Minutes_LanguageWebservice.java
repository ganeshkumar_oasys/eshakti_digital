package com.yesteam.eshakti.webservices;

import java.io.IOException;
import java.net.ConnectException;

import org.apache.http.conn.ConnectTimeoutException;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.utils.CryptographyUtils;
import com.yesteam.eshakti.utils.PrefUtils;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class Get_Minutes_LanguageWebservice extends
		AsyncTask<String, String, String> {

	private TaskListener mListener;

	String mTag = LoginTask.class.getSimpleName();

	public static String NAMESPACE = "";

	public static String SOAP_ADDRESS = "";

	public static String sMinutesTask_Response = null;

	public static String UserName = null;
	public static String Ngo_Id = null;
	public static String Trainer_Id = null;
	public static String User_RegLanguage = null;
	static String mUserType = null;

	Context context;

	public Get_Minutes_LanguageWebservice(TaskListener listener) {
		this.mListener = listener;
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub

		try {

			SoapObject request = new SoapObject(LoginTask.NAMESPACE,
					SOAP_OperationConstants.OPERATION_NAME_GET_MINUTES_LANGUAGE);

			request.addProperty("ngo_id",
					CryptographyUtils.Encrypt(SelectedGroupsTask.Ngo_Id));
			request.addProperty("Language",
					CryptographyUtils.Encrypt(PrefUtils.getUserlangcode()));

			Log.e(mTag, request.toString() + "");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.setOutputSoapObject(request);
			envelope.dotNet = true;

			HttpTransportSE httpSe = new HttpTransportSE(LoginTask.SOAP_ADDRESS);

			try {
				httpSe.call(
						LoginTask.NAMESPACE
								+ SOAP_OperationConstants.OPERATION_NAME_GET_MINUTES_LANGUAGE,
						envelope);

				Object result = envelope.getResponse();
				sMinutesTask_Response = CryptographyUtils.Decrypt(result
						.toString());

				publicValues.mGetMinutesLanguageValues = sMinutesTask_Response;

				Constants.NETWORKCOMMONFLAG = "SUCCESS";

			} catch (ArrayIndexOutOfBoundsException e) {
				e.printStackTrace();
			} catch (ConnectTimeoutException e) {
				// TODO: handle exception
				e.printStackTrace();
				mListener.onTaskFinished(Constants.EXCEPTION);
				Log.e("TIME OUT ", "Success");
				Constants.NETWORKCOMMONFLAG = "FAIL";
			} catch (ConnectException e) {
				e.printStackTrace();
				Log.v("CONNECT EXCEPTION ", "Success");
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			} catch (XmlPullParserException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			}

		} catch (Exception e) {
			e.printStackTrace();
			Log.v("Yes!!!!!!!", "!!!!!!!!!");
		}

		return Constants.NETWORKCOMMONFLAG;
	}

	@Override
	protected void onPreExecute() {
		mListener.onTaskStarted();
	}

	@Override
	protected void onPostExecute(String result) {
		if (result == null) {
			result = Constants.EXCEPTION;

			mListener.onTaskFinished(result);
		} else {
			mListener.onTaskFinished(result);
		}
	}
}
