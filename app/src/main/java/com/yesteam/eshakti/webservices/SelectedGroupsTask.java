package com.yesteam.eshakti.webservices;

import java.io.IOException;
import java.util.Vector;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.sqlite.db.model.GetGroupMemberDetails;
import com.yesteam.eshakti.sqlite.db.model.LoginCheck;
import com.yesteam.eshakti.utils.ConnectionUtils;
import com.yesteam.eshakti.utils.CryptographyUtils;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.view.activity.GroupListActivity;
import com.yesteam.eshakti.view.activity.LoginActivity;

import android.os.AsyncTask;
import android.util.Log;

public class SelectedGroupsTask extends AsyncTask<String, String, String> {

    private TaskListener mListener;

    public static String sSelectedGroupTask_Response = null;
    public static String sLastTransactionDate_Response = null;
    public static String sBalanceSheetDate_Response = null;

    public static String sCashinHand = "";
    public static String sCashatBank = "";

    public static String UserName = null;
    public static String Ngo_Id = null;
    public static String Group_Id = null;
    public static String Group_Name = null;
    public static String sTr_ID = null;
    public static String sTypeID = null;
    public static String sLoanAccDetails = null;

    public static Vector<String> sBankNames = new Vector<String>();
    public static Vector<String> sEngBankNames = new Vector<String>();
    public static Vector<String> sBankAmt = new Vector<String>();
    public static Vector<String> member_Id = new Vector<String>();
    public static Vector<String> member_Name = new Vector<String>();
    public static Vector<String> loan_Id = new Vector<String>();
    public static Vector<String> loan_Name = new Vector<String>();
    public static Vector<String> loan_EngName = new Vector<String>();

    public static Vector<String> loanAcc_loanId = new Vector<String>();
    public static Vector<String> loanAcc_loanEngName = new Vector<String>();
    public static Vector<String> loanAcc_loanRegName = new Vector<String>();
    public static Vector<String> loanAcc_bankName = new Vector<String>();
    public static Vector<String> loanAcc_loanAccNo = new Vector<String>();
    public static Vector<String> loanAcc_loanDisbursementDate = new Vector<String>();

    private String mTag = SelectedGroupsTask.class.getSimpleName();

    public SelectedGroupsTask(TaskListener listener) {
        // TODO Auto-generated constructor stub
        this.mListener = listener;

        UserName = "";
        Ngo_Id = "";
        Group_Id = "";
        sTr_ID = "";
        sLoanAccDetails = "";

        sBankNames.clear();

        sEngBankNames.clear();
        sBankAmt.clear();
        member_Id.clear();
        member_Name.clear();
        loan_Id.clear();
        loan_Name.clear();
        loan_EngName.clear();

        loanAcc_loanId.clear();
        loanAcc_loanEngName.clear();
        loanAcc_loanRegName.clear();
        loanAcc_bankName.clear();
        loanAcc_loanAccNo.clear();
        loanAcc_loanDisbursementDate.clear();

        sBankAmt = new Vector<String>();
        sEngBankNames = new Vector<String>();
        sBankAmt = new Vector<String>();
        member_Id = new Vector<String>();
        member_Name = new Vector<String>();
        loan_Id = new Vector<String>();
        loan_Name = new Vector<String>();
        loan_EngName = new Vector<String>();

        loanAcc_loanId = new Vector<String>();
        loanAcc_loanEngName = new Vector<String>();
        loanAcc_loanRegName = new Vector<String>();
        loanAcc_bankName = new Vector<String>();
        loanAcc_loanAccNo = new Vector<String>();
        loanAcc_loanDisbursementDate = new Vector<String>();

    }

    @Override
    protected String doInBackground(String... params) {
        // TODO Auto-generated method stub

        SoapObject request = new SoapObject(LoginTask.NAMESPACE,
                SOAP_OperationConstants.OPERATION_NAME_SELECTED_GROUPS);

        System.out.println("NAMESPACE : " + LoginTask.NAMESPACE);

        System.out.println(LoginTask.UserName + "  " + LoginTask.Ngo_Id + "   " + LoginTask.Trainer_Id + "   "
                + GroupListActivity.sSelectedGroup);

        try {

            if (PrefUtils.getGrouplistValues() != null && PrefUtils.getGrouplistValues().equals("1")
                    && GroupListActivity.isGetOfflineData) {

                request.addProperty("UserName", CryptographyUtils.Encrypt(String.valueOf(LoginActivity.sUName)));
                request.addProperty("Ngo_id", CryptographyUtils.Encrypt(String.valueOf(LoginCheck.getUserNgoId())));
                request.addProperty("Trainer_id", CryptographyUtils.Encrypt(String.valueOf(LoginCheck.getTrainerId())));
                request.addProperty("Group_id",
                        CryptographyUtils.Encrypt(String.valueOf(GroupListActivity.sSelectedGroup)));
                request.addProperty("Latitude", CryptographyUtils.Encrypt(String.valueOf(LoginActivity.sLatitude)));
                request.addProperty("Longitude", CryptographyUtils.Encrypt(String.valueOf(LoginActivity.sLongitude)));

            } else {

                request.addProperty("UserName", CryptographyUtils.Encrypt(String.valueOf(LoginTask.UserName)));
                request.addProperty("Ngo_id", CryptographyUtils.Encrypt(String.valueOf(LoginTask.Ngo_Id)));
                request.addProperty("Trainer_id", CryptographyUtils.Encrypt(String.valueOf(LoginTask.Trainer_Id)));
                request.addProperty("Group_id",
                        CryptographyUtils.Encrypt(String.valueOf(GroupListActivity.sSelectedGroup)));
                request.addProperty("Latitude", CryptographyUtils.Encrypt(String.valueOf(LoginActivity.sLatitude)));
                request.addProperty("Longitude", CryptographyUtils.Encrypt(String.valueOf(LoginActivity.sLongitude)));
            }
        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        Log.e("Selected Group Task", request.toString() + "");
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.setOutputSoapObject(request);
        envelope.dotNet = true;

        HttpTransportSE httpSe = new HttpTransportSE(LoginTask.SOAP_ADDRESS);
        try {
            httpSe.call(LoginTask.NAMESPACE + SOAP_OperationConstants.OPERATION_NAME_SELECTED_GROUPS, envelope);

            Object result = envelope.getResponse();

            sSelectedGroupTask_Response = CryptographyUtils.Decrypt(String.valueOf(result));
            Log.d(mTag, sSelectedGroupTask_Response);

            if (sSelectedGroupTask_Response.equals("LoginName or password incorrect.")) {
                // sSelectedGroupTask_Response = String.valueOf(result);
            } else {

                getGroupDetails(sSelectedGroupTask_Response);
                Constants.NETWORKCOMMONFLAG = "SUCCESS";
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            mListener.onTaskFinished(Constants.EXCEPTION);
            Constants.NETWORKCOMMONFLAG = "FAIL";
        } catch (XmlPullParserException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            mListener.onTaskFinished(Constants.EXCEPTION);
            Constants.NETWORKCOMMONFLAG = "FAIL";
        } catch (Exception e) {
            // TODO: handle exception
            mListener.onTaskFinished(Constants.EXCEPTION);
            Constants.NETWORKCOMMONFLAG = "FAIL";
        }

        return Constants.NETWORKCOMMONFLAG;
    }

    @Override
    protected void onPreExecute() {
        mListener.onTaskStarted();
    }

    @Override
    protected void onPostExecute(String result) {

        if (result == null) {
            result = Constants.EXCEPTION;
            mListener.onTaskFinished(result);
        } else {
            mListener.onTaskFinished(result);
        }

    }

    public static void getGroupDetails(String string) throws IndexOutOfBoundsException, RuntimeException, IOException {
        if (ConnectionUtils.isNetworkAvailable(EShaktiApplication.getInstance())) {

            if (PrefUtils.getGrouplistValues() != null && PrefUtils.getGrouplistValues().equals("1")
                    && GroupListActivity.isGetOfflineData && GetGroupMemberDetails.getGroupId() != null) {

                try {

                    UserName = "";
                    Ngo_Id = "";
                    Group_Id = "";
                    sTr_ID = "";

                    sBankNames.clear();

                    sEngBankNames.clear();
                    sBankAmt.clear();
                    member_Id.clear();
                    member_Name.clear();
                    loan_Id.clear();
                    loan_Name.clear();
                    loan_EngName.clear();

                    loanAcc_loanId.clear();
                    loanAcc_loanEngName.clear();
                    loanAcc_loanRegName.clear();
                    loanAcc_bankName.clear();
                    loanAcc_loanAccNo.clear();
                    loanAcc_loanDisbursementDate.clear();

                    loanAcc_loanId = new Vector<String>();
                    loanAcc_loanEngName = new Vector<String>();
                    loanAcc_loanRegName = new Vector<String>();
                    loanAcc_bankName = new Vector<String>();
                    loanAcc_loanAccNo = new Vector<String>();
                    loanAcc_loanDisbursementDate = new Vector<String>();

                    sBankAmt = new Vector<String>();
                    sEngBankNames = new Vector<String>();
                    sBankAmt = new Vector<String>();
                    member_Id = new Vector<String>();
                    member_Name = new Vector<String>();
                    loan_Id = new Vector<String>();
                    loan_Name = new Vector<String>();
                    loan_EngName = new Vector<String>();

                    Group_Name = "";

                    Log.d("HANDLING GROUP ", "RESPONSE ");

                    Log.e("Offline Data Values>>>>", string);

                    String response[] = string.split("#");

                    /** TEST **/
                    for (int i = 0; i < response.length; i++) {
                        System.out.println("Response Array : " + response[i] + " i POS : " + i);
                    }

                    SelectedGroupsTask.UserName = String.valueOf(response[0]);
                    System.out.println("SelectedGroupsTask.UserName : " + SelectedGroupsTask.UserName);

                    SelectedGroupsTask.Ngo_Id = String.valueOf(response[1]);
                    System.out.println("SelectedGroupsTask.Ngo_Id : " + SelectedGroupsTask.Ngo_Id);

                    SelectedGroupsTask.Group_Id = String.valueOf(response[2]);
                    System.out.println("SelectedGroupsTask.Group_Id : " + SelectedGroupsTask.Group_Id);

                    SelectedGroupsTask.sTypeID = String.valueOf(response[2]);
                    System.out.println("SelectedGroupsTask.sTypeID : " + SelectedGroupsTask.sTypeID);

                    SelectedGroupsTask.Group_Name = String.valueOf(response[3]);
                    System.out.println("SelectedGroupsTask.Group_Name : " + SelectedGroupsTask.Group_Name);

                    String[] memberInfo = String.valueOf(response[4]).split("~");
                    int count = 0;

                    try {
                        for (int i = 0; i < memberInfo.length; i++) {

                            if ((i % 2 == 0) && (count == 0)) {
                                System.out.println("Memeber ID : " + memberInfo[i]);
                                SelectedGroupsTask.member_Id.add(String.valueOf(memberInfo[i]));
                                count = 1;
                            } else if (count == 1) {
                                System.out.println("Member Name : " + memberInfo[i]);
                                SelectedGroupsTask.member_Name.add(String.valueOf(memberInfo[i]));
                                count = 0;
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    String loanInfo[] = String.valueOf(response[5]).split("~");
                    int loanCount = 0;

                    try {

                        for (int i = 0; i < loanInfo.length; i++) {
                            if (loanCount == 0) {
                                System.out.println("Loan ID  : " + loanInfo[i]);
                                SelectedGroupsTask.loan_Id.add(String.valueOf(loanInfo[i]));
                                loanCount = 1;
                            } else if (loanCount == 1) {
                                System.out.println("Loan ENGLISH Name : " + loanInfo[i]);
                                SelectedGroupsTask.loan_EngName.add(String.valueOf(loanInfo[i]));
                                loanCount = 2;
                            } else if (loanCount == 2) {
                                System.out.println("Loan Regional Name : " + loanInfo[i]);
                                SelectedGroupsTask.loan_Name.add(String.valueOf(loanInfo[i]));
                                loanCount = 0;
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    SelectedGroupsTask.sLastTransactionDate_Response = String.valueOf(response[6]);
                    System.out.println("Last Transaction Date : " + SelectedGroupsTask.sLastTransactionDate_Response);

                    SelectedGroupsTask.sBalanceSheetDate_Response = String.valueOf(response[7]);
                    System.out.println("Balance Sheet Date : " + SelectedGroupsTask.sBalanceSheetDate_Response);

                    String bankInfo[] = response[8].split("~");

                    Log.e("Selected Group Task ", response[8]);

                    SelectedGroupsTask.sCashinHand = String.valueOf(bankInfo[1]);
                    System.out.println("CASH IN HAND : " + SelectedGroupsTask.sCashinHand);

                    SelectedGroupsTask.sCashatBank = String.valueOf(bankInfo[3]);
                    System.out.println("CASH AT BANK : " + SelectedGroupsTask.sCashatBank);

                    try {

                        int j = 0;
                        for (int i = 4; i < bankInfo.length; i++) {
                            if (j == 2) {
                                if (!bankInfo[i].toString().equals("-")) { // "0"
                                    SelectedGroupsTask.sBankAmt.addElement(bankInfo[i].toString());
                                    System.out.println("Amount : " + i + " " + bankInfo[i].toString());
                                }
                            }
                            if (j == 1) {
                                if (!bankInfo[i].toString().equals("-")) {
                                    SelectedGroupsTask.sBankNames.addElement(bankInfo[i].toString());
                                    System.out.println("Regional bankName : " + i + " " + bankInfo[i].toString());
                                }

                            }
                            if (j == 0) {
                                if (!bankInfo[i].toString().equals("-")) {
                                    SelectedGroupsTask.sEngBankNames.addElement(bankInfo[i].toString());
                                    System.out.println("English bankName : " + i + " " + bankInfo[i].toString());
                                }
                            }
                            j = j + 1;
                            if (j > 2) {
                                j = 0;
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    SelectedGroupsTask.sTr_ID = String.valueOf(response[9]);
                    System.out.println("TRANSACTION ID : " + SelectedGroupsTask.sTr_ID);

                    SelectedGroupsTask.sLoanAccDetails = String.valueOf(response[10]);
                    System.out.println("LOAN ACCOUNT DETAILS : " + SelectedGroupsTask.sLoanAccDetails);

                    if (!SelectedGroupsTask.sLoanAccDetails.equals("No")) {
                        String[] loanAcc_info = SelectedGroupsTask.sLoanAccDetails.split("[%]");
                        Log.e("loanAcc_info size", loanAcc_info.length + "");
                        for (int i = 0; i < loanAcc_info.length; i++) {
                            String indv_loanAccDetails = loanAcc_info[i];
                            String[] indv_loanAcc_info = indv_loanAccDetails.split("~");

                            SelectedGroupsTask.loanAcc_loanId.addElement(indv_loanAcc_info[0].toString());
                            SelectedGroupsTask.loanAcc_loanEngName.addElement(indv_loanAcc_info[1].toString());
                            SelectedGroupsTask.loanAcc_loanRegName.addElement(indv_loanAcc_info[2].toString());
                            SelectedGroupsTask.loanAcc_bankName.addElement(indv_loanAcc_info[3].toString());
                            SelectedGroupsTask.loanAcc_loanAccNo.addElement(indv_loanAcc_info[4].toString());
                            SelectedGroupsTask.loanAcc_loanDisbursementDate.addElement(indv_loanAcc_info[5].toString());
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {



                    try {

                        String response[] = string.split("#");

                        /** TEST **/
                        for (int i = 0; i < response.length; i++) {
                            System.out.println("Response Array : " + response[i] + " i POS : " + i);
                        }

                        SelectedGroupsTask.UserName = String.valueOf(response[0]);
                        System.out.println("SelectedGroupsTask.UserName : " + SelectedGroupsTask.UserName);

                        SelectedGroupsTask.Ngo_Id = String.valueOf(response[1]);
                        System.out.println("SelectedGroupsTask.Ngo_Id : " + SelectedGroupsTask.Ngo_Id);

                        SelectedGroupsTask.Group_Id = String.valueOf(response[2]);
                        System.out.println("SelectedGroupsTask.Group_Id : " + SelectedGroupsTask.Group_Id);

                        String[] memberInfo = String.valueOf(response[3]).split("~");
                        int count = 0;

                        try {
                            for (int i = 0; i < memberInfo.length; i++) {

                                if ((i % 2 == 0) && (count == 0)) {
                                    System.out.println("Memeber ID : " + memberInfo[i]);
                                    SelectedGroupsTask.member_Id.add(String.valueOf(memberInfo[i]));
                                    count = 1;
                                } else if (count == 1) {
                                    System.out.println("Member Name : " + memberInfo[i]);
                                    SelectedGroupsTask.member_Name.add(String.valueOf(memberInfo[i]));
                                    count = 0;
                                }
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        String loanInfo[] = String.valueOf(response[4]).split("~");
                        int loanCount = 0;

                        try {

                            for (int i = 0; i < loanInfo.length; i++) {
                                if (loanCount == 0) {
                                    System.out.println("Loan ID  : " + loanInfo[i]);
                                    SelectedGroupsTask.loan_Id.add(String.valueOf(loanInfo[i]));
                                    loanCount = 1;
                                } else if (loanCount == 1) {
                                    System.out.println("Loan ENGLISH Name : " + loanInfo[i]);
                                    SelectedGroupsTask.loan_EngName.add(String.valueOf(loanInfo[i]));
                                    loanCount = 2;
                                } else if (loanCount == 2) {
                                    System.out.println("Loan Regional Name : " + loanInfo[i]);
                                    SelectedGroupsTask.loan_Name.add(String.valueOf(loanInfo[i]));
                                    loanCount = 0;
                                }
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        SelectedGroupsTask.sLastTransactionDate_Response = String.valueOf(response[5]);
                        System.out
                                .println("Last Transaction Date : " + SelectedGroupsTask.sLastTransactionDate_Response);

                        SelectedGroupsTask.sBalanceSheetDate_Response = String.valueOf(response[6]);
                        System.out.println("Balance Sheet Date : " + SelectedGroupsTask.sBalanceSheetDate_Response);

                        String bankInfo[] = response[7].split("~");

                        SelectedGroupsTask.sCashinHand = String.valueOf(bankInfo[1]);
                        System.out.println("CASH IN HAND : " + SelectedGroupsTask.sCashinHand);

                        SelectedGroupsTask.sCashatBank = String.valueOf(bankInfo[3]);
                        System.out.println("CASH AT BANK : " + SelectedGroupsTask.sCashatBank);

                        try {

                            int j = 0;
                            for (int i = 4; i < bankInfo.length; i++) {
                                if (j == 2) {
                                    if (!bankInfo[i].toString().equals("-")) { // "0"
                                        SelectedGroupsTask.sBankAmt.addElement(bankInfo[i].toString());
                                        System.out.println("Amount : " + i + " " + bankInfo[i].toString());
                                    }
                                }
                                if (j == 1) {
                                    if (!bankInfo[i].toString().equals("-")) {
                                        SelectedGroupsTask.sBankNames.addElement(bankInfo[i].toString());
                                        System.out.println("Regional bankName : " + i + " " + bankInfo[i].toString());
                                    }

                                }
                                if (j == 0) {
                                    if (!bankInfo[i].toString().equals("-")) {
                                        SelectedGroupsTask.sEngBankNames.addElement(bankInfo[i].toString());
                                        System.out.println("English bankName : " + i + " " + bankInfo[i].toString());
                                    }
                                }
                                j = j + 1;
                                if (j > 2) {
                                    j = 0;
                                }
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        SelectedGroupsTask.sTr_ID = String.valueOf(response[8]);
                        System.out.println("TRANSACTION ID : " + SelectedGroupsTask.sTr_ID);

                        SelectedGroupsTask.sLoanAccDetails = String.valueOf(response[9]);
                        System.out.println("LOAN ACCOUNT DETAILS : " + SelectedGroupsTask.sLoanAccDetails);

                        if (!SelectedGroupsTask.sLoanAccDetails.equals("No")) {
                            String[] loanAcc_info = SelectedGroupsTask.sLoanAccDetails.split("[%]");
                            Log.e("loanAcc_info size", loanAcc_info.length + "");
                            for (int i = 0; i < loanAcc_info.length; i++) {
                                String indv_loanAccDetails = loanAcc_info[i];
                                String[] indv_loanAcc_info = indv_loanAccDetails.split("~");

                                SelectedGroupsTask.loanAcc_loanId.addElement(indv_loanAcc_info[0].toString());
                                SelectedGroupsTask.loanAcc_loanEngName.addElement(indv_loanAcc_info[1].toString());
                                SelectedGroupsTask.loanAcc_loanRegName.addElement(indv_loanAcc_info[2].toString());
                                SelectedGroupsTask.loanAcc_bankName.addElement(indv_loanAcc_info[3].toString());
                                SelectedGroupsTask.loanAcc_loanAccNo.addElement(indv_loanAcc_info[4].toString());
                                SelectedGroupsTask.loanAcc_loanDisbursementDate
                                        .addElement(indv_loanAcc_info[5].toString());
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }



                try {
                    Group_Name = "";

                    Log.d("HANDLING GROUP ", "RESPONSE ");

                    String response[] = string.split("#");

                    /** TEST **/
                    for (int i = 0; i < response.length; i++) {
                        System.out.println("Response Array : " + response[i] + " i POS : " + i);
                    }

                    SelectedGroupsTask.UserName = String.valueOf(response[0]);
                    System.out.println("SelectedGroupsTask.UserName : " + SelectedGroupsTask.UserName);

                    SelectedGroupsTask.Ngo_Id = String.valueOf(response[1]);
                    System.out.println("SelectedGroupsTask.Ngo_Id : " + SelectedGroupsTask.Ngo_Id);

                    SelectedGroupsTask.Group_Id = String.valueOf(response[2]);
                    System.out.println("SelectedGroupsTask.Group_Id : " + SelectedGroupsTask.Group_Id);

                    SelectedGroupsTask.sTypeID = String.valueOf(response[2]);
                    System.out.println("SelectedGroupsTask.sTypeID : " + SelectedGroupsTask.sTypeID);

                    SelectedGroupsTask.Group_Name = String.valueOf(response[3]);
                    System.out.println("SelectedGroupsTask.Group_Name : " + SelectedGroupsTask.Group_Name);

                    String[] memberInfo = String.valueOf(response[4]).split("~");
                    int count = 0;

                    try {
                        for (int i = 0; i < memberInfo.length; i++) {

                            if ((i % 2 == 0) && (count == 0)) {
                                System.out.println("Memeber ID : " + memberInfo[i]);
                                SelectedGroupsTask.member_Id.add(String.valueOf(memberInfo[i]));
                                count = 1;
                            } else if (count == 1) {
                                System.out.println("Member Name : " + memberInfo[i]);
                                SelectedGroupsTask.member_Name.add(String.valueOf(memberInfo[i]));
                                count = 0;
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    String loanInfo[] = String.valueOf(response[5]).split("~");
                    int loanCount = 0;

                    try {

                        for (int i = 0; i < loanInfo.length; i++) {
                            if (loanCount == 0) {
                                System.out.println("Loan ID  : " + loanInfo[i]);
                                SelectedGroupsTask.loan_Id.add(String.valueOf(loanInfo[i]));
                                loanCount = 1;
                            } else if (loanCount == 1) {
                                System.out.println("Loan ENGLISH Name : " + loanInfo[i]);
                                SelectedGroupsTask.loan_EngName.add(String.valueOf(loanInfo[i]));
                                loanCount = 2;
                            } else if (loanCount == 2) {
                                System.out.println("Loan Regional Name : " + loanInfo[i]);
                                SelectedGroupsTask.loan_Name.add(String.valueOf(loanInfo[i]));
                                loanCount = 0;
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    SelectedGroupsTask.sLastTransactionDate_Response = String.valueOf(response[6]);
                    System.out
                            .println("Last Transaction Date : " + SelectedGroupsTask.sLastTransactionDate_Response);

                    SelectedGroupsTask.sBalanceSheetDate_Response = String.valueOf(response[7]);
                    System.out.println("Balance Sheet Date : " + SelectedGroupsTask.sBalanceSheetDate_Response);

                    String bankInfo[] = response[8].split("~");

                    Log.e("Selected ", response[8]);

                    SelectedGroupsTask.sCashinHand = String.valueOf(bankInfo[1]);
                    System.out.println("CASH IN HAND : " + SelectedGroupsTask.sCashinHand);

                    SelectedGroupsTask.sCashatBank = String.valueOf(bankInfo[3]);
                    System.out.println("CASH AT BANK : " + SelectedGroupsTask.sCashatBank);

                    try {

                        int j = 0;
                        for (int i = 4; i < bankInfo.length; i++) {
                            if (j == 2) {
                                if (!bankInfo[i].toString().equals("-")) { // "0"
                                    SelectedGroupsTask.sBankAmt.addElement(bankInfo[i].toString());
                                    System.out.println("Amount : " + i + " " + bankInfo[i].toString());
                                }
                            }
                            if (j == 1) {
                                if (!bankInfo[i].toString().equals("-")) {
                                    SelectedGroupsTask.sBankNames.addElement(bankInfo[i].toString());
                                    System.out.println("Regional bankName : " + i + " " + bankInfo[i].toString());
                                }

                            }
                            if (j == 0) {
                                if (!bankInfo[i].toString().equals("-")) {
                                    SelectedGroupsTask.sEngBankNames.addElement(bankInfo[i].toString());
                                    System.out.println("English bankName : " + i + " " + bankInfo[i].toString());
                                }
                            }
                            j = j + 1;
                            if (j > 2) {
                                j = 0;
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    SelectedGroupsTask.sTr_ID = String.valueOf(response[9]);
                    System.out.println("TRANSACTION ID : " + SelectedGroupsTask.sTr_ID);

                    SelectedGroupsTask.sLoanAccDetails = String.valueOf(response[9]);
                    System.out.println("LOAN ACCOUNT DETAILS : " + SelectedGroupsTask.sLoanAccDetails);

                    if (!SelectedGroupsTask.sLoanAccDetails.equals("No")) {
                        String[] loanAcc_info = SelectedGroupsTask.sLoanAccDetails.split("[%]");
                        Log.e("loanAcc_info size", loanAcc_info.length + "");
                        for (int i = 0; i < loanAcc_info.length; i++) {
                            String indv_loanAccDetails = loanAcc_info[i];
                            String[] indv_loanAcc_info = indv_loanAccDetails.split("~");

                            SelectedGroupsTask.loanAcc_loanId.addElement(indv_loanAcc_info[0].toString());
                            SelectedGroupsTask.loanAcc_loanEngName.addElement(indv_loanAcc_info[1].toString());
                            SelectedGroupsTask.loanAcc_loanRegName.addElement(indv_loanAcc_info[2].toString());
                            SelectedGroupsTask.loanAcc_bankName.addElement(indv_loanAcc_info[3].toString());
                            SelectedGroupsTask.loanAcc_loanAccNo.addElement(indv_loanAcc_info[4].toString());
                            SelectedGroupsTask.loanAcc_loanDisbursementDate
                                    .addElement(indv_loanAcc_info[5].toString());
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }


        } else {

            try {

                UserName = "";
                Ngo_Id = "";
                Group_Id = "";
                sTr_ID = "";

                sBankNames.clear();

                sEngBankNames.clear();
                sBankAmt.clear();
                member_Id.clear();
                member_Name.clear();
                loan_Id.clear();
                loan_Name.clear();
                loan_EngName.clear();

                loanAcc_loanId.clear();
                loanAcc_loanEngName.clear();
                loanAcc_loanRegName.clear();
                loanAcc_bankName.clear();
                loanAcc_loanAccNo.clear();
                loanAcc_loanDisbursementDate.clear();

                loanAcc_loanId = new Vector<String>();
                loanAcc_loanEngName = new Vector<String>();
                loanAcc_loanRegName = new Vector<String>();
                loanAcc_bankName = new Vector<String>();
                loanAcc_loanAccNo = new Vector<String>();
                loanAcc_loanDisbursementDate = new Vector<String>();

                sBankAmt = new Vector<String>();
                sEngBankNames = new Vector<String>();
                sBankAmt = new Vector<String>();
                member_Id = new Vector<String>();
                member_Name = new Vector<String>();
                loan_Id = new Vector<String>();
                loan_Name = new Vector<String>();
                loan_EngName = new Vector<String>();

                Group_Name = "";

                Log.d("HANDLING GROUP ", "RESPONSE ");

                Log.e("Offline Data Values->>>", string);

                String response[] = string.split("#");

                /** TEST **/
                for (int i = 0; i < response.length; i++) {
                    System.out.println("Response Array : " + response[i] + " i POS : " + i);
                }

                SelectedGroupsTask.UserName = String.valueOf(response[0]);
                System.out.println("SelectedGroupsTask.UserName : " + SelectedGroupsTask.UserName);

                SelectedGroupsTask.Ngo_Id = String.valueOf(response[1]);
                System.out.println("SelectedGroupsTask.Ngo_Id : " + SelectedGroupsTask.Ngo_Id);

                SelectedGroupsTask.Group_Id = String.valueOf(response[2]);
                System.out.println("SelectedGroupsTask.Group_Id : " + SelectedGroupsTask.Group_Id);

                SelectedGroupsTask.sTypeID = String.valueOf(response[2]);
                System.out.println("SelectedGroupsTask.sTypeID : " + SelectedGroupsTask.sTypeID);

                SelectedGroupsTask.Group_Name = String.valueOf(response[3]);
                System.out.println("SelectedGroupsTask.Group_Name : " + SelectedGroupsTask.Group_Name);

                String[] memberInfo = String.valueOf(response[4]).split("~");
                int count = 0;

                try {
                    for (int i = 0; i < memberInfo.length; i++) {

                        if ((i % 2 == 0) && (count == 0)) {
                            System.out.println("Memeber ID : " + memberInfo[i]);
                            SelectedGroupsTask.member_Id.add(String.valueOf(memberInfo[i]));
                            count = 1;
                        } else if (count == 1) {
                            System.out.println("Member Name : " + memberInfo[i]);
                            SelectedGroupsTask.member_Name.add(String.valueOf(memberInfo[i]));
                            count = 0;
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                String loanInfo[] = String.valueOf(response[5]).split("~");
                int loanCount = 0;

                try {

                    for (int i = 0; i < loanInfo.length; i++) {
                        if (loanCount == 0) {
                            System.out.println("Loan ID  : " + loanInfo[i]);
                            SelectedGroupsTask.loan_Id.add(String.valueOf(loanInfo[i]));
                            loanCount = 1;
                        } else if (loanCount == 1) {
                            System.out.println("Loan ENGLISH Name : " + loanInfo[i]);
                            SelectedGroupsTask.loan_EngName.add(String.valueOf(loanInfo[i]));
                            loanCount = 2;
                        } else if (loanCount == 2) {
                            System.out.println("Loan Regional Name : " + loanInfo[i]);
                            SelectedGroupsTask.loan_Name.add(String.valueOf(loanInfo[i]));
                            loanCount = 0;
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                SelectedGroupsTask.sLastTransactionDate_Response = String.valueOf(response[6]);
                System.out.println("Last Transaction Date : " + SelectedGroupsTask.sLastTransactionDate_Response);

                SelectedGroupsTask.sBalanceSheetDate_Response = String.valueOf(response[7]);
                System.out.println("Balance Sheet Date : " + SelectedGroupsTask.sBalanceSheetDate_Response);

                String bankInfo[] = response[8].split("~");

                Log.e("Selected Grp > sss", response[8]);

                SelectedGroupsTask.sCashinHand = String.valueOf(bankInfo[1]);
                System.out.println("CASH IN HAND : " + SelectedGroupsTask.sCashinHand);

                SelectedGroupsTask.sCashatBank = String.valueOf(bankInfo[3]);
                System.out.println("CASH AT BANK : " + SelectedGroupsTask.sCashatBank);

                try {

                    int j = 0;
                    for (int i = 4; i < bankInfo.length; i++) {
                        if (j == 2) {
                            if (!bankInfo[i].toString().equals("-")) { // "0"
                                SelectedGroupsTask.sBankAmt.addElement(bankInfo[i].toString());
                                System.out.println("Amount : " + i + " " + bankInfo[i].toString());
                            }
                        }
                        if (j == 1) {
                            if (!bankInfo[i].toString().equals("-")) {
                                SelectedGroupsTask.sBankNames.addElement(bankInfo[i].toString());
                                System.out.println("Regional bankName : " + i + " " + bankInfo[i].toString());
                            }

                        }
                        if (j == 0) {
                            if (!bankInfo[i].toString().equals("-")) {
                                SelectedGroupsTask.sEngBankNames.addElement(bankInfo[i].toString());
                                System.out.println("English bankName : " + i + " " + bankInfo[i].toString());
                            }
                        }
                        j = j + 1;
                        if (j > 2) {
                            j = 0;
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                SelectedGroupsTask.sTr_ID = String.valueOf(response[9]);
                System.out.println("TRANSACTION ID : " + SelectedGroupsTask.sTr_ID);

                SelectedGroupsTask.sLoanAccDetails = String.valueOf(response[10]);
                System.out.println("LOAN ACCOUNT DETAILS : " + SelectedGroupsTask.sLoanAccDetails);

                if (!SelectedGroupsTask.sLoanAccDetails.equals("No")) {
                    String[] loanAcc_info = SelectedGroupsTask.sLoanAccDetails.split("[%]");
                    Log.e("loanAcc_info size", loanAcc_info.length + "");
                    for (int i = 0; i < loanAcc_info.length; i++) {
                        String indv_loanAccDetails = loanAcc_info[i];
                        String[] indv_loanAcc_info = indv_loanAccDetails.split("~");

                        SelectedGroupsTask.loanAcc_loanId.addElement(indv_loanAcc_info[0].toString());
                        SelectedGroupsTask.loanAcc_loanEngName.addElement(indv_loanAcc_info[1].toString());
                        SelectedGroupsTask.loanAcc_loanRegName.addElement(indv_loanAcc_info[2].toString());
                        SelectedGroupsTask.loanAcc_bankName.addElement(indv_loanAcc_info[3].toString());
                        SelectedGroupsTask.loanAcc_loanAccNo.addElement(indv_loanAcc_info[4].toString());
                        SelectedGroupsTask.loanAcc_loanDisbursementDate.addElement(indv_loanAcc_info[5].toString());
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        return;
    }

}
