package com.yesteam.eshakti.webservices;

import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.model.InternalBankMFIModel;
import com.yesteam.eshakti.utils.CryptographyUtils;
import com.yesteam.eshakti.utils.GetResponseInfo;
import com.yesteam.eshakti.view.fragment.Transaction_InternalBank_MFI_LoanFragment;

import android.os.AsyncTask;
import android.util.Log;

public class GetAddLoanTask extends AsyncTask<String, String, String> {

	private TaskListener mListener;

	public static String sBankLoanType_Response = null;
	public static String sBankLoanType_Response_Values = null;

	public GetAddLoanTask(TaskListener listener) {
		// TODO Auto-generated constructor stub
		mListener = listener;
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub

		SoapObject request = new SoapObject(LoginTask.NAMESPACE,
				SOAP_OperationConstants.OPERATION_NAME_GET_ADDLOAN_FINAL);

		try {

			request.addProperty("Ngo_Id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Ngo_Id)));
			request.addProperty("Group_Id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Group_Id)));
			request.addProperty("Loan_Type", CryptographyUtils.Encrypt(InternalBankMFIModel.getLoanType()));
			request.addProperty("Loan_Source", CryptographyUtils.Encrypt(InternalBankMFIModel.getLoanSource()));
			request.addProperty("Loan_Name", CryptographyUtils.Encrypt(InternalBankMFIModel.getLoanName()));
			request.addProperty("Bank_Name", CryptographyUtils.Encrypt(InternalBankMFIModel.getBankName()));
			request.addProperty("Branch_Name", CryptographyUtils.Encrypt(InternalBankMFIModel.getBankBranchName()));
			request.addProperty("NBFC_Name", CryptographyUtils.Encrypt(InternalBankMFIModel.getNBFCName()));
			request.addProperty("Loan_Account_No", CryptographyUtils.Encrypt(InternalBankMFIModel.getLoanAccNo()));
			request.addProperty("Loan_Sanction_Date",
					CryptographyUtils.Encrypt(InternalBankMFIModel.getLoan_Sanction_Date()));
			request.addProperty("Loan_Sanction_Amount",
					CryptographyUtils.Encrypt(InternalBankMFIModel.getLoan_Sanction_Amount()));
			request.addProperty("New_Bank_Charges",
					CryptographyUtils.Encrypt(InternalBankMFIModel.getLoanBankCharges()));
			request.addProperty("Loan_Disbursement_Date",
					CryptographyUtils.Encrypt(InternalBankMFIModel.getLoan_Disbursement_Date()));
			request.addProperty("Loan_Disbursement_Amount",
					CryptographyUtils.Encrypt(InternalBankMFIModel.getLoan_Disbursement_Amount()));
			request.addProperty("Loan_Tenure", CryptographyUtils.Encrypt(InternalBankMFIModel.getLoan_Tenure()));
			request.addProperty("Installment_Type",
					CryptographyUtils.Encrypt(InternalBankMFIModel.getInstallment_Type()));
			request.addProperty("Subsidy_Amount", CryptographyUtils.Encrypt(InternalBankMFIModel.getSubsidy_Amount()));
			request.addProperty("Subsidy_Reserve_Fund",
					CryptographyUtils.Encrypt(InternalBankMFIModel.getSubsidy_Reserve_Fund()));
			request.addProperty("Purpose_of_Loan",
					CryptographyUtils.Encrypt(InternalBankMFIModel.getPurpose_of_Loan()));
			request.addProperty("Interest_Rate", CryptographyUtils.Encrypt(InternalBankMFIModel.getInterest_Rate()));
			request.addProperty("Values", CryptographyUtils.Encrypt(InternalBankMFIModel.getValues()));

			request.addProperty("Tr_Date", CryptographyUtils.Encrypt(EShaktiApplication.getTransactionDate()));
			request.addProperty("Trans_Type",
					CryptographyUtils.Encrypt(Transaction_InternalBank_MFI_LoanFragment.selectedType));
			if (Transaction_InternalBank_MFI_LoanFragment.selectedType.equals("Bank")) {
				request.addProperty("SB_BankName",
						CryptographyUtils.Encrypt(Transaction_InternalBank_MFI_LoanFragment.mBankValue));
			} else {
				request.addProperty("SB_BankName", CryptographyUtils.Encrypt(""));
			}

			Log.e("Request---------------->", request.toString());
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

		envelope.setOutputSoapObject(request);

		envelope.dotNet = true;

		try {

			HttpTransportSE http_se = new HttpTransportSE(LoginTask.SOAP_ADDRESS);

			http_se.call(LoginTask.NAMESPACE + SOAP_OperationConstants.OPERATION_NAME_GET_ADDLOAN_FINAL, envelope);

			Object result = envelope.getResponse();

			sBankLoanType_Response = CryptographyUtils.Decrypt(result.toString());
			GetResponseInfo.getResponseInfo(sBankLoanType_Response);

			String[] mString = sBankLoanType_Response.split("#");
			sBankLoanType_Response_Values = mString[0];

			System.out.println("Service Response:" + sBankLoanType_Response);
			Constants.NETWORKCOMMONFLAG = "SUCCESS";
		} catch (IOException e) {
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (XmlPullParserException e) {
			// TODO: handle exception
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		}

		return Constants.NETWORKCOMMONFLAG;

	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		mListener.onTaskStarted();
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		if (result == null) {
			result = Constants.EXCEPTION;
			mListener.onTaskFinished(result);
		} else {
			mListener.onTaskFinished(result);
		}
	}

}