package com.yesteam.eshakti.webservices;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.view.fragment.MemberList_Fragment;
import com.yesteam.eshakti.view.fragment.Profile_Member_Aadhaar_Image_View_MenuFragment;

import android.os.AsyncTask;
import android.util.Log;


public class Member_View_Image_Task extends AsyncTask<Void, Void, String> {
	private TaskListener mListener;

	public Member_View_Image_Task(TaskListener listener) {
		// TODO Auto-generated constructor stub
		this.mListener = listener;

	}

	@Override
	protected String doInBackground(Void... params) {
		String response = "";

		uploadImage();

		return response;
	}

	@Override
	protected void onPreExecute() {
		mListener.onTaskStarted();
	}

	@Override
	protected void onPostExecute(String result) {
		mListener.onTaskFinished(result);
	}

	public String uploadImage() {

		String mNgo_Id, mGroup_Id, mMember_Id;
		mNgo_Id = SelectedGroupsTask.Ngo_Id;
		mGroup_Id = SelectedGroupsTask.Group_Id;
		mMember_Id = MemberList_Fragment.sSelected_MemberId;

		// LoginTask.NAMESPACE = "http://yshpiaws.yesbooks.in/";
		SoapObject request = new SoapObject(LoginTask.NAMESPACE,
				SOAP_OperationConstants.OPERATION_NAME_GET_MEMBER_PHOTO);
		Log.v("WEB Services", LoginTask.NAMESPACE
				+ SOAP_OperationConstants.OPERATION_NAME_GET_MEMBER_PHOTO);
		try {
			request.addProperty("ngo_id", mNgo_Id);
			request.addProperty("g_id", mGroup_Id);
			request.addProperty("m_id", mMember_Id);

			Log.i("Step 1", "11111111111111111");
			
			Log.e("Resuest", request.toString());

		} catch (Exception e) {
			e.getStackTrace();
		}
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.setOutputSoapObject(request);
		envelope.dotNet = true;

		HttpTransportSE androidHttpTransport = new HttpTransportSE(
				LoginTask.SOAP_ADDRESS);
		Log.i("step 2", "222222222222");
		try {

			Log.i("Step 3", "33333333333333333");
			androidHttpTransport.call(LoginTask.NAMESPACE
					+ SOAP_OperationConstants.OPERATION_NAME_GET_MEMBER_PHOTO,
					envelope);
			Log.i("Step 4", "4444444444444444");
			Object result = envelope.getResponse();

			String sGroupLoginTask_Response = String.valueOf(result);
			Log.i("Image Output String", sGroupLoginTask_Response);

			Profile_Member_Aadhaar_Image_View_MenuFragment.mServiceResponse = sGroupLoginTask_Response;

		} catch (Exception e) {
			Log.v("Exception", "Values $$$$");
			e.printStackTrace();
		}
		Log.v("Step 5", "555555555555555");
		return "";
	}
}