package com.yesteam.eshakti.webservices;

import java.io.IOException;

import org.apache.http.conn.ConnectTimeoutException;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.utils.CryptographyUtils;

import android.os.AsyncTask;
import android.util.Log;

public class DeactivateTask extends AsyncTask<String, String, String> {

	private TaskListener mListener;

	public static String sDeactivate_Response = null;

	public DeactivateTask(TaskListener listener) {
		// TODO Auto-generated constructor stub
		this.mListener = listener;
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub

		SoapObject request = new SoapObject(LoginTask.NAMESPACE, SOAP_OperationConstants.OPERATION_NAME_DEACTIVATE);

		System.out.println("USER TYPE ********* " + EShaktiApplication.getmUserType());

		try {
			request.addProperty("ngo_Id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Ngo_Id)));
			request.addProperty("Type_Id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.sTypeID)));
			request.addProperty("Type", CryptographyUtils.Encrypt(String.valueOf(EShaktiApplication.getmUserType())));
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Log.e("DeactivateTask", request + "");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.setOutputSoapObject(request);
		envelope.dotNet = true;

		HttpTransportSE httpSe = new HttpTransportSE(LoginTask.SOAP_ADDRESS);

		try {

			httpSe.call(LoginTask.NAMESPACE + SOAP_OperationConstants.OPERATION_NAME_DEACTIVATE, envelope);

			Object result = envelope.getResponse();

			sDeactivate_Response = CryptographyUtils.Decrypt(String.valueOf(result));
			Constants.NETWORKCOMMONFLAG = "SUCCESS";
		} catch (ConnectTimeoutException e) {
			// TODO: handle exception
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		}

		return Constants.NETWORKCOMMONFLAG;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		mListener.onTaskStarted();
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		if (result == null) {
			result = Constants.EXCEPTION;
			mListener.onTaskFinished(result);
		} else {
			mListener.onTaskFinished(result);
		}
	}
}
