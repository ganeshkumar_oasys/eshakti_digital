package com.yesteam.eshakti.webservices;

import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.utils.CryptographyUtils;
import com.yesteam.eshakti.view.fragment.Transaction_MemLoanRepaid_MenuFragment;

import android.os.AsyncTask;
import android.util.Log;

public class Get_All_Mem_OutstandingTask extends AsyncTask<String, String, String> {

	public static String sGet_All_Mem_OutstandingTask_Response = null;

	private TaskListener mListener;

	public Get_All_Mem_OutstandingTask(TaskListener listener) {
		// TODO Auto-generated constructor stub
		this.mListener = listener;
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub

		SoapObject request = new SoapObject(LoginTask.NAMESPACE,
				SOAP_OperationConstants.OPERATION_NAME_GET_ALL_MEM_OUTSTANDING_FINAL);

		try {
			request.addProperty("Ngo_id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Ngo_Id)));
			request.addProperty("Group_id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Group_Id)));
			request.addProperty("Loanid", CryptographyUtils.Encrypt(Transaction_MemLoanRepaid_MenuFragment.sSEND_TO_SERVER_LOANID));// pass
			// the
			// loan
			// id
			// from
			// the
			// MemberLoan
			// Reapid
			// Menu
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.setOutputSoapObject(request);
		envelope.dotNet = true;
		Log.e("Out Standingsssssssssssss", request.toString().trim());
		HttpTransportSE httpSe = new HttpTransportSE(LoginTask.SOAP_ADDRESS);
		try {
			httpSe.call(LoginTask.NAMESPACE + SOAP_OperationConstants.OPERATION_NAME_GET_ALL_MEM_OUTSTANDING_FINAL, envelope);

			Object result = envelope.getResponse();

			sGet_All_Mem_OutstandingTask_Response = CryptographyUtils.Decrypt(result.toString());
			if (sGet_All_Mem_OutstandingTask_Response!=null) {
				String [] mValues = sGet_All_Mem_OutstandingTask_Response.split("#");
				sGet_All_Mem_OutstandingTask_Response = mValues[0];
				Get_PurposeOfLoanTask.sGet_PurposeOfLoan_Response =mValues[1]; 
			}
			Log.v("Get All Member Out Standing", sGet_All_Mem_OutstandingTask_Response);

			Constants.NETWORKCOMMONFLAG = "SUCCESS";

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (Exception e) {
			// TODO: handle exception
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		}

		return Constants.NETWORKCOMMONFLAG;
	}

	@Override
	protected void onPreExecute() {
		mListener.onTaskStarted();
	}

	@Override
	protected void onPostExecute(String result) {
		if (result == null) {
			result = Constants.EXCEPTION;
			mListener.onTaskFinished(result);
		} else {
			mListener.onTaskFinished(result);
		}

	}

}
