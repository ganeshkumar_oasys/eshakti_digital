package com.yesteam.eshakti.webservices;

import java.io.IOException;
import java.net.ConnectException;

import org.apache.http.conn.ConnectTimeoutException;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.utils.CryptographyUtils;
import com.yesteam.eshakti.view.activity.GroupListActivity;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class Get_Edit_OpeningBalanceUpdateWebservice extends AsyncTask<String, String, String> {

	private TaskListener mListener;

	String mTag = LoginTask.class.getSimpleName();

	public static String NAMESPACE = "";

	public static String SOAP_ADDRESS = "";

	public static String sGetEditOpeningBalance_Update_Response = null;

	Context context;

	public Get_Edit_OpeningBalanceUpdateWebservice(TaskListener listener) {
		this.mListener = listener;

	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub

		try {

			SoapObject request = new SoapObject(LoginTask.NAMESPACE,
					SOAP_OperationConstants.OPERATION_NAME_GET_EDIT_OPENINGBALANCE_UPDATE);

			request.addProperty("User_Name", CryptographyUtils.Encrypt(LoginTask.UserName));
			request.addProperty("Ngo_id", CryptographyUtils.Encrypt(LoginTask.Ngo_Id));
			request.addProperty("Group_id", CryptographyUtils.Encrypt(SelectedGroupsTask.Group_Id));
			request.addProperty("Values", CryptographyUtils.Encrypt(publicValues.mMaster_SendtoserverValues));

			Log.e(mTag, request.toString() + "");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.setOutputSoapObject(request);
			envelope.dotNet = true;

			HttpTransportSE httpSe = new HttpTransportSE(LoginTask.SOAP_ADDRESS);

			try {
				httpSe.call(LoginTask.NAMESPACE + SOAP_OperationConstants.OPERATION_NAME_GET_EDIT_OPENINGBALANCE_UPDATE,
						envelope);

				Object result = envelope.getResponse();
				sGetEditOpeningBalance_Update_Response = CryptographyUtils.Decrypt(result.toString());

				publicValues.mMasterEditOpeningBalance_UpdateValues = sGetEditOpeningBalance_Update_Response;
				
				Log.e("Edit update Values ----->>>>", publicValues.mMasterEditOpeningBalance_UpdateValues);

				Constants.NETWORKCOMMONFLAG = "SUCCESS";

			} catch (ArrayIndexOutOfBoundsException e) {
				e.printStackTrace();
			} catch (ConnectTimeoutException e) {
				// TODO: handle exception
				e.printStackTrace();
				mListener.onTaskFinished(Constants.EXCEPTION);
				Log.e("TIME OUT ", "Success");
				Constants.NETWORKCOMMONFLAG = "FAIL";
			} catch (ConnectException e) {
				e.printStackTrace();
				Log.v("CONNECT EXCEPTION ", "Success");
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			} catch (XmlPullParserException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			}

		} catch (Exception e) {
			e.printStackTrace();
			Log.v("Yes!!!!!!!", "!!!!!!!!!");
		}

		return Constants.NETWORKCOMMONFLAG;
	}

	@Override
	protected void onPreExecute() {
		mListener.onTaskStarted();
	}

	@Override
	protected void onPostExecute(String result) {
		if (result == null) {
			result = Constants.EXCEPTION;

			mListener.onTaskFinished(result);
		} else {
			mListener.onTaskFinished(result);
		}
	}
}
