package com.yesteam.eshakti.webservices;

import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.utils.CryptographyUtils;

import android.os.AsyncTask;

public class GetBankLoanTypeTask extends AsyncTask<String, String, String> {

	private TaskListener mListener;

	String sBankLoanType_Response = null;

	public GetBankLoanTypeTask(TaskListener listener) {
		// TODO Auto-generated constructor stub
		mListener = listener;
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub

		SoapObject request = new SoapObject(LoginTask.NAMESPACE,
				SOAP_OperationConstants.OPERATION_NAME_GET_BANKLOAN_TYPE);

		try {

			request.addProperty("Ngo_Id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Ngo_Id)));
			request.addProperty("Group_Id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Group_Id)));

		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

		envelope.setOutputSoapObject(request);

		envelope.dotNet = true;

		try {

			HttpTransportSE http_se = new HttpTransportSE(LoginTask.SOAP_ADDRESS);

			http_se.call(LoginTask.NAMESPACE + SOAP_OperationConstants.OPERATION_NAME_GET_BANKLOAN_TYPE, envelope);

			Object result = envelope.getResponse();

			sBankLoanType_Response = CryptographyUtils.Decrypt(result.toString());
			publicValues.mBANKLoanType = sBankLoanType_Response;

			System.out.println("Service Response:" + sBankLoanType_Response);
			Constants.NETWORKCOMMONFLAG = "SUCCESS";
		} catch (IOException e) {
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (XmlPullParserException e) {
			// TODO: handle exception
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		}

		return Constants.NETWORKCOMMONFLAG;

	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		mListener.onTaskStarted();
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		if (result == null) {
			result = Constants.EXCEPTION;
			mListener.onTaskFinished(result);
		} else {
			mListener.onTaskFinished(result);
		}
	}

}