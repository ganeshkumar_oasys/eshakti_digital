package com.yesteam.eshakti.webservices;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.utils.CryptographyUtils;
import com.yesteam.eshakti.view.fragment.MemberList_Fragment;
import com.yesteam.eshakti.view.fragment.Profile_Member_Aadhaar_Image_View_MenuFragment;

import android.os.AsyncTask;
import android.util.Log;

public class Member_AadhaarCard_View_Task extends AsyncTask<Void, Void, String> {
	private TaskListener mListener;

	public Member_AadhaarCard_View_Task(TaskListener listener) {
		// TODO Auto-generated constructor stub
		this.mListener = listener;

	}

	@Override
	protected String doInBackground(Void... params) {
		String response = "";

		uploadImage();

		return Constants.NETWORKCOMMONFLAG;// response;
	}

	@Override
	protected void onPreExecute() {
		mListener.onTaskStarted();
	}

	@Override
	protected void onPostExecute(String result) {
		if (result == null) {
			result = Constants.EXCEPTION;
			mListener.onTaskFinished(result);
		} else {
			mListener.onTaskFinished(result);
		}
	}

	public String uploadImage() {

		String mNgo_Id, mGroup_Id, mMember_Id;
		mNgo_Id = SelectedGroupsTask.Ngo_Id;
		mGroup_Id = SelectedGroupsTask.Group_Id;
		mMember_Id = MemberList_Fragment.sSelected_MemberId;

		SoapObject request = new SoapObject(LoginTask.NAMESPACE,
				SOAP_OperationConstants.OPERATION_NAME_GET_MEMBER_AADHAAR_INFO);
		try {
			request.addProperty("Ngo_id", CryptographyUtils.Encrypt(mNgo_Id));
			request.addProperty("Group_id", CryptographyUtils.Encrypt(mGroup_Id));
			request.addProperty("Mem_id", CryptographyUtils.Encrypt(mMember_Id));

			Log.i("Step 1", "11111111111111111");

			Log.e("Member_AadhaarCard_View_Task", request.toString() + "");
		} catch (Exception e) {
			e.getStackTrace();
		}
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.setOutputSoapObject(request);
		envelope.dotNet = true;
		HttpTransportSE androidHttpTransport = new HttpTransportSE(LoginTask.SOAP_ADDRESS);
		Log.i("step 2", "222222222222");
		try {

			androidHttpTransport.call(
					LoginTask.NAMESPACE + SOAP_OperationConstants.OPERATION_NAME_GET_MEMBER_AADHAAR_INFO, envelope);

			Object result = envelope.getResponse();

			String sGroupLoginTask_Response = CryptographyUtils.Decrypt(String.valueOf(result));
			Log.i("Image Output String", sGroupLoginTask_Response);

			Profile_Member_Aadhaar_Image_View_MenuFragment.mServiceResponse = sGroupLoginTask_Response;

			Constants.NETWORKCOMMONFLAG = "SUCCESS";

		} catch (Exception e) {

			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		}

		return "SUCCESS";
	}
}
