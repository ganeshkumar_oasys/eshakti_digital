package com.yesteam.eshakti.webservices;

import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import com.yesteam.eshakti.aadharcard.AadhaarCard;
import com.yesteam.eshakti.aadharcard.AadhaarXMLParser;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.utils.CryptographyUtils;
import com.yesteam.eshakti.view.fragment.MemberList_Fragment;

import android.os.AsyncTask;
import android.util.Log;

public class Member_Aadhar_InfoTask extends AsyncTask<String, String, String> {

	private TaskListener mListener;

	public static String sMember_Aadhar_Info_Response = null;
	String mAadhaarCardValues;
	public static AadhaarCard newCard;

	public String mAadhaarCardDetails;

	public Member_Aadhar_InfoTask(TaskListener listener, String text) {
		// TODO Auto-generated constructor stub
		this.mListener = listener;
		this.mAadhaarCardDetails = text;
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub

		SoapObject request = new SoapObject(LoginTask.NAMESPACE,
				SOAP_OperationConstants.OPERATION_NAME_MEMBER_AADHAR_INFO);
		Log.i("Yes Checking ", "Do in back Ground");
		try {

			if (mAadhaarCardDetails.length() == 12) {

			} else {
				newCard = new AadhaarXMLParser().parse(mAadhaarCardDetails);
			}
		} catch (XmlPullParserException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (mAadhaarCardDetails.length() == 12) {

			mAadhaarCardValues = mAadhaarCardDetails + "~" + "" + "~" + "" + "~" + "" + "~" + "" + "~" + "" + "~" + ""
					+ "~" + "" + "~" + "" + "~" + "" + "~" + "" + "~" + "" + "~" + "";
			Log.e("!!!!!!!!!!!!!!!!!@@@@@@@@@@@@@@", mAadhaarCardValues);

		} else {
			mAadhaarCardValues = newCard.uid + "~" + newCard.name + "~" + newCard.gender + "~" + newCard.co + "~"
					+ newCard.house + "~" + newCard.lm + "~" + newCard.vtc + "~" + newCard.po + "~" + newCard.dist + "~"
					+ newCard.subdist + "~" + newCard.state + "~" + newCard.pincode + "~" + newCard.dob;
			Log.e("!!!!!!!!!!!!!!!!!@@@@@@@@@@@@@@", mAadhaarCardValues);
		}
		try {
			request.addProperty("User_Name", CryptographyUtils.Encrypt(String.valueOf(LoginTask.UserName)));
			request.addProperty("Ngo_id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Ngo_Id)));
			request.addProperty("Group_id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Group_Id)));
			request.addProperty("Mem_id",
					CryptographyUtils.Encrypt(String.valueOf(MemberList_Fragment.sSelected_MemberId)));
			request.addProperty("Values", CryptographyUtils.Encrypt(mAadhaarCardValues));
			Log.e("Member_Aadhar_InfoTask", request.toString() + "");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.setOutputSoapObject(request);
		envelope.dotNet = true;

		HttpTransportSE httpSe = new HttpTransportSE(LoginTask.SOAP_ADDRESS);

		try {
			httpSe.call(LoginTask.NAMESPACE + SOAP_OperationConstants.OPERATION_NAME_MEMBER_AADHAR_INFO, envelope);

			Object result = envelope.getResponse();

			sMember_Aadhar_Info_Response = CryptographyUtils.Decrypt(String.valueOf(result));

			Constants.NETWORKCOMMONFLAG = "SUCCESS";

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		}

		return Constants.NETWORKCOMMONFLAG;// sMember_Aadhar_Info_Response;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		mListener.onTaskStarted();
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		if (result == null) {
			result = Constants.EXCEPTION;
			mListener.onTaskFinished(result);
		} else {
			mListener.onTaskFinished(result);
		}
	}
}
