package com.yesteam.eshakti.webservices;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;
import com.yesteam.eshakti.utils.CryptographyUtils;
import com.yesteam.eshakti.utils.GetIndividualInfo;
import com.yesteam.eshakti.utils.GetResponseInfo;
import com.yesteam.eshakti.view.fragment.Transaction_GroupLoanRepaidFragment;
import com.yesteam.eshakti.view.fragment.Transaction_GroupLoanRepaidMenuFragment;

import android.os.AsyncTask;
import android.util.Log;

public class Get_GroupLoanRepaidTask extends AsyncTask<String, String, String> {

	private TaskListener mListener;
	public static String sGroup_Loan_repaid_Response = null;
	public static String responseArr[];
	String sendLoanInfo = "";
	String memberId = "";

	public Get_GroupLoanRepaidTask(TaskListener listener) {
		// TODO Auto-generated constructor stub
		mListener = listener;
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		try {
			sendLoanInfo = GetIndividualInfo.getLoanInfo();
			memberId = GetIndividualInfo.getMemberID_Info();

			SoapObject request = new SoapObject(LoginTask.NAMESPACE,
					SOAP_OperationConstants.OPERATION_NAME_GET_GROUP_LOAN_REPAID);

			request.addProperty("User_Name", CryptographyUtils.Encrypt(String.valueOf(LoginTask.UserName)));
			request.addProperty("Ngo_id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Ngo_Id)));
			request.addProperty("Group_id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Group_Id)));
			request.addProperty("Member_id", CryptographyUtils.Encrypt(memberId));
			request.addProperty("Loans", CryptographyUtils.Encrypt(sendLoanInfo));
			request.addProperty("Loan_id",
					CryptographyUtils.Encrypt(String.valueOf(Transaction_GroupLoanRepaidMenuFragment.mSendTo_ServerLoan_Id)));
			request.addProperty("Bank_Interst",
					CryptographyUtils.Encrypt(String.valueOf(Transaction_GroupLoanRepaidFragment.sLoanAmount[0])));
			request.addProperty("Bank_Charges",
					CryptographyUtils.Encrypt(String.valueOf(Transaction_GroupLoanRepaidFragment.sLoanAmount[1])));
			request.addProperty("Repaid_Amount",
					CryptographyUtils.Encrypt(String.valueOf(Transaction_GroupLoanRepaidFragment.sLoanAmount[2])));
			request.addProperty("Int_Subvention",
					CryptographyUtils.Encrypt(String.valueOf(Transaction_GroupLoanRepaidFragment.sLoanAmount[3])));
			request.addProperty("Trdate",
					CryptographyUtils.Encrypt(String.valueOf(DatePickerDialog.sSend_To_Server_Date)));
			
			request.addProperty("T_Type",
					CryptographyUtils.Encrypt(String.valueOf(Transaction_GroupLoanRepaidFragment.selectedType)));
			
			if (Transaction_GroupLoanRepaidFragment.selectedType.equals("Cash")) {
				request.addProperty("Bank_Name",
						CryptographyUtils.Encrypt(String.valueOf("")));
				request.addProperty("SBBank_Charges",
						CryptographyUtils.Encrypt(String.valueOf("")));
			}else {
				request.addProperty("Bank_Name",
						CryptographyUtils.Encrypt(String.valueOf(Transaction_GroupLoanRepaidFragment.mBankNameValue)));
				request.addProperty("SBBank_Charges",
						CryptographyUtils.Encrypt(String.valueOf(Transaction_GroupLoanRepaidFragment.mBankChargesAmount)));
			}
			
			
			
			

			Log.e("Get_GroupLoanRepaidTask", request.toString() + "");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

			envelope.setOutputSoapObject(request);

			envelope.dotNet = true;

			try {

				HttpTransportSE http_se = new HttpTransportSE(LoginTask.SOAP_ADDRESS);

				http_se.call(LoginTask.NAMESPACE + SOAP_OperationConstants.OPERATION_NAME_GET_GROUP_LOAN_REPAID,
						envelope);

				System.out.println("Envelope Test :" + envelope.getResponse());

				Object result = envelope.getResponse();

				sGroup_Loan_repaid_Response = CryptographyUtils.Decrypt(result.toString());

				System.out.println("Service_Response :" + sGroup_Loan_repaid_Response);
				Log.e("Group Loan Repaid ment @@@@@", sGroup_Loan_repaid_Response);

				GetResponseInfo.getResponseInfo(sGroup_Loan_repaid_Response);

				Constants.NETWORKCOMMONFLAG = "SUCCESS";

			} catch (Exception e) {
				e.printStackTrace();
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return Constants.NETWORKCOMMONFLAG;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		mListener.onTaskStarted();
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		if (result == null) {
			result = Constants.EXCEPTION;
			mListener.onTaskFinished(result);
		} else {
			mListener.onTaskFinished(result);
		}

	}

}
