package com.yesteam.eshakti.webservices;

import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;
import com.yesteam.eshakti.utils.CryptographyUtils;
import com.yesteam.eshakti.utils.GetIndividualInfo;
import com.yesteam.eshakti.utils.GetResponseInfo;
import com.yesteam.eshakti.view.fragment.Transaction_SavingsFragment;

import android.os.AsyncTask;
import android.util.Log;

public class GetMember_SavingsTask extends AsyncTask<String, String, String> {

	public static String sGetMember_Savings_Task_Response = null;

	private TaskListener mListener;

	String sendLoanInfo = "";

	public GetMember_SavingsTask(TaskListener listener) {
		// TODO Auto-generated constructor stub
		this.mListener = listener;
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub

		sendLoanInfo = "";

		sendLoanInfo = GetIndividualInfo.getLoanInfo();
		System.out.println("LOAN INFO : " + sendLoanInfo);

		SoapObject request = new SoapObject(LoginTask.NAMESPACE,
				SOAP_OperationConstants.OPERATION_NAME_GET_MEMBER_SAVINGS);

		try {
			request.addProperty("User_Name", CryptographyUtils.Encrypt(String.valueOf(LoginTask.UserName)));
			request.addProperty("Ngo_id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Ngo_Id)));
			request.addProperty("Group_id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Group_Id)));

			request.addProperty("Values",
					CryptographyUtils.Encrypt(String.valueOf(Transaction_SavingsFragment.sSendToServer_Savings)));// tempvalue
			request.addProperty("Loanid", CryptographyUtils.Encrypt(sendLoanInfo));
			request.addProperty("Trdate", CryptographyUtils.Encrypt(DatePickerDialog.sSend_To_Server_Date));// pass

		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.setOutputSoapObject(request);

		envelope.dotNet = true;

		Log.e("All Response Values::", request + "");

		HttpTransportSE httpSe = new HttpTransportSE(LoginTask.SOAP_ADDRESS);
		try {

			httpSe.call(LoginTask.NAMESPACE + SOAP_OperationConstants.OPERATION_NAME_GET_MEMBER_SAVINGS, envelope);

			Object result = envelope.getResponse();

			sGetMember_Savings_Task_Response = CryptographyUtils.Decrypt(result.toString());
			String[] sResponseUpdate = sGetMember_Savings_Task_Response.split("#");

			if (sResponseUpdate[0].equals("Yes")) {
				
				Log.e("Current Bank Amount Values-------", SelectedGroupsTask.sBankAmt.size()+"");

				GetResponseInfo.getResponseInfo(sGetMember_Savings_Task_Response);
				Log.e("Savings values from Webservicesssssss", sGetMember_Savings_Task_Response);
			}
			Constants.NETWORKCOMMONFLAG = "SUCCESS";

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		}
		return Constants.NETWORKCOMMONFLAG;
	}

	@Override
	protected void onPreExecute() {
		mListener.onTaskStarted();
	}

	@Override
	protected void onPostExecute(String result) {
		if (result == null) {
			result = Constants.EXCEPTION;
			mListener.onTaskFinished(result);
		} else {
			mListener.onTaskFinished(result);
		}

	}

}
