package com.yesteam.eshakti.webservices;

import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;
import com.yesteam.eshakti.utils.CryptographyUtils;
import com.yesteam.eshakti.utils.GetResponseInfo;
import com.yesteam.eshakti.view.fragment.Transaction_LoanAccountFragment;

import android.os.AsyncTask;
import android.util.Log;

public class Get_GL_Fixed_Deposit_WithdrawalWebservice extends AsyncTask<String, String, String> {

	private TaskListener mListener;

	// String mTag = LoginChooseTask.class.getSimpleName();

	public static String mLoanAccTask_Response = null;

	public Get_GL_Fixed_Deposit_WithdrawalWebservice(TaskListener listener) {
		// TODO Auto-generated constructor stub
		this.mListener = listener;

	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub

		SoapObject request = new SoapObject(LoginTask.NAMESPACE,
				SOAP_OperationConstants.OPERATION_NAME_GET_GL_FIXED_DEPOSIT_WITHDRAWAL);

		try {

			request.addProperty("User_Name", CryptographyUtils.Encrypt(String.valueOf(LoginTask.UserName)));
			request.addProperty("Ngo_id", CryptographyUtils.Encrypt(String.valueOf(LoginTask.Ngo_Id)));
			request.addProperty("Group_id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Group_Id)));

			request.addProperty("Loan_Id", CryptographyUtils.Encrypt(EShaktiApplication.getLoanaccLoanId()));
			request.addProperty("Loan_Type", CryptographyUtils.Encrypt(EShaktiApplication.getLoanaccLoanType()));
			request.addProperty("Type", CryptographyUtils.Encrypt(Transaction_LoanAccountFragment.selectedType));
			request.addProperty("FD_Bank_Name",
					CryptographyUtils.Encrypt(EShaktiApplication.getLoanaccBankNameSendtoServer()));
			request.addProperty("Bank_Name", CryptographyUtils.Encrypt(Transaction_LoanAccountFragment.mBankNameValue));
			request.addProperty("withdrawal", CryptographyUtils.Encrypt(Transaction_LoanAccountFragment.mWithdrawalValue));
			request.addProperty("Bank_Charges", CryptographyUtils.Encrypt(Transaction_LoanAccountFragment.mExpensesValue));
			request.addProperty("Trdate", CryptographyUtils.Encrypt(DatePickerDialog.sSend_To_Server_Date));
			
			Log.e("Bank Values-->", Transaction_LoanAccountFragment.mBankNameValue);
			Log.e("Withdrawal", Transaction_LoanAccountFragment.mWithdrawalValue);
			Log.e("Expensess", Transaction_LoanAccountFragment.mExpensesValue);
			

		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Log.e("", request.toString() + "");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.setOutputSoapObject(request);
		envelope.dotNet = true;

		HttpTransportSE httpSe = new HttpTransportSE(LoginTask.SOAP_ADDRESS);
		try {
			httpSe.call(LoginTask.NAMESPACE + SOAP_OperationConstants.OPERATION_NAME_GET_GL_FIXED_DEPOSIT_WITHDRAWAL,
					envelope);

			Object result = envelope.getResponse();

			mLoanAccTask_Response = CryptographyUtils.Decrypt(result.toString());

			GetResponseInfo.getResponseInfo(mLoanAccTask_Response);
			Log.e("------------------------>>>>", mLoanAccTask_Response);

			try {

			} catch (Exception e) {
				mListener.onTaskFinished(Constants.EXCEPTION);
				e.printStackTrace();
			}

			Constants.NETWORKCOMMONFLAG = "SUCCESS";

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		}

		return Constants.NETWORKCOMMONFLAG;
	}

	@Override
	protected void onPreExecute() {
		mListener.onTaskStarted();
	}

	@Override
	protected void onPostExecute(String result) {
		if (result == null) {
			result = Constants.EXCEPTION;

			mListener.onTaskFinished(result);
		} else {
			mListener.onTaskFinished(result);
		}
	}

}
