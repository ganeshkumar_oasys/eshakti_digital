package com.yesteam.eshakti.webservices;

import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.sqlite.database.GroupProvider;
import com.yesteam.eshakti.sqlite.db.model.GroupResponseUpdate;
import com.yesteam.eshakti.sqlite.db.model.TransIdUpdate;
import com.yesteam.eshakti.sqlite.db.transactions.TransactionManager.DataType;
import com.yesteam.eshakti.utils.CryptographyUtils;
import com.yesteam.eshakti.utils.Put_DB_GroupNameDetail_Response;

import android.os.AsyncTask;
import android.util.Log;

public class Get_LastTransactionID extends AsyncTask<String, String, String> {

	private TaskListener mListener;

	public static String sGet_TrIdTask_Response = null;

	public Get_LastTransactionID(TaskListener listener) {
		// TODO Auto-generated constructor stub
		this.mListener = listener;
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub

		SoapObject request = new SoapObject(LoginTask.NAMESPACE, SOAP_OperationConstants.OPERTAION_NAME_GET_TR_ID);

		try {
			request.addProperty("Ngo_Id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Ngo_Id)));
			request.addProperty("Group_Id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Group_Id)));
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Log.e("Get_LastTransactionID", request.toString()+"");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.setOutputSoapObject(request);
		envelope.dotNet = true;

		HttpTransportSE httpSe = new HttpTransportSE(LoginTask.SOAP_ADDRESS);

		try {
			httpSe.call(LoginTask.NAMESPACE + SOAP_OperationConstants.OPERTAION_NAME_GET_TR_ID, envelope);

			Object result = envelope.getResponse();
			sGet_TrIdTask_Response = CryptographyUtils.Decrypt(String.valueOf(result));
			Log.e("Transaction Id Valuessssssssssssssss", sGet_TrIdTask_Response);
			SelectedGroupsTask.sTr_ID = sGet_TrIdTask_Response;

			EShaktiApplication.getInstance().getTransactionManager().startTransaction(DataType.TRANS_ID_UPDATE,
					new TransIdUpdate(SelectedGroupsTask.Group_Id, SelectedGroupsTask.sTr_ID));

			Thread.sleep(300);

			String mValues = Put_DB_GroupNameDetail_Response
					.put_DB_GroupNameDetails_Response(EShaktiApplication.getGroupResponse());

			GroupProvider.updateGroupResponse(
					new GroupResponseUpdate(EShaktiApplication.getGroupId_GroupLastTransDate(), mValues));

			Constants.NETWORKCOMMONFLAG = "SUCCESS";

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		}

		return Constants.NETWORKCOMMONFLAG;// sGet_TrIdTask_Response;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		mListener.onTaskStarted();
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		if (result == null) {
			result = Constants.EXCEPTION;
			mListener.onTaskFinished(result);
		} else {
			mListener.onTaskFinished(result);
		}
	}

}