package com.yesteam.eshakti.webservices;

import java.io.IOException;
import java.net.ConnectException;

import org.apache.http.conn.ConnectTimeoutException;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.utils.Configuration;
import com.yesteam.eshakti.utils.CryptographyUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

public class GetResolution_Webservices extends AsyncTask<String, String, String> {

	private TaskListener mListener;

	String mTag = LoginTask.class.getSimpleName();

	public static String NAMESPACE = "";

	public static String SOAP_ADDRESS = "";

	public static String mGetShgResolutionTask_Response = null;
	private Bitmap mFilename;

	public GetResolution_Webservices(TaskListener listener, Bitmap fileName) {
		this.mListener = listener;
		this.mFilename = fileName;

	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub

		try {

			SoapObject request = new SoapObject(LoginTask.NAMESPACE,
					SOAP_OperationConstants.OPERATION_NAME_GET_SHG_RESOLUTION);

			String encodedString = Configuration.toByteCode(mFilename);

			request.addProperty("UserName", SelectedGroupsTask.UserName);
			request.addProperty("Ngo_Id", SelectedGroupsTask.Ngo_Id);
			request.addProperty("Group_Id", SelectedGroupsTask.Group_Id);
			request.addProperty("strImage", encodedString);

			Log.e(mTag, request.toString() + "");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.setOutputSoapObject(request);
			envelope.dotNet = true;

			HttpTransportSE httpSe = new HttpTransportSE(LoginTask.SOAP_ADDRESS);

			try {
				httpSe.call(LoginTask.NAMESPACE + SOAP_OperationConstants.OPERATION_NAME_GET_SHG_RESOLUTION, envelope);

				Object result = envelope.getResponse();
				System.out.println("Values ---- Server!!" + result.toString());
				mGetShgResolutionTask_Response = result.toString();
				publicValues.mGetSHGResultionValues = mGetShgResolutionTask_Response;

				Constants.NETWORKCOMMONFLAG = "SUCCESS";

			} catch (ArrayIndexOutOfBoundsException e) {
				e.printStackTrace();
			} catch (ConnectTimeoutException e) {
				// TODO: handle exception
				e.printStackTrace();
				mListener.onTaskFinished(Constants.EXCEPTION);
				Log.e("TIME OUT ", "Success");
				Constants.NETWORKCOMMONFLAG = "FAIL";
			} catch (ConnectException e) {
				e.printStackTrace();
				Log.v("CONNECT EXCEPTION ", "Success");
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			} catch (XmlPullParserException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			}

		} catch (Exception e) {
			e.printStackTrace();

		}

		return Constants.NETWORKCOMMONFLAG;
	}

	@Override
	protected void onPreExecute() {
		mListener.onTaskStarted();
	}

	@Override
	protected void onPostExecute(String result) {
		if (result == null) {
			result = Constants.EXCEPTION;

			mListener.onTaskFinished(result);
		} else {
			mListener.onTaskFinished(result);
		}
	}
}
