package com.yesteam.eshakti.webservices;

import java.io.IOException;
import java.util.Vector;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.utils.CryptographyUtils;

import android.os.AsyncTask;
import android.util.Log;

public class ViewGroupsTask extends AsyncTask<String, String, String> {

	private TaskListener mListener;

	String mTag = ViewGroupsTask.class.getSimpleName();

	public static String sViewGroupsTask_Response = null;

	public static String sAgentId;
	public static String sAgent_ngoId;
	public static Vector<String> sAgent_GIDs;
	public static Vector<String> sAgent_Gname;

	public static String[] groupList;

	public ViewGroupsTask(TaskListener listener) {
		// TODO Auto-generated constructor stub
		this.mListener = listener;

		sAgent_GIDs = new Vector<String>();
		sAgent_Gname = new Vector<String>();
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub

		SoapObject request = new SoapObject(LoginTask.NAMESPACE, SOAP_OperationConstants.OPERATION_NAME_VIEWGROUPS);

		try {
			request.addProperty("UserName", CryptographyUtils.Encrypt(String.valueOf(LoginTask.UserName)));
			request.addProperty("Ngo_id", CryptographyUtils.Encrypt(String.valueOf(LoginTask.Ngo_Id)));
			request.addProperty("Trainer_id", CryptographyUtils.Encrypt(String.valueOf(LoginTask.Trainer_Id)));
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Log.e(mTag, request.toString()+"");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.setOutputSoapObject(request);
		envelope.dotNet = true;

		HttpTransportSE httpSe = new HttpTransportSE(LoginTask.SOAP_ADDRESS);
		try {
			httpSe.call(LoginTask.NAMESPACE + SOAP_OperationConstants.OPERATION_NAME_VIEWGROUPS, envelope);

			Object result = envelope.getResponse();

			sViewGroupsTask_Response = CryptographyUtils.Decrypt(result.toString());

			Log.d(mTag, sViewGroupsTask_Response);

			String arr[] = sViewGroupsTask_Response.split("#");

			groupList = arr[2].split("~");

			try {
				for (int i = 0; i < groupList.length; i++) {
					if (i % 2 == 0) {
						// System.out.println("Agent Group Id :" +
						// captureImg[i].toString());
						sAgent_GIDs.addElement(groupList[i].toString());
					} else {
						// System.out.println("Member image :" +
						// captureImg[i].toString());
						sAgent_Gname.addElement(groupList[i].toString());
					}
				}
			} catch (Exception e) {
				mListener.onTaskFinished(Constants.EXCEPTION);
				e.printStackTrace();
			}

			Constants.NETWORKCOMMONFLAG = "SUCCESS";

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		}

		return Constants.NETWORKCOMMONFLAG;
	}

	@Override
	protected void onPreExecute() {
		mListener.onTaskStarted();
	}

	@Override
	protected void onPostExecute(String result) {
		if (result == null) {
			result = Constants.EXCEPTION;

			mListener.onTaskFinished(result);
		} else {
			mListener.onTaskFinished(result);
		}
	}

}
