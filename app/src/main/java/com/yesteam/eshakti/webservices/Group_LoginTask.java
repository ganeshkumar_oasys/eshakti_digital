package com.yesteam.eshakti.webservices;

import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.utils.CryptographyUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;

import android.os.AsyncTask;
import android.util.Log;

public class Group_LoginTask extends AsyncTask<String, String, String> {

	private TaskListener mListener;
	public static String sGroup_LoginTask_Response = null;

	public Group_LoginTask(TaskListener listener) {
		// TODO Auto-generated constructor stub
		this.mListener = listener;
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub

		SoapObject request = new SoapObject(LoginTask.NAMESPACE, SOAP_OperationConstants.OPERTAION_NAME_GROUP_LOGIN);

		if (EShaktiApplication.getIMEI_NO() != null && !EShaktiApplication.getIMEI_NO().equals("")) {
		try {
			request.addProperty("UserName", CryptographyUtils.Encrypt(String.valueOf(LoginActivity.sUName)));
			request.addProperty("Password", CryptographyUtils.Encrypt(String.valueOf(LoginActivity.sPwd)));
			request.addProperty("IMEI", CryptographyUtils.Encrypt(EShaktiApplication.getIMEI_NO()));
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		}
		Log.e("Group Login ", request.toString()+"");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.setOutputSoapObject(request);
		envelope.dotNet = true;

		HttpTransportSE httpSe = new HttpTransportSE(LoginTask.SOAP_ADDRESS);

		try {
			httpSe.call(LoginTask.NAMESPACE + SOAP_OperationConstants.OPERTAION_NAME_GROUP_LOGIN, envelope);

			Object result = envelope.getResponse();

			sGroup_LoginTask_Response = CryptographyUtils.Decrypt(String.valueOf(result));

			Log.v("GROUP LOGIN ", sGroup_LoginTask_Response);

			String arr[] = sGroup_LoginTask_Response.split("#");

			LoginTask.UserName = arr[0];
			System.out.println("UserName :" + LoginTask.UserName);

			LoginTask.Ngo_Id = arr[1];
			System.out.println("Ngo_Id :" + LoginTask.Ngo_Id);

			LoginTask.Trainer_Id = arr[2];
			System.out.println("Trainer_Id :" + LoginTask.Trainer_Id);

			LoginTask.User_RegLanguage = arr[3];
			System.out.println("User_RegLanguage : " + LoginTask.User_RegLanguage);

			Constants.NETWORKCOMMONFLAG = "SUCCESS";

		} catch (ArrayIndexOutOfBoundsException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		}

		return Constants.NETWORKCOMMONFLAG;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		mListener.onTaskStarted();
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub

		if (result == null) {
			result = Constants.EXCEPTION;

			mListener.onTaskFinished(result);
		} else {
			mListener.onTaskFinished(result);
		}
	}

}
