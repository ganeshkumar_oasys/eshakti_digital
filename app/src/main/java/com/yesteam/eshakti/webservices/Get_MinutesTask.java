package com.yesteam.eshakti.webservices;

import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;
import com.yesteam.eshakti.utils.CryptographyUtils;
import com.yesteam.eshakti.utils.GetIndividualInfo;
import com.yesteam.eshakti.view.fragment.Meeting_MinutesOfMeetingFragment;

import android.os.AsyncTask;
import android.util.Log;

public class Get_MinutesTask extends AsyncTask<String, String, String> {

	private TaskListener mListener;

	public static String sMinutes_Response = null;

	String sendLoanInfo = "";
	String memberId = "";

	public Get_MinutesTask(TaskListener listener) {
		// TODO Auto-generated constructor stub
		mListener = listener;
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		sendLoanInfo = GetIndividualInfo.getLoanInfo();
		memberId = GetIndividualInfo.getMemberID_Info();

		SoapObject request = new SoapObject(LoginTask.NAMESPACE, SOAP_OperationConstants.OPERATION_NAME_GET_MINUTES);

		try {
			request.addProperty("User_Name", CryptographyUtils.Encrypt(String.valueOf(LoginTask.UserName)));
			request.addProperty("Ngo_id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Ngo_Id)));
			request.addProperty("Group_id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Group_Id)));
			request.addProperty("Member_id", CryptographyUtils.Encrypt(memberId));
			request.addProperty("Loanid", CryptographyUtils.Encrypt(sendLoanInfo));
			request.addProperty("Values",
					CryptographyUtils.Encrypt(String.valueOf(Meeting_MinutesOfMeetingFragment.Send_to_server_values)));
			request.addProperty("Trdate",
					CryptographyUtils.Encrypt(String.valueOf(DatePickerDialog.sSend_To_Server_Date)));
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Log.e("SERVICE REQUEST - Get_MinutesTask:", request + "");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

		envelope.setOutputSoapObject(request);

		envelope.dotNet = true;

		try {

			HttpTransportSE http_se = new HttpTransportSE(LoginTask.SOAP_ADDRESS);

			http_se.call(LoginTask.NAMESPACE + SOAP_OperationConstants.OPERATION_NAME_GET_MINUTES, envelope);

			Object result = envelope.getResponse();

			String getResponse = CryptographyUtils.Decrypt(result.toString());
			String[] responseArray = getResponse.split("[#]");
			
			sMinutes_Response = responseArray[0];
			SelectedGroupsTask.sLastTransactionDate_Response = responseArray[1];

			System.out.println("Service Response:" + sMinutes_Response);
			Constants.NETWORKCOMMONFLAG = "SUCCESS";
		} catch (IOException e) {
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (XmlPullParserException e) {
			// TODO: handle exception
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		}

		return Constants.NETWORKCOMMONFLAG;

	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		mListener.onTaskStarted();
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		if (result == null) {
			result = Constants.EXCEPTION;
			mListener.onTaskFinished(result);
		} else {
			mListener.onTaskFinished(result);
		}
	}

}
