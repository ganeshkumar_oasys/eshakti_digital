package com.yesteam.eshakti.webservices;

import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.utils.CryptographyUtils;
import com.yesteam.eshakti.view.fragment.Reports_GroupLoanListReportFragment;

import android.os.AsyncTask;
import android.util.Log;


public class Get_GroupLoanSummaryTask extends AsyncTask<String, String, String> {

	private TaskListener mListener;
	public static String sGroup_Loan_summary_response = null;

	public Get_GroupLoanSummaryTask(TaskListener listener) {
		// TODO Auto-generated constructor stub
		mListener = listener;
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub

		System.out.println("user_name " + SelectedGroupsTask.UserName.toString() + "Ngo_id :"
				+ SelectedGroupsTask.Ngo_Id.toString() + "Group_id" + SelectedGroupsTask.Group_Id.toString() + "loanid "
				+ Reports_GroupLoanListReportFragment.SEND_TO_SERVER_LOANID);
		SoapObject request = new SoapObject(LoginTask.NAMESPACE,
				SOAP_OperationConstants.OPERATION_NAME_GROUP_LOAN_SUMMARY);

		try {
			request.addProperty("user_name", CryptographyUtils.Encrypt(SelectedGroupsTask.UserName.toString()));
			request.addProperty("ngo_id", CryptographyUtils.Encrypt(SelectedGroupsTask.Ngo_Id.toString()));
			request.addProperty("group_id", CryptographyUtils.Encrypt(SelectedGroupsTask.Group_Id.toString()));
			request.addProperty("Loan_id",
					CryptographyUtils.Encrypt(Reports_GroupLoanListReportFragment.SEND_TO_SERVER_LOANID.toString()));
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Log.e("Get_GroupLoanSummaryTask", request.toString() + "");

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

		envelope.setOutputSoapObject(request);

		envelope.dotNet = true;

		try {

			HttpTransportSE http_se = new HttpTransportSE(LoginTask.SOAP_ADDRESS);

			http_se.call(LoginTask.NAMESPACE + SOAP_OperationConstants.OPERATION_NAME_GROUP_LOAN_SUMMARY, envelope);

			Object result = envelope.getResponse();

			sGroup_Loan_summary_response = CryptographyUtils.Decrypt(result.toString());

			System.out.println("Service Response:" + sGroup_Loan_summary_response);

			Constants.NETWORKCOMMONFLAG = "SUCCESS";

		} catch (IOException e) {
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (XmlPullParserException e) {
			// TODO: handle exception
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		}

		return Constants.NETWORKCOMMONFLAG;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		mListener.onTaskStarted();
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		if (result == null) {
			result = Constants.EXCEPTION;
			mListener.onTaskFinished(result);
		} else {
			mListener.onTaskFinished(result);
		}
	}

}
