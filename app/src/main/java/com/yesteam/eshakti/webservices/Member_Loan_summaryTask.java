package com.yesteam.eshakti.webservices;

import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.utils.CryptographyUtils;
import com.yesteam.eshakti.view.fragment.MemberList_Fragment;
import com.yesteam.eshakti.view.fragment.Reports_MemberReports_LoanSummaryMenuFragment;

import android.os.AsyncTask;
import android.util.Log;

public class Member_Loan_summaryTask extends AsyncTask<String, String, String> {

	private TaskListener mListener;

	public static String sMember_Loan_summary_ServiceResponse = null;

	public Member_Loan_summaryTask(TaskListener listener) {
		// TODO Auto-generated constructor stub
		this.mListener = listener;
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub

		SoapObject request = new SoapObject(LoginTask.NAMESPACE,
				SOAP_OperationConstants.OPERATION_NAME_MEMBER_LOAN_SUMMARY);

		try {
			request.addProperty("user_name", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.UserName)));
			request.addProperty("ngo_id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Ngo_Id)));
			request.addProperty("group_id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Group_Id)));
			request.addProperty("member_id",
					CryptographyUtils.Encrypt(String.valueOf(MemberList_Fragment.sSelected_MemberId)));
			request.addProperty("Loan_id", CryptographyUtils
					.Encrypt(String.valueOf(Reports_MemberReports_LoanSummaryMenuFragment.sReports_SlelectedLoanID)));
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Log.e("Member_Loan_summaryTask", request.toString() + "");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.setOutputSoapObject(request);
		envelope.dotNet = true;

		HttpTransportSE httpSe = new HttpTransportSE(LoginTask.SOAP_ADDRESS);

		try {
			httpSe.call(LoginTask.NAMESPACE + SOAP_OperationConstants.OPERATION_NAME_MEMBER_LOAN_SUMMARY, envelope);

			Object result = envelope.getResponse();

			sMember_Loan_summary_ServiceResponse = CryptographyUtils.Decrypt(String.valueOf(result));

			Constants.NETWORKCOMMONFLAG = "SUCCESS";

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		}

		return Constants.NETWORKCOMMONFLAG;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		mListener.onTaskStarted();
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		if (result == null) {
			result = Constants.EXCEPTION;
			mListener.onTaskFinished(result);
		} else {
			mListener.onTaskFinished(result);
		}
	}
}
