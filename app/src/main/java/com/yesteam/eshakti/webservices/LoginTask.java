package com.yesteam.eshakti.webservices;

import java.io.IOException;
import java.net.ConnectException;

import org.apache.http.conn.ConnectTimeoutException;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.utils.CryptographyUtils;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

@SuppressWarnings("deprecation")
public class LoginTask extends AsyncTask<String, String, String> {

	private TaskListener mListener;

	String mTag = LoginTask.class.getSimpleName();

	public static String NAMESPACE = "";

	public static String SOAP_ADDRESS = "";

	public static String sLoginTask_Response = null;

	public static String UserName = null;
	public static String Ngo_Id = null;
	public static String Trainer_Id = null;
	public static String User_RegLanguage = null;
	static String mUserType = null;

	Context context;

	public LoginTask(TaskListener listener) {
		this.mListener = listener;
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub

		try {

			SoapObject request = new SoapObject(LoginTask.NAMESPACE,
					SOAP_OperationConstants.OPERATION_NAME_GET_LOGINCHOOSE);

			Log.d(mTag, String.valueOf(LoginActivity.sUName) + "   " + String.valueOf(LoginActivity.sPwd));

			if (EShaktiApplication.getIMEI_NO() != null && !EShaktiApplication.getIMEI_NO().equals("")) {
				request.addProperty("UserName", CryptographyUtils.Encrypt(String.valueOf(LoginActivity.sUName)));
				request.addProperty("Password", CryptographyUtils.Encrypt(String.valueOf(LoginActivity.sPwd)));
				request.addProperty("IMEI", CryptographyUtils.Encrypt(EShaktiApplication.getIMEI_NO()));

			}
			Log.e(mTag, request.toString() + "");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.setOutputSoapObject(request);
			envelope.dotNet = true;

			HttpTransportSE httpSe = new HttpTransportSE(LoginTask.SOAP_ADDRESS);

			try {
				httpSe.call(LoginTask.NAMESPACE + SOAP_OperationConstants.OPERATION_NAME_GET_LOGINCHOOSE, envelope);

				Object result = envelope.getResponse();
				sLoginTask_Response = CryptographyUtils.Decrypt(result.toString());

				String arr[] = sLoginTask_Response.split("#");
				if (sLoginTask_Response.equals("LoginName or password incorrect.")) {
					Log.e("Login Server Response-->>>", sLoginTask_Response);
				} else {
					UserName = arr[0];
					System.out.println("UserName :" + UserName);

					Ngo_Id = arr[1];
					System.out.println("Ngo_Id :" + Ngo_Id);

					Trainer_Id = arr[2];
					System.out.println("Trainer_Id :" + Trainer_Id);

					SelectedGroupsTask.sTypeID = String.valueOf(arr[2]);
					System.out.println("SelectedGroupsTask.sTypeID : " + SelectedGroupsTask.sTypeID);

					User_RegLanguage = arr[3];
					System.out.println("User_RegLanguage : " + User_RegLanguage);

					mUserType = arr[4];

					if (mUserType != null) {
						if (mUserType.equals("ANIMATOR")) {
							EShaktiApplication.setmUserType("AGENT");
							EShaktiApplication.isAgent = true;
							PrefUtils.setAgentUserName(LoginActivity.sUName);
							PrefUtils.setAppUsertype("ANIMATOR");
						} else if (mUserType.equals("SHG")) {
							EShaktiApplication.setmUserType("GROUP");
							EShaktiApplication.isAgent = false;
							PrefUtils.setGroupUserName(LoginActivity.sUName);
							PrefUtils.setAppUsertype("SHG");
						}
					}
				}
				Constants.NETWORKCOMMONFLAG = "SUCCESS";

			} catch (ArrayIndexOutOfBoundsException e) {
				e.printStackTrace();
			} catch (ConnectTimeoutException e) {
				// TODO: handle exception
				e.printStackTrace();
				mListener.onTaskFinished(Constants.EXCEPTION);
				Log.e("TIME OUT ", "Success");
				Constants.NETWORKCOMMONFLAG = "FAIL";
			} catch (ConnectException e) {
				e.printStackTrace();
				Log.v("CONNECT EXCEPTION ", "Success");
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			} catch (XmlPullParserException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			}

		} catch (Exception e) {
			e.printStackTrace();
			Log.v("Yes!!!!!!!", "!!!!!!!!!");
		}

		return Constants.NETWORKCOMMONFLAG;
	}

	@Override
	protected void onPreExecute() {
		mListener.onTaskStarted();
	}

	@Override
	protected void onPostExecute(String result) {
		if (result == null) {
			result = Constants.EXCEPTION;

			mListener.onTaskFinished(result);
		} else {
			mListener.onTaskFinished(result);
		}
	}

}
