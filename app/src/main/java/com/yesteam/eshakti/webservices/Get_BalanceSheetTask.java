package com.yesteam.eshakti.webservices;

import java.util.Vector;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.utils.CryptographyUtils;
import com.yesteam.eshakti.view.fragment.Reports_Trial_BalanceSheetFragment;

import android.os.AsyncTask;
import android.util.Log;

public class Get_BalanceSheetTask extends AsyncTask<String, String, String> {

	private TaskListener mListener;
	public static String sBalanceSheet_Response = null;
	public static String[] result;

	public static String groupName;

	public static String ngoName;

	public static String bankName;

	public static Vector<String> cashFlowPaymentVector;

	public static Vector<String> cashFlowReceiptVector;

	public static String leftTotal, rightTotal;

	public static String incomeTotal, expenditureTotal;

	public static String last_left_Total, last_right_Total;

	public static Vector<String> income_total;
	public static Vector<String> expenditure_total;

	public static Vector<String> balance_left;

	public static Vector<String> balance_right;

	public Get_BalanceSheetTask(TaskListener listener) {
		// TODO Auto-generated constructor stub
		mListener = listener;
		cashFlowPaymentVector = new Vector<String>();
		cashFlowReceiptVector = new Vector<String>();
		income_total = new Vector<String>();
		expenditure_total = new Vector<String>();
		balance_left = new Vector<String>();
		balance_right = new Vector<String>();

	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		try {

			SoapObject request = new SoapObject(LoginTask.NAMESPACE,
					SOAP_OperationConstants.OPERATION_NAME_GET_BALANCESHEET);

			request.addProperty("User_Name", CryptographyUtils.Encrypt(SelectedGroupsTask.UserName));
			request.addProperty("Ngo_id", CryptographyUtils.Encrypt(SelectedGroupsTask.Ngo_Id));
			request.addProperty("Group_id", CryptographyUtils.Encrypt(SelectedGroupsTask.Group_Id));
			request.addProperty("FromDate", CryptographyUtils.Encrypt(Reports_Trial_BalanceSheetFragment.fromDate));
			request.addProperty("ToDate", CryptographyUtils.Encrypt(Reports_Trial_BalanceSheetFragment.toDate));

			Log.e("SERVICE REQUEST :-Get_BalanceSheetTask", request + "");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

			envelope.setOutputSoapObject(request);

			envelope.dotNet = true;

			try {

				HttpTransportSE http_se = new HttpTransportSE(LoginTask.SOAP_ADDRESS);
				http_se.call(LoginTask.NAMESPACE + SOAP_OperationConstants.OPERATION_NAME_GET_BALANCESHEET, envelope);

				System.out.println("Envelope test :" + envelope.getResponse());

				Object result = envelope.getResponse();
				Log.e("Encryption Values---->>>", result.toString());

				sBalanceSheet_Response = CryptographyUtils.Decrypt(result.toString());

				System.out.println("Service_Response : " + sBalanceSheet_Response);
				String responseArr[] = split(sBalanceSheet_Response, "#");
				String[] group_ngo_bank_details = null;
				String[] cash_flow_payment = null;
				String[] cash_flow_receipt = null;
				String[] income_array = null;
				String[] expenditure_array = null;
				String[] balance_left_array = null;
				String[] balance_right_array = null;

				try {
					for (int i = 0; i < responseArr.length; i++) {

						if (i == 0) {
							String groupdetails[] = split(responseArr[i], "!");
							for (int j = 0; j < groupdetails.length; j++) {
								if (j == 0) {
									group_ngo_bank_details = split(groupdetails[j], "~");
									System.out.println("groupdetails j==0 is" + groupdetails[j]);
								} else {
									cash_flow_payment = split(groupdetails[j], "~");
									System.out.println("groupdetails is" + groupdetails[j]);
								}
							}
						}
						if (i == 1) {
							String secondrow[] = split(responseArr[i], "!");
							for (int j = 0; j < secondrow.length; j++) {
								if (j == 0) {
									cash_flow_receipt = split(secondrow[j], "~");
									System.out.println("secondcolumn j==0 is" + secondrow[j]);
								} else {
									income_array = split(secondrow[j], "~");
									if (j == 1) {
										String total[] = split(secondrow[j], "~");
										leftTotal = total[1];
										System.out.println("leftTotal :" + leftTotal);
										rightTotal = total[3];
										System.out.println("rightTotal :" + rightTotal);
									}
									System.out.println("secondcolumn is" + secondrow[j]);
								}
							}
						}
						if (i == 2) {
							String thirdrow[] = split(responseArr[i], "!");
							for (int j = 0; j < thirdrow.length; j++) {
								if (j == 0) {
									expenditure_array = split(thirdrow[j], "~");
									System.out.println("thirdcolumn j==0 is" + thirdrow[j]);
								} else if (j == 1) {
									String total[] = split(thirdrow[j], "~");
									incomeTotal = total[1];
									expenditureTotal = total[3];
									System.out.println("thirdcolumn j==1 is incomeTotal : " + incomeTotal);
									System.out.println("thirdcolumn j==1 is expenditureTotal : " + expenditureTotal);
									System.out.println("thirdcolumn j==1 is" + thirdrow[j]);
								} else {
									balance_left_array = split(thirdrow[j], "~");
									System.out.println("thirdcolumn is" + thirdrow[j]);
								}
							}
						}
						if (i > 2) {
							if (i == 3) {
								String[] findTotal = split(responseArr[i], "!");
								balance_right_array = split(findTotal[0], "~");
								String[] grapTotal = split(findTotal[1], "~");
								last_left_Total = grapTotal[1];
								last_right_Total = grapTotal[3];
							}
							System.out.println("arr[i] : " + i + " value :" + responseArr[i].toString());
						}
					}
				} catch (Exception e) {
					System.out.println("Exception in parsing :" + e.toString());
				}

				groupName = group_ngo_bank_details[0] + " " + group_ngo_bank_details[1];

				ngoName = group_ngo_bank_details[2];

				bankName = group_ngo_bank_details[3] + " " + group_ngo_bank_details[4] + " "
						+ group_ngo_bank_details[5];

				int size = cash_flow_payment.length;

				for (int i = 0; i < size; i++) {
					cashFlowPaymentVector.add(cash_flow_payment[i].toString());
				}

				size = cash_flow_receipt.length;

				for (int i = 0; i < size; i++) {
					cashFlowReceiptVector.add(cash_flow_receipt[i].toString());
				}
				size = income_array.length;
				for (int i = 0; i < size; i++) {
					income_total.add(income_array[i].toString());
					System.out.println("income_total[] :" + income_total.elementAt(i).toString());
				}
				size = expenditure_array.length;
				for (int i = 0; i < size; i++) {
					expenditure_total.add(expenditure_array[i].toString());
					System.out.println("expenditure_total[] :" + expenditure_total.elementAt(i).toString());
				}
				size = balance_left_array.length;
				System.out.println("Size------>" + size);
				for (int i = 0; i < size; i++) {
					balance_left.add(balance_left_array[i].toString());
					System.out.println("balance_left[] :" + balance_left.elementAt(i).toString());
				}
				size = balance_right_array.length;
				for (int i = 0; i < size; i++) {
					balance_right.add(balance_right_array[i].toString());
				}

				Constants.NETWORKCOMMONFLAG = "SUCCESS";

			} catch (Exception e) {
				e.printStackTrace();
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return Constants.NETWORKCOMMONFLAG;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private String[] split(String original, String split) {
		// TODO Auto-generated method stub
		try {
			original.trim();
			Vector nodes = new Vector();
			String separator = split;

			int index = original.indexOf(separator);
			while (index >= 0) {
				nodes.addElement(original.substring(0, index));
				original = original.substring(index + separator.length());
				index = original.indexOf(separator);
			}
			nodes.addElement(original);

			result = new String[nodes.size()];
			if (nodes.size() > 0) {
				for (int loop = 0; loop < nodes.size(); loop++) {
					result[loop] = (String) nodes.elementAt(loop);
				}
			}
		} catch (Exception e) {
			System.out.println("");
		}

		return result;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		mListener.onTaskStarted();
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		if (result == null) {
			result = Constants.EXCEPTION;
			mListener.onTaskFinished(result);
		} else {
			mListener.onTaskFinished(result);
		}
	}

}
