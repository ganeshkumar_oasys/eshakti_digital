package com.yesteam.eshakti.webservices;

import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.utils.CryptographyUtils;

import android.os.AsyncTask;
import android.util.Log;

public class Get_GroupProfileTask extends AsyncTask<String, String, String> {

	private TaskListener mListener;

	String mTag = Get_GroupProfileTask.class.getSimpleName();

	public static String sGetGroupProfile_Response = null;

	public Get_GroupProfileTask(TaskListener listener) {
		// TODO Auto-generated constructor stub
		this.mListener = listener;
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		try {
			SoapObject request = new SoapObject(LoginTask.NAMESPACE,
					SOAP_OperationConstants.OPERATION_NAME_GETGROUPPROFILE);

			System.out.println("Ngo_Id :" + SelectedGroupsTask.Ngo_Id.toString() + "Group_Id:"
					+ String.valueOf(SelectedGroupsTask.Group_Id));

			try {
				request.addProperty("Ngo_Id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Ngo_Id)));
				request.addProperty("Group_Id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Group_Id)));
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			Log.e(mTag, request.toString()+"");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

			envelope.setOutputSoapObject(request);

			envelope.dotNet = true;

			HttpTransportSE http_se = new HttpTransportSE(LoginTask.SOAP_ADDRESS);
			try {
				http_se.call(LoginTask.NAMESPACE + SOAP_OperationConstants.OPERATION_NAME_GETGROUPPROFILE, envelope);

				Object result = envelope.getResponse();

				sGetGroupProfile_Response = CryptographyUtils.Decrypt(result.toString());

				System.out.println("Service Response:" + sGetGroupProfile_Response);

				Constants.NETWORKCOMMONFLAG = "SUCCESS";
			} catch (IOException e) {
				e.printStackTrace();
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			} catch (XmlPullParserException e) {
				// TODO: handle exception
				e.printStackTrace();
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return Constants.NETWORKCOMMONFLAG;

	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		mListener.onTaskStarted();
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub

		if (result == null) {
			result = Constants.EXCEPTION;
			mListener.onTaskFinished(result);
		} else {
			mListener.onTaskFinished(result);
		}
	}
}
