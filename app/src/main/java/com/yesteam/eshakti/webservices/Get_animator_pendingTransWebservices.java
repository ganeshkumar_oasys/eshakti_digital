package com.yesteam.eshakti.webservices;

import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.utils.CryptographyUtils;
import com.yesteam.eshakti.utils.PrefUtils;

import android.os.AsyncTask;

public class Get_animator_pendingTransWebservices extends AsyncTask<String, String, String> {

	private TaskListener mListener;

	String mTag = Get_AttendanceTask.class.getSimpleName();

	public static String get_AnimatorTrans_Response = null;

	public Get_animator_pendingTransWebservices(TaskListener listener) {
		// TODO Auto-generated constructor stub
		this.mListener = listener;
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub

		try {
			SoapObject request = new SoapObject(LoginTask.NAMESPACE,
					SOAP_OperationConstants.OPERATION_NAME_ANIMATOR_PENDING);

			request.addProperty("ngo_id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Ngo_Id)));
			request.addProperty("Trainer_Id", CryptographyUtils.Encrypt(String.valueOf(LoginTask.Trainer_Id)));

			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

			envelope.setOutputSoapObject(request);

			envelope.dotNet = true;

			try {

				HttpTransportSE http_se = new HttpTransportSE(LoginTask.SOAP_ADDRESS);

				http_se.call(LoginTask.NAMESPACE + SOAP_OperationConstants.OPERATION_NAME_ANIMATOR_PENDING, envelope);

				Object result = envelope.getResponse();

				get_AnimatorTrans_Response = CryptographyUtils.Decrypt(result.toString());
				PrefUtils.setAnimatorTargetValues(get_AnimatorTrans_Response);

				System.out.println("Service Response:" + get_AnimatorTrans_Response);

				Constants.NETWORKCOMMONFLAG = "SUCESS";

			} catch (IOException e) {
				e.printStackTrace();
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			} catch (XmlPullParserException e) {
				// TODO: handle exception
				e.printStackTrace();
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return Constants.NETWORKCOMMONFLAG;

	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		mListener.onTaskStarted();
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		if (result == null) {
			result = Constants.EXCEPTION;
			mListener.onTaskFinished(result);
		} else {
			mListener.onTaskFinished(result);
		}
	}
}
