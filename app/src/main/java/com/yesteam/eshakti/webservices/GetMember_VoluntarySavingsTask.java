package com.yesteam.eshakti.webservices;

import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;
import com.yesteam.eshakti.utils.CryptographyUtils;
import com.yesteam.eshakti.utils.GetIndividualInfo;
import com.yesteam.eshakti.utils.GetResponseInfo;
import com.yesteam.eshakti.view.fragment.Transaction_SavingsFragment;

import android.os.AsyncTask;
import android.util.Log;

public class GetMember_VoluntarySavingsTask extends AsyncTask<String, String, String> {

	private TaskListener mListener;

	String sendLoanInfo = "";
	String sGetMember_VoluntarySavingsTask = null;

	public GetMember_VoluntarySavingsTask(TaskListener listener) {
		// TODO Auto-generated constructor stub
		this.mListener = listener;
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub

		sendLoanInfo = "";

		sendLoanInfo = GetIndividualInfo.getLoanInfo();
		System.out.println("LOAN INFO : " + sendLoanInfo);

		SoapObject request = new SoapObject(LoginTask.NAMESPACE,
				SOAP_OperationConstants.OPERATION_NAME_GET_MEMBER_VOLUNTARY_SAVINGS);

		try {
			request.addProperty("User_Name", CryptographyUtils.Encrypt(String.valueOf(LoginTask.UserName)));
			request.addProperty("Ngo_id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Ngo_Id)));
			request.addProperty("Group_id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Group_Id)));
			request.addProperty("Values",
					CryptographyUtils.Encrypt(Transaction_SavingsFragment.sSendToServer_VSavings));// tempvalue
			request.addProperty("Loanid", CryptographyUtils.Encrypt(sendLoanInfo));
			request.addProperty("Trdate", CryptographyUtils.Encrypt(DatePickerDialog.sSend_To_Server_Date));// pas
			// the
			// calendar
			// date
			// in
			// the
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Log.e("User_Name GetMemberVSavings", String.valueOf(LoginTask.UserName));
		Log.e("Ngo_id GetMemberVSavings", String.valueOf(SelectedGroupsTask.Ngo_Id));
		Log.e("Group_id GetMemberVSavings", String.valueOf(SelectedGroupsTask.Group_Id));
		Log.e("Values GetMemberVSavings", String.valueOf(Transaction_SavingsFragment.sSendToServer_VSavings));
		Log.e("Loanid GetMemberVSavings", sendLoanInfo);
		Log.e("Trdate GetMemberVSavings", DatePickerDialog.sSend_To_Server_Date);
		// format of mm/dd/yyyy
		Log.e("All Response Values::", request + "");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.setOutputSoapObject(request);
		envelope.dotNet = true;

		HttpTransportSE httpSe = new HttpTransportSE(LoginTask.SOAP_ADDRESS);
		try {

			httpSe.call(LoginTask.NAMESPACE + SOAP_OperationConstants.OPERATION_NAME_GET_MEMBER_VOLUNTARY_SAVINGS,
					envelope);

			Object result = envelope.getResponse();

			sGetMember_VoluntarySavingsTask = CryptographyUtils.Decrypt(result.toString());

			Log.v("VS RESPONSE ", sGetMember_VoluntarySavingsTask);

			String[] sResponseUpdate = sGetMember_VoluntarySavingsTask.split("#");

			if (sResponseUpdate[0].equals("Yes")) {
				GetResponseInfo.getResponseInfo(sGetMember_VoluntarySavingsTask);
			}

			Constants.NETWORKCOMMONFLAG = "SUCCESS";

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		}
		return Constants.NETWORKCOMMONFLAG;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		mListener.onTaskStarted();
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		if (result == null) {
			result = Constants.EXCEPTION;
			mListener.onTaskFinished(result);
		} else {
			mListener.onTaskFinished(result);
		}

	}
}
