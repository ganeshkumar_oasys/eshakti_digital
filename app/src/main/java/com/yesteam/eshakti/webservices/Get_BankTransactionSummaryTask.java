package com.yesteam.eshakti.webservices;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.utils.CryptographyUtils;

import android.os.AsyncTask;
import android.util.Log;

public class Get_BankTransactionSummaryTask extends AsyncTask<String, String, String> {

	private TaskListener mListener;
	public static String sBank_summary_Response = null;

	public Get_BankTransactionSummaryTask(TaskListener listener) {
		// TODO Auto-generated constructor stub
		mListener = listener;
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		try {
			SoapObject request = new SoapObject(LoginTask.NAMESPACE,
					SOAP_OperationConstants.OPERATION_NAME_BANK_SUMMARY);

			request.addProperty("user_name", CryptographyUtils.Encrypt(SelectedGroupsTask.UserName.toString()));
			request.addProperty("ngo_id", CryptographyUtils.Encrypt(SelectedGroupsTask.Ngo_Id.toString()));
			request.addProperty("group_id", CryptographyUtils.Encrypt(SelectedGroupsTask.Group_Id.toString()));
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

			Log.e("Get_BankTransactionSummaryTask", request.toString() + "");

			envelope.setOutputSoapObject(request);

			envelope.dotNet = true;

			try {

				HttpTransportSE http_se = new HttpTransportSE(LoginTask.SOAP_ADDRESS);
				http_se.call(LoginTask.NAMESPACE + SOAP_OperationConstants.OPERATION_NAME_BANK_SUMMARY, envelope);

				System.out.println("Envelope Test :" + envelope.getResponse());

				Object result = envelope.getResponse();

				sBank_summary_Response = CryptographyUtils.Decrypt(result.toString());

				System.out.println("Service_Response :" + sBank_summary_Response);
				Constants.NETWORKCOMMONFLAG = "SUCCESS";

			} catch (Exception e) {
				System.out.println("Exception :" + e.toString());
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return Constants.NETWORKCOMMONFLAG;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		mListener.onTaskStarted();
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		if (result == null) {
			result = Constants.EXCEPTION;
			mListener.onTaskFinished(result);
		} else {
			mListener.onTaskFinished(result);
		}
	}

}
