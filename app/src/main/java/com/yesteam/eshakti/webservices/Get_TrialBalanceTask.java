package com.yesteam.eshakti.webservices;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.utils.CryptographyUtils;
import com.yesteam.eshakti.view.fragment.Reports_Trial_BalanceSheetFragment;

import android.os.AsyncTask;
import android.util.Log;


public class Get_TrialBalanceTask extends AsyncTask<String, String, String> {

	private TaskListener mListener;
	public static String sTrailBalance_Response = null;
	public static String responseArr[];

	public Get_TrialBalanceTask(TaskListener listener) {
		// TODO Auto-generated constructor stub
		mListener = listener;
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		try {

			SoapObject request = new SoapObject(LoginTask.NAMESPACE,
					SOAP_OperationConstants.OPERATION_NAME_GET_TRIALBALANCE);

			request.addProperty("User_Name", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.UserName)));
			request.addProperty("Ngo_id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Ngo_Id)));
			request.addProperty("Group_id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Group_Id)));
			request.addProperty("FromDate",
					CryptographyUtils.Encrypt(String.valueOf(Reports_Trial_BalanceSheetFragment.fromDate)));
			request.addProperty("ToDate", CryptographyUtils.Encrypt(String.valueOf(Reports_Trial_BalanceSheetFragment.toDate)));
			Log.e("SERVICE REQUEST : - Get_TrialBalanceTask", request + "");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

			envelope.setOutputSoapObject(request);
			envelope.dotNet = true;

			try {
				HttpTransportSE http_se = new HttpTransportSE(LoginTask.SOAP_ADDRESS);

				http_se.call(LoginTask.NAMESPACE + SOAP_OperationConstants.OPERATION_NAME_GET_TRIALBALANCE, envelope);

				Object result = envelope.getResponse();

				sTrailBalance_Response = CryptographyUtils.Decrypt(result.toString());

				responseArr = sTrailBalance_Response.split("#");
				for (int i = 0; i < responseArr.length; i++) {
					System.out.println("responseArr val:" + responseArr[i].toString());
				}

				Constants.NETWORKCOMMONFLAG = "SUCCESS";

			} catch (Exception e) {
				e.printStackTrace();
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return Constants.NETWORKCOMMONFLAG;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		mListener.onTaskStarted();
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		if (result == null) {
			result = Constants.EXCEPTION;
			mListener.onTaskFinished(result);
		} else {
			mListener.onTaskFinished(result);
		}
	}
}
