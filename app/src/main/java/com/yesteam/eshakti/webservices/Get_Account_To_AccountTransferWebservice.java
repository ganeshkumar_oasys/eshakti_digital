package com.yesteam.eshakti.webservices;

import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;
import com.yesteam.eshakti.utils.CryptographyUtils;
import com.yesteam.eshakti.utils.GetResponseInfo;
import com.yesteam.eshakti.view.fragment.Transaction_AcctoAccTransferFragment;

import android.os.AsyncTask;
import android.util.Log;

public class Get_Account_To_AccountTransferWebservice extends AsyncTask<String, String, String> {

	private TaskListener mListener;
	public static String sGet_AccountTransfer_Response = null;

	public Get_Account_To_AccountTransferWebservice(TaskListener listener) {
		// TODO Auto-generated constructor stub
		this.mListener = listener;
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		SoapObject request = new SoapObject(LoginTask.NAMESPACE,
				SOAP_OperationConstants.OPERATION_NAME_GET_BANK_ACCOUNT_TRANSFER);

		try {
			if (LoginTask.UserName != null && SelectedGroupsTask.Ngo_Id != null && SelectedGroupsTask.Group_Id != null
					&& EShaktiApplication.getAcctoaccSendtoserverBank() != null
					&& Transaction_AcctoAccTransferFragment.mBankNameValue != null
					&& Transaction_AcctoAccTransferFragment.mAcctransferAmount != null
					&& Transaction_AcctoAccTransferFragment.mAcctransferCharge != null
					&& DatePickerDialog.sSend_To_Server_Date != null) {

				request.addProperty("User_Name", CryptographyUtils.Encrypt(String.valueOf(LoginTask.UserName)));
				request.addProperty("Ngo_id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Ngo_Id)));
				request.addProperty("Group_id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Group_Id)));
				request.addProperty("From_Bank_Name",
						CryptographyUtils.Encrypt(String.valueOf(EShaktiApplication.getAcctoaccSendtoserverBank())));
				request.addProperty("To_Bank_Name", CryptographyUtils.Encrypt(Transaction_AcctoAccTransferFragment.mBankNameValue));
				request.addProperty("Deposit", CryptographyUtils.Encrypt(Transaction_AcctoAccTransferFragment.mAcctransferAmount));
				request.addProperty("Bank_Charges",
						CryptographyUtils.Encrypt(Transaction_AcctoAccTransferFragment.mAcctransferCharge));
				request.addProperty("Trdate",
						CryptographyUtils.Encrypt(String.valueOf(DatePickerDialog.sSend_To_Server_Date)));
			}

		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.setOutputSoapObject(request);
		envelope.dotNet = true;

		HttpTransportSE httpSe = new HttpTransportSE(LoginTask.SOAP_ADDRESS);
		try {

			httpSe.call(LoginTask.NAMESPACE + SOAP_OperationConstants.OPERATION_NAME_GET_BANK_ACCOUNT_TRANSFER,
					envelope);

			Object result = envelope.getResponse();

			sGet_AccountTransfer_Response = CryptographyUtils.Decrypt(String.valueOf(result));
			GetResponseInfo.getResponseInfo(sGet_AccountTransfer_Response);
			Log.e("Acc to Acc Bank Values---->>>>", sGet_AccountTransfer_Response);
		

			Constants.NETWORKCOMMONFLAG = "SUCCESS";

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		}

		return Constants.NETWORKCOMMONFLAG;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		mListener.onTaskStarted();
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		if (result == null) {
			result = Constants.EXCEPTION;
			mListener.onTaskFinished(result);
		} else {
			Log.e("Cash at bank ", SelectedGroupsTask.sCashatBank);
			mListener.onTaskFinished(result);
		}
	}

}