package com.yesteam.eshakti.webservices;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.utils.CryptographyUtils;
import com.yesteam.eshakti.view.fragment.MemberList_Fragment;
import com.yesteam.eshakti.view.fragment.Reports_MonthYear_PickerReportsFragment;

import android.os.AsyncTask;
import android.util.Log;

public class MonthReportTask extends AsyncTask<String, String, String> {

	private TaskListener mListener;
	public static String sMonthSummary_Response = null;

	public MonthReportTask(TaskListener listener) {
		// TODO Auto-generated constructor stub
		mListener = listener;
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		try {
			SoapObject request = new SoapObject(LoginTask.NAMESPACE,
					SOAP_OperationConstants.OPERATION_NAME_GET_MONTH_SUMMARY);

			request.addProperty("ngo_id", CryptographyUtils.Encrypt(SelectedGroupsTask.Ngo_Id.toString()));
			request.addProperty("g_id", CryptographyUtils.Encrypt(SelectedGroupsTask.Group_Id.toString()));
			request.addProperty("m_id", CryptographyUtils.Encrypt(MemberList_Fragment.sSelected_MemberId.toString()));
			request.addProperty("Month",
					CryptographyUtils.Encrypt(Reports_MonthYear_PickerReportsFragment.monthValue.toString()));
			request.addProperty("Year",
					CryptographyUtils.Encrypt(Reports_MonthYear_PickerReportsFragment.yearValue.toString()));
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

			Log.e("MonthReportTask", request.toString() + "");

			envelope.setOutputSoapObject(request);

			envelope.dotNet = true;
			try {

				HttpTransportSE http_se = new HttpTransportSE(LoginTask.SOAP_ADDRESS);
				http_se.call(LoginTask.NAMESPACE + SOAP_OperationConstants.OPERATION_NAME_GET_MONTH_SUMMARY, envelope);

				System.out.println("Envelope Test :" + envelope.getResponse());

				Object result = envelope.getResponse();

				sMonthSummary_Response = CryptographyUtils.Decrypt(result.toString());

				System.out.println("Service_Response :" + sMonthSummary_Response);

				Constants.NETWORKCOMMONFLAG = "SUCCESS";

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return Constants.NETWORKCOMMONFLAG;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		mListener.onTaskStarted();
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		if (result == null) {
			result = Constants.EXCEPTION;
			mListener.onTaskFinished(result);
		} else {
			mListener.onTaskFinished(result);
		}
	}

}
