package com.yesteam.eshakti.webservices;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.utils.Configuration;
import com.yesteam.eshakti.view.fragment.MemberList_Fragment;
import com.yesteam.eshakti.view.fragment.Profile_MemberPhoto_AadharInfoMenuFragment;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

public class MemberImageUploadTask extends AsyncTask<Void, Void, String> {
	private TaskListener mListener;
	private Bitmap mFilename;

	static String encodedString;

	public MemberImageUploadTask(TaskListener listener, Bitmap fileName) {
		// TODO Auto-generated constructor stub
		this.mListener = listener;
		this.mFilename = fileName;

	}

	@Override
	protected String doInBackground(Void... params) {
		String response = "";

		uploadImage(mFilename);

		return Constants.NETWORKCOMMONFLAG;// response;
	}

	@Override
	protected void onPreExecute() {
		mListener.onTaskStarted();
	}

	@Override
	protected void onPostExecute(String result) {
		if (result == null) {
			result = Constants.EXCEPTION;
			mListener.onTaskFinished(result);
		} else {
			mListener.onTaskFinished(result);
		}
	}

	public String uploadImage(Bitmap fileName) {

		// String TAG = "Service Unavailable.. Check Internet Settings";
		// String resuq = "";
		String uimgstatus = null;

		// getImageRotation(mFile);

		String mUsername, mNgoId, mGroupId, mMemberId;
		mUsername = LoginTask.UserName;
		mNgoId = SelectedGroupsTask.Ngo_Id;
		mGroupId = SelectedGroupsTask.Group_Id;
		mMemberId = MemberList_Fragment.sSelected_MemberId;
		Log.v("Member Id", mMemberId);
		encodedString = Configuration.toByteCode(fileName);
		if (TextUtils.equals(encodedString, Constants.EXCEPTION)) {
			return Constants.EXCEPTION;
		}

		// LoginTask.NAMESPACE = "http://yshpiaws.yesbooks.in/";
		SoapObject request = new SoapObject(LoginTask.NAMESPACE, SOAP_OperationConstants.OPERATION_NAME_GET_IMAGE);
		Log.v("WEB Services", LoginTask.NAMESPACE + SOAP_OperationConstants.OPERATION_NAME_GET_IMAGE);
		try {
			request.addProperty("UserName", mUsername);
			request.addProperty("Ngo_Id", mNgoId);
			request.addProperty("Group_Id", mGroupId);
			request.addProperty("Mem_Id", mMemberId);
			request.addProperty("image", encodedString);

		} catch (Exception e) {
			e.getStackTrace();
		}
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.setOutputSoapObject(request);
		envelope.dotNet = true;
		// LoginTask.SOAP_ADDRESS = "http://yshpiaws.yesbooks.in/service.asmx";
		HttpTransportSE androidHttpTransport = new HttpTransportSE(LoginTask.SOAP_ADDRESS);
		Log.i("step 2", "222222222222");
		System.out.println("Request String Values ---- >>>>>>>>>>>>>>         " + request.toString());
		try {

			Log.i("Step 3", "33333333333333333");
			androidHttpTransport.call(LoginTask.NAMESPACE + SOAP_OperationConstants.OPERATION_NAME_GET_IMAGE, envelope);
			Log.i("Step 4", "4444444444444444");
			Object result = envelope.getResponse();

			String sGroupLoginTask_Response = String.valueOf(result);
			Log.i("Image Output String", sGroupLoginTask_Response);

			Profile_MemberPhoto_AadharInfoMenuFragment.mServiceResponse = sGroupLoginTask_Response;

			Constants.NETWORKCOMMONFLAG = "SUCCESS";

		} catch (Exception e) {
			Log.v("Exception", "Values $$$$");
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		}
		Log.v("Step 5", "555555555555555");
		return uimgstatus;
	}

}
