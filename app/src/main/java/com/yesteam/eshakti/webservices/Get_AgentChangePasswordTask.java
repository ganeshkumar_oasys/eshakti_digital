package com.yesteam.eshakti.webservices;

import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.utils.CryptographyUtils;
import com.yesteam.eshakti.view.fragment.Settings_ChangePasswordFragment;

import android.os.AsyncTask;
import android.util.Log;

public class Get_AgentChangePasswordTask extends AsyncTask<String, String, String> {

	private TaskListener mListener;

	String mTag = Get_AgentChangePasswordTask.class.getSimpleName();

	public static String sGet_AgentChangpassword_ServiceResponse = null;

	public Get_AgentChangePasswordTask(TaskListener listener) {
		// TODO Auto-generated constructor stub
		this.mListener = listener;
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		SoapObject request = new SoapObject(LoginTask.NAMESPACE,
				SOAP_OperationConstants.OPERATION_NAME_GET_AGENTCHANGEPASSWORD);

		try {
			request.addProperty("User_Name", CryptographyUtils.Encrypt(String.valueOf(LoginTask.UserName)));
			request.addProperty("Ngo_id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Ngo_Id)));
			request.addProperty("Agent_id", CryptographyUtils.Encrypt(String.valueOf(LoginTask.Trainer_Id)));
			request.addProperty("Old_Password",
					CryptographyUtils.Encrypt(String.valueOf(Settings_ChangePasswordFragment.OldPwd)));
			request.addProperty("New_Password",
					CryptographyUtils.Encrypt(String.valueOf(Settings_ChangePasswordFragment.NewPwd)));
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Log.e("SERVICE REQUEST : - Get_AgentChangePasswordTask", request + "");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

		envelope.setOutputSoapObject(request);

		envelope.dotNet = true;

		try {

			HttpTransportSE http_se = new HttpTransportSE(LoginTask.SOAP_ADDRESS);

			http_se.call(LoginTask.NAMESPACE + SOAP_OperationConstants.OPERATION_NAME_GET_AGENTCHANGEPASSWORD,
					envelope);

			Object result = envelope.getResponse();

			sGet_AgentChangpassword_ServiceResponse = CryptographyUtils.Decrypt(result.toString());

			System.out.println("Service Response:" + sGet_AgentChangpassword_ServiceResponse);

			Constants.NETWORKCOMMONFLAG = "SUCCESS";
		} catch (IOException e) {
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (XmlPullParserException e) {
			// TODO: handle exception
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		}

		return Constants.NETWORKCOMMONFLAG;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		mListener.onTaskStarted();
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		if (result == null) {
			result = Constants.EXCEPTION;
			mListener.onTaskFinished(result);
		} else {
			mListener.onTaskFinished(result);
		}
	}
}
