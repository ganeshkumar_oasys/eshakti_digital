package com.yesteam.eshakti.webservices;

import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.utils.CryptographyUtils;

import android.os.AsyncTask;

public class Get_AgentProfileTask extends AsyncTask<String, String, String> {

	private TaskListener mListener;

	String mTag = Get_AgentProfileTask.class.getSimpleName();

	public static String sGetAgentProfile_Response = null;

	public Get_AgentProfileTask(TaskListener listener) {
		// TODO Auto-generated constructor stub
		this.mListener = listener;
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		SoapObject request = new SoapObject(LoginTask.NAMESPACE,
				SOAP_OperationConstants.OPERATION_NAME_GETAGENTPROFILE);

		System.out.println(
				"Ngo_Id :" + SelectedGroupsTask.Ngo_Id.toString() + "Agent_id:" + LoginTask.Trainer_Id.toString());

		try {
			request.addProperty("Ngo_Id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Ngo_Id)));
			request.addProperty("Agent_Id", CryptographyUtils.Encrypt(LoginTask.Trainer_Id.toString()));
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

		envelope.setOutputSoapObject(request);

		envelope.dotNet = true;

		try {

			HttpTransportSE http_se = new HttpTransportSE(LoginTask.SOAP_ADDRESS);

			http_se.call(LoginTask.NAMESPACE + SOAP_OperationConstants.OPERATION_NAME_GETAGENTPROFILE, envelope);

			Object result = envelope.getResponse();

			sGetAgentProfile_Response = CryptographyUtils.Decrypt(result.toString());

			System.out.println("Service Response:" + sGetAgentProfile_Response);

			Constants.NETWORKCOMMONFLAG = "SUCCESS";

		} catch (IOException e) {
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (XmlPullParserException e) {
			// TODO: handle exception
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		}

		return Constants.NETWORKCOMMONFLAG;

	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		mListener.onTaskStarted();
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		if (result == null) {
			result = Constants.EXCEPTION;
			mListener.onTaskFinished(result);
		} else {
			mListener.onTaskFinished(result);
		}
	}
}
