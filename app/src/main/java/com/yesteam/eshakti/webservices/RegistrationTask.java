package com.yesteam.eshakti.webservices;

import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.utils.CryptographyUtils;
import com.yesteam.eshakti.view.activity.RegisterActivity;

import android.os.AsyncTask;
import android.util.Log;

public class RegistrationTask extends AsyncTask<String, String, String> {

	private TaskListener mListener;

	public static String sRegistration_ServiceResponse = null;

	public RegistrationTask(TaskListener listener) {
		// TODO Auto-generated constructor stub
		this.mListener = listener;
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub

		SoapObject request = new SoapObject(LoginTask.NAMESPACE, SOAP_OperationConstants.OPERATION_NAME_REGISTRATION);

		try {
			request.addProperty("UserName", CryptographyUtils.Encrypt(String.valueOf(RegisterActivity.reg_UserName)));
			request.addProperty("Password", CryptographyUtils.Encrypt(String.valueOf(RegisterActivity.reg_Password)));
			request.addProperty("Mob_No", CryptographyUtils.Encrypt(String.valueOf(RegisterActivity.reg_MobileNo)));
			request.addProperty("IMEI_No", CryptographyUtils.Encrypt(String.valueOf(RegisterActivity.reg_IMEI)));
			Log.e("RegistrationTask", request.toString() + "");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.setOutputSoapObject(request);
		envelope.dotNet = true;

		HttpTransportSE httpSe = new HttpTransportSE(LoginTask.SOAP_ADDRESS);

		try {
			httpSe.call(LoginTask.NAMESPACE + SOAP_OperationConstants.OPERATION_NAME_REGISTRATION, envelope);

			Object result = envelope.getResponse();

			sRegistration_ServiceResponse = CryptographyUtils.Decrypt(String.valueOf(result));
			
			Log.e("-----------------------?????",sRegistration_ServiceResponse);

			Constants.NETWORKCOMMONFLAG = "SUCCESS";

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			mListener.onTaskFinished(Constants.EXCEPTION);
			Constants.NETWORKCOMMONFLAG = "FAIL";
		}

		return Constants.NETWORKCOMMONFLAG;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		mListener.onTaskStarted();
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		if (result == null) {
			result = Constants.EXCEPTION;
			mListener.onTaskFinished(result);
		} else {
			mListener.onTaskFinished(result);
		}
	}
}
