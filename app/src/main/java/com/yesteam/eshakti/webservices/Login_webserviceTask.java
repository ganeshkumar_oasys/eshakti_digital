package com.yesteam.eshakti.webservices;

import java.io.IOException;
import java.net.ConnectException;
import java.util.Vector;

import org.apache.http.conn.ConnectTimeoutException;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.appConstants.publicValues;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.utils.CryptographyUtils;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.view.activity.LoginActivity;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.util.Log;

@SuppressWarnings("deprecation")
public class Login_webserviceTask extends AsyncTask<String, String, String> {

	private TaskListener mListener;

	String mTag = LoginTask.class.getSimpleName();

	public static String NAMESPACE = "";

	public static String SOAP_ADDRESS = "";

	public static String sLoginTask_Response = null;

	public static String UserName = null;
	public static String Ngo_Id = null;
	public static String Trainer_Id = null;
	public static String User_RegLanguage = null;
	String mUserType = null;

	public static String mGroupActiveStatus = null;

	Context context;
	public static String sAgentId;
	public static String sAgent_ngoId;
	public static Vector<String> sAgent_GIDs;
	public static Vector<String> sAgent_Gname;
	public static Vector<String> sAgent_GActiveStatus;
	public static Vector<String> sGroupTransId;

	public static Vector<String> sSHG_Codes;
	public static Vector<String> sSHG_BlockNames;
	public static Vector<String> sSHG_PanchayatNames;
	public static Vector<String> sSHG_VillageNames;
	public static Vector<String> sSHG_TransactionDate;

	public static Vector<String> sSHG_Codes_Label;
	public static Vector<String> sSHG_BlockNames_Label;
	public static Vector<String> sSHG_PanchayatNames_Label;
	public static Vector<String> sSHG_VillageNames_Label;
	public static Vector<String> sSHG_TransactionDate_Label;

	public static Vector<String> _Audit_VerifiedValue;
	public static Vector<String> _VerifiedLastAuditDate;

	String[] groupList, shgcodeList, groupDetails;
	String[] mGroupListMaster;
	public static String mViewGroupValues;
	public static String mAnimatorName = null;
	public static String[] mTitleValues = null;
	public static boolean mFirstAddTitle = false;
	public static String mMasterValues = null;

	public Login_webserviceTask(TaskListener listener) {
		this.mListener = listener;
		sAgent_GIDs = new Vector<String>();
		sAgent_Gname = new Vector<String>();
		sAgent_GActiveStatus = new Vector<String>();
		sGroupTransId = new Vector<String>();

		sSHG_Codes = new Vector<String>();
		sSHG_BlockNames = new Vector<String>();
		sSHG_PanchayatNames = new Vector<String>();
		sSHG_VillageNames = new Vector<String>();
		sSHG_TransactionDate = new Vector<String>();

		sSHG_Codes_Label = new Vector<String>();
		sSHG_BlockNames_Label = new Vector<String>();
		sSHG_PanchayatNames_Label = new Vector<String>();
		sSHG_VillageNames_Label = new Vector<String>();
		sSHG_TransactionDate_Label = new Vector<String>();

		_Audit_VerifiedValue = new Vector<String>();
		_VerifiedLastAuditDate = new Vector<String>();

		_Audit_VerifiedValue.clear();
		_VerifiedLastAuditDate.clear();

		mTitleValues = null;
		mFirstAddTitle = false;

	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub

		try {

			SoapObject request = new SoapObject(LoginTask.NAMESPACE,
					SOAP_OperationConstants.OPERATION_NAME_GET_LOGIN_FINAL);

			Log.d(mTag, String.valueOf(LoginActivity.sUName) + "   " + String.valueOf(LoginActivity.sPwd));

			PackageManager manager = EShaktiApplication.getAppContext().getPackageManager();
			PackageInfo info;
			try {
				info = manager.getPackageInfo(EShaktiApplication.getAppContext().getPackageName(), 0);
				int mVersion = info.versionCode;
				String mVersionName = info.versionName;
				LoginActivity._VersionCode = String.valueOf(mVersion);
				LoginActivity._VersionName = String.valueOf(mVersionName);
			} catch (NameNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (EShaktiApplication.getIMEI_NO() != null && !EShaktiApplication.getIMEI_NO().equals("")) {
				request.addProperty("UserName", CryptographyUtils.Encrypt(String.valueOf(LoginActivity.sUName)));
				request.addProperty("Password", CryptographyUtils.Encrypt(String.valueOf(LoginActivity.sPwd)));
				request.addProperty("IMEI", CryptographyUtils.Encrypt(EShaktiApplication.getIMEI_NO()));
				request.addProperty("Version_Name", CryptographyUtils.Encrypt(LoginActivity._VersionName));
				request.addProperty("Version_Code", CryptographyUtils.Encrypt(LoginActivity._VersionCode));
				request.addProperty("Latitude", CryptographyUtils.Encrypt(LoginActivity.sLatitude));
				request.addProperty("Longitude", CryptographyUtils.Encrypt(LoginActivity.sLongitude));

			}
			Log.e(mTag, request.toString() + "");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.setOutputSoapObject(request);
			envelope.dotNet = true;

			HttpTransportSE httpSe = new HttpTransportSE(LoginTask.SOAP_ADDRESS);

			Log.e("Login Name Space--->>>>", LoginTask.NAMESPACE);

			try {
				httpSe.call(LoginTask.NAMESPACE + SOAP_OperationConstants.OPERATION_NAME_GET_LOGIN_FINAL, envelope);

				Object result = envelope.getResponse();
				sLoginTask_Response = CryptographyUtils.Decrypt(result.toString());
				Log.e("Current values---->>>>", sLoginTask_Response);

				if (!sLoginTask_Response.equals("LoginName or password incorrect.")) {

					EShaktiApplication.setGroupResponse(null);
					EShaktiApplication.setGroupResponse(sLoginTask_Response);

					String arr_Master[] = sLoginTask_Response.split("!");
					mMasterValues = sLoginTask_Response;

					if (!arr_Master[0].equals("No")) {

						String mLoginValues = arr_Master[0];
						mViewGroupValues = arr_Master[1];
						mAnimatorName = arr_Master[2];
						String mAnimatorValues = arr_Master[3];
						PrefUtils.setAnimatorName(mAnimatorName);
						Log.e("Animator Name", PrefUtils.getAnimatorName());
						PrefUtils.setAnimatorTargetValues(mAnimatorValues);

						publicValues.mGetAlertMessageValues = arr_Master[4];

						String arr[] = mLoginValues.split("#");

						UserName = arr[0];
						System.out.println("UserName :" + UserName);

						Ngo_Id = arr[1];
						System.out.println("Ngo_Id :" + Ngo_Id);

						Trainer_Id = arr[2];
						System.out.println("Trainer_Id :" + Trainer_Id);

						SelectedGroupsTask.sTypeID = String.valueOf(arr[2]);
						System.out.println("SelectedGroupsTask.sTypeID : " + SelectedGroupsTask.sTypeID);

						User_RegLanguage = arr[3];
						System.out.println("User_RegLanguage : " + User_RegLanguage);
						mUserType = arr[4];
						EShaktiApplication.setEshaktiContacts(null);
						EShaktiApplication.setAppVersionCode(null);
						EShaktiApplication.setmAppVersionName(null);

						LoginTask.UserName = UserName;
						LoginTask.Ngo_Id = Ngo_Id;
						LoginTask.Trainer_Id = Trainer_Id;
						LoginTask.User_RegLanguage = User_RegLanguage;
						LoginTask.mUserType = mUserType;
						if (mUserType != null) {

							if (mUserType.equals("ANIMATOR")) {

								EShaktiApplication.setEshaktiContacts(arr[5]);
								EShaktiApplication.setAppVersionCode(arr[6]);
								EShaktiApplication.setmAppVersionName(arr[7]);
								PrefUtils.setOfflineContacts(null);

								PrefUtils.setOfflineContacts(arr[5]);
								EShaktiApplication.setmUserType("AGENT");
								EShaktiApplication.isAgent = true;
								PrefUtils.setAgentUserName(LoginActivity.sUName);
								PrefUtils.setAppUsertype("ANIMATOR");

								String arr_viewGroups[] = mViewGroupValues.split("#");

								mGroupListMaster = arr_viewGroups[2].split("%");

								try {
									Log.e("mGroupListMaster.length", mGroupListMaster.length + "");
									for (int i = 0; i < mGroupListMaster.length; i++) {

										groupDetails = mGroupListMaster[i].split("[$]");

										groupList = groupDetails[0].split("~");
										shgcodeList = groupDetails[1].split("~");

										sAgent_GIDs.addElement(groupList[0].toString());

										sAgent_Gname.addElement(groupList[1].toString());
										sAgent_GActiveStatus.addElement(groupList[2].toString());

										sGroupTransId.addElement(groupList[3].toString());
										_Audit_VerifiedValue.addElement(groupList[4].toString());
										_VerifiedLastAuditDate.addElement(groupList[5].toString());

										if (!mFirstAddTitle) {
											mTitleValues = new String[5];
											mTitleValues[0] = shgcodeList[0].toString();
											mTitleValues[1] = shgcodeList[2].toString();
											mTitleValues[2] = shgcodeList[4].toString();
											mTitleValues[3] = shgcodeList[6].toString();
											mTitleValues[4] = shgcodeList[8].toString();
											mFirstAddTitle = true;
										}

										sSHG_Codes_Label.addElement(shgcodeList[0].toString());
										sSHG_BlockNames_Label.addElement(shgcodeList[2].toString());
										sSHG_PanchayatNames_Label.addElement(shgcodeList[4].toString());
										sSHG_VillageNames_Label.addElement(shgcodeList[6].toString());
										sSHG_TransactionDate_Label.addElement(shgcodeList[8].toString());

										sSHG_Codes.addElement(shgcodeList[1].toString());
										sSHG_BlockNames.addElement(shgcodeList[3].toString());
										sSHG_PanchayatNames.addElement(shgcodeList[5].toString());
										sSHG_VillageNames.addElement(shgcodeList[7].toString());
										sSHG_TransactionDate.addElement(shgcodeList[9].toString());

									}

									Log.e("Size ------>>>>", sAgent_Gname.size() + "");

								} catch (Exception e) {
									// TODO: handle exception
								}

							} else if (mUserType.equals("SHG")) {
								mGroupActiveStatus = arr[5];
								EShaktiApplication.setEshaktiContacts(arr[6]);
								EShaktiApplication.setAppVersionCode(arr[7]);
								EShaktiApplication.setmAppVersionName(arr[8]);

								PrefUtils.setOfflineContacts(null);

								PrefUtils.setOfflineContacts(arr[6]);

								EShaktiApplication.setmUserType("GROUP");
								EShaktiApplication.isAgent = false;
								PrefUtils.setGroupUserName(LoginActivity.sUName);
								PrefUtils.setAppUsertype("SHG");

								SelectedGroupsTask.sBankNames.clear();

								SelectedGroupsTask.sEngBankNames.clear();
								SelectedGroupsTask.sBankAmt.clear();
								SelectedGroupsTask.member_Id.clear();
								SelectedGroupsTask.member_Name.clear();
								SelectedGroupsTask.loan_Id.clear();
								SelectedGroupsTask.loan_Name.clear();
								SelectedGroupsTask.loan_EngName.clear();

								SelectedGroupsTask.getGroupDetails(mViewGroupValues);
							}
						}

					} else {
						Login_webserviceTask.sLoginTask_Response = "No";
					}

				}
				Constants.NETWORKCOMMONFLAG = "SUCCESS";

			} catch (ArrayIndexOutOfBoundsException e) {
				e.printStackTrace();
			} catch (ConnectTimeoutException e) {
				// TODO: handle exception
				e.printStackTrace();
				mListener.onTaskFinished(Constants.EXCEPTION);
				Log.e("TIME OUT ", "Success");
				Constants.NETWORKCOMMONFLAG = "FAIL";
			} catch (ConnectException e) {
				e.printStackTrace();
				Log.v("CONNECT EXCEPTION ", "Success");
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			} catch (XmlPullParserException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			}

		} catch (Exception e) {
			e.printStackTrace();
			Log.v("Yes!!!!!!!", "!!!!!!!!!");
		}

		return Constants.NETWORKCOMMONFLAG;
	}

	@Override
	protected void onPreExecute() {
		mListener.onTaskStarted();
	}

	@Override
	protected void onPostExecute(String result) {
		if (result == null) {
			result = Constants.EXCEPTION;

			mListener.onTaskFinished(result);
		} else {
			mListener.onTaskFinished(result);
		}
	}
}
