package com.yesteam.eshakti.webservices;

import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;
import com.yesteam.eshakti.utils.CryptographyUtils;
import com.yesteam.eshakti.utils.GetIndividualInfo;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.view.fragment.Meeting_AttendanceFragment;

import android.os.AsyncTask;
import android.util.Log;

public class Get_AttendanceTask extends AsyncTask<String, String, String> {

	private TaskListener mListener;

	String mTag = Get_AttendanceTask.class.getSimpleName();

	public static String get_Attendance_Response = null;

	String sendLoanInfo = "";
	String memberId = "";

	public Get_AttendanceTask(TaskListener listener) {
		// TODO Auto-generated constructor stub
		this.mListener = listener;
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub

		try {
			sendLoanInfo = GetIndividualInfo.getLoanInfo();
			memberId = GetIndividualInfo.getMemberID_Info();

			SoapObject request = new SoapObject(LoginTask.NAMESPACE,
					SOAP_OperationConstants.OPERATION_NAME_GET_ATTENDANCE);

			request.addProperty("User_Name", CryptographyUtils.Encrypt(String.valueOf(LoginTask.UserName)));
			request.addProperty("Ngo_id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Ngo_Id)));
			request.addProperty("Group_id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Group_Id)));
			request.addProperty("Values",
					CryptographyUtils.Encrypt(Meeting_AttendanceFragment.Send_to_server_values.toString()));
			request.addProperty("Loanid", CryptographyUtils.Encrypt(sendLoanInfo));
			request.addProperty("Trdate", CryptographyUtils.Encrypt(DatePickerDialog.sSend_To_Server_Date));
			request.addProperty("Latitude", CryptographyUtils.Encrypt(String.valueOf(LoginActivity.sLatitude)));
			request.addProperty("Longitude", CryptographyUtils.Encrypt(String.valueOf(LoginActivity.sLongitude)));
			Log.e("SERVICE REQUEST - Get_AttendanceTask:", request + "");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

			envelope.setOutputSoapObject(request);

			envelope.dotNet = true;

			try {

				HttpTransportSE http_se = new HttpTransportSE(LoginTask.SOAP_ADDRESS);

				http_se.call(LoginTask.NAMESPACE + SOAP_OperationConstants.OPERATION_NAME_GET_ATTENDANCE, envelope);

				Object result = envelope.getResponse();

				String getResponse = null;
				getResponse = CryptographyUtils.Decrypt(result.toString());
				String[] responseArray = getResponse.split("[#]");
				
				get_Attendance_Response = responseArray[0];
				SelectedGroupsTask.sLastTransactionDate_Response = responseArray[1];
				System.out.println("Service Response:" + get_Attendance_Response);

				Constants.NETWORKCOMMONFLAG = "SUCESS";

			} catch (IOException e) {
				e.printStackTrace();
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			} catch (XmlPullParserException e) {
				// TODO: handle exception
				e.printStackTrace();
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return Constants.NETWORKCOMMONFLAG;

	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		mListener.onTaskStarted();
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		if (result == null) {
			result = Constants.EXCEPTION;
			mListener.onTaskFinished(result);
		} else {
			mListener.onTaskFinished(result);
		}
	}
}
