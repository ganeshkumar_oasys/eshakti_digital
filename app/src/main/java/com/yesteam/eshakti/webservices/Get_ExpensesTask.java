package com.yesteam.eshakti.webservices;

import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;
import com.yesteam.eshakti.utils.CryptographyUtils;
import com.yesteam.eshakti.utils.GetIndividualInfo;
import com.yesteam.eshakti.utils.GetResponseInfo;
import com.yesteam.eshakti.view.fragment.Transaction_ExpensesFragment;

import android.os.AsyncTask;

public class Get_ExpensesTask extends AsyncTask<String, String, String> {

	private TaskListener mListener;
	public static String sExpenses_Response = null;
	public static String responseArr[];
	String sendLoanInfo = "";
	String memberId = "";
	String sExpensesAmounts[];

	public Get_ExpensesTask(TaskListener listener) {
		// TODO Auto-generated constructor stub
		mListener = listener;
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		try {
			sendLoanInfo = GetIndividualInfo.getLoanInfo();
			memberId = GetIndividualInfo.getMemberID_Info();
			SoapObject request = new SoapObject(LoginTask.NAMESPACE,
					SOAP_OperationConstants.OPERATION_NAME_GET_EXPENSES);
			if (Transaction_ExpensesFragment.sSendToServer_Expenses != null) {

				sExpensesAmounts = Transaction_ExpensesFragment.sSendToServer_Expenses.split("~");
			}

			request.addProperty("User_Name", CryptographyUtils.Encrypt(SelectedGroupsTask.UserName.toString()));
			request.addProperty("Ngo_id", CryptographyUtils.Encrypt(SelectedGroupsTask.Ngo_Id.toString()));
			request.addProperty("Group_id", CryptographyUtils.Encrypt(SelectedGroupsTask.Group_Id.toString()));
			request.addProperty("Member_id", CryptographyUtils.Encrypt(memberId));
			request.addProperty("Loanid", CryptographyUtils.Encrypt(sendLoanInfo));
			request.addProperty("travelling", CryptographyUtils.Encrypt("0"));
			request.addProperty("tea_expenses", CryptographyUtils.Encrypt("0"));
			request.addProperty("telephone", CryptographyUtils.Encrypt("0"));

			request.addProperty("stationary", CryptographyUtils.Encrypt(sExpensesAmounts[0]));
			request.addProperty("Federation_Expenses", CryptographyUtils.Encrypt(sExpensesAmounts[1]));
			request.addProperty("meeting", CryptographyUtils.Encrypt(sExpensesAmounts[2]));
			request.addProperty("others", CryptographyUtils.Encrypt(sExpensesAmounts[3]));
			request.addProperty("Trdate", CryptographyUtils.Encrypt(DatePickerDialog.sSend_To_Server_Date));

			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

			envelope.setOutputSoapObject(request);

			envelope.dotNet = true;

			try {

				HttpTransportSE http_se = new HttpTransportSE(LoginTask.SOAP_ADDRESS);

				http_se.call(LoginTask.NAMESPACE + SOAP_OperationConstants.OPERATION_NAME_GET_EXPENSES, envelope);

				Object result = envelope.getResponse();

				sExpenses_Response = CryptographyUtils.Decrypt(result.toString());

				System.out.println("Service Response:" + sExpenses_Response);

				GetResponseInfo.getResponseInfo(sExpenses_Response);

				Constants.NETWORKCOMMONFLAG = "SUCCESS";

			} catch (IOException e) {
				e.printStackTrace();
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			} catch (XmlPullParserException e) {
				// TODO: handle exception
				e.printStackTrace();
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			} catch (Exception e) {
				// TODO: handle exception
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return Constants.NETWORKCOMMONFLAG;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		mListener.onTaskStarted();
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		if (result == null) {
			result = Constants.EXCEPTION;
			mListener.onTaskFinished(result);
		} else {
			mListener.onTaskFinished(result);
		}

	}

}
