package com.yesteam.eshakti.webservices;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import com.yesteam.eshakti.appConstants.Constants;
import com.yesteam.eshakti.appConstants.SOAP_OperationConstants;
import com.yesteam.eshakti.interfaces.TaskListener;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;
import com.yesteam.eshakti.utils.CryptographyUtils;
import com.yesteam.eshakti.utils.GetIndividualInfo;
import com.yesteam.eshakti.utils.GetResponseInfo;
import com.yesteam.eshakti.view.fragment.Transaction_BankDepositFragment;
import com.yesteam.eshakti.view.fragment.Transaction_DepositEntryFragment;

import android.os.AsyncTask;
import android.util.Log;

public class Get_DepositEntryTask extends AsyncTask<String, String, String> {

	private TaskListener mListener;
	public static String sBalance_Details_Response = null;
	public static String responseArr[];
	String sendLoanInfo = "";
	String memberId = "";

	public Get_DepositEntryTask(TaskListener listener) {
		// TODO Auto-generated constructor stub
		mListener = listener;
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		try {
			sendLoanInfo = GetIndividualInfo.getLoanInfo();
			memberId = GetIndividualInfo.getMemberID_Info();

			System.out.println("DEPOSIT : " + String.valueOf(Transaction_DepositEntryFragment.sBank_deposit) + "   "
					+ String.valueOf(Transaction_DepositEntryFragment.sBank_Interest));

			SoapObject request = new SoapObject(LoginTask.NAMESPACE,
					SOAP_OperationConstants.OPERATION_NAME_GET_BALANCE_DETAILS);

			request.addProperty("User_Name", CryptographyUtils.Encrypt(String.valueOf(LoginTask.UserName)));
			request.addProperty("Ngo_id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Ngo_Id)));
			request.addProperty("Group_id", CryptographyUtils.Encrypt(String.valueOf(SelectedGroupsTask.Group_Id)));
			request.addProperty("Member_id", CryptographyUtils.Encrypt(memberId));
			request.addProperty("loans", CryptographyUtils.Encrypt(sendLoanInfo));
			request.addProperty("Bank_Name",
					CryptographyUtils.Encrypt(String.valueOf(Transaction_BankDepositFragment.sSendToServer_BankName)));
			request.addProperty("Deposit",
					CryptographyUtils.Encrypt(String.valueOf(Transaction_DepositEntryFragment.sBank_deposit)));
			request.addProperty("Bank_Interest",
					CryptographyUtils.Encrypt(String.valueOf(Transaction_DepositEntryFragment.sBank_Interest)));
			request.addProperty("withdrawal",
					CryptographyUtils.Encrypt(String.valueOf(Transaction_DepositEntryFragment.sBank_withdrawl)));
			request.addProperty("Bank_Charges",
					CryptographyUtils.Encrypt(String.valueOf(Transaction_DepositEntryFragment.sBank_expenses)));
			request.addProperty("Int_Subvention",
					CryptographyUtils.Encrypt(String.valueOf(Transaction_DepositEntryFragment.sBank_IntrSubvention)));
			request.addProperty("Trdate",
					CryptographyUtils.Encrypt(String.valueOf(DatePickerDialog.sSend_To_Server_Date)));

			Log.e("Get_DepositEntryTask", request.toString() + "");
			Log.e("Bank Subvention---------->>>", Transaction_DepositEntryFragment.sBank_IntrSubvention+"");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

			envelope.setOutputSoapObject(request);

			envelope.dotNet = true;

			try {

				HttpTransportSE http_se = new HttpTransportSE(LoginTask.SOAP_ADDRESS);

				http_se.call(LoginTask.NAMESPACE + SOAP_OperationConstants.OPERATION_NAME_GET_BALANCE_DETAILS,
						envelope);

				System.out.println("Envelope Test :" + envelope.getResponse());

				Object result = envelope.getResponse();

				sBalance_Details_Response = CryptographyUtils.Decrypt(result.toString());

				System.out.println("Service_Response :" + sBalance_Details_Response);

				GetResponseInfo.getResponseInfo(sBalance_Details_Response);

				Constants.NETWORKCOMMONFLAG = "SUCCESS";

			} catch (Exception e) {
				e.printStackTrace();
				mListener.onTaskFinished(Constants.EXCEPTION);
				Constants.NETWORKCOMMONFLAG = "FAIL";
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return Constants.NETWORKCOMMONFLAG;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		mListener.onTaskStarted();
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		if (result == null) {
			result = Constants.EXCEPTION;
			mListener.onTaskFinished(result);
		} else {
			mListener.onTaskFinished(result);
		}

	}

}
