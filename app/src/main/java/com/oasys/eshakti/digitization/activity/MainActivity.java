package com.oasys.eshakti.digitization.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.core.view.GravityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.R;

import java.util.List;
import java.util.Locale;

import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.yesteam.eshakti.materialdaterangepicker.DatePickerDialog;
import com.yesteam.eshakti.receiver.ConnectivityReceiver;
import com.yesteam.eshakti.view.fragment.FragmentDrawer;

@SuppressWarnings("deprecation")
public class MainActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener, OnClickListener,
		DatePickerDialog.OnDateSetListener, ConnectivityReceiver.ConnectivityReceiverListener, NewTaskListener {

	public static String calNavItem = "";

	private Toolbar mToolbar;
	private FragmentDrawer drawerFragment;
	// private Button mHomeButton;
	public static TextView mDateView;
	public static String mLanguageLocalae;

	ProgressDialog pd;
	int backStackEntryCount;
	ProgressDialog pd1;

	// Fragment fragment;
	List<Fragment> fragmentName;
	Locale locale;
	Dialog mProgressDialog;
	public static boolean mStepwiseBackKey = false;
	ImageView mLogout;
	int mCount = 0;
	public static boolean isQRCode_Back_Pressed = false;



	@SuppressLint("LongLogTag")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);


	}

	private void checkConnection() {

		final int version = Build.VERSION.SDK_INT;

	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		checkConnection();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	//	EShaktiApplication.getInstance().setConnectivityListener(MainActivity.this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.


		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onDrawerItemSelected(View view, int position) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {
		case R.id.toolbar_title:


			break;

		default:
			break;
		}

	}

	@SuppressLint("LongLogTag")
	@SuppressWarnings("unused")
	private void calendarDialogShow() {
		// TODO Auto-generated method stub

		if (mLanguageLocalae.equalsIgnoreCase("English")) {
			locale = new Locale("en");
		} else if (mLanguageLocalae.equalsIgnoreCase("Tamil")) {
			locale = new Locale("ta");
		} else if (mLanguageLocalae.equalsIgnoreCase("Hindi")) {
			locale = new Locale("hi");
		} else if (mLanguageLocalae.equalsIgnoreCase("Marathi")) {
			locale = new Locale("ma");
		} else if (mLanguageLocalae.equalsIgnoreCase("Malayalam")) {
			locale = new Locale("ml");
		} else if (mLanguageLocalae.equalsIgnoreCase("Kannada")) {
			locale = new Locale("kn");
		} else if (mLanguageLocalae.equalsIgnoreCase("Bengali")) {
			locale = new Locale("bn");
		} else if (mLanguageLocalae.equalsIgnoreCase("Gujarati")) {
			locale = new Locale("gu");
		} else if (mLanguageLocalae.equalsIgnoreCase("Punjabi")) {
			locale = new Locale("pa");
		} else if (mLanguageLocalae.equalsIgnoreCase("Assamese")) {
			locale = new Locale("as");
		} else {
			locale = new Locale("en");
		}


	}

	@Override
	public void onBackPressed() {

		if (FragmentDrawer.mDrawerLayout.isDrawerOpen(GravityCompat.START)) {

			FragmentDrawer.mDrawerLayout.closeDrawer(GravityCompat.START);

		} else {

		}
	}




	@Override
	public void onTaskStarted() {
		// TODO Auto-generated method stub

		try {

			mProgressDialog = AppDialogUtils.createProgressDialog(this);
			mProgressDialog.show();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	@Override
	public void onTaskFinished(final String result, final ServiceType serviceType) {
		// TODO Auto-generated method stub



	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub

		super.onPause();
	}

	@SuppressLint("LongLogTag")
	private void onSetCalendarValues() {
		// TODO Auto-generated method stub



	}

	@Override
	public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

	}

	@Override
	public void onNetworkConnectionChanged(boolean isConnected) {

	}
}
