package com.oasys.eshakti.digitization.fragment;

import android.app.Dialog;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.RegionalConversion;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.activity.LoginActivity;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;
import com.oasys.eshakti.digitization.database.SHGTable;


public class DeactivateAccount extends Fragment implements
        View.OnClickListener, NewTaskListener {

    private View rootView;
    private TextView mGroupName;
    private Button mButton;
    private Dialog mProgressDilaog;
    private NetworkConnection networkConnection;
    private ListOfShg shgDto;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_new_deactivate, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        try {
            shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
            networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
            mGroupName = (TextView) rootView.findViewById(R.id.groupname);
            mGroupName.setTypeface(LoginActivity.sTypeface);

            /*
             * mHeadertext = (TextView) rootView
             * .findViewById(R.id.fragment_changepassword_headertext);
             * mHeadertext.setText("DEACTIVATE ACCOUNT");
             * mHeadertext.setTypeface(LoginActivity.sTypeface);
             * mHeadertext.setTextColor(color.header_color);
             */
            mButton = (Button) rootView
                    .findViewById(R.id.fragment_deactivate_button);
            mButton.setText(RegionalConversion.getRegionalConversion(AppStrings.deactivateAccount));
            mButton.setTypeface(LoginActivity.sTypeface, Typeface.BOLD);
            mButton.setOnClickListener(this);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.fragment_deactivate_button:
            try {
                if (networkConnection.isNetworkAvailable()) {
                    onTaskStarted();
                    ListOfShg shg = new ListOfShg();
                    shg.setShgId(shgDto.getShgId());
                    String sreqString = new Gson().toJson(shg);
                    RestClient.getRestClient(this).callRestWebServiceForPutMethod(Constants.BASE_URL + Constants.SHG_DEACTIVATE+shgDto.getShgId(), sreqString, getActivity(), ServiceType.SHG_DEACTIVATE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {

        if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
            mProgressDilaog.dismiss();
            mProgressDilaog = null;
        }

        switch (serviceType) {
            case SHG_DEACTIVATE:
                try {
                    if (result != null && result.length() > 0) {
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();
                        ResponseDto mrDto = gson.fromJson(result, ResponseDto.class);
                        int statusCode = mrDto.getStatusCode();
                        Log.d("Main Frag response ", " " + statusCode);
                        if (statusCode == 400 || statusCode == 401 || statusCode == 403 || statusCode == 500 || statusCode == 503) {
                            // showMessage(statusCode);

                        } else if (statusCode == Utils.Success_Code) {
                            FragmentManager fm = getFragmentManager();
                            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                            MainFragment mainFragment = new MainFragment();
                            Bundle bundles = new Bundle();
                            bundles.putString("Setting",MainFragment.Flag_Setting);
                            mainFragment.setArguments(bundles);
                            NewDrawerScreen.showFragment(mainFragment);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }

    }
}
