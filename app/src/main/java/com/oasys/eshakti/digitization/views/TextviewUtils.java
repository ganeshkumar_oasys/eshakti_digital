package com.oasys.eshakti.digitization.views;


import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.view.animation.Animation;
import android.widget.TextView;

import com.oasys.eshakti.digitization.R;

public class TextviewUtils {

	@SuppressLint("WrongConstant")
	@SuppressWarnings("deprecation")
	public static void manageBlinkEffect(TextView textView, Activity activity) {
		ObjectAnimator anim = ObjectAnimator.ofInt(textView, "backgroundColor", Color.WHITE,
				activity.getResources().getColor(R.color.blink_yellow), Color.WHITE);
		anim.setDuration(1500);
		anim.setEvaluator(new ArgbEvaluator());
		anim.setRepeatMode(Animation.REVERSE);
		anim.setRepeatCount(Animation.INFINITE);

		anim.start();
	}

}
