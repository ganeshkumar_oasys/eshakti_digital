package com.oasys.eshakti.digitization.Dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class EnquiryDetails implements Serializable {

    String shgbatchId;
    String transation;
    String transaction_request_date;


}
