package com.oasys.eshakti.digitization.Dto;


import lombok.Data;

@Data
public class LoanSummary {
    private String id;
    private String BulkLoan,bulk_outstanding;
    private String CifLoan,cif_outstanding;
    private String CashCredit,CashCredit_outstanding;
    private String MfiLoan,Mfi_outstanding;
    private String TermLoan,term_outstanding;
}
