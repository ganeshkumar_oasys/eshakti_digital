package com.oasys.eshakti.digitization.Dto;


import com.google.gson.annotations.Expose;

import java.io.Serializable;

import lombok.Data;

@Data
public class ShgInternalLoanListDTO implements Serializable {

    @Expose
    private String amount;
    @Expose
    private String memberName;
    @Expose
    private String memeberId;
    @Expose
    private String loanId;


}
