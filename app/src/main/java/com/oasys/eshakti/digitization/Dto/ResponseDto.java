package com.oasys.eshakti.digitization.Dto;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.Data;

@Data
public class ResponseDto implements Serializable
{
    private String message;

    private int statusCode;

    private ResponseContent responseContent;

    private ArrayList<ResponseContents> responseContents;

    private String cookie;




}
