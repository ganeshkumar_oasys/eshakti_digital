package com.oasys.eshakti.digitization.fragment;


import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class Selectpayment extends Fragment implements View.OnClickListener {
    private View view;
    private TextView mUpipayment_text;
    private LinearLayout mUpipayment_method;
    private TextView mCashpayment_text;
    private LinearLayout mCashpayment_method;
    private ImageView mUpiimage;
    DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    private String currentDateStr;
    private TextView daily_date_time;

    public Selectpayment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_selectpayment, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {

        try {
            Calendar calender = Calendar.getInstance();
            currentDateStr = df.format(calender.getTime());
            mUpipayment_method = (LinearLayout) view.findViewById(R.id.mUpipayment_method);
            mUpipayment_method.setOnClickListener(this);
            mUpipayment_text = (TextView) view.findViewById(R.id.mUpipayment_text);
            daily_date_time = (TextView) view.findViewById(R.id.daily_date_time);
            daily_date_time.setText(currentDateStr);

            mCashpayment_method = (LinearLayout) view.findViewById(R.id.mCashpayment_method);
//            mCashpayment_method.setOnClickListener(this);
            mCashpayment_text = (TextView) view.findViewById(R.id.mCashpayment_text);

            mUpiimage = (ImageView) view.findViewById(R.id.mUpiimage);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mUpipayment_method:

                mUpipayment_method.setBackgroundResource(R.color.colorPrimary);
                mUpipayment_text.setTextColor(Color.WHITE);
                mCashpayment_method.setBackgroundResource(R.drawable.walletcash);
                mCashpayment_text.setTextColor(Color.BLACK);
                FragmentManager fm = getFragmentManager();
                //   fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                Rechargeamount rechargeamount = new Rechargeamount();
                NewDrawerScreen.showFragment(rechargeamount);
                break;

            case R.id.mCashpayment_method:
                mCashpayment_method.setBackgroundResource(R.color.colorPrimary);
                mCashpayment_text.setTextColor(Color.WHITE);
                mUpipayment_method.setBackgroundResource(R.drawable.walletcash);
                mUpipayment_text.setTextColor(Color.BLACK);
                FragmentManager fm1 = getFragmentManager();
                // fm1.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                Cashpayment cashpayment = new Cashpayment();
                NewDrawerScreen.showFragment(cashpayment);
                break;
        }
    }
}
