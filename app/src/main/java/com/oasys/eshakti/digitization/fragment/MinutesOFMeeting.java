package com.oasys.eshakti.digitization.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.Dto.MinutesOfMeeting;
import com.oasys.eshakti.digitization.Dto.RequestDto.MinutesId;
import com.oasys.eshakti.digitization.Dto.RequestDto.MinutesofmeetingsRequestDto;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;
import com.oasys.eshakti.digitization.database.SHGTable;
import com.tutorialsee.lib.TastyToast;
import com.yesteam.eshakti.view.activity.LoginActivity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


public class MinutesOFMeeting extends Fragment implements View.OnClickListener, NewTaskListener {
    View rootView;
    private ListOfShg shgDto;
    private TextView mGroupName;
    private TextView mCashinHand;
    private TextView mCashatBank;
    private TextView mHeadertext;
    private Button mRaised_Submit_Button;
    private Button mEdit_RaisedButton, mOk_RaisedButton;
    Button mPerviousButton, mNextButton;
    private NetworkConnection networkConnection;
    ResponseDto momdto;
    ArrayList<MinutesOfMeeting> momlist;
    int momsize;
    TableLayout minutesTable;
    private String tempvalues = "";
    private String tempId = "";
    Dialog confirmationDialog;
    String valuesArr[];
    String valuesID[];
    public static CheckedTextView sCheckBox[];
    int isGetBankLoan = 0;
    String isGetBankId = "";
    String shgId = "";
    private ArrayList<MinutesId> arrmomid;
    private String flag = "0";

    public MinutesOFMeeting() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        rootView = inflater.inflate(R.layout.fragment_minutes_of_meeting, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        if (networkConnection.isNetworkAvailable()) {
            RestClient.getRestClient(this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.GETMINUTES_OF_MEETINGS, getActivity(), ServiceType.GETMINUTES);
        }
        try {
            Bundle bundle = getArguments();
            if (bundle != null) {
                Attendance.flag = bundle.getString("stepwise");
                Log.d("velladhurai", Attendance.flag);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        mGroupName = (TextView) rootView.findViewById(R.id.groupname);
        mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());

        mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
        mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());

        mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
        mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());

        mHeadertext = (TextView) rootView.findViewById(R.id.mom_fragment_attendance_minutes_headertext);


        mPerviousButton = (Button) rootView.findViewById(R.id.mom_fragment_minutesPreviousbutton_default);
        mPerviousButton.setOnClickListener(this);

        mRaised_Submit_Button = (Button) rootView.findViewById(R.id.mom_fragment_attendance_minutes_Submitbutton);
        mRaised_Submit_Button.setOnClickListener(this);

        mNextButton = (Button) rootView.findViewById(R.id.mom_fragment_minutesNextbutton_default);
        mNextButton.setOnClickListener(this);



    }

    public void init() {
        try {
            momsize = momlist.size();
            sCheckBox = new CheckedTextView[momsize];
            minutesTable = (TableLayout) rootView.findViewById(R.id.mom_minutesTable);

            TableLayout.LayoutParams params = new TableLayout.LayoutParams(TableLayout.LayoutParams.FILL_PARENT,
                    TableLayout.LayoutParams.WRAP_CONTENT, 1f);
            params.setMargins(40, 20, 5, 5);

            for (int i = 0; i < momsize; i++) {
                TableRow indv_row = new TableRow(getActivity());

                indv_row.setLayoutParams(params);

                sCheckBox[i] = new CheckedTextView(getActivity());
//                sCheckBox[i].setBackgroundResource(R.drawable.btn_check_to_off_mtrl_013);
                sCheckBox[i].setText(momlist.get(i).getName().toString());
                sCheckBox[i].setTextSize(18);
                sCheckBox[i].setChecked(false);
                sCheckBox[i].setCheckMarkDrawable(android.R.drawable.checkbox_off_background);
                sCheckBox[i].setTypeface(LoginActivity.sTypeface);
                sCheckBox[i].setSingleLine(false);
                sCheckBox[i].setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
                sCheckBox[i].setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);


                final int finalI = i;

                sCheckBox[i].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        sCheckBox[finalI].setChecked(!sCheckBox[finalI].isChecked());
                        sCheckBox[finalI].setCheckMarkDrawable(sCheckBox[finalI].isChecked() ? android.R.drawable.checkbox_on_background : android.R.drawable.checkbox_off_background);

                    }
                });


                indv_row.addView(sCheckBox[i]);

                /*TableRow.LayoutParams textParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                textParams.setMargins(0, 0, 5, 15);
                TextView minutesOfMeeting = new TextView(getActivity());

                String mlist = momlist.get(i).getName().toString();
                if (mlist != null) {
                    minutesOfMeeting.setText(momlist.get(i).getName().toString());
                }                //minutesOfMeeting.setTypeface(LoginActivity.sTypeface);

                minutesOfMeeting.setTextColor(R.color.black);
                minutesOfMeeting.setSingleLine(false);
                minutesOfMeeting.setLayoutParams(textParams);
                indv_row.addView(minutesOfMeeting);*/

                minutesTable.addView(indv_row);
            }

        } catch (Exception e) {
            Log.e("momlist", e.toString());
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.mom_fragment_attendance_minutes_Submitbutton:
                try {
                    arrmomid = new ArrayList<>();
                    tempvalues = "";
                    for (int i = 0; i < momsize; i++) {
                        if (sCheckBox[i].isChecked()) {
                            tempvalues = tempvalues + momlist.get(i).getName().toString() + "~";
                            tempId = tempId + momlist.get(i).getId().toString() + ",";


                        }
                    }

                    valuesArr = tempvalues.split("~");
                    valuesID = tempId.split(",");

                    if (!tempvalues.equals("")) {
                        // Goto Confirmation page

                        confirmationDialog = new Dialog(getActivity());

                        LayoutInflater inflater = getActivity().getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.dialog_confirmation, null);
                        dialogView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT));

                        TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
                        confirmationHeader.setText("Confirmation");

                        TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

                        try {

                            System.out.println("valueArr size:" + valuesArr.length);
                            CheckBox checkBox[] = new CheckBox[valuesArr.length];

                            for (int j = 0; j < valuesArr.length; j++) {
                                TableRow indv_valueRow = new TableRow(getActivity());

                                TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                                contentParams.setMargins(10, 5, 10, 5);

                                checkBox[j] = new CheckBox(getActivity());
                                // Log.d(TAG, "Values >>>> :" + valuesArr[j]);
                                checkBox[j].setText(String.valueOf(valuesArr[j]));
                                checkBox[j].setChecked(true);
                                checkBox[j].setClickable(false);
                                checkBox[j].setTextColor(R.color.black);
                                checkBox[j].setTypeface(LoginActivity.sTypeface);
                                indv_valueRow.addView(checkBox[j], contentParams);
                                MinutesId mid = new MinutesId();
                                mid.setId(valuesID[j]);
                                arrmomid.add(mid);

                                if (valuesArr[j].equalsIgnoreCase("Decided to get the Bank Loan")) {

                                    isGetBankLoan = 1;
                                    isGetBankId = valuesID[j];
                                }

                                /*if (valuesArr[j].equals(mMinutesLangValues)) {
                                    isGetBankLoan = true;

                                }*/
                                confirmationTable.addView(indv_valueRow,
                                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                            }

                        } catch (Exception e) {
                            // TODO: handle exception
                            System.out.println("checkbox layout error:" + e.toString());
                        }

                        mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit_button);
                        mEdit_RaisedButton.setText("Edit");
                        mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
                        // 205,
                        // 0));
                        mEdit_RaisedButton.setOnClickListener(this);

                        mOk_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Ok_button);
                        mOk_RaisedButton.setText("OK");
                        mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
                        mOk_RaisedButton.setOnClickListener(this);

                        confirmationDialog.getWindow()
                                .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        confirmationDialog.setCanceledOnTouchOutside(false);
                        confirmationDialog.setContentView(dialogView);
                        confirmationDialog.setCancelable(true);
                        confirmationDialog.show();

                        ViewGroup.MarginLayoutParams margin = (ViewGroup.MarginLayoutParams) dialogView.getLayoutParams();
                        margin.leftMargin = 10;
                        margin.rightMargin = 10;
                        margin.topMargin = 10;
                        margin.bottomMargin = 10;
                        margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);
                    } else {

                        TastyToast.makeText(getActivity(), AppStrings.MinutesAlert, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);
                        // Send_to_server_values = Reset.reset(Send_to_server_values);
                        tempvalues = "";
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.fragment_Edit_button:
                mRaised_Submit_Button.setClickable(true);

                isGetBankLoan = 0;
                isGetBankId = "";

                tempvalues = "";
                // Send_to_server_values = Reset.reset(Send_to_server_values);
                confirmationDialog.dismiss();
                //isServiceCall = false;
                break;
            case R.id.fragment_Ok_button:
                shgId = shgDto.getShgId();
                Calendar calender = Calendar.getInstance();

                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String formattedDate = df.format(calender.getTime());

                DateFormat simple = new SimpleDateFormat("yyyy-MM-dd");
                Date d = new Date(Long.parseLong(shgDto.getLastTransactionDate()));
                String dateStr = simple.format(d);

                MinutesofmeetingsRequestDto momdto = new MinutesofmeetingsRequestDto();
                momdto.setShgId(shgId);
                momdto.setMobileDate(formattedDate);
                momdto.setTransactionDate(dateStr);
                momdto.setMinutesId(arrmomid);

                String sreqString = new Gson().toJson(momdto);
                if (networkConnection.isNetworkAvailable()) {
                    RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.SHGMEETINGSPOST, sreqString, getActivity(), ServiceType.SHGMEETINGSPOST);
                }


                break;
        }

    }

    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        switch (serviceType) {
            case GETMINUTES:
                try {
                    momdto = new Gson().fromJson(result, ResponseDto.class);
                    if (momdto.getStatusCode() == Utils.Success_Code) {
                        Log.d("momlist", momdto.getResponseContent().getMinutesOfMeeting().toString());
                        momlist = momdto.getResponseContent().getMinutesOfMeeting();
                        init();
                    }else {
                        if (momdto.getStatusCode() == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                        }
                        Utils.showToast(getActivity(), "Network Error");
                    }
                } catch (Exception e) {

                }
                break;
            case SHGMEETINGSPOST:
                try {
                    momdto = new Gson().fromJson(result, ResponseDto.class);

                    if (momdto.getStatusCode() == Utils.Success_Code) {

                        if (Attendance.flag == "1") {
                            if (isGetBankLoan == 1) {
                                confirmationDialog.dismiss();
                                String meetingId = momdto.getResponseContent().getMeetingId();
                                String message = momdto.getMessage();
                                GetBankloanFragment fragment = new GetBankloanFragment();
                                Bundle bundle = new Bundle();
                                bundle.putString("bankid", meetingId);
                                bundle.putString("stepwise", Attendance.flag);
                                fragment.setArguments(bundle);
                                NewDrawerScreen.showFragment(fragment);
                                Utils.showToast(getActivity(), message);
                                // MemberDrawerScreen.showFragment(new GetBankloanFragment());
                            } else {
                                StepwiseFinalReports stepwiseFinalReports = new StepwiseFinalReports();
                                confirmationDialog.dismiss();
                                String message = momdto.getMessage();
                                if (shgDto.getFFlag() == null || !shgDto.getFFlag().equals("1")) {
                                    SHGTable.updateTransactionSHGDetails(shgDto.getShgId());
                                }
                                FragmentManager fm = getFragmentManager();
                                fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                NewDrawerScreen.showFragment(stepwiseFinalReports);
                                Utils.showToast(getActivity(), message);
                            }
                        } else if (isGetBankLoan == 1) {
                            confirmationDialog.dismiss();
                            String meetingId = momdto.getResponseContent().getMeetingId();
                            String message = momdto.getMessage();
                            GetBankloanFragment fragment = new GetBankloanFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("bankid", meetingId);
                            fragment.setArguments(bundle);
                            NewDrawerScreen.showFragment(fragment);
                            Utils.showToast(getActivity(), message);
                            // MemberDrawerScreen.showFragment(new GetBankloanFragment());
                        } else {
                            confirmationDialog.dismiss();
                            String message = momdto.getMessage();
                            if (shgDto.getFFlag() == null || !shgDto.getFFlag().equals("1")) {
                                SHGTable.updateTransactionSHGDetails(shgDto.getShgId());
                            }
                            FragmentManager fm = getFragmentManager();
                            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                            MainFragment mainFragment = new MainFragment();
                            Bundle bundles = new Bundle();
                            bundles.putString("Meeting",MainFragment.Flag_Meeting);
                            mainFragment.setArguments(bundles);
                            NewDrawerScreen.showFragment(mainFragment);
                            Utils.showToast(getActivity(), message);
                        }
                    }else{
                        if (momdto.getStatusCode() == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                        }
                        Utils.showToast(getActivity(), "Network Error");
                    }
                } catch (Exception e) {

                }
                break;
        }
    }
}
