package com.oasys.eshakti.digitization.Dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class BranchList implements Serializable {

    private String branchId;
    private String branchName;
}
