package com.oasys.eshakti.digitization.Adapter;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasys.eshakti.digitization.Dto.EnquiryDetails;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.activity.LoginActivity;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;
import com.oasys.eshakti.digitization.fragment.Digitization_Member_Saving_Details;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class DigitizationReportAdapter extends RecyclerView.Adapter<DigitizationReportAdapter.MyViewholder> {

    private Context context;
    private ArrayList<EnquiryDetails> responseContentsArrayList;

    public DigitizationReportAdapter(Context context, ArrayList<EnquiryDetails> responseContentsArrayList) {
        this.context = context;
        this.responseContentsArrayList = responseContentsArrayList;
    }


    @NonNull
    @Override
    public MyViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.digital_tx_summary_ly, parent, false);
        return new MyViewholder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewholder holder, final int position) {

        holder.batchId.setText(responseContentsArrayList.get(position).getShgbatchId());
        holder.txType.setText(responseContentsArrayList.get(position).getTransation());


        DateFormat simple = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat time = new SimpleDateFormat("HH:mm:ss");
        Date d = new Date(Long.parseLong(responseContentsArrayList.get(position).getTransaction_request_date()));
        String dateStr = simple.format(d);
        String timeStr = time.format(d);
        holder.tx_req_date.setText(dateStr + " " +timeStr);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String batchId = responseContentsArrayList.get(position).getShgbatchId();
                String transtype = responseContentsArrayList.get(position).getTransation();
                String transdate = responseContentsArrayList.get(position).getTransaction_request_date();

                if (batchId != null && batchId.length() > 0) {
                    Digitization_Member_Saving_Details frag = new Digitization_Member_Saving_Details();
                    Bundle d = new Bundle();
                    d.putString("batchId", batchId);
                    d.putString("transactiontype", transtype);
                    d.putString("transactiondate", transdate);
                    frag.setArguments(d);
                    NewDrawerScreen.showFragment(frag);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return responseContentsArrayList.size();
    }



    class MyViewholder extends RecyclerView.ViewHolder {
         CardView cardView;
        TextView batchId, txType, tx_req_date;

        public MyViewholder(View itemView) {
            super(itemView);

            cardView= (CardView)itemView.findViewById(R.id.card_view);
            batchId = (TextView) itemView.findViewById(R.id.batchId);
            batchId.setTypeface(LoginActivity.sTypeface);

            txType = (TextView) itemView.findViewById(R.id.txType);
            txType.setTypeface(LoginActivity.sTypeface);

            tx_req_date = (TextView) itemView.findViewById(R.id.tx_req_date);
            tx_req_date.setTypeface(LoginActivity.sTypeface);
        }
    }
}
