package com.oasys.eshakti.digitization.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.oasys.eshakti.digitization.Adapter.CustomAdapter;
import com.oasys.eshakti.digitization.Dto.CashOfGroup;
import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.Dto.LoanDto;
import com.oasys.eshakti.digitization.Dto.MemberList;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.Dto.SavingRequest;
import com.oasys.eshakti.digitization.Dto.ShgBankDetails;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.GetSpanText;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.activity.LoginActivity;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;
import com.oasys.eshakti.digitization.database.BankTable;
import com.oasys.eshakti.digitization.database.SHGTable;
import com.oasys.eshakti.digitization.model.RowItem;
import com.oasys.eshakti.digitization.views.ButtonFlat;
import com.oasys.eshakti.digitization.views.CustomHorizontalScrollView;
import com.oasys.eshakti.digitization.views.Get_EdiText_Filter;
import com.oasys.eshakti.digitization.views.TextviewUtils;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Dell on 15 Dec, 2018.
 */

public class LD_IL_Entry extends Fragment implements View.OnClickListener, NewTaskListener {

    public static final String TAG = LD_IL_Entry.class.getSimpleName();

    public static List<EditText> sPL_Fields = new ArrayList<EditText>();
    public static List<EditText> sTenure_Fields = new ArrayList<EditText>();
    public static List<TextView> sPOL_Fields = new ArrayList<TextView>();
    public static String[] sPOLvalues;
    public static String sPL_Amounts[];
    public static String sTenurePeriod[];
    public static String sSelected_POL[], sSelected_POL_Id[];
    public static String sSendToServer_PLdisburse;
    public static int sPL_total;
    public static boolean isPL_Disburse_Submit;
    @SuppressWarnings("unused")
    private String[] mPol_IdValues, mRadio_Pol_id;
    private TextView mGroupName, mCashinHand, mCashatBank, mHeader, mLoanType;

    private Button mSubmit_Raised_Button, mEdit_RaisedButton, mOk_RaisedButton;
    private EditText mPL_values, mTenure;
    private String mOutstanding[];
    String response[], toBeEdit_PLdisburse[], mPOL_Response[], mPOL_Values[], mPOL_ID[];
    String memOuts = "";
    String nullAmount = "0";

    Dialog confirmationDialog;
    AlertDialog alertDialog;
    CustomAdapter custAdapter;

    private Dialog mProgressDilaog;
    private String mPldbrepayamount[];
    public static List<RowItem> sRowItems;

    int[] validatedOutstanding;
    boolean isError, isnullAmountError;
    String mLastTrDate = null, mLastTr_ID = null;
    String mRepayment;

    String mSelectedPOL_Values[], mPOLText_Values[];
    String mSelected_POL_Values = "", mPOL_Text = "";

    String loanType;
    RadioButton radioButton;
    int selectedId = 100, mPOL_Size;
    String mSelectedLoanType;
    String check;

    private TableLayout mLeftHeaderTable, mRightHeaderTable, mLeftContentTable, mRightContentTable;
    private CustomHorizontalScrollView mHSRightHeader, mHSRightContent;

    String width[] = {AppStrings.amount, AppStrings.OutsatndingAmt, AppStrings.purposeOfLoan, AppStrings.tenure};
    int[] rightHeaderWidth = new int[width.length];
    int[] rightContentWidth = new int[width.length];
    int mSavings_Values = 0;
    int InternalLoan = 0;
    public static int mTotalCollection = 0;
    public static int mTotalDisbursement = 0;
    boolean isGetTrid = false;
    private Button mPerviousButton, mNextButton;
    boolean isMeetingValues = false;
    boolean isPrevious = false;
    boolean isServiceCall = false;
    String mSqliteDBStoredValues_InternalLoanDisburseValues = null;
    LinearLayout mMemberNameLayout;
    TextView mMemberName;
    public static int groupLoanCount = 0;
    private boolean isGroupLoanOutstanding = false;
    String mGroupOS_Offlineresponse = null;

    private int mSize;
    private List<MemberList> memList;
    private ListOfShg shgDto;
    private NetworkConnection networkConnection;
    private ArrayList<LoanDto> arrLoanType;
    private LoanDto ldto = new LoanDto();
    private ArrayList<ShgBankDetails> bankdetails;
    private View rootView;
    private String flag = "0";
    String shgId = "";
    private ArrayList<CashOfGroup> csGrp;
    private int sum = 0;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_new_all_transaction, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // mSize = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, "")).size();
        //    memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgId = shgDto.getShgId();
        bankdetails = BankTable.getBankName(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        loanType = AppStrings.chooseLabel;

        Bundle bundle = getArguments();
        if (bundle != null) {
            Attendance.flag = bundle.getString("stepwise");
            Log.d("LOANID", Attendance.flag);
        }
        if (Attendance.flag == "1") {


            if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {            //  onTaskStarted();
                RestClient.getRestClient(this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.NAV_DETAILS + shgDto.getId(), getActivity(), ServiceType.NAV_DETAILS);

            }
        }


        OnCallInternalloanValues();
        stepWiseLoanType();


    }

    public void stepWiseLoanType() {
        try {
            if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {
                String url = Constants.BASE_URL + Constants.GETGROUPLOANTYPES + shgId;
                RestClient.getRestClient(LD_IL_Entry.this).callWebServiceForGetMethod(url, getActivity(), ServiceType.GETLOANTYPES_STEPWISES);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() {
        try {

            validatedOutstanding = new int[mSize];
            mOutstanding = new String[memList.size()];
            //TODO::  RestClient.getRestClient(this).callWebServiceForGetMethod(Constants.BASE_URL+Constants.BT_ENTRY,getActivity(),ServiceType.ANIMATOR_PROFILE); //TODO::

            mGroupName = (TextView) rootView.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
            mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashinHand.setTypeface(LoginActivity.sTypeface);

            mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
            mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashatBank.setTypeface(LoginActivity.sTypeface);
            /**
             *
             */
            mMemberNameLayout = (LinearLayout) rootView.findViewById(R.id.member_name_layout);
            mMemberName = (TextView) rootView.findViewById(R.id.member_name);
            //  mMemberName.setTypeface(LoginActivity.sTypeface);
            mHeader = (TextView) rootView.findViewById(R.id.fragmentHeader);

            mHeader.setTypeface(LoginActivity.sTypeface);

            if (Attendance.flag == "1") {
                mHeader.setText(AppStrings.InternalLoanDisbursement);
            } else {
                mHeader.setText(AppStrings.LoanDisbursement);
            }

            mHeader.setText(AppStrings.InternalLoanDisbursement);

//            if (EShaktiApplication.isStepWiseFragment()) {
//
//                mHeader.setText(
//                        "" + String.valueOf(AppStrings.mStepWise_LoanDibursement));
//            } else {
//
//                mHeader.setText(
//                        "" + String.valueOf(AppStrings.InternalLoanDisbursement));
//            }


            Log.d(TAG, String.valueOf(mSize));

            mLeftHeaderTable = (TableLayout) rootView.findViewById(R.id.LeftHeaderTable);
            mRightHeaderTable = (TableLayout) rootView.findViewById(R.id.RightHeaderTable);
            mLeftContentTable = (TableLayout) rootView.findViewById(R.id.LeftContentTable);
            mRightContentTable = (TableLayout) rootView.findViewById(R.id.RightContentTable);

            mHSRightHeader = (CustomHorizontalScrollView) rootView.findViewById(R.id.rightHeaderHScrollView);
            mHSRightContent = (CustomHorizontalScrollView) rootView.findViewById(R.id.rightContentHScrollView);

            mHSRightHeader.setOnScrollChangedListener(new CustomHorizontalScrollView.onScrollChangedListener() {

                public void onScrollChanged(int l, int t, int oldl, int oldt) {
                    // TODO Auto-generated method stub

                    mHSRightContent.scrollTo(l, 0);

                }
            });

            mHSRightContent.setOnScrollChangedListener(new CustomHorizontalScrollView.onScrollChangedListener() {
                @Override
                public void onScrollChanged(int l, int t, int oldl, int oldt) {
                    // TODO Auto-generated method stub
                    mHSRightHeader.scrollTo(l, 0);
                }
            });
            TableRow leftHeaderRow = new TableRow(getActivity());

            TableRow.LayoutParams lHeaderParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);

            TextView mMemberName_Header = new TextView(getActivity());
            mMemberName_Header.setText("" + String.valueOf(AppStrings.memberName));
            mMemberName_Header.setTypeface(LoginActivity.sTypeface);
            mMemberName_Header.setTextColor(Color.WHITE);
            mMemberName_Header.setPadding(10, 5, 10, 5);
            mMemberName_Header.setLayoutParams(lHeaderParams);
            leftHeaderRow.addView(mMemberName_Header);
            mLeftHeaderTable.addView(leftHeaderRow);

            TableRow rightHeaderRow = new TableRow(getActivity());
            TableRow.LayoutParams rHeaderParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            rHeaderParams.setMargins(10, 0, 10, 0);

            TextView mPL_Header = new TextView(getActivity());
            mPL_Header.setText("" + String.valueOf(AppStrings.amount));
            mPL_Header.setTypeface(LoginActivity.sTypeface);
            mPL_Header.setTextColor(Color.WHITE);
            mPL_Header.setPadding(10, 5, 10, 5);
            mPL_Header.setGravity(Gravity.CENTER);
            mPL_Header.setLayoutParams(rHeaderParams);
            mPL_Header.setBackgroundResource(R.color.tableHeader);
            rightHeaderRow.addView(mPL_Header);

            TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            contentParams.setMargins(10, 0, 10, 0);

            TextView mOutstanding_Header = new TextView(getActivity());
            mOutstanding_Header
                    .setText(String.valueOf(AppStrings.OutsatndingAmt));
            mOutstanding_Header.setTypeface(LoginActivity.sTypeface);
            mOutstanding_Header.setTextColor(Color.WHITE);
            mOutstanding_Header.setPadding(25, 5, 10, 5);
            mOutstanding_Header.setGravity(Gravity.RIGHT);
            mOutstanding_Header.setLayoutParams(contentParams);
            mOutstanding_Header.setBackgroundResource(R.color.tableHeader);
            rightHeaderRow.addView(mOutstanding_Header);

            TableRow.LayoutParams POLParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            POLParams.setMargins(10, 0, 10, 0);

            TextView mPurposeOfLoan_Header = new TextView(getActivity());
            mPurposeOfLoan_Header.setText(String.valueOf(AppStrings.purposeOfLoan));
            mPurposeOfLoan_Header.setTypeface(LoginActivity.sTypeface);
            mPurposeOfLoan_Header.setTextColor(Color.WHITE);
            mPurposeOfLoan_Header.setGravity(Gravity.CENTER);
            mPurposeOfLoan_Header.setLayoutParams(POLParams);
            mPurposeOfLoan_Header.setPadding(10, 5, 10, 5);
            mPurposeOfLoan_Header.setBackgroundResource(R.color.tableHeader);
            rightHeaderRow.addView(mPurposeOfLoan_Header);

            TableRow.LayoutParams tenureParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            tenureParams.setMargins(10, 0, 20, 0);

            TextView mTenure_Header = new TextView(getActivity());
            mTenure_Header.setText(String.valueOf(AppStrings.tenure));
            mTenure_Header.setTypeface(LoginActivity.sTypeface);
            mTenure_Header.setTextColor(Color.WHITE);
            mTenure_Header.setSingleLine(true);
            mTenure_Header.setGravity(Gravity.CENTER);
            mTenure_Header.setLayoutParams(tenureParams);// (rHeaderParams);
            mTenure_Header.setBackgroundResource(R.color.tableHeader);
            mTenure_Header.setPadding(10, 5, 10, 5);
            rightHeaderRow.addView(mTenure_Header);

            mRightHeaderTable.addView(rightHeaderRow);

            for (int i = 0; i < mSize; i++) {
                mOutstanding[i] = memList.get(i).getLoanOutstanding();//TODO on RESPONSE
                Log.v("I Values Is ", i + "");
            }

            try {

                for (int j = 0; j < mSize; j++) {

                    TableRow leftContentRow = new TableRow(getActivity());
                    TableRow.LayoutParams leftContentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                    leftContentParams.setMargins(5, 5, 5, 15);

                    final TextView memberName_Text = new TextView(getActivity());
                    memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
                            String.valueOf("" + memList.get(j).getMemberName())));
                    memberName_Text.setTextColor(R.color.black);
                    memberName_Text.setPadding(5, 5, 5, 5);
                    memberName_Text.setLayoutParams(leftContentParams);
                    leftContentRow.addView(memberName_Text);
                    mLeftContentTable.addView(leftContentRow);

                    TableRow rightContentRow = new TableRow(getActivity());
                    TableRow.LayoutParams rightContentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                    rightContentParams.setMargins(10, 5, 10, 5);

                    mPL_values = new EditText(getActivity());
                    mPL_values.setId(j);
                    sPL_Fields.add(mPL_values);
                    mPL_values.setInputType(InputType.TYPE_CLASS_NUMBER);
                    mPL_values.setPadding(5, 5, 5, 5);
                    mPL_values.setFilters(Get_EdiText_Filter.editText_filter());
                    mPL_values.setBackgroundResource(R.drawable.edittext_background);
                    mPL_values.setLayoutParams(rightContentParams);
                    mPL_values.setWidth(150);
                    mPL_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            // TODO Auto-generated method stub
                            if (hasFocus) {
                                ((EditText) v).setGravity(Gravity.LEFT);

                                mMemberNameLayout.setVisibility(View.VISIBLE);
                                mMemberName.setText(memberName_Text.getText().toString().trim());
                                TextviewUtils.manageBlinkEffect(mMemberName, getActivity());
                            } else {
                                ((EditText) v).setGravity(Gravity.RIGHT);
                                mMemberNameLayout.setVisibility(View.GONE);
                                mMemberName.setText("");
                            }
                        }
                    });
                    rightContentRow.addView(mPL_values);

                    TableRow.LayoutParams contentRow_Params = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                    contentRow_Params.setMargins(10, 5, 10, 5);

                    TextView outstanding = new TextView(getActivity());
                    outstanding.setText(GetSpanText.getSpanString(getActivity(), String.valueOf((int) (Double.parseDouble(mOutstanding[j])))));
                    outstanding.setTextColor(R.color.black);
                    outstanding.setGravity(Gravity.RIGHT);
                    outstanding.setLayoutParams(contentRow_Params);// (rightContentParams);
                    outstanding.setPadding(10, 0, 10, 5);
                    rightContentRow.addView(outstanding);

                    TableRow.LayoutParams contentRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                    contentRowParams.setMargins(10, 5, 10, 5);

                    mLoanType = new TextView(getActivity());
                    mLoanType.setText(loanType);

                    mLoanType.setId(j);
                    mLoanType.setLayoutParams(contentRowParams);// (rightContentParams);
                    mLoanType.setPadding(10, 0, 10, 5);
                    sPOL_Fields.add(mLoanType);
                    mLoanType.setOnClickListener(new View.OnClickListener() {

                        @SuppressWarnings("deprecation")
                        @Override
                        public void onClick(final View v) {
                            // TODO Auto-generated method stub

                            try {

                                mMemberNameLayout.setVisibility(View.VISIBLE);
                                mMemberName.setText(memberName_Text.getText().toString().trim());
                                TextviewUtils.manageBlinkEffect(mMemberName, getActivity());
                                final Dialog ChooseLoanType;
                                final View dialogView;
                                final RadioGroup radioGroup;
                                ChooseLoanType = new Dialog(getActivity());

                                LayoutInflater li = (LayoutInflater) getActivity()
                                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                dialogView = li.inflate(R.layout.dialog_new_loantype, null, false);

                                TextView confirmationHeader = (TextView) dialogView
                                        .findViewById(R.id.dialog_ChooseLabel);
                                confirmationHeader.setText(AppStrings.chooseLabel);
                                confirmationHeader.setTypeface(LoginActivity.sTypeface);
                                int radioColor = getResources().getColor(R.color.pink);
                                radioGroup = (RadioGroup) dialogView.findViewById(R.id.dialog_RadioGroup);
                                radioGroup.removeAllViews();

                                for (int j = 0; j < mPOL_Values.length; j++) {

                                    radioButton = new RadioButton(getActivity());
                                    radioButton.setText(mPOL_Values[j]);
                                    radioButton.setId(j);
                                    int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                                    if (currentapiVersion >= android.os.Build.VERSION_CODES.LOLLIPOP) {

                                        radioButton.setButtonTintList(ColorStateList.valueOf(radioColor));
                                    }

                                    radioGroup.addView(radioButton);

                                }

                                ButtonFlat okButton = (ButtonFlat) dialogView.findViewById(R.id.dialog_yes_button);
                                okButton.setText(AppStrings.dialogOk);
                                okButton.setTypeface(LoginActivity.sTypeface);
                                okButton.setOnClickListener(new View.OnClickListener() {

                                    @Override
                                    public void onClick(View view) {
                                        // TODO Auto-generated method
                                        // stub

                                        selectedId = radioGroup.getCheckedRadioButtonId();
                                        Log.e(TAG, String.valueOf(selectedId));
                                        if ((selectedId != 100) && (selectedId != (-1))) {

                                            // find the radiobutton by
                                            // returned id
                                            RadioButton radioLoanButton = (RadioButton) dialogView
                                                    .findViewById(selectedId);

                                            mSelectedLoanType = radioLoanButton.getText().toString();
                                            Log.v("On Selected LOAN TYPE", mSelectedLoanType);

                                            Log.v("view ID check : ", String.valueOf(v.getId()));

                                            TextView selectedTextView = (TextView) v.findViewById(v.getId());

                                            if (selectedId == 0) {
                                                selectedTextView.setText(AppStrings.chooseLabel);
                                            } else {
                                                selectedTextView.setText(mSelectedLoanType);
                                            }


                                            System.out.println("------idssssssssssss-----" + v.getId());

                                            ChooseLoanType.dismiss();

                                        } else {
                                            TastyToast.makeText(getActivity(), AppStrings.choosePOLAlert,
                                                    TastyToast.LENGTH_SHORT, TastyToast.WARNING);

                                        }

                                    }

                                });

                                ChooseLoanType.getWindow()
                                        .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                ChooseLoanType.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                ChooseLoanType.setCanceledOnTouchOutside(false);
                                ChooseLoanType.setContentView(dialogView);
                                ChooseLoanType.setCancelable(true);
                                ChooseLoanType.show();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    });
                    rightContentRow.addView(mLoanType);

                    TableRow.LayoutParams contentRowParams1 = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                    contentRowParams1.setMargins(10, 5, 10, 5);

                    mTenure = new EditText(getActivity());
                    mTenure.setId(j);
                    sTenure_Fields.add(mTenure);
                    mTenure.setInputType(InputType.TYPE_CLASS_NUMBER);
                    mTenure.setPadding(5, 5, 5, 5);
                    mTenure.setFilters(Get_EdiText_Filter.editText_tenure_filter());
                    mTenure.setBackgroundResource(R.drawable.edittext_background);
                    mTenure.setLayoutParams(contentRowParams1);// (rightContentParams);
                    mTenure.setWidth(150);
                    mTenure.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            // TODO Auto-generated method stub
                            if (hasFocus) {
                                ((EditText) v).setGravity(Gravity.LEFT);

                                mMemberNameLayout.setVisibility(View.VISIBLE);
                                mMemberName.setText(memberName_Text.getText().toString().trim());
                                TextviewUtils.manageBlinkEffect(mMemberName, getActivity());

                            } else {

                                mMemberNameLayout.setVisibility(View.GONE);
                                mMemberName.setText("");

                                ((EditText) v).setGravity(Gravity.RIGHT);
                            }

                        }
                    });
                    rightContentRow.addView(mTenure);

                    mRightContentTable.addView(rightContentRow);

                }

            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
                TastyToast.makeText(getActivity(), AppStrings.adminAlert, TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                getActivity().finish();
            }

            mSubmit_Raised_Button = (Button) rootView.findViewById(R.id.fragment_Submit_button);
            mSubmit_Raised_Button.setText("" + AppStrings.submit);
            mSubmit_Raised_Button.setTypeface(LoginActivity.sTypeface);
            mSubmit_Raised_Button.setOnClickListener(this);

            mPerviousButton = (Button) rootView.findViewById(R.id.fragment_Previousbutton);
            mPerviousButton.setText("" + AppStrings.mPervious);
            mPerviousButton.setTypeface(LoginActivity.sTypeface);
            mPerviousButton.setOnClickListener(this);

            mNextButton = (Button) rootView.findViewById(R.id.fragment_disbursementskipbutton);
            mNextButton.setText("" + AppStrings.mNext);
            mNextButton.setTypeface(LoginActivity.sTypeface);
            mNextButton.setOnClickListener(this);

//            if (EShaktiApplication.isDefault()) {
            mPerviousButton.setVisibility(View.INVISIBLE);
//                mNextButton.setVisibility(View.VISIBLE);
//            }
            if (Attendance.flag == "1") {
//                if (sum == 0) {
                mNextButton.setVisibility(View.VISIBLE);
//                }
            }
            resizeMemberNameWidth();
            resizeRightSideTable();
            resizeBodyTableRowHeight();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void OnCallInternalloanValues() {

        if (networkConnection.isNetworkAvailable()) {
            onTaskStarted();
            RestClient.getRestClient(LD_IL_Entry.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.LD_IL_OTList + shgDto.getShgId(), getActivity(), ServiceType.LD_IL_OTList);
            //RestClient.getRestClient(LD_IL_Entry.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.LD_IL_LOANTYPE, getActivity(), ServiceType.LD_IL_LOANTYPE);

        }
        sSendToServer_PLdisburse = "0";
        sPL_total = 0;
        sPL_Fields.clear();
        sTenure_Fields.clear();
        sPOL_Fields.clear();
        mTotalDisbursement = 0;
    }

    private void resizeRightSideTable() {
        // TODO Auto-generated method stub
        int rightHeaderCount = (((TableRow) mRightHeaderTable.getChildAt(0)).getChildCount());
        for (int i = 0; i < rightHeaderCount; i++) {
            rightHeaderWidth[i] = viewWidth(((TableRow) mRightHeaderTable.getChildAt(0)).getChildAt(i));
            rightContentWidth[i] = viewWidth(((TableRow) mRightContentTable.getChildAt(0)).getChildAt(i));
        }
        for (int i = 0; i < rightHeaderCount; i++) {
            if (rightHeaderWidth[i] < rightContentWidth[i]) {
                ((TableRow) mRightHeaderTable.getChildAt(0)).getChildAt(i)
                        .getLayoutParams().width = rightContentWidth[i];
            } else {
                ((TableRow) mRightContentTable.getChildAt(0)).getChildAt(i)
                        .getLayoutParams().width = rightHeaderWidth[i];
            }
        }
    }

    private void resizeMemberNameWidth() {
        // TODO Auto-generated method stub
        int leftHeadertWidth = viewWidth(mLeftHeaderTable);
        int leftContentWidth = viewWidth(mLeftContentTable);

        if (leftHeadertWidth < leftContentWidth) {
            mLeftHeaderTable.getLayoutParams().width = leftContentWidth;
        } else {
            mLeftContentTable.getLayoutParams().width = leftHeadertWidth;
        }
    }

    private void resizeBodyTableRowHeight() {

        int leftContentTable_ChildCount = mLeftContentTable.getChildCount();

        for (int x = 0; x < leftContentTable_ChildCount; x++) {

            TableRow leftContentTableRow = (TableRow) mLeftContentTable.getChildAt(x);
            TableRow rightContentTableRow = (TableRow) mRightContentTable.getChildAt(x);

            int rowLeftHeight = viewHeight(leftContentTableRow);
            int rowRightHeight = viewHeight(rightContentTableRow);

            TableRow tableRow = rowLeftHeight < rowRightHeight ? leftContentTableRow : rightContentTableRow;
            int finalHeight = rowLeftHeight > rowRightHeight ? rowLeftHeight : rowRightHeight;

            this.matchLayoutHeight(tableRow, finalHeight);
        }

    }

    private void matchLayoutHeight(TableRow tableRow, int height) {

        int tableRowChildCount = tableRow.getChildCount();

        // if a TableRow has only 1 child
        if (tableRow.getChildCount() == 1) {

            View view = tableRow.getChildAt(0);
            TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();
            params.height = height - (params.bottomMargin + params.topMargin);

            return;
        }

        // if a TableRow has more than 1 child
        for (int x = 0; x < tableRowChildCount; x++) {

            View view = tableRow.getChildAt(x);

            TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();

            if (!isTheHeighestLayout(tableRow, x)) {
                params.height = height - (params.bottomMargin + params.topMargin);
                return;
            }
        }

    }

    // check if the view has the highest height in a TableRow
    private boolean isTheHeighestLayout(TableRow tableRow, int layoutPosition) {

        int tableRowChildCount = tableRow.getChildCount();
        int heighestViewPosition = -1;
        int viewHeight = 0;

        for (int x = 0; x < tableRowChildCount; x++) {
            View view = tableRow.getChildAt(x);
            int height = this.viewHeight(view);

            if (viewHeight < height) {
                heighestViewPosition = x;
                viewHeight = height;
            }
        }

        return heighestViewPosition == layoutPosition;
    }

    // read a view's height
    private int viewHeight(View view) {
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        return view.getMeasuredHeight();
    }

    // read a view's width
    private int viewWidth(View view) {
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        return view.getMeasuredWidth();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.fragment_Submit_button:

                mMemberNameLayout.setVisibility(View.GONE);
                mMemberName.setText("");

                // To avoid Double click
                mSubmit_Raised_Button.setClickable(false);

                isPL_Disburse_Submit = true;
                sPL_total = 0;
                mTotalDisbursement = 0;

                try {

                    arrLoanType = new ArrayList<>();

                    mSelectedPOL_Values = new String[sPL_Fields.size()];

                    sPL_Amounts = new String[sPL_Fields.size()];
                    sTenurePeriod = new String[sTenure_Fields.size()];
                    sPOLvalues = new String[sPOL_Fields.size()];
                    mPol_IdValues = new String[sPOL_Fields.size()];
                    mSelected_POL_Values = "";
                    sSendToServer_PLdisburse = "";

                    sSelected_POL_Id = new String[mSize];

                    for (int i = 0; i < sPL_Amounts.length; i++) {
                        ldto = new LoanDto();

                        sPL_Amounts[i] = String.valueOf(sPL_Fields.get(i).getText());

                        if ((sPL_Amounts[i].equals("")) || (sPL_Amounts[i] == null)) {
                            sPL_Amounts[i] = nullAmount;
                        }

                        if (sPL_Amounts[i].matches("\\d*\\.?\\d+")) { // match a
                            // decimal
                            // number

                            int amount = (int) Math.round(Double.parseDouble(sPL_Amounts[i]));
                            sPL_Amounts[i] = String.valueOf(amount);
                        }

                        sTenurePeriod[i] = String.valueOf(sTenure_Fields.get(i).getText());
                        if ((sTenurePeriod[i].equals("")) || (sTenurePeriod[i] == null)) {
                            sTenurePeriod[i] = nullAmount;
                        }

                        sPOLvalues[i] = String.valueOf(sPOL_Fields.get(i).getText());
                        Log.v("sPOLvalues", sPOLvalues[i]);

                        if (sPOLvalues[i].equals(loanType)) {
                            sSelected_POL[i] = nullAmount;
                            sSelected_POL_Id[i] = "0";
                        } else if (!sPOLvalues[i].equals(loanType)) {
                            sSelected_POL[i] = sPOLvalues[i]; // sSelected_POL[i];

                            for (int j = 0; j < mPOL_Values.length; j++) {

                                if (mPOL_Values[j].equals(sPOLvalues[i])) {
                                    ldto.setPloanTypeId(sPOLvalues[i]);
                                    Log.e("mPOL_ID", mPOL_ID[j]);
                                    sSelected_POL_Id[i] = mPOL_ID[j];
                                    Log.e("Selected pol ", sSelected_POL_Id[i] + "");
                                }

                            }
                            Log.e("cccccccccccccccc", String.valueOf(sSelected_POL[i]));

                        }
                        Log.v("Selected pol id", sSelected_POL_Id[i] + "");

                        if (!sPL_Amounts[i].equals(nullAmount)) {
                            if ((sSelected_POL_Id[i].equals(nullAmount)) || (sTenurePeriod[i].equals(nullAmount))) {

                                isError = true;
                            }
                        }
                        if (sPL_Amounts[i].equals(nullAmount)) {
                            if ((!sSelected_POL_Id[i].equals(nullAmount)) || (!sTenurePeriod[i].equals(nullAmount))) {

                                isnullAmountError = true;
                            }
                        }


                        if (sSelected_POL_Id[i] != null) {
                            if (sSelected_POL_Id[i].equals(nullAmount)) {
                                mSelected_POL_Values = mSelected_POL_Values + AppStrings.dialogNo + ",";
                            } else if (!sSelected_POL_Id[i].equals(nullAmount)) {
                                mSelected_POL_Values = mSelected_POL_Values + sSelected_POL[i] + ",";
                            }

                            Log.v("kkkkkkkkkkkkkkk ", mSelected_POL_Values);

                            sPL_total = sPL_total + Integer.parseInt(sPL_Amounts[i]);

                            sSendToServer_PLdisburse = sSendToServer_PLdisburse
                                    + String.valueOf("" + memList.get(i).getMemberId()) + "~"
                                    + String.valueOf(sPL_Amounts[i]).trim() + "~" + String.valueOf(sSelected_POL_Id[i])// String.valueOf(sSelected_POL[i].trim())
                                    + "~" + String.valueOf(sTenurePeriod[i]) + "~";
                        }


                        ldto.setMemberId(memList.get(i).getMemberId());
                        ldto.setTenure(sTenurePeriod[i]);
                        ldto.setInternalLoanAmount(sPL_Amounts[i]);
                        //   ldto.setLoanTypeId();//TODO::
                        arrLoanType.add(ldto);


                    }

                    mSelectedPOL_Values = mSelected_POL_Values.split(",");

                    Log.d(TAG, "Total " + Integer.toString(sPL_total));

                    Log.d(TAG, "Vals" + sSendToServer_PLdisburse);

                    if (((sPL_total != 0) && (!Boolean.valueOf(isError)) && (!Boolean.valueOf(isnullAmountError)))) {
                        System.out.println("Do Navigate");

                        confirmationDialog = new Dialog(getActivity());

                        LayoutInflater inflater = getActivity().getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.dialog_new_confirmation, null);
                        dialogView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT));

                        TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
                        confirmationHeader.setText(AppStrings.confirmation);
                        confirmationHeader.setTypeface(LoginActivity.sTypeface);

                        TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

                        TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                        contentParams.setMargins(10, 5, 10, 5);

                        for (int i = 0; i < sPL_Amounts.length; i++) {

                            TableRow indv_SavingsRow = new TableRow(getActivity());

                            TextView memberName_Text = new TextView(getActivity());
                            memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
                                    String.valueOf("" + memList.get(i).getMemberName())));
                            memberName_Text.setTextColor(R.color.black);
                            memberName_Text.setPadding(5, 5, 5, 5);
                            memberName_Text.setLayoutParams(contentParams);
                            indv_SavingsRow.addView(memberName_Text);

                            TextView confirm_values = new TextView(getActivity());
                            confirm_values
                                    .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sPL_Amounts[i])));
                            confirm_values.setTextColor(R.color.black);
                            confirm_values.setPadding(5, 5, 5, 5);
                            confirm_values.setGravity(Gravity.RIGHT);
                            confirm_values.setLayoutParams(contentParams);
                            indv_SavingsRow.addView(confirm_values);

                            TextView POL_values = new TextView(getActivity());
                            POL_values.setText(
                                    GetSpanText.getSpanString(getActivity(), String.valueOf(mSelectedPOL_Values[i])));
                            POL_values.setTextColor(R.color.black);
                            POL_values.setPadding(5, 5, 5, 5);
                            POL_values.setGravity(Gravity.RIGHT);
                            POL_values.setLayoutParams(contentParams);
                            indv_SavingsRow.addView(POL_values);

                            TextView tenure_values = new TextView(getActivity());
                            tenure_values
                                    .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sTenurePeriod[i])));
                            tenure_values.setTextColor(R.color.black);
                            tenure_values.setPadding(5, 5, 5, 5);
                            tenure_values.setGravity(Gravity.RIGHT);
                            tenure_values.setLayoutParams(contentParams);
                            indv_SavingsRow.addView(tenure_values);

                            confirmationTable.addView(indv_SavingsRow,
                                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                        }
                        View rullerView = new View(getActivity());
                        rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
                        rullerView.setBackgroundColor(Color.rgb(0, 199, 140));// rgb(255,
                        // 229,
                        // 242));
                        confirmationTable.addView(rullerView);

                        TableRow totalRow = new TableRow(getActivity());

                        TextView totalText = new TextView(getActivity());
                        totalText.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.total)));
                        totalText.setTypeface(LoginActivity.sTypeface);
                        totalText.setTextColor(R.color.black);
                        totalText.setPadding(5, 5, 5, 5);// (5, 10, 5, 10);
                        totalText.setLayoutParams(contentParams);
                        totalRow.addView(totalText);

                        TextView totalAmount = new TextView(getActivity());
                        totalAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sPL_total)));
                        totalAmount.setTextColor(R.color.black);
                        totalAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
                        totalAmount.setGravity(Gravity.RIGHT);
                        totalAmount.setLayoutParams(contentParams);
                        totalRow.addView(totalAmount);

                        TextView emptyAmount = new TextView(getActivity());
                        emptyAmount.setText("");
                        emptyAmount.setTextColor(R.color.black);
                        emptyAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
                        emptyAmount.setGravity(Gravity.RIGHT);
                        emptyAmount.setLayoutParams(contentParams);
                        totalRow.addView(emptyAmount);

                        TextView total_Amount = new TextView(getActivity());
                        total_Amount.setText("");
                        total_Amount.setTextColor(R.color.black);
                        total_Amount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
                        total_Amount.setGravity(Gravity.RIGHT);
                        total_Amount.setLayoutParams(contentParams);
                        totalRow.addView(total_Amount);

                        confirmationTable.addView(totalRow,
                                new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                        mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit);
                        mEdit_RaisedButton.setText("" + AppStrings.edit);
                        mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
                        mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
                        // 205,
                        // 0));
                        mEdit_RaisedButton.setOnClickListener(this);

                        mOk_RaisedButton = (Button) dialogView.findViewById(R.id.frag_Ok);
                        mOk_RaisedButton.setText("" + AppStrings.yes);
                        mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
                        mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
                        mOk_RaisedButton.setOnClickListener(this);

                        mTotalDisbursement = sPL_total;
                        Log.e("PL Total Valuesssss", mTotalDisbursement + "");

                        confirmationDialog.getWindow()
                                .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        confirmationDialog.setCanceledOnTouchOutside(false);
                        confirmationDialog.setContentView(dialogView);
                        confirmationDialog.setCancelable(true);
                        confirmationDialog.show();

                        ViewGroup.MarginLayoutParams margin = (ViewGroup.MarginLayoutParams) dialogView.getLayoutParams();
                        margin.leftMargin = 10;
                        margin.rightMargin = 10;
                        margin.topMargin = 10;
                        margin.bottomMargin = 10;
                        margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);

                    } else if (Boolean.valueOf(isnullAmountError) || (sPL_total == Integer.parseInt(nullAmount))) {

                        isnullAmountError = false;
                        TastyToast.makeText(getActivity(), AppStrings.nullAlert, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);

                        sSendToServer_PLdisburse = "0";
                        sPL_total = Integer.parseInt(nullAmount);

                    } else if (Boolean.valueOf(isError)) {

                        isError = false;

                        TastyToast.makeText(getActivity(), AppStrings.Tenure_POL_Alert, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);
                        sSendToServer_PLdisburse = "0";
                        sPL_total = Integer.parseInt(nullAmount);

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                // }
                break;

            case R.id.frag_Ok:
                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();
                SavingRequest dto = new SavingRequest();
                dto.setShgId(shgDto.getShgId());
                dto.setModeOfCash("2");
                dto.setMobileDate(System.currentTimeMillis() + "");
                dto.setTransactionDate(shgDto.getLastTransactionDate());
                dto.setLoanDetails(arrLoanType);
                String sreqString = new Gson().toJson(dto);
                if (networkConnection.isNetworkAvailable()) {
                    onTaskStarted();
                    RestClient.getRestClient(LD_IL_Entry.this).callRestWebService(Constants.BASE_URL + Constants.LD_IL_ENTRY, sreqString, getActivity(), ServiceType.LD_IL_ENTRY);
                }
                break;

            case R.id.fragment_Edit:
                sSendToServer_PLdisburse = "0";
                sPL_total = 0;
                //  sPL_Fields.clear();
                // sTenure_Fields.clear();
                //   sPOL_Fields.clear();
                //   mTotalDisbursement = 0;
                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();
                break;
            case R.id.fragment_disbursementskipbutton:
                try {

                    Utils.loanTypes.remove(0);

                    if (Utils.loanTypes.size() > 0) {

                        Transaction_LoanType_Details transaction_loanType_details = new Transaction_LoanType_Details();
                        Bundle bundles = new Bundle();
                        bundles.putString("stepwise", Attendance.flag);
                        bundles.putString("loan_id", Utils.loanTypes.get(0).getLoanId());
                        bundles.putString("loan_type", Utils.loanTypes.get(0).getLoanTypeName());
                        bundles.putString("account_number", Utils.loanTypes.get(0).getAccountNumber());
                        bundles.putString("bank_name", Utils.loanTypes.get(0).getBankName());
                        transaction_loanType_details.setArguments(bundles);
                        NewDrawerScreen.showFragment(transaction_loanType_details);
                    } else {
                        MinutesOFMeeting minutesOFMeeting = new MinutesOFMeeting();
                        Bundle bundles = new Bundle();
                        bundles.putString("stepwise", Attendance.flag);
                        minutesOFMeeting.setArguments(bundles);
                        NewDrawerScreen.showFragment(minutesOFMeeting);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

        }

    }

    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();
    }


    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        if (mProgressDilaog != null) {
            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                mProgressDilaog.dismiss();
            }
        }

        switch (serviceType) {
            case LD_IL_LOANTYPE:
                try {
                    ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    String message = cdto.getMessage();
                    int statusCode = cdto.getStatusCode();
                    if (statusCode == Utils.Success_Code) {
                        Utils.showToast(getActivity(), message);

                        try {
                            mOutstanding = new String[memList.size()];
                            mPOLText_Values = new String[memList.size()];
                            mRepayment = "";
                            sSelected_POL = new String[memList.size()];
                            mRadio_Pol_id = new String[memList.size()];

                            /** PL Outstanding **/


                            System.out.println("PL DISBURSE OUTS : " + response);


                            Log.e("Size values Is ", mSize + "");

                            for (int i = 0; i < mSize; i++) {

                                mOutstanding[i] = memList.get(i).getLoanOutstanding();//TODO on RESPONSE
                                Log.v("I Values Is ", i + "");
                                // TypeCasting the double values into integer
                                  /*  validatedOutstanding[i] = Integer.parseInt(mOutstanding[i].substring(0, mOutstanding[i].indexOf(".")));
                                    System.out.println("mOutstanding " + validatedOutstanding[i] + " POS " + i);*/

                            }


                            mPOL_ID = new String[cdto.getResponseContent().getPurposeOfInternalLoanTypes().size() + 1];
                            mPOL_ID[0] = "0";

                            mPOL_Values = new String[cdto.getResponseContent().getPurposeOfInternalLoanTypes().size() + 1];
                            mPOL_Values[0] = String.valueOf(AppStrings.mNone);

                            for (int i = 0; i < cdto.getResponseContent().getPurposeOfInternalLoanTypes().size(); i++) {

                                mPOL_Values[i + 1] = cdto.getResponseContent().getPurposeOfInternalLoanTypes().get(i).getLoanTypeName();
                                mPOL_ID[i + 1] = cdto.getResponseContent().getPurposeOfInternalLoanTypes().get(i).getLoanTypeId();
                            }

                            for (int i = 0; i < mPOL_Values.length; i++) {
                                System.out.println("---------mPOL_Values--------" + mPOL_Values[i] + "    i   :" + i);
                                System.out.println("---------mPOL_ID--------" + mPOL_ID[i] + "    i   :" + i);
                            }

                            sRowItems = new ArrayList<RowItem>();

                            mPOL_Size = mPOL_Values.length;
                            Log.e("LENGTH of POL ", String.valueOf(mPOL_Size));

                            for (int i = 0; i < mPOL_Values.length; i++) {
                                // RowItem item = new RowItem(mPOL_Values[i]);
                                RowItem item = new RowItem(mPOL_Values[i]);

                                sRowItems.add(item);
                            }
                            custAdapter = new CustomAdapter(getActivity(), sRowItems);
                            init();

                            System.out.println(" Loan type =  " + loanType);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                          /*  LD_IL_Bank_MFI_Fed_Entry pl_DisbursementFragment = new LD_IL_Bank_MFI_Fed_Entry();
                            MemberDrawerScreen.showFragment(pl_DisbursementFragment);*/
                    } else {
                        if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                        }
                        Utils.showToast(getActivity(), message);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case LD_IL_ENTRY:
                try {
                    ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    String message = cdto.getMessage();
                    int statusCode = cdto.getStatusCode();

                    if (statusCode == Utils.Success_Code) {
                        Utils.showToast(getActivity(), message);

                        if (Attendance.flag == "1") {
                            if (Utils.loanTypes.size() > 0) {

                                if (confirmationDialog.isShowing()) {
                                    confirmationDialog.dismiss();
                                }
                                Transaction_LoanType_Details transaction_loanType_details = new Transaction_LoanType_Details();
                                Bundle bundles = new Bundle();
                                bundles.putString("stepwise", Attendance.flag);
                                bundles.putString("loan_id", Utils.loanTypes.get(0).getLoanId());
                                bundles.putString("loan_type", Utils.loanTypes.get(0).getLoanTypeName());
                                bundles.putString("account_number", Utils.loanTypes.get(0).getAccountNumber());
                                bundles.putString("bank_name", Utils.loanTypes.get(0).getBankName());
                                bundles.putString("outstanding", (Utils.loanTypes.get(0).getLoanOutstanding()));
                                transaction_loanType_details.setArguments(bundles);
                                NewDrawerScreen.showFragment(transaction_loanType_details);

                            }
                        } else if (shgDto.getFFlag() == null || !shgDto.getFFlag().equals("1")) {
                            SHGTable.updateTransactionSHGDetails(shgDto.getShgId());
                            FragmentManager fm = getFragmentManager();
                            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                            MainFragment mainFragment = new MainFragment();
                            Bundle bundles = new Bundle();
                            bundles.putString("Transaction", MainFragment.Flag_Transaction);
                            mainFragment.setArguments(bundles);
                            NewDrawerScreen.showFragment(mainFragment);

                        } else {
                            Transactionloantype_Complete frag = new Transactionloantype_Complete();
                            FragmentManager fm = getFragmentManager();
                            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                            NewDrawerScreen.showFragment(frag);
                        }


                    } else {
                        if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                        }
                        Utils.showToast(getActivity(), message);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case LD_IL_OTList:
                try {
                    ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    String message = cdto.getMessage();
                    int statusCode = cdto.getStatusCode();
                    if (statusCode == Utils.Success_Code) {
                        Utils.showToast(getActivity(), message);
                        memList = cdto.getResponseContent().getMemberloanRepaymentList();
                        mSize = memList.size();

                        if (networkConnection.isNetworkAvailable()) {
                            onTaskStarted();
                            RestClient.getRestClient(LD_IL_Entry.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.LD_IL_LOANTYPE, getActivity(), ServiceType.LD_IL_LOANTYPE);

                        }


                    } else {

                        if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                        }
                        Utils.showToast(getActivity(), message);

                    }
//                    if (flag == "1") {
//                        if (cdto != null) {
//                            for (int i = 0; i < cdto.getResponseContent().getMemberloanRepaymentList().size(); i++) {
//                                String s = cdto.getResponseContent().getMemberloanRepaymentList().get(i).getLoanOutstanding();
//                                int store = (int) Double.parseDouble(s);
//                                sum = sum + store;
//
//                            }
//
//                        }
//                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case GETLOANTYPES_STEPWISES:
                ResponseDto responseDto = new Gson().fromJson(result, ResponseDto.class);
                Log.d("GETLOANTYPE", responseDto.toString());
                if (responseDto.getStatusCode() == Utils.Success_Code) {
                    Utils.loanTypes = responseDto.getResponseContent().getLoansList();
                }else{
                    if (responseDto.getStatusCode()  == 401) {

                        Log.e("Group Logout", "Logout Sucessfully");
                        AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                    }
                }
                break;
            case NAV_DETAILS:
                try {
                    if (result != null && result.length() > 0) {
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();
                        ResponseDto mrDto = gson.fromJson(result, ResponseDto.class);
                        int statusCode = mrDto.getStatusCode();
                        Log.d("Main Frag response ", " " + statusCode);
                        if (statusCode == 400 || statusCode == 403 || statusCode == 500 || statusCode == 503) {
                            // showMessage(statusCode);

                        } else if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                        } else if (statusCode == Utils.Success_Code) {

                            csGrp = mrDto.getResponseContent().getCashOfGroup();
                            SHGTable.updateSHGDetails(csGrp.get(0), shgDto.getId());
                            shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }


    }
}
