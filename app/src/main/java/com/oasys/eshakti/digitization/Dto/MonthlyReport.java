package com.oasys.eshakti.digitization.Dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class MonthlyReport implements Serializable {

    private String internal_Loan_Repay;

    private String internal_Loan_Disbursment;

    private String mfi_Loan_Disbursment;

    private String subscription;

    private String term_Loan_Repay;

    private String term_Loan_Disbursment;

   // private String name;

    private String savings;

    private String other_income;

    private String penalty;

    private String mfi_Loan_Repay;

    private String id, Name, Amount;


}
