package com.oasys.eshakti.digitization.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.digitization.Adapter.CustomListAdapter;
import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.Dto.LoanDisbursementDto;
import com.oasys.eshakti.digitization.Dto.LoanDto;
import com.oasys.eshakti.digitization.Dto.MemberList;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.Dto.ShgBankDetails;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.activity.LoginActivity;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;
import com.oasys.eshakti.digitization.database.BankTable;
import com.oasys.eshakti.digitization.database.LoanDisbursementTable;
import com.oasys.eshakti.digitization.database.MemberTable;
import com.oasys.eshakti.digitization.database.SHGTable;
import com.oasys.eshakti.digitization.model.ListItem;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;

import java.util.ArrayList;
import java.util.List;

public class LoanDisbursement extends Fragment implements AdapterView.OnItemClickListener, NewTaskListener {

    private View rootView;
    public static String TAG = LoanDisbursement.class.getSimpleName();

    private TextView mGroupName, mCashinHand, mCashatBank;
    public static String sSelectedIncomeMenu = null;
    String[] mLoanMenu;

    private ListView mListView;
    private List<ListItem> listItems;
    private CustomListAdapter mAdapter;
    int listImage;
    private TextView mHeader;
    private int mSize;
    private List<MemberList> memList;
    private ListOfShg shgDto;
    private NetworkConnection networkConnection;
    private ArrayList<MemberList> arrMem;
    private ArrayList<ShgBankDetails> bankdetails;
    private Dialog mProgressDilaog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_new_menulist, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mSize = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, "")).size();
        memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        bankdetails = BankTable.getBankName(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        mLoanMenu = new String[]{AppStrings.mNewLoan, AppStrings.mExistingLoan};
        init();
    }

    private void init() {

        if (networkConnection.isNetworkAvailable()) {
            onTaskStarted();
            RestClient.getRestClient(this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.LD_BL_POL, getActivity(), ServiceType.LD_BL_POL);
            RestClient.getRestClient(this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.LD_BK_LTYPE, getActivity(), ServiceType.LD_BK_LTYPE);
            RestClient.getRestClient(this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.LD_INST_TYPE, getActivity(), ServiceType.LD_INST_TYPE);
        }

        try {
            mGroupName = (TextView) rootView.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName()+" / "+shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);
            mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
            mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashinHand.setTypeface(LoginActivity.sTypeface);
            mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
            mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashatBank.setTypeface(LoginActivity.sTypeface);
            mHeader = (TextView) rootView.findViewById(R.id.submenuHeaderTextview);
            mHeader.setTypeface(LoginActivity.sTypeface);
            mHeader.setVisibility(View.VISIBLE);
            mHeader.setText(AppStrings.LoanDisbursement);

            listItems = new ArrayList<ListItem>();
            mListView = (ListView) rootView.findViewById(R.id.fragment_List);
            listImage = R.drawable.ic_navigate_next_white_24dp;

            for (int i = 0; i < mLoanMenu.length; i++) {

                ListItem rowItem = new ListItem();
                rowItem.setTitle(mLoanMenu[i].toString());
                rowItem.setImageId(listImage);
                listItems.add(rowItem);
            }

            mAdapter = new CustomListAdapter(getActivity(), listItems);
            mListView.setAdapter(mAdapter);
            mListView.setOnItemClickListener(this);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        TextView textColor_Change = (TextView) view.findViewById(R.id.dynamicText);
        textColor_Change.setText(String.valueOf(mLoanMenu[position]));
        textColor_Change.setTextColor(Color.rgb(251, 161, 108));
        sSelectedIncomeMenu = String.valueOf(mLoanMenu[position]);
        if (position == 0) {
            LD_InternalLoan internalBankNewLoanFragment = new LD_InternalLoan();
            NewDrawerScreen.showFragment(internalBankNewLoanFragment);
        } else if (position == 1) {
            LD_ExternalLoan internalBank_Loans_Fragment = new LD_ExternalLoan();
            NewDrawerScreen.showFragment(internalBank_Loans_Fragment);
        }

    }

    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        if (mProgressDilaog != null) {
            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                mProgressDilaog.dismiss();
                mProgressDilaog = null;
            }
        }

        switch (serviceType) {

            case LD_BL_POL:
                try {
                    ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    String message = cdto.getMessage();
                    int statusCode = cdto.getStatusCode();
                    if (statusCode == Utils.Success_Code) {
                        Utils.showToast(getActivity(), message);
                        for (int i = 0; i < cdto.getResponseContent().getPurposeOfloanTypeList().size(); i++) {
                            LoanDto loanDto = cdto.getResponseContent().getPurposeOfloanTypeList().get(i);
                            LoanDisbursementTable.insertLD_POL(loanDto);
                        }

                    } else {
                        Utils.showToast(getActivity(), message);

                        if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case LD_INST_TYPE:
                try {
                    ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    String message = cdto.getMessage();
                    int statusCode = cdto.getStatusCode();
                    if (statusCode == Utils.Success_Code) {
                        //  installmentArr
                        Utils.showToast(getActivity(), message);
                        for (int i = 0; i < cdto.getResponseContent().getInstallmentTypeList().size(); i++) {
                            LoanDisbursementDto loanDto = cdto.getResponseContent().getInstallmentTypeList().get(i);
                            LoanDisbursementTable.insertLD_INS_TYPE(loanDto);
                        }

                    } else {
                        Utils.showToast(getActivity(), message);

                        if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case LD_BK_LTYPE:
                try {
                    ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    String message = cdto.getMessage();
                    int statusCode = cdto.getStatusCode();
                    if (statusCode == Utils.Success_Code) {
                        Utils.showToast(getActivity(), message);
                        for (int i = 0; i < cdto.getResponseContent().getLoanSettingList().size(); i++) {
                            LoanDisbursementDto loanDto = cdto.getResponseContent().getLoanSettingList().get(i);
                            LoanDisbursementTable.insertLD_LTYPE_SETTING(loanDto);
                        }
                    } else {
                        Utils.showToast(getActivity(), message);

                        if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;




        }

    }
}
