package com.oasys.eshakti.digitization.database;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.oasys.eshakti.digitization.Dto.ExpensesTypeDtoList;
import com.oasys.eshakti.digitization.EShaktiApplication;

import java.util.ArrayList;
import java.util.List;

public class ExpenseTable {
    private static SQLiteDatabase database;
    private static DbHelper dataHelper;
    static Cursor cursor;

    public ExpenseTable(Activity activity) {
        dataHelper = new DbHelper(activity);
    }

    public static void openDatabase() {
        dataHelper = new DbHelper(EShaktiApplication.getInstance());
        dataHelper.onOpen(database);
        database = dataHelper.getWritableDatabase();
    }

    public static void closeDatabase() {
        if (database != null && database.isOpen()) {
            database.close();
        }
    }

    public static void insertExpenseData(ExpensesTypeDtoList memDto) {

        if (memDto != null) {
            try {
                openDatabase();
                ContentValues values = new ContentValues();
                values.put(TableConstants.EXP_TYPE_ID, (memDto.getExpensesTypeId() != null && memDto.getExpensesTypeId().length() > 0) ? memDto.getExpensesTypeId() : "");
                values.put(TableConstants.EXP_TYPE_NAME, (memDto.getExpensesTypeName() != null && memDto.getExpensesTypeName().length() > 0) ? memDto.getExpensesTypeName() : "");
                database.insertWithOnConflict(TableName.TABLE_EXPENSE_DETAILS, TableConstants.EXP_TYPE_ID, values, SQLiteDatabase.CONFLICT_REPLACE);
            } catch (Exception e) {
                Log.e("insertEnergyException", e.toString());
            } finally {
                closeDatabase();
            }
        }

    }

    public static List<ExpensesTypeDtoList> getExpList() {
        List<ExpensesTypeDtoList> expList = new ArrayList<>();

        try {
            openDatabase();


            String selectQuery = "SELECT * FROM " + TableName.TABLE_EXPENSE_DETAILS;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {

                do {
                    ExpensesTypeDtoList loanDto = new ExpensesTypeDtoList();
                    loanDto.setExpensesTypeId(cursor.getString(cursor.getColumnIndex(TableConstants.EXP_TYPE_ID)));
                    loanDto.setExpensesTypeName(cursor.getString(cursor.getColumnIndex(TableConstants.EXP_TYPE_NAME)));

                    //if (loanDto.getLoanTypeId() != null && loanDto.getLoanTypeId().length() > 0)
                    expList.add(loanDto);
                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

        return expList;
    }


}
