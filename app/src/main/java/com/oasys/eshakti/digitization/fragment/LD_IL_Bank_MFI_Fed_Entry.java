package com.oasys.eshakti.digitization.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.oasys.eshakti.digitization.Adapter.CustomItemAdapter;
import com.oasys.eshakti.digitization.Dto.InternalLoanEntry;
import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.Dto.LoanBankDto;
import com.oasys.eshakti.digitization.Dto.LoanDisbursementDto;
import com.oasys.eshakti.digitization.Dto.LoanDto;
import com.oasys.eshakti.digitization.Dto.MemberList;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.Dto.ShgBankDetails;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.GetSpanText;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.activity.LoginActivity;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;
import com.oasys.eshakti.digitization.database.BankTable;
import com.oasys.eshakti.digitization.database.LoanDisbursementTable;
import com.oasys.eshakti.digitization.database.SHGTable;
import com.oasys.eshakti.digitization.model.InternalBankMFIModel;
import com.oasys.eshakti.digitization.model.RowItem;
import com.oasys.eshakti.digitization.views.MaterialSpinner;
import com.oasys.eshakti.digitization.views.RaisedButton;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.Service.NewTaskListener;

import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Dell on 15 Dec, 2018.sIncomeFields
 */

public class LD_IL_Bank_MFI_Fed_Entry extends Fragment implements NewTaskListener, View.OnClickListener {

    private TextView mGroupName, mCashInHand, mCashAtBank;
    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
    public static TextView mLoanSancDateTxt;
    public static TextView mLoanDistDateTxt;
    private RaisedButton mRaised_SubmitButton;
    private MaterialSpinner spn_label_bankName, spn_label_loanType, spn_label_installmentType, spn_label_POL,
            spn_label_Bankbranch, materialSpinner_Bank, mTenureSpinner;
    private EditText mLoan_Acc_NoEditText, mLoan_Sanc_AmountEditText, mLoan_Dist_AmountEditText, // mLoan_TenureEditText,
            mSubsidy_AmountEditText, mSubsidy_Reserve_fundEditText, mInterest_RateEditText, mNBFCEditText, mBranchName,
            mBankCharges;
    CustomItemAdapter bankNameAdapter, loanTypeAdapter, installmentTypeAdapter, POLAdapter, mBankBranchAdapter,
            bankAdapter, tenureAdapter;
    private Dialog mProgressDialog;
    private List<RowItem> bankNameItems, loanTypeItems, installmentTypeItems, POLItems, bankBranchItems, bankItems,
            tenureItems;
    private String[] bankNameArr;
    private String[] loanTypeArr;
    private LoanDisbursementDto[] installmentArr;
    private String[] POLArr;
    private String[] mBankBranchArr;
    private String mBankNameValue = "", mLoanTypeValue = "", mInstallmentTypeValue = "", mPOLValue = "",
            mNBFCValue = "", mBankBranchValue = "";
    private String mLoan_Acc_NoValue = "", mLoan_Sanc_AmountValue = "", mLoan_Dist_AmountValue = "",
            mLoan_TenureValue = "", mSubsidy_AmountValue = "", mSubsidy_Reserve_fundValue = "",
            mInterest_RateValue = "", mBankChargesValues = "";
    LoanDisbursementDto[] mLoanTypeArray;
    LoanDisbursementDto[] mBankNameArray;
    LoanBankDto[] mBankBranchArray;
    private String mLanguageLocale = "";
    Locale locale;
    public static String bankLoan_check = "";

    public static String loanSancDate = "", loanDistDate = "";
    String mLoanType, mLoanSource;
    private TextView mFedBankNameHeader, mFedBankName;
    private String[] mConfirm_BankLoan, mConfirmation_values;
    Dialog confirmationDialog;
    int mSize;
    private Button mEdit_RaisedButton, mOk_RaisedButton;
    RadioButton mCashRadio, mBankRadio;
    public static String selectedType, selectedItemBank;

    public static String mBankValue = null;
    LinearLayout mSpinnerLayout;
    ArrayList<String> mBanknames_Array = new ArrayList<String>();
    ArrayList<String> mBanknamesId_Array = new ArrayList<String>();
    ArrayList<String> mEngSendtoServerBank_Array = new ArrayList<String>();
    ArrayList<String> mEngSendtoServerBankId_Array = new ArrayList<String>();

    private String[] mTenureArr, mTenureArray;

    Date sanctionDate, openinigDate, currentDate, disbursementDate;

    private ListOfShg shgDto;
    private NetworkConnection networkConnection;
    private ArrayList<MemberList> arrMem;
    private ArrayList<ShgBankDetails> bankdetails;
    private View rootView;
    private Dialog mProgressDilaog;
    private List<LoanBankDto> internalBankList;
    private List<LoanDisbursementDto> internalInstallmenttype, internalLT, internalFED, internalMFI;
    private List<LoanDto> internalPOL;
    private List<LoanBankDto> internalBranchList = new ArrayList<>();
    private String[] branchNameArr;
    private String[] installmentNameArr;
    private String shgformationMillis, shgTrMillis;

    public static InternalLoanEntry loanEntry = new InternalLoanEntry();
    private String mBankNameIdValue;
    private String mLoanTypeIdValue;
    private String mInstallTypeId;
    private String mLoanId;
    private String mselectedPOLId;
    private String mSelectionBranchId;
    private ShgBankDetails sSelectedSavingbank;
    private LoanBankDto[] mBankNameArray1;
    private int loanType;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_new_bank_mfi_loan, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        bankdetails = BankTable.getBankName(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        internalBankList = LoanDisbursementTable.getBankList();
        internalInstallmenttype = LoanDisbursementTable.getInstallmentType();
        internalPOL = LoanDisbursementTable.getPOLType();
        internalLT = LoanDisbursementTable.getIL_LoanType();
        internalMFI = LoanDisbursementTable.getMFIType();
        internalFED = LoanDisbursementTable.getFedType();

        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        loanSancDate = "";
        loanDistDate = "";
        selectedType = "";
        mBankValue = "";

        if (LD_InternalLoan.sMenuSelection.equals("Bank Loan")) {
            mConfirm_BankLoan = new String[]{AppStrings.bankName,
                    AppStrings.loanType,
                    AppStrings.bankBranch,
                    AppStrings.Loan_Account_No,
                    AppStrings.Loan_Sanction_Date,
                    AppStrings.Loan_Sanction_Amount,
                    AppStrings.mBankCharges,
                    AppStrings.mLoanAccType,
                    AppStrings.Loan_Disbursement_Date,
                    AppStrings.Loan_Disbursement_Amount,
                    AppStrings.Loan_Tenure,
                    AppStrings.installment_type,
                    AppStrings.Subsidy_Amount,
                    AppStrings.Subsidy_Reserve_Fund,
                    AppStrings.purposeOfLoan,
                    AppStrings.Interest_Rate};
        } else if (LD_InternalLoan.sMenuSelection.equals("MFI Loan")) {
            mConfirm_BankLoan = new String[]{AppStrings.mfiname,
                    AppStrings.bankBranch,
                    AppStrings.Loan_Account_No,
                    AppStrings.Loan_Sanction_Date,
                    AppStrings.Loan_Sanction_Amount,
                    AppStrings.mBankCharges,
                    AppStrings.mLoanAccType,
                    AppStrings.Loan_Disbursement_Date,
                    AppStrings.Loan_Disbursement_Amount,
                    AppStrings.Loan_Tenure,
                    AppStrings.installment_type,
                    AppStrings.purposeOfLoan,
                    AppStrings.Interest_Rate};
        } else if (LD_InternalLoan.sMenuSelection.equals("Federation Loan")) {
            mConfirm_BankLoan = new String[]{
                    AppStrings.mFedBankName,
                    AppStrings.loanType,
                    AppStrings.Loan_Sanction_Date,
                    AppStrings.Loan_Sanction_Amount,
                    AppStrings.mBankCharges,
                    AppStrings.mLoanAccType,
                    AppStrings.Loan_Disbursement_Date,
                    AppStrings.Loan_Disbursement_Amount,
                    AppStrings.Loan_Tenure,
                    AppStrings.installment_type,
                    AppStrings.purposeOfLoan,
                    AppStrings.Interest_Rate
            };
        }

        init();
    }

    public void callMasterData(String id) {
        if (networkConnection.isNetworkAvailable()) {
            onTaskStarted();
            RestClient.getRestClient(this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.LD_BRANCH_DETAILS + shgDto.getDistrictId() + "&bankId=" + id, getActivity(), ServiceType.LD_BRANCH_DETAILS);
        }
    }

    private void init() {
        try {
//            callMasterData("0");  TODO::
            mGroupName = (TextView) rootView.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
            mCashInHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashInHand.setTypeface(LoginActivity.sTypeface);

            mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
            mCashAtBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashAtBank.setTypeface(LoginActivity.sTypeface);

            mRaised_SubmitButton = (RaisedButton) rootView.findViewById(R.id.fragment_monthyear_picker_Submitbutton);
            mRaised_SubmitButton.setText("" + AppStrings.next);
            mRaised_SubmitButton.setTypeface(LoginActivity.sTypeface);
            mRaised_SubmitButton.setOnClickListener(this);

            spn_label_bankName = (MaterialSpinner) rootView.findViewById(R.id.spinner_bankName);
            spn_label_loanType = (MaterialSpinner) rootView.findViewById(R.id.spinner_loanType);
            spn_label_installmentType = (MaterialSpinner) rootView.findViewById(R.id.spinner_installment_type);
            spn_label_POL = (MaterialSpinner) rootView.findViewById(R.id.spinner_purposeOfLoan);
            spn_label_Bankbranch = (MaterialSpinner) rootView.findViewById(R.id.spinner_bankBranch);

            spn_label_bankName.setBaseColor(R.color.grey_400);
            spn_label_loanType.setBaseColor(R.color.grey_400);
            spn_label_installmentType.setBaseColor(R.color.grey_400);
            spn_label_POL.setBaseColor(R.color.grey_400);
            spn_label_Bankbranch.setBaseColor(R.color.grey_400);


            if (LD_InternalLoan.sMenuSelection.equals("Bank Loan")) {
                spn_label_bankName.setFloatingLabelText(String.valueOf(AppStrings.bankName));
            } else if (LD_InternalLoan.sMenuSelection.equals("MFI Loan")) {
                spn_label_bankName.setFloatingLabelText(String.valueOf(AppStrings.mfiname));// (AppStrings.mfiname));
            }

            spn_label_loanType.setFloatingLabelText(String.valueOf(AppStrings.loanType));
            spn_label_installmentType.setFloatingLabelText(String.valueOf(AppStrings.installment_type));
            spn_label_POL.setFloatingLabelText(String.valueOf(AppStrings.purposeOfLoan));
            spn_label_Bankbranch.setFloatingLabelText(String.valueOf(AppStrings.bankBranch));

            spn_label_bankName.setPaddingSafe(10, 0, 10, 0);
            spn_label_loanType.setPaddingSafe(10, 0, 10, 0);
            spn_label_installmentType.setPaddingSafe(10, 0, 10, 0);
            spn_label_POL.setPaddingSafe(10, 0, 10, 0);
            spn_label_Bankbranch.setPaddingSafe(10, 0, 10, 0);

            mFedBankNameHeader = (TextView) rootView.findViewById(R.id.fedBankNameHeader);
            mFedBankNameHeader.setTypeface(LoginActivity.sTypeface);
            mFedBankName = (TextView) rootView.findViewById(R.id.fedBankName);
            mFedBankName.setTypeface(LoginActivity.sTypeface);

            if (LD_InternalLoan.sMenuSelection.equals("Federation Loan")) {

                mFedBankNameHeader.setText("" + AppStrings.mFedBankName);
                mFedBankNameHeader.setTypeface(LoginActivity.sTypeface);
                mFedBankNameHeader.setVisibility(View.VISIBLE);

                mFedBankName.setText((shgDto.getPanchayatName() != null && shgDto.getPanchayatName().length() > 0) ? shgDto.getPanchayatName() : "NA");
                mFedBankName.setTypeface(LoginActivity.sTypeface);
                mFedBankName.setVisibility(View.VISIBLE);

            }

            mLoan_Acc_NoEditText = (EditText) rootView.findViewById(R.id.editText_LoanAccNo);
            mLoan_Acc_NoEditText.setHint("" + AppStrings.Loan_Account_No);
            mLoan_Acc_NoEditText.setTypeface(LoginActivity.sTypeface);
            mLoan_Acc_NoEditText.setBackgroundResource(R.drawable.edittext_background);
            // mLoan_Acc_NoEditText.setInputType(InputType.TYPE_CLASS_PHONE);

            mLoan_Sanc_AmountEditText = (EditText) rootView.findViewById(R.id.editText_LoanSanctionAmount);
            mLoan_Sanc_AmountEditText
                    .setHint("" + AppStrings.Loan_Sanction_Amount);
            mLoan_Sanc_AmountEditText.setTypeface(LoginActivity.sTypeface);
            mLoan_Sanc_AmountEditText.setBackgroundResource(R.drawable.edittext_background);
            mLoan_Sanc_AmountEditText.setInputType(InputType.TYPE_CLASS_NUMBER);

            mBranchName = (EditText) rootView.findViewById(R.id.editText_BankBranch_Name);
            mBranchName.setHint("" + AppStrings.bankBranch);
            mBranchName.setTypeface(LoginActivity.sTypeface);
            mBranchName.setBackgroundResource(R.drawable.edittext_background);
            mBranchName.setInputType(InputType.TYPE_CLASS_TEXT);
            if (LD_InternalLoan.sMenuSelection.equals("Federation Loan")) {
                mBranchName.setText("" + "");
                mBranchName.setEnabled(false);
            }

            mLoan_Dist_AmountEditText = (EditText) rootView.findViewById(R.id.editText_loan_disbursement_amount);
            mLoan_Dist_AmountEditText
                    .setHint("" + AppStrings.Loan_Disbursement_Amount);
            mLoan_Dist_AmountEditText.setTypeface(LoginActivity.sTypeface);
            mLoan_Dist_AmountEditText.setBackgroundResource(R.drawable.edittext_background);
            mLoan_Dist_AmountEditText.setInputType(InputType.TYPE_CLASS_NUMBER);

            // Tenure spinner
            mTenureSpinner = (MaterialSpinner) rootView.findViewById(R.id.bank_mfi_tenure_spinner);
            mTenureSpinner.setFloatingLabelText(AppStrings.tenure);
            mTenureSpinner.setPaddingSafe(10, 0, 10, 0);

            mTenureArr = new String[]{"6", "10", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22",
                    "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38",
                    "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54",
                    "55", "56", "57", "58", "59", "60"};
            mTenureArray = new String[mTenureArr.length + 1];
            mTenureArray[0] = String.valueOf(AppStrings.tenure);
            for (int i = 0; i < mTenureArr.length; i++) {
                mTenureArray[i + 1] = mTenureArr[i].toString();
            }

            tenureItems = new ArrayList<RowItem>();
            for (int i = 0; i < mTenureArray.length; i++) {
                RowItem rowItem = new RowItem(mTenureArray[i]);
                tenureItems.add(rowItem);
            }

            tenureAdapter = new CustomItemAdapter(getActivity(), tenureItems);
            mTenureSpinner.setAdapter(tenureAdapter);
            mTenureSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    // TODO Auto-generated method stub
                    Log.d("TENURE POSITION", String.valueOf(position));

                    String selectedItem;
                    if (position == 0) {
                        selectedItem = mTenureArray[0];
                        mLoan_TenureValue = "0";
                    } else {
                        selectedItem = mTenureArray[position];
                        System.out.println("SELECTED TENURE:" + selectedItem);

                        mLoan_TenureValue = selectedItem;

                    }
                    Log.e("Tenure value", mLoan_TenureValue);

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    // TODO Auto-generated method stub

                }
            });

            mSubsidy_AmountEditText = (EditText) rootView.findViewById(R.id.editText_Subsidy_amount);
            mSubsidy_AmountEditText.setHint("" + AppStrings.Subsidy_Amount);
            mSubsidy_AmountEditText.setTypeface(LoginActivity.sTypeface);
            mSubsidy_AmountEditText.setBackgroundResource(R.drawable.edittext_background);
            mSubsidy_AmountEditText.setInputType(InputType.TYPE_CLASS_NUMBER);

            mSubsidy_Reserve_fundEditText = (EditText) rootView.findViewById(R.id.editText_Subsidy_Reserve_fund);
            mSubsidy_Reserve_fundEditText
                    .setHint("" + AppStrings.Subsidy_Reserve_Fund);
            mSubsidy_Reserve_fundEditText.setTypeface(LoginActivity.sTypeface);
            mSubsidy_Reserve_fundEditText.setBackgroundResource(R.drawable.edittext_background);
            mSubsidy_Reserve_fundEditText.setInputType(InputType.TYPE_CLASS_NUMBER);

            mInterest_RateEditText = (EditText) rootView.findViewById(R.id.editText_interest_rate);
            mInterest_RateEditText.setHint("" + AppStrings.Interest_Rate);
            mInterest_RateEditText.setTypeface(LoginActivity.sTypeface);
            mInterest_RateEditText.setBackgroundResource(R.drawable.edittext_background);
            // mInterest_RateEditText.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);

            mLoanSancDateTxt = (TextView) rootView.findViewById(R.id.loan_sanction_date);

            mLoanSancDateTxt.setHint(GetSpanText.getSpanString(getActivity(), AppStrings.Loan_Sanction_Date));
            mLoanSancDateTxt.setText("");
            mLoanSancDateTxt.setTypeface(LoginActivity.sTypeface);
            mLoanSancDateTxt.setOnClickListener(this);

            mLoanDistDateTxt = (TextView) rootView.findViewById(R.id.loan_disbursement_date);
            mLoanDistDateTxt.setHint(GetSpanText.getSpanString(getActivity(), AppStrings.Loan_Disbursement_Date));
            mLoanDistDateTxt.setText("");
            mLoanDistDateTxt.setTypeface(LoginActivity.sTypeface);
            mLoanDistDateTxt.setOnClickListener(this);

            mNBFCEditText = (EditText) rootView.findViewById(R.id.editText_NBFC_Name);
            mNBFCEditText.setHint("" + AppStrings.NBFC_Name);
            mNBFCEditText.setTypeface(LoginActivity.sTypeface);
            mNBFCEditText.setBackgroundResource(R.drawable.edittext_background);
            mNBFCEditText.setVisibility(View.GONE);

            mBankCharges = (EditText) rootView.findViewById(R.id.editText_LoanBankCharges);
            mBankCharges.setHint("" + AppStrings.mBankCharges);
            mBankCharges.setTypeface(LoginActivity.sTypeface);
            mBankCharges.setBackgroundResource(R.drawable.edittext_background);
            mBankCharges.setInputType(InputType.TYPE_CLASS_NUMBER);

            mCashRadio = (RadioButton) rootView.findViewById(R.id.radiobank_mfi_cash);
            mBankRadio = (RadioButton) rootView.findViewById(R.id.radiobank_mfi_bank);


            mSpinnerLayout = (LinearLayout) rootView.findViewById(R.id.bank_mfi_bankspinner_layout);
            mSpinnerLayout.setVisibility(View.GONE);

            RadioGroup radioGroup = (RadioGroup) rootView.findViewById(R.id.radiobank_mfi);
            radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    // checkedId is the RadioButton selected
                    switch (group.getCheckedRadioButtonId()) {
                        case R.id.radiobank_mfi_cash:
                            selectedType = "Cash";
                            mBankValue = "";
                            mBankRadio.setSelected(true);
                            mSpinnerLayout.setVisibility(View.GONE);
                            break;

                        case R.id.radiobank_mfi_bank:
                            selectedType = "Bank";
                            mSpinnerLayout.setVisibility(View.VISIBLE);

                            break;
                    }
                }
            });

            for (int i = 0; i < bankdetails.size(); i++) {
                mBanknames_Array.add(bankdetails.get(i).getBankName());
                mBanknamesId_Array.add(bankdetails.get(i).getBankId());
            }

            for (int i = 0; i < bankdetails.size(); i++) {
                mEngSendtoServerBank_Array.add(bankdetails.get(i).getBankName());
                mEngSendtoServerBankId_Array.add(bankdetails.get(i).getBankId());
            }

            materialSpinner_Bank = (MaterialSpinner) rootView.findViewById(R.id.bank_mfi_bankspinner);
            materialSpinner_Bank.setFloatingLabelText(AppStrings.bankName);
            materialSpinner_Bank.setPaddingSafe(10, 0, 10, 0);

            final String[] bankNames = new String[bankdetails.size() + 1];

            final String[] bankNames_BankID = new String[bankdetails.size() + 1];

            bankNames[0] = String.valueOf("" + AppStrings.bankName);
            for (int i = 0; i < bankdetails.size(); i++) {
                bankNames[i + 1] = bankdetails.get(i).getBankName().toString();
            }

            bankNames_BankID[0] = String.valueOf("" + AppStrings.bankName);
            for (int i = 0; i < bankdetails.size(); i++) {
                bankNames_BankID[i + 1] = bankdetails.get(i).getBankId().toString();
            }
            int size = bankNames.length;

            bankItems = new ArrayList<RowItem>();
            for (int i = 0; i < size; i++) {
                RowItem rowItem = new RowItem(bankNames[i]);// ""+sBankNames.elementAt(i).toString());
                bankItems.add(rowItem);
            }

            bankAdapter = new CustomItemAdapter(getActivity(), bankItems);
            materialSpinner_Bank.setAdapter(bankAdapter);

            materialSpinner_Bank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    // TODO Auto-generated method stub

                    if (position == 0) {
                        selectedItemBank = bankNames_BankID[0];
                        mBankValue = "0";

                    } else {
                        selectedItemBank = bankNames_BankID[position];
                        System.out.println("SELECTED BANK NAME : " + selectedItemBank);
                        mBankValue = selectedItemBank;

                        sSelectedSavingbank = bankdetails.get(position - 1);
                       /* String mBankname = null;
                        for (int i = 0; i < mBanknames_Array.size(); i++) {
                            if (selectedItemBank.equals(mBanknamesId_Array.get(i))) {
                                mBankname = mBanknamesId_Array.get(i);
                            }
                        }

                        mBankValue = mBankname;*/
                    }
                    Log.e("Selected Bank ", mBankValue);

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    // TODO Auto-generated method stub

                }
            });


            if (LD_InternalLoan.sMenuSelection.equals("Bank Loan")) {

                LoanBankDto[] stockArr = new LoanBankDto[internalBankList.size()];
                mBankNameArray1 = internalBankList.toArray(stockArr);

                bankNameArr = new String[mBankNameArray1.length + 1];
                bankNameArr[0] = String.valueOf("" + AppStrings.bankName);
                Log.e("Length Values", mBankNameArray1.length + "");
                Log.e("Next Length", mBankNameArray1.length + "");
                for (int i = 0; i < mBankNameArray1.length; i++) {
                    bankNameArr[i + 1] = internalBankList.get(i).getName().toString();
                    Log.i("Bank Name", i + "     " + bankNameArr[i + 1].toString());
                }

                bankNameItems = new ArrayList<RowItem>();
                for (int i = 0; i < bankNameArr.length; i++) {
                    RowItem rowItem = new RowItem(bankNameArr[i]);
                    bankNameItems.add(rowItem);
                }
                bankNameAdapter = new CustomItemAdapter(getActivity(), bankNameItems);
                spn_label_bankName.setAdapter(bankNameAdapter);

            } else if (LD_InternalLoan.sMenuSelection.equals("MFI Loan")) {

                LoanDisbursementDto[] stockArr = new LoanDisbursementDto[internalMFI.size()];
                mBankNameArray = internalMFI.toArray(stockArr);
                bankNameArr = new String[mBankNameArray.length + 1];
                bankNameArr[0] = String.valueOf("" + AppStrings.mfiname);
                for (int i = 0; i < mBankNameArray.length; i++) {
                    bankNameArr[i + 1] = mBankNameArray[i].getLoanSettingName().toString();

                }

                bankNameItems = new ArrayList<RowItem>();
                for (int i = 0; i < bankNameArr.length; i++) {
                    RowItem rowItem = new RowItem(bankNameArr[i]);
                    bankNameItems.add(rowItem);
                }
                bankNameAdapter = new CustomItemAdapter(getActivity(), bankNameItems);
                spn_label_bankName.setAdapter(bankNameAdapter);

            }
            spn_label_bankName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    // TODO Auto-generated method stub
                    Log.d("BankName POSITION", String.valueOf(position));

                    String selectedItem;
                    if (position == 0) {
                        selectedItem = bankNameArr[0];
                        mBankNameValue = "0";
                    } else {
                        selectedItem = bankNameArr[position];
                        mBankNameValue = selectedItem;

                        if (LD_InternalLoan.sMenuSelection.equals("Bank Loan")) {

                            System.out.println("SELECTED Bank Name :" + selectedItem);

                            mBankNameIdValue = internalBankList.get(position - 1).getId();

                        } else if (LD_InternalLoan.sMenuSelection.equals("MFI Loan")) {
                            loanType = internalMFI.get(position - 1).getSflag();
                            mLoanId = internalMFI.get(position - 1).getLoanSettingId();


                        }

                        if (LD_InternalLoan.sMenuSelection.equals("Bank Loan")) {
                         /*   EShaktiApplication.setBankName_InternalLoan(mBankNameValue);
                            new GetBankBranchListTask(Transaction_InternalBank_MFI_LoanFragment.this).execute();*///TODO

                            callMasterData(internalBankList.get(position - 1).getId());
                        }
                    }
                    Log.e("Bank Name value", mBankNameValue);

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    // TODO Auto-generated method stub

                }
            });


            LoanBankDto[] stockArr1 = new LoanBankDto[internalBranchList.size()];
            mBankBranchArray = internalBranchList.toArray(stockArr1);
            mBankBranchArr = new String[]{AppStrings.bankBranch};


            branchNameArr = new String[mBankBranchArray.length + 1];
            branchNameArr[0] = String.valueOf("" + AppStrings.bankBranch);

            for (int i = 0; i < mBankBranchArray.length; i++) {
                branchNameArr[i + 1] = internalBranchList.get(i).getName().toString();
                Log.i("Bank Name", i + "     " + branchNameArr[i + 1].toString());
            }

            bankBranchItems = new ArrayList<RowItem>();
            for (int i = 0; i < branchNameArr.length; i++) {
                RowItem rowItem = new RowItem(branchNameArr[i]);
                bankBranchItems.add(rowItem);
            }

            mBankBranchAdapter = new CustomItemAdapter(getActivity(), bankBranchItems);
            spn_label_Bankbranch.setAdapter(mBankBranchAdapter);


            if (LD_InternalLoan.sMenuSelection.equals("Bank Loan")) {

                LoanDisbursementDto[] stockArr = new LoanDisbursementDto[internalLT.size()];
                mLoanTypeArray = internalLT.toArray(stockArr);
                loanTypeArr = new String[mLoanTypeArray.length + 1];
                loanTypeArr[0] = String.valueOf("" + AppStrings.loanType);
                for (int i = 0; i < mLoanTypeArray.length; i++) {
                    loanTypeArr[i + 1] = mLoanTypeArray[i].getLoanSettingName().toString();
                    Log.i("Loan Type", loanTypeArr[i].toString());
                }

                loanTypeItems = new ArrayList<RowItem>();
                for (int i = 0; i < loanTypeArr.length; i++) {
                    RowItem rowItem = new RowItem(loanTypeArr[i]);
                    loanTypeItems.add(rowItem);
                }

            } else if (LD_InternalLoan.sMenuSelection.equals("MFI Loan")) {

            } else if (LD_InternalLoan.sMenuSelection.endsWith("Federation Loan")) {
                LoanDisbursementDto[] stockArr = new LoanDisbursementDto[internalFED.size()];
                mLoanTypeArray = internalFED.toArray(stockArr);
                loanTypeArr = new String[mLoanTypeArray.length + 1];
                loanTypeArr[0] = String.valueOf("" + AppStrings.loanType);
                for (int i = 0; i < mLoanTypeArray.length; i++) {
                    loanTypeArr[i + 1] = mLoanTypeArray[i].getLoanSettingName().toString();
                    Log.i("Loan Type", loanTypeArr[i].toString());
                }

                loanTypeItems = new ArrayList<RowItem>();
                for (int i = 0; i < loanTypeArr.length; i++) {
                    RowItem rowItem = new RowItem(loanTypeArr[i]);
                    loanTypeItems.add(rowItem);
                }

                loanTypeAdapter = new CustomItemAdapter(getActivity(), loanTypeItems);
                spn_label_loanType.setAdapter(loanTypeAdapter);

            }

            if (LD_InternalLoan.sMenuSelection.equals("Bank Loan")) {
                loanTypeItems = new ArrayList<RowItem>();
                for (int i = 0; i < loanTypeArr.length; i++) {
                    RowItem rowItem = new RowItem(loanTypeArr[i]);
                    loanTypeItems.add(rowItem);
                }

                loanTypeAdapter = new CustomItemAdapter(getActivity(), loanTypeItems);
                spn_label_loanType.setAdapter(loanTypeAdapter);
            }
            spn_label_loanType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    // TODO Auto-generated method stub
                    Log.d("LOAN TYPE POSITION", String.valueOf(position));

                    String selectedItem;
                    if (position == 0) {
                        selectedItem = loanTypeArr[0];
                        mLoanTypeValue = "0";
                    } else {
                        selectedItem = loanTypeArr[position];
                        System.out.println("SELECTED LOAN TYPE : " + selectedItem);
                        mLoanTypeValue = selectedItem;
                        if (LD_InternalLoan.sMenuSelection.equals("Bank Loan")) {
                            mLoanTypeIdValue = internalLT.get(position - 1).getLoanSettingId();
                            loanType = internalLT.get(position - 1).getSflag();
                        } else if (LD_InternalLoan.sMenuSelection.endsWith("Federation Loan")) {
                            mLoanTypeIdValue = internalFED.get(position - 1).getLoanSettingId();
                            loanType = internalFED.get(position - 1).getSflag();
                        }

                        if (mLoanTypeValue.equals("Cash Credit")) {
                            mTenureSpinner.setVisibility(View.GONE);
                            spn_label_installmentType.setVisibility(View.GONE);
                        } else {
                            mTenureSpinner.setVisibility(View.VISIBLE);
                            spn_label_installmentType.setVisibility(View.VISIBLE);
                        }

                    }
                    Log.e("Loan Type value", mLoanTypeValue);

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    // TODO Auto-generated method stub

                }
            });


            LoanDisbursementDto[] stockArr = new LoanDisbursementDto[internalInstallmenttype.size()];
            installmentArr = internalInstallmenttype.toArray(stockArr);


            installmentNameArr = new String[installmentArr.length + 1];
            installmentNameArr[0] = String.valueOf("" + AppStrings.installment_type);
            for (
                    int i = 0;
                    i < installmentArr.length; i++)

            {
                installmentNameArr[i + 1] = installmentArr[i].getInstallmentTypeName().toString();
            }

            installmentTypeItems = new ArrayList<RowItem>();
            for (
                    int i = 0;
                    i < installmentNameArr.length; i++)

            {
                RowItem rowItem = new RowItem(installmentNameArr[i]);
                installmentTypeItems.add(rowItem);
            }

            installmentTypeAdapter = new

                    CustomItemAdapter(getActivity(), installmentTypeItems);
            spn_label_installmentType.setAdapter(installmentTypeAdapter);
            spn_label_installmentType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()

            {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position,
                                           long id) {
                    // TODO Auto-generated method stub
                    Log.d(" POSITION", String.valueOf(position));

                    String selectedItem;
                    if (position == 0) {
                        selectedItem = installmentNameArr[0];
                        mInstallmentTypeValue = "0";
                    } else {
                        selectedItem = installmentNameArr[position];
                        mInstallTypeId = internalInstallmenttype.get(position - 1).getInstallmentTypeId();
                        System.out.println("SELECTED InstallmentType :" + selectedItem);

                        mInstallmentTypeValue = selectedItem.toUpperCase();

                      /*  if (position == 3) {
                            mInstallmentTypeValue = "MONTHLY";
                        } else if (position == 5) {
                            mInstallmentTypeValue = "QUARTERLY";
                        } else if (position == 1) {
                            mInstallmentTypeValue = "HALF YEARLY";
                        } else if (position == 4) {
                            mInstallmentTypeValue = "YEARLY";
                        } else if (position == 2) {
                            mInstallmentTypeValue = "Internal Loan";
                        }*/

                        Log.e("Install ment Type", mInstallmentTypeValue);
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    // TODO Auto-generated method stub

                }
            });

            //   POLArr = new String[]{AppStrings.purposeOfLoan, AppStrings.mAgri, AppStrings.mMSME, AppStrings.mOthers};


            LoanDto[] polArr = new LoanDto[internalPOL.size()];
            polArr = internalPOL.toArray(polArr);


            POLArr = new String[polArr.length + 1];
            POLArr[0] = String.valueOf("" + AppStrings.purposeOfLoan);
            for (
                    int i = 0;
                    i < polArr.length; i++)

            {
                POLArr[i + 1] = polArr[i].getLoanTypeName().toString();

            }

            POLItems = new ArrayList<RowItem>();
            for (
                    int i = 0;
                    i < POLArr.length; i++)

            {
                RowItem rowItem = new RowItem(POLArr[i]);
                POLItems.add(rowItem);
            }

            POLAdapter = new

                    CustomItemAdapter(getActivity(), POLItems);
            spn_label_POL.setAdapter(POLAdapter);
            spn_label_POL.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()

            {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position,
                                           long id) {
                    // TODO Auto-generated method stub
                    Log.d("POL POSITION", String.valueOf(position));

                    String selectedItem;
                    if (position == 0) {
                        selectedItem = POLArr[0];
                        mPOLValue = "0";
                    } else {
                        selectedItem = POLArr[position];
                        mselectedPOLId = internalPOL.get(position - 1).getLoanTypeId();
                        System.out.println("SELECTED POL :" + selectedItem);

                        mPOLValue = selectedItem.toUpperCase();

                   /*     if (position == 1) {
                            mPOLValue = "OTHERS";

                        } else if (position == 2) {
                            mPOLValue = "AGRI";
                        } else if (position == 3) {
                            mPOLValue = "MSME";
                        }*/

                        Log.e("Pol Values-------->", mPOLValue);

                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    // TODO Auto-generated method stub

                }
            });


            spn_label_Bankbranch.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()

            {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position,
                                           long id) {
                    // TODO Auto-generated method stub
                    Log.d("BankName POSITION", String.valueOf(position));

                    String selectedItem;
                    if (position == 0) {
                        selectedItem = branchNameArr[0];
                        mBankBranchValue = "0";
                    } else {
                        selectedItem = branchNameArr[position];
                        mSelectionBranchId = internalBranchList.get(position - 1).getId();
                        System.out.println("SELECTED Bank Name :" + selectedItem);

                        mBankBranchValue = selectedItem;

                    }
                    Log.e("Bank Branch Name value", mBankBranchValue);

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    // TODO Auto-generated method stub

                }
            });

            setVisibility();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void setVisibility() {
        // TODO Auto-generated method stub

        if (LD_InternalLoan.sMenuSelection.equals("Bank Loan")) {
            mBranchName.setVisibility(View.GONE);
            mFedBankNameHeader.setVisibility(View.GONE);
            mFedBankName.setVisibility(View.GONE);

        } else if (LD_InternalLoan.sMenuSelection.equals("MFI Loan")) {
            mNBFCEditText.setVisibility(View.GONE);
            spn_label_loanType.setVisibility(View.GONE);
            spn_label_Bankbranch.setVisibility(View.GONE);
            mFedBankNameHeader.setVisibility(View.GONE);
            mFedBankName.setVisibility(View.GONE);
            mSubsidy_AmountEditText.setVisibility(View.GONE);
            mSubsidy_Reserve_fundEditText.setVisibility(View.GONE);

        } else if (LD_InternalLoan.sMenuSelection.equals("Federation Loan")) {
            mNBFCEditText.setVisibility(View.GONE);
            spn_label_Bankbranch.setVisibility(View.GONE);
            spn_label_bankName.setVisibility(View.GONE);
            mSubsidy_AmountEditText.setVisibility(View.GONE);
            mSubsidy_Reserve_fundEditText.setVisibility(View.GONE);
            mLoan_Acc_NoEditText.setVisibility(View.GONE);
            mBranchName.setVisibility(View.GONE);
        }
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.loan_sanction_date:
                try {
                    callSanctionDate();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.loan_disbursement_date:
                try {
                    callDisbursementdate();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                break;

            case R.id.fragment_monthyear_picker_Submitbutton:
                mLoan_Acc_NoValue = mLoan_Acc_NoEditText.getText().toString().trim();
                mLoan_Sanc_AmountValue = mLoan_Sanc_AmountEditText.getText().toString().trim();
                mLoan_Dist_AmountValue = mLoan_Dist_AmountEditText.getText().toString().trim();
                // mLoan_TenureValue =
                // mLoan_TenureEditText.getText().toString().trim();
                mSubsidy_AmountValue = mSubsidy_AmountEditText.getText().toString().trim();
                mSubsidy_Reserve_fundValue = mSubsidy_Reserve_fundEditText.getText().toString().trim();
                mInterest_RateValue = mInterest_RateEditText.getText().toString().trim();
                mNBFCValue = mNBFCEditText.getText().toString().trim();
                mBankChargesValues = mBankCharges.getText().toString().trim();

                if (mBankChargesValues.equals("")) {
                    mBankChargesValues = "0";
                }
                if (mLoan_Sanc_AmountValue.equals("")) {
                    mLoan_Sanc_AmountValue = "0";
                }

                int availableLoanDisAmount = Integer.parseInt(mLoan_Sanc_AmountValue)
                        - Integer.parseInt(mBankChargesValues);



                try {
                    String sancdate = sanctionDate.getTime() + "";
                    String disbdate = disbursementDate.getTime() + "";
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    sanctionDate = sdf.parse(sancdate);
                    disbursementDate = sdf.parse(disbdate);
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (LD_InternalLoan.sMenuSelection.equals("Bank Loan")) {
                    boolean iSCashCreditLoan = false;
                    if (mLoanTypeValue.equals("Cash Credit")) {
                        iSCashCreditLoan = true;
                        mInstallmentTypeValue = "YEARLY";
                        mLoan_TenureValue = "12";


                    }

                    if ((!mBankNameValue.equals("")) && (!mBankNameValue.equals("0")) && (!mBankBranchValue.equals(""))
                            && (!mBankBranchValue.equals("0")) && (!mLoanTypeValue.equals(""))
                            && (!mLoanTypeValue.equals("0"))
                            && (!mInstallmentTypeValue.equals("") && !(mInstallmentTypeValue.equals("0")))
                            && (!mPOLValue.equals("") && (!mPOLValue.equals("0"))) && !mLoan_Acc_NoValue.isEmpty()
                            && !mLoan_Sanc_AmountValue.isEmpty() && !mLoan_Dist_AmountValue.isEmpty()
                            && !mLoan_TenureValue.isEmpty() && !mInterest_RateValue.isEmpty() && !disbursementDate.toString().isEmpty()
                            && !sanctionDate.toString().isEmpty() && !mBankBranchValue.equals("") && !mBankBranchValue.equals("0")
                            && !mBankChargesValues.isEmpty() && !selectedType.isEmpty()) {


                        if (disbursementDate.compareTo(sanctionDate) >= 0) {
                            if (!mLoan_Sanc_AmountValue.equals("0") && !mLoan_Dist_AmountValue.equals("0")) {

                                if (Integer.parseInt(mLoan_Sanc_AmountValue) >= Integer.parseInt(mLoan_Dist_AmountValue)) {

                                    if (Integer.parseInt(mLoan_Dist_AmountValue) <= availableLoanDisAmount) {

                                        if (LD_InternalLoan.sMenuSelection.equals("Bank Loan")) {
                                            mLoanType = "BANK LOAN";
                                            mLoanSource = "BANK";
                                        }
                                        if ((mSubsidy_AmountValue.equals("") && mSubsidy_Reserve_fundValue.equals(""))
                                                || (mSubsidy_AmountValue == null
                                                && mSubsidy_Reserve_fundEditText == null)) {
                                            mSubsidy_AmountValue = "0";
                                            mSubsidy_Reserve_fundValue = "0";
                                        }
                                        mNBFCValue = "0";
                                        // mNBFCValue = "";
                                        System.out.println("-------Subsidy AMount --------" + mSubsidy_AmountValue);
                                        System.out.println(
                                                "-------Subsidy Reserve AMount --------" + mSubsidy_Reserve_fundValue);


                                        if (selectedType.equals("Cash")) {
                                            if (!mBankValue.equals("0") && mBankValue != null) {
                                                if (mLoanTypeValue.equals("Cash Credit")) {
                                                    mInstallmentTypeValue = "0";
                                                    mLoan_TenureValue = "0";

                                                }

                                                new InternalBankMFIModel(mLoanType, mLoanSource, mBankNameValue, mNBFCValue,
                                                        mLoan_Acc_NoValue, sanctionDate.getTime() + "", mLoan_Sanc_AmountValue,
                                                        mBankChargesValues,
                                                        selectedType,
                                                        disbursementDate.getTime() + "", mLoan_Dist_AmountValue, mLoan_TenureValue,
                                                        mInstallmentTypeValue, mSubsidy_AmountValue,
                                                        mSubsidy_Reserve_fundValue, mPOLValue, "", mInterest_RateValue,
                                                        mLoanTypeValue, mBankBranchValue);
                                                if (iSCashCreditLoan) {
                                                    mConfirm_BankLoan = new String[]{
                                                            AppStrings.bankName,
                                                            AppStrings.loanType,
                                                            AppStrings.bankBranch,
                                                            AppStrings.Loan_Account_No,
                                                            AppStrings.Loan_Sanction_Date,
                                                            AppStrings.Loan_Sanction_Amount,
                                                            AppStrings.mBankCharges,
                                                            AppStrings.mLoanAccType,

                                                            AppStrings.Loan_Disbursement_Date,

                                                            AppStrings.Loan_Disbursement_Amount,

                                                            AppStrings.Subsidy_Amount,
                                                            AppStrings.Subsidy_Reserve_Fund,
                                                            AppStrings.purposeOfLoan,
                                                            AppStrings.Interest_Rate};

                                                    loanEntry.setBank(InternalBankMFIModel.getBankName());
                                                    loanEntry.setShgId(shgDto.getShgId());
                                                    loanEntry.setPurposeOfLoanId(mselectedPOLId);
                                                    loanEntry.setInstallmentTypeId(mLoanTypeIdValue);
                                                    loanEntry.setLoanTypeName(InternalBankMFIModel.getLoanType());
                                                    loanEntry.setLoanId(mInstallTypeId);
                                                    loanEntry.setLoanType(loanType);
                                                    loanEntry.setPurposeOfloans(InternalBankMFIModel.getPurpose_of_Loan());
                                                    loanEntry.setBankId(mBankNameIdValue);
                                                    loanEntry.setBranch(InternalBankMFIModel.getBankBranchName());
                                                    loanEntry.setBranchId(mSelectionBranchId);
                                                    loanEntry.setInstallmentTypeName(InternalBankMFIModel.getInstallment_Type());
                                                    loanEntry.setLoanAccountNumber(InternalBankMFIModel.getLoanAccNo());
                                                    loanEntry.setSanctionDate(df1.format(new Date(Long.parseLong(InternalBankMFIModel.getLoan_Sanction_Date()))));
                                                    loanEntry.setSanctionAmount(InternalBankMFIModel.getLoan_Sanction_Amount());
                                                    loanEntry.setDisbursmentDate(df1.format(new Date(Long.parseLong(InternalBankMFIModel.getLoan_Disbursement_Date()))));
                                                    loanEntry.setDisbursmentAmount(InternalBankMFIModel.getLoan_Disbursement_Amount());
                                                    loanEntry.setBankCharge(InternalBankMFIModel.getLoanBankCharges());
                                                    loanEntry.setType(InternalBankMFIModel.getType());
                                                    loanEntry.setTenure(mLoan_TenureValue);
                                                    loanEntry.setSubsidyReserveAmount(InternalBankMFIModel.getSubsidy_Reserve_Fund());
                                                    loanEntry.setSubsidyAmount(InternalBankMFIModel.getSubsidy_Amount());
                                                    loanEntry.setInterestRate(InternalBankMFIModel.getInterest_Rate());
                                                    loanEntry.setTransactionDate(shgDto.getLastTransactionDate());
                                                    loanEntry.setMobileDate(System.currentTimeMillis() + "");

                                                    mConfirmation_values = new String[]{
                                                            InternalBankMFIModel.getBankName(),
                                                            InternalBankMFIModel.getLoanName(),
                                                            InternalBankMFIModel.getBankBranchName(),
                                                            InternalBankMFIModel.getLoanAccNo(),
                                                            df.format(Long.parseLong(InternalBankMFIModel.getLoan_Sanction_Date())),
                                                            InternalBankMFIModel.getLoan_Sanction_Amount(),
                                                            InternalBankMFIModel.getLoanBankCharges(),
                                                            InternalBankMFIModel.getType(),
                                                            df.format(Long.parseLong(InternalBankMFIModel.getLoan_Disbursement_Date())),
                                                            InternalBankMFIModel.getLoan_Disbursement_Amount(),
                                                            InternalBankMFIModel.getSubsidy_Amount(),
                                                            InternalBankMFIModel.getSubsidy_Reserve_Fund(),
                                                            InternalBankMFIModel.getPurpose_of_Loan(),
                                                            InternalBankMFIModel.getInterest_Rate()};

                                                    showConfirmationDialog(mConfirm_BankLoan, mConfirmation_values);
                                                } else {

                                                    loanEntry.setBank(InternalBankMFIModel.getBankName());
                                                    loanEntry.setShgId(shgDto.getShgId());
                                                    loanEntry.setPurposeOfLoanId(mselectedPOLId);
                                                    loanEntry.setInstallmentTypeId(mLoanTypeIdValue);
                                                    loanEntry.setLoanTypeName(InternalBankMFIModel.getLoanType());
                                                    loanEntry.setLoanId(mInstallTypeId);
                                                    loanEntry.setLoanType(loanType);
                                                    loanEntry.setPurposeOfloans(InternalBankMFIModel.getPurpose_of_Loan());
                                                    loanEntry.setBankId(mBankNameIdValue);
                                                    loanEntry.setBranch(InternalBankMFIModel.getBankBranchName());
                                                    loanEntry.setBranchId(mSelectionBranchId);

                                                    loanEntry.setInstallmentTypeName(InternalBankMFIModel.getInstallment_Type());

                                                    loanEntry.setLoanAccountNumber(InternalBankMFIModel.getLoanAccNo());
                                                    loanEntry.setSanctionDate(df1.format(new Date(Long.parseLong(InternalBankMFIModel.getLoan_Sanction_Date()))));
                                                    loanEntry.setSanctionAmount(InternalBankMFIModel.getLoan_Sanction_Amount());
                                                    loanEntry.setDisbursmentDate(df1.format(new Date(Long.parseLong(InternalBankMFIModel.getLoan_Disbursement_Date()))));
                                                    loanEntry.setDisbursmentAmount(InternalBankMFIModel.getLoan_Disbursement_Amount());
                                                    loanEntry.setBankCharge(InternalBankMFIModel.getLoanBankCharges());
                                                    loanEntry.setType(InternalBankMFIModel.getType());
                                                    loanEntry.setTenure(mLoan_TenureValue);
                                                    loanEntry.setSubsidyReserveAmount(InternalBankMFIModel.getSubsidy_Reserve_Fund());
                                                    loanEntry.setSubsidyAmount(InternalBankMFIModel.getSubsidy_Amount());
                                                    loanEntry.setInterestRate(InternalBankMFIModel.getInterest_Rate());
                                                    loanEntry.setTransactionDate(shgDto.getLastTransactionDate());
                                                    loanEntry.setMobileDate(System.currentTimeMillis() + "");


                                                    mConfirmation_values = new String[]{
                                                            InternalBankMFIModel.getBankName(),
                                                            InternalBankMFIModel.getLoanName(),
                                                            InternalBankMFIModel.getBankBranchName(),
                                                            InternalBankMFIModel.getLoanAccNo(),
                                                            df.format(Long.parseLong(InternalBankMFIModel.getLoan_Sanction_Date())),
                                                            InternalBankMFIModel.getLoan_Sanction_Amount(),
                                                            InternalBankMFIModel.getLoanBankCharges(),
                                                            InternalBankMFIModel.getType(),
                                                            df.format(Long.parseLong(InternalBankMFIModel.getLoan_Disbursement_Date())),
                                                            InternalBankMFIModel.getLoan_Disbursement_Amount(),
                                                            InternalBankMFIModel.getLoan_Tenure(),
                                                            InternalBankMFIModel.getInstallment_Type(),
                                                            InternalBankMFIModel.getSubsidy_Amount(),
                                                            InternalBankMFIModel.getSubsidy_Reserve_Fund(),
                                                            InternalBankMFIModel.getPurpose_of_Loan(),
                                                            InternalBankMFIModel.getInterest_Rate()};

                                                    showConfirmationDialog(mConfirm_BankLoan, mConfirmation_values);
                                                }

                                            } else {
                                                TastyToast.makeText(getActivity(), AppStrings.mLoanaccBankNullToast,
                                                        TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                                            }

                                        } else {


                                            new InternalBankMFIModel(mLoanType, mLoanSource, mBankNameValue, mNBFCValue,
                                                    mLoan_Acc_NoValue, sanctionDate.getTime() + "", mLoan_Sanc_AmountValue,
                                                    mBankChargesValues,
                                                    selectedType, disbursementDate.getTime() + "",
                                                    mLoan_Dist_AmountValue, mLoan_TenureValue, mInstallmentTypeValue,
                                                    mSubsidy_AmountValue, mSubsidy_Reserve_fundValue, mPOLValue, "",
                                                    mInterest_RateValue, mLoanTypeValue, mBankBranchValue);
                                            if (iSCashCreditLoan) {
                                                mConfirm_BankLoan = new String[]{
                                                        AppStrings.bankName,
                                                        AppStrings.loanType,
                                                        AppStrings.bankBranch,
                                                        AppStrings.Loan_Account_No,
                                                        AppStrings.Loan_Sanction_Date,
                                                        AppStrings.Loan_Sanction_Amount,
                                                        AppStrings.mBankCharges,
                                                        AppStrings.mLoanAccType,
                                                        AppStrings.Loan_Disbursement_Date,
                                                        AppStrings.Loan_Disbursement_Amount,

                                                        AppStrings.Subsidy_Amount,
                                                        AppStrings.Subsidy_Reserve_Fund,
                                                        AppStrings.purposeOfLoan,
                                                        AppStrings.Interest_Rate};

                                                loanEntry.setBank(InternalBankMFIModel.getBankName());
                                                loanEntry.setShgId(shgDto.getShgId());
                                                loanEntry.setPurposeOfLoanId(mselectedPOLId);
                                                loanEntry.setInstallmentTypeId(mLoanTypeIdValue);
                                                loanEntry.setLoanTypeName(InternalBankMFIModel.getLoanType());
                                                loanEntry.setLoanId(mInstallTypeId);
                                                loanEntry.setLoanType(loanType);
                                                loanEntry.setPurposeOfloans(InternalBankMFIModel.getPurpose_of_Loan());
                                                loanEntry.setBankId(mBankNameIdValue);
                                                loanEntry.setBranch(InternalBankMFIModel.getBankBranchName());
                                                loanEntry.setBranchId(mSelectionBranchId);
                                                loanEntry.setInstallmentTypeName(InternalBankMFIModel.getInstallment_Type());
                                                loanEntry.setLoanAccountNumber(InternalBankMFIModel.getLoanAccNo());
                                                loanEntry.setSanctionDate(df1.format(new Date(Long.parseLong(InternalBankMFIModel.getLoan_Sanction_Date()))));
                                                loanEntry.setSanctionAmount(InternalBankMFIModel.getLoan_Sanction_Amount());
                                                loanEntry.setDisbursmentDate(df1.format(new Date(Long.parseLong(InternalBankMFIModel.getLoan_Disbursement_Date()))));
                                                loanEntry.setDisbursmentAmount(InternalBankMFIModel.getLoan_Disbursement_Amount());
                                                loanEntry.setBankCharge(InternalBankMFIModel.getLoanBankCharges());
                                                loanEntry.setType(InternalBankMFIModel.getType());
                                                loanEntry.setTenure(mLoan_TenureValue);
                                                loanEntry.setSubsidyReserveAmount(InternalBankMFIModel.getSubsidy_Reserve_Fund());
                                                loanEntry.setSubsidyAmount(InternalBankMFIModel.getSubsidy_Amount());
                                                loanEntry.setInterestRate(InternalBankMFIModel.getInterest_Rate());
                                                loanEntry.setTransactionDate(shgDto.getLastTransactionDate());
                                                loanEntry.setMobileDate(System.currentTimeMillis() + "");
                                                loanEntry.setSavingsAccountId(sSelectedSavingbank.getShgSavingsAccountId());
                                                loanEntry.setSbAccountId(sSelectedSavingbank.getShgSavingsAccountId());


                                                mConfirmation_values = new String[]{InternalBankMFIModel.getBankName(),
                                                        InternalBankMFIModel.getLoanName(),
                                                        InternalBankMFIModel.getBankBranchName(),
                                                        InternalBankMFIModel.getLoanAccNo(),
                                                        df.format(Long.parseLong(InternalBankMFIModel.getLoan_Sanction_Date())),
                                                        InternalBankMFIModel.getLoan_Sanction_Amount(),
                                                        InternalBankMFIModel.getLoanBankCharges(),
                                                        InternalBankMFIModel.getType(),
                                                        df.format(Long.parseLong(InternalBankMFIModel.getLoan_Disbursement_Date())),
                                                        InternalBankMFIModel.getLoan_Disbursement_Amount(),

                                                        InternalBankMFIModel.getSubsidy_Amount(),
                                                        InternalBankMFIModel.getSubsidy_Reserve_Fund(),
                                                        InternalBankMFIModel.getPurpose_of_Loan(),
                                                        InternalBankMFIModel.getInterest_Rate()};
                                                showConfirmationDialog(mConfirm_BankLoan, mConfirmation_values);
                                            } else {

                                                loanEntry.setBank(InternalBankMFIModel.getBankName());
                                                loanEntry.setShgId(shgDto.getShgId());
                                                loanEntry.setPurposeOfLoanId(mselectedPOLId);
                                                loanEntry.setInstallmentTypeId(mLoanTypeIdValue);
                                                loanEntry.setLoanTypeName(InternalBankMFIModel.getLoanType());
                                                loanEntry.setLoanId(mInstallTypeId);
                                                loanEntry.setLoanType(loanType);
                                                loanEntry.setPurposeOfloans(InternalBankMFIModel.getPurpose_of_Loan());
                                                loanEntry.setBankId(mBankNameIdValue);
                                                loanEntry.setBranch(InternalBankMFIModel.getBankBranchName());
                                                loanEntry.setBranchId(mSelectionBranchId);
                                                loanEntry.setInstallmentTypeName(InternalBankMFIModel.getInstallment_Type());
                                                loanEntry.setTransactionDate(shgDto.getLastTransactionDate());
                                                loanEntry.setMobileDate(System.currentTimeMillis() + "");
                                                loanEntry.setLoanAccountNumber(InternalBankMFIModel.getLoanAccNo());
                                                loanEntry.setSanctionDate(df1.format(new Date(Long.parseLong(InternalBankMFIModel.getLoan_Sanction_Date()))));
                                                loanEntry.setSanctionAmount(InternalBankMFIModel.getLoan_Sanction_Amount());
                                                loanEntry.setDisbursmentDate(df1.format(new Date(Long.parseLong(InternalBankMFIModel.getLoan_Disbursement_Date()))));
                                                loanEntry.setDisbursmentAmount(InternalBankMFIModel.getLoan_Disbursement_Amount());
                                                loanEntry.setBankCharge(InternalBankMFIModel.getLoanBankCharges());
                                                loanEntry.setType(InternalBankMFIModel.getType());
                                                loanEntry.setTenure(mLoan_TenureValue);
                                                loanEntry.setSubsidyReserveAmount(InternalBankMFIModel.getSubsidy_Reserve_Fund());
                                                loanEntry.setSubsidyAmount(InternalBankMFIModel.getSubsidy_Amount());
                                                loanEntry.setInterestRate(InternalBankMFIModel.getInterest_Rate());
                                                loanEntry.setSavingsAccountId(sSelectedSavingbank.getShgSavingsAccountId());
                                                loanEntry.setSbAccountId(sSelectedSavingbank.getShgSavingsAccountId());

                                                mConfirmation_values = new String[]{InternalBankMFIModel.getBankName(),
                                                        InternalBankMFIModel.getLoanName(),
                                                        InternalBankMFIModel.getBankBranchName(),
                                                        InternalBankMFIModel.getLoanAccNo(),
                                                        df.format(Long.parseLong(InternalBankMFIModel.getLoan_Sanction_Date())),
                                                        InternalBankMFIModel.getLoan_Sanction_Amount(),
                                                        InternalBankMFIModel.getLoanBankCharges(),
                                                        InternalBankMFIModel.getType(),
                                                        df.format(Long.parseLong(InternalBankMFIModel.getLoan_Disbursement_Date())),
                                                        InternalBankMFIModel.getLoan_Disbursement_Amount(),
                                                        InternalBankMFIModel.getLoan_Tenure(),
                                                        InternalBankMFIModel.getInstallment_Type(),
                                                        InternalBankMFIModel.getSubsidy_Amount(),
                                                        InternalBankMFIModel.getSubsidy_Reserve_Fund(),
                                                        InternalBankMFIModel.getPurpose_of_Loan(),
                                                        InternalBankMFIModel.getInterest_Rate()};
                                                showConfirmationDialog(mConfirm_BankLoan, mConfirmation_values);
                                            }

                                        }

                                    } else {

                                        mLoan_Dist_AmountEditText.setError(
                                                "AVAILABLE LOAN DISBURSEMENT AMOUNT IS " + availableLoanDisAmount);
                                        TastyToast.makeText(getActivity(), AppStrings.mLoanSanc_loanDist,
                                                TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                                    }

                                } else {

                                    TastyToast.makeText(getActivity(), AppStrings.mLoanSanc_loanDist,
                                            TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                                }
                            } else {

                                if (mLoan_Sanc_AmountValue.equals("0") && mLoan_Dist_AmountValue.equals("0")) {
                                    mLoan_Sanc_AmountEditText.setError(AppStrings.mCheckLoanSanctionAmount);
                                    mLoan_Dist_AmountEditText.setError(AppStrings.mCheckLoanDisbursementAmount);
                                    TastyToast.makeText(getActivity(), AppStrings.mCheckLoanAmounts,
                                            TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                                } else if (mLoan_Sanc_AmountValue.equals("0") && !mLoan_Dist_AmountValue.equals("0")) {
                                    mLoan_Sanc_AmountEditText.setError(AppStrings.mCheckLoanSanctionAmount);
                                    TastyToast.makeText(getActivity(), AppStrings.mCheckLoanAmounts,
                                            TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                                } else if (!mLoan_Sanc_AmountValue.equals("0") && mLoan_Dist_AmountValue.equals("0")) {
                                    mLoan_Dist_AmountEditText.setError(AppStrings.mCheckLoanDisbursementAmount);
                                    TastyToast.makeText(getActivity(), AppStrings.mCheckLoanAmounts,
                                            TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                                }

                            }
                        } else {
                            TastyToast.makeText(getActivity(), AppStrings.mCheckLoanSancDate_LoanDisbDate,
                                    TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                        }
                    } else {
                        TastyToast.makeText(getActivity(), AppStrings.nullDetailsAlert, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);
                    }
                } else if (LD_InternalLoan.sMenuSelection.equals("MFI Loan")) {
                    mBankBranchValue = mBranchName.getText().toString();
                    if ((!mBankNameValue.equals("")) && (!mBankNameValue.equals("0"))
                            && (!mInstallmentTypeValue.equals("") && !(mInstallmentTypeValue.equals("0")))
                            && (!mPOLValue.equals("") && (!mPOLValue.equals("0"))) && !mLoan_Acc_NoValue.isEmpty()
                            && !mLoan_Sanc_AmountValue.isEmpty() && !mLoan_Dist_AmountValue.isEmpty()
                            && !mLoan_TenureValue.isEmpty() && !mInterest_RateValue.isEmpty() && !disbursementDate.toString().isEmpty()
                            && !sanctionDate.toString().isEmpty() && !mBankBranchValue.equals("") && !mBankBranchValue.equals("0")
                            && !mBankChargesValues.isEmpty() && !selectedType.isEmpty()) {
                        if (disbursementDate.compareTo(sanctionDate) >= 0) {
                            if (!mLoan_Sanc_AmountValue.equals("0") && !mLoan_Dist_AmountValue.equals("0")) {
                                if (Integer.parseInt(mLoan_Sanc_AmountValue) >= Integer.parseInt(mLoan_Dist_AmountValue)) {
                                    if (Integer.parseInt(mLoan_Dist_AmountValue) <= availableLoanDisAmount) {
                                        if (mSubsidy_AmountValue.isEmpty() && mSubsidy_Reserve_fundValue.isEmpty()) {

                                            mSubsidy_AmountValue = "0";
                                            mSubsidy_Reserve_fundValue = "0";
                                        }
                                        if (LD_InternalLoan.sMenuSelection.equals("MFI Loan")) {
                                            mLoanType = "OTHER LOAN";
                                            mLoanSource = "MFI";
                                            mLoanTypeValue = "MFI LOAN";
                                        }

                                        mNBFCValue = "0";
                                        System.out.println("-------Subsidy AMount --------" + mSubsidy_AmountValue);
                                        System.out.println(
                                                "-------Subsidy Reserve AMount --------" + mSubsidy_Reserve_fundValue);

                                        if (selectedType.equals("Cash")) {
                                            if (!mBankValue.equals("0") && mBankValue != null) {
                                                new InternalBankMFIModel(mLoanType, mLoanSource, mBankNameValue, mNBFCValue,
                                                        mLoan_Acc_NoValue, sanctionDate.getTime() + "", mLoan_Sanc_AmountValue,
                                                        mBankChargesValues,
                                                        selectedType,
                                                        disbursementDate.getTime() + "", mLoan_Dist_AmountValue, mLoan_TenureValue,
                                                        mInstallmentTypeValue, mSubsidy_AmountValue,
                                                        mSubsidy_Reserve_fundValue, mPOLValue, "", mInterest_RateValue,
                                                        mLoanTypeValue, mBankBranchValue);

                                                //  loanEntry.setBankName(InternalBankMFIModel.getBankName());
                                                //   loanEntry.setBankId(mBankNameIdValue);
                                                loanEntry.setBank(InternalBankMFIModel.getBankName());
                                                loanEntry.setBankname(mLoanId);
                                                loanEntry.setShgId(shgDto.getShgId());
                                                loanEntry.setPurposeOfLoanId(mselectedPOLId);
                                                loanEntry.setInstallmentTypeId(mLoanTypeIdValue);
                                                loanEntry.setLoanTypeName(InternalBankMFIModel.getLoanType());
                                                loanEntry.setLoanId(mInstallTypeId);
                                                loanEntry.setLoanType(loanType);
                                                loanEntry.setPurposeOfloans(InternalBankMFIModel.getPurpose_of_Loan());
                                                loanEntry.setBankId(mBankNameIdValue);
                                                loanEntry.setBranch(InternalBankMFIModel.getBankBranchName());
                                                loanEntry.setBranchId(mSelectionBranchId);
                                                loanEntry.setTransactionDate(shgDto.getLastTransactionDate());
                                                loanEntry.setMobileDate(System.currentTimeMillis() + "");
                                                loanEntry.setInstallmentTypeName(InternalBankMFIModel.getInstallment_Type());
                                                loanEntry.setLoanAccountNumber(InternalBankMFIModel.getLoanAccNo());
                                                loanEntry.setSanctionDate(df1.format(new Date(Long.parseLong(InternalBankMFIModel.getLoan_Sanction_Date()))));
                                                loanEntry.setSanctionAmount(InternalBankMFIModel.getLoan_Sanction_Amount());
                                                loanEntry.setDisbursmentDate(df1.format(new Date(Long.parseLong(InternalBankMFIModel.getLoan_Disbursement_Date()))));
                                                loanEntry.setDisbursmentAmount(InternalBankMFIModel.getLoan_Disbursement_Amount());
                                                loanEntry.setBankCharge(InternalBankMFIModel.getLoanBankCharges());
                                                loanEntry.setType(InternalBankMFIModel.getType());
                                                loanEntry.setTenure(mLoan_TenureValue);
                                                loanEntry.setSubsidyReserveAmount(InternalBankMFIModel.getSubsidy_Reserve_Fund());
                                                loanEntry.setSubsidyAmount(InternalBankMFIModel.getSubsidy_Amount());
                                                loanEntry.setInterestRate(InternalBankMFIModel.getInterest_Rate());

                                                mConfirmation_values = new String[]{InternalBankMFIModel.getBankName(),
                                                        InternalBankMFIModel.getBankBranchName(),
                                                        InternalBankMFIModel.getLoanAccNo(),
                                                        df.format(Long.parseLong(InternalBankMFIModel.getLoan_Sanction_Date())),
                                                        InternalBankMFIModel.getLoan_Sanction_Amount(),
                                                        InternalBankMFIModel.getLoanBankCharges(),
                                                        InternalBankMFIModel.getType(),
                                                        df.format(Long.parseLong(InternalBankMFIModel.getLoan_Disbursement_Date())),
                                                        InternalBankMFIModel.getLoan_Disbursement_Amount(),
                                                        InternalBankMFIModel.getLoan_Tenure(),
                                                        InternalBankMFIModel.getInstallment_Type(),
                                                        InternalBankMFIModel.getPurpose_of_Loan(),
                                                        InternalBankMFIModel.getInterest_Rate()};
                                                showConfirmationDialog(mConfirm_BankLoan, mConfirmation_values);
                                            } else {
                                                TastyToast.makeText(getActivity(), AppStrings.mLoanaccBankNullToast,
                                                        TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                                            }

                                        } else {
                                            new InternalBankMFIModel(mLoanType, mLoanSource, mBankNameValue, mNBFCValue,
                                                    mLoan_Acc_NoValue, sanctionDate.getTime() + "", mLoan_Sanc_AmountValue,
                                                    mBankChargesValues,
                                                    selectedType, disbursementDate.getTime() + "",
                                                    mLoan_Dist_AmountValue, mLoan_TenureValue, mInstallmentTypeValue,
                                                    mSubsidy_AmountValue, mSubsidy_Reserve_fundValue, mPOLValue, "",
                                                    mInterest_RateValue, mLoanTypeValue, mBankBranchValue);

                                            loanEntry.setBank(InternalBankMFIModel.getBankName());
                                            loanEntry.setBankname(mLoanId);
                                            loanEntry.setShgId(shgDto.getShgId());
                                            loanEntry.setPurposeOfLoanId(mselectedPOLId);
                                            loanEntry.setInstallmentTypeId(mLoanTypeIdValue);
                                            loanEntry.setLoanTypeName(InternalBankMFIModel.getLoanType());
                                            loanEntry.setLoanId(mInstallTypeId);
                                            loanEntry.setLoanType(loanType);
                                            loanEntry.setPurposeOfloans(InternalBankMFIModel.getPurpose_of_Loan());
                                            loanEntry.setBankId(mBankNameIdValue);
                                            loanEntry.setBranch(InternalBankMFIModel.getBankBranchName());
                                            loanEntry.setBranchId(mSelectionBranchId);
                                            loanEntry.setTransactionDate(shgDto.getLastTransactionDate());
                                            loanEntry.setMobileDate(System.currentTimeMillis() + "");
                                            loanEntry.setInstallmentTypeName(InternalBankMFIModel.getInstallment_Type());

                                            loanEntry.setLoanAccountNumber(InternalBankMFIModel.getLoanAccNo());
                                            loanEntry.setSanctionDate(df1.format(new Date(Long.parseLong(InternalBankMFIModel.getLoan_Sanction_Date()))));
                                            loanEntry.setSanctionAmount(InternalBankMFIModel.getLoan_Sanction_Amount());
                                            loanEntry.setDisbursmentDate(df1.format(new Date(Long.parseLong(InternalBankMFIModel.getLoan_Disbursement_Date()))));
                                            loanEntry.setDisbursmentAmount(InternalBankMFIModel.getLoan_Disbursement_Amount());
                                            loanEntry.setBankCharge(InternalBankMFIModel.getLoanBankCharges());
                                            loanEntry.setType(InternalBankMFIModel.getType());
                                            loanEntry.setTenure(mLoan_TenureValue);
                                            loanEntry.setSubsidyReserveAmount(InternalBankMFIModel.getSubsidy_Reserve_Fund());
                                            loanEntry.setSubsidyAmount(InternalBankMFIModel.getSubsidy_Amount());
                                            loanEntry.setInterestRate(InternalBankMFIModel.getInterest_Rate());

                                            loanEntry.setSavingsAccountId(sSelectedSavingbank.getShgSavingsAccountId());
                                            loanEntry.setSbAccountId(sSelectedSavingbank.getShgSavingsAccountId());

                                            mConfirmation_values = new String[]{InternalBankMFIModel.getBankName(),
                                                    InternalBankMFIModel.getBankBranchName(),
                                                    InternalBankMFIModel.getLoanAccNo(),
                                                    df.format(Long.parseLong(InternalBankMFIModel.getLoan_Sanction_Date())),
                                                    InternalBankMFIModel.getLoan_Sanction_Amount(),
                                                    InternalBankMFIModel.getLoanBankCharges(),
                                                    InternalBankMFIModel.getType(),
                                                    df.format(Long.parseLong(InternalBankMFIModel.getLoan_Disbursement_Date())),
                                                    InternalBankMFIModel.getLoan_Disbursement_Amount(),
                                                    InternalBankMFIModel.getLoan_Tenure(),
                                                    InternalBankMFIModel.getInstallment_Type(),
                                                    InternalBankMFIModel.getPurpose_of_Loan(),
                                                    InternalBankMFIModel.getInterest_Rate()};
                                            showConfirmationDialog(mConfirm_BankLoan, mConfirmation_values);

                                        }
                                    } else {

                                        mLoan_Dist_AmountEditText.setError(
                                                "AVAILABLE LOAN DISBURSEMENT AMOUNT IS " + availableLoanDisAmount);
                                        TastyToast.makeText(getActivity(), AppStrings.mLoanSanc_loanDist,
                                                TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                                    }
                                } else {
                                    TastyToast.makeText(getActivity(), AppStrings.mLoanSanc_loanDist,
                                            TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                                }
                            } else {

                                if (mLoan_Sanc_AmountValue.equals("0") && mLoan_Dist_AmountValue.equals("0")) {
                                    mLoan_Sanc_AmountEditText.setError(AppStrings.mCheckLoanSanctionAmount);
                                    mLoan_Dist_AmountEditText.setError(AppStrings.mCheckLoanDisbursementAmount);
                                    TastyToast.makeText(getActivity(), AppStrings.mCheckLoanAmounts,
                                            TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                                } else if (mLoan_Sanc_AmountValue.equals("0") && !mLoan_Dist_AmountValue.equals("0")) {
                                    mLoan_Sanc_AmountEditText.setError(AppStrings.mCheckLoanSanctionAmount);
                                    TastyToast.makeText(getActivity(), AppStrings.mCheckLoanAmounts,
                                            TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                                } else if (!mLoan_Sanc_AmountValue.equals("0") && mLoan_Dist_AmountValue.equals("0")) {
                                    mLoan_Dist_AmountEditText.setError(AppStrings.mCheckLoanDisbursementAmount);
                                    TastyToast.makeText(getActivity(), AppStrings.mCheckLoanAmounts,
                                            TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                                }

                            }
                        } else {
                            TastyToast.makeText(getActivity(), AppStrings.mCheckLoanSancDate_LoanDisbDate,
                                    TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                        }
                    } else {
                        TastyToast.makeText(getActivity(), AppStrings.nullDetailsAlert, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);
                    }


                } else if (LD_InternalLoan.sMenuSelection.equals("Federation Loan")) {

                    mBankNameValue = (shgDto.getPanchayatName() != null && shgDto.getPanchayatName().length() > 0) ? shgDto.getPanchayatName() : "NA";
                    mBankBranchValue = (shgDto.getPanchayatName() != null && shgDto.getPanchayatName().length() > 0) ? shgDto.getPanchayatName() : "NA";
                    mLoan_Acc_NoValue = "0";
                    Log.e("FEDLOAN Bank @", mBankBranchValue + "");
                    if ((!mBankNameValue.equals("")) && (!mBankNameValue.equals("0"))
                            && (!mInstallmentTypeValue.equals("") && !(mInstallmentTypeValue.equals("0")))
                            && !mLoan_Acc_NoValue.isEmpty() && (!mPOLValue.equals("") && (!mPOLValue.equals("0")))
                            && !mLoan_Sanc_AmountValue.isEmpty() && !mLoan_Dist_AmountValue.isEmpty()
                            && !mLoan_TenureValue.isEmpty() && !mInterest_RateValue.isEmpty() && !disbursementDate.toString().isEmpty()
                            && !sanctionDate.toString().isEmpty() && !mBankBranchValue.equals("") && !mBankBranchValue.equals("0")
                            && !mBankChargesValues.isEmpty() && !selectedType.isEmpty() && !mLoanTypeValue.equals("0")) {

                        if (disbursementDate.compareTo(sanctionDate) >= 0) {
                            if (!mLoan_Sanc_AmountValue.equals("0") && !mLoan_Dist_AmountValue.equals("0")) {
                                if (Integer.parseInt(mLoan_Sanc_AmountValue) >= Integer.parseInt(mLoan_Dist_AmountValue)) {

                                    System.out.println("---------------------1");
                                    if (Integer.parseInt(mLoan_Dist_AmountValue) <= availableLoanDisAmount) {
                                        if (LD_InternalLoan.sMenuSelection.equals("Federation Loan")) {
                                            mLoanType = "OTHER LOAN";
                                            mLoanSource = "FEDERATION";
                                        }
                                        if (mSubsidy_AmountValue.isEmpty() && mSubsidy_Reserve_fundValue.isEmpty()) {

                                            mSubsidy_AmountValue = "0";
                                            mSubsidy_Reserve_fundValue = "0";
                                        }

                                        mNBFCValue = "0";
                                        // mNBFCValue = "";
                                        System.out.println("-------Subsidy AMount --------" + mSubsidy_AmountValue);
                                        System.out.println(
                                                "-------Subsidy Reserve AMount --------" + mSubsidy_Reserve_fundValue);

                                        if (selectedType.equals("Cash")) {
                                            if (!mBankValue.equals("0") && mBankValue != null) {
                                                new InternalBankMFIModel(mLoanType, mLoanSource, mBankNameValue, mNBFCValue,
                                                        mLoan_Acc_NoValue, sanctionDate.getTime() + "", mLoan_Sanc_AmountValue,
                                                        mBankChargesValues,
                                                        selectedType,
                                                        disbursementDate.getTime() + "", mLoan_Dist_AmountValue, mLoan_TenureValue,
                                                        mInstallmentTypeValue, mSubsidy_AmountValue,
                                                        mSubsidy_Reserve_fundValue, mPOLValue, "", mInterest_RateValue,
                                                        mLoanTypeValue, mBankBranchValue);

                                                loanEntry.setBank(InternalBankMFIModel.getBankName());
                                                loanEntry.setShgId(shgDto.getShgId());
                                                loanEntry.setPurposeOfLoanId(mselectedPOLId);
                                                loanEntry.setInstallmentTypeId(mLoanTypeIdValue);
                                                loanEntry.setLoanTypeName(InternalBankMFIModel.getLoanType());
                                                loanEntry.setLoanId(mInstallTypeId);
                                                loanEntry.setLoanType(loanType);
                                                loanEntry.setPurposeOfloans(InternalBankMFIModel.getPurpose_of_Loan());
                                                loanEntry.setBankId(mBankNameIdValue);
                                                loanEntry.setBranch(InternalBankMFIModel.getBankBranchName());
                                                loanEntry.setBranchId(mSelectionBranchId);
                                                loanEntry.setTransactionDate(shgDto.getLastTransactionDate());
                                                loanEntry.setMobileDate(System.currentTimeMillis() + "");
                                                loanEntry.setInstallmentTypeName(InternalBankMFIModel.getInstallment_Type());

                                                loanEntry.setLoanAccountNumber(InternalBankMFIModel.getLoanAccNo());
                                                loanEntry.setSanctionDate(df1.format(new Date(Long.parseLong(InternalBankMFIModel.getLoan_Sanction_Date()))));
                                                loanEntry.setSanctionAmount(InternalBankMFIModel.getLoan_Sanction_Amount());
                                                loanEntry.setDisbursmentDate(df1.format(new Date(Long.parseLong(InternalBankMFIModel.getLoan_Disbursement_Date()))));
                                                loanEntry.setDisbursmentAmount(InternalBankMFIModel.getLoan_Disbursement_Amount());
                                                loanEntry.setBankCharge(InternalBankMFIModel.getLoanBankCharges());
                                                loanEntry.setType(InternalBankMFIModel.getType());
                                                loanEntry.setTenure(mLoan_TenureValue);
                                                loanEntry.setSubsidyReserveAmount(InternalBankMFIModel.getSubsidy_Reserve_Fund());
                                                loanEntry.setSubsidyAmount(InternalBankMFIModel.getSubsidy_Amount());
                                                loanEntry.setInterestRate(InternalBankMFIModel.getInterest_Rate());

                                                //  loanEntry.setSavingsAccountId(sSelectedSavingbank.getShgSavingsAccountId());

                                                mConfirmation_values = new String[]{InternalBankMFIModel.getBankName(),
                                                        InternalBankMFIModel.getLoanName(),
                                                        df.format(Long.parseLong(InternalBankMFIModel.getLoan_Sanction_Date())),
                                                        InternalBankMFIModel.getLoan_Sanction_Amount(),
                                                        InternalBankMFIModel.getLoanBankCharges(),
                                                        InternalBankMFIModel.getType(),
                                                        df.format(Long.parseLong(InternalBankMFIModel.getLoan_Disbursement_Date())),
                                                        InternalBankMFIModel.getLoan_Disbursement_Amount(),
                                                        InternalBankMFIModel.getLoan_Tenure(),
                                                        InternalBankMFIModel.getInstallment_Type(),
                                                        InternalBankMFIModel.getPurpose_of_Loan(),
                                                        InternalBankMFIModel.getInterest_Rate()};
                                                showConfirmationDialog(mConfirm_BankLoan, mConfirmation_values);
                                            } else {
                                                TastyToast.makeText(getActivity(), AppStrings.mLoanaccBankNullToast,
                                                        TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                                            }
                                        } else {
                                            new InternalBankMFIModel(mLoanType, mLoanSource, mBankNameValue, mNBFCValue,
                                                    mLoan_Acc_NoValue, sanctionDate.getTime() + "", mLoan_Sanc_AmountValue,
                                                    mBankChargesValues,
                                                    selectedType, disbursementDate.getTime() + "",
                                                    mLoan_Dist_AmountValue, mLoan_TenureValue, mInstallmentTypeValue,
                                                    mSubsidy_AmountValue, mSubsidy_Reserve_fundValue, mPOLValue, "",
                                                    mInterest_RateValue, mLoanTypeValue, mBankBranchValue);


                                            loanEntry.setBank(InternalBankMFIModel.getBankName());
                                            loanEntry.setShgId(shgDto.getShgId());
                                            loanEntry.setPurposeOfLoanId(mselectedPOLId);
                                            loanEntry.setInstallmentTypeId(mLoanTypeIdValue);
                                            loanEntry.setLoanTypeName(InternalBankMFIModel.getLoanType());
                                            loanEntry.setLoanId(mInstallTypeId);
                                            loanEntry.setLoanType(loanType);
                                            loanEntry.setPurposeOfloans(InternalBankMFIModel.getPurpose_of_Loan());
                                            loanEntry.setBankId(mBankNameIdValue);
                                            loanEntry.setBranch(InternalBankMFIModel.getBankBranchName());
                                            loanEntry.setBranchId(mSelectionBranchId);
                                            loanEntry.setTransactionDate(shgDto.getLastTransactionDate());
                                            loanEntry.setMobileDate(System.currentTimeMillis() + "");
                                            loanEntry.setInstallmentTypeName(InternalBankMFIModel.getInstallment_Type());

                                            loanEntry.setLoanAccountNumber(InternalBankMFIModel.getLoanAccNo());
                                            loanEntry.setSanctionDate(df1.format(new Date(Long.parseLong(InternalBankMFIModel.getLoan_Sanction_Date()))));
                                            loanEntry.setSanctionAmount(InternalBankMFIModel.getLoan_Sanction_Amount());
                                            loanEntry.setDisbursmentDate(df1.format(new Date(Long.parseLong(InternalBankMFIModel.getLoan_Disbursement_Date()))));
                                            loanEntry.setDisbursmentAmount(InternalBankMFIModel.getLoan_Disbursement_Amount());
                                            loanEntry.setBankCharge(InternalBankMFIModel.getLoanBankCharges());
                                            loanEntry.setType(InternalBankMFIModel.getType());
                                            loanEntry.setTenure(mLoan_TenureValue);
                                            loanEntry.setSubsidyReserveAmount(InternalBankMFIModel.getSubsidy_Reserve_Fund());
                                            loanEntry.setSubsidyAmount(InternalBankMFIModel.getSubsidy_Amount());
                                            loanEntry.setInterestRate(InternalBankMFIModel.getInterest_Rate());

                                            loanEntry.setSavingsAccountId(sSelectedSavingbank.getShgSavingsAccountId());
                                            loanEntry.setSbAccountId(sSelectedSavingbank.getShgSavingsAccountId());

                                            mConfirmation_values = new String[]{InternalBankMFIModel.getBankName(),
                                                    InternalBankMFIModel.getLoanName(),
                                                    df.format(Long.parseLong(InternalBankMFIModel.getLoan_Sanction_Date())),
                                                    InternalBankMFIModel.getLoan_Sanction_Amount(),
                                                    InternalBankMFIModel.getLoanBankCharges(),
                                                    InternalBankMFIModel.getType(),
                                                    df.format(Long.parseLong(InternalBankMFIModel.getLoan_Disbursement_Date())),
                                                    InternalBankMFIModel.getLoan_Disbursement_Amount(),
                                                    InternalBankMFIModel.getLoan_Tenure(),
                                                    InternalBankMFIModel.getInstallment_Type(),
                                                    InternalBankMFIModel.getPurpose_of_Loan(),
                                                    InternalBankMFIModel.getInterest_Rate()};
                                            showConfirmationDialog(mConfirm_BankLoan, mConfirmation_values);
                                        }
                                    } else {

                                        mLoan_Dist_AmountEditText.setError(
                                                "AVAILABLE LOAN DISBURSEMENT AMOUNT IS " + availableLoanDisAmount);
                                        TastyToast.makeText(getActivity(), AppStrings.mLoanSanc_loanDist,
                                                TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                                    }

                                } else {

                                    TastyToast.makeText(getActivity(), AppStrings.mLoanSanc_loanDist,
                                            TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                                }
                            } else {

                                if (mLoan_Sanc_AmountValue.equals("0") && mLoan_Dist_AmountValue.equals("0")) {
                                    mLoan_Sanc_AmountEditText.setError(AppStrings.mCheckLoanSanctionAmount);
                                    mLoan_Dist_AmountEditText.setError(AppStrings.mCheckLoanDisbursementAmount);
                                    TastyToast.makeText(getActivity(), AppStrings.mCheckLoanAmounts,
                                            TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                                } else if (mLoan_Sanc_AmountValue.equals("0") && !mLoan_Dist_AmountValue.equals("0")) {
                                    mLoan_Sanc_AmountEditText.setError(AppStrings.mCheckLoanSanctionAmount);
                                    TastyToast.makeText(getActivity(), AppStrings.mCheckLoanAmounts,
                                            TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                                } else if (!mLoan_Sanc_AmountValue.equals("0") && mLoan_Dist_AmountValue.equals("0")) {
                                    mLoan_Dist_AmountEditText.setError(AppStrings.mCheckLoanDisbursementAmount);
                                    TastyToast.makeText(getActivity(), AppStrings.mCheckLoanAmounts,
                                            TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                                }

                            }
                        } else {
                            TastyToast.makeText(getActivity(), AppStrings.mCheckLoanSancDate_LoanDisbDate,
                                    TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                        }

                    } else {

                        TastyToast.makeText(getActivity(), AppStrings.nullDetailsAlert, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);
                    }
                }


                break;

            case R.id.fragment_Edit:

                mRaised_SubmitButton.setClickable(true);
                confirmationDialog.dismiss();

                break;
            case R.id.frag_Ok:

                confirmationDialog.dismiss();
                LD_IL_Mem_Disburse frag = new LD_IL_Mem_Disburse();
                NewDrawerScreen.showFragment(frag);

                break;

        }

    }


    private void showConfirmationDialog(String[] mConfirm_text, String[] mConfirmation_values) {
        // TODO Auto-generated method stub

        confirmationDialog = new Dialog(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_new_confirmation, null);
        dialogView.setLayoutParams(
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
        confirmationHeader.setText(AppStrings.confirmation);
        confirmationHeader.setTypeface(LoginActivity.sTypeface);
        TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

        mSize = mConfirm_BankLoan.length;

        for (int i = 0; i < mSize; i++) {

            TableRow indv_Row = new TableRow(getActivity());
            @SuppressWarnings("deprecation")
            TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            contentParams.setMargins(10, 5, 10, 5);

            TextView bankName_Text = new TextView(getActivity());
            bankName_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(mConfirm_text[i])));
            bankName_Text.setTextColor(R.color.white);
            bankName_Text.setPadding(5, 5, 5, 5);
            bankName_Text.setLayoutParams(contentParams);
            indv_Row.addView(bankName_Text);

            TextView confirm_values = new TextView(getActivity());
            confirm_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(mConfirmation_values[i])));
            confirm_values.setTextColor(R.color.white);
            confirm_values.setPadding(5, 5, 5, 5);
            confirm_values.setGravity(Gravity.RIGHT);
            confirm_values.setLayoutParams(contentParams);
            indv_Row.addView(confirm_values);

            confirmationTable.addView(indv_Row,
                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            if (mConfirmation_values[i].equals("Bank")) {

                if (selectedType.equals("Bank")) {

                    TableRow increaseLimitRow1 = new TableRow(getActivity());

                    TextView increaseLimitText1 = new TextView(getActivity());
                    increaseLimitText1
                            .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.bankName)));
                    increaseLimitText1.setTypeface(LoginActivity.sTypeface);
                    increaseLimitText1.setTextColor(R.color.white);
                    increaseLimitText1.setPadding(5, 5, 5, 5);
                    increaseLimitText1.setLayoutParams(contentParams);
                    increaseLimitRow1.addView(increaseLimitText1);

                    TextView increaseLimit_values1 = new TextView(getActivity());
                    increaseLimit_values1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sSelectedSavingbank.getBankName())));
                    increaseLimit_values1.setTextColor(R.color.white);
                    increaseLimit_values1.setPadding(5, 5, 5, 5);
                    increaseLimit_values1.setGravity(Gravity.RIGHT);
                    increaseLimit_values1.setLayoutParams(contentParams);
                    increaseLimitRow1.addView(increaseLimit_values1);

                    confirmationTable.addView(increaseLimitRow1,
                            new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                }
            }
        }

        mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit);
        mEdit_RaisedButton.setText(AppStrings.edit);
        mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
        mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
        // 205,
        // 0));
        mEdit_RaisedButton.setOnClickListener(this);

        mOk_RaisedButton = (Button) dialogView.findViewById(R.id.frag_Ok);
        mOk_RaisedButton.setText(AppStrings.yes);
        mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
        mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
        mOk_RaisedButton.setOnClickListener(this);

        confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmationDialog.setCanceledOnTouchOutside(false);
        confirmationDialog.setContentView(dialogView);
        confirmationDialog.setCancelable(true);
        confirmationDialog.show();

        ViewGroup.MarginLayoutParams margin = (ViewGroup.MarginLayoutParams) dialogView.getLayoutParams();
        margin.leftMargin = 10;
        margin.rightMargin = 10;
        margin.topMargin = 10;
        margin.bottomMargin = 10;
        margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);

    }


    private void callDisbursementdate() throws ParseException {

        Calendar now = Calendar.getInstance();
        final DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {

                Calendar selectionCal = Calendar.getInstance();
                selectionCal.set(year, month, day);
                String formattedDate = df.format(selectionCal.getTime());
                try {
                    disbursementDate = df.parse(formattedDate);
                    mLoanDistDateTxt.setText(formattedDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        }, now.get(Calendar.YEAR), now.get(Calendar.DATE), now.get(Calendar.DAY_OF_MONTH));

        Calendar min_Cal = Calendar.getInstance();
        Calendar lastDate = Calendar.getInstance();

        if (sanctionDate != null) {
            Calendar fcal = Calendar.getInstance();
            fcal.setTimeInMillis(sanctionDate.getTime());
            int fyear = fcal.get(Calendar.YEAR); // this is deprecated
            int fmonth = fcal.get(Calendar.MONTH); // this is deprecated
            int fday = fcal.get(Calendar.DATE);

            if (shgDto != null)
                shgTrMillis = shgDto.getLastTransactionDate();

            Date cDate = new Date(Long.parseLong(shgTrMillis));
            String formattedDate = df.format(cDate.getTime());
            currentDate = df.parse(formattedDate);
            Calendar ccal1 = Calendar.getInstance();
            ccal1.setTimeInMillis(currentDate.getTime());
            int cyear = ccal1.get(Calendar.YEAR); // this is deprecated
            int cmonth = ccal1.get(Calendar.MONTH); // this is deprecated
            int cday = ccal1.get(Calendar.DATE);
            lastDate.set(cyear, cmonth, cday);
            min_Cal.set(fyear, fmonth, fday);

            datePickerDialog.getDatePicker().setMinDate(min_Cal.getTimeInMillis());
            datePickerDialog.getDatePicker().setMaxDate(lastDate.getTimeInMillis());
            datePickerDialog.show();
        } else {
            Toast.makeText(getActivity(), "Select a SanctionDate", Toast.LENGTH_LONG).show();
        }

    }

    private void callSanctionDate() throws ParseException {

        if (shgDto != null)
            shgformationMillis = "315513000000";
        //  Date opDate = new Date(Long.parseLong(shgformationMillis));
        Date opDate = new Date(Long.parseLong(shgDto.getOpeningDate()));
        String formattedDate1 = df.format(opDate.getTime());
        openinigDate = df.parse(formattedDate1);
        Calendar now = Calendar.getInstance();
        final DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {

                Calendar selectionCal = Calendar.getInstance();
                selectionCal.set(year, month, day);
                String formattedDate = df.format(selectionCal.getTime());
                try {
                    sanctionDate = df.parse(formattedDate);
                    mLoanSancDateTxt.setText(formattedDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        }, now.get(Calendar.YEAR), now.get(Calendar.DATE), now.get(Calendar.DAY_OF_MONTH));
        Calendar min_Cal = Calendar.getInstance();
        Calendar lastDate = Calendar.getInstance();

        Calendar fcal = Calendar.getInstance();
        fcal.setTimeInMillis(openinigDate.getTime());
        int fyear = fcal.get(Calendar.YEAR); // this is deprecated
        int fmonth = fcal.get(Calendar.MONTH); // this is deprecated
        int fday = fcal.get(Calendar.DATE);

        if (shgDto != null)
            shgTrMillis = shgDto.getLastTransactionDate();

        Date cDate = new Date(Long.parseLong(shgTrMillis));
        String formattedDate = df.format(cDate.getTime());
        currentDate = df.parse(formattedDate);
        Calendar ccal1 = Calendar.getInstance();
        ccal1.setTimeInMillis(currentDate.getTime());
        int cyear = ccal1.get(Calendar.YEAR); // this is deprecated
        int cmonth = ccal1.get(Calendar.MONTH); // this is deprecated
        int cday = ccal1.get(Calendar.DATE);
        lastDate.set(cyear, cmonth, cday);

        min_Cal.set(fyear, fmonth, fday + 1);
        datePickerDialog.getDatePicker().setMinDate(min_Cal.getTimeInMillis());
        datePickerDialog.getDatePicker().setMaxDate(lastDate.getTimeInMillis());
        datePickerDialog.show();
    }


    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        if (mProgressDilaog != null) {
            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                mProgressDilaog.dismiss();
                mProgressDilaog = null;
            }
        }


        switch (serviceType) {


            case LD_BRANCH_DETAILS:
                try {
                    if (internalBranchList != null && internalBranchList.size() > 0) {
                        internalBranchList.clear();
                    }
                    ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    String message = cdto.getMessage();
                    int statusCode = cdto.getStatusCode();
                    if (statusCode == Utils.Success_Code) {
                        Utils.showToast(getActivity(), message);

                        for (int i = 0; i < cdto.getResponseContent().getBranchNameList().size(); i++) {
                            LoanBankDto loanDto = cdto.getResponseContent().getBranchNameList().get(i);
                            internalBranchList.add(loanDto);
                        }

                        LoanBankDto[] stockArr = new LoanBankDto[internalBranchList.size()];
                        mBankBranchArray = internalBranchList.toArray(stockArr);
                        mBankBranchArr = new String[]{AppStrings.bankBranch};


                        branchNameArr = new String[mBankBranchArray.length + 1];
                        branchNameArr[0] = String.valueOf("" + AppStrings.bankBranch);

                        for (int i = 0; i < mBankBranchArray.length; i++) {
                            branchNameArr[i + 1] = internalBranchList.get(i).getName().toString();
                            Log.i("Bank Name", i + "     " + branchNameArr[i + 1].toString());
                        }

                        bankBranchItems = new ArrayList<RowItem>();
                        for (int i = 0; i < branchNameArr.length; i++) {
                            RowItem rowItem = new RowItem(branchNameArr[i]);
                            bankBranchItems.add(rowItem);
                        }

                        mBankBranchAdapter = new CustomItemAdapter(getActivity(), bankBranchItems);
                        spn_label_Bankbranch.setAdapter(mBankBranchAdapter);

                    } else {
                        if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                        }
                        Utils.showToast(getActivity(), message);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

        }

    }


}
