package com.oasys.eshakti.digitization.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.model.ListItem;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class TransactionDetailsAdapter extends RecyclerView.Adapter<TransactionDetailsAdapter.TransactionDetails> {


    private Context context;
    private ArrayList<ListItem> listItems;

    public TransactionDetailsAdapter(Context context, ArrayList<ListItem> listItems) {
        this.context = context;
        this.listItems = listItems;

    }

    @NonNull
    @Override
    public TransactionDetails onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.walletbalancehistory, parent, false);
        return new TransactionDetails(v);
    }

    @Override
    public void onBindViewHolder(@NonNull TransactionDetails holder, final
    int position) {

        DateFormat simple = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat df1 = new SimpleDateFormat("HH:mm:ss");
        Date d = new Date(Long.parseLong(listItems.get(position).getTransactionDate()));
        String time = df1.format(new Date(Long.parseLong(listItems.get(position).getTransactionDate())));
        String dateStr = simple.format(d);
        holder.wRecharge_date.setText("DATE :"+dateStr+" "+time);
        holder.wRecharge_Transaction_id.setText("TRANSACTION ID :"+listItems.get(position).getTransactionId());
        String boldamt = AppStrings.amount + "<b>" + listItems.get(position).getAmount() + "</b> ";
        holder.wAmount.setText((Html.fromHtml(boldamt)));
        holder.wRecharge_Transaction_type.setText("TRANSACTION TYPE :"+listItems.get(position).getTransactionType());
   //     holder.wRecharge_TransactioStatus.setText("STATUS:"+listItems.get(position).getTransactionStatus());

        String first = AppStrings.txStatus;
        if (listItems.get(position).getTransactionStatus().toLowerCase().equals("success")) {
            String next = "<font color='#2f9e44'>" + listItems.get(position).getTransactionStatus() + "</font>";
            holder.wRecharge_TransactioStatus.setText(Html.fromHtml(first + next));
        } else {
            String next = "<font color='#EE0000'>" + listItems.get(position).getTransactionStatus() + "</font>";
            holder.wRecharge_TransactioStatus.setText(Html.fromHtml(first + next));
        }

    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }


    class TransactionDetails extends  RecyclerView.ViewHolder
    {

        CardView cardView;
        TextView wRecharge_date, wRecharge_Transaction_id, wAmount,wRecharge_Transaction_type,wRecharge_TransactioStatus,wTransaction_type,wRemarks;

        public TransactionDetails(@NonNull View itemView) {
            super(itemView);

            cardView= (CardView)itemView.findViewById(R.id.card_view);
            wRecharge_date = (TextView) itemView.findViewById(R.id.digitization_wallet_date);
            wRecharge_Transaction_id = (TextView) itemView.findViewById(R.id.digitization_wallet_transaction_id);
            wAmount = (TextView) itemView.findViewById(R.id.digitization_wallet_previous_balance);
            wRecharge_Transaction_type = (TextView) itemView.findViewById(R.id.digitization_wallet_amount);
            wRecharge_TransactioStatus = (TextView) itemView.findViewById(R.id.digitization_wallet_update_balance);
            wTransaction_type = (TextView) itemView.findViewById(R.id.digitization_wallet_transaction_type);
            wTransaction_type.setVisibility(View.GONE);
            wRemarks = (TextView) itemView.findViewById(R.id.digitization_wallet_remarks);
            wRemarks.setVisibility(View.GONE);

        }
    }
}
