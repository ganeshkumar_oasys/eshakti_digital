package com.oasys.eshakti.digitization.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.InputType;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.GetExit;
import com.oasys.eshakti.digitization.OasysUtils.GetSpanText;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.R.color;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.database.SHGTable;
import com.oasys.eshakti.digitization.views.ButtonFlat;
import com.oasys.eshakti.digitization.views.CustomHorizontalScrollView;
import com.oasys.eshakti.digitization.views.Get_EdiText_Filter;
import com.oasys.eshakti.digitization.views.RaisedButton;
import com.tutorialsee.lib.TastyToast;


import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import static com.oasys.eshakti.digitization.activity.NewDrawerScreen.shgDetails;

public class EditOpeningBalanceBankDetailsActivity extends AppCompatActivity implements OnClickListener, NewTaskListener {

    public static final String TAG = EditOpeningBalanceBankDetailsActivity.class.getSimpleName();
    private TextView mGroupName, mHeaderTextView;
    private TableLayout mCashInHandTable;
    private RaisedButton mSubmit_RaisedButton;

    List<EditText> sCashAtBankFields;
    List<EditText> sFixedDepositFields;
    List<EditText> sCashInHandFields;
    List<EditText> sEmptyFields;

    private EditText mCashAtBank_values, mFixedDeposit_Values, mCashInHand_Values;
    int mSize;

    private String[] cashInHandArr = {AppStrings.mCashInHand, AppStrings.mShgSeedFund, AppStrings.mFixedAssets};
    String[] mEditMasterValues;
    String[] mEditBank_CashinHand;
    String[] mEditBankDisplayValues;
    Vector<String> mBankName;
    Vector<String> mBankRegName;
    Vector<String> mBankAmount;
    Vector<String> mBankFixedAmount;
    String mCashinHand, mShgseedfund, mFixedAssets;

    String sEditCashAtbankAmounts[], sEditFixedDepositAmount[], sEditCashInHandAmount[];
    public static String sEditSendToServer_CashAtBank = "", sEditSendToServer_FixedDeposit = "";
    public static int sEditSavings_Total, sEditVSavings_Total;
    private Button mEdit_RaisedButton, mOk_RaisedButton;
    String[] editConfirmArr;
    String nullVlaue = "0";
    Dialog confirmationDialog;
    String mLanguageLocalae;
    private Dialog mProgressDialog;
    boolean isGetGroupDetails = false;

    int headerColumnWidth[], contentColumnWidth[];

    int mCount_Service = 0;
    private TableLayout mLeftHeaderTable, mRightHeaderTable, mLeftContentTable, mRightContentTable;
    private CustomHorizontalScrollView mHSRightHeader, mHSRightContent, mHSLeftHeader, mHSLeftContent;

    String width[] = {AppStrings.mCashInHand, AppStrings.mShgSeedFund, AppStrings.mFixedAssets};
    int[] rightHeaderWidth = new int[width.length];
    int[] rightContentWidth = new int[width.length];
    boolean mIsNegativeValues = false;

    boolean mIsSelectGroupTask = false;
    boolean mIsNavigateMainDashBoard = false;
    boolean isService = false;
    private NetworkConnection networkConnection;
    private Toolbar mToolbar;
    private TextView mTitle;

    public EditOpeningBalanceBankDetailsActivity() {
        // TODO Auto-generated constructor stub
    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();

        sEditCashAtbankAmounts = null;
        sEditFixedDepositAmount = null;
        sEditCashInHandAmount = null;
        editConfirmArr = null;

    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_new_ob_bankdetails);

        EShaktiApplication.setEditFinancialGroupId(null);
        mBankName = new Vector<String>();
        mBankRegName = new Vector<String>();
        mBankAmount = new Vector<String>();
        mBankFixedAmount = new Vector<String>();

        mBankName.clear();
        mBankRegName.clear();
        mBankAmount.clear();
        mBankFixedAmount.clear();

        sCashAtBankFields = new ArrayList<EditText>();
        sFixedDepositFields = new ArrayList<EditText>();
        sCashInHandFields = new ArrayList<EditText>();
        sEmptyFields = new ArrayList<EditText>();

        try {
            networkConnection = NetworkConnection.getNetworkConnection(getApplicationContext());
            shgDetails = SHGTable.getSHGDetails(MySharedPreference.readString(this, MySharedPreference.SHG_ID, ""));

            mToolbar = (Toolbar) findViewById(R.id.toolbar_grouplist);
            mTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("");
            mTitle.setText("ESHAKTI");
            mTitle.setGravity(Gravity.CENTER);

            mGroupName = (TextView) findViewById(R.id.groupname_edit_bank_details);
            mGroupName.setText(String.valueOf(""));
            mGroupName.setText(shgDetails.getName() + " / " + shgDetails.getPresidentName());

            mHeaderTextView = (TextView) findViewById(R.id.fragmentHeader_edit_bank_details);
            mHeaderTextView.setText(AppStrings.bankName+" "+AppStrings.details);

            mCashInHandTable = (TableLayout) findViewById(R.id.contentTableLayout_edit_cashinhand_details);


            if (EShaktiApplication.vertficationDto.getShgBankList() != null && EShaktiApplication.vertficationDto.getShgBankList().size() > 0) {

                for (int i = 0; i < EShaktiApplication.vertficationDto.getShgBankList().size(); i++) {


                    mBankName.addElement((EShaktiApplication.vertficationDto.getShgBankList().get(i).getBankName() != null) ? EShaktiApplication.vertficationDto.getShgBankList().get(i).getBankName() : "NA");
                    mBankRegName.addElement((EShaktiApplication.vertficationDto.getShgBankList().get(i).getBankName() != null) ? EShaktiApplication.vertficationDto.getShgBankList().get(i).getBankName() : "NA");
                    mBankAmount.addElement((EShaktiApplication.vertficationDto.getShgBankList().get(i).getCashAtBankAmount() != null) ? EShaktiApplication.vertficationDto.getShgBankList().get(i).getCashAtBankAmount() : "0.0");
                    mBankFixedAmount.addElement((EShaktiApplication.vertficationDto.getShgBankList().get(i).getFixedDeposit() != null) ? EShaktiApplication.vertficationDto.getShgBankList().get(i).getFixedDeposit() : "0.0");

                }
                /* if (mEditBank_CashinHand.length > 1) { */

                mCashinHand = (EShaktiApplication.vertficationDto.getShgBalanceDetailsDTO().getShgCashInHand() != null) ? EShaktiApplication.vertficationDto.getShgBalanceDetailsDTO().getShgCashInHand() : "0.0";
                mShgseedfund = (EShaktiApplication.vertficationDto.getShgBalanceDetailsDTO().getShgSeedFund() != null) ? EShaktiApplication.vertficationDto.getShgBalanceDetailsDTO().getShgSeedFund() : "0.0";
                mFixedAssets = (EShaktiApplication.vertficationDto.getShgBalanceDetailsDTO().getAssetsAndLiabilitiesDTO() != null && EShaktiApplication.vertficationDto.getShgBalanceDetailsDTO().getAssetsAndLiabilitiesDTO().getTransaction() != null && EShaktiApplication.vertficationDto.getShgBalanceDetailsDTO().getAssetsAndLiabilitiesDTO().getTransaction().getFixedAssets().getAmount() != null) ? EShaktiApplication.vertficationDto.getShgBalanceDetailsDTO().getAssetsAndLiabilitiesDTO().getTransaction().getFixedAssets().getAmount() : "0.0";
                ;
                /*
                 * }else{ mCashinHand = "0"; mShgseedfund = "0"; mFixedAssets = "0"; }
                 */
            }

            mSubmit_RaisedButton = (RaisedButton) findViewById(R.id.fragment_Submit_button_edit_bank_details);
            mSubmit_RaisedButton.setText(AppStrings.mConfirm);
            mSubmit_RaisedButton.setOnClickListener(this);

            buildTableContentValues();
            buildTableCashInHand();

            Log.e("ewewweew", mBankName.size() + "");
            Log.e("ewewweew", mBankRegName.size() + "");
            Log.e("ewewweew", mBankAmount.size() + "");
            Log.e("ewewweew", mBankFixedAmount.size() + "");

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

    }

    @SuppressWarnings("deprecation")
    private void buildTableContentValues() {
        // TODO Auto-generated method stub
        mSize = mBankRegName.size();
        Log.d(TAG, String.valueOf(mSize));

        mLeftHeaderTable = (TableLayout) findViewById(R.id.LeftHeaderTable);
        mRightHeaderTable = (TableLayout) findViewById(R.id.RightHeaderTable);
        mLeftContentTable = (TableLayout) findViewById(R.id.LeftContentTable);
        mRightContentTable = (TableLayout) findViewById(R.id.RightContentTable);

        mHSLeftHeader = (CustomHorizontalScrollView) findViewById(R.id.leftHeaderHScrollView);
        mHSLeftContent = (CustomHorizontalScrollView) findViewById(R.id.leftContentHScrollView);

        mHSLeftHeader.setOnScrollChangedListener(new CustomHorizontalScrollView.onScrollChangedListener() {

            @Override
            public void onScrollChanged(int l, int t, int oldl, int oldt) {
                // TODO Auto-generated method stub
                mHSLeftContent.scrollTo(l, 0);
            }
        });

        mHSLeftContent.setOnScrollChangedListener(new CustomHorizontalScrollView.onScrollChangedListener() {

            @Override
            public void onScrollChanged(int l, int t, int oldl, int oldt) {
                // TODO Auto-generated method stub
                mHSLeftHeader.scrollTo(l, 0);
            }
        });

        mHSRightHeader = (CustomHorizontalScrollView) findViewById(R.id.rightHeaderHScrollView);
        mHSRightContent = (CustomHorizontalScrollView) findViewById(R.id.rightContentHScrollView);

        mHSRightHeader.setOnScrollChangedListener(new CustomHorizontalScrollView.onScrollChangedListener() {

            public void onScrollChanged(int l, int t, int oldl, int oldt) {
                // TODO Auto-generated method stub

                mHSRightContent.scrollTo(l, 0);

            }
        });

        mHSRightContent.setOnScrollChangedListener(new CustomHorizontalScrollView.onScrollChangedListener() {
            @Override
            public void onScrollChanged(int l, int t, int oldl, int oldt) {
                // TODO Auto-generated method stub
                mHSRightHeader.scrollTo(l, 0);
            }
        });

        TableRow leftHeaderRow = new TableRow(this);

        TableRow.LayoutParams lHeaderParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT, 1f);

        TextView mMemberName_headerText = new TextView(this);
        mMemberName_headerText.setText(String.valueOf(AppStrings.bankName));
        mMemberName_headerText.setTextColor(Color.WHITE);
        mMemberName_headerText.setPadding(20, 5, 10, 5);
        mMemberName_headerText.setLayoutParams(lHeaderParams);
        leftHeaderRow.addView(mMemberName_headerText);

        mLeftHeaderTable.addView(leftHeaderRow);

        TableRow rightHeaderRow = new TableRow(this);
        TableRow.LayoutParams rHeaderParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT, 1f);
        rHeaderParams.setMargins(10, 0, 10, 0);
        TextView mSavingsAmount_HeaderText = new TextView(this);
        mSavingsAmount_HeaderText
                .setText(String.valueOf(AppStrings.mCashAtBank));
        mSavingsAmount_HeaderText.setTextColor(Color.WHITE);
        mSavingsAmount_HeaderText.setPadding(5, 5, 5, 5);
        mSavingsAmount_HeaderText.setLayoutParams(rHeaderParams);
        mSavingsAmount_HeaderText.setGravity(Gravity.CENTER);
        mSavingsAmount_HeaderText.setSingleLine(true);
        rightHeaderRow.addView(mSavingsAmount_HeaderText);

        TextView mVSavingsAmount_HeaderText = new TextView(this);
        mVSavingsAmount_HeaderText
                .setText(String.valueOf(AppStrings.fixedDeposit));// (AppStrings.mFixedAssets)));
        mVSavingsAmount_HeaderText.setTextColor(Color.WHITE);
        mVSavingsAmount_HeaderText.setPadding(25, 5, 5, 5);
        // mVSavingsAmount_HeaderText.setGravity(Gravity.CENTER);
        mVSavingsAmount_HeaderText.setLayoutParams(rHeaderParams);
        mVSavingsAmount_HeaderText.setSingleLine(true);
        rightHeaderRow.addView(mVSavingsAmount_HeaderText);

        mRightHeaderTable.addView(rightHeaderRow);

        getTableRowHeaderCellWidth();
        for (int i = 0; i < mSize; i++) {
            TableRow leftContentRow = new TableRow(this);

            TableRow.LayoutParams leftContentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT, 60, 1f);
            leftContentParams.setMargins(5, 5, 5, 5);

            TextView memberName_Text = new TextView(this);
            memberName_Text.setText(GetSpanText.getSpanString(this,
                    String.valueOf(GetSpanText.getSpanString(this, String.valueOf(mBankRegName.elementAt(i))))));
            memberName_Text.setTextColor(color.black);
            memberName_Text.setPadding(15, 5, 5, 5);
            memberName_Text.setLayoutParams(leftContentParams);
            leftContentRow.addView(memberName_Text);

            mLeftContentTable.addView(leftContentRow);

            TableRow rightContentRow = new TableRow(this);

            TableRow.LayoutParams rightContentParams = new TableRow.LayoutParams(rightHeaderWidth[1],
                    LayoutParams.WRAP_CONTENT, 1f);
            rightContentParams.setMargins(20, 5, 20, 5);

            mCashAtBank_values = new EditText(this);

            mCashAtBank_values.setId(i);
            sCashAtBankFields.add(mCashAtBank_values);
            mCashAtBank_values.setPadding(5, 5, 5, 5);
            mCashAtBank_values.setBackgroundResource(R.drawable.edittext_background);
            mCashAtBank_values.setLayoutParams(rightContentParams);
            mCashAtBank_values.setTextAppearance(this, R.style.MyMaterialTheme);
            mCashAtBank_values.setFilters(Get_EdiText_Filter.editText_filter());
            mCashAtBank_values.setInputType(InputType.TYPE_CLASS_NUMBER);
            mCashAtBank_values.setTextColor(color.black);
            // mSavings_values.setWidth(150);
            mCashAtBank_values.setText(mBankAmount.elementAt(i));
            mCashAtBank_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    // TODO Auto-generated method stub
                    if (hasFocus) {
                        ((EditText) v).setGravity(Gravity.LEFT);
                    } else {
                        ((EditText) v).setGravity(Gravity.RIGHT);
                    }

                }
            });
            rightContentRow.addView(mCashAtBank_values);

            mFixedDeposit_Values = new EditText(this);
            mFixedDeposit_Values.setId(i);
            sFixedDepositFields.add(mFixedDeposit_Values);
            mFixedDeposit_Values.setPadding(5, 5, 5, 5);
            mFixedDeposit_Values.setBackgroundResource(R.drawable.edittext_background);
            mFixedDeposit_Values.setLayoutParams(rightContentParams);
            mFixedDeposit_Values.setTextAppearance(this, R.style.MyMaterialTheme);
            mFixedDeposit_Values.setFilters(Get_EdiText_Filter.editText_filter());
            mFixedDeposit_Values.setInputType(InputType.TYPE_CLASS_NUMBER);
            mFixedDeposit_Values.setTextColor(color.black);
            // mVSavings_values.setWidth(150);
            mFixedDeposit_Values.setText(mBankFixedAmount.elementAt(i));
            mFixedDeposit_Values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    // TODO Auto-generated method stub
                    if (hasFocus) {
                        ((EditText) v).setGravity(Gravity.LEFT);
                    } else {
                        ((EditText) v).setGravity(Gravity.RIGHT);
                    }
                }
            });
            rightContentRow.addView(mFixedDeposit_Values);

            mRightContentTable.addView(rightContentRow);

        }

        resizeMemberNameWidth();
        // resizeRightSideTable();

        resizeBodyTableRowHeight();

    }

    @SuppressWarnings("deprecation")
    private void buildTableCashInHand() {
        // TODO Auto-generated method stub

        mSize = 3;
        Log.d(TAG, String.valueOf(mSize));

        for (int i = 0; i < mSize; i++) {
            TableRow leftContentRow = new TableRow(this);

            TableRow.LayoutParams leftContentParams = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
                    LayoutParams.WRAP_CONTENT, 1f);
            leftContentParams.setMargins(5, 5, 5, 5);

            TextView cashInHand_Text = new TextView(this);
            cashInHand_Text.setText(GetSpanText.getSpanString(this, String.valueOf(cashInHandArr[i])));
            cashInHand_Text.setTextColor(color.black);
            cashInHand_Text.setPadding(15, 5, 5, 5);
            cashInHand_Text.setLayoutParams(leftContentParams);
            leftContentRow.addView(cashInHand_Text);

            TableRow.LayoutParams leftContentParams1 = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,
                    LayoutParams.WRAP_CONTENT, 1f);
            leftContentParams1.setMargins(5, 5, 80, 5);

            mCashInHand_Values = new EditText(this);

            mCashInHand_Values.setId(i);
            sCashInHandFields.add(mCashInHand_Values);
            mCashInHand_Values.setPadding(5, 5, 5, 5);
            mCashInHand_Values.setBackgroundResource(R.drawable.edittext_background);
            mCashInHand_Values.setLayoutParams(leftContentParams1);
            mCashInHand_Values.setTextAppearance(this, R.style.MyMaterialTheme);
            mCashInHand_Values.setFilters(Get_EdiText_Filter.editText_filter());
            mCashInHand_Values.setInputType(InputType.TYPE_CLASS_NUMBER);
            mCashInHand_Values.setTextColor(color.black);
            mCashInHand_Values.setWidth(150);
            if (i == 0) {
                mCashInHand_Values.setText(mCashinHand);
            } else if (i == 1) {
                mCashInHand_Values.setText(mShgseedfund);
            } else if (i == 2) {
                mCashInHand_Values.setText(mFixedAssets);
            }

            mCashInHand_Values.setGravity(Gravity.RIGHT);
            leftContentRow.addView(mCashInHand_Values);

            mCashInHandTable.addView(leftContentRow);

        }

    }

    @SuppressWarnings("deprecation")
    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub

        sEditCashAtbankAmounts = new String[mBankRegName.size()];
        sEditFixedDepositAmount = new String[mBankRegName.size()];
        sEditCashInHandAmount = new String[3];

        Log.e("------099009090909990>", mBankRegName.size() + "");
        switch (v.getId()) {
            case R.id.fragment_Submit_button_edit_bank_details:

                try {

                    sEditSavings_Total = 0;
                    sEditVSavings_Total = 0;

                    sEditSendToServer_CashAtBank = "";
                    sEditSendToServer_FixedDeposit = "";
                    // Do edit values here

                    StringBuilder builder = new StringBuilder();
                    Log.e("------>", mBankRegName.size() + "");
                    for (int i = 0; i < mBankRegName.size(); i++) {

                        sEditCashAtbankAmounts[i] = String.valueOf(sCashAtBankFields.get(i).getText());
                        sEditFixedDepositAmount[i] = String.valueOf(sFixedDepositFields.get(i).getText());

                        if ((sEditCashAtbankAmounts[i].equals("")) || (sEditCashAtbankAmounts[i] == null)) {
                            sEditCashAtbankAmounts[i] = nullVlaue;
                        }

                        if ((sEditFixedDepositAmount[i].equals("")) || (sEditFixedDepositAmount[i] == null)) {
                            sEditFixedDepositAmount[i] = nullVlaue;
                        }
                        double cashAtBankAmount = 0;
                        double fixedDepositAmount = 0;
                        if (sEditCashAtbankAmounts[i].matches("\\d*\\.?\\d+")) { // match
                            // a
                            // decimal
                            // number

                            cashAtBankAmount = Double.parseDouble(sEditCashAtbankAmounts[i]);
                            sEditCashAtbankAmounts[i] = String.valueOf(cashAtBankAmount);
                        }


                        if (sEditFixedDepositAmount[i].matches("\\d*\\.?\\d+")) { // match
                            // a
                            // decimal
                            // number

                            fixedDepositAmount = Double.parseDouble(sEditFixedDepositAmount[i]);
                            sEditFixedDepositAmount[i] = String.valueOf(fixedDepositAmount);
                        }

                        if (cashAtBankAmount < 0) {
                            mIsNegativeValues = true;
                        }
                        if (fixedDepositAmount < 0) {
                            mIsNegativeValues = true;
                        }

                        EShaktiApplication.vertficationDto.getShgBankList().get(i).setCashAtBankAmount(sEditCashAtbankAmounts[i]);
                        EShaktiApplication.vertficationDto.getShgBankList().get(i).setFixedDeposit(sEditFixedDepositAmount[i]);

                        sEditSendToServer_CashAtBank = sEditSendToServer_CashAtBank + mBankName.elementAt(i) + "~"
                                + mBankRegName.elementAt(i) + "~" + sCashAtBankFields.get(i).getText() + "~"
                                + sFixedDepositFields.get(i).getText() + "%";

                        builder.append(sEditCashAtbankAmounts[i]).append(",");

                    }

                    sEditSendToServer_CashAtBank = sEditSendToServer_CashAtBank + "#" + sCashInHandFields.get(0).getText()
                            + "#" + sCashInHandFields.get(1).getText() + "#" + sCashInHandFields.get(2).getText();
                    for (int i = 0; i < sCashInHandFields.size(); i++) {

                        sEditCashInHandAmount[i] = String.valueOf(sCashInHandFields.get(i).getText());

                        if (sEditCashInHandAmount[i].matches("\\d*\\.?\\d+")) { // match

                            int cashInHandAmount = (int) Math.round(Double.parseDouble(sEditCashInHandAmount[i]));
                            sEditCashInHandAmount[i] = String.valueOf(cashInHandAmount);

                            if (i == 0) {
                                EShaktiApplication.vertficationDto.getShgBalanceDetailsDTO().setShgCashInHand(sEditCashInHandAmount[i]);
                            } else if (i == 1) {
                                EShaktiApplication.vertficationDto.getShgBalanceDetailsDTO().setShgSeedFund(sEditCashInHandAmount[i]);
                            } else if (i == 2) {
                                if (EShaktiApplication.vertficationDto.getShgBalanceDetailsDTO().getAssetsAndLiabilitiesDTO() != null)
                                    EShaktiApplication.vertficationDto.getShgBalanceDetailsDTO().getAssetsAndLiabilitiesDTO().getTransaction().getFixedAssets().setAmount(sEditCashInHandAmount[i]);
                            }
                        }

                    }

                    Log.d(TAG, sEditSendToServer_CashAtBank);

                    Log.d(TAG, sEditSendToServer_CashAtBank);

                    Log.d(TAG, "TOTAL " + String.valueOf(sEditSavings_Total));

                    Log.d(TAG, "VS TOTAL " + String.valueOf(sEditVSavings_Total));


                    if ((int) Double.parseDouble(mShgseedfund) < 0) {
                        mIsNegativeValues = true;
                    }
                    if ((int) Double.parseDouble(mFixedAssets) < 0) {
                        mIsNegativeValues = true;
                    }
                    if ((int) Double.parseDouble(mCashinHand) < 0) {
                        mIsNegativeValues = true;
                    }


                    // Do the SP insertion
                    if (!mIsNegativeValues) {

                        confirmationDialog = new Dialog(this);

                        LayoutInflater inflater = this.getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.dialog_new_confirmation, null);

                        LayoutParams lParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
                        dialogView.setLayoutParams(lParams);

                        TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
                        confirmationHeader.setText(AppStrings.confirmation);

                        TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);
                        Log.e("Bank Size", mBankRegName.size() + "");
                        for (int i = 0; i < mBankRegName.size(); i++) {
                            Log.e("Bank Size", mBankRegName.size() + "");
                            Log.e("Bank Namessssssssss", mBankRegName.elementAt(i));

                            TableRow indv_SavingsRow = new TableRow(this);

                            TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
                                    LayoutParams.WRAP_CONTENT, 1f);
                            contentParams.setMargins(10, 5, 10, 5);

                            TextView memberName_Text = new TextView(this);
                            memberName_Text
                                    .setText(GetSpanText.getSpanString(this, String.valueOf(mBankRegName.elementAt(i))));
                            memberName_Text.setTextColor(color.black);
                            memberName_Text.setPadding(5, 5, 5, 5);
                            memberName_Text.setSingleLine(true);
                            memberName_Text.setLayoutParams(contentParams);
                            indv_SavingsRow.addView(memberName_Text);

                            TextView confirm_values = new TextView(this);
                            confirm_values
                                    .setText(GetSpanText.getSpanString(this, String.valueOf(sEditCashAtbankAmounts[i])));
                            confirm_values.setTextColor(color.black);
                            confirm_values.setPadding(10, 5, 10, 5);
                            confirm_values.setGravity(Gravity.RIGHT);
                            confirm_values.setLayoutParams(contentParams);
                            indv_SavingsRow.addView(confirm_values);

                            TextView confirm_VSvalues = new TextView(this);
                            confirm_VSvalues
                                    .setText(GetSpanText.getSpanString(this, String.valueOf(sEditFixedDepositAmount[i])));
                            confirm_VSvalues.setTextColor(color.black);
                            confirm_VSvalues.setPadding(10, 5, 10, 5);
                            confirm_VSvalues.setGravity(Gravity.RIGHT);
                            confirm_VSvalues.setLayoutParams(contentParams);
                            indv_SavingsRow.addView(confirm_VSvalues);

                            confirmationTable.addView(indv_SavingsRow,
                                    new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

                        }

                        for (int i = 0; i < 3; i++) {

                            TableRow indv_SavingsRow = new TableRow(this);

                            TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
                                    LayoutParams.WRAP_CONTENT, 1f);
                            contentParams.setMargins(10, 5, 10, 5);

                            TextView memberName_Text = new TextView(this);
                            memberName_Text.setText(GetSpanText.getSpanString(this, String.valueOf(cashInHandArr[i])));
                            memberName_Text.setTextColor(color.black);
                            memberName_Text.setPadding(5, 5, 5, 5);
                            memberName_Text.setSingleLine(true);
                            memberName_Text.setLayoutParams(contentParams);
                            indv_SavingsRow.addView(memberName_Text);

                            TextView confirm_values = new TextView(this);
                            confirm_values
                                    .setText(GetSpanText.getSpanString(this, String.valueOf(sEditCashInHandAmount[i])));
                            confirm_values.setTextColor(color.black);
                            confirm_values.setPadding(5, 5, 5, 5);
                            confirm_values.setGravity(Gravity.RIGHT);
                            confirm_values.setLayoutParams(contentParams);
                            indv_SavingsRow.addView(confirm_values);

                            confirmationTable.addView(indv_SavingsRow,
                                    new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

                        }

                        mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit);
                        mEdit_RaisedButton.setText(AppStrings.edit);
                        mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
                        // 205,
                        // 0));
                        mEdit_RaisedButton.setOnClickListener(this);

                        mOk_RaisedButton = (Button) dialogView.findViewById(R.id.frag_Ok);
                        mOk_RaisedButton.setText(AppStrings.mVerified);
                        mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
                        mOk_RaisedButton.setOnClickListener(this);

                        confirmationDialog.getWindow()
                                .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        confirmationDialog.setCanceledOnTouchOutside(false);
                        confirmationDialog.setContentView(dialogView);
                        confirmationDialog.setCancelable(true);
                        confirmationDialog.show();

                        MarginLayoutParams margin = (MarginLayoutParams) dialogView.getLayoutParams();
                        margin.leftMargin = 10;
                        margin.rightMargin = 10;
                        margin.topMargin = 10;
                        margin.bottomMargin = 10;
                        margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);
                    } else {

                        TastyToast.makeText(getApplicationContext(), AppStrings.mIsNegativeOpeningBalance,
                                TastyToast.LENGTH_SHORT, TastyToast.WARNING);

                        sEditSendToServer_CashAtBank = "0";

                        mCashinHand = "0";
                        mShgseedfund = "0";

                        mFixedAssets = "0";

                        mIsNegativeValues = false;

                        Intent intent_ = new Intent(EditOpeningBalanceBankDetailsActivity.this, SHGGroupActivity.class);
                        intent_.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent_.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent_);
                        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);
                        finish();


                    }

                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }

                break;

            case R.id.fragment_Edit:

                sEditSendToServer_CashAtBank = "0";
                sEditSavings_Total = Integer.valueOf(nullVlaue);
                sEditSendToServer_CashAtBank = "0";
                sEditVSavings_Total = Integer.valueOf(nullVlaue);
                mSubmit_RaisedButton.setClickable(true);

                confirmationDialog.dismiss();
                break;

            case R.id.frag_Ok:

                confirmationDialog.dismiss();
                mIsNegativeValues = false;

                String sreqString = new Gson().toJson(EShaktiApplication.vertficationDto);
                if (networkConnection.isNetworkAvailable()) {
                    onTaskStarted();
                    RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.VERIFY_UPDATE, sreqString, this, ServiceType.VERIFY_UPDATE);
                }
                break;
        }

    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_edit_ob, menu);
        MenuItem item = menu.getItem(0);
        item.setVisible(true);
        MenuItem logOutItem = menu.getItem(1);
        logOutItem.setVisible(true);

        SpannableStringBuilder SS = new SpannableStringBuilder(AppStrings.groupList);
        SpannableStringBuilder logOutBuilder = new SpannableStringBuilder(AppStrings.logOut);

        if (item.getItemId() == R.id.action_grouplist_edit) {

            item.setTitle(SS);

        }

        if (logOutItem.getItemId() == R.id.menu_logout_edit) {

            logOutItem.setTitle(logOutBuilder);
        }


        return true;

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_grouplist_edit) {

            try {
                startActivity(new Intent(EditOpeningBalanceBankDetailsActivity.this, SHGGroupActivity.class));
                finish();
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
/*                if (ConnectionUtils.isNetworkAvailable(getApplicationContext())) {
                    PrefUtils.setLoginGroupService("2");
                    new Login_webserviceTask(MainActivity.this).execute();
                } else {
                    startActivity(new Intent(this, SHGGroupActivity.class));
                    overridePendingTransition(R.anim.right_to_left_in, R.anim.right_to_left_out);
                    finish();
                }*/
            return true;

        } else if (id == R.id.menu_logout_edit) {
            Log.e(" Logout", "Logout Sucessfully");
            startActivity(new Intent(GetExit.getExitIntent(getApplicationContext())));
            this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTaskStarted() {
        // TODO Auto-generated method stub

        mProgressDialog = AppDialogUtils.createProgressDialog(this);
        mProgressDialog.show();

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        if (mProgressDialog != null) {
            if ((mProgressDialog != null) && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
                mProgressDialog = null;
                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();
            }
            switch (serviceType) {
                case VERIFY_UPDATE:
                    try {
                        ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                        String message = cdto.getMessage();
                        int statusCode = cdto.getStatusCode();
                        if (statusCode == Utils.Success_Code) {
                            Utils.showToast(this, message);
                            showGIFImageDialog();
                        } else {
                            if (statusCode == 401) {

                                Log.e("Group Logout", "Logout Sucessfully");
                                AppDialogUtils.showConfirmation_LogoutDialog(EditOpeningBalanceBankDetailsActivity.this);

                            }
                            Utils.showToast(this, message);
                          /*  Intent intent_ = new Intent(EditOpeningBalanceBankDetailsActivity.this, SHGGroupActivity.class);
                            intent_.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent_.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent_);
                            overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);
                            finish();*/

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }

        }

    }



    private void showGIFImageDialog() {
        // TODO Auto-generated method stub
        final Dialog dialog = new Dialog(this);
        LayoutInflater li = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View dialogView = li.inflate(R.layout.dialog_new_gif_image, null, false);

        WebView image = (WebView) dialogView.findViewById(R.id.dialogImageView);
        image.loadUrl("file:///android_asset/check1.gif");

        ButtonFlat doneButton = (ButtonFlat) dialogView.findViewById(R.id.dialog_Yes_button);
        doneButton.setText(AppStrings.mProceed);
        doneButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                dialog.dismiss();
                Intent intent_ = new Intent(EditOpeningBalanceBankDetailsActivity.this, NewDrawerScreen.class);
                intent_.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent_.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent_);
                overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);
                finish();

            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(dialogView);
        dialog.setCancelable(false);
        dialog.show();

    }


	/*private void showGIFImageDialog(String values) {
		// TODO Auto-generated method stub
		final Dialog dialog = new Dialog(this);
		LayoutInflater li = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final View dialogView = li.inflate(R.layout.dialog_gif_image, null, false);

		WebView image = (WebView) dialogView.findViewById(R.id.dialogImageView);
		image.loadUrl("file:///android_asset/check1.gif");

		ButtonFlat doneButton = (ButtonFlat) dialogView.findViewById(R.id.dialog_Yes_button);
		doneButton.setText(AppStrings.mProceed));
		doneButton.setTypeface(LoginActivity.sTypeface);
		doneButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				dialog.dismiss();
				EShaktiApplication.setEditOpeningScreen(false);
				Intent intent_nav = new Intent(EditOpeningBalanceBankDetailsActivity.this, MainActivity.class);
				intent_nav.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent_nav.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent_nav);
				overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);

				finish();

			}
		});

		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setCanceledOnTouchOutside(false);
		dialog.setContentView(dialogView);
		dialog.setCancelable(false);
		dialog.show();

	}*/

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        // unregisterReceiver(receiver);
    }

    private void getTableRowHeaderCellWidth() {

        int lefHeaderChildCount = ((TableRow) mLeftHeaderTable.getChildAt(0)).getChildCount();
        int rightHeaderChildCount = ((TableRow) mRightHeaderTable.getChildAt(0)).getChildCount();

        for (int x = 0; x < (lefHeaderChildCount + rightHeaderChildCount); x++) {

            if (x == 0) {
                rightHeaderWidth[x] = viewWidth(((TableRow) mLeftHeaderTable.getChildAt(0)).getChildAt(x));
            } else {
                rightHeaderWidth[x] = viewWidth(((TableRow) mRightHeaderTable.getChildAt(0)).getChildAt(x - 1));
            }

        }
    }

    private void resizeMemberNameWidth() {
        // TODO Auto-generated method stub
        int leftHeadertWidth = viewWidth(mLeftHeaderTable);
        int leftContentWidth = viewWidth(mLeftContentTable);

        if (leftHeadertWidth < leftContentWidth) {
            mLeftHeaderTable.getLayoutParams().width = leftContentWidth;
        } else {
            mLeftContentTable.getLayoutParams().width = leftHeadertWidth;
        }
    }

    private void resizeBodyTableRowHeight() {

        int leftContentTable_ChildCount = mLeftContentTable.getChildCount();

        for (int x = 0; x < leftContentTable_ChildCount; x++) {

            TableRow leftContentTableRow = (TableRow) mLeftContentTable.getChildAt(x);
            TableRow rightContentTableRow = (TableRow) mRightContentTable.getChildAt(x);

            int rowLeftHeight = viewHeight(leftContentTableRow);
            int rowRightHeight = viewHeight(rightContentTableRow);

            TableRow tableRow = rowLeftHeight < rowRightHeight ? leftContentTableRow : rightContentTableRow;
            int finalHeight = rowLeftHeight > rowRightHeight ? rowLeftHeight : rowRightHeight;

            this.matchLayoutHeight(tableRow, finalHeight);
        }

    }

    private void matchLayoutHeight(TableRow tableRow, int height) {

        int tableRowChildCount = tableRow.getChildCount();

        // if a TableRow has only 1 child
        if (tableRow.getChildCount() == 1) {

            View view = tableRow.getChildAt(0);
            TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();
            params.height = height - (params.bottomMargin + params.topMargin);

            return;
        }

        // if a TableRow has more than 1 child
        for (int x = 0; x < tableRowChildCount; x++) {

            View view = tableRow.getChildAt(x);

            TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();

            if (!isTheHeighestLayout(tableRow, x)) {
                params.height = height - (params.bottomMargin + params.topMargin);
                return;
            }
        }

    }

    // check if the view has the highest height in a TableRow
    private boolean isTheHeighestLayout(TableRow tableRow, int layoutPosition) {

        int tableRowChildCount = tableRow.getChildCount();
        int heighestViewPosition = -1;
        int viewHeight = 0;

        for (int x = 0; x < tableRowChildCount; x++) {
            View view = tableRow.getChildAt(x);
            int height = this.viewHeight(view);

            if (viewHeight < height) {
                heighestViewPosition = x;
                viewHeight = height;
            }
        }

        return heighestViewPosition == layoutPosition;
    }

    // read a view's height
    private int viewHeight(View view) {
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        return view.getMeasuredHeight();
    }

    // read a view's width
    private int viewWidth(View view) {
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        return view.getMeasuredWidth();
    }

}
