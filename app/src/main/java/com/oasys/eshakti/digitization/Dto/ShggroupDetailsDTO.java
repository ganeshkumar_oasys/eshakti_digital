package com.oasys.eshakti.digitization.Dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class ShggroupDetailsDTO  implements Serializable {

    private String contactNumber;

    private String presidentName;

    private String secretaryName;

    private String blockName;

    private String transactionDate;

    private String shGType;

    private String shgName;

    private String panchayatName;

    private String shgCode;

    private String treasureName;

    private String totalNumbers;

    private String villageName;

    private String shgCreationDate;

    private String groupId;

}
