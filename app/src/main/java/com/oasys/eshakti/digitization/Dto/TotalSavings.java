package com.oasys.eshakti.digitization.Dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class TotalSavings implements Serializable {

    private String savings;
}
