package com.oasys.eshakti.digitization.fragment;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasys.eshakti.digitization.Adapter.MemberreportAdapter;
import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.Dto.MemberList;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.RecyclerItemClickListener;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.activity.LoginActivity;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;
import com.oasys.eshakti.digitization.database.SHGTable;

import java.util.ArrayList;
import java.util.List;

public class MemberViewphotoFragment extends Fragment {

    private RecyclerView recyclerViewMemberReport;
    private LinearLayoutManager linearLayoutManager;
    private static FragmentManager fm;
    private List<MemberList> memList;
    private ListOfShg shgDto;
    private TextView mGroupName;
    private TextView mCashinHand;
    private TextView mHeader, mCashatBank;
    private String getmem_id;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_member_report, container, false);
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        recyclerViewMemberReport = (RecyclerView) view.findViewById(R.id.recyclerViewMemberReport);
        mGroupName = (TextView) view.findViewById(R.id.groupname);
        mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
        mGroupName.setTypeface(LoginActivity.sTypeface);

        mCashinHand = (TextView) view.findViewById(R.id.ch);
        mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
        mCashinHand.setTypeface(LoginActivity.sTypeface);

        mCashatBank = (TextView) view.findViewById(R.id.cb);
        mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
        mCashatBank.setTypeface(LoginActivity.sTypeface);

        mHeader = (TextView) view.findViewById(R.id.fragmentHeader);
        mHeader.setText(AppStrings.Memberreports);
        mHeader.setTypeface(LoginActivity.sTypeface);


        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerViewMemberReport.setLayoutManager(linearLayoutManager);
        recyclerViewMemberReport.setHasFixedSize(true);


        Bundle bundle = getArguments();
        getmem_id = bundle.getString("memid");
        Log.d("Mem", getmem_id);


        memList = new ArrayList<>();
        MemberList ml1 = new MemberList();
        ml1.setMemberName(AppStrings.viewPhoto);
        memList.add(ml1);

        MemberreportAdapter memberreportAdapter = new MemberreportAdapter(getActivity(), memList);
        recyclerViewMemberReport.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerViewMemberReport.setAdapter(memberreportAdapter);

        fm = getActivity().getSupportFragmentManager();

        recyclerViewMemberReport.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Profile_Mem_View_Image_Fragment reportLoanMenu = new Profile_Mem_View_Image_Fragment();
                Bundle bundle = new Bundle();
                bundle.putString("memid", getmem_id);
                Log.d("Mem1", getmem_id);
                reportLoanMenu.setArguments(bundle);
                NewDrawerScreen.showFragment(reportLoanMenu);
            }
        }));


        return view;
    }


}
