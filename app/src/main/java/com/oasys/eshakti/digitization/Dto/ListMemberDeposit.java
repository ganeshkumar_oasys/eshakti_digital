package com.oasys.eshakti.digitization.Dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class ListMemberDeposit implements Serializable {

    private String amount;

    private String memberId;
}
