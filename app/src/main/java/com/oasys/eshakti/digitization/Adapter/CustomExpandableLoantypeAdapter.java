package com.oasys.eshakti.digitization.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.oasys.eshakti.digitization.Dto.ExistingLoan;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.ExpandListItemClickListener;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.activity.LoginActivity;
import com.oasys.eshakti.digitization.model.Loantype;
import com.oasys.eshakti.digitization.views.RaisedButton;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

/**
 * Created by MuthukumarPandi on 12/17/2018.
 */

public class CustomExpandableLoantypeAdapter extends BaseExpandableListAdapter {

    private final ArrayList<ExistingLoan> loanTypes;
    Context context;
    List<Loantype> _listDataHeader;
    private ArrayList<HashMap<String, String>> listDataChild;
    private ExpandListItemClickListener mCallBack;
    boolean flag = false;
    Vector<String> mGroupId;
    Vector<String> mGroupValues;
    TextView listTitle;

    public CustomExpandableLoantypeAdapter(Context context, ArrayList<ExistingLoan> loanTypedto, List<Loantype> listDataHeader,
                                           ArrayList<HashMap<String, String>> listChildData, ExpandListItemClickListener callback) {
        // TODO Auto-generated constructor stub
        this.context = context;
        this._listDataHeader = listDataHeader;
        this.listDataChild = listChildData;
        this.loanTypes = loanTypedto;
        this.mCallBack = callback;
    }

    @Override
    public int getGroupCount() {
        // TODO Auto-generated method stub
        return this._listDataHeader.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        // TODO Auto-generated method stub
        return 1;
    }


    @Override
    public Object getGroup(int groupPosition) {
        // TODO Auto-generated method stub
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public HashMap<String, String> getChild(int groupPosition, int childPosition) {
        // TODO Auto-generated method stub
        return this.listDataChild.get(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        // TODO Auto-generated method stub
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        // TODO Auto-generated method stub
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        try {

            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.view_new_row, null);
            }

            // ImageView listLeftImage = (ImageView) convertView.findViewById(R.id.dynamicImage_left);
            listTitle = (TextView) convertView.findViewById(R.id.dynamicText);
           listTitle.setTypeface(LoginActivity.sTypeface);
            ImageView listImage = (ImageView) convertView.findViewById(R.id.dynamicImage);

            //  listLeftImage.setVisibility(View.VISIBLE);

            Loantype group_listItem = (Loantype) getGroup(groupPosition);
            // LoansList dto = this.loanTypes.get(groupPosition);

            listTitle.setText(String.valueOf(group_listItem.getTitle()));
            // listTitle.setTypeface(LoginActivity.sTypeface);
            listTitle.setSelected(true);


            //  listLeftImage.setVisibility(View.INVISIBLE);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, int childPosition, boolean isLastChild, View convertView,
                             final ViewGroup parent) {
        // TODO Auto-generated method stub
        HashMap<String, String> childMenu = getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.memberloanrepay_menuitem, null);
        }

        RaisedButton button = (RaisedButton) convertView.findViewById(R.id.mlr_menulist_next_button);
        TextView maccountnumber = (TextView) convertView.findViewById(R.id.mlr_accNumber);
        TextView maccountnumber_Values = (TextView) convertView.findViewById(R.id.mlr_AccNumber_values);
        TextView mBankname = (TextView) convertView.findViewById(R.id.mlr_bankName);
        TextView mBanknameValues = (TextView) convertView.findViewById(R.id.mlr_bankName_values);
        TextView mdisbursement = (TextView) convertView.findViewById(R.id.mlr_disbursementTime);
        TextView mdisbursementValues = (TextView) convertView.findViewById(R.id.mlr_disbursementTime_values);

       button.setTypeface(LoginActivity.sTypeface);
        maccountnumber.setTypeface(LoginActivity.sTypeface);
   //   maccountnumber_Values.setTypeface(LoginActivity.sTypeface);
        mBankname.setTypeface(LoginActivity.sTypeface);
        mBanknameValues.setTypeface(LoginActivity.sTypeface);
        mdisbursement.setTypeface(LoginActivity.sTypeface);
        //mdisbursementValues.setTypeface(LoginActivity.sTypeface);

       /* mShgName.setTextColor(Color.BLACK);
        mShgName_Values.setTextColor(Color.BLACK);
        mBlockname.setTextColor(Color.BLACK);
        mBlockNameValues.setTextColor(Color.BLACK);

        mPanchayatName.setTextColor(Color.BLACK);
        mPanchayatNameValues.setTextColor(Color.BLACK);
        mVillagename.setTextColor(Color.BLACK);
        mVillagenameValues.setTextColor(Color.BLACK);

        mTransactionDate.setTextColor(Color.BLACK);
        mTransactionDateValues.setTextColor(Color.BLACK);*/
        if (listTitle.getText().toString().equals("Internal loan")) {
            maccountnumber.setVisibility(View.GONE);
            maccountnumber_Values.setVisibility(View.GONE);
            mBankname.setVisibility(View.GONE);
            mBanknameValues.setVisibility(View.GONE);
            mdisbursement.setVisibility(View.GONE);
            mdisbursementValues.setVisibility(View.GONE);
        } else {
            maccountnumber.setVisibility(View.VISIBLE);
            maccountnumber_Values.setVisibility(View.VISIBLE);
            mBankname.setVisibility(View.VISIBLE);
            mBanknameValues.setVisibility(View.VISIBLE);
            mdisbursement.setVisibility(View.VISIBLE);
            mdisbursementValues.setVisibility(View.VISIBLE);

        }
        button.setText("SELECT");

        maccountnumber.setText(childMenu.get("accountNumber_Label") + "  :  ");
        maccountnumber_Values.setText(childMenu.get(AppStrings.mAccountNumber));
        mBankname.setText(childMenu.get("bankName_Label") + "  :  ");
        mBanknameValues.setText(childMenu.get(AppStrings.bankName));
        mdisbursement.setText(childMenu.get("disbursement_label") + "  :  ");


        if (listTitle.getText().toString().equals("Internal loan")) {

        } else {
            DateFormat simple = new SimpleDateFormat("dd/MM/yyyy");
            Date d = new Date(Long.parseLong(childMenu.get(AppStrings.Loan_Disbursement_Date)));
            String dateStr = simple.format(d);
            mdisbursementValues.setText(dateStr);
        }


        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mCallBack.onItemClick(parent, v, groupPosition);
            }
        });
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        // TODO Auto-generated method stub
        return false;
    }

}
