package com.oasys.eshakti.digitization.Dto;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.Data;

@Data
public class ResponseContent implements Serializable {
    private String meetingId;

    private String name,accessToken,expiresInMilliseconds,refreshToken,ImageByteCode;

    private String userId;

    private ArrayList<ListOfShg> listOfShg;

    private ArrayList<CashOfGroup> cashOfGroup;
    private CashOfGroup savingsBalance;

    private ArrayList<MemberList> members;
    private ArrayList<Wallet_Bank_CW> bankNames;

    private ArrayList<ShgInternalLoanListDTO> shgInternalLoanListDTO;
    private ListCountLoan listCountLoan;
    private ArrayList<GroupfinancialDetails> groupfinancialDetails;

    private ShggroupDetailsDTO shggroupDetailsDTO;

    private ArrayList<ShgBankList> shgBankList;

    private ShgBalanceDetailsDTO shgBalanceDetailsDTO;

    private ArrayList<MemberfinancialDetails> memberfinancialDetails;


    private ArrayList<Transactiontype> transactionType;
    private ArrayList<EnquiryDetails> enquirydetails;

    private String animatorId;
    private String animatorName, fixedDepositBalance;


    private String disbursementAmount, outStanding, memberDisbursementAmount;
    private String balanceAmount, balance;
    private String sansactionAmount, sanctionAmount, bankCharges;

    private AnimatorProfile animatorProfile;

    private ShgProfile shgProfile;

    private ArrayList<GroupSavingsSummary> groupSavingsSummary;
    private ArrayList<ExistingLoan> settingsList;

    // private String totalSavings;
    private ArrayList<ExpensesTypeDtoList> expensesTypeDTOList;
    private ArrayList<LoanDisbursementDto> loanSettingList, financeBanksList;
    private ArrayList<LoanDisbursementDto> InstallmentTypeList;

    private ArrayList<LoanBankDto> bankNamesList, branchNameList;

    /*  private String totalSavings;*/

    private String totalVoluntarySaving;

    private String[] yearList;

    // private ArrayList<MonthlyReport> monthlyReport;

    private ArrayList<ExistingLoan> loansList;

    private ArrayList<LoanDto> purposeOfloanTypeList, newLoanTypes;

    private ArrayList<LoanDto> purposeOfInternalLoanTypes;
    private CheckListDto CheckList;
    private ArrayList<CheckListDto> CheckListDto;


    private ArrayList<LoanDto> loanTypes;

    private ArrayList<TotalSavings> totalSavings;

    private String[] loansummary;

    private ArrayList<MemberList> memberloanRepaymentList;

    private ArrayList<ShgBankDetails> shgBankDetails;

    private ArrayList<IncomeType> incomeType;

    private ArrayList<BankBalanceDTOList> bankBalanceDTOList;

    private ArrayList<GroupBankTransactionSummaryList> groupBankTransactionSummaryList;

    private ArrayList<ShgBankDTOList> shgBankDTOList;

    // private ArrayList<LoansList> loansList;

    //private ArrayList<MemberloanRepaymentList> memberloanRepaymentList;
    private ArrayList<SavingsAccounts> savingsAccounts;

    private ArrayList<MinutesOfMeeting> minutesOfMeeting;
    private ArrayList<PurposeOfLoans> purposeOfLoans;


    private ArrayList<MemberInternalLoan> memberInternalLoan;


    private ArrayList<GroupInternalLoansList> groupInternalLoansList;


    private String loanOutstanding;

    private ArrayList<TotalSavingsAmount> totalSavingsAmount;
    private ArrayList<InternalLoan> memberinternallaon;
    private ArrayList<MonthlyReport> membermonthlyReport;
    private ArrayList<LoanSummary> memberreportloansummary;
    private String otherloanOutstanding;
    private ArrayList<MemberLoanSummary> memberLoanSummary;
    private String totalGrouploan, TotalAmount, Total, totalMemberSavings, totalMemberSavingsSummary, totalMemberSavingsDisbursment;
    private ArrayList<MemberSavingsSummary> memberSavingsSummary, memberSavingsDisbursment;
    private String grouptotalSavings;
    private String merchantId, vpaNo, transactionId, walletId,agentId, walletAmount, masterWalletAmount, reservedWalletAmount, virtualWalletAmount;
    private GroupLoanSummary groupLoanSummary;
    private ArrayList<TrainingsList> TrainingsList, languageList;
    private String auditDate;


//    Digitization

    private String cashInHand;

    private String currentCashInHand;

    private String cashOutHand;

    private String refNo;

    private String updateCashInHand;


    private ArrayList<VpaList> vpaList;

    private String statusDescription;


//    private String name;

    private String yesBankTransaction;

    private String virtualAddress;

    private String status;


//    cashless

    private String amount,rrn,uid,date,stan,statusCode,txnAmount,retailerTxnId,terminalId,uidaiauthenticationCode;

    private String payerAccountNo;

    private String custRefID;

    private String errorCode;

    private String payerMobileNo;

    private String approvalNumber;

//    private String referenceID;

    private String referenceId;


    private String responseCode;

//    private String statusDescription;

    private String payeeVirtualAddress;

    private String transactionNote;

    private String payerIfsc;

    private String yesbankTransactionReferenceNumber;

    private String merchantTransactionId;

    private String npciUpiTrnxId;

    private String payerVirtualAddress;

//    private String status;


}
