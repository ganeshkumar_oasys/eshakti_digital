package com.oasys.eshakti.digitization.fragment;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.oasys.eshakti.digitization.Adapter.Digitization_deposit_Member_Details_Adapter;
import com.oasys.eshakti.digitization.Dto.ResponseContents;
import com.oasys.eshakti.digitization.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class Digitization_Deposit_Details extends Fragment {


    private RecyclerView recyclerview_digitization_depositdetails;
    private View view;
    private LinearLayoutManager linearLayoutManager;
    private Digitization_deposit_Member_Details_Adapter digitization_deposit_member_details_adapter;
    public ArrayList<ResponseContents> responseContentsList;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_digitization__deposit__details, container, false);
        responseContentsList = new ArrayList<>();
        init();
        return  view;
    }

    private void init() {


        recyclerview_digitization_depositdetails = (RecyclerView) view.findViewById(R.id.recyclerView_deposit_details);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerview_digitization_depositdetails.setLayoutManager(linearLayoutManager);
        recyclerview_digitization_depositdetails.setHasFixedSize(true);
        recyclerview_digitization_depositdetails.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayout.VERTICAL));
    }

}
