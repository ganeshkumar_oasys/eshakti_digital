package com.oasys.eshakti.digitization.database;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.oasys.eshakti.digitization.Dto.MemberList;
import com.oasys.eshakti.digitization.EShaktiApplication;

import java.util.ArrayList;
import java.util.List;

public class MemberTable {

    private static SQLiteDatabase database;
    private static DbHelper dataHelper;
    static Cursor cursor;

    public MemberTable(Activity activity) {
        dataHelper = new DbHelper(activity);
    }

    public static void openDatabase() {
        dataHelper = new DbHelper(EShaktiApplication.getInstance());
        dataHelper.onOpen(database);
        database = dataHelper.getWritableDatabase();
    }

    public static void closeDatabase() {
        if (database != null && database.isOpen()) {
            database.close();
        }
    }

    public static void insertMemberData(MemberList memDto) {

        if (memDto != null) {
            try {
                openDatabase();
                ContentValues values = new ContentValues();
                values.put(TableConstants.SHG_ID, (memDto.getShgId() != null && memDto.getShgId().length() > 0) ? memDto.getShgId() : "");
                values.put(TableConstants.MEMBER_ID, (memDto.getMemberId() != null && memDto.getMemberId().length() > 0) ? memDto.getMemberId() : "");
                values.put(TableConstants.MEMBER_NAME, (memDto.getMemberName() != null && memDto.getMemberName().length() > 0) ? memDto.getMemberName() : "");
                values.put(TableConstants.PHONE_NO, (memDto.getPhoneNumber() != null && memDto.getPhoneNumber().length() > 0) ? memDto.getPhoneNumber() : "");
                values.put(TableConstants.MEMBER_USER_ID, (memDto.getMemberUserId() != null && memDto.getMemberUserId().length() > 0) ? memDto.getMemberUserId() : "");
                values.put(TableConstants.BANK_ID, (memDto.getBankId() != null && memDto.getBankId().length() > 0) ? memDto.getBankId() : "");
                values.put(TableConstants.BRANCHNAME_ID, (memDto.getBranchNameId() != null && memDto.getBranchNameId().length() > 0) ? memDto.getBranchNameId() : "");
                values.put(TableConstants.BANKNAME_ID, (memDto.getBankNameId() != null && memDto.getBankNameId().length() > 0) ? memDto.getBankNameId() : "");
                values.put(TableConstants.BANKNAME, (memDto.getBankName() != null && memDto.getBankName().length() > 0) ? memDto.getBankName() : "");
                values.put(TableConstants.BRANCHNAME, (memDto.getBranchName() != null && memDto.getBranchName().length() > 0) ? memDto.getBranchName() : "");
                values.put(TableConstants.ACCOUNT_NO, (memDto.getAccountNumber() != null && memDto.getAccountNumber().length() > 0) ? memDto.getAccountNumber() : "");
                database.insertWithOnConflict(TableName.TABLE_MEMBER_DETAILS, TableConstants.SHG_ID, values, SQLiteDatabase.CONFLICT_REPLACE);
            } catch (Exception e) {
                Log.e("insertEnergyException", e.toString());
            } finally {
                closeDatabase();
            }
        }

    }

    public static List<MemberList> getMemberList(String shg_id) {
        List<MemberList> memList = new ArrayList<>();
        if (shg_id.length() > 0) {
            try {
                openDatabase();
                String selectQuery = "SELECT * FROM " + TableName.TABLE_MEMBER_DETAILS + " where " + TableConstants.SHG_ID + " LIKE '" + shg_id + "'";
                Log.e("TABLE_SHG QUERY:", selectQuery);
                Cursor cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {

                    do {
                        MemberList memberList = new MemberList();
                        memberList.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                        memberList.setMemberId(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_ID)));
                        memberList.setMemberName(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_NAME)));
                        memberList.setPhoneNumber(cursor.getString(cursor.getColumnIndex(TableConstants.PHONE_NO)));
                        memberList.setBankName(cursor.getString(cursor.getColumnIndex(TableConstants.BANKNAME)));
                        memberList.setBranchName(cursor.getString(cursor.getColumnIndex(TableConstants.BRANCHNAME)));
                        memberList.setBranchNameId(cursor.getString(cursor.getColumnIndex(TableConstants.BRANCHNAME_ID)));
                        memberList.setAccountNumber(cursor.getString(cursor.getColumnIndex(TableConstants.ACCOUNT_NO)));

                        memberList.setBankId(cursor.getString(cursor.getColumnIndex(TableConstants.BANK_ID)));
                        memberList.setMemberUserId(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_USER_ID)));
                        if (memberList.getMemberName() != null && memberList.getMemberName().length() > 0)
                            memList.add(memberList);
                    } while (cursor.moveToNext());
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                closeDatabase();
            }
        }
        return memList;
    }

    public static MemberList getMember(String u_id) {
        MemberList memberList = new MemberList();
        if (u_id.length() > 0) {
            try {
                openDatabase();
                String selectQuery = "SELECT  * FROM " + TableName.TABLE_MEMBER_DETAILS + " where " + TableConstants.USERID + " LIKE '" + u_id + "'";
                Log.e("TABLE_SHG QUERY:", selectQuery);
                Cursor cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    do {
                        memberList.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                        memberList.setMemberId(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_ID)));
                        memberList.setMemberName(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_NAME)));
                        memberList.setPhoneNumber(cursor.getString(cursor.getColumnIndex(TableConstants.PHONE_NO)));
                        memberList.setMemberUserId(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_USER_ID)));

                    } while (cursor.moveToNext());
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                closeDatabase();
            }

        }
        return memberList;
    }

    public static void deleteMemberList(String userId) {
        try {
            openDatabase();
            database.execSQL("DELETE FROM " + TableName.TABLE_MEMBER_DETAILS);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

    }

}
