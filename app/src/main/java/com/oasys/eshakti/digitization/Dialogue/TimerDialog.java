package com.oasys.eshakti.digitization.Dialogue;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;


/**
 * This dialog will appear on the time of user logout
 */
public class TimerDialog extends Dialog implements View.OnClickListener {


    private final Activity context;  //    Context from the user

    /*Constructor class for this dialog*/
    public TimerDialog(Activity _context) {
        super(_context);
        context = _context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setContentView(R.layout.timer_ly);
        setCancelable(false);

        EditText editUpitimeout = ((EditText) findViewById(R.id.editUpitimeout));
        EditText editfttimeout = ((EditText) findViewById(R.id.editfttimeout));

        editfttimeout.setText("180");
        editfttimeout.setBackgroundResource(R.drawable.edittext_background);
        editUpitimeout.setText("180");
        editUpitimeout.setBackgroundResource(R.drawable.edittext_background);


//        editfttimeout.setText(MySharedPreference.readString(context, MySharedPreference.ftimeout, "180000"));
//        editfttimeout.setBackgroundResource(R.drawable.edittext_background);
//        editUpitimeout.setText(MySharedPreference.readString(context, MySharedPreference.upi_timeout, "180000"));
//        editUpitimeout.setBackgroundResource(R.drawable.edittext_background);

        TextView textViewTitle = (TextView) findViewById(R.id.textViewNwTitle);
        Button okButton = (Button) findViewById(R.id.buttonNwOk);
        okButton.setOnClickListener(this);
        Button cancelButton = (Button) findViewById(R.id.buttonNwCancel);
        cancelButton.setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonNwOk:
                if (storeInLocal()) {
                    //Constants.setBaseUrl(MySharedPreference.readString(context, MySharedPreference.ChangeUrl, "http://192.168.3.253:8631"));
                    dismiss();

                     /* Intent i = new Intent(context, LoginActivity.class);
                    context.startActivity(i);*/
                }else{
                    Utils.showToast(context,"");
                }
                break;
            case R.id.buttonNwCancel:
                dismiss();
                break;
        }
    }

    /**
     * Store changed ip in shared preference
     * returns true if value present else false
     */
    private boolean storeInLocal() {
        String time = ((EditText) findViewById(R.id.editfttimeout)).getText().toString().trim();

        if((Integer.parseInt(time)) >180)
        {
            long secondstomillisec_upi = Long.parseLong(time)*1000;
            MySharedPreference.writeLong(context, MySharedPreference.fttimeout, secondstomillisec_upi);
        }
        else
        {

            Toast.makeText(context,"Pls Enter above 180 seconds",Toast.LENGTH_SHORT).show();
        }

        String time1 = ((EditText) findViewById(R.id.editUpitimeout)).getText().toString().trim();

        if((Integer.parseInt(time1)) >180)
        {
            long secondstomillisec_ft = Integer.parseInt(time1)*1000;
            MySharedPreference.writeLong(context, MySharedPreference.upi_timeout, secondstomillisec_ft);
        }
        else
        {
            Toast.makeText(context,"Pls Enter above 180 seconds",Toast.LENGTH_SHORT).show();
        }



        return true;
    }


    /**
     * Tamil text textView typeface
     * input  textView name and text string input
     */
    public void setTamilText(TextView textName, String text) {

        textName.setText(text);
    }

    /**
     * Tamil text Button typeface
     * input  textView name and text string input
     */
    public void setTamilText(Button btn, String text) {
        btn.setText(text);
    }

    /**
     * Tamil text Button typeface
     * input  textView name and text string input
     */
    public void setTamilText(EditText editText, String text) {
        editText.setHint(text);
    }

}