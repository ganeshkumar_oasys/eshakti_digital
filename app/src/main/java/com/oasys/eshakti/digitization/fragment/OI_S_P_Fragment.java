package com.oasys.eshakti.digitization.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.Dto.MemberList;
import com.oasys.eshakti.digitization.Dto.ResponseContents;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.Dto.SavingRequest;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.GetSpanText;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.Service.Restclient_Timeout;
import com.oasys.eshakti.digitization.activity.LoginActivity;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;
import com.oasys.eshakti.digitization.database.MemberTable;
import com.oasys.eshakti.digitization.database.SHGTable;
import com.oasys.eshakti.digitization.views.CustomHorizontalScrollView;
import com.oasys.eshakti.digitization.views.Get_EdiText_Filter;
import com.oasys.eshakti.digitization.views.TextviewUtils;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class OI_S_P_Fragment extends Fragment implements View.OnClickListener, NewTaskListener {

    private View rootView;
    public static String TAG = OI_S_P_Fragment.class.getSimpleName();

    List<EditText> sIncomeFields = new ArrayList<EditText>();
    private static String sIncomeAmounts[];
    public static String sSendToServer_Income;

    private TextView mGroupName, mCashinHand, mCashatBank, mHeader, mAutoFill_label, mAutoFill_label1;
    private CheckBox mAutoFill, mAutoFill1;
    private Button mSubmit_Raised_Button;
    private Button mEdit_RaisedButton, mOk_RaisedButton;
    private EditText mIncome_values;
    private TableLayout mIncomeTable;
    private CustomHorizontalScrollView mHSRightHeader;
    private CustomHorizontalScrollView mHSRightContent;

    public ArrayList<ResponseContents> responseContentsList;

    private Button mPerviousButton;
    private Button mNextButton;

    private TableLayout mLeftHeaderTable;
    private int responsesize;
    private TableLayout mRightHeaderTable;
    private TableLayout mLeftContentTable;
    private TableLayout mRightContentTable;
    private CheckBox sCheckBox1;
    private static List<CheckBox> mCheckBoxFields;

    private Dialog mProgressDilaog;
    Dialog confirmationDialog;

    Boolean isValues;
    String nullVlaue = "0";
    String toBeEditArr[];

    String[] confirmArr;
    int mSize;
    public static int sIncome_Total = 0;
    public static Boolean isIncome = false;

    AlertDialog alertDialog;
    String mLastTrDate = null, mLastTr_ID = null;
    boolean isGetTrid = false;
    private LinearLayout mOthersLayout;
    private TextView mOthersTextView;
    private EditText mOthersEditText;
    private String mOthersAmount_Values;
    boolean isServiceCall = false;
    String mSqliteDBStoredValues_mOtherincomevalues = null, mSqliteDBStoredValues_mSubscriptionIncomevalues = null,
            mSqliteDBStoredValues_mPenaltyIncomevalues = null;

    LinearLayout mMemberNameLayout;
    TextView mMemberName;
    private List<MemberList> memList;
    private ListOfShg shgDto;
    private NetworkConnection networkConnection;
    private ArrayList<MemberList> arrMem;
    private TableLayout headerTable;
    private EditText mamount_values;
    private TextView memberName_Text;
    private String transaction;
    public static long timeout_saving;
    private TextView counterTextView;
    private TextView counterTimerStatusTxt;
    private CountDownTimer cndr;
    private static final String FORMAT_TIMER = "%02d:%02d";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_new_savings, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mSize = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, "")).size();
        memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        arrMem = new ArrayList<>();
        mCheckBoxFields = new ArrayList<>();

        Bundle bundle = getArguments();
        transaction = bundle.getString("trnsactiontype");
        init();
    }

    private void init() {
        try {
            responseContentsList = new ArrayList<>();
            mGroupName = (TextView) rootView.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
            mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashinHand.setTypeface(LoginActivity.sTypeface);

            mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
            mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashatBank.setTypeface(LoginActivity.sTypeface);

            mMemberNameLayout = (LinearLayout) rootView.findViewById(R.id.member_name_layout);
            mMemberName = (TextView) rootView.findViewById(R.id.member_name);
            //  mMemberName.setTypeface(LoginActivity.sTypeface);

            /** UI Mapping **/

            mHeader = (TextView) rootView.findViewById(R.id.fragmentHeader);
            mHeader.setText(Income.sSelectedIncomeMenu.getIncomeType());
            //    mHeader.setTypeface(LoginActivity.sTypeface);

            if (Income.sSelectedIncomeMenu.getIncomeType().toUpperCase().equals(AppStrings.subscriptioncharges) || Income.sSelectedIncomeMenu.getIncomeType().toUpperCase().equals(AppStrings.penalty)) {
                mAutoFill_label = (TextView) rootView.findViewById(R.id.autofillLabel);
                mAutoFill_label.setText(AppStrings.autoFill);
                mAutoFill_label.setTypeface(LoginActivity.sTypeface);
                mAutoFill_label.setVisibility(View.VISIBLE);

                mAutoFill = (CheckBox) rootView.findViewById(R.id.autoFill);
                mAutoFill.setVisibility(View.VISIBLE);
                mAutoFill.setOnClickListener(this);
            }/* else {
                mAutoFill_label = (TextView) rootView.findViewById(R.id.autofillLabel);
                mAutoFill_label.setText(AppStrings.autoFill);
                mAutoFill_label.setVisibility(View.GONE);

                mAutoFill = (CheckBox) rootView.findViewById(R.id.autoFill);
                mAutoFill.setVisibility(View.GONE);
                mAutoFill.setOnClickListener(this);

          *//*      mAutoFill_label1 = (TextView) rootView.findViewById(R.id.autofillLabel1);
                mAutoFill_label1.setText(AppStrings.autoFill);
                mAutoFill_label1.setVisibility(View.VISIBLE);

                mAutoFill1 = (CheckBox) rootView.findViewById(R.id.autoFill1);
                mAutoFill1.setVisibility(View.VISIBLE);
                mAutoFill1.setOnClickListener(this);*//*

            }*/

            Log.d(TAG, String.valueOf(mSize));

            mOthersLayout = (LinearLayout) rootView.findViewById(R.id.otherIncomeLayout);
            mOthersTextView = (TextView) rootView.findViewById(R.id.otherLoanTextView);
            mOthersTextView.setTypeface(LoginActivity.sTypeface);
            mOthersEditText = (EditText) rootView.findViewById(R.id.otherLoanEditText);
            mOthersTextView.setText(AppStrings.mOthers);

            mOthersEditText.setTextAppearance(getActivity(), R.style.MyMaterialTheme);
            mOthersEditText.setFilters(Get_EdiText_Filter.editText_filter());
            mOthersEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
            mOthersEditText.setTextColor(R.color.black);
            mOthersEditText.setPadding(5, 5, 5, 5);
            mOthersEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    // TODO Auto-generated method stub
                    if (hasFocus) {
                        ((EditText) v).setGravity(Gravity.LEFT);

                    } else {
                        ((EditText) v).setGravity(Gravity.RIGHT);

                    }
                }
            });

            if (Income.sSelectedIncomeMenu.getIncomeType().toUpperCase().equals(AppStrings.subscriptioncharges) || Income.sSelectedIncomeMenu.getIncomeType().toUpperCase().equals(AppStrings.penalty)) {
                mOthersLayout.setVisibility(View.GONE);
            } else {
                mOthersLayout.setVisibility(View.GONE);
            }

           /* headerTable = (TableLayout) rootView.findViewById(R.id.savingsTable);
            mIncomeTable = (TableLayout) rootView.findViewById(R.id.fragment_contentTable);
*/

            mLeftHeaderTable = (TableLayout) rootView.findViewById(R.id.mlr_LeftHeaderTable);
            mRightHeaderTable = (TableLayout) rootView.findViewById(R.id.mlr_RightHeaderTable);
            mLeftContentTable = (TableLayout) rootView.findViewById(R.id.mlr_LeftContentTable);
            mRightContentTable = (TableLayout) rootView.findViewById(R.id.mlr_RightContentTable);
            mHSRightHeader = (CustomHorizontalScrollView) rootView.findViewById(R.id.mlr_rightHeaderHScrollView);
            mHSRightContent = (CustomHorizontalScrollView) rootView.findViewById(R.id.mlr_rightContentHScrollView);
            mHSRightHeader.setOnScrollChangedListener(new CustomHorizontalScrollView.onScrollChangedListener() {

                public void onScrollChanged(int l, int t, int oldl, int oldt) {
                    // TODO Auto-generated method stub

                    mHSRightContent.scrollTo(l, 0);

                }
            });

            mHSRightContent.setOnScrollChangedListener(new CustomHorizontalScrollView.onScrollChangedListener() {
                @Override
                public void onScrollChanged(int l, int t, int oldl, int oldt) {
                    // TODO Auto-generated method stub
                    mHSRightHeader.scrollTo(l, 0);
                }
            });


      /*      TableRow savingsHeader = new TableRow(getActivity());
            savingsHeader.setBackgroundResource(R.color.tableHeader);

            ViewGroup.LayoutParams headerParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,
                    1f);

            TextView mMemberName_headerText = new TextView(getActivity());
            mMemberName_headerText
                    .setText(AppStrings.memberName);
            mMemberName_headerText.setTypeface(LoginActivity.sTypeface);
            mMemberName_headerText.setTextColor(Color.WHITE);
            mMemberName_headerText.setPadding(20, 5, 10, 5);
            mMemberName_headerText.setLayoutParams(headerParams);
            savingsHeader.addView(mMemberName_headerText);

            TextView mIncomeAmount_HeaderText = new TextView(getActivity());
            mIncomeAmount_HeaderText
                    .setText(AppStrings.amount);
            mIncomeAmount_HeaderText.setTypeface(LoginActivity.sTypeface);
            mIncomeAmount_HeaderText.setTextColor(Color.WHITE);
            mIncomeAmount_HeaderText.setPadding(50, 5, 50, 5);
            mIncomeAmount_HeaderText.setGravity(Gravity.RIGHT);
            mIncomeAmount_HeaderText.setLayoutParams(headerParams);
            mIncomeAmount_HeaderText.setBackgroundResource(R.color.tableHeader);
            savingsHeader.addView(mIncomeAmount_HeaderText);

            TextView cashMode_Text = new TextView(getActivity());
            cashMode_Text.setText("" + String.valueOf(AppStrings.cashmode));
            cashMode_Text.setTypeface(LoginActivity.sTypeface);
            cashMode_Text.setPadding(50, 5, 50, 5);
            cashMode_Text.setTextColor(Color.WHITE);
            cashMode_Text.setLayoutParams(headerParams);
            cashMode_Text.setBackgroundResource(R.color.tableHeader);
            mIncomeAmount_HeaderText.setGravity(Gravity.RIGHT);
            cashMode_Text.setSingleLine(false);
            savingsHeader.addView(cashMode_Text);

            headerTable.addView(savingsHeader,
                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            for (int i = 0; i < mSize; i++) {

                TableRow indv_IncomeRow = new TableRow(getActivity());

                TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,
                        60,1f);
                contentParams.setMargins(10, 5, 10, 5);

                final TextView memberName_Text = new TextView(getActivity());
                memberName_Text.setText(memList.get(i).getMemberName());
                memberName_Text.setTextColor(R.color.black);
                memberName_Text.setPadding(10, 0, 10, 5);
                memberName_Text.setLayoutParams(contentParams);
                memberName_Text.setSingleLine(true);
                memberName_Text.setEllipsize(TextUtils.TruncateAt.END);
                indv_IncomeRow.addView(memberName_Text);

                TableRow.LayoutParams contentEditParams = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,1f);
                contentEditParams.setMargins(30, 5, 10, 20);

                mIncome_values = new EditText(getActivity());
                mIncome_values.setId(i);
                sIncomeFields.add(mIncome_values);
                mIncome_values.setGravity(Gravity.END);
                mIncome_values.setTextColor(Color.BLACK);
                mIncome_values.setPadding(5, 5, 5, 5);
                mIncome_values.setBackgroundResource(R.drawable.edittext_background);
                mIncome_values.setLayoutParams(contentEditParams);// contentParams
                // lParams
                mIncome_values.setTextAppearance(getActivity(), R.style.MyMaterialTheme);
                mIncome_values.setFilters(Get_EdiText_Filter.editText_filter());
                mIncome_values.setInputType(InputType.TYPE_CLASS_NUMBER);
                mIncome_values.setTextColor(R.color.black);
                mIncome_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        // TODO Auto-generated method stub
                        if (hasFocus) {
                            ((EditText) v).setGravity(Gravity.LEFT);

                            mMemberNameLayout.setVisibility(View.VISIBLE);
                            mMemberName.setText(memberName_Text.getText().toString().trim());
                            TextviewUtils.manageBlinkEffect(mMemberName, getActivity());

                        } else {
                            ((EditText) v).setGravity(Gravity.RIGHT);

                            mMemberNameLayout.setVisibility(View.GONE);
                            mMemberName.setText("");
                        }

                    }
                });
                indv_IncomeRow.addView(mIncome_values);


                TableRow.LayoutParams contentParams3 = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,
                        LayoutParams.WRAP_CONTENT,0f);
                contentParams3.setMargins(30, 5, 30, 5);
                sCheckBox1 = new CheckBox(getActivity());
                sCheckBox1.setId(i);
                sCheckBox1.setWidth(50);
                sCheckBox1.setPadding(5, 5, 5, 5);
                mCheckBoxFields.add(sCheckBox1);
                sCheckBox1.setBackgroundResource(R.drawable.btn_check_to_off_mtrl_013);
                sCheckBox1.setClickable(true);
                final int position = i;
                sCheckBox1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if (compoundButton.isChecked()) {
                            mCheckBoxFields.get(position).setChecked(true);
                        } else {
                            mCheckBoxFields.get(position).setChecked(false);
                        }

                    }
                });
                sCheckBox1.setLayoutParams(contentParams3);
                sCheckBox1.setGravity(Gravity.LEFT);
                indv_IncomeRow.addView(sCheckBox1);


                mIncomeTable.addView(indv_IncomeRow,
                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            }
          //  resizeMemberNameWidth();
            resizeBodyTableRowHeight();

            mSubmit_Raised_Button = (Button) rootView.findViewById(R.id.fragment_Submit);
            mSubmit_Raised_Button.setText(AppStrings.submit);
            mSubmit_Raised_Button.setOnClickListener(this);
            mSubmit_Raised_Button.setTypeface(LoginActivity.sTypeface);

            setVisibility();*/


            TableRow leftHeaderRow = new TableRow(getActivity());

            TableRow.LayoutParams lHeaderParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);

            TextView mMemberName_headerText = new TextView(getActivity());
            mMemberName_headerText
                    .setText("" + String.valueOf(AppStrings.memberName));
            mMemberName_headerText.setTypeface(LoginActivity.sTypeface);
            mMemberName_headerText.setTextColor(Color.WHITE);
            mMemberName_headerText.setPadding(10, 5, 10, 5);
            mMemberName_headerText.setLayoutParams(lHeaderParams);
            leftHeaderRow.addView(mMemberName_headerText);

            mLeftHeaderTable.addView(leftHeaderRow);

            TableRow rightHeaderRow = new TableRow(getActivity());
            TableRow.LayoutParams rHeaderParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            rHeaderParams.setMargins(10, 0, 20, 0);
            TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            contentParams.setMargins(10, 0, 10, 0);


            TextView mVAmount_HeaderText = new TextView(getActivity());
            mVAmount_HeaderText
                    .setText("" + String.valueOf(AppStrings.amount));
            mVAmount_HeaderText.setTypeface(LoginActivity.sTypeface);
            mVAmount_HeaderText.setTextColor(Color.WHITE);

            mVAmount_HeaderText.setLayoutParams(rHeaderParams);
            mVAmount_HeaderText.setSingleLine(true);
            mVAmount_HeaderText.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
            rightHeaderRow.addView(mVAmount_HeaderText);


            TextView cHeadView = new TextView(getActivity());
            cHeadView
                    .setText("" + String.valueOf(AppStrings.cashmode));
            cHeadView.setTypeface(LoginActivity.sTypeface);
            cHeadView.setTextColor(Color.WHITE);

            cHeadView.setLayoutParams(rHeaderParams);
            cHeadView.setSingleLine(true);
            cHeadView.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
            rightHeaderRow.addView(cHeadView);

            mRightHeaderTable.addView(rightHeaderRow);
            //   getTableRowHeaderCellWidth();

            //code
            //responsesize = responseDto.getResponseContent().getMemberloanRepaymentList().size();
            for (int i = 0; i < mSize; i++) {

                TableRow leftContentRow = new TableRow(getActivity());

                TableRow.LayoutParams leftContentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                leftContentParams.setMargins(5, 5, 5, 5);
                final TextView memberName_Text = new TextView(getActivity());
                memberName_Text.setText(memList.get(i).getMemberName());
                memberName_Text.setTextColor(R.color.black);
                memberName_Text.setPadding(5, 5, 5, 5);
                memberName_Text.setLayoutParams(leftContentParams);
                memberName_Text.setWidth(200);
                memberName_Text.setSingleLine(true);
                memberName_Text.setEllipsize(TextUtils.TruncateAt.END);
                leftContentRow.addView(memberName_Text);

                mLeftContentTable.addView(leftContentRow);

                TableRow rightContentRow = new TableRow(getActivity());
                TableRow.LayoutParams rightContentParams = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);// (150,60,1f);
                rightContentParams.setMargins(10, 5, 10, 5);

                TableRow.LayoutParams contentRow_Params = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 60,
                        1f);

                contentRow_Params.setMargins(10, 5, 10, 5);


                mamount_values = new EditText(getActivity());
                mamount_values.setId(i);
                sIncomeFields.add(mamount_values);
                mamount_values.setPadding(5, 5, 5, 5);
                mamount_values.setBackgroundResource(R.drawable.edittext_background);
                mamount_values.setLayoutParams(rightContentParams);
                mamount_values.setTextAppearance(getActivity(), R.style.MyMaterialTheme);
                mamount_values.setFilters(Get_EdiText_Filter.editText_filter());
                mamount_values.setInputType(InputType.TYPE_CLASS_NUMBER);
                mamount_values.setTextColor(R.color.black);
                // mSavings_values.setWidth(150);
                mamount_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        // TODO Auto-generated method stub
                        if (hasFocus) {
                            ((EditText) v).setGravity(Gravity.LEFT);

                            mMemberNameLayout.setVisibility(View.VISIBLE);
                            mMemberName.setText(memberName_Text.getText().toString().trim());
                            TextviewUtils.manageBlinkEffect(mMemberName, getActivity());

                        } else {

                            ((EditText) v).setGravity(Gravity.RIGHT);
                            mMemberNameLayout.setVisibility(View.GONE);
                            mMemberName.setText("");
                        }

                    }
                });
                rightContentRow.addView(mamount_values);


                //   mRightContentTable.addView(rightContentRow);


                TableRow.LayoutParams contentParams3 = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 0f);
                contentParams3.setMargins(30, 5, 10, 5);
                sCheckBox1 = new CheckBox(getActivity());
                sCheckBox1.setId(i);
                sCheckBox1.setWidth(50);
                mCheckBoxFields.add(sCheckBox1);
                sCheckBox1.setBackgroundResource(R.drawable.btn_check_to_off_mtrl_013);
                sCheckBox1.setClickable(true);
                final int position = i;
                sCheckBox1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if (compoundButton.isChecked()) {
                            mCheckBoxFields.get(position).setChecked(true);
                        } else {
                            mCheckBoxFields.get(position).setChecked(false);
                        }

                    }
                });
                sCheckBox1.setLayoutParams(contentParams3);
                sCheckBox1.setGravity(Gravity.LEFT);
                rightContentRow.addView(sCheckBox1);
                mRightContentTable.addView(rightContentRow);


            }


            mSubmit_Raised_Button = (Button) rootView.findViewById(R.id.mlr_fragment_Submit_button);
            mSubmit_Raised_Button.setText(AppStrings.submit);
            mSubmit_Raised_Button.setTypeface(LoginActivity.sTypeface);
            mSubmit_Raised_Button.setOnClickListener(this);

            mPerviousButton = (Button) rootView.findViewById(R.id.mlr_fragment_Previousbutton);
            //  mPerviousButton.setText("Savings" + AppStrings.mPervious);
            mPerviousButton.setOnClickListener(this);

            mNextButton = (Button) rootView.findViewById(R.id.mlr_fragment_memberskipbutton);
            //     mNextButton.setText("Savings" + AppStrings.mNext);
            mNextButton.setOnClickListener(this);


            mPerviousButton.setVisibility(View.INVISIBLE);
            mNextButton.setVisibility(View.INVISIBLE);
            resizeMemberNameWidth();
            //      resizeRightSideTable();
            resizeBodyTableRowHeight();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setVisibility() {
        // TODO Auto-generated method stub

    }


    private int viewHeight(View view) {
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        return view.getMeasuredHeight();
    }

    // read a view's width
    private int viewWidth(View view) {
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        return view.getMeasuredWidth();
    }


    private void matchLayoutHeight(TableRow tableRow, int height) {

        int tableRowChildCount = tableRow.getChildCount();

        // if a TableRow has only 1 child
        if (tableRow.getChildCount() == 1) {

            View view = tableRow.getChildAt(0);
            TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();
            params.height = height - (params.bottomMargin + params.topMargin);

            return;
        }

        // if a TableRow has more than 1 child
        for (int x = 0; x < tableRowChildCount; x++) {

            View view = tableRow.getChildAt(x);

            TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();

            if (!isTheHeighestLayout(tableRow, x)) {
                params.height = height - (params.bottomMargin + params.topMargin);
                return;
            }
        }

    }

    // check if the view has the highest height in a TableRow
    private boolean isTheHeighestLayout(TableRow tableRow, int layoutPosition) {

        int tableRowChildCount = tableRow.getChildCount();
        int heighestViewPosition = -1;
        int viewHeight = 0;

        for (int x = 0; x < tableRowChildCount; x++) {
            View view = tableRow.getChildAt(x);
            int height = this.viewHeight(view);

            if (viewHeight < height) {
                heighestViewPosition = x;
                viewHeight = height;
            }
        }
        return heighestViewPosition == layoutPosition;
    }


    private void resizeMemberNameWidth() {
        // TODO Auto-generated method stub
        int leftHeadertWidth = viewWidth(mLeftHeaderTable);
        int leftContentWidth = viewWidth(mLeftContentTable);

        if (leftHeadertWidth < leftContentWidth) {
            mLeftHeaderTable.getLayoutParams().width = leftContentWidth;
        } else {
            mLeftContentTable.getLayoutParams().width = leftHeadertWidth;
        }
    }

    private void resizeBodyTableRowHeight() {

        int leftContentTable_ChildCount = mLeftContentTable.getChildCount();

        for (int x = 0; x < leftContentTable_ChildCount; x++) {

            TableRow leftContentTableRow = (TableRow) mLeftContentTable.getChildAt(x);
            TableRow rightContentTableRow = (TableRow) mRightContentTable.getChildAt(x);

            int rowLeftHeight = viewHeight(leftContentTableRow);
            int rowRightHeight = viewHeight(rightContentTableRow);

            TableRow tableRow = rowLeftHeight < rowRightHeight ? leftContentTableRow : rightContentTableRow;
            int finalHeight = rowLeftHeight > rowRightHeight ? rowLeftHeight : rowRightHeight;

            this.matchLayoutHeight(tableRow, finalHeight);
        }

    }


    @Override
    public void onClick(View view) {

        sIncomeAmounts = new String[sIncomeFields.size()];
        switch (view.getId()) {
            case R.id.mlr_fragment_Submit_button:
                try {
                    sIncome_Total = 0;
                    isIncome = true;
                    confirmArr = new String[mSize];
                    mOthersAmount_Values = "";
                    mOthersAmount_Values = mOthersEditText.getText().toString();


                    if (mOthersAmount_Values.equals("") || mOthersAmount_Values == null) {
                        mOthersAmount_Values = nullVlaue;
                    } else {
                        mOthersAmount_Values = mOthersEditText.getText().toString();
                    }

                    if (mOthersAmount_Values.matches("\\d*\\.?\\d+")) { // match

                        int amount = (int) Math.round(Double.parseDouble(mOthersAmount_Values));
                        mOthersAmount_Values = String.valueOf(amount);
                    }

                    if (arrMem != null && arrMem.size() > 0) {
                        arrMem.clear();
                    }

                    int cash_count = 0;

                    for (int i = 0; i < mSize; i++) {
                        MemberList mem = new MemberList();
                        mem.setMemberId(memList.get(i).getMemberId());
                        mem.setAmount(sIncomeFields.get(i).getText().toString());

                        if (mCheckBoxFields.get(i).isChecked()) {
                            mem.setCashOrCashless(true);
                            mem.setCash(true);
                        } else {
                            mem.setCashOrCashless(false);
                            mem.setCash(false);
                        }

                        arrMem.add(mem);

                        if (arrMem.get(i).isCash()) {

                            cash_count = cash_count + 1;
                        }

                        sIncomeAmounts[i] = sIncomeFields.get(i).getText().toString();

                        sIncome_Total += Integer.parseInt((!sIncomeFields.get(i).getText().toString().equals("") && sIncomeFields.get(i).getText().toString().length() > 0) ? sIncomeFields.get(i).getText().toString() : "0");
                    }

                    if (arrMem.size() == cash_count) {
                        timeout_saving = MySharedPreference.readLong(getActivity(), MySharedPreference.fttimeout, 180000);

                    } else if (cash_count == 0) {
                        timeout_saving = MySharedPreference.readLong(getActivity(), MySharedPreference.upi_timeout, 180000);

                    } else {
                        timeout_saving = Math.max(MySharedPreference.readLong(getActivity(), MySharedPreference.fttimeout, 180000), MySharedPreference.readLong(getActivity(), MySharedPreference.upi_timeout, 180000));

                    }

                    if (mOthersAmount_Values != null && mOthersAmount_Values.length() > 0)
                        sIncome_Total = sIncome_Total + Integer.parseInt(mOthersAmount_Values);

                    if (sIncome_Total != 0) {

                        confirmationDialog = new Dialog(getActivity());
                        LayoutInflater inflater = getActivity().getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.dialog_new_confirmation, null);
                        dialogView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
                                LayoutParams.WRAP_CONTENT));

                        TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
                        confirmationHeader.setText(AppStrings.confirmation);
                        confirmationHeader.setTypeface(LoginActivity.sTypeface);
                        TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

                        for (int i = 0; i < mSize; i++) {

                            TableRow indv_SavingsRow = new TableRow(getActivity());

                            @SuppressWarnings("deprecation")
                            TableRow.LayoutParams contentParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
                                    LayoutParams.WRAP_CONTENT, 1f);
                            contentParams.setMargins(10, 5, 10, 5);

                            TextView memberName_Text = new TextView(getActivity());
                            memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
                                    memList.get(i).getMemberName()));
                            memberName_Text.setPadding(5, 5, 5, 5);
                            memberName_Text.setLayoutParams(contentParams);
                            indv_SavingsRow.addView(memberName_Text);

                            TextView confirm_values = new TextView(getActivity());
                            confirm_values
                                    .setText(GetSpanText.getSpanString(getActivity(), String.valueOf((sIncomeAmounts[i] != null && sIncomeAmounts[i].length() > 0) ? sIncomeAmounts[i] : "0")));
                            confirm_values.setPadding(5, 5, 5, 5);
                            confirm_values.setGravity(Gravity.RIGHT);
                            confirm_values.setLayoutParams(contentParams);
                            indv_SavingsRow.addView(confirm_values);

                            CheckBox sCheckBox1 = new CheckBox(getActivity());
                            sCheckBox1.setId(i);
                            sCheckBox1.setBackgroundResource(R.drawable.btn_check_to_off_mtrl_013);
                            // sCheckBox1.setPadding(5, 5, 5, 5);
                            sCheckBox1.setClickable(false);
                            sCheckBox1.setLayoutParams(contentParams);
                            if (mCheckBoxFields.get(i).isChecked())
                                sCheckBox1.setChecked(true);
                            else
                                sCheckBox1.setChecked(false);

                            sCheckBox1.setGravity(Gravity.TOP);
                            indv_SavingsRow.addView(sCheckBox1);

                            confirmationTable.addView(indv_SavingsRow,
                                    new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

                        }

                        if (Income.sSelectedIncomeMenu.getIncomeType().toUpperCase().equals(AppStrings.otherincome)) {

                            TableRow othersRow = new TableRow(getActivity());

                            @SuppressWarnings("deprecation")
                            TableRow.LayoutParams others_contentParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
                                    LayoutParams.WRAP_CONTENT, 1f);
                            others_contentParams.setMargins(10, 5, 10, 5);

                            TextView memberName_Text = new TextView(getActivity());
                            memberName_Text
                                    .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mOthers)));
                            memberName_Text.setPadding(5, 5, 5, 5);
                            memberName_Text.setLayoutParams(others_contentParams);
                            othersRow.addView(memberName_Text);

                            TextView confirm_values = new TextView(getActivity());
                            confirm_values.setText(
                                    GetSpanText.getSpanString(getActivity(), String.valueOf(mOthersAmount_Values)));
                            confirm_values.setPadding(5, 5, 5, 5);
                            confirm_values.setGravity(Gravity.RIGHT);
                            confirm_values.setLayoutParams(others_contentParams);
                            othersRow.addView(confirm_values);

                            confirmationTable.addView(othersRow,
                                    new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
                        } else {

                        }

                        View rullerView = new View(getActivity());
                        rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
                        rullerView.setBackgroundColor(Color.rgb(0, 199, 140));// rgb(255,
                        // 229,
                        // 242));
                        confirmationTable.addView(rullerView);

                        TableRow totalRow = new TableRow(getActivity());

                        @SuppressWarnings("deprecation")
                        TableRow.LayoutParams totalParams = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,
                                LayoutParams.WRAP_CONTENT, 1f);
                        totalParams.setMargins(10, 5, 10, 5);

                        TextView totalText = new TextView(getActivity());
                        totalText.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.total)));
                        totalText.setPadding(5, 5, 5, 5);// (5, 10, 5, 10);
                        totalText.setLayoutParams(totalParams);
                        totalRow.addView(totalText);

                        TextView totalAmount = new TextView(getActivity());
                        totalAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sIncome_Total)));// SavingsFragment.sSavings_Total
                        totalAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
                        totalAmount.setGravity(Gravity.RIGHT);
                        totalAmount.setLayoutParams(totalParams);
                        totalRow.addView(totalAmount);
                     /*   TextView checkAmount = new TextView(getActivity());
                        //  checkAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sVSavings_Total)));
                        checkAmount.setTextColor(R.color.black);
                        checkAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
                        checkAmount.setGravity(Gravity.RIGHT);
                        checkAmount.setVisibility(View.INVISIBLE);
                        checkAmount.setLayoutParams(totalParams);
                        totalRow.addView(checkAmount);
*/


                        confirmationTable.addView(totalRow,
                                new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

                        TableRow totalwalletRow = new TableRow(getActivity());

                        TableRow.LayoutParams totalwalletParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                        totalwalletParams.setMargins(10, 5, 10, 5);

                        TextView totalwalletText = new TextView(getActivity());
                        totalwalletText.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.walletbalanceTxt)));
                        //    totalwalletText.setTextColor(R.color.green_border_color);
                        totalwalletText.setTextColor(getActivity().getResources().getColor(R.color.green_border_color));
                        totalwalletText.setPadding(5, 5, 5, 5);// (5, 10, 5, 10);
                        totalwalletText.setLayoutParams(totalwalletParams);
                        totalwalletText.setTextSize(20);
                        totalwalletRow.addView(totalwalletText);

                        TextView totalwalletAmount = new TextView(getActivity());
                        totalwalletAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(((Integer.parseInt(shgDto.getCashInHand()))))));// + sSavings_Total+sVSavings_Total
                        totalwalletAmount.setTextColor(R.color.green_border_color);
                        totalwalletAmount.setPadding(5, 5, 25, 5);// (5, 10, 100, 10);
                        totalwalletAmount.setGravity(Gravity.RIGHT);
                        totalwalletText.setTextSize(20);
                        totalwalletAmount.setLayoutParams(totalwalletParams);
                        totalwalletRow.addView(totalwalletAmount);
                        TextView checkAmount1 = new TextView(getActivity());
                        //  checkAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sVSavings_Total)));
                        checkAmount1.setTextColor(R.color.black);
                        checkAmount1.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
                        checkAmount1.setGravity(Gravity.RIGHT);
                        checkAmount1.setVisibility(View.INVISIBLE);
                        checkAmount1.setLayoutParams(totalParams);
                        totalRow.addView(checkAmount1);


//                        TextView totalVSAmount = new TextView(getActivity());
//                        totalVSAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sVSavings_Total)));
//                        totalVSAmount.setTextColor(R.color.black);
//                        totalVSAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
//                        totalVSAmount.setGravity(Gravity.RIGHT);
//                        totalVSAmount.setLayoutParams(totalParams);
//                        totalRow.addView(totalVSAmount);


                        confirmationTable.addView(totalwalletRow,
                                new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));


                        mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit);
                        mEdit_RaisedButton.setText(AppStrings.edit);
                        mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
                        mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
                        // 205,
                        // 0));
                        mEdit_RaisedButton.setOnClickListener(this);

                        mOk_RaisedButton = (Button) dialogView.findViewById(R.id.frag_Ok);
                        mOk_RaisedButton.setText(AppStrings.yes);
                        mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
                        mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
                        mOk_RaisedButton.setOnClickListener(this);

                        confirmationDialog.getWindow()
                                .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        confirmationDialog.setCanceledOnTouchOutside(false);
                        confirmationDialog.setContentView(dialogView);
                        confirmationDialog.setCancelable(true);
                        confirmationDialog.show();

                        MarginLayoutParams margin = (MarginLayoutParams) dialogView.getLayoutParams();
                        margin.leftMargin = 10;
                        margin.rightMargin = 10;
                        margin.topMargin = 10;
                        margin.bottomMargin = 10;
                        margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);
                    } else {

                        TastyToast.makeText(getActivity(), AppStrings.nullAlert, TastyToast.LENGTH_SHORT, TastyToast.WARNING);

                    }

                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }
                break;
            case R.id.fragment_Edit:
                sIncome_Total = 0;
                mOthersAmount_Values = "0";
                mSubmit_Raised_Button.setClickable(true);

                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();

                break;
            case R.id.frag_Ok:


                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();


                if (Income.sSelectedIncomeMenu.getIncomeType().toUpperCase().equals(AppStrings.subscriptioncharges)) {
                    SavingRequest sr = new SavingRequest();
//                    sr.setIncomeTypeId(Income.sSelectedIncomeMenu.getId());
                    sr.setTransactionType(transaction);
                    sr.setDigital(true);
                    sr.setMobileDate(System.currentTimeMillis() + "");
                    sr.setModeOfCash("2");
                    sr.setUserId(MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, ""));
                    sr.setAgentId(MySharedPreference.readString(getActivity(), MySharedPreference.AGENT_ID, ""));
                    sr.setShgId(shgDto.getShgId());
                    sr.setTransactionDate(shgDto.getLastTransactionDate());
                    sr.setMemberSaving(arrMem);
                    sr.setAmount(mOthersAmount_Values);
                    String sreqString = new Gson().toJson(sr);

                    if (networkConnection.isNetworkAvailable()) {
                        onTaskStarted();

                        ViewGroup viewGroup = rootView.findViewById(android.R.id.content);
                        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.online_transaction_timer, viewGroup, false);
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setView(dialogView);
                        alertDialog = builder.create();
                        alertDialog.show();
                        alertDialog.setCancelable(false);
                        counterTextView = dialogView.findViewById(R.id.counterTimerTxt);
                        counterTimerStatusTxt = dialogView.findViewById(R.id.counterTimerStatusTxt);


                        Restclient_Timeout.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.SUBSCRIPTION, sreqString, getActivity(), ServiceType.OI_S_P);
                        cndr = new CountDownTimer(timeout_saving, 1000) { // adjust the milli seconds here
                            public void onTick(long millisUntilFinished) {
                                counterTextView.setText("" + String.format(FORMAT_TIMER,
                                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(
                                                TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                                                TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))) + " minutes");
                            }

                            @Override
                            public void onFinish() {
//                            MemberDrawerScreen.showFragment(new Digitization_Deposit_Details());
                                cndr.cancel();
                                alertDialog.dismiss();
                                TastyToast.makeText(getActivity(), AppStrings.counterDismiss, TastyToast.LENGTH_SHORT,
                                        TastyToast.WARNING);
//                            timer.cancel();
                            }


                        }.start();
                    }


                } else if (Income.sSelectedIncomeMenu.getIncomeType().toUpperCase().equals(AppStrings.otherincome)) {
                    SavingRequest sr = new SavingRequest();
                    sr.setTransactionType(transaction);
                    sr.setDigital(true);
                    sr.setMobileDate(System.currentTimeMillis() + "");
                    sr.setModeOfCash("2");
                    sr.setUserId(MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, ""));
                    sr.setAgentId(MySharedPreference.readString(getActivity(), MySharedPreference.AGENT_ID, ""));
                    sr.setShgId(shgDto.getShgId());
                    sr.setTransactionDate(shgDto.getLastTransactionDate());
                    sr.setMemberSaving(arrMem);
                    sr.setAmount(mOthersAmount_Values);
                    String sreqString = new Gson().toJson(sr);
                    if (networkConnection.isNetworkAvailable()) {
                        onTaskStarted();

                        ViewGroup viewGroup = rootView.findViewById(android.R.id.content);
                        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.online_transaction_timer, viewGroup, false);
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setView(dialogView);
                        alertDialog = builder.create();
                        alertDialog.show();
                        alertDialog.setCancelable(false);
                        counterTextView = dialogView.findViewById(R.id.counterTimerTxt);
                        counterTimerStatusTxt = dialogView.findViewById(R.id.counterTimerStatusTxt);


                        RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.OTHERINCOME, sreqString, getActivity(), ServiceType.OI_S_P);

                        cndr = new CountDownTimer(timeout_saving, 1000) { // adjust the milli seconds here
                            public void onTick(long millisUntilFinished) {
                                counterTextView.setText("" + String.format(FORMAT_TIMER,
                                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(
                                                TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                                                TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))) + " minutes");
                            }

                            @Override
                            public void onFinish() {
//                            MemberDrawerScreen.showFragment(new Digitization_Deposit_Details());
                                cndr.cancel();
                                alertDialog.dismiss();
                                TastyToast.makeText(getActivity(), AppStrings.counterDismiss, TastyToast.LENGTH_SHORT,
                                        TastyToast.WARNING);

//                            timer.cancel();
                            }


                        }.start();

                    }

                } else if (Income.sSelectedIncomeMenu.getIncomeType().toUpperCase().equals(AppStrings.penalty)) {
                    SavingRequest sr = new SavingRequest();
                    sr.setTransactionType(transaction);
                    sr.setDigital(true);
                    sr.setMobileDate(System.currentTimeMillis() + "");
                    sr.setModeOfCash("2");
                    sr.setUserId(MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, ""));
                    sr.setAgentId(MySharedPreference.readString(getActivity(), MySharedPreference.AGENT_ID, ""));
                    sr.setShgId(shgDto.getShgId());
                    sr.setTransactionDate(shgDto.getLastTransactionDate());
                    sr.setMemberSaving(arrMem);
                    sr.setAmount(mOthersAmount_Values);
                    String sreqString = new Gson().toJson(sr);
                    if (networkConnection.isNetworkAvailable()) {
                        onTaskStarted();

                        ViewGroup viewGroup = rootView.findViewById(android.R.id.content);
                        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.online_transaction_timer, viewGroup, false);
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setView(dialogView);
                        alertDialog = builder.create();
                        alertDialog.show();
                        alertDialog.setCancelable(false);
                        counterTextView = dialogView.findViewById(R.id.counterTimerTxt);
                        counterTimerStatusTxt = dialogView.findViewById(R.id.counterTimerStatusTxt);


                        Restclient_Timeout.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.PENALTY, sreqString, getActivity(), ServiceType.OI_S_P);

                        cndr = new CountDownTimer(timeout_saving, 1000) { // adjust the milli seconds here
                            public void onTick(long millisUntilFinished) {
                                counterTextView.setText("" + String.format(FORMAT_TIMER,
                                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(
                                                TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                                                TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))) + " minutes");
                            }

                            @Override
                            public void onFinish() {
//                            MemberDrawerScreen.showFragment(new Digitization_Deposit_Details());
                                cndr.cancel();
//                            timer.cancel();
                            }


                        }.start();
                    }

                }
                break;

            case R.id.autoFill:

                String similiar_Income;
                // isValues = true;

                if (mAutoFill.isChecked()) {

                    try {

                        // Makes all edit fields holds the same savings
                        similiar_Income = sIncomeFields.get(0).getText().toString();

                        for (int i = 0; i < sIncomeAmounts.length; i++) {
                            sIncomeFields.get(i).setText(similiar_Income);
                            sIncomeFields.get(i).setGravity(Gravity.RIGHT);
                            sIncomeFields.get(i).clearFocus();
                            sIncomeAmounts[i] = similiar_Income;

                        }

                        /** To clear the values of EditFields in case of uncheck **/

                    } catch (ArrayIndexOutOfBoundsException e) {
                        e.printStackTrace();

                        TastyToast.makeText(getActivity(), AppStrings.adminAlert, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);
                    }
                } else {

                }

                break;

            case R.id.autoFill1:

                String similiar_Income1;
                // isValues = true;

                if (mAutoFill1.isChecked()) {

                    try {

                        // Makes all edit fields holds the same savings
                        similiar_Income1 = sIncomeFields.get(0).getText().toString();

                        for (int i = 0; i < sIncomeAmounts.length; i++) {
                            sIncomeFields.get(i).setText(similiar_Income1);
                            sIncomeFields.get(i).setGravity(Gravity.RIGHT);
                            sIncomeFields.get(i).clearFocus();
                            sIncomeAmounts[i] = similiar_Income1;

                        }

                        /** To clear the values of EditFields in case of uncheck **/

                    } catch (ArrayIndexOutOfBoundsException e) {
                        e.printStackTrace();

                        TastyToast.makeText(getActivity(), AppStrings.adminAlert, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);
                    }
                } else {

                }

                break;
        }


    }

    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        if (mProgressDilaog != null) {
            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                mProgressDilaog.dismiss();
                mProgressDilaog = null;
                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();
            }
            switch (serviceType) {
                case OI_S_P:
                    try {
                        if (result != null) {
                            ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                            String message = cdto.getMessage();
                            int statusCode = cdto.getStatusCode();
                            if (statusCode == Utils.Success_Code) {
                                Utils.showToast(getActivity(), message);


                                if (cdto.getResponseContents() != null) {
                                    for (int i = 0; i < cdto.getResponseContents().size(); i++) {
                                        ResponseContents contents = new ResponseContents();
                                        contents.setMemberName(cdto.getResponseContents().get(i).getMemberName());
                                        contents.setTransactionId(cdto.getResponseContents().get(i).getTransactionId());
                                        contents.setMessage(cdto.getResponseContents().get(i).getMessage());
                                        contents.setTxType(AppStrings.income);
                                        responseContentsList.add(contents);
                                    }

                                    ViewSavingTransactionDetails viewSavingTransactionDetails = new ViewSavingTransactionDetails();
                                    Bundle bundle = new Bundle();
                                    bundle.putSerializable("transactiondetails", responseContentsList);
                                    viewSavingTransactionDetails.setArguments(bundle);
                                    NewDrawerScreen.showFragment(viewSavingTransactionDetails);
                                    TastyToast.makeText(getActivity(), "Transaction Completed",
                                            TastyToast.LENGTH_SHORT, TastyToast.SUCCESS);
                                    alertDialog.dismiss();
                                    cndr.cancel();
                                }

/*
                                if (shgDto.getFFlag() == null || !shgDto.getFFlag().equals("1")) {
                                    SHGTable.updateTransactionSHGDetails(shgDto.getShgId());
                                }

                                MemberDrawerScreen.showFragment(new Transactionpaymentsuccess());
                                TastyToast.makeText(getActivity(), "Transaction Completed",
                                        TastyToast.LENGTH_SHORT, TastyToast.SUCCESS);
                                alertDialog.dismiss();
                                cndr.cancel();*/
                          /*  FragmentManager fm = getFragmentManager();
                            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                            MemberDrawerScreen.showFragment(new MainFragment());*/

                            } else {

                                if (statusCode == 401) {

                                    Log.e("Group Logout", "Logout Sucessfully");
                                    AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                                    if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                        mProgressDilaog.dismiss();
                                        mProgressDilaog = null;
                                    }
                                }
                                Utils.showToast(getActivity(), message);
                                TastyToast.makeText(getActivity(), "Transaction Timeout",
                                        TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                                alertDialog.dismiss();
                                cndr.cancel();

                            }
                        } else {
                            cndr.cancel();
                            alertDialog.dismiss();
                            TastyToast.makeText(getActivity(), AppStrings.failedDigiResponse, TastyToast.LENGTH_SHORT,
                                    TastyToast.WARNING);
                        }
                    } catch (Exception e) {

                    }
                    break;
            }


        }
    }

}
