package com.oasys.eshakti.digitization.model;

import java.io.Serializable;

public class InternalBankMFIModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8313834883703591456L;
	private static String mLoanType;
	private static String mLoanSource;
	private static String mBankName;
	private static String mNBFCName;
	private static String mLoanAccNo;
	private static String mLoan_Sanction_Date;
	private static String mLoan_Sanction_Amount;
	private static String mLoanBankCharges;
	private static String mType;
	private static String mLoan_Disbursement_Date;
	private static String mLoan_Disbursement_Amount;
	private static String mLoan_Tenure;
	private static String mInstallment_Type;
	private static String mSubsidy_Amount;
	private static String mSubsidy_Reserve_Fund;
	private static String mPurpose_of_Loan;
	private static String mValues;
	private static String mInterest_Rate;
	private static String mLoanName;
	private static String mBankBranchName;

	public InternalBankMFIModel(final String loantype, final String loansource, final String bankname,
                                final String nbfcname, final String loanaccno, final String sanctiondate, final String sanctionAmount,
                                final String mLoanBankCharges, final String mType, final String disbursementdate, final String disbursementamount,
                                final String tenure, final String installmentType, final String subsidyAmount,
                                final String subsidyReserveFund, final String purposeofLoan, final String values, final String InterestRate,
                                final String loanName, final String bankbranchname) {

		InternalBankMFIModel.setLoanType(loantype);
		InternalBankMFIModel.setLoanSource(loansource);
		InternalBankMFIModel.setBankName(bankname);
		InternalBankMFIModel.setNBFCName(nbfcname);
		InternalBankMFIModel.setLoanAccNo(loanaccno);
		InternalBankMFIModel.setLoan_Sanction_Date(sanctiondate);
		InternalBankMFIModel.setLoan_Sanction_Amount(sanctionAmount);
		InternalBankMFIModel.setLoanBankCharges(mLoanBankCharges);
		InternalBankMFIModel.setType(mType);
		InternalBankMFIModel.setLoan_Disbursement_Date(disbursementdate);
		InternalBankMFIModel.setLoan_Disbursement_Amount(disbursementamount);
		InternalBankMFIModel.setLoan_Tenure(tenure);
		InternalBankMFIModel.setInstallment_Type(installmentType);
		InternalBankMFIModel.setSubsidy_Amount(subsidyAmount);
		InternalBankMFIModel.setSubsidy_Reserve_Fund(subsidyReserveFund);
		InternalBankMFIModel.setPurpose_of_Loan(purposeofLoan);
		InternalBankMFIModel.setValues(values);
		InternalBankMFIModel.setInterest_Rate(InterestRate);
		InternalBankMFIModel.setLoanName(loanName);
		InternalBankMFIModel.setBankBranchName(bankbranchname);

	}

	public static String getLoanType() {
		return mLoanType;
	}

	public static void setLoanType(String mLoanType) {
		InternalBankMFIModel.mLoanType = mLoanType;
	}

	public static String getLoanSource() {
		return mLoanSource;
	}

	public static void setLoanSource(String mLoanSource) {
		InternalBankMFIModel.mLoanSource = mLoanSource;
	}

	public static String getBankName() {
		return mBankName;
	}

	public static void setBankName(String mBankName) {
		InternalBankMFIModel.mBankName = mBankName;
	}

	public static String getNBFCName() {
		return mNBFCName;
	}

	public static void setNBFCName(String mNBFCName) {
		InternalBankMFIModel.mNBFCName = mNBFCName;
	}

	public static String getLoanAccNo() {
		return mLoanAccNo;
	}

	public static void setLoanAccNo(String mLoanAccNo) {
		InternalBankMFIModel.mLoanAccNo = mLoanAccNo;
	}

	public static String getLoan_Sanction_Date() {
		return mLoan_Sanction_Date;
	}

	public static void setLoan_Sanction_Date(String mLoan_Sanction_Date) {
		InternalBankMFIModel.mLoan_Sanction_Date = mLoan_Sanction_Date;
	}

	public static String getLoan_Sanction_Amount() {
		return mLoan_Sanction_Amount;
	}

	public static void setLoan_Sanction_Amount(String mLoan_Sanction_Amount) {
		InternalBankMFIModel.mLoan_Sanction_Amount = mLoan_Sanction_Amount;
	}

	public static String getLoan_Disbursement_Date() {
		return mLoan_Disbursement_Date;
	}

	public static void setLoan_Disbursement_Date(String mLoan_Disbursement_Date) {
		InternalBankMFIModel.mLoan_Disbursement_Date = mLoan_Disbursement_Date;
	}

	public static String getLoan_Disbursement_Amount() {
		return mLoan_Disbursement_Amount;
	}

	public static void setLoan_Disbursement_Amount(String mLoan_Disbursement_Amount) {
		InternalBankMFIModel.mLoan_Disbursement_Amount = mLoan_Disbursement_Amount;
	}

	public static String getLoan_Tenure() {
		return mLoan_Tenure;
	}

	public static void setLoan_Tenure(String mLoan_Tenure) {
		InternalBankMFIModel.mLoan_Tenure = mLoan_Tenure;
	}

	public static String getInstallment_Type() {
		return mInstallment_Type;
	}

	public static void setInstallment_Type(String mInstallment_Type) {
		InternalBankMFIModel.mInstallment_Type = mInstallment_Type;
	}

	public static String getSubsidy_Amount() {
		return mSubsidy_Amount;
	}

	public static void setSubsidy_Amount(String mSubsidy_Amount) {
		InternalBankMFIModel.mSubsidy_Amount = mSubsidy_Amount;
	}

	public static String getSubsidy_Reserve_Fund() {
		return mSubsidy_Reserve_Fund;
	}

	public static void setSubsidy_Reserve_Fund(String mSubsidy_Reserve_Fund) {
		InternalBankMFIModel.mSubsidy_Reserve_Fund = mSubsidy_Reserve_Fund;
	}

	public static String getPurpose_of_Loan() {
		return mPurpose_of_Loan;
	}

	public static void setPurpose_of_Loan(String mPurpose_of_Loan) {
		InternalBankMFIModel.mPurpose_of_Loan = mPurpose_of_Loan;
	}

	public static String getValues() {
		return mValues;
	}

	public static void setValues(String mValues) {
		InternalBankMFIModel.mValues = mValues;
	}

	public static String getInterest_Rate() {
		return mInterest_Rate;
	}

	public static void setInterest_Rate(String mInterest_Rate) {
		InternalBankMFIModel.mInterest_Rate = mInterest_Rate;
	}

	public static String getLoanName() {
		return mLoanName;
	}

	public static void setLoanName(String mLoanName) {
		InternalBankMFIModel.mLoanName = mLoanName;
	}

	public static String getBankBranchName() {
		return mBankBranchName;
	}

	public static void setBankBranchName(String mBankBranchName) {
		InternalBankMFIModel.mBankBranchName = mBankBranchName;
	}

	public static String getLoanBankCharges() {
		return mLoanBankCharges;
	}

	public static void setLoanBankCharges(String mLoanBankCharges) {
		InternalBankMFIModel.mLoanBankCharges = mLoanBankCharges;
	}

	public static String getType() {
		return mType;
	}

	public static void setType(String mType) {
		InternalBankMFIModel.mType = mType;
	}
}
