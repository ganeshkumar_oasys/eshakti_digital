package com.oasys.eshakti.digitization.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.oasys.eshakti.digitization.Adapter.CustomItemAdapter;
import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.Dto.MemberList;
import com.oasys.eshakti.digitization.Dto.ShgBankDetails;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.GetSpanText;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.database.BankTable;
import com.oasys.eshakti.digitization.database.MemberTable;
import com.oasys.eshakti.digitization.database.SHGTable;
import com.oasys.eshakti.digitization.model.RowItem;
import com.oasys.eshakti.digitization.views.Get_EdiText_Filter;
import com.oasys.eshakti.digitization.views.MaterialSpinner;
import com.oasys.eshakti.digitization.views.RaisedButton;
import com.tutorialsee.lib.TastyToast;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.view.fragment.Transaction_Loan_disburse_LoanAccFragment;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.yesteam.eshakti.webservices.SelectedGroupsTask.sLastTransactionDate_Response;

/**
 * Created by Dell on 15 Dec, 2018.
 */

public class LD_EL_LoanAccFragment extends Fragment implements View.OnClickListener {

    private TextView mGroupName, mCashInHand, mCashAtBank, mHeader;
    private TextView mSanctionAmountText, mDisbursementAmountText, mBalanceAmountText, mSanctionAmount_value,
            mDisbursementAmount_value, mBalanceAmount_value, mDis_AmountText;
    public static TextView mDis_DateText;
    private EditText mDis_Amount_editText;
    RadioButton mCashRadio, mBankRadio;
    private RaisedButton mSubmitButton;
    View rootView;
    public static String disbursementDate = "";
    public static String selectedType, selectedItemBank;
    public static String disbursementAmount;
    public static String disbursementDate_check = "";
    private String mLanguageLocale = "";
    Locale locale;
    MaterialSpinner materialSpinner_Bank;
    CustomItemAdapter bankNameAdapter;
    private List<RowItem> bankNameItems;
    public static String mBankNameValue = null;
    LinearLayout mSpinnerLayout;
    ArrayList<String> mBanknames_Array = new ArrayList<String>();
    ArrayList<String> mBanknamesId_Array = new ArrayList<String>();
    ArrayList<String> mEngSendtoServerBank_Array = new ArrayList<String>();
    ArrayList<String> mEngSendtoServerBankId_Array = new ArrayList<String>();
    Dialog confirmationDialog;
    private Button mEdit_RaisedButton, mOk_RaisedButton;
    Date date_dashboard, date_loanDisb;


    private int mSize;
    private List<MemberList> memList;
    private ListOfShg shgDto;
    private NetworkConnection networkConnection;
    private ArrayList<ShgBankDetails> bankdetails;


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.loan_dis_disDateTV:

                disbursementDate_check = "1";
           //     onSetCalendarValues();
                try {
                    Calendar calender = Calendar.getInstance();

                    SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                    String formattedDate = df.format(calender.getTime());
                    Log.e("Device Date  =  ", formattedDate + "");

                    String mBalanceSheetDate = sLastTransactionDate_Response; // dd/MM/yyyy
                    String formatted_balancesheetDate = mBalanceSheetDate.replace("/", "-");
                    Log.e("Balancesheet Date = ", formatted_balancesheetDate + "");

                    Date balanceDate = df.parse(formatted_balancesheetDate);
                    Date systemDate = df.parse(formattedDate);

                    if (balanceDate.compareTo(systemDate) < 0 || balanceDate.compareTo(systemDate) == 0) {

                        calendarDialogShow();
                    } else {
                        TastyToast.makeText(getActivity(), "PLEASE SET YOUR DEVICE DATE CORRECTLY", TastyToast.LENGTH_SHORT,
                                TastyToast.ERROR);

                    }
                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }

                break;

            case R.id.loanDisbursement_submit:

                disbursementAmount = "0";
                disbursementAmount = mDis_Amount_editText.getText().toString();
                String balanceAmount = mBalanceAmount_value.getText().toString();
                if (disbursementAmount.equals("") || disbursementAmount == null) {
                    disbursementAmount = "0";
                }

                if (!disbursementAmount.equals("0") && !selectedType.equals("") && !disbursementDate.equals("")) {

                    // String dashBoardDate = DatePickerDialog.sDashboardDate;
                    String dashBoardDate = null;

                    String loanDisbArr[] = EShaktiApplication.getLoanAcc_LoanDisbursementDate().split("/");
                    String loanDisbDate = loanDisbArr[1] + "-" + loanDisbArr[0] + "-" + loanDisbArr[2];

                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

                    try {
                        date_dashboard = sdf.parse(dashBoardDate);
                        date_loanDisb = sdf.parse(loanDisbDate);
                    } catch (ParseException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }

                    if (date_dashboard.compareTo(date_loanDisb) >= 0) {
                        if (Integer.parseInt(balanceAmount) != 0) {

                            if (Integer.parseInt(disbursementAmount) <= Integer.parseInt(balanceAmount)) {

                                EShaktiApplication.setLoanAccBalanceAmount(disbursementAmount);
                                if (mBankRadio.isChecked()) {
                                    if (!mBankNameValue.equals("0") && mBankNameValue != null) {

                                        onShowConfirmationDialog();

                                    } else {
                                        TastyToast.makeText(getActivity(), AppStrings.mLoanaccBankNullToast,
                                                TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                                    }

                                } else {
                                    onShowConfirmationDialog();
                                }

                            } else {
                                TastyToast.makeText(getActivity(), AppStrings.mCheckDisbursementAlert,
                                        TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                            }

                        } else {
                            TastyToast.makeText(getActivity(), AppStrings.mCheckbalanceAlert, TastyToast.LENGTH_SHORT,
                                    TastyToast.WARNING);
                        }

                    } else {
                        TastyToast.makeText(getActivity(), AppStrings.mCheckDisbursementDate, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);
                    }
                } else {
                    TastyToast.makeText(getActivity(), AppStrings.nullDetailsAlert, TastyToast.LENGTH_SHORT,
                            TastyToast.WARNING);
                }
                break;

            case R.id.fragment_Edit_button:
                disbursementAmount = "";
                mSubmitButton.setClickable(true);
                confirmationDialog.dismiss();

                break;
            case R.id.fragment_Ok_button:
                confirmationDialog.dismiss();
               /* Transaction_Loan_SB_disbursementFragment loan_SB_disbursementFragment = new Transaction_Loan_SB_disbursementFragment();
                setFragment(loan_SB_disbursementFragment);*/

                break;

        }
    }

    private void calendarDialogShow() {

    }

    private void onShowConfirmationDialog() {
        // TODO Auto-generated method stub
        confirmationDialog = new Dialog(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_confirmation, null);
        dialogView.setLayoutParams(
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
        confirmationHeader.setText(RegionalConversion.getRegionalConversion(com.yesteam.eshakti.appConstants.AppStrings.confirmation));
        confirmationHeader.setTypeface(LoginActivity.sTypeface);

        TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

        TableRow increaseLimitRow = new TableRow(getActivity());

        @SuppressWarnings("deprecation")
        TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
        contentParams.setMargins(10, 5, 10, 5);

        TextView increaseLimitText = new TextView(getActivity());
        increaseLimitText.setText(com.yesteam.eshakti.utils.GetSpanText.getSpanString(getActivity(), String.valueOf(com.yesteam.eshakti.appConstants.AppStrings.mLoanAccType)));
        increaseLimitText.setTypeface(LoginActivity.sTypeface);
        increaseLimitText.setTextColor(R.color.white);
        increaseLimitText.setPadding(5, 5, 5, 5);
        increaseLimitText.setLayoutParams(contentParams);
        increaseLimitRow.addView(increaseLimitText);

        TextView increaseLimit_values = new TextView(getActivity());
        increaseLimit_values.setText(com.yesteam.eshakti.utils.GetSpanText.getSpanString(getActivity(), String.valueOf(selectedType)));
        increaseLimit_values.setTextColor(R.color.white);
        increaseLimit_values.setPadding(5, 5, 5, 5);
        increaseLimit_values.setGravity(Gravity.RIGHT);
        increaseLimit_values.setLayoutParams(contentParams);
        increaseLimitRow.addView(increaseLimit_values);

        confirmationTable.addView(increaseLimitRow,
                new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        if (selectedType.equals("Bank")) {

            TableRow increaseLimitRow1 = new TableRow(getActivity());

            TextView increaseLimitText1 = new TextView(getActivity());
            increaseLimitText1.setText(com.yesteam.eshakti.utils.GetSpanText.getSpanString(getActivity(), String.valueOf(com.yesteam.eshakti.appConstants.AppStrings.bankName)));
            increaseLimitText1.setTypeface(LoginActivity.sTypeface);
            increaseLimitText1.setTextColor(R.color.white);
            increaseLimitText1.setPadding(5, 5, 5, 5);
            increaseLimitText1.setLayoutParams(contentParams);
            increaseLimitRow1.addView(increaseLimitText1);

            TextView increaseLimit_values1 = new TextView(getActivity());
            increaseLimit_values1.setText(com.yesteam.eshakti.utils.GetSpanText.getSpanString(getActivity(), String.valueOf(mBankNameValue)));
            increaseLimit_values1.setTextColor(R.color.white);
            increaseLimit_values1.setPadding(5, 5, 5, 5);
            increaseLimit_values1.setGravity(Gravity.RIGHT);
            increaseLimit_values1.setLayoutParams(contentParams);
            increaseLimitRow1.addView(increaseLimit_values1);

            confirmationTable.addView(increaseLimitRow1,
                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        }

        TableRow bankChargeRow = new TableRow(getActivity());

        TextView bankChargeText = new TextView(getActivity());
        bankChargeText
                .setText(com.yesteam.eshakti.utils.GetSpanText.getSpanString(getActivity(), String.valueOf(com.yesteam.eshakti.appConstants.AppStrings.mDisbursementAmount)));
        bankChargeText.setTypeface(LoginActivity.sTypeface);
        bankChargeText.setTextColor(R.color.white);
        bankChargeText.setPadding(5, 5, 5, 5);
        bankChargeText.setLayoutParams(contentParams);
        bankChargeRow.addView(bankChargeText);

        TextView bankCharge_values = new TextView(getActivity());
        bankCharge_values.setText(com.yesteam.eshakti.utils.GetSpanText.getSpanString(getActivity(), String.valueOf(disbursementAmount)));
        bankCharge_values.setTextColor(R.color.white);
        bankCharge_values.setPadding(5, 5, 5, 5);
        bankCharge_values.setGravity(Gravity.RIGHT);
        bankCharge_values.setLayoutParams(contentParams);
        bankChargeRow.addView(bankCharge_values);

        confirmationTable.addView(bankChargeRow,
                new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        TableRow tenureRow = new TableRow(getActivity());

        TextView tenureText = new TextView(getActivity());
        tenureText.setText(com.yesteam.eshakti.utils.GetSpanText.getSpanString(getActivity(), String.valueOf(com.yesteam.eshakti.appConstants.AppStrings.mDisbursementDate)));
        tenureText.setTypeface(LoginActivity.sTypeface);
        tenureText.setTextColor(R.color.white);
        tenureText.setPadding(5, 5, 5, 5);
        tenureText.setLayoutParams(contentParams);
        tenureRow.addView(tenureText);

        TextView tenure_values = new TextView(getActivity());
        tenure_values.setText(com.yesteam.eshakti.utils.GetSpanText.getSpanString(getActivity(),
                String.valueOf(Transaction_Loan_disburse_LoanAccFragment.disbursementDate)));
        tenure_values.setTextColor(R.color.white);
        tenure_values.setPadding(5, 5, 5, 5);
        tenure_values.setGravity(Gravity.RIGHT);
        tenure_values.setLayoutParams(contentParams);
        tenureRow.addView(tenure_values);

        confirmationTable.addView(tenureRow,
                new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        mEdit_RaisedButton = (com.yesteam.eshakti.views.RaisedButton) dialogView.findViewById(R.id.fragment_Edit_button);
        mEdit_RaisedButton.setText(RegionalConversion.getRegionalConversion(com.yesteam.eshakti.appConstants.AppStrings.edit));
        mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
        mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
        // 205,
        // 0));
        mEdit_RaisedButton.setOnClickListener(this);

        mOk_RaisedButton = (com.yesteam.eshakti.views.RaisedButton) dialogView.findViewById(R.id.fragment_Ok_button);
        mOk_RaisedButton.setText(RegionalConversion.getRegionalConversion(com.yesteam.eshakti.appConstants.AppStrings.yes));
        mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
        mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
        mOk_RaisedButton.setOnClickListener(this);

        confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmationDialog.setCanceledOnTouchOutside(false);
        confirmationDialog.setContentView(dialogView);
        confirmationDialog.setCancelable(true);
        confirmationDialog.show();

        ViewGroup.MarginLayoutParams margin = (ViewGroup.MarginLayoutParams) dialogView.getLayoutParams();
        margin.leftMargin = 10;
        margin.rightMargin = 10;
        margin.topMargin = 10;
        margin.bottomMargin = 10;
        margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_loan_from_loanacc_disburse, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mSize = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, "")).size();
        memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        bankdetails = BankTable.getBankName(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        init();
    }

    private void init() {
        try {

            mGroupName = (TextView) rootView.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName()+" / "+shgDto.getPresidentName());

            mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
            mCashInHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());

            mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
            mCashAtBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());

            mHeader = (TextView) rootView.findViewById(R.id.loanDisbursementheader);
            mSanctionAmountText = (TextView) rootView.findViewById(R.id.loan_dis_sanctionAmountTextView);
            mSanctionAmount_value = (TextView) rootView.findViewById(R.id.loan_dis_sanctionAmount_values);
            mDisbursementAmountText = (TextView) rootView.findViewById(R.id.loan_dis_disbursementAmountTextView);
            mDisbursementAmount_value = (TextView) rootView.findViewById(R.id.loan_dis_disbursementAmount_values);
            mBalanceAmountText = (TextView) rootView.findViewById(R.id.loan_dis_balanceTextView);
            mBalanceAmount_value = (TextView) rootView.findViewById(R.id.loan_dis_balance_values);
            mDis_AmountText = (TextView) rootView.findViewById(R.id.loanDisb_disbursementAmountTextView);
            mDis_DateText = (TextView) rootView.findViewById(R.id.loan_dis_disDateTV);
            mCashRadio = (RadioButton) rootView.findViewById(R.id.radioDisbursementLimitCash);
            mBankRadio = (RadioButton) rootView.findViewById(R.id.radioDisbursementLimitBank);

            mHeader.setText((AppStrings.mLoanDisbursementFromLoanAcc));
            mSanctionAmountText.setText((AppStrings.mSanctionAmount));
            mDisbursementAmountText.setText((AppStrings.mDisbursementAmount));
            mBalanceAmountText.setText((AppStrings.mBalanceAmount));
            mDis_AmountText.setText((AppStrings.mDisbursementAmount));
            mCashRadio.setText((AppStrings.mLoanaccCash));
            mBankRadio.setText((AppStrings.mLoanaccBank));

            mDis_DateText.setHint(GetSpanText.getSpanString(getActivity(), AppStrings.mDisbursementDate));
            mDis_DateText.setText(Transaction_Loan_disburse_LoanAccFragment.disbursementDate);
            mDis_DateText.setOnClickListener(this);


            mDis_Amount_editText = (EditText) rootView.findViewById(R.id.loanDisb_disbursementAmount_value);
            mDis_Amount_editText.setInputType(InputType.TYPE_CLASS_NUMBER);
            mDis_Amount_editText.setPadding(5, 5, 5, 5);
            mDis_Amount_editText.setFilters(Get_EdiText_Filter.editText_filter());
            mDis_Amount_editText.setBackgroundResource(R.drawable.edittext_background);

            mSubmitButton = (RaisedButton) rootView.findViewById(R.id.loanDisbursement_submit);
            mSubmitButton.setText((AppStrings.next));
            mSubmitButton.setOnClickListener(this);

            mSpinnerLayout = (LinearLayout) rootView.findViewById(R.id.loan_dis_bankSpinnerlayout);
            mSpinnerLayout.setVisibility(View.GONE);

            materialSpinner_Bank = (MaterialSpinner) rootView.findViewById(R.id.loan_dis_bankspinner);

            RadioGroup radioGroup = (RadioGroup) rootView.findViewById(R.id.radioDisbursementLimit);
            radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    // checkedId is the RadioButton selected
                    switch (checkedId) {
                        case R.id.radioDisbursementLimitCash:
                            selectedType = "Cash";
                            mSpinnerLayout.setVisibility(View.GONE);
                            break;

                        case R.id.radioDisbursementLimitBank:
                            selectedType = "Bank";
                            mSpinnerLayout.setVisibility(View.VISIBLE);
                            break;
                    }
                }
            });


            //init();

            for (int i = 0; i < bankdetails.size(); i++) {
                mBanknames_Array.add(bankdetails.get(i).getBankName().toString());
                mBanknamesId_Array.add(String.valueOf(i));
            }

            for (int i = 0; i < bankdetails.size(); i++) {
                mEngSendtoServerBank_Array.add(bankdetails.get(i).getBankId().toString());
                mEngSendtoServerBankId_Array.add(String.valueOf(i));
            }

            materialSpinner_Bank.setBaseColor(R.color.grey_400);

            materialSpinner_Bank.setFloatingLabelText(AppStrings.bankName);

            materialSpinner_Bank.setPaddingSafe(10, 0, 10, 0);

            final String[] bankNames = new String[bankdetails.size() + 1];

            final String[] bankNames_BankID = new String[bankdetails.size() + 1];

            bankNames[0] = String.valueOf((AppStrings.bankName));
            for (int i = 0; i < bankdetails.size(); i++) {
                bankNames[i + 1] = bankdetails.get(i).getBankName().toString();
            }

            bankNames_BankID[0] = String.valueOf((AppStrings.bankName));
            for (int i = 0; i < bankdetails.size(); i++) {
                bankNames_BankID[i + 1] = bankdetails.get(i).getBankId().toString();
            }

            int size = bankNames.length;

            bankNameItems = new ArrayList<RowItem>();
            for (int i = 0; i < size; i++) {
                RowItem rowItem = new RowItem(bankNames[i]);// sBankNames.elementAt(i).toString());
                bankNameItems.add(rowItem);
            }
            bankNameAdapter = new CustomItemAdapter(getActivity(), bankNameItems);
            materialSpinner_Bank.setAdapter(bankNameAdapter);

            materialSpinner_Bank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    // TODO Auto-generated method stub

                    if (position == 0) {
                        selectedItemBank = bankNames_BankID[0];
                        mBankNameValue = "0";

                    } else {
                        selectedItemBank = bankNames_BankID[position];
                        System.out.println("SELECTED BANK NAME : " + selectedItemBank);
                        mBankNameValue = selectedItemBank;
                        String mBankname = null;
                        for (int i = 0; i < mBanknames_Array.size(); i++) {
                            if (selectedItemBank.equals(mEngSendtoServerBank_Array.get(i))) {
                                mBankname = mEngSendtoServerBank_Array.get(i);
                            }
                        }

                        mBankNameValue = mBankname;

                    }
                    Log.e("Selected value", mBankNameValue);

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    // TODO Auto-generated method stub

                }
            });

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }
}
