package com.oasys.eshakti.digitization.views;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.widget.ExpandableListView;

import com.oasys.eshakti.digitization.EShaktiApplication;

public class MyExpandableListview extends ExpandableListView {

	public MyExpandableListview(Context context) {
		super(context);
		initParam();
	}

	public MyExpandableListview(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		initParam();
	}

	private void initParam() {

		this.setGroupIndicator(null);

		this.setVerticalScrollBarEnabled(false);

		setCacheColorHint(Color.TRANSPARENT);
		
		if (EShaktiApplication.isFragmentMenuListView()) {
			setDividerHeight(0);	
		} else {
			setDividerHeight(1);
		}
		setChildrenDrawnWithCacheEnabled(false);
		setGroupIndicator(null);

		ColorDrawable drawable_tranparent_ = new ColorDrawable(
				Color.TRANSPARENT);
		setSelector(drawable_tranparent_);
	}
}
