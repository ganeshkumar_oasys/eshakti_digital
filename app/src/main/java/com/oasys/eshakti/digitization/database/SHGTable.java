package com.oasys.eshakti.digitization.database;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.oasys.eshakti.digitization.Dto.CashOfGroup;
import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.EShaktiApplication;

import java.util.ArrayList;

public class SHGTable {

    private static SQLiteDatabase database;
    private static DbHelper dataHelper;
    static Cursor cursor;

    public SHGTable(Activity activity) {
        dataHelper = new DbHelper(activity);
    }

    public static void openDatabase() {
        dataHelper = new DbHelper(EShaktiApplication.getInstance());
        dataHelper.onOpen(database);
        database = dataHelper.getWritableDatabase();
    }

    public static void closeDatabase() {
        if (database != null && database.isOpen()) {
            database.close();
        }
    }

    public static void insertSHGData(ListOfShg shgDto, String u_id) {
        if (shgDto != null) {
            try {
                String CHvalue = "" + "0", CBvalue = "" + "0";
                if (shgDto.getCashInHand()!=null && shgDto.getCashInHand().length() > 0 && shgDto.getCashAtBank()!=null && shgDto.getCashAtBank().length() > 0) {
                    CHvalue = "" + (int) Double.parseDouble(shgDto.getCashInHand());
                    CBvalue = "" + (int) Double.parseDouble(shgDto.getCashAtBank());
                }

                openDatabase();

                ContentValues values = new ContentValues();
                values.put(TableConstants.SHG_CODE, (shgDto.getCode() != null && shgDto.getCode().length() > 0) ? shgDto.getCode() : "");
                values.put(TableConstants.SHG_ID, (shgDto.getId() != null && shgDto.getId().length() > 0) ? shgDto.getId() : "");
                values.put(TableConstants.USERID, (u_id != null && u_id.length() > 0) ? u_id : "");
                values.put(TableConstants.CASH_IN_HAND, ((CHvalue + "") != null && (CHvalue + "").length() > 0) ? (CHvalue + "") : "");
                values.put(TableConstants.CASH_AT_BANK, ((CBvalue + "") != null && (CBvalue + "").length() > 0) ? (CBvalue + "") : "");
                values.put(TableConstants.GRP_FORMATION_DATE, (shgDto.getGroupFormationDate() != null && shgDto.getGroupFormationDate().length() > 0) ? shgDto.getGroupFormationDate() : "");
                values.put(TableConstants.OPENINGDATE, (shgDto.getOpeningDate() != null && shgDto.getOpeningDate().length() > 0) ? shgDto.getOpeningDate() : "");
                values.put(TableConstants.NAME, (shgDto.getName() != null && shgDto.getName().length() > 0) ? shgDto.getName() : "");
                values.put(TableConstants.BLOCKNAME, (shgDto.getBlockName() != null && shgDto.getBlockName().length() > 0) ? shgDto.getBlockName() : "");
                values.put(TableConstants.VILLAGENAME, (shgDto.getVillageName() != null && shgDto.getVillageName().length() > 0) ? shgDto.getVillageName() : "");
                values.put(TableConstants.PANCHAYATNAME, (shgDto.getPanchayatName() != null && shgDto.getPanchayatName().length() > 0) ? shgDto.getPanchayatName() : "");
                values.put(TableConstants.LASTTRANSACTIONDATE, (shgDto.getLastTransactionDate() != null && shgDto.getLastTransactionDate().length() > 0) ? shgDto.getLastTransactionDate() : "");
                values.put(TableConstants.DISTRICT_ID, (shgDto.getDistrictId() != null && shgDto.getDistrictId().length() > 0) ? shgDto.getDistrictId() : "");
                //   values.put(TableConstants.MODIFIEDDATE, System.currentTimeMillis() + "");
              /*  if (MySharedPreference.readString(EShaktiApplication.getInstance(), MySharedPreference.SHG_ID, "") != null && MySharedPreference.readString(EShaktiApplication.getInstance(), MySharedPreference.SHG_ID, "").equals(shgDto.getShgId().trim())) {
                    if (MySharedPreference.readBoolean(EShaktiApplication.getInstance(), MySharedPreference.LOGOUT, false))
                        values.put(TableConstants.FIRST_SFLAG, "1");
                    else if (MySharedPreference.readBoolean(EShaktiApplication.getInstance(), MySharedPreference.SINGIN_DIFF, false))
                        values.put(TableConstants.FIRST_SFLAG, "0");
                }*/


                values.put(TableConstants.ACTIVEFLAG, (shgDto.isVerified()) ? shgDto.getLastTransactionDate() : "false");
                values.put(TableConstants.PRESIDENT_NAME, (shgDto.getPresidentName() != null && shgDto.getPresidentName().length() > 0) ? shgDto.getPresidentName() : "NA");

                database.insertWithOnConflict(TableName.TABLE_GROUP_NAME_DETAILS, TableConstants.SHG_ID, values, SQLiteDatabase.CONFLICT_REPLACE);
            } catch (Exception e) {
                Log.e("insertSHGDetails", e.toString());
            } finally {
                closeDatabase();
            }
        }

    }

    public static void updateSHGDetails(CashOfGroup rDto, String shgId) {

        if (rDto != null) {
            try {
                String CHvalue = "" + "0", CBvalue = "" + "0";
                if (rDto.getCashInHand()!=null && rDto.getCashInHand().length() > 0 && rDto.getCashAtBank()!=null && rDto.getCashAtBank().length() > 0) {
                    CHvalue = (int) Double.parseDouble(rDto.getCashInHand()) + "";
                    CBvalue = "" + (int) Double.parseDouble(rDto.getCashAtBank());
                }
                openDatabase();
                ContentValues values = new ContentValues();
                values.put(TableConstants.CASH_IN_HAND, ((CHvalue + "") != null && (CHvalue + "").length() > 0) ? (CHvalue + "") : "");
                values.put(TableConstants.CASH_AT_BANK, ((CBvalue + "") != null && (CBvalue + "").length() > 0) ? (CBvalue + "") : "");
                values.put(TableConstants.LASTTRANSACTIONDATE, (rDto.getLastTransactionDate() != null && rDto.getLastTransactionDate().length() > 0) ? rDto.getLastTransactionDate() : "NA");
                database.update(TableName.TABLE_GROUP_NAME_DETAILS, values, TableConstants.SHG_ID + " = '" + shgId + "'", null);
            } catch (Exception e) {
                Log.e("insertEnergyException", e.toString());
            } finally {
                closeDatabase();
            }
        }

    }

    public static void updateTransactionSHGDetails(String shgId) {

        try {
            openDatabase();
            ContentValues values = new ContentValues();
            values.put(TableConstants.MODIFIEDDATE, System.currentTimeMillis() + "");
            values.put(TableConstants.FIRST_SFLAG, "1");
            database.update(TableName.TABLE_GROUP_NAME_DETAILS, values, TableConstants.SHG_ID + " = '" + shgId + "'", null);
        } catch (Exception e) {
            Log.e("insertEnergyException", e.toString());
        } finally {
            closeDatabase();
        }


    }

    public static void updateSIgnInDiffUserDetails(String shgId) {

        try {

            openDatabase();
            ContentValues values = new ContentValues();
            values.put(TableConstants.MODIFIEDDATE, System.currentTimeMillis() + "");
            values.put(TableConstants.FIRST_SFLAG, "0");
            database.update(TableName.TABLE_GROUP_NAME_DETAILS, values, TableConstants.SHG_ID + " = '" + shgId + "'", null);
        } catch (Exception e) {
            Log.e("insertEnergyException", e.toString());
        } finally {
            closeDatabase();
        }


    }


    public static ArrayList<ListOfShg> getSHGDetails() {
        ArrayList<ListOfShg> shgList = new ArrayList<>();

        try {
            openDatabase();

            String selectQuery = "SELECT  * FROM " + TableName.TABLE_GROUP_NAME_DETAILS;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    ListOfShg shg = new ListOfShg();
                    shg.setCode(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_CODE)));
                    shg.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    shg.setId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    shg.setName(cursor.getString(cursor.getColumnIndex(TableConstants.NAME)));
                    shg.setCashAtBank(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_AT_BANK)));
                    shg.setCashInHand(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_IN_HAND)));
                    shg.setGroupFormationDate(cursor.getString(cursor.getColumnIndex(TableConstants.GRP_FORMATION_DATE)));
                    shg.setLastTransactionDate(cursor.getString(cursor.getColumnIndex(TableConstants.LASTTRANSACTIONDATE)));
                    shg.setOpeningDate(cursor.getString(cursor.getColumnIndex(TableConstants.OPENINGDATE)));
                    shg.setModifiedDate(cursor.getString(cursor.getColumnIndex(TableConstants.MODIFIEDDATE)));
                    shg.setFFlag(cursor.getString(cursor.getColumnIndex(TableConstants.FIRST_SFLAG)));
                    shg.setPanchayatName(cursor.getString(cursor.getColumnIndex(TableConstants.PANCHAYATNAME)));
                    shg.setBlockName(cursor.getString(cursor.getColumnIndex(TableConstants.BLOCKNAME)));
                    shg.setVillageName(cursor.getString(cursor.getColumnIndex(TableConstants.VILLAGENAME)));
                    shg.setDistrictId(cursor.getString(cursor.getColumnIndex(TableConstants.DISTRICT_ID)));
                    shg.setPresidentName(cursor.getString(cursor.getColumnIndex(TableConstants.PRESIDENT_NAME)));
                    shgList.add(shg);

                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }
        return shgList;

    }


    public static ListOfShg getSHGDetails(String shg_id) {
        ListOfShg shg = new ListOfShg();
        try {
            openDatabase();

            String selectQuery = "SELECT  * FROM " + TableName.TABLE_GROUP_NAME_DETAILS + " where " + TableConstants.SHG_ID + " LIKE '" + shg_id + "'";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    shg.setCode(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_CODE)));
                    shg.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    shg.setId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    shg.setName(cursor.getString(cursor.getColumnIndex(TableConstants.NAME)));
                    shg.setCashAtBank(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_AT_BANK)));
                    shg.setCashInHand(cursor.getString(cursor.getColumnIndex(TableConstants.CASH_IN_HAND)));
                    shg.setGroupFormationDate(cursor.getString(cursor.getColumnIndex(TableConstants.GRP_FORMATION_DATE)));
                    shg.setOpeningDate(cursor.getString(cursor.getColumnIndex(TableConstants.OPENINGDATE)));
                    shg.setLastTransactionDate(cursor.getString(cursor.getColumnIndex(TableConstants.LASTTRANSACTIONDATE)));
                    shg.setModifiedDate(cursor.getString(cursor.getColumnIndex(TableConstants.MODIFIEDDATE)));
                    shg.setFFlag(cursor.getString(cursor.getColumnIndex(TableConstants.FIRST_SFLAG)));
                    shg.setPanchayatName(cursor.getString(cursor.getColumnIndex(TableConstants.PANCHAYATNAME)));
                    shg.setBlockName(cursor.getString(cursor.getColumnIndex(TableConstants.BLOCKNAME)));
                    shg.setVillageName(cursor.getString(cursor.getColumnIndex(TableConstants.VILLAGENAME)));
                    shg.setDistrictId(cursor.getString(cursor.getColumnIndex(TableConstants.DISTRICT_ID)));
                    shg.setPresidentName(cursor.getString(cursor.getColumnIndex(TableConstants.PRESIDENT_NAME)));

                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }
        return shg;

    }


    public static void deleteSHG(String userId) {
        try {
            openDatabase();
            database.execSQL("DELETE FROM " + TableName.TABLE_GROUP_NAME_DETAILS);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

    }


}
