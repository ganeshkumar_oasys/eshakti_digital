package com.oasys.eshakti.digitization.Dto;

import android.database.Cursor;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class AadhaarPayDto {
    @SerializedName("RetailerTxnId")
    String RetailerTxnId;

    @SerializedName("acquirerInstitutationId")
    String acquirerInstitutationId;

    @SerializedName("aadharNumber")
    String aadharNumber;

   /*//@SerializedName("IIN")
    IINDto IIN;*/

    @SerializedName("iin")
    String iin;

    @SerializedName("amount")
    String amount;

    @SerializedName("RetailerId")
    String RetailerId;

    @SerializedName("biometricData")
    String biometricData;

    @SerializedName("TxnTypeCode")
    String TxnTypeCode;

    @SerializedName("AppId")
    String AppId;

    @SerializedName("AppVersion")
    String AppVersion;

    @SerializedName("CustomerConsent")
    String CustomerConsent;

    @SerializedName("Param1")
    String Param1;

    @SerializedName("Param2")
    String Param2;

    @SerializedName("Param3")
    String Param3;

    @SerializedName("Param4")
    String Param4;

    @SerializedName("Param5")
    String Param5;




    public AadhaarPayDto() {

    }

    public AadhaarPayDto(Cursor cursor) {

    }
}
