package com.oasys.eshakti.digitization.Dto;


import java.util.ArrayList;

import lombok.Data;

@Data
public class HistoryDto {

    int statusCode;
    String message;
    ArrayList<Wallet_Bank_CW> responseContents;
}
