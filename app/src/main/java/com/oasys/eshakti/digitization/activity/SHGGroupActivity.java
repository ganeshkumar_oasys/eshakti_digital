package com.oasys.eshakti.digitization.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.oasys.eshakti.digitization.Adapter.CustomExpandableGroupListAdapter;
import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.Dto.ResponseContent;
import com.oasys.eshakti.digitization.Dto.ShgBankDetails;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.ExpandListItemClickListener;
import com.oasys.eshakti.digitization.OasysUtils.GetExit;
import com.oasys.eshakti.digitization.OasysUtils.GroupListItem;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.database.BankTable;
import com.oasys.eshakti.digitization.database.LoginTable;
import com.oasys.eshakti.digitization.database.SHGTable;
import com.oasys.eshakti.digitization.views.ButtonFlat;
import com.oasys.eshakti.digitization.views.MyExpandableListview;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;



public class SHGGroupActivity extends AppCompatActivity implements NewTaskListener, ExpandListItemClickListener {

    Toolbar mToolbar;
    Context context;

    private Dialog mProgressDialog;

    private List<GroupListItem> listItems;
    // private GroupListAdapter mAdapter;
    private CustomExpandableGroupListAdapter mAdapter;
    int listImage, leftImage;


    public static ArrayList<String> mGroupIdlist = new ArrayList<String>();
    boolean isNavigateGroupProfile = false;
    public static String mLanguageLocalae;
    // ExpandableLayoutListView expandableLayoutListView;
    MyExpandableListview expandableLayoutListView;

    private ArrayList<HashMap<String, String>> childList;
    private int lastExpandedPosition = -1;
    private NetworkConnection networkConnection;
    public  String userId;
    private ResponseDto gdto;
    private TextView mTitle;
    private String nameStr, mobStr;
    private String langId;
    private String shggroupId;
    public static  String vpanumber;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_expandablelistview);

        nameStr = MySharedPreference.readString(SHGGroupActivity.this, MySharedPreference.ANIMATOR_NAME, "");
        mobStr = MySharedPreference.readString(SHGGroupActivity.this, MySharedPreference.USERNAME, "");
        userId = MySharedPreference.readString(SHGGroupActivity.this, MySharedPreference.ANIMATOR_ID, "");
        langId = MySharedPreference.readString(SHGGroupActivity.this, MySharedPreference.LANG_ID, "");
        callShgList(userId, langId);

        expandableLayoutListView = (MyExpandableListview) findViewById(R.id.listview);

        try {
            mToolbar = (Toolbar) findViewById(R.id.toolbar_grouplist);
            mTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("");
            mTitle.setText(nameStr + "    " + mobStr);
            //     mTitle.setTypeface(LoginActivity.sTypeface);
            mTitle.setGravity(Gravity.CENTER);

            Toolbar mToolBar_Target = (Toolbar) findViewById(R.id.toolbar_grouplist_target);
            TextView mTargetValues = (TextView) mToolBar_Target.findViewById(R.id.toolbar_title_target);
            mTargetValues.setText(
                    "TARGET : " + 0 + "  COMPLETED : " + 0 + "  PENDING : " + 0);
            mTargetValues.setGravity(Gravity.CENTER);

            context = this.getApplicationContext();

            listItems = new ArrayList<GroupListItem>();
            listImage = R.drawable.ic_navigate_next_white_24dp;
            leftImage = R.drawable.star;
            expandableLayoutListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

                @Override
                public void onGroupExpand(int groupPosition) {
                    if (lastExpandedPosition != -1 && groupPosition != lastExpandedPosition) {
                        expandableLayoutListView.collapseGroup(lastExpandedPosition);
                    }
                    lastExpandedPosition = groupPosition;

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void callShgList(String id, String lang_id) {
        networkConnection = NetworkConnection.getNetworkConnection(getApplicationContext());
       /* ResponseContent dto = new ResponseContent();
        dto.setUserId(MySharedPreference.readString(SHGGroupActivity.this, MySharedPreference.ANIMATOR_ID, ""));
        if (dto == null && dto.getUserId().length() <= 0) {
            return;
        }
        String loginReqJson = new Gson().toJson(dto);
        Log.e("Log in REQUEST ", " ::::::::: " + loginReqJson);
*/
        if (networkConnection.isNetworkAvailable()) {
            EShaktiApplication.setOfflineTrans(false);

            onTaskStarted();
            String url = Constants.BASE_URL + Constants.SHG_URL + userId + "&languageId=" + lang_id;
            RestClient.getRestClient(SHGGroupActivity.this).callWebServiceForGetMethod(url, SHGGroupActivity.this, ServiceType.SHG_LIST);

        } else {

            if (!EShaktiApplication.isOfflineTrans()) {
                EShaktiApplication.setOfflineTrans(true);
                try {
                    ResponseDto lrDto = new ResponseDto();
                    ResponseContent rCont = new ResponseContent();
                    ArrayList<ListOfShg> newArrayList = SHGTable.getSHGDetails();
                    rCont.setListOfShg(newArrayList);
                    lrDto.setResponseContent(rCont);
                    CommonSetRecyclerview(lrDto);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {

            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_ob_group, menu);
        MenuItem item = menu.getItem(0);
        item.setVisible(true);
        SpannableStringBuilder logOutBuilder = new SpannableStringBuilder(AppStrings.logOut);
        if (item.getItemId() == R.id.group_logout_edit) {
            item.setTitle(logOutBuilder);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();
        if (id == R.id.group_logout_edit) {
            Log.e("Group Logout", "Logout Sucessfully");

           // AppDialogUtils.showConfirmation_LogoutDialog(SHGGroupActivity.this);
            showConfirmation_LogoutDialog1(SHGGroupActivity.this);

            //startActivity(new Intent(GetExit.getExitIntent(getApplicationContext())));
            // this.finish();

            return true;

        }

        return super.onOptionsItemSelected(item);
    }


    public void showConfirmation_LogoutDialog1(final Activity activity) {

        try {
            final Dialog confirmationDialog = new Dialog(activity);

            LayoutInflater li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View customView = li.inflate(R.layout.dialog_new_logout, null, false);
            TextView mHeaderView, mMessageView;

            mHeaderView = (TextView) customView.findViewById(R.id.dialog_Title_logout);
            mMessageView = (TextView) customView.findViewById(R.id.dialog_Message_logout);


            final ButtonFlat mConYesButton, mConNoButton;

            mConYesButton = (ButtonFlat) customView.findViewById(R.id.fragment_ok_button_alert_logout);
            mConNoButton = (ButtonFlat) customView.findViewById(R.id.fragment_cancel_button_alert_logout);

            mConYesButton.setText(AppStrings.dialogOk);
            mConYesButton.setTypeface(LoginActivity.sTypeface);
            mConNoButton.setTypeface(LoginActivity.sTypeface);
            mConNoButton.setText(AppStrings.dialogNo);

            mHeaderView.setText(AppStrings.logOut);
            mHeaderView.setTypeface(LoginActivity.sTypeface);
            mHeaderView.setVisibility(View.INVISIBLE);

            if (MySharedPreference.readBoolean(activity, MySharedPreference.UNAUTH, false))
                mMessageView.setText(AppStrings.mUnAuth);
            else
                mMessageView.setText(AppStrings.mAskLogout);
            mMessageView.setTypeface(LoginActivity.sTypeface);


            mConYesButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    confirmationDialog.dismiss();

                    try {

                        if (networkConnection.isNetworkAvailable()) {

                            RestClient.getRestClient(SHGGroupActivity.this).callRestWebServiceForDelete(Constants.BASE_URL + Constants.LOGOUT_TOKENDELETION, SHGGroupActivity.this, ServiceType.LOG_OUT, "");

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            });

            mConNoButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    confirmationDialog.dismiss();
                }
            });

            confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            confirmationDialog.setCanceledOnTouchOutside(false);
            confirmationDialog.setContentView(customView);
            confirmationDialog.setCancelable(false);
            confirmationDialog.show();

        } catch (Exception E) {
            E.printStackTrace();
        }

    }

    @SuppressLint("LongLogTag")
    @Override
    public void onItemClick(ViewGroup parent, View view, int position) {
        // TODO Auto-generated method stub

        String Shgid = gdto.getResponseContent().getListOfShg().get(position).getId();
        String ltd = gdto.getResponseContent().getListOfShg().get(position).getLastTransactionDate();
        String cih = gdto.getResponseContent().getListOfShg().get(position).getCashInHand();
        String cth = gdto.getResponseContent().getListOfShg().get(position).getCashAtBank();


        MySharedPreference.writeString(SHGGroupActivity.this, MySharedPreference.SHG_ID, Shgid);
        MySharedPreference.writeString(SHGGroupActivity.this, MySharedPreference.LAST_TRANSACTION, ltd);
        MySharedPreference.writeString(SHGGroupActivity.this, MySharedPreference.CASHATBANK, cih);
        MySharedPreference.writeString(SHGGroupActivity.this, MySharedPreference.CASHINHAND, cth);


        MySharedPreference.writeInteger(SHGGroupActivity.this, MySharedPreference.MENU_SELECTED, 0);
        Intent intent_ = new Intent(SHGGroupActivity.this, NewDrawerScreen.class);
        intent_.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent_.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent_);
        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);
        finish();
    }

    @Override
    public void onItemClickVerification(ViewGroup parent, View view, int position) {
        String Shgid = gdto.getResponseContent().getListOfShg().get(position).getId();
        String ltd = gdto.getResponseContent().getListOfShg().get(position).getLastTransactionDate();
        String cih = gdto.getResponseContent().getListOfShg().get(position).getCashInHand();
        String cth = gdto.getResponseContent().getListOfShg().get(position).getCashAtBank();

        MySharedPreference.writeString(SHGGroupActivity.this, MySharedPreference.SHG_ID, Shgid);
        MySharedPreference.writeString(SHGGroupActivity.this, MySharedPreference.LAST_TRANSACTION, ltd);
        MySharedPreference.writeString(SHGGroupActivity.this, MySharedPreference.CASHATBANK, cih);
        MySharedPreference.writeString(SHGGroupActivity.this, MySharedPreference.CASHINHAND, cth);
        MySharedPreference.writeInteger(SHGGroupActivity.this, MySharedPreference.MENU_SELECTED, 0);
        Intent intent_ = new Intent(SHGGroupActivity.this, GroupProfileActivity.class);
        intent_.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent_.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent_);
        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);
        finish();
    }


    private void CommonSetRecyclerview(ResponseDto lrdto) {

        this.gdto = lrdto;
        // TODO Auto-generated method stub
        ArrayList<ListOfShg> listSHG = lrdto.getResponseContent().getListOfShg();
        for (int i = 0; i < listSHG.size(); i++) {
            GroupListItem gp = new GroupListItem(leftImage, listSHG.get(i).getName(), listSHG.get(i).getPresidentName(), listImage);
            listItems.add(gp);
        }
        setCustomAdapter(listSHG);
    }

    private void setCustomAdapter(ArrayList<ListOfShg> shgDto) {
        // TODO Auto-generated method stub
        childList = new ArrayList<HashMap<String, String>>();

        for (int i = 0; i < shgDto.size(); i++) {
            HashMap<String, String> temp = new HashMap<String, String>();
            temp.put("SHGCode", (shgDto.get(i).getCode() != null && shgDto.get(i).getCode().length() > 0) ? shgDto.get(i).getCode().toUpperCase() : "NA");
            temp.put("BlockName", (shgDto.get(i).getBlockName() != null && shgDto.get(i).getBlockName().length() > 0) ? shgDto.get(i).getBlockName().toUpperCase() : "NA");
            temp.put("PanchayatName", (shgDto.get(i).getPanchayatName() != null && shgDto.get(i).getPanchayatName().length() > 0) ? shgDto.get(i).getPanchayatName().toUpperCase() : "NA");
            temp.put("VillageName", (shgDto.get(i).getVillageName() != null && shgDto.get(i).getVillageName().length() > 0) ? shgDto.get(i).getVillageName().toUpperCase() : shgDto.get(i).getPanchayatName());
            temp.put("LastTransactionDate", (shgDto.get(i).getLastTransactionDate() != null && shgDto.get(i).getLastTransactionDate().length() > 0) ? shgDto.get(i).getLastTransactionDate().toUpperCase() : "NA");

            temp.put("SHGCode_Label", "SHG Code");
            temp.put("BlockName_Label", "Block Name");
            temp.put("PanchayatName_Label", "Panchayat Name");
            temp.put("VillageName_Label", "Village Name");
            temp.put("LastTransactionDate_Label", "Last Transaction Date");
            childList.add(temp);
        }

        mAdapter = new CustomExpandableGroupListAdapter(SHGGroupActivity.this, shgDto, listItems, childList, this);
        expandableLayoutListView.setAdapter(mAdapter);

    }


    @Override
    public void onTaskStarted() {
        mProgressDialog = AppDialogUtils.createProgressDialog(this);
        mProgressDialog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {


        switch (serviceType) {
            case SHG_LIST:
                if (result != null && result.length() > 0) {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    ResponseDto lrDto = gson.fromJson(result, ResponseDto.class);
                    int statusCode = lrDto.getStatusCode();
                    String message = lrDto.getMessage();
                    Log.d("response status", " " + statusCode);
                    if (statusCode == 400 || statusCode == 403 || statusCode == 500 || statusCode == 503) {
                        // showMessage(statusCode);
                        Utils.showToast(this, message);
                        if (mProgressDialog != null) {
                            mProgressDialog.dismiss();
                        }

                    } else if (statusCode == 401) {

                        Log.e("Group Logout", "Logout Sucessfully");
                        AppDialogUtils.showConfirmation_LogoutDialog(SHGGroupActivity.this);
                        if (mProgressDialog != null) {
                            mProgressDialog.dismiss();
                        }
                    } else if (statusCode == Utils.Success_Code) {
                        Utils.showToast(this, message);
                        if (lrDto.getResponseContent().getListOfShg().size() > 0) {
                                //  MySharedPreference.writeString(SHGGroupActivity.this, MySharedPreference.ANIMATOR_ID, lrDto.getResponseContent().getAnimatorId());
                                MySharedPreference.writeString(SHGGroupActivity.this, MySharedPreference.ANIMATOR_NAME, lrDto.getResponseContent().getAnimatorName());
                                Log.e("vpaNumber",lrDto.getResponseContent().getVpaNo());
                                MySharedPreference.writeString(SHGGroupActivity.this, MySharedPreference.VPA_NUMBER, lrDto.getResponseContent().getVpaNo());
                                MySharedPreference.writeString(SHGGroupActivity.this, MySharedPreference.WALLET_ID, lrDto.getResponseContent().getWalletId());
                                MySharedPreference.writeString(SHGGroupActivity.this, MySharedPreference.AGENT_ID, lrDto.getResponseContent().getAgentId());
                                try {
                                    if (MySharedPreference.readString(SHGGroupActivity.this, MySharedPreference.ANIMATOR_NAME, "") != null && MySharedPreference.readString(SHGGroupActivity.this, MySharedPreference.ANIMATOR_NAME, "").length() > 0)
                                        mTitle.setText(MySharedPreference.readString(SHGGroupActivity.this, MySharedPreference.ANIMATOR_NAME, "") + "    " + mobStr);
                                    //  mTitle.setText(Name + "    " + mobile);

                                    if (SHGTable.getSHGDetails() != null && SHGTable.getSHGDetails().size() > 0) {

                                        ArrayList<ListOfShg> shgListArray = SHGTable.getSHGDetails();
                                        ArrayList<ListOfShg> resposneSHGListArray = lrDto.getResponseContent().getListOfShg();

                                        ArrayList<String> shgListArrayString = new ArrayList<>();
                                        ArrayList<String> resposneSHGListString = new ArrayList<>();

                                        for (int i = 0; i < shgListArray.size(); i++) {
                                            shgListArrayString.add(shgListArray.get(i).getId());
                                        }

                                        for (int i = 0; i < resposneSHGListArray.size(); i++) {
                                            resposneSHGListString.add(resposneSHGListArray.get(i).getId());
                                        }

                                        Collections.sort(shgListArrayString);
                                        Collections.sort(resposneSHGListString);

                                        boolean isEqual = shgListArrayString.equals(resposneSHGListString);

                                        if (isEqual) {

                                        } else {
                                            SHGTable.deleteSHG(userId);
                                            for (int i = 0; i < lrDto.getResponseContent().getListOfShg().size(); i++) {
                                                ListOfShg lg = lrDto.getResponseContent().getListOfShg().get(i);

                                                SHGTable.insertSHGData(lg, userId);
                                            }
                                        }
                                    } else {
                                        for (int i = 0; i < lrDto.getResponseContent().getListOfShg().size(); i++) {
                                            ListOfShg lg = lrDto.getResponseContent().getListOfShg().get(i);
                                            //  SHGTable.deleteSHG(userId);
                                            SHGTable.insertSHGData(lg, userId);
                                        }
                                    }


                                    //     for (int i = 0; i < lrDto.getResponseContent().getShgBankDetails().size(); i++) {
                                    if (lrDto.getResponseContent().getShgBankDetails().size() > 0) {
                                        for (ShgBankDetails lgl : lrDto.getResponseContent().getShgBankDetails()) {
                                            BankTable.insertBankData(lgl);
                                        }
                                    }
                                    if (mProgressDialog != null) {
                                        mProgressDialog.dismiss();
                                    }

                                    if (lrDto.getResponseContent().getAnimatorId() != null) {
                                        lrDto.getResponseContent().setUserId(userId);
                                        LoginTable.updateAnimatorLogin(lrDto);
                                    }

                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.e("***SHG ACTIVITY **", e.toString());
                            }
                            CommonSetRecyclerview(lrDto);
                        } else {
                            Toast.makeText(SHGGroupActivity.this, "SHG List gets empty", Toast.LENGTH_SHORT).show();
                        }


                    } else {
                        Utils.showToast(this, message);
                        if (mProgressDialog != null) {
                            mProgressDialog.dismiss();
                        }

                    }
                }
                break;

            case LOG_OUT:

                if (result != null && result.length() > 0) {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    ResponseDto lrDto = gson.fromJson(result, ResponseDto.class);
                    int statusCode = lrDto.getStatusCode();
                    String message = lrDto.getMessage();
                    Log.d("response status", " " + statusCode);

                    if (statusCode == Utils.Success_Code) {
                        Utils.showToast(this, message);

                         startActivity(new Intent(GetExit.getExitIntent(getApplicationContext())));
                         this.finish();
                    }

                }

                break;

        }


    }


}
