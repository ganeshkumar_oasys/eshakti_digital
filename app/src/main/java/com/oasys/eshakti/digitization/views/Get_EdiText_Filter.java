package com.oasys.eshakti.digitization.views;

import android.text.InputFilter;

public class Get_EdiText_Filter {

	public static InputFilter[] editText_filter() {

		InputFilter[] filter = new InputFilter[1];
		filter[0] = new InputFilter.LengthFilter(10);

		return filter;
	}

	public static InputFilter[] editText_mobile_number_filter() {

		InputFilter[] filter = new InputFilter[1];
		filter[0] = new InputFilter.LengthFilter(10);

		return filter;
	}

	public static InputFilter[] editText_tenure_filter() {

		InputFilter[] filter = new InputFilter[1];
		filter[0] = new InputFilter.LengthFilter(3);

		return filter;
	}

	public static InputFilter[] editText_aadhaar_number_filter() {

		InputFilter[] filter = new InputFilter[1];
		filter[0] = new InputFilter.LengthFilter(12);

		return filter;
	}

	public static InputFilter[] editText_AccNo_filter() {

		InputFilter[] filter = new InputFilter[1];
		filter[0] = new InputFilter.LengthFilter(20);

		return filter;
	}
}
