package com.oasys.eshakti.digitization.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.model.ListItem;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class TR_SUmmary_Adapter extends RecyclerView.Adapter<TR_SUmmary_Adapter.MemberHolderDetails> {

    private Context context;
    private ArrayList<ListItem> responseSaving_transaction_details;

    public TR_SUmmary_Adapter(Context context, ArrayList<ListItem> responseSaving_transaction_details) {
        this.context = context;
        this.responseSaving_transaction_details = responseSaving_transaction_details;
    }

    @NonNull
    @Override
    public MemberHolderDetails onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.digital_tr_summary, parent, false);
        return new MemberHolderDetails(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MemberHolderDetails holder, int position) {


        DateFormat simple = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat df1 = new SimpleDateFormat("HH:mm:ss");
        Date d = new Date(Long.parseLong(responseSaving_transaction_details.get(position).getTransactionDate()));
        String time = df1.format(new Date(Long.parseLong(responseSaving_transaction_details.get(position).getTransactionDate())));
        String dateStr = simple.format(d);
        holder.tx_date.setText(AppStrings.tx_Date + dateStr + " " + time);
        String boldamt = AppStrings.amount + "<b>" + responseSaving_transaction_details.get(position).getAmount() + "</b> ";
        holder.amt.setText((Html.fromHtml(boldamt)));
        holder.tx_id.setText(AppStrings.tx_id + responseSaving_transaction_details.get(position).getTransactionId());
        holder.refId.setText(AppStrings.refId + responseSaving_transaction_details.get(position).getReferenceId());
        String first = AppStrings.txStatus;
        if (responseSaving_transaction_details.get(position).getTransactionStatus().toLowerCase().equals("success")) {
            String next = "<font color='#2f9e44'>" + responseSaving_transaction_details.get(position).getTransactionStatus() + "</font>";
            holder.txStatus.setText(Html.fromHtml(first + next));
        } else {
            String next = "<font color='#EE0000'>" + responseSaving_transaction_details.get(position).getTransactionStatus() + "</font>";
            holder.txStatus.setText(Html.fromHtml(first + next));
        }
        holder.txType.setText(AppStrings.txType + responseSaving_transaction_details.get(position).getTransactionType());
        holder.animName.setText(AppStrings.a_name + ((responseSaving_transaction_details.get(position).getAnimatorName() != null) ? responseSaving_transaction_details.get(position).getAnimatorName() : "NA"));

    }

    @Override
    public int getItemCount() {
        return responseSaving_transaction_details.size();
    }

    class MemberHolderDetails extends RecyclerView.ViewHolder {

        TextView merchantid, tx_date, amt, tx_id, refId, txStatus, txType, animName;

        public MemberHolderDetails(View itemView) {
            super(itemView);

         /*   merchantid = (TextView) itemView.findViewById(R.id.merchantid);
            merchantid.setText(AppStrings.m_id);*/
            //  merchantid.setTypeface(LoginActivity.sTypeface);

            tx_date = (TextView) itemView.findViewById(R.id.tx_date);
            tx_date.setText(AppStrings.tx_Date);
            //   tx_date.setTypeface(LoginActivity.sTypeface);

            amt = (TextView) itemView.findViewById(R.id.amt);
            amt.setText(AppStrings.amount);
            // amt.setTypeface(LoginActivity.sTypeface);

            tx_id = (TextView) itemView.findViewById(R.id.tx_id);
            tx_id.setText(AppStrings.tx_id);
            //   tx_id.setTypeface(LoginActivity.sTypeface);

            refId = (TextView) itemView.findViewById(R.id.refId);
            refId.setText(AppStrings.refId);
            // refId.setTypeface(LoginActivity.sTypeface);

            txStatus = (TextView) itemView.findViewById(R.id.txStatus);
            txStatus.setText(AppStrings.txStatus);
            //   txStatus.setTypeface(LoginActivity.sTypeface);

            txType = (TextView) itemView.findViewById(R.id.txtype);
            txType.setText(AppStrings.txType);
            // txType.setTypeface(LoginActivity.sTypeface);

            animName = (TextView) itemView.findViewById(R.id.animName);
            animName.setText(AppStrings.a_name);
            //animName.setTypeface(LoginActivity.sTypeface);
        }
    }
}
