package com.oasys.eshakti.digitization.Dto;


import lombok.Data;

@Data
public class HistoryDetailsDto {
    String fromDate,merchantId,walletId,userId,agentId;
    String toDate,animatorNameId,shgId,transationtype;

}
