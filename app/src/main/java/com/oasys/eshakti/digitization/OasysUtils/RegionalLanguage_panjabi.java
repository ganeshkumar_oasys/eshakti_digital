package com.oasys.eshakti.digitization.OasysUtils;

import com.yesteam.eshakti.appConstants.AppStrings;

public class RegionalLanguage_panjabi {
	public RegionalLanguage_panjabi() {
		// TODO Auto-generated constructor stub
	}

	public static void RegionalStrings() {

		AppStrings.LoginScreen = "ਲਾਗਇਨ ਸਕਰੀਨ";
		AppStrings.userName = "ਮੋਬਾਈਲ ਨੰਬਰ";
		AppStrings.passWord = "ਪਾਸਵਰਡ";
		AppStrings.transaction = "ਸੰਚਾਰ";
		AppStrings.reports = "ਦੀ ਰਿਪੋਰਟ";
		AppStrings.profile = "ਪ੍ਰੋਫਾਈਲ";
		AppStrings.meeting = "ਮੀਟਿੰਗ ਨੂੰ";
		AppStrings.settings = "ਸੈਟਿੰਗਜ਼";
		AppStrings.help = "ਮਦਦ ਕਰੋ";
		AppStrings.Attendance = "ਹਾਜ਼ਰੀ";
		AppStrings.MinutesofMeeting = "ਮੀਟਿੰਗ ਦੀ ਮਿੰਟ";
		AppStrings.savings = "ਸੇਵਿੰਗਜ਼";
		AppStrings.InternalLoanDisbursement = "ਕਰਜ਼ ਦੀ ਵੰਡ";
		AppStrings.expenses = "ਖਰਚੇ";
		AppStrings.cashinhand = "ਹੱਥ ਵਿਚ ਨਕਦ :  ₹ ";
		AppStrings.cashatBank = "ਬਕ 'ਤੇ ਨਕਦ :  ₹ ";
		AppStrings.memberloanrepayment = "ਸਦੱਸ ਕਰਜ਼ੇ ਦੇਮੁੜ";
		AppStrings.grouploanrepayment = "ਗਰੁੱਪ ਲੋਨ ਦੇਮੁੜ";
		AppStrings.income = "ਆਮਦਨੀ";
		AppStrings.bankTransaction = "ਬਕ ਸੰਚਾਰ";
		AppStrings.fixedDeposit = "ਨਿਸ਼ਚਿਤ ਡਿਪਾਜ਼ਿਟ";
		AppStrings.otherincome = "ਹੋਰ ਆਮਦਨ";
		AppStrings.subscriptioncharges = "ਗਾਹਕੀ";
		AppStrings.penalty = "ਸਜ਼ਾ";
		AppStrings.donation = "ਦਾਨ";
		AppStrings.federationincome = "FEDS";//"ਫੈਡਰੇਸ਼ਨ ਆਮਦਨੀ";
		AppStrings.date = "ਤਾਰੀਖ਼";
		AppStrings.passwordchange = "ਪਾਸਵਰਡ ਬਦਲੋ";
		AppStrings.listofexpenses = "ਖਰਚੇ ਦੀ ਸੂਚੀ";
		AppStrings.transport = "ਆਵਾਜਾਈ";
		AppStrings.snacks = "ਚਾਹ";
		AppStrings.telephone = "ਟੈਲੀਫੋਨ";
		AppStrings.stationary = "ਸਟੇਸ਼ਨਰੀ";
		AppStrings.otherExpenses = "ਹੋਰ ਖਰਚੇ";
		AppStrings.federationIncomePaid = "ਫੈਡਰੇਸ਼ਨ ਆਮਦਨੀ (ਭੁਗਤਾਨ)";
		AppStrings.bankInterest = "ਬਕ ਦਿਲਚਸਪੀ";
		AppStrings.bankExpenses = "ਬਕ ਖਰਚੇ";
		AppStrings.bankrepayment = "ਬਕ ਦੇਮੁੜ";
		AppStrings.total = "ਕੁਲ";
		AppStrings.summary = "ਸਾਰ";
		AppStrings.savingsSummary = "ਸੇਵਿੰਗਜ਼ ਸਾਰ";
		AppStrings.loanSummary = "ਕਰਜ਼ਾ ਸਾਰ";
		AppStrings.lastMonthReport = "ਮਹੀਨਾਵਾਰ ਰਿਪੋਰਟ";
		AppStrings.Memberreports = "ਸਦੱਸ ਦੀ ਰਿਪੋਰਟ";
		AppStrings.GroupReports = "ਗਰੁੱਪ ਦੀ ਰਿਪੋਰਟ";
		AppStrings.transactionsummary = "ਬਕ ਸੰਚਾਰ ਸਾਰ";
		AppStrings.balanceSheet = "ਸੰਤੁਲਨ ਸ਼ੀਟ";
		AppStrings.balanceSheetHeader = "ਸੰਤੁਲਨ ਸ਼ੀਟ 'ਤੇ";
		AppStrings.trialBalance = "ਮੁਕੱਦਮੇ ਦਾ ਸੰਤੁਲਨ";
		AppStrings.outstanding = "ਵਧੀਆ";
		AppStrings.totalSavings = "ਕੁਲ ਸੇਵਿੰਗਜ਼";
		AppStrings.groupSavingssummary = "ਗਰੁੱਪ ਸੇਵਿੰਗ ਸਾਰ";
		AppStrings.groupLoansummary = "ਗਰੁੱਪ ਲੋਨ ਸਾਰ";
		AppStrings.amount = "ਰਕਮ";
		AppStrings.principleAmount = "ਰਕਮ";
		AppStrings.interest = "ਦਿਲਚਸਪੀ";
		AppStrings.groupLoan = "ਗਰੁੱਪ ਲੋਨ";
		AppStrings.balance = "ਬਕਾਇਆ";
		AppStrings.Name = "ਨਾਮ";
		AppStrings.Repaymenybalance = "ਬਕਾਇਆ";
		AppStrings.details = "ਵੇਰਵੇ";
		AppStrings.payment = "ਭੁਗਤਾਨ";
		AppStrings.receipt = "ਹੇਠ ਪ੍ਰਾਪਤ";
		AppStrings.FromDate = "ਮਿਤੀ ਤੱਕ";
		AppStrings.ToDate = "ਮਿਤੀ ਤੱਕ";
		AppStrings.fromDate = "ਤ";
		AppStrings.toDate = "ਲਈ";
		AppStrings.OldPassword = "ਪੁਰਾਣਾ ਪਾਸਵਰਡ";
		AppStrings.NewPassword = "ਨਵਾਂ ਪਾਸਵਰਡ";
		AppStrings.ConfirmPassword = "ਪਾਸਵਰਡ ਪੱਕਾ ਕਰੋ";
		AppStrings.agentProfile = "ਏਜੰਟ ਪ੍ਰੋਫਾਈਲ";
		AppStrings.groupProfile = "ਗਰੁੱਪ ਪ੍ਰੋਫਾਈਲ";
		AppStrings.bankDeposit = "ਬਕ ਪੇਸ਼ਗੀ";
		AppStrings.withdrawl = "ਕਢਵਾਉਣਾ";
		AppStrings.OutstandingAmount = "ਵਧੀਆ";
		AppStrings.GroupLogin = "ਗਰੁੱਪ ਲਾਗਇਨ";
		AppStrings.AgentLogin = "ਏਜੰਟ ਲਾਗਇਨ";
		AppStrings.AboutLicense = "ਲਾਇਸਸ";
		AppStrings.contacts = "ਸੰਪਰਕ";
		AppStrings.contactNo = "ਸੰਪਰਕ ਨਹ";
		AppStrings.cashFlowStatement = "ਨਕਦ ਵਹਾਅ ਬਿਆਨ";
		AppStrings.IncomeExpenditure = "ਆਮਦਨੀ ਖਰਚੇ";
		AppStrings.transactionCompleted = "ਲੈਣਦੇਣ ਪੂਰਾ";
		AppStrings.loginAlert = "ਯੂਜ਼ਰ ਅਤੇ ਪਾਸਵਰਡ ਗਲਤ ਹਨ";
		AppStrings.loginUserAlert = "ਮੁਹੱਈਆ ਉਪਭੋਗੀ ਵੇਰਵਾ";
		AppStrings.loginPwdAlert = "ਮੁਹੱਈਆ ਪਾਸਵਰਡ ਵੇਰਵੇ";
		AppStrings.pwdChangeAlert = "ਪਾਸਵਰਡ ਠੀਕ ਬਦਲ ਦਿੱਤਾ ਗਿਆ ਹੈ";
		AppStrings.pwdIncorrectAlert = "ਸਹੀ ਪਾਸਵਰਡ ਭਰੋ";
		AppStrings.groupRepaidAlert = "ਆਪਣੇ ਬਕਾਇਆ ਰਕਮ ਦੀ ਜਾਂਚ ਕਰੋ";//"ਤੁਹਾਡੀ ਰਕਮ ਲਗਾਓ";
		AppStrings.cashinHandAlert = "ਹੱਥ ਵਿਚ ਨਕਦ ਲਗਾਓ";
		AppStrings.cashatBankAlert = "ਬਕ 'ਤੇ ਨਕਦ ਲਗਾਓ";
		AppStrings.nullAlert = "ਮੁਹੱਈਆ ਨਕਦ ਵੇਰਵੇ";
		AppStrings.DepositnullAlert = "ਮੁਹੱਈਆ ਸਾਰੇ ਨਕਦ ਵੇਰਵਾ";
		AppStrings.MinutesAlert = "ਿਕਰਪਾ ਕਰਕੇ ਉੱਪਰਲੀ ਜੋਕੋਈ ਚੁਣੋ";
		AppStrings.afterDate = "ਇਸ ਤਾਰੀਖ ਦੇ ਬਾਅਦ ਇੱਕ ਮਿਤੀ ਚੁਣੋ";
		AppStrings.last5Transaction = "ਪਿਛਲੇ ਪੰਜ ਲੈਣ-ਦੇਣ";
		AppStrings.internetError = "ਚੈੱਕ ਕਰੋ ਜੀ ਆਪਣੇ ਇੰਟਰਨੈੱਟ ਕੁਨੈਕਸ਼ਨ";
		AppStrings.lastTransactionDate = "ਆਖਰੀ ਸੰਚਾਰ ਿਮਤੀ";
		AppStrings.yes = "ਠੀਕ ਹੈ";
		AppStrings.credit = "ਕ੍ਰੇਡਿਟ";
		AppStrings.debit = "ਡੇਬਿਟ";
		AppStrings.memberName = "ਸਦੱਸ ਨਾਮ";
		AppStrings.OutsatndingAmt = "ਵਧੀਆ";
		AppStrings.calFromToDateAlert = "ਦੀ ਚੋਣ ਕਰੋ ਦੋਨੋ ਿਮਤੀ ਦੇ";
		AppStrings.selectedDate = "ਚੁਣੀ ਤਾਰੀਖ";
		AppStrings.calAlert = "ਵੱਖਰੀ ਿਮਤੀ ਚੁਣੋ";
		AppStrings.dialogMsg = "ਤੁਹਾਨੂੰ ਇਸ ਮਿਤੀ ਨਾਲ ਜਾਰੀ ਰੱਖਣਾ ਚਾਹੁੰਦੇ ਹੋ";
		AppStrings.dialogOk = "ਹ";
		AppStrings.dialogNo = "ਕੋਈ";

		AppStrings.aboutYesbooks = "EShakti ਜ ਸਵੈ ਦੇ ਡਿਜ਼ੀਟਾਈਜ਼ੇਸ਼ਨ ਸਾਡੇ ਮਾਨਯੋਗ ਪ੍ਰਧਾਨ ਮੰਤਰੀ ਦਾ ਬਿਆਨ, ਦੇ ਨਾਲ ਲਾਈਨ ਵਿੱਚ ਨਾਬਾਰਡ ਦੇ ਮਾਈਕਰੋ ਕ੍ਰੈਡਿਟ ਅਤੇ ਅਵਿਸ਼ਕਾਰ ਵਿਭਾਗ ਦੇ ਇਕ ਪਹਿਲ ਹੈ '"
				+ "' ਸਾਨੂੰ ਇਲੈਕਟ੍ਰਾਨਿਕ ਡਿਜ਼ੀਟਲ ਭਾਰਤ ਦੇ ਸੁਪਨੇ ਨਾਲ ਜਾਣ ... '. ਡਿਜੀਟਲ ਭਾਰਤ ਨੂੰ ਭਾਰਤ ਸਰਕਾਰ ਦੀ ਇੱਕ ਰੁਪਏ ਦੀ 1.13 ਲੱਖ ਕਰੋੜ ਰੁਪਏ ਪਹਿਲ ਸਰਕਾਰ ਨੇ ਵਿਭਾਗ ਅਤੇ ਭਾਰਤ ਦੇ ਲੋਕ ਜੋੜ ਕਰਨ ਅਤੇ ਪ੍ਰਭਾਵਸ਼ਾਲੀ ਪ੍ਰਸ਼ਾਸਨ ਨੂੰ ਯਕੀਨੀ ਬਣਾਉਣ ਲਈ ਹੈ. "
				+ "ਇਹ ਡਿਜ਼ੀਟਲ ਅਧਿਕਾਰ ਸਮਾਜ ਅਤੇ ਗਿਆਨ ਦੀ ਆਰਥਿਕਤਾ ਵਿੱਚ ਭਾਰਤ ਨੂੰ ਬਦਲ ਲਈ ਹੈ.";
		AppStrings.groupList = "ਸਮੂਹ";// GROUP LIST
		AppStrings.login = "   ਲਾਗਿਨ   ";
		AppStrings.submit = "ਪੇਸ਼ ਕਰੋ";
		AppStrings.edit = "ਸੰਪਾਦਿਤ";
		AppStrings.bankBalance = "ਬਕ ਸੰਤੁਲਨ";
		AppStrings.balanceAsOn = " ਤੌਰ 'ਤੇ ਸੰਤੁਿਲਤ : ";
		AppStrings.savingsBook = " ਬਚਤ ਖਾਤਾ ਪ੍ਰਤੀ ਕਿਤਾਬ ਦੇ ਰੂਪ : ";
		AppStrings.savingsBank = "  ਸੇਵਿੰਗਜ਼ ਪ੍ਰਤੀ ਬਕ ਖਾਤੇ : ";
		AppStrings.difference = " ਫਰਕ : ";
		AppStrings.loanOutstandingBook = " ਵਧੀਆ ਪ੍ਰਤੀ ਕਿਤਾਬ ਦੇ ਰੂਪ : ";
		AppStrings.loanOutstandingBank = " ਵਧੀਆ ਪ੍ਰਤੀ ਬਕ : ";
		AppStrings.values = new String[] { "ਨੇਟਿਵ (ਪਿੰਡ).",

				"ਸਾਰੇ ਅੰਗ ਮੌਜੂਦ ਹਨ.",

				"ਬਚਤ ਦੀ ਰਕਮ ਇਕੱਠੀ ਕੀਤੀ.",

				"ਨਿੱਜੀ ਲੋਨ ਵੰਡਣ ਬਾਰੇ ਚਰਚਾ.",

				"ਅਦਾਇਗੀ ਦੀ ਰਕਮ ਅਤੇ ਵਿਆਜ ਇਕੱਠੀ ਕੀਤੀ.",

				"ਸਿਖਲਾਈ ਹੋਣ ਬਾਰੇ ਚਰਚਾ.",

				"ਬਕ ਲੋਨ ਪ੍ਰਾਪਤ ਕਰਨ ਦਾ ਫੈਸਲਾ ਕੀਤਾ.",

				"ਆਮ ਵਿਚਾਰ ਬਾਰੇ ਚਰਚਾ.",

				"ਸਫ਼ਾਈ ਬਾਰੇ ਚਰਚਾ.",

				"ਸਿੱਖਿਆ ਦੀ ਮਹੱਤਤਾ ਬਾਰੇ ਚਰਚਾ." };

		AppStrings.January = "ਜਨਵਰੀ";
		AppStrings.February = "ਫਰਵਰੀ";
		AppStrings.March = "ਮਾਰਚ";
		AppStrings.April = "ਅਪ੍ਰੈਲ";
		AppStrings.May = "ਮਈ";
		AppStrings.June = "ਜੂਨ";
		AppStrings.July = "ਜੁਲਾਈ";
		AppStrings.August = "ਅਗਸਤ";
		AppStrings.September = "ਸਤੰਬਰ";
		AppStrings.October = "ਅਕਤੂਬਰ";
		AppStrings.November = "ਨਵੰਬਰ";
		AppStrings.December = "ਦਸੰਬਰ";
		AppStrings.uploadPhoto = "ਫੋਟੋ ਅਪਲੋਡ";
		AppStrings.Il_Disbursed = "ਅੰਦਰੂਨੀ ਕਰਜ਼ੇ ਵੰਡੇ";
		AppStrings.IL_Loan = "ਅੰਦਰੂਨੀ ਕਰਜ਼ਾ";
		AppStrings.Il_Repaid = "ਅੰਦਰੂਨੀ ਕਰਜ਼ਾ ਵਾਪਸ";
		AppStrings.offlineTransactionCompleted = "ਤੁਹਾਡੇ ਔਫਲਾਈਨ ਸੰਚਾਰ ਨੂੰ ਸਫਲਤਾਪੂਰਕ ਨੂੰ ਸੰਭਾਲਿਆ ਗਿਆ ਹੈ";
		AppStrings.offlineTransactionreports = "ਔਫਲਾਈਨ ਸੰਚਾਰ ਦੀ ਰਿਪੋਰਟ";
		AppStrings.offlineTransaction = "ਔਫਲਾਈਨ ਲੈਣ";
		AppStrings.disconnectInternet = "ਿਕਰਪਾ ਕਰਕੇ ਆਪਣੇ ਇੰਟਰਨੈੱਟ ਕੁਨੈਕਸ਼ਨ ਬੰਦ ਔਫਲਾਈਨ ਸੇਵਾ ਜਾਰੀ ਰੱਖਣ ਦੀ";
		AppStrings.Dashboard = "ਡੈਸ਼ਬੋਰਡ";
		AppStrings.bankTransactionSummary = "ਬਕ ਸੰਚਾਰ ਸਾਰ";
		AppStrings.BL_Disbursed = "ਵੰਡੇ";
		AppStrings.BL_Repaid = "ਵਾਪਸ";
		AppStrings.auditing = "ਆਡਿਟਿੰਗ";
		AppStrings.training = "ਟ੍ਰੇਨਿੰਗ";
		AppStrings.auditor_Name = "ਆਡੀਟਰ ਨਾਮ";
		AppStrings.auditing_Date = "ਆਡਿਟਿੰਗ ਿਮਤੀ";
		AppStrings.training_Date = "ਟ੍ਰੇਨਿੰਗ ਿਮਤੀ";
		AppStrings.auditing_Period = "ਲੇਿਾ ਅੰਤਰਾਲ ਚੁਣੋ";
		AppStrings.training_types = new String[] { "ਸਿਹਤ ਜਾਗਰੂਕਤਾ", "ਕਿਤਾਬ ਵਿਕਟਕੀਪਰ ਜਾਗਰੂਕਤਾ", "ਰੋਜ਼ੀ ਜਾਗਰੂਕਤਾ",
				"ਸੋਸ਼ਲ ਜਾਗਰੂਕਤਾ", "ਵਿਦਿਅਕ ਜਾਗਰੂਕਤਾ" };
		AppStrings.changeLanguage = "ਭਾਸ਼ਾ ਬਦਲਣ";
		AppStrings.interestRepayAmount = "ਮੌਜੂਦਾ ਹੋਣ";
		AppStrings.interestRate = "ਦਿਲਚਸਪੀ ਰੇਟ";
		AppStrings.savingsAmount = "ਸੇਵਿੰਗਜ਼ ਰਕਮ";
		AppStrings.noGroupLoan_Alert = "ਕੋਈ ਗਰੁੱਪ ਲੋਨ ਉਪਲੱਬਧ ਹੈ.";
		AppStrings.confirmation = "ਕੌਨਫੋਰਮੇਸ਼ਨ";
		AppStrings.logOut = "ਲਾੱਗ ਆਊਟ, ਬਾਹਰ ਆਉਣਾ";
		AppStrings.fromDateAlert = "ਮਿਤੀ ਤੱਕ ਪ੍ਰਦਾਨ ਕਰੋ";
		AppStrings.toDateAlert = "ਦੀ ਤਾਰੀਖ ਨੂੰ ਪ੍ਰਦਾਨ ਕਰੋ";
		AppStrings.auditingDateAlert = "ਿਕਰਪਾ ਕਰਕੇ ਮੁਹੱਈਆ ਆਡਿਟਿੰਗ ਿਮਤੀ";
		AppStrings.auditorAlert = "ਿਕਰਪਾ ਕਰਕੇ ਮੁਹੱਈਆ ਆਡੀਟਰ ਨਾਮ";
		AppStrings.nullDetailsAlert = "ਮੁਹੱਈਆ ਸਾਰੇ ਵੇਰਵੇ";
		AppStrings.trainingDateAlert = "ਟ੍ਰੇਨਿੰਗ ਿਮਤੀ ਮੁਹੱਈਆ";
		AppStrings.trainingTypeAlert = "ਘੱਟੋ ਇੱਕ ਟ੍ਰੇਨਿੰਗ ਕਿਸਮ ਚੁਣੋ";
		AppStrings.offline_ChangePwdAlert = "ਤੁਹਾਨੂੰ ਇੰਟਰਨੈੱਟ ਮੁਫ਼ਤ ਪਾਸਵਰਡ ਬਦਲੋ ਨਾ ਕਰ ਸਕਦਾ ਹੈ.";
		AppStrings.transactionFailAlert = "ਤੁਹਾਡੇ ਸੰਚਾਰ ਅਸਫਲ ਹੈ";
		AppStrings.voluntarySavings = "ਸਵੈਇੱਛਤ ਸੇਵਿੰਗਜ਼";
		AppStrings.tenure = "ਕਾਰਜਕਾਲ";
		AppStrings.purposeOfLoan = "ਕਰਜ਼ਾ ਦਾ ਉਦੇਸ਼";
		AppStrings.chooseLabel = "ਕਰਜ਼ਾ ਚੁਣੋ";
		AppStrings.Tenure_POL_Alert = "ਿਕਰਪਾ ਕਰਕੇ ਕਰਜ਼ ਅਤੇ ਕਾਰਜਕਾਲ ਦੀ ਮਿਆਦ ਦੇ ਮਕਸਦ ਦਿਓ";
		AppStrings.interestSubvention = "ਵਿਆਜ";
		AppStrings.adminAlert = "ਿਕਰਪਾ ਕਰਕੇ ਸੰਪਰਕ ਕਰੋ ਪ੍ਰਬੰਧਕ";
		AppStrings.userNotExist = "ਲਾਗਇਨ ਵੇਰਵਾ ਮੌਜੂਦ ਨਹੀ ਹੈ. ਿਕਰਪਾ ਕਰਕੇ ਆਨ ਲਾਗਇਨ ਜਾਰੀ.";
		AppStrings.tryLater = "ਬਾਅਦ ਵਿੱਚ ਕੋਸ਼ਿਸ਼";
		AppStrings.offlineDataAvailable = "ਔਫਲਾਈਨ ਉਪਲਬਧ ਡਾਟਾ. ਿਕਰਪਾ ਕਰਕੇ ਲਾਗਆਉਟ ਅਤੇ ਮੁੜ ਕੋਸ਼ਿਸ ਕਰੋ.";
		AppStrings.choosePOLAlert = "ਕਰਜ਼ਾ ਦਾ ਉਦੇਸ਼ ਦੀ ਚੋਣ ਕਰੋ";
		AppStrings.InternalLoan = "ਅੰਦਰੂਨੀ ਕਰਜ਼ਾ";
		AppStrings.TrainingTypes = "ਟ੍ਰੇਨਿੰਗ ਕਿਸਮ";
		AppStrings.uploadInfo = "ਸਦੱਸ ਵੇਰਵੇ";
		AppStrings.uploadAadhar = "ਅਪਲੋਡ ਆਧਾਰ";
		AppStrings.month = "-- ਮਹੀਨਾ ਚੁਣੋ --";
		AppStrings.year = "-- ਸਾਲ ਚੁਣੋ --";
		AppStrings.monthYear_Alert = "ਿਕਰਪਾ ਕਰਕੇ ਮਹੀਨਾ ਅਤੇ ਸਾਲ ਦੀ ਚੋਣ ਕਰੋ.";
		AppStrings.chooseLanguage = "ਭਾਸ਼ਾ ਚੁਣੋ";
		AppStrings.autoFill = "ਆਟੋ ਭਰਨ ਦੀ";
		AppStrings.viewAadhar = "ਦੇਖੋ ਆਧਾਰ";
		AppStrings.viewPhoto = "ਵੇਖੋ ਫੋਟੋ";
		AppStrings.uploadAlert = "ਤੁਹਾਨੂੰ ਹੋ ਸਕਦਾ ਹੈ ਨਾ ਦੇਖੋ ਆਧਾਰ ਅਤੇ ਫੋਟੋ ਮੁਫ਼ਤ ਇੰਟਰਨੈੱਟ ਕੁਨੈਕਸ਼ਨ";//"ਤੁਹਾਨੂੰ ਆਧਾਰ ਅਤੇ ਫੋਟੋ ਇੰਟਰਨੈੱਟ ਕੁਨੈਕਸ਼ਨ ਬਿਨਾ ਨੂੰ ਅੱਪਲੋਡ ਨਹੀ ਕਰ ਸਕਦੇ";
		AppStrings.upload = "ਅਪਲੋਡ";
		AppStrings.view = "ਦੇਖੋ";
		AppStrings.showAadharAlert = "ਿਕਰਪਾ ਕਰਕੇ ਦਿਖਾਉਣ ਆਪਣੇ ਆਧਾਰ ਕਾਰਡ.";
		AppStrings.deactivateAccount = "ਖਾਤਾ ਐਕਟੀਵੇਟ";
		AppStrings.deactivateAccount_SuccessAlert = "ਆਪਣੇ ਖਾਤੇ ਨੂੰ ਸਫਲਤਾ ਨਾਲ ਅਯੋਗ ਹੈ";
		AppStrings.deactivateAccount_FailAlert = "ਆਪਣੇ ਖਾਤੇ-ਐਕਟੀਵੇਟ ਕਰਨ 'ਚ ਨਾਕਾਮਯਾਬ ਗਈ ਹੈ";
		AppStrings.deactivateNetworkAlert = "ਤੁਹਾਨੂੰ ਇੰਟਰਨੈੱਟ ਕੁਨੈਕਸ਼ਨ ਬਿਨਾ ਆਪਣੇ ਖਾਤੇ ਐਕਟੀਵੇਟ ਨਾ ਕਰ ਸਕਦਾ ਹੈ";
		AppStrings.offlineReports = "ਔਫਲਾਈਨ ਦੀ ਰਿਪੋਰਟ";
		AppStrings.registrationSuccess = "ਤੁਹਾਨੂੰ ਸਫਲਤਾ ਰਜਿਸਟਰ";
		AppStrings.reg_MobileNo = "ਰਜਿਸਟਰ ਕੀਤਾ ਮੋਬਾਈਲ ਨੰਬਰ ਦਰਜ";
		AppStrings.reg_IMEINo = "ਐਕਸੇਸ ਡਿਨਾਇਡ.";
		AppStrings.reg_BothNo = "ਐਕਸੇਸ ਡਿਨਾਇਡ.";
		AppStrings.reg_IMEIUsed = "ਐਕਸੇਸ ਡਿਨਾਇਡ. ਜੰਤਰ ਮੇਲ";
		AppStrings.signInAsDiffUser = "ਵੱਖਰੇ ਯੂਜ਼ਰ ਵਿੱਚ ਸਾਈਨ";
		AppStrings.incorrectUserNameAlert = "ਗਲਤ ਯੂਜ਼ਰ";

		AppStrings.noofflinedatas = "ਕੋਈ ਔਫਲਾਈਨ ਸੰਚਾਰ ਇੰਦਰਾਜ਼";
		AppStrings.uploadprofilealert = "ਅਪਲੋਡ ਕਰੋ ਆਪਣੀ ਵੇਰਵੇ";
		AppStrings.monthYear_Warning = "ਸਹੀ ਮਹੀਨੇ ਅਤੇ ਸਾਲ ਦੀ ਚੋਣ ਕਰੋ ਜੀ";
		AppStrings.of = " ਦੇ ";
		AppStrings.previous_DateAlert = "ਪਿਛਲੀ ਚੁਣੇ ਿਮਤੀ";
		AppStrings.nobanktransactionreportdatas = "ਕੋਈ ਬਕ ਸੰਚਾਰ ਦੀ ਰਿਪੋਰਟ ਡਾਟਾ";

		AppStrings.mDefault = "ਮੂਲ ਰੂਪ ਵਿੱਚ";
		AppStrings.mTotalDisbursement = "ਕੁਲ ਵੰਡ";
		AppStrings.mInterloan = "ਨਿੱਜੀ ਕਰਜ਼ੇ ਦੇਮੁੜ";
		AppStrings.mTotalSavings = "ਕੁਲ ਸੰਗ੍ਰਹਿ";
		AppStrings.mCommonNetworkErrorMsg = "ਚੈੱਕ ਕਰੋ ਜੀ ਆਪਣੇ ਨੈਟਵਰਕ ਕਨੈਕਸ਼ਨ";
		AppStrings.mLoginAlert_ONOFF = "ਕਿਰਪਾ ਕਰਕੇ ਦੁਬਾਰਾ ਲਾਗਇਨ ਇੰਟਰਨੈੱਟ ਕੁਨੈਕਸ਼ਨ ਦੀ ਵਰਤ";
		AppStrings.changeLanguageNetworkException = "ਿਕਰਪਾ ਕਰਕੇ ਲਾਗਆਉਟ, ਮੁੜ ਲਾਗਇਨ ਕਰਨ ਲਈ ਇੰਟਰਨੈੱਟ ਕੁਨੈਕਸ਼ਨ ਦੀ ਕੋਸ਼ਿਸ਼ ਕਰੋ";
		AppStrings.photouploadmsg = "ਫੋਟੋ ਸਫਲਤਾਪੂਰਕ ਅੱਪਲੋਡ";
		AppStrings.loginAgainAlert = "ਪਿਛੋਕੜ ਡਾਟਾ ਅਜੇ ਵੀ ਲੋਡ ਕਰ ਰਿਹਾ ਹੈ ਕੁਝ ਵਾਰ ਦੀ ਉਡੀਕ ਕਰੋ.";//"ਕਿਰਪਾ ਕਰਕੇ ਦੁਬਾਰਾ ਲਾਗਇਨ";

		/* Interloan Disbursement menu */
		AppStrings.mInternalInterloanMenu = "ਅੰਦਰੂਨੀ ਕਰਜ਼ਾ";
		AppStrings.mInternalBankLoanMenu = "ਬਕ ਕਰਜ਼ਾ";
		AppStrings.mInternalMFILoanMenu = "MFI ਕਰਜ਼ਾ";
		AppStrings.mInternalFederationMenu = "ਫੈਡਰੇਸ਼ਨ ਦਾ ਕਰਜ਼ਾ";

		AppStrings.bankName = "ਬਕ ਦਾ ਨਾਮ";
		AppStrings.mfiname = "MFI ਨਾਮ";
		AppStrings.loanType = "ਕਰਜ਼ਾ ਦੀ ਕਿਸਮ";
		AppStrings.bankBranch = "ਬਕ ਸ਼ਾਖਾ";
		AppStrings.installment_type = "ਕਿਸ਼ਤ ਦੀ ਕਿਸਮ";
		AppStrings.NBFC_Name = "NBFC ਨਾਮ";
		AppStrings.Loan_Account_No = "ਕਰਜ਼ਾ ਖਾਤਾ ਨੰਬਰ";
		AppStrings.Loan_Sanction_Amount = "ਕਰਜ਼ਾ ਮਨਜ਼ੂਰੀ ਰਕਮ";
		AppStrings.Loan_Sanction_Date = "ਕਰਜ਼ਾ ਮਨਜ਼ੂਰੀ ਦੀ ਮਿਤੀ";
		AppStrings.Loan_Disbursement_Amount = "ਕਰਜ਼ ਦੀ ਵੰਡ ਰਕਮ";

		AppStrings.Loan_Disbursement_Date = "ਕਰਜ਼ਾ ਵੰਡ ਦੀ ਮਿਤੀ";
		AppStrings.Loan_Tenure = "ਕਰਜ਼ਾ ਕਾਰਜਕਾਲ";
		AppStrings.Subsidy_Amount = "ਸਬਸਿਡੀ ਦੀ ਰਾਸ਼ੀ";
		AppStrings.Subsidy_Reserve_Fund = "ਸਬਸਿਡੀ ਰਿਜ਼ਰਵ ਰਕਮ";
		AppStrings.Interest_Rate = "ਦਿਲਚਸਪੀ ਰੇਟ";

		AppStrings.mMonthly = "ਮਹੀਨਾਵਾਰ";
		AppStrings.mQuarterly = "ਿਤੰਨ";
		AppStrings.mHalf_Yearly = "ਛਿਮਾਹੀ";
		AppStrings.mYearly = "ਸਾਲਾਨਾ";
		AppStrings.mAgri = "ਐਗਰੀ";
		AppStrings.mMSME = "ਐਮਐਸਐਮਈ";
		AppStrings.mOthers = "ਹੋਰ";

		AppStrings.mMFI_NAME_SPINNER = "MFI ਨਾਮ";
		AppStrings.mMFINabfins = "NABFINS";
		AppStrings.mMFIOthers = "ਹੋਰ";
		AppStrings.mInternalTypeLoan = "ਅੰਦਰੂਨੀ ਕਰਜ਼ਾ";

		AppStrings.mPervious = "ਪਿਛਲੇ";
		AppStrings.mNext = "ਅਗਲਾ";
		AppStrings.mPasswordUpdate = "ਤੁਹਾਡਾ ਪਾਸਵਰਡ ਸਫਲਤਾਪੂਰਕ ਅਪਡੇਟ";

		AppStrings.mFixedDepositeAmount = "ਸਥਿਰ ਪੇਸ਼ਗੀ ਰਕਮ";
		AppStrings.mFedBankName = "ਫੈਡਰੇਸ਼ਨ ਦਾ ਨਾਮ";//"BANK/NBFC NAME";
		AppStrings.mLoanSanc_loanDist = "ਕਰਜ਼ਾ ਵੰਡ ਦੀ ਰਕਮ ਚੈੱਕ ਕਰੋ ਜੀ";
		AppStrings.mStepWise_LoanDibursement = "ਅੰਦਰੂਨੀ ਕਰਜ਼ਾ ਵੰਡ";

		AppStrings.mExpense_meeting="ਖਰਚੇ";//ਮੀਟਿੰਗ ਲਈ ਖ਼ਰਚ";
		AppStrings.mSubmitbutton = "ਠੀਕ ਹੈ";
		AppStrings.mNewUserSignup="ਰਜਿਸਟਰੇਸ਼ਨ";
		AppStrings.mMobilenumber="ਮੋਬਾਈਲ ਨੰਬਰ";
		AppStrings.mIsAadharAvailable = "ਆਧਾਰ ਜਾਣਕਾਰੀ ਨੂੰ ਅੱਪਡੇਟ ਨਾ";
		AppStrings.mSavingsTransaction = "ਬੱਚਤ ਸੰਚਾਰ";
		AppStrings.mBankCharges = "ਬਕ ਦੋਸ਼";
		AppStrings.mCashDeposit = "ਨਕਦ ਪੇਸ਼ਗੀ";
		AppStrings.mLimit = "ਕਰਜ਼ਾ";

		AppStrings.mAccountToAccountTransfer = "ਖਾਤਾ ਦਾ ਤਬਾਦਲਾ ਕਰਨ ਲਈ ਖਾਤਾ";
		AppStrings.mTransferFromBank = "ਬਕ";
		AppStrings.mTransferToBank = "ਲਈ ਬਕ";
		AppStrings.mTransferAmount = "ਤਬਦੀਲੀ ਰਕਮ";
		AppStrings.mTransferCharges = "ਤਬਦੀਲੀ ਖ਼ਰਚੇ";
		AppStrings.mTransferSpinnerFloating = "ਲਈ ਬਕ";
		AppStrings.mTransferNullToast = "ਪ੍ਰਦਾਨ ਕਰੋ ਸਾਰੇ ਵੇਰਵੇ";
		AppStrings.mAccToAccTransferToast = "ਤੁਹਾਨੂੰ ਸਿਰਫ ਇਕ ਬਕ ਖਾਤਾ ਹੈ";

		/* Loan Account */
		AppStrings.mLoanaccHeader = "ਕਰਜ਼ਾ ਖਾਤਾ";
		AppStrings.mLoanaccCash = "ਨਕਦ";
		AppStrings.mLoanaccBank = "ਬਕ";
		AppStrings.mLoanaccWithdrawal = "ਕਢਵਾਉਣ";
		AppStrings.mLoanaccExapenses = "ਖਰਚੇ";
		AppStrings.mLoanaccSpinnerFloating = "ਕਰਜ਼ਾ ਖਾਤਾ ਬਕ";
		AppStrings.mLoanaccCash_BankToast = "ਕਿਰਪਾ ਕਰਕੇ ਚੁਣੋ ਇੱਕ ਨਕਦ ਜ ਬਕ";
		AppStrings.mLoanaccNullToast = "ਪ੍ਰਦਾਨ ਕਰੋ ਸਾਰੇ ਵੇਰਵੇ";
		AppStrings.mLoanaccBankNullToast = "ਕਿਰਪਾ ਕਰਕੇ ਚੁਣੋ ਇੱਕ ਬਕ ਦਾ ਨਾਮ";

		AppStrings.mBankTransSavingsAccount = "ਬਚਤ ਖਾਤਾ";
		AppStrings.mBankTransLoanAccount = "ਕਰਜ਼ਾ ਖਾਤਾ";

		AppStrings.mLoanAccType = "ਿਕਸਮ";
		AppStrings.mLoanAccBankAmountAlert = "ਚੈੱਕ ਕਰੋ ਜੀ ਬਕ ਸੰਤੁਲਨ";

		AppStrings.mCashAtBank = "ਬਕ 'ਤੇ ਨਕਦ";
		AppStrings.mCashInHand = "ਹੱਥ ਵਿਚ ਨਕਦ";
		AppStrings.mShgSeedFund = "SHG ਸੰਤਾਨ ਫੰਡ";
		AppStrings.mFixedAssets = "ਸਥਿਰ ਜਾਇਦਾਦ";
		AppStrings.mVerified = "ਤਸਦੀਕ";
		AppStrings.mConfirm = "ਪੁਸ਼ਟੀ";
		AppStrings.mProceed = "ਜਾਰੀ";
		AppStrings.mDialogAlertText = "ਆਪਣੇ ਉਦਘਾਟਨੀ ਸੰਤੁਲਨ datas ਸਫਲਤਾਪੂਰਕ ਅੱਪਡੇਟ ਹਨ, ਹੁਣ ਤੁਹਾਨੂੰ ਜਾਰੀ ਕਰ ਸਕਦਾ ਹੈ.";
		AppStrings.mBalanceSheetDate = "ਸੰਤੁਲਨ ਸ਼ੀਟ 'ਤੇ ਅਪਲੋਡ ਕੀਤਾ ਗਿਆ ਹੈ";
		AppStrings.mCheckMonthlyEntries = "ਪਿਛਲੇ ਮਹੀਨੇ ਲਈ ਕੋਈ ਇੰਦਰਾਜ਼, ਇਸ ਲਈ ਤੁਹਾਨੂੰ ਪਿਛਲੇ ਮਹੀਨੇ ਦੇ ਲਈ datas ਪਾ ਦੇਣਾ ਚਾਹੀਦਾ ਹੈ";
		AppStrings.mTermLoanOutstanding = "OUTSTANDING";
		AppStrings.mCashCreditOutstanding = "OUTSTANDING";
		AppStrings.mGroupLoanOutstanding = "ਗਰੁੱਪ ਲੋਨ ਵਧੀਆ";
		AppStrings.mMemberLoanOutstanding = "ਸਦੱਸ ਕਰਜ਼ਾ ਬਕਾਇਆ";
		AppStrings.mContinueWithDate = "ਤੁਹਾਨੂੰ ਇਸ ਤਾਰੀਖ਼ ਨੂੰ ਤਬਦੀਲ ਕਰਨਾ ਚਾਹੁੰਦੇ ਹੋ";
		AppStrings.mCheckList = "ਸੂਚੀ ਵਿੱਚ ਚੈੱਕ";
		AppStrings.mMicro_Credit_Plan = "ਮਾਈਕਰੋ ਕਰੈਡਿਟ ਯੋਜਨਾ ਨੂੰ";
		AppStrings.mAadharCard_Invalid = "ਆਪਣੇ ਆਧਾਰ ਕਾਰਡ ਗਲਤ ਹੈ";
		AppStrings.mIsNegativeOpeningBalance = "ਨਕਾਰਾਤਮਕ ਸੰਤੁਲਨ ਨੂੰ ਸੁਧਾਰਨ";
		AppStrings.mLoginDetailsDelete = "ਪਿਛਲੇ ਲਾਗਇਨ ਵੇਰਵੇ ਹਟਾ ਦਿੱਤਾ ਜਾਵੇਗਾ. ਤੁਹਾਨੂੰ ਡਾਟਾ ਨੂੰ ਹਟਾਉਣਾ ਚਾਹੁੰਦੇ ਹੋ?";
		AppStrings.mVerifyGroupAlert = "ਤੁਹਾਨੂੰ ਤਸਦੀਕ ਕਰਨ ਲਈ ਆਨਲਾਈਨ ਹੋਣਾ ਚਾਹੀਦਾ ਹੈ";
		AppStrings.next = "ਅਗਲਾ";
		AppStrings.mNone = "ਕਿਸੇ ਨੂੰ ਨਾ ਚੁਣੋ";
		AppStrings.mSelect = "ਦੀ ਚੋਣ ਕਰੋ";
		AppStrings.mAnimatorName = "ਐਨੀਮੇਟਰ ਨਾਮ";
		AppStrings.mSavingsAccount = "ਬਚਤ ਖਾਤਾ";
		AppStrings.mAccountNumber = "ਅਕਾਊਂਟ ਨੰਬਰ";
		AppStrings.mLoanAccountAlert = "ਆਫਲਾਇਨ ਢੰਗ ਵਿੱਚ ਕਰਜ਼ਾ ਖਾਤਾ ਦਾ ਤਬਾਦਲਾ ਨਾ ਕਰ ਸਕਦਾ ਹੈ";
		AppStrings.mSeedFund = "ਬੀਜ ਫੰਡ";
		AppStrings.mLoanTypeAlert = "ਕਿਰਪਾ ਕਰਕੇ ਚੁਣੋ ਕਰਜ਼ਾ ਖਾਤਾ ਪ੍ਰਕਾਰ";
		AppStrings.mOutstandingAlert = "ਚੈੱਕ ਕਰੋ ਜੀ ਤੁਹਾਡੇ ਬਕਾਇਆ ਰਾਸ਼ੀ";
		AppStrings.mLoanAccTransfer = "ਕਰਜ਼ਾ ਖਾਤੇ ਦਾ ਸੰਚਾਰ";
		AppStrings.mLoanName = "ਕਰਜ਼ਾ ਨਾਮ";
		AppStrings.mLoanId = "ਕਰਜ਼ਾ ID";
		AppStrings.mLoanDisbursementDate = "ਕਰਜ਼ਾ ਵੰਡ ਦੀ ਮਿਤੀ";
		AppStrings.mAdd = "ਜੋਡ਼ਨ";
		AppStrings.mLess = "ਘੱਟ";
		AppStrings.mRepaymentWithInterest = "ਭੁਗਤਾਨ (ਵਿਆਜ ਦੇ ਨਾਲ)";
		AppStrings.mCharges = "ਦੋਸ਼";
		AppStrings.mExistingLoan = "ਮੌਜੂਦਾ ਕਰਜ਼ਾ";
		AppStrings.mNewLoan = "ਨਵ ਕਰਜ਼ਾ";
		AppStrings.mIncreaseLimit = "ਵਾਧਾ ਸੀਮਾ";
		AppStrings.mAvailableLimit = "ਉਪਲੱਬਧ ਸੀਮਾ";
		AppStrings.mDisbursementDate = "ਵੰਡ ਦੀ ਮਿਤੀ";
		AppStrings.mDisbursementAmount = "ਵੰਡ ਦੀ ਰਕਮ";
		AppStrings.mSanctionAmount = "ਮਨਜ਼ੂਰੀ ਦੀ ਰਕਮ";
		AppStrings.mBalanceAmount = "ਸੰਤੁਲਨ ਦੀ ਰਕਮ";
		AppStrings.mMemberDisbursementAmount = "ਅੰਗ ਵੰਡ ਦੀ ਰਕਮ";
		AppStrings.mLoanDisbursementFromLoanAcc = "ਕਰਜ਼ਾ ਖਾਤੇ ਤੱਕ ਦਾ ਕਰਜ਼ਾ ਵੰਡ";
		AppStrings.mLoanDisbursementFromSbAcc = "SB ਖਾਤੇ ਤੱਕ ਦਾ ਕਰਜ਼ਾ ਵੰਡ";
		AppStrings.mRepaid = "ਵਾਪਸ";
		AppStrings.mCheckBackLog = "ਬੈਕਲਾਗ ਨੂੰ ਚੈੱਕ";
		AppStrings.mTarget = "ਟੀਚੇ ਦਾ";
		AppStrings.mCompleted = "ਨੂੰ ਪੂਰਾ";
		AppStrings.mPending = "ਊਣਾ";
		AppStrings.mAskLogout = "ਤੁਹਾਨੂੰ ਲਾਗਆਉਟ ਕਰਨਾ ਚਾਹੁੰਦੇ ਹੋ ?";
		AppStrings.mCheckDisbursementAlert = "ਆਪਣੇ ਵੰਡ ਦੀ ਰਕਮ ਚੈੱਕ ਕਰੋ ਜੀ";
		AppStrings.mCheckbalanceAlert = "ਆਪਣੇ ਬਕਾਇਆ ਰਕਮ ਦਾ ਚੈੱਕ ਕਰੋ ਜੀ";
		AppStrings.mVideoManual = "ਵੀਡੀਓ ਦਸਤਾਵੇਜ਼";
		AppStrings.mPdfManual = "PDF ਦਸਤਾਵੇਜ਼";
		AppStrings.mAmountDisbursingAlert = "ਤੁਹਾਨੂੰ ਮਬਰ ਲਈ ਵੰਡ ਬਿਨਾ ਨੂੰ ਜਾਰੀ ਕਰਨ ਲਈ ਚਾਹੁੰਦੇ ਹੋ ?";
		AppStrings.mOpeningDate = "ਉਦਘਾਟਨੀ ਦੀ ਮਿਤੀ";
		AppStrings.mCheckLoanSancDate_LoanDisbDate = "ਆਪਣੇ ਕਰਜ਼ੇ ਦੀ ਮਨਜ਼ੂਰੀ ਮਿਤੀ ਅਤੇ ਕਰਜ਼ ਦੀ ਵੰਡ ਦੀ ਮਿਤੀ ਚੈੱਕ ਕਰੋ ਜੀ.";
		AppStrings.mLoanDisbursementFromRepaid = "ਸੀਸੀ ਲੋਨ ਕਢਵਾਉਣਾ";
		AppStrings.mExpense = "ਖਰਚੇ";
		AppStrings.mBeforeLoanPay = "ਕਰਜ਼ੇ ਦੀ ਅਦਾਇਗੀ ਤੋਂ ਪਹਿਲਾਂ";
		AppStrings.mLoans = "ਲੋਨ";
		AppStrings.mSurplus = "ਵਾਧੂ ਰਕਮ";
		AppStrings.mPOLAlert = "ਕਿਰਪਾ ਕਰਕੇ ਕਰਜ਼ੇ ਦਾ ਉਦੇਸ਼ ਦਰਜ ਕਰੋ";
		AppStrings.mCheckLoanAmounts = "ਕਿਰਪਾ ਕਰਕੇ ਆਪਣੇ ਕਰਜ਼ੇ ਦੀ ਪ੍ਰਵਾਨਗੀ ਰਕਮ ਅਤੇ ਕਰਜ਼ ਵੰਡਣ ਦੀ ਰਕਮ ਨੂੰ ਚੈੱਕ ਕਰੋ";
		AppStrings.mCheckLoanSanctionAmount = "ਆਪਣੀ ਲੋਨ ਮਨਜ਼ੂਰੀ ਰਕਮ ਦਾਖਲ ਕਰੋ";
		AppStrings.mCheckLoanDisbursementAmount = "ਆਪਣੇ ਕਰਜ਼ੇ ਦੀ ਵੰਡ ਰਕਮ ਦਾਖਲ ਕਰੋ";
		AppStrings.mIsPasswordSame = "ਕਿਰਪਾ ਕਰਕੇ ਆਪਣਾ ਪੁਰਾਣਾ ਪਾਸਵਰਡ ਚੈੱਕ ਕਰੋ ਅਤੇ ਨਵਾਂ ਪਾਸਵਰਡ ਇਕੋ ਜਿਹਾ ਨਹੀਂ ਹੋਣਾ ਚਾਹੀਦਾ.";
		AppStrings.mSavings_Banktransaction = "ਬੱਚਤ - ਬੈਂਕ ਟ੍ਰਾਂਜੈਕਸ਼ਨ";
		AppStrings.mCheckBackLogOffline = "ਚੈਕ ਬੈਲੋਲ ਔਫਲਾਈਨ ਦੇਖ ਨਹੀਂ ਸਕਦੇ";
		AppStrings.mCredit = "ਕ੍ਰੈਡਿਟ";
		AppStrings.mDebit = "ਡੈਬਿਟ";
		AppStrings.mCheckFixedDepositAmount = "ਕਿਰਪਾ ਕਰਕੇ ਆਪਣੀ ਫਿਕਸਡ ਡਿਪਾਜ਼ਿਟ ਰਕਮ ਚੈੱਕ ਕਰੋ.";
		AppStrings.mCheckDisbursementDate = "ਕਿਰਪਾ ਕਰਕੇ ਆਪਣੇ ਕਰਜ਼ੇ ਦੀ ਵੰਡ ਦੀ ਮਿਤੀ ਦੀ ਜਾਂਚ ਕਰੋ";
		AppStrings.mSendEmail = "EMAIL";
		AppStrings.mTotalInterest = "ਕੁੱਲ ਦਿਲਚਸਪੀ";
		AppStrings.mTotalCharges = "ਕੁੱਲ ਖਰਚੇ";
		AppStrings.mTotalRepayment = "ਕੁੱਲ ਮੁੜਭੁਗਤਾਨ";
		AppStrings.mTotalInterestSubventionRecevied = "ਕੁੱਲ ਵਿਆਜ਼ ਦੀ ਮੁਆਵਜ਼ਾ ਪ੍ਰਾਪਤ ਕੀਤੀ";
		AppStrings.mTotalBankCharges = "ਕੁੱਲ ਬੈਂਕ ਚਾਰਜ";
		AppStrings.mDone = "DONE";
		AppStrings.mPayment = "ਭੁਗਤਾਨ";
		AppStrings.mCashWithdrawal = "ਨਕਦ ਕਢਵਾਉਣਾ";
		AppStrings.mFundTransfer = "ਫੰਡ ਟ੍ਰਾਂਸਫਰ";
		AppStrings.mDepositAmount = "ਜਮ੍ਹਾ ਰਕਮ";
		AppStrings.mWithdrawalAmount = "ਵਾਪਸ ਲੈਣ ਦੀ ਰਕਮ";
		AppStrings.mTransactionMode = "ਸੰਚਾਰ ਮੋਡ";
		AppStrings.mBankLoanDisbursement = "ਬੈਂਕ ਕਰਜ਼ ਵੰਡ";
		AppStrings.mMemberToMember = "ਮੈਂਬਰ ਤੋਂ ਦੂਜੇ ਮੈਂਬਰ ਲਈ";
		AppStrings.mDebitAccount = "ਡੈਬਿਟ ਖਾਤਾ";
		AppStrings.mSHG_SavingsAccount = "shg ਬਚਤ ਖਾਤਾ";
		AppStrings.mDestinationMemberName = "ਮੰਜ਼ਿਲ ਮੈਂਬਰ ਨਾਂ";
		AppStrings.mSourceMemberName = "ਸਰੋਤ ਮੈਂਬਰ ਨਾਮ";
		AppStrings.mMobileNoUpdation = "ਮੋਬਾਈਲ ਨੰਬਰ ਅਪਡੇਟ ਕਰਨਾ";
		AppStrings.mMobileNo = "ਮੋਬਾਈਲ ਨੰਬਰ";
		AppStrings.mMobileNoUpdateSuccessAlert = "ਮੋਬਾਈਲ ਨੰਬਰ ਸਫਲਤਾਪੂਰਕ ਅਪਡੇਟ ਕੀਤਾ ਗਿਆ ਹੈ";
		AppStrings.mAadhaarNoUpdation = "ਆਧਾਰ ਨੰਬਰ ਉਪਿਦਿਨ";
		AppStrings.mSHGAccountNoUpdation = "ਸ਼ੋਅ ਖਾਤਾ ਨੰਬਰ ਨੰਬਰ";
		AppStrings.mAccountNoUpdation = "ਮੈਂਬਰ ਖਾਤਾ ਨੰਬਰ ਅਪਡੇਟ ਕਰਨਾ";
		AppStrings.mAadhaarNo = "ਆਧਾਰ ਨੰਬਰ ਇਕ";
		AppStrings.mBranchName = "ਬ੍ਰਾਂਚ ਦਾ ਨਾਮ";
		AppStrings.mAadhaarNoUpdateSuccessAlert = "ਆਧਾਰ ਨੰਬਰ ਸਫਲਤਾਪੂਰਵਕ ਅਪਡੇਟ ਕੀਤਾ ਗਿਆ ਹੈ";
		AppStrings.mMemberAccNoUpdateSuccessAlert = "ਸਦੱਸ ਖਾਤਾ ਨੰਬਰ ਸਫਲਤਾਪੂਰਵਕ ਅਪਡੇਟ ਕੀਤਾ ਗਿਆ";
		AppStrings.mSHGAccNoUpdateSuccessAlert = "ਐਸ.ਐਚ.ਜੀ. ਖਾਤਾ ਨੰਬਰ ਸਫਲਤਾਪੂਰਵਕ ਅਪਡੇਟ ਕੀਤਾ ਗਿਆ";
		AppStrings.mCheckValidMobileNo = "ਕਿਰਪਾ ਕਰਕੇ ਵੈਧ ਮੋਬਾਈਲ ਨੰਬਰ ਦਾਖਲ ਕਰੋ";
		AppStrings.mInvalidAadhaarNo = "ਅਯੋਗ ਆਧਾਰ ਨੰਬਰ";
		AppStrings.mCheckValidAadhaarNo = "ਕਿਰਪਾ ਕਰਕੇ ਇੱਕ ਠੀਕ ਆਧਾਰ ਨੰਬਰ ਦਾਖਲ ਕਰੋ";
		AppStrings.mInvalidAccountNo = "ਅਯੋਗ ਖਾਤਾ ਨੰਬਰ";
		AppStrings.mCheckAccountNumber = "ਕਿਰਪਾ ਕਰਕੇ ਆਪਣਾ ਖਾਤਾ ਨੰਬਰ ਦੇਖੋ";
		AppStrings.mInvalidMobileNo = "ਅਪ੍ਰਮਾਣਿਕ ਮੋਬਾਈਲ ਨੰਬਰ";
		AppStrings.mUploadScheduleAlert = "CAN'T UPLOAD SCHEDULE VI IN OFFLINE MODE";
		AppStrings.mIsMobileNoRepeat = "ਮੋਬਾਈਲ ਨੰਬਰ ਦੁਹਰਾਏ";
		AppStrings.mMobileNoUpdationNetworkCheck = "ਔਫਲਾਈਨ ਮੋਡ ਵਿੱਚ ਮੋਬਾਈਲ ਨੰਬਰ ਨੂੰ ਅਪਡੇਟ ਨਹੀਂ ਕਰ ਸਕਦਾ";
		AppStrings.mAadhaarNoNetworkCheck = "ਆਫਲਾਈਨ ਮੋਡ ਵਿੱਚ ਆਧਾਰ ਨੰਬਰ ਨੂੰ ਅਪਡੇਟ ਨਹੀਂ ਕਰ ਸਕਦਾ";
		AppStrings.mShgAccNoNetworkCheck = "ਐਸਐਚਜੀ ਅਕਾਊਂਟ ਨੰਬਰ ਨੂੰ ਆਫਲਾਈਨ ਮੋਡ ਵਿੱਚ ਅਪਡੇਟ ਨਹੀਂ ਕਰ ਸਕਦਾ";
		AppStrings.mAccNoNetworkCheck = "ਆਫਲਾਈਨ ਮੋਡ ਵਿਚ ਮੈਂਬਰ ਖਾਤਾ ਨੰਬਰ ਅਪਡੇਟ ਨਹੀਂ ਕਰ ਸਕਦਾ";
		AppStrings.mAccountNoRepeat = "ਖਾਤਾ ਨੰਬਰ ਦੁਹਰਾਇਆ";
		AppStrings.mCheckBankAndBranchNameSelected = "ਕਿਰਪਾ ਕਰਕੇ ਅਨੁਸਾਰੀ ਖਾਤਾ ਨੰਬਰ ਲਈ ਚੁਣਿਆ ਗਿਆ ਬਕ ਅਤੇ ਬ੍ਰਾਂਚ ਨਾਮ ਦੇਖੋ";
		AppStrings.mCheckNewLoanSancDate = "ਕਿਰਪਾ ਕਰਕੇ ਇਕ ਲੋਨ ਪ੍ਰਵਾਨਗੀ ਦੀ ਤਾਰੀਖ ਦੀ ਚੋਣ ਕਰੋ";
		AppStrings.mCreditLinkageInfo = "ਕ੍ਰੈਡਿਟ ਲਿੰਕੇਜ ਜਾਣਕਾਰੀ";
		AppStrings.mCreditLinkage = "ਕ੍ਰੈਡਿਟ ਲਿੰਕਜ";
		AppStrings.mCreditLinkage_content = "Number of times bank loans were taken by the SHG and repaid completely.";
		AppStrings.mCreditLinkageAlert = "ਆਫਲਾਈਨ ਮੋਡ ਵਿੱਚ ਕ੍ਰੈਡਿਟ ਲਿੰਕੇਜ ਜਾਣਕਾਰੀ ਨੂੰ ਅਪਡੇਟ ਨਹੀਂ ਕਰ ਸਕਦਾ";
	}
}
