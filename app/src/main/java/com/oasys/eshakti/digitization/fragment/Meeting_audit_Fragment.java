package com.oasys.eshakti.digitization.fragment;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.Dto.RequestDto.MeetingAuditingDetailsDto;
import com.oasys.eshakti.digitization.Dto.ResponseContent;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.database.SHGTable;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;
import com.tutorialsee.lib.TastyToast;
import com.yesteam.eshakti.view.activity.LoginActivity;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class Meeting_audit_Fragment extends Fragment implements View.OnClickListener, DatePickerDialog.OnDateSetListener, NewTaskListener {

    private View view;
    private TextView groupname;
    private TextView memberName;
    private TextView cashinHand;
    private TextView cashatBank;
    private LinearLayout container_toolbar;
    private TextView audit_auditFromdate;
    private LinearLayout audit_auditFromdate_layout;
    private TextView audit_auditTodate;
    private LinearLayout audit_auditTodate_layout;
    private TextView audit_auditdate;
    private LinearLayout audit_auditdate_layout;
    private EditText audit_auditornameeditText;
    private Button audit_Submitbutton;
    public static String audit_check = "";
    private ListOfShg shgDto;
    private String shgformationMillis;
    private String apiauditformationMillis;
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    private Date openingDate, lastTransaDate, systemDate, apiAuditdate;
    private Date chosenDate;
    private String dateclick;
    private String shgId;
    ResponseDto responseDto;
    private static FragmentManager fm;
    private ResponseContent responseContent;


    public Meeting_audit_Fragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_meeting_audit, container, false);
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgId = shgDto.getShgId();
        initView(view);
        fm = getActivity().getSupportFragmentManager();
        return view;
    }

    private void initView(View view) {
        groupname = (TextView) view.findViewById(R.id.groupname);
        groupname.setText(shgDto.getName());
        memberName = (TextView) view.findViewById(R.id.memberName);
        cashinHand = (TextView) view.findViewById(R.id.cashinHand);
        cashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
        cashatBank = (TextView) view.findViewById(R.id.cashatBank);
        cashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
        container_toolbar = (LinearLayout) view.findViewById(R.id.container_toolbar);
        audit_auditFromdate = (TextView) view.findViewById(R.id.audit_auditFromdate);
        audit_auditFromdate_layout = (LinearLayout) view.findViewById(R.id.audit_auditFromdate_layout);
        audit_auditTodate = (TextView) view.findViewById(R.id.audit_auditTodate);
        audit_auditTodate_layout = (LinearLayout) view.findViewById(R.id.audit_auditTodate_layout);
        audit_auditdate = (TextView) view.findViewById(R.id.audit_auditdate);
        audit_auditdate_layout = (LinearLayout) view.findViewById(R.id.audit_auditdate_layout);
        audit_auditornameeditText = (EditText) view.findViewById(R.id.audit_auditornameeditText);
        audit_Submitbutton = (Button) view.findViewById(R.id.audit_Submitbutton);

        audit_auditFromdate_layout.setOnClickListener(this);
        audit_auditTodate_layout.setOnClickListener(this);
        audit_auditdate_layout.setOnClickListener(this);
        audit_auditornameeditText.setOnClickListener(this);
        audit_Submitbutton.setOnClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {

            RestClient.getRestClient(Meeting_audit_Fragment.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.MEETING_AUDITINGDATE + shgId, getActivity(), ServiceType.MEETING_AUDITING_DATE);

//            RestClient.getRestClient(Meeting_audit_Fragment.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.MEETING_AUDITINGDATE , getActivity(), ServiceType.MEETING_AUDITING_DATE);
        } else {
            Utils.showToast(getActivity(), "Network Not Available");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.audit_auditFromdate_layout:
                dateclick = "1";

                try {
                    if (responseDto.getResponseContent().getAuditDate() != null) {

                        apiauditformationMillis = responseDto.getResponseContent().getAuditDate();
                        Date opDate = new Date(Long.parseLong(apiauditformationMillis));
                        String formattedDate1 = df.format(opDate.getTime());
                        apiAuditdate = df.parse(formattedDate1);

                        Calendar calender = Calendar.getInstance();

                        String formattedDate = df.format(calender.getTime());
                        Log.e("Device Date  =  ", formattedDate + "");

                        String mBalanceSheetDate = shgDto.getLastTransactionDate(); // dd/MM/yyyy
                        // String mBalanceSheetDate = "1512844200000";
                        Date d = new Date(Long.parseLong(mBalanceSheetDate));
                        String dateStr = df.format(d);
                        String formatted_balancesheetDate = dateStr;
                        Log.e("Balancesheet Date = ", formatted_balancesheetDate + "");

                        lastTransaDate = df.parse(formatted_balancesheetDate);
                        systemDate = df.parse(formattedDate);


                        if (apiAuditdate.compareTo(lastTransaDate) < 0) {
                            if (lastTransaDate.compareTo(systemDate) < 0 || lastTransaDate.compareTo(systemDate) == 0) {

                                calendarDialogApiAuditdateShow();

                            } else {
                                TastyToast.makeText(getActivity(), "PLEASE SET YOUR DEVICE DATE CORRECTLY",
                                        TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                            }
                        } else {
                            Toast.makeText(getActivity(), "Check Group opening date", Toast.LENGTH_LONG).show();
                        }
                    } else {


                        if (shgDto != null)
                            shgformationMillis = shgDto.getGroupFormationDate();
                        Date opDate = new Date(Long.parseLong(shgformationMillis));
                        String formattedDate1 = df.format(opDate.getTime());
                        openingDate = df.parse(formattedDate1);

                        Calendar calender = Calendar.getInstance();

                        String formattedDate = df.format(calender.getTime());
                        Log.e("Device Date  =  ", formattedDate + "");

                        String mBalanceSheetDate = shgDto.getLastTransactionDate(); // dd/MM/yyyy
                        // String mBalanceSheetDate = "1512844200000";
                        Date d = new Date(Long.parseLong(mBalanceSheetDate));
                        String dateStr = df.format(d);
                        String formatted_balancesheetDate = dateStr;
                        Log.e("Balancesheet Date = ", formatted_balancesheetDate + "");

                        lastTransaDate = df.parse(formatted_balancesheetDate);
                        systemDate = df.parse(formattedDate);


                        if (openingDate.compareTo(lastTransaDate) < 0) {
                            if (lastTransaDate.compareTo(systemDate) < 0 || lastTransaDate.compareTo(systemDate) == 0) {

                                calendarDialogShow();

                            } else {
                                TastyToast.makeText(getActivity(), "PLEASE SET YOUR DEVICE DATE CORRECTLY",
                                        TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                            }
                        } else {
                            Toast.makeText(getActivity(), "Check Group opening date", Toast.LENGTH_LONG).show();
                        }

                    }


                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                break;
            case R.id.audit_auditTodate_layout:
                dateclick = "2";
                try {
                    if (shgDto != null)
                        shgformationMillis = shgDto.getGroupFormationDate();
                    Date opDate = new Date(Long.parseLong(shgformationMillis));
                    String formattedDate1 = df.format(opDate.getTime());
                    openingDate = df.parse(formattedDate1);

                    Calendar calender = Calendar.getInstance();

                    String formattedDate = df.format(calender.getTime());
                    Log.e("Device Date  =  ", formattedDate + "");

                    String mBalanceSheetDate = shgDto.getLastTransactionDate(); // dd/MM/yyyy
                    // String mBalanceSheetDate = "1512844200000";
                    Date d = new Date(Long.parseLong(mBalanceSheetDate));
                    String dateStr = df.format(d);
                    String formatted_balancesheetDate = dateStr;
                    Log.e("Balancesheet Date = ", formatted_balancesheetDate + "");

                    lastTransaDate = df.parse(formatted_balancesheetDate);
                    systemDate = df.parse(formattedDate);


                    if (openingDate.compareTo(lastTransaDate) < 0) {
                        if (lastTransaDate.compareTo(systemDate) < 0 || lastTransaDate.compareTo(systemDate) == 0) {

                            calendarTodateDialogShow();

                        } else {
                            TastyToast.makeText(getActivity(), "PLEASE SET YOUR DEVICE DATE CORRECTLY",
                                    TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                        }
                    } else {
                        Toast.makeText(getActivity(), "Check Group opening date", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                break;
            case R.id.audit_auditdate_layout:
                dateclick = "3";
                try {
                    if (shgDto != null)
                        shgformationMillis = shgDto.getGroupFormationDate();
                    Date opDate = new Date(Long.parseLong(shgformationMillis));
                    String formattedDate1 = df.format(opDate.getTime());
                    openingDate = df.parse(formattedDate1);

                    Calendar calender = Calendar.getInstance();

                    String formattedDate = df.format(calender.getTime());
                    Log.e("Device Date  =  ", formattedDate + "");

                    String mBalanceSheetDate = shgDto.getLastTransactionDate(); // dd/MM/yyyy
                    // String mBalanceSheetDate = "1512844200000";
                    Date d = new Date(Long.parseLong(mBalanceSheetDate));
                    String dateStr = df.format(d);
                    String formatted_balancesheetDate = dateStr;
                    Log.e("Balancesheet Date = ", formatted_balancesheetDate + "");

                    lastTransaDate = df.parse(formatted_balancesheetDate);
                    systemDate = df.parse(formattedDate);


                    if (openingDate.compareTo(lastTransaDate) < 0) {
                        if (lastTransaDate.compareTo(systemDate) < 0 || lastTransaDate.compareTo(systemDate) == 0) {

                            calendarAuditDialogShow();

                        } else {
                            TastyToast.makeText(getActivity(), "PLEASE SET YOUR DEVICE DATE CORRECTLY",
                                    TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                        }
                    } else {
                        Toast.makeText(getActivity(), "Check Group opening date", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                break;
            case R.id.audit_auditornameeditText:

                break;
            case R.id.audit_Submitbutton:
                submit();
                break;
        }
    }

    private void calendarTodateDialogShow() {
        Calendar now = Calendar.getInstance();
        final DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), this, now.get(Calendar.YEAR), now.get(Calendar.DATE), now.get(Calendar.DAY_OF_MONTH));
        Calendar min_Cal = Calendar.getInstance();
        Calendar lastDate = Calendar.getInstance();

        Calendar fcal = Calendar.getInstance();
        fcal.setTimeInMillis(chosenDate.getTime());
        int oyear = fcal.get(Calendar.YEAR); // this is deprecated
        int omonth = fcal.get(Calendar.MONTH); // this is deprecated
        int oday = fcal.get(Calendar.DATE);

        Calendar lcal = Calendar.getInstance();
        lcal.setTimeInMillis(lastTransaDate.getTime());
        int lyear = lcal.get(Calendar.YEAR); // this is deprecated
        int lmonth = lcal.get(Calendar.MONTH); // this is deprecated
        int lday = lcal.get(Calendar.DATE);

        Calendar ccal1 = Calendar.getInstance();
        ccal1.setTimeInMillis(systemDate.getTime());
        int cyear = ccal1.get(Calendar.YEAR); // this is deprecated
        int cmonth = ccal1.get(Calendar.MONTH); // this is deprecated
        int cday = ccal1.get(Calendar.DATE);

        if (oday == lday && omonth == lmonth && oyear == lyear) {
            min_Cal.set(lyear, lmonth, lday);
            lastDate.set(cyear, cmonth, cday);
        } else {
            int diffYear = ccal1.get(Calendar.YEAR) - lcal.get(Calendar.YEAR);
            int diffMonth = diffYear * 12 + ccal1.get(Calendar.MONTH) - lcal.get(Calendar.MONTH);
            //
            // if (((cmonth - lmonth) <= 1 && (cyear == lyear || cyear > lyear)) && ((lmonth - cmonth) <= 1 && (cyear == lyear || cyear > lyear))) {
            if (diffMonth <= 1) {
                min_Cal.set(oyear, omonth, oday);
                lastDate.set(cyear, cmonth, cday);
            } else if (diffMonth > 1) {
                min_Cal.set(oyear, omonth, oday);
                    Calendar c = Calendar.getInstance();
                    c.setTime(lastTransaDate);
                    c.add(Calendar.MONTH, 1);
                    c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
                    lastDate.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE));
               /* } else {
                    Calendar c = Calendar.getInstance();
                    c.setTime(lastTransaDate);
                                   *//* c.add(Calendar.MONTH, 1);
                                    c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));*//*
                    lastDate.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE));
                }*/
                //    cInstance.set(cyear, cmonth, cday);
            }



          /*  if (cmonth == lmonth && cyear == lyear) {
                min_Cal.set(oyear, omonth, oday);
                lastDate.set(lyear, lmonth, lday);

            } else if (cmonth <= lmonth && cyear > lyear) {
                min_Cal.set(oyear, omonth, oday);
                Calendar c = Calendar.getInstance();
                c.setTime(lastTransaDate);
                c.add(Calendar.MONTH, 1);
                c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
                lastDate.set(lyear, lmonth, lday);
                // lastDate.set(cyear, c.get(Calendar.MONTH), c.get(Calendar.DATE));

            } else if (cmonth > lmonth && cyear == lyear) {
                min_Cal.set(oyear, omonth, oday);
                if ((cmonth - lmonth) == 1) {
                    lastDate.set(lyear, lmonth, lday);
                } else if ((cmonth - lmonth) > 1) {
                    Calendar c = Calendar.getInstance();
                    c.setTime(lastTransaDate);
                    c.add(Calendar.MONTH, 1);
                    c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
                    lastDate.set(lyear, lmonth, lday);
                    //lastDate.set(cyear, c.get(Calendar.MONTH), c.get(Calendar.DATE));
                }


            } else if (cmonth > lmonth && cyear > lyear) {
                min_Cal.set(oyear, omonth, oday);
                Calendar c = Calendar.getInstance();
                c.setTime(lastTransaDate);
                c.add(Calendar.MONTH, 1);
                c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
                lastDate.set(lyear, lmonth, lday);
                //lastDate.set(lyear, c.get(Calendar.MONTH), c.get(Calendar.DATE));
            }*/

        }


        datePickerDialog.getDatePicker().setMinDate(fcal.getTimeInMillis());
        datePickerDialog.getDatePicker().setMaxDate(lastDate.getTimeInMillis());

        datePickerDialog.show();
    }

    private void calendarDialogShow() {

        Calendar now = Calendar.getInstance();
        final DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), this, now.get(Calendar.YEAR), now.get(Calendar.DATE), now.get(Calendar.DAY_OF_MONTH));
        Calendar min_Cal = Calendar.getInstance();
        Calendar lastDate = Calendar.getInstance();

        Calendar fcal = Calendar.getInstance();
        fcal.setTimeInMillis(openingDate.getTime());
        int oyear = fcal.get(Calendar.YEAR); // this is deprecated
        int omonth = fcal.get(Calendar.MONTH); // this is deprecated
        int oday = fcal.get(Calendar.DATE);

        Calendar lcal = Calendar.getInstance();
        lcal.setTimeInMillis(lastTransaDate.getTime());
        int lyear = lcal.get(Calendar.YEAR); // this is deprecated
        int lmonth = lcal.get(Calendar.MONTH); // this is deprecated
        int lday = lcal.get(Calendar.DATE);

        Calendar ccal1 = Calendar.getInstance();
        ccal1.setTimeInMillis(systemDate.getTime());
        int cyear = ccal1.get(Calendar.YEAR); // this is deprecated
        int cmonth = ccal1.get(Calendar.MONTH); // this is deprecated
        int cday = ccal1.get(Calendar.DATE);

        if (oday == lday && omonth == lmonth && oyear == lyear) {
            min_Cal.set(lyear, lmonth, lday);
            lastDate.set(cyear, cmonth, cday);
        } else {


            int diffYear = ccal1.get(Calendar.YEAR) - lcal.get(Calendar.YEAR);
            int diffMonth = diffYear * 12 + ccal1.get(Calendar.MONTH) - lcal.get(Calendar.MONTH);

            //
            // if (((cmonth - lmonth) <= 1 && (cyear == lyear || cyear > lyear)) && ((lmonth - cmonth) <= 1 && (cyear == lyear || cyear > lyear))) {
            if (diffMonth <= 1) {
                lastDate.set(cyear, cmonth, cday);
                min_Cal.set(oyear, omonth, oday);
            } else if (diffMonth > 1) {
                min_Cal.set(oyear, omonth, oday);
                    Calendar c = Calendar.getInstance();
                    c.setTime(lastTransaDate);
                    c.add(Calendar.MONTH, 1);
                    c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
                    lastDate.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE));
               /* } else {
                    Calendar c = Calendar.getInstance();
                    c.setTime(lastTransaDate);
                                   *//* c.add(Calendar.MONTH, 1);
                                    c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));*//*
                    lastDate.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE));
                }*/
                //    cInstance.set(cyear, cmonth, cday);
            }


            /*if (cmonth == lmonth && cyear == lyear) {
                min_Cal.set(oyear, omonth, oday);
                lastDate.set(lyear, lmonth, lday);

            } else if (cmonth <= lmonth && cyear > lyear) {
                min_Cal.set(oyear, omonth, oday);
                Calendar c = Calendar.getInstance();
                c.setTime(lastTransaDate);
                c.add(Calendar.MONTH, 1);
                c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
                lastDate.set(lyear, lmonth, lday);
                // lastDate.set(cyear, c.get(Calendar.MONTH), c.get(Calendar.DATE));

            } else if (cmonth > lmonth && cyear == lyear) {
                min_Cal.set(oyear, omonth, oday);
                if ((cmonth - lmonth) == 1) {
                    lastDate.set(lyear, lmonth, lday);
                } else if ((cmonth - lmonth) > 1) {
                    Calendar c = Calendar.getInstance();
                    c.setTime(lastTransaDate);
                    c.add(Calendar.MONTH, 1);
                    c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
                    lastDate.set(lyear, lmonth, lday);
                    //lastDate.set(cyear, c.get(Calendar.MONTH), c.get(Calendar.DATE));
                }


            } else if (cmonth > lmonth && cyear > lyear) {
                min_Cal.set(oyear, omonth, oday);
                Calendar c = Calendar.getInstance();
                c.setTime(lastTransaDate);
                c.add(Calendar.MONTH, 1);
                c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
                lastDate.set(lyear, lmonth, lday);
                //lastDate.set(lyear, c.get(Calendar.MONTH), c.get(Calendar.DATE));
            }*/

        }

        datePickerDialog.getDatePicker().setMinDate(min_Cal.getTimeInMillis());
        datePickerDialog.getDatePicker().setMaxDate(lastDate.getTimeInMillis());

        datePickerDialog.show();
    }

    // Audit 1'st
    private void calendarDialogApiAuditdateShow() {

        Calendar now = Calendar.getInstance();
        final DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), this, now.get(Calendar.YEAR), now.get(Calendar.DATE), now.get(Calendar.DAY_OF_MONTH));
        Calendar min_Cal = Calendar.getInstance();
        Calendar lastDate = Calendar.getInstance();

        Calendar fcal = Calendar.getInstance();
        fcal.setTimeInMillis(apiAuditdate.getTime());
        int oyear = fcal.get(Calendar.YEAR); // this is deprecated
        int omonth = fcal.get(Calendar.MONTH); // this is deprecated
        int oday = fcal.get(Calendar.DATE);

        Calendar lcal = Calendar.getInstance();
        lcal.setTimeInMillis(lastTransaDate.getTime());
        int lyear = lcal.get(Calendar.YEAR); // this is deprecated
        int lmonth = lcal.get(Calendar.MONTH); // this is deprecated
        int lday = lcal.get(Calendar.DATE);

        Calendar ccal1 = Calendar.getInstance();
        ccal1.setTimeInMillis(systemDate.getTime());
        int cyear = ccal1.get(Calendar.YEAR); // this is deprecated
        int cmonth = ccal1.get(Calendar.MONTH); // this is deprecated
        int cday = ccal1.get(Calendar.DATE);

        if (oday == lday && omonth == lmonth && oyear == lyear) {
            min_Cal.set(lyear, lmonth, lday + 1);
            lastDate.set(cyear, cmonth, cday);
        } else if (oyear <= lyear) {

            if (cmonth == lmonth && cyear == lyear) {
                min_Cal.set(oyear, omonth, oday);
                lastDate.set(lyear, lmonth, lday);

            } else if (cmonth <= lmonth && cyear > lyear) {
                min_Cal.set(oyear, omonth, oday);
                Calendar c = Calendar.getInstance();
                c.setTime(lastTransaDate);
                c.add(Calendar.MONTH, 1);
                c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
                lastDate.set(lyear, lmonth, lday);
                // lastDate.set(cyear, c.get(Calendar.MONTH), c.get(Calendar.DATE));

            } else if (cmonth > lmonth && cyear == lyear) {
                min_Cal.set(oyear, omonth, oday);
                if ((cmonth - lmonth) == 1) {
                    lastDate.set(lyear, lmonth, lday);
                } else if ((cmonth - lmonth) > 1) {
                    Calendar c = Calendar.getInstance();
                    c.setTime(lastTransaDate);
                    c.add(Calendar.MONTH, 1);
                    c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
                    lastDate.set(lyear, lmonth, lday);
                    //lastDate.set(cyear, c.get(Calendar.MONTH), c.get(Calendar.DATE));
                }

            } else if (cmonth > lmonth && cyear > lyear) {
                min_Cal.set(oyear, omonth, oday);
                Calendar c = Calendar.getInstance();
                c.setTime(lastTransaDate);
                c.add(Calendar.MONTH, 1);
                c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
                lastDate.set(lyear, lmonth, lday);
                //lastDate.set(lyear, c.get(Calendar.MONTH), c.get(Calendar.DATE));
            }

        }


        datePickerDialog.getDatePicker().setMinDate(min_Cal.getTimeInMillis());
        datePickerDialog.getDatePicker().setMaxDate(lastDate.getTimeInMillis());

        datePickerDialog.show();
    }

    private void calendarAuditDialogShow() {
        Calendar now = Calendar.getInstance();
        final DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), this, now.get(Calendar.YEAR), now.get(Calendar.DATE), now.get(Calendar.DAY_OF_MONTH));
        Calendar min_Cal = Calendar.getInstance();
        Calendar lastDate = Calendar.getInstance();

        Calendar fcal = Calendar.getInstance();
        fcal.setTimeInMillis(openingDate.getTime());
        int oyear = fcal.get(Calendar.YEAR); // this is deprecated
        int omonth = fcal.get(Calendar.MONTH); // this is deprecated
        int oday = fcal.get(Calendar.DATE);

        Calendar lcal = Calendar.getInstance();
        lcal.setTimeInMillis(lastTransaDate.getTime());
        int lyear = lcal.get(Calendar.YEAR); // this is deprecated
        int lmonth = lcal.get(Calendar.MONTH); // this is deprecated
        int lday = lcal.get(Calendar.DATE);

        Calendar ccal1 = Calendar.getInstance();
        ccal1.setTimeInMillis(systemDate.getTime());
        int cyear = ccal1.get(Calendar.YEAR); // this is deprecated
        int cmonth = ccal1.get(Calendar.MONTH); // this is deprecated
        int cday = ccal1.get(Calendar.DATE);

        if (oday == lday && omonth == lmonth && oyear == lyear) {
            min_Cal.set(lyear, lmonth, lday + 1);
            lastDate.set(cyear, cmonth, cday);
        } else if (oyear <= lyear) {

            if (cmonth == lmonth && cyear == lyear) {
                min_Cal.set(oyear, omonth, oday);
                lastDate.set(cyear, cmonth, cday);

            } else if (cmonth <= lmonth && cyear > lyear) {
                min_Cal.set(oyear, omonth, oday);
                Calendar c = Calendar.getInstance();
                c.setTime(systemDate);
                c.add(Calendar.MONTH, 1);
                c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
                lastDate.set(cyear, cmonth, cday);
                // lastDate.set(cyear, c.get(Calendar.MONTH), c.get(Calendar.DATE));

            } else if (cmonth > lmonth && cyear == lyear) {
                min_Cal.set(oyear, omonth, oday);
                if ((cmonth - lmonth) == 1) {
                    lastDate.set(cyear, cmonth, cday);
                } else if ((cmonth - lmonth) > 1) {
                    Calendar c = Calendar.getInstance();
                    c.setTime(systemDate);
                    c.add(Calendar.MONTH, 1);
                    c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
                    lastDate.set(cyear, cmonth, cday);
                    //lastDate.set(cyear, c.get(Calendar.MONTH), c.get(Calendar.DATE));
                }


            } else if (cmonth > lmonth && cyear > lyear) {
                min_Cal.set(oyear, omonth, oday);
                Calendar c = Calendar.getInstance();
                c.setTime(systemDate);
                c.add(Calendar.MONTH, 1);
                c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
                lastDate.set(cyear, cmonth, cday);
                //lastDate.set(lyear, c.get(Calendar.MONTH), c.get(Calendar.DATE));
            }

        }


        datePickerDialog.getDatePicker().setMinDate(min_Cal.getTimeInMillis());
        datePickerDialog.getDatePicker().setMaxDate(lastDate.getTimeInMillis());

        datePickerDialog.show();
    }

    private void submit() {

        String auditornameeditText = audit_auditornameeditText.getText().toString().trim();

        if (TextUtils.isEmpty(auditornameeditText)) {
            Toast.makeText(getContext(), "Auditor Name Should not be Empty", Toast.LENGTH_SHORT).show();
        } else {
            displayDialogWindow();
        }


    }

    public void displayDialogWindow() {

        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.audit_confirmationdialog);

        TextView auditconfirmation = (TextView) dialog.findViewById(R.id.auditconfirmation);
        auditconfirmation.setTypeface(LoginActivity.sTypeface);

        TextView type = (TextView) dialog.findViewById(R.id.transaction_loantypevalue);
        type.setTypeface(LoginActivity.sTypeface);
        type.setText(audit_auditFromdate.getText().toString());

        TextView interest = (TextView) dialog.findViewById(R.id.transaction_loan_interestvalue);
        interest.setTypeface(LoginActivity.sTypeface);
        interest.setText(audit_auditTodate.getText().toString());

        TextView charges = (TextView) dialog.findViewById(R.id.transaction_loanchargesvalue);
        charges.setTypeface(LoginActivity.sTypeface);
        charges.setText(audit_auditornameeditText.getText().toString());

        TextView repayment = (TextView) dialog.findViewById(R.id.transaction_loanrepaymentvalue);
        repayment.setTypeface(LoginActivity.sTypeface);

        TextView interest_subvention = (TextView) dialog.findViewById(R.id.transaction_loan_interest_subventionvalue);
        interest_subvention.setTypeface(LoginActivity.sTypeface);

        TextView bankname = (TextView) dialog.findViewById(R.id.transaction_loanbanknamevalue);
        bankname.setTypeface(LoginActivity.sTypeface);
        bankname.setText(audit_auditdate.getText().toString());

        TextView bankcharges = (TextView) dialog.findViewById(R.id.transaction_loanbankchargesvalue);
        bankcharges.setTypeface(LoginActivity.sTypeface);

        TextView from_date = (TextView) dialog.findViewById(R.id.from_date);
        from_date.setTypeface(LoginActivity.sTypeface);
        from_date.setText("FROM DATE");

        TextView to_date = (TextView) dialog.findViewById(R.id.to_date);
        to_date.setTypeface(LoginActivity.sTypeface);
        to_date.setText("TO DATE");

        TextView audit_date = (TextView) dialog.findViewById(R.id.audit_date);
        audit_date.setTypeface(LoginActivity.sTypeface);
        audit_date.setText("AUDITING DATE");

        TextView audit_name = (TextView) dialog.findViewById(R.id.audit_name);
        audit_name.setTypeface(LoginActivity.sTypeface);
        audit_name.setText("AUDITOR NAME");

        LinearLayout repaymenttext = (LinearLayout) dialog.findViewById(R.id.repaymenttext);
        repaymenttext.setVisibility(View.GONE);
        LinearLayout interesttext = (LinearLayout) dialog.findViewById(R.id.interesttext);
        interesttext.setVisibility(View.GONE);
        Button edit = (Button) dialog.findViewById(R.id.edit);
        edit.setTypeface(LoginActivity.sTypeface);

        Button ok = (Button) dialog.findViewById(R.id.ok);
        ok.setTypeface(LoginActivity.sTypeface);

        LinearLayout dialog_bank_charge_layout = (LinearLayout) dialog.findViewById(R.id.dialog_bank_charge_layout);
        LinearLayout dialog_bankname_layout = (LinearLayout) dialog.findViewById(R.id.dialog_bankname_layout);

        dialog.show();
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {

//                    Meeting_audit_Fragment.showFragment(new MainFragment());
                    MainFragment mainFragment = new MainFragment();
                    Bundle bundles = new Bundle();
                    bundles.putString("Meeting", MainFragment.Flag_Meeting);
                    mainFragment.setArguments(bundles);
                    NewDrawerScreen.showFragment(mainFragment);
                    dialog.dismiss();

//                Calendar calender = Calendar.getInstance();
//
//                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
//                String formattedDate = df.format(calender.getTime());
//
//                DateFormat simple = new SimpleDateFormat("yyyy-MM-dd");
//                Date d = new Date(Long.parseLong(shgDto.getLastTransactionDate()));
//                String dateStr = simple.format(d);
                    MeetingAuditingDetailsDto meetingAuditingDetailsDto = new MeetingAuditingDetailsDto();
                    meetingAuditingDetailsDto.setFromDate(audit_auditFromdate.getText().toString());
                    meetingAuditingDetailsDto.setShgId(shgId);
                    meetingAuditingDetailsDto.setAuditingDate(audit_auditTodate.getText().toString());
                    meetingAuditingDetailsDto.setToDate(audit_auditdate.getText().toString());
                    meetingAuditingDetailsDto.setAuditorName(audit_auditornameeditText.getText().toString());


                    String meeting_auditing_details_end = new Gson().toJson(meetingAuditingDetailsDto);
                    Log.d("dtodata's", meeting_auditing_details_end);


                    if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {

                        RestClient.getRestClient(Meeting_audit_Fragment.this).callRestWebService(Constants.BASE_URL + Constants.MEETING_AUDITING_DETAILS, meeting_auditing_details_end, getActivity(), ServiceType.MEETING_AUDITING_DETAILS_ENDPOST);

                    } else {

                        Utils.showToast(getActivity(), "Network Not Available");
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });

    }

    public static void showFragment(Fragment fragment) {

        FragmentTransaction trans = fm.beginTransaction();
        trans.replace(R.id.static_frame, fragment);
        trans.setCustomAnimations(R.anim.right_to_left_in, 0, 0, R.anim.right_to_left_out);
        trans.show(fragment).commit();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String date = dayOfMonth + "-" + month + "-" + year;
        try {
            if (dateclick.equals("1")) {

                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(0);
                cal.set(year, month, dayOfMonth, 0, 0, 0);
                chosenDate = cal.getTime();
                DateFormat df_medium_uk = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.UK);
                String df_medium_uk_str = df_medium_uk.format(chosenDate);
                DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date dates1 = df_medium_uk.parse(df_medium_uk_str);
                String outputDateStr = outputFormat.format(dates1);
                audit_auditFromdate.setText(outputDateStr);
                // Display the formatted date
                // tv.setText(tv.getText() + df_medium_uk_str + " (DateFormat.MEDIUM, Locale.UK)\n");
                lastTransaDate = df.parse(date);
            } else if (dateclick.equals("2")) {

                Calendar cal_to = Calendar.getInstance();
                cal_to.setTimeInMillis(0);
                cal_to.set(year, month, dayOfMonth, 0, 0, 0);
                chosenDate = cal_to.getTime();
                DateFormat df_medium_uk1 = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.UK);
                String df_medium_uk_str1 = df_medium_uk1.format(chosenDate);
                DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date dates = df_medium_uk1.parse(df_medium_uk_str1);
                String outputDateStr = outputFormat.format(dates);
                audit_auditTodate.setText(outputDateStr);
                // Display the formatted date
                // tv.setText(tv.getText() + df_medium_uk_str + " (DateFormat.MEDIUM, Locale.UK)\n");
                lastTransaDate = df.parse(date);

            } else if (dateclick.equals("3")) {
                Calendar cal_audit = Calendar.getInstance();
                cal_audit.setTimeInMillis(0);
                cal_audit.set(year, month, dayOfMonth, 0, 0, 0);
                chosenDate = cal_audit.getTime();
                DateFormat df_medium_uk2 = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.UK);
                String df_medium_uk_str2 = df_medium_uk2.format(chosenDate);
                DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date dates3 = df_medium_uk2.parse(df_medium_uk_str2);
                String outputDateStr = outputFormat.format(dates3);
                audit_auditdate.setText(outputDateStr);
                // Display the formatted date
                // tv.setText(tv.getText() + df_medium_uk_str + " (DateFormat.MEDIUM, Locale.UK)\n");
                lastTransaDate = df.parse(date);

            }
            // MySharedPreference.writeString(getActivity(), MySharedPreference.LAST_TRANSACTION, lastTransaDate.getTime() + "");
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {

        switch (serviceType) {
            case MEETING_AUDITING_DATE:
                Log.d("getDetails", " " + result.toString());
                responseDto = new Gson().fromJson(result.toString(), ResponseDto.class);
                if (responseDto.getStatusCode() == (Utils.Success_Code)) {
                    Utils.showToast(getActivity(), responseDto.getMessage());
                    responseDto.getResponseContent().getAuditDate();
                    Log.d("check", "" + responseDto.getResponseContent().getAuditDate());
                    String Date = responseDto.getResponseContent().getAuditDate();
                    if (Date != null) {
                        Date d = new Date(Long.parseLong(Date));
                        String dateStr = df.format(d);
                        Log.e("Balancesheet Date = ", dateStr + "");
                    }


                } else {
                    if (responseDto.getStatusCode() == 401) {

                        Log.e("Group Logout", "Logout Sucessfully");
                        AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                    }
                    Utils.showToast(getActivity(), "Network error");
                }
                break;
            case MEETING_AUDITING_DETAILS_ENDPOST:
                Log.d("getDetails", " " + result);
                if (result != null) {
                    responseDto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    if (responseDto.getStatusCode() == (Utils.Success_Code)) {
                        Utils.showToast(getActivity(), responseDto.getMessage());
                        TastyToast.makeText(getActivity(), "Transaction Completed",
                                TastyToast.LENGTH_SHORT, TastyToast.SUCCESS);
                        Log.d("TastyToast", " " + result);
                    } else {
                        if (responseDto.getStatusCode() == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                        }
                        Utils.showToast(getActivity(), "Network error");
                    }
                }
                break;
        }

    }
}
