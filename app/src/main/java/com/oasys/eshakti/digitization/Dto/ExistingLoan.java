package com.oasys.eshakti.digitization.Dto;

import lombok.Data;

@Data
public class ExistingLoan {

    private String loanId,loanAccountId;
    private String shgId;
    private String loanTypeName;
    private String accountNumber;
    private String bankName;
    private String disbursmentDate;
    private String loanOutstanding;

    private String id;
    private String name;

}
