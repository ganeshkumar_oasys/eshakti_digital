package com.oasys.eshakti.digitization.Dto;


import lombok.Data;

@Data
public class WalletResponseDto {

    int statusCode;
    String message;
    String responseContent;

}
