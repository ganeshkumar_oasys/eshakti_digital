package com.oasys.eshakti.digitization.activity;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.database.SHGTable;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Animator extends Fragment implements NewTaskListener {

    private TextView cashinHand;
    private TextView cashinhandvalue;
    private TextView cashatbank;
    private TextView cashatbankvalue;
    private TextView animatorprofile;
    private TextView animatorname;
    private TextView animatornamevalue;
    private TextView totalgroup;
    private TextView totalgroupvalue;
    private TextView telephonenumber;
    private TextView telephonenumbervalue;
    private TextView age;
    private TextView agevalue;
    private TextView date;
    private TextView animator_name;
    private TextView datevalue;
    ResponseDto animatorProfileDto;
    private View rootView;
    private ListOfShg shgDto;
    private String userId;
    private String langId;
    private String animatorid;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_animator, container, false);
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));


        userId = MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, "");
        langId = MySharedPreference.readString(getActivity(), MySharedPreference.LANG_ID, "");

        if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {

            String url = Constants.BASE_URL + Constants.SHG_URL + userId + "&languageId=" + langId;
            RestClient.getRestClient(Animator.this).callWebServiceForGetMethod(url, getActivity(), ServiceType.SHG_LIST);

        }


        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();

    }

    private void initView() {
        cashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
        cashinHand.setTypeface(LoginActivity.sTypeface);

        animator_name = (TextView) rootView.findViewById(R.id.animator_name);
        animator_name.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
        animator_name.setTypeface(LoginActivity.sTypeface);

        cashinhandvalue = (TextView) rootView.findViewById(R.id.cashinhandvalue);
        cashinhandvalue.setText("₹" + shgDto.getCashInHand());
        cashinhandvalue.setTypeface(LoginActivity.sTypeface);

        cashatbank = (TextView) rootView.findViewById(R.id.cashatbank);
        cashatbank.setTypeface(LoginActivity.sTypeface);

        cashatbankvalue = (TextView) rootView.findViewById(R.id.cashatbankvalue);
        cashatbankvalue.setText("₹" + shgDto.getCashAtBank());
        cashatbankvalue.setTypeface(LoginActivity.sTypeface);

        animatorprofile = (TextView) rootView.findViewById(R.id.animatorprofile);
        animatorprofile.setTypeface(LoginActivity.sTypeface);
        animatorname = (TextView) rootView.findViewById(R.id.animatorname);
        animatorname.setTypeface(LoginActivity.sTypeface);
        animatornamevalue = (TextView) rootView.findViewById(R.id.animatornamevalue);
        animatornamevalue.setTypeface(LoginActivity.sTypeface);
        totalgroup = (TextView) rootView.findViewById(R.id.totalgroup);
        totalgroup.setTypeface(LoginActivity.sTypeface);
        totalgroupvalue = (TextView) rootView.findViewById(R.id.totalgroupvalue);
        totalgroupvalue.setTypeface(LoginActivity.sTypeface);
        telephonenumber = (TextView) rootView.findViewById(R.id.telephonenumber);
        telephonenumber.setTypeface(LoginActivity.sTypeface);
        telephonenumbervalue = (TextView) rootView.findViewById(R.id.telephonenumbervalue);
        telephonenumbervalue.setTypeface(LoginActivity.sTypeface);
        age = (TextView) rootView.findViewById(R.id.age);
        age.setTypeface(LoginActivity.sTypeface);
        agevalue = (TextView) rootView.findViewById(R.id.agevalue);
        agevalue.setTypeface(LoginActivity.sTypeface);
        date = (TextView) rootView.findViewById(R.id.date);
        date.setTypeface(LoginActivity.sTypeface);
        datevalue = (TextView) rootView.findViewById(R.id.datevalue);
        datevalue.setTypeface(LoginActivity.sTypeface);


    }


    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        switch (serviceType) {

            case SHG_LIST:
                try {

                    if (result != null && result.length() > 0) {
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();
                        ResponseDto lrDto = gson.fromJson(result, ResponseDto.class);
                        int statusCode = lrDto.getStatusCode();
                        String message = lrDto.getMessage();
                        Log.d("response status", " " + statusCode);
                        if (statusCode == 400 || statusCode == 403 || statusCode == 500 || statusCode == 503) {
                            // showMessage(statusCode);
                            Utils.showToast(getActivity(), message);

                        } else if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                        } else if (statusCode == Utils.Success_Code) {

                            animatorid = lrDto.getResponseContent().getAnimatorId();

                            String url = Constants.BASE_URL + Constants.PROFILE_ANIMATOR + animatorid;
                            Log.d("url", url);

                            if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {
                                RestClient.getRestClient(Animator.this).callWebServiceForGetMethod(url, getActivity(), ServiceType.ANIMATOR_PROFILE);
                            } else {
                                Utils.showToast(getActivity(), "Network Not Available");
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case ANIMATOR_PROFILE:
                Log.d("getDetails", " " + result.toString());
                try {
                    animatorProfileDto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    if (animatorProfileDto.getStatusCode() == Utils.Success_Code) {
                        Utils.showToast(getActivity(), animatorProfileDto.getMessage());

                        if ((animatorProfileDto.getResponseContent() != null)) {

                            animatornamevalue.setText(animatorProfileDto.getResponseContent().getAnimatorProfile().getAnimatorName());
                            totalgroupvalue.setText(animatorProfileDto.getResponseContent().getAnimatorProfile().getTotalGroup());
                            telephonenumbervalue.setText(animatorProfileDto.getResponseContent().getAnimatorProfile().getPhoneNumber());
                            agevalue.setText(animatorProfileDto.getResponseContent().getAnimatorProfile().getAge());

                            if (animatorProfileDto.getResponseContent().getAnimatorProfile() != null && animatorProfileDto.getResponseContent().getAnimatorProfile().getDateOfAssigning() != null && !animatorProfileDto.getResponseContent().getAnimatorProfile().getDateOfAssigning().equals("-")) {
                                if (animatorProfileDto.getResponseContent().getAnimatorProfile().getDateOfAssigning() != null && !animatorProfileDto.getResponseContent().getAnimatorProfile().getDateOfAssigning().equals("-")) {
                                    DateFormat simple = new SimpleDateFormat("dd/MM/yyyy");
                                    Date d = new Date(Long.parseLong(animatorProfileDto.getResponseContent().getAnimatorProfile().getDateOfAssigning()));
                                    String dateStr = simple.format(d);
                                    datevalue.setText(dateStr);
                                }
                            } else {
                                datevalue.setText("-");
                            }


                        }


                    } else {

                        if (animatorProfileDto.getStatusCode() == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                        }
                        Utils.showToast(getActivity(), "Network error");
                    }
                    break;

                } catch (
                        Exception e)

                {
                    e.printStackTrace();
                }
        }
    }
}



