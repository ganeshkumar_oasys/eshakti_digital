package com.oasys.eshakti.digitization.fragment;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.RegionalConversion;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.activity.LoginActivity;
import com.oasys.eshakti.digitization.database.SHGTable;

public class EShaktiApp extends Fragment {
    private TextView mGroupName, mCashInHand, mCashAtBank, mHeadertext;
    private ListOfShg shgDto;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_about_yesbooks, container, false);



        try {
            shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
            mGroupName = (TextView) rootView.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
            mCashInHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashInHand.setTypeface(LoginActivity.sTypeface);

            mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
            mCashAtBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashAtBank.setTypeface(LoginActivity.sTypeface);

            PackageManager manager = getActivity().getPackageManager();
            PackageInfo info = manager.getPackageInfo(getActivity().getPackageName(), 0);
            String version = info.versionName;

            mHeadertext = (TextView) rootView.findViewById(R.id.titleHead);
            mHeadertext.setText(" VERSION  :    " + version);
            mHeadertext.setTypeface(LoginActivity.sTypeface);

            TextView content = (TextView) rootView.findViewById(R.id.contentTxt);
            content.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.aboutYesbooks)));
            content.setTypeface(LoginActivity.sTypeface);
            content.setTextColor(R.color.black);

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return rootView;
    }
}
