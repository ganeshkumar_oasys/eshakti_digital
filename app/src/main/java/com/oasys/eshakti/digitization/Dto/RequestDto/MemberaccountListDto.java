package com.oasys.eshakti.digitization.Dto.RequestDto;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.Data;

@Data
public class MemberaccountListDto implements Serializable {

    ArrayList<ShgSavingsAccountDTOList> shgSavingsAccountDTOList;
}
