package com.oasys.eshakti.digitization.fragment;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.activity.LoginActivity;


public class Profile_Mem_View_Image_Fragment extends Fragment implements NewTaskListener {

    private TextView mMemberName;
    private ImageView imageView;
    private String getmem_id;

    public Profile_Mem_View_Image_Fragment() {
        // TODO Auto-generated constructor stub
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {
            onTaskStarted();
            RestClient.getRestClient(this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.GET_MEM_PHOTO + getmem_id, getActivity(), ServiceType.GET_MEM_PHOTO);
        } else {
            Utils.showToast(getActivity(), "Network not available");
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View rootView = inflater.inflate(R.layout.fragment_memberview_image, container, false);

        mMemberName = (TextView) rootView.findViewById(R.id.memberName);
        mMemberName.setText(MySharedPreference.readString(getActivity(), MySharedPreference.MEM_NAME_SUMMARY, ""));
        mMemberName.setTypeface(LoginActivity.sTypeface);

        imageView = (ImageView) rootView.findViewById(R.id.member_imageview);
        Bundle bundle = getArguments();
        getmem_id = bundle.getString("memid");
        Log.d("Mem", getmem_id);

		/*byte[] imageAsBytes = Base64.decode(Profile_Member_Aadhaar_Image_View_MenuFragment.mServiceResponse.getBytes(),
				Base64.DEFAULT);*/


        return rootView;
    }

    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        switch (serviceType) {
            case GET_MEM_PHOTO:
                try {
                    if (result != null) {
                        ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                        String message = cdto.getMessage();
                        int statusCode = cdto.getStatusCode();
                        if (statusCode == Utils.Success_Code) {
                            if (cdto.getResponseContent() != null) {
                                if (cdto.getResponseContent().getImageByteCode() != null) {
                                    String imagebytes = cdto.getResponseContent().getImageByteCode();
                                    byte[] imageAsBytes = Base64.decode(imagebytes, Base64.DEFAULT);
                                    imageView.setImageBitmap(BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length));
                                }
                            }
                        } else {

                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }
}