package com.oasys.eshakti.digitization.Dto.RequestDto;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.Data;

/**
 * Created by MuthukumarPandi on 12/28/2018.
 */
@Data
public class MicrocreditplanDto implements Serializable {
    private String groupId;

    private String minutesofMeetingId;

    private ArrayList<MembersDetailsDto> membersDetails;

    private String mobileDate;

    private String transactionDate;
}
