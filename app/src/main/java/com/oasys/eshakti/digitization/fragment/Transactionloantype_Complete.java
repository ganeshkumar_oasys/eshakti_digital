package com.oasys.eshakti.digitization.fragment;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;

/**
 * A simple {@link Fragment} subclass.
 */
public class Transactionloantype_Complete extends Fragment implements View.OnClickListener {
    private View view;
    private TextView wallet_continue;


    public Transactionloantype_Complete() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_transactionloantype__complete, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {

        wallet_continue = (TextView) view.findViewById(R.id.wallet_continue);
        wallet_continue.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.wallet_continue:

                NewDrawerScreen.showFragment(new MainFragment());
                FragmentManager fm = getFragmentManager();
                fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                break;
        }
    }
}
