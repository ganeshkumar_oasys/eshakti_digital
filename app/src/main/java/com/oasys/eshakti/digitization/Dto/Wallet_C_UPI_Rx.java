package com.oasys.eshakti.digitization.Dto;


import lombok.Data;

@Data
public class Wallet_C_UPI_Rx {

    String amount,merchantId,walletId,userId,agentId;
    String batchId;
    ModeOfpayment modeOfPayment;

}
