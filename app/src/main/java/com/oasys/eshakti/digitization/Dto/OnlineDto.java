package com.oasys.eshakti.digitization.Dto;

import java.util.ArrayList;

import lombok.Data;

@Data
public class OnlineDto {

    public String upiCollect;
    public String upiValidate;
    public String upiTransactionStatus;
    public String aeps;
    public ArrayList<Upi_VCS_Request> upiCollectRequest;
    public ArrayList<Upi_VCS_Request> upiValidateRequest;
    public ArrayList<Upi_VCS_Request> upiTransactionStatusRequest;


}
