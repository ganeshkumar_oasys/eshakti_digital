package com.oasys.eshakti.digitization.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.Dto.MemberList;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.GetSpanText;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.activity.LoginActivity;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;
import com.oasys.eshakti.digitization.database.MemberTable;
import com.oasys.eshakti.digitization.database.SHGTable;
import com.oasys.eshakti.digitization.views.CustomHorizontalScrollView;
import com.oasys.eshakti.digitization.views.Get_EdiText_Filter;
import com.oasys.eshakti.digitization.views.TextviewUtils;
import com.tutorialsee.lib.TastyToast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MemberCashWithdrawal extends Fragment implements View.OnClickListener,NewTaskListener {

    private View rootView;
    private TextView mGroupName;
    private TextView mCashinHand;
    private TextView mCashatBank;
    private TextView Wallet_balance, Wallet_CIH;
    private TextView mHeader, mAutoFilllabel, mMemberName;
    private LinearLayout mMemberNameLayout;
    private TableLayout mLeftHeaderTable;
    private int mSize = 0;
    private TableLayout mRightHeaderTable;
    private TableLayout mLeftContentTable;
    private TableLayout mRightContentTable;
    private CustomHorizontalScrollView mHSRightHeader;
    private CustomHorizontalScrollView mHSRightContent;
    private Button mSubmit_Raised_Button;
    private Button mPerviousButton;
    private Button mNextButton;
    String width[] = {AppStrings.memberName, AppStrings.savingsAmount, AppStrings.voluntarySavings};
    int[] rightHeaderWidth = new int[width.length];
    int[] rightContentWidth = new int[width.length];
    private EditText mSavings_values;
    private EditText mVSavings_values;
    private List<EditText> sSavingsFields;
    private List<EditText> sVSavingsFields;
    public String[] sSavingsAmounts;
    public String[] sVSavingsAmount;
    private Button mEdit_RaisedButton;
    private Button mOk_RaisedButton;
    private List<MemberList> memList;
    private ArrayList<MemberList> arrMem;
    private int sSavings = 0;
    private int vSavings = 0;
    private ListOfShg shgDto;
    private Dialog mProgressDilaog;
    private NetworkConnection networkConnection;
    private CheckBox mAutoFill;
    private String flag = "0";
    private MemberList member;
    private ArrayList<MemberList> listMember;
    public  String userId;
    private RestClient restClient;
    private  int mWalletamount;
    private  int mAniiamtorCH;
    private TextView daily_date_time;
    private String currentDateStr;
    DateFormat df = new SimpleDateFormat("dd/MM/yyyy");


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_deposit, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle bundle = getArguments();
        member = (MemberList) bundle.getSerializable("member");
        listMember = new ArrayList<>();
        listMember.add(member);

        mSize = listMember.size();
        memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        userId = MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, "");

        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        arrMem = new ArrayList<>();

        if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {
            onTaskStarted();
            restClient =RestClient.getRestClient(MemberCashWithdrawal.this);
            restClient.callWebServiceForGetMethod(Constants.BASE_URL + Constants.WALLETCASH_INHAND + userId, getActivity(), ServiceType.WALLET_CASHIN_HAND);

        } else {
            Utils.showToast(getActivity(), "Network Not Available");
        }

        init();
    }

    private void init() {

        try {
            Calendar calender = Calendar.getInstance();
            currentDateStr = df.format(calender.getTime());

            daily_date_time = (TextView) rootView.findViewById(R.id.daily_date_time);
            daily_date_time.setText(currentDateStr);
            sSavingsFields = new ArrayList<EditText>();
            sVSavingsFields = new ArrayList<EditText>();
            mGroupName = (TextView) rootView.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
            mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashinHand.setTypeface(LoginActivity.sTypeface);

            mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
            mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashatBank.setTypeface(LoginActivity.sTypeface);
            /** UI Mapping **/

            Wallet_balance = (TextView) rootView.findViewById(R.id.Wallet_balance);
//            Wallet_balance.setText(AppStrings.walletbalance + shgDto.getCashInHand());
//            Wallet_balance.setTypeface(LoginActivity.sTypeface);

            Wallet_CIH = (TextView) rootView.findViewById(R.id.Wallet_CIH);
//            Wallet_CIH.setText(AppStrings.walletCIH + "₹2100");
//            Wallet_CIH.setTypeface(LoginActivity.sTypeface);

            mHeader = (TextView) rootView.findViewById(R.id.fragmentHeader);
            mHeader.setText(AppStrings.mCashWithdrawal);
            mHeader.setTypeface(LoginActivity.sTypeface);

            mAutoFilllabel = (TextView) rootView.findViewById(R.id.autofillLabel);
            mAutoFilllabel.setText(AppStrings.autoFill);
            mAutoFilllabel.setTypeface(LoginActivity.sTypeface);
            mAutoFilllabel.setVisibility(View.VISIBLE);

            mAutoFill = (CheckBox) rootView.findViewById(R.id.autoFill);
            mAutoFill.setVisibility(View.VISIBLE);
            mAutoFill.setOnClickListener(this);

            mMemberNameLayout = (LinearLayout) rootView.findViewById(R.id.member_name_layout);
            mMemberName = (TextView) rootView.findViewById(R.id.member_name);
            // mMemberName.setTypeface(LoginActivity.sTypeface);

            Log.d("Savings", String.valueOf(mSize));

            mLeftHeaderTable = (TableLayout) rootView.findViewById(R.id.LeftHeaderTable);
            mRightHeaderTable = (TableLayout) rootView.findViewById(R.id.RightHeaderTable);
            mLeftContentTable = (TableLayout) rootView.findViewById(R.id.LeftContentTable);
            mRightContentTable = (TableLayout) rootView.findViewById(R.id.RightContentTable);

            mHSRightHeader = (CustomHorizontalScrollView) rootView.findViewById(R.id.rightHeaderHScrollView);
            mHSRightContent = (CustomHorizontalScrollView) rootView.findViewById(R.id.rightContentHScrollView);


            mHSRightHeader.setOnScrollChangedListener(new CustomHorizontalScrollView.onScrollChangedListener() {

                public void onScrollChanged(int l, int t, int oldl, int oldt) {
                    // TODO Auto-generated method stub

                    mHSRightContent.scrollTo(l, 0);

                }
            });

            mHSRightContent.setOnScrollChangedListener(new CustomHorizontalScrollView.onScrollChangedListener() {
                @Override
                public void onScrollChanged(int l, int t, int oldl, int oldt) {
                    // TODO Auto-generated method stub
                    mHSRightHeader.scrollTo(l, 0);
                }
            });
            TableRow leftHeaderRow = new TableRow(getActivity());

            TableRow.LayoutParams lHeaderParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);

            TextView mMemberName_headerText = new TextView(getActivity());
            mMemberName_headerText
                    .setText("" + String.valueOf(AppStrings.memberName));
            mMemberName_headerText.setTypeface(LoginActivity.sTypeface);
            mMemberName_headerText.setTextColor(Color.WHITE);
            mMemberName_headerText.setPadding(10, 5, 10, 5);
            mMemberName_headerText.setLayoutParams(lHeaderParams);
            mMemberName_headerText.setGravity(Gravity.CENTER);
            leftHeaderRow.addView(mMemberName_headerText);
            mLeftHeaderTable.addView(leftHeaderRow);

            TableRow rightHeaderRow = new TableRow(getActivity());
            TableRow.LayoutParams rHeaderParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            rHeaderParams.setMargins(10, 0, 20, 0);
            TextView mSavingsAmount_HeaderText = new TextView(getActivity());
            mSavingsAmount_HeaderText
                    .setText("" + String.valueOf(AppStrings.amount));
            mSavingsAmount_HeaderText.setTypeface(LoginActivity.sTypeface);

            mSavingsAmount_HeaderText.setTextColor(Color.WHITE);
            mSavingsAmount_HeaderText.setPadding(5, 5, 5, 5);
            mSavingsAmount_HeaderText.setLayoutParams(rHeaderParams);
            mSavingsAmount_HeaderText.setGravity(Gravity.CENTER);
            mSavingsAmount_HeaderText.setSingleLine(true);
            rightHeaderRow.addView(mSavingsAmount_HeaderText);

          /*  TextView mVSavingsAmount_HeaderText = new TextView(getActivity());
            mVSavingsAmount_HeaderText
                    .setText("" + String.valueOf(AppStrings.voluntarySavings));
            mVSavingsAmount_HeaderText.setTypeface(LoginActivity.sTypeface);
            mVSavingsAmount_HeaderText.setTextColor(Color.WHITE);

            mVSavingsAmount_HeaderText.setLayoutParams(rHeaderParams);
            mVSavingsAmount_HeaderText.setSingleLine(true);
            rightHeaderRow.addView(mVSavingsAmount_HeaderText);*/

            mRightHeaderTable.addView(rightHeaderRow);

            getTableRowHeaderCellWidth();

            for (int i = 0; i < mSize; i++) {
                TableRow leftContentRow = new TableRow(getActivity());

                TableRow.LayoutParams leftContentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 60, 1f);
                leftContentParams.setMargins(5, 5, 5, 5);

                final TextView memberName_Text = new TextView(getActivity());
                memberName_Text.setText(listMember.get(i).getMemberName());
                memberName_Text.setTextColor(R.color.black);
                memberName_Text.setPadding(5, 5, 5, 5);
                memberName_Text.setLayoutParams(leftContentParams);
                memberName_Text.setWidth(300);
                memberName_Text.setSingleLine(true);
                memberName_Text.setEllipsize(TextUtils.TruncateAt.END);
                leftContentRow.addView(memberName_Text);
                mLeftContentTable.addView(leftContentRow);

                TableRow rightContentRow = new TableRow(getActivity());
                TableRow.LayoutParams rightContentParams = new TableRow.LayoutParams(300,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 0f);
                rightContentParams.setMargins(20, 5, 10, 5);

                mSavings_values = new EditText(getActivity());
                mSavings_values.setId(i);
                sSavingsFields.add(mSavings_values);
                mSavings_values.setPadding(5, 5, 5, 5);
                mSavings_values.setBackgroundResource(R.drawable.edittext_background);
                mSavings_values.setLayoutParams(rightContentParams);
                mSavings_values.setTextAppearance(getActivity(), R.style.MyMaterialTheme);
                mSavings_values.setFilters(Get_EdiText_Filter.editText_filter());
                mSavings_values.setInputType(InputType.TYPE_CLASS_NUMBER);
                mSavings_values.setTextColor(R.color.black);
                // mSavings_values.setWidth(150);
                mSavings_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        // TODO Auto-generated method stub
                        if (hasFocus) {
                            ((EditText) v).setGravity(Gravity.LEFT);

                            mMemberNameLayout.setVisibility(View.VISIBLE);
                            mMemberName.setText(memberName_Text.getText().toString().trim());
                            TextviewUtils.manageBlinkEffect(mMemberName, getActivity());

                        } else {

                            ((EditText) v).setGravity(Gravity.RIGHT);
                            mMemberNameLayout.setVisibility(View.GONE);
                            mMemberName.setText("");
                        }

                    }
                });
                rightContentRow.addView(mSavings_values);

              /*  mVSavings_values = new EditText(getActivity());
                mVSavings_values.setId(i);
                sVSavingsFields.add(mVSavings_values);
                mVSavings_values.setPadding(5, 5, 5, 5);
                mVSavings_values.setBackgroundResource(R.drawable.edittext_background);
                mVSavings_values.setLayoutParams(rightContentParams);
                mVSavings_values.setTextAppearance(getActivity(), R.style.MyMaterialTheme);
                mVSavings_values.setFilters(Get_EdiText_Filter.editText_filter());
                mVSavings_values.setInputType(InputType.TYPE_CLASS_NUMBER);
                mVSavings_values.setTextColor(R.color.black);
                // mVSavings_values.setWidth(150);
                mVSavings_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        // TODO Auto-generated method stub
                        if (hasFocus) {
                            ((EditText) v).setGravity(Gravity.LEFT);

                            mMemberNameLayout.setVisibility(View.VISIBLE);
                            mMemberName.setText(memberName_Text.getText().toString().trim());
                            TextviewUtils.manageBlinkEffect(mMemberName, getActivity());

                        } else {

                            ((EditText) v).setGravity(Gravity.RIGHT);
                            mMemberNameLayout.setVisibility(View.GONE);
                            mMemberName.setText("");
                        }
                    }
                });
                rightContentRow.addView(mVSavings_values);*/

                mRightContentTable.addView(rightContentRow);

            }

            resizeMemberNameWidth();
            // resizeRightSideTable();

            resizeBodyTableRowHeight();

            mSubmit_Raised_Button = (Button) rootView.findViewById(R.id.fragment_Submit_button);
            mSubmit_Raised_Button.setText(AppStrings.submit);
            mSubmit_Raised_Button.setTypeface(LoginActivity.sTypeface);
            mSubmit_Raised_Button.setOnClickListener(this);

            mPerviousButton = (Button) rootView.findViewById(R.id.fragment_Previousbutton);
            //  mPerviousButton.setText("Savings" + AppStrings.mPervious);
            mPerviousButton.setOnClickListener(this);

            mNextButton = (Button) rootView.findViewById(R.id.fragment_Nextbutton);
            //     mNextButton.setText("Savings" + AppStrings.mNext);
            mNextButton.setOnClickListener(this);


            mPerviousButton.setVisibility(View.INVISIBLE);
            mNextButton.setVisibility(View.INVISIBLE);

        } catch (Exception e) {
            e.printStackTrace();

          /*  TastyToast.makeText(getActivity(), AppStrings.adminAlert, TastyToast.LENGTH_SHORT, TastyToast.WARNING);

            Intent intent = new Intent(getActivity(), ExitActivity.class);
            startActivity(intent);*/
        }
    }


    private void getTableRowHeaderCellWidth() {
        int lefHeaderChildCount = ((TableRow) mLeftHeaderTable.getChildAt(0)).getChildCount();
        int rightHeaderChildCount = ((TableRow) mRightHeaderTable.getChildAt(0)).getChildCount();

        for (int x = 0; x < (lefHeaderChildCount + rightHeaderChildCount); x++) {
            if (x == 0) {
                rightHeaderWidth[x] = viewWidth(((TableRow) mLeftHeaderTable.getChildAt(0)).getChildAt(x));
            } else {
                rightHeaderWidth[x] = viewWidth(((TableRow) mRightHeaderTable.getChildAt(0)).getChildAt(x - 1));
            }
        }
    }

    private void resizeMemberNameWidth() {
        // TODO Auto-generated method stub
        int leftHeadertWidth = viewWidth(mLeftHeaderTable);
        int leftContentWidth = viewWidth(mLeftContentTable);

        if (leftHeadertWidth < leftContentWidth) {
            mLeftHeaderTable.getLayoutParams().width = leftContentWidth;
        } else {
            mLeftContentTable.getLayoutParams().width = leftHeadertWidth;
        }
    }

    private void resizeBodyTableRowHeight() {

        int leftContentTable_ChildCount = mLeftContentTable.getChildCount();

        for (int x = 0; x < leftContentTable_ChildCount; x++) {

            TableRow leftContentTableRow = (TableRow) mLeftContentTable.getChildAt(x);
            TableRow rightContentTableRow = (TableRow) mRightContentTable.getChildAt(x);

            int rowLeftHeight = viewHeight(leftContentTableRow);
            int rowRightHeight = viewHeight(rightContentTableRow);

            TableRow tableRow = rowLeftHeight < rowRightHeight ? leftContentTableRow : rightContentTableRow;
            int finalHeight = rowLeftHeight > rowRightHeight ? rowLeftHeight : rowRightHeight;

            this.matchLayoutHeight(tableRow, finalHeight);
        }

    }

    private void matchLayoutHeight(TableRow tableRow, int height) {

        int tableRowChildCount = tableRow.getChildCount();

        // if a TableRow has only 1 child
        if (tableRow.getChildCount() == 1) {

            View view = tableRow.getChildAt(0);
            TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();
            params.height = height - (params.bottomMargin + params.topMargin);

            return;
        }

        // if a TableRow has more than 1 child
        for (int x = 0; x < tableRowChildCount; x++) {

            View view = tableRow.getChildAt(x);

            TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();

            if (!isTheHeighestLayout(tableRow, x)) {
                params.height = height - (params.bottomMargin + params.topMargin);
                return;
            }
        }

    }

    // check if the view has the highest height in a TableRow
    private boolean isTheHeighestLayout(TableRow tableRow, int layoutPosition) {

        int tableRowChildCount = tableRow.getChildCount();
        int heighestViewPosition = -1;
        int viewHeight = 0;

        for (int x = 0; x < tableRowChildCount; x++) {
            View view = tableRow.getChildAt(x);
            int height = this.viewHeight(view);

            if (viewHeight < height) {
                heighestViewPosition = x;
                viewHeight = height;
            }
        }
        return heighestViewPosition == layoutPosition;
    }

    // read a view's height
    private int viewHeight(View view) {
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        return view.getMeasuredHeight();
    }

    // read a view's width
    private int viewWidth(View view) {
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        return view.getMeasuredWidth();
    }

    public static int sSavings_Total, sVSavings_Total;
    public static String sSendToServer_Savings, sSendToServer_VSavings;
    String nullVlaue = "0";
    String[] confirmArr;
    Dialog confirmationDialog;


    @Override
    public void onClick(View view) {
        sSavingsAmounts = new String[sSavingsFields.size()];
        sVSavingsAmount = new String[sVSavingsFields.size()];

        Log.d("Saving", "sSavingsAmounts size : " + sSavingsFields.size() + "");

        switch (view.getId()) {

            case R.id.fragment_Submit_button:
                try {
                    sSavings_Total = 0;
                    sVSavings_Total = 0;
                    sSendToServer_Savings = "";
                    sSendToServer_VSavings = "";

                    confirmArr = new String[mSize];
                    // Do edit values here

                    if (arrMem != null && arrMem.size() > 0) {
                        arrMem.clear();
                    }

                    for (int i = 0; i < mSize; i++) {
                        MemberList ml = new MemberList();
                        ml.setMemberId(memList.get(i).getMemberId());
                        ml.setSavingsAmount(sSavingsFields.get(i).getText().toString());
                        //  ml.setVoluntarySavingsAmount(sVSavingsFields.get(i).getText().toString());

                        sSavingsAmounts[i] = sSavingsFields.get(i).getText().toString();
                        sSavings_Total += Integer.parseInt((!sSavingsFields.get(i).getText().toString().equals("") && sSavingsFields.get(i).getText().toString().length() > 0) ? sSavingsFields.get(i).getText().toString() : "0");
                     /*   sVSavingsAmount[i] = sVSavingsFields.get(i).getText().toString();
                        sVSavings_Total += Integer.parseInt((!sVSavingsFields.get(i).getText().toString().equals("") && sVSavingsFields.get(i).getText().toString().length() > 0) ? sVSavingsFields.get(i).getText().toString() : "0");*/
                        arrMem.add(ml);
                    }
                    Utils.sum_of_savings = sSavings_Total + sVSavings_Total;
                    Log.d("sum_of_saving", "" + Utils.sum_of_savings);

                    if ((sSavings_Total >= 100) && (sSavings_Total <= 10000)) {

                        confirmationDialog = new Dialog(getActivity());

                        LayoutInflater inflater = getActivity().getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.dialog_new_confirmation, null);

                        ViewGroup.LayoutParams lParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        dialogView.setLayoutParams(lParams);

                        TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
                        confirmationHeader.setText("" + AppStrings.confirmation);
                        confirmationHeader.setTypeface(LoginActivity.sTypeface);
                        TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

                        for (int i = 0; i < mSize; i++) {

                            TableRow indv_SavingsRow = new TableRow(getActivity());

                            TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                            contentParams.setMargins(10, 5, 10, 5);

                            TextView memberName_Text = new TextView(getActivity());
                            memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
                                    memList.get(i).getMemberName()));
                            memberName_Text.setTextColor(R.color.black);
                            memberName_Text.setPadding(5, 5, 5, 5);
                            memberName_Text.setSingleLine(true);
                            memberName_Text.setLayoutParams(contentParams);
                            indv_SavingsRow.addView(memberName_Text);

                            TextView confirm_values = new TextView(getActivity());
                            confirm_values
                                    .setText(GetSpanText.getSpanString(getActivity(), String.valueOf((sSavingsAmounts[i] != null && sSavingsAmounts[i].length() > 0) ? sSavingsAmounts[i] : "0")));
                            confirm_values.setTextColor(R.color.black);
                            confirm_values.setPadding(5, 5, 5, 5);
                            confirm_values.setGravity(Gravity.RIGHT);
                            confirm_values.setLayoutParams(contentParams);
                            indv_SavingsRow.addView(confirm_values);

                           /* TextView confirm_VSvalues = new TextView(getActivity());
                            confirm_VSvalues
                                    .setText(GetSpanText.getSpanString(getActivity(), String.valueOf((sVSavingsAmount[i] != null && sVSavingsAmount[i].length() > 0) ? sVSavingsAmount[i] : "0")));
                            confirm_VSvalues.setTextColor(R.color.black);
                            confirm_VSvalues.setPadding(5, 5, 5, 5);
                            confirm_VSvalues.setGravity(Gravity.RIGHT);
                            confirm_VSvalues.setLayoutParams(contentParams);
                            indv_SavingsRow.addView(confirm_VSvalues);*/

                            confirmationTable.addView(indv_SavingsRow,
                                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                        }

                        View rullerView = new View(getActivity());
                        rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
                        rullerView.setBackgroundColor(Color.rgb(0, 199, 140));// rgb(255,
                        // 229,
                        // 242));
                        confirmationTable.addView(rullerView);

                        TableRow totalRow = new TableRow(getActivity());

                        TableRow.LayoutParams totalParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                        totalParams.setMargins(10, 5, 10, 5);

                        TextView totalText = new TextView(getActivity());
                        totalText.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.total)));
                        totalText.setTextColor(R.color.black);
                        totalText.setPadding(5, 5, 5, 5);// (5, 10, 5, 10);
                        totalText.setLayoutParams(totalParams);
                        totalRow.addView(totalText);

                        TextView totalAmount = new TextView(getActivity());
                        totalAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sSavings_Total)));
                        totalAmount.setTextColor(R.color.black);
                        totalAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
                        totalAmount.setGravity(Gravity.RIGHT);
                        totalAmount.setLayoutParams(totalParams);
                        totalRow.addView(totalAmount);
/*
                        TextView totalVSAmount = new TextView(getActivity());
                        totalVSAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sVSavings_Total)));
                        totalVSAmount.setTextColor(R.color.black);
                        totalVSAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
                        totalVSAmount.setGravity(Gravity.RIGHT);
                        totalVSAmount.setLayoutParams(totalParams);
                        totalRow.addView(totalVSAmount);*/


                        confirmationTable.addView(totalRow,
                                new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                        TableRow totalwalletRow = new TableRow(getActivity());

                 /*   TableRow.LayoutParams totalwalletParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                    totalwalletParams.setMargins(10, 5, 10, 5);

                    TextView totalwalletText = new TextView(getActivity());
                    totalwalletText.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.walletCIH)));
                    totalwalletText.setTextColor(R.color.green_border_color);
                    totalwalletText.setPadding(5, 5, 5, 5);// (5, 10, 5, 10);
                    totalwalletText.setLayoutParams(totalwalletParams);
                    totalwalletRow.addView(totalwalletText);

                    TextView totalwalletAmount = new TextView(getActivity());
                    totalwalletAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(((Integer.parseInt(shgDto.getCashInHand()))))));// + sSavings_Total
                    totalwalletAmount.setTextColor(R.color.green_border_color);
                    totalwalletAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
                    totalwalletAmount.setGravity(Gravity.RIGHT);
                    totalwalletAmount.setLayoutParams(totalwalletParams);
                    totalwalletRow.addView(totalwalletAmount);*/

//                        TextView totalVSAmount = new TextView(getActivity());
//                        totalVSAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sVSavings_Total)));
//                        totalVSAmount.setTextColor(R.color.black);
//                        totalVSAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
//                        totalVSAmount.setGravity(Gravity.RIGHT);
//                        totalVSAmount.setLayoutParams(totalParams);
//                        totalRow.addView(totalVSAmount);


                        confirmationTable.addView(totalwalletRow,
                                new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));


                        mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit);
                        mEdit_RaisedButton.setText(AppStrings.edit);
                        mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
                        mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
                        // 205,
                        // 0));
                        mEdit_RaisedButton.setOnClickListener(this);

                        mOk_RaisedButton = (Button) dialogView.findViewById(R.id.frag_Ok);
                        mOk_RaisedButton.setText(AppStrings.yes);
                        mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
                        mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
                        mOk_RaisedButton.setOnClickListener(this);

                        confirmationDialog.getWindow()
                                .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        confirmationDialog.setCanceledOnTouchOutside(false);
                        confirmationDialog.setContentView(dialogView);
                        confirmationDialog.setCancelable(true);
                        confirmationDialog.show();

                        ViewGroup.MarginLayoutParams margin = (ViewGroup.MarginLayoutParams) dialogView.getLayoutParams();
                        margin.leftMargin = 10;
                        margin.rightMargin = 10;
                        margin.topMargin = 10;
                        margin.bottomMargin = 10;
                        margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);

                    } else {

                        TastyToast.makeText(getActivity(), "Enter a valid amount(Between 100 - 10000)", TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);

                        sSendToServer_Savings = "";
                        sSendToServer_VSavings = "";
                        sSavings_Total = 0;
                        sVSavings_Total = 0;
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.fragment_Edit:
                sSendToServer_Savings = "";
                sSendToServer_VSavings = "";
                sSavings_Total = 0;
                sVSavings_Total = 0;
                mSubmit_Raised_Button.setClickable(true);

                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();

                break;
            case R.id.frag_Ok:
                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();

                FingerPrintAuthentication fp = new FingerPrintAuthentication();
                Bundle bundle = new Bundle();
                bundle.putString("uid", "904473369640");
                bundle.putSerializable("member", member);
                if (Utils.sum_of_savings > 0)
                    bundle.putInt("amt", Utils.sum_of_savings);
                else
                    bundle.putInt("amt", 0);
                fp.setArguments(bundle);
                NewDrawerScreen.showFragment(fp);

//TODO::
               /* if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();
                SavingRequest sr = new SavingRequest();
                sr.setSavingsAmount(arrMem);
                sr.setShgId(shgDto.getShgId());
                sr.setModeOfCash("2");
                sr.setMobileDate(System.currentTimeMillis() + "");
                sr.setTransactionDate(shgDto.getLastTransactionDate());
                String sreqString = new Gson().toJson(sr);
                if (networkConnection.isNetworkAvailable()) {
                    onTaskStarted();
                    RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.SAVINGS, sreqString, getActivity(), ServiceType.SAVINGS);
                }*/

                break;
            case R.id.fragment_Previousbutton:
                break;
            case R.id.fragment_Nextbutton:
                break;

            case R.id.autoFill:

                String similiar_Savings;

                if (mAutoFill.isChecked()) {

                    try {

                        // Makes all edit fields holds the same savings
                        if (!String.valueOf(sSavingsFields.get(0).getText()).equals("")) {


                            similiar_Savings = sSavingsFields.get(0).getText().toString();

                            // send_To_Server_SavingsType = AppStrings.savings;

                            System.out.println("CB Amount : " + similiar_Savings);

                            for (int i = 0; i < sSavingsAmounts.length; i++) {
                                sSavingsFields.get(i).setText(similiar_Savings);
                                sSavingsFields.get(i).setGravity(Gravity.RIGHT);
                                sSavingsFields.get(i).clearFocus();
                                sSavingsAmounts[i] = similiar_Savings;
                            }

                        }

                        /** To clear the values of EditFields in case of uncheck **/

                    } catch (ArrayIndexOutOfBoundsException e) {
                        e.printStackTrace();

                        TastyToast.makeText(getActivity(), AppStrings.adminAlert, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);

                    }
                } else {
                    // Do check box unCheck
                }

                break;
            default:
                break;
        }
    }

    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {

        if (mProgressDilaog != null) {
            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                mProgressDilaog.dismiss();
                mProgressDilaog = null;
//                if (confirmationDialog.isShowing())
//                    confirmationDialog.dismiss();
            }


            switch (serviceType) {

                case WALL_TXANIMATOR:
                    Log.d("ddd","txt");

                    try {
                        ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                        String message = cdto.getMessage();
                        int statusCode = cdto.getStatusCode();
                        if (statusCode == Utils.Success_Code) {
                            Utils.showToast(getActivity(), message);
                            String walletamount = cdto.getResponseContent().getWalletAmount();
                            mWalletamount= (int)Double.parseDouble(walletamount);
                            Wallet_balance.setText(AppStrings.w_b + String.valueOf(mWalletamount));
                            Wallet_balance.setTypeface(LoginActivity.sTypeface);
                        } else {
                            Utils.showToast(getActivity(), message);

                            if (statusCode == 401) {

                                Log.e("Group Logout", "Logout Sucessfully");
                                AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                                if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                    mProgressDilaog.dismiss();
                                    mProgressDilaog = null;
                                }
                            }
                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;

                case WALLET_CASHIN_HAND:
                    try {
                        ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                        String message = cdto.getMessage();
                        int statusCode = cdto.getStatusCode();
                        if (statusCode == Utils.Success_Code) {
                            Utils.showToast(getActivity(), message);
                            String animatorCashinHand = cdto.getResponseContent().getCurrentCashInHand();
                            mAniiamtorCH = (int)Double.parseDouble(animatorCashinHand);
                            Wallet_CIH.setText(AppStrings.w_cih+ mAniiamtorCH);
                            Wallet_CIH.setTypeface(LoginActivity.sTypeface);
                        } else {
                            Utils.showToast(getActivity(), message);

                            if (statusCode == 401) {

                                Log.e("Group Logout", "Logout Sucessfully");
                                AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                                if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                    mProgressDilaog.dismiss();
                                    mProgressDilaog = null;
                                }
                            }

                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    onTaskStarted();
                    restClient.callWebServiceForGetMethod(Constants.BASE_URL + Constants.WALL_BAL + userId, getActivity(), ServiceType.WALL_TXANIMATOR);
                    break;
            }
        }

    }
}



