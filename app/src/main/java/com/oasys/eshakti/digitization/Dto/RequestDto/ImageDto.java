package com.oasys.eshakti.digitization.Dto.RequestDto;

import java.io.Serializable;

import lombok.Data;

@Data
public class ImageDto implements Serializable {

    private String url;
}
