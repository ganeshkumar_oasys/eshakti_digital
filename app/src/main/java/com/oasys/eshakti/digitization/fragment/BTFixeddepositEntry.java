package com.oasys.eshakti.digitization.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.digitization.Dto.AddBTDto;
import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.Dto.MemberList;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.Dto.ShgBankDetails;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.GetSpanText;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.activity.LoginActivity;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;
import com.oasys.eshakti.digitization.database.BankTable;
import com.oasys.eshakti.digitization.database.MemberTable;
import com.oasys.eshakti.digitization.database.SHGTable;
import com.oasys.eshakti.digitization.views.Get_EdiText_Filter;
import com.tutorialsee.lib.TastyToast;

import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;

import java.util.ArrayList;
import java.util.List;

public class BTFixeddepositEntry extends Fragment implements View.OnClickListener, NewTaskListener {

    private View rootView;

    public static String TAG = BTFixeddepositEntry.class.getSimpleName();
    private TextView mGroupName, mCashInHand, mCashAtBank, mHeadertext;
    private Button mRaised_Submit_Button, mEdit_RaisedButton, mOk_RaisedButton;
    public static EditText mEditAmount;

    public static String sSend_To_Server_FD;
    public static String[] sFixedDepositItem;

    public static List<EditText> sEditTextFields = new ArrayList<EditText>();
    public static String[] sFixedDepositAmount;
    boolean alert;
    public static Boolean isFixedDepositEntry = false;
    public static int sFixedDepositTotal;

    String toBeEditArr[];
    int size;
    public static int bank_deposit, bank_withdrawl, bank_expenses, bank_interest;

    private Dialog mProgressDilaog;
    Dialog confirmationDialog;

    String mDeposit_Fixed_offline, mInterest_Fixed_offline, mWithdrawl_Fixed_offline, mBankExpenses_Fixed_offline;

    String amount = "0.0";
    public static String fd_Response[];
    String mLastTrDate = null, mLastTr_ID = null;
    String mOfflineBankname;
    boolean isGetTrid = false;
    String mSelectedCashatBank = "0.0";
    public static String mCashatBank_individual = null;
    boolean isServiceCall = false;
    private int mSize;
    private List<MemberList> memList;
    private ListOfShg shgDto;
    private ArrayList<ShgBankDetails> bankdetails;
    private String[] menuStr;

    String mSqliteDBStoredValues_FDValues = null;
    private NetworkConnection networkConnection;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_new_deposit_entry, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mSize = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, "")).size();
        memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        bankdetails = BankTable.getBankName(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        sSend_To_Server_FD = "0";
        sFixedDepositTotal = 0;

        //   mSelectedCashatBank = .sSelected_BankDepositAmount;

        sFixedDepositItem = new String[]{AppStrings.bankDeposit,
                AppStrings.bankInterest,
                AppStrings.withdrawl,
                AppStrings.bankExpenses};
        sEditTextFields = new ArrayList<EditText>();

        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        if (networkConnection.isNetworkAvailable()) {            //  onTaskStarted();
            RestClient.getRestClient(this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.BT_CB_FD_VALUE + BankTransaction.sSelectedBank.getShgSavingsAccountId(), getActivity(), ServiceType.FD_VALUE);
        }
    }

    private void init() {
        mGroupName = (TextView) rootView.findViewById(R.id.groupname);
        mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
        mGroupName.setTypeface(LoginActivity.sTypeface);
        mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
        mCashInHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
        mCashInHand.setTypeface(LoginActivity.sTypeface);
        mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
        mCashAtBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
        mCashAtBank.setTypeface(LoginActivity.sTypeface);
        mHeadertext = (TextView) rootView.findViewById(R.id.fragment_deposit_entry_headertext);
        mHeadertext.setText(AppStrings.fixedDeposit);
        mHeadertext.setTypeface(LoginActivity.sTypeface);
        mRaised_Submit_Button = (Button) rootView.findViewById(R.id.fragment_deposit_entry_Submitbutton);
        mRaised_Submit_Button.setText(AppStrings.submit);
        mRaised_Submit_Button.setTypeface(LoginActivity.sTypeface);
        mRaised_Submit_Button.setOnClickListener(this);

        size = sFixedDepositItem.length;
        System.out.println("Size of values:>>>" + size);

        try {

            TableLayout tableLayout = (TableLayout) rootView.findViewById(R.id.fragmentDeposit_contentTable);
            TableRow outerRow = new TableRow(getActivity());

            TableRow.LayoutParams header_ContentParams = new TableRow.LayoutParams(250, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            header_ContentParams.setMargins(10, 5, 10, 5);

            TextView bankName = new TextView(getActivity());
            bankName.setText(GetSpanText.getSpanString(getActivity(),
                    String.valueOf(BankTransaction.sSelected_BankName)));
            bankName.setPadding(15, 5, 5, 5);
            bankName.setTextColor(R.color.black);
            bankName.setLayoutParams(header_ContentParams);
            outerRow.addView(bankName);

            TableRow.LayoutParams amountParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            amountParams.setMargins(5, 5, 20, 5);

            TextView bankDeposit_Amount = new TextView(getActivity());
            bankDeposit_Amount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(amount)));
            bankDeposit_Amount.setTextColor(R.color.black);
            bankDeposit_Amount.setPadding(5, 5, 5, 5);
            bankDeposit_Amount.setGravity(Gravity.RIGHT);
            bankDeposit_Amount.setLayoutParams(amountParams);
            outerRow.addView(bankDeposit_Amount);

            tableLayout.addView(outerRow, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            for (int i = 0; i < size; i++) {

                TableRow innerRow = new TableRow(getActivity());

                TextView memberName = new TextView(getActivity());
                memberName.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sFixedDepositItem[i])));
                memberName.setTypeface(LoginActivity.sTypeface);
                memberName.setPadding(15, 0, 5, 5);
                memberName.setLayoutParams(header_ContentParams);
                memberName.setTextColor(R.color.black);
                innerRow.addView(memberName);

                mEditAmount = new EditText(getActivity());
                sEditTextFields.add(mEditAmount);
                mEditAmount.setId(i);
                mEditAmount.setInputType(InputType.TYPE_CLASS_NUMBER);
                mEditAmount.setPadding(5, 5, 5, 5);
                mEditAmount.setFilters(Get_EdiText_Filter.editText_filter());
                mEditAmount.setGravity(Gravity.RIGHT);
                mEditAmount.setLayoutParams(amountParams);
                mEditAmount.setWidth(150);
                mEditAmount.setBackgroundResource(R.drawable.edittext_background);
                mEditAmount.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        // TODO Auto-generated method stub
                        if (hasFocus) {
                            ((EditText) v).setGravity(Gravity.LEFT);
                        } else {
                            ((EditText) v).setGravity(Gravity.RIGHT);
                        }

                    }
                });
                innerRow.addView(mEditAmount);

                tableLayout.addView(innerRow,
                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onClick(View view) {
        sFixedDepositAmount = new String[sEditTextFields.size()];

        switch (view.getId()) {
            case R.id.fragment_deposit_entry_Submitbutton:

                try {
                    sFixedDepositTotal = Integer.valueOf("0");
                    sSend_To_Server_FD = "0";

                    for (int i = 0; i < sFixedDepositAmount.length; i++) {

                        sFixedDepositAmount[i] = String.valueOf(sEditTextFields.get(i).getText());

                        if (sFixedDepositAmount[i] == null || sFixedDepositAmount[i].equals("")) {
                            sFixedDepositAmount[i] = "0";
                        }

                        if (sFixedDepositAmount[i].matches("\\d*\\.?\\d+")) { // match
                            Log.e("Double valuesssssss", sFixedDepositAmount[i]);
                            int amount = (int) Math.round(Double.parseDouble(sFixedDepositAmount[i]));
                            sFixedDepositAmount[i] = String.valueOf(amount);
                        }

                        Log.e("Integer valuesssssss", sFixedDepositAmount[i]);

                        sFixedDepositTotal = sFixedDepositTotal + Integer.parseInt(sFixedDepositAmount[i]);

                        sSend_To_Server_FD = sSend_To_Server_FD + sFixedDepositAmount[i] + "~";

                    }
                    System.out.println(" Total Value:>>>>" + sFixedDepositTotal);

                    System.out.println("Send To Confirmation Value:>>>" + sSend_To_Server_FD);

                    bank_deposit = Integer.parseInt(sFixedDepositAmount[0]);
                    bank_interest = Integer.parseInt(sFixedDepositAmount[1]);
                    bank_withdrawl = Integer.parseInt(sFixedDepositAmount[2]);
                    bank_expenses = Integer.parseInt(sFixedDepositAmount[3]);

                    System.out.println("Bank Deposit:>>>" + bank_deposit);
                    System.out.println("Bank Withdrawl:>>>" + bank_withdrawl);

                    int fdAvailableWithdrawlAmt = (int) Double.parseDouble(amount.trim()) + Integer.parseInt(sFixedDepositAmount[0])
                            + Integer.parseInt(sFixedDepositAmount[1]) - Integer.parseInt(sFixedDepositAmount[3]);

                    System.out.println("Fixed Deposit Available Withdrawl Amount:" + fdAvailableWithdrawlAmt);
                    //   if (sFixedDepositTotal != 0 && bank_withdrawl <= fdAvailableWithdrawlAmt) {
                    if (sFixedDepositTotal != 0 && bank_deposit <= ((int) Double.parseDouble(mSelectedCashatBank.trim()))
                            && bank_withdrawl <= fdAvailableWithdrawlAmt
                            && bank_expenses <=((int) Double.parseDouble(mSelectedCashatBank.trim()))) {

                        mDeposit_Fixed_offline = sFixedDepositAmount[0];
                        mInterest_Fixed_offline = sFixedDepositAmount[1];
                        mWithdrawl_Fixed_offline = sFixedDepositAmount[2];
                        mBankExpenses_Fixed_offline = sFixedDepositAmount[3];

                        confirmationDialog = new Dialog(getActivity());

                        LayoutInflater inflater = getActivity().getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.dialog_new_confirmation, null);
                        dialogView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT));

                        TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
                        confirmationHeader.setText(AppStrings.confirmation);
                        confirmationHeader.setTypeface(LoginActivity.sTypeface);
                        TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

                        for (int i = 0; i < size; i++) {

                            TableRow indv_DepositEntryRow = new TableRow(getActivity());

                            TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                            contentParams.setMargins(10, 5, 10, 5);

                            TextView memberName_Text = new TextView(getActivity());
                            memberName_Text.setText(
                                    GetSpanText.getSpanString(getActivity(), String.valueOf(sFixedDepositItem[i])));
                            memberName_Text.setTypeface(LoginActivity.sTypeface);
                            memberName_Text.setTextColor(R.color.black);
                            memberName_Text.setPadding(5, 5, 5, 5);
                            memberName_Text.setLayoutParams(contentParams);
                            indv_DepositEntryRow.addView(memberName_Text);

                            TextView confirm_values = new TextView(getActivity());
                            confirm_values.setText(
                                    GetSpanText.getSpanString(getActivity(), String.valueOf(sFixedDepositAmount[i])));
                            confirm_values.setTextColor(R.color.black);
                            confirm_values.setPadding(5, 5, 5, 5);
                            confirm_values.setGravity(Gravity.RIGHT);
                            confirm_values.setLayoutParams(contentParams);
                            indv_DepositEntryRow.addView(confirm_values);

                            confirmationTable.addView(indv_DepositEntryRow,
                                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                        }
                        mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit);
                        mEdit_RaisedButton.setText(AppStrings.edit);
                        mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
                        mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
                        // 205,
                        // 0));
                        mEdit_RaisedButton.setOnClickListener(this);

                        mOk_RaisedButton = (Button) dialogView.findViewById(R.id.frag_Ok);
                        mOk_RaisedButton.setText(AppStrings.yes);
                        mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
                        mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
                        mOk_RaisedButton.setOnClickListener(this);

                        confirmationDialog.getWindow()
                                .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        confirmationDialog.setCanceledOnTouchOutside(false);
                        confirmationDialog.setContentView(dialogView);
                        confirmationDialog.setCancelable(true);
                        confirmationDialog.show();

                        ViewGroup.MarginLayoutParams margin = (ViewGroup.MarginLayoutParams) dialogView.getLayoutParams();
                        margin.leftMargin = 10;
                        margin.rightMargin = 10;
                        margin.topMargin = 10;
                        margin.bottomMargin = 10;
                        margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);

                    } else if (bank_withdrawl > fdAvailableWithdrawlAmt) {

                        TastyToast.makeText(getActivity(), AppStrings.cashatBankAlert, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);
                        sSend_To_Server_FD = "0";
                        sFixedDepositTotal = Integer.valueOf("0");

                    } else if (bank_withdrawl > fdAvailableWithdrawlAmt
                            || bank_expenses >((int) Double.parseDouble(mSelectedCashatBank.trim()))) {

                        if (bank_expenses > ((int) Double.parseDouble(mSelectedCashatBank.trim()))) {

                            TastyToast.makeText(getActivity(), com.yesteam.eshakti.appConstants.AppStrings.cashatBankAlert, TastyToast.LENGTH_SHORT,
                                    TastyToast.WARNING);
                        } else if (bank_withdrawl > fdAvailableWithdrawlAmt) {

                            TastyToast.makeText(getActivity(), com.yesteam.eshakti.appConstants.AppStrings.mCheckFixedDepositAmount, TastyToast.LENGTH_SHORT,
                                    TastyToast.WARNING);

                        }

                        sSend_To_Server_FD = "0";
                        sFixedDepositTotal = Integer.valueOf("0");

                    } else {
                        TastyToast.makeText(getActivity(), AppStrings.DepositnullAlert, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);

                        sSend_To_Server_FD = "0";
                        sFixedDepositTotal = Integer.valueOf("0");

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;

            case R.id.fragment_Edit:

                confirmationDialog.dismiss();
                sSend_To_Server_FD = "0";
                sFixedDepositTotal = Integer.valueOf("0");
                mRaised_Submit_Button.setClickable(true);
                isServiceCall = false;
                break;

            case R.id.frag_Ok:

                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();
                AddBTDto bt = new AddBTDto();
                bt.setSavingsBankId(BankTransaction.sSelectedBank.getShgSavingsAccountId());
                bt.setBankInterest(bank_interest + "");
                bt.setBankCharges(bank_expenses + "");
                bt.setShgId(shgDto.getShgId());
                bt.setWithdrawal(bank_withdrawl + "");
                bt.setCashDeposit(bank_deposit + "");
                bt.setMobileDate(System.currentTimeMillis() + "");
                bt.setTransactionDate(shgDto.getLastTransactionDate());
                String sreqString = new Gson().toJson(bt);
                if (networkConnection.isNetworkAvailable()) {
                    onTaskStarted();
                    RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.BT_FIXED, sreqString, getActivity(), ServiceType.BT_FIXED);
                }
                break;
        }
    }

    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        if (mProgressDilaog != null) {
            mProgressDilaog.dismiss();
            mProgressDilaog = null;
           /* if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {

                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();
            }*/
        }
        switch (serviceType) {
            case BT_FIXED:

                try {
                    ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    String message = cdto.getMessage();
                    int statusCode = cdto.getStatusCode();
                    if (statusCode == Utils.Success_Code) {
                        Utils.showToast(getActivity(), message);

                        if (shgDto.getFFlag() == null || !shgDto.getFFlag().equals("1")) {
                            SHGTable.updateTransactionSHGDetails(shgDto.getShgId());
                        }
                        FragmentManager fm = getFragmentManager();
                        fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        MainFragment mainFragment =new MainFragment();
                        Bundle bundles = new Bundle();
                        bundles.putString("Transaction",MainFragment.Flag_Transaction);
                        mainFragment.setArguments(bundles);
                        NewDrawerScreen.showFragment(mainFragment);
//                        MemberDrawerScreen.showFragment(new MainFragment());

                    } else {


                        if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        }
                        Utils.showToast(getActivity(), message);

                    }
                } catch (Exception e) {

                }
                break;


            case FD_VALUE:

                try {
                    ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    String message = cdto.getMessage();
                    int statusCode = cdto.getStatusCode();
                    if (statusCode == Utils.Success_Code) {
                        Utils.showToast(getActivity(), message);

                        amount = cdto.getResponseContent().getSavingsBalance().getCurrentFixedDeposit();
                        mSelectedCashatBank = cdto.getResponseContent().getSavingsBalance().getCurrentBalance();
                        init();

                    } else {


                        if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        }
                        Utils.showToast(getActivity(), message);

                    }
                } catch (Exception e) {

                }
                break;

        }

    }
}
