package com.oasys.eshakti.digitization.Dto.RequestDto;

import java.io.Serializable;

import lombok.Data;

/**
 * Created by MuthukumarPandi on 12/12/2018.
 */
@Data
public class ShgAccountNumbersUpdateDTOList implements Serializable {

    private String accountNumber;

    private String bankId;

    private String shgId;
}
