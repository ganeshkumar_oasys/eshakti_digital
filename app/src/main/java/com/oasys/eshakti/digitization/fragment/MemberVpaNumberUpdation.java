package com.oasys.eshakti.digitization.fragment;


import android.app.AlertDialog;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.database.SHGTable;
import com.yesteam.eshakti.Config.utils.RegionalConversion;
import com.yesteam.eshakti.appConstants.AppStrings;
import com.yesteam.eshakti.view.activity.LoginActivity;
import com.yesteam.eshakti.views.CustomHorizontalScrollView;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class MemberVpaNumberUpdation extends Fragment implements NewTaskListener {
    private View rootview;
    private TextView mGroupName, mCashinHand, mCashatBank, mHeader;
    private ListOfShg shgDto;
    LinearLayout mMemberNameLayout;
    TextView mMemberName;
    private TableLayout mLeftHeaderTable, mRightHeaderTable, mLeftContentTable, mRightContentTable,mRightContentTable1;
    private CustomHorizontalScrollView mHSRightHeader, mHSRightContent;
    private ResponseDto responseDto;
    private  ArrayList<String> vpaname;
    private  ArrayList<String> vpanumber;
    private  ArrayList<String> vpaverify_list;
    private  ArrayList<String> vpaverify_sbaccount;
    private String name,id,vpano,vpaverify,sbaccount;
    private  TextView  mVpaverify, verify;
    private String verifystatus;
    int j, selId;
    private AlertDialog.Builder builder;
    private  LayoutInflater layoutinflater;
    private EditText vpa_input_value;
    private Button vpa_ok,vpa_cancel;
    private  AlertDialog alertdialog;

    public MemberVpaNumberUpdation() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootview = inflater.inflate(R.layout.fragment_member_vpa_number_updation, container, false);
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));

        vpaname= new ArrayList<>();
        vpanumber = new ArrayList<>();
        vpaverify_list = new ArrayList<>();
        vpaverify_sbaccount = new ArrayList<>();


        try {

            mGroupName = (TextView) rootview.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashinHand = (TextView) rootview.findViewById(R.id.cashinHand);
            mCashinHand.setText(com.oasys.eshakti.digitization.OasysUtils.AppStrings.cashinhand + shgDto.getCashInHand());
            mCashinHand.setTypeface(LoginActivity.sTypeface);

            mCashatBank = (TextView) rootview.findViewById(R.id.cashatBank);
            mCashatBank.setText(com.oasys.eshakti.digitization.OasysUtils.AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashatBank.setTypeface(LoginActivity.sTypeface);

            mHeader = (TextView) rootview.findViewById(R.id.fragmentHeader);
            mHeader.setText("MEMBER VPA NUMBER UPDATION");
            mHeader.setTypeface(LoginActivity.sTypeface);

            mMemberNameLayout = (LinearLayout) rootview.findViewById(R.id.member_name_layout);
            mMemberName = (TextView) rootview.findViewById(R.id.member_name);


            mLeftHeaderTable = (TableLayout) rootview.findViewById(R.id.LeftHeaderTable);
            mRightHeaderTable = (TableLayout) rootview.findViewById(R.id.RightHeaderTable);
            mLeftContentTable = (TableLayout) rootview.findViewById(R.id.LeftContentTable);
            mRightContentTable = (TableLayout) rootview.findViewById(R.id.RightContentTable);
//            mRightContentTable1 = (TableLayout) rootview.findViewById(R.id.RightContentTable1);


            mHSRightHeader = (CustomHorizontalScrollView) rootview.findViewById(R.id.rightHeaderHScrollView);
            mHSRightContent = (CustomHorizontalScrollView) rootview.findViewById(R.id.rightContentHScrollView);

            mHSRightHeader.setOnScrollChangedListener(new CustomHorizontalScrollView.onScrollChangedListener() {
                @Override
                public void onScrollChanged(int l, int t, int oldl, int oldt) {

                    mHSRightContent.scrollTo(l, 0);
                }
            });


            mHSRightContent.setOnScrollChangedListener(new CustomHorizontalScrollView.onScrollChangedListener() {
                @Override
                public void onScrollChanged(int l, int t, int oldl, int oldt) {
                    mHSRightHeader.scrollTo(l, 0);
                }
            });

            TableRow leftHeaderRow = new TableRow(getActivity());

            TableRow.LayoutParams lHeaderParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);

            TextView mMemberName_Header = new TextView(getActivity());
            mMemberName_Header.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.memberName)));
            mMemberName_Header.setTypeface(LoginActivity.sTypeface);
            mMemberName_Header.setWidth(300);
            mMemberName_Header.setTextColor(Color.WHITE);
            mMemberName_Header.setPadding(10, 5, 10, 5);
            mMemberName_Header.setLayoutParams(lHeaderParams);
            leftHeaderRow.addView(mMemberName_Header);

            mLeftHeaderTable.addView(leftHeaderRow);

            TableRow rightHeaderRow = new TableRow(getActivity());

            /*TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            contentParams.setMargins(10, 0, 10, 0);

            TextView mOutstanding_Header = new TextView(getActivity());
            mOutstanding_Header.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.accountNummber)));
            mOutstanding_Header.setTypeface(LoginActivity.sTypeface);
            mOutstanding_Header.setTextColor(Color.WHITE);
            mOutstanding_Header.setPadding(10, 5, 10, 5);
            mOutstanding_Header.setGravity(Gravity.LEFT);
            mOutstanding_Header.setLayoutParams(contentParams);
            mOutstanding_Header.setBackgroundResource(R.color.tableHeader);
            rightHeaderRow.addView(mOutstanding_Header);*/

            TableRow.LayoutParams POLParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            POLParams.setMargins(10, 0, 10, 0);

            TextView mPurposeOfLoan_Header = new TextView(getActivity());
            mPurposeOfLoan_Header
                    .setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.vpaNummber)));
            mPurposeOfLoan_Header.setTypeface(LoginActivity.sTypeface);
            mPurposeOfLoan_Header.setTextColor(Color.WHITE);
            mPurposeOfLoan_Header.setGravity(Gravity.RIGHT);
            mPurposeOfLoan_Header.setLayoutParams(POLParams);
            mPurposeOfLoan_Header.setPadding(25, 5, 10, 5);
            mPurposeOfLoan_Header.setBackgroundResource(R.color.tableHeader);
            rightHeaderRow.addView(mPurposeOfLoan_Header);

            TableRow.LayoutParams VERParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            VERParams.setMargins(10, 0, 10, 0);

            TextView mVerify_Header = new TextView(getActivity());
            mVerify_Header
                    .setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.memberStatus)));
            mVerify_Header.setTypeface(LoginActivity.sTypeface);
            mVerify_Header.setTextColor(Color.WHITE);
            mVerify_Header.setGravity(Gravity.RIGHT);
            mVerify_Header.setLayoutParams(VERParams);
            mVerify_Header.setPadding(40, 5, 10, 5);
            mVerify_Header.setBackgroundResource(R.color.tableHeader);
            rightHeaderRow.addView(mVerify_Header);


            TableRow.LayoutParams rHeaderParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            rHeaderParams.setMargins(13, 0, 10, 0);

            TextView mPL_Header = new TextView(getActivity());
            mPL_Header.setText(RegionalConversion.getRegionalConversion(String.valueOf(AppStrings.verifyStatus)));
            mPL_Header.setTypeface(LoginActivity.sTypeface);
            mPL_Header.setTextColor(Color.WHITE);
            mPL_Header.setPadding(10, 5, 10, 5);
            mPL_Header.setGravity(Gravity.CENTER);
            mPL_Header.setLayoutParams(rHeaderParams);
            mPL_Header.setBackgroundResource(R.color.tableHeader);
            rightHeaderRow.addView(mPL_Header);
            mRightHeaderTable.addView(rightHeaderRow);

            if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {
                RestClient.getRestClient(MemberVpaNumberUpdation.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.WALLETVPA_LIST+shgDto.getShgId(), getActivity(), ServiceType.MEMBER_VPANUMBER);
            } else {
                Utils.showToast(getActivity(), "Network Not Available");
            }




        } catch (Exception e) {
            e.printStackTrace();
        }

        return rootview;

    }

    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {

        switch (serviceType) {

            case MEMBER_VPANUMBER:
                try {

                    Log.d("getDetails", " " + result.toString());
                    responseDto = new Gson().fromJson(result.toString(), ResponseDto.class);



                    if (responseDto.getStatusCode() == Utils.Success_Code) {
                        Utils.showToast(getActivity(), responseDto.getMessage());



                        for (int i = 0; i < responseDto.getResponseContent().getVpaList().size(); i++) {


                             id = responseDto.getResponseContent().getVpaList().get(i).getId();
                             name = responseDto.getResponseContent().getVpaList().get(i).getName();
                             vpano=responseDto.getResponseContent().getVpaList().get(i).getVpaNo();
                             vpaverify = responseDto.getResponseContent().getVpaList().get(i).getVpaVerified();
                             sbaccount=responseDto.getResponseContent().getVpaList().get(i).getSbaccountNo();

                            vpaname.add(name);
                            vpanumber.add(vpano);
                            vpaverify_list.add(vpaverify);
                            vpaverify_sbaccount.add(sbaccount);

                        }


                        for (int i = 0; i < vpaname.size(); i++) {

                            TableRow leftContentRow = new TableRow(getActivity());

                            TableRow.LayoutParams leftContentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                    60, 1f);
                            leftContentParams.setMargins(5, 5, 5, 5);

                            final TextView memberName_Text = new TextView(getActivity());
                            memberName_Text.setText(com.oasys.eshakti.digitization.OasysUtils.GetSpanText.getSpanString(getActivity(),
                                    String.valueOf(vpaname.get(i))));
                            memberName_Text.setTypeface(LoginActivity.sTypeface);
                            memberName_Text.setTextColor(R.color.black);
                            memberName_Text.setPadding(5, 5, 5, 5);
                            memberName_Text.setLayoutParams(leftContentParams);
                            memberName_Text.setWidth(300);
                            memberName_Text.setSingleLine(true);
                            memberName_Text.setEllipsize(TextUtils.TruncateAt.END);
                            leftContentRow.addView(memberName_Text);
                            mLeftContentTable.addView(leftContentRow);

                        }

                        setLeftTableData();



                    } else {
                        Utils.showToast(getActivity(), "Null Value");
                    }
                    break;
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

            case VPAVERIFICATION:
                try {

                    Log.d("getDetails", " " + result.toString());
                    responseDto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    TextView tv = (TextView)mRightContentTable.findViewById(selId);
                    if (responseDto.getStatusCode() == Utils.Success_Code) {
                        Utils.showToast(getActivity(), responseDto.getMessage());

//                        verifystatus = responseDto.getResponseContent().getStatus();

                        tv.setTextSize(16);
//                        if(verifystatus.equalsIgnoreCase("VE"))
//                        {
                            Log.e("selId......",""+selId);

                            tv.setText("TRUE");
                        }
                        else
                        {
                           tv.setText("FALSE");
                        }




                }catch (Exception e)
                {
                    e.printStackTrace();
                }
        }
    }

    private void setLeftTableData() {
        for (j = 0; j < vpaverify_sbaccount.size(); j++) {
            TableRow rightContentRow = new TableRow(getActivity());
//            TableRow.LayoutParams rightContentTextviewParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
//                    ViewGroup.LayoutParams.MATCH_PARENT, 1f);
//            rightContentTextviewParams.setMargins(20, 10, 20, 8);
//            final TextView mAccount_number = new TextView(getActivity());
//            mAccount_number.setText(com.oasys.eshakti.digitization.OasysUtils.GetSpanText.getSpanString(getActivity(), String.valueOf(vpaverify_sbaccount.get(j))));
//            mAccount_number.setTypeface(LoginActivity.sTypeface);
//            mAccount_number.setTextColor(R.color.black);
//            mAccount_number.setPadding(5, 5, 5, 5);
//            mAccount_number.setLayoutParams(rightContentTextviewParams);
//            mAccount_number.setWidth(340);
//            mAccount_number.setSingleLine(true);
//            mAccount_number.setEllipsize(TextUtils.TruncateAt.END);
//            rightContentRow.addView(mAccount_number);

            TableRow.LayoutParams rightContentTextviewParams1 = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    60, 1f);
            rightContentTextviewParams1.setMargins(5, 5, 5, 5);

            final TextView mVpaNumber = new TextView(getActivity());
            mVpaNumber.setText(com.oasys.eshakti.digitization.OasysUtils.GetSpanText.getSpanString(getActivity(), String.valueOf(vpanumber.get(j))));
            mVpaNumber.setTypeface(LoginActivity.sTypeface);
            mVpaNumber.setTextColor(R.color.black);
            mVpaNumber.setPadding(5, 5, 5, 5);
            mVpaNumber.setLayoutParams(rightContentTextviewParams1);
            mVpaNumber.setWidth(280);
            mVpaNumber.setBackgroundResource(R.drawable.layoutbackground);
            mVpaNumber.setSingleLine(true);
            mVpaNumber.setEllipsize(TextUtils.TruncateAt.END);
            mVpaNumber.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    builder = new AlertDialog.Builder(getActivity());
                    layoutinflater = getLayoutInflater();
                    View Dview = layoutinflater.inflate(R.layout.vpaaccount_no_prompt,null);
                    builder.setCancelable(false);
                    builder.setView(Dview);
                    vpa_input_value = (EditText)Dview.findViewById(R.id.vpa_input_value);
                    vpa_ok = (Button)Dview.findViewById(R.id.vpa_ok);
                    vpa_cancel = (Button)Dview.findViewById(R.id.vpa_cancel);
                    alertdialog = builder.create();
                    vpa_ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if(vpa_input_value.getText().toString().equalsIgnoreCase(""))
                            { }
                            else {
                                alertdialog.cancel();
                                mVpaNumber.setTextColor(R.color.black);
                                mVpaNumber.setText(vpa_input_value.getText().toString());
                            }
                        }
                    });

                    vpa_cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertdialog.cancel();
                        }
                    });
                    alertdialog.show();

                }
            });
            rightContentRow.addView(mVpaNumber);

            TableRow.LayoutParams rightContentTextviewParams2 = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    60, 1f);
            rightContentTextviewParams2.setMargins(10, 5, 5, 5);
            mVpaverify = new TextView(getActivity());

            if(vpaverify_list.get(j).equals("false")) {

                mVpaverify.setText(com.oasys.eshakti.digitization.OasysUtils.GetSpanText.getSpanString(getActivity(),"FALSE"));
            }
            mVpaverify.setTypeface(LoginActivity.sTypeface);
            mVpaverify.setTextColor(R.color.black);
            mVpaverify.setPadding(5, 5, 5, 5);
            mVpaverify.setLayoutParams(rightContentTextviewParams1);
            mVpaverify.setWidth(160);
            mVpaverify.setSingleLine(true);
            mVpaverify.setEllipsize(TextUtils.TruncateAt.END);
            mVpaverify.setId(j);
            rightContentRow.addView(mVpaverify);

            TableRow.LayoutParams rightContentTextviewParams3 = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                   60, 1f);
            rightContentTextviewParams3.setMargins(5, 5, 5, 5);
            final TextView verify = new TextView(getActivity());
            verify.setText("Verify");
            verify.setTypeface(LoginActivity.sTypeface);
            verify.setTextColor(Color.parseColor("#FFFFFF"));
            verify.setPadding(5, 5, 5, 5);
            verify.setLayoutParams(rightContentTextviewParams1);
            verify.setWidth(200);
            verify.setSingleLine(true);
            verify.setGravity(Gravity.CENTER);
            verify.setBackgroundResource(R.color.colorPrimary);
            verify.setEllipsize(TextUtils.TruncateAt.END);
            verify.setId(j);
            verify.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selId = verify.getId();
                    Log.e("click...",""+mVpaverify.getId()+" / "+verify.getId());
                    if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {

                        RestClient.getRestClient(MemberVpaNumberUpdation.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.VERIFYVPA_LIST + mVpaNumber.getText().toString(), getActivity(), ServiceType.VPAVERIFICATION);
                    } else {
                        Utils.showToast(getActivity(), "Network Not Available");
                    }
                }
            });
            rightContentRow.addView(verify);
            mRightContentTable.addView(rightContentRow);
        }
    }
}
