package com.oasys.eshakti.digitization.model;

import lombok.Data;

@Data
public class ListItem {

    private String id;
    private String title;
    private String walletValue;
    private int imageId;
    private boolean isSelected;

    private String name, amt, txId, sts;

    String transactionDate, transactionId, referenceId, transType, updatedWalletAmount, previousWalletAmount;
    String amount,amountProcessed, status,merchantId;
    String transactionMode,transRemarks;
    String transactionDescription;
    String transactionType;
    String animatorName,merchantNameId;
    String transactionStatus;
    String cashOutAmount;
    String openingAmount;
    String cashInAmount;
    String outStandingAmount;




}
