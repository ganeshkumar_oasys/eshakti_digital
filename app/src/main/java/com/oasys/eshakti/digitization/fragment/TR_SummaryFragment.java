package com.oasys.eshakti.digitization.fragment;

import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.oasys.eshakti.digitization.Adapter.TR_SUmmary_Adapter;
import com.oasys.eshakti.digitization.Dto.HistoryDetailsDto;
import com.oasys.eshakti.digitization.Dto.HistoryDto;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;
import com.oasys.eshakti.digitization.model.ListItem;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class TR_SummaryFragment extends Fragment implements View.OnClickListener, NewTaskListener {

    private View view;
    private String fromdateStr, todateStr, type, animator_id, user_id;
    private NetworkConnection networkConnection;
    private Dialog mProgressDilaog;
    private ArrayList<ListItem> listItems;
    private ListView list_details;
    private TR_SUmmary_Adapter mAdapter;
    private RecyclerView recyclerview_digitization_memberdetails;
    private LinearLayoutManager linearLayoutManager;
    private String currentDateStr;
    private TextView daily_date_time;

    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    private TextView fragmentHeader;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_digitization__member__saving__details, container, false);
        try {
            Bundle bundle = getArguments();
            fromdateStr = bundle.getString("fromdate");
            todateStr = bundle.getString("todate");
            type = bundle.getString("type");
            animator_id = bundle.getString("animatorid");
            listItems = new ArrayList<ListItem>();

            Calendar calender = Calendar.getInstance();
            currentDateStr = df.format(calender.getTime());
            daily_date_time = (TextView) view.findViewById(R.id.daily_date_time);
            daily_date_time.setText(currentDateStr);
            fragmentHeader = (TextView) view.findViewById(R.id.fragmentHeader);
            fragmentHeader.setVisibility(View.VISIBLE);
            daily_date_time.setVisibility(View.VISIBLE);
            fragmentHeader.setText(AppStrings.mO_t_history);


            networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());


            recyclerview_digitization_memberdetails = (RecyclerView) view.findViewById(R.id.recyclerView_memberSaving_details);
            linearLayoutManager = new LinearLayoutManager(getActivity());
            recyclerview_digitization_memberdetails.setLayoutManager(linearLayoutManager);
            recyclerview_digitization_memberdetails.setHasFixedSize(true);
            recyclerview_digitization_memberdetails.addItemDecoration(new DividerItemDecoration(getActivity(), 0));




            if (type.equals(NewDrawerScreen.O_TR_REP)) {

                //TODO:: WALLET BALANCE
                HistoryDetailsDto hdto = new HistoryDetailsDto();
                hdto.setFromDate(fromdateStr);
                hdto.setUserId(MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, ""));
                hdto.setAgentId(MySharedPreference.readString(getActivity(), MySharedPreference.AGENT_ID, ""));


                // hdto.setMerchantId(MySharedPreference.readString(getActivity(), MySharedPreference.MERCHANT_ID, ""));
       /* hdto.setMerchantId(MySharedPreference.readString(getActivity(),MySharedPreference.MERCHANT_ID,""));
        hdto.setWalletId(MySharedPreference.readString(getActivity(),MySharedPreference.WALLET_ID,""));*/
                hdto.setToDate(todateStr);
                String sreqString = new Gson().toJson(hdto);
                onTaskStarted();
                RestClient.getRestClient(TR_SummaryFragment.this).callRestWebService(Constants.BASE_URL + Constants.WALLET_TRS_HIS, sreqString, getActivity(), ServiceType.WALLET_TRS_HIS);
            }else{
                Utils.showToast(getActivity(), "Network Not Available");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
            mProgressDilaog.dismiss();
            mProgressDilaog = null;
        }
        switch (serviceType) {
            case WALLET_TRS_HIS:

                if (result != null && result.length() > 0) {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    HistoryDto lrDto = gson.fromJson(result, HistoryDto.class);
                    int statusCode = lrDto.getStatusCode();
                    String message = lrDto.getMessage();
                    Log.d("response status", " " + statusCode);
                    if (statusCode == 400 || statusCode == 401 || statusCode == 403 || statusCode == 500 || statusCode == 503) {
                        // showMessage(statusCode);
                        Utils.showToast(getActivity(), message);
                        if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        }

                        if (statusCode == 503) {

                            Utils.showToast(getActivity(), AppStrings.service_unavailable);

                        }

                        // init();

                    } else if (statusCode == Utils.Success_Code) {
                        Utils.showToast(getActivity(), message);

                        if (listItems != null && listItems.size() > 0) {
                            listItems.clear();
                        }

                        if (lrDto.getResponseContents() != null && lrDto.getResponseContents().size() > 0) {

                            list_details = (ListView) view.findViewById(R.id.list_details);


                            for (int i = 0; i < lrDto.getResponseContents().size(); i++) {
                                ListItem rowItem = new ListItem();
                                rowItem.setMerchantId(lrDto.getResponseContents().get(i).getMerchantNameId());
                                rowItem.setTransactionDate(lrDto.getResponseContents().get(i).getTransactionDate());
                                rowItem.setAmount(lrDto.getResponseContents().get(i).getAmount());
                                rowItem.setTransactionId(lrDto.getResponseContents().get(i).getTransactionId());
                                rowItem.setReferenceId(lrDto.getResponseContents().get(i).getReferenceId());
                                rowItem.setTransactionStatus(lrDto.getResponseContents().get(i).getTransactionStatus());
                                rowItem.setTransactionType(lrDto.getResponseContents().get(i).getTransactionType());
                                rowItem.setAnimatorName(lrDto.getResponseContents().get(i).getAnimatorName());
                                listItems.add(rowItem);
                            }
                            mAdapter = new TR_SUmmary_Adapter(getActivity(), listItems);
                            recyclerview_digitization_memberdetails.setAdapter(mAdapter);
                            //init();

                        } else {
                            Utils.showToast(getActivity(), message);
                        }
                    }
                }

                break;
        }
    }

}
