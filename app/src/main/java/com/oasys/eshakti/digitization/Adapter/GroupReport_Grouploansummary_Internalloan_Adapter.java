package com.oasys.eshakti.digitization.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasys.eshakti.digitization.Dto.GroupInternalLoansList;
import com.oasys.eshakti.digitization.R;

import java.util.ArrayList;

public class GroupReport_Grouploansummary_Internalloan_Adapter extends RecyclerView.Adapter<GroupReport_Grouploansummary_Internalloan_Adapter.MyViewholder> {

    private Context context;
    private ArrayList<GroupInternalLoansList> groupInternalLoansLists;

    public GroupReport_Grouploansummary_Internalloan_Adapter(Context context, ArrayList<GroupInternalLoansList> groupInternalLoansLists) {
        this.context = context;
        this.groupInternalLoansLists = groupInternalLoansLists;
    }

    @NonNull
    @Override
    public MyViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.groupsavingviews, parent, false);
        return new MyViewholder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewholder holder, int position) {

        holder.groupsavings_name.setText(groupInternalLoansLists.get(position).getGroupMemberName());
        holder.groupsavings_amount.setText(groupInternalLoansLists.get(position).getAmount());
        holder.groupsavings_voluntarysaving.setText(groupInternalLoansLists.get(position).getLoanAmountRepaid());

    }

    @Override
    public int getItemCount() {
        return groupInternalLoansLists.size();
    }


    class MyViewholder extends RecyclerView.ViewHolder {
        TextView groupsavings_name, groupsavings_amount, groupsavings_voluntarysaving;


        public MyViewholder(View itemView) {
            super(itemView);


            groupsavings_name = (TextView) itemView.findViewById(R.id.mGroupreportName);
            groupsavings_amount = (TextView) itemView.findViewById(R.id.mGroupreportsavings);
            groupsavings_voluntarysaving = (TextView) itemView.findViewById(R.id.mGroupreporVoluntary);

        }
    }
}
