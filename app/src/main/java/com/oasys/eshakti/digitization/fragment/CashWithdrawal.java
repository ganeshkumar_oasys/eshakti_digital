package com.oasys.eshakti.digitization.fragment;

import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.digitization.Adapter.CustomListAdapter;
import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.Dto.MemberList;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.Dto.ShgBankDetails;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.activity.LoginActivity;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;
import com.oasys.eshakti.digitization.database.BankTable;
import com.oasys.eshakti.digitization.database.MemberTable;
import com.oasys.eshakti.digitization.database.SHGTable;
import com.oasys.eshakti.digitization.model.ListItem;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CashWithdrawal extends Fragment implements AdapterView.OnItemClickListener,NewTaskListener {

    private View rootView;
    private int mSize;

    private ArrayList<MemberList> arrMem;
    private ListOfShg shgDto;
    private NetworkConnection networkConnection;
    private List<MemberList> memList;
    private ArrayList<ShgBankDetails> bankdetails;
    private TextView mGroupName;
    private TextView mCashInHand;
    private TextView mCashAtBank,mHeader;
    private int size;
    private ArrayList<ListItem> listItems;
    private ListView mListView;
    private CustomListAdapter mAdapter;
    private TextView Wallet_balance,Wallet_CIH;
    private Dialog mProgressDilaog;
    public  String userId;
    private RestClient restClient;
    private  int mWalletamount;
    private  int mAniiamtorCH;
    private TextView daily_date_time;
    private String currentDateStr;
    DateFormat df = new SimpleDateFormat("dd/MM/yyyy");


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_new_banktransaction_menulist, container, false);
        return rootView;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mSize = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, "")).size();
        memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        bankdetails = BankTable.getBankName(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        userId = MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, "");

        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        arrMem = new ArrayList<>();

        if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {
            onTaskStarted();
            restClient =RestClient.getRestClient(CashWithdrawal.this);
            restClient.callWebServiceForGetMethod(Constants.BASE_URL + Constants.WALLETCASH_INHAND + userId, getActivity(), ServiceType.WALLET_CASHIN_HAND);

        } else {
            Utils.showToast(getActivity(), "Network Not Available");
        }

        init();

    }

    private void init() {
        try {
            Calendar calender = Calendar.getInstance();
            currentDateStr = df.format(calender.getTime());

            daily_date_time = (TextView) rootView.findViewById(R.id.daily_date_time);
            daily_date_time.setText(currentDateStr);

            mGroupName = (TextView) rootView.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
            mCashInHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashInHand.setTypeface(LoginActivity.sTypeface);

            mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
            mCashAtBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashAtBank.setTypeface(LoginActivity.sTypeface);

            mHeader = (TextView) rootView.findViewById(R.id.fragmentHeader);
            mHeader.setText(AppStrings.cash_withdrawal);
            mHeader.setTypeface(LoginActivity.sTypeface);

            Wallet_balance = (TextView) rootView.findViewById(R.id.Wallet_balance);
//            Wallet_balance.setText(AppStrings.walletbalance + shgDto.getCashInHand());


            Wallet_CIH = (TextView) rootView.findViewById(R.id.Wallet_CIH);


            String[] bankNames = new String[bankdetails.size()];
            size = bankNames.length;

            listItems = new ArrayList<ListItem>();
            mListView = (ListView) rootView.findViewById(R.id.fragment_List);
            //   listImage = R.drawable.ic_navigate_next_white_24dp;
            /* for checking */


            for (int i = 0; i < mSize; i++) {
                if (!memList.get(i).getMemberId().equals("")
                        && !memList.get(i).getMemberName().equals("")) {

                    ListItem rowItem = new ListItem();
                    rowItem.setId(memList.get(i).getMemberId());
                    rowItem.setTitle(memList.get(i).getMemberName());
                    listItems.add(rowItem);
                }

            }

            mAdapter = new CustomListAdapter(getActivity(), listItems);
            mListView.setAdapter(mAdapter);
            mListView.setOnItemClickListener(this);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        MemberList member = memList.get(i);
        MemberCashWithdrawal mc = new MemberCashWithdrawal();
        Bundle d = new Bundle();
        d.putSerializable("member", member);
        mc.setArguments(d);
        NewDrawerScreen.showFragment(mc);


    }

    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {

        if (mProgressDilaog != null) {
            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                mProgressDilaog.dismiss();
                mProgressDilaog = null;
//                if (confirmationDialog.isShowing())
//                    confirmationDialog.dismiss();
            }


            switch (serviceType) {

                case WALL_TXANIMATOR:
                    Log.d("ddd","txt");

                    try {
                        ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                        String message = cdto.getMessage();
                        int statusCode = cdto.getStatusCode();
                        if (statusCode == Utils.Success_Code) {
                            Utils.showToast(getActivity(), message);
                            String walletamount = cdto.getResponseContent().getWalletAmount();
                            mWalletamount= (int)Double.parseDouble(walletamount);
                            Wallet_balance.setText(AppStrings.w_b+ String.valueOf(mWalletamount));
                            Wallet_balance.setTypeface(LoginActivity.sTypeface);
                        } else {
                            Utils.showToast(getActivity(), message);

                            if (statusCode == 401) {

                                Log.e("Group Logout", "Logout Sucessfully");
                                AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                                if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                    mProgressDilaog.dismiss();
                                    mProgressDilaog = null;
                                }
                            }

                            if (statusCode == 503) {

                                Utils.showToast(getActivity(), AppStrings.service_unavailable);

                            }

                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;

                case WALLET_CASHIN_HAND:
                    try {
                        ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                        String message = cdto.getMessage();
                        int statusCode = cdto.getStatusCode();
                        if (statusCode == Utils.Success_Code) {
                            Utils.showToast(getActivity(), message);

                            String animatorCashinHand = cdto.getResponseContent().getCurrentCashInHand();
                            mAniiamtorCH = (int)Double.parseDouble(animatorCashinHand);
                            Wallet_CIH.setText(AppStrings.w_cih+ String.valueOf(mAniiamtorCH));
                            Wallet_CIH.setTypeface(LoginActivity.sTypeface);
                        } else {
                            Utils.showToast(getActivity(), message);

                            if (statusCode == 401) {

                                Log.e("Group Logout", "Logout Sucessfully");
                                AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                                if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                    mProgressDilaog.dismiss();
                                    mProgressDilaog = null;
                                }
                            }

                            if (statusCode == 503) {

                                Utils.showToast(getActivity(), AppStrings.service_unavailable);

                            }

                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    onTaskStarted();
                    restClient.callWebServiceForGetMethod(Constants.BASE_URL + Constants.WALL_BAL + userId, getActivity(), ServiceType.WALL_TXANIMATOR);
                    break;
            }
        }

    }
}
