package com.oasys.eshakti.digitization.Dto;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.Data;

@Data
public class GroupLoanSummary implements Serializable {

    private String loanOutstandingAmount;

    private String loanSanctionAmount;

    private ArrayList <GroupLoanSummaryDTOList> groupLoanSummaryDTOList;
}
