package com.oasys.eshakti.digitization.Dto;


import java.io.Serializable;

import lombok.Data;

@Data
public class MemberList implements Serializable {
    private String shgId;
    private String memberId;
    private String memberName;
    private String phoneNumber;
    private String bankName;
    private String branchName;
    private String bankId;
    private String accountNumber;
    private String bankNameId;
    private String branchNameId;
    private String memberUserId;
    private boolean isCash;

    private boolean cashOrCashless;

    private String savingsAmount;
    private String voluntarySavingsAmount;

    private String loanOutstanding;

    private String amount;
    private String loanAmount;


}
