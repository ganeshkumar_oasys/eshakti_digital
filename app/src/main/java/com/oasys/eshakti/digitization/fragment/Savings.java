package com.oasys.eshakti.digitization.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AlertDialog;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.Dto.MemberList;
import com.oasys.eshakti.digitization.Dto.ResponseContents;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.Dto.SavingRequest;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.GetSpanText;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.Service.Restclient_Timeout;
import com.oasys.eshakti.digitization.activity.LoginActivity;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;
import com.oasys.eshakti.digitization.database.MemberTable;
import com.oasys.eshakti.digitization.database.SHGTable;
import com.oasys.eshakti.digitization.views.CustomHorizontalScrollView;
import com.oasys.eshakti.digitization.views.Get_EdiText_Filter;
import com.oasys.eshakti.digitization.views.TextviewUtils;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Savings extends Fragment implements View.OnClickListener, NewTaskListener {

    private View rootView;
    private TextView mGroupName;
    private TextView mCashinHand;
    private TextView mCashatBank, Wallet_balance;
    private TextView mHeader, mAutoFilllabel, mMemberName;
    private LinearLayout mMemberNameLayout;
    private TableLayout mLeftHeaderTable;
    private int mSize;
    private TableLayout mRightHeaderTable;
    private TableLayout mLeftContentTable;
    private TableLayout mRightContentTable;
    private CustomHorizontalScrollView mHSRightHeader;
    private CustomHorizontalScrollView mHSRightContent;
    private Button mSubmit_Raised_Button;
    private Button mPerviousButton;
    private Button mNextButton;
    String width[] = {AppStrings.memberName, AppStrings.savingsAmount, AppStrings.voluntarySavings, AppStrings.cashmode};
    int[] rightHeaderWidth = new int[width.length];
    int[] rightContentWidth = new int[width.length];
    private EditText mSavings_values;
    private EditText mVSavings_values;
    private List<EditText> sSavingsFields;
    private List<EditText> sVSavingsFields;
    private static List<CheckBox> mCheckBoxFields;
    public String[] sSavingsAmounts;
    public String[] sVSavingsAmount;
    private Button mEdit_RaisedButton;
    private Button mOk_RaisedButton;
    private List<MemberList> memList;
    private int sSavings = 0;
    private int vSavings = 0;
    private ArrayList<MemberList> arrMem;
    private ListOfShg shgDto;
    private Dialog mProgressDilaog;
    private NetworkConnection networkConnection;
    private CheckBox mAutoFill;
    private String flag = "0";
    public static int sum_of_savings = 0;
    private CheckBox sCheckBox1;
    public String userId;
    public String agentId;
    public ArrayList<ResponseContents> responseContentsList;
    private TextView mAnimatorCash_in_hand;
    private int mAniiamtorCH;
    private int totalcheck;
    private RestClient restClient;
    private int mWalletamount;
    private TextView daily_date_time;
    private String currentDateStr;
    DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    private AlertDialog alertDialog;
    private TextView counterTextView;
    private TextView counterTimerStatusTxt;
    private CountDownTimer cndr;
    private static final String FORMAT_TIMER = "%02d:%02d";
    public static long timeout_saving;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.saving_fragment, container, false);
        userId = MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, "");
        agentId = MySharedPreference.readString(getActivity(), MySharedPreference.AGENT_ID, "");


        Bundle bundle = getArguments();
        if (bundle != null) {
            Attendance.flag = bundle.getString("stepwise");
            Log.d("LOANID", Attendance.flag);
        }

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mSize = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, "")).size();
        memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        arrMem = new ArrayList<>();

        if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {
            onTaskStarted();
            restClient = RestClient.getRestClient(Savings.this);
            restClient.callWebServiceForGetMethod(Constants.BASE_URL + Constants.WALLETCASH_INHAND + userId, getActivity(), ServiceType.WALLET_CASHIN_HAND);

        } else {
            Utils.showToast(getActivity(), "Network Not Available");
        }

        init();
    }

    private void init() {
        sSavingsFields = new ArrayList<EditText>();
        sVSavingsFields = new ArrayList<EditText>();
        mCheckBoxFields = new ArrayList<CheckBox>();
        responseContentsList = new ArrayList<>();
        try {
            Calendar calender = Calendar.getInstance();
            currentDateStr = df.format(calender.getTime());

            daily_date_time = (TextView) rootView.findViewById(R.id.daily_date_time);
            daily_date_time.setText(currentDateStr);
            mGroupName = (TextView) rootView.findViewById(R.id.groupname);


            mGroupName = (TextView) rootView.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
            mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashinHand.setTypeface(LoginActivity.sTypeface);

            mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
            mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashatBank.setTypeface(LoginActivity.sTypeface);
            /** UI Mapping **/

            Wallet_balance = (TextView) rootView.findViewById(R.id.Wallet_balance);
//            Wallet_balance.setText(AppStrings.walletbalance + shgDto.getCashInHand());


            mAnimatorCash_in_hand = (TextView) rootView.findViewById(R.id.Wallet_CIH);


            mHeader = (TextView) rootView.findViewById(R.id.fragmentHeader);
            mHeader.setText(AppStrings.savings);
            mHeader.setTypeface(LoginActivity.sTypeface);

            mAutoFilllabel = (TextView) rootView.findViewById(R.id.autofillLabel);
            mAutoFilllabel.setText(AppStrings.autoFill);
            mAutoFilllabel.setTypeface(LoginActivity.sTypeface);
            mAutoFilllabel.setVisibility(View.VISIBLE);

            mAutoFill = (CheckBox) rootView.findViewById(R.id.autoFill);
            mAutoFill.setVisibility(View.VISIBLE);
            mAutoFill.setOnClickListener(this);

            mMemberNameLayout = (LinearLayout) rootView.findViewById(R.id.member_name_layout);
            mMemberName = (TextView) rootView.findViewById(R.id.member_name);
            // mMemberName.setTypeface(LoginActivity.sTypeface);

            Log.d("Savings", String.valueOf(mSize));

            mLeftHeaderTable = (TableLayout) rootView.findViewById(R.id.LeftHeaderTable);
            mRightHeaderTable = (TableLayout) rootView.findViewById(R.id.RightHeaderTable);
            mLeftContentTable = (TableLayout) rootView.findViewById(R.id.LeftContentTable);
            mRightContentTable = (TableLayout) rootView.findViewById(R.id.RightContentTable);

            mHSRightHeader = (CustomHorizontalScrollView) rootView.findViewById(R.id.rightHeaderHScrollView);
            mHSRightContent = (CustomHorizontalScrollView) rootView.findViewById(R.id.rightContentHScrollView);


            mHSRightHeader.setOnScrollChangedListener(new CustomHorizontalScrollView.onScrollChangedListener() {

                public void onScrollChanged(int l, int t, int oldl, int oldt) {
                    // TODO Auto-generated method stub

                    mHSRightContent.scrollTo(l, 0);

                }
            });

            mHSRightContent.setOnScrollChangedListener(new CustomHorizontalScrollView.onScrollChangedListener() {
                @Override
                public void onScrollChanged(int l, int t, int oldl, int oldt) {
                    // TODO Auto-generated method stub
                    mHSRightHeader.scrollTo(l, 0);
                }
            });
            TableRow leftHeaderRow = new TableRow(getActivity());

            TableRow.LayoutParams lHeaderParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);

            TextView mMemberName_headerText = new TextView(getActivity());
            mMemberName_headerText
                    .setText("" + String.valueOf(AppStrings.memberName));
            mMemberName_headerText.setTypeface(LoginActivity.sTypeface);
            mMemberName_headerText.setTextColor(Color.WHITE);
//            mMemberName_headerText.setMaxLines(2);
            mMemberName_headerText.setPadding(10, 5, 10, 5);
            mMemberName_headerText.setLayoutParams(lHeaderParams);
            leftHeaderRow.addView(mMemberName_headerText);
            mLeftHeaderTable.addView(leftHeaderRow);

            TableRow rightHeaderRow = new TableRow(getActivity());
            TableRow.LayoutParams rHeaderParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            rHeaderParams.setMargins(10, 0, 20, 0);
            TextView mSavingsAmount_HeaderText = new TextView(getActivity());
            mSavingsAmount_HeaderText
                    .setText("" + String.valueOf(AppStrings.savingsAmount));
//            mSavingsAmount_HeaderText.setMaxLines(2);
            mSavingsAmount_HeaderText.setTypeface(LoginActivity.sTypeface);
            mSavingsAmount_HeaderText.setTextColor(Color.WHITE);
            mSavingsAmount_HeaderText.setPadding(5, 5, 5, 5);
            mSavingsAmount_HeaderText.setLayoutParams(rHeaderParams);
            mSavingsAmount_HeaderText.setGravity(Gravity.CENTER);
            mSavingsAmount_HeaderText.setSingleLine(false);
            rightHeaderRow.addView(mSavingsAmount_HeaderText);

            TextView mVSavingsAmount_HeaderText = new TextView(getActivity());
            mVSavingsAmount_HeaderText
                    .setText("" + String.valueOf(AppStrings.voluntarySavings));
//            mVSavingsAmount_HeaderText.setMaxLines(2);
            mVSavingsAmount_HeaderText.setTypeface(LoginActivity.sTypeface);
            mVSavingsAmount_HeaderText.setTextColor(Color.WHITE);
            mVSavingsAmount_HeaderText.setPadding(5, 5, 5, 5);
            mVSavingsAmount_HeaderText.setLayoutParams(rHeaderParams);
            mVSavingsAmount_HeaderText.setSingleLine(false);
            rightHeaderRow.addView(mVSavingsAmount_HeaderText);


            TextView cashMode_Text = new TextView(getActivity());
            cashMode_Text
                    .setText("" + String.valueOf(AppStrings.cashmode));
//            cashMode_Text.setMaxLines(2);
            cashMode_Text.setTypeface(LoginActivity.sTypeface);
            cashMode_Text.setTextColor(Color.WHITE);
            cashMode_Text.setLayoutParams(rHeaderParams);
            cashMode_Text.setPadding(5, 5, 5, 5);
            cashMode_Text.setSingleLine(false);
            rightHeaderRow.addView(cashMode_Text);

            mRightHeaderTable.addView(rightHeaderRow);

            getTableRowHeaderCellWidth();

            for (int i = 0; i < mSize; i++) {
                TableRow leftContentRow = new TableRow(getActivity());

                TableRow.LayoutParams leftContentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 60, 1f);
                leftContentParams.setMargins(5, 5, 5, 5);
                final TextView memberName_Text = new TextView(getActivity());
                memberName_Text.setText(memList.get(i).getMemberName());
                memberName_Text.setTextColor(R.color.black);
                memberName_Text.setPadding(5, 5, 5, 5);
                memberName_Text.setLayoutParams(leftContentParams);
                memberName_Text.setWidth(200);
                memberName_Text.setSingleLine(true);
                memberName_Text.setEllipsize(TextUtils.TruncateAt.END);
                leftContentRow.addView(memberName_Text);

                mLeftContentTable.addView(leftContentRow);

                TableRow rightContentRow = new TableRow(getActivity());


                TableRow.LayoutParams rightContentParams = new TableRow.LayoutParams(rightHeaderWidth[1],
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                rightContentParams.setMargins(20, 5, 20, 5);

                mSavings_values = new EditText(getActivity());
                mSavings_values.setId(i);
//                mSavings_values.setWidth(100);
                sSavingsFields.add(mSavings_values);
                mSavings_values.setPadding(5, 5, 5, 5);
                mSavings_values.setBackgroundResource(R.drawable.edittext_background);
                mSavings_values.setLayoutParams(rightContentParams);
                mSavings_values.setTextAppearance(getActivity(), R.style.MyMaterialTheme);
                mSavings_values.setFilters(Get_EdiText_Filter.editText_filter());
                mSavings_values.setInputType(InputType.TYPE_CLASS_NUMBER);
                mSavings_values.setTextColor(R.color.black);
                // mSavings_values.setWidth(150);
                mSavings_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        // TODO Auto-generated method stub
                        if (hasFocus) {
                            ((EditText) v).setGravity(Gravity.LEFT);

                            mMemberNameLayout.setVisibility(View.VISIBLE);
                            mMemberName.setText(memberName_Text.getText().toString().trim());
                            TextviewUtils.manageBlinkEffect(mMemberName, getActivity());

                        } else {

                            ((EditText) v).setGravity(Gravity.RIGHT);
                            mMemberNameLayout.setVisibility(View.GONE);
                            mMemberName.setText("");
                        }

                    }
                });
                rightContentRow.addView(mSavings_values);

                mVSavings_values = new EditText(getActivity());
                mVSavings_values.setId(i);
                sVSavingsFields.add(mVSavings_values);
                mVSavings_values.setWidth(100);
                mVSavings_values.setPadding(5, 5, 5, 5);
                mVSavings_values.setBackgroundResource(R.drawable.edittext_background);
                mVSavings_values.setLayoutParams(rightContentParams);
                mVSavings_values.setTextAppearance(getActivity(), R.style.MyMaterialTheme);
                mVSavings_values.setFilters(Get_EdiText_Filter.editText_filter());
                mVSavings_values.setInputType(InputType.TYPE_CLASS_NUMBER);
                mVSavings_values.setTextColor(R.color.black);
                // mVSavings_values.setWidth(150);
                mVSavings_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        // TODO Auto-generated method stub
                        if (hasFocus) {
                            ((EditText) v).setGravity(Gravity.LEFT);

                            mMemberNameLayout.setVisibility(View.VISIBLE);
                            mMemberName.setText(memberName_Text.getText().toString().trim());
                            TextviewUtils.manageBlinkEffect(mMemberName, getActivity());

                        } else {

                            ((EditText) v).setGravity(Gravity.RIGHT);
                            mMemberNameLayout.setVisibility(View.GONE);
                            mMemberName.setText("");
                        }
                    }
                });
                rightContentRow.addView(mVSavings_values);

                TableRow.LayoutParams contentParams3 = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 0f);
                contentParams3.setMargins(30, 5, 10, 5);
                sCheckBox1 = new CheckBox(getActivity());
                sCheckBox1.setId(i);
//                sCheckBox1.setWidth(50);
                mCheckBoxFields.add(sCheckBox1);
                sCheckBox1.setBackgroundResource(R.drawable.btn_check_to_off_mtrl_013);
                sCheckBox1.setClickable(true);
                final int position = i;
                sCheckBox1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if (compoundButton.isChecked()) {
                            mCheckBoxFields.get(position).setChecked(true);
                        } else {
                            mCheckBoxFields.get(position).setChecked(false);
                        }

                    }
                });
                sCheckBox1.setLayoutParams(contentParams3);
                sCheckBox1.setGravity(Gravity.TOP);
                rightContentRow.addView(sCheckBox1);
                mRightContentTable.addView(rightContentRow);

            }

            resizeMemberNameWidth();
            // resizeRightSideTable();

            resizeBodyTableRowHeight();

            mSubmit_Raised_Button = (Button) rootView.findViewById(R.id.fragment_Submit_button);
            mSubmit_Raised_Button.setText(AppStrings.submit);
            mSubmit_Raised_Button.setTypeface(LoginActivity.sTypeface);
            mSubmit_Raised_Button.setOnClickListener(this);

            mPerviousButton = (Button) rootView.findViewById(R.id.fragment_Previousbutton);
            //  mPerviousButton.setText("Savings" + AppStrings.mPervious);
            mPerviousButton.setOnClickListener(this);

            mNextButton = (Button) rootView.findViewById(R.id.fragment_Nextbutton);
            //     mNextButton.setText("Savings" + AppStrings.mNext);
            mNextButton.setOnClickListener(this);


            mPerviousButton.setVisibility(View.INVISIBLE);
            mNextButton.setVisibility(View.INVISIBLE);

        } catch (Exception e) {
            e.printStackTrace();

          /*  TastyToast.makeText(getActivity(), AppStrings.adminAlert, TastyToast.LENGTH_SHORT, TastyToast.WARNING);

            Intent intent = new Intent(getActivity(), ExitActivity.class);
            startActivity(intent);*/
        }
    }


    private void getTableRowHeaderCellWidth() {
        int lefHeaderChildCount = ((TableRow) mLeftHeaderTable.getChildAt(0)).getChildCount();
        int rightHeaderChildCount = ((TableRow) mRightHeaderTable.getChildAt(0)).getChildCount();

        for (int x = 0; x < (lefHeaderChildCount + rightHeaderChildCount); x++) {
            if (x == 0) {
                rightHeaderWidth[x] = viewWidth(((TableRow) mLeftHeaderTable.getChildAt(0)).getChildAt(x));
            } else {
                rightHeaderWidth[x] = viewWidth(((TableRow) mRightHeaderTable.getChildAt(0)).getChildAt(x - 1));
            }
        }
    }

    private void resizeMemberNameWidth() {
        // TODO Auto-generated method stub
        int leftHeadertWidth = viewWidth(mLeftHeaderTable);
        int leftContentWidth = viewWidth(mLeftContentTable);

        if (leftHeadertWidth < leftContentWidth) {
            mLeftHeaderTable.getLayoutParams().width = leftContentWidth;
        } else {
            mLeftContentTable.getLayoutParams().width = leftHeadertWidth;
        }
    }

    private void resizeBodyTableRowHeight() {

        int leftContentTable_ChildCount = mLeftContentTable.getChildCount();

        for (int x = 0; x < leftContentTable_ChildCount; x++) {

            TableRow leftContentTableRow = (TableRow) mLeftContentTable.getChildAt(x);
            TableRow rightContentTableRow = (TableRow) mRightContentTable.getChildAt(x);

            int rowLeftHeight = viewHeight(leftContentTableRow);
            int rowRightHeight = viewHeight(rightContentTableRow);

            TableRow tableRow = rowLeftHeight < rowRightHeight ? leftContentTableRow : rightContentTableRow;
            int finalHeight = rowLeftHeight > rowRightHeight ? rowLeftHeight : rowRightHeight;

            this.matchLayoutHeight(tableRow, finalHeight);
        }

    }

    private void matchLayoutHeight(TableRow tableRow, int height) {

        int tableRowChildCount = tableRow.getChildCount();

        // if a TableRow has only 1 child
        if (tableRow.getChildCount() == 1) {

            View view = tableRow.getChildAt(0);
            TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();
            params.height = height - (params.bottomMargin + params.topMargin);

            return;
        }

        // if a TableRow has more than 1 child
        for (int x = 0; x < tableRowChildCount; x++) {

            View view = tableRow.getChildAt(x);

            TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();

            if (!isTheHeighestLayout(tableRow, x)) {
                params.height = height - (params.bottomMargin + params.topMargin);
                return;
            }
        }

    }

    // check if the view has the highest height in a TableRow
    private boolean isTheHeighestLayout(TableRow tableRow, int layoutPosition) {

        int tableRowChildCount = tableRow.getChildCount();
        int heighestViewPosition = -1;
        int viewHeight = 0;

        for (int x = 0; x < tableRowChildCount; x++) {
            View view = tableRow.getChildAt(x);
            int height = this.viewHeight(view);

            if (viewHeight < height) {
                heighestViewPosition = x;
                viewHeight = height;
            }
        }
        return heighestViewPosition == layoutPosition;
    }

    // read a view's height
    private int viewHeight(View view) {
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        return view.getMeasuredHeight();
    }

    // read a view's width
    private int viewWidth(View view) {
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        return view.getMeasuredWidth();
    }

    public static int sSavings_Total, sVSavings_Total;
    public static String sSendToServer_Savings, sSendToServer_VSavings;
    String nullVlaue = "0";
    String[] confirmArr;
    Dialog confirmationDialog;


    @Override
    public void onClick(View view) {
        sSavingsAmounts = new String[sSavingsFields.size()];
        sVSavingsAmount = new String[sVSavingsFields.size()];

        Log.d("Saving", "sSavingsAmounts size : " + sSavingsFields.size() + "");

        switch (view.getId()) {

            case R.id.fragment_Submit_button:
                try {
                    sSavings_Total = 0;
                    sVSavings_Total = 0;
                    sSendToServer_Savings = "";
                    sSendToServer_VSavings = "";

                    confirmArr = new String[mSize];
                    // Do edit values here

                    if (arrMem != null && arrMem.size() > 0) {
                        arrMem.clear();
                    }

                    int cash_count = 0;
                    for (int i = 0; i < memList.size(); i++) {
                        MemberList ml = new MemberList();
                        ml.setMemberId(memList.get(i).getMemberId());

                        String savings_value = sSavingsFields.get(i).getText().toString();
                        String voluntary_savings_value = sVSavingsFields.get(i).getText().toString();

                        if (savings_value == null || savings_value.isEmpty()) {
                            savings_value = "0";
                        }
                        ml.setSavingsAmount(savings_value);

                        if (voluntary_savings_value == null || voluntary_savings_value.isEmpty()) {
                            voluntary_savings_value = "0";
                        }
                        ml.setVoluntarySavingsAmount(voluntary_savings_value);

                        if (mCheckBoxFields.get(i).isChecked()) {
                            ml.setCashOrCashless(true);
                            ml.setCash(true);
                        } else {
                            ml.setCashOrCashless(false);
                            ml.setCash(false);
                        }
                        sSavingsAmounts[i] = savings_value;
                        sSavings_Total += Integer.parseInt(savings_value);
                        sVSavingsAmount[i] = voluntary_savings_value;
                        sVSavings_Total += Integer.parseInt(voluntary_savings_value);
                        arrMem.add(ml);

                        if (arrMem.get(i).isCash()) {
                            totalcheck = totalcheck + (Integer.parseInt(arrMem.get(i).getVoluntarySavingsAmount()) + Integer.parseInt(arrMem.get(i).getSavingsAmount()));
                            cash_count = cash_count + 1;
                        }
                    }

                    if (arrMem.size() == cash_count) {
                        timeout_saving = MySharedPreference.readLong(getActivity(), MySharedPreference.fttimeout, 180000);

                    } else if (cash_count == 0) {
                        timeout_saving = MySharedPreference.readLong(getActivity(), MySharedPreference.upi_timeout, 180000);

                    } else {
                        timeout_saving = Math.max(MySharedPreference.readLong(getActivity(), MySharedPreference.fttimeout, 180000), MySharedPreference.readLong(getActivity(), MySharedPreference.upi_timeout, 180000));

                    }
                    sum_of_savings = sSavings_Total + sVSavings_Total;
                    Log.d("sum_of_saving", "" + sum_of_savings);


                    if ((sSavings_Total != 0) || (sVSavings_Total != 0)) {


                        if (mWalletamount > sum_of_savings) {

                            confirmationDialog = new Dialog(getActivity());

                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.dialog_new_confirmation, null);

                            ViewGroup.LayoutParams lParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                            dialogView.setLayoutParams(lParams);

                            TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
                            confirmationHeader.setText("" + AppStrings.confirmation);
                            confirmationHeader.setTypeface(LoginActivity.sTypeface);
                            TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

                            for (int i = 0; i < memList.size(); i++) {

                                TableRow indv_SavingsRow = new TableRow(getActivity());

                                TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                                contentParams.setMargins(10, 5, 10, 5);

                                TextView memberName_Text = new TextView(getActivity());
                                memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
                                        memList.get(i).getMemberName()));
                                memberName_Text.setTextColor(R.color.black);
                                memberName_Text.setPadding(5, 5, 5, 5);
                                memberName_Text.setSingleLine(true);
                                memberName_Text.setLayoutParams(contentParams);
                                indv_SavingsRow.addView(memberName_Text);

                                TextView confirm_values = new TextView(getActivity());
                                confirm_values
                                        .setText(GetSpanText.getSpanString(getActivity(), String.valueOf((sSavingsAmounts[i] != null && sSavingsAmounts[i].length() > 0) ? sSavingsAmounts[i] : "0")));
                                confirm_values.setTextColor(R.color.black);
                                confirm_values.setPadding(5, 5, 5, 5);
                                confirm_values.setGravity(Gravity.RIGHT);
                                confirm_values.setLayoutParams(contentParams);
                                indv_SavingsRow.addView(confirm_values);

                                TextView confirm_VSvalues = new TextView(getActivity());
                                confirm_VSvalues
                                        .setText(GetSpanText.getSpanString(getActivity(), String.valueOf((sVSavingsAmount[i] != null && sVSavingsAmount[i].length() > 0) ? sVSavingsAmount[i] : "0")));
                                confirm_VSvalues.setTextColor(R.color.black);
                                confirm_VSvalues.setPadding(5, 5, 5, 5);
                                confirm_VSvalues.setGravity(Gravity.RIGHT);
                                confirm_VSvalues.setLayoutParams(contentParams);
                                indv_SavingsRow.addView(confirm_VSvalues);

                                CheckBox sCheckBox1 = new CheckBox(getActivity());
                                sCheckBox1.setId(i);
                                sCheckBox1.setBackgroundResource(R.drawable.btn_check_to_off_mtrl_013);
                                // sCheckBox1.setPadding(5, 5, 5, 5);
                                sCheckBox1.setClickable(false);
                                sCheckBox1.setWidth(50);
                                sCheckBox1.setLayoutParams(contentParams);
                                if (mCheckBoxFields.get(i).isChecked())
                                    sCheckBox1.setChecked(true);
                                else
                                    sCheckBox1.setChecked(false);

                                sCheckBox1.setGravity(Gravity.LEFT);
                                indv_SavingsRow.addView(sCheckBox1);

                                confirmationTable.addView(indv_SavingsRow,
                                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                            }

                            View rullerView = new View(getActivity());
                            rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
                            rullerView.setBackgroundColor(Color.rgb(0, 199, 140));// rgb(255,
                            // 229,
                            // 242));
                            confirmationTable.addView(rullerView);

                            TableRow totalRow = new TableRow(getActivity());

                            TableRow.LayoutParams totalParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                            totalParams.setMargins(10, 5, 10, 5);

                            TextView totalText = new TextView(getActivity());
                            totalText.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.total)));
                            totalText.setTextColor(R.color.black);
                            totalText.setPadding(5, 5, 5, 5);// (5, 10, 5, 10);
                            totalText.setLayoutParams(totalParams);
                            totalRow.addView(totalText);

                            TextView totalAmount = new TextView(getActivity());
                            totalAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sSavings_Total)));
                            totalAmount.setTextColor(R.color.black);
                            totalAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
                            totalAmount.setGravity(Gravity.RIGHT);
                            totalAmount.setLayoutParams(totalParams);
                            totalRow.addView(totalAmount);

                            TextView totalVSAmount = new TextView(getActivity());
                            totalVSAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sVSavings_Total)));
                            totalVSAmount.setTextColor(R.color.black);
                            totalVSAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
                            totalVSAmount.setGravity(Gravity.RIGHT);
                            totalVSAmount.setLayoutParams(totalParams);
                            totalRow.addView(totalVSAmount);

                            TextView checkAmount = new TextView(getActivity());
                            //  checkAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sVSavings_Total)));
                            checkAmount.setTextColor(R.color.black);
                            checkAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
                            checkAmount.setGravity(Gravity.RIGHT);
                            checkAmount.setVisibility(View.INVISIBLE);
                            checkAmount.setLayoutParams(totalParams);
                            totalRow.addView(checkAmount);

                            confirmationTable.addView(totalRow,
                                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));


                            TableRow totalwalletRow = new TableRow(getActivity());
                            TableRow.LayoutParams totalwalletParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                            totalwalletParams.setMargins(10, 5, 10, 5);
                            TextView totalwalletText = new TextView(getActivity());
                            totalwalletText.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.walletbalanceTxt)));
                            //    totalwalletText.setTextColor(R.color.green_border_color);
                            totalwalletText.setTextColor(Color.rgb(0, 199, 140));
                            totalwalletText.setPadding(5, 5, 5, 5);// (5, 10, 5, 10);
                            totalwalletText.setLayoutParams(totalwalletParams);
                            totalwalletText.setTextSize(20);
                            totalwalletRow.addView(totalwalletText);

                            TextView totalwalletAmount = new TextView(getActivity());
                            totalwalletAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(((Integer.parseInt(String.valueOf(mWalletamount)))))));// + sSavings_Total+sVSavings_Total
                            totalwalletAmount.setTextColor(Color.rgb(0, 199, 140));
                            totalwalletAmount.setPadding(5, 5, 25, 5);// (5, 10, 100, 10);
                            totalwalletAmount.setGravity(Gravity.RIGHT);
                            totalwalletText.setTextSize(20);
                            totalwalletAmount.setLayoutParams(totalwalletParams);
                            totalwalletRow.addView(totalwalletAmount);

                            TextView checkAmount1 = new TextView(getActivity());
                            //  checkAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sVSavings_Total)));
                            checkAmount1.setTextColor(R.color.black);
                            checkAmount1.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
                            checkAmount1.setGravity(Gravity.RIGHT);
                            checkAmount1.setVisibility(View.INVISIBLE);
                            checkAmount1.setLayoutParams(totalParams);
                            totalRow.addView(checkAmount1);

//                        TextView totalVSAmount = new TextView(getActivity());
//                        totalVSAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sVSavings_Total)));
//                        totalVSAmount.setTextColor(R.color.black);
//                        totalVSAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
//                        totalVSAmount.setGravity(Gravity.RIGHT);
//                        totalVSAmount.setLayoutParams(totalParams);
//                        totalRow.addView(totalVSAmount);


                            confirmationTable.addView(totalwalletRow,
                                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                               /* if (Utils.sum_of_savings > (Integer.parseInt(shgDto.getCashInHand()))) {

                                    totalwalletAmount.setTextColor(Color.rgb(128, 0, 0));
                                    TableRow totalwalletButtonRow = new TableRow(getActivity());
                                    TableRow.LayoutParams totalwalletButtonRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                                            ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                                    totalwalletButtonRowParams.setMargins(10, 5, 10, 5);
                                    Button totalwalletButton = new Button(getActivity());
                                    totalwalletButton.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mWallet_Rx_Wallet)));
                                    //    totalwalletText.setTextColor(R.color.green_border_color);
                                    totalwalletButton.setTextColor(R.color.white);
                                    totalwalletButton.setPadding(5, 5, 5, 5);// (5, 10, 5, 10);
                                    totalwalletButton.setLayoutParams(totalwalletButtonRowParams);
                                    totalwalletButton.setBackgroundResource(R.color.green_border_color);
                                    totalwalletButton.setTextSize(20);
                                    totalwalletButtonRow.addView(totalwalletButton);
                                    totalwalletButton.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            if (confirmationDialog.isShowing())
                                                confirmationDialog.dismiss();
                                            FragmentManager fm = getFragmentManager();
                                            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                            MemberDrawerScreen.showFragment(new Selectpayment());


                                        }
                                    });


//                        TextView totalVSAmount = new TextView(getActivity());
//                        totalVSAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sVSavings_Total)));
//                        totalVSAmount.setTextColor(R.color.black);
//                        totalVSAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
//                        totalVSAmount.setGravity(Gravity.RIGHT);
//                        totalVSAmount.setLayoutParams(totalParams);
//                        totalRow.addView(totalVSAmount);


                                    confirmationTable.addView(totalwalletButtonRow,
                                            new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                                }
*/

                            mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit);
                            mEdit_RaisedButton.setText(AppStrings.edit);
                            mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
                            mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
                            // 205,
                            // 0));
                            mEdit_RaisedButton.setOnClickListener(this);

                            mOk_RaisedButton = (Button) dialogView.findViewById(R.id.frag_Ok);
                            mOk_RaisedButton.setText(AppStrings.yes);
                            mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
                            mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
                            mOk_RaisedButton.setOnClickListener(this);

                            confirmationDialog.getWindow()
                                    .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            confirmationDialog.setCanceledOnTouchOutside(false);
                            confirmationDialog.setContentView(dialogView);
                            confirmationDialog.setCancelable(true);
                            confirmationDialog.show();

                            ViewGroup.MarginLayoutParams margin = (ViewGroup.MarginLayoutParams) dialogView.getLayoutParams();
                            margin.leftMargin = 10;
                            margin.rightMargin = 10;
                            margin.topMargin = 10;
                            margin.bottomMargin = 10;
                            margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);

                        } else {

                            TastyToast.makeText(getActivity(), "PLS CHECK THE WALLET BALANCE", TastyToast.LENGTH_SHORT,
                                    TastyToast.WARNING);
                        }

                    } else {

                        TastyToast.makeText(getActivity(), AppStrings.nullAlert, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);

                        sSendToServer_Savings = "";
                        sSendToServer_VSavings = "";
                        sSavings_Total = 0;
                        sVSavings_Total = 0;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.fragment_Edit:
                sSendToServer_Savings = "";
                sSendToServer_VSavings = "";
                sSavings_Total = 0;
                sVSavings_Total = 0;
                mSubmit_Raised_Button.setClickable(true);

                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();

                break;
            case R.id.frag_Ok:

            /*    if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();

                MemberDrawerScreen.showFragment(new Transactionpaymentsuccess());*/

                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();
                SavingRequest sr = new SavingRequest();
                sr.setSavingsAmount(arrMem);
                sr.setShgId(shgDto.getShgId());
                sr.setModeOfCash("2");
                sr.setAgentId(agentId);
                sr.setUserId(userId);
                sr.setDigital(true);
                sr.setMobileDate(System.currentTimeMillis() + "");
                sr.setTransactionDate(shgDto.getLastTransactionDate());
                String sreqString = new Gson().toJson(sr);

                if (networkConnection.isNetworkAvailable()) {
                    onTaskStarted();
                    ViewGroup viewGroup = rootView.findViewById(android.R.id.content);
                    View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.online_transaction_timer, viewGroup, false);
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setView(dialogView);
                    alertDialog = builder.create();
                    alertDialog.show();
                    alertDialog.setCancelable(false);
                    counterTextView = dialogView.findViewById(R.id.counterTimerTxt);
                    counterTimerStatusTxt = dialogView.findViewById(R.id.counterTimerStatusTxt);


                    Restclient_Timeout.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.SAVINGS, sreqString, getActivity(), ServiceType.SAVINGS);

                    cndr = new CountDownTimer(timeout_saving, 1000) { // adjust the milli seconds here
                        public void onTick(long millisUntilFinished) {
                            counterTextView.setText("" + String.format(FORMAT_TIMER,
                                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(
                                            TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                                            TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))) + " minutes");
                        }

                        @Override
                        public void onFinish() {
//                            MemberDrawerScreen.showFragment(new Digitization_Deposit_Details());
                            cndr.cancel();
                            alertDialog.dismiss();
                            TastyToast.makeText(getActivity(), AppStrings.counterDismiss, TastyToast.LENGTH_SHORT,
                                    TastyToast.WARNING);
//                            timer.cancel();
                        }


                    }.start();
                }

                break;
            case R.id.fragment_Previousbutton:
                break;
            case R.id.fragment_Nextbutton:
                break;

            case R.id.autoFill:

                String similiar_Savings;

                if (mAutoFill.isChecked()) {

                    try {

                        // Makes all edit fields holds the same savings
                        if (!String.valueOf(sSavingsFields.get(0).getText()).equals("")) {


                            similiar_Savings = sSavingsFields.get(0).getText().toString();

                            // send_To_Server_SavingsType = AppStrings.savings;

                            System.out.println("CB Amount : " + similiar_Savings);

                            for (int i = 0; i < sSavingsAmounts.length; i++) {
                                sSavingsFields.get(i).setText(similiar_Savings);
                                sSavingsFields.get(i).setGravity(Gravity.RIGHT);
                                sSavingsFields.get(i).clearFocus();
                                sSavingsAmounts[i] = similiar_Savings;
                            }

                        }

                        /** To clear the values of EditFields in case of uncheck **/

                    } catch (ArrayIndexOutOfBoundsException e) {
                        e.printStackTrace();

                        TastyToast.makeText(getActivity(), AppStrings.adminAlert, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);

                    }
                } else {
                    // Do check box unCheck
                }

                break;
            default:
                break;
        }
    }

    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        if (mProgressDilaog != null) {
            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                mProgressDilaog.dismiss();
                mProgressDilaog = null;
//                if (confirmationDialog.isShowing())
//                    confirmationDialog.dismiss();
            }
            switch (serviceType) {
                case SAVINGS:
                    try {
                        if (result != null) {
                            ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                            String message = cdto.getMessage();
                            int statusCode = cdto.getStatusCode();
                            if (statusCode == Utils.Success_Code) {
                                TastyToast.makeText(getActivity(), "Transaction Completed",
                                        TastyToast.LENGTH_SHORT, TastyToast.SUCCESS);
                                if (alertDialog != null && alertDialog.isShowing())
                                    alertDialog.dismiss();
                                if (cndr != null)
                                    cndr.cancel();
                                Log.d("response", result.toString());
                                Utils.showToast(getActivity(), message);
                                if (Attendance.flag == "1") {
                                    confirmationDialog.dismiss();
                                    Internalloan_repayment internalloanRepayment = new Internalloan_repayment();
                                    Bundle bundles = new Bundle();
                                    bundles.putString("stepwise", Attendance.flag);
                                    internalloanRepayment.setArguments(bundles);
                                    FragmentManager fm = getFragmentManager();
                                    fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                    NewDrawerScreen.showFragment(internalloanRepayment);
                                } else {
                                    if (shgDto.getFFlag() == null || !shgDto.getFFlag().equals("1")) {
                                        SHGTable.updateTransactionSHGDetails(shgDto.getShgId());
                                    }

                                    if (confirmationDialog.isShowing())
                                        confirmationDialog.dismiss();

                                    if (cdto.getResponseContents() != null) {
                                        for (int i = 0; i < cdto.getResponseContents().size(); i++) {
                                            ResponseContents contents = new ResponseContents();
                                            contents.setMemberName(cdto.getResponseContents().get(i).getMemberName());
                                            contents.setTransactionId(cdto.getResponseContents().get(i).getTransactionId());
                                            contents.setMessage(cdto.getResponseContents().get(i).getMessage());
                                            contents.setTxType(AppStrings.savings);

                                            responseContentsList.add(contents);

                                        }
                                        ViewSavingTransactionDetails viewSavingTransactionDetails = new ViewSavingTransactionDetails();
                                        Bundle bundle = new Bundle();
                                        bundle.putSerializable("transactiondetails", responseContentsList);
                                        viewSavingTransactionDetails.setArguments(bundle);
                                        NewDrawerScreen.showFragment(viewSavingTransactionDetails);

                                    } else {
                                        FragmentManager fm = getFragmentManager();
                                        fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                        MainFragment mainFragment = new MainFragment();
                                        Bundle bundles = new Bundle();
                                        bundles.putString("Transaction", MainFragment.Flag_Transaction);
                                        mainFragment.setArguments(bundles);
                                        NewDrawerScreen.showFragment(mainFragment);
                                    }

                                }


                            } else {

                                if (statusCode == 401) {

                                    Log.e("Group Logout", "Logout Sucessfully");
                                    AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                                    if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                        mProgressDilaog.dismiss();
                                        mProgressDilaog = null;
                                    }
                                }

                                if (statusCode == 503) {

                                    Utils.showToast(getActivity(), AppStrings.service_unavailable);

                                }

                                TastyToast.makeText(getActivity(), AppStrings.failedDigiResponse,
                                        TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                                if (alertDialog != null && alertDialog.isShowing())
                                    alertDialog.dismiss();
                                if (cndr != null)
                                    cndr.cancel();
                            }
                        } else {
                            TastyToast.makeText(getActivity(), AppStrings.failedDigiResponse,
                                    TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                            if (alertDialog != null && alertDialog.isShowing())
                                alertDialog.dismiss();
                            if (cndr != null)
                                cndr.cancel();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;

                case WALL_TXANIMATOR:
                    Log.d("ddd", "txt");

                    try {
                        ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                        String message = cdto.getMessage();
                        int statusCode = cdto.getStatusCode();
                        if (statusCode == Utils.Success_Code) {
                            Utils.showToast(getActivity(), message);
                            String walletamount = cdto.getResponseContent().getWalletAmount();
                            mWalletamount = (int) Double.parseDouble(walletamount);
                            Wallet_balance.setText(AppStrings.w_b + String.valueOf(mWalletamount));
                            Wallet_balance.setTypeface(LoginActivity.sTypeface);
                        } else {
                            Utils.showToast(getActivity(), message);

                            if (statusCode == 401) {

                                Log.e("Group Logout", "Logout Sucessfully");
                                AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                                if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                    mProgressDilaog.dismiss();
                                    mProgressDilaog = null;
                                }
                            }

                            if (statusCode == 503) {

                                Utils.showToast(getActivity(), AppStrings.service_unavailable);

                            }

                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;

                case WALLET_CASHIN_HAND:
                    try {
                        ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                        String message = cdto.getMessage();
                        int statusCode = cdto.getStatusCode();
                        if (statusCode == Utils.Success_Code) {
                            Utils.showToast(getActivity(), message);

                            String animatorCashinHand = cdto.getResponseContent().getCurrentCashInHand().trim();
                            mAniiamtorCH = (int) Double.parseDouble(animatorCashinHand);
                            mAnimatorCash_in_hand.setText(AppStrings.w_cih + String.valueOf(mAniiamtorCH));
                            mAnimatorCash_in_hand.setTypeface(LoginActivity.sTypeface);
                        } else {
                            Utils.showToast(getActivity(), message);

                            if (statusCode == 401) {

                                Log.e("Group Logout", "Logout Sucessfully");
                                AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                                if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                    mProgressDilaog.dismiss();
                                    mProgressDilaog = null;
                                }
                            }
                            if (statusCode == 503) {

                                Utils.showToast(getActivity(), AppStrings.service_unavailable);

                            }

                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    onTaskStarted();
                    restClient.callWebServiceForGetMethod(Constants.BASE_URL + Constants.WALL_BAL + userId, getActivity(), ServiceType.WALL_TXANIMATOR);
                    break;
            }

        }

    }
}
