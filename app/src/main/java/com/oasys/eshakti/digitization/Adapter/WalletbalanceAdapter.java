package com.oasys.eshakti.digitization.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.model.ListItem;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class WalletbalanceAdapter extends RecyclerView.Adapter<WalletbalanceAdapter.WalletBalanceDetails> {


    private Context context;
    private ArrayList<ListItem> listItems;


    public WalletbalanceAdapter(Context context, ArrayList<ListItem> listItems) {
        this.context = context;
        this.listItems = listItems;
    }

    @NonNull
    @Override
    public WalletBalanceDetails onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.walletbalancehistory, parent, false);
        return new WalletBalanceDetails(v);

    }

    @Override
    public void onBindViewHolder(@NonNull WalletBalanceDetails holder, final int position) {


        DateFormat simple = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat df1 = new SimpleDateFormat("HH:mm:ss");
        Date d = new Date(Long.parseLong(listItems.get(position).getTransactionDate()));
        String time = df1.format(new Date(Long.parseLong(listItems.get(position).getTransactionDate())));
        String dateStr = simple.format(d);
        holder.wWallet_date.setText("DATE :" + dateStr + " " + time);
        holder.wTransaction_id.setText("TRANSACTION ID :" + listItems.get(position).getTransactionId());
        holder.wPrevious_balance.setText("PREVIOUES WALLET BALANCE :" + listItems.get(position).getPreviousWalletAmount());
        holder.wAmount.setText("AMOUNT :" + listItems.get(position).getAmountProcessed());
        holder.wUpdated_balance.setText("UPDATED WALLET BALANCE :" + listItems.get(position).getUpdatedWalletAmount());

        holder.wTransaction_type.setText("TRANSACTION TYPE :" + listItems.get(position).getTransType());

        String first = AppStrings.txStatus;
        if (listItems.get(position).getTransType() != null && listItems.get(position).getTransType().toLowerCase().equals("credit")) {
            String next = "<font color='#2f9e44'>" + listItems.get(position).getTransType() + "</font>";
            holder.wTransaction_type.setText(Html.fromHtml(first + next));
        } else if (listItems.get(position).getTransType() != null && listItems.get(position).getTransType().toLowerCase().equals("debit")) {
            String next = "<font color='#EE0000'>" + listItems.get(position).getTransType() + "</font>";
            holder.wTransaction_type.setText(Html.fromHtml(first + next));
        }

        holder.wRemarks.setText("REMARKS :" + listItems.get(position).getTransRemarks());


    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    class WalletBalanceDetails extends RecyclerView.ViewHolder {

        CardView cardView;
        TextView wWallet_date, wTransaction_id, wPrevious_balance, wAmount, wUpdated_balance, wTransaction_type, wRemarks;

        public WalletBalanceDetails(View itemView) {
            super(itemView);

            cardView = (CardView) itemView.findViewById(R.id.card_view);

            wWallet_date = (TextView) itemView.findViewById(R.id.digitization_wallet_date);


            wTransaction_id = (TextView) itemView.findViewById(R.id.digitization_wallet_transaction_id);


            wPrevious_balance = (TextView) itemView.findViewById(R.id.digitization_wallet_previous_balance);

            wAmount = (TextView) itemView.findViewById(R.id.digitization_wallet_amount);


            wUpdated_balance = (TextView) itemView.findViewById(R.id.digitization_wallet_update_balance);


            wTransaction_type = (TextView) itemView.findViewById(R.id.digitization_wallet_transaction_type);


            wRemarks = (TextView) itemView.findViewById(R.id.digitization_wallet_remarks);


        }
    }
}
