package com.oasys.eshakti.digitization.Dto.RequestDto;

import java.io.Serializable;

import lombok.Data;

@Data
public class Wallet_cash implements Serializable {

    private String cashInHand;

    private String currentCashInHand;

    private String userId;

    private String remarks;

    private String cashOutHand;
}
