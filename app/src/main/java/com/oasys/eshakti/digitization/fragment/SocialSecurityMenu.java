package com.oasys.eshakti.digitization.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.oasys.eshakti.digitization.Adapter.CustomListAdapter;
import com.oasys.eshakti.digitization.Dto.ExistingLoan;
import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.Dto.MemberList;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.Dto.ShgBankDetails;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.activity.LoginActivity;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;
import com.oasys.eshakti.digitization.database.BankTable;
import com.oasys.eshakti.digitization.database.MemberTable;
import com.oasys.eshakti.digitization.database.SHGTable;
import com.oasys.eshakti.digitization.model.ListItem;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Dell on 15 Dec, 2018.
 */

public class SocialSecurityMenu extends Fragment implements AdapterView.OnItemClickListener, NewTaskListener {
    public static String TAG = LD_EL_SBLoanMenu.class.getSimpleName();

    private TextView mGroupName, mCashinHand, mCashatBank;
    public static String sSelectedIncomeMenu = null;

    String[] mLoanMenu;

    private ListView mListView;
    private List<ListItem> listItems;
    private CustomListAdapter mAdapter;
    int listImage;
    private TextView mHeader;
    boolean isCashCredit = false;
    private View rootView;

    private int mSize;
    private List<MemberList> memList;
    private ListOfShg shgDto;
    private NetworkConnection networkConnection;
    private ArrayList<ShgBankDetails> bankdetails;
    private Dialog mProgressDilaog;
    private ExistingLoan[] mExistLoantypes;
    private ArrayList<ExistingLoan> exLoanList;
    public static ExistingLoan sE_MenuSelection;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_new_menulist, container, false);
        return rootView;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mSize = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, "")).size();
        memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        bankdetails = BankTable.getBankName(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));

        /*networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        if (networkConnection.isNetworkAvailable()) {            //  onTaskStarted();
            RestClient.getRestClient(this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.EXT_LOAN_TYPES, getActivity(), ServiceType.EXT_LOAN_TYPES);
        }*/


        mLoanMenu = new String[]{AppStrings.mJhanDan, AppStrings.pmsby, AppStrings.pmjjby, AppStrings.apy, AppStrings.other_M_I};

        init();

    }

    private void init() {
        try {
            mGroupName = (TextView) rootView.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
            mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashinHand.setTypeface(LoginActivity.sTypeface);

            mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
            mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashatBank.setTypeface(LoginActivity.sTypeface);

            mHeader = (TextView) rootView.findViewById(R.id.submenuHeaderTextview);
            mHeader.setVisibility(View.GONE);
            // mHeader.setText("" + LD_ExternalLoan.sExistingLoagSelection.getLoanTypeName());
            mHeader.setTypeface(LoginActivity.sTypeface);

            listItems = new ArrayList<ListItem>();
            mListView = (ListView) rootView.findViewById(R.id.fragment_List);
            listImage = R.drawable.ic_navigate_next_white_24dp;

            for (int i = 0; i < mLoanMenu.length; i++) {
                ListItem rowItem = new ListItem();
                rowItem.setTitle(mLoanMenu[i].toString());
                rowItem.setImageId(listImage);
                listItems.add(rowItem);
            }

            mAdapter = new CustomListAdapter(getActivity(), listItems);
            mListView.setAdapter(mAdapter);
            mListView.setOnItemClickListener(this);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        TextView textColor_Change = (TextView) view.findViewById(R.id.dynamicText);
        textColor_Change.setText(String.valueOf(mLoanMenu[position]));
        textColor_Change.setTextColor(Color.rgb(251, 161, 108));

        sSelectedIncomeMenu = String.valueOf(mLoanMenu[position]);

        /*if (exLoanList != null && exLoanList.size() > 0)
            sE_MenuSelection = exLoanList.get(position);*/


        if (position == 0) {
            JDFragment jDFragment = new JDFragment();
            NewDrawerScreen.showFragment(jDFragment);
        } else if (position == 1) {
            //  EShaktiApplication.setLoanDisburseValues("LOANACC");
            PMSBYFragment pMSBYFragment = new PMSBYFragment();
            NewDrawerScreen.showFragment(pMSBYFragment);
        } else if (position == 2) {
            // EShaktiApplication.setLoanDisburseValues("SBACC");
            PMJJBY pMJJBY = new PMJJBY();
            NewDrawerScreen.showFragment(pMJJBY);
        } else if (position == 3) {
            //  EShaktiApplication.setLoanDisburseValues("REPAID");
            APYFragment aPYFragment = new APYFragment();
            NewDrawerScreen.showFragment(aPYFragment);

        } else if (position == 4) {
            //  EShaktiApplication.setLoanDisburseValues("REPAID");
            O_MI o_MI = new O_MI();
            NewDrawerScreen.showFragment(o_MI);

        }

    }

    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
            mProgressDilaog.dismiss();
            mProgressDilaog = null;
        }

        switch (serviceType) {
            case EXT_LOAN_TYPES:
                try {
                    if (result != null && result.length() > 0) {
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();
                        ResponseDto mrDto = gson.fromJson(result, ResponseDto.class);
                        int statusCode = mrDto.getStatusCode();
                        Log.d("Main Frag response ", " " + statusCode);
                        if (statusCode == 400 || statusCode == 401 || statusCode == 403 || statusCode == 500 || statusCode == 503) {
                            // showMessage(statusCode);

                        } else if (statusCode == Utils.Success_Code) {

                            exLoanList = mrDto.getResponseContent().getSettingsList();

                            isCashCredit = false;
                            if (LD_ExternalLoan.sExistingLoagSelection.getLoanTypeName().equals("Cash Credit")) {

                                ExistingLoan[] stockArr = new ExistingLoan[exLoanList.size()];
                                mExistLoantypes = exLoanList.toArray(stockArr);

                                mLoanMenu = new String[mExistLoantypes.length];

                                for (int i = 0; i < mExistLoantypes.length; i++) {
                                    mLoanMenu[i] = mExistLoantypes[i].getName();
                                    Log.i("Loan Type", mLoanMenu[i].toString());
                                }
/*
                                mLoanMenu = new String[]{AppStrings.mIncreaseLimit, AppStrings.mLoanDisbursementFromLoanAcc,
                                        AppStrings.mLoanDisbursementFromSbAcc, AppStrings.mLoanDisbursementFromRepaid};
                                EShaktiApplication.setIsLoanDisBurseRepaid(false);*/
                                isCashCredit = true;
                            } else {
                                ExistingLoan[] stockArr = new ExistingLoan[exLoanList.size()];
                                mExistLoantypes = exLoanList.toArray(stockArr);

                                mLoanMenu = new String[mExistLoantypes.length];

                                for (int i = 0; i < mExistLoantypes.length; i++) {
                                    mLoanMenu[i] = mExistLoantypes[i].getName();
                                    Log.i("Loan Type", mLoanMenu[i].toString());
                                }
                            }


                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }
}
