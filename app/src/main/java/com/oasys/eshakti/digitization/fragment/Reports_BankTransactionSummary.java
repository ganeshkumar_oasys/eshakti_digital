package com.oasys.eshakti.digitization.fragment;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.digitization.Adapter.BankTransactionDetailsAdapter;
import com.oasys.eshakti.digitization.Dto.GroupBankTransactionSummaryList;
import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.activity.LoginActivity;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;
import com.oasys.eshakti.digitization.database.SHGTable;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.tutorialsee.lib.TastyToast;
import java.util.ArrayList;


public class Reports_BankTransactionSummary extends Fragment implements NewTaskListener {
    private View rootView;
    private RecyclerView banktransaction_recyclerview;
    ResponseDto bankTransactionResponseDto;
    private BankTransactionDetailsAdapter bankTransactionDetailsAdapter;
    private LinearLayoutManager linearLayoutManager;
    private ListOfShg shgDto;
    private String shgId;
    ArrayList<GroupBankTransactionSummaryList> groupBankTransactionSummaryLists;
    private TextView mGroupName;
    private TextView mCashinHand;
    private TextView mCashatBank,date,amt,inrt,mHeader;
    public Reports_BankTransactionSummary() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgId = shgDto.getShgId();
        rootView = inflater.inflate(R.layout.fragment_reports__bank_transaction_summary, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mGroupName = (TextView) rootView.findViewById(R.id.groupname);
        mGroupName.setText(shgDto.getName()+" / "+shgDto.getPresidentName());
        mGroupName.setTypeface(LoginActivity.sTypeface);

        mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
        mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
        mCashinHand.setTypeface(LoginActivity.sTypeface);

        mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
        mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
        mCashatBank.setTypeface(LoginActivity.sTypeface);
        mHeader = (TextView) rootView.findViewById(R.id.fragHeader);
        mHeader.setTypeface(LoginActivity.sTypeface);

        date = (TextView) rootView.findViewById(R.id.date);
        date.setTypeface(LoginActivity.sTypeface);
        amt = (TextView) rootView.findViewById(R.id.amt);
        amt.setTypeface(LoginActivity.sTypeface);
        inrt = (TextView) rootView.findViewById(R.id.inrt);
        inrt.setTypeface(LoginActivity.sTypeface);

        init();
        String url = Constants.BASE_URL + Constants.REPORT_BANKTRANSACTION + shgId;

        //String url = Constants.BASE_URL + Constants.REPORT_BANKTRANSACTION + "a68547c5-4815-4868-9d50-3c820ab04bc7";
        if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {
            RestClient.getRestClient(Reports_BankTransactionSummary.this).callWebServiceForGetMethod(url, getActivity(), ServiceType.BANKTRANSACTIONSUMMARY);
        } else {
            NewDrawerScreen.showFragment(new MainFragment());
            Utils.showToast(getActivity(), "No network Available");
        }
        linearLayoutManager = new LinearLayoutManager(getActivity());
        banktransaction_recyclerview.setLayoutManager(linearLayoutManager);
        banktransaction_recyclerview.setHasFixedSize(true);
        banktransaction_recyclerview.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        groupBankTransactionSummaryLists = new ArrayList<>();
    }

    public void init() {
        banktransaction_recyclerview = (RecyclerView) rootView.findViewById(R.id.bts_recyclerview);
    }

    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        try {
            switch (serviceType) {
                case BANKTRANSACTIONSUMMARY:
                    Log.d("getDetails", " " + result.toString());
                    bankTransactionResponseDto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    if (bankTransactionResponseDto.getStatusCode() == Utils.Success_Code) {
                        Log.d("NumbersDTOLists", " " + bankTransactionResponseDto.getResponseContent().getGroupBankTransactionSummaryList());

                        Utils.showToast(getActivity(), bankTransactionResponseDto.getMessage());
                        if ((bankTransactionResponseDto.getResponseContent().getGroupBankTransactionSummaryList() != null)) {
                            bankTransactionDetailsAdapter = new BankTransactionDetailsAdapter(getActivity(),
                                    bankTransactionResponseDto.getResponseContent().getGroupBankTransactionSummaryList());
                            banktransaction_recyclerview.setAdapter(bankTransactionDetailsAdapter);
                        } else {
                            Utils.showToast(getActivity(), "Null Value");
                        }

                    } else if (bankTransactionResponseDto.getStatusCode() == 401) {

                        Log.e("Group Logout", "Logout Sucessfully");
                        AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                    } else {
                        TastyToast.makeText(getActivity(), "No BANKTRANSACTION REPORTS DATA",
                                TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                    }

                    break;


            }
        } catch (Exception e) {
            Log.e("BAnktransaction", e.toString());

        }
    }
}
