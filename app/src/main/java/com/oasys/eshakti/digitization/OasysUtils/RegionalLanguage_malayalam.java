package com.oasys.eshakti.digitization.OasysUtils;

import com.yesteam.eshakti.appConstants.AppStrings;

public class RegionalLanguage_malayalam {

	public RegionalLanguage_malayalam() {
		// TODO Auto-generated constructor stub
	}

	public static void RegionalStrings() {

		AppStrings.LoginScreen = "ലോഗിൻ സ്ക്രീൻ";
		AppStrings.userName = "മൊബൈൽ നമ്പർ";
		AppStrings.passWord = "പാസ്സ്‌വേർഡ്";
		AppStrings.transaction = "ധന ഇടപാടുകൾ";
		AppStrings.reports = "റിപ്പോർട്ടുകൾ";
		AppStrings.profile = "പ്രൊഫൈൽ";
		AppStrings.meeting = "യോഗ വിവരങ്ങൾ";
		AppStrings.settings = "സെറ്റിങ്‌സ്";
		AppStrings.help = "സഹായത്തിന്";
		AppStrings.Attendance = "ഹാജർ നില";
		AppStrings.MinutesofMeeting = "യോഗ നടപടിക്രമം";
		AppStrings.savings = " സേവിങ്‌സ്";
		AppStrings.InternalLoanDisbursement = "വായ്പാ വിതരണം";
		AppStrings.expenses = "ചിലവ്";
		AppStrings.cashinhand = "കൈവശമുള്ള തുക :  ₹ ";
		AppStrings.cashatBank = "ബാങ്കിലുള്ള തുക :  ₹ ";
		AppStrings.memberloanrepayment = "അംഗത്തിൻ്റെ  വായ്പാ തിരിച്ചടവ്";
		AppStrings.grouploanrepayment = "ഗ്രൂപ്പ് ലോൺ തിരിച്ചടവ്";
		AppStrings.income = "വരുമാനം";
		AppStrings.bankTransaction = "ബാങ്ക് ഇടപാട്";
		AppStrings.fixedDeposit = "സ്ഥിര നിക്ഷേപം";
		AppStrings.otherincome = "മറ്റു വരുമാനം";
		AppStrings.subscriptioncharges = "വരിസംഖ്യ";
		AppStrings.penalty = "പിഴ";
		AppStrings.donation = "സംഭാവന";
		AppStrings.federationincome = "ഫെഡറേഷൻ വരുമാനം";
		AppStrings.date = "തീയതി";
		AppStrings.passwordchange = "പാസ്സ്‌വേർഡ് മാറ്റുക";
		AppStrings.listofexpenses = "ചിലവ് പട്ടിക ";
		AppStrings.transport = "ഗതാഗതം";
		AppStrings.snacks = "ചായ";
		AppStrings.telephone = "ടെലിഫോൺ";
		AppStrings.stationary = "ലേഖനസാമഗ്രികള്‍";
		AppStrings.otherExpenses = "മറ്റു ചിലവുകൾ";
		AppStrings.federationIncomePaid = "ഫെഡറേഷൻ വരുമാനം (പെയ്ഡ്)";
		AppStrings.bankInterest = "ബാങ്ക് പലിശ";
		AppStrings.bankExpenses = "ആകെത്തുക";
		AppStrings.bankrepayment = "സംഗ്രഹം/ ചുരുക്കം";
		AppStrings.total = "ആകെത്തുക ";
		AppStrings.summary = "സംഗ്രഹം/ ചുരുക്കം ";
		AppStrings.savingsSummary = "സേവിങ്‌സ് സംഗ്രഹം ";
		AppStrings.loanSummary = "വായ്പാ സംഗ്രഹം ";
		AppStrings.lastMonthReport = "പ്രതിമാസ റിപ്പോർട്ട്";
		AppStrings.Memberreports = "അംഗത്തിൻറെ റിപ്പോർട്ടുകൾ";
		AppStrings.GroupReports = "ഗ്രൂപ്പ് റിപ്പോർട്ടുകൾ";
		AppStrings.transactionsummary = "ബാങ്കിടപാട് സംഗ്രഹം ";
		AppStrings.balanceSheet = "ബാലൻസ് ഷീറ്റ് ";
		AppStrings.balanceSheetHeader = "ബാലൻസ് ഷീറ്റ് തീയതി ";
		AppStrings.trialBalance = "ട്രയൽ ബാലൻസ്";
		AppStrings.outstanding = "കുടിശ്ശികയായ തുക ";
		AppStrings.totalSavings = "ആകെ സേവിങ്സ് തുക ";
		AppStrings.groupSavingssummary = "ഗ്രൂപ്പിന്റെ  സേവിങ്സ് സംഗ്രഹം  ";
		AppStrings.groupLoansummary = "ഗ്രൂപ്പിന്റെ  വായ്പാ സംഗ്രഹം  ";
		AppStrings.amount = "തുക";
		AppStrings.principleAmount = "തുക";
		AppStrings.interest = "പലിശ";
		AppStrings.groupLoan = "സംഘത്തിൻ്റെ വായ്പ ";
		AppStrings.balance = "ബാലൻസ്";
		AppStrings.Name = "പേര് ";
		AppStrings.Repaymenybalance = "തിരിച്ചടവ് ബാലൻസ്";
		AppStrings.details = "വിശദാംശങ്ങൾ";
		AppStrings.payment = "പേയ്മെന്റ്";
		AppStrings.receipt = "വരവ് ";
		AppStrings.FromDate = "തീയതി മുതൽ";
		AppStrings.ToDate = "തീയതി വരെ ";
		AppStrings.fromDate = "മുതൽ ";
		AppStrings.toDate = "വരെ ";
		AppStrings.OldPassword = "പഴയ  പാസ്സ്‌വേർഡ് ";
		AppStrings.NewPassword = "പുതിയ പാസ്സ്‌വേർഡ് ";
		AppStrings.ConfirmPassword = "പാസ്സ്‌വേർഡ്  സ്ഥിരീകരിക്കുക ";
		AppStrings.agentProfile = "ഏജൻറ് പ്രൊഫൈൽ";
		AppStrings.groupProfile = "ഗ്രൂപ്പ് പ്രൊഫൈൽ";
		AppStrings.bankDeposit = "ബാങ്ക് ഡെപ്പോസിറ്റ്";
		AppStrings.withdrawl = "പിൻവലിക്കൽ";
		AppStrings.OutstandingAmount = "കുടിശ്ശിക";
		AppStrings.GroupLogin = "ഗ്രൂപ്പ് ലോഗിൻ";
		AppStrings.AgentLogin = "ഏജൻറ് ലോഗിൻ";
		AppStrings.AboutLicense = "ലൈസൻസ്";
		AppStrings.contacts = "കോൺടാക്ട്";
		AppStrings.contactNo = "ബന്ധപ്പെടേണ്ട നമ്പർ ";
		AppStrings.cashFlowStatement = "പണത്തിന്റെ  വരവ് ചെലവ് പട്ടിക ";
		AppStrings.IncomeExpenditure = "വരുമാന ചെലവ് പട്ടിക ";
		AppStrings.transactionCompleted = "ഇടപാട് പൂർത്തിയായി";
		AppStrings.loginAlert = "ലോഗിൻ വാക്കും പാസ്സ്‌വേർഡും തെറ്റാണ് ";
		AppStrings.loginUserAlert = "ലോഗിൻ വാക്ക് ";
		AppStrings.loginPwdAlert = "പാസ്സ്‌വേർഡ്  വിശദാംശങ്ങൾ നൽകുക";
		AppStrings.pwdChangeAlert = "പാസ്സ്‌വേർഡ് വിജയകരമായി മാറ്റിയിരിക്കുന്നു";
		AppStrings.pwdIncorrectAlert = "ശരിയായ പാസ്സ്‌വേർഡ് നൽകുക";
		AppStrings.groupRepaidAlert = "നിങ്ങളുടെ മികച്ച തുക പരിശോധിക്കുക";//"തുക പരിശോധിക്കൂ ";
		AppStrings.cashinHandAlert = "കയ്യിലിരുപ്പ് സംഖ്യ പരിശോധിക്കൂ ";
		AppStrings.cashatBankAlert = "ബാങ്കിലുള്ള  സംഖ്യ പരിശോധിക്കൂ ";
		AppStrings.nullAlert = "പണത്തിൻ്റെ വിശദാംശങ്ങൾ നൽകുക";
		AppStrings.DepositnullAlert = "എല്ലാ ക്യാഷ് വിശദാംശങ്ങൾ നൽകുക";
		AppStrings.MinutesAlert = "മുകളിൽ നിന്ന്  തിരഞ്ഞെടുക്കുക";
		AppStrings.afterDate = "ഈ തീയതിക്കുശേഷം ഒരു തീയതി തിരഞ്ഞെടുക്കുക";
		AppStrings.last5Transaction = "കഴിഞ്ഞ അഞ്ച് ഇടപാടുകൾ ";
		AppStrings.internetError = "നിങ്ങളുടെ ഇന്റർനെറ്റ് കണക്ഷൻ പരിശോധിക്കുക";
		AppStrings.lastTransactionDate = "കഴിഞ്ഞ ഇടപാടിന്റെ തീയതി";
		AppStrings.yes = "ഓ.കെ ";
		AppStrings.credit = "ക്രെഡിറ്റ്";
		AppStrings.debit = "ഡെബിറ്റ്";
		AppStrings.memberName = "അംഗത്തിൻ്റെ പേര് ";
		AppStrings.OutsatndingAmt = "കുടിശ്ശിക";
		AppStrings.calFromToDateAlert = "രണ്ടു തീയതിയും തിരഞ്ഞെടുക്കൂ ";
		AppStrings.selectedDate = "തിരഞ്ഞെടുത്ത തീയതി";
		AppStrings.calAlert = "വ്യത്യസ്ത തീയതി തിരഞ്ഞെടുക്കുക";
		AppStrings.dialogMsg = "ഈ തീയതിയുമായി തുടരണോ ";
		AppStrings.dialogOk = "അതെ";
		AppStrings.dialogNo = "ഇല്ല";

		AppStrings.aboutYesbooks = "ബഹുമാനപ്പെട്ട പ്രധാനമന്ത്രിയുടെ ഇലക്ട്രോണിക് ഡിജിറ്റൽ ഇന്ത്യ എന്ന സ്വപ്നസാക്ഷാത്ക്കാരത്തിനായി നബാർഡ് "
				+ "പ്രധാന കാര്യാലയത്തിലെ മൈക്രോ ക്രെഡിറ്റ് ഇന്നവേഷൻസ് വിഭാഗം പ്രാരംഭം കുറിക്കുന്ന പദ്ധതിയാണ് ഇ-ശക്തി അഥവാ സ്വയം സഹായ സംഘങ്ങളുടെ "
				+ "ഡിജിറ്റൈസേഷൻ . മികച്ച ഭരണനിര്‍വഹണം ഉറപ്പു വരുത്താനായി Rs.1.13  ലക്ഷം കോടി രൂപയോളം വകയിരുത്തി  സർക്കാർ വകുപ്പുകളും"
				+ " ജനങ്ങളും തമ്മിൽ സമന്വയിച്ചു പ്രവർത്തിക്കാനായി തുടങ്ങിയ പദ്ധതിയാണ് ഡിജിറ്റൽ ഇന്ത്യ. തദ്‌ഫലമായി ജനതയുടെ ഡിജിറ്റൽ ശാക്തീകരണവും "
				+ "രാജ്യത്തിന്റെ മികച്ച വിവരാധിഷ്ഠിത സമ്പദ്‌വ്യവസ്ഥയും ലക്ഷ്യമാക്കുന്നു.";
		AppStrings.groupList = "ഗ്രൂപ്പ് തിരഞ്ഞെടുക്കുക";// GROUP LIST
		AppStrings.login = "   ലോഗിൻ   ";
		AppStrings.submit = "സബ്‌മിറ്റ്‌ ചെയ്യുക ";
		AppStrings.edit = " തിരുത്തൽ ";
		AppStrings.bankBalance = "ബാങ്ക് ബാലൻസ്";
		AppStrings.balanceAsOn = " ഏതു ദിവസത്തെ ബാലൻസ് : ";
		AppStrings.savingsBook = " ബുക്ക്  പ്രകാരമുള്ള സേവിങ്‌സ് തുക : ";
		AppStrings.savingsBank = "  ബാങ്ക് പ്രകാരമുള്ള സേവിങ്‌സ് തുക : ";
		AppStrings.difference = " വ്യത്യാസം : ";
		AppStrings.loanOutstandingBook = " ബുക്ക്  പ്രകാരമുള്ള കുടിശ്ശിക : ";
		AppStrings.loanOutstandingBank = " ബാങ്ക്  പ്രകാരമുള്ള കുടിശ്ശിക : ";
		AppStrings.values = new String[] { "ജന്മ സ്ഥലം (ഗ്രാമം)",

				"എല്ലാ അംഗങ്ങളും സന്നിഹിതർ ",

				"സേവിങ്സ് തുക ശേഖരിച്ചു ",

				"വ്യക്തിഗത വായ്പ വിതരണം ചെയ്യുന്നതിനെ പറ്റി ചർച്ച ചെയ്തു ",

				"തിരിച്ചടവ് തുകയും പലിശയും ശേഖരിച്ചു ",

				"ട്രെയിനിങ് / പരിശീലനത്തെ കുറിച്ച് ചർച്ച ചെയ്തു ",

				"ബാങ്ക് വായ്പ എടുക്കാൻ തീരുമാനിച്ചു ",

				"പൊതുവായുള്ള അഭിപ്രായങ്ങൾ ചർച്ച ചെയ്തു ",

				"ശുചിത്ത്വത്തെ കുറിച്ച് ചർച്ച ചെയ്തു ",

				"വിദ്യാഭാസത്തിന്റെ പ്രാധാന്യത്തിനെ കുറിച്ച് ചർച്ച ചെയ്തു " };

		AppStrings.January = "ജനുവരി";
		AppStrings.February = "ഫെബ്രുവരി";
		AppStrings.March = "മാർച്ച്";
		AppStrings.April = "ഏപ്രിൽ ";
		AppStrings.May = "മെയ് ";
		AppStrings.June = "ജൂൺ";
		AppStrings.July = "ജൂലൈ";
		AppStrings.August = "ആഗസ്റ്റ്";
		AppStrings.September = "സെപ്റ്റംബർ";
		AppStrings.October = "ഒക്ടോബർ";
		AppStrings.November = "നവംബർ";
		AppStrings.December = "ഡിസംബർ";
		AppStrings.uploadPhoto = "ഫോട്ടോ അപ്ലോഡ് ചെയ്യൂ";
		AppStrings.Il_Disbursed = "അംഗംകൾക്കുള്ള വായ്പ വിതരണം ";
		AppStrings.IL_Loan = "അംഗങ്ങൾക്ക് കൊടുത്ത വായ്പ ";
		AppStrings.Il_Repaid = "അംഗങ്ങൾക്ക് കൊടുത്ത വായ്പ തിരച്ചടച്ചത് ";
		AppStrings.offlineTransactionCompleted = "നിങ്ങളുടെ ഓഫ്‌ലൈൻ ഇടപാട് സേവ് ചെയ്തിരിക്കുന്നു ";
		AppStrings.offlineTransactionreports = "ഓഫ്‌ലൈൻ ഇടപാടിന്റെ റിപ്പോർട്ടുകൾ";
		AppStrings.offlineTransaction = "ഓഫ്‌ലൈൻ ഇടപാടുകൾ";
		AppStrings.disconnectInternet = "ഓഫ്‌ലൈൻ സേവനം തുടരാനായി നിങ്ങളുടെ ഇന്റർനെറ്റ് ഡിസ്കണക്റ്റ് ചെയ്യുക ";
		AppStrings.Dashboard = "ഡാഷ്ബോർഡ്";
		AppStrings.bankTransactionSummary = "ബാങ്ക് ഇടപാട് ചുരുക്കത്തിൽ ";
		AppStrings.BL_Disbursed = "വിതരണം ചെയ്തു ";
		AppStrings.BL_Repaid = "തിരിച്ചടച്ചു ";
		AppStrings.auditing = "ഓഡിറ്റിംഗ്";
		AppStrings.training = "പരിശീലനം";
		AppStrings.auditor_Name = "ഓഡിറ്ററുടെ പേര് ";
		AppStrings.auditing_Date = "ഓഡിറ്റിംഗ് തീയതി";
		AppStrings.training_Date = "ട്രെയിനിംഗ് തീയതി";
		AppStrings.auditing_Period = "ഓഡിറ്റ് കാലയളവ് തിരഞ്ഞെടുക്കുക";
		AppStrings.training_types = new String[] { "ആരോഗ്യ ബോധവത്കരണം ", "ബുക്ക് കീപ്പറുടെ അവബോധം ", "ഉപജീവനമാർഗത്തെക്കുറിച്ചുള്ള  അവബോധം ",
				"സാമൂഹിക   അവബോധം ", "വിദ്യാഭ്യാസ   അവബോധം " };
		AppStrings.changeLanguage = "ഭാഷ മാറ്റുക";
		AppStrings.interestRepayAmount = "ഇപ്പോൾ അടയ്‌ക്കേണ്ട തുക";
		AppStrings.interestRate = "പലിശ നിരക്ക്";
		AppStrings.savingsAmount = "പലിശ നിരക്ക്";
		AppStrings.noGroupLoan_Alert = "ഗ്രൂപ്പിനുള്ള വായ്പ ലഭ്യമല്ല ";
		AppStrings.confirmation = "സ്ഥിരീകരണം ";
		AppStrings.logOut = "ലോഗ് ഔട്ട് ചെയ്യുക";
		AppStrings.fromDateAlert = "എന്നു മുതൽ";
		AppStrings.toDateAlert = "എന്നു വരെ ";
		AppStrings.auditingDateAlert = "ഓഡിറ്റിംഗ് തീയതി  നൽകുക";
		AppStrings.auditorAlert = "ഓഡിറ്ററുടെ പേര് നൽകുക ";
		AppStrings.nullDetailsAlert = "വിശദാംശങ്ങളെല്ലാം നൽകുക";
		AppStrings.trainingDateAlert = "ട്രെയിനിങ് തീയതി നൽകുക";
		AppStrings.trainingTypeAlert = "കുറഞ്ഞത് ഒരു ട്രെയിനിങ് രീതി  തിരഞ്ഞെടുക്കുക ";
		AppStrings.offline_ChangePwdAlert = "ഇന്റർനെറ്റ് കണക്ഷൻ ഇല്ലാതെ നിങ്ങൾക്ക് പാസ്സ്‌വേർഡ് മാറ്റാൻ കഴിയില്ല ";
		AppStrings.transactionFailAlert = "നിങ്ങളുടെ ഇടപാട് പൂർത്തിയായില്ല ";
		AppStrings.voluntarySavings = "അംഗത്തിൻ്റെ സ്വേച്ഛാനുസാരമുള്ല സേവിങ്‌സ് ";
		AppStrings.tenure = "കാലാവധി";
		AppStrings.purposeOfLoan = "വായ്പയുടെ ഉദ്ദേശ്യം ";
		AppStrings.chooseLabel = "വായ്പ തിരഞ്ഞെടുക്കുക ";
		AppStrings.Tenure_POL_Alert = "വായ്പയുടെ ഉദ്ദേശ്യവും കാലയളവും നൽകുക ";
		AppStrings.interestSubvention = "പലിശയിലുള്ള കിഴിവ്";
		AppStrings.adminAlert = "നിങ്ങളുടെ അഡ്മിനുമായി ബന്ധപ്പെടുക ";
		AppStrings.userNotExist = "ലോഗിൻ വിശദാംശങ്ങൾ നിലവിലില്ല. ഓൺലൈൻ ലോഗിൻ തുടരുക.";
		AppStrings.tryLater = "ദയവായി അല്പം കഴിഞ്ഞു ശ്രമിക്കുക";
		AppStrings.offlineDataAvailable = "ഓഫ്‌ലൈൻ വിവരങ്ങൾ  ലഭ്യമാണ്. ദയവായി ലോഗ് ഔട്ട് ചെയ്തിട്ട് വീണ്ടും ശ്രമിക്കുക  ";
		AppStrings.choosePOLAlert = "വായ്പയുടെ ഉദ്ദേശ്യം തിരഞ്ഞെടുക്കുക ";
		AppStrings.InternalLoan = "അംഗങ്ങൾക്കുള്ള വായ്പ ";
		AppStrings.TrainingTypes = "ട്രെയിനിങ് തരങ്ങൾ ";
		AppStrings.uploadInfo = "അംഗത്തിൻ്റെ വിവരങ്ങൾ ";
		AppStrings.uploadAadhar = "ആധാർ അപ്‌ലോഡ് ചെയ്യുക ";
		AppStrings.month = "-- മാസം തിരഞ്ഞെടുക്കുക --";
		AppStrings.year = "-- വർഷം തിരഞ്ഞെടുക്കുക --";
		AppStrings.monthYear_Alert = "മാസവും കൊല്ലവും തിരഞ്ഞെടുക്കുക.";
		AppStrings.chooseLanguage = "ഭാഷ തിരഞ്ഞെടുക്കുക";
		AppStrings.autoFill = "സിസ്റ്റം സ്വയം ഫില്ല് ചെയ്യാൻ ";
		AppStrings.viewAadhar = "ആധാർ കാണാൻ ";
		AppStrings.viewPhoto = "ഫോട്ടോ കാണാൻ ";
		AppStrings.uploadAlert = "ഇന്റർനെറ്റ് കണക്ഷൻ ഇല്ലാതെ നിങ്ങൾക്ക് ഫോട്ടോയും ആധാറും അപ്‌ലോഡ് ചെയ്യാൻ കഴിയില്ല ";
		AppStrings.upload = "അപ്‌ലോഡ് ";
		AppStrings.view = "കാണാൻ ";
		AppStrings.showAadharAlert = "നിങ്ങളുടെ ആധാർ കാർഡ് കാണിക്കൂ.";
		AppStrings.deactivateAccount = "ഈ ഉപകരണത്തിൽ  അക്കൗണ്ട് പ്രവർത്തനരഹിതമാക്കാൻ ";
		AppStrings.deactivateAccount_SuccessAlert = "നിങ്ങളുടെ അക്കൗണ്ട് പ്രവർത്തനരഹിതമായിരിക്കുന്നു ";
		AppStrings.deactivateAccount_FailAlert = "നിങ്ങളുടെ അക്കൗണ്ട് പ്രവർത്തനരഹിതമാക്കാൻ സാധിച്ചില്ല ";
		AppStrings.deactivateNetworkAlert = "ഇന്റർനെറ്റ് കണക്ഷൻ ഇല്ലാതെ നിങ്ങൾക്ക്  അക്കൗണ്ട് പ്രവർത്തനരഹിതമാക്കാൻ കഴിയില്ല ";
		AppStrings.offlineReports = "ഓഫ്‌ലൈൻ റിപ്പോർട്ടുകൾ ";
		AppStrings.registrationSuccess = "നിങ്ങൾ രജിസ്റ്റർ ചെയ്തിരിക്കുന്നു ";
		AppStrings.reg_MobileNo = "രജിസ്റ്റർ ചെയ്ത മൊബൈൽ നമ്പർ നൽകുക";
		AppStrings.reg_IMEINo = " ലോഗിൻ നിഷേധിച്ചിരിക്കുന്നു ";
		AppStrings.reg_BothNo = " ലോഗിൻ നിഷേധിച്ചിരിക്കുന്നു ";
		AppStrings.reg_IMEIUsed = "രജിസ്റ്റർ ചെയ്ത ഉപകരണം പരിശോധിക്കുക. പ്രവേശനാനുമതി നിഷേധിച്ചിരിക്കുന്നു ";
		AppStrings.signInAsDiffUser = "വേറെ ഉപയോക്താവായി ലോഗിൻ ചെയ്യുക  ";
		AppStrings.incorrectUserNameAlert = "നിങ്ങൾ ടൈപ്പ് ചെയ്ത ലോഗിൻ ഐ ഡി തെറ്റാണ് ";

		AppStrings.noofflinedatas = "ഓഫ്‌ലൈൻ ഇടപാട് അനുവദനീയമല്ല ";
		AppStrings.uploadprofilealert = "സ്വന്തം വിശദാംശങ്ങൾ അപ്‌ലോഡ് ചെയ്യുക ";
		AppStrings.monthYear_Warning = "ശരിയായ മാസവും കൊല്ലവും തിരഞ്ഞെടുക്കുക";
		AppStrings.of = " ഓഫ് ";
		AppStrings.previous_DateAlert = "മുമ്പത്തെ തീയതി തിരഞ്ഞെടുക്കുക";
		AppStrings.nobanktransactionreportdatas = "ബാങ്കിടപാടിൻ്റെ വിവരങ്ങൾ ലഭ്യമല്ല ";

		AppStrings.mDefault = "കുടിശ്ശിക വരുത്തൽ ";
		AppStrings.mTotalDisbursement = "ആകെ വിതരണം ചെയ്ത തുക ";
		AppStrings.mInterloan = "വ്യക്തിഗത വായ്പ തിരിച്ചടവ് ";
		AppStrings.mTotalSavings = "മൊത്തം സമാഹരിച്ച തുക ";
		AppStrings.mCommonNetworkErrorMsg = "നിങ്ങളുടെ നെറ്റ്‌വർക്ക് കണക്ഷൻ പരിശോധിക്കുക ";
		AppStrings.mLoginAlert_ONOFF = "ഇന്റർനെറ്റ് കണക്ഷൻ ഉപയോഗിച്ച് വീണ്ടും ലോഗിൻ ചെയ്യുക";
		AppStrings.changeLanguageNetworkException = "ദയവായി ലോഗ് ഔട്ട് ചെയ്തിട്ട് ഇന്റർനെറ്റ് കണക്ഷൻ മുഖാന്തരം വീണ്ടും ലോഗിൻ ചെയ്യുക ";
		AppStrings.photouploadmsg = "ഫോട്ടോ അപ്‌ലോഡ് ആയിക്കഴിഞ്ഞു ";
		AppStrings.loginAgainAlert = "പശ്ചാത്തല ഡാറ്റ ഇപ്പോഴും ചില സമയത്ത് ദയവായി കാത്തിരിക്കുക ലോഡ് ചെയ്യുന്നു.";//"ദയവായി വീണ്ടും ലോഗിൻ ചെയ്യുക ";

		/* Interloan Disbursement menu */
		AppStrings.mInternalInterloanMenu = "അംഗങ്ങൾക്കുള്ള വായ്പ ";
		AppStrings.mInternalBankLoanMenu = "ബാങ്ക് വായ്പ";
		AppStrings.mInternalMFILoanMenu = "മൈക്രോ ധനകാര്യ സ്ഥാപനങ്ങളിൽ നിന്നുള്ള വായ്പ ";
		AppStrings.mInternalFederationMenu = "ഫെഡറേഷൻ വായ്പ";

		AppStrings.bankName = "ബാങ്കിൻ്റെ പേര് ";
		AppStrings.mfiname = "മൈക്രോ ധനകാര്യ സ്ഥാപനത്തിൻ്റെ പേര് ";
		AppStrings.loanType = "വായ്പയുടെ തരം ";
		AppStrings.bankBranch = "ബാങ്ക് ശാഖ";
		AppStrings.installment_type = "ഗഡു കാലാവധിയുടെ തരം ";
		AppStrings.NBFC_Name = "ബാങ്കേതര ധനകാര്യ സ്ഥാപനത്തിൻ്റെ പേര് ";
		AppStrings.Loan_Account_No = "വായ്പ അക്കൗണ്ട് നമ്പർ ";
		AppStrings.Loan_Sanction_Amount = "വായ്പയ്ക്ക് അനുവദിച്ച തുക ";
		AppStrings.Loan_Sanction_Date = "വായ്പ അനുവദിച്ച തീയതി ";
		AppStrings.Loan_Disbursement_Amount = "വായ്പാ വിതരണം ചെയ്യുന്ന തുക ";

		AppStrings.Loan_Disbursement_Date = "വായ്പ വിതരണം ചെയ്ത  തീയതി ";
		AppStrings.Loan_Tenure = "വായ്പാ കാലാവധി";
		AppStrings.Subsidy_Amount = "സബ്സിഡി തുക";
		AppStrings.Subsidy_Reserve_Fund = "സബ്സിഡി കരുതൽ തുക";
		AppStrings.Interest_Rate = "പലിശ നിരക്ക്";

		AppStrings.mMonthly = "പ്രതിമാസ";
		AppStrings.mQuarterly = "ത്രൈമാസ ";
		AppStrings.mHalf_Yearly = "അർദ്ധവാർഷിക ";
		AppStrings.mYearly = "വാർഷിക ";
		AppStrings.mAgri = "കാർഷിക";
		AppStrings.mMSME = "സബ്സിഡി കരുതൽ തുക";
		AppStrings.mOthers = "മറ്റുള്ളവ";

		AppStrings.mMFI_NAME_SPINNER = "മൈക്രോ ധനകാര്യ സ്ഥാപനത്തിൻ്റെ പേര് ";
		AppStrings.mMFINabfins = "നാബ്ഫിൻസ് ";
		AppStrings.mMFIOthers = "മറ്റുള്ളവ";
		AppStrings.mInternalTypeLoan = "ആന്തരിക വായ്പ";

		AppStrings.mPervious = "മുമ്പത്തെ";
		AppStrings.mNext = "അടുത്തത്";
		AppStrings.mPasswordUpdate = "നിങ്ങളുടെ പാസ്സ്‌വേർഡ് മാറ്റിക്കഴിഞ്ഞു ";

		AppStrings.mFixedDepositeAmount = " ദീർഘ നിക്ഷേപ  തുക";
		AppStrings.mFedBankName = "ഫെഡറേഷൻ്റെ പേര് ";//"BANK/NBFC NAME";
		AppStrings.mLoanSanc_loanDist = "വായ്പ വിതരണ തുക പരിശോധിക്കുക ";
		AppStrings.mStepWise_LoanDibursement = "അംഗംകൾക്കുള്ള വായ്പ വിതരണം ";

		AppStrings.mExpense_meeting="യോഗത്തിനായുള്ള ചിലവുകൾ ";//ചെലവിൽ കൂടിക്കാഴ്ച";
		AppStrings.mSubmitbutton = "ഓ.കെ";
		AppStrings.mNewUserSignup="രജിസ്ട്രേഷൻ";
		AppStrings.mMobilenumber="മൊബൈൽ നമ്പർ";
		AppStrings.mIsAadharAvailable = "ആധാർ വിവരങ്ങൾ പുതുക്കിയിട്ടില്ല ";
		AppStrings.mSavingsTransaction = "സേവിംഗ്സ് ഇടപാട്";
		AppStrings.mBankCharges = "ബാങ്ക് ചാർജുകൾ";
		AppStrings.mCashDeposit = "പണം ഡെപ്പോസിറ്റ്";
		AppStrings.mLimit = "വായ്പാ പരിധി ";

		AppStrings.mAccountToAccountTransfer = "കൈമാറുന്നു അക്കൗണ്ട് അക്കൗണ്ട്";
		AppStrings.mTransferFromBank = "ബാങ്കിൽ നിന്ന്";
		AppStrings.mTransferToBank = "ബാങ്കിലേക്ക്";
		AppStrings.mTransferAmount = "ട്രാൻസ്ഫർ തുക";
		AppStrings.mTransferCharges = "കൈമാറുന്നു ചാര്ജുകള്";
		AppStrings.mTransferSpinnerFloating = "ബാങ്കിലേക്ക്";
		AppStrings.mTransferNullToast = "എല്ലാ വിശദാംശങ്ങളും നൽകുക";
		AppStrings.mAccToAccTransferToast = "നിങ്ങൾക്ക് ഒരു ബാങ്ക് അക്കൗണ്ട്";

		/* Loan Account */
		AppStrings.mLoanaccHeader = "വായ്പാ കാലാവധിക്കു";
		AppStrings.mLoanaccCash = "പണം";
		AppStrings.mLoanaccBank = "ബാങ്ക്";
		AppStrings.mLoanaccWithdrawal = "പിൻവലിക്കൽ";
		AppStrings.mLoanaccExapenses = "ചെലവുകൾ";
		AppStrings.mLoanaccSpinnerFloating = "വായ്പാ കാലാവധിക്കു ബാങ്ക്";
		AppStrings.mLoanaccCash_BankToast = "ഒരു ക്യാഷ് അല്ലെങ്കിൽ ബാങ്ക് തിരഞ്ഞെടുക്കുക";
		AppStrings.mLoanaccNullToast = "എല്ലാ വിശദാംശങ്ങളും നൽകുക";
		AppStrings.mLoanaccBankNullToast = "ഒരു ബാങ്ക് നാമം തിരഞ്ഞെടുക്കുക";

		AppStrings.mBankTransSavingsAccount = "സേവിംഗ് അക്കൗണ്ട്";
		AppStrings.mBankTransLoanAccount = "വായ്പാ കാലാവധിക്കു";

		AppStrings.mLoanAccType = "തരം";
		AppStrings.mLoanAccBankAmountAlert = "ബാങ്ക് ബാലൻസ് പരിശോധിക്കുക";

		AppStrings.mCashAtBank = "ബാങ്കിലുള്ള തുക";
		AppStrings.mCashInHand = "കൈവശമുള്ള തുക";
		AppStrings.mShgSeedFund = "SHG സീഡ് ഫണ്ട്";
		AppStrings.mFixedAssets = "സ്ഥിര ആസ്തി";
		AppStrings.mVerified = "പരിശോധിച്ചു";
		AppStrings.mConfirm = "ഉറപ്പിക്കുക";
		AppStrings.mProceed = "മുന്നോട്ട്";
		AppStrings.mDialogAlertText = "നിങ്ങളുടെ ഓപ്പണിംഗ് ബാലൻസ് datas വിജയകരമായി അപ്ഡേറ്റ് ചെയ്യുന്നു, ഇപ്പോൾ നിങ്ങൾ മുന്നോട്ട്";
		AppStrings.mBalanceSheetDate = "ബാലൻസ് ഷീറ്റിൽ അപ്ലോഡുചെയ്തു";
		AppStrings.mCheckMonthlyEntries = "മുമ്പത്തെ മാസം എൻട്രികൾ, അതിനാൽ നിങ്ങൾ മുമ്പത്തെ മാസം datas വയ്ക്കും";
		AppStrings.mTermLoanOutstanding = "OUTSTANDING";
		AppStrings.mCashCreditOutstanding = "OUTSTANDING";
		AppStrings.mGroupLoanOutstanding = "ശേഷിക്കുന്ന ഗ്രൂപ്പ് ലോൺ";
		AppStrings.mMemberLoanOutstanding = "ശേഷിക്കുന്ന അംഗം വായ്പ";
		AppStrings.mContinueWithDate = "ഈ തീയതി മാറ്റണോ";
		AppStrings.mCheckList = "ചെക്ക് ലിസ്റ്റ്";
		AppStrings.mMicro_Credit_Plan = "മൈക്രോ ക്രെഡിറ്റ് പദ്ധതി";
		AppStrings.mAadharCard_Invalid = "നിങ്ങളുടെ ആധാർ കാർഡ് അസാധുവാണ്";
		AppStrings.mIsNegativeOpeningBalance = "നെഗറ്റീവ് ബാലൻസ് പരിഹരിക്കാൻ";
		AppStrings.mLoginDetailsDelete = "മുൻ ലോഗിൻ വിശദാംശങ്ങൾ ഇല്ലാതാക്കും. നിങ്ങൾ ഡാറ്റ നീക്കണമെന്ന്?";
		AppStrings.mVerifyGroupAlert = "നിങ്ങൾ പരിശോധിക്കാൻ ഓൺലൈനിൽ ഉണ്ടായിരിക്കണം";
		AppStrings.next = "അടുത്തത്";
		AppStrings.mNone = "ആരും";
		AppStrings.mSelect = "തെരഞ്ഞെടുക്കുക";
		AppStrings.mAnimatorName = "ആനിമേറ്റർ പേര്";
		AppStrings.mSavingsAccount = "സേവിംഗ്സ് അക്കൗണ്ട്";
		AppStrings.mAccountNumber = "അക്കൗണ്ട് നമ്പർ";
		AppStrings.mLoanAccountAlert = "ഓഫ്ലൈൻ മോഡിൽ വായ്പാ കാലാവധിക്കു കൈമാറാനോ കഴിയില്ല";
		AppStrings.mSeedFund = "വിത്തു ഫണ്ട്";
		AppStrings.mLoanTypeAlert = "വായ്പ അക്കൗണ്ട് തരം തിരഞ്ഞെടുക്കുക";
		AppStrings.mOutstandingAlert = "നിങ്ങളുടെ ശ്രദ്ധേയമായ AMOUNT നെ പരിശോധിക്കുക";
		AppStrings.mLoanAccTransfer = "വായ്പാ കാലാവധിക്കു ട്രാൻസ്ഫർ";
		AppStrings.mLoanName = "വായ്പ പേര്";
		AppStrings.mLoanId = "വായ്പ ഐഡി";
		AppStrings.mLoanDisbursementDate = "വായ്പാ വിതരണത്തിൽ തീയതി";
		AppStrings.mAdd = "ചേർക്കുക";
		AppStrings.mLess = "കുറവ്";
		AppStrings.mRepaymentWithInterest = "തിരിച്ചടവ് (പലിശയും)";
		AppStrings.mCharges = "വില";
		AppStrings.mExistingLoan = "നിലവിലുള്ള വായ്പ";
		AppStrings.mNewLoan = "പുതിയ വായ്പ";
		AppStrings.mIncreaseLimit = "പരിധി വർദ്ധിപ്പിക്കാൻ";
		AppStrings.mAvailableLimit = "ലഭ്യമായ പരിധി";
		AppStrings.mDisbursementDate = "ചിലവ് തീയതി";
		AppStrings.mDisbursementAmount = "ചിലവ് തുക";
		AppStrings.mSanctionAmount = "അനുമതി തുക";
		AppStrings.mBalanceAmount = "ബാലൻസ് തുക";
		AppStrings.mMemberDisbursementAmount = "അംഗം വിതരണത്തിൽ തുക";
		AppStrings.mLoanDisbursementFromLoanAcc = "വായ്പാ കാലാവധിക്കു വായ്പ വിതരണത്തിൽ";
		AppStrings.mLoanDisbursementFromSbAcc = "എസ്.ബി അക്കൗണ്ടിൽ നിന്ന് വായ്പ വിതരണത്തിൽ";
		AppStrings.mRepaid = "പ്രതിഫലം";
		AppStrings.mCheckBackLog = "ബാധിച്ചിട്ടില്ല പരിശോധിക്കുക";
		AppStrings.mTarget = "ടാർഗെറ്റ്";
		AppStrings.mCompleted = "പൂർത്തിയായി";
		AppStrings.mPending = "തീരുമാനിക്കപ്പെടാത്ത";
		AppStrings.mAskLogout = "നിങ്ങൾ ലോഗൗട്ട് ആഗ്രഹിക്കുന്നത് ?";
		AppStrings.mCheckDisbursementAlert = "നിങ്ങളുടെ ചിലവ് തുക പരിശോധിക്കുക";
		AppStrings.mCheckbalanceAlert = "നിങ്ങളുടെ ബാലൻസ് തുക പരിശോധിക്കുക";
		AppStrings.mVideoManual = "വീഡിയോ മാനുവൽ";
		AppStrings.mPdfManual = "പിഡിഎഫ് മാനുവൽ";
		AppStrings.mAmountDisbursingAlert = "നിങ്ങൾ അംഗങ്ങൾക്ക് ദിസ്ബുര്സിന്ഗ് ഇല്ലാതെ തുടരണോ ?";
		AppStrings.mOpeningDate = "ആദ്യ തീയതി";
		AppStrings.mCheckLoanSancDate_LoanDisbDate = "നിങ്ങളുടെ വായ്പ അനുമതി തീയതിയും വായ്പാ വിതരണത്തിൽ തീയതി പരിശോധിക്കുക.";
		AppStrings.mLoanDisbursementFromRepaid = "Cc വായ്പ പിൻവലിക്കൽ";
		AppStrings.mExpense = "ചെലവുകൾ";
		AppStrings.mBeforeLoanPay = "വായ്പ തിരിച്ചടയ്ക്കുന്നതിന് മുമ്പ്";
		AppStrings.mLoans = "വായ്പകൾ";
		AppStrings.mSurplus = "മിച്ചം";
		AppStrings.mPOLAlert = "വായ്പയുടെ ലക്ഷ്യം നൽകുക";
		AppStrings.mCheckLoanAmounts = "നിങ്ങളുടെ വായ്പ അനുവദിക്കുന്നതും വായ്പ തിരിച്ചടവ് തുകയും പരിശോധിക്കുക";
		AppStrings.mCheckLoanSanctionAmount = "നിങ്ങളുടെ വായ്പാ തുക നൽകുക";
		AppStrings.mCheckLoanDisbursementAmount = "നിങ്ങളുടെ വായ്പാ വിതരണ തുക നൽകുക";
		AppStrings.mIsPasswordSame = "ദയവായി നിങ്ങളുടെ പഴയ രഹസ്യവാക്ക് പരിശോധിച്ച് പുതിയ പാസ്സ്വേർഡ് പാടില്ല.";
		AppStrings.mSavings_Banktransaction = "സേവിംഗ്സ് - ബാങ്ക് ഇടപാട്";
		AppStrings.mCheckBackLogOffline = "ഓഫ്ലൈനിൽ ചെക്ക് ബാക്ക്ലോക്ക് കാണാൻ കഴിയില്ല";
		AppStrings.mCredit = "ക്രെഡിറ്റ്";
		AppStrings.mDebit = "ഡെബിറ്റ്";
		AppStrings.mCheckFixedDepositAmount = "നിങ്ങളുടെ സ്ഥിര നിക്ഷേപ തുക പരിശോധിക്കുക.";
		AppStrings.mCheckDisbursementDate = "ദയവായി നിങ്ങളുടെ വായ്പാ വിതരണ തീയതി പരിശോധിക്കുക";
		AppStrings.mSendEmail = "EMAIL";
		AppStrings.mTotalInterest = "മൊത്തം താൽപ്പര്യം";
		AppStrings.mTotalCharges = "മൊത്തം നിരക്കുകൾ";
		AppStrings.mTotalRepayment = "മൊത്തം തിരിച്ചടവ്";
		AppStrings.mTotalInterestSubventionRecevied = "മൊത്തം പലിശ സബ്വെൻഷൻ സ്വീകരിച്ചു";
		AppStrings.mTotalBankCharges = "മൊത്തം ബാങ്ക് ചാർജ്";
		AppStrings.mDone = "DONE";
		AppStrings.mPayment = "പേയ്മെന്റ്";
		AppStrings.mCashWithdrawal = "പണം പിൻവലിക്കൽ";
		AppStrings.mFundTransfer = "ഫണ്ട് ട്രാൻസ്ഫർ";
		AppStrings.mDepositAmount = "നിക്ഷേപ തുക";
		AppStrings.mWithdrawalAmount = "പിൻവലിക്കൽ തുക";
		AppStrings.mTransactionMode = "ഇടപാട് മോഡ്";
		AppStrings.mBankLoanDisbursement = "ബാങ്ക് വായ്പ വിതരണം";
		AppStrings.mMemberToMember = "അംഗം മറ്റൊരു അംഗമായി";
		AppStrings.mDebitAccount = "ഡെബിറ്റ് അക്കൗണ്ട്";
		AppStrings.mSHG_SavingsAccount = "Shg സേവിംഗ്സ് അക്കൗണ്ട്";
		AppStrings.mDestinationMemberName = "ഉദ്ദിഷ്ടസ്ഥാന അംഗം";
		AppStrings.mSourceMemberName = "ഉറവിട അംഗത്തിന്റെ പേര്";
		AppStrings.mMobileNoUpdation = "മൊബൈൽ നമ്പർ അപ്ഡേഷൻ";
		AppStrings.mMobileNo = "മൊബൈൽ നമ്പർ";
		AppStrings.mMobileNoUpdateSuccessAlert = "മൊബൈൽ നമ്പർ വിജയകരമായി അപ്ഡേറ്റുചെയ്തു.";
		AppStrings.mAadhaarNoUpdation = "ആധാർ നമ്പർ നമ്പർ അപ്ഡിൻ";
		AppStrings.mSHGAccountNoUpdation = "ഷാ അക്കൗണ്ട് നമ്പർ നമ്പറുകൾ";
		AppStrings.mAccountNoUpdation = "അംഗത്വ അക്കൗണ്ട് നമ്പർ അപ്ഡേഷൻ";
		AppStrings.mAadhaarNo = "ആധാർ നമ്പർ ഒന്ന്";
		AppStrings.mBranchName = "ബ്രാഞ്ച് പേര്";
		AppStrings.mAadhaarNoUpdateSuccessAlert = "ആധാർ നമ്പർ വിജയകരമായി അപ്ഡേറ്റുചെയ്തു";
		AppStrings.mMemberAccNoUpdateSuccessAlert = "അംഗത്വ അക്കൗണ്ട് നമ്പർ വിജയകരമായി അപ്ഡേറ്റുചെയ്തു";
		AppStrings.mSHGAccNoUpdateSuccessAlert = "SHG അക്കൗണ്ട് നമ്പർ വിജയകരമായി അപ്ഡേറ്റുചെയ്തു";
		AppStrings.mCheckValidMobileNo = "സാധുവായ ഒരു മൊബൈൽ നമ്പർ നൽകുക";
		AppStrings.mInvalidAadhaarNo = "അസാധുവായ ആധാർ നമ്പർ";
		AppStrings.mCheckValidAadhaarNo = "സാധുവായ ഒരു ആധാർ നമ്പർ നൽകുക";
		AppStrings.mInvalidAccountNo = "അസാധുവായ അക്കൗണ്ട് നമ്പർ";
		AppStrings.mCheckAccountNumber = "ദയവായി നിങ്ങളുടെ അക്കൗണ്ട് നമ്പർ പരിശോധിക്കുക";
		AppStrings.mInvalidMobileNo = "മൊബൈൽ നമ്പർ അസാധുവാണ്";
		AppStrings.mUploadScheduleAlert = "CAN'T UPLOAD SCHEDULE VI IN OFFLINE MODE";
		AppStrings.mIsMobileNoRepeat = "മൊബൈൽ നമ്പറുകൾ ആവർത്തിച്ചു";
		AppStrings.mMobileNoUpdationNetworkCheck = "ഓഫ്ലൈൻ മോഡിൽ മൊബൈൽ നമ്പർ അപ്ഡേറ്റ് ചെയ്യാൻ കഴിയില്ല";
		AppStrings.mAadhaarNoNetworkCheck = "ഓഫ്ലൈൻ മോഡിൽ ആധാർ നമ്പർ അപ്ഡേറ്റുചെയ്യാൻ കഴിയില്ല";
		AppStrings.mShgAccNoNetworkCheck = "ഓഫ്ലൈൻ മോഡിൽ SHG അക്കൗണ്ട് നമ്പർ അപ്ഡേറ്റ് ചെയ്യാൻ കഴിയില്ല";
		AppStrings.mAccNoNetworkCheck = "ഓഫ്ലൈൻ മോഡിൽ അംഗത്വ അക്കൗണ്ട് നമ്പർ അപ്ഡേറ്റ് ചെയ്യാൻ കഴിയില്ല";
		AppStrings.mAccountNoRepeat = "അക്കൗണ്ട് നമ്പറുകൾ ആവർത്തിച്ചു";
		AppStrings.mCheckBankAndBranchNameSelected = "ദയവായി ബന്ധപ്പെട്ട അക്കൗണ്ട് നമ്പറിനായി തിരഞ്ഞെടുത്ത ബാങ്കും ബ്രാഞ്ചിന്റെ പേരും പരിശോധിക്കുക";
		AppStrings.mCheckNewLoanSancDate = "വായ്പ അനുവദിക്കാനുള്ള തീയതി തിരഞ്ഞെടുക്കുക";
		AppStrings.mCreditLinkageInfo = "ക്രെഡിറ്റ് ലിങ്കേജ് വിവരം";
		AppStrings.mCreditLinkage = "ക്രെഡിറ്റ് ലിങ്കേജ്";
		AppStrings.mCreditLinkage_content = "Number of times bank loans were taken by the SHG and repaid completely.";
		AppStrings.mCreditLinkageAlert = "ഓഫ്ലൈൻ മോഡിൽ ക്രെഡിറ്റ് ലിങ്കേജ് വിവരം അപ്ഡേറ്റ് ചെയ്യാൻ കഴിയില്ല";
	}
}
