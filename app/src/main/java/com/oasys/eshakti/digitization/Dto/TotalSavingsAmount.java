package com.oasys.eshakti.digitization.Dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class TotalSavingsAmount implements Serializable {

    private String savings;
    public String total_Savings;

}
