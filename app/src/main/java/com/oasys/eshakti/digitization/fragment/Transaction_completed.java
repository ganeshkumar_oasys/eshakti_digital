package com.oasys.eshakti.digitization.fragment;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class Transaction_completed extends Fragment implements View.OnClickListener {
    private View view;
    private TextView wallet_continue, requestid, currentcash;
    private TextView daily_date_time;
    private String currentDateStr;
    DateFormat df = new SimpleDateFormat("dd/MM/yyyy");


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_transaction_completed, container, false);
        inIt(view);
        return view;
    }

    private void inIt(View view) {

        try {
            Calendar calender = Calendar.getInstance();
            currentDateStr = df.format(calender.getTime());

            daily_date_time = (TextView) view.findViewById(R.id.daily_date_time);
            daily_date_time.setText(currentDateStr);
            wallet_continue = (TextView) view.findViewById(R.id.wallet_continue);
            wallet_continue.setOnClickListener(this);

            requestid = (TextView) view.findViewById(R.id.requestid);
            currentcash = (TextView) view.findViewById(R.id.currentcash);
            Bundle bundle = getArguments();
            requestid.setText(bundle.getString("ref_no"));
            currentcash.setText("₹" + bundle.getString("request_id"));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.wallet_continue:
                FragmentManager fm = getFragmentManager();
                fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                WalletDetails walletDetails = new WalletDetails();
                NewDrawerScreen.showFragment(walletDetails);
                break;

        }
    }
}
