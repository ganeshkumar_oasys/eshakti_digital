package com.oasys.eshakti.digitization.Dto.RequestDto;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.Data;

/**
 * Created by MuthukumarPandi on 12/20/2018.
 */
@Data
public class MemberloanRepayRequestDto implements Serializable {
    private String shgId;

    private String loanUuid;

    private String mobileDate;

    private String transactionDate;

    private String modeOfCash;

    private String userId;

    private ArrayList<RepaymentDetails> repaymentDetails;

}
