package com.oasys.eshakti.digitization.Dto;

import lombok.Data;

@Data
public class IncomeType {

    String id;
    String incomeType;
}
