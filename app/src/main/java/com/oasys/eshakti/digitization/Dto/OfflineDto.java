package com.oasys.eshakti.digitization.Dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class OfflineDto implements Serializable {

    String id,shgId,animatorId;
    String totalIncomeAmount,savAmount,vSavAmount,cashAtBank,cashInhand;
    String totalExpenseAmount;
    String grp_interest;
    String mem_amount;
    String memInterest;
    String btInterest;
    String btDeposit;
    String btWithdraw;
    String btExpense,btFromSavingAcId;
    String btToSavingAcId,shgSavingsAccountId;
    String btToLoanId;
    String btCharge;
    String transferCharge;
    String transferAmount;
    String memCurrentDue;
    String grp_repayment;
    String grp_intSubventionRecieved;
    String bt_intSubventionRecieved;
    String grp_charge;
    String ic_exp_Amount;
    String otherIncome;
    String totalSavAmount;
    String modeOCash;
    String txType;
    String txSubtype;

    String memberId;
    String memberName;
    String expenseTypeName;
    String expenseTypeId;
    String member_user_id;

    String bankId,loanId,OutStanding,loanTypeName;
    String fromAccountNumber,toAccountNumber,accountNumber,sSelectedBank,branchName;
    String sanctionDateTime,disbursementDateTime,modifiedDateTime,lastTransactionDateTime,groupFormationDate;


}
