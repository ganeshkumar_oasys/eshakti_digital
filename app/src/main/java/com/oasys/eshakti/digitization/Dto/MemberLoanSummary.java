package com.oasys.eshakti.digitization.Dto;

import java.io.Serializable;

import lombok.Data;

@Data
public  class MemberLoanSummary implements Serializable {

    private String amount;

    private String transactiondate;

    private String interest;
}
