package com.oasys.eshakti.digitization.Dto;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.Data;

@Data
public class MemberfinancialDetails implements Serializable {


    private ArrayList<VLoanTypes> Savings;

    private ArrayList<VLoanTypes> MFILoan;
    private ArrayList<VLoanTypes> MFILoan1;
    private ArrayList<VLoanTypes> MFILoan2;
    private ArrayList<VLoanTypes> MFILoan3;
    private ArrayList<VLoanTypes> MFILoan4;
    private ArrayList<VLoanTypes> MFILoan5;
    private ArrayList<VLoanTypes> MFILoan6;
    private ArrayList<VLoanTypes> MFILoan7;
    private ArrayList<VLoanTypes> MFILoan8;
    private ArrayList<VLoanTypes> MFILoan9;
    private ArrayList<VLoanTypes> MFILoan10;
    private ArrayList<VLoanTypes> MFILoan11;
    private ArrayList<VLoanTypes> MFILoan12;
    private ArrayList<VLoanTypes> MFILoan13;
    private ArrayList<VLoanTypes> MFILoan14;
    private ArrayList<VLoanTypes> MFILoan15;
    private ArrayList<VLoanTypes> MFILoan16;
    private ArrayList<VLoanTypes> MFILoan17;


    private ArrayList<VLoanTypes> CIF;
    private ArrayList<VLoanTypes> CIF1;
    private ArrayList<VLoanTypes> CIF2;
    private ArrayList<VLoanTypes> CIF3;
    private ArrayList<VLoanTypes> CIF4;
    private ArrayList<VLoanTypes> CIF5;
    private ArrayList<VLoanTypes> CIF6;
    private ArrayList<VLoanTypes> CIF7;
    private ArrayList<VLoanTypes> CIF8;
    private ArrayList<VLoanTypes> CIF9;
    private ArrayList<VLoanTypes> CIF10;
    private ArrayList<VLoanTypes> CIF11;
    private ArrayList<VLoanTypes> CIF12;
    private ArrayList<VLoanTypes> CIF13;
    private ArrayList<VLoanTypes> CIF14;
    private ArrayList<VLoanTypes> CIF15;
    private ArrayList<VLoanTypes> CIF16;


    private ArrayList<VLoanTypes> RFA;
    private ArrayList<VLoanTypes> RFA1;
    private ArrayList<VLoanTypes> RFA2;
    private ArrayList<VLoanTypes> RFA3;
    private ArrayList<VLoanTypes> RFA4;
    private ArrayList<VLoanTypes> RFA5;
    private ArrayList<VLoanTypes> RFA6;
    private ArrayList<VLoanTypes> RFA7;
    private ArrayList<VLoanTypes> RFA8;
    private ArrayList<VLoanTypes> RFA9;
    private ArrayList<VLoanTypes> RFA10;
    private ArrayList<VLoanTypes> RFA11;
    private ArrayList<VLoanTypes> RFA12;
    private ArrayList<VLoanTypes> RFA13;
    private ArrayList<VLoanTypes> RFA14;
    private ArrayList<VLoanTypes> RFA15;


    private ArrayList<VLoanTypes> CashCredit;
    private ArrayList<VLoanTypes> CashCredit1;
    private ArrayList<VLoanTypes> CashCredit2;
    private ArrayList<VLoanTypes> CashCredit3;
    private ArrayList<VLoanTypes> CashCredit4;
    private ArrayList<VLoanTypes> CashCredit5;
    private ArrayList<VLoanTypes> CashCredit6;
    private ArrayList<VLoanTypes> CashCredit7;
    private ArrayList<VLoanTypes> CashCredit8;
    private ArrayList<VLoanTypes> CashCredit9;
    private ArrayList<VLoanTypes> CashCredit10;
    private ArrayList<VLoanTypes> CashCredit11;
    private ArrayList<VLoanTypes> CashCredit12;
    private ArrayList<VLoanTypes> CashCredit13;
    private ArrayList<VLoanTypes> CashCredit14;
    private ArrayList<VLoanTypes> CashCredit15;


    private ArrayList<VLoanTypes> BulkLoan;
    private ArrayList<VLoanTypes> BulkLoan1;
    private ArrayList<VLoanTypes> BulkLoan2;
    private ArrayList<VLoanTypes> BulkLoan3;
    private ArrayList<VLoanTypes> BulkLoan4;
    private ArrayList<VLoanTypes> BulkLoan5;
    private ArrayList<VLoanTypes> BulkLoan6;
    private ArrayList<VLoanTypes> BulkLoan7;
    private ArrayList<VLoanTypes> BulkLoan8;
    private ArrayList<VLoanTypes> BulkLoan9;
    private ArrayList<VLoanTypes> BulkLoan10;
    private ArrayList<VLoanTypes> BulkLoan11;
    private ArrayList<VLoanTypes> BulkLoan12;
    private ArrayList<VLoanTypes> BulkLoan13;
    private ArrayList<VLoanTypes> BulkLoan14;
    private ArrayList<VLoanTypes> BulkLoan15;


    private ArrayList<VLoanTypes> TermLoan;

    private ArrayList<VLoanTypes> TermLoan1;
    private ArrayList<VLoanTypes> TermLoan2;
    private ArrayList<VLoanTypes> TermLoan3;
    private ArrayList<VLoanTypes> TermLoan4;
    private ArrayList<VLoanTypes> TermLoan5;
    private ArrayList<VLoanTypes> TermLoan6;
    private ArrayList<VLoanTypes> TermLoan7;
    private ArrayList<VLoanTypes> TermLoan8;
    private ArrayList<VLoanTypes> TermLoan9;
    private ArrayList<VLoanTypes> TermLoan10;
    private ArrayList<VLoanTypes> TermLoan11;
    private ArrayList<VLoanTypes> TermLoan12;
    private ArrayList<VLoanTypes> TermLoan13;
    private ArrayList<VLoanTypes> TermLoan14;
    private ArrayList<VLoanTypes> TermLoan15;


}
