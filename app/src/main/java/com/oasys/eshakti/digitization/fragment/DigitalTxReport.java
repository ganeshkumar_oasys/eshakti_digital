package com.oasys.eshakti.digitization.fragment;


import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.oasys.eshakti.digitization.Adapter.CustomItemAdapter;
import com.oasys.eshakti.digitization.Adapter.DigitizationReportAdapter;
import com.oasys.eshakti.digitization.Dto.EnquiryDetails;
import com.oasys.eshakti.digitization.Dto.HistoryDetailsDto;
import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.Dto.Transactiontype;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.activity.LoginActivity;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;
import com.oasys.eshakti.digitization.database.SHGTable;
import com.oasys.eshakti.digitization.model.RowItem;
import com.oasys.eshakti.digitization.views.MaterialSpinner;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class DigitalTxReport extends Fragment implements NewTaskListener {

    private String fromdateStr, todateStr, type, shgId, str_materialSpinner = "SELECT TRANSACTION TYPE";
    ;
    private NetworkConnection networkConnection;
    private Dialog mProgressDilaog;
    private View view;
    private RecyclerView recyclerViewmembersavingtransactionsummary;
    private LinearLayoutManager linearLayoutManager;
    private DigitizationReportAdapter digital_transactionAdapter;

    ArrayList<Transactiontype> transactiontype;
    ArrayList<EnquiryDetails> enquirydetails, selectedTypeArr;

    private MaterialSpinner txTypeSpinner;
    private String txtypeStr;
    private ArrayList<RowItem> arrStr;
    private CustomItemAdapter txAdapter;
    private ListOfShg shgDto;
    private TextView mGroupName, mCashInHand, mCashAtBank, mHeader;
    private TextView mAnimatorCash_in_hand;
    private TextView mWallet_balance;
    private TextView daily_date_time;
    private String currentDateStr;
    DateFormat df = new SimpleDateFormat("dd/MM/yyyy");


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.digitzation_summary_ly, container, false);
        init();
        return view;
    }

    private void init() {
        try {

            Calendar calender = Calendar.getInstance();
            currentDateStr = df.format(calender.getTime());

            daily_date_time = (TextView) view.findViewById(R.id.daily_date_time);
            daily_date_time.setText(currentDateStr);

            shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
            mGroupName = (TextView) view.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashInHand = (TextView) view.findViewById(R.id.cashinHand);
            mCashInHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashInHand.setTypeface(LoginActivity.sTypeface);

            mCashAtBank = (TextView) view.findViewById(R.id.cashatBank);
            mCashAtBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashAtBank.setTypeface(LoginActivity.sTypeface);
            /** UI Mapping **/

            mAnimatorCash_in_hand = (TextView) view.findViewById(R.id.Wallet_CIH);
            mWallet_balance = (TextView) view.findViewById(R.id.Wallet_balance);

            mHeader = (TextView) view.findViewById(R.id.fragmentHeader);
            mHeader.setText(AppStrings.mDigitization);
            mHeader.setTypeface(LoginActivity.sTypeface);

            Bundle bundle = getArguments();
            fromdateStr = bundle.getString("fromdate");
            todateStr = bundle.getString("todate");
            type = bundle.getString("type");
            shgId = bundle.getString("shgId");

            txTypeSpinner = (MaterialSpinner) view.findViewById(R.id.txType);
            txTypeSpinner.setHintColor(Color.BLACK);
            txTypeSpinner.setBaseColor(R.color.grey_400);
            txTypeSpinner.setFloatingLabelText(str_materialSpinner);
            txTypeSpinner.setPaddingSafe(10, 0, 10, 0);
            txTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    txtypeStr = ((RowItem) txTypeSpinner.getSelectedItem()).getTitle().toString();

                    if (i == 0) {
                        if (enquirydetails != null && enquirydetails.size() > 0) {
                            digital_transactionAdapter = new DigitizationReportAdapter(getActivity(), enquirydetails);
                            recyclerViewmembersavingtransactionsummary.setAdapter(digital_transactionAdapter);
                            digital_transactionAdapter.notifyDataSetChanged();
                        }
                    } else {
                        selectedTypeArr = new ArrayList<>();

                        if (enquirydetails != null && enquirydetails.size() > 0) {
                            for (EnquiryDetails enq : enquirydetails) {
                                if (txtypeStr.equals(enq.getTransation())) {
                                    selectedTypeArr.add(enq);
                                }
                            }


                            digital_transactionAdapter = new DigitizationReportAdapter(getActivity(), selectedTypeArr);
                            recyclerViewmembersavingtransactionsummary.setAdapter(digital_transactionAdapter);
                            digital_transactionAdapter.notifyDataSetChanged();
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            recyclerViewmembersavingtransactionsummary = (RecyclerView) view.findViewById(R.id.recyclerViewmembersavingtransactionsummary);
            linearLayoutManager = new LinearLayoutManager(getActivity());
            recyclerViewmembersavingtransactionsummary.setLayoutManager(linearLayoutManager);
            recyclerViewmembersavingtransactionsummary.setHasFixedSize(true);


            networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());

            if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {
                //  onTaskStarted();
                if (type.equals(NewDrawerScreen.DIGITIZATION)) {
                    HistoryDetailsDto hdto = new HistoryDetailsDto();
                    hdto.setFromDate(fromdateStr);

                    // hdto.setMerchantId(MySharedPreference.readString(getActivity(), MySharedPreference.MERCHANT_ID, ""));
       /* hdto.setMerchantId(MySharedPreference.readString(getActivity(),MySharedPreference.MERCHANT_ID,""));
        hdto.setWalletId(MySharedPreference.readString(getActivity(),MySharedPreference.WALLET_ID,""));*/
                    hdto.setToDate(todateStr);
                    hdto.setShgId(shgId);
                    hdto.setTransationtype("");
                    String sreqString = new Gson().toJson(hdto);
                    RestClient.getRestClient(DigitalTxReport.this).callRestWebService(Constants.BASE_URL + Constants.DIGITIZATION_SUMMARY, sreqString, getActivity(), ServiceType.DIGITIZATION_SUMMARY);

                }
            } else {
                Utils.showToast(getActivity(), "Network Not Available");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {

        if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
            mProgressDilaog.dismiss();
            mProgressDilaog = null;
        }
        switch (serviceType) {
            case DIGITIZATION_SUMMARY:

                if (result != null && result.length() > 0) {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    ResponseDto lrDto = gson.fromJson(result, ResponseDto.class);
                    int statusCode = lrDto.getStatusCode();
                    String message = lrDto.getMessage();
                    Log.d("response status", " " + statusCode);
                    if (statusCode == 400 || statusCode == 401 || statusCode == 403 || statusCode == 500 || statusCode == 503) {
                        // showMessage(statusCode);
                        Utils.showToast(getActivity(), message);
                        if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        }

                        if (statusCode == 503) {

                            Utils.showToast(getActivity(), AppStrings.service_unavailable);

                        }

                        // init();

                    } else if (statusCode == Utils.Success_Code) {
                        Utils.showToast(getActivity(), message);

                        // recyclerViewmembersavingtransactionsummary.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
                        transactiontype = lrDto.getResponseContent().getTransactionType();

                        if (transactiontype == null || enquirydetails == null) {
                           // Utils.showToast(getActivity(), "Empty data");
                            arrStr = new ArrayList<RowItem>();
                            arrStr.add(new RowItem(str_materialSpinner));

                            txAdapter = new CustomItemAdapter(getActivity(), arrStr);
                            txTypeSpinner.setAdapter(txAdapter);
                        }

                        if (transactiontype != null && transactiontype.size() > 0) {
                            int size = transactiontype.size();
                            arrStr = new ArrayList<RowItem>();
                            arrStr.add(new RowItem(str_materialSpinner));
                            for (int i = 0; i < size; i++) {
                                RowItem rowItem = new RowItem(transactiontype.get(i).getName());// SelectedGroupsTask.sBankNames.elementAt(i).toString());
                                arrStr.add(rowItem);
                            }
                            txAdapter = new CustomItemAdapter(getActivity(), arrStr);
                            txTypeSpinner.setAdapter(txAdapter);
                        }

                        enquirydetails = lrDto.getResponseContent().getEnquirydetails();

                    }
                    else
                    {
                        Utils.showToast(getActivity(), message);

                    }

                }

                break;
        }
    }


}
