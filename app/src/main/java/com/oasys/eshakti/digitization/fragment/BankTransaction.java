package com.oasys.eshakti.digitization.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.digitization.Adapter.CustomListAdapter;
import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.Dto.MemberList;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.Dto.ShgBankDetails;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;

import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.activity.LoginActivity;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;
import com.oasys.eshakti.digitization.database.BankTable;
import com.oasys.eshakti.digitization.database.MemberTable;
import com.oasys.eshakti.digitization.database.SHGTable;
import com.oasys.eshakti.digitization.model.ListItem;
import com.oasys.eshakti.digitization.model.RowItem;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;


import java.util.ArrayList;
import java.util.List;

public class BankTransaction extends Fragment implements AdapterView.OnItemClickListener, NewTaskListener {

    private View rootView;


    private TextView mGroupName, mCashInHand, mCashAtBank;
    public static String sSelected_BankName;
    public static ShgBankDetails sSelectedBank;
    List<RowItem> rowItems;

    int size;

    private ListView mListView;
    private List<ListItem> listItems;
    private CustomListAdapter mAdapter;
    int listImage;

    List<RowItem> rowItems_loanacc;

    int size_loanacc;

    private ListView mListView_loanacc;
    private List<ListItem> listItems_loanacc;
    private CustomListAdapter mAdapter_loanacc;
    int listImage_loanacc;
    private Dialog mProgressDilaog;
    TextView mSavingaccTextview, mLoanaccTextview;

    ArrayList<String> mBankName = new ArrayList<String>();
    ArrayList<String> mBankNameSendtoServer = new ArrayList<String>();
    ArrayList<String> mLoanId = new ArrayList<String>();
    ArrayList<String> mLoanType = new ArrayList<String>();
    ArrayList<String> mFixedDeposit = new ArrayList<String>();
    private ArrayList<MemberList> arrMem;
    private ListOfShg shgDto;
    private NetworkConnection networkConnection;
    private List<MemberList> memList;
    private int mSize;
    private ArrayList<ShgBankDetails> bankdetails;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_new_banktransaction_menulist, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mSize = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, "")).size();
        memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        bankdetails = BankTable.getBankName(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        arrMem = new ArrayList<>();
        init();

    }

    private void init() {
        try {
            mGroupName = (TextView) rootView.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
            mCashInHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashInHand.setTypeface(LoginActivity.sTypeface);

            mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
            mCashAtBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashAtBank.setTypeface(LoginActivity.sTypeface);

            String[] bankNames = new String[bankdetails.size()];
            size = bankNames.length;

            listItems = new ArrayList<ListItem>();
            mListView = (ListView) rootView.findViewById(R.id.fragment_List);
            listImage = R.drawable.ic_navigate_next_white_24dp;
            /* for checking */

            listItems_loanacc = new ArrayList<ListItem>();
            mListView_loanacc = (ListView) rootView.findViewById(R.id.fragment_List_loanacc);
            listImage_loanacc = R.drawable.ic_navigate_next_white_24dp;

            mSavingaccTextview = (TextView) rootView.findViewById(R.id.fragmentHeader);
            mLoanaccTextview = (TextView) rootView.findViewById(R.id.loanaccheader);

            mSavingaccTextview.setText(AppStrings.mBankTransSavingsAccount);
            mSavingaccTextview.setTypeface(LoginActivity.sTypeface);
            mLoanaccTextview.setText(AppStrings.mBankTransLoanAccount);
            mLoanaccTextview.setTypeface(LoginActivity.sTypeface);
            //
            for (int i = 0; i < size; i++) {
                if (!bankdetails.get(i).getBankId().equals("")
                        && !bankdetails.get(i).getBankName().equals("")) {

                    ListItem rowItem = new ListItem();
                    rowItem.setId(bankdetails.get(i).getBankId());
                    rowItem.setTitle(bankdetails.get(i).getBankName());
                    rowItem.setImageId(listImage);
                    listItems.add(rowItem);
                }


            }

            mAdapter = new CustomListAdapter(getActivity(), listItems);
            mListView.setAdapter(mAdapter);
            mListView.setOnItemClickListener(this);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
            mProgressDilaog.dismiss();
            mProgressDilaog = null;

        }
        switch (serviceType) {

            case FD_VALUE:

                try {
                    ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    String message = cdto.getMessage();
                    int statusCode = cdto.getStatusCode();
                    if (statusCode == Utils.Success_Code) {
                        Utils.showToast(getActivity(), message);

                        String amount = cdto.getResponseContent().getSavingsBalance().getCurrentBalance();
                        String fd = cdto.getResponseContent().getSavingsBalance().getCurrentFixedDeposit();
                        sSelectedBank.setCurrentBalance(amount);
                        sSelectedBank.setCurrentFixedDepositBalance(fd);

                        BankTransaction_DepositMenu fragment = new BankTransaction_DepositMenu();
                        NewDrawerScreen.showFragment(fragment);

                    } else {


                        if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        }
                        Utils.showToast(getActivity(), message);

                    }
                } catch (Exception e) {

                }
                break;

        }


    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        try {

            TextView textColor_Change = (TextView) view.findViewById(R.id.dynamicText);
            textColor_Change.setText(String.valueOf(bankdetails.get(position).getBankName()));
            textColor_Change.setTextColor(Color.rgb(251, 161, 108));

            sSelectedBank = bankdetails.get(position);

            for (int i = 0; i < size; i++) {

                if (sSelectedBank.getBankName().equals(bankdetails.get(i).getBankName().toString())) {
                    sSelected_BankName = bankdetails.get(i).getBankName();
                    if (networkConnection.isNetworkAvailable()) {
                        onTaskStarted();
                        RestClient.getRestClient(this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.BT_CB_FD_VALUE + sSelectedBank.getShgSavingsAccountId(), getActivity(), ServiceType.FD_VALUE);

                    }
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
