package com.oasys.eshakti.digitization.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListPopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.oasys.ediary.Activity.DiaryHome;
import com.oasys.eshakti.digitization.Dialogue.ChangeUrlDialog;
import com.oasys.eshakti.digitization.Dialogue.LanguageMenuAdapter;
import com.oasys.eshakti.digitization.Dialogue.TimerDialog;
import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.Dto.LoginDto;
import com.oasys.eshakti.digitization.Dto.MenuDataDto;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.Dto.TrainingsList;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.OasysUtils.AESCrypt;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.GetTypeface;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.RegionalConversion;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.database.LoginTable;
import com.oasys.eshakti.digitization.database.MemberTable;
import com.oasys.eshakti.digitization.database.SHGTable;
import com.oasys.eshakti.digitization.views.ButtonFlat;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.tutorialsee.lib.TastyToast;
import com.yesteam.eshakti.service.GPSTracker;
import com.yesteam.eshakti.utils.PrefUtils;
import com.yesteam.eshakti.views.RaisedButton;

import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LoginActivity extends BaseActivity implements View.OnClickListener, NewTaskListener, AdapterView.OnItemClickListener, LocationListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static final int READ_PHONE = 123;
    private static final int CALL_PHONE = 456;
    private EditText mUsername;
    private EditText mPassword;
    public static ResponseDto logResDto;
    public static final String TAG = LoginActivity.class.getSimpleName();
    RaisedButton mRaisedLoginButton;
    Dialog mProgressDialog;
    private TextView mSignUpdiffuser;
    public static Typeface sTypeface;
    private TextView mNewUser;
    private NetworkConnection networkConnection;
    private LoginDto ldto;
    private ListPopupWindow popupWindow;
    private String mLanguageLocalae;
    private TelephonyManager telephonyManager;
    private String imei;
    int PERMISSION_ALL = 1;
    private String[] PERMISSIONS = {
            android.Manifest.permission.READ_CONTACTS,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            android.Manifest.permission.CALL_PHONE, android.Manifest.permission.ACCESS_NETWORK_STATE, android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_WIFI_STATE,
            android.Manifest.permission.CAMERA
    };
    private GoogleApiClient mGoogleApiClient;
    private LocationManager locationManager;
    private boolean isNetworkEnabled = false;
    boolean canGetLocation = false;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute
    private Location location;
    private double latitude;
    private double longitude;
    private LocationRequest mLocationRequest;
    private PendingResult<LocationSettingsResult> result;
    final static int REQUEST_LOCATION = 199;
    GPSTracker gps;
    public static String sLatitude = "", sLongitude = "";
    static int REQUEST_CODE_SOME_FEATURES_PERMISSIONS = 100;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        networkConnection = NetworkConnection.getNetworkConnection(getApplicationContext());
        // MySharedPreference.writeString(this, MySharedPreference.ChangeUrl, "http://192.168.4.202:9821");
        //MySharedPreference.writeString(this, MySharedPreference.ChangeUrl, "https://eshdevapi.oasys.co");// TODO:: DEV
        // MySharedPreference.writeString(this, MySharedPreference.ChangeUrl, "https://eshtestapi.oasys.co"); // TODO:: TEST
        MySharedPreference.writeString(this, MySharedPreference.ChangeUrl, "https://uateshserver.oasys.co"); // TODO:: VAPT/UAT
        MySharedPreference.writeString(this, MySharedPreference.ACCESS_TOKEN, "");

        try {

//            callRequestPermission(PERMISSION_ALL, PERMISSIONS);

            mGoogleApiClient = new GoogleApiClient.Builder(LoginActivity.this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            mGoogleApiClient.connect();

            if ((String.valueOf(latitude) == null || String.valueOf(latitude).equals("0.0")) && (String.valueOf(longitude) == null || String.valueOf(longitude).equals("0.0")))
                turnGPSOn();


            try {

                if (MySharedPreference.getUserlangcode(this) != null) {
                    mLanguageLocalae = MySharedPreference.getUserlangcode(this);
                    if (MySharedPreference.readString(LoginActivity.this, MySharedPreference.LANG_CODE, "") != null && MySharedPreference.readString(LoginActivity.this, MySharedPreference.LANG_CODE, "").length() > 0)
                        GetTypeface.changeLanguage(LoginActivity.this, MySharedPreference.readString(LoginActivity.this, MySharedPreference.LANG_CODE, ""));
                    else {
                       /* if (mLanguageLocalae.equals("Hindi")) {
                            MySharedPreference.writeString(LoginActivity.this, MySharedPreference.LANG_CODE, "hi");
                        } else if (mLanguageLocalae.equals("Marathi")) {
                            MySharedPreference.writeString(LoginActivity.this, MySharedPreference.LANG_CODE, "mr");
                        } else if (mLanguageLocalae.equals("Kannada")) {
                            MySharedPreference.writeString(LoginActivity.this, MySharedPreference.LANG_CODE, "kn");
                        } else if (mLanguageLocalae.equals("Malayalam")) {
                            MySharedPreference.writeString(LoginActivity.this, MySharedPreference.LANG_CODE, "ml");
                        } else if (mLanguageLocalae.equals("Punjabi")) {
                            MySharedPreference.writeString(LoginActivity.this, MySharedPreference.LANG_CODE, "pa");
                        } else if (mLanguageLocalae.equals("Gujarathi")) {
                            MySharedPreference.writeString(LoginActivity.this, MySharedPreference.LANG_CODE, "gu");
                        } else if (mLanguageLocalae.equals("Bengali")) {
                            MySharedPreference.writeString(LoginActivity.this, MySharedPreference.LANG_CODE, "bn");
                        } else if (mLanguageLocalae.equals("Tamil")) {
                            MySharedPreference.writeString(LoginActivity.this, MySharedPreference.LANG_CODE, "ta");
                        } else if (mLanguageLocalae.equals("Assamese")) {
                            MySharedPreference.writeString(LoginActivity.this, MySharedPreference.LANG_CODE, "as");
                        }*/
                    }

                } else {
                    mLanguageLocalae = null;
                }

                sTypeface = GetTypeface.getTypeface(getApplicationContext(), mLanguageLocalae);

            } catch (Exception e) {
                e.printStackTrace();
            }


            mUsername = (EditText) findViewById(R.id.activity_username_edittext);
            // System.out.println("PREF USERNAME " + PrefUtils.getUsernameKey());

            String name = MySharedPreference.readString(LoginActivity.this, MySharedPreference.USERNAME, "");

            if (name != null && name.length() > 0) {
                System.out.println("User name Pref");
                mUsername.setText(name);
                mUsername.setEnabled(false);
                mUsername.setBackground(null);
            } else {
                System.out.println("User name No Pref");
                mUsername.setHint(RegionalConversion.getRegionalConversion(AppStrings.userName));
                mUsername.setTypeface(sTypeface);
            }

            mUsername.setHintTextColor(Color.LTGRAY);
            mUsername.addTextChangedListener(username_watcher);

            mPassword = (EditText) findViewById(R.id.activity_password_edittext);
            mPassword.setHint(RegionalConversion.getRegionalConversion(AppStrings.passWord));
            mPassword.setTypeface(sTypeface);
            mPassword.setHintTextColor(Color.LTGRAY);

            mPassword.setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    // TODO Auto-generated method stub

                    mPassword.setHint(null);
                    mPassword.setTypeface(null);
                    return false;
                }
            });

            mPassword.addTextChangedListener(watch);

            mRaisedLoginButton = (RaisedButton) findViewById(R.id.activity_login_button);
            mRaisedLoginButton.setText(RegionalConversion.getRegionalConversion(AppStrings.submit));
            mRaisedLoginButton.setTextSize(13);
            mRaisedLoginButton.setTypeface(sTypeface);
            mRaisedLoginButton.setOnClickListener(this);

            mSignUpdiffuser = (TextView) findViewById(R.id.activity_signin_diff);
            mSignUpdiffuser.setText(RegionalConversion.getRegionalConversion(AppStrings.signInAsDiffUser));
            mSignUpdiffuser.setTextSize(18);
            mSignUpdiffuser.setTypeface(sTypeface, Typeface.BOLD);
            mSignUpdiffuser.setOnClickListener(this);

            try {
                PackageManager manager = getPackageManager();
                PackageInfo info = manager.getPackageInfo(getPackageName(), 0);
                String version = info.versionName;

                mNewUser = (TextView) findViewById(R.id.activity_signup);
                mNewUser.setText(RegionalConversion.getRegionalConversion(AppStrings.mNewUserSignup));
                mNewUser.setTypeface(sTypeface);

                mNewUser.setVisibility(View.VISIBLE);
                mNewUser.setText(AppStrings.version + " " + version);
                mNewUser.setTextSize(15);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            mUsername.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    // TODO Auto-generated method stub
                    if (hasFocus) {
                        mUsername.setCompoundDrawablesWithIntrinsicBounds(R.drawable.usericon_focus, 0, 0, 0);
                        mPassword.setCompoundDrawablesWithIntrinsicBounds(R.drawable.passicon, 0, 0, 0);
                    }

                }
            });

            mPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    // TODO Auto-generated method stub
                    if (hasFocus) {

                        mPassword.setCompoundDrawablesWithIntrinsicBounds(R.drawable.passicon_focus, 0, 0, 0);
                        mUsername.setCompoundDrawablesWithIntrinsicBounds(R.drawable.usericon, 0, 0, 0);
                    }

                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    private void callRequestPermission(int PERMISSION_ALL, String[] PERMISSIONS) {
//        if (!hasPermissions(this, PERMISSIONS)) {
//            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
//        } else {
//            imei = getUniqueIMEIId(LoginActivity.this);
//        }
//    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    private void turnGPSOn() {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            getLocation();
        } else {
            showGPSDisabledAlertToUser();
            return;
        }
    }

    @SuppressLint("MissingPermission")
    public Location getLocation() {
        try {
            locationManager = (LocationManager) getApplicationContext()
                    .getSystemService(LOCATION_SERVICE);

            // getting GPS status
            boolean isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
            } else {
                this.canGetLocation = true;
                // First get location from Network Provider
                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    Log.d("Network", "Network");
                    if (locationManager != null) {
                        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();

                            //    new Locationtask().execute();
                            Log.e("LOCATION::::", "latitude: ::::::" + latitude + "longitude:::::" + longitude);
                        }
                    }
                }
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                        Log.d("GPS Enabled", "GPS Enabled");
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                                Log.e("isGPSEnabled::::", "latitude: ::::::" + latitude + "longitude:::::" + longitude);
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }


    private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                            }
                        });
        alertDialogBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }


//    public static boolean hasPermissions(Context context, String... permissions) {
//        if (context != null && permissions != null) {
//            for (String permission : permissions) {
//                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
//                    return false;
//                }
//            }
//        }
//        return true;
//    }


   /* public static String getUniqueIMEIId(Context context) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return "";
            }
            String imei = telephonyManager.getDeviceId();
            Log.e("imei", "=" + imei);
            if (imei != null && !imei.isEmpty()) {
                return imei;
            } else {
                return android.os.Build.SERIAL;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "not_found";
    }*/

/*
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 0:
                boolean isPerpermissionForAllGranted = false;
                if (grantResults.length > 0 && permissions.length == grantResults.length) {
                    for (int i = 0; i < permissions.length; i++) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            isPerpermissionForAllGranted = true;

                        } else {
                            isPerpermissionForAllGranted = false;
                        }
                    }

                    imei = getUniqueIMEIId(LoginActivity.this);
                    Log.e("value", "Permission Granted, Now you can use local drive .");
                } else {
                    isPerpermissionForAllGranted = true;
                    Log.e("value", "Permission Denied, You cannot use local drive .");
                }
                */
/*if (isPerpermissionForAllGranted) {
                    callRequestPermission(PERMISSION_ALL, PERMISSIONS);
                }*//*

                break;
        }



       */
/* switch (requestCode) {
            case READ_PHONE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    try {
                        imei = getUniqueIMEIId(LoginActivity.this);
                    } catch (Exception e) {
                        Log.e("onReqPermissionResult ", e.toString());
                    }

                } else {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, READ_PHONE);
                }
                break;
            case CALL_PHONE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                  *//*
*/
/*  try {
                        imei = getUniqueIMEIId(LoginActivity.this);
                    } catch (Exception e) {
                        Log.e("onReqPermissionResult ", e.toString());
                    }*//*
*/
/*

                } else {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, CALL_PHONE);
                }
                break;

        }*//*


    }
*/

    @Override
    protected void onStart() {
        super.onStart();


        if ((int) Build.VERSION.SDK_INT < 23) {

            TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            String mIMEINo = mngr.getDeviceId();
            EShaktiApplication.setIMEI_NO(mIMEINo);

            /** Gets the Location **/
            gps = new GPSTracker(LoginActivity.this);

            // check if GPS enabled
            if (gps.canGetLocation()) {

                double latitude = gps.getLatitude();
                double longitude = gps.getLongitude();

                sLatitude = String.valueOf(latitude);
                sLongitude = String.valueOf(longitude);

            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }

        } else if ((int) Build.VERSION.SDK_INT >= 23) {

            int hasLocationPermission = checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);
            int hasLocationCOARSEPermission = checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION);
            int hasReadPhoto = checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
            int haswritePhoto = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int hasReadPhonePermission = checkSelfPermission(Manifest.permission.READ_PHONE_STATE);
            int haswriteCemera = checkSelfPermission(Manifest.permission.CAMERA);
            List<String> permissions = new ArrayList<String>();
            if (hasLocationPermission != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
            }

            if (hasLocationCOARSEPermission != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
            }

            if (hasReadPhoto != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }

            if (haswritePhoto != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
            if (hasReadPhonePermission != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.READ_PHONE_STATE);
            }
            if (haswriteCemera != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.CAMERA);
            }

            if (!permissions.isEmpty()) {
                requestPermissions(permissions.toArray(new String[permissions.size()]),
                        REQUEST_CODE_SOME_FEATURES_PERMISSIONS);
            } else {

                TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

                String mIMEINo = null;

                try{
                    mIMEINo = mngr.getDeviceId();
                }catch (Exception e){
                    Log.i("print","Exception : "+e.getMessage());
                }

                //String mIMEINo = mngr.getDeviceId();

                EShaktiApplication.setIMEI_NO(mIMEINo);
                /** Gets the Location **/
                gps = new GPSTracker(LoginActivity.this);

                // check if GPS enabled
                if (gps.canGetLocation()) {

                    double latitude = gps.getLatitude();
                    double longitude = gps.getLongitude();

                    sLatitude = String.valueOf(latitude);
                    sLongitude = String.valueOf(longitude);

                } else {
                    // can't get location
                    // GPS or Network is not enabled
                    // Ask user to enable GPS/network in settings
                    gps.showSettingsAlert();
                }

            }

        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 100: {
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        Log.d("Permissions", "Permission Granted: " + permissions[i]);
                        if (permissions[i].equals("android.permission.ACCESS_FINE_LOCATION")) {

                            PrefUtils.setLocationPermission("true");
                        } else if (permissions[i].equals("android.permission.ACCESS_COARSE_LOCATION")) {
                            PrefUtils.setLocationPermission("true");
                        } else if (permissions[i].equals("android.permission.READ_EXTERNAL_STORAGE")) {

                            PrefUtils.setStoragePermission("true");
                        } else if (permissions[i].equals("android.permission.WRITE_EXTERNAL_STORAGE")) {
                            PrefUtils.setStoragePermission("true");
                        } else if (permissions[i].equals("android.permission.READ_PHONE_STATE")) {

                            PrefUtils.setPhonePermission("true");
                        } else if (permissions[i].equals("android.permission.CAMERA")) {

                            PrefUtils.setCameraPermission("true");
                        }
                    } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        Log.d("Permissions", "Permission Denied: " + permissions[i]);
                        if (permissions[i].equals("android.permission.ACCESS_FINE_LOCATION")) {
                            PrefUtils.setLocationPermission("false");
                        } else if (permissions[i].equals("android.permission.ACCESS_COARSE_LOCATION")) {
                            PrefUtils.setLocationPermission("false");
                        } else if (permissions[i].equals("android.permission.READ_EXTERNAL_STORAGE")) {
                            PrefUtils.setStoragePermission("false");
                        } else if (permissions[i].equals("android.permission.WRITE_EXTERNAL_STORAGE")) {
                            PrefUtils.setStoragePermission("false");
                        } else if (permissions[i].equals("android.permission.READ_PHONE_STATE")) {
                            PrefUtils.setPhonePermission("false");
                        } else if (permissions[i].equals("android.permission.CAMERA")) {
                            PrefUtils.setCameraPermission("false");
                        }
                    }

                }

                if (PrefUtils.getLocationPermission() != null) {
                    if (PrefUtils.getLocationPermission().equals("true")) {

                        TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        String mIMEINo = mngr.getDeviceId();

                        EShaktiApplication.setIMEI_NO(mIMEINo);
                        /** Gets the Location **/
                        gps = new GPSTracker(LoginActivity.this);

                        // check if GPS enabled
                        if (gps.canGetLocation()) {

                            double latitude = gps.getLatitude();
                            double longitude = gps.getLongitude();

                            sLatitude = String.valueOf(latitude);
                            sLongitude = String.valueOf(longitude);

                        } else {
                            // can't get location
                            // GPS or Network is not enabled
                            // Ask user to enable GPS/network in settings
                            gps.showSettingsAlert();
                        }

                    } else {
                        Toast.makeText(getApplicationContext(), "PLEASE PROVIDE A PERMISSION", Toast.LENGTH_LONG).show();
                        finish();
                    }
                }
            }
            break;
            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }
    TextWatcher username_watcher = new TextWatcher() {

        @Override
        public void afterTextChanged(Editable edit) {

            if (edit.length() == 10) {
                mPassword.requestFocus();
                mPassword.setCursorVisible(true);
            } else {

            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onTextChanged(CharSequence s, int a, int b, int c) {
            // TODO Auto-generated method stub

        }
    };

    TextWatcher watch = new TextWatcher() {

        @Override
        public void afterTextChanged(Editable edit) {

            if (edit.length() == 6) {
                LoginValidation();
            } else {
                Log.v("Edit Text Length", edit.length() + "");
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onTextChanged(CharSequence s, int a, int b, int c) {
            // TODO Auto-generated method stub

        }
    };


    public static String sUName = null, sPwd = null;

    private void LoginValidation() {
        // TODO Auto-generated method stub

        sUName = mUsername.getText().toString();
        sPwd = mPassword.getText().toString();
        String encrpt_string;

        if ((String.valueOf(latitude) == null || String.valueOf(latitude).equals("0.0")) && (String.valueOf(longitude) == null || String.valueOf(longitude).equals("0.0"))) {
            turnGPSOn();
        } else
            loginAuthentication();
    }

    private RestClient restClient;

    private void loginAuthentication() {

        ldto = new LoginDto();
        ldto.setUsername(String.valueOf(LoginActivity.sUName));
        //   dto.setUsername("testAnimator");
        try {
            AESCrypt.encrypt(String.valueOf(LoginActivity.sPwd));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d("hs", AESCrypt.encryptedValue);

        ldto.setPassword(AESCrypt.encryptedValue);
        ldto.setImeiNo(imei);
        ldto.setLat(latitude);
        ldto.setLong(longitude);

        if (getMacAddr() != null && getMacAddr().length() > 0 && !getMacAddr().equals("02:00:00:00:00:00")) {
            ldto.setMacAddress(getMacAddr());
        }

        if (ldto == null) {
            return;
        }

        String loginReqJson = new Gson().toJson(ldto);
        Log.e("Log in REQUEST ", " ::::::::: " + loginReqJson);

        if (mUsername.getText().toString().equals("") || mPassword.getText().toString().equals("")) {
            TastyToast.makeText(getApplicationContext(), "PROVIDE THE USER DETAILS", TastyToast.LENGTH_SHORT, TastyToast.WARNING);
        } else {
            if (networkConnection.isNetworkAvailable()) {
                EShaktiApplication.setOfflineTrans(false);
                onTaskStarted();
                RestClient.getRestClient(LoginActivity.this).callRestWebService(Constants.BASE_URL + Constants.LOGIN_URL, loginReqJson, LoginActivity.this, ServiceType.LOGIN);
            } else {

                Utils.showToast(this, "Check your network connectivity!");
          /*  LoginDto offlineLogin = LoginTable.getUserLogin(MySharedPreference.readString(LoginActivity.this, MySharedPreference.USERNAME, ""));
            EShaktiApplication.setOfflineTrans(true);
            if (offlineLogin != null) {
                if (offlineLogin.getPassword().equals(mPassword.getText().toString())) {
                    Intent intent_login = new Intent(LoginActivity.this,
                            SHGGroupActivity.class);
                    intent_login.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                            | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent_login);
                    overridePendingTransition(R.anim.right_to_left_in,
                            R.anim.right_to_left_out);
                }
            }*/


            }
        }
    }


    public static String getMacAddr() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(String.format("%02X:", b));
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
            return null;
        }
        return "02:00:00:00:00:00";
    }


    @Override
    public void onClick(View view) {

        try {
            switch (view.getId()) {
                case R.id.activity_login_button:
//                    LoginValidation();
                    int passlength = Integer.parseInt(String.valueOf(mPassword.getText().toString().length()));
//                    if(mPassword.getText().toString().equals("6"))
                    if(passlength==4 ||passlength==6)
                    {
                        LoginValidation();
                    }
                    else
                    {
                        TastyToast.makeText(getApplicationContext(), AppStrings.mPasswordErrorMsg,
                                TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                    }
                    break;


                case R.id.activity_signin_diff:
                    if (networkConnection.isNetworkAvailable()) {
                        final Dialog confirmationDialog = new Dialog(this);
                        LayoutInflater li = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View customView = li.inflate(R.layout.dialog_new_singin_diff, null, false);
                        final ButtonFlat mConYesButton;
                        final ButtonFlat mConNoButton;

                        TextView mConfirmationHeadertextview;
                        TextView mConfirmationTexttextview;

                        mConfirmationHeadertextview = (TextView) customView.findViewById(R.id.dialog_Title);
                        mConfirmationTexttextview = (TextView) customView.findViewById(R.id.dialog_Message);

                        mConYesButton = (ButtonFlat) customView.findViewById(R.id.fragment_ok_button_alert);
                        mConNoButton = (ButtonFlat) customView.findViewById(R.id.fragment_cancel_button_alert);

                        mConYesButton.setText(AppStrings.dialogOk);
                        mConNoButton.setTypeface(LoginActivity.sTypeface);
                        mConYesButton.setTypeface(LoginActivity.sTypeface);
                        mConNoButton.setText(AppStrings.dialogNo);

                        mConfirmationHeadertextview.setText(AppStrings.confirmation);
                        // mConfirmationHeadertextview.setTypeface(LoginActivity.sTypeface);

                        mConfirmationTexttextview.setText(AppStrings.mLoginDetailsDelete);
                        mConfirmationTexttextview.setTypeface(LoginActivity.sTypeface);
                        mConYesButton.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                // TODO Auto-generated method stub
                                try {
                                    SHGTable.deleteSHG(MySharedPreference.readString(LoginActivity.this, MySharedPreference.ANIMATOR_ID, ""));
                                    MemberTable.deleteMemberList(MySharedPreference.readString(LoginActivity.this, MySharedPreference.SHG_ID, ""));
                                    MySharedPreference.writeString(LoginActivity.this, MySharedPreference.USERNAME, "");
                                    MySharedPreference.writeString(LoginActivity.this, MySharedPreference.LANG, "");
                                    MySharedPreference.writeString(LoginActivity.this, MySharedPreference.LANG_ID, "");  //TODO:: dummy value

                                    if (MySharedPreference.readString(LoginActivity.this, MySharedPreference.SHG_ID, "") != null && MySharedPreference.readString(LoginActivity.this, MySharedPreference.SHG_ID, "").length() > 0) {
                                        ListOfShg shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(LoginActivity.this, MySharedPreference.SHG_ID, ""));
                                        if (shgDto != null && shgDto.getFFlag() != null) {
                                            if (!shgDto.getFFlag().equals("0"))
                                                SHGTable.updateSIgnInDiffUserDetails(shgDto.getShgId());
                                        }
                                    }
                                    MySharedPreference.writeString(LoginActivity.this, MySharedPreference.SHG_ID, "");
                                    MySharedPreference.writeBoolean(LoginActivity.this, MySharedPreference.SINGIN_DIFF, true);
                                    MySharedPreference.writeBoolean(LoginActivity.this, MySharedPreference.LOGOUT, false);


                                    if (confirmationDialog.isShowing() && confirmationDialog != null) {
                                        confirmationDialog.dismiss();
                                    }

                                    Thread.sleep(200);
                                } catch (InterruptedException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }

                                Intent intent = new Intent(LoginActivity.this, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                overridePendingTransition(R.anim.trans_right_in, R.anim.trans_left_out);
                                finish();

                            }
                        });

                        mConNoButton.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                // TODO Auto-generated method stub
                                if (confirmationDialog.isShowing() && confirmationDialog != null) {
                                    confirmationDialog.dismiss();
                                }
                            }
                        });


                        confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        confirmationDialog.setCanceledOnTouchOutside(false);
                        confirmationDialog.setContentView(customView);
                        confirmationDialog.setCancelable(true);
                        confirmationDialog.show();
                    } else {

                        TastyToast.makeText(getApplicationContext(), AppStrings.mCommonNetworkErrorMsg,
                                TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                    }

                    break;

                case R.id.activity_signup:
                    break;


            }

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskStarted() {
        mProgressDialog = AppDialogUtils.createProgressDialog(this);
        mProgressDialog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
        switch (serviceType) {
            case GETLANGUAGE:
                if (result != null && result.length() > 0) {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    final ResponseDto lrDto = gson.fromJson(result, ResponseDto.class);
                    String message = lrDto.getMessage();
                    int statusCode = lrDto.getStatusCode();


                    Log.d("response status", " " + statusCode);
                    if (statusCode == 400 || statusCode == 403 || statusCode == 500 || statusCode == 503) {
                        Utils.showToast(this, message);
                    } else if (statusCode == 401) {

                        Utils.showToast(this, message);
                    } else {
                        Utils.showToast(this, message);

                        if (lrDto != null && lrDto.getResponseContent().getLanguageList() != null && lrDto.getResponseContent().getLanguageList().size() > 0) {

                            final ArrayList<TrainingsList> langList = lrDto.getResponseContent().getLanguageList();

                            for (TrainingsList ls : langList) {
                                if (!ls.getName().equals("English")) {
                                    EShaktiApplication.setUser_RegLanguage(ls.getName());
                                    // EShaktiApplication.setUser_RegLanguage("Tamil");
                                }
                            }


                            final Dialog ChangeLanguageDialog = new Dialog(this);

                            LayoutInflater li = (LayoutInflater) this
                                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            final View dialogView = li.inflate(R.layout.dialog_new_choose_language, null,
                                    false);

                            TextView confirmationHeader = (TextView) dialogView
                                    .findViewById(R.id.chooseLanguageHeader);
                            confirmationHeader.setText(RegionalConversion
                                    .getRegionalConversion(AppStrings.chooseLanguage));
                            confirmationHeader.setTypeface(LoginActivity.sTypeface);
                            final RadioGroup radioGroup = (RadioGroup) dialogView
                                    .findViewById(R.id.radioLanguage);
                            RadioButton radioButton = (RadioButton) dialogView
                                    .findViewById(R.id.radioEnglish);
                            RadioButton radioButton_reg = (RadioButton) dialogView
                                    .findViewById(R.id.radioRegional);
                            radioButton.setText("English");
                            //   radioButton.setTypeface(LoginActivity.sTypeface);
                            if (EShaktiApplication.getUser_RegLanguage() != null) {
                                radioButton_reg.setVisibility(View.VISIBLE);
                                if (EShaktiApplication.getUser_RegLanguage().equals("Hindi")) {
                                    radioButton_reg.setText("हिंदी");
                                    Typeface typeface = Typeface.createFromAsset(getAssets(),
                                            "font/MANGAL.TTF");
                                    radioButton_reg.setTypeface(typeface);
                                } else if (EShaktiApplication.getUser_RegLanguage().equals("Marathi")) {
                                    radioButton_reg.setText("मराठी");
                                    Typeface typeface = Typeface.createFromAsset(getAssets(),
                                            "font/MANGALHindiMarathi.TTF");
                                    radioButton_reg.setTypeface(typeface);
                                } else if (EShaktiApplication.getUser_RegLanguage().equals("Kannada")) {
                                    radioButton_reg.setText("ಕನ್ನಡ");
                                    Typeface typeface = Typeface.createFromAsset(getAssets(),
                                            "font/tungaKannada.ttf");
                                    radioButton_reg.setTypeface(typeface);
                                } else if (EShaktiApplication.getUser_RegLanguage().equals("Malayalam")) {
                                    radioButton_reg.setText("മലയാളം");
                                    Typeface typeface = Typeface.createFromAsset(getAssets(),
                                            "font/MLKR0nttMalayalam.ttf");
                                    radioButton_reg.setTypeface(typeface);
                                } else if (EShaktiApplication.getUser_RegLanguage().equals("Punjabi")) {
                                    radioButton_reg.setText("ਪੰਜਾਬੀ ");
                                    Typeface typeface = Typeface.createFromAsset(getAssets(),
                                            "font/mangal-1361510185.ttf");
                                    radioButton_reg.setTypeface(typeface);
                                } else if (EShaktiApplication.getUser_RegLanguage().equals("Gujarathi")) {
                                    radioButton_reg.setText("ગુજરાતી");
                                    Typeface typeface = Typeface.createFromAsset(getAssets(),
                                            "font/shrutiGujarathi.ttf");
                                    radioButton_reg.setTypeface(typeface);
                                } else if (EShaktiApplication.getUser_RegLanguage().equals("Bengali")) {
                                    radioButton_reg.setText("বাঙ্গালী");
                                    Typeface typeface = Typeface.createFromAsset(getAssets(),
                                            "font/kalpurushBengali.ttf");
                                    radioButton_reg.setTypeface(typeface);
                                } else if (EShaktiApplication.getUser_RegLanguage().equals("Tamil")) {
                                    radioButton_reg.setText("தமிழ்");
                                    Typeface typeface = Typeface.createFromAsset(getAssets(),
                                            "font/TSCu_SaiIndira.ttf");
                                    radioButton_reg.setTypeface(typeface);
                                } else if (EShaktiApplication.getUser_RegLanguage().equals("Assamese")) {
                                    radioButton_reg.setText("Assamese");
                                    Typeface typeface = Typeface.createFromAsset(getAssets(),
                                            "font/KirtanUni_Assamese.ttf");
                                    radioButton_reg.setTypeface(typeface);
                                } else {
                                    radioButton_reg.setText("ENGLISH");//TODO::
                                    Typeface typeface = Typeface.createFromAsset(getAssets(),
                                            "font/Exo-Medium.ttf");
                                    radioButton_reg.setTypeface(typeface);
                                }
                            } else {
                                radioButton_reg.setVisibility(View.GONE);
                            }
                            // radioButton_reg.setTypeface(LoginActivity.sTypeface);

                            ButtonFlat okButton = (ButtonFlat) dialogView
                                    .findViewById(R.id.dialog_yes_button);
                            okButton.setText(
                                    RegionalConversion.getRegionalConversion(AppStrings.dialogOk));
                            okButton.setTypeface(LoginActivity.sTypeface);
                            okButton.setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated
                                    // method
                                    // stub
                                    int selectedId = radioGroup.getCheckedRadioButtonId();
                                    RadioButton radioLanguageButton = (RadioButton) dialogView
                                            .findViewById(selectedId);
                                    String mSelectedLang = radioLanguageButton.getText().toString();                                    // find the radiobutton by

                                    Log.v("On Selected language", mSelectedLang);
                                    try {
                                        /**
                                         * THE LANGUAGE VALUE INSERTS INTO PREFERENCE
                                         **/
                                        if (mSelectedLang.equals("हिंदी")) {
                                            mSelectedLang = "Hindi";
                                        } else if (mSelectedLang.equals("मराठी")) {
                                            mSelectedLang = "Marathi";
                                        } else if (mSelectedLang.equals("ಕನ್ನಡ")) {
                                            mSelectedLang = "Kannada";
                                        } else if (mSelectedLang.equals("മലയാളം")) {
                                            mSelectedLang = "Malayalam";
                                        } else if (mSelectedLang.equals("ਪੰਜਾਬੀ")) {
                                            mSelectedLang = "Punjabi";
                                        } else if (mSelectedLang.equals("ગુજરાતી")) {
                                            mSelectedLang = "Gujarathi";
                                        } else if (mSelectedLang.equals("বাঙ্গালী")) {
                                            mSelectedLang = "Bengali";
                                        } else if (mSelectedLang.equals("தமிழ்")) {
                                            mSelectedLang = "Tamil";
                                        } else if (mSelectedLang.equals("Assamese")) {
                                            mSelectedLang = "Assamese";
                                        }

                                        for (TrainingsList ls : langList) {
                                            if (mSelectedLang.equals("हिंदी")) {
                                                mSelectedLang = "Hindi";
                                            } else if (mSelectedLang.equals("मराठी")) {
                                                mSelectedLang = "Marathi";
                                            } else if (mSelectedLang.equals("ಕನ್ನಡ")) {
                                                mSelectedLang = "Kannada";
                                            } else if (mSelectedLang.equals("മലയാളം")) {
                                                mSelectedLang = "Malayalam";
                                            } else if (mSelectedLang.equals("ਪੰਜਾਬੀ")) {
                                                mSelectedLang = "Punjabi";
                                            } else if (mSelectedLang.equals("ગુજરાતી")) {
                                                mSelectedLang = "Gujarathi";
                                            } else if (mSelectedLang.equals("বাঙ্গালী")) {
                                                mSelectedLang = "Bengali";
                                            } else if (mSelectedLang.equals("தமிழ்")) {
                                                mSelectedLang = "Tamil";
                                            } else if (mSelectedLang.equals("Assamese")) {
                                                mSelectedLang = "Assamese";
                                            }
                                            if (ls.getName().equals(mSelectedLang)) {
                                                MySharedPreference.writeString(LoginActivity.this, MySharedPreference.LANG_ID, ls.getId());
                                            }
                                        }
                                        MySharedPreference.writeString(LoginActivity.this, MySharedPreference.LANG, mSelectedLang);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    LoginActivity.sTypeface = GetTypeface
                                            .getTypeface(LoginActivity.this, mSelectedLang);

                                    Intent intent_login = new Intent(LoginActivity.this,
                                            SHGGroupActivity.class);
                                    intent_login.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                                            | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent_login);
                                    overridePendingTransition(R.anim.right_to_left_in,
                                            R.anim.right_to_left_out);
                                    finish();

                      /*  RegionalserviceUtil.getRegionalService(LoginActivity.this,
                                mSelectedLang);*/

                                    // isLanguageSelection =
                                    // true;


                                    ChangeLanguageDialog.dismiss();


                                }
                            });

                            ChangeLanguageDialog.getWindow().setBackgroundDrawable(
                                    new ColorDrawable(Color.TRANSPARENT));
                            ChangeLanguageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            ChangeLanguageDialog.setCanceledOnTouchOutside(false);
                            ChangeLanguageDialog.setContentView(dialogView);
                            ChangeLanguageDialog.setCancelable(false);
                            ChangeLanguageDialog.show();
                        }

                    }

                }

                break;

            case LOGIN:
                if (result != null && result.length() > 0) {

                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    ResponseDto lrDto = gson.fromJson(result, ResponseDto.class);
                    String message = lrDto.getMessage();
                    int statusCode = lrDto.getStatusCode();
                    Log.d("response status", " " + statusCode);
                    if (statusCode == 400 || statusCode == 401 || statusCode == 403 || statusCode == 500 || statusCode == 503) {
                        Utils.showToast(this, message);
                        // showMessage(statusCode);
                        logResDto = null;
                    } else if (statusCode == Utils.Success_Code) {
                        EShaktiApplication.setOfflineTrans(false);
                        Utils.showToast(this, message);
                        if (sUName.length() > 0)
                            MySharedPreference.writeString(LoginActivity.this, MySharedPreference.USERNAME, sUName);
                     /*   if (sPwd.length() > 0)
                            MySharedPreference.writeString(LoginActivity.this, MySharedPreference.PASSWORD, sPwd);*/
                        // if (lrDto != null && lrDto.getResponseContent() != null && lrDto.getResponseContent().getUserId().length() > 0)
                        MySharedPreference.writeString(LoginActivity.this, MySharedPreference.ANIMATOR_ID, lrDto.getResponseContent().getUserId());
                        //   if (lrDto != null && lrDto.getResponseContent() != null && lrDto.getResponseContent().getName().length() > 0)
                        MySharedPreference.writeString(LoginActivity.this, MySharedPreference.ANIMATOR_NAME, "ANIMATOR");
                        MySharedPreference.writeString(LoginActivity.this, MySharedPreference.ACCESS_TOKEN, lrDto.getResponseContent().getAccessToken());
                        MySharedPreference.writeString(LoginActivity.this, MySharedPreference.REFRESH_TOKEN, lrDto.getResponseContent().getRefreshToken());
                        if (ldto.getUsername() != null && lrDto != null)
                            LoginTable.insertLoginDetails(lrDto, ldto);

                        if (MySharedPreference.readString(LoginActivity.this, MySharedPreference.LANG, "") == null || MySharedPreference.readString(LoginActivity.this, MySharedPreference.LANG, "").length() <= 0) {
                            if (lrDto != null && lrDto.getResponseContent().getUserId() != null) {
                                if (networkConnection.isNetworkAvailable()) {
                                    onTaskStarted();
                                    RestClient.getRestClient(LoginActivity.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.GET_LANGUAGE + lrDto.getResponseContent().getUserId(), LoginActivity.this, ServiceType.GETLANGUAGE);
                                }
                            }
                        } else {
                            Intent intent_login = new Intent(LoginActivity.this,
                                    SHGGroupActivity.class);
                            intent_login.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent_login);
                            overridePendingTransition(R.anim.right_to_left_in,
                                    R.anim.right_to_left_out);
                            finish();
                        }


                    } else {
                        Utils.showToast(this, message);
                    }

                } else {

                    //TODO ::  Static Login

                    // if (lrDto != null && lrDto.getResponseContent() != null && lrDto.getResponseContent().getUserId().length() > 0)
                    MySharedPreference.writeString(LoginActivity.this, MySharedPreference.ANIMATOR_ID, "d4ae5cb3-0a26-4832-8624-e2a5122387ce");
                    //   if (lrDto != null && lrDto.getResponseContent() != null && lrDto.getResponseContent().getName().length() > 0)
                    MySharedPreference.writeString(LoginActivity.this, MySharedPreference.ANIMATOR_NAME, "ANIMATOR");

                    Utils.showToast(this, "Login Failed!");
              /*  Intent intent_login = new Intent(LoginActivity.this,
                        SHGGroupActivity.class);
                intent_login.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent_login);
                overridePendingTransition(R.anim.right_to_left_in,
                        R.anim.right_to_left_out);
                finish();*/
                }
                break;
        }

    }

    public void showPopupMenu(View v) {
        PackageInfo packageInfo;
        try {
            packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            Log.e("PackageName", packageInfo.toString());
            List<MenuDataDto> menuDto = new ArrayList<>();
            //   menuDto.add(new MenuDataDto("Language", R.drawable.icon_language, new Util().unicodeToLocalLanguage(getResources().getString(R.string.languageSelection))));
            menuDto.add(new MenuDataDto("Change URL", R.drawable.icon_server, AppStrings.ChangeUrl));
            menuDto.add(new MenuDataDto("Time Out", R.drawable.icon_server, AppStrings.timeout));
            menuDto.add(new MenuDataDto("Version" + "  " + packageInfo.versionName, R.drawable.icon_server, "Version" + "  " + packageInfo.versionName));
            menuDto.add(new MenuDataDto("eDiary", R.drawable.icon_server, "eDiary"));
            popupWindow = new ListPopupWindow(this);
            ListAdapter adapter = new LanguageMenuAdapter(this, menuDto); // The view ids to map the data to
            popupWindow.setAnchorView(v);
            popupWindow.setAdapter(adapter);
            popupWindow.setWidth(400); // note: don't use pixels, use a dimen resource
            popupWindow.setOnItemClickListener(this); // the callback for when a list item is selected
            popupWindow.show();
        } catch (Exception e) {
            Log.e(LoginActivity.class.getSimpleName(), e.toString(), e);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view,
                            int position, /* Menu item click listener*/ long id) {
        popupWindow.dismiss();
        switch (position) {
            case 1:
                new TimerDialog(this).show();
                break;
            case 0:
                new ChangeUrlDialog(this).show();
                break;
            case 3:
                Intent intent = new Intent(LoginActivity.this, DiaryHome.class);
                startActivity(intent);
                break;
        }
    }


    @Override
    public void onLocationChanged(Location location) {
        turnGPSOn();
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(10 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);

        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        //...

                        getLocation();

                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    LoginActivity.this,
                                    REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        break;
                }
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("onActivityResult()", Integer.toString(resultCode));

        //final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
        switch (requestCode) {
            case REQUEST_LOCATION:
                switch (resultCode) {
                    case Activity.RESULT_OK: {
                        // All required changes were successfully made
                        Toast.makeText(LoginActivity.this, "High-Accuracy mode Turned On!", Toast.LENGTH_LONG).show();
                        break;
                    }
                    case Activity.RESULT_CANCELED: {
                        // The user was asked to change settings, but chose not to
                        Toast.makeText(LoginActivity.this, "High-Accuracy mode Turned Off!", Toast.LENGTH_LONG).show();
                        break;
                    }
                    default: {
                        break;
                    }
                }
                break;
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(LoginActivity.this, connectionResult.getErrorMessage(), Toast.LENGTH_LONG).show();
    }
}
