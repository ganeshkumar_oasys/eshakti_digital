package com.oasys.eshakti.digitization.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.oasys.eshakti.digitization.Dto.ResponseContents;
import com.oasys.eshakti.digitization.R;

import java.util.ArrayList;

public class EnrollFpAdapter extends BaseAdapter {


    private final ArrayList<ResponseContents> list;
    private final Context context;

    private class ViewHolder {
        TextView name;

    }

    public EnrollFpAdapter(Context context, ArrayList<ResponseContents> contList) {
        this.context = context;
        this.list = contList;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public ResponseContents getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return list.indexOf(getItem(i));
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        try {
            ViewHolder holder = null;
            LayoutInflater mInflater = (LayoutInflater) context
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (view == null || !(view.getTag() instanceof ViewHolder)) {
                view = mInflater.inflate(R.layout.tx_details_child_ly, null);
                holder = new ViewHolder();
                holder.name = (TextView) view.findViewById(R.id.name);
                view.setTag(holder);

            } else {
                holder = (ViewHolder) view.getTag();
            }

            ResponseContents listItem = list.get(i);
            holder.name.setText("" + String
                    .valueOf(listItem.getDesignation()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;

    }
}
