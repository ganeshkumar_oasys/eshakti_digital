package com.oasys.eshakti.digitization.OasysUtils;

import android.os.AsyncTask;
import android.os.Environment;


import com.oasys.eshakti.digitization.Service.NewTaskListener;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class DownloadPdfFileTask extends AsyncTask<String, String, String> {

    private final ServiceType type;
    private NewTaskListener mListener;
    String dest_file_path = "mobileapplication.pdf";
    int totalsize;

    public DownloadPdfFileTask(NewTaskListener listener, ServiceType type) {
        // TODO Auto-generated constructor stub
        this.mListener = listener;
        this.type = type;
    }

    @Override
    protected String doInBackground(String... params) {
        // TODO Auto-generated method stub
        String path = "";
        try {
            String downloadUrl = Constants.DOWNLOAD__PDF_URL;
            File pdfFile = downloadFile(downloadUrl);
            path = pdfFile.getAbsolutePath();


        } catch (Exception e) {
            e.printStackTrace();
        }
        return path;
    }

    File downloadFile(String dwnload_file_path) {
        File file = null;
        try {

            URL url = new URL(dwnload_file_path);
            HttpURLConnection urlConnection = (HttpURLConnection) url
                    .openConnection();

            urlConnection.setRequestMethod("GET");
            urlConnection.setDoOutput(true);

            urlConnection.connect();

            File SDCardRoot = Environment.getExternalStorageDirectory();
            file = new File(SDCardRoot, dest_file_path);

            totalsize = urlConnection.getContentLength();
            InputStream inputStream = new BufferedInputStream(url.openStream());
            OutputStream output = new FileOutputStream(file);

            byte[] buffer = new byte[1024 * 1024];
            int bufferLength = 0;

            while ((bufferLength = inputStream.read(buffer)) > 0) {
                output.write(buffer, 0, bufferLength);
            }
            output.close();

        } catch (final MalformedURLException e) {
            e.printStackTrace();

        } catch (final IOException e) {
            e.printStackTrace();

        } catch (final Exception e) {
            e.printStackTrace();

        }
        return file;
    }

    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();
        mListener.onTaskStarted();
    }

    @Override
    protected void onPostExecute(String result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);
        if (result == null) {
            result = Constants.EXCEPTION;
            mListener.onTaskFinished(result, type);
        } else {
            mListener.onTaskFinished(result, type);
        }
    }

}
