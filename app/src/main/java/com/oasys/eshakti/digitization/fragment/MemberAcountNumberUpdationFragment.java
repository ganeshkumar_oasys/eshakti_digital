package com.oasys.eshakti.digitization.fragment;


import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;

import com.oasys.eshakti.digitization.Adapter.DialogBranchAdapter;
import com.oasys.eshakti.digitization.Adapter.Dialog_AccountNoUpdationFragment;
import com.oasys.eshakti.digitization.Dto.BranchList;
import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.Dto.LoanBankDto;
import com.oasys.eshakti.digitization.Dto.MemberList;
import com.oasys.eshakti.digitization.Dto.RequestDto.MemberaccountListDto;
import com.oasys.eshakti.digitization.Dto.RequestDto.ShgSavingsAccountDTOList;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.GetSpanText;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.activity.LoginActivity;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;
import com.oasys.eshakti.digitization.database.BankBranchTable;
import com.oasys.eshakti.digitization.database.MemberTable;
import com.oasys.eshakti.digitization.database.SHGTable;
import com.oasys.eshakti.digitization.views.CustomHorizontalScrollView;
import com.oasys.eshakti.digitization.views.Get_EdiText_Filter;

import com.oasys.eshakti.digitization.views.RaisedButton;
import com.tutorialsee.lib.TastyToast;
import com.yesteam.eshakti.sqlite.db.model.BranchNameList;
import com.yesteam.eshakti.utils.AccountNumberValidationUtils;
import com.yesteam.eshakti.utils.Reset;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class MemberAcountNumberUpdationFragment extends Fragment implements NewTaskListener, View.OnClickListener {

    private TextView mGroupName, mCashinHand, mCashatBank, mHeader;
    private ListOfShg shgDto;
    private List<MemberList> memList;
    LinearLayout mMemberNameLayout;
    TextView mMemberName;
    private TableLayout mLeftHeaderTable, mRightHeaderTable, mLeftContentTable, mRightContentTable;
    private CustomHorizontalScrollView mHSRightHeader, mHSRightContent;
    private TextView mBranchName_values;
    public static List<TextView> sBankName_Fields = new ArrayList<TextView>();
    public static List<TextView> sBranch_Fields = new ArrayList<TextView>();
    public static List<EditText> sAccountNumber_Fields = new ArrayList<EditText>();
    private NetworkConnection networkConnection;
    private EditText mAccountNumber_values;
    private String bankid;
    public static ArrayList<BranchNameList> BranchList;
    ResponseDto cdto;
    ArrayList<String> mBankName_member = new ArrayList<String>();
    ArrayList<String> mBranchName_member = new ArrayList<String>();
    ArrayList<String> mAccountNumber_member = new ArrayList<String>();
    private LoanBankDto loanBankDto;
    private ArrayList<LoanBankDto> loanBankDtoArrayList;
    public List<LoanBankDto> bankList;
    public List<BranchList> branchList;
    public static List<BranchList> branchList_;
    private List<LoanBankDto> mBankNameWithoutDupList;
    int selId;
    public static HashMap<Integer, List<BranchList>> selected_bank_branches;
    private RaisedButton mSubmit_Raised_Button, mEdit_RaisedButton, mOk_RaisedButton;
    String[] accNoArr_value, bankNameArr_value, branchNameArr_value;
    Dialog confirmationDialog;
    public static String mSendToServer_AccNo = null;
    String[] accNoArr_server, bankName_server, branchName_server;
    boolean isError = false;
    boolean isServiceCall = false;
    ArrayList<ShgSavingsAccountDTOList> savingsAccountDTOLists;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_member_acount_number_updation, container, false);
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        selected_bank_branches = new HashMap<>();
        savingsAccountDTOLists = new ArrayList<>();


        sBankName_Fields.clear();
        sBranch_Fields.clear();
        sAccountNumber_Fields.clear();

        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        // banknames_name =new ArrayList<>();
        OnBankandBranchNameValues();

//       Log.e("bakvalues",""+bankList.size());
        try {
            mGroupName = (TextView) rootView.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
            mCashinHand.setText(com.oasys.eshakti.digitization.OasysUtils.AppStrings.cashinhand + shgDto.getCashInHand());
            mCashinHand.setTypeface(LoginActivity.sTypeface);

            mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
            mCashatBank.setText(com.oasys.eshakti.digitization.OasysUtils.AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashatBank.setTypeface(LoginActivity.sTypeface);

            mHeader = (TextView) rootView.findViewById(R.id.fragmentHeader);
            mHeader.setText("MEMBER ACCOUNT NUMBER UPDATION");
            mHeader.setTypeface(LoginActivity.sTypeface);

            mMemberNameLayout = (LinearLayout) rootView.findViewById(R.id.member_name_layout);
            mMemberName = (TextView) rootView.findViewById(R.id.member_name);

            mLeftHeaderTable = (TableLayout) rootView.findViewById(R.id.LeftHeaderTable);
            mRightHeaderTable = (TableLayout) rootView.findViewById(R.id.RightHeaderTable);
            mLeftContentTable = (TableLayout) rootView.findViewById(R.id.LeftContentTable);
            mRightContentTable = (TableLayout) rootView.findViewById(R.id.RightContentTable);

            mHSRightHeader = (CustomHorizontalScrollView) rootView.findViewById(R.id.rightHeaderHScrollView);
            mHSRightContent = (CustomHorizontalScrollView) rootView.findViewById(R.id.rightContentHScrollView);

            mHSRightHeader.setOnScrollChangedListener(new CustomHorizontalScrollView.onScrollChangedListener() {
                @Override
                public void onScrollChanged(int l, int t, int oldl, int oldt) {

                    mHSRightContent.scrollTo(l, 0);
                }
            });


            mHSRightContent.setOnScrollChangedListener(new CustomHorizontalScrollView.onScrollChangedListener() {
                @Override
                public void onScrollChanged(int l, int t, int oldl, int oldt) {
                    mHSRightHeader.scrollTo(l, 0);
                }
            });


            TableRow leftHeaderRow = new TableRow(getActivity());

            TableRow.LayoutParams lHeaderParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);

            TextView mMemberName_Header = new TextView(getActivity());
            mMemberName_Header.setText(AppStrings.memberName);
            mMemberName_Header.setTypeface(LoginActivity.sTypeface);
            mMemberName_Header.setTextColor(Color.WHITE);
            mMemberName_Header.setPadding(10, 5, 10, 5);
            mMemberName_Header.setLayoutParams(lHeaderParams);
            leftHeaderRow.addView(mMemberName_Header);

            mLeftHeaderTable.addView(leftHeaderRow);


            TableRow rightHeaderRow = new TableRow(getActivity());

            TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            contentParams.setMargins(10, 0, 10, 0);

            TextView mOutstanding_Header = new TextView(getActivity());
            mOutstanding_Header.setText(AppStrings.bankName);
            mOutstanding_Header.setTypeface(LoginActivity.sTypeface);
            mOutstanding_Header.setTextColor(Color.WHITE);
            mOutstanding_Header.setPadding(10, 5, 10, 5);
            mOutstanding_Header.setGravity(Gravity.LEFT);
            mOutstanding_Header.setLayoutParams(contentParams);
            mOutstanding_Header.setBackgroundResource(R.color.tableHeader);
            rightHeaderRow.addView(mOutstanding_Header);


            TableRow.LayoutParams POLParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            POLParams.setMargins(10, 0, 10, 0);

            TextView mPurposeOfLoan_Header = new TextView(getActivity());
            mPurposeOfLoan_Header
                    .setText(AppStrings.mBranchName);
            mPurposeOfLoan_Header.setTypeface(LoginActivity.sTypeface);
            mPurposeOfLoan_Header.setTextColor(Color.WHITE);
            mPurposeOfLoan_Header.setGravity(Gravity.RIGHT);
            mPurposeOfLoan_Header.setLayoutParams(POLParams);
            mPurposeOfLoan_Header.setPadding(25, 5, 10, 5);
            mPurposeOfLoan_Header.setBackgroundResource(R.color.tableHeader);
            rightHeaderRow.addView(mPurposeOfLoan_Header);


            TableRow.LayoutParams rHeaderParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            rHeaderParams.setMargins(10, 0, 10, 0);

            TextView mPL_Header = new TextView(getActivity());
            mPL_Header.setText(AppStrings.mAccountNumber);
            mPL_Header.setTypeface(LoginActivity.sTypeface);
            mPL_Header.setTextColor(Color.WHITE);
            mPL_Header.setPadding(10, 5, 10, 5);
            mPL_Header.setGravity(Gravity.CENTER);
            mPL_Header.setLayoutParams(rHeaderParams);
            mPL_Header.setBackgroundResource(R.color.tableHeader);
            rightHeaderRow.addView(mPL_Header);
            mRightHeaderTable.addView(rightHeaderRow);

            for (int j = 0; j < memList.size(); j++) {

                final int pos = j;

//                Member Name
                TableRow leftContentRow = new TableRow(getActivity());

                TableRow.LayoutParams leftContentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                leftContentParams.setMargins(5, 15, 5, 15);

                final TextView memberName_Text = new TextView(getActivity());
                memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
                        String.valueOf(memList.get(j).getMemberName())));
                memberName_Text.setTypeface(LoginActivity.sTypeface);
                memberName_Text.setTextColor(R.color.black);
                memberName_Text.setPadding(5, 5, 5, 5);
                memberName_Text.setLayoutParams(leftContentParams);
                memberName_Text.setWidth(200);
                memberName_Text.setSingleLine(true);
                memberName_Text.setEllipsize(TextUtils.TruncateAt.END);
                leftContentRow.addView(memberName_Text);
                mLeftContentTable.addView(leftContentRow);

//           Bank Name

                TableRow rightContentRow = new TableRow(getActivity());

                TableRow.LayoutParams rightContentTextviewParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                rightContentTextviewParams.setMargins(5, 15, 5, 15);
                final TextView mBankName_values = new TextView(getActivity());
                mBankName_values.setId(j);
                sBankName_Fields.add(mBankName_values);
                if (memList.get(j).getBankName() != null) {
                    mBankName_values.setText(GetSpanText.getSpanString(getActivity(), memList.get(j).getBankName()));
                } else
                    mBankName_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf("SELECT A BANK")));
                mBankName_values.setTypeface(LoginActivity.sTypeface);
                mBankName_values.setTextColor(R.color.black);
                mBankName_values.setPadding(5, 5, 5, 5);
                mBankName_values.setWidth(270);
                mBankName_values.setLayoutParams(rightContentTextviewParams);
                mBankName_values.setSingleLine(true);
                mBankName_values.setEllipsize(TextUtils.TruncateAt.END);

                mBankName_values.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
//                        bankname_position.put(pos,pos);
                        selId = mBankName_values.getId();
                        EShaktiApplication.setAccountNumberBankName(true);
                        FragmentManager fm = getActivity().getSupportFragmentManager();

                        if (BankBranchTable.bankname != null) {
                            Dialog_AccountNoUpdationFragment dialog = new Dialog_AccountNoUpdationFragment(getActivity(), BankBranchTable.bankname, pos, 0, mRightContentTable);
                            dialog.show(fm, "");
                        } else {
                            TastyToast.makeText(getActivity(), "Bank List Empty!", TastyToast.LENGTH_SHORT,
                                    TastyToast.ERROR);
                        }

                    }

                });

                rightContentRow.addView(mBankName_values);


//                Select Branch
                TableRow.LayoutParams rightContentTextviewParams1 = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                rightContentTextviewParams1.setMargins(10, 5, 10, 0);

                mBranchName_values = new TextView(getActivity());
                mBranchName_values.setId(j);
                sBranch_Fields.add(mBranchName_values);


                if (memList.get(j).getBranchName() != null) {
                    mBranchName_values.setText(GetSpanText.getSpanString(getActivity(), memList.get(j).getBranchName()));
                } else
                    mBranchName_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf("SELECT A BRANCH")));
                mBranchName_values.setTypeface(LoginActivity.sTypeface);
                mBranchName_values.setTextColor(R.color.black);
                mBranchName_values.setWidth(320);
                mBranchName_values.setPadding(5, 5, 5, 5);
                mBranchName_values.setLayoutParams(rightContentTextviewParams1);
                mBranchName_values.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {

                            Log.e("position_id", "" + pos);


                            EShaktiApplication.setAccountNumberBankName(true);
                            FragmentManager fm = getActivity().getSupportFragmentManager();
                            Log.e("selected_bank_branches", "" + selected_bank_branches.get(pos).toString());
                            if (selected_bank_branches.containsKey(pos)) {
                                List<String> branchnames = new ArrayList<>();
                                for (int i = 0; i < selected_bank_branches.get(pos).size(); i++) {
                                    Log.e("getBranchName", "" + selected_bank_branches.get(pos).get(i).getBranchName());
                                    branchnames.add(selected_bank_branches.get(pos).get(i).getBranchName());
                                }
                                Log.e("branchnames", "" + branchnames.toString());
                                DialogBranchAdapter dialog = new DialogBranchAdapter(getActivity(), branchnames, pos, 1, mRightContentTable);
                                dialog.show(fm, "");
                            } else {
                                List<String> no = new ArrayList<>();
                                no.add("NONE");
                                DialogBranchAdapter dialog = new DialogBranchAdapter(getActivity(), no, pos, 1, mRightContentTable);
                                dialog.show(fm, "");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();

                            Log.e("error", "" + e.toString());
                        }
                    }
                });
                rightContentRow.addView(mBranchName_values);


//                Account Number

                TableRow.LayoutParams rightContentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                rightContentParams.setMargins(10, 5, 10, 5);

                mAccountNumber_values = new EditText(getActivity());
                mAccountNumber_values.setId(j);
                sAccountNumber_Fields.add(mAccountNumber_values);
                mAccountNumber_values.setMaxLines(1);
                mAccountNumber_values.setSingleLine(true);
                mAccountNumber_values.setEllipsize(TextUtils.TruncateAt.END);
                mAccountNumber_values.setPadding(5, 5, 5, 5);
                mAccountNumber_values.setFilters(Get_EdiText_Filter.editText_AccNo_filter());
                mAccountNumber_values.setBackgroundResource(R.drawable.edittext_background);
                mAccountNumber_values.setLayoutParams(rightContentParams);
                mAccountNumber_values.setWidth(300);
                if (memList.get(j).getAccountNumber() != null)
                    mAccountNumber_values.setText(memList.get(j).getAccountNumber());

                rightContentRow.addView(mAccountNumber_values);
                mRightContentTable.addView(rightContentRow);

            }
            mSubmit_Raised_Button = (RaisedButton) rootView.findViewById(R.id.fragment_Submit_button);
            mSubmit_Raised_Button.setText(AppStrings.submit);
            mSubmit_Raised_Button.setTypeface(LoginActivity.sTypeface);
            mSubmit_Raised_Button.setOnClickListener(this);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return rootView;
    }

    private void OnBankandBranchNameValues() {

        if (networkConnection.isNetworkAvailable()) {
            onTaskStarted();
            RestClient.getRestClient(MemberAcountNumberUpdationFragment.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.PROFILE_MEMBER_ACCOUNT_NUMBER_UPDATION + shgDto.getDistrictId(), getActivity(), ServiceType.PROFILE_MEMBER_ACCOUNT_NUMBER_UPDATION_LIST);
        }
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.fragment_Submit_button:
                boolean _MasterisValid = true;
                boolean _IsEmpty = true;

                boolean _IsRepeatedAccountNumberNo = false;

//                sSelected_POL_Id = new String[memList.size()];

                accNoArr_value = new String[sAccountNumber_Fields.size()];
                bankNameArr_value = new String[sAccountNumber_Fields.size()];
                branchNameArr_value = new String[sAccountNumber_Fields.size()];

                accNoArr_server = new String[sAccountNumber_Fields.size()];
                bankName_server = new String[sAccountNumber_Fields.size()];
                branchName_server = new String[sAccountNumber_Fields.size()];


                for (int i = 0; i < memList.size(); i++) {
                    accNoArr_value[i] = sAccountNumber_Fields.get(i).getText().toString();
                    accNoArr_server[i] = sAccountNumber_Fields.get(i).getText().toString();

                    bankNameArr_value[i] = sBankName_Fields.get(i).getText().toString();
                    bankName_server[i] = sBankName_Fields.get(i).getText().toString();

                    branchNameArr_value[i] = sBranch_Fields.get(i).getText().toString();
                    branchName_server[i] = sBranch_Fields.get(i).getText().toString();

                    if (bankNameArr_value[i].equals("SELECT A BANK") || bankNameArr_value[i].equals("NONE") || (BankBranchTable.bankname_id == null || BankBranchTable.bankname_id.equals(""))) {
                        bankNameArr_value[i] = "No";
                        // BankBranchTable.bankname_id.add(i, "No");
                    }

                    if (branchNameArr_value[i].equals("SELECT A BRANCH") || branchNameArr_value[i].equals("NONE") || (BankBranchTable.branchnames_id == null || BankBranchTable.branchnames_id.equals(""))) {
                        branchNameArr_value[i] = "No";
                        // BankBranchTable.branchnames_id.add(i, "No");
                    }
                    if (accNoArr_value[i].equals("")) {
                        accNoArr_value[i] = "No";
                    }
                }

                mSendToServer_AccNo = Reset.reset(mSendToServer_AccNo);

                for (int i = 0; i < memList.size(); i++) {
                    boolean isValid = false;

                    if (!sAccountNumber_Fields.get(i).getText().toString().isEmpty()) {
                        _IsEmpty = false;
                    }
                    isValid = AccountNumberValidationUtils.validateAccountNumber(accNoArr_server[i]);

                    Log.e("Valid Position------------------" + i + "     Pos", isValid + "");
                    if (!isValid) {
                        sAccountNumber_Fields.get(i)
                                .setError(AppStrings.mInvalidAccountNo);
                        _MasterisValid = false;
                    }
                }
                    /*if (_MasterisValid && !_IsEmpty) {

                        for (int i = 0; i < memList.size(); i++) {

                            if (mAccountNumber_member.get(i).equals("")) {
                                mAccountNumber_member.set(i, "No");
                            }

                            if (mBankName_member.get(i).equals("SELECT A BANK") || mBankName_member.get(i).equals("NONE")) {
                                mBankName_member.set(i, "No");
                            }

                            if (mBranchName_member.get(i).equals("SELECT A BRANCH")
                                    || mBranchName_member.get(i).equals("NONE")) {
                                mBranchName_member.set(i, "No");
                            }

                            if (bankName_server[i].equals("SELECT A BANK") || bankName_server[i].equals("NONE")) {
                                bankName_server[i] = "";
                            }

                            if (branchName_server[i].equals("SELECT A BRANCH") || branchName_server[i].equals("NONE")) {
                                branchName_server[i] = "";
                            }

                            if (!bankName_server[i].equals("") && branchName_server[i].equals("")
                                    && accNoArr_server[i].equals("")) {
                                // isError = true;//

                            } else if (bankName_server[i].equals("") && !branchName_server[i].equals("")
                                    && accNoArr_server[i].equals("")) {
                                // isError = true;//

                            } else if (bankName_server[i].equals("") && branchName_server[i].equals("")
                                    && !accNoArr_server[i].equals("")) {
                                isError = true;

                            } else if (!bankName_server[i].equals("") && !branchName_server[i].equals("")
                                    && accNoArr_server[i].equals("")) {
                                // isError = true;//

                            } else if (bankName_server[i].equals("") && !branchName_server[i].equals("")
                                    && !accNoArr_server[i].equals("")) {
                                isError = true;

                            } else if (!bankName_server[i].equals("") && branchName_server[i].equals("")
                                    && !accNoArr_server[i].equals("")) {
                                isError = true;

                            }

                        }

                    }*/
                Log.e("SendToServer value  = ", mSendToServer_AccNo + "");

                for (int j = 0; j < memList.size(); j++) {
                    String _AccountNumber = accNoArr_server[j];
                    String _BranchName = branchName_server[j];
                    String _BankName = bankName_server[j];
                    for (int k = 0; k < memList.size(); k++) {
                        if (j != k) {
                            if (!accNoArr_server[k].isEmpty()) {
                                if (accNoArr_server[k].equals(_AccountNumber)
                                        && branchName_server[k].equals(_BranchName)
                                        && bankName_server[k].equals(_BankName)) {
                                    _IsRepeatedAccountNumberNo = true;
                                }
                            }

                        }

                    }

                }
                /*System.out.println("-------------------- Flag  =  " + isError + "");
                if (!isError) {
                    if (!_IsRepeatedAccountNumberNo) {
                        for (int i = 0; i < memList.size(); i++) {
                            mSendToServer_AccNo = mSendToServer_AccNo
                                    + String.valueOf(memList.get(i)) + "~"
                                    + mBankName_member.get(i).toString() + "~" + mBranchName_member.get(i).toString()
                                    + "~" + mAccountNumber_member.get(i).toString() + "~" + bankName_server[i] + "~"
                                    + branchName_server[i] + "~" + accNoArr_server[i] + "#";

                        }

                        callConfirmationDialog();
                    } else {
                        TastyToast.makeText(getActivity(), "ACCOUNT NUMBERS REPEATED", TastyToast.LENGTH_SHORT,
                                TastyToast.ERROR);
                    }

                } else {

                    isError = false;
                    TastyToast.makeText(getActivity(), AppStrings.mCheckBankAndBranchNameSelected,
                            TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                }*/
                callConfirmationDialog();
                break;

            case R.id.fragment_Edit:

                mSendToServer_AccNo = Reset.reset(mSendToServer_AccNo);
                mSubmit_Raised_Button.setClickable(true);
                confirmationDialog.dismiss();
                isServiceCall = false;
                break;
            case R.id.frag_Ok:
                confirmationDialog.dismiss();

                MemberaccountListDto memberaccountListDto = new MemberaccountListDto();
                memberaccountListDto.setShgSavingsAccountDTOList(savingsAccountDTOLists);
                String sreqString = new Gson().toJson(memberaccountListDto);

                if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {

                    RestClient.getRestClient(MemberAcountNumberUpdationFragment.this).callRestWebServiceForPutMethod(Constants.BASE_URL + Constants.MEMBER_ACCOUNT_NUMBER_UPDATION, sreqString, getActivity(), ServiceType.MEMBER_ACCOUNTNUMBER_UPDATION);
                } else {
                    Utils.showToast(getActivity(), "Network Not Available");
                }
                break;
            default:
                break;

        }

    }

    private void callConfirmationDialog() {
        savingsAccountDTOLists.clear();
        confirmationDialog = new Dialog(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_new_confirmation, null);
        dialogView.setLayoutParams(
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
        confirmationHeader.setText(AppStrings.confirmation);
        confirmationHeader.setTypeface(LoginActivity.sTypeface);

        TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

        TableRow header_row = new TableRow(getActivity());

        TableRow.LayoutParams headerParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
        headerParams.setMargins(10, 5, 10, 5);

        TextView bankName_header = new TextView(getActivity());
        bankName_header.setText(AppStrings.bankName);
        bankName_header.setTypeface(LoginActivity.sTypeface);
        bankName_header.setTextColor(R.color.black);
        bankName_header.setPadding(5, 5, 5, 5);
        bankName_header.setLayoutParams(headerParams);
        header_row.addView(bankName_header);

        TextView branchName_header = new TextView(getActivity());
        branchName_header.setText(AppStrings.mBranchName);
        branchName_header.setTypeface(LoginActivity.sTypeface);
        branchName_header.setTextColor(R.color.black);
        branchName_header.setPadding(5, 5, 5, 5);
        branchName_header.setLayoutParams(headerParams);
        header_row.addView(branchName_header);

        TextView accNo_header = new TextView(getActivity());
        accNo_header.setText(AppStrings.mAccountNumber);
        accNo_header.setTypeface(LoginActivity.sTypeface);
        accNo_header.setTextColor(R.color.black);
        accNo_header.setPadding(5, 5, 5, 5);
        accNo_header.setLayoutParams(headerParams);
        header_row.addView(accNo_header);

        confirmationTable.addView(header_row,
                new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        for (int i = 0; i < memList.size(); i++) {

            TableRow indv_SavingsRow = new TableRow(getActivity());

            @SuppressWarnings("deprecation")
            TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            contentParams.setMargins(10, 5, 10, 5);
            TextView bankName_Text = new TextView(getActivity());
            bankName_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(bankNameArr_value[i])));
            bankName_Text.setTypeface(LoginActivity.sTypeface);
            bankName_Text.setTextColor(R.color.black);
            bankName_Text.setPadding(5, 5, 5, 5);
            bankName_Text.setLayoutParams(contentParams);
            indv_SavingsRow.addView(bankName_Text);

            TextView branchName_Text = new TextView(getActivity());
            branchName_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(branchNameArr_value[i])));
            branchName_Text.setTextColor(R.color.black);
            branchName_Text.setPadding(5, 5, 5, 5);
            branchName_Text.setGravity(Gravity.RIGHT);
            branchName_Text.setLayoutParams(contentParams);
            indv_SavingsRow.addView(branchName_Text);

            TextView accNo_Text = new TextView(getActivity());
            accNo_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(accNoArr_value[i])));
            accNo_Text.setTextColor(R.color.black);
            accNo_Text.setPadding(5, 5, 5, 5);
            accNo_Text.setGravity(Gravity.RIGHT);
            accNo_Text.setLayoutParams(contentParams);
            indv_SavingsRow.addView(accNo_Text);

            confirmationTable.addView(indv_SavingsRow,
                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            ShgSavingsAccountDTOList Mddto = new ShgSavingsAccountDTOList();
            Mddto.setShgId(memList.get(i).getShgId());
            Mddto.setBankId(memList.get(i).getBankId());
            Mddto.setAccountNumber(accNoArr_value[i]);
//                Mddto.setBankNameId(BankBranchTable.bankList.get(i).getId());

            if (bankNameArr_value[i].equals("SELECT A BANK") || bankNameArr_value[i].equals("NONE") || (BankBranchTable.bankname_id == null || BankBranchTable.bankname_id.equals(""))) {
                Mddto.setBankNameId("No");
            } else {
                for (int i1 = 0; i1 < BankBranchTable.bankList.size(); i1++) {
                    if (bankNameArr_value[i].equals(BankBranchTable.bankList.get(i1).getName())) {
                        Mddto.setBankNameId(BankBranchTable.bankname_id.get(i1));
                    }
                }

            }

            List<BranchList> branchList = BankBranchTable.getBranchList(Mddto.getBankNameId());


            if (branchNameArr_value[i].equals("SELECT A BRANCH") || branchNameArr_value[i].equals("NONE") || (BankBranchTable.branchnames_id == null || BankBranchTable.branchnames_id.equals(""))) {
                Mddto.setBranchNameId("No");
            } else {

                for (int i1 = 0; i1 < branchList.size(); i1++) {
                    if (branchNameArr_value[i].equals(branchList.get(i1).getBranchName())) {
                        Mddto.setBranchNameId(branchList.get(i1).getBranchId());
                    }
                }
                //   Mddto.setBranchNameId(BankBranchTable.branchnames_id.get(i));
            }

            Mddto.setBankName(bankNameArr_value[i]);
            Mddto.setBranchName(branchNameArr_value[i]);
            Mddto.setMemberId(memList.get(i).getMemberId());


            if (accNoArr_value[i] == "No") {

            } else {
                savingsAccountDTOLists.add(Mddto);
            }

        }

        mEdit_RaisedButton = (RaisedButton) dialogView.findViewById(R.id.fragment_Edit);
        mEdit_RaisedButton.setText(AppStrings.edit);
        mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
        mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
        mEdit_RaisedButton.setOnClickListener(this);

        mOk_RaisedButton = (RaisedButton) dialogView.findViewById(R.id.frag_Ok);
        mOk_RaisedButton.setText(AppStrings.yes);
        mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
        mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
        mOk_RaisedButton.setOnClickListener(this);


        confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmationDialog.setCanceledOnTouchOutside(false);
        confirmationDialog.setContentView(dialogView);
        confirmationDialog.setCancelable(true);
        confirmationDialog.show();


        ViewGroup.MarginLayoutParams margin = (ViewGroup.MarginLayoutParams) dialogView.getLayoutParams();
        margin.leftMargin = 10;
        margin.rightMargin = 10;
        margin.topMargin = 10;
        margin.bottomMargin = 10;
        margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);
    }


    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {

        switch (serviceType) {

            case PROFILE_MEMBER_ACCOUNT_NUMBER_UPDATION_LIST:

                try {
                    cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    int statusCode = cdto.getStatusCode();
                    String message = cdto.getMessage();
                    if (statusCode == 400 || statusCode == 403 || statusCode == 500 || statusCode == 503 || statusCode == 409) {
                        // showMessage(statusCode);
                        Utils.showToast(getActivity(), message);
                    } else if (statusCode == 401) {

                        Log.e("Group Logout", "Logout Sucessfully");
                        AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                    } else if (statusCode == Utils.Success_Code) {

                        loanBankDtoArrayList = cdto.getResponseContent().getBankNamesList();

                        BankBranchTable.inserBranchBankData(cdto);
                        bankList = BankBranchTable.getBankname();

                    }
/*

                        for (int i = 0; i < loanBankDtoArrayList.size(); i++) {

                            loanBankDtoArrayList.get(i).getId();
                            loanBankDtoArrayList.get(i).getName();

                            Log.e("postion_id", "" + i + "====" + loanBankDtoArrayList.get(i).getId());
                            Log.e("postion_name", "" + i + "====" + loanBankDtoArrayList.get(i).getName());

                            for (int j = 0; j <loanBankDtoArrayList.get(i).getBranchList().size() ; j++) {

                                loanBankDtoArrayList.get(i).getBranchList().get(j).getBranchId();
                                loanBankDtoArrayList.get(i).getBranchList().get(j).getBranchName();
                                loanBankDtoArrayList.add(loanBankDto);

                                Log.e("postion_Branchid", "" + i + "====" + loanBankDtoArrayList.get(i).getBranchList().get(j).getBranchId());
                                Log.e("postion_Brancename", "" + i + "====" + loanBankDtoArrayList.get(i).getBranchList().get(j).getBranchName());

                                Log.d("localDb"," "+ DebugDB.getAddressLog());

                            }
*/


//                        }

//                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case MEMBER_ACCOUNTNUMBER_UPDATION:
                try {
                    cdto = new Gson().fromJson(result, ResponseDto.class);
                    String message = cdto.getMessage();
                    int statusCode = cdto.getStatusCode();
                    if (statusCode == 200) {
                        Utils.showToast(getActivity(), message);
                        FragmentManager fm = getFragmentManager();
                        fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        MainFragment mainFragment = new MainFragment();
                        Bundle bundles = new Bundle();
                        bundles.putString("Profile", MainFragment.Flag_Profile);
                        mainFragment.setArguments(bundles);
                        NewDrawerScreen.showFragment(mainFragment);
//                        MemberDrawerScreen.showFragment(new MainFragment());

                    } else {
                        if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                        }
                        Utils.showToast(getActivity(), message);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

        }
    }


}
