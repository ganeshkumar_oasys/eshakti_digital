package com.oasys.eshakti.digitization.Dto;

import java.util.ArrayList;

import lombok.Data;

@Data
public class SavingRequest {

    private String shgId;
    private String modeOfCash;
    private String shgSavingsAccountId,bankId;
    private ArrayList<MemberList> savingsAmount;
    private String transactionDate;
    private String mobileDate;
    private String transactionType;
  /*  private boolean isCash;*/
    private boolean isDigital;

    private String incomeTypeId;
    private String amount;
    private ArrayList<MemberList> memberSaving;
    private ArrayList<LoanDto> loanDetails;
    ArrayList<ExpensesTypeDtoList> expense;
    private String groupId;
    private String agentId;
    private String userId;

}
