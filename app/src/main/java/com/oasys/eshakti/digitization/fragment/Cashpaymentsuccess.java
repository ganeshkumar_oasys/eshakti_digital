package com.oasys.eshakti.digitization.fragment;


import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class Cashpaymentsuccess extends Fragment implements View.OnClickListener {
    private TextView wallet_final_continue, upi_info;
    private View view;
    private String pay_type;
    private ImageView wallet_succecc;
    private TextView msg;
    private TextView amt, tr_id, sts;
    private String amtStr, tr_idStr;
    private LinearLayout ly_ra, ly_rid, ly_sts;


    DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    private String currentDateStr;
    private TextView daily_date_time;
    private String status;


    public Cashpaymentsuccess() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_cashpaymentsuccess, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {

        try {

            Calendar calender = Calendar.getInstance();
            currentDateStr = df.format(calender.getTime());
            daily_date_time = (TextView) view.findViewById(R.id.daily_date_time);
            daily_date_time.setText(currentDateStr);

            Bundle bundle = getArguments();
            pay_type = bundle.getString("type");
            amtStr = bundle.getString("amt");
            tr_idStr = bundle.getString("tr_id");
            status = bundle.getString("status");
            wallet_final_continue = (TextView) view.findViewById(R.id.wallet_final_continue);
            wallet_succecc = (ImageView) view.findViewById(R.id.tick);
            upi_info = (TextView) view.findViewById(R.id.upi_info);
            msg = (TextView) view.findViewById(R.id.msg);
            amt = (TextView) view.findViewById(R.id.amt);
            tr_id = (TextView) view.findViewById(R.id.tr_id);
            sts = (TextView) view.findViewById(R.id.sts);
            ly_ra = (LinearLayout) view.findViewById(R.id.ly_ra);
            ly_rid = (LinearLayout) view.findViewById(R.id.ly_rid);
            ly_sts = (LinearLayout) view.findViewById(R.id.ly_sts);

            if (pay_type != null && pay_type.equals("upi")) {
                upi_info.setVisibility(View.GONE);
                msg.setText(getResources().getString(R.string.Mutiline_Textsuccess));
                amt.setVisibility(View.VISIBLE);
                amt.setText("₹" + amtStr);
                sts.setText(status);
                tr_id.setVisibility(View.VISIBLE);
                ly_ra.setVisibility(View.VISIBLE);
                ly_rid.setVisibility(View.VISIBLE);
                if (tr_idStr != null && tr_idStr.length() > 0)
                    tr_id.setText(tr_idStr);
                else
                    tr_id.setText("9003112205");

                wallet_succecc.setImageResource(R.drawable.tick_success);
            } else if (pay_type != null && pay_type.equals("upi")) {
                upi_info.setVisibility(View.VISIBLE);
            } else if (pay_type != null && pay_type.equals("cash_fail")) {
                upi_info.setVisibility(View.GONE);
                msg.setText(getResources().getString(R.string.Mutiline_Textfail));
                amt.setVisibility(View.VISIBLE);
                amt.setText("₹" + amtStr);
                sts.setText(status);
                tr_id.setVisibility(View.VISIBLE);
                ly_ra.setVisibility(View.GONE);
                //  ly_rid.setVisibility(View.GONE);
                ly_rid.setVisibility(View.VISIBLE);
                if (tr_idStr != null && tr_idStr.length() > 0)
                    tr_id.setText(tr_idStr);
                else
                    tr_id.setText("9003112205");
                wallet_succecc.setImageResource(R.drawable.icon_failure);
            }

            wallet_final_continue.setOnClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // init();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.wallet_final_continue:
                FragmentManager fm = getFragmentManager();
                fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                NewDrawerScreen.showFragment(new WalletDetails());
                break;
        }
    }


}
