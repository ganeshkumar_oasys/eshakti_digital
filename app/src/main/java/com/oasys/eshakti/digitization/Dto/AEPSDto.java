package com.oasys.eshakti.digitization.Dto;


import lombok.Data;

@Data
public class AEPSDto {

    String userId;
    String merchantId, walletId, amount;
    AadhaarPayDto aepsRequest;

    MemMasterDto memberMasterDTO;
    ShgMasDto shgMasterDTO;

}
