package com.oasys.eshakti.digitization.database;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.oasys.eshakti.digitization.Dto.ShgBankDetails;
import com.oasys.eshakti.digitization.EShaktiApplication;

import java.util.ArrayList;

public class BankTable {

    private static SQLiteDatabase database;
    private static DbHelper dataHelper;
    static Cursor cursor;

    public BankTable(Activity activity) {
        dataHelper = new DbHelper(activity);
    }

    public static void openDatabase() {
        dataHelper = new DbHelper(EShaktiApplication.getInstance());
        dataHelper.onOpen(database);
        database = dataHelper.getWritableDatabase();
    }

    public static void closeDatabase() {
        if (database != null && database.isOpen()) {
            database.close();
        }
    }

    public static void insertBankData(ShgBankDetails shgDto) {
        if (shgDto != null) {
            try {
                openDatabase();
                ContentValues values = new ContentValues();
                values.put(TableConstants.BANK_ID, (shgDto.getBankId() != null && shgDto.getBankId().length() > 0) ? shgDto.getBankId() : "");
                values.put(TableConstants.SHG_ID, (shgDto.getShgId() != null && shgDto.getShgId().length() > 0) ? shgDto.getShgId() : "");
                values.put(TableConstants.BANKNAME, (shgDto.getBankName() != null && shgDto.getBankName().length() > 0) ? shgDto.getBankName() : "");
                values.put(TableConstants.BANKNAME_ID, (shgDto.getBankNameId() != null && shgDto.getBankNameId().length() > 0) ? shgDto.getBankNameId() : "");
                values.put(TableConstants.BRANCHNAME, (shgDto.getBranchName() != null && shgDto.getBranchName().length() > 0) ? shgDto.getBranchName() : "");
                values.put(TableConstants.BRANCHNAME_ID, (shgDto.getBranchNameId() != null && shgDto.getBranchNameId().length() > 0) ? shgDto.getBranchNameId() : "");
                values.put(TableConstants.ACCOUNT_NO, (shgDto.getAccountNumber() != null && shgDto.getAccountNumber().length() > 0) ? shgDto.getAccountNumber() : "");
                values.put(TableConstants.CURR_BALANCE, (shgDto.getCurrentBalance() != null && shgDto.getCurrentBalance().length() > 0) ? shgDto.getCurrentBalance() : "");
                values.put(TableConstants.CURR_FD_BALANCE, (shgDto.getCurrentFixedDepositBalance() != null && shgDto.getCurrentFixedDepositBalance().length() > 0) ? shgDto.getCurrentFixedDepositBalance() : "");
                values.put(TableConstants.SHG_SB_AC_ID, (shgDto.getShgSavingsAccountId() != null && shgDto.getShgSavingsAccountId().length() > 0) ? shgDto.getShgSavingsAccountId() : "");
                database.insertWithOnConflict(TableName.TABLE_BANK_DETAILS, TableConstants.ACCOUNT_NO, values, SQLiteDatabase.CONFLICT_REPLACE);
            } catch (Exception e) {
                Log.e("insertSHGDetails", e.toString());
            } finally {
                closeDatabase();
            }
        }

    }

    public static ArrayList<ShgBankDetails> getBankName(String shgId) {

        ArrayList<ShgBankDetails> arrBankdetails= new ArrayList<>();

        try {
            openDatabase();

            String selectQuery = "SELECT * FROM " + TableName.TABLE_BANK_DETAILS + " where " + TableConstants.SHG_ID + " LIKE '" + shgId + "'";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {

                do {
                    ShgBankDetails bD = new ShgBankDetails();
                    bD.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    bD.setAccountNumber(cursor.getString(cursor.getColumnIndex(TableConstants.ACCOUNT_NO)));
                    bD.setBankName(cursor.getString(cursor.getColumnIndex(TableConstants.BANKNAME)));
                    bD.setBankId(cursor.getString(cursor.getColumnIndex(TableConstants.BANK_ID)));
                    bD.setBankNameId(cursor.getString(cursor.getColumnIndex(TableConstants.BANKNAME_ID)));
                    bD.setBranchName(cursor.getString(cursor.getColumnIndex(TableConstants.BRANCHNAME)));
                    bD.setBranchNameId(cursor.getString(cursor.getColumnIndex(TableConstants.BRANCHNAME_ID)));
                    bD.setShgSavingsAccountId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_SB_AC_ID)));
                    bD.setCurrentBalance(cursor.getString(cursor.getColumnIndex(TableConstants.CURR_BALANCE)));
                    bD.setCurrentFixedDepositBalance(cursor.getString(cursor.getColumnIndex(TableConstants.CURR_FD_BALANCE)));
                    arrBankdetails.add(bD);

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

        return arrBankdetails;
    }


}
