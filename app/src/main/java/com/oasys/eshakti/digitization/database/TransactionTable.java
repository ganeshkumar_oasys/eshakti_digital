package com.oasys.eshakti.digitization.database;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.oasys.eshakti.digitization.Dto.OfflineDto;
import com.oasys.eshakti.digitization.EShaktiApplication;

import java.util.ArrayList;

public class TransactionTable {

    private static SQLiteDatabase database;
    private static DbHelper dataHelper;
    static Cursor cursor;

    public TransactionTable(Activity activity) {
        dataHelper = new DbHelper(activity);
    }

    public static void openDatabase() {
        dataHelper = new DbHelper(EShaktiApplication.getInstance());
        dataHelper.onOpen(database);
        database = dataHelper.getWritableDatabase();
    }

    public static void closeDatabase() {
        if (database != null && database.isOpen()) {
            database.close();
        }
    }

    public static void insertTransSavingData(OfflineDto shgDto) {
        if (shgDto != null) {
            try {
                openDatabase();
                ContentValues values = new ContentValues();
                values.put(TableConstants.SHG_ID, (shgDto.getShgId() != null && shgDto.getShgId().length() > 0) ? shgDto.getShgId() : "");
                values.put(TableConstants.OFF_SAVING, (shgDto.getSavAmount() != null && shgDto.getSavAmount().length() > 0) ? shgDto.getSavAmount() : "");
                values.put(TableConstants.OFF_V_SAVING, (shgDto.getVSavAmount() != null && shgDto.getVSavAmount().length() > 0) ? shgDto.getVSavAmount() : "");
                values.put(TableConstants.OFF_MODE_OF_CASH, (shgDto.getModeOCash() != null && shgDto.getModeOCash().length() > 0) ? shgDto.getModeOCash() : "");
                values.put(TableConstants.OFF_LASTTRANSACTIONDATE_TIME, (shgDto.getLastTransactionDateTime() != null && shgDto.getLastTransactionDateTime().length() > 0) ? shgDto.getLastTransactionDateTime() : "");
                values.put(TableConstants.TRANSACTION_TYPE, (shgDto.getTxType() != null && shgDto.getTxType().length() > 0) ? shgDto.getTxType() : "");
                values.put(TableConstants.TLT_SAV_AMOUNT, (shgDto.getTotalSavAmount() != null && shgDto.getTotalSavAmount().length() > 0) ? shgDto.getTotalSavAmount() : "");
                values.put(TableConstants.CASH_AT_BANK, (shgDto.getCashAtBank() != null && shgDto.getCashAtBank().length() > 0) ? shgDto.getCashAtBank() : "");
                values.put(TableConstants.CASH_IN_HAND, (shgDto.getCashInhand() != null && shgDto.getCashInhand().length() > 0) ? shgDto.getCashInhand() : "");
                values.put(TableConstants.MEMBER_ID, (shgDto.getMemberId() != null && shgDto.getMemberId().length() > 0) ? shgDto.getMemberId() : "");
                values.put(TableConstants.MEMBER_NAME, (shgDto.getMemberName() != null && shgDto.getMemberName().length() > 0) ? shgDto.getMemberName() : "");
                values.put(TableConstants.MODIFIEDDATE, (shgDto.getModifiedDateTime() != null && shgDto.getModifiedDateTime().length() > 0) ? shgDto.getModifiedDateTime() : "");
                values.put(TableConstants.ANIMATOR_ID, (shgDto.getAnimatorId() != null && shgDto.getAnimatorId().length() > 0) ? shgDto.getAnimatorId() : "");

                database.insertWithOnConflict(TableName.TABLE_TRANSACTION, TableConstants.ACCOUNT_NO, values, SQLiteDatabase.CONFLICT_REPLACE);
            } catch (Exception e) {
                Log.e("insertSHGDetails", e.toString());
            } finally {
                closeDatabase();
            }
        }

    }


    public static void insertTransIncomeData(OfflineDto shgDto) {
        if (shgDto != null) {
            try {
                openDatabase();
                ContentValues values = new ContentValues();
                values.put(TableConstants.SHG_ID, (shgDto.getShgId() != null && shgDto.getShgId().length() > 0) ? shgDto.getShgId() : "");
                values.put(TableConstants.TLT_INCOME_AMOUNT, (shgDto.getTotalIncomeAmount() != null && shgDto.getTotalIncomeAmount().length() > 0) ? shgDto.getTotalIncomeAmount() : "");
                values.put(TableConstants.IC_EXP_AMOUNT, (shgDto.getIc_exp_Amount() != null && shgDto.getIc_exp_Amount().length() > 0) ? shgDto.getIc_exp_Amount() : "");
                values.put(TableConstants.OTHER_INCOME, (shgDto.getOtherIncome() != null && shgDto.getOtherIncome().length() > 0) ? shgDto.getOtherIncome() : "");
                values.put(TableConstants.OFF_MODE_OF_CASH, (shgDto.getModeOCash() != null && shgDto.getModeOCash().length() > 0) ? shgDto.getModeOCash() : "");
                values.put(TableConstants.OFF_LASTTRANSACTIONDATE_TIME, (shgDto.getLastTransactionDateTime() != null && shgDto.getLastTransactionDateTime().length() > 0) ? shgDto.getLastTransactionDateTime() : "");
                values.put(TableConstants.TRANSACTION_TYPE, (shgDto.getTxType() != null && shgDto.getTxType().length() > 0) ? shgDto.getTxType() : "");
                values.put(TableConstants.TRANSACTION_SUB_TYPE, (shgDto.getTxSubtype() != null && shgDto.getTxSubtype().length() > 0) ? shgDto.getTxSubtype() : "");
                values.put(TableConstants.CASH_AT_BANK, (shgDto.getCashAtBank() != null && shgDto.getCashAtBank().length() > 0) ? shgDto.getCashAtBank() : "");
                values.put(TableConstants.CASH_IN_HAND, (shgDto.getCashInhand() != null && shgDto.getCashInhand().length() > 0) ? shgDto.getCashInhand() : "");
                values.put(TableConstants.MEMBER_ID, (shgDto.getMemberId() != null && shgDto.getMemberId().length() > 0) ? shgDto.getMemberId() : "");
                values.put(TableConstants.MEMBER_NAME, (shgDto.getMemberName() != null && shgDto.getMemberName().length() > 0) ? shgDto.getMemberName() : "");
                values.put(TableConstants.MODIFIEDDATE, (shgDto.getModifiedDateTime() != null && shgDto.getModifiedDateTime().length() > 0) ? shgDto.getModifiedDateTime() : "");
                values.put(TableConstants.ANIMATOR_ID, (shgDto.getAnimatorId() != null && shgDto.getAnimatorId().length() > 0) ? shgDto.getAnimatorId() : "");
                values.put(TableConstants.BANK_ID, (shgDto.getBankId() != null && shgDto.getBankId().length() > 0) ? shgDto.getBankId() : "");
                values.put(TableConstants.BANK_SB_AC_ID, (shgDto.getShgSavingsAccountId() != null && shgDto.getShgSavingsAccountId().length() > 0) ? shgDto.getShgSavingsAccountId() : "");
                values.put(TableConstants.BANKNAME, (shgDto.getSSelectedBank() != null && shgDto.getSSelectedBank().length() > 0) ? shgDto.getSSelectedBank() : "");

                database.insertWithOnConflict(TableName.TABLE_TRANSACTION, TableConstants.ACCOUNT_NO, values, SQLiteDatabase.CONFLICT_REPLACE);
            } catch (Exception e) {
                Log.e("insertSHGDetails", e.toString());
            } finally {
                closeDatabase();
            }
        }

    }


    public static void insertTransExpenseData(OfflineDto shgDto) {
        if (shgDto != null) {
            try {
                openDatabase();
                ContentValues values = new ContentValues();
                values.put(TableConstants.SHG_ID, (shgDto.getShgId() != null && shgDto.getShgId().length() > 0) ? shgDto.getShgId() : "");
                values.put(TableConstants.EXP_TYPE_ID, (shgDto.getExpenseTypeId() != null && shgDto.getExpenseTypeId().length() > 0) ? shgDto.getExpenseTypeId() : "");
                values.put(TableConstants.EXP_TYPE_NAME, (shgDto.getExpenseTypeName() != null && shgDto.getExpenseTypeName().length() > 0) ? shgDto.getExpenseTypeName() : "");
                values.put(TableConstants.TLT_EXP_AMOUNT, (shgDto.getTotalExpenseAmount() != null && shgDto.getTotalExpenseAmount().length() > 0) ? shgDto.getTotalExpenseAmount() : "");
                values.put(TableConstants.IC_EXP_AMOUNT, (shgDto.getIc_exp_Amount() != null && shgDto.getIc_exp_Amount().length() > 0) ? shgDto.getIc_exp_Amount() : "");
                values.put(TableConstants.OFF_MODE_OF_CASH, (shgDto.getModeOCash() != null && shgDto.getModeOCash().length() > 0) ? shgDto.getModeOCash() : "");
                values.put(TableConstants.OFF_LASTTRANSACTIONDATE_TIME, (shgDto.getLastTransactionDateTime() != null && shgDto.getLastTransactionDateTime().length() > 0) ? shgDto.getLastTransactionDateTime() : "");
                values.put(TableConstants.TRANSACTION_TYPE, (shgDto.getTxType() != null && shgDto.getTxType().length() > 0) ? shgDto.getTxType() : "");
                values.put(TableConstants.TRANSACTION_SUB_TYPE, (shgDto.getTxSubtype() != null && shgDto.getTxSubtype().length() > 0) ? shgDto.getTxSubtype() : "");
                values.put(TableConstants.CASH_AT_BANK, (shgDto.getCashAtBank() != null && shgDto.getCashAtBank().length() > 0) ? shgDto.getCashAtBank() : "");
                values.put(TableConstants.CASH_IN_HAND, (shgDto.getCashInhand() != null && shgDto.getCashInhand().length() > 0) ? shgDto.getCashInhand() : "");
                values.put(TableConstants.MODIFIEDDATE, (shgDto.getModifiedDateTime() != null && shgDto.getModifiedDateTime().length() > 0) ? shgDto.getModifiedDateTime() : "");
                values.put(TableConstants.ANIMATOR_ID, (shgDto.getAnimatorId() != null && shgDto.getAnimatorId().length() > 0) ? shgDto.getAnimatorId() : "");
                database.insertWithOnConflict(TableName.TABLE_TRANSACTION, TableConstants.ACCOUNT_NO, values, SQLiteDatabase.CONFLICT_REPLACE);
            } catch (Exception e) {
                Log.e("insertSHGDetails", e.toString());
            } finally {
                closeDatabase();
            }
        }

    }


    public static void insertTransBTData(OfflineDto shgDto) {
        if (shgDto != null) {
            try {
                openDatabase();
                ContentValues values = new ContentValues();
                values.put(TableConstants.SHG_ID, (shgDto.getShgId() != null && shgDto.getShgId().length() > 0) ? shgDto.getShgId() : "");
                values.put(TableConstants.BT_F_BANK_SBAC_ID, (shgDto.getBtFromSavingAcId() != null && shgDto.getBtFromSavingAcId().length() > 0) ? shgDto.getBtFromSavingAcId() : "");
                values.put(TableConstants.BT_T_BANK_SBAC_ID, (shgDto.getBtToSavingAcId() != null && shgDto.getBtToSavingAcId().length() > 0) ? shgDto.getBtToSavingAcId() : "");
                values.put(TableConstants.OFF_F_ACCOUNT_NO, (shgDto.getFromAccountNumber() != null && shgDto.getFromAccountNumber().length() > 0) ? shgDto.getFromAccountNumber() : "");
                values.put(TableConstants.OFF_T_ACCOUNT_NO, (shgDto.getToAccountNumber() != null && shgDto.getToAccountNumber().length() > 0) ? shgDto.getToAccountNumber() : "");
                values.put(TableConstants.OFF_MODE_OF_CASH, (shgDto.getModeOCash() != null && shgDto.getModeOCash().length() > 0) ? shgDto.getModeOCash() : "");
                values.put(TableConstants.BT_EXPENSE, (shgDto.getBtExpense() != null && shgDto.getBtExpense().length() > 0) ? shgDto.getBtExpense() : "");
                values.put(TableConstants.BT_WITHDRAW, (shgDto.getBtWithdraw() != null && shgDto.getBtWithdraw().length() > 0) ? shgDto.getBtWithdraw() : "");
                values.put(TableConstants.BT_CHARGE, (shgDto.getBtCharge() != null && shgDto.getBtCharge().length() > 0) ? shgDto.getBtCharge() : "");
                values.put(TableConstants.BT_T_CHARGE, (shgDto.getTransferCharge() != null && shgDto.getTransferCharge().length() > 0) ? shgDto.getTransferCharge() : "");
                values.put(TableConstants.BT_T_Amount, (shgDto.getTransferAmount() != null && shgDto.getTransferAmount().length() > 0) ? shgDto.getTransferAmount() : "");
                values.put(TableConstants.BT_INT_SUB_RX, (shgDto.getBt_intSubventionRecieved() != null && shgDto.getBt_intSubventionRecieved().length() > 0) ? shgDto.getBt_intSubventionRecieved() : "");
                values.put(TableConstants.BT_INTEREST, (shgDto.getBtInterest() != null && shgDto.getBtInterest().length() > 0) ? shgDto.getBtInterest() : "");
                values.put(TableConstants.BT_DEPOSIT, (shgDto.getBtDeposit() != null && shgDto.getBtDeposit().length() > 0) ? shgDto.getBtDeposit() : "");
                values.put(TableConstants.BT_T_LOAN_ACID, (shgDto.getLoanId() != null && shgDto.getLoanId().length() > 0) ? shgDto.getLoanId() : "");
                values.put(TableConstants.OFF_ACCOUNT_NO, (shgDto.getAccountNumber() != null && shgDto.getAccountNumber().length() > 0) ? shgDto.getAccountNumber() : "");
                values.put(TableConstants.OFF_LOAN_OS, (shgDto.getOutStanding() != null && shgDto.getOutStanding().length() > 0) ? shgDto.getOutStanding() : "");
                values.put(TableConstants.OFF_LASTTRANSACTIONDATE_TIME, (shgDto.getLastTransactionDateTime() != null && shgDto.getLastTransactionDateTime().length() > 0) ? shgDto.getLastTransactionDateTime() : "");
                values.put(TableConstants.TRANSACTION_TYPE, (shgDto.getTxType() != null && shgDto.getTxType().length() > 0) ? shgDto.getTxType() : "");
                values.put(TableConstants.TRANSACTION_SUB_TYPE, (shgDto.getTxSubtype() != null && shgDto.getTxSubtype().length() > 0) ? shgDto.getTxSubtype() : "");
                values.put(TableConstants.CASH_AT_BANK, (shgDto.getCashAtBank() != null && shgDto.getCashAtBank().length() > 0) ? shgDto.getCashAtBank() : "");
                values.put(TableConstants.CASH_IN_HAND, (shgDto.getCashInhand() != null && shgDto.getCashInhand().length() > 0) ? shgDto.getCashInhand() : "");
                values.put(TableConstants.MODIFIEDDATE, (shgDto.getModifiedDateTime() != null && shgDto.getModifiedDateTime().length() > 0) ? shgDto.getModifiedDateTime() : "");
                values.put(TableConstants.ANIMATOR_ID, (shgDto.getAnimatorId() != null && shgDto.getAnimatorId().length() > 0) ? shgDto.getAnimatorId() : "");
                database.insertWithOnConflict(TableName.TABLE_TRANSACTION, TableConstants.ACCOUNT_NO, values, SQLiteDatabase.CONFLICT_REPLACE);
            } catch (Exception e) {
                Log.e("insertSHGDetails", e.toString());
            } finally {
                closeDatabase();
            }
        }

    }


    public static void insertTransMRData(OfflineDto shgDto) {
        if (shgDto != null) {
            try {
                openDatabase();
                ContentValues values = new ContentValues();
                values.put(TableConstants.SHG_ID, (shgDto.getShgId() != null && shgDto.getShgId().length() > 0) ? shgDto.getShgId() : "");
                values.put(TableConstants.MEMBER_ID, (shgDto.getMemberId() != null && shgDto.getMemberId().length() > 0) ? shgDto.getMemberId() : "");
                values.put(TableConstants.MEMBER_NAME, (shgDto.getMemberName() != null && shgDto.getMemberName().length() > 0) ? shgDto.getMemberName() : "");
                values.put(TableConstants.OFF_LOAN_ID, (shgDto.getLoanId() != null && shgDto.getLoanId().length() > 0) ? shgDto.getLoanId() : "");
                values.put(TableConstants.OFF_LOAN_ACC_ID, (shgDto.getAccountNumber() != null && shgDto.getAccountNumber().length() > 0) ? shgDto.getAccountNumber() : "");
                values.put(TableConstants.OFF_ACCOUNT_NO, (shgDto.getAccountNumber() != null && shgDto.getAccountNumber().length() > 0) ? shgDto.getAccountNumber() : "");
                values.put(TableConstants.MEM_AMOUNT, (shgDto.getMem_amount() != null && shgDto.getMem_amount().length() > 0) ? shgDto.getMem_amount() : "");
                values.put(TableConstants.MEM_INT_CDUE, (shgDto.getMemCurrentDue() != null && shgDto.getMemCurrentDue().length() > 0) ? shgDto.getMemCurrentDue() : "");
                values.put(TableConstants.MEM_INTEREST, (shgDto.getMemInterest() != null && shgDto.getMemInterest().length() > 0) ? shgDto.getMemInterest() : "");
                values.put(TableConstants.OFF_LOAN_OS, (shgDto.getOutStanding() != null && shgDto.getOutStanding().length() > 0) ? shgDto.getOutStanding() : "");
                values.put(TableConstants.OFF_MODE_OF_CASH, (shgDto.getModeOCash() != null && shgDto.getModeOCash().length() > 0) ? shgDto.getModeOCash() : "");
                values.put(TableConstants.OFF_LASTTRANSACTIONDATE_TIME, (shgDto.getLastTransactionDateTime() != null && shgDto.getLastTransactionDateTime().length() > 0) ? shgDto.getLastTransactionDateTime() : "");
                values.put(TableConstants.TRANSACTION_TYPE, (shgDto.getTxType() != null && shgDto.getTxType().length() > 0) ? shgDto.getTxType() : "");
                values.put(TableConstants.TRANSACTION_SUB_TYPE, (shgDto.getTxSubtype() != null && shgDto.getTxSubtype().length() > 0) ? shgDto.getTxSubtype() : "");
                values.put(TableConstants.CASH_AT_BANK, (shgDto.getCashAtBank() != null && shgDto.getCashAtBank().length() > 0) ? shgDto.getCashAtBank() : "");
                values.put(TableConstants.CASH_IN_HAND, (shgDto.getCashInhand() != null && shgDto.getCashInhand().length() > 0) ? shgDto.getCashInhand() : "");
                values.put(TableConstants.MODIFIEDDATE, (shgDto.getModifiedDateTime() != null && shgDto.getModifiedDateTime().length() > 0) ? shgDto.getModifiedDateTime() : "");
                values.put(TableConstants.ANIMATOR_ID, (shgDto.getAnimatorId() != null && shgDto.getAnimatorId().length() > 0) ? shgDto.getAnimatorId() : "");
                database.insertWithOnConflict(TableName.TABLE_TRANSACTION, TableConstants.ACCOUNT_NO, values, SQLiteDatabase.CONFLICT_REPLACE);
            } catch (Exception e) {
                Log.e("insertSHGDetails", e.toString());
            } finally {
                closeDatabase();
            }
        }

    }

    public static void insertTransGRData(OfflineDto shgDto) {
        if (shgDto != null) {
            try {
                openDatabase();
                ContentValues values = new ContentValues();
                values.put(TableConstants.SHG_ID, (shgDto.getShgId() != null && shgDto.getShgId().length() > 0) ? shgDto.getShgId() : "");
                values.put(TableConstants.GRP_CHARGE, (shgDto.getGrp_charge() != null && shgDto.getGrp_charge().length() > 0) ? shgDto.getGrp_charge() : "");
                values.put(TableConstants.GRP_INT_SUB_RX, (shgDto.getGrp_intSubventionRecieved() != null && shgDto.getGrp_intSubventionRecieved().length() > 0) ? shgDto.getGrp_intSubventionRecieved() : "");
                values.put(TableConstants.GRP_REPAYMENT, (shgDto.getGrp_repayment() != null && shgDto.getGrp_repayment().length() > 0) ? shgDto.getGrp_repayment() : "");
                values.put(TableConstants.GRP_INTEREST, (shgDto.getGrp_interest() != null && shgDto.getGrp_interest().length() > 0) ? shgDto.getGrp_interest() : "");
                values.put(TableConstants.OFF_LOAN_OS, (shgDto.getOutStanding() != null && shgDto.getOutStanding().length() > 0) ? shgDto.getOutStanding() : "");
                values.put(TableConstants.OFF_MODE_OF_CASH, (shgDto.getModeOCash() != null && shgDto.getModeOCash().length() > 0) ? shgDto.getModeOCash() : "");
                values.put(TableConstants.OFF_LOAN_ID, (shgDto.getLoanId() != null && shgDto.getLoanId().length() > 0) ? shgDto.getLoanId() : "");
                values.put(TableConstants.OFF_LOAN_ACC_ID, (shgDto.getAccountNumber() != null && shgDto.getAccountNumber().length() > 0) ? shgDto.getAccountNumber() : "");
                values.put(TableConstants.OFF_ACCOUNT_NO, (shgDto.getAccountNumber() != null && shgDto.getAccountNumber().length() > 0) ? shgDto.getAccountNumber() : "");
                values.put(TableConstants.OFF_LASTTRANSACTIONDATE_TIME, (shgDto.getLastTransactionDateTime() != null && shgDto.getLastTransactionDateTime().length() > 0) ? shgDto.getLastTransactionDateTime() : "");
                values.put(TableConstants.TRANSACTION_TYPE, (shgDto.getTxType() != null && shgDto.getTxType().length() > 0) ? shgDto.getTxType() : "");
                values.put(TableConstants.TRANSACTION_SUB_TYPE, (shgDto.getTxSubtype() != null && shgDto.getTxSubtype().length() > 0) ? shgDto.getTxSubtype() : "");
                values.put(TableConstants.CASH_AT_BANK, (shgDto.getCashInhand() != null && shgDto.getCashInhand().length() > 0) ? shgDto.getCashInhand() : "");
                values.put(TableConstants.CASH_IN_HAND, (shgDto.getCashAtBank() != null && shgDto.getCashAtBank().length() > 0) ? shgDto.getCashAtBank() : "");
                values.put(TableConstants.MODIFIEDDATE, (shgDto.getModifiedDateTime() != null && shgDto.getModifiedDateTime().length() > 0) ? shgDto.getModifiedDateTime() : "");
                values.put(TableConstants.ANIMATOR_ID, (shgDto.getAnimatorId() != null && shgDto.getAnimatorId().length() > 0) ? shgDto.getAnimatorId() : "");
                database.insertWithOnConflict(TableName.TABLE_TRANSACTION, TableConstants.ACCOUNT_NO, values, SQLiteDatabase.CONFLICT_REPLACE);
            } catch (Exception e) {
                Log.e("insertSHGDetails", e.toString());
            } finally {
                closeDatabase();
            }
        }

    }


    public static ArrayList<OfflineDto> getBankName(String shgId) {

        ArrayList<OfflineDto> arrBankdetails = new ArrayList<>();
        try {
            openDatabase();
            String selectQuery = "SELECT * FROM " + TableName.TABLE_BANK_DETAILS + " where " + TableConstants.SHG_ID + " LIKE '" + shgId + "'";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {

                do {
                    OfflineDto bD = new OfflineDto();
                    bD.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    bD.setAccountNumber(cursor.getString(cursor.getColumnIndex(TableConstants.ACCOUNT_NO)));

                    bD.setShgSavingsAccountId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_SB_AC_ID)));
                    arrBankdetails.add(bD);

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

        return arrBankdetails;
    }


}
