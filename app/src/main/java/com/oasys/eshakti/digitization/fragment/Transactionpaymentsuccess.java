package com.oasys.eshakti.digitization.fragment;


import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;

/**
 * A simple {@link Fragment} subclass.
 */
public class Transactionpaymentsuccess extends Fragment implements View.OnClickListener {
    private TextView wallet_final_continue, upi_info, amt, rtxnId;
    private View view;
    private String pay_type;
    private String txnAmount, retailerTxnId, terminalId;


    public Transactionpaymentsuccess() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.tx_success_ly, container, false);
        initView(view);
        return view;
    }


    private void initView(View view) {

        try {
            wallet_final_continue = (TextView) view.findViewById(R.id.wallet_final_continue);
            rtxnId = (TextView) view.findViewById(R.id.rtxnId);
            amt = (TextView) view.findViewById(R.id.amt);

              upi_info = (TextView) view.findViewById(R.id.upi_info);

            wallet_final_continue.setOnClickListener(this);
            upi_info.setOnClickListener(this);
            Bundle bundle = getArguments();
            txnAmount = bundle.getString("txnAmount");
            retailerTxnId = bundle.getString("retailerTxnId");
            terminalId = bundle.getString("terminalId");
            rtxnId.setText(retailerTxnId);
            amt.setText(txnAmount);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // init();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.wallet_final_continue:
                FragmentManager fm = getFragmentManager();
                fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                NewDrawerScreen.showFragment(new MainFragment());
                break;

            case R.id.upi_info:
                ViewTransactionDetails frag = new ViewTransactionDetails();
                Bundle d = new Bundle();
                d.putString("fromdate", "");
                d.putString("todate", "");
                frag.setArguments(d);
                NewDrawerScreen.showFragment(frag);
                break;
        }
    }


}
