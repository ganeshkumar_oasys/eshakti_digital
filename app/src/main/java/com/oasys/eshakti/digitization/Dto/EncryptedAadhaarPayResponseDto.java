package com.oasys.eshakti.digitization.Dto;

import android.database.Cursor;


import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class EncryptedAadhaarPayResponseDto {
    @SerializedName("statusCode")
    String statusCode;
    @SerializedName("message")
    String message;
    @SerializedName("responseContent")
     ResponseContent responseContent;

    public EncryptedAadhaarPayResponseDto() {

    }

    public EncryptedAadhaarPayResponseDto(Cursor cursor) {

    }
}
