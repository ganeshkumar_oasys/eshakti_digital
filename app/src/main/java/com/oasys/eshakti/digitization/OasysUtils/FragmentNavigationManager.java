package com.oasys.eshakti.digitization.OasysUtils;


import androidx.appcompat.app.AppCompatActivity;

import androidx.fragment.app.FragmentManager;

import com.oasys.eshakti.digitization.activity.NewDrawerScreen;

public class FragmentNavigationManager implements NavigationManager {
    private static FragmentNavigationManager mInstance;
    private static FragmentManager mFragmentManager;
    private NewDrawerScreen newDrawerScreen;


    public static FragmentNavigationManager getInstance(AppCompatActivity newDrawerScreen) {
        if (mInstance == null)
            mInstance = new FragmentNavigationManager();
        mInstance.configure(newDrawerScreen);
        return mInstance;

    }


    private void configure(AppCompatActivity newDrawerScreen) {
        newDrawerScreen = newDrawerScreen;
        mFragmentManager = newDrawerScreen.getSupportFragmentManager();
    }

    @Override
    public void showFragment(String title) {

    }
}
