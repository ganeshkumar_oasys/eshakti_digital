package com.oasys.eshakti.digitization.Dto;

import java.io.Serializable;

import lombok.Data;

/**
 * Created by MuthukumarPandi on 12/27/2018.
 */
@Data
public class MinutesOfMeeting implements Serializable {
    private String id;

    private String name;
}
