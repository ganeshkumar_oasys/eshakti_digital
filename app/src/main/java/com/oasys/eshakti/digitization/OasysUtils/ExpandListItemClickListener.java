package com.oasys.eshakti.digitization.OasysUtils;

import android.view.View;
import android.view.ViewGroup;

public interface ExpandListItemClickListener {

	void onItemClick(ViewGroup parent, View view, int position);
	void onItemClickVerification(ViewGroup parent, View view, int position);
}
