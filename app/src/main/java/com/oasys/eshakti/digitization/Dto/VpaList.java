package com.oasys.eshakti.digitization.Dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class VpaList implements Serializable {

    private String name;

    private String id;

    private String vpaNo;

    private String vpaVerified;

    private String sbaccountNo;
}
