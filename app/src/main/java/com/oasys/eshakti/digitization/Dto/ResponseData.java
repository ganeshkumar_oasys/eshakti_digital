package com.oasys.eshakti.digitization.Dto;

import lombok.Data;

@Data
public class ResponseData {
    private String message;
    private String fid_1;
    private String fid_2;
}
