package com.oasys.eshakti.digitization.Dto;

import android.database.Cursor;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class EncryptedAadhaarPayRequestDto {
    @SerializedName("data")
    String data;
    @SerializedName("deviceId")
    String deviceId;
    String userId;
    String agentId;
    String memberId;
    String shgId;



    public EncryptedAadhaarPayRequestDto() {

    }

    public EncryptedAadhaarPayRequestDto(Cursor cursor) {

    }
}
