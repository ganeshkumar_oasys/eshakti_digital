package com.oasys.eshakti.digitization.Adapter;

import java.io.Serializable;

import lombok.Data;

@Data
public class DashboardDto implements Serializable
{
    String name;
    int image;
}
