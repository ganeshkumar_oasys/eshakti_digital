package com.oasys.eshakti.digitization.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasys.eshakti.digitization.Dto.ResponseContents;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.activity.LoginActivity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Digitization_saving_Member_Details_Adapter extends RecyclerView.Adapter<Digitization_saving_Member_Details_Adapter.MemberHolderDetails> {

    private Context context;
    private ArrayList<ResponseContents> responseSaving_transaction_details;

    public Digitization_saving_Member_Details_Adapter(Context context, ArrayList<ResponseContents> responseSaving_transaction_details) {
        this.context = context;
        this.responseSaving_transaction_details = responseSaving_transaction_details;
    }

    @NonNull
    @Override
    public MemberHolderDetails onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.digitization_saving_details, parent, false);
        return new MemberHolderDetails(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MemberHolderDetails holder, int position) {

        holder.sav_membername.setText("MEMBER NAME:" +responseSaving_transaction_details.get(position).getMember_name());
        holder.sav_memberamount.setText("AMOUNT:"+responseSaving_transaction_details.get(position).getAmount());
        holder.sav_transactionno.setText("TRANSACTION NO:"+responseSaving_transaction_details.get(position).getTrans_ref());
        holder.sav_status.setText("STATUS :"+responseSaving_transaction_details.get(position).getError_name());
        holder.saving_digitization_transtype.setText("TRANSACTION TYPE :"+responseSaving_transaction_details.get(position).getTransation());
        DateFormat simple = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat df1 = new SimpleDateFormat("HH:mm:ss");
        Date d = new Date(Long.parseLong(responseSaving_transaction_details.get(position).getTransaction_request_date()));
        String dateStr = simple.format(d);
        String timeStr = df1.format(d);
        holder.saving_digitization_transdate.setText("TRANSACTION DATE :"+dateStr +" "+ timeStr);

    }

    @Override
    public int getItemCount() {
        return responseSaving_transaction_details.size();
    }

     class MemberHolderDetails extends RecyclerView.ViewHolder{

        TextView sav_membername,sav_memberamount,sav_transactionno,sav_status,saving_digitization_transtype,saving_digitization_transdate;

        public MemberHolderDetails(View itemView) {
            super(itemView);

            sav_membername = (TextView)itemView.findViewById(R.id.saving_digitization_Membername);
            sav_membername.setTypeface(LoginActivity.sTypeface);

            sav_memberamount = (TextView)itemView.findViewById(R.id.saving_digitization_amount);
            sav_memberamount.setTypeface(LoginActivity.sTypeface);

            sav_transactionno = (TextView)itemView.findViewById(R.id.saving_digitization_amount_transactionno);
            sav_transactionno.setTypeface(LoginActivity.sTypeface);

            sav_status = (TextView)itemView.findViewById(R.id.saving_digitization_status);
            sav_status.setTypeface(LoginActivity.sTypeface);

            saving_digitization_transtype = (TextView)itemView.findViewById(R.id.saving_digitization_transtype);
            saving_digitization_transtype.setTypeface(LoginActivity.sTypeface);

            saving_digitization_transdate = (TextView)itemView.findViewById(R.id.saving_digitization_transdate);
            saving_digitization_transdate.setTypeface(LoginActivity.sTypeface);
        }
    }
}
