package com.oasys.eshakti.digitization.Dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class GroupSavingsSummary implements Serializable {

    private String name;

    private String voluntrySavingsAmount;

    private String savingsAmount;
}
