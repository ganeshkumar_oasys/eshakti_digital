package com.oasys.eshakti.digitization.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.oasys.eshakti.digitization.Adapter.CustomTransactionListAdapter;
import com.oasys.eshakti.digitization.Dto.HistoryDetailsDto;
import com.oasys.eshakti.digitization.Dto.HistoryDto;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.activity.LoginActivity;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;
import com.oasys.eshakti.digitization.model.ListItem;
import com.oasys.eshakti.digitization.views.CustomHorizontalScrollView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class ViewRechargeDetails extends Fragment implements View.OnClickListener, NewTaskListener {

    private View view;
    private ListView list_details;
    private TextView f_date, t_date;
    private ArrayList<ListItem> listItems;
    String[] mLoanMenu;
    private CustomTransactionListAdapter mAdapter;

    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat df1 = new SimpleDateFormat("hh:mm:ss");
    private Date openinigDate;
    private Date currentDate;
    private String fromdateStr, todateStr, type, animator_id,user_id;
    private TableLayout mLeftHeaderTable, mRightHeaderTable, mLeftContentTable, mRightContentTable;
    private CustomHorizontalScrollView mHSRightHeader, mHSRightContent;
    private NetworkConnection networkConnection;
    private Dialog mProgressDilaog;
    private TextView stsStr, tx_text, tx_id, amttext, tx_type;
    private TextView daily_date_time, fragmentHeader;
    private String currentDateStr;
    private TextView ttypeText, ttypeText1;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.tx_rx_history, container, false);
        try {
            Bundle bundle = getArguments();
            fromdateStr = bundle.getString("fromdate");
            todateStr = bundle.getString("todate");
            type = bundle.getString("type");
            animator_id = bundle.getString("animatorid");

            networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());


            if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {
                //  onTaskStarted();
                if (type.equals(NewDrawerScreen.W_RC_REP)) {
                    HistoryDetailsDto hdto = new HistoryDetailsDto();
                    hdto.setFromDate(fromdateStr);

                    hdto.setUserId(MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, ""));
                    hdto.setAgentId(MySharedPreference.readString(getActivity(), MySharedPreference.AGENT_ID, ""));
       /* hdto.setMerchantId(MySharedPreference.readString(getActivity(),MySharedPreference.MERCHANT_ID,""));
        hdto.setWalletId(MySharedPreference.readString(getActivity(),MySharedPreference.WALLET_ID,""));*/
                    hdto.setToDate(todateStr);
                    String sreqString = new Gson().toJson(hdto);
                    RestClient.getRestClient(ViewRechargeDetails.this).callRestWebService(Constants.BASE_URL + Constants.WALLET_RX_HIS, sreqString, getActivity(), ServiceType.WALLET_RX_HIS);

                } else if (type.equals(NewDrawerScreen.W_WB_REP)) {

                    //TODO:: WALLET BALANCE
                    HistoryDetailsDto hdto = new HistoryDetailsDto();
                    hdto.setFromDate(fromdateStr);
                    hdto.setUserId(MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, ""));
                    hdto.setAgentId(MySharedPreference.readString(getActivity(), MySharedPreference.AGENT_ID, ""));

                    // hdto.setMerchantId(MySharedPreference.readString(getActivity(), MySharedPreference.MERCHANT_ID, ""));
       /* hdto.setMerchantId(MySharedPreference.readString(getActivity(),MySharedPreference.MERCHANT_ID,""));
        hdto.setWalletId(MySharedPreference.readString(getActivity(),MySharedPreference.WALLET_ID,""));*/
                    hdto.setToDate(todateStr);
                    String sreqString = new Gson().toJson(hdto);
                    RestClient.getRestClient(ViewRechargeDetails.this).callRestWebService(Constants.BASE_URL + Constants.WALLET_WB_HIS, sreqString, getActivity(), ServiceType.WALLET_WB_HIS);
                } /*else if (type.equals(MemberDrawerScreen.W_RX_REP)) {

                }*/ else if (type.equals(NewDrawerScreen.O_TR_REP)) {

                    //TODO:: WALLET BALANCE
                    HistoryDetailsDto hdto = new HistoryDetailsDto();
                    hdto.setFromDate(fromdateStr);
                    hdto.setUserId(MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, ""));
                    hdto.setAgentId(MySharedPreference.readString(getActivity(), MySharedPreference.AGENT_ID, ""));


                    // hdto.setMerchantId(MySharedPreference.readString(getActivity(), MySharedPreference.MERCHANT_ID, ""));
       /* hdto.setMerchantId(MySharedPreference.readString(getActivity(),MySharedPreference.MERCHANT_ID,""));
        hdto.setWalletId(MySharedPreference.readString(getActivity(),MySharedPreference.WALLET_ID,""));*/
                    hdto.setToDate(todateStr);
                    String sreqString = new Gson().toJson(hdto);
                    RestClient.getRestClient(ViewRechargeDetails.this).callRestWebService(Constants.BASE_URL + Constants.WALLET_TRS_HIS, sreqString, getActivity(), ServiceType.WALLET_TRS_HIS);
                } else if (type.equals(NewDrawerScreen.TRANS_DET)) {

                    //TODO:: WALLET BALANCE
                    HistoryDetailsDto hdto = new HistoryDetailsDto();
                    hdto.setFromDate(fromdateStr);
                    hdto.setUserId(MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, ""));
                    hdto.setAgentId(MySharedPreference.readString(getActivity(), MySharedPreference.AGENT_ID, ""));
                    // hdto.setMerchantId(MySharedPreference.readString(getActivity(), MySharedPreference.MERCHANT_ID, ""));
       /* hdto.setMerchantId(MySharedPreference.readString(getActivity(),MySharedPreference.MERCHANT_ID,""));
        hdto.setWalletId(MySharedPreference.readString(getActivity(),MySharedPreference.WALLET_ID,""));*/
                    hdto.setToDate(todateStr);
                    String sreqString = new Gson().toJson(hdto);
                    RestClient.getRestClient(ViewRechargeDetails.this).callRestWebService(Constants.BASE_URL + Constants.WALLET_TRS_DETAILS, sreqString, getActivity(), ServiceType.WALLET_TRS_DETAILS);
                } else if (type.equals(NewDrawerScreen.CASHIN_TRANS_HIS)) {

                    //TODO:: WALLET BALANCE
                    HistoryDetailsDto hdto = new HistoryDetailsDto();
                    hdto.setFromDate(fromdateStr);
                    hdto.setUserId(MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, ""));
                    hdto.setAgentId(MySharedPreference.readString(getActivity(), MySharedPreference.AGENT_ID, ""));
                    // hdto.setMerchantId(MySharedPreference.readString(getActivity(), MySharedPreference.MERCHANT_ID, ""));
       /* hdto.setMerchantId(MySharedPreference.readString(getActivity(),MySharedPreference.MERCHANT_ID,""));
        hdto.setWalletId(MySharedPreference.readString(getActivity(),MySharedPreference.WALLET_ID,""));*/
                    hdto.setToDate(todateStr);
                    hdto.setAnimatorNameId(animator_id);
                    String sreqString = new Gson().toJson(hdto);
                    RestClient.getRestClient(ViewRechargeDetails.this).callRestWebService(Constants.BASE_URL + Constants.WALLET_CASHIN_HAND_HISTORY, sreqString, getActivity(), ServiceType.WALLET_CASHIN_HAND_HISTORY);
                }else if (type.equals(NewDrawerScreen.DIGITIZATION)) {

                    //TODO:: WALLET BALANCE
                    HistoryDetailsDto hdto = new HistoryDetailsDto();
                    hdto.setFromDate(fromdateStr);
                    hdto.setUserId(MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, ""));
                    hdto.setAgentId(MySharedPreference.readString(getActivity(), MySharedPreference.AGENT_ID, ""));
                    // hdto.setMerchantId(MySharedPreference.readString(getActivity(), MySharedPreference.MERCHANT_ID, ""));
       /* hdto.setMerchantId(MySharedPreference.readString(getActivity(),MySharedPreference.MERCHANT_ID,""));
        hdto.setWalletId(MySharedPreference.readString(getActivity(),MySharedPreference.WALLET_ID,""));*/
                    hdto.setToDate(todateStr);
                    hdto.setAnimatorNameId(animator_id);
                    String sreqString = new Gson().toJson(hdto);
                    RestClient.getRestClient(ViewRechargeDetails.this).callRestWebService(Constants.BASE_URL + Constants.WALLET_CASHIN_HAND_HISTORY, sreqString, getActivity(), ServiceType.WALLET_CASHIN_HAND_HISTORY);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }


    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }


    private void init() {

        try {
            Calendar calender = Calendar.getInstance();
            currentDateStr = df.format(calender.getTime());
            daily_date_time = (TextView) view.findViewById(R.id.daily_date_time);
            daily_date_time.setText(currentDateStr);
            fragmentHeader = (TextView) view.findViewById(R.id.fragmentHeader);

            if (type.equals(NewDrawerScreen.W_RC_REP)) {
                fragmentHeader.setText(AppStrings.mRx_history);
            } else if (type.equals(NewDrawerScreen.W_WB_REP)) {
                fragmentHeader.setText(AppStrings.mWB_history);
            } else if (type.equals(NewDrawerScreen.O_TR_REP)) {
                fragmentHeader.setText(AppStrings.mO_t_history);
            } else if (type.equals(NewDrawerScreen.TRANS_DET)) {
                fragmentHeader.setText(AppStrings.mTR_Details);
            } else if (type.equals(NewDrawerScreen.CASHIN_TRANS_HIS)) {
                fragmentHeader.setText(AppStrings.mCash_I_H_History);
            }


            mLeftHeaderTable = (TableLayout) view.findViewById(R.id.mlr_LeftHeaderTable);
            mRightHeaderTable = (TableLayout) view.findViewById(R.id.mlr_RightHeaderTable);
            mLeftContentTable = (TableLayout) view.findViewById(R.id.mlr_LeftContentTable);
            mRightContentTable = (TableLayout) view.findViewById(R.id.mlr_RightContentTable);


            mHSRightHeader = (CustomHorizontalScrollView) view.findViewById(R.id.mlr_rightHeaderHScrollView);
            mHSRightContent = (CustomHorizontalScrollView) view.findViewById(R.id.mlr_rightContentHScrollView);
            //mloantype.setText(loantype + " - " + bankname);

            mHSRightHeader.setOnScrollChangedListener(new CustomHorizontalScrollView.onScrollChangedListener() {

                public void onScrollChanged(int l, int t, int oldl, int oldt) {
                    // TODO Auto-generated method stub

                    mHSRightContent.scrollTo(l, 0);

                }
            });

            mHSRightContent.setOnScrollChangedListener(new CustomHorizontalScrollView.onScrollChangedListener() {
                @Override
                public void onScrollChanged(int l, int t, int oldl, int oldt) {
                    // TODO Auto-generated method stub
                    mHSRightHeader.scrollTo(l, 0);
                }
            });


            TableRow leftHeaderRow = new TableRow(getActivity());
            TableRow.LayoutParams lHeaderParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);

            if (type.equals(NewDrawerScreen.W_WB_REP) || type.equals(NewDrawerScreen.W_RC_REP)) {

                TextView mMemberName_headerText = new TextView(getActivity());
                mMemberName_headerText.setText("" + String.valueOf(AppStrings.date));
                mMemberName_headerText.setWidth(270);
                mMemberName_headerText.setPadding(80, 5, 5, 5);
                mMemberName_headerText.setTypeface(LoginActivity.sTypeface);
                mMemberName_headerText.setTextColor(Color.WHITE);
                mMemberName_headerText.setLayoutParams(lHeaderParams);
                leftHeaderRow.addView(mMemberName_headerText);

            } else {
                TextView mMemberName_headerText = new TextView(getActivity());
                mMemberName_headerText.setText("" + String.valueOf(AppStrings.transactio_date));
                mMemberName_headerText.setWidth(350);
                mMemberName_headerText.setPadding(50, 5, 5, 5);
                mMemberName_headerText.setTypeface(LoginActivity.sTypeface);
                mMemberName_headerText.setTextColor(Color.WHITE);
                mMemberName_headerText.setLayoutParams(lHeaderParams);
                leftHeaderRow.addView(mMemberName_headerText);

            }
            mLeftHeaderTable.addView(leftHeaderRow);


            TableRow rightHeaderRow = new TableRow(getActivity());
            TableRow.LayoutParams rHeaderParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            rHeaderParams.setMargins(10, 0, 20, 0);
            TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            contentParams.setMargins(10, 0, 10, 0);

            if (type.equals(NewDrawerScreen.W_WB_REP) || type.equals(NewDrawerScreen.W_RC_REP) || type.equals(NewDrawerScreen.O_TR_REP) || type.equals(NewDrawerScreen.TRANS_DET)) {

                TextView moutstandingAmount_HeaderText = new TextView(getActivity());
                moutstandingAmount_HeaderText.setText("" + String.valueOf(AppStrings.Transaction_id));
                moutstandingAmount_HeaderText.setWidth(650);
                moutstandingAmount_HeaderText.setTypeface(LoginActivity.sTypeface);
                moutstandingAmount_HeaderText.setTextColor(Color.WHITE);
                moutstandingAmount_HeaderText.setPadding(5, 5, 0, 5);
                moutstandingAmount_HeaderText.setLayoutParams(rHeaderParams);
                moutstandingAmount_HeaderText.setGravity(Gravity.CENTER);
                moutstandingAmount_HeaderText.setSingleLine(true);
                rightHeaderRow.addView(moutstandingAmount_HeaderText);
            } else {
                TextView moutstandingAmount_HeaderText = new TextView(getActivity());
                moutstandingAmount_HeaderText.setText("" + String.valueOf(AppStrings.Opening_amount));
                moutstandingAmount_HeaderText.setWidth(370);
                moutstandingAmount_HeaderText.setTypeface(LoginActivity.sTypeface);
                moutstandingAmount_HeaderText.setTextColor(Color.WHITE);
                moutstandingAmount_HeaderText.setPadding(5, 5, 0, 5);
                moutstandingAmount_HeaderText.setLayoutParams(rHeaderParams);
                moutstandingAmount_HeaderText.setGravity(Gravity.CENTER);
                moutstandingAmount_HeaderText.setSingleLine(true);
                rightHeaderRow.addView(moutstandingAmount_HeaderText);
            }


            if (type.equals(NewDrawerScreen.W_RC_REP) || type.equals(NewDrawerScreen.O_TR_REP) || type.equals(NewDrawerScreen.TRANS_DET)) {
                TextView ttypeText = new TextView(getActivity());
                ttypeText.setText("" + String.valueOf(AppStrings.amount));
                ttypeText.setWidth(200);
                ttypeText.setTypeface(LoginActivity.sTypeface);
                ttypeText.setTextColor(Color.WHITE);
                ttypeText.setPadding(5, 5, 5, 5);
                ttypeText.setGravity(Gravity.CENTER);
                ttypeText.setLayoutParams(rHeaderParams);
                ttypeText.setSingleLine(true);
                rightHeaderRow.addView(ttypeText);

            } else if (type.equals(NewDrawerScreen.CASHIN_TRANS_HIS)) {

                TextView ttypeText = new TextView(getActivity());
                ttypeText.setText("" + String.valueOf(AppStrings.cashin_amount));
                ttypeText.setWidth(510);
                ttypeText.setTypeface(LoginActivity.sTypeface);
                ttypeText.setTextColor(Color.WHITE);
                ttypeText.setPadding(5, 5, 5, 5);
                ttypeText.setGravity(Gravity.CENTER);
                ttypeText.setLayoutParams(rHeaderParams);
                ttypeText.setSingleLine(true);
                rightHeaderRow.addView(ttypeText);

            } else {
                TextView ttypeText = new TextView(getActivity());
                ttypeText.setText("" + String.valueOf(AppStrings.PREV_WB));
                ttypeText.setWidth(510);
                ttypeText.setTypeface(LoginActivity.sTypeface);
                ttypeText.setTextColor(Color.WHITE);
                ttypeText.setPadding(5, 5, 5, 5);
                ttypeText.setGravity(Gravity.CENTER);
                ttypeText.setLayoutParams(rHeaderParams);
                ttypeText.setSingleLine(true);
                rightHeaderRow.addView(ttypeText);
            }


            if (type.equals(NewDrawerScreen.W_RC_REP) || type.equals(NewDrawerScreen.O_TR_REP) || type.equals(NewDrawerScreen.TRANS_DET)) {
                TextView mVAmount_HeaderText = new TextView(getActivity());
                mVAmount_HeaderText.setText("" + String.valueOf(AppStrings.Transactiontype));
                mVAmount_HeaderText.setTypeface(LoginActivity.sTypeface);
                mVAmount_HeaderText.setWidth(400);
                mVAmount_HeaderText.setPadding(50, 5, 5, 5);
                mVAmount_HeaderText.setTextColor(Color.WHITE);
                mVAmount_HeaderText.setLayoutParams(rHeaderParams);
                rightHeaderRow.addView(mVAmount_HeaderText);

            } else if (type.equals(NewDrawerScreen.CASHIN_TRANS_HIS)) {

                TextView mVAmount_HeaderText = new TextView(getActivity());
                mVAmount_HeaderText.setText("" + String.valueOf(AppStrings.cashoutamount));
                mVAmount_HeaderText.setTypeface(LoginActivity.sTypeface);
                mVAmount_HeaderText.setWidth(320);
                mVAmount_HeaderText.setTextColor(Color.WHITE);
                mVAmount_HeaderText.setLayoutParams(rHeaderParams);
                mVAmount_HeaderText.setSingleLine(true);
                rightHeaderRow.addView(mVAmount_HeaderText);
            } else {
                TextView mVAmount_HeaderText = new TextView(getActivity());
                mVAmount_HeaderText.setText("" + String.valueOf(AppStrings.amount));
                mVAmount_HeaderText.setTypeface(LoginActivity.sTypeface);
                mVAmount_HeaderText.setWidth(150);
                mVAmount_HeaderText.setTextColor(Color.WHITE);
                mVAmount_HeaderText.setLayoutParams(rHeaderParams);
                mVAmount_HeaderText.setSingleLine(true);
                rightHeaderRow.addView(mVAmount_HeaderText);
            }

            if (type.equals(NewDrawerScreen.W_RC_REP) || type.equals(NewDrawerScreen.O_TR_REP) || type.equals(NewDrawerScreen.TRANS_DET)) {
                TextView ttypeText1 = new TextView(getActivity());
                ttypeText1.setText("" + String.valueOf(AppStrings.Status));
                ttypeText1.setWidth(280);
                ttypeText1.setTypeface(LoginActivity.sTypeface);
                ttypeText1.setTextColor(Color.WHITE);
                ttypeText1.setPadding(5, 5, 5, 5);
                ttypeText1.setGravity(Gravity.CENTER);
                ttypeText1.setLayoutParams(rHeaderParams);
                ttypeText1.setSingleLine(true);
                rightHeaderRow.addView(ttypeText1);
            }

            if (type.equals(NewDrawerScreen.CASHIN_TRANS_HIS)) {
                TextView ttypeText1 = new TextView(getActivity());
                ttypeText1.setText("" + String.valueOf(AppStrings.outstandingamount));
                ttypeText1.setWidth(500);
                ttypeText1.setTypeface(LoginActivity.sTypeface);
                ttypeText1.setTextColor(Color.WHITE);
                ttypeText1.setPadding(5, 5, 5, 5);
                ttypeText1.setGravity(Gravity.CENTER);
                ttypeText1.setLayoutParams(rHeaderParams);
                ttypeText1.setSingleLine(true);
                rightHeaderRow.addView(ttypeText1);
            }
            if (type.equals(NewDrawerScreen.W_WB_REP)) {

                TextView ttypeText1 = new TextView(getActivity());
                ttypeText1.setText("" + String.valueOf(AppStrings.UPDATED_WB));
                ttypeText1.setWidth(500);
                ttypeText1.setTypeface(LoginActivity.sTypeface);
                ttypeText1.setTextColor(Color.WHITE);
                ttypeText1.setPadding(5, 5, 5, 5);
                ttypeText1.setGravity(Gravity.CENTER);
                ttypeText1.setLayoutParams(rHeaderParams);
                ttypeText1.setSingleLine(true);
                rightHeaderRow.addView(ttypeText1);


                TextView mVIntrestAmount_HeaderText = new TextView(getActivity());
                mVIntrestAmount_HeaderText.setText("" + String.valueOf(AppStrings.Transactiontype));
                mVIntrestAmount_HeaderText.setWidth(330);

                mVIntrestAmount_HeaderText.setTypeface(LoginActivity.sTypeface);
                mVIntrestAmount_HeaderText.setTextColor(Color.WHITE);
                mVIntrestAmount_HeaderText.setSingleLine(true);
                mVIntrestAmount_HeaderText.setLayoutParams(rHeaderParams);
                rightHeaderRow.addView(mVIntrestAmount_HeaderText);


                TextView cHeadView = new TextView(getActivity());
                cHeadView.setText("" + String.valueOf(AppStrings.Remarks));
                cHeadView.setWidth(650);
                cHeadView.setTypeface(LoginActivity.sTypeface);
                cHeadView.setTextColor(Color.WHITE);
                cHeadView.setPadding(0, 5, 5, 5);
                cHeadView.setGravity(Gravity.CENTER);
                cHeadView.setLayoutParams(rHeaderParams);
                cHeadView.setSingleLine(true);
                rightHeaderRow.addView(cHeadView);
            }
            mRightHeaderTable.addView(rightHeaderRow);


            for (int i = 0; i < listItems.size(); i++) {

                TableRow leftContentRow = new TableRow(getActivity());
                TableRow.LayoutParams leftContentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 100, 1f);
                leftContentParams.setMargins(0, 0, 0, 0);

                if (type.equals(NewDrawerScreen.W_RC_REP) || type.equals(NewDrawerScreen.W_WB_REP)) {

                    if (listItems.get(i).getTransactionDate() != null && listItems.get(i).getTransactionDate().length() > 0) {
                        tx_text = new TextView(getActivity());
                        String date = df.format(new Date(Long.parseLong(listItems.get(i).getTransactionDate())));
                        String time = df1.format(new Date(Long.parseLong(listItems.get(i).getTransactionDate())));
                        tx_text.setText(date + "\n" + time);
                        //memberName_Text.setText(memList.get(i).getMemberName());
                        tx_text.setTextColor(R.color.black);
                        tx_text.setWidth(300);
                        tx_text.setBackgroundResource(R.drawable.report_layout);
                        tx_text.setPadding(20, 5, 5, 5);
                        tx_text.setLayoutParams(leftContentParams);
//        tx_text.setSingleLine(true);
//        tx_text.setEllipsize(TextUtils.TruncateAt.END);
                        leftContentRow.addView(tx_text);
                    } else {
                        tx_text = new TextView(getActivity());
                        tx_text.setText("-");
                        //memberName_Text.setText(memList.get(i).getMemberName());
                        tx_text.setTextColor(R.color.black);
                        tx_text.setPadding(5, 5, 5, 5);
                        tx_text.setLayoutParams(leftContentParams);
                        tx_text.setBackgroundResource(R.drawable.report_layout);
                        tx_text.setWidth(220);
                        tx_text.setSingleLine(true);
                        tx_text.setEllipsize(TextUtils.TruncateAt.END);
                        leftContentRow.addView(tx_text);
                    }
                } else {

                    if (listItems.get(i).getTransactionDate() != null && listItems.get(i).getTransactionDate().length() > 0) {
                        tx_text = new TextView(getActivity());
                        String date = df.format(new Date(Long.parseLong(listItems.get(i).getTransactionDate())));
                        String time = df1.format(new Date(Long.parseLong(listItems.get(i).getTransactionDate())));
                        tx_text.setText(date + "\n" + time);
                        //memberName_Text.setText(memList.get(i).getMemberName());
                        tx_text.setTextColor(R.color.black);
                        tx_text.setWidth(350);
                        tx_text.setPadding(5, 5, 5, 5);
                        tx_text.setLayoutParams(leftContentParams);
//                        tx_text.setSingleLine(true);
                        tx_text.setEllipsize(TextUtils.TruncateAt.END);
                        tx_text.setBackgroundResource(R.drawable.report_layout);
                        leftContentRow.addView(tx_text);


                    } else {
                        tx_text = new TextView(getActivity());
                        tx_text.setText("-");
                        //memberName_Text.setText(memList.get(i).getMemberName());
                        tx_text.setTextColor(R.color.black);
                        tx_text.setPadding(5, 5, 5, 5);
                        tx_text.setLayoutParams(leftContentParams);
                        tx_text.setWidth(220);
                        tx_text.setSingleLine(true);
                        tx_text.setBackgroundResource(R.drawable.report_layout);
                        tx_text.setEllipsize(TextUtils.TruncateAt.END);
                        leftContentRow.addView(tx_text);

                    }

                }


             /*   View v = new View(getActivity());
                v.setMinimumWidth(5);
                v.setMinimumHeight(2);
                v.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                leftContentRow.addView(v);*/
                mLeftContentTable.addView(leftContentRow);

                TableRow rightContentRow = new TableRow(getActivity());
                TableRow.LayoutParams rightContentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);// (150,60,1f);
                rightContentParams.setMargins(0, 0, 0, 0);

                TableRow.LayoutParams contentRow_Params = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 100, 1f);
                contentRow_Params.setMargins(0, 0, 0, 0);

                if (type.equals(NewDrawerScreen.W_WB_REP) || type.equals(NewDrawerScreen.W_RC_REP) || type.equals(NewDrawerScreen.O_TR_REP) || type.equals(NewDrawerScreen.TRANS_DET)) {
                    tx_id = new TextView(getActivity());
                    String out = "";
                    out = listItems.get(i).getTransactionId();
                    tx_id.setText(out + "");
                    tx_id.setWidth(600);
                    tx_id.setTextColor(R.color.black);
                    tx_id.setBackgroundResource(R.drawable.report_layout);
                    tx_id.setGravity(Gravity.CENTER);
                    tx_id.setLayoutParams(contentRow_Params);
                    tx_id.setPadding(20, 0, 10, 5);
                    rightContentRow.addView(tx_id);
                } else {
                    tx_id = new TextView(getActivity());
                    String out = "";
                    out = listItems.get(i).getOpeningAmount();
                    tx_id.setText(out + "");
                    tx_id.setWidth(450);
                    tx_id.setTextColor(R.color.black);
                    tx_id.setGravity(Gravity.CENTER);
                    tx_id.setBackgroundResource(R.drawable.report_layout);
                    tx_id.setLayoutParams(contentRow_Params);
                    tx_id.setPadding(40, 0, 10, 5);
                    rightContentRow.addView(tx_id);
                }

              /*  View tx_id = new View(getActivity());
                tx_id.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                tx_id.setMinimumWidth(5);
                tx_id.setMinimumHeight(2);
                rightContentRow.addView(tx_id);
*/
                if (type.equals(NewDrawerScreen.W_RC_REP)) {

                    ttypeText = new TextView(getActivity());
                    String tx_tyStr = "";
                    tx_tyStr = listItems.get(i).getAmount();
                    ttypeText.setText(tx_tyStr + "");
                    ttypeText.setTextColor(R.color.black);
                    ttypeText.setWidth(320);
                    ttypeText.setGravity(Gravity.CENTER);
                    ttypeText.setBackgroundResource(R.drawable.report_layout);
                    ttypeText.setLayoutParams(contentRow_Params);
                    ttypeText.setPadding(10, 0, 10, 5);
                    rightContentRow.addView(ttypeText);

                } else if (type.equals(NewDrawerScreen.O_TR_REP) || type.equals(NewDrawerScreen.TRANS_DET)) {
                    ttypeText = new TextView(getActivity());
                    String tx_tyStr = "";
                    tx_tyStr = listItems.get(i).getAmount();
                    ttypeText.setText(tx_tyStr + "");
                    ttypeText.setTextColor(R.color.black);
                    ttypeText.setWidth(340);
                    ttypeText.setGravity(Gravity.CENTER);
                    ttypeText.setBackgroundResource(R.drawable.report_layout);
                    ttypeText.setLayoutParams(contentRow_Params);
                    ttypeText.setPadding(10, 0, 10, 5);
                    rightContentRow.addView(ttypeText);
                } else if (type.equals(NewDrawerScreen.W_WB_REP)) {
                    ttypeText = new TextView(getActivity());
                    String tx_tyStr = "";
                    tx_tyStr = listItems.get(i).getPreviousWalletAmount();
                    ttypeText.setText(tx_tyStr + "");
                    ttypeText.setTextColor(R.color.black);
                    ttypeText.setBackgroundResource(R.drawable.report_layout);
                    ttypeText.setWidth(550);
                    ttypeText.setGravity(Gravity.CENTER);
                    ttypeText.setLayoutParams(contentRow_Params);
                    ttypeText.setPadding(10, 0, 10, 5);
                    rightContentRow.addView(ttypeText);
                } else {
                    ttypeText = new TextView(getActivity());
                    String tx_tyStr = "";
                    tx_tyStr = listItems.get(i).getCashInAmount();
                    ttypeText.setText(tx_tyStr + "");
                    ttypeText.setTextColor(R.color.black);
                    ttypeText.setWidth(500);
                    ttypeText.setGravity(Gravity.CENTER);
                    ttypeText.setBackgroundResource(R.drawable.report_layout);
                    ttypeText.setLayoutParams(contentRow_Params);
                    ttypeText.setPadding(10, 0, 10, 5);
                    rightContentRow.addView(ttypeText);
                }

              /*  View ttypeText = new View(getActivity());
                ttypeText.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                ttypeText.setMinimumWidth(5);
                ttypeText.setMinimumHeight(2);
                rightContentRow.addView(ttypeText);*/

                if (type.equals(NewDrawerScreen.W_RC_REP)) {

                    amttext = new TextView(getActivity());
                    String outStr = "";
                    outStr = listItems.get(i).getTransactionMode();
                    amttext.setText(outStr + "");
                    amttext.setTextColor(R.color.black);
                    amttext.setWidth(350);
                    amttext.setGravity(Gravity.CENTER);
                    amttext.setBackgroundResource(R.drawable.report_layout);
                    amttext.setLayoutParams(contentRow_Params);
                    amttext.setPadding(10, 0, 10, 5);
                    rightContentRow.addView(amttext);

                } else if (type.equals(NewDrawerScreen.O_TR_REP) || type.equals(NewDrawerScreen.TRANS_DET))

                {
                    amttext = new TextView(getActivity());
                    String outStr = "";
                    outStr = listItems.get(i).getTransactionType();
                    amttext.setText(outStr + "");
                    amttext.setTextColor(R.color.black);
                    amttext.setWidth(400);
                    amttext.setGravity(Gravity.CENTER);
                    amttext.setLayoutParams(contentRow_Params);
                    amttext.setBackgroundResource(R.drawable.report_layout);
                    amttext.setPadding(10, 0, 10, 5);
                    rightContentRow.addView(amttext);
                } else if (type.equals(NewDrawerScreen.CASHIN_TRANS_HIS)) {

                    amttext = new TextView(getActivity());
                    String outStr = "";
                    outStr = listItems.get(i).getCashOutAmount();
                    amttext.setText(outStr + "");
                    amttext.setTextColor(R.color.black);
                    amttext.setWidth(380);
                    amttext.setGravity(Gravity.CENTER);
                    amttext.setLayoutParams(contentRow_Params);
                    amttext.setBackgroundResource(R.drawable.report_layout);
                    amttext.setPadding(10, 0, 10, 5);
                    rightContentRow.addView(amttext);
                } else {
                    amttext = new TextView(getActivity());
                    String outStr = "";
                    outStr = listItems.get(i).getAmountProcessed();
                    amttext.setText(outStr + "");
                    amttext.setTextColor(R.color.black);
                    amttext.setWidth(200);
                    amttext.setGravity(Gravity.CENTER);
                    amttext.setBackgroundResource(R.drawable.report_layout);
                    amttext.setLayoutParams(contentRow_Params);
                    amttext.setPadding(10, 0, 10, 5);
                    rightContentRow.addView(amttext);
                }
                /*View amttext = new View(getActivity());
                amttext.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                amttext.setMinimumWidth(5);
                amttext.setMinimumHeight(2);
                rightContentRow.addView(amttext);
*/
                if (type.equals(NewDrawerScreen.W_RC_REP) || type.equals(NewDrawerScreen.O_TR_REP) || type.equals(NewDrawerScreen.TRANS_DET)) {

                    ttypeText1 = new TextView(getActivity());
                    String tx_tyStr1 = "";
                    tx_tyStr1 = listItems.get(i).getTransactionStatus();
                    ttypeText1.setText(tx_tyStr1 + "");
                    ttypeText1.setTextColor(R.color.black);
                    ttypeText1.setWidth(300);
                    ttypeText1.setGravity(Gravity.CENTER);
                    ttypeText1.setBackgroundResource(R.drawable.report_layout);
                    ttypeText1.setLayoutParams(contentRow_Params);
                    ttypeText1.setPadding(10, 0, 10, 5);
                    rightContentRow.addView(ttypeText1);

                } else if (type.equals(NewDrawerScreen.CASHIN_TRANS_HIS)) {
                    ttypeText1 = new TextView(getActivity());
                    String tx_tyStr1 = "";
                    tx_tyStr1 = listItems.get(i).getOutStandingAmount();
                    ttypeText1.setText(tx_tyStr1 + "");
                    ttypeText1.setTextColor(R.color.black);
                    ttypeText1.setWidth(500);
                    ttypeText1.setGravity(Gravity.CENTER);
                    ttypeText1.setBackgroundResource(R.drawable.report_layout);
                    ttypeText1.setLayoutParams(contentRow_Params);
                    ttypeText1.setPadding(10, 0, 10, 5);
                    rightContentRow.addView(ttypeText1);

                } else {
                    ttypeText1 = new TextView(getActivity());
                    String tx_tyStr1 = "";
                    tx_tyStr1 = listItems.get(i).getUpdatedWalletAmount();
                    ttypeText1.setText(tx_tyStr1 + "");
                    ttypeText1.setTextColor(R.color.black);
                    ttypeText1.setWidth(520);
                    ttypeText1.setGravity(Gravity.CENTER);
                    ttypeText1.setBackgroundResource(R.drawable.report_layout);
                    ttypeText1.setLayoutParams(contentRow_Params);
                    ttypeText1.setPadding(10, 0, 10, 5);
                    rightContentRow.addView(ttypeText1);
                }

              /*  View ttypeText1 = new View(getActivity());
                ttypeText1.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                ttypeText1.setMinimumWidth(5);
                ttypeText1.setMinimumHeight(2);
                rightContentRow.addView(ttypeText1);*/

                if (type.equals(NewDrawerScreen.W_WB_REP)) {
                    tx_type = new TextView(getActivity());
                    String tx_tyStr2 = "";
                    tx_tyStr2 = listItems.get(i).getTransType();
                    tx_type.setText(tx_tyStr2 + "");
                    tx_type.setTextColor(R.color.black);
                    tx_type.setWidth(340);
                    tx_type.setGravity(Gravity.CENTER);
                    tx_type.setBackgroundResource(R.drawable.report_layout);
                    tx_type.setLayoutParams(contentRow_Params);
                    tx_type.setPadding(10, 0, 10, 5);
                    rightContentRow.addView(tx_type);

                    stsStr = new TextView(getActivity());
                    String stsStrs = "";
                    stsStrs = listItems.get(i).getTransRemarks();
                    stsStr.setText(stsStrs + "");
                    stsStr.setTextColor(R.color.black);
                    stsStr.setWidth(650);
                    stsStr.setBackgroundResource(R.drawable.report_layout);
                    stsStr.setGravity(Gravity.CENTER);
                    stsStr.setLayoutParams(contentRow_Params);
                    stsStr.setPadding(10, 0, 10, 5);
                    rightContentRow.addView(stsStr);

                }
                mRightContentTable.addView(rightContentRow);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onClick(View view) {


    }

    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
            mProgressDilaog.dismiss();
            mProgressDilaog = null;
        }
        switch (serviceType) {

            case WALLET_WB_HIS:
                if (result != null && result.length() > 0) {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    HistoryDto lrDto = gson.fromJson(result, HistoryDto.class);
                    int statusCode = lrDto.getStatusCode();
                    String message = lrDto.getMessage();
                    Log.d("response status", " " + statusCode);
                    if (statusCode == 400 || statusCode == 401 || statusCode == 403 || statusCode == 500 || statusCode == 503) {
                        // showMessage(statusCode);
                        Utils.showToast(getActivity(), message);

                        if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        }

                        if (statusCode == 503) {

                            Utils.showToast(getActivity(), AppStrings.service_unavailable);

                        }

                        // init();

                    } else if (statusCode == Utils.Success_Code) {
                        Utils.showToast(getActivity(), message);
                        if (listItems != null && listItems.size() > 0) {
                            listItems.clear();
                        }

                        if (lrDto.getResponseContents() != null && lrDto.getResponseContents().size() > 0) {

                            list_details = (ListView) view.findViewById(R.id.list_details);
                            listItems = new ArrayList<ListItem>();

                            for (int i = 0; i < lrDto.getResponseContents().size(); i++) {
                                ListItem rowItem = new ListItem();
                                rowItem.setTransactionDate(lrDto.getResponseContents().get(i).getTransactionDate());
//                                rowItem.setTransactionDescription(lrDto.getResponseContents().get(i).getTransactionDescription());
                                rowItem.setTransactionId(lrDto.getResponseContents().get(i).getTransactionId());
//                                rowItem.setAmount(lrDto.getResponseContents().get(i).getAmount());
//                                rowItem.setTransactionMode(lrDto.getResponseContents().get(i).getTransactionMode());
                                rowItem.setPreviousWalletAmount(lrDto.getResponseContents().get(i).getPreviousWalletAmount());
                                rowItem.setUpdatedWalletAmount(lrDto.getResponseContents().get(i).getUpdatedWalletAmount());
                                rowItem.setTransType(lrDto.getResponseContents().get(i).getTransType());
                                rowItem.setTransRemarks(lrDto.getResponseContents().get(i).getTransRemarks());
                                rowItem.setAmountProcessed(lrDto.getResponseContents().get(i).getAmountProcessed());
                                listItems.add(rowItem);
                            }
                            mAdapter = new CustomTransactionListAdapter(getActivity(), listItems);
                            list_details.setAdapter(mAdapter);
                            init();
                        }
                    }
                }

                break;

            case WALLET_RX_HIS:

                if (result != null && result.length() > 0) {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    HistoryDto lrDto = gson.fromJson(result, HistoryDto.class);
                    int statusCode = lrDto.getStatusCode();
                    String message = lrDto.getMessage();
                    Log.d("response status", " " + statusCode);
                    if (statusCode == 400 || statusCode == 401 || statusCode == 403 || statusCode == 500 || statusCode == 503) {
                        // showMessage(statusCode);
                        Utils.showToast(getActivity(), message);

                        if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        }

                        if (statusCode == 503) {

                            Utils.showToast(getActivity(), AppStrings.service_unavailable);

                        }

                        // init();

                    } else if (statusCode == Utils.Success_Code) {
                        Utils.showToast(getActivity(), message);

                        if (listItems != null && listItems.size() > 0) {
                            listItems.clear();
                        }

                        if (lrDto.getResponseContents() != null && lrDto.getResponseContents().size() > 0) {

                            list_details = (ListView) view.findViewById(R.id.list_details);
                            listItems = new ArrayList<ListItem>();

                            for (int i = 0; i < lrDto.getResponseContents().size(); i++) {
                                ListItem rowItem = new ListItem();
                                rowItem.setTransactionDate(lrDto.getResponseContents().get(i).getTransactionDate());
                                rowItem.setTransactionDescription(lrDto.getResponseContents().get(i).getTransactionDescription());
                                rowItem.setTransactionId(lrDto.getResponseContents().get(i).getTransactionId());
                                rowItem.setAmount(lrDto.getResponseContents().get(i).getAmount());
                                rowItem.setTransactionMode(lrDto.getResponseContents().get(i).getTransactionMode());
                                rowItem.setTransactionStatus(lrDto.getResponseContents().get(i).getTransactionStatus());

                                listItems.add(rowItem);
                            }
                            mAdapter = new CustomTransactionListAdapter(getActivity(), listItems);
                            list_details.setAdapter(mAdapter);
                            init();

                        } else {
                            Utils.showToast(getActivity(), message);
                        }
                    }

                }
                break;


            case WALLET_TRS_HIS:

                if (result != null && result.length() > 0) {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    HistoryDto lrDto = gson.fromJson(result, HistoryDto.class);
                    int statusCode = lrDto.getStatusCode();
                    String message = lrDto.getMessage();
                    Log.d("response status", " " + statusCode);
                    if (statusCode == 400 || statusCode == 401 || statusCode == 403 || statusCode == 500 || statusCode == 503) {
                        // showMessage(statusCode);
                        Utils.showToast(getActivity(), message);
                        if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        }

                        if (statusCode == 503) {

                            Utils.showToast(getActivity(), AppStrings.service_unavailable);

                        }

                        // init();

                    } else if (statusCode == Utils.Success_Code) {
                        Utils.showToast(getActivity(), message);

                        if (listItems != null && listItems.size() > 0) {
                            listItems.clear();
                        }

                        if (lrDto.getResponseContents() != null && lrDto.getResponseContents().size() > 0) {

                            list_details = (ListView) view.findViewById(R.id.list_details);
                            listItems = new ArrayList<ListItem>();

                            for (int i = 0; i < lrDto.getResponseContents().size(); i++) {
                                ListItem rowItem = new ListItem();
                                rowItem.setMerchantId(lrDto.getResponseContents().get(i).getMerchantId());
                                rowItem.setTransactionDate(lrDto.getResponseContents().get(i).getTransactionDate());
                                rowItem.setAmount(lrDto.getResponseContents().get(i).getAmount());
                                rowItem.setTransactionId(lrDto.getResponseContents().get(i).getTransactionId());
                                rowItem.setReferenceId(lrDto.getResponseContents().get(i).getReferenceId());
                                rowItem.setTransactionStatus(lrDto.getResponseContents().get(i).getTransactionStatus());
                                rowItem.setTransactionType(lrDto.getResponseContents().get(i).getTransactionType());
                                rowItem.setAnimatorName(lrDto.getResponseContents().get(i).getAnimatorName());
                                listItems.add(rowItem);
                            }
                            mAdapter = new CustomTransactionListAdapter(getActivity(), listItems);
                            list_details.setAdapter(mAdapter);
                            init();

                        } else {
                            Utils.showToast(getActivity(), message);
                        }
                    }

                }
                break;

            case WALLET_TRS_DETAILS:

                if (result != null && result.length() > 0) {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    HistoryDto lrDto = gson.fromJson(result, HistoryDto.class);
                    int statusCode = lrDto.getStatusCode();
                    String message = lrDto.getMessage();
                    Log.d("response status", " " + statusCode);
                    if (statusCode == 400 || statusCode == 401 || statusCode == 403 || statusCode == 500 || statusCode == 503) {
                        // showMessage(statusCode);
                        Utils.showToast(getActivity(), message);

                        if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        }

                        if (statusCode == 503) {

                            Utils.showToast(getActivity(), AppStrings.service_unavailable);

                        }

                        // init();

                    } else if (statusCode == Utils.Success_Code) {
                        Utils.showToast(getActivity(), message);

                        if (listItems != null && listItems.size() > 0) {
                            listItems.clear();
                        }

                        if (lrDto.getResponseContents() != null && lrDto.getResponseContents().size() > 0) {

                            list_details = (ListView) view.findViewById(R.id.list_details);
                            listItems = new ArrayList<ListItem>();

                            for (int i = 0; i < lrDto.getResponseContents().size(); i++) {
                                ListItem rowItem = new ListItem();
                                rowItem.setMerchantId(lrDto.getResponseContents().get(i).getMerchantId());
                                rowItem.setTransactionDate(lrDto.getResponseContents().get(i).getTransactionDate());
                                rowItem.setAmount(lrDto.getResponseContents().get(i).getAmount());
                                rowItem.setTransactionId(lrDto.getResponseContents().get(i).getTransactionId());
                                rowItem.setReferenceId(lrDto.getResponseContents().get(i).getReferenceId());
                                rowItem.setTransactionStatus(lrDto.getResponseContents().get(i).getTransactionStatus());
                                rowItem.setTransactionType(lrDto.getResponseContents().get(i).getTransactionType());
                                rowItem.setAnimatorName(lrDto.getResponseContents().get(i).getAnimatorName());
                                listItems.add(rowItem);
                            }
                            mAdapter = new CustomTransactionListAdapter(getActivity(), listItems);
                            list_details.setAdapter(mAdapter);
                            init();

                        } else {
                            Utils.showToast(getActivity(), message);
                        }
                    }else {
                        Utils.showToast(getActivity(), message);
                    }

                }
                break;


            case WALLET_CASHIN_HAND_HISTORY:

                if (result != null && result.length() > 0) {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    HistoryDto lrDto = gson.fromJson(result, HistoryDto.class);
                    int statusCode = lrDto.getStatusCode();
                    String message = lrDto.getMessage();
                    Log.d("response status", " " + statusCode);
                    if (statusCode == 400 || statusCode == 401 || statusCode == 403 || statusCode == 500 || statusCode == 503) {
                        // showMessage(statusCode);
                        Utils.showToast(getActivity(), message);
                        if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        }

                        if (statusCode == 503) {

                            Utils.showToast(getActivity(), AppStrings.service_unavailable);

                        }
                        Utils.showToast(getActivity(), message);


                        // init();

                    } else if (statusCode == Utils.Success_Code) {
                        Utils.showToast(getActivity(), message);

                        if (listItems != null && listItems.size() > 0) {
                            listItems.clear();
                        }

                        if (lrDto.getResponseContents() != null && lrDto.getResponseContents().size() > 0) {

                            list_details = (ListView) view.findViewById(R.id.list_details);
                            listItems = new ArrayList<ListItem>();

                            for (int i = 0; i < lrDto.getResponseContents().size(); i++) {
                                ListItem rowItem = new ListItem();
                                rowItem.setTransactionDate(lrDto.getResponseContents().get(i).getTransactionDate());
                                rowItem.setOpeningAmount(lrDto.getResponseContents().get(i).getOpeningAmount());
                                rowItem.setOutStandingAmount(lrDto.getResponseContents().get(i).getOutStandingAmount());
                                rowItem.setCashInAmount(lrDto.getResponseContents().get(i).getCashInAmount());
                                rowItem.setCashOutAmount(lrDto.getResponseContents().get(i).getCashOutAmount());
                                listItems.add(rowItem);
                            }
                            mAdapter = new CustomTransactionListAdapter(getActivity(), listItems);
                            list_details.setAdapter(mAdapter);
                            init();

                        } else {
                            Utils.showToast(getActivity(), message);
                        }
                    }else{
                        Utils.showToast(getActivity(), message);
                    }

                }
                break;

        }

    }
}





