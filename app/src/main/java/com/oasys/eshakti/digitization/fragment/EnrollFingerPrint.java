package com.oasys.eshakti.digitization.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.SystemClock;
import android.speech.tts.TextToSpeech;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mantra.mfs100.DeviceInfo;
import com.mantra.mfs100.FingerData;
import com.mantra.mfs100.MFS100;
import com.mantra.mfs100.MFS100Event;
import com.oasys.eshakti.digitization.Adapter.EnrollFpAdapter;
import com.oasys.eshakti.digitization.Dto.EnrollFP.EnrollFP;
import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.Dto.MantraDto.Opts;
import com.oasys.eshakti.digitization.Dto.MantraDto.PidOptions;
import com.oasys.eshakti.digitization.Dto.ResponseContents;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.Dto.VerifyFPDto;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.activity.LoginActivity;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;
import com.oasys.eshakti.digitization.database.EnrollFPData;
import com.oasys.eshakti.digitization.database.SHGTable;
import com.oasys.eshakti.digitization.views.MaterialSpinner;
import com.oasys.eshakti.digitization.views.RaisedButton;

import org.json.JSONObject;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Locale;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class EnrollFingerPrint extends Fragment implements MFS100Event, View.OnClickListener, NewTaskListener, AdapterView.OnItemSelectedListener {


    private View rootView;
    private ListOfShg shgDto;
    private MaterialSpinner spinner_enrollfinger;
    private TextView mGroupName;
    private TextView mCashInHand;
    private TextView mCashAtBank;
    private RaisedButton mRaised_SubmitButton;
    private EditText edit_Name;
    private TextView header;
    private RaisedButton fragment_start_scan, fragment_start_scan1;
    private Dialog mProgressDilaog;
    private String toSpeak;
    private TextToSpeech textToSpeech;
    private ImageView img_fp, img_fp1;
    private String pidDate;
    public ArrayList<ResponseContents> responseContentsList = new ArrayList<>();

    private static long mLastClkTime = 0;

    private static long Threshold = 1500;

    private static String finger1 = null;
    private static String finger2 = null;
    private EShaktiApplication globalVariable;
    private ResponseContents value;
    private String id;
    private boolean fin1Flag = false;
    private boolean fin2Flag = false;


    @Override
    public void onStart() {
        super.onStart();
        try {
            if (Utils.mfs100 == null) {
                Utils.mfs100 = new MFS100(this);
            }
            if (Utils.mfs100 != null) {
                Utils.mfs100.SetApplicationContext(getActivity());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.enrollment_ly, container, false);
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        globalVariable = (EShaktiApplication) getActivity().getApplicationContext();
        MySharedPreference.writeBoolean(getActivity(), MySharedPreference.FINGER_FLAG1, false);
        MySharedPreference.writeBoolean(getActivity(), MySharedPreference.FINGER_FLAG2, false);
        return rootView;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        try {
            try {
                if (globalVariable.is_OTG()) {
                    if (Utils.mfs100 == null) {
                        Utils.mfs100 = new MFS100(this);
                    } else {
                        //      InitScanner();
                    }
                    if (Utils.mfs100 != null) {
                        Utils.mfs100.SetApplicationContext(getActivity());
                    }
                } else {
                    Toast.makeText(getActivity(), "Enable OTG device connectivity!", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            RestClient.getRestClient(EnrollFingerPrint.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.GET_ENROLL_LIST + shgDto.getShgId(), getActivity(), ServiceType.GET_ENROLLMENT_LIST);

            toSpeak = "Place Your FINGER on FINGER PRINT Scanner";
//            callMasterData("0");  TODO::
            mGroupName = (TextView) rootView.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
            mCashInHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashInHand.setTypeface(LoginActivity.sTypeface);

            mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
            mCashAtBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashAtBank.setTypeface(LoginActivity.sTypeface);

            header = (TextView) rootView.findViewById(R.id.header);
            header.setText(AppStrings.enrollment);
            header.setTypeface(LoginActivity.sTypeface);

            spinner_enrollfinger = (MaterialSpinner) rootView.findViewById(R.id.spinner_enrollfinger);
            spinner_enrollfinger.setFloatingLabelText(AppStrings.tenure);
            spinner_enrollfinger.setPaddingSafe(10, 0, 10, 0);


            mRaised_SubmitButton = (RaisedButton) rootView.findViewById(R.id.fragment_Submitbutton);
            mRaised_SubmitButton.setText("" + AppStrings.submit);
            mRaised_SubmitButton.setTypeface(LoginActivity.sTypeface);
            mRaised_SubmitButton.setOnClickListener(this);
            mRaised_SubmitButton.setEnabled(false);
            img_fp = (ImageView) rootView.findViewById(R.id.img_fp);
            img_fp1 = (ImageView) rootView.findViewById(R.id.img_fp1);

            fragment_start_scan = (RaisedButton) rootView.findViewById(R.id.fragment_start_scan);
            fragment_start_scan.setText("" + AppStrings.str_scan);
            fragment_start_scan.setTypeface(LoginActivity.sTypeface);
            fragment_start_scan.setOnClickListener(this);
            fragment_start_scan1 = (RaisedButton) rootView.findViewById(R.id.fragment_start_scan1);
            fragment_start_scan1.setText("" + AppStrings.str_scan);
            fragment_start_scan1.setTypeface(LoginActivity.sTypeface);
            fragment_start_scan1.setOnClickListener(this);

            edit_Name = (EditText) rootView.findViewById(R.id.edit_Name);
            edit_Name.setHint("" + AppStrings.Name);
            edit_Name.setClickable(false);
            edit_Name.setTypeface(LoginActivity.sTypeface);
            edit_Name.setBackgroundResource(R.drawable.edittext_background);

            spinner_enrollfinger = (MaterialSpinner) rootView.findViewById(R.id.spinner_enrollfinger);
            spinner_enrollfinger.setBaseColor(R.color.colorPrimary);
            spinner_enrollfinger.setFloatingLabelText(String.valueOf(AppStrings.enrollment));
            spinner_enrollfinger.setOnItemSelectedListener(this);


            textToSpeech = new TextToSpeech(getActivity().getApplicationContext(), new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    if (status == TextToSpeech.SUCCESS) {

                        textToSpeech.setLanguage(Locale.US);
                        //  textToSpeech.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);

                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onStop() {
        if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }

        UnInitScanner();
//        Toast.makeText(MemberUpdateAuthenticationActivity.this, "Device removed", Toast.LENGTH_SHORT).show();
        try {
            if (mProgressDilaog != null) {
                mProgressDilaog.dismiss();
                fragment_start_scan.setEnabled(true);
                finger1 = null;
                finger2 = null;
                img_fp.setImageBitmap(null);
                img_fp1.setImageBitmap(null);
                bitmapImg = BitmapFactory.decodeResource(getResources(), R.drawable.thumb_impress);
                img_fp.setImageBitmap(bitmapImg);
                img_fp1.setImageBitmap(bitmapImg);
            }

        } catch (Exception e) {

        }
        super.onStop();
    }

    @Override
    public void onDestroy() {
        if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }

        UnInitScanner();
//        Toast.makeText(MemberUpdateAuthenticationActivity.this, "Device removed", Toast.LENGTH_SHORT).show();
        try {
            if (mProgressDilaog != null) {
                mProgressDilaog.dismiss();
                fragment_start_scan.setEnabled(true);
                finger1 = null;
                finger2 = null;
                img_fp.setImageBitmap(null);
                img_fp1.setImageBitmap(null);
                bitmapImg = BitmapFactory.decodeResource(getResources(), R.drawable.thumb_impress);
                img_fp.setImageBitmap(bitmapImg);
                img_fp1.setImageBitmap(bitmapImg);
            }

        } catch (Exception e) {

        }

        super.onDestroy();
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.fragment_start_scan:


                mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
                mProgressDilaog.show();
                textToSpeech.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
                if (fragment_start_scan.getText().equals(AppStrings.str_scan)) {
                    //   mProgressDilaog.dismiss();
                    fragment_start_scan.setEnabled(false);
                    img_fp.setImageResource(R.drawable.thumb_impress);
                    if (edit_Name.getText().toString().length() <= 0) {
                        Toast.makeText(getActivity(), "Enter Name", Toast.LENGTH_SHORT).show();
                        fragment_start_scan.setEnabled(true);
                        if (mProgressDilaog != null && mProgressDilaog.isShowing())
                            mProgressDilaog.dismiss();
                        return;
                    }


                    final EShaktiApplication globalVariable = (EShaktiApplication) getActivity().getApplicationContext();
                    if (globalVariable.is_OTG()) {
                        // RdScan();
                        MySharedPreference.writeBoolean(getActivity(), MySharedPreference.FINGER_FLAG1, true);
                        MySharedPreference.writeBoolean(getActivity(), MySharedPreference.FINGER_FLAG2, false);
                        finger1 = scan();

                    } else {
                        fragment_start_scan.setEnabled(true);
                        if (mProgressDilaog != null && mProgressDilaog.isShowing())
                            mProgressDilaog.dismiss();
                        Toast.makeText(getActivity(), "Enable OTG device connectivity!", Toast.LENGTH_SHORT).show();
                    }
                 /*   count++;
                    mSubmit_Raised_Button.setText(AppStrings.scanning);

                    if (count % 2 != 0) {
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //Do something after 100ms
                                mProgressDilaog.dismiss();
                                mSubmit_Raised_Button.setText(AppStrings.continue_flow);
                                mSubmit_Raised_Button.setVisibility(View.VISIBLE);
                                retry_Raised_Button.setVisibility(View.GONE);
                                skip_Raised_Button.setVisibility(View.GONE);
                            }
                        }, 100);
                    } else {
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //Do something after 100ms
                                mProgressDilaog.dismiss();
                                mSubmit_Raised_Button.setText(AppStrings.str_scan);
                                mSubmit_Raised_Button.setVisibility(View.GONE);
                                retry_Raised_Button.setVisibility(View.VISIBLE);
                                skip_Raised_Button.setVisibility(View.VISIBLE);
                            }
                        }, 100);
                    }*/
                } else {
                    fragment_start_scan.setEnabled(true);
                    if (mProgressDilaog != null && mProgressDilaog.isShowing())
                        mProgressDilaog.dismiss();


                    //Perform your functionality
                    //   sendRequest();


                }
                break;

            case R.id.fragment_start_scan1:

                if (!MySharedPreference.readBoolean(getActivity(), MySharedPreference.FINGER_FLAG1, false)) {
                    Toast.makeText(getActivity(), "Finger 1 Biometric is empty!", Toast.LENGTH_SHORT).show();
                    return;
                }
                mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
                mProgressDilaog.show();
                textToSpeech.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
                if (fragment_start_scan1.getText().equals(AppStrings.str_scan)) {
                    //   mProgressDilaog.dismiss();
                    fragment_start_scan1.setEnabled(false);
                    img_fp1.setImageResource(R.drawable.thumb_impress);
                    if (edit_Name.getText().toString().length() <= 0) {
                        Toast.makeText(getActivity(), "Enter Name", Toast.LENGTH_SHORT).show();
                        fragment_start_scan1.setEnabled(true);
                        if (mProgressDilaog != null && mProgressDilaog.isShowing())
                            mProgressDilaog.dismiss();
                        return;
                    }


                    if (globalVariable.is_OTG()) {
                        // RdScan();
                        MySharedPreference.writeBoolean(getActivity(), MySharedPreference.FINGER_FLAG2, true);
                        MySharedPreference.writeBoolean(getActivity(), MySharedPreference.FINGER_FLAG1, false);
                        finger2 = scan();


                    } else {
                        fragment_start_scan1.setEnabled(true);
                        if (mProgressDilaog != null && mProgressDilaog.isShowing())
                            mProgressDilaog.dismiss();
                        Toast.makeText(getActivity(), "Enable OTG device connectivity!", Toast.LENGTH_SHORT).show();
                    }
                    if (finger1 != null && finger2 != null) {
                        mRaised_SubmitButton.setEnabled(true);
                    }
                 /*   count++;
                    mSubmit_Raised_Button.setText(AppStrings.scanning);

                    if (count % 2 != 0) {
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //Do something after 100ms
                                mProgressDilaog.dismiss();
                                mSubmit_Raised_Button.setText(AppStrings.continue_flow);
                                mSubmit_Raised_Button.setVisibility(View.VISIBLE);
                                retry_Raised_Button.setVisibility(View.GONE);
                                skip_Raised_Button.setVisibility(View.GONE);
                            }
                        }, 100);
                    } else {
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //Do something after 100ms
                                mProgressDilaog.dismiss();
                                mSubmit_Raised_Button.setText(AppStrings.str_scan);
                                mSubmit_Raised_Button.setVisibility(View.GONE);
                                retry_Raised_Button.setVisibility(View.VISIBLE);
                                skip_Raised_Button.setVisibility(View.VISIBLE);
                            }
                        }, 100);
                    }*/
                } else {
                    fragment_start_scan1.setEnabled(true);
                    if (mProgressDilaog != null && mProgressDilaog.isShowing())
                        mProgressDilaog.dismiss();

                    //Perform your functionality
                    //   sendRequest();

                }

                break;
            case R.id.fragment_Submitbutton:

               /* if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {
                    JSONObject jsonobj = new JSONObject();
                    try {
                        jsonobj.put("member_id", value.getId());
                        RestClient.getRestClient(EnrollFingerPrint.this).callRestWebService(Constants.VERIFY_LIST, jsonobj.toString(), getActivity(), ServiceType.VERIFY_LIST);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }*/

               /* if (!MySharedPreference.readBoolean(getActivity(), MySharedPreference.FINGER_FLAG1, false)) {
                    Toast.makeText(getActivity(), "Finger 1 Biometric is empty!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (!MySharedPreference.readBoolean(getActivity(), MySharedPreference.FINGER_FLAG2, false)) {
                    Toast.makeText(getActivity(), "Finger 2 Biometric is empty!", Toast.LENGTH_SHORT).show();
                    return;
                }*/
                try {
                    EnrollFP fp = new EnrollFP();
                    fp.setShgId(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
                    fp.setUserId(MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, ""));
                    fp.setId(id);
                    fp.setFinger1(finger1);
                    fp.setFinger2(finger2);
                    fp.setName(edit_Name.getText().toString());

                    callServer(fp);

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            EnrollFPData.insertFPData(fp);
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

        }

    }

    int timeout = 10000;
    DeviceInfo deviceIfo = null;
    byte[] Enroll_Template;
    byte[] isoFeatureSet;
    Bitmap bitmapImg;
    int mfsVer = 41;

    private String scan() {
        String encodedBioMetricInfo = null;
        bitmapImg = StartSyncCapture();
        if (MySharedPreference.readBoolean(getActivity(), MySharedPreference.FINGER_FLAG1, false))
            img_fp.setImageBitmap(bitmapImg);

        if (MySharedPreference.readBoolean(getActivity(), MySharedPreference.FINGER_FLAG2, false))
            img_fp1.setImageBitmap(bitmapImg);

        isoFeatureSet = Enroll_Template;
        try {
            if (isoFeatureSet.length > 0) {
              //  Log.e("MemberUpdateAuthAct", "isoFeatureSet..." + Arrays.toString(isoFeatureSet));
//                        findViewById(R.id.btnCancel).setVisibility(View.VISIBLE);

                encodedBioMetricInfo = Base64.encodeToString(isoFeatureSet, Base64.DEFAULT);
                String data = Encrypt(encodedBioMetricInfo, Utils.ENCRYPT_DECRYPT_KEY);

                return data;
            }

        } catch (Exception e) {
            fragment_start_scan.setEnabled(true);
            fragment_start_scan1.setEnabled(true);
            if (mProgressDilaog != null && mProgressDilaog.isShowing())
                mProgressDilaog.dismiss();

        }
        return null;
    }


    @Override
    public void onResume() {
        super.onResume();

    }


    private void InitScanner() {
        try {
            int ret = Utils.mfs100.Init();
            if (ret != 0) {
                //   SetTextOnUIThread(Utils.mfs100.GetErrorMsg(ret));
                Toast.makeText(getActivity(), Utils.mfs100.GetErrorMsg(ret), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity(), "Init success", Toast.LENGTH_SHORT).show();
                String info = "Serial: " + Utils.mfs100.GetDeviceInfo().SerialNo()
                        + " Make: " + Utils.mfs100.GetDeviceInfo().Make()
                        + " Model: " + Utils.mfs100.GetDeviceInfo().Model()
                        + "\nCertificate: " + Utils.mfs100.GetCertification();
                // SetLogOnUIThread(info);
            }
        } catch (Exception ex) {
            Toast.makeText(getActivity(), "Init failed, unhandled exception",
                    Toast.LENGTH_LONG).show();
            //  SetTextOnUIThread();
            Toast.makeText(getActivity(), "Init failed, unhandled exception", Toast.LENGTH_SHORT).show();
        }
    }

    private Bitmap StartSyncCapture() {
        try {
            Thread trd = new Thread(new Runnable() {
                @Override
                public void run() {
                    FingerData fingerData = new FingerData();
                    int ret = Utils.mfs100.AutoCapture(fingerData, timeout, false);
                    Log.e("sample app", "ret value..." + ret);
                    Log.e("sample app", "ret value..." + Utils.mfs100.GetErrorMsg(ret));
                    if (ret != 0) {
                        Toast.makeText(getActivity(), Utils.mfs100.GetErrorMsg(ret), Toast.LENGTH_SHORT).show();
                        Enroll_Template = null;
                        bitmapImg = BitmapFactory.decodeResource(getResources(), R.drawable.thumb_impress);
                        // bitmapImg = icon;

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                img_fp.setImageBitmap(bitmapImg);
                                img_fp1.setImageBitmap(bitmapImg);


                            }
                        });


                    } else {
                        Enroll_Template = new byte[fingerData.ISOTemplate().length];
                        System.arraycopy(fingerData.ISOTemplate(), 0, Enroll_Template, 0, fingerData.ISOTemplate().length);
                        bitmapImg = BitmapFactory.decodeByteArray(fingerData.FingerImage(), 0, fingerData.FingerImage().length);
                        //     img_fp.setImageBitmap(bitmapImg);

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (MySharedPreference.readBoolean(getActivity(), MySharedPreference.FINGER_FLAG1, false))
                                    img_fp.setImageBitmap(bitmapImg);
                                if (MySharedPreference.readBoolean(getActivity(), MySharedPreference.FINGER_FLAG2, false))
                                    img_fp1.setImageBitmap(bitmapImg);


                                //fragment_start_scan.setEnabled(false);


                            }
                        });
                    }
                }
            });
            trd.run();
            trd.join();
        } catch (Exception ex) {
            ex.printStackTrace();

            //CommonMethod.writeLog("Exception in ContinuesScan(). Message:- " + ex.getMessage());
        } finally {
            fragment_start_scan.setEnabled(true);
            fragment_start_scan1.setEnabled(true);
            if (mProgressDilaog != null && mProgressDilaog.isShowing())
                mProgressDilaog.dismiss();

        }

        return bitmapImg;
    }


    public String Encrypt(String text, String Key) throws Exception {

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        byte[] keyBytes = new byte[16];
        byte[] pwdBytes = Key.getBytes();
        int len = pwdBytes.length;
        if (len > keyBytes.length)
            len = keyBytes.length;
        System.arraycopy(pwdBytes, 0, keyBytes, 0, len);
        SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
        IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);

        byte[] results = cipher.doFinal(text.getBytes("UTF-8"));
        String value = Base64.encodeToString(results, Base64.DEFAULT);
        //  Log.e("TAG", "byte array in encryption..."+results.toString());
        Log.e("TAG", "base 64 in encryption..." + new String(Base64.encode(results, Base64.DEFAULT), "UTF-8"));
        return value; // it returns the result as a String
    }


    public byte[] Decrypt(String text, String Key) throws Exception {

        byte[] textbytes = Base64.decode(text, Base64.DEFAULT);
        if (Key.length() != 16) {
            throw new IllegalArgumentException("Invalid key size.");
        }

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        byte[] keyBytes = new byte[16];
        byte[] pwdBytes = Key.getBytes();
        int len = pwdBytes.length;
        if (len > keyBytes.length)
            len = keyBytes.length;
        System.arraycopy(pwdBytes, 0, keyBytes, 0, len);
        SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
        IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);
        cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);

        //byte[] results = cipher.doFinal(text.getBytes("UTF-8"));
        byte[] results = cipher.doFinal(textbytes);
        //  Log.e("TAG", "byte array in encryption..."+results.toString());
        Log.e("TAG", "base 64 in decryption..." + new String(Base64.decode(results, Base64.DEFAULT), "UTF-8"));
        return results; // it returns the result as a String
    }

    private void callServer(EnrollFP pidDate) {
        try {
            Log.e("Encrypt req", new Gson().toJson(pidDate));

            JSONObject jsonobj = new JSONObject();
            jsonobj.put("member_id", pidDate.getUserId()+":"+pidDate.getId());

            jsonobj.put("user_id", pidDate.getUserId());

            jsonobj.put("fid_1", finger1);

            jsonobj.put("fid_2", finger2);

            onTaskStarted();
            RestClient.getRestClient(EnrollFingerPrint.this).callRestWebService(Constants.ENROLL_FINGERPRINT, jsonobj.toString(), getActivity(), ServiceType.ENROLL_FP);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void logLargeString(String str) {
        if (str.length() > 3000) {
            Log.e("TAG", "encrypted Req log..." + str.substring(0, 3000));
            logLargeString(str.substring(3000));
        } else {
            Log.e("TAG", "" + str); // continuation
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 2:
                if (resultCode == Activity.RESULT_OK) {
                    try {
                        if (data != null) {
                            pidDate = data.getStringExtra("PID_DATA");
                            //  parseXmlData(pidDate);
                            Log.e("TAG", "PID DATA : " + "Successfully recieved");
                            Serializer serializer = new Persister();
                            //   PidData pidData = serializer.read(PidData.class, result);

                        }
                        fragment_start_scan.setEnabled(true);
                        if (mProgressDilaog != null && mProgressDilaog.isShowing())
                            mProgressDilaog.dismiss();
                    } catch (Exception e) {
                        Log.e("Error", "Error while deserialize pid data", e);
                        fragment_start_scan.setEnabled(true);
                        if (mProgressDilaog != null && mProgressDilaog.isShowing())
                            mProgressDilaog.dismiss();

                    }
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    private void RdScan() {
        try {
            String pidOption = getPIDOptions();
            Log.e("TAG", "PID OPTION : " + pidOption);
            if (pidOption != null) {
                Intent intent2 = new Intent();
                intent2.setAction("in.gov.uidai.rdservice.fp.CAPTURE");
                intent2.putExtra("PID_OPTIONS", pidOption);
                startActivityForResult(intent2, 2);
            }
        } catch (Exception e) {
            Log.e("Error", e.toString());
            if (mProgressDilaog != null && mProgressDilaog.isShowing())
                mProgressDilaog.dismiss();
            fragment_start_scan.setEnabled(true);
        }
    }

    private String getPIDOptions() {
        try {
            int fingerCount = 1;
            int fingerType = 0;
            int fingerFormat = 1;
            String pidVer = "2.0";
            String timeOut = "10000";
            String posh = "UNKNOWN";


            Opts opts = new Opts();
            opts.fCount = String.valueOf(fingerCount);
            opts.fType = String.valueOf(fingerType);
            opts.iCount = "0";
            opts.iType = "0";
            opts.pCount = "0";
            opts.pType = "0";
            opts.format = String.valueOf(fingerFormat);
            opts.pidVer = pidVer;
            opts.timeout = timeOut;
//            opts.otp = "";
            opts.posh = posh;
            String env = "P";
          /*  switch (rgEnv.getCheckedRadioButtonId()) {
                case R.id.rbStage:
                    env = "S";
                    break;
                case R.id.rbPreProd:
                    env = "PP";
                    break;
                case R.id.rbProd:
                    env = "P";
                    break;
            }*/
            opts.env = env;

            PidOptions pidOptions = new PidOptions();
            pidOptions.ver = pidVer;
            pidOptions.Opts = opts;

            Serializer serializer = new Persister();
            StringWriter writer = new StringWriter();
            serializer.write(pidOptions, writer);
            return writer.toString();
        } catch (Exception e) {
            Log.e("Error", e.toString());
        }
        return null;
    }


    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();
    }

    private byte[] finger1bytes, finger2bytes;

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
            mProgressDilaog.dismiss();
            mProgressDilaog = null;
        }
        switch (serviceType) {
            case ENROLL_FP:
                //getAadhaarResponse(result);  //TODO::
                if (result != null && result.length() > 0) {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    VerifyFPDto mrDto = gson.fromJson(result, VerifyFPDto.class);
                    if (mrDto.getIsSuccess() == 1) {
                        Utils.showToast(getActivity(), mrDto.getResponseObject().getMessage());
                        Utils.showToast(getActivity(), "Enrolled success!");
                        NewDrawerScreen.showFragment(new MainFragment());
                    } else {
                        Utils.showToast(getActivity(), mrDto.getResponseObject().getMessage());
                        Utils.showToast(getActivity(), "Enrolled failed!");
                    }
                } else {
                    Utils.showToast(getActivity(), "Enrolled failed!");
                }
                break;

            case VERIFY_LIST:
                if (result != null && result.length() > 0) {
                    byte[] finger1Local = null;
                    byte[] finger2Local = null;


                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    VerifyFPDto mrDto = gson.fromJson(result, VerifyFPDto.class);
                    if (finger1.equals("") || finger1.isEmpty()) {
                        Toast.makeText(getActivity(), "Add two fingers must", Toast.LENGTH_SHORT).show();
                    } else if (finger2.equals("") || finger2.isEmpty()) {
                        Toast.makeText(getActivity(), "Add two fingers must", Toast.LENGTH_SHORT).show();
                    } else if (mrDto.getIsSuccess() == 1) {
                        Toast.makeText(getActivity(), "User Already Enrolled ", Toast.LENGTH_SHORT).show();
                        try {
                            finger1bytes = Base64.decode(Decrypt(mrDto.getResponseObject().getFid_1(), Utils.ENCRYPT_DECRYPT_KEY), Base64.DEFAULT);
                            finger2bytes = Base64.decode(Decrypt(mrDto.getResponseObject().getFid_2(), Utils.ENCRYPT_DECRYPT_KEY), Base64.DEFAULT);
                        //    Log.e("finger1bytes", mrDto.getResponseObject().getFid_1());
                         //   Log.e("finger2bytes", mrDto.getResponseObject().getFid_2());

                            try {
                                finger1Local = Base64.decode(Decrypt(finger1, Utils.ENCRYPT_DECRYPT_KEY), Base64.DEFAULT);
                                finger2Local = Base64.decode(Decrypt(finger2, Utils.ENCRYPT_DECRYPT_KEY), Base64.DEFAULT);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            Verify(finger1bytes, finger2bytes, finger1Local, finger2Local);
                       /* finger1bytes = Decrypt(mrDto.getResponseObject().getFid_1(), Utils.ENCRYPT_DECRYPT_KEY);
                        finger2bytes = Decrypt(mrDto.getResponseObject().getFid_2(), Utils.ENCRYPT_DECRYPT_KEY);*/
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {

                    }
                }
                break;

            case GET_ENROLLMENT_LIST:
                //getAadhaarResponse(result);  //TODO::
                if (result != null && result.length() > 0) {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    ResponseDto lrDto = gson.fromJson(result, ResponseDto.class);
                    int statusCode = lrDto.getStatusCode();
                    String message = lrDto.getMessage();
                    Log.d("response status", " " + statusCode);
                    if (statusCode == 400 || statusCode == 401 || statusCode == 403 || statusCode == 500 || statusCode == 503) {
                        // showMessage(statusCode);
                        Utils.showToast(getActivity(), message);

                    } else if (statusCode == Utils.Success_Code) {
                        Utils.showToast(getActivity(), message);
                        if (lrDto.getResponseContents() != null) {
                            if (lrDto.getResponseContents() != null) {
                                responseContentsList.clear();
                                ResponseContents rs = new ResponseContents();
                                rs.setDesignation("SELECT ENROLLER");
                                responseContentsList.add(rs);

                                for (int i = 0; i < lrDto.getResponseContents().size(); i++) {
                                    ResponseContents contents = new ResponseContents();
                                    contents.setDesignation(lrDto.getResponseContents().get(i).getDesignation());
                                    contents.setName(lrDto.getResponseContents().get(i).getName());
                                    contents.setId(lrDto.getResponseContents().get(i).getId());
                                    contents.setShgId(shgDto.getShgId());
                                    responseContentsList.add(contents);
                                }

                                EnrollFpAdapter aa = new EnrollFpAdapter(getActivity(), responseContentsList);
                                spinner_enrollfinger.setAdapter(aa);

                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        EnrollFPData.insertENROLLMEMBER(responseContentsList);
                                    }
                                });

                            }
                        }
                    }
                } else {

                }
                break;
        }

    }

    private void Verify(byte[] finger1bytes, byte[] finger2bytes, byte[] finger1Local, byte[] finger2Local) {

        if (finger1bytes.length > 0) {

            try {

                int ret = Utils.mfs100.MatchISO(finger1Local, finger1bytes);
                Utils.showToast(getActivity(), ret + "");

                if (ret >= 96) {
                    fin1Flag = true;

                }

            } catch (Exception ee) {
                ee.printStackTrace();
            }
        } else if (finger2bytes.length > 0) {

            try {

                int ret = Utils.mfs100.MatchISO(finger2Local, finger2bytes);
                Utils.showToast(getActivity(), ret + "");

                if (ret >= 96) {
                    fin2Flag = true;
                }

            } catch (Exception ee) {
                ee.printStackTrace();
            }


        }


    }

    private long mLastAttTime = 0l;


    @Override
    public void OnDeviceAttached(int vid, int pid, boolean hasPermission) {
        if (SystemClock.elapsedRealtime() - mLastAttTime < Threshold) {
            return;
        }
        if (!globalVariable.is_OTG()) {
            return;
        }
        mLastAttTime = SystemClock.elapsedRealtime();
        int ret;
        if (!hasPermission) {
            Toast.makeText(getActivity(), "Permission denied", Toast.LENGTH_SHORT).show();
            return;
        }
        try {
            if (vid == 1204 || vid == 11279) {
                if (pid == 34323) {
                    ret = Utils.mfs100.LoadFirmware();
                    if (ret != 0) {
                        Toast.makeText(getActivity(), Utils.mfs100.GetErrorMsg(ret), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), "Load firmware success", Toast.LENGTH_SHORT).show();
                    }
                } else if (pid == 4101) {
                    String key = "Without Key";
                    ret = Utils.mfs100.Init();
                    if (ret == 0) {
                        showSuccessLog(key);
                    } else {
                        //     SetTextOnUIThread(Utils.mfs100.GetErrorMsg(ret));
                        Toast.makeText(getActivity(), Utils.mfs100.GetErrorMsg(ret), Toast.LENGTH_SHORT).show();
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showSuccessLog(String key) {
        try {
            Toast.makeText(getActivity(), "Init success", Toast.LENGTH_SHORT).show();
            String info = "\nKey: " + key + "\nSerial: "
                    + Utils.mfs100.GetDeviceInfo().SerialNo() + " Make: "
                    + Utils.mfs100.GetDeviceInfo().Make() + " Model: "
                    + Utils.mfs100.GetDeviceInfo().Model()
                    + "\nCertificate: " + Utils.mfs100.GetCertification();
            Toast.makeText(getActivity(), info, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
        }
    }

    /*   @Override
       public void OnPreview(FingerData fingerData) {

       }

       @Override
       public void OnCaptureCompleted(boolean b, int i, String s, FingerData fingerData) {

       }
   */
    long mLastDttTime = 0l;

    @Override
    public void OnDeviceDetached() {
        try {
            if (SystemClock.elapsedRealtime() - mLastDttTime < Threshold) {
                return;
            }
            mLastDttTime = SystemClock.elapsedRealtime();
            UnInitScanner();

            Toast.makeText(getActivity(), "Device removed", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
        }
    }

    @Override
    public void OnHostCheckFailed(String s) {

    }


    public void UnInitScanner() {
        try {
            deviceIfo = null;
            if (Utils.mfs100 != null) {
                int ret = Utils.mfs100.UnInit();
                if (ret != 0) {
//                    Toast.makeText(MemberUpdateAuthenticationActivity.this, Util.mfs100.GetErrorMsg(ret), Toast.LENGTH_SHORT).show();
                } else {
//                    Toast.makeText(MemberUpdateAuthenticationActivity.this, "Uninit Success", Toast.LENGTH_SHORT).show();
                }
            }
        } catch (Exception e) {
            Log.e("UnInitScanner.EX", e.toString());
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        value = (ResponseContents) adapterView.getItemAtPosition(i);
        if (value != null && !value.getDesignation().trim().equals("SELECT ENROLLER")) {
            id = value.getId();
            edit_Name.setText(value.getName());
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
