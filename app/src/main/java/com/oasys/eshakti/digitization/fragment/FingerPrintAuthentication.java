package com.oasys.eshakti.digitization.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.icu.util.GregorianCalendar;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.oasys.eshakti.digitization.Adapter.WalletBankIdAdapter;
import com.oasys.eshakti.digitization.Dto.AEPSDto;
import com.oasys.eshakti.digitization.Dto.AadhaarPayDto;
import com.oasys.eshakti.digitization.Dto.EncryptedAadhaarPayRequestDto;
import com.oasys.eshakti.digitization.Dto.EncryptedAadhaarPayResponseDto;
import com.oasys.eshakti.digitization.Dto.IINDto;
import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.Dto.MantraDto.Opts;
import com.oasys.eshakti.digitization.Dto.MantraDto.PidOptions;
import com.oasys.eshakti.digitization.Dto.MemMasterDto;
import com.oasys.eshakti.digitization.Dto.MemberList;
import com.oasys.eshakti.digitization.Dto.ResponseContents;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.Dto.ShgMasDto;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.OasysUtils.AadhaarVerhoeffAlgorithm;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.activity.LoginActivity;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;
import com.oasys.eshakti.digitization.database.SHGTable;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.StringWriter;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


public class FingerPrintAuthentication extends Fragment implements View.OnClickListener, NewTaskListener {

    public static int count = 1;
    private View rootView;
    private Dialog mProgressDilaog;
    private Button mSubmit_Raised_Button, skip_Raised_Button, retry_Raised_Button;
    private TextView mGroupName;
    private TextView mCashinHand;
    private TextView mCashatBank;
    private TextView Wallet_balance, Wallet_CIH;
    private TextView mHeader, amountStr, nameStr;
    private EditText uidStr;
    private ListOfShg shgDto;
    private TableLayout mLeftHeaderTable;
    String width[] = {AppStrings.memberName, AppStrings.savingsAmount};
    int[] rightHeaderWidth = new int[width.length];
    int[] rightContentWidth = new int[width.length];
    private int amt;
    private String uid;
    private String encryptedPidXmlStr;
    private String encryptedSkey;
    private String encryptedHmacStr;
    private String mc, mi, rdsVer, rdsId, dpId, udc, ci;
    private String pidDate;
    private NetworkConnection networkConnection;
    int counter = 3;
    private int mSize;
    private Spinner mCaspayment_spinner;
    private String namejson, idjson, bankiidjson, statusjson, codejson;
    public ArrayList<ResponseContents> responseContentsList = new ArrayList<>();
    private ResponseContents rsContent;
    private MemberList member;
    private ImageView img_fp;

    private static final int MAX_LENGTH = 12;
    private static final int MIN_LENGTH = 4;

    String a;
    int keyDel;
    private TextToSpeech textToSpeech;
    private String toSpeak;
    public String userId;
    private int mWalletamount;
    private int mAniiamtorCH;
    private RestClient restClient;
    private TextView daily_date_time;
    private String currentDateStr;
    DateFormat df = new SimpleDateFormat("dd/MM/yyyy");


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.frag_authentication_ly, container, false);

        try {
            toSpeak = "Place Your FINGER on FINGER PRINT Scanner";
            shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
            networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
            userId = MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, "");

            Bundle bundle = getArguments();
            amt = bundle.getInt("amt");
            uid = bundle.getString("uid");
            member = (MemberList) bundle.getSerializable("member");

            if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {
                //      onTaskStarted();

                RestClient.getRestClient(FingerPrintAuthentication.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.WALLETCASH_INHAND + userId, getActivity(), ServiceType.WALLET_CASHIN_HAND);
                RestClient.getRestClient(this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.WALL_BAL + userId, getActivity(), ServiceType.WALL_TXANIMATOR);
            } else {
                Utils.showToast(getActivity(), "Network Not Available");
            }

            Calendar calender = Calendar.getInstance();
            currentDateStr = df.format(calender.getTime());

            daily_date_time = (TextView) rootView.findViewById(R.id.daily_date_time);
            daily_date_time.setText(currentDateStr);

            mGroupName = (TextView) rootView.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
            mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashinHand.setTypeface(LoginActivity.sTypeface);

            mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
            mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashatBank.setTypeface(LoginActivity.sTypeface);
            /** UI Mapping **/

            Wallet_balance = (TextView) rootView.findViewById(R.id.Wallet_balance);
//            Wallet_balance.setText(AppStrings.walletbalance + shgDto.getCashInHand());
//            Wallet_balance.setTypeface(LoginActivity.sTypeface);

            Wallet_CIH = (TextView) rootView.findViewById(R.id.Wallet_CIH);
//            Wallet_CIH.setText(AppStrings.walletCIH + "₹2100");
//            Wallet_CIH.setTypeface(LoginActivity.sTypeface);

            mCaspayment_spinner = (Spinner) rootView.findViewById(R.id.mCaspayment_spinner);
            amountStr = (TextView) rootView.findViewById(R.id.amount);
            amountStr.setText(amt + "");

            textToSpeech = new TextToSpeech(getActivity().getApplicationContext(), new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    if (status == TextToSpeech.SUCCESS) {

                        textToSpeech.setLanguage(Locale.US);
                        textToSpeech.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);

                    }
                }
            });

            img_fp = (ImageView) rootView.findViewById(R.id.img_fp);
            uidStr = (EditText) rootView.findViewById(R.id.uid);

            uidStr.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                    boolean flag = true;
                    String eachBlock[] = uidStr.getText().toString().split("-");
                    for (int i = 0; i < eachBlock.length; i++) {
                        if (eachBlock[i].length() > 4) {
                            flag = false;
                        }
                    }
                    if (flag) {
                        uidStr.setOnKeyListener(new View.OnKeyListener() {

                            @Override
                            public boolean onKey(View v, int keyCode, KeyEvent event) {

                                if (keyCode == KeyEvent.KEYCODE_DEL)
                                    keyDel = 1;
                                return false;
                            }
                        });

                        if (keyDel == 0) {

                            if (((uidStr.getText().length() + 1) % 5) == 0) {

                                if (uidStr.getText().toString().split("-").length <= 3) {
                                    uidStr.setText(uidStr.getText() + "-");
                                    uidStr.setSelection(uidStr.getText().length());
                                }
                            }
                            a = uidStr.getText().toString();
                        } else {
                            a = uidStr.getText().toString();
                            keyDel = 0;
                        }

                    } else {
                        uidStr.setText(a);
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {


                }
            });

            uidStr.setTransformationMethod(new MyPasswordTransformationMethod());
            uidStr.setText(uid);

            nameStr = (TextView) rootView.findViewById(R.id.name);
            nameStr.setText(member.getMemberName());

            mCaspayment_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                   /* namejson = responseContentsList.get(i).getName();
                    idjson = responseContentsList.get(i).getId();
                    bankiidjson = responseContentsList.get(i).getBankIID();
                    statusjson = responseContentsList.get(i).getBankStatus();
                    codejson = responseContentsList.get(i).getCode();*/

                    rsContent = (ResponseContents) adapterView.getItemAtPosition(i);


                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            mLeftHeaderTable = (TableLayout) rootView.findViewById(R.id.LeftHeaderTable);

            TableRow leftHeaderRow = new TableRow(getActivity());

            TableRow.LayoutParams lHeaderParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);

            TextView mMemberName_headerText = new TextView(getActivity());
            mMemberName_headerText
                    .setText("" + String.valueOf(AppStrings.fng_prnt_auth));
            mMemberName_headerText.setTypeface(LoginActivity.sTypeface);
            mMemberName_headerText.setTextColor(Color.WHITE);
            mMemberName_headerText.setPadding(10, 5, 10, 5);
            mMemberName_headerText.setLayoutParams(lHeaderParams);
            leftHeaderRow.addView(mMemberName_headerText);
            mLeftHeaderTable.addView(leftHeaderRow);

            getTableRowHeaderCellWidth();

            mHeader = (TextView) rootView.findViewById(R.id.fragmentHeader);
            mHeader.setText(AppStrings.mCashWithdrawal);
            mHeader.setTypeface(LoginActivity.sTypeface);
            mSubmit_Raised_Button = (Button) rootView.findViewById(R.id.fragment_Submit_button);
            mSubmit_Raised_Button.setText(AppStrings.str_scan);
            mSubmit_Raised_Button.setTypeface(LoginActivity.sTypeface);
            mSubmit_Raised_Button.setOnClickListener(this);


            retry_Raised_Button = (Button) rootView.findViewById(R.id.fragment_Previousbutton);
            retry_Raised_Button.setText(AppStrings.retry);
            retry_Raised_Button.setTypeface(LoginActivity.sTypeface);
            retry_Raised_Button.setOnClickListener(this);

            skip_Raised_Button = (Button) rootView.findViewById(R.id.fragment_Nextbutton);
            skip_Raised_Button.setText(AppStrings.skip);
            skip_Raised_Button.setTypeface(LoginActivity.sTypeface);
            skip_Raised_Button.setOnClickListener(this);
            retry_Raised_Button.setVisibility(View.GONE);
            skip_Raised_Button.setVisibility(View.GONE);

            if (networkConnection.isNetworkAvailable()) {
                EShaktiApplication.setOfflineTrans(false);
                onTaskStarted();
                String url = Constants.BASE_URL + Constants.BANK_CW;
                RestClient.getRestClient(FingerPrintAuthentication.this).callWebServiceForGetMethod(url, getActivity(), ServiceType.WALLET_B_CW);

            }


        } catch (Exception E) {
            E.printStackTrace();
        }
        return rootView;
    }
    // maskAadhaarNumber(updatedText, "XXXXXXXX####")

    public static class MyPasswordTransformationMethod extends PasswordTransformationMethod {
        @Override
        public CharSequence getTransformation(CharSequence source, View view) {
            return new PasswordCharSequence(source);
        }

        private class PasswordCharSequence implements CharSequence {
            private CharSequence mSource;

            public PasswordCharSequence(CharSequence source) {
                mSource = source;
            }

            public char charAt(int index) {
                char caracter;
                switch (index) {
                    case 4:
                        caracter = ' ';
                        break;
                    case 9:
                        caracter = ' ';
                        break;
                    case 14:
                        caracter = ' ';
                        break;
                    default:
                        if (index < 10)
                            return '*';
                        else
                            caracter = mSource.charAt(index);
                        break;


                }


                return caracter;
            }

            public int length() {
                return mSource.length();
            }

            public CharSequence subSequence(int start, int end) {
                return mSource.subSequence(start, end);
            }
        }
    }

    public static String maskAadhaarNumber(String uid, String mask) {
        int index = 0;
        StringBuilder maskedNumber = new StringBuilder();

        // if (mask.length() == 12) {
        for (int i = 0; i < mask.length(); i++) {
            char c = mask.charAt(i);
            if (c == '#') {
                maskedNumber.append(uid.charAt(index));
                index++;
            } else if (c == 'X') {
                maskedNumber.append(c);
                index++;
            } else {
                maskedNumber.append(c);
            }
        }

        return maskedNumber.toString();
    }

    private void getTableRowHeaderCellWidth() {
        int lefHeaderChildCount = ((TableRow) mLeftHeaderTable.getChildAt(0)).getChildCount();

        for (int x = 0; x < (lefHeaderChildCount); x++) {
            if (x == 0) {
                rightHeaderWidth[x] = viewWidth(((TableRow) mLeftHeaderTable.getChildAt(0)).getChildAt(x));
            }
        }
    }

    private int viewWidth(View view) {
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        return view.getMeasuredWidth();
    }

    @Override
    public void onPause() {
       /* if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }*/
        super.onPause();

    }

    @Override
    public void onStop() {
        if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
        super.onStop();
    }

    @Override
    public void onDestroy() {
        if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.fragment_Previousbutton:
                mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
                mProgressDilaog.show();
                toSpeak = "Place Your FINGER on FINGER PRINT Scanner";
                textToSpeech.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
                if (mSubmit_Raised_Button.getText().equals(AppStrings.str_scan)) {
                    if (uidStr.getText().toString().length() <= 0) {
                        Toast.makeText(getActivity(), "Enter Aadhaar No.", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    AadhaarVerhoeffAlgorithm aadhaarVerhoeffAlgorithm = new AadhaarVerhoeffAlgorithm();
                    Boolean isAadharNumber = aadhaarVerhoeffAlgorithm.validateVerhoeff(uidStr.getText().toString());
                    if (!isAadharNumber) {
                        Toast.makeText(getActivity(), "Enter valid Aadhaar No.", Toast.LENGTH_SHORT).show();
                        return;
                    }


                    final EShaktiApplication globalVariable = (EShaktiApplication) getActivity().getApplicationContext();
                    if (globalVariable.is_OTG()) {
                        RdScan();
                    } else {
                        Toast.makeText(getActivity(), "Enable OTG device connectivity!", Toast.LENGTH_SHORT).show();
                    }

                    mProgressDilaog.dismiss();
                   /* count++;
                    mSubmit_Raised_Button.setText(AppStrings.scanning);

                    if (count % 2 != 0) {
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //Do something after 100ms
                                mProgressDilaog.dismiss();
                                mSubmit_Raised_Button.setText(AppStrings.continue_flow);
                                mSubmit_Raised_Button.setVisibility(View.VISIBLE);
                                retry_Raised_Button.setVisibility(View.GONE);
                                skip_Raised_Button.setVisibility(View.GONE);
                            }
                        }, 100);
                    } else {
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //Do something after 100ms
                                mProgressDilaog.dismiss();
                                mSubmit_Raised_Button.setText(AppStrings.str_scan);
                                mSubmit_Raised_Button.setVisibility(View.GONE);
                                retry_Raised_Button.setVisibility(View.VISIBLE);
                                skip_Raised_Button.setVisibility(View.VISIBLE);
                            }
                        }, 100);
                    }*/
                } else {
                    mProgressDilaog.dismiss();


                    //Perform your functionality
                    //  sendRequest();

                    //     MemberDrawerScreen.showFragment(new Transactionpaymentsuccess());
                }
                break;
            case R.id.fragment_Submit_button:
                mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
                mProgressDilaog.show();

                textToSpeech.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
                if (mSubmit_Raised_Button.getText().equals(AppStrings.str_scan)) {
                    //   mProgressDilaog.dismiss();
                    mSubmit_Raised_Button.setEnabled(false);
                    img_fp.setImageResource(R.drawable.thumb_impress);
                    if (uidStr.getText().toString().length() <= 0) {
                        Toast.makeText(getActivity(), "Enter Aadhaar No.", Toast.LENGTH_SHORT).show();
                        mSubmit_Raised_Button.setEnabled(true);
                        if (mProgressDilaog != null && mProgressDilaog.isShowing())
                            mProgressDilaog.dismiss();
                        return;
                    }

                    AadhaarVerhoeffAlgorithm aadhaarVerhoeffAlgorithm = new AadhaarVerhoeffAlgorithm();
                    Boolean isAadharNumber = aadhaarVerhoeffAlgorithm.validateVerhoeff(uidStr.getText().toString().replaceAll("[-\\[\\]^/,'*:.!><~@#$%+=?|\"\\\\()]+", ""));
                    if (!isAadharNumber) {
                        mSubmit_Raised_Button.setEnabled(true);
                        if (mProgressDilaog != null && mProgressDilaog.isShowing())
                            mProgressDilaog.dismiss();
                        Toast.makeText(getActivity(), "VerhoeffAlgorithm validation failed! Kindly enter valid aadhaar no.", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    final EShaktiApplication globalVariable = (EShaktiApplication) getActivity().getApplicationContext();
                    if (globalVariable.is_OTG()) {
                        RdScan();
                    } else {
                        mSubmit_Raised_Button.setEnabled(true);
                        if (mProgressDilaog != null && mProgressDilaog.isShowing())
                            mProgressDilaog.dismiss();
                        Toast.makeText(getActivity(), "Enable OTG device connectivity!", Toast.LENGTH_SHORT).show();
                    }
                 /*   count++;
                    mSubmit_Raised_Button.setText(AppStrings.scanning);

                    if (count % 2 != 0) {
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //Do something after 100ms
                                mProgressDilaog.dismiss();
                                mSubmit_Raised_Button.setText(AppStrings.continue_flow);
                                mSubmit_Raised_Button.setVisibility(View.VISIBLE);
                                retry_Raised_Button.setVisibility(View.GONE);
                                skip_Raised_Button.setVisibility(View.GONE);
                            }
                        }, 100);
                    } else {
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //Do something after 100ms
                                mProgressDilaog.dismiss();
                                mSubmit_Raised_Button.setText(AppStrings.str_scan);
                                mSubmit_Raised_Button.setVisibility(View.GONE);
                                retry_Raised_Button.setVisibility(View.VISIBLE);
                                skip_Raised_Button.setVisibility(View.VISIBLE);
                            }
                        }, 100);
                    }*/
                } else {
                    if (mProgressDilaog != null && mProgressDilaog.isShowing())
                        mProgressDilaog.dismiss();



                    //Perform your functionality
                    //   sendRequest();


                }
                break;
            case R.id.fragment_Nextbutton:
                FragmentManager fm = getFragmentManager();
                fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                NewDrawerScreen.showFragment(new CashWithdrawal());
                break;
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 2:
                if (resultCode == Activity.RESULT_OK) {
                    try {
                        if (data != null) {
                            pidDate = data.getStringExtra("PID_DATA");
                            parseXmlData(pidDate);
                            Log.e("TAG", "PID DATA : " + "Successfully recieved");
                            Serializer serializer = new Persister();
                            //   PidData pidData = serializer.read(PidData.class, result);

                        }
                    } catch (Exception e) {
                        Log.e("Error", "Error while deserialize pid data", e);
                        mSubmit_Raised_Button.setEnabled(true);
                        if (mProgressDilaog != null && mProgressDilaog.isShowing())
                            mProgressDilaog.dismiss();

                    }
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /*  @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 2:
                if (resultCode == Activity.RESULT_OK) {
                    try {
                        if (data != null) {
                            String result = data.getStringExtra("PID_DATA");
                            parseXmlData(result);
                            Log.e("TAG","PID DATA : "+result);
                            Serializer serializer = new Persister();
                            //  PidData pidData = serializer.read(PidData.class, result);

                        }
                    } catch (Exception e) {
                        Log.e("Error", "Error while deserialize pid data", e);
                    }
                }
                break;
        }
    }*/

    /*private void parseXmlData(String xmlData) {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(new InputSource(new java.io.StringReader(xmlData)));
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("PidData");
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    NamedNodeMap mapResp = eElement.getElementsByTagName("Resp").item(0).getAttributes();
                    errorCode = ((Node)mapResp.getNamedItem("errCode")).getTextContent();
                    errorInfo = ((Node)mapResp.getNamedItem("errInfo")).getTextContent();
//                    if(!errorInfo.equalsIgnoreCase("")) {
//                        GlobalVariables.setFailMsg(errorInfo);
//                    }
//                    errorInfo = "";
                    fCount = ((Node)mapResp.getNamedItem("fCount")).getTextContent();
                    fType = ((Node)mapResp.getNamedItem("fType")).getTextContent();
                    iCount = ((Node)mapResp.getNamedItem("iCount")).getTextContent();
                    iType = ((Node)mapResp.getNamedItem("iType")).getTextContent();
                    nmPoints = ((Node)mapResp.getNamedItem("nmPoints")).getTextContent();
                    pCount = ((Node)mapResp.getNamedItem("pCount")).getTextContent();
                    pType = ((Node)mapResp.getNamedItem("pType")).getTextContent();
                    Log.e(TAG, "pType..."+pType);
                    qScore = ((Node)mapResp.getNamedItem("qScore")).getTextContent();
                    if (errorCode.equalsIgnoreCase("0")) {
                        //Log.e(TAG,"Data : " + eElement.getElementsByTagName("Data").item(0).getTextContent());
                        encryptedPidXmlStr=eElement.getElementsByTagName("Data").item(0).getTextContent();
                        //Log.e(TAG,"Hmac : " + eElement.getElementsByTagName("Hmac").item(0).getTextContent());
                        encryptedHmacStr=eElement.getElementsByTagName("Hmac").item(0).getTextContent();
                        //Log.e(TAG,"Skey : " + eElement.getElementsByTagName("Skey").item(0).getTextContent());
                        encryptedSkey=eElement.getElementsByTagName("Skey").item(0).getTextContent();
                        NamedNodeMap mapDeviceINfo = eElement.getElementsByTagName("DeviceInfo").item(0).getAttributes();
                        dpId=((Node)mapDeviceINfo.getNamedItem("dpId")).getTextContent();
                        //Log.e(TAG,"dpId : "+dpId);
                        mc=((Node)mapDeviceINfo.getNamedItem("mc")).getTextContent();
                        //Log.e(TAG,"mc :"+mc);
                        dc=((Node)mapDeviceINfo.getNamedItem("dc")).getTextContent();
                        //Log.e(TAG,"dc :"+dc);
                        mi=((Node)mapDeviceINfo.getNamedItem("mi")).getTextContent();
                        //Log.e(TAG,"mi :"+mi);
                        rdsId=((Node)mapDeviceINfo.getNamedItem("rdsId")).getTextContent();
                        //Log.e(TAG,"rdsId :"+rdsId);
                        rdsVer=((Node)mapDeviceINfo.getNamedItem("rdsVer")).getTextContent();
                        //Log.e(TAG,"rdsVer :"+rdsVer);
                        NamedNodeMap mapSkey = eElement.getElementsByTagName("Skey").item(0).getAttributes();
                        ci=((Node)mapSkey.getNamedItem("ci")).getTextContent();
                        //Log.e(TAG,"ci   : "+ci);
                        playSound();
//                        fingerScan.setImageResource(R.drawable.scan_success_icon);
                        sendRequest();
                    } else {
                        showAlert(errorInfo);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    private void parseXmlData(String xmlData) {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(new InputSource(new java.io.StringReader(xmlData)));
            doc.getDocumentElement().normalize();
            System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
            NodeList nList = doc.getElementsByTagName("PidData");

            Log.e("TAG", "<---------------START-------------->");
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                System.out.println("\nCurrent Element :" + nNode.getNodeName());
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;

                    NamedNodeMap mapResp = eElement.getElementsByTagName("Resp").item(0).getAttributes();
                    String errorCode = ((Node) mapResp.getNamedItem("errCode")).getTextContent();
                    String errorInfo = ((Node) mapResp.getNamedItem("errInfo")).getTextContent();
                    String fCount = ((Node) mapResp.getNamedItem("fCount")).getTextContent();
                    String fType = ((Node) mapResp.getNamedItem("fType")).getTextContent();
                    String iCount = ((Node) mapResp.getNamedItem("iCount")).getTextContent();
                    String iType = ((Node) mapResp.getNamedItem("iType")).getTextContent();
                    String nmPoints = ((Node) mapResp.getNamedItem("nmPoints")).getTextContent();
                    String pCount = ((Node) mapResp.getNamedItem("pCount")).getTextContent();
                    String pType = ((Node) mapResp.getNamedItem("pType")).getTextContent();
                    Log.e("TAG", "pType..." + pType);
                    String qScore = ((Node) mapResp.getNamedItem("qScore")).getTextContent();
                    System.out.println("errCode   : " + errorCode);
                    System.out.println("errorInfo   : " + errorInfo);
                    if (errorCode.equalsIgnoreCase("0")) {

                        Log.e("TAG", "Data : " + eElement.getElementsByTagName("Data").item(0).getTextContent());
                        encryptedPidXmlStr = eElement.getElementsByTagName("Data").item(0).getTextContent();
                        Log.e("TAG", "Hmac : " + eElement.getElementsByTagName("Hmac").item(0).getTextContent());
                        encryptedHmacStr = eElement.getElementsByTagName("Hmac").item(0).getTextContent();
                        Log.e("TAG", "Skey : " + eElement.getElementsByTagName("Skey").item(0).getTextContent());
                        encryptedSkey = eElement.getElementsByTagName("Skey").item(0).getTextContent();

                        NamedNodeMap mapDeviceINfo = eElement.getElementsByTagName("DeviceInfo").item(0).getAttributes();
                        dpId = ((Node) mapDeviceINfo.getNamedItem("dpId")).getTextContent();
                        Log.e("TAG", "dpId : " + dpId);

                        mc = ((Node) mapDeviceINfo.getNamedItem("mc")).getTextContent();
                        Log.e("TAG", "mc :" + mc);

                        mi = ((Node) mapDeviceINfo.getNamedItem("mi")).getTextContent();
                        Log.e("TAG", "mi :" + mi);

                        rdsId = ((Node) mapDeviceINfo.getNamedItem("rdsId")).getTextContent();
                        Log.e("TAG", "rdsId :" + rdsId);

                        rdsVer = ((Node) mapDeviceINfo.getNamedItem("rdsVer")).getTextContent();
                        Log.e("TAG", "rdsVer :" + rdsVer);

                        udc = ((Node) mapDeviceINfo.getNamedItem("dc")).getTextContent();
                        Log.e("TAG", "dc :" + udc);

                        NamedNodeMap mapSkey = eElement.getElementsByTagName("Skey").item(0).getAttributes();
                        ci = ((Node) mapSkey.getNamedItem("ci")).getTextContent();
                        Log.e("TAG", "ci   : " + ci);

                        //  new SendMemberDetails().execute();
                        /*mSubmit_Raised_Button.setText(AppStrings.continue_flow);
                        mSubmit_Raised_Button.setVisibility(View.VISIBLE);
                        retry_Raised_Button.setVisibility(View.GONE);
                        skip_Raised_Button.setVisibility(View.GONE);*/
                        img_fp.setImageResource(R.drawable.fp_123);

                        sendRequest();

                    } else {
                        Toast.makeText(getActivity(), "" + errorInfo, Toast.LENGTH_SHORT).show();
                        mSubmit_Raised_Button.setEnabled(true);
                        if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                            mProgressDilaog.dismiss();
                            mProgressDilaog = null;
                        }
                      /*  mSubmit_Raised_Button.setText(AppStrings.str_scan);
                        mSubmit_Raised_Button.setVisibility(View.GONE);
                        retry_Raised_Button.setVisibility(View.VISIBLE);
                        skip_Raised_Button.setVisibility(View.VISIBLE);*/
                        img_fp.setImageResource(R.drawable.thumb_impress);
                    }

                }
            }
            Log.e("TAG", "<---------------END-------------->");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void sendRequest() {
        try {
            AEPSDto aepsDto = new AEPSDto();
            aepsDto.setMerchantId(MySharedPreference.readString(getActivity(), MySharedPreference.MERCHANT_ID, ""));
            MemMasterDto mmd = new MemMasterDto();
            ShgMasDto smdto = new ShgMasDto();
            mmd.setId(member.getMemberId());
            smdto.setId(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
            aepsDto.setMemberMasterDTO(mmd);
            aepsDto.setShgMasterDTO(smdto);
            aepsDto.setAmount(amt + "");
            aepsDto.setWalletId(MySharedPreference.readString(getActivity(), MySharedPreference.MERCHANT_ID, ""));
            aepsDto.setUserId(MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, ""));
            AadhaarPayDto aadhaarPayDto = new AadhaarPayDto();
            DateFormat df = new SimpleDateFormat("ddMMyyHHmmss", Locale.getDefault());
            String transactionId = df.format(new Date());
            //   aadhaarPayDto.setRetailerTxnId(trans_edit.getText().toString());
            aadhaarPayDto.setAadharNumber(uidStr.getText().toString().replaceAll("[-\\[\\]^/,'*:.!><~@#$%+=?|\"\\\\()]+", ""));
            //  string = string;
            IINDto ind = new IINDto();
            ind.setUuid(rsContent.getBankIID());
            aadhaarPayDto.setIin(rsContent.getBankIID());
            aadhaarPayDto.setAmount(amt + "");
            aadhaarPayDto.setBiometricData(pidDate);
            aepsDto.setAepsRequest(aadhaarPayDto);
           /* aadhaarPayDto.setTxnTypeCode("121");
            aadhaarPayDto.setCustomerConsent("1");
            if (counter == 2) {
                aadhaarPayDto.setParam1("p");
                aadhaarPayDto.setParam2("p");
                aadhaarPayDto.setParam3("p");
            }

            aadhaarPayDto.setParam4("0123456789ABCDEF");
            aadhaarPayDto.setParam5("shopno");*/

            if (networkConnection.isNetworkAvailable()) {
                Gson gson2 = new Gson();
                String aadhaarPayReqJson = gson2.toJson(aepsDto);
                //  Log.e("Aadhaar pay req...", aadhaarPayReqJson); //omiited for audit //////////////////
//                  Transaction key for Aadhaar pay
//                    String data = new String(Encrypt(aadhaarPayReqJson, "i5C3Eft6X8"));
//                  Transaction key for AEPS
                //      String data = new String(Encrypt(aadhaarPayReqJson, "d7JZf2i6KT"));
                String data = new String(Base64.encode(Encrypt(aadhaarPayReqJson, "i5C3Eft6X8"), Base64.DEFAULT), "UTF-8");
            //    Log.e("TAG", "data..." + data);  //omited for audit
                logLargeString(data);
                EncryptedAadhaarPayRequestDto encryptedAadhaarPayDto = new EncryptedAadhaarPayRequestDto();
                encryptedAadhaarPayDto.setData(data);
                encryptedAadhaarPayDto.setAgentId(MySharedPreference.readString(getActivity(), MySharedPreference.AGENT_ID, ""));
                encryptedAadhaarPayDto.setUserId(MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, ""));
                encryptedAadhaarPayDto.setMemberId(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_GROUP_ID, ""));
                encryptedAadhaarPayDto.setShgId(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
                encryptedAadhaarPayDto.setDeviceId("");
//                  User id for Aadhaar pay
//                    encryptedAadhaarPayDto.setUserId(pref.getString("UserId", ""));
//                  User id for AEPS
//                    encryptedAadhaarPayDto.setUserId("1000002723");


                String encryptedAadhaarPayReqJson = new Gson().toJson(encryptedAadhaarPayDto);
                logLargeString(encryptedAadhaarPayReqJson);
               Log.e("Aadhaar encrypted req", encryptedAadhaarPayReqJson);
                //omitted for audit
                //    StringEntity se = new StringEntity(encryptedAadhaarPayReqJson, HTTP.UTF_8);

//                    String url = "https://uatsky.yesbank.in/app/uat/AadhaarPayment/AEPSTransaction";
                //   String url = "https://uattsdppos.oasyspay.in/v1/payment/post";   //Real
                //    String url = "https://uattsdppos.oasyspay.in/v1/payment/post";
                Log.e("TAG", "Sending timestamp...." + new Timestamp(System.currentTimeMillis()));  //omitted for audit
                //   httpConnection.sendRequest(url, null, ServiceListenerType.PAYMENT, SyncHandler, RequestType.POST, se, FingerprintScanActivity.this);
                //  httpConnection.sendRequest(url, null, ServiceListenerType.PAYMENT, SyncHandler, RequestType.POST, se, FingerprintScanActivity.this);
                RestClient.getRestClient(FingerPrintAuthentication.this).callRestWebService(Constants.BASE_URL + Constants.WALLET_CASH_WITHDRAWAL, encryptedAadhaarPayReqJson, getActivity(), ServiceType.FPS_YESBANK_REQ);
                //  mSubmit_Raised_Button.setEnabled(false);
            } else {
                if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                    mProgressDilaog.dismiss();
                    mProgressDilaog = null;
                }
                //  counter--;
                // Util.messageBar(FingerprintScanActivity.this, FingerprintScanActivity.this.getString(R.string.noNetworkConnection));
            }
        } catch (Exception e) {
            e.printStackTrace();
            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                mProgressDilaog.dismiss();
                mProgressDilaog = null;
            }
            ///      //    Log.e("Aadhaar pay exc...", e.toString(), e);   // omitted for audit
        }
    }

    private void getAadhaarResponse(String message) {

        try {

            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                mProgressDilaog.dismiss();
                mProgressDilaog = null;
            }
            String response = message;
          //  Log.e("TAG", "Aadhaar pay response..." + response);//omitted for audit
            GregorianCalendar gc2 = new GregorianCalendar();
            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");
            String timeStamp = sdf2.format(gc2.getTime());

            if (response != null) {
                Log.e("TAG", "Util.statusCodeOfResponse..." + "need");//omitted for audit
                try {
                    EncryptedAadhaarPayResponseDto encryptedAadhaarPayResponseDto = new Gson().fromJson(response, EncryptedAadhaarPayResponseDto.class);
                    int statusCode = Integer.parseInt(encryptedAadhaarPayResponseDto.getStatusCode());
                    String msg = encryptedAadhaarPayResponseDto.getMessage();
                    if (statusCode == Utils.Success_Code) {

                        Utils.showToast(getActivity(), msg);
                       /* Transactionpaymentsuccess tps = new Transactionpaymentsuccess();
                        Bundle bundle = new Bundle();
                        bundle.putString("txnAmount", encryptedAadhaarPayResponseDto.getResponseContent().getTxnAmount());
                        bundle.putString("retailerTxnId", encryptedAadhaarPayResponseDto.getResponseContent().getRetailerTxnId());
                        bundle.putString("terminalId", encryptedAadhaarPayResponseDto.getResponseContent().getTerminalId());

                        tps.setArguments(bundle);
                        MemberDrawerScreen.showFragment(tps);*/

                        if (encryptedAadhaarPayResponseDto.getResponseContent() != null)
                            AppDialogUtils.showAepsConfirmationDialog(getActivity(), encryptedAadhaarPayResponseDto);


                   /*     rrnvalue = encryptedAadhaarPayResponseDto.getResponseContent().getRrn();
                        terminalId = encryptedAadhaarPayResponseDto.getResponseContent().getTerminalId();
                        stan = encryptedAadhaarPayResponseDto.getResponseContent().getStan();
                        uidaiAuthCode = encryptedAadhaarPayResponseDto.getResponseContent().getUidaiauthenticationCode();

                           *//* SaleTransactionCompleted saleTransactionCompleted = new SaleTransactionCompleted(FingerprintScanActivity.this);
                            saleTransactionCompleted.setCanceledOnTouchOutside(false);
                            saleTransactionCompleted.show();
                            PrintData();
*//*
                        isSaleCompleted = true;
                       *//* SharedPreferences.Editor editor = getSharedPreferences("FPS_TELANGANA", MODE_PRIVATE).edit();
                        editor.putString("LastSaleRCNumber", LoginData.getInstance().getRationCardNo());
                        editor.commit();*//*
                        //  PrintData();
                        SaleTransactionCompletedAdhaarPay saleTransactionCompleted = new SaleTransactionCompletedAdhaarPay(FingerprintScanActivity.this);
                        saleTransactionCompleted.setCanceledOnTouchOutside(false);
                        saleTransactionCompleted.show();*/
                    } else if (encryptedAadhaarPayResponseDto.getStatusCode().equalsIgnoreCase("99")) {
                        if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                            mProgressDilaog.dismiss();
                            mProgressDilaog = null;
                        }
                    /*    btnFingerScan.setVisibility(View.VISIBLE);
                        btnFingerScan.setText("Retry");
                        transactionTypeCode = encryptedAadhaarPayResponseDto.getStatusCode();
                        handleOnClick();*/
                    } else if (encryptedAadhaarPayResponseDto.getStatusCode().equalsIgnoreCase("101")) {
                        if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                            mProgressDilaog.dismiss();
                            mProgressDilaog = null;
                        }
                        /*btnFingerScan.setVisibility(View.VISIBLE);
                        btnFingerScan.setText("Reversal");
                        transactionTypeCode = encryptedAadhaarPayResponseDto.getStatusCode();
                        handleOnClick();*/
                    } else {
                        if (encryptedAadhaarPayResponseDto.getResponseContent() != null)
                            AppDialogUtils.showAepsConfirmationDialog(getActivity(), encryptedAadhaarPayResponseDto);

                        Utils.showToast(getActivity(), msg);
                        if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                            mProgressDilaog.dismiss();
                            mProgressDilaog = null;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        } catch (Exception e) {

        }

    }

    public void logLargeString(String str) {
        if (str.length() > 3000) {
            Log.e("TAG", "encrypted Req log..." + str.substring(0, 3000));
            logLargeString(str.substring(3000));
        } else {
            Log.e("TAG", "" + str); // continuation
        }
    }

   /* public String Encrypt(String text) throws Exception {

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
        byte[] keyBytes = new byte[16];
        byte[] pwdBytes = getKeyBytes();
        int len = pwdBytes.length;
        if (len > keyBytes.length)
            len = keyBytes.length;
        System.arraycopy(pwdBytes, 0, keyBytes, 0, len);
        SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
        IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);

        byte[] results = cipher.doFinal(text.getBytes("UTF-8"));
        return Base64.encodeToString(results, Base64.DEFAULT); // it returns the result as a String
    }

    private byte[] getKeyBytes() throws UnsupportedEncodingException {
        InputStream fileStream = null;
        String destDir = Environment.getExternalStorageDirectory().toString() + "/Fps/UP_GOVT.key";

        try {
            fileStream = new FileInputStream(new File(destDir));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        String keys = null;
        byte[] buffer = new byte[0];
        try {
            int length = fileStream.available();
            buffer = new byte[length];
            int count;
            int sum = 0;
            while ((count = fileStream.read(buffer, sum, length - sum)) > 0)
                sum += count;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fileStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return buffer;
    }*/

    public byte[] Encrypt(String text, String Key) throws Exception {

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        byte[] keyBytes = new byte[16];
        byte[] pwdBytes = Key.getBytes();
        int len = pwdBytes.length;
        if (len > keyBytes.length)
            len = keyBytes.length;
        System.arraycopy(pwdBytes, 0, keyBytes, 0, len);
        SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
        IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);

        byte[] results = cipher.doFinal(text.getBytes("UTF-8"));
        //  Log.e("TAG", "byte array in encryption..."+results.toString());
        Log.e("TAG", "base 64 in encryption..." + new String(Base64.encode(results, Base64.DEFAULT), "UTF-8"));
        return results; // it returns the result as a String
    }


    public static byte[] _encrypt(String plainText, String key) throws Exception {
        byte[] clean = plainText.getBytes();

        // Generating IV.
        int ivSize = 16;
        byte[] iv = new byte[ivSize];
        SecureRandom random = new SecureRandom();
        random.nextBytes(iv);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);

        // Hashing key.
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        digest.update(key.getBytes("UTF-8"));
        byte[] keyBytes = new byte[16];
        System.arraycopy(digest.digest(), 0, keyBytes, 0, keyBytes.length);
        SecretKeySpec secretKeySpec = new SecretKeySpec(keyBytes, "AES");

        // Encrypt.
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);
        byte[] encrypted = cipher.doFinal(clean);

        // Combine IV and encrypted part.
        byte[] encryptedIVAndText = new byte[ivSize + encrypted.length];
        System.arraycopy(iv, 0, encryptedIVAndText, 0, ivSize);
        System.arraycopy(encrypted, 0, encryptedIVAndText, ivSize, encrypted.length);

        return encryptedIVAndText;
    }


    private void RdScan() {
        try {
            String pidOption = getPIDOptions();
            Log.e("TAG", "PID OPTION : " + pidOption);
            if (pidOption != null) {
                Intent intent2 = new Intent();
                intent2.setAction("in.gov.uidai.rdservice.fp.CAPTURE");
                intent2.putExtra("PID_OPTIONS", pidOption);
                startActivityForResult(intent2, 2);
            }
        } catch (Exception e) {
            Log.e("Error", e.toString());
            if (mProgressDilaog != null && mProgressDilaog.isShowing())
                mProgressDilaog.dismiss();
            mSubmit_Raised_Button.setEnabled(true);
        }
    }

    private String getPIDOptions() {
        try {
            int fingerCount = 1;
            int fingerType = 0;
            int fingerFormat = 1;
            String pidVer = "2.0";
            String timeOut = "10000";
            String posh = "UNKNOWN";


            Opts opts = new Opts();
            opts.fCount = String.valueOf(fingerCount);
            opts.fType = String.valueOf(fingerType);
            opts.iCount = "0";
            opts.iType = "0";
            opts.pCount = "0";
            opts.pType = "0";
            opts.format = String.valueOf(fingerFormat);
            opts.pidVer = pidVer;
            opts.timeout = timeOut;
//            opts.otp = "";
            opts.posh = posh;
            String env = "P";
          /*  switch (rgEnv.getCheckedRadioButtonId()) {
                case R.id.rbStage:
                    env = "S";
                    break;
                case R.id.rbPreProd:
                    env = "PP";
                    break;
                case R.id.rbProd:
                    env = "P";
                    break;
            }*/
            opts.env = env;

            PidOptions pidOptions = new PidOptions();
            pidOptions.ver = pidVer;
            pidOptions.Opts = opts;

            Serializer serializer = new Persister();
            StringWriter writer = new StringWriter();
            serializer.write(pidOptions, writer);
            return writer.toString();
        } catch (Exception e) {
            Log.e("Error", e.toString());
        }
        return null;
    }

    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
            mProgressDilaog.dismiss();
            mProgressDilaog = null;
        }
        switch (serviceType) {
            case FPS_YESBANK_REQ:
                mSubmit_Raised_Button.setEnabled(true);
                getAadhaarResponse(result);
                break;
            case WALLET_B_CW:
                if (result != null && result.length() > 0) {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    ResponseDto lrDto = gson.fromJson(result, ResponseDto.class);
                    int statusCode = lrDto.getStatusCode();
                    String message = lrDto.getMessage();
                    Log.d("response status", " " + statusCode);
                    if (statusCode == 400 || statusCode == 401 || statusCode == 403 || statusCode == 500 || statusCode == 503) {
                        // showMessage(statusCode);
                        Utils.showToast(getActivity(), message);


                    } else if (statusCode == Utils.Success_Code) {
                        Utils.showToast(getActivity(), message);
                        if (lrDto.getResponseContent() != null) {
                            if (lrDto.getResponseContent().getBankNames() != null) {

                                for (int i = 0; i < lrDto.getResponseContent().getBankNames().size(); i++) {
                                    ResponseContents contents = new ResponseContents();
                                    contents.setCode(lrDto.getResponseContent().getBankNames().get(i).getCode());
                                    contents.setBankIID(lrDto.getResponseContent().getBankNames().get(i).getBankIID());
                                    contents.setName(lrDto.getResponseContent().getBankNames().get(i).getName());
                                    contents.setId(lrDto.getResponseContent().getBankNames().get(i).getId());
                                    contents.setStatus(lrDto.getResponseContent().getBankNames().get(i).getStatus());
                                    responseContentsList.add(contents);
                                }

                                WalletBankIdAdapter aa = new WalletBankIdAdapter(getActivity(), responseContentsList);
                                mCaspayment_spinner.setAdapter(aa);


                            }
                        }
                    }
                }
                break;

            case WALL_TXANIMATOR:

                try {
                    ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    String message = cdto.getMessage();
                    int statusCode = cdto.getStatusCode();
                    if (statusCode == Utils.Success_Code) {
                        Utils.showToast(getActivity(), message);
                        String walletamount = cdto.getResponseContent().getWalletAmount();
                        mWalletamount = (int) Double.parseDouble(walletamount);
                        Wallet_balance.setText(AppStrings.w_b + String.valueOf(mWalletamount));
                        Wallet_balance.setTypeface(LoginActivity.sTypeface);
                    } else {
                        Utils.showToast(getActivity(), message);


                        if (statusCode == 503) {
                            Utils.showToast(getActivity(), AppStrings.service_unavailable);

                        }

                        if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        }
                        if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                            mProgressDilaog.dismiss();
                            mProgressDilaog = null;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;

            case WALLET_CASHIN_HAND:
                try {
                    ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    String message = cdto.getMessage();
                    int statusCode = cdto.getStatusCode();
                    if (statusCode == Utils.Success_Code) {
                        Utils.showToast(getActivity(), message);
                        String animatorCashinHand = cdto.getResponseContent().getCurrentCashInHand();
                        mAniiamtorCH = (int) Double.parseDouble(animatorCashinHand);
                        Wallet_CIH.setText(AppStrings.w_cih + mAniiamtorCH);
                        Wallet_CIH.setTypeface(LoginActivity.sTypeface);
                    } else {
                        Utils.showToast(getActivity(), message);

                        if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        }

                        if (statusCode == 503) {
                            Utils.showToast(getActivity(), AppStrings.service_unavailable);

                        }

                        if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                            mProgressDilaog.dismiss();
                            mProgressDilaog = null;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

        }
    }
}