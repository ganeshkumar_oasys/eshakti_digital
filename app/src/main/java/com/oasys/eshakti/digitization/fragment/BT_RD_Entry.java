package com.oasys.eshakti.digitization.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.digitization.Adapter.CustomItemAdapter;
import com.oasys.eshakti.digitization.Dto.AddBTDto;
import com.oasys.eshakti.digitization.Dto.ExistingLoan;
import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.Dto.MemberList;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.Dto.ShgBankDetails;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.GetSpanText;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.activity.LoginActivity;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;
import com.oasys.eshakti.digitization.database.BankTable;
import com.oasys.eshakti.digitization.database.LoanTable;
import com.oasys.eshakti.digitization.database.MemberTable;
import com.oasys.eshakti.digitization.database.SHGTable;
import com.oasys.eshakti.digitization.model.RowItem;
import com.oasys.eshakti.digitization.views.Get_EdiText_Filter;
import com.oasys.eshakti.digitization.views.MaterialSpinner;
import com.oasys.eshakti.digitization.views.RaisedButton;
import com.tutorialsee.lib.TastyToast;

import java.util.ArrayList;
import java.util.List;

public class BT_RD_Entry extends Fragment implements View.OnClickListener, NewTaskListener {

    private View rootView;
    private int mSize;
    private List<MemberList> memList;
    private ListOfShg shgDto;
    private ArrayList<ShgBankDetails> bankdetails;
    private String[] menuStr;
    private NetworkConnection networkConnection;
    private TextView mGroupName, mCashInHand, mCashAtBank, mHeadertext;
    private TextView mLoanOutstandingTextView, mBankName_value_TextView, mBankNameTextView, mLoanAccNo_value_TextView, mAccNo_ValeTextView, mAcctoaccFromBankEdittext, accNotext, mLoanAccNoTextView, mLoanOutstanding_value_TextView, mAccNoTextView;
    private Button mRaised_Submit_Button, mSubmitButton, mEdit_RaisedButton, mOk_RaisedButton;

    private Dialog confirmationDialog;
    MaterialSpinner mSpinner_tobank, mSpinner_loanAcc;
    CustomItemAdapter bankNameAdapter, loanAccAdapter;
    private LinearLayout mSavingsAccLayout, trFlowSelection, mLoanAccLayout;
    private RadioButton mSavingsAccRadio, mLoanAccRadio;
    private RadioGroup mRadioGroup;
    public static EditText mEditAmount;

    public static String sSend_To_Server_FD;
    public static String[] sFixedDepositItem;
    public static List<EditText> sEditTextFields = new ArrayList<EditText>();
    String mDeposit_Fixed_offline, mInterest_Fixed_offline, mWithdrawl_Fixed_offline, mBankExpenses_Fixed_offline;
    public static int bank_deposit = 0, bank_withdrawl = 0, bank_expenses, bank_interest;
    private int mRDSize;
    private String amount = "0.0";
    String mSelectedCashatBank = "0.0";
    private Dialog mProgressDilaog;
    private List<ExistingLoan> loanDetails;

    public static String selectedItemBank, selectedLoanAcc, selectedLoanId;
    public static String mBankNameValue = null, mBankName = "", mTempBankId = "", mLoanAccValue = null, mLoanAccIdValue = null;
    private List<RowItem> bankNameItems, loanAccItems;
    public static int sFixedDepositTotal;

    private ArrayList<ShgBankDetails> bankSortedList;

    ArrayList<String> mBanknames_Array = new ArrayList<String>();
    ArrayList<String> mBanknamesId_Array = new ArrayList<String>();

    private String[] mLoanTypeArray, mLoanIdArray;

    ArrayList<String> mEngSendtoServerBank_Array = new ArrayList<String>();
    ArrayList<String> mEngSendtoServerBankId_Array = new ArrayList<String>();

    String selectedRadio = "NONE";
    String ToLoanAccNo = "", outstandingAmt = "";
    private ExistingLoan selectedLoan;
    private ExistingLoan selectedLoantype;
    private ShgBankDetails selectedSavinAc;
    private String[] sFixedDepositAmount;
    private int transactionFlowType = 0;
    private TextView acctoaccfrombanktext, acctoaccfrombank;
    private LinearLayout ply;


    @Override
    public void onClick(View view) {


        switch (view.getId()) {

            case R.id.fragment_Edit:

                confirmationDialog.dismiss();
                sSend_To_Server_FD = "0";
                sFixedDepositTotal = Integer.valueOf("0");
                break;

            case R.id.frag_Ok:

                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();

                AddBTDto bt = new AddBTDto();
                bt.setSavingsBankId(BankTransaction.sSelectedBank.getShgSavingsAccountId());
                bt.setBankInterest(bank_interest + "");
                bt.setBankCharges(bank_expenses + "");
                bt.setShgId(shgDto.getShgId());
                if (transactionFlowType != 1) {
                    if (selectedSavinAc != null && selectedSavinAc.getShgSavingsAccountId() != null)
                        bt.setToBankId(selectedSavinAc.getShgSavingsAccountId());
                } else {
                    if (selectedLoan != null && selectedLoan.getLoanId() != null)
                        bt.setToBankId(selectedLoan.getLoanId());
                }

                bt.setTransactionFlowType(transactionFlowType);
                bt.setWithdrawal(bank_withdrawl + "");
                bt.setCashDeposit(bank_deposit + "");
                bt.setMobileDate(System.currentTimeMillis() + "");
                bt.setTransactionDate(shgDto.getLastTransactionDate());
                String sreqString = new Gson().toJson(bt);
                if (networkConnection.isNetworkAvailable()) {
                    onTaskStarted();
                    RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.BT_RD, sreqString, getActivity(), ServiceType.BT_RD);
                }

                break;
        }
    }


    @Override
    public void onStart() {
        super.onStart();
//        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
//        if (networkConnection.isNetworkAvailable()) {            //  onTaskStarted();
//            RestClient.getRestClient(this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.BT_CB_FD_VALUE + BankTransaction.sSelectedBank.getShgSavingsAccountId(), getActivity(), ServiceType.RD_VALUE);
//            //  onTaskStarted();
//            RestClient.getRestClient(this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.BT_PLOAN + shgDto.getShgId(), getActivity(), ServiceType.LOANTYPE);
//        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        mSize = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, "")).size();
        memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        bankdetails = BankTable.getBankName(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        //  loanDetails = LoanTable.getLoanDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        sFixedDepositItem = new String[]{AppStrings.bankDeposit,
                AppStrings.bankInterest,
                AppStrings.withdrawl,
                AppStrings.bankExpenses};
        sEditTextFields = new ArrayList<EditText>();
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        if (networkConnection.isNetworkAvailable()) {            //  onTaskStarted();
            RestClient.getRestClient(this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.BT_CB_FD_VALUE + BankTransaction.sSelectedBank.getShgSavingsAccountId(), getActivity(), ServiceType.RD_VALUE);
            //  onTaskStarted();
            RestClient.getRestClient(this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.BT_PLOAN + shgDto.getShgId(), getActivity(), ServiceType.LOANTYPE);
        }


        //init();
    }

    private void init() {
        ply.setVisibility(View.VISIBLE);
        mGroupName = (TextView) rootView.findViewById(R.id.groupname);
        mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
        mGroupName.setTypeface(LoginActivity.sTypeface);
        mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
        mCashInHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
        mCashInHand.setTypeface(LoginActivity.sTypeface);
        mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
        mCashAtBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
        mCashAtBank.setTypeface(LoginActivity.sTypeface);
        mHeadertext = (TextView) rootView.findViewById(R.id.fragment_deposit_entry_headertext);
        mHeadertext.setText(AppStrings.recurringDeposit);
        mHeadertext.setTypeface(LoginActivity.sTypeface);

        mSubmitButton = (RaisedButton) rootView.findViewById(R.id.acctoacc_submit);
        mSubmitButton.setText(AppStrings.submit);
        mSubmitButton.setTypeface(LoginActivity.sTypeface);
        mSubmitButton.setOnClickListener(this);

        mSpinner_tobank = (MaterialSpinner) rootView.findViewById(R.id.acctoacctobankspinner);
        mSpinner_loanAcc = (MaterialSpinner) rootView.findViewById(R.id.loanAccTransactionspinner);
        mSpinner_loanAcc.setBaseColor(R.color.grey_400);
        mSpinner_loanAcc.setFloatingLabelText(AppStrings.mLoanAccType);
        mSpinner_loanAcc.setPaddingSafe(10, 0, 10, 0);
        mSpinner_tobank.setFloatingLabelText(AppStrings.mTransferSpinnerFloating);
        mSpinner_tobank.setPaddingSafe(10, 0, 10, 0);


        trFlowSelection = (LinearLayout) rootView.findViewById(R.id.trFlowSelection);
        trFlowSelection.setVisibility(View.GONE);
        mSavingsAccLayout = (LinearLayout) rootView.findViewById(R.id.acctoacctobankspinnerlayout);
        mLoanAccLayout = (LinearLayout) rootView.findViewById(R.id.loanAccTransactionspinnerLayout);

        mRadioGroup = (RadioGroup) rootView.findViewById(R.id.radio_loanAccTransaction);
        mSavingsAccRadio = (RadioButton) rootView.findViewById(R.id.radioloanaccTransaction_SavingsAcc);
        mLoanAccRadio = (RadioButton) rootView.findViewById(R.id.radioloanaccTransaction_LoanAcc);
        mSavingsAccRadio.setText(AppStrings.mSavingsAccount);
        mSavingsAccRadio.setTypeface(LoginActivity.sTypeface);

        mLoanAccRadio.setText(AppStrings.mLoanaccHeader);
        mLoanAccRadio.setTypeface(LoginActivity.sTypeface);

        mLoanOutstandingTextView = (TextView) rootView.findViewById(R.id.loanAccTransaction_outstandingTextView);
        mLoanOutstandingTextView.setTypeface(LoginActivity.sTypeface);
        mLoanOutstanding_value_TextView = (TextView) rootView.findViewById(R.id.loanAccTransaction_outstanding_value);
        mLoanOutstanding_value_TextView.setTypeface(LoginActivity.sTypeface);

        mBankNameTextView = (TextView) rootView.findViewById(R.id.loanAccTransaction_BankNameTextView);
        mBankNameTextView.setTypeface(LoginActivity.sTypeface);
        acctoaccfrombanktext = (TextView) rootView.findViewById(R.id.acctoaccfrombanktext);
        acctoaccfrombanktext.setText(AppStrings.mTransferFromBank);
       acctoaccfrombanktext.setTypeface(LoginActivity.sTypeface);
        acctoaccfrombank = (TextView) rootView.findViewById(R.id.acctoaccfrombank);
       // acctoaccfrombank.setTypeface(LoginActivity.sTypeface);
        mBankName_value_TextView = (TextView) rootView.findViewById(R.id.loanAccTransaction_BankName_value);
        mBankName_value_TextView.setTypeface(LoginActivity.sTypeface);

        mLoanAccNoTextView = (TextView) rootView.findViewById(R.id.loanAccTransaction_accNoTextView);
        mLoanAccNoTextView.setTypeface(LoginActivity.sTypeface);

        accNotext = (TextView) rootView.findViewById(R.id.accNotext);
        accNotext.setText(AppStrings.mAccountNumber);
        accNotext.setTypeface(LoginActivity.sTypeface);
        mLoanAccNo_value_TextView = (TextView) rootView.findViewById(R.id.loanAccTransaction_accNo_value);
        mLoanAccNo_value_TextView.setTypeface(LoginActivity.sTypeface);
        mSubmitButton.setText(AppStrings.submit);
        mSubmitButton.setTypeface(LoginActivity.sTypeface);
        mAccNo_ValeTextView = (TextView) rootView.findViewById(R.id.accNo_value);
        mAccNo_ValeTextView.setText(BankTransaction.sSelectedBank.getAccountNumber());
        mAccNo_ValeTextView.setTypeface(LoginActivity.sTypeface);
        mAcctoaccFromBankEdittext = (TextView) rootView.findViewById(R.id.acctoaccfrombank);
        mAcctoaccFromBankEdittext.setTypeface(LoginActivity.sTypeface);

        if (amount != null) {
            mAcctoaccFromBankEdittext.setText(BankTransaction.sSelectedBank.getBankName() + "  :  "
                    + String.valueOf(amount));
        } else {
            mAcctoaccFromBankEdittext.setText(BankTransaction.sSelectedBank.getBankName() + "  :  "
                    + String.valueOf("0.0"));
        }
        mLoanOutstandingTextView.setText(AppStrings.outstanding);
        mBankNameTextView.setText(AppStrings.bankName);
        mLoanAccNoTextView.setText(AppStrings.mAccountNumber);

        bankSortedList = new ArrayList<>();
        for (ShgBankDetails details : bankdetails) {
            if (details.getShgSavingsAccountId() != null && !(details.getShgSavingsAccountId().equals(BankTransaction.sSelectedBank.getShgSavingsAccountId())))
                bankSortedList.add(details);
        }


        for (int i = 0; i < bankSortedList.size(); i++) {
            mBankName = mBankName + bankSortedList.get(i).getBankName().toString() + "~";

        }

        for (int i = 0; i < bankSortedList.size(); i++) {

            mTempBankId = mTempBankId + bankSortedList.get(i).getBankName().toString() + "~";
        }

        Log.e("Temp Bank ID  Values_>", mTempBankId);

        for (int i = 0; i < bankSortedList.size(); i++) {
            mBanknames_Array.add(bankSortedList.get(i).getBankName());
            mBanknamesId_Array.add(bankSortedList.get(i).getBankId());
        }

        for (int i = 0; i < bankSortedList.size(); i++) {
            mEngSendtoServerBank_Array.add(bankSortedList.get(i).getBankName());
            mEngSendtoServerBankId_Array.add(bankSortedList.get(i).getBankId());
        }


        final String[] bankNames = new String[bankSortedList.size() + 1];
        bankNames[0] = String.valueOf(AppStrings.S_B_N);
        for (int i = 0; i < bankSortedList.size(); i++) {
            //if (!bankdetails.get(i).getShgSavingsAccountId().equals(BankTransaction.sSelectedBank.getShgSavingsAccountId()))
            bankNames[i + 1] = bankSortedList.get(i).getBankName().toString();
            System.out.println("-----------------------" + bankNames[i + 1]);
        }

        final String[] bankNames_Id = new String[bankSortedList.size() + 1];
        bankNames_Id[0] = String.valueOf(AppStrings.S_B_N);
        for (int i = 0; i < bankSortedList.size(); i++) {
            //  if (!bankdetails.get(i).getShgSavingsAccountId().equals(BankTransaction.sSelectedBank.getShgSavingsAccountId()))
            bankNames_Id[i + 1] = bankSortedList.get(i).getBankId().toString();
            System.out.println("-----------------------" + bankNames_Id[i + 1]);
        }

        if (bankSortedList.size() < 1) {
            mSavingsAccRadio.setClickable(false);
        } else {
            mSavingsAccRadio.setClickable(true);
        }

        int size = bankNames.length;
        bankNameItems = new ArrayList<RowItem>();
        for (int i = 0; i < size; i++) {
            RowItem rowItem = new RowItem(bankNames[i]);// sBankNames.elementAt(i).toString());
            bankNameItems.add(rowItem);
        }
        bankNameAdapter = new CustomItemAdapter(getActivity(), bankNameItems);
        mSpinner_tobank.setAdapter(bankNameAdapter);

        mSpinner_tobank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub

                if (position == 0) {
                    selectedItemBank = bankNames_Id[0];
                    mBankNameValue = "0";

                } else {

                    selectedItemBank = bankNames_Id[position];
                    selectedSavinAc = bankSortedList.get(position - 1);

                    System.out.println("SELECTED BANK NAME : " + selectedItemBank);
                    String mBankname = null;
                    for (int i = 0; i < mBanknames_Array.size(); i++) {
                        if (selectedItemBank.equals(mEngSendtoServerBankId_Array.get(i))) {
                            mBankname = mEngSendtoServerBank_Array.get(i);
                        }
                    }
                    mBankNameValue = mBankname;
                    Log.e("Bank N Name--->>>", mBankNameValue);


                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        mSubmitButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                System.out.println("------  " + selectedRadio.toString());

                // TODO Auto-generated method stub


                if (!selectedRadio.equals("")) {

                    Log.e("Selected Radio Values ", selectedRadio);

                    if (selectedRadio.equals("LOANACCOUNT")) {

                        // String dashBoardDate = DatePickerDialog.sDashboardDate;

                        onShowConfirmationDialog();


                    } else {
                        if (( mBankNameValue != null&& !mBankNameValue.equals("0"))) {
                            String result = shgDto.getCashAtBank().replace("\u20B9", "");
                            //  if (totalAmount <= (int) Double.parseDouble(result)) {

                            onShowConfirmationDialog();

                          /*  } else {
                                TastyToast.makeText(getActivity(), AppStrings.cashatBankAlert,
                                        TastyToast.LENGTH_SHORT, TastyToast.WARNING);

                            }*/
                        } else {

                            TastyToast.makeText(getActivity(), AppStrings.mLoanaccBankNullToast,
                                    TastyToast.LENGTH_SHORT, TastyToast.WARNING);

                        }
                    }

                } else if (selectedRadio.equals("NONE")) {

                    onShowConfirmationDialog();
                } else {
                    if (mBankNameValue != null && mBankNameValue.equals("0")) {
                        TastyToast.makeText(getActivity(), AppStrings.mLoanaccBankNullToast,
                                TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                    } else {
                        TastyToast.makeText(getActivity(), AppStrings.mTransferNullToast,
                                TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                    }

                }

            }
        });

        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // TODO Auto-generated method stub

                try {
                    int selectedId = group.getCheckedRadioButtonId();

                    if (selectedId == R.id.radioloanaccTransaction_SavingsAcc) {
                        System.out.println("-------Savings_acc----");
                        selectedRadio = "SAVINGSACCOUNT";

                        if (bankdetails.size() <= 1) {
                            TastyToast.makeText(getActivity(), AppStrings.mAccToAccTransferToast, TastyToast.LENGTH_SHORT,
                                    TastyToast.WARNING);
                            mSubmitButton.setClickable(false);

                        } else {
                            transactionFlowType = 0;
                            mSavingsAccLayout.setVisibility(View.VISIBLE);
                            mLoanAccLayout.setVisibility(View.GONE);
                            mSubmitButton.setClickable(true);
                        }
                    } else if (selectedId == R.id.radioloanaccTransaction_LoanAcc) {
                        System.out.println("-------Loan_acc----");

                        if (loanDetails.size() >= 1) {
                            selectedRadio = "LOANACCOUNT";
                            transactionFlowType = 1;
                            mLoanAccLayout.setVisibility(View.VISIBLE);
                            mSavingsAccLayout.setVisibility(View.GONE);

                            mLoanTypeArray = new String[loanDetails.size() + 1];
                            mLoanTypeArray[0] = String.valueOf(AppStrings.mLoanAccType);

                            mLoanIdArray = new String[loanDetails.size() + 1];
                            for (int i = 0; i < loanDetails.size(); i++) {
                                if (loanDetails.get(i).getLoanTypeName() != null && loanDetails.get(i).getLoanTypeName().length() > 0) {
                                    mLoanTypeArray[i + 1] = loanDetails.get(i).getLoanTypeName().toString();
                                    mLoanIdArray[i] = loanDetails.get(i).getLoanId().toString();
                                }
                            }

                            loanAccItems = new ArrayList<RowItem>();
                            for (int i = 0; i < loanDetails.size() + 1; i++) {
                                RowItem rowItem = new RowItem(mLoanTypeArray[i]);
                                loanAccItems.add(rowItem);
                            }
                            loanAccAdapter = new CustomItemAdapter(getActivity(), loanAccItems);
                            mSpinner_loanAcc.setAdapter(loanAccAdapter);
                            mSpinner_loanAcc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    // TODO Auto-generated method stub

                                    if (position == 0) {
                                        selectedLoanAcc = mLoanTypeArray[0];
                                        mLoanAccValue = "0";
                                        mLoanAccIdValue = "0";

                                        mLoanOutstanding_value_TextView.setText("");
                                        mBankName_value_TextView.setText("");
                                        mLoanAccNo_value_TextView.setText("");


                                    } else {
                                        selectedLoan = loanDetails.get(position - 1);

                                        if (loanDetails.get(position - 1).getLoanTypeName() != null) {
                                            selectedLoantype = loanDetails.get(position - 1);
                                            mLoanOutstanding_value_TextView.setText(loanDetails.get(position - 1).getLoanOutstanding());

                                            mBankName_value_TextView.setText(loanDetails.get(position - 1).getBankName());
                                            mLoanAccNo_value_TextView.setText(loanDetails.get(position - 1).getAccountNumber());
                                            ToLoanAccNo = loanDetails.get(position - 1).getAccountNumber();
                                        }
                                        // String dashBoardDate = DatePickerDialog.sDashboardDate;

                                    }
                                    selectedLoanAcc = mLoanTypeArray[position];
                                    mLoanAccValue = selectedLoanAcc;

                                    Log.e("Loan Name", mLoanAccValue + "");
                                    Log.e("Loan Id", mLoanAccIdValue + "");
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                    // TODO Auto-generated method stub

                                }
                            });
                        } else {

                            TastyToast.makeText(getActivity(), AppStrings.noGroupLoan_Alert, TastyToast.LENGTH_SHORT,
                                    TastyToast.WARNING);
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });

        size = sFixedDepositItem.length;
        System.out.println("Size of values:>>>" + size);

        try {

            TableLayout tableLayout = (TableLayout) rootView.findViewById(R.id.fragmentDeposit_contentTable);
            TableRow.LayoutParams header_ContentParams = new TableRow.LayoutParams(250, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            header_ContentParams.setMargins(10, 5, 10, 5);
            TableRow.LayoutParams amountParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            amountParams.setMargins(5, 5, 20, 5);
          /*  TableRow outerRow = new TableRow(getActivity());

            TableRow.LayoutParams header_ContentParams = new TableRow.LayoutParams(250, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            header_ContentParams.setMargins(10, 5, 10, 5);

            TextView bankName = new TextView(getActivity());
            bankName.setText(GetSpanText.getSpanString(getActivity(),
                    String.valueOf(BankTransaction.sSelected_BankName)));
            bankName.setPadding(15, 5, 5, 5);
            bankName.setTextColor(R.color.black);
            bankName.setLayoutParams(header_ContentParams);
            outerRow.addView(bankName);

            TableRow.LayoutParams amountParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            amountParams.setMargins(5, 5, 20, 5);

            TextView bankDeposit_Amount = new TextView(getActivity());
            bankDeposit_Amount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(amount)));
            bankDeposit_Amount.setTextColor(R.color.black);
            bankDeposit_Amount.setPadding(5, 5, 5, 5);
            bankDeposit_Amount.setGravity(Gravity.RIGHT);
            bankDeposit_Amount.setLayoutParams(amountParams);
            outerRow.addView(bankDeposit_Amount);

            tableLayout.addView(outerRow, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));*/

            for (int i = 0; i < size; i++) {

                TableRow innerRow = new TableRow(getActivity());
                TextView memberName = new TextView(getActivity());
                memberName.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sFixedDepositItem[i])));
                memberName.setTypeface(LoginActivity.sTypeface);
                memberName.setPadding(15, 0, 5, 5);
                memberName.setLayoutParams(header_ContentParams);
                memberName.setTextColor(R.color.black);
                innerRow.addView(memberName);

                if (i == 2) {
                    mEditAmount = new EditText(getActivity());
                    sEditTextFields.add(mEditAmount);
                    mEditAmount.setId(i);
                    mEditAmount.setInputType(InputType.TYPE_CLASS_NUMBER);
                    mEditAmount.setPadding(5, 5, 5, 5);
                    mEditAmount.setFilters(Get_EdiText_Filter.editText_filter());
                    mEditAmount.setGravity(Gravity.RIGHT);
                    mEditAmount.setLayoutParams(amountParams);
                    mEditAmount.setWidth(150);
                    mEditAmount.setBackgroundResource(R.drawable.edittext_background);

                    mEditAmount.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            // TODO Auto-generated method stub
                            trFlowSelection.setVisibility(View.VISIBLE);
                            if (hasFocus) {
                                ((EditText) v).setGravity(Gravity.LEFT);
                            } else {
                                ((EditText) v).setGravity(Gravity.RIGHT);
                            }
                        }
                    });


                    innerRow.addView(mEditAmount);
                } else {
                    trFlowSelection.setVisibility(View.GONE);
                    mEditAmount = new EditText(getActivity());
                    sEditTextFields.add(mEditAmount);
                    mEditAmount.setId(i);
                    mEditAmount.setInputType(InputType.TYPE_CLASS_NUMBER);
                    mEditAmount.setPadding(5, 5, 5, 5);
                    mEditAmount.setFilters(Get_EdiText_Filter.editText_filter());
                    mEditAmount.setGravity(Gravity.RIGHT);
                    mEditAmount.setLayoutParams(amountParams);
                    mEditAmount.setWidth(150);
                    mEditAmount.setBackgroundResource(R.drawable.edittext_background);
                    mEditAmount.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            // TODO Auto-generated method stub
                            if (hasFocus) {
                                ((EditText) v).setGravity(Gravity.LEFT);
                            } else {
                                ((EditText) v).setGravity(Gravity.RIGHT);
                            }

                        }
                    });
                    innerRow.addView(mEditAmount);
                }

                tableLayout.addView(innerRow,
                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onShowConfirmationDialog() {
        // TODO Auto-generated method stub

        try {

            sFixedDepositAmount = new String[sEditTextFields.size()];
            mRDSize = sEditTextFields.size();
            sFixedDepositTotal = Integer.valueOf("0");
            sSend_To_Server_FD = "0";

            for (int i = 0; i < sFixedDepositAmount.length; i++) {

                sFixedDepositAmount[i] = String.valueOf(sEditTextFields.get(i).getText());

                if (sFixedDepositAmount[i] == null || sFixedDepositAmount[i].equals("")) {
                    sFixedDepositAmount[i] = "0";
                }

                if (sFixedDepositAmount[i].matches("\\d*\\.?\\d+")) { // match
                    Log.e("Double valuesssssss", sFixedDepositAmount[i]);
                    int amount = (int) Math.round(Double.parseDouble(sFixedDepositAmount[i]));
                    sFixedDepositAmount[i] = String.valueOf(amount);
                }

                Log.e("Integer valuesssssss", sFixedDepositAmount[i]);

                sFixedDepositTotal = sFixedDepositTotal + Integer.parseInt(sFixedDepositAmount[i]);
                sSend_To_Server_FD = sSend_To_Server_FD + sFixedDepositAmount[i] + "~";
            }

            System.out.println(" Total Value:>>>>" + sFixedDepositTotal);

            System.out.println("Send To Confirmation Value:>>>" + sSend_To_Server_FD);

            bank_deposit = Integer.parseInt(sFixedDepositAmount[0]);
            bank_interest = Integer.parseInt(sFixedDepositAmount[1]);
            bank_withdrawl = Integer.parseInt(sFixedDepositAmount[2]);
            bank_expenses = Integer.parseInt(sFixedDepositAmount[3]);

            System.out.println("Bank Deposit:>>>" + bank_deposit);
            System.out.println("Bank Withdrawl:>>>" + bank_withdrawl);
            int fdAvailableWithdrawlAmt = 0;
            if (amount != null) {
                fdAvailableWithdrawlAmt = (int) Double.parseDouble(amount.trim()) + Integer.parseInt(sFixedDepositAmount[0])
                        + Integer.parseInt(sFixedDepositAmount[1]) - Integer.parseInt(sFixedDepositAmount[3]);
            } else {
                fdAvailableWithdrawlAmt = (int) Double.parseDouble("0.0") + Integer.parseInt(sFixedDepositAmount[0])
                        + Integer.parseInt(sFixedDepositAmount[1]) - Integer.parseInt(sFixedDepositAmount[3]);
            }

            System.out.println("Fixed Deposit Available Withdrawl Amount:" + fdAvailableWithdrawlAmt);
            //   if (sFixedDepositTotal != 0 && bank_withdrawl <= fdAvailableWithdrawlAmt) {

            if (bank_withdrawl > 0) {
                if (sFixedDepositTotal != 0 && bank_deposit <= ((int) Double.parseDouble(mSelectedCashatBank.trim()))
                        && bank_withdrawl <= fdAvailableWithdrawlAmt
                        && bank_expenses <= ((int) Double.parseDouble(mSelectedCashatBank.trim()))) {

                mDeposit_Fixed_offline = sFixedDepositAmount[0];
                mInterest_Fixed_offline = sFixedDepositAmount[1];
                mWithdrawl_Fixed_offline = sFixedDepositAmount[2];
                mBankExpenses_Fixed_offline = sFixedDepositAmount[3];

                confirmationDialog = new Dialog(getActivity());

                LayoutInflater inflater = getActivity().getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.dialog_new_confirmation, null);
                dialogView.setLayoutParams(
                        new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
                confirmationHeader.setText("" + (AppStrings.confirmation));
                confirmationHeader.setTypeface(LoginActivity.sTypeface);

                TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

                TableRow typeRow = new TableRow(getActivity());

                @SuppressWarnings("deprecation")
                TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                contentParams.setMargins(10, 5, 10, 5);

                TextView memberName_Text = new TextView(getActivity());
                memberName_Text.setText(
                        GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mTransferFromBank) + " " + "SB"));
                memberName_Text.setTypeface(LoginActivity.sTypeface);
                memberName_Text.setPadding(5, 5, 5, 5);
                memberName_Text.setLayoutParams(contentParams);
                typeRow.addView(memberName_Text);

                TextView confirm_values = new TextView(getActivity());
                confirm_values.setText(
                        GetSpanText.getSpanString(getActivity(), String.valueOf(BankTransaction.sSelectedBank.getBankName())));
                confirm_values.setPadding(5, 5, 5, 5);
                confirm_values.setGravity(Gravity.RIGHT);
                confirm_values.setLayoutParams(contentParams);
                typeRow.addView(confirm_values);

                confirmationTable.addView(typeRow,
                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                // if (ConnectionUtils.isNetworkAvailable(getActivity())) {
                TableRow sbAccNoRow = new TableRow(getActivity());

                @SuppressWarnings("deprecation")
                TableRow.LayoutParams sbAccParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                sbAccParams.setMargins(10, 5, 10, 5);

                TextView accNo_Text = new TextView(getActivity());
                accNo_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mAccountNumber)));
                accNo_Text.setTypeface(LoginActivity.sTypeface);
                accNo_Text.setPadding(5, 5, 5, 5);
                accNo_Text.setLayoutParams(sbAccParams);
                sbAccNoRow.addView(accNo_Text);

                TextView accNo_values = new TextView(getActivity());
                accNo_values
                        .setText(GetSpanText.getSpanString(getActivity(), String.valueOf((selectedSavinAc != null && selectedSavinAc.getAccountNumber() != null && selectedSavinAc.getAccountNumber().length() > 0) ? selectedSavinAc.getAccountNumber() : "NA")));
                accNo_values.setPadding(5, 5, 5, 5);
                accNo_values.setGravity(Gravity.RIGHT);
                accNo_values.setLayoutParams(sbAccParams);
                sbAccNoRow.addView(accNo_values);

                confirmationTable.addView(sbAccNoRow,
                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                // }

                TableRow bankNameRow = new TableRow(getActivity());

                @SuppressWarnings("deprecation")
                TableRow.LayoutParams bankNameParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                bankNameParams.setMargins(10, 5, 10, 5);

                TextView bankName_Text = new TextView(getActivity());
                if (selectedRadio.equals("SAVINGSACCOUNT")) {
                    bankName_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mTransferToBank)));
                    bankName_Text.setTypeface(LoginActivity.sTypeface);
                } else {
                    bankName_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mLoanAccType)));
                    bankName_Text.setTypeface(LoginActivity.sTypeface);
                }
                bankName_Text.setPadding(5, 5, 5, 5);
                bankName_Text.setLayoutParams(bankNameParams);
                bankNameRow.addView(bankName_Text);

                TextView bankName_values = new TextView(getActivity());
                if (selectedRadio.equals("SAVINGSACCOUNT")) {
                    bankName_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(selectedSavinAc.getBankName())));
                } else {
                    bankName_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(mLoanAccValue)));
                }

                bankName_values.setPadding(5, 5, 5, 5);
                bankName_values.setGravity(Gravity.RIGHT);
                bankName_values.setLayoutParams(bankNameParams);
                bankNameRow.addView(bankName_values);

                confirmationTable.addView(bankNameRow,
                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                if (!selectedRadio.equals("SAVINGSACCOUNT")) {
                    TableRow sbAccNoRow1 = new TableRow(getActivity());

                    @SuppressWarnings("deprecation")
                    TableRow.LayoutParams sbAccParams1 = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                    sbAccParams1.setMargins(10, 5, 10, 5);

                    TextView accNo_Text1 = new TextView(getActivity());
                    accNo_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mAccountNumber)));
                    accNo_Text1.setTypeface(LoginActivity.sTypeface);
                    accNo_Text1.setPadding(5, 5, 5, 5);
                    accNo_Text1.setLayoutParams(sbAccParams1);
                    sbAccNoRow1.addView(accNo_Text1);

                    TextView accNo_values1 = new TextView(getActivity());
                    accNo_values1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(ToLoanAccNo)));
                    accNo_values1.setPadding(5, 5, 5, 5);
                    accNo_values1.setGravity(Gravity.RIGHT);
                    accNo_values1.setLayoutParams(sbAccParams1);
                    sbAccNoRow1.addView(accNo_values1);

                    confirmationTable.addView(sbAccNoRow1,
                            new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                }


                for (int i = 0; i < mRDSize; i++) {

                    TableRow indv_DepositEntryRow = new TableRow(getActivity());

                    TableRow.LayoutParams contentParams1 = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                    contentParams1.setMargins(10, 5, 10, 5);

                    TextView memberName_Text1 = new TextView(getActivity());
                    memberName_Text1.setText(
                            GetSpanText.getSpanString(getActivity(), String.valueOf(sFixedDepositItem[i])));
                    memberName_Text1.setTypeface(LoginActivity.sTypeface);
                    memberName_Text1.setTextColor(R.color.black);
                    memberName_Text1.setPadding(5, 5, 5, 5);
                    memberName_Text1.setLayoutParams(contentParams1);
                    indv_DepositEntryRow.addView(memberName_Text1);

                    TextView confirm_values1 = new TextView(getActivity());
                    confirm_values1.setText(
                            GetSpanText.getSpanString(getActivity(), String.valueOf(sFixedDepositAmount[i])));
                    confirm_values1.setTextColor(R.color.black);
                    confirm_values1.setPadding(5, 5, 5, 5);
                    confirm_values1.setGravity(Gravity.RIGHT);
                    confirm_values1.setLayoutParams(contentParams1);
                    indv_DepositEntryRow.addView(confirm_values1);

                    confirmationTable.addView(indv_DepositEntryRow,
                            new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                }


                mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit);
                mEdit_RaisedButton.setText("" + (AppStrings.edit));
                mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
                mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
                // 205,
                // 0));
                mEdit_RaisedButton.setOnClickListener(this);

                mOk_RaisedButton = (Button) dialogView.findViewById(R.id.frag_Ok);
                mOk_RaisedButton.setText("" + AppStrings.yes);
                mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
                mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
                mOk_RaisedButton.setOnClickListener(this);

                confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                confirmationDialog.setCanceledOnTouchOutside(false);
                confirmationDialog.setContentView(dialogView);
                confirmationDialog.setCancelable(true);
                confirmationDialog.show();

                ViewGroup.MarginLayoutParams margin = (ViewGroup.MarginLayoutParams) dialogView.getLayoutParams();
                margin.leftMargin = 10;
                margin.rightMargin = 10;
                margin.topMargin = 10;
                margin.bottomMargin = 10;
                margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);
            } else if (bank_withdrawl > fdAvailableWithdrawlAmt) {

                TastyToast.makeText(getActivity(), AppStrings.cashatBankAlert, TastyToast.LENGTH_SHORT,
                        TastyToast.WARNING);
                sSend_To_Server_FD = "0";
                sFixedDepositTotal = Integer.valueOf("0");

            } else if (bank_withdrawl > fdAvailableWithdrawlAmt
                    || bank_expenses > ((int) Double.parseDouble(mSelectedCashatBank.trim()))) {

                if (bank_expenses > ((int) Double.parseDouble(mSelectedCashatBank.trim()))) {

                    TastyToast.makeText(getActivity(), AppStrings.cashatBankAlert, TastyToast.LENGTH_SHORT,
                            TastyToast.WARNING);
                } else if (bank_withdrawl > fdAvailableWithdrawlAmt) {

                    TastyToast.makeText(getActivity(), AppStrings.mCheckFixedDepositAmount, TastyToast.LENGTH_SHORT,
                            TastyToast.WARNING);

                }

                sSend_To_Server_FD = "0";
                sFixedDepositTotal = Integer.valueOf("0");

            } else {
                TastyToast.makeText(getActivity(), AppStrings.DepositnullAlert, TastyToast.LENGTH_SHORT,
                        TastyToast.WARNING);

                sSend_To_Server_FD = "0";
                sFixedDepositTotal = Integer.valueOf("0");

                }

            } else {


                mDeposit_Fixed_offline = sFixedDepositAmount[0];
                mInterest_Fixed_offline = sFixedDepositAmount[1];
                mWithdrawl_Fixed_offline = sFixedDepositAmount[2];
                mBankExpenses_Fixed_offline = sFixedDepositAmount[3];

                confirmationDialog = new Dialog(getActivity());

                LayoutInflater inflater = getActivity().getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.dialog_new_confirmation, null);
                dialogView.setLayoutParams(
                        new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
                confirmationHeader.setText("" + (AppStrings.confirmation));
                confirmationHeader.setTypeface(LoginActivity.sTypeface);

                TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

                TableRow typeRow = new TableRow(getActivity());

                @SuppressWarnings("deprecation")
                TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                contentParams.setMargins(10, 5, 10, 5);

                TextView memberName_Text = new TextView(getActivity());
                memberName_Text.setText(
                        GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mTransferFromBank) + " " + "SB"));
                memberName_Text.setTypeface(LoginActivity.sTypeface);
                memberName_Text.setPadding(5, 5, 5, 5);
                memberName_Text.setLayoutParams(contentParams);
                typeRow.addView(memberName_Text);

                TextView confirm_values = new TextView(getActivity());
                confirm_values.setText(
                        GetSpanText.getSpanString(getActivity(), String.valueOf(BankTransaction.sSelectedBank.getBankName())));
                confirm_values.setPadding(5, 5, 5, 5);
                confirm_values.setGravity(Gravity.RIGHT);
                confirm_values.setLayoutParams(contentParams);
                typeRow.addView(confirm_values);

                confirmationTable.addView(typeRow,
                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                // if (ConnectionUtils.isNetworkAvailable(getActivity())) {
                TableRow sbAccNoRow = new TableRow(getActivity());

                @SuppressWarnings("deprecation")
                TableRow.LayoutParams sbAccParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                sbAccParams.setMargins(10, 5, 10, 5);

                TextView accNo_Text = new TextView(getActivity());
                accNo_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mAccountNumber)));
                accNo_Text.setTypeface(LoginActivity.sTypeface);
                accNo_Text.setPadding(5, 5, 5, 5);
                accNo_Text.setLayoutParams(sbAccParams);
                sbAccNoRow.addView(accNo_Text);

                TextView accNo_values = new TextView(getActivity());
                accNo_values
                        .setText(GetSpanText.getSpanString(getActivity(), String.valueOf((selectedSavinAc != null && selectedSavinAc.getAccountNumber() != null && selectedSavinAc.getAccountNumber().length() > 0) ? selectedSavinAc.getAccountNumber() : "NA")));
                accNo_values.setPadding(5, 5, 5, 5);
                accNo_values.setGravity(Gravity.RIGHT);
                accNo_values.setLayoutParams(sbAccParams);
                sbAccNoRow.addView(accNo_values);

                confirmationTable.addView(sbAccNoRow,
                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                // }

                TableRow bankNameRow = new TableRow(getActivity());

                @SuppressWarnings("deprecation")
                TableRow.LayoutParams bankNameParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                bankNameParams.setMargins(10, 5, 10, 5);

                TextView bankName_Text = new TextView(getActivity());
                if (!selectedRadio.equals("NONE")) {
                    if (selectedRadio.equals("SAVINGSACCOUNT")) {
                        bankName_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mTransferToBank)));
                        bankName_Text.setTypeface(LoginActivity.sTypeface);
                    } else {
                        bankName_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mLoanAccType)));
                        bankName_Text.setTypeface(LoginActivity.sTypeface);
                    }
                    bankName_Text.setPadding(5, 5, 5, 5);
                    bankName_Text.setLayoutParams(bankNameParams);
                    bankNameRow.addView(bankName_Text);
                }

                TextView bankName_values = new TextView(getActivity());
                if (!selectedRadio.equals("NONE")) {
                    if (selectedRadio.equals("SAVINGSACCOUNT")) {
                        bankName_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(selectedSavinAc.getBankName())));
                    } else {
                        bankName_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(mLoanAccValue)));
                    }


                    bankName_values.setPadding(5, 5, 5, 5);
                    bankName_values.setGravity(Gravity.RIGHT);
                    bankName_values.setLayoutParams(bankNameParams);
                    bankNameRow.addView(bankName_values);
                }
                confirmationTable.addView(bankNameRow,
                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                if (!selectedRadio.equals("NONE")) {
                    if (!selectedRadio.equals("SAVINGSACCOUNT")) {
                        TableRow sbAccNoRow1 = new TableRow(getActivity());

                        @SuppressWarnings("deprecation")
                        TableRow.LayoutParams sbAccParams1 = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                        sbAccParams1.setMargins(10, 5, 10, 5);

                        TextView accNo_Text1 = new TextView(getActivity());
                        accNo_Text1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mAccountNumber)));
                        accNo_Text1.setTypeface(LoginActivity.sTypeface);
                        accNo_Text1.setPadding(5, 5, 5, 5);
                        accNo_Text1.setLayoutParams(sbAccParams1);
                        sbAccNoRow1.addView(accNo_Text1);

                        TextView accNo_values1 = new TextView(getActivity());
                        accNo_values1.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(ToLoanAccNo)));
                        accNo_values1.setPadding(5, 5, 5, 5);
                        accNo_values1.setGravity(Gravity.RIGHT);
                        accNo_values1.setLayoutParams(sbAccParams1);
                        sbAccNoRow1.addView(accNo_values1);

                        confirmationTable.addView(sbAccNoRow1,
                                new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    }
                }

                for (int i = 0; i < mRDSize; i++) {

                    TableRow indv_DepositEntryRow = new TableRow(getActivity());

                    TableRow.LayoutParams contentParams1 = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                    contentParams1.setMargins(10, 5, 10, 5);

                    TextView memberName_Text1 = new TextView(getActivity());
                    memberName_Text1.setText(
                            GetSpanText.getSpanString(getActivity(), String.valueOf(sFixedDepositItem[i])));
                    memberName_Text1.setTypeface(LoginActivity.sTypeface);
                    memberName_Text1.setTextColor(R.color.black);
                    memberName_Text1.setPadding(5, 5, 5, 5);
                    memberName_Text1.setLayoutParams(contentParams1);
                    indv_DepositEntryRow.addView(memberName_Text1);

                    TextView confirm_values1 = new TextView(getActivity());
                    confirm_values1.setText(
                            GetSpanText.getSpanString(getActivity(), String.valueOf(sFixedDepositAmount[i])));
                    confirm_values1.setTextColor(R.color.black);
                    confirm_values1.setPadding(5, 5, 5, 5);
                    confirm_values1.setGravity(Gravity.RIGHT);
                    confirm_values1.setLayoutParams(contentParams1);
                    indv_DepositEntryRow.addView(confirm_values1);

                    confirmationTable.addView(indv_DepositEntryRow,
                            new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                }


                mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit);
                mEdit_RaisedButton.setText("" + (AppStrings.edit));
                mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
                mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
                // 205,
                // 0));
                mEdit_RaisedButton.setOnClickListener(this);

                mOk_RaisedButton = (Button) dialogView.findViewById(R.id.frag_Ok);
                mOk_RaisedButton.setText("" + AppStrings.yes);
                mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
                mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
                mOk_RaisedButton.setOnClickListener(this);

                confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                confirmationDialog.setCanceledOnTouchOutside(false);
                confirmationDialog.setContentView(dialogView);
                confirmationDialog.setCancelable(true);
                confirmationDialog.show();

                ViewGroup.MarginLayoutParams margin = (ViewGroup.MarginLayoutParams) dialogView.getLayoutParams();
                margin.leftMargin = 10;
                margin.rightMargin = 10;
                margin.topMargin = 10;
                margin.bottomMargin = 10;
                margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);


            }
        } catch (
                Exception e)

        {
            e.printStackTrace();
        }

    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_new_rd_layout, container, false);
        ply = (LinearLayout) rootView.findViewById(R.id.pLy);
        ply.setVisibility(View.GONE);
        return rootView;
    }


    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        if (mProgressDilaog != null) {
            mProgressDilaog.dismiss();
            mProgressDilaog = null;
           /* if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {

                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();
            }*/
        }
        switch (serviceType) {

            case LOANTYPE:
                try {
                    ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    String message = cdto.getMessage();
                    int statusCode = cdto.getStatusCode();
                    if (statusCode == 400 || statusCode == 403 || statusCode == 500 || statusCode == 503 || statusCode == 409) {                       // showMessage(statusCode);
                        Utils.showToast(getActivity(), message);
                        if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                            mProgressDilaog.dismiss();
                            mProgressDilaog = null;
                        }
                    } else if (statusCode == 401) {

                        Log.e("Group Logout", "Logout Sucessfully");
                        AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                        if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                            mProgressDilaog.dismiss();
                            mProgressDilaog = null;
                        }
                    } else if (statusCode == Utils.Success_Code) {

                        if (mProgressDilaog != null) {
                            mProgressDilaog.dismiss();
                            mProgressDilaog = null;
           /* if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {

                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();
            }*/
                        }
                        Utils.showToast(getActivity(), message);
                        for (int i = 0; i < cdto.getResponseContent().getLoansList().size(); i++) {
                            ExistingLoan loanDto = cdto.getResponseContent().getLoansList().get(i);
                            loanDto.setShgId(shgDto.getShgId());
                            LoanTable.insertLoanDetails(loanDto);
                        }

                        loanDetails = LoanTable.getLoanDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
                        init();

                    } else {
                        if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                            mProgressDilaog.dismiss();
                            mProgressDilaog = null;
                        }
                    }
                } catch (Exception e) {

                }
                break;

            case BT_RD:
                try {
                    ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    String message = cdto.getMessage();
                    int statusCode = cdto.getStatusCode();
                    if (statusCode == Utils.Success_Code) {

                        if (mProgressDilaog != null) {
                            mProgressDilaog.dismiss();
                            mProgressDilaog = null;

                        }
                        Utils.showToast(getActivity(), message);
                        FragmentManager fm = getFragmentManager();
                        fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        MainFragment mainFragment = new MainFragment();
                        Bundle bundles = new Bundle();
                        bundles.putString("Transaction", MainFragment.Flag_Transaction);
                        mainFragment.setArguments(bundles);
                        NewDrawerScreen.showFragment(mainFragment);
//                        MemberDrawerScreen.showFragment(new MainFragment());
                    } else {

                        if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        }
                        Utils.showToast(getActivity(), message);
                    }
                } catch (Exception e) {

                }
                break;

            case RD_VALUE:
                try {
                    ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    String message = cdto.getMessage();
                    int statusCode = cdto.getStatusCode();
                    if (statusCode == Utils.Success_Code) {
                        Utils.showToast(getActivity(), message);
                        amount = cdto.getResponseContent().getSavingsBalance().getRecurringDepositedBalance();
                        mSelectedCashatBank = cdto.getResponseContent().getSavingsBalance().getCurrentBalance();
                        //init();
                    } else {

                        if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        }
                        Utils.showToast(getActivity(), message);

                    }
                } catch (Exception e) {

                }
                break;

        }

    }
}
