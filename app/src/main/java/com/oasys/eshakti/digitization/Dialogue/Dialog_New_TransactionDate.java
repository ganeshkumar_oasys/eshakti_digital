package com.oasys.eshakti.digitization.Dialogue;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.oasys.eshakti.digitization.Dto.CashOfGroup;
import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.Dto.MemberList;
import com.oasys.eshakti.digitization.Dto.ShgBankDetails;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.activity.LoginActivity;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;
import com.oasys.eshakti.digitization.database.SHGTable;
import com.oasys.eshakti.digitization.fragment.Attendance;
import com.oasys.eshakti.digitization.fragment.BankTransaction;
import com.oasys.eshakti.digitization.fragment.CashWithdrawal;
import com.oasys.eshakti.digitization.fragment.Deposit;
import com.oasys.eshakti.digitization.fragment.Expense;
import com.oasys.eshakti.digitization.fragment.GroupLoanRepayment;
import com.oasys.eshakti.digitization.fragment.Income;
import com.oasys.eshakti.digitization.fragment.LoanDisbursement;
import com.oasys.eshakti.digitization.fragment.Meeting_audit_Fragment;
import com.oasys.eshakti.digitization.fragment.Meetings_training;
import com.oasys.eshakti.digitization.fragment.MinutesOFMeeting;
import com.oasys.eshakti.digitization.fragment.FinancialConfirmation;
import com.oasys.eshakti.digitization.fragment.Savings;
import com.oasys.eshakti.digitization.fragment.Transaction_memberloan_repayment;
import com.oasys.eshakti.digitization.views.ButtonFlat;
import com.tutorialsee.lib.TastyToast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Dialog_New_TransactionDate extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    static String sItem = null;
    static Context mContext;
    private View rootView;
    private int mSize;
    private List<MemberList> memList;
    private ArrayList<ShgBankDetails> bankdetails;
    private ListOfShg shgDto;
    private NetworkConnection networkConnection;
    SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
    private Date lastTransaDate, systemDate, openingDate, mod_date, newTransa_Date;
    private String shgformationMillis;

    public Dialog_New_TransactionDate() {
        // TODO Auto-generated constructor stub
    }

    @SuppressLint("ValidFragment")
    public Dialog_New_TransactionDate(Context context, String item) {
        // TODO Auto-generated constructor stub
        mContext = context;
        sItem = item;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        // setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        rootView = inflater.inflate(R.layout.dialog_new_transactiondate, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        init();
    }

    private void init() {
        // networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        TextView dialogMsg = (TextView) rootView.findViewById(R.id.dialog_continueDate);
        dialogMsg.setTypeface(LoginActivity.sTypeface);
        TextView dialogTrans = (TextView) rootView.findViewById(R.id.dialog_TransactionDate);
        dialogTrans.setTypeface(LoginActivity.sTypeface);

        DateFormat simple = new SimpleDateFormat("dd-MM-yyyy");
        DateFormat simple1 = new SimpleDateFormat("dd/MM/yyyy");
     /*   Date d = new Date(Long.parseLong(SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, "")).getLastTransactionDate()));
        String dateStr = simple.format(d);
        dialogMsg.setText(dateStr + " : " + AppStrings.dialogMsg);*/

        if (shgDto.getLastTransactionDate() != null && shgDto.getLastTransactionDate().length() > 0) {
            Date d = new Date(Long.parseLong(shgDto.getLastTransactionDate()));
            String dateStr = simple.format(d);
            String dateStr1 = simple1.format(d);

            try {
                Calendar calender = Calendar.getInstance();
                String formattedDate = df.format(calender.getTime());
                String mBalanceSheetDate = shgDto.getLastTransactionDate(); // dd/MM/yyyy
                String modifiedDate = null;
                if (shgDto.getModifiedDate() != null) {
                    modifiedDate = shgDto.getModifiedDate();
                    Date d1 = new Date(Long.parseLong(mBalanceSheetDate));
                    Date d2 = new Date(Long.parseLong(modifiedDate));
                    String dateStr3 = df.format(d1);
                    String formatted_balancesheetDate = dateStr3;

                    String dateStr4 = df.format(d2);
                    String formatted_modifiedDate = dateStr4;
                    Log.e("Balancesheet Date = ", formatted_balancesheetDate + "");
                    lastTransaDate = df.parse(formatted_balancesheetDate);
                    mod_date = df.parse(formatted_modifiedDate);
                    systemDate = df.parse(formattedDate);
                } else {
                    Date d1 = new Date(Long.parseLong(mBalanceSheetDate));
                    //   Date d2 = new Date(Long.parseLong(modifiedDate));
                    String dateStr3 = df.format(d1);
                    String formatted_balancesheetDate = dateStr3;

                    Log.e("Balancesheet Date = ", formatted_balancesheetDate + "");
                    lastTransaDate = df.parse(formatted_balancesheetDate);
                    mod_date = df.parse(formattedDate);
                    systemDate = df.parse(formattedDate);
                }
                // String mBalanceSheetDate = "1512844200000";


                if (mod_date != null) {
                    if (mod_date.compareTo(systemDate) == 0) {
                        //  if (shgDto.getFFlag() == "1") {  //TODO:: FIRST TRANSADATE UPDATE
                        Calendar cInstance = Calendar.getInstance();
                        Calendar lcal = Calendar.getInstance();
                        lcal.setTimeInMillis(lastTransaDate.getTime());
                        int lyear = lcal.get(Calendar.YEAR); // this is deprecated
                        int lmonth = lcal.get(Calendar.MONTH); // this is deprecated
                        int lday = lcal.get(Calendar.DATE);
                        Calendar ccal1 = Calendar.getInstance();
                        ccal1.setTimeInMillis(systemDate.getTime());
                        int cyear = ccal1.get(Calendar.YEAR); // this is deprecated
                        int cmonth = ccal1.get(Calendar.MONTH); // this is deprecated
                        int cday = ccal1.get(Calendar.DATE);
                        if (cday == lday && cmonth == lmonth && cyear == lyear) {
                            cInstance.set(cyear, cmonth, cday);
                        } else {

                            int diffYear = ccal1.get(Calendar.YEAR) - lcal.get(Calendar.YEAR);
                            int diffMonth = diffYear * 12 + ccal1.get(Calendar.MONTH) - lcal.get(Calendar.MONTH);
                            //
                            // if (((cmonth - lmonth) <= 1 && (cyear == lyear || cyear > lyear)) && ((lmonth - cmonth) <= 1 && (cyear == lyear || cyear > lyear))) {
                            if (diffMonth <= 1) {
                                cInstance.set(cyear, cmonth, cday);
                            } else if (diffMonth > 1) {
                                if (shgDto.getFFlag() == null || !shgDto.getFFlag().equals("1")) {
                                    Calendar c = Calendar.getInstance();
                                    c.setTime(lastTransaDate);
                                    c.add(Calendar.MONTH, 1);
                                    c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));
                                    cInstance.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE));
                                } else {
                                    Calendar c = Calendar.getInstance();
                                    c.setTime(lastTransaDate);
                                   /* c.add(Calendar.MONTH, 1);
                                    c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));*/
                                    cInstance.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE));
                                }
                                //    cInstance.set(cyear, cmonth, cday);
                            }

                        }
                        dateStr = df.format(cInstance.getTime());
                        newTransa_Date = df.parse(dateStr);
                        /*} else {  //TODO:: fIRST tRANSAdATE UPDATE
                            Calendar cInstance = Calendar.getInstance();
                            Calendar lcal = Calendar.getInstance();
                            lcal.setTimeInMillis(lastTransaDate.getTime());
                            int lyear = lcal.get(Calendar.YEAR); // this is deprecated
                            int lmonth = lcal.get(Calendar.MONTH); // this is deprecated
                            int lday = lcal.get(Calendar.DATE);

                            Calendar ccal1 = Calendar.getInstance();
                            ccal1.setTimeInMillis(systemDate.getTime());
                            int cyear = ccal1.get(Calendar.YEAR); // this is deprecated
                            int cmonth = ccal1.get(Calendar.MONTH); // this is deprecated
                            int cday = ccal1.get(Calendar.DATE);

                        }*/


                    } else if (mod_date.compareTo(systemDate) < 0) {

                        Calendar cInstance = Calendar.getInstance();
                        Calendar lcal = Calendar.getInstance();
                        lcal.setTimeInMillis(lastTransaDate.getTime());
                        int lyear = lcal.get(Calendar.YEAR); // this is deprecated
                        int lmonth = lcal.get(Calendar.MONTH); // this is deprecated
                        int lday = lcal.get(Calendar.DATE);
                        Calendar ccal1 = Calendar.getInstance();
                        ccal1.setTimeInMillis(systemDate.getTime());
                        int cyear = ccal1.get(Calendar.YEAR); // this is deprecated
                        int cmonth = ccal1.get(Calendar.MONTH); // this is deprecated
                        int cday = ccal1.get(Calendar.DATE);
                        if ((((cmonth - lmonth) <= 1 && (lmonth - cmonth) <= 1)) && (cyear == lyear || cyear > lyear)) {
                            cInstance.set(cyear, cmonth, cday);
                        } else {
                            Calendar c = Calendar.getInstance();
                            c.setTime(lastTransaDate);
                            c.add(Calendar.MONTH, 1);
                            c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));
                            cInstance.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE));

                        }
                        dateStr = df.format(cInstance.getTime());
                        newTransa_Date = df.parse(dateStr);
                    }

                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

            dialogMsg.setText(dateStr + " : " + AppStrings.dialogMsg);
//            dialogMsg.setText(EShaktiApplication.vertficationDto.getShggroupDetailsDTO().getTransactionDate() + " : " + AppStrings.dialogMsg);
            dialogTrans.setText(AppStrings.lastTransactionDate + " " + dateStr1);
        } else {
            Date d = new Date();
            String opdateStr = simple.format(d);
            String opdateStr1 = simple1.format(d);
            dialogMsg.setText(opdateStr + " : " + AppStrings.dialogMsg);
            dialogTrans.setText(AppStrings.lastTransactionDate + opdateStr1);
        }

        ButtonFlat okButton = (ButtonFlat) rootView.findViewById(R.id.f_Yes_button);
        okButton.setText(AppStrings.dialogOk);
        okButton.setTypeface(LoginActivity.sTypeface);

        ButtonFlat noButton = (ButtonFlat) rootView.findViewById(R.id.f_No_button);
        noButton.setText(AppStrings.dialogNo);
        noButton.setTypeface(LoginActivity.sTypeface);


        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();

                try {
                    lastTransaDate = newTransa_Date;
                    MySharedPreference.writeString(getActivity(), MySharedPreference.LAST_TRANSACTION, lastTransaDate.getTime() + "");
                    CashOfGroup cg = new CashOfGroup();
                    cg.setCashAtBank(shgDto.getCashAtBank());
                    cg.setCashInHand(shgDto.getCashInHand());
                    cg.setLastTransactionDate(lastTransaDate.getTime() + "");
                    SHGTable.updateSHGDetails(cg, shgDto.getShgId());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                switch (sItem) {
                    case NewDrawerScreen.DEPOSIT:
                        NewDrawerScreen.showFragment(new Deposit());
                        break;
//                    case NewDrawerScreen.DEPOSIT:
//                        NewDrawerScreen.showFragment(new Audit_fragment());
//                        break;
                    case NewDrawerScreen.SAVINGS:
                        NewDrawerScreen.showFragment(new Savings());
                        break;
                    case NewDrawerScreen.INCOME:
                        NewDrawerScreen.showFragment(new Income());
                        break;
                    case NewDrawerScreen.EXPENCE:
                        NewDrawerScreen.showFragment(new Expense());
                        break;
                    case NewDrawerScreen.BANK_TRANSACTION:
                        NewDrawerScreen.showFragment(new BankTransaction());
                        break;

                    case NewDrawerScreen.MEMBER_LOAN_REPAYMENT:
                        NewDrawerScreen.showFragment(new Transaction_memberloan_repayment());
                        break;

                    case NewDrawerScreen.LOAN_DISBURSEMENT:
                        NewDrawerScreen.showFragment(new LoanDisbursement());
                        break;

                    case NewDrawerScreen.GROUP_LOAN_REPAYMENT:
                        NewDrawerScreen.showFragment(new GroupLoanRepayment());
                        break;
                    case NewDrawerScreen.MINUTES_OF_MEETINGS:
                        NewDrawerScreen.showFragment(new MinutesOFMeeting());
                        break;
                    case NewDrawerScreen.ATTENDANCE:
                        NewDrawerScreen.showFragment(new Attendance());
                        break;

                    case NewDrawerScreen.CW:
                        NewDrawerScreen.showFragment(new CashWithdrawal());
                        break;
                    case NewDrawerScreen.AUDITING:
                        NewDrawerScreen.showFragment(new Meeting_audit_Fragment());
                        break;
                    case NewDrawerScreen.TRAINING:
                        NewDrawerScreen.showFragment(new Meetings_training());
                        break;

                    case NewDrawerScreen.STEP_WISE:
                        Attendance attendancefragment = new Attendance();
                        Bundle bundle = new Bundle();
                        bundle.putString("stepwise", "1");
                        attendancefragment.setArguments(bundle);
                        NewDrawerScreen.showFragment(attendancefragment);
                        break;

                    case NewDrawerScreen.FINANCIAL_VERIFICATION:

                        NewDrawerScreen.showFragment(new FinancialConfirmation());
                        break;
                }
            }
        });

        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    dismiss();
                    if (shgDto != null)
                        shgformationMillis = shgDto.getOpeningDate();
                    Date opDate = new Date(Long.parseLong(shgformationMillis));
                    String formattedDate1 = df.format(opDate.getTime());
                    openingDate = df.parse(formattedDate1);

                    Calendar calender = Calendar.getInstance();

                    String formattedDate = df.format(calender.getTime());
                    Log.e("Device Date  =  ", formattedDate + "");

                    String mBalanceSheetDate = shgDto.getLastTransactionDate(); // dd/MM/yyyy
                    // String mBalanceSheetDate = "1512844200000";
                    Date d = new Date(Long.parseLong(mBalanceSheetDate));
                    String dateStr = df.format(d);
                    String formatted_balancesheetDate = dateStr;
                    Log.e("Balancesheet Date = ", formatted_balancesheetDate + "");

                    lastTransaDate = df.parse(formatted_balancesheetDate);
                    systemDate = df.parse(formattedDate);
                    if (openingDate.compareTo(lastTransaDate) < 0) {
                        if (lastTransaDate.compareTo(systemDate) < 0 || lastTransaDate.compareTo(systemDate) == 0) {

                            calendarDialogShow(view);
                        } else {
                            TastyToast.makeText(getActivity(), "PLEASE SET YOUR DEVICE DATE CORRECTLY",
                                    TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                        }
                    } else {
                        Toast.makeText(getActivity(), "Check Group opening date", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }


        });
        ImageView image = (ImageView) rootView.findViewById(R.id.dialog_imageView);
        image.setBackgroundResource(R.drawable.lauchericon);

    }

    private void calendarDialogShow(View view) {


        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);

// ...Irrelevant code for customizing the buttons and title


        LayoutInflater inflater = LayoutInflater.from(mContext);
        View customView = inflater.inflate(R.layout.datepickerlayout_custom_ly, null);
        dialogBuilder.setView(customView);
        final DatePicker dpStartDate = (DatePicker) customView.findViewById(R.id.dpStartDate);
        final TextView title = (TextView) customView.findViewById(R.id.title);
        final TextView sub_tit = (TextView) customView.findViewById(R.id.sub_tit);

        Calendar now = Calendar.getInstance();
        // final DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this, now.get(Calendar.YEAR), now.get(Calendar.DATE), now.get(Calendar.DAY_OF_MONTH));
        Calendar min_Cal = Calendar.getInstance();
        Calendar lastDate = Calendar.getInstance();

        Calendar fcal = Calendar.getInstance();
        fcal.setTimeInMillis(openingDate.getTime());
        int fyear = fcal.get(Calendar.YEAR); // this is deprecated
        int fmonth = fcal.get(Calendar.MONTH); // this is deprecated
        int fday = fcal.get(Calendar.DATE);

        Calendar lcal = Calendar.getInstance();
        lcal.setTimeInMillis(lastTransaDate.getTime());
        int lyear = lcal.get(Calendar.YEAR); // this is deprecated
        int lmonth = lcal.get(Calendar.MONTH); // this is deprecated
        int lday = lcal.get(Calendar.DATE);

        Calendar ccal1 = Calendar.getInstance();
        ccal1.setTimeInMillis(systemDate.getTime());
        int cyear = ccal1.get(Calendar.YEAR); // this is deprecated
        int cmonth = ccal1.get(Calendar.MONTH); // this is deprecated
        int cday = ccal1.get(Calendar.DATE);

        if (fday == lday && fmonth == lmonth && fyear == lyear) {
            min_Cal.set(lyear, lmonth, lday);
            lastDate.set(cyear, cmonth, cday);
        } else if (fyear <= lyear) {

            if (cmonth == lmonth && cyear == lyear) {
                min_Cal.set(lyear, lmonth, lday);
                lastDate.set(cyear, cmonth, cday);

            } else if (cmonth <= lmonth && cyear > lyear) {
                min_Cal.set(lyear, lmonth, lday);
                Calendar c = Calendar.getInstance();
                c.setTime(lastTransaDate);
                c.add(Calendar.MONTH, 1);
                c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
                lastDate.set(lyear, c.get(Calendar.MONTH), c.get(Calendar.DATE));

            } else if (cmonth > lmonth && cyear == lyear) {
                min_Cal.set(lyear, lmonth, lday);
                if ((cmonth - lmonth) == 1) {
                    lastDate.set(cyear, cmonth, cday);
                } else if ((cmonth - lmonth) > 1) {
                    Calendar c = Calendar.getInstance();
                    c.setTime(lastTransaDate);
                    c.add(Calendar.MONTH, 1);
                    c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
                    lastDate.set(cyear, c.get(Calendar.MONTH), c.get(Calendar.DATE));
                }

            } else if (cmonth > lmonth && cyear > lyear) {
                min_Cal.set(lyear, lmonth, lday);
                Calendar c = Calendar.getInstance();
                c.setTime(lastTransaDate);
                c.add(Calendar.MONTH, 1);
                c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
                lastDate.set(lyear, c.get(Calendar.MONTH), c.get(Calendar.DATE));
            }

        }

      /*  datePickerDialog.getDatePicker().setMinDate(min_Cal.getTimeInMillis());
        datePickerDialog.getDatePicker().setMaxDate(lastDate.getTimeInMillis());*/

        dpStartDate.setMinDate(min_Cal.getTimeInMillis());
        dpStartDate.setMaxDate(lastDate.getTimeInMillis());


        dpStartDate.setOnDateChangedListener(new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker datePicker, int year, int month, int dayOfMonth) {
                Log.e("onDateSet() call", dayOfMonth + "-" + month + "-" + year);
                //String date = dayOfMonth + "-" + (month + 1) + "-" + year;
                try {

                    Calendar c = Calendar.getInstance();
                    c.set(year, month, dayOfMonth);
                    String formattedDate = df.format(c.getTime());
                    lastTransaDate = df.parse(formattedDate);
                    MySharedPreference.writeString(getActivity(), MySharedPreference.LAST_TRANSACTION, lastTransaDate.getTime() + "");

                    CashOfGroup cg = new CashOfGroup();
                    cg.setCashAtBank(shgDto.getCashAtBank());
                    cg.setCashInHand(shgDto.getCashInHand());
                    cg.setLastTransactionDate(lastTransaDate.getTime() + "");
                    SHGTable.updateSHGDetails(cg, shgDto.getShgId());
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });


        Calendar c = Calendar.getInstance();
        c.setTime(lastTransaDate);
        DateFormat simple1 = new SimpleDateFormat("dd/MM/yyyy");
        String dateStr = simple1.format(c.getTime());

        title.setText(dateStr + " PICK A DATE AFTER THIS DATE");
        sub_tit.setText("Selected Date:");

        dialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                switch (sItem) {
                    case NewDrawerScreen.DEPOSIT:
                        NewDrawerScreen.showFragment(new Deposit());
                        break;
                    case NewDrawerScreen.SAVINGS:
                        NewDrawerScreen.showFragment(new Savings());
                        break;
                    case NewDrawerScreen.INCOME:
                        NewDrawerScreen.showFragment(new Income());
                        break;
                    case NewDrawerScreen.EXPENCE:
                        NewDrawerScreen.showFragment(new Expense());
                        break;
                    case NewDrawerScreen.BANK_TRANSACTION:
                        NewDrawerScreen.showFragment(new BankTransaction());
                        break;
                    case NewDrawerScreen.LOAN_DISBURSEMENT:
                        NewDrawerScreen.showFragment(new LoanDisbursement());
                        break;
                    case NewDrawerScreen.GROUP_LOAN_REPAYMENT:
                        NewDrawerScreen.showFragment(new GroupLoanRepayment());
                        break;
                    case NewDrawerScreen.CREDIT_LINKAGE_INFO:
                        break;
                    case NewDrawerScreen.ANIMATOR_PROFILE:
                        break;
                    case NewDrawerScreen.MEMBER_LOAN_REPAYMENT:
                        NewDrawerScreen.showFragment(new Transaction_memberloan_repayment());
                        break;
                    case NewDrawerScreen.MINUTES_OF_MEETINGS:
                        NewDrawerScreen.showFragment(new MinutesOFMeeting());
                        break;
                    case NewDrawerScreen.ATTENDANCE:
                        NewDrawerScreen.showFragment(new Attendance());
                        break;
                    case NewDrawerScreen.AUDITING:
                        NewDrawerScreen.showFragment(new Meeting_audit_Fragment());
                        break;
                    case NewDrawerScreen.TRAINING:
                        NewDrawerScreen.showFragment(new Meetings_training());
                        break;

                    case NewDrawerScreen.CW:
                        NewDrawerScreen.showFragment(new CashWithdrawal());
                        break;

                }
            }
        });
        dialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();

            }
        });
        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    /*    title.setTextColor(Color.parseColor("#FFFFFF"));
        title.setBackgroundColor(Color.parseColor("#008000"));*/

        // Set the newly created TextView as a custom tile of DatePickerDialog
        //dpd.setCustomTitle(tv);

        // Or you can simply set a tile for DatePickerDialog
            /*
                setTitle(CharSequence title)
                    Set the title text for this dialog's window.
            */
        /*          datePickerDialog.show();*/
    }


    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
    }


    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Log.e("onDateSet() call", dayOfMonth + "-" + month + "-" + year);
        //String date = dayOfMonth + "-" + (month + 1) + "-" + year;
        try {

            Calendar c = Calendar.getInstance();
            c.set(year, month, dayOfMonth);
            String formattedDate = df.format(c.getTime());
            lastTransaDate = df.parse(formattedDate);
            MySharedPreference.writeString(getActivity(), MySharedPreference.LAST_TRANSACTION, lastTransaDate.getTime() + "");

            CashOfGroup cg = new CashOfGroup();
            cg.setCashAtBank(shgDto.getCashAtBank());
            cg.setCashInHand(shgDto.getCashInHand());
            cg.setLastTransactionDate(lastTransaDate.getTime() + "");
            SHGTable.updateSHGDetails(cg, shgDto.getShgId());
        } catch (Exception e) {
            e.printStackTrace();
        }

        switch (sItem) {
            case NewDrawerScreen.DEPOSIT:
                NewDrawerScreen.showFragment(new Deposit());
                break;
            case NewDrawerScreen.SAVINGS:
                NewDrawerScreen.showFragment(new Savings());
                break;
            case NewDrawerScreen.INCOME:
                NewDrawerScreen.showFragment(new Income());
                break;
            case NewDrawerScreen.EXPENCE:
                NewDrawerScreen.showFragment(new Expense());
                break;
            case NewDrawerScreen.BANK_TRANSACTION:
                NewDrawerScreen.showFragment(new BankTransaction());
                break;
            case NewDrawerScreen.LOAN_DISBURSEMENT:
                NewDrawerScreen.showFragment(new LoanDisbursement());
                break;
            case NewDrawerScreen.GROUP_LOAN_REPAYMENT:
                NewDrawerScreen.showFragment(new GroupLoanRepayment());
                break;
            case NewDrawerScreen.CREDIT_LINKAGE_INFO:
                break;
            case NewDrawerScreen.ANIMATOR_PROFILE:
                break;
            case NewDrawerScreen.MEMBER_LOAN_REPAYMENT:
                NewDrawerScreen.showFragment(new Transaction_memberloan_repayment());
                break;
            case NewDrawerScreen.MINUTES_OF_MEETINGS:
                NewDrawerScreen.showFragment(new MinutesOFMeeting());
                break;
            case NewDrawerScreen.ATTENDANCE:
                NewDrawerScreen.showFragment(new Attendance());
                break;
            case NewDrawerScreen.AUDITING:
                NewDrawerScreen.showFragment(new Meeting_audit_Fragment());
                break;
            case NewDrawerScreen.TRAINING:
                NewDrawerScreen.showFragment(new Meetings_training());
                break;

            case NewDrawerScreen.CW:
                NewDrawerScreen.showFragment(new CashWithdrawal());
                break;

            case NewDrawerScreen.FINANCIAL_VERIFICATION:
                NewDrawerScreen.showFragment(new FinancialConfirmation());
                break;

        }
    }
}
