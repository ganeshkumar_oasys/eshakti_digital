package com.oasys.eshakti.digitization.Dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class VTransaction implements Serializable {

    private VLoanTypes SubsidyReserverFund;

    private VLoanTypes PreviousYear;

    private VLoanTypes FixedAssets;

    private VLoanTypes Subsidy;


}
