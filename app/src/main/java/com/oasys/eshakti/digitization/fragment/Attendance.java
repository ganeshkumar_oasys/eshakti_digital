package com.oasys.eshakti.digitization.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.Dto.MemberList;
import com.oasys.eshakti.digitization.Dto.RequestDto.AttendanceRequestDto;
import com.oasys.eshakti.digitization.Dto.RequestDto.Members;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;
import com.oasys.eshakti.digitization.database.MemberTable;
import com.oasys.eshakti.digitization.database.SHGTable;
import com.tutorialsee.lib.TastyToast;

import com.yesteam.eshakti.view.activity.LoginActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Attendance extends Fragment implements View.OnClickListener, NewTaskListener {
    private View view;
    private ListOfShg shgDto;
    private TextView mGroupName;
    private TextView mCashinHand;
    private TextView mCashatBank;
    private Button mRaised_Submit_Button;
    private Button mEdit_RaisedButton, mOk_RaisedButton;
    Button mPerviousButton, mNextButton;
    private NetworkConnection networkConnection;
    public static String members;
    private List<MemberList> memList;
    private int mSize;
    private LinearLayout mLayout;
    public static CheckBox sCheckBox[];
    String memberName, memberid;
    Dialog confirmationDialog;
    private ArrayList<Members> arrmemid;
    AttendanceRequestDto attendanceRequestDto = new AttendanceRequestDto();
    ResponseDto attendanceresponse;
    public  static  String flag="0";
    private  TextView attendance_headertext,confirmationHeader;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        Attendance.flag = "0";
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        mSize = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, "")).size();
        memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        view = inflater.inflate(R.layout.fragment_attendance, container, false);
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        mGroupName = (TextView) view.findViewById(R.id.groupname);
        mGroupName.setText(shgDto.getName()+" / "+shgDto.getPresidentName());
        mGroupName.setTypeface(LoginActivity.sTypeface);

        mCashinHand = (TextView) view.findViewById(R.id.cashinHand);
        mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
        mCashinHand.setTypeface(LoginActivity.sTypeface);

        mCashatBank = (TextView) view.findViewById(R.id.cashatBank);
        mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
        mCashatBank.setTypeface(LoginActivity.sTypeface);


        attendance_headertext = (TextView) view.findViewById(R.id.attendance_headertext);
        attendance_headertext.setTypeface(LoginActivity.sTypeface);
        attendance_headertext.setText(AppStrings.Attendance);


        mRaised_Submit_Button = (Button) view.findViewById(R.id.attendance_Submitbutton);
        mRaised_Submit_Button.setText(AppStrings.submit);
        mRaised_Submit_Button.setTypeface(LoginActivity.sTypeface);
        mRaised_Submit_Button.setOnClickListener(this);

        mPerviousButton = (Button) view.findViewById(R.id.attendance_Previousbutton);
        //  mPerviousButton.setText(RegionalConversion.getRegionalConversion(com.yesteam.eshakti.appConstants.AppStrings.mPervious));
        //  mPerviousButton.setTypeface(LoginActivity.sTypeface);
        mPerviousButton.setOnClickListener(this);

        mNextButton = (Button) view.findViewById(R.id.attendance_Nextbutton);
        //mNextButton.setText(RegionalConversion.getRegionalConversion(com.yesteam.eshakti.appConstants.AppStrings.mNext));
        // mNextButton.setTypeface(LoginActivity.sTypeface);
        mNextButton.setOnClickListener(this);

        Bundle bundle = getArguments();
        if (bundle != null) {
            flag = bundle.getString("stepwise");
            Log.d("LOANID", flag);
        }

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        try {
            mLayout = (LinearLayout) view.findViewById(R.id.linearLayout);
            sCheckBox = new CheckBox[mSize];
            for (int i = 0; i < mSize; i++) {

                LinearLayout linearLayout = new LinearLayout(getActivity());
                linearLayout.setOrientation(LinearLayout.HORIZONTAL);
                sCheckBox[i] = new CheckBox(getActivity());
                sCheckBox[i].setChecked(true);

                sCheckBox[i].setBackgroundResource(R.drawable.btn_check_to_off_mtrl_013);

                linearLayout.addView(sCheckBox[i]);

                TextView memberName = new TextView(getActivity());
                memberName.setText(memList.get(i).getMemberName());
                memberName.setTypeface(LoginActivity.sTypeface);
                memberName.setPadding(20, 10, 0, 10);
                linearLayout.addView(memberName);

                mLayout.addView(linearLayout);
            }
        } catch (Exception e) {

        }


    }

    public void init() {


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.attendance_Submitbutton:
                try {
                    arrmemid = new ArrayList<>();
                    String Yes_No = "0";
                    memberName = "";
                    memberid = "";

                    for (int i = 0; i < mSize; i++) {

                        if (sCheckBox[i].isChecked()) {
                            Yes_No = "1";
                            memberName = memberName + memList.get(i).getMemberName() + "~";
                            memberid = memberid + memList.get(i).getMemberId() + "~";
                        } else {
                            Yes_No = "0";
                        }


                    }


                    String memberNameArr[] = memberName.split("~");
                    String memberidArr[] = memberid.split("~");

                    if (!memberName.equals("")) {

                        confirmationDialog = new Dialog(getActivity());

                        LayoutInflater inflater = getActivity().getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.dialog_confirmation, null);
                        dialogView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT));

                        TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
                        confirmationHeader.setText(AppStrings.confirmation);

                        TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

                        try {

                            CheckBox checkBox[] = new CheckBox[memberNameArr.length];

                            for (int j = 0; j < memberNameArr.length; j++) {
                                TableRow indv_memberNameRow = new TableRow(getActivity());

                                TableRow.LayoutParams contentParams = new TableRow.LayoutParams(
                                        TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);
                                contentParams.setMargins(10, 5, 10, 5);

                                checkBox[j] = new CheckBox(getActivity());
                                checkBox[j].setText(memberNameArr[j]);
                                checkBox[j].setChecked(true);
                                checkBox[j].setClickable(false);
                                checkBox[j].setTextColor(R.color.black);
                                indv_memberNameRow.addView(checkBox[j]);

                                Members memid = new Members();
                                memid.setMemberId(memberidArr[j]);
                                arrmemid.add(memid);

                                confirmationTable.addView(indv_memberNameRow,
                                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                            }

                        } catch (Exception e) {
                            // TODO: handle exception
                            System.out.println("checkbox layout error:" + e.toString());
                        }

                        confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
                        confirmationHeader.setTypeface(LoginActivity.sTypeface);

                        mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit_button);
                        mEdit_RaisedButton.setText(AppStrings.edit);
                        mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
                        mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
                        // 205,
                        // 0));
                        mEdit_RaisedButton.setOnClickListener(this);

                        mOk_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Ok_button);
                        mOk_RaisedButton.setText(AppStrings.yes);
                        mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
                        mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
                        mOk_RaisedButton.setOnClickListener(this);

                        confirmationDialog.getWindow()
                                .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        confirmationDialog.setCanceledOnTouchOutside(false);
                        confirmationDialog.setContentView(dialogView);
                        confirmationDialog.setCancelable(true);
                        confirmationDialog.show();

                        ViewGroup.MarginLayoutParams margin = (ViewGroup.MarginLayoutParams) dialogView.getLayoutParams();
                        margin.leftMargin = 10;
                        margin.rightMargin = 10;
                        margin.topMargin = 10;
                        margin.bottomMargin = 10;
                        margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);
                    } else {
                        TastyToast.makeText(getActivity(), AppStrings.MinutesAlert, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);
                        memberName = "";
                    }

                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }
                break;
            case R.id.fragment_Edit_button:

                memberName = "";
                mRaised_Submit_Button.setClickable(true);
                confirmationDialog.dismiss();
                break;
            case R.id.fragment_Ok_button:
                try {
                    String shgId = shgDto.getShgId();
                    Calendar calender = Calendar.getInstance();
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    String formattedDate = df.format(calender.getTime());
                    attendanceRequestDto.setShgId(shgId);
                    attendanceRequestDto.setTransactionDate(formattedDate);
                    attendanceRequestDto.setMembers(arrmemid);
                    String sreqString = new Gson().toJson(attendanceRequestDto);
                    if (networkConnection.isNetworkAvailable()) {
                        RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.ATTENDANCEPOST, sreqString, getActivity(), ServiceType.ATTENDANCEPOST);
                    }
                } catch (Exception e) {
                    Log.e("", e.toString());
                }
                break;
        }
    }

    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        switch (serviceType) {
            case ATTENDANCEPOST:
                attendanceresponse = new Gson().fromJson(result, ResponseDto.class);
                try {
                    Bundle bundle = getArguments();
                    flag = bundle.getString("stepwise");
                    Log.d("LOANID", flag);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                if(flag == "1")
                {
                    confirmationDialog.dismiss();
                    Savings savings = new Savings();
                    Bundle bundle =new Bundle();
                    bundle.putString("stepwise",flag);
                    savings.setArguments(bundle);
                    NewDrawerScreen.showFragment(savings);
                }
                else if (result != null) {
                    if (attendanceresponse.getStatusCode() == Utils.Success_Code) {
                        if (confirmationDialog.isShowing()) {
                            confirmationDialog.dismiss();
                        }
                        Utils.showToast(getActivity(), attendanceresponse.getMessage());
                        FragmentManager fm = getFragmentManager();
                        fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        MainFragment mainFragment = new MainFragment();
                        Bundle bundles = new Bundle();
                        bundles.putString("Meeting",MainFragment.Flag_Meeting);
                        mainFragment.setArguments(bundles);
                        NewDrawerScreen.showFragment(mainFragment);
//                        MemberDrawerScreen.showFragment(new MainFragment());
                    } else {

                        if (attendanceresponse.getStatusCode() == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                        }
                        confirmationDialog.dismiss();
                        Utils.showToast(getActivity(), attendanceresponse.getMessage());
                    }
                }
                break;
        }

    }
}
