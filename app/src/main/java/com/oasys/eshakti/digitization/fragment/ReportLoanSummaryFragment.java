package com.oasys.eshakti.digitization.fragment;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.digitization.Adapter.CustomExpandableLoantypeAdapter;
import com.oasys.eshakti.digitization.Dto.ExistingLoan;
import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;

import com.oasys.eshakti.digitization.OasysUtils.ExpandListItemClickListener;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.activity.LoginActivity;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;
import com.oasys.eshakti.digitization.database.SHGTable;
import com.oasys.eshakti.digitization.model.Loantype;
import com.oasys.eshakti.digitization.views.MyExpandableListview;
import com.oasys.eshakti.digitization.Service.NewTaskListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReportLoanSummaryFragment extends Fragment implements NewTaskListener, ExpandListItemClickListener {
    private View view;
    private MyExpandableListview expandableLayoutListView;
    private List<Loantype> listItems;
    private String shgId;
    private Loantype lt;
    ResponseDto responseDto;
    private ListOfShg shgDto;
    int listImage;
    private ArrayList<HashMap<String, String>> childList;
    private int lastExpandedPosition = -1;
    private CustomExpandableLoantypeAdapter mAdapter;
    private HashMap<String, String> temp;
    private String memberid;
    private TextView mGroupName;
    private TextView mCashinHand;
    private TextView mCashatBank;
    private TextView mHeader;
    private TextView Name;


    public ReportLoanSummaryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgId = shgDto.getShgId();


        //MySharedPreference.writeString(getContext(), MySharedPreference.SHG_ID, shgId);
        Log.d("GETLOANTYPE", "SHGID :" + shgId);
        view = inflater.inflate(R.layout.fragment_transaction_memberloan_repayment, container, false);

        //mListView = (MyExpandableListview) view.findViewById(R.id.listview_memberloanrepayment);

        Bundle bundle2 = getArguments();
        memberid = bundle2.getString("Memberid");
        Log.d("Mem3", memberid);

        getloantype();

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        expandableLayoutListView = (MyExpandableListview) view.findViewById(R.id.listview_memberloanrepayment);
        mGroupName = (TextView) view.findViewById(R.id.groupname);
        mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
        mGroupName.setTypeface(LoginActivity.sTypeface);

        mCashinHand = (TextView) view.findViewById(R.id.cashinhand);
        mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
        mCashinHand.setTypeface(LoginActivity.sTypeface);

        mCashatBank = (TextView) view.findViewById(R.id.cashatbank);
        mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
        mCashatBank.setTypeface(LoginActivity.sTypeface);

        Name = (TextView) view.findViewById(R.id.Name);
        Name.setText(MySharedPreference.readString(getActivity(), MySharedPreference.MEM_NAME_SUMMARY, ""));
    //    Name.setTypeface(LoginActivity.sTypeface);


        mHeader = (TextView) view.findViewById(R.id.fragmentHeader);
        mHeader.setText(AppStrings.loanSummary);
        mHeader.setTypeface(LoginActivity.sTypeface);

        listItems = new ArrayList<Loantype>();
        listImage = R.drawable.ic_navigate_next_white_24dp;

        expandableLayoutListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                if (lastExpandedPosition != -1 && groupPosition != lastExpandedPosition) {
                    expandableLayoutListView.collapseGroup(lastExpandedPosition);
                }
                lastExpandedPosition = groupPosition;

            }
        });
    }

    public void getloantype() {
        try {
            if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {
                String url = Constants.BASE_URL + Constants.GETGROUPLOANTYPES + shgDto.getShgId();
                RestClient.getRestClient(ReportLoanSummaryFragment.this).callWebServiceForGetMethod(url, getActivity(), ServiceType.GETLOANTYPES);
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        try {

            switch (serviceType) {
                case GETLOANTYPES:
                    if (result != null && result.length() > 0) {
                        responseDto = new Gson().fromJson(result, ResponseDto.class);
                        Log.d("GETLOANTYPE", responseDto.toString());
                        if (responseDto.getStatusCode() == Utils.Success_Code) {
                            ArrayList<ExistingLoan> loanTypes = responseDto.getResponseContent().getLoansList();

                            for (int i = 0; i < loanTypes.size(); i++) {
                                lt = new Loantype(loanTypes.get(i).getLoanTypeName() + "", listImage);
                                listItems.add(lt);
                            }

                            lt = new Loantype("Internal loan", listImage);
                            Log.d("Loantype", lt.toString());
                            listItems.add(lt);
                            setCustomAdapter(loanTypes);
                        }else {
                            if (responseDto.getStatusCode() == 401) {

                                Log.e("Group Logout", "Logout Sucessfully");
                                AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                            }
                        }
                    }
                    break;
            }
        } catch (Exception e) {

        }
    }

    public void setCustomAdapter(ArrayList<ExistingLoan> lonetypedto) {
        childList = new ArrayList<HashMap<String, String>>();
        for (int i = 0; i < lonetypedto.size(); i++) {
            temp = new HashMap<String, String>();
            temp.put(AppStrings.loanType, (lonetypedto.get(i).getLoanTypeName() != null && lonetypedto.get(i).getLoanTypeName().length() > 0) ? lonetypedto.get(i).getLoanTypeName().toUpperCase() : "NA");
            temp.put(AppStrings.bankName, (lonetypedto.get(i).getBankName() != null && lonetypedto.get(i).getBankName().length() > 0) ? lonetypedto.get(i).getBankName().toUpperCase() : "NA");
            temp.put(AppStrings.mAccountNumber, (lonetypedto.get(i).getAccountNumber() != null && lonetypedto.get(i).getAccountNumber().length() > 0) ? lonetypedto.get(i).getAccountNumber().toUpperCase() : "NA");
            temp.put("loanId", (lonetypedto.get(i).getLoanId() != null && lonetypedto.get(i).getLoanId().length() > 0) ? lonetypedto.get(i).getLoanId().toUpperCase() : "NA");
            temp.put(AppStrings.Loan_Disbursement_Date, (lonetypedto.get(i).getDisbursmentDate() != null && lonetypedto.get(i).getDisbursmentDate().length() > 0) ? lonetypedto.get(i).getDisbursmentDate().toUpperCase() : "NA");

            temp.put("loanTypeName_Label", AppStrings.loanType);
            temp.put("bankName_Label", AppStrings.bankName);
            temp.put("accountNumber_Label", AppStrings.mAccountNumber);
            temp.put("loanId_Label", "loanId");
            temp.put("disbursement_label", AppStrings.Loan_Disbursement_Date);
            childList.add(temp);
        }
        temp = new HashMap<String, String>();
        temp.put("loanTypeName_Label", AppStrings.InternalLoan);
        childList.add(temp);
        mAdapter = new CustomExpandableLoantypeAdapter(getContext(), lonetypedto, listItems, childList, this);
        expandableLayoutListView.setAdapter(mAdapter);

    }

    @Override
    public void onItemClick(ViewGroup parent, View view, int position) {
        try {

            String title = listItems.get(position).getTitle().toString();

            if (title != "Internal loan") {
                Report_Member_LoanSummary_Otherloan fragment = new Report_Member_LoanSummary_Otherloan();
                Bundle bundle = new Bundle();
                bundle.putString("loan_id", (responseDto.getResponseContent().getLoansList().get(position).getLoanId().toString()));
                bundle.putString("account_number", (responseDto.getResponseContent().getLoansList().get(position).getAccountNumber().toString()));
                bundle.putString("loan_type", (responseDto.getResponseContent().getLoansList().get(position).getLoanTypeName().toString()));
                bundle.putString("bank_name", (responseDto.getResponseContent().getLoansList().get(position).getBankName().toString()));
                bundle.putString("memid", memberid);
                fragment.setArguments(bundle);
                NewDrawerScreen.showFragment(fragment);
            } else {
                GroupReportInternalLoan fragment = new GroupReportInternalLoan();
                Bundle bundle = new Bundle();
                bundle.putString("memid", memberid);
                fragment.setArguments(bundle);
                NewDrawerScreen.showFragment(fragment);
            }
        } catch (Exception e) {
        }
    }

    @Override
    public void onItemClickVerification(ViewGroup parent, View view, int position) {

    }

}
