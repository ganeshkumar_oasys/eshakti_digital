package com.oasys.eshakti.digitization.fragment;


import android.app.Dialog;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.digitization.Dto.RequestDto.Wallet_cash;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;
import com.tutorialsee.lib.TastyToast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class WalletCash_in_Hand extends Fragment implements View.OnClickListener, NewTaskListener {

    private View view;
    private TextView mWalletcash_amount;
    private EditText mWalletCash_in;
    private EditText mWalletCash_out, mWalletCash_remarks;
    private TextView mWallet_submit;
    private TextView mWallet_cancel;
    ResponseDto animatorProfileDto;
    private String userId;
    private Dialog dialog;
    private Wallet_cash wallet_cash_Dto;
    private String str_mWalletcash_amount, str_cash_in, str_cash_out, str_remarks;
    private TextView daily_date_time;
    private String currentDateStr;
    DateFormat df = new SimpleDateFormat("dd/MM/yyyy");


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_wallet_cash_in__hand, container, false);
        init(view);
        userId = MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, "");
        dialog = new Dialog(getActivity());


        String url = Constants.BASE_URL + Constants.WALLETCASH_INHAND + userId;

        if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {
            RestClient.getRestClient(WalletCash_in_Hand.this).callWebServiceForGetMethod(url, getActivity(), ServiceType.WALLET_CASHIN_HAND);
        } else {
            Utils.showToast(getActivity(), "Network Not Available");
        }

        return view;
    }

    private void init(View view) {

        try {
            Calendar calender = Calendar.getInstance();
            currentDateStr = df.format(calender.getTime());

            daily_date_time = (TextView) view.findViewById(R.id.daily_date_time);
            daily_date_time.setText(currentDateStr);

            mWalletcash_amount = (TextView) view.findViewById(R.id.mWalletcash_amount);

            mWalletCash_in = (EditText) view.findViewById(R.id.mWalletCash_in);
            mWalletCash_out = (EditText) view.findViewById(R.id.mWalletCash_out);
            mWalletCash_remarks = (EditText) view.findViewById(R.id.mWalletCash_remarks);

            mWallet_submit = (TextView) view.findViewById(R.id.mWallet_submit);
            mWallet_submit.setOnClickListener(this);

            mWallet_cancel = (TextView) view.findViewById(R.id.mWallet_cancel);
            mWallet_cancel.setOnClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mWallet_submit:
                buttonValues();

                break;

            case R.id.mWallet_cancel:
                FragmentManager fm = getFragmentManager();
                fm.popBackStackImmediate();
                break;

        }
    }


    public void buttonValues() {


        displayDialogWindow();

    }

    private void displayDialogWindow() {

        dialog.setContentView(R.layout.confirmation_dialog);


        str_mWalletcash_amount = mWalletcash_amount.getText().toString();
        if (str_mWalletcash_amount.isEmpty()) {
            str_mWalletcash_amount = "0";
        }

        str_cash_in = mWalletCash_in.getText().toString();
        if (str_cash_in.isEmpty()) {
            str_cash_in = "0";
        }

        str_cash_out = mWalletCash_out.getText().toString();

        if (str_cash_out.isEmpty()) {
            str_cash_out = "0";
        }

        str_remarks = mWalletCash_remarks.getText().toString();
        if (str_remarks.isEmpty()) {
            str_remarks = "0";
        }


        TextView type = (TextView) dialog.findViewById(R.id.transaction_loantypevalue);
        type.setText(str_mWalletcash_amount);

        TextView interest = (TextView) dialog.findViewById(R.id.transaction_loanbanknamevalue);
        interest.setText(str_cash_in);

        TextView charges = (TextView) dialog.findViewById(R.id.transaction_loan_interestvalue);
        charges.setText(str_cash_out);

        TextView repayment = (TextView) dialog.findViewById(R.id.transaction_loanchargesvalue);
        repayment.setText(str_remarks);

        TextView current_cash = (TextView) dialog.findViewById(R.id.currentcash);
        current_cash.setText("Current Cash in Hand");
        TextView cash_in = (TextView) dialog.findViewById(R.id.cash_in);
        cash_in.setText("Cashin");
        TextView cash_out = (TextView) dialog.findViewById(R.id.cash_out);
        cash_out.setText("Cashout");
        TextView remarks = (TextView) dialog.findViewById(R.id.remarks);
        remarks.setText("Remarks");

        Button edit = (Button) dialog.findViewById(R.id.edit);
        Button ok = (Button) dialog.findViewById(R.id.ok);
        LinearLayout dialog_bank_charge_layout = (LinearLayout) dialog.findViewById(R.id.hide1);
        dialog_bank_charge_layout.setVisibility(View.GONE);
        LinearLayout dialog_bankname_layout = (LinearLayout) dialog.findViewById(R.id.hide2);
        dialog_bankname_layout.setVisibility(View.GONE);
        LinearLayout dialog_charges_layout = (LinearLayout) dialog.findViewById(R.id.hide3);
        dialog_bankname_layout.setVisibility(View.GONE);

        dialog.show();

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                wallet_cash_Dto = new Wallet_cash();
                wallet_cash_Dto.setCurrentCashInHand(str_mWalletcash_amount);
                wallet_cash_Dto.setCashInHand(str_cash_in);
                wallet_cash_Dto.setCashOutHand(str_cash_out);
                wallet_cash_Dto.setRemarks(str_remarks);
                wallet_cash_Dto.setUserId(userId);


                String transaction_walletcash_end = new Gson().toJson(wallet_cash_Dto);
                Log.d("trans", transaction_walletcash_end);

                if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {

                    RestClient.getRestClient(WalletCash_in_Hand.this).callRestWebService(Constants.BASE_URL + Constants.WALLETCASH_END, transaction_walletcash_end, getActivity(), ServiceType.WALLET_CASH);
                } else {
                    Utils.showToast(getActivity(), "Network Not Available");
                }

            }
        });
    }


    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {

        switch (serviceType) {
            case WALLET_CASHIN_HAND:
                Log.d("getDetails", " " + result.toString());
                animatorProfileDto = new Gson().fromJson(result.toString(), ResponseDto.class);
                if (animatorProfileDto.getStatusCode() == Utils.Success_Code) {
                    Utils.showToast(getActivity(), animatorProfileDto.getMessage());

                    if ((animatorProfileDto.getResponseContent() != null)) {

                        mWalletcash_amount.setText(animatorProfileDto.getResponseContent().getCurrentCashInHand());

                    }

                } else {
                    Utils.showToast(getActivity(), animatorProfileDto.getMessage());
                    if (animatorProfileDto.getStatusCode() == 401) {

                        Log.e("Group Logout", "Logout Sucessfully");
                        AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                           /* if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }*/
                    }
                }
                break;

            case WALLET_CASH:
                try {
                    Log.d("getDetails", " " + result);
                    animatorProfileDto = new Gson().fromJson(result, ResponseDto.class);

                    if (animatorProfileDto.getStatusCode() == Utils.Success_Code) {
                        Transaction_completed transaction_completed = new Transaction_completed();
                        Bundle bundles = new Bundle();
                        bundles.putString("request_id", animatorProfileDto.getResponseContent().getUpdateCashInHand());
                        bundles.putString("ref_no", animatorProfileDto.getResponseContent().getRefNo());
                        transaction_completed.setArguments(bundles);
                        FragmentManager fm = getFragmentManager();
                        fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        NewDrawerScreen.showFragment(transaction_completed);
                        dialog.dismiss();
                        TastyToast.makeText(getActivity(), "Transaction Completed",
                                TastyToast.LENGTH_SHORT, TastyToast.SUCCESS);
                    } else {
                        Utils.showToast(getActivity(), animatorProfileDto.getMessage());
                        if (animatorProfileDto.getStatusCode() == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                           /* if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }*/
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

        }

    }
}
