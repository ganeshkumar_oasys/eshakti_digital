package com.oasys.eshakti.digitization.database;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.oasys.eshakti.digitization.Dto.ExistingLoan;
import com.oasys.eshakti.digitization.EShaktiApplication;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell on 16 Dec, 2018.
 */

public class LoanTable {
    private static SQLiteDatabase database;
    private static DbHelper dataHelper;
    static Cursor cursor;

    public LoanTable(Activity activity) {
        dataHelper = new DbHelper(activity);
    }

    public static void openDatabase() {
        dataHelper = new DbHelper(EShaktiApplication.getInstance());
        dataHelper.onOpen(database);
        database = dataHelper.getWritableDatabase();
    }

    public static void closeDatabase() {
        if (database != null && database.isOpen()) {
            database.close();
        }
    }

    public static void insertLoanDetails(ExistingLoan memDto) {

        if (memDto != null) {
            try {
                openDatabase();
                ContentValues values = new ContentValues();
                if ((memDto.getLoanId() != null && memDto.getLoanId().length() > 0)) {
                    values.put(TableConstants.SHG_ID, (memDto.getShgId() != null && memDto.getShgId().length() > 0) ? memDto.getShgId() : "");
                    values.put(TableConstants.LOAN_ID, (memDto.getLoanId() != null && memDto.getLoanId().length() > 0) ? memDto.getLoanId() : "");
                    values.put(TableConstants.LOAN_TYPE_ID, (memDto.getLoanId() != null && memDto.getLoanId().length() > 0) ? memDto.getLoanId() : "");
                    values.put(TableConstants.LOAN_NAME, (memDto.getLoanTypeName() != null && memDto.getLoanTypeName().length() > 0) ? memDto.getLoanTypeName() : "");
                    values.put(TableConstants.LOAN_AMOUNT, (memDto.getLoanOutstanding() != null && memDto.getLoanOutstanding().length() > 0) ? memDto.getLoanOutstanding() : "");
                    values.put(TableConstants.LOAN_ACC_NO, (memDto.getAccountNumber() != null && memDto.getAccountNumber().length() > 0) ? memDto.getAccountNumber() : "");
                    values.put(TableConstants.LOAN_ACC_ID, (memDto.getLoanAccountId() != null && memDto.getLoanAccountId().length() > 0) ? memDto.getLoanAccountId() : "");
                    values.put(TableConstants.LOAN_AMOUNT, (memDto.getLoanOutstanding() != null && memDto.getLoanOutstanding().length() > 0) ? memDto.getLoanOutstanding() : "");
                    values.put(TableConstants.BANKNAME, (memDto.getBankName() != null && memDto.getBankName().length() > 0) ? memDto.getBankName() : "");
                    database.insertWithOnConflict(TableName.TABLE_LOAN_DETAILS, TableConstants.SHG_ID, values, SQLiteDatabase.CONFLICT_REPLACE);
                }
            } catch (Exception e) {
                Log.e("insertEnergyException", e.toString());
            } finally {
                closeDatabase();
            }
        }

    }

    public static List<ExistingLoan> getLoanDetails(String shg_id) {
        List<ExistingLoan> memList = new ArrayList<>();
        if (shg_id.length() > 0) {
            try {
                openDatabase();

                String selectQuery = "SELECT * FROM " + TableName.TABLE_LOAN_DETAILS + " WHERE " + TableConstants.SHG_ID + " LIKE '" + shg_id + "'";
                Log.e("TABLE_SHG QUERY:", selectQuery);
                Cursor cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    do {
                        ExistingLoan loanDto = new ExistingLoan();
                        loanDto.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                        loanDto.setLoanId(cursor.getString(cursor.getColumnIndex(TableConstants.LOAN_ID)));
                        loanDto.setLoanTypeName(cursor.getString(cursor.getColumnIndex(TableConstants.LOAN_NAME)));
                        loanDto.setLoanAccountId(cursor.getString(cursor.getColumnIndex(TableConstants.LOAN_ACC_ID)));
                        loanDto.setLoanOutstanding(cursor.getString(cursor.getColumnIndex(TableConstants.LOAN_AMOUNT)));
                        loanDto.setAccountNumber(cursor.getString(cursor.getColumnIndex(TableConstants.LOAN_ACC_NO)));
                        loanDto.setBankName(cursor.getString(cursor.getColumnIndex(TableConstants.BANKNAME)));

                        memList.add(loanDto);
                    } while (cursor.moveToNext());
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                closeDatabase();
            }
        }
        return memList;
    }


}
