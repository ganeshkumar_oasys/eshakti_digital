package com.oasys.eshakti.digitization.fragment;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.oasys.eshakti.digitization.Adapter.Digitization_saving_Member_Details_Adapter;
import com.oasys.eshakti.digitization.Dto.ResponseContents;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.Service.RestClient;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class Digitization_Member_Saving_Details extends Fragment implements NewTaskListener {

    private RecyclerView recyclerview_digitization_memberdetails;
    private View view;
    private LinearLayoutManager linearLayoutManager;
    private Digitization_saving_Member_Details_Adapter digitization_saving_member_details_adapter;
    public ArrayList<ResponseContents> responseContentsList;
    private  String mTrans_type;
    private  String mTrans_date;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_digitization__member__saving__details, container, false);
        responseContentsList = new ArrayList<>();
        init();
        return view;
    }

    private void init() {

        recyclerview_digitization_memberdetails = (RecyclerView) view.findViewById(R.id.recyclerView_memberSaving_details);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerview_digitization_memberdetails.setLayoutManager(linearLayoutManager);
        recyclerview_digitization_memberdetails.setHasFixedSize(true);
        recyclerview_digitization_memberdetails.addItemDecoration(new DividerItemDecoration(getActivity(), 0));
        Bundle bundle = getArguments();
        String mBatch_id = bundle.getString("batchId");
         mTrans_type = bundle.getString("transactiontype");
         mTrans_date = bundle.getString("transactiondate");

        if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {

            RestClient.getRestClient(Digitization_Member_Saving_Details.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.DIGITIZATION_MEMBER_REPORTS + mBatch_id, getActivity(), ServiceType.DIGITIZATION_MEMBER_REPORTS_SERVICE);
        } else {
            Utils.showToast(getActivity(), "Network Not Available");
        }


    }

    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        switch (serviceType) {
            case DIGITIZATION_MEMBER_REPORTS_SERVICE:
                try {

                    ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    String message = cdto.getMessage();
                    int statusCode = cdto.getStatusCode();
                    if (statusCode == Utils.Success_Code) {
                        Utils.showToast(getActivity(), message);
                        for (int i = 0; i < cdto.getResponseContents().size(); i++) {

                            ResponseContents contents = new ResponseContents();
                            contents.setMember_name(cdto.getResponseContents().get(i).getMember_name());
                            contents.setAmount(cdto.getResponseContents().get(i).getAmount());
                            contents.setTrans_ref(cdto.getResponseContents().get(i).getTrans_ref());
                            contents.setError_name(cdto.getResponseContents().get(i).getError_name());
                            contents.setTransation(mTrans_type);
                            contents.setTransaction_request_date(mTrans_date);
                            responseContentsList.add(contents);

                            digitization_saving_member_details_adapter = new Digitization_saving_Member_Details_Adapter(getActivity(), responseContentsList);
                            recyclerview_digitization_memberdetails.setAdapter(digitization_saving_member_details_adapter);
                        }

                    } else {
                        if (statusCode == 503) {

                            Utils.showToast(getActivity(), AppStrings.service_unavailable);

                        }


                        Utils.showToast(getActivity(), message);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
    }
}