package com.oasys.eshakti.digitization.Dto;


import java.io.Serializable;

import lombok.Data;

@Data
public class AssetsAndLiabilitiesDTO  implements Serializable {

    private VTransaction transaction;
}
