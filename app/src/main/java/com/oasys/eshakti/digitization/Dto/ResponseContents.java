package com.oasys.eshakti.digitization.Dto;

import lombok.Data;

@Data
public class ResponseContents {

    String createdDate;
    String createdBy;
    String modifiedDate;
    String modifiedBy;
    String id;
    String name;
    String designation;
    String code;
    String shgId,userId;
    String bankStatus,status;
    String bankTypeId,bankIID;
    String orderNo;
    Transactiontype transactionType;

    Transactiontype transactionStatusId;


    private String batchId;

    private String TxType;

    private String message;

    private String transactionId,merchantId,merchantTransactionId;

    private String referenceId;

    private String memberBatchId;

    private String statusCode;
    private String memberName;

    private String error_name;

    private String amount;

    private String trans_ref;

    private String member_name;

    private String transation;
    private String transaction_request_date;



}
