package com.oasys.eshakti.digitization.Dto;


import lombok.Data;

@Data
class SavingAccountId {

    String id;
}
