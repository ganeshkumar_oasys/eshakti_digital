package com.oasys.eshakti.digitization.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.Dto.MemberList;
import com.oasys.eshakti.digitization.Dto.ResponseContents;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.Dto.SavingRequest;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.GetSpanText;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.activity.LoginActivity;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;
import com.oasys.eshakti.digitization.database.MemberTable;
import com.oasys.eshakti.digitization.database.SHGTable;
import com.oasys.eshakti.digitization.views.Get_EdiText_Filter;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Donation_Income extends Fragment implements View.OnClickListener, NewTaskListener {
    private View rootView;
    private TextView mGroupName, mCashinHand, mCashatBank, mHeader;
    private Button mSubmit_Raised_Button, mEdit_RaisedButton, mOk_RaisedButton;
    EditText amountEdit;
    public static String sSend_To_Server_Donation_FIncome;
    private Dialog mProgressDilaog;

    Dialog confirmationDialog;
    public ArrayList<ResponseContents> responseContentsList;

    String mLastTrDate = null, mLastTr_ID = null;
    private int mSize;
    private List<MemberList> memList;
    private ListOfShg shgDto;
    private NetworkConnection networkConnection;
    private ArrayList<MemberList> arrMem;
    private String transaction;
    AlertDialog alertDialog;
    private TextView counterTextView;
    private TextView counterTimerStatusTxt;
    private CountDownTimer cndr;
    private static final String FORMAT_TIMER = "%02d:%02d";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_new_doantion_fincome, container, false);
        return rootView;

    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mSize = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, "")).size();
        memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        arrMem = new ArrayList<>();
        Bundle bundle = getArguments();
        transaction = bundle.getString("trnsactiontype");
        init();
    }


    private void init() {
        try {
            responseContentsList = new ArrayList<>();
            mGroupName = (TextView) rootView.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);
            mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
            mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashinHand.setTypeface(LoginActivity.sTypeface);
            mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
            mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashatBank.setTypeface(LoginActivity.sTypeface);
            mHeader = (TextView) rootView.findViewById(R.id.fragment_header);
            mHeader.setText(Income.sSelectedIncomeMenu.getIncomeType());
            //   mHeader.setTypeface(LoginActivity.sTypeface);
            TextView labelText = (TextView) rootView.findViewById(R.id.fragment_label_text);
            labelText.setText(Income.sSelectedIncomeMenu.getIncomeType());
            labelText.setTextColor(R.color.black);

            amountEdit = (EditText) rootView.findViewById(R.id.fragment_amount_editText);
            amountEdit.setInputType(InputType.TYPE_CLASS_NUMBER);
            amountEdit.setPadding(5, 5, 5, 5);
            amountEdit.setWidth(150);
            amountEdit.setHeight(60);
            amountEdit.setFilters(Get_EdiText_Filter.editText_filter());
            amountEdit.setGravity(Gravity.RIGHT);

            mSubmit_Raised_Button = (Button) rootView.findViewById(R.id.fragment_Submit);
            mSubmit_Raised_Button.setText(AppStrings.submit);
            mSubmit_Raised_Button.setTypeface(LoginActivity.sTypeface);
            mSubmit_Raised_Button.setOnClickListener(this);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fragment_Submit:
                sSend_To_Server_Donation_FIncome = String.valueOf(amountEdit.getText());
                if ((sSend_To_Server_Donation_FIncome.equals("")) || (sSend_To_Server_Donation_FIncome == null)) {
                    sSend_To_Server_Donation_FIncome = "0";
                }
                if ((!sSend_To_Server_Donation_FIncome.equals("0"))) {

                    confirmationDialog = new Dialog(getActivity());

                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.dialog_new_confirmation, null);
                    dialogView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT));

                    TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
                    confirmationHeader.setText(AppStrings.confirmation);
                    confirmationHeader.setTypeface(LoginActivity.sTypeface);

                    TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

                    TableRow indv_SavingsRow = new TableRow(getActivity());

                    TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                    contentParams.setMargins(10, 5, 10, 5);

                    TextView lableItem = new TextView(getActivity());
                    lableItem.setText(GetSpanText.getSpanString(getActivity(), Income.sSelectedIncomeMenu.getIncomeType()));
                    lableItem.setTextColor(R.color.black);
                    lableItem.setPadding(5, 5, 5, 5);
                    lableItem.setLayoutParams(contentParams);
                    indv_SavingsRow.addView(lableItem);

                    TextView confirm_values = new TextView(getActivity());
                    confirm_values.setText(
                            GetSpanText.getSpanString(getActivity(), String.valueOf(sSend_To_Server_Donation_FIncome)));
                    confirm_values.setTextColor(R.color.black);
                    confirm_values.setPadding(5, 5, 15, 5);
                    confirm_values.setGravity(Gravity.RIGHT);
                    confirm_values.setLayoutParams(contentParams);
                    indv_SavingsRow.addView(confirm_values);

                    confirmationTable.addView(indv_SavingsRow,
                            new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                    mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit);
                    mEdit_RaisedButton.setText(AppStrings.edit);
                    mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
                    mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
                    // 205,
                    // 0));
                    mEdit_RaisedButton.setOnClickListener(this);

                    mOk_RaisedButton = (Button) dialogView.findViewById(R.id.frag_Ok);
                    mOk_RaisedButton.setText(AppStrings.yes);
                    mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
                    mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
                    mOk_RaisedButton.setOnClickListener(this);

                    confirmationDialog.getWindow()
                            .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    confirmationDialog.setCanceledOnTouchOutside(false);
                    confirmationDialog.setContentView(dialogView);
                    confirmationDialog.setCancelable(true);
                    confirmationDialog.show();

                    ViewGroup.MarginLayoutParams margin = (ViewGroup.MarginLayoutParams) dialogView.getLayoutParams();
                    margin.leftMargin = 10;
                    margin.rightMargin = 10;
                    margin.topMargin = 10;
                    margin.bottomMargin = 10;
                    margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);

                } else {
                    sSend_To_Server_Donation_FIncome = "0";
                    TastyToast.makeText(getActivity(), AppStrings.nullAlert, TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                }
                break;

            case R.id.frag_Ok:
                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();
                SavingRequest sr = new SavingRequest();
//                sr.setIncomeTypeId(Income.sSelectedIncomeMenu.getId());
//                sr.setModeOfCash("2");
//                sr.setShgId(shgDto.getShgId());
//                sr.setTransactionDate(shgDto.getLastTransactionDate());
//                sr.setMobileDate(System.currentTimeMillis() + "");
//                sr.setAmount(sSend_To_Server_Donation_FIncome);
                sr.setTransactionType(transaction);
                sr.setDigital(true);
                sr.setMobileDate(System.currentTimeMillis() + "");
                sr.setModeOfCash("2");
                sr.setUserId(MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, ""));
                sr.setAgentId(MySharedPreference.readString(getActivity(), MySharedPreference.AGENT_ID, ""));
                sr.setShgId(shgDto.getShgId());
                sr.setTransactionDate(shgDto.getLastTransactionDate());
//                sr.setMemberSaving(arrMem);
                sr.setAmount(sSend_To_Server_Donation_FIncome);

                String sreqString = new Gson().toJson(sr);
                if (networkConnection.isNetworkAvailable()) {
                    onTaskStarted();

                    ViewGroup viewGroup = rootView.findViewById(android.R.id.content);
                    View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.online_transaction_timer, viewGroup, false);
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setView(dialogView);
                    alertDialog = builder.create();
                    alertDialog.show();
                    alertDialog.setCancelable(false);
                    counterTextView = dialogView.findViewById(R.id.counterTimerTxt);
                    counterTimerStatusTxt = dialogView.findViewById(R.id.counterTimerStatusTxt);

                    RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.DONATION, sreqString, getActivity(), ServiceType.DONATION_INCOME);
                    cndr = new CountDownTimer(MySharedPreference.readLong(getActivity(), MySharedPreference.fttimeout, 180000), 1000) { // adjust the milli seconds here
                        public void onTick(long millisUntilFinished) {
                            counterTextView.setText("" + String.format(FORMAT_TIMER,
                                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(
                                            TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                                            TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))) + " minutes");
                        }

                        @Override
                        public void onFinish() {
//                            MemberDrawerScreen.showFragment(new Digitization_Deposit_Details());
                            cndr.cancel();
                            alertDialog.dismiss();
                            TastyToast.makeText(getActivity(), AppStrings.counterDismiss, TastyToast.LENGTH_SHORT,
                                    TastyToast.WARNING);

//                            timer.cancel();
                        }


                    }.start();
                }


                break;
            case R.id.fragment_Edit:
                sSend_To_Server_Donation_FIncome = "" + 0;
                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();
                break;
        }
    }

    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        if (mProgressDilaog != null) {
            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                mProgressDilaog.dismiss();
                mProgressDilaog = null;
                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();
            }

            switch (serviceType) {
                case DONATION_INCOME:
                    try {
                        if (result != null) {
                            ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                            String message = cdto.getMessage();
                            int statusCode = cdto.getStatusCode();
                            if (statusCode == Utils.Success_Code) {
                                Utils.showToast(getActivity(), message);



                                if (cdto.getResponseContents() != null) {
                                    for (int i = 0; i < cdto.getResponseContents().size(); i++) {
                                        ResponseContents contents = new ResponseContents();
                                        contents.setMemberName(cdto.getResponseContents().get(i).getMemberName());
                                        contents.setTransactionId(cdto.getResponseContents().get(i).getTransactionId());
                                        contents.setMessage(cdto.getResponseContents().get(i).getMessage());
                                        contents.setTxType(AppStrings.income);
                                        responseContentsList.add(contents);
                                    }

                                    ViewSavingTransactionDetails viewSavingTransactionDetails = new ViewSavingTransactionDetails();
                                    Bundle bundle = new Bundle();
                                    bundle.putSerializable("transactiondetails", responseContentsList);
                                    viewSavingTransactionDetails.setArguments(bundle);
                                    NewDrawerScreen.showFragment(viewSavingTransactionDetails);
                                    TastyToast.makeText(getActivity(), "Transaction Completed",
                                            TastyToast.LENGTH_SHORT, TastyToast.SUCCESS);
                                    alertDialog.dismiss();
                                    cndr.cancel();
                                }




                               /* if (shgDto.getFFlag() == null || !shgDto.getFFlag().equals("1")) {
                                    SHGTable.updateTransactionSHGDetails(shgDto.getShgId());
                                }
                                FragmentManager fm = getFragmentManager();
                                fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                MainFragment mainFragment = new MainFragment();
                                Bundle bundles = new Bundle();
                                bundles.putString("Transaction", MainFragment.Flag_Transaction);
                                mainFragment.setArguments(bundles);
                                MemberDrawerScreen.showFragment(mainFragment);
//                            MemberDrawerScreen.showFragment(new MainFragment());
                                TastyToast.makeText(getActivity(), "Transaction Completed",
                                        TastyToast.LENGTH_SHORT, TastyToast.SUCCESS);
                                alertDialog.dismiss();
                                cndr.cancel();*/
                            } else {

                                if (statusCode == 401) {

                                    Log.e("Group Logout", "Logout Sucessfully");
                                    AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                                    if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                        mProgressDilaog.dismiss();
                                        mProgressDilaog = null;
                                    }
                                }
                                Utils.showToast(getActivity(), message);
                                TastyToast.makeText(getActivity(), "Transaction Timeout",
                                        TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                                alertDialog.dismiss();
                                cndr.cancel();
                            }
                        } else {
                            cndr.cancel();
                            alertDialog.dismiss();
                            TastyToast.makeText(getActivity(), AppStrings.failedDigiResponse, TastyToast.LENGTH_SHORT,
                                    TastyToast.WARNING);
                        }
                    } catch (Exception e) {

                    }
                    break;
            }


        }
    }
}
