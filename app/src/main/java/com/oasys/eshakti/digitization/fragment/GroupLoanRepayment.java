package com.oasys.eshakti.digitization.fragment;


import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.digitization.Adapter.Transaction_GroupLoanRepayment_Adapter;
import com.oasys.eshakti.digitization.Dto.ExistingLoan;
import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.activity.LoginActivity;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;
import com.oasys.eshakti.digitization.database.SHGTable;
import com.yesteam.eshakti.interfaces.ExpandListItemClickListener;
import com.oasys.eshakti.digitization.Service.NewTaskListener;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class GroupLoanRepayment extends Fragment implements NewTaskListener, ExpandListItemClickListener {
    private View view;
    private ExpandableListView mTrasnsactionloan_repayment;
    public static Transaction_GroupLoanRepayment_Adapter transaction_groupLoanRepayment_adapter;
    private ArrayList<ExistingLoan> listDataHeader;
    private HashMap<String, ArrayList<ExistingLoan>> listDataChild;
    ResponseDto responseDto;
    private ListOfShg shgDto;
    private String shgId;
    private TextView cashinhandgroupreport, grpname,cashatbankgroupreport;
    private TextView grouploan_groupname;


    public GroupLoanRepayment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_group_loan_repayment, container, false);

        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgId = shgDto.getShgId();
        listDataChild = new HashMap<String, ArrayList<ExistingLoan>>();
        listDataHeader = new ArrayList<ExistingLoan>();
        initView();
        return view;
    }

    public void initView() {
        mTrasnsactionloan_repayment = (ExpandableListView) view.findViewById(R.id.GrouploanRepayment_ExpandableListView);
        cashinhandgroupreport = (TextView) view.findViewById(R.id.cashinhandgroupreport);
        cashinhandgroupreport.setTypeface(LoginActivity.sTypeface);
        cashinhandgroupreport.setText(shgDto.getCashInHand());
        cashatbankgroupreport = (TextView) view.findViewById(R.id.cashatbankgroupreport);
        cashatbankgroupreport.setTypeface(LoginActivity.sTypeface);
        cashatbankgroupreport.setText(shgDto.getCashAtBank());
        grouploan_groupname = (TextView) view.findViewById(R.id.grouploan_groupname);
        grouploan_groupname.setTypeface(LoginActivity.sTypeface);
        grouploan_groupname.setText(shgDto.getName() + " / " + shgDto.getPresidentName());

        grpname = (TextView) view.findViewById(R.id.grpname);
        grpname.setText(AppStrings.grouploanrepayment);
        grpname.setTypeface(LoginActivity.sTypeface);

        mTrasnsactionloan_repayment.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            int previous = -1;

            @Override
            public void onGroupExpand(int groupPosition) {

                if (groupPosition != previous) {
                    mTrasnsactionloan_repayment.collapseGroup(previous);
                    previous = groupPosition;
                }

            }
        });

        mTrasnsactionloan_repayment.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int i) {

            }
        });

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {
         //   RestClient.getRestClient(GroupLoanRepayment.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.GROUP_LOAN_REPAYMENT + "" + shgId, getActivity(), ServiceType.TRANSACTION_GROUP_LOAN_REPAYMENT);

            RestClient.getRestClient(GroupLoanRepayment.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.GROUP_LOAN_REPAYMENT + shgId, getActivity(), ServiceType.TRANSACTION_GROUP_LOAN_REPAYMENT);
        } else {
            Utils.showToast(getActivity(), "Network Not Available");
        }
    }

    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {

        try {

            switch (serviceType) {

                case TRANSACTION_GROUP_LOAN_REPAYMENT:

                    Log.d("getDetails", " " + result.toString());
                    responseDto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    if (responseDto.getStatusCode() == (Utils.Success_Code)) {
                        Utils.showToast(getActivity(), responseDto.getMessage());
                        responseDto.getResponseContent().getLoansList();
                        Log.d("check", "" + responseDto.getResponseContent().getLoansList());

                        for (int i = 0; i < responseDto.getResponseContent().getLoansList().size(); i++) {
                            listDataChild.put(responseDto.getResponseContent().getLoansList().get(0).getLoanId(), responseDto.getResponseContent().getLoansList());
                        }

//                    Log.e("Arraylist..........", "" + responseDto.getResponseContent().getLoansList());
//                    Log.e("Hashmap..........", "" + listDataChild.toString());

                        transaction_groupLoanRepayment_adapter = new Transaction_GroupLoanRepayment_Adapter(getActivity(), responseDto.getResponseContent().getLoansList(), listDataChild, this);

                        mTrasnsactionloan_repayment.setAdapter(transaction_groupLoanRepayment_adapter);
                    }else {
                        Utils.showToast(getActivity(), responseDto.getMessage());
                        if (responseDto.getStatusCode() == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                        }
                    }

                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemClick(ViewGroup parent, View view, int position) {

        Transaction_LoanType_Details fragment = new Transaction_LoanType_Details();
        Bundle bundle = new Bundle();
        bundle.putString("loan_id", (responseDto.getResponseContent().getLoansList().get(position).getLoanId()));
        bundle.putString("account_number", (responseDto.getResponseContent().getLoansList().get(position).getAccountNumber()));
        bundle.putString("loan_type", (responseDto.getResponseContent().getLoansList().get(position).getLoanTypeName()));
        bundle.putString("bank_name", (responseDto.getResponseContent().getLoansList().get(position).getBankName()));
        bundle.putString("outstanding", (responseDto.getResponseContent().getLoansList().get(position).getLoanOutstanding()));
        fragment.setArguments(bundle);
        NewDrawerScreen.showFragment(fragment);
/*

        FragmentTransaction transection=getFragmentManager().beginTransaction();
        Transaction_LoanType_Details transaction_loanType_details=new Transaction_LoanType_Details();
        transection.replace(R.id.static_frame, transaction_loanType_details);
        transection.commit();
*/

    }

    @Override
    public void onItemClickVerification(ViewGroup parent, View view, int position) {

    }
}
