package com.oasys.eshakti.digitization.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.digitization.Adapter.CustomItemAdapter;
import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.Dto.MemberList;
import com.oasys.eshakti.digitization.Dto.ResponseContents;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.Dto.SavingRequest;
import com.oasys.eshakti.digitization.Dto.ShgBankDetails;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.GetSpanText;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.activity.LoginActivity;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;
import com.oasys.eshakti.digitization.database.BankTable;
import com.oasys.eshakti.digitization.database.MemberTable;
import com.oasys.eshakti.digitization.database.SHGTable;
import com.oasys.eshakti.digitization.model.RowItem;
import com.oasys.eshakti.digitization.views.MaterialSpinner;
import com.oasys.eshakti.digitization.views.RaisedButton;
import com.tutorialsee.lib.TastyToast;


import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class SeedFund extends Fragment implements View.OnClickListener, NewTaskListener {
    private TextView mGroupName, mCashInHand, mCashAtBank;
    TextView mHeader, mLoanaccFixeddeposit, mLoanaccFixedDepositeText, mLoanaccWithdrawText, mLoanaccExpensesText;
    RadioButton mCashRadio, mBankRadio;
    EditText mWithdrawal, mExpenses;
    MaterialSpinner materialSpinner_Bank;
    RaisedButton mSubmitButton;
    View rootView;

    public static String mWithdrawalValue, mExpensesValue, mSelectedTypeValue;
    private Dialog mProgressDilaog;
    public static String mBankNameValue = null;
    public static String selectedItemBank, selectedType, selectedSBAcId, selectedBankAmount;
    private Dialog confirmationDialog;
    private Button mEdit_RaisedButton, mOk_RaisedButton;
    String mLastTrDate = null, mLastTr_ID = null;

    ArrayList<String> mBanknames_Array = new ArrayList<String>();
    ArrayList<String> mBanknamesId_Array = new ArrayList<String>();

    ArrayList<String> mEngSendtoServerBank_Array = new ArrayList<String>();
    ArrayList<String> mEngSendtoServerBankId_Array = new ArrayList<String>();
    boolean isGetTrid = false;

    ArrayList<String> mBankName = new ArrayList<String>();
    public ArrayList<ResponseContents> responseContentsList;
    private int mSize;
    private List<MemberList> memList;
    private ListOfShg shgDto;
    private NetworkConnection networkConnection;
    private ArrayList<ShgBankDetails> bankdetails;
    private ArrayList<RowItem> stateNameItems;
    private CustomItemAdapter bankNameAdapter;
    private String selectedItemBank_Selection;
    private String transaction;
    AlertDialog alertDialog;
    private TextView counterTextView;
    private TextView counterTimerStatusTxt;
    private CountDownTimer cndr;
    private static final String FORMAT_TIMER = "%02d:%02d";


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_new_loanaccount, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mSize = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, "")).size();
        memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        bankdetails = BankTable.getBankName(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        Bundle bundle = getArguments();
        transaction =  bundle.getString("trnsactiontype");
        init();

    }

    private void init() {
        responseContentsList = new ArrayList<>();
        mHeader = (TextView) rootView.findViewById(R.id.loanaccheader);
        mLoanaccFixeddeposit = (TextView) rootView.findViewById(R.id.loanaccfixeddeposit);
        mSubmitButton = (RaisedButton) rootView.findViewById(R.id.loanacc_submit);
        materialSpinner_Bank = (MaterialSpinner) rootView.findViewById(R.id.loanaccbankspinner);
        mWithdrawal = (EditText) rootView.findViewById(R.id.loanaccwithdrawal);
        mExpenses = (EditText) rootView.findViewById(R.id.loanaccexpenses);
        mCashRadio = (RadioButton) rootView.findViewById(R.id.radioloanaccCash);
        mBankRadio = (RadioButton) rootView.findViewById(R.id.radioLoanaccBank);
        mLoanaccFixedDepositeText = (TextView) rootView.findViewById(R.id.loanaccfixeddepositText);
        mLoanaccWithdrawText = (TextView) rootView.findViewById(R.id.loanaccWithdrawalTextView);
        mLoanaccExpensesText = (TextView) rootView.findViewById(R.id.loanaccexpenseslTextView);

        try {
            mGroupName = (TextView) rootView.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName()+" / "+shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
            mCashInHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashInHand.setTypeface(LoginActivity.sTypeface);

            mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
            mCashAtBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashAtBank.setTypeface(LoginActivity.sTypeface);
            RadioGroup radioGroup = (RadioGroup) rootView.findViewById(R.id.radio);
            radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    // checkedId is the RadioButton selected
                    switch (checkedId) {
                        case R.id.radioloanaccCash:
                            materialSpinner_Bank.setVisibility(View.GONE);
                            selectedType = "CASH";
                            break;

                        case R.id.radioLoanaccBank:
                            selectedType = "BANK";
                            materialSpinner_Bank.setVisibility(View.VISIBLE);
                            break;
                    }
                }
            });

            for (int i = 0; i < bankdetails.size(); i++) {
                mBanknames_Array.add(bankdetails.get(i).getBankName());
                mBanknamesId_Array.add(bankdetails.get(i).getShgSavingsAccountId());
            }

            for (int i = 0; i < bankdetails.size(); i++) {
                mEngSendtoServerBank_Array.add(bankdetails.get(i).getBankName());
                mEngSendtoServerBankId_Array.add(bankdetails.get(i).getBankId());
            }


            materialSpinner_Bank.setBaseColor(R.color.grey_400);

            materialSpinner_Bank.setFloatingLabelText(AppStrings.bankName);

            materialSpinner_Bank.setPaddingSafe(10, 0, 10, 0);

            mHeader.setText(AppStrings.mSeedFund);
            mHeader.setTypeface(LoginActivity.sTypeface);
            mLoanaccFixedDepositeText.setVisibility(View.GONE);
            mLoanaccExpensesText.setVisibility(View.GONE);
            mExpenses.setVisibility(View.GONE);
            mLoanaccFixeddeposit.setVisibility(View.GONE);


            mLoanaccWithdrawText.setText(AppStrings.amount);
            mLoanaccWithdrawText.setTypeface(LoginActivity.sTypeface);
            mLoanaccExpensesText.setText(AppStrings.mLoanaccExapenses);
            mLoanaccExpensesText.setTypeface(LoginActivity.sTypeface);

            mCashRadio.setText(AppStrings.mLoanaccCash);
            mCashRadio.setTypeface(LoginActivity.sTypeface);
            mBankRadio.setText(AppStrings.mLoanaccBank);
            mBankRadio.setTypeface(LoginActivity.sTypeface);

            mSubmitButton.setText(AppStrings.submit);
            mSubmitButton.setTypeface(LoginActivity.sTypeface);

            // mWithdrawal.setHint(AppStrings.mLoanaccWithdrawal);
            // mExpenses.setHint(AppStrings.mLoanaccExapenses);
            final String[] bankNames = new String[bankdetails.size() + 1];

            final String[] bankNames_BankID = new String[bankdetails.size() + 1];

            final String[] bankAmount = new String[bankdetails.size() + 1];

            bankNames[0] = String.valueOf(AppStrings.S_B_N);
            for (int i = 0; i < bankdetails.size(); i++) {
                bankNames[i + 1] = bankdetails.get(i).getBankName();
            }

            bankNames_BankID[0] = String.valueOf(AppStrings.S_B_N);
            for (int i = 0; i < bankdetails.size(); i++) {
                bankNames_BankID[i + 1] = bankdetails.get(i).getBankId();
            }

            bankAmount[0] = String.valueOf("Bank Amount");
            for (int i = 0; i < bankdetails.size(); i++) {
                bankAmount[i + 1] = bankdetails.get(i).toString();
            }


            int size = bankNames.length;

            stateNameItems = new ArrayList<RowItem>();
            for (int i = 0; i < size; i++) {
                RowItem rowItem = new RowItem(bankNames[i]);// SelectedGroupsTask.sBankNames.elementAt(i).toString());
                stateNameItems.add(rowItem);
            }
            bankNameAdapter = new CustomItemAdapter(getActivity(), stateNameItems);
            materialSpinner_Bank.setAdapter(bankNameAdapter);

          /*  bankNameAdapter = new CustomItemAdapter(getActivity(), stateNameItems);  // TODO::
            materialSpinner_Bank.setAdapter(bankNameAdapter);*/

            materialSpinner_Bank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    // TODO Auto-generated method stub

                    if (position == 0) {
                        selectedItemBank = bankNames_BankID[0];
                        mBankNameValue = "0";

                    } else {
                        selectedItemBank = bankNames_BankID[position];
                        selectedSBAcId = bankdetails.get(position - 1).getShgSavingsAccountId();
                        // selectedItemBank_Selection=bankdetails.get(position).getShgSavingsAccountId();

                        System.out.println("SELECTED BANK NAME : " + selectedItemBank);
                        mBankNameValue = selectedItemBank;
                        // selectedBankAmount = bankAmount[position];
                        String mBankname = null;
                        for (int i = 0; i < mBanknames_Array.size(); i++) {
                            if (selectedItemBank.equals(mEngSendtoServerBankId_Array.get(i))) {
                                mBankname = mEngSendtoServerBank_Array.get(i);
                            }
                        }

                        mBankNameValue = mBankname;

                    }
                    // Log.e("Selected Bank Name", mBankNameValue + "     Amount = " + selectedBankAmount);

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    // TODO Auto-generated method stub

                }
            });

            mSubmitButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    mWithdrawalValue = "0";
                    mExpensesValue = "0";
                    mSelectedTypeValue = "";

                    mWithdrawalValue = mWithdrawal.getText().toString().trim();
                    if (mWithdrawalValue.isEmpty()) {

                        TastyToast.makeText(getActivity(), AppStrings.mLoanaccNullToast, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);
                        /*
                        if (mBankRadio.isChecked() || mCashRadio.isChecked()) {


                            if (mBankRadio.isChecked()) {
                                mSelectedTypeValue = mBankRadio.getText().toString();
                                if (!mBankNameValue.equals("0") && mBankNameValue != null) {

                                    onShowConfirmationDialog();

                                } else {
                                    TastyToast.makeText(getActivity(), AppStrings.mLoanaccBankNullToast,
                                            TastyToast.LENGTH_SHORT, TastyToast.WARNING);

                                }
                            } else {
                                mSelectedTypeValue = mCashRadio.getText().toString();

                                selectedItemBank = "";

                                onShowConfirmationDialog();

                            }

                        } else {
                            TastyToast.makeText(getActivity(), AppStrings.mLoanaccCash_BankToast, TastyToast.LENGTH_SHORT,
                                    TastyToast.WARNING);
                        }*/
                    } else {
                        onShowConfirmationDialog();
                    }
                }


            });

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    private void onShowConfirmationDialog() {
        // TODO Auto-generated method stub
        confirmationDialog = new Dialog(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_new_confirmation, null);
        dialogView.setLayoutParams(
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
        confirmationHeader.setText(AppStrings.confirmation);
        confirmationHeader.setTypeface(LoginActivity.sTypeface);
        TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

        TableRow typeRow = new TableRow(getActivity());

        @SuppressWarnings("deprecation")
        TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
        contentParams.setMargins(10, 5, 10, 5);

        TextView memberName_Text = new TextView(getActivity());
        memberName_Text.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mLoanAccType)));
        memberName_Text.setTypeface(LoginActivity.sTypeface);
        memberName_Text.setTextColor(R.color.white);
        memberName_Text.setPadding(5, 5, 5, 5);
        memberName_Text.setLayoutParams(contentParams);
        typeRow.addView(memberName_Text);

        TextView confirm_values = new TextView(getActivity());
        confirm_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(mSelectedTypeValue)));
        confirm_values.setTextColor(R.color.white);
        confirm_values.setPadding(5, 5, 5, 5);
        confirm_values.setGravity(Gravity.RIGHT);
        confirm_values.setLayoutParams(contentParams);
        typeRow.addView(confirm_values);

        confirmationTable.addView(typeRow,
                new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        if (mBankRadio.isChecked()) {
            TableRow bankNameRow = new TableRow(getActivity());

            @SuppressWarnings("deprecation")
            TableRow.LayoutParams bankNameParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            bankNameParams.setMargins(10, 5, 10, 5);

            TextView bankName_Text = new TextView(getActivity());
            bankName_Text.setText(
                    GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mLoanaccSpinnerFloating)));
            bankName_Text.setTypeface(LoginActivity.sTypeface);
            bankName_Text.setTextColor(R.color.white);
            bankName_Text.setPadding(5, 5, 5, 5);
            bankName_Text.setLayoutParams(bankNameParams);
            bankNameRow.addView(bankName_Text);

            TextView bankName_values = new TextView(getActivity());
            bankName_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(mBankNameValue)));
            bankName_values.setTextColor(R.color.white);
            bankName_values.setPadding(5, 5, 5, 5);
            bankName_values.setGravity(Gravity.RIGHT);
            bankName_values.setLayoutParams(bankNameParams);
            bankNameRow.addView(bankName_values);

            confirmationTable.addView(bankNameRow,
                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        }

        TableRow withdrawRow = new TableRow(getActivity());
        @SuppressWarnings("deprecation")
        TableRow.LayoutParams withdrawParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
        withdrawParams.setMargins(10, 5, 10, 5);

        TextView withdraw = new TextView(getActivity());
        withdraw.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.amount)));
        withdraw.setTypeface(LoginActivity.sTypeface);
        withdraw.setTextColor(R.color.white);
        withdraw.setPadding(5, 5, 5, 5);
        withdraw.setLayoutParams(withdrawParams);
        withdrawRow.addView(withdraw);

        TextView withdraw_values = new TextView(getActivity());
        withdraw_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(mWithdrawalValue)));
        withdraw_values.setTextColor(R.color.white);
        withdraw_values.setPadding(5, 5, 5, 5);
        withdraw_values.setGravity(Gravity.RIGHT);
        withdraw_values.setLayoutParams(withdrawParams);
        withdrawRow.addView(withdraw_values);

        confirmationTable.addView(withdrawRow,
                new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit);
        mEdit_RaisedButton.setText(AppStrings.edit);
        mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
        mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
        // 205,
        // 0));
        mEdit_RaisedButton.setOnClickListener(this);

        mOk_RaisedButton = (Button) dialogView.findViewById(R.id.frag_Ok);
        mOk_RaisedButton.setText(AppStrings.yes);
        mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
        mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
        mOk_RaisedButton.setOnClickListener(this);

        confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmationDialog.setCanceledOnTouchOutside(false);
        confirmationDialog.setContentView(dialogView);
        confirmationDialog.setCancelable(true);
        confirmationDialog.show();

        ViewGroup.MarginLayoutParams margin = (ViewGroup.MarginLayoutParams) dialogView.getLayoutParams();
        margin.leftMargin = 10;
        margin.rightMargin = 10;
        margin.topMargin = 10;
        margin.bottomMargin = 10;
        margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fragment_Edit:
                mWithdrawalValue = "0";
                mExpensesValue = "0";
                mBankNameValue = "0";
                selectedItemBank = "0";
                mSelectedTypeValue = "0";
                mSubmitButton.setClickable(true);
                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();
                break;
            case R.id.frag_Ok:
                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();
                SavingRequest sr = new SavingRequest();
//                sr.setIncomeTypeId(Income.sSelectedIncomeMenu.getId());
//                if (selectedType.equals(AppStrings.mLoanaccCash)) {
//                    sr.setModeOfCash("2");
//                } else {
//                    sr.setModeOfCash("1");
////                    sr.setShgSavingsAccountId(selectedSBAcId);
////                    sr.setBankId(selectedItemBank);
//                }
                sr.setModeOfCash("2");
                sr.setTransactionType(transaction);
                sr.setDigital(true);
                sr.setUserId(MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, ""));
                sr.setAgentId(MySharedPreference.readString(getActivity(), MySharedPreference.AGENT_ID, ""));
                sr.setShgId(shgDto.getShgId());
                sr.setTransactionDate(shgDto.getLastTransactionDate());
//                sr.setMemberSaving(arrMem);
                sr.setMobileDate(System.currentTimeMillis() + "");
                sr.setAmount(mWithdrawalValue);
                String sreqString = new Gson().toJson(sr);
                if (networkConnection.isNetworkAvailable()) {
                    onTaskStarted();

                    ViewGroup viewGroup = rootView.findViewById(android.R.id.content);
                    View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.online_transaction_timer, viewGroup, false);
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setView(dialogView);
                    alertDialog = builder.create();
                    alertDialog.show();
                    alertDialog.setCancelable(false);
                    counterTextView = dialogView.findViewById(R.id.counterTimerTxt);
                    counterTimerStatusTxt = dialogView.findViewById(R.id.counterTimerStatusTxt);

                    RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.SEEDFUND, sreqString, getActivity(), ServiceType.SEED_FUND);

                    cndr = new CountDownTimer(MySharedPreference.readLong(getActivity(), MySharedPreference.fttimeout,180000), 1000) { // adjust the milli seconds here
                        public void onTick(long millisUntilFinished) {
                            counterTextView.setText("" + String.format(FORMAT_TIMER,
                                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(
                                            TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                                            TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))) + " minutes");
                        }

                        @Override
                        public void onFinish() {
//                            MemberDrawerScreen.showFragment(new Digitization_Deposit_Details());
                            cndr.cancel();
                            alertDialog.dismiss();
                            TastyToast.makeText(getActivity(), AppStrings.counterDismiss, TastyToast.LENGTH_SHORT,
                                    TastyToast.WARNING);
//                            timer.cancel();
                        }


                    }.start();
                }
                break;
        }
    }

    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        if (mProgressDilaog != null) {
            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                mProgressDilaog.dismiss();
                mProgressDilaog = null;
                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();
            }

            switch (serviceType) {
                case SEED_FUND:
                    try {
                        if(result!=null) {
                            ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                            String message = cdto.getMessage();
                            int statusCode = cdto.getStatusCode();
                            if (statusCode == Utils.Success_Code) {
                                Utils.showToast(getActivity(), message);


                                if (cdto.getResponseContents() != null) {
                                    for (int i = 0; i < cdto.getResponseContents().size(); i++) {
                                        ResponseContents contents = new ResponseContents();
                                        contents.setMemberName(cdto.getResponseContents().get(i).getMemberName());
                                        contents.setTransactionId(cdto.getResponseContents().get(i).getTransactionId());
                                        contents.setMessage(cdto.getResponseContents().get(i).getMessage());
                                        contents.setTxType(AppStrings.income);
                                        responseContentsList.add(contents);
                                    }

                                    ViewSavingTransactionDetails viewSavingTransactionDetails = new ViewSavingTransactionDetails();
                                    Bundle bundle = new Bundle();
                                    bundle.putSerializable("transactiondetails", responseContentsList);
                                    viewSavingTransactionDetails.setArguments(bundle);
                                    NewDrawerScreen.showFragment(viewSavingTransactionDetails);
                                    TastyToast.makeText(getActivity(), "Transaction Completed",
                                            TastyToast.LENGTH_SHORT, TastyToast.SUCCESS);
                                    alertDialog.dismiss();
                                    cndr.cancel();
                                }


                                /*if (shgDto.getFFlag() == null || !shgDto.getFFlag().equals("1")) {
                                    SHGTable.updateTransactionSHGDetails(shgDto.getShgId());
                                }
                                FragmentManager fm = getFragmentManager();
                                fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                MainFragment mainFragment = new MainFragment();
                                Bundle bundles = new Bundle();
                                bundles.putString("Transaction", MainFragment.Flag_Transaction);
                                mainFragment.setArguments(bundles);
                                MemberDrawerScreen.showFragment(mainFragment);
//                            MemberDrawerScreen.showFragment(new MainFragment());
                                TastyToast.makeText(getActivity(), "Transaction Completed",
                                        TastyToast.LENGTH_SHORT, TastyToast.SUCCESS);
                                alertDialog.dismiss();
                                cndr.cancel();*/

                            } else {

                                if (statusCode == 401) {

                                    Log.e("Group Logout", "Logout Sucessfully");
                                    AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                                    if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                        mProgressDilaog.dismiss();
                                        mProgressDilaog = null;
                                    }
                                }
                                Utils.showToast(getActivity(), message);
                                TastyToast.makeText(getActivity(), "Transaction Timeout",
                                        TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                                alertDialog.dismiss();
                                cndr.cancel();

                            }
                        }else {
                            TastyToast.makeText(getActivity(), AppStrings.failedDigiResponse,
                                    TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                            alertDialog.dismiss();
                            cndr.cancel();
                        }
                    } catch (Exception e) {

                    }
                    break;
            }


        }

    }
}
