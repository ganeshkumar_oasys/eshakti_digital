package com.oasys.eshakti.digitization.fragment;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.SystemClock;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mantra.mfs100.DeviceInfo;
import com.mantra.mfs100.FingerData;
import com.mantra.mfs100.MFS100;
import com.mantra.mfs100.MFS100Event;
import com.oasys.eshakti.digitization.Adapter.EnrollFpAdapter;
import com.oasys.eshakti.digitization.Adapter.WalletBankIdAdapter;
import com.oasys.eshakti.digitization.Dto.EnrollFP.EnrollFP;
import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.Dto.ResponseContents;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.Dto.VerifyFPDto;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.activity.LoginActivity;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;
import com.oasys.eshakti.digitization.database.EnrollFPData;
import com.oasys.eshakti.digitization.database.SHGTable;
import com.oasys.eshakti.digitization.views.MaterialSpinner;
import com.oasys.eshakti.digitization.views.RaisedButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class FinancialConfirmation extends Fragment implements MFS100Event, View.OnClickListener, NewTaskListener, AdapterView.OnItemSelectedListener {

    private View rootView;
    private ListOfShg shgDto;
    private EShaktiApplication globalVariable;

    private TextView mGroupName;
    private TextView mCashInHand;
    private TextView mCashAtBank;
    private TextView header;
    private RaisedButton mRaised_SubmitButton;
    private Dialog mProgressDilaog;
    private RaisedButton fragment_start_scan;
    private ImageView img_fp;
    private Object finger1;
    private MaterialSpinner spinner_enrollfinger;

    public ArrayList<ResponseContents> responseContentsList = new ArrayList<>();

    private static long Threshold = 1500;
    private byte[] finger1bytes, finger2bytes;
    private ResponseContents value;
    private String memid;
    private EditText edit_Name;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.financial_ly, container, false);
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        globalVariable = (EShaktiApplication) getActivity().getApplicationContext();

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        try {
            if (globalVariable.is_OTG()) {
                if (Utils.mfs100 == null) {
                    Utils.mfs100 = new MFS100(this);
                } else {
                    //      InitScanner();
                }
                if (Utils.mfs100 != null) {
                    Utils.mfs100.SetApplicationContext(getActivity());
                }
            } else {
                Toast.makeText(getActivity(), "Enable OTG device connectivity!", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        try {
            if (globalVariable.is_OTG()) {
                if (Utils.mfs100 == null) {
                    Utils.mfs100 = new MFS100(this);
                } else {
                    //      InitScanner();
                }
                if (Utils.mfs100 != null) {
                    Utils.mfs100.SetApplicationContext(getActivity());
                }
            } else {
                Toast.makeText(getActivity(), "Enable OTG device connectivity!", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        try {

            try {
                if (globalVariable.is_OTG()) {
                    if (Utils.mfs100 == null) {
                        Utils.mfs100 = new MFS100(this);
                    } else {
                        //      InitScanner();
                    }
                    if (Utils.mfs100 != null) {
                        Utils.mfs100.SetApplicationContext(getActivity());
                    }
                } else {
                    Toast.makeText(getActivity(), "Enable OTG device connectivity!", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {
                List<EnrollFP> lrDto = EnrollFPData.getAllList();
                for (int i = 0; i < lrDto.size(); i++) {
                    ResponseContents contents = new ResponseContents();
                    contents.setDesignation(lrDto.get(i).getDesignation());
                    contents.setName(lrDto.get(i).getName());
                    contents.setId(lrDto.get(i).getId());
                    responseContentsList.add(contents);
                }

                WalletBankIdAdapter aa = new WalletBankIdAdapter(getActivity(), responseContentsList);
                spinner_enrollfinger.setAdapter(aa);

            } else {
                RestClient.getRestClient(FinancialConfirmation.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.GET_ENROLL_LIST + shgDto.getShgId(), getActivity(), ServiceType.GET_ENROLLMENT_LIST);
            }
            mGroupName = (TextView) rootView.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
            mCashInHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashInHand.setTypeface(LoginActivity.sTypeface);

            mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
            mCashAtBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashAtBank.setTypeface(LoginActivity.sTypeface);

            spinner_enrollfinger = (MaterialSpinner) rootView.findViewById(R.id.spinner_enrollfinger);
            spinner_enrollfinger.setBaseColor(R.color.grey_400);
            spinner_enrollfinger.setFloatingLabelText(String.valueOf(AppStrings.enrollment));
            spinner_enrollfinger.setOnItemSelectedListener(this);

            edit_Name = (EditText) rootView.findViewById(R.id.edit_Name);
            edit_Name.setHint("" + AppStrings.Name);
            edit_Name.setClickable(false);
            edit_Name.setTypeface(LoginActivity.sTypeface);
            edit_Name.setBackgroundResource(R.drawable.edittext_background);


            header = (TextView) rootView.findViewById(R.id.header);
            header.setText(AppStrings.enrollment);
            header.setTypeface(LoginActivity.sTypeface);

            img_fp = (ImageView) rootView.findViewById(R.id.img_fp);

            fragment_start_scan = (RaisedButton) rootView.findViewById(R.id.fragment_start_scan);
            fragment_start_scan.setTypeface(LoginActivity.sTypeface);
            fragment_start_scan.setOnClickListener(this);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public byte[] Decrypt(String text, String Key) throws Exception {

        byte[] textbytes = Base64.decode(text, Base64.DEFAULT);

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        byte[] keyBytes = new byte[16];
        byte[] pwdBytes = Key.getBytes();
        int len = pwdBytes.length;
        if (len > keyBytes.length)
            len = keyBytes.length;
        System.arraycopy(pwdBytes, 0, keyBytes, 0, len);
        SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
        IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);
        cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);

        //byte[] results = cipher.doFinal(text.getBytes("UTF-8"));
        byte[] results = cipher.doFinal(textbytes);
        //  Log.e("TAG", "byte array in encryption..."+results.toString());
        Log.e("TAG", "base 64 in decryption..." + new String(Base64.decode(results, Base64.DEFAULT), "UTF-8"));
        return results; // it returns the result as a String
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.fragment_start_scan:

                mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
                mProgressDilaog.show();
                //  textToSpeech.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
                if (fragment_start_scan.getText().equals("VERIFY")) {
                    //   mProgressDilaog.dismiss();
                    fragment_start_scan.setEnabled(false);
                    img_fp.setImageResource(R.drawable.thumb_impress);
                 /*   if (edit_Name.getText().toString().length() <= 0) {
                        Toast.makeText(getActivity(), "Enter Name", Toast.LENGTH_SHORT).show();
                        fragment_start_scan.setEnabled(true);
                        if (mProgressDilaog != null && mProgressDilaog.isShowing())
                            mProgressDilaog.dismiss();
                        return;
                    }*/


                    final EShaktiApplication globalVariable = (EShaktiApplication) getActivity().getApplicationContext();
                    if (globalVariable.is_OTG()) {
                        // RdScan();
                        MySharedPreference.writeBoolean(getActivity(), MySharedPreference.FINGER_FLAG1, true);

                        scan();
                    } else {
                        fragment_start_scan.setEnabled(true);
                        if (mProgressDilaog != null && mProgressDilaog.isShowing())
                            mProgressDilaog.dismiss();
                        Toast.makeText(getActivity(), "Enable OTG device connectivity!", Toast.LENGTH_SHORT).show();
                    }
                 /*   count++;
                    mSubmit_Raised_Button.setText(AppStrings.scanning);

                    if (count % 2 != 0) {
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //Do something after 100ms
                                mProgressDilaog.dismiss();
                                mSubmit_Raised_Button.setText(AppStrings.continue_flow);
                                mSubmit_Raised_Button.setVisibility(View.VISIBLE);
                                retry_Raised_Button.setVisibility(View.GONE);
                                skip_Raised_Button.setVisibility(View.GONE);
                            }
                        }, 100);
                    } else {
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //Do something after 100ms
                                mProgressDilaog.dismiss();
                                mSubmit_Raised_Button.setText(AppStrings.str_scan);
                                mSubmit_Raised_Button.setVisibility(View.GONE);
                                retry_Raised_Button.setVisibility(View.VISIBLE);
                                skip_Raised_Button.setVisibility(View.VISIBLE);
                            }
                        }, 100);
                    }*/
                } else {
                    fragment_start_scan.setEnabled(true);
                    if (mProgressDilaog != null && mProgressDilaog.isShowing())
                        mProgressDilaog.dismiss();


                    //Perform your functionality
                    //   sendRequest();


                }
                break;


        }

    }


    int timeout = 10000;
    DeviceInfo deviceIfo = null;
    byte[] Enroll_Template;
    byte[] isoFeatureSet;
    Bitmap bitmapImg;
    int mfsVer = 41;

    private void scan() {
        String encodedBioMetricInfo = null;
        bitmapImg = StartSyncCapture();
        if (MySharedPreference.readBoolean(getActivity(), MySharedPreference.FINGER_FLAG1, false))
            img_fp.setImageBitmap(bitmapImg);


        isoFeatureSet = Enroll_Template;
        try {
            if (isoFeatureSet.length > 0) {
                Log.e("MemberUpdateAuthAct", "isoFeatureSet..." + Arrays.toString(isoFeatureSet));
//                        findViewById(R.id.btnCancel).setVisibility(View.VISIBLE);
                fragment_start_scan.setEnabled(true);
                verify(isoFeatureSet, finger1bytes, finger2bytes);
                encodedBioMetricInfo = Base64.encodeToString(isoFeatureSet, Base64.DEFAULT);
                Log.e("verifydata", encodedBioMetricInfo);
                // String data = new String(Base64.encode(Encrypt(encodedBioMetricInfo, Utils.ENCRYPT_DECRYPT_KEY), Base64.DEFAULT), "UTF-8");*
            }

        } catch (Exception e) {
            fragment_start_scan.setEnabled(true);
            if (mProgressDilaog != null && mProgressDilaog.isShowing())
                mProgressDilaog.dismiss();

        }
    }

    private void verify(byte[] isoFeatureSet, byte[] finger1bytes, byte[] finger2bytes) {
        try {
            if (finger1bytes == null || finger1bytes == null) {
                return;
            }

            int ret = Utils.mfs100.MatchISO(finger1bytes, isoFeatureSet);
            int ret1 = Utils.mfs100.MatchISO(finger2bytes, isoFeatureSet);

            if (ret < 0) {
                Toast.makeText(getActivity(), "Error: " + ret + "(" + Utils.mfs100.GetErrorMsg(ret) + ")", Toast.LENGTH_SHORT).show();
            } else {
                if (ret >= 96 || ret1 >= 96) {

                    Toast.makeText(getActivity(), "Verified Successfully", Toast.LENGTH_SHORT).show();

                    if (ret >= 96) {
                        Toast.makeText(getActivity(), "Finger matched", Toast.LENGTH_SHORT).show();
                    } else if (ret1 >= 96) {
                        //  SetTextOnUIThread("Finger matched");
                        Toast.makeText(getActivity(), "Finger matched", Toast.LENGTH_SHORT).show();
                    }
                    NewDrawerScreen.showFragment(new GroupReportFragment());

                } else {
                    // SetTextOnUIThread();
                    Toast.makeText(getActivity(), "Finger not matched", Toast.LENGTH_SHORT).show();
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Bitmap StartSyncCapture() {
        try {
            Thread trd = new Thread(new Runnable() {
                @Override
                public void run() {
                    FingerData fingerData = new FingerData();
                    int ret = Utils.mfs100.AutoCapture(fingerData, timeout, false);
                    Log.e("sample app", "ret value..." + ret);
                    Log.e("sample app", "ret value..." + Utils.mfs100.GetErrorMsg(ret));
                    if (ret != 0) {
                        Toast.makeText(getActivity(), Utils.mfs100.GetErrorMsg(ret), Toast.LENGTH_SHORT).show();
                        Enroll_Template = null;
                        bitmapImg = BitmapFactory.decodeResource(getResources(), R.drawable.thumb_impress);
                        // bitmapImg = icon;

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                img_fp.setImageBitmap(bitmapImg);


                            }
                        });


                    } else {
                        Enroll_Template = new byte[fingerData.ISOTemplate().length];
                        System.arraycopy(fingerData.ISOTemplate(), 0, Enroll_Template, 0, fingerData.ISOTemplate().length);
                        bitmapImg = BitmapFactory.decodeByteArray(fingerData.FingerImage(), 0, fingerData.FingerImage().length);
                        //     img_fp.setImageBitmap(bitmapImg);

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (MySharedPreference.readBoolean(getActivity(), MySharedPreference.FINGER_FLAG1, false))
                                    img_fp.setImageBitmap(bitmapImg);


                                //fragment_start_scan.setEnabled(false);


                            }
                        });
                    }
                }
            });
            trd.run();
            trd.join();
        } catch (Exception ex) {
            ex.printStackTrace();

            //CommonMethod.writeLog("Exception in ContinuesScan(). Message:- " + ex.getMessage());
        } finally {
            fragment_start_scan.setEnabled(true);
            if (mProgressDilaog != null && mProgressDilaog.isShowing())
                mProgressDilaog.dismiss();

        }

        return bitmapImg;
    }


    private long mLastAttTime = 0l;


    public byte[] Encrypt(String text, String Key) throws Exception {

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        byte[] keyBytes = new byte[16];
        byte[] pwdBytes = Key.getBytes();
        int len = pwdBytes.length;
        if (len > keyBytes.length)
            len = keyBytes.length;
        System.arraycopy(pwdBytes, 0, keyBytes, 0, len);
        SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
        IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);

        byte[] results = cipher.doFinal(text.getBytes("UTF-8"));
        //  Log.e("TAG", "byte array in encryption..."+results.toString());
        Log.e("TAG", "base 64 in encryption..." + new String(Base64.encode(results, Base64.DEFAULT), "UTF-8"));
        return results; // it returns the result as a String
    }


    @Override
    public void OnDeviceAttached(int vid, int pid, boolean hasPermission) {
        if (SystemClock.elapsedRealtime() - mLastAttTime < Threshold) {
            return;
        }
        if (!globalVariable.is_OTG()) {
            return;
        }
        mLastAttTime = SystemClock.elapsedRealtime();
        int ret;
        if (!hasPermission) {
            Toast.makeText(getActivity(), "Permission denied", Toast.LENGTH_SHORT).show();
            return;
        }
        try {
            if (vid == 1204 || vid == 11279) {
                if (pid == 34323) {
                    ret = Utils.mfs100.LoadFirmware();
                    if (ret != 0) {
                        Toast.makeText(getActivity(), Utils.mfs100.GetErrorMsg(ret), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), "Load firmware success", Toast.LENGTH_SHORT).show();
                    }
                } else if (pid == 4101) {
                    String key = "Without Key";
                    ret = Utils.mfs100.Init();
                    if (ret == 0) {
                        showSuccessLog(key);
                    } else {
                        //     SetTextOnUIThread(Utils.mfs100.GetErrorMsg(ret));
                        Toast.makeText(getActivity(), Utils.mfs100.GetErrorMsg(ret), Toast.LENGTH_SHORT).show();
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showSuccessLog(String key) {
        try {
            Toast.makeText(getActivity(), "Init success", Toast.LENGTH_SHORT).show();
            String info = "\nKey: " + key + "\nSerial: "
                    + Utils.mfs100.GetDeviceInfo().SerialNo() + " Make: "
                    + Utils.mfs100.GetDeviceInfo().Make() + " Model: "
                    + Utils.mfs100.GetDeviceInfo().Model()
                    + "\nCertificate: " + Utils.mfs100.GetCertification();
            Toast.makeText(getActivity(), info, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
        }
    }

    /*   @Override
       public void OnPreview(FingerData fingerData) {

       }

       @Override
       public void OnCaptureCompleted(boolean b, int i, String s, FingerData fingerData) {

       }
   */
    long mLastDttTime = 0l;

    @Override
    public void OnDeviceDetached() {
        try {

            if (SystemClock.elapsedRealtime() - mLastDttTime < Threshold) {
                return;
            }
            mLastDttTime = SystemClock.elapsedRealtime();
            UnInitScanner();

            Toast.makeText(getActivity(), "Device removed", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
        }
    }

    public void UnInitScanner() {
        try {
            deviceIfo = null;
            if (Utils.mfs100 != null) {
                int ret = Utils.mfs100.UnInit();
                if (ret != 0) {
//                    Toast.makeText(MemberUpdateAuthenticationActivity.this, Util.mfs100.GetErrorMsg(ret), Toast.LENGTH_SHORT).show();
                } else {
//                    Toast.makeText(MemberUpdateAuthenticationActivity.this, "Uninit Success", Toast.LENGTH_SHORT).show();
                }
            }
        } catch (Exception e) {
            Log.e("UnInitScanner.EX", e.toString());
        }
    }


    @Override
    public void OnHostCheckFailed(String s) {

    }

    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
            mProgressDilaog.dismiss();
            mProgressDilaog = null;
        }
        switch (serviceType) {
            case VERIFY_LIST:
                if (result != null && result.length() > 0) {

                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    VerifyFPDto mrDto = gson.fromJson(result, VerifyFPDto.class);

                    try {
                        finger1bytes = Base64.decode(Decrypt(mrDto.getResponseObject().getFid_1(), Utils.ENCRYPT_DECRYPT_KEY), Base64.DEFAULT);
                        finger2bytes = Base64.decode(Decrypt(mrDto.getResponseObject().getFid_2(), Utils.ENCRYPT_DECRYPT_KEY), Base64.DEFAULT);
                        Log.e("finger1bytes", mrDto.getResponseObject().getFid_1());
                        Log.e("finger2bytes", mrDto.getResponseObject().getFid_2());
                       /* finger1bytes = Decrypt(mrDto.getResponseObject().getFid_1(), Utils.ENCRYPT_DECRYPT_KEY);
                        finger2bytes = Decrypt(mrDto.getResponseObject().getFid_2(), Utils.ENCRYPT_DECRYPT_KEY);*/
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;

            case GET_ENROLLMENT_LIST:
                //getAadhaarResponse(result);  //TODO::
                if (result != null && result.length() > 0) {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    ResponseDto lrDto = gson.fromJson(result, ResponseDto.class);
                    int statusCode = lrDto.getStatusCode();
                    String message = lrDto.getMessage();
                    Log.d("response status", " " + statusCode);
                    if (statusCode == 400 || statusCode == 401 || statusCode == 403 || statusCode == 500 || statusCode == 503) {
                        // showMessage(statusCode);
                        Utils.showToast(getActivity(), message);


                    } else if (statusCode == Utils.Success_Code) {
                        Utils.showToast(getActivity(), message);
                        if (lrDto.getResponseContents() != null) {
                            if (lrDto.getResponseContents() != null) {
                                responseContentsList.clear();
                                ResponseContents rs = new ResponseContents();
                                rs.setDesignation("SELECT ENROLLER");
                                responseContentsList.add(rs);


                                for (int i = 0; i < lrDto.getResponseContents().size(); i++) {
                                    ResponseContents contents = new ResponseContents();
                                    contents.setDesignation(lrDto.getResponseContents().get(i).getDesignation());
                                    contents.setName(lrDto.getResponseContents().get(i).getName());
                                    contents.setId(lrDto.getResponseContents().get(i).getId());
                                    responseContentsList.add(contents);
                                }

                                EnrollFpAdapter aa = new EnrollFpAdapter(getActivity(), responseContentsList);
                                spinner_enrollfinger.setAdapter(aa);

                            }
                        }
                    }
                } else {

                }
                break;
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        value = (ResponseContents) adapterView.getItemAtPosition(i);

        if (value != null && !value.getDesignation().trim().equals("SELECT ENROLLER")) {
            edit_Name.setText(value.getName());


           /* String colonDelimited = "1:2:3:4:5";
            String[] numbers = colonDelimited.split(":");
            System.out.println(Arrays.toString(numbers));


            Read more: https://javarevisited.blogspot.com/2017/01/how-to-split-string-based-on-delimiter-in-java.html#ixzz60pNaLhVV*/


            if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {
                JSONObject jsonobj = new JSONObject();
                try {
                    jsonobj.put("member_id", MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, "")+":"+value.getId());
                    RestClient.getRestClient(FinancialConfirmation.this).callRestWebService(Constants.VERIFY_LIST, jsonobj.toString(), getActivity(), ServiceType.VERIFY_LIST);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                List<EnrollFP> enrollFPS = EnrollFPData.getEnrollFpList(shgDto.getShgId(), value.getId());
                try {
                    finger1bytes = Base64.decode(Decrypt(enrollFPS.get(0).getFinger1(), Utils.ENCRYPT_DECRYPT_KEY), Base64.DEFAULT);
                    finger2bytes = Base64.decode(Decrypt(enrollFPS.get(0).getFinger2(), Utils.ENCRYPT_DECRYPT_KEY), Base64.DEFAULT);
                 //   Log.e("finger1bytes", enrollFPS.get(0).getFinger1());
                  //  Log.e("finger2bytes", enrollFPS.get(0).getFinger2());
                 /*   finger1bytes = Decrypt(enrollFPS.get(0).getFinger1(), Utils.ENCRYPT_DECRYPT_KEY);
                    finger2bytes = Decrypt(enrollFPS.get(0).getFinger2(), Utils.ENCRYPT_DECRYPT_KEY);*/
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}

