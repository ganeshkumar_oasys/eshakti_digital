package com.oasys.eshakti.digitization.Service;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.fragment.Deposit;
import com.oasys.eshakti.digitization.fragment.Savings;

import org.apache.http.HttpStatus;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Restclient_Timeout {

//    private static final int MY_SOCKET_TIMEOUT_MS = 180000;
    private static NewTaskListener onCallSuccessCallback;
    private String TAG = "RestClient";
    private Dialog dialog;

    public static Restclient_Timeout restClient;

    public static Restclient_Timeout getRestClient(NewTaskListener NewTaskListener) {
        if (restClient == null) {
            restClient = new Restclient_Timeout();
        }
        onCallSuccessCallback = NewTaskListener;
        return restClient;
    }

    public void callRestWebService(String _url, String reqJson, final Context context, final ServiceType serviceType) {
        JsonObjectRequest jsonObjReq = null;
        try {
            final String url = _url;
            Log.e("request", reqJson);
            //showProgressDialog(context);
            jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(reqJson),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("URL", url);
                            Log.e(TAG, "volley response......" + response.toString());
                            onCallSuccessCallback.onTaskFinished(response.toString(), serviceType);
                            MySharedPreference.writeBoolean(context, MySharedPreference.UNAUTH, false);
                            //dismisProgressDialog();
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    NetworkResponse networkResponse = error.networkResponse;
                    if (networkResponse != null && networkResponse.statusCode == HttpStatus.SC_UNAUTHORIZED) {
                        // HTTP Status Code: 401 Unauthorized
                        MySharedPreference.writeBoolean(context, MySharedPreference.UNAUTH, true);
                        final ResponseDto lrDto = new ResponseDto();
                        lrDto.setStatusCode(401);
                        lrDto.setMessage("Unauthorized! Invalid credentials.");
                        String e_response = new Gson().toJson(lrDto);
                        Log.e(TAG, "volley response......" + e_response);
                        onCallSuccessCallback.onTaskFinished(e_response, serviceType);
                    } else if (networkResponse != null && networkResponse.statusCode == HttpStatus.SC_SERVICE_UNAVAILABLE) {
                        // HTTP Status Code: 401 Unauthorized
                        //MySharedPreference.writeBoolean(context, MySharedPreference.UNAUTH, true);
                        final ResponseDto lrDto = new ResponseDto();
                        lrDto.setStatusCode(503);
                        lrDto.setMessage(AppStrings.service_unavailable);
                        String e_response = new Gson().toJson(lrDto);
                        Log.e(TAG, "volley response......" + e_response);
                        onCallSuccessCallback.onTaskFinished(e_response, serviceType);
                    } else {
                        MySharedPreference.writeBoolean(context, MySharedPreference.UNAUTH, false);
                        if (error instanceof NoConnectionError) {
                            Log.e(TAG, "volley error response......" + error.getMessage());
                            onCallSuccessCallback.onTaskFinished(null, serviceType);
                        } else if (error instanceof ServerError) {
                            Log.e(TAG, "volley error response......" + error.getMessage());
                            onCallSuccessCallback.onTaskFinished(null, serviceType);
                        } else if (error instanceof TimeoutError) {
                            Log.e(TAG, "volley error response......" + error.getMessage());
                            onCallSuccessCallback.onTaskFinished(null, serviceType);
                        } else {
                            Log.e(TAG, "volley error response......" + error.getMessage());
                        }
                    }
                    // dismisProgressDialog();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    if (MySharedPreference.readString(context, MySharedPreference.ACCESS_TOKEN, "") != null && MySharedPreference.readString(context, MySharedPreference.ACCESS_TOKEN, "").length() > 0)
                        headers.put("Authorization", "Bearer " + MySharedPreference.readString(context, MySharedPreference.ACCESS_TOKEN, ""));

                    return headers;
                }
            };
        } catch (Exception e) {
        }


        int  MY_SOCKET_TIMEOUT_MS = 0;

        switch (serviceType)
        {
            case DEPOSIT:

            MY_SOCKET_TIMEOUT_MS =  (int)MySharedPreference.readLong(context,MySharedPreference.fttimeout,180000);
            break;

            case SAVINGS:

            MY_SOCKET_TIMEOUT_MS =  (int) Savings.timeout_saving;
            break;

            case OI_S_P:

                MY_SOCKET_TIMEOUT_MS =  (int) Savings.timeout_saving;
                break;

            case WALLET_ONLINE:

                MY_SOCKET_TIMEOUT_MS =  (int)MySharedPreference.readLong(context,MySharedPreference.upi_timeout,180000);
                break;

        }
        RetryPolicy retryPolicy = new DefaultRetryPolicy(MY_SOCKET_TIMEOUT_MS, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonObjReq.setRetryPolicy(retryPolicy);
        EShaktiApplication.getInstance().addToRequestQueue(jsonObjReq);
    }

    public void callRestWebServiceForPutMethod(String _url, String reqJson, final Context context, final ServiceType serviceType) {
        JsonObjectRequest jsonObjReq = null;
        try {
            final String url = _url;
            Log.e("request", reqJson);
            showProgressDialog(context);
            jsonObjReq = new JsonObjectRequest(Request.Method.PUT, url, new JSONObject(reqJson),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("URL", url);
                            Log.e(TAG, "volley response......" + response.toString());
                            onCallSuccessCallback.onTaskFinished(response.toString(), serviceType);
                            MySharedPreference.writeBoolean(context, MySharedPreference.UNAUTH, false);
                            dismisProgressDialog();
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    NetworkResponse networkResponse = error.networkResponse;
                    if (networkResponse != null && networkResponse.statusCode == HttpStatus.SC_UNAUTHORIZED) {
                        // HTTP Status Code: 401 Unauthorized
                        MySharedPreference.writeBoolean(context, MySharedPreference.UNAUTH, true);
                        final ResponseDto lrDto = new ResponseDto();
                        lrDto.setStatusCode(401);
                        lrDto.setMessage("Unauthorized! Invalid credentials.");
                        String e_response = new Gson().toJson(lrDto);
                        Log.e(TAG, "volley response......" + e_response);
                        onCallSuccessCallback.onTaskFinished(e_response, serviceType);
                    }  else if (networkResponse != null && networkResponse.statusCode == HttpStatus.SC_SERVICE_UNAVAILABLE) {
                        // HTTP Status Code: 401 Unauthorized
                        //MySharedPreference.writeBoolean(context, MySharedPreference.UNAUTH, true);
                        final ResponseDto lrDto = new ResponseDto();
                        lrDto.setStatusCode(503);
                        lrDto.setMessage(AppStrings.service_unavailable);
                        String e_response = new Gson().toJson(lrDto);
                        Log.e(TAG, "volley response......" + e_response);
                        onCallSuccessCallback.onTaskFinished(e_response, serviceType);
                    } else {
                        MySharedPreference.writeBoolean(context, MySharedPreference.UNAUTH, false);
                        if (error instanceof NoConnectionError) {
                            Log.e(TAG, "volley error response......" + error.getMessage());
                            onCallSuccessCallback.onTaskFinished(null, serviceType);
                        } else if (error instanceof ServerError) {
                            Log.e(TAG, "volley error response......" + error.getMessage());
                            onCallSuccessCallback.onTaskFinished(null, serviceType);
                        } else if (error instanceof TimeoutError) {
                            Log.e(TAG, "volley error response......" + error.getMessage());
                            onCallSuccessCallback.onTaskFinished(null, serviceType);
                        } else {
                            Log.e(TAG, "volley error response......" + error.getMessage());
                        }
                    }
                    dismisProgressDialog();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    if (MySharedPreference.readString(context, MySharedPreference.ACCESS_TOKEN, "") != null && MySharedPreference.readString(context, MySharedPreference.ACCESS_TOKEN, "").length() > 0)
                        headers.put("Authorization", "Bearer " + MySharedPreference.readString(context, MySharedPreference.ACCESS_TOKEN, ""));
                    return headers;
                }
            };
        } catch (Exception e) {
        }
//        RetryPolicy retryPolicy = new DefaultRetryPolicy(MY_SOCKET_TIMEOUT_MS, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//        jsonObjReq.setRetryPolicy(retryPolicy);
        EShaktiApplication.getInstance().addToRequestQueue(jsonObjReq);
    }

    public void callWebServiceForGetMethod(final String url, final Context context, final ServiceType serviceType) {
        String tag_string_req = "string_req";
        showProgressDialog(context);


        StringRequest strReq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("URL", url);
                Log.e(TAG, "volley response......" + response);
                onCallSuccessCallback.onTaskFinished(response, serviceType);
                MySharedPreference.writeBoolean(context, MySharedPreference.UNAUTH, false);
                dismisProgressDialog();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.statusCode == HttpStatus.SC_UNAUTHORIZED) {
                    // HTTP Status Code: 401 Unauthorized
                    MySharedPreference.writeBoolean(context, MySharedPreference.UNAUTH, true);
                    final ResponseDto lrDto = new ResponseDto();
                    lrDto.setStatusCode(401);
                    lrDto.setMessage("Unauthorized! Invalid credentials.");
                    String e_response = new Gson().toJson(lrDto);
                    Log.e(TAG, "volley response......" + e_response);
                    onCallSuccessCallback.onTaskFinished(e_response, serviceType);
                }  else if (networkResponse != null && networkResponse.statusCode == HttpStatus.SC_SERVICE_UNAVAILABLE) {
                    // HTTP Status Code: 401 Unauthorized
                    //MySharedPreference.writeBoolean(context, MySharedPreference.UNAUTH, true);
                    final ResponseDto lrDto = new ResponseDto();
                    lrDto.setStatusCode(503);
                    lrDto.setMessage(AppStrings.service_unavailable);
                    String e_response = new Gson().toJson(lrDto);
                    Log.e(TAG, "volley response......" + e_response);
                    onCallSuccessCallback.onTaskFinished(e_response, serviceType);
                } else {
                    MySharedPreference.writeBoolean(context, MySharedPreference.UNAUTH, false);
                    if (error instanceof NoConnectionError) {
                        Log.e(TAG, "volley error response......" + error.getMessage());
                        onCallSuccessCallback.onTaskFinished(null, serviceType);
                    } else if (error instanceof ServerError) {
                        Log.e(TAG, "volley error response......" + error.getMessage());
                        onCallSuccessCallback.onTaskFinished(null, serviceType);
                    } else if (error instanceof TimeoutError) {
                        Log.e(TAG, "volley error response......" + error.getMessage());
                        onCallSuccessCallback.onTaskFinished(null, serviceType);
                    } else {
                        Log.e(TAG, "volley error response......" + error.getMessage());
                    }
                }
                dismisProgressDialog();

            }

        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                if (MySharedPreference.readString(context, MySharedPreference.ACCESS_TOKEN, "") != null && MySharedPreference.readString(context, MySharedPreference.ACCESS_TOKEN, "").length() > 0)
                    headers.put("Authorization", "Bearer " + MySharedPreference.readString(context, MySharedPreference.ACCESS_TOKEN, ""));
                return headers;
            }
        };


//        RetryPolicy retryPolicy = new DefaultRetryPolicy(MY_SOCKET_TIMEOUT_MS, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//        strReq.setRetryPolicy(retryPolicy);
        EShaktiApplication.getInstance().addToRequestQueue(strReq);
    }

    public void callRestWebServiceForDelete(final String url, final Context context, final ServiceType serviceType, final String sessionId) {
        String tag_string_req = "string_req";
        final ProgressDialog pDialog = new ProgressDialog(context);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.DELETE, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e("URL", url);
                Log.e(TAG, "volley response......" + response);
                onCallSuccessCallback.onTaskFinished(response, serviceType);
                pDialog.dismiss();

            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(context,
                            context.getString(R.string.error_network_timeout),
                            Toast.LENGTH_LONG).show();
                }
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                pDialog.dismiss();
            }

        });

        EShaktiApplication.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    public void showProgressDialog(Context context) {
        if (dialog == null || (!dialog.isShowing())) {
            dialog = AppDialogUtils.createProgressDialog(context);
            dialog.show();

        }
    }

    public void dismisProgressDialog() {
        if (dialog != null) {
            if (dialog.isShowing()) {
                dialog.dismiss();
                dialog = null;
            }
        }
    }
}
