package com.oasys.eshakti.digitization.Dto;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.Data;

@Data
public class Transactiontype implements Serializable {
    String name;
    String createdDate;
    String createdBy;
    String modifiedDate;
    String modifiedBy;
    String id,errorCode,errorName,description;
    String settings;
    String status;
    String parentId;
    String sflag;
    String pflag;


}
