package com.oasys.eshakti.digitization.Dto.EnrollFP;


import lombok.Data;

@Data
public class EnrollFP {

    String userId, id, name;
    String shgId;
    String designation;
    String finger1;
    String finger2;


}
