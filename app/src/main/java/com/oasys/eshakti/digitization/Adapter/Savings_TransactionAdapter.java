package com.oasys.eshakti.digitization.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasys.eshakti.digitization.Dto.ResponseContents;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.activity.LoginActivity;

import java.util.ArrayList;

public class Savings_TransactionAdapter extends RecyclerView.Adapter<Savings_TransactionAdapter.MyViewholder> {


    private Context context;
    private ArrayList<ResponseContents> responseContentsArrayList;

    public Savings_TransactionAdapter(Context context, ArrayList<ResponseContents> responseContentsArrayList) {
        this.context = context;
        this.responseContentsArrayList = responseContentsArrayList;

    }


    @NonNull
    @Override
    public MyViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.savingstransactionsummary, parent, false);
        return new MyViewholder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewholder holder, int position) {

        holder.member_name.setText("MEMBER NAME :"+responseContentsArrayList.get(position).getMemberName());
        holder.transactionid.setText("TRANSACTION ID :" +responseContentsArrayList.get(position).getTransactionId());
        holder.statuscode.setText("STATUS :"+responseContentsArrayList.get(position).getMessage());

    }

    @Override
    public int getItemCount() {
        return responseContentsArrayList.size();
    }

    class MyViewholder extends  RecyclerView.ViewHolder
    {
        TextView member_name, transactionid,statuscode ;

        public MyViewholder(View itemView) {
            super(itemView);

            member_name = (TextView) itemView.findViewById(R.id.mMembernamevalue);
            member_name.setTypeface(LoginActivity.sTypeface);

            transactionid = (TextView) itemView.findViewById(R.id.mMembertransactionid);
            transactionid.setTypeface(LoginActivity.sTypeface);

            statuscode = (TextView) itemView.findViewById(R.id.mMemberstatus);
            statuscode.setTypeface(LoginActivity.sTypeface);
        }
    }
}
