package com.oasys.eshakti.digitization.Dto;

import lombok.Data;

@Data
public class CheckListDto {

    private String BankTransaction;
    private String Savings;
    private String Income;
    private String BankLoanDisbursement;
    private String Expense, MemberLoanRepayment, InternalLoanRepayment, GroupLoanRepayment, InternalLoanDisbursement;
    private String name;
    private boolean status;

}
