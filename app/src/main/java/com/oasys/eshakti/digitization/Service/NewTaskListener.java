package com.oasys.eshakti.digitization.Service;


import com.oasys.eshakti.digitization.OasysUtils.ServiceType;

public interface NewTaskListener {
	
	void onTaskStarted();
	
	void onTaskFinished(String result, ServiceType serviceType);

}
