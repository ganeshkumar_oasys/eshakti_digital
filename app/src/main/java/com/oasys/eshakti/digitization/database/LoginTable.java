package com.oasys.eshakti.digitization.database;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.oasys.eshakti.digitization.Dto.LoginDto;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.EShaktiApplication;

public class LoginTable {

    private static SQLiteDatabase database;
    private static DbHelper dataHelper;
    static Cursor cursor;

    public LoginTable(Activity activity) {
        dataHelper = new DbHelper(activity);
    }

    public static void openDatabase() {
        dataHelper = new DbHelper(EShaktiApplication.getInstance());
        dataHelper.onOpen(database);
        database = dataHelper.getWritableDatabase();
    }

    public static void closeDatabase() {
        if (database != null && database.isOpen()) {
            database.close();
        }
    }

    public static void insertLoginDetails(ResponseDto rDto, LoginDto ldto) {

        if (rDto != null) {
            try {
                openDatabase();
                ContentValues values = new ContentValues();
                values.put(TableConstants.USERID, (rDto.getResponseContent().getUserId().length() > 0) ? rDto.getResponseContent().getUserId() : "");
                values.put(TableConstants.USERNAME, ldto.getUsername());
                values.put(TableConstants.PASSWORD, ldto.getPassword());
                values.put(TableConstants.LANGUAGE, "");

                database.insertWithOnConflict(TableName.TABLE_LOGIN, TableConstants.USERID, values, SQLiteDatabase.CONFLICT_REPLACE);
            } catch (Exception e) {
                Log.e("insertEnergyException", e.toString());
            } finally {
                closeDatabase();
            }
        }

    }

    public static void updateAnimatorLogin(ResponseDto rDto) {

        if (rDto != null) {
            try {
                openDatabase();
                ContentValues values = new ContentValues();
                values.put(TableConstants.ANIMATOR_ID, (rDto.getResponseContent().getAnimatorId().length() > 0) ? rDto.getResponseContent().getAnimatorId() : "");
                database.update(TableName.TABLE_LOGIN, values, TableConstants.USERID + " = " + rDto.getResponseContent().getUserId(), null);
            } catch (Exception e) {
                Log.e("insertEnergyException", e.toString());
            } finally {
                closeDatabase();
            }
        }

    }

    public static LoginDto getUserLogin(String UserName) {

        LoginDto bD = new LoginDto();
        try {
            openDatabase();

            String selectQuery = "SELECT * FROM " + TableName.TABLE_LOGIN + " where " + TableConstants.USERNAME + " LIKE '" + UserName + "'";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    bD.setUsername(cursor.getString(cursor.getColumnIndex(TableConstants.USERNAME)));
                    bD.setPassword(cursor.getString(cursor.getColumnIndex(TableConstants.PASSWORD)));
                    bD.setLanguage(cursor.getString(cursor.getColumnIndex(TableConstants.LANGUAGE)));

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

        return bD;

    }
}
