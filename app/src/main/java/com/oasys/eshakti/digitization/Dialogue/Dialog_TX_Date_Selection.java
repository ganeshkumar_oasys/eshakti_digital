package com.oasys.eshakti.digitization.Dialogue;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;
import com.oasys.eshakti.digitization.database.SHGTable;
import com.oasys.eshakti.digitization.fragment.CashInHandHistory;
import com.oasys.eshakti.digitization.fragment.DigitalTxReport;
import com.oasys.eshakti.digitization.fragment.RechargeHistory;
import com.oasys.eshakti.digitization.fragment.TR_SummaryFragment;
import com.oasys.eshakti.digitization.fragment.TransactionDetails;
import com.oasys.eshakti.digitization.fragment.ViewRechargeDetails;
import com.oasys.eshakti.digitization.fragment.WalletBalanceHistory;
import com.oasys.eshakti.digitization.views.RaisedButton;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Dialog_TX_Date_Selection extends DialogFragment implements NewTaskListener {

    static Context mContext;
    public String mitem;
    private View rootView;
    private ListOfShg shgDto;
    SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    Date fromDate, openinigDate, toDate;
    private TextView f_date, t_date;
    private Dialog mProgressDilaog;
    private NetworkConnection networkConnection;
    private String f_datestr;
    private String t_datestr;
    private String userId;
    private String langId;
    private String animatorid;
    private String flag;

    public Dialog_TX_Date_Selection() {
        // TODO Auto-generated constructor stub
    }


    @SuppressLint("ValidFragment")
    public Dialog_TX_Date_Selection(Context context, String item, String flag) {
        // TODO Auto-generated constructor stub
        mContext = context;
        mitem = item;
        this.flag = flag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        // setRetainInstance(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        rootView = inflater.inflate(R.layout.view_tx_date_ly, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());

        userId = MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, "");
        langId = MySharedPreference.readString(getActivity(), MySharedPreference.LANG_ID, "");

        if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {

            String url = Constants.BASE_URL + Constants.SHG_URL + userId + "&languageId=" + langId;
            RestClient.getRestClient(Dialog_TX_Date_Selection.this).callWebServiceForGetMethod(url, getActivity(), ServiceType.SHG_LIST);

        }

        init();
    }

    private void init() {

        RaisedButton submit = (RaisedButton) rootView.findViewById(R.id.fragment_Submit_button);
        submit.setText(AppStrings.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //dismiss();

                if (flag == "WALLET") {

                    if (t_date.getText().toString() == null) {
                        return;
                    }

                    if (f_date.getText().toString() == null) {
                        return;
                    }


                    if (f_date.getText().toString() != null && f_date.getText().toString().length() > 0 && t_date.getText().toString() != null && t_date.getText().toString().length() > 0) {

                        if (mitem.equals(NewDrawerScreen.O_TR_REP)) {

                            dismiss();
                            TR_SummaryFragment frag = new TR_SummaryFragment();
                            Bundle d = new Bundle();
                            d.putString("fromdate", f_datestr);
                            d.putString("todate", t_datestr);
                            d.putString("type", mitem);
                            d.putString("userid", userId);
                            d.putString("animatorid", animatorid);
                            frag.setArguments(d);
                            NewDrawerScreen.showFragment(frag);

                        }
                        else if(mitem.equals(NewDrawerScreen.W_WB_REP))
                        {
                            dismiss();
                            WalletBalanceHistory frag = new WalletBalanceHistory();
                            Bundle d = new Bundle();
                            d.putString("fromdate", f_datestr);
                            d.putString("todate", t_datestr);
                            d.putString("type", mitem);
                            d.putString("userid",  userId);
                            d.putString("animatorid", animatorid);
                            frag.setArguments(d);
                            NewDrawerScreen.showFragment(frag);

                        }
                        else if(mitem.equals(NewDrawerScreen.W_RC_REP))
                        {
                            dismiss();
                            RechargeHistory frag = new RechargeHistory();
                            Bundle d = new Bundle();
                            d.putString("fromdate", f_datestr);
                            d.putString("todate", t_datestr);
                            d.putString("type", mitem);
                            d.putString("userid",  userId);
                            d.putString("animatorid", animatorid);
                            frag.setArguments(d);
                            NewDrawerScreen.showFragment(frag);

                        }
                        else if(mitem.equals(NewDrawerScreen.CASHIN_TRANS_HIS))
                        {
                            dismiss();
                            CashInHandHistory frag = new CashInHandHistory();
                            Bundle d = new Bundle();
                            d.putString("fromdate", f_datestr);
                            d.putString("todate", t_datestr);
                            d.putString("type", mitem);
                            d.putString("userid",  userId);
                            d.putString("animatorid", animatorid);
                            frag.setArguments(d);
                            NewDrawerScreen.showFragment(frag);

                        }
                        else if(mitem.equals(NewDrawerScreen.TRANS_DET)){

                            dismiss();
                            TransactionDetails frag = new TransactionDetails();
                            Bundle d = new Bundle();
                            d.putString("fromdate", f_datestr);
                            d.putString("todate", t_datestr);
                            d.putString("type", mitem);
                            d.putString("userid", userId);
                            d.putString("animatorid", animatorid);
                            frag.setArguments(d);
                            NewDrawerScreen.showFragment(frag);
                        }

                    } else {
                        Utils.showToast(mContext, "Select a transaction period! ");
                    }
                } else {
                    if (t_date.getText().toString() == null) {
                        return;
                    }

                    if (f_date.getText().toString() == null) {
                        return;
                    }
                    if (f_date.getText().toString() != null && f_date.getText().toString().length() > 0 && t_date.getText().toString() != null && t_date.getText().toString().length() > 0) {
                        dismiss();
                        DigitalTxReport frag = new DigitalTxReport();
                        Bundle d = new Bundle();
                        d.putString("fromdate", f_datestr);
                        d.putString("todate", t_datestr);
                        d.putString("type", mitem);
                        d.putString("shgId", shgDto.getShgId());
                        frag.setArguments(d);
                        NewDrawerScreen.showFragment(frag);
                    } else {
                        Utils.showToast(mContext, "Select a transaction period! ");
                    }

                   // Toast.makeText(getActivity(), "velladhurai", Toast.LENGTH_SHORT).show();
                }
            }
        });
        f_date = (TextView) rootView.findViewById(R.id.f_date);
        f_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Date cDate = new Date(System.currentTimeMillis());
                    String formattedDate = df.format(cDate.getTime());
                    Date currentDate = df.parse(formattedDate);
                    Calendar ccal1 = Calendar.getInstance();
                    ccal1.setTimeInMillis(currentDate.getTime());
                    int cyear = ccal1.get(Calendar.YEAR); // this is deprecated
                    int cmonth = ccal1.get(Calendar.MONTH); // this is deprecated
                    int cday = ccal1.get(Calendar.DATE);
                    Calendar now = Calendar.getInstance();

                    final DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker datePicker, int year, int month, int day) {

                            Calendar selectionCal = Calendar.getInstance();
                            selectionCal.set(year, month, day);
                            String formattedDate = df.format(selectionCal.getTime());
                            String formattedDate1 = df1.format(selectionCal.getTime());
                            f_datestr = formattedDate1;
                            try {
                                fromDate = df.parse(formattedDate);
                                //   disbursementDate = formattedDate + "";
                                f_date.setText(formattedDate);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                        }
                    }, now.get(Calendar.YEAR), now.get(Calendar.DATE), now.get(Calendar.DAY_OF_MONTH));
                    now.set(cyear, cmonth, cday);
                    datePickerDialog.getDatePicker().setMaxDate(now.getTimeInMillis());
                    datePickerDialog.show();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


        t_date = (TextView) rootView.findViewById(R.id.t_date);
        t_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Date cDate = new Date(System.currentTimeMillis());
                    String formattedDate = df.format(cDate.getTime());
                    Date currentDate = df.parse(formattedDate);
                    Calendar ccal1 = Calendar.getInstance();
                    ccal1.setTimeInMillis(currentDate.getTime());
                    int cyear = ccal1.get(Calendar.YEAR); // this is deprecated
                    int cmonth = ccal1.get(Calendar.MONTH); // this is deprecated
                    int cday = ccal1.get(Calendar.DATE);
                    Calendar now = Calendar.getInstance();

                    final DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker datePicker, int year, int month, int day) {

                            Calendar selectionCal = Calendar.getInstance();
                            selectionCal.set(year, month, day);
                            String formattedDate = df.format(selectionCal.getTime());
                            String formattedDate1 = df1.format(selectionCal.getTime());
                            t_datestr = formattedDate1;
                            try {
                                toDate = df.parse(formattedDate);
                                // disbursementDate = formattedDate + "";
                                t_date.setText(formattedDate);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                        }
                    }, now.get(Calendar.YEAR), now.get(Calendar.DATE), now.get(Calendar.DAY_OF_MONTH));
                    now.set(cyear, cmonth, cday);
                    datePickerDialog.getDatePicker().setMaxDate(now.getTimeInMillis());
                    datePickerDialog.show();


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }


    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
            mProgressDilaog.dismiss();
            mProgressDilaog = null;
        }
        switch (serviceType) {

            case SHG_LIST:
                try {

                    if (result != null && result.length() > 0) {
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();
                        ResponseDto lrDto = gson.fromJson(result, ResponseDto.class);
                        int statusCode = lrDto.getStatusCode();
                        String message = lrDto.getMessage();
                        Log.d("response status", " " + statusCode);
                        if (statusCode == 400 || statusCode == 401 || statusCode == 403 || statusCode == 500 || statusCode == 503) {
                            // showMessage(statusCode);
                            Utils.showToast(getActivity(), message);
                            if (statusCode == 401) {

                                Log.e("Group Logout", "Logout Sucessfully");
                                AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                                if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                    mProgressDilaog.dismiss();
                                    mProgressDilaog = null;
                                }
                            }

                            if (statusCode == 503) {

                                Utils.showToast(getActivity(), AppStrings.service_unavailable);

                            }


                        } else if (statusCode == Utils.Success_Code) {

                            animatorid = lrDto.getResponseContent().getAnimatorId();


                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;


            case VIEW_TX:
                if (result != null && result.length() > 0) {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    ResponseDto lrDto = gson.fromJson(result, ResponseDto.class);
                    int statusCode = lrDto.getStatusCode();
                    String message = lrDto.getMessage();
                    Log.d("response status", " " + statusCode);
                    if (statusCode == 400 || statusCode == 401 || statusCode == 403 || statusCode == 500 || statusCode == 503) {

                        Utils.showToast(getActivity(), message);
                        if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        }

                        if (statusCode == 503) {

                            Utils.showToast(getActivity(), AppStrings.service_unavailable);

                        }


                    } else if (statusCode == Utils.Success_Code) {

                        if (f_date.getText().toString() != null && f_date.getText().toString().length() > 0 && t_date.getText().toString() != null && t_date.getText().toString().length() > 0) {
                            dismiss();
                            ViewRechargeDetails frag = new ViewRechargeDetails();
                            Bundle d = new Bundle();
                            d.putString("fromdate", f_datestr);
                            d.putString("todate", t_datestr);
                            d.putString("type", mitem);
                            d.putString("animatorid", animatorid);
                            frag.setArguments(d);
                            NewDrawerScreen.showFragment(frag);
                        } else {
                            Utils.showToast(mContext, "Select a transaction period! ");
                        }
                    }
                }
                break;

        }
    }
}
