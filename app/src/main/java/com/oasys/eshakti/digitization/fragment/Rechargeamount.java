package com.oasys.eshakti.digitization.fragment;


import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.digitization.Dto.ModeOfpayment;
import com.oasys.eshakti.digitization.Dto.OnlineDto;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.Dto.Upi_VCS_Request;
import com.oasys.eshakti.digitization.Dto.Wallet_C_UPI_Rx;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.Service.Restclient_Timeout;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;
import com.tutorialsee.lib.TastyToast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

/**
 * A simple {@link Fragment} subclass.
 */
public class Rechargeamount extends Fragment implements View.OnClickListener, NewTaskListener {
    private View view;
    private TextView mRecharge_wallet_submit, mRecharge_wallet_cancel;

    DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    private String currentDateStr;
    private TextView daily_date_time;
    private EditText rechargeamount_edit, vpa_number;
    private NetworkConnection networkConnection;
    private String userId;
    private String vpanumber;
    private AlertDialog alertDialog;
    private TextView counterTextView;
    private TextView counterTimerStatusTxt;
    private CountDownTimer cndr;
    private static final String FORMAT_TIMER = "%02d:%02d";
    private Handler handler;
    private Runnable mRunnable;


    public Rechargeamount() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_rechargeamount, container, false);
        userId = MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, "");

        initView(view);
        return view;
    }

    private void initView(View view) {
        try {
            Calendar calender = Calendar.getInstance();
            currentDateStr = df.format(calender.getTime());
            mRecharge_wallet_submit = (TextView) view.findViewById(R.id.mRecharge_wallet_submit);
            mRecharge_wallet_submit.setOnClickListener(this);
            mRecharge_wallet_cancel = (TextView) view.findViewById(R.id.mRecharge_wallet_cancel);
            mRecharge_wallet_cancel.setOnClickListener(this);
            daily_date_time = (TextView) view.findViewById(R.id.daily_date_time);
            daily_date_time.setText(currentDateStr);
            rechargeamount_edit = (EditText) view.findViewById(R.id.rechargeamount_edit);
            vpa_number = (EditText) view.findViewById(R.id.edit_vpaNumber);
            vpanumber = MySharedPreference.readString(getActivity(), MySharedPreference.VPA_NUMBER, "");
            vpa_number.setText(vpanumber);


            networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.mRecharge_wallet_submit:
                try {

                    if (rechargeamount_edit.getText().toString() != null && rechargeamount_edit.getText().toString().length() > 0) {

                        Wallet_C_UPI_Rx sr = new Wallet_C_UPI_Rx();
                        ModeOfpayment md = new ModeOfpayment();
                        OnlineDto onlineDto = new OnlineDto();
                        ArrayList<Upi_VCS_Request> cr = new ArrayList<>();
                        Upi_VCS_Request upi_vcs_request = new Upi_VCS_Request();
                        upi_vcs_request.setAmount(rechargeamount_edit.getText().toString());

                        if (vpa_number.getText() != null && vpa_number.getText().length() > 1)
                            upi_vcs_request.setPayerVirtualAddress(vpa_number.getText().toString());
                        else {
                            Utils.showToast(getActivity(), "Enter valid VPA number!");
                            return;
                        }
                        upi_vcs_request.setRemarks("Wallet Recharge");
                        cr.add(upi_vcs_request);

                        sr.setMerchantId(MySharedPreference.readString(getActivity(), MySharedPreference.MERCHANT_ID, ""));
                        sr.setAmount(rechargeamount_edit.getText().toString());
                        sr.setWalletId(MySharedPreference.readString(getActivity(), MySharedPreference.WALLET_ID, ""));
                        sr.setAgentId(MySharedPreference.readString(getActivity(), MySharedPreference.AGENT_ID, ""));
                        sr.setUserId(userId);
                        md.setType("online");
                        onlineDto.setUpiCollect("1");
                        onlineDto.setUpiCollectRequest(cr);
                        md.setOnline(onlineDto);
                        sr.setModeOfPayment(md);
                        String sreqString = new Gson().toJson(sr);
                        Log.e("totaldata", "" + sreqString);
                        mRecharge_wallet_submit.setClickable(false);// TODO:: OnResponse

                        if (networkConnection.isNetworkAvailable()) {
                            onTaskStarted();

                            ViewGroup viewGroup = view.findViewById(android.R.id.content);
                            View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.online_transaction_timer, viewGroup, false);
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setView(dialogView);
                            alertDialog = builder.create();
                            alertDialog.show();
                            alertDialog.setCancelable(false);
                            counterTextView = dialogView.findViewById(R.id.counterTimerTxt);
                            counterTimerStatusTxt = dialogView.findViewById(R.id.counterTimerStatusTxt);

                            Restclient_Timeout.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.WALLET_RE_CHR, sreqString, getActivity(), ServiceType.WALLET_ONLINE);

                          /*  cndr = new CountDownTimer(MySharedPreference.readLong(getActivity(), MySharedPreference.upi_timeout, 180000), 1000) { // adjust the milli seconds here
                                public void onTick(long millisUntilFinished) {
                                    counterTextView.setText("" + String.format(FORMAT_TIMER,
                                            TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(
                                                    TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                                            TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                                                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))) + " minutes");
                                }

                                @Override
                                public void onFinish() {
                                    cndr.cancel();
                                    alertDialog.dismiss();
                                }


                            }.start();*/
                        } else {
                            Utils.showToast(getActivity(), "Please enter an amount!");
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.mRecharge_wallet_cancel:
                FragmentManager fm = getFragmentManager();
                fm.popBackStackImmediate();
                break;
        }
    }

    @Override
    public void onTaskStarted() {

    }


    @Override
    public void onStop() {
        super.onStop();
        if (handler != null)
            handler.removeCallbacks(mRunnable);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (handler != null)
            handler.removeCallbacks(mRunnable);
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        switch (serviceType) {

            case CALL_B_RECHARGE:

                if (result != null) {
                    ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    String message = cdto.getMessage();
                    int statusCode = cdto.getStatusCode();

                    if (statusCode == Utils.Success_Code || statusCode == 320) {
                        Utils.showToast(getActivity(), message);
                        if (statusCode == Utils.Success_Code) {

                            if (alertDialog != null && alertDialog.isShowing())
                                alertDialog.dismiss();
                            if (cndr != null)
                                cndr.cancel();
                            if (handler != null)
                                handler.removeCallbacks(mRunnable);
                            String merchant_id = cdto.getResponseContents().get(0).getMerchantTransactionId();
                            String status = cdto.getResponseContents().get(0).getTransactionStatusId().getDescription();
                            String amt = cdto.getResponseContents().get(0).getAmount();
                            Cashpaymentsuccess cashpaymentsuccess = new Cashpaymentsuccess();
                            Bundle bundle = new Bundle();
                            if (status.equals("Transaction Failure")) {
                                bundle.putString("type", "cash_fail");
                            } else {
                                TastyToast.makeText(getActivity(), "Transaction Completed",
                                        TastyToast.LENGTH_SHORT, TastyToast.SUCCESS);
                                bundle.putString("type", "upi");
                            }
                            bundle.putString("amt", amt);
                            bundle.putString("tr_id", merchant_id);
                            bundle.putString("status", status);
                            cashpaymentsuccess.setArguments(bundle);
                            NewDrawerScreen.showFragment(cashpaymentsuccess);
                        }
                    } else {
                        Utils.showToast(getActivity(), message);
                        if (handler != null)
                            handler.removeCallbacks(mRunnable);

                        TastyToast.makeText(getActivity(), AppStrings.failedDigiResponse,
                                TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                        if (alertDialog != null && alertDialog.isShowing())
                            alertDialog.dismiss();
                        if (cndr != null)
                            cndr.cancel();
                    }
                } else {
                    TastyToast.makeText(getActivity(), AppStrings.failedDigiResponse,
                            TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                    if (alertDialog != null && alertDialog.isShowing())
                        alertDialog.dismiss();
                    if (cndr != null)
                        cndr.cancel();
                }

                break;


            case WALLET_ONLINE:
                try {
                    if (result != null) {
                        ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                        String message = cdto.getMessage();
                        int statusCode = cdto.getStatusCode();

                        if (statusCode == Utils.Success_Code) {

                            String merchant_id = cdto.getResponseContent().getMerchantTransactionId();
                            Log.e("merchant_id", merchant_id);
                            mRecharge_wallet_submit.setClickable(true);
                           /* Cashpaymentsuccess cashpaymentsuccess = new Cashpaymentsuccess();
                            Bundle bundle = new Bundle();
                            bundle.putString("type", "upi");
                            bundle.putString("amt", rechargeamount_edit.getText().toString());
                            bundle.putString("tr_id", merchant_id);
                            cashpaymentsuccess.setArguments(bundle);
                            MemberDrawerScreen.showFragment(cashpaymentsuccess);*/
                          /*  TastyToast.makeText(getActivity(), "Transaction Completed",
                                    TastyToast.LENGTH_SHORT, TastyToast.SUCCESS);*/
                            Utils.showToast(getActivity(), message);

                            cndr = new CountDownTimer(MySharedPreference.readLong(getActivity(), MySharedPreference.upi_timeout, 180000), 1000) { // adjust the milli seconds here
                                public void onTick(long millisUntilFinished) {
                                    counterTextView.setText("" + String.format(FORMAT_TIMER,
                                            TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(
                                                    TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                                            TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                                                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))) + " minutes");
                                }

                                @Override
                                public void onFinish() {
                                    cndr.cancel();
                                    alertDialog.dismiss();
                                    if (handler != null)
                                        handler.removeCallbacks(mRunnable);
                                    TastyToast.makeText(getActivity(), AppStrings.counterDismiss, TastyToast.LENGTH_SHORT,
                                            TastyToast.WARNING);
                                }


                            }.start();

                          /*  handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    //Do something after 20 seconds

                                    handler.postDelayed(this, 10000);
                                }
                            }, 20000);*/
                            handler = new Handler();
                            mRunnable = new Runnable() {
                                @Override
                                public void run() {
                                    Restclient_Timeout.getRestClient(Rechargeamount.this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.CALL_B_RECHARGE + merchant_id + "&transactionType=WALLET_RECHARGE", getActivity(), ServiceType.CALL_B_RECHARGE);
                                    handler.postDelayed(this, 20000);
                                }
                            };

                            handler.post(mRunnable);

                            Utils.showToast(getActivity(), message);

                        } else {
                            Utils.showToast(getActivity(), message);

                            if (statusCode == 401) {

                                Log.e("Group Logout", "Logout Sucessfully");
                                AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                            }

                            if (statusCode == 503) {

                                Utils.showToast(getActivity(), AppStrings.service_unavailable);

                            }

                            TastyToast.makeText(getActivity(), AppStrings.failedDigiResponse,
                                    TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                            if (alertDialog != null && alertDialog.isShowing())
                                alertDialog.dismiss();
                            if (cndr != null)
                                cndr.cancel();


                        }
                    } else {
                        TastyToast.makeText(getActivity(), AppStrings.failedDigiResponse,
                                TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                        if (alertDialog != null && alertDialog.isShowing())
                            alertDialog.dismiss();
                        if (cndr != null)
                            cndr.cancel();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

        }

    }
}
