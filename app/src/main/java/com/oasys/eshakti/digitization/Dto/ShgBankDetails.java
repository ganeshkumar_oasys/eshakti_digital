package com.oasys.eshakti.digitization.Dto;


import lombok.Data;

@Data
public class ShgBankDetails {
    private String shgId;
    private String bankId;
    private String bankName;
    private String branchName;
    private String accountNumber;
    private String bankNameId;
    private String branchNameId;
    private String shgSavingsAccountId,currentBalance,currentFixedDepositBalance;

}
