package com.oasys.eshakti.digitization.database;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.oasys.eshakti.digitization.Dto.EnrollFP.EnrollFP;
import com.oasys.eshakti.digitization.Dto.LoanBankDto;
import com.oasys.eshakti.digitization.Dto.LoanDisbursementDto;
import com.oasys.eshakti.digitization.Dto.LoanDto;
import com.oasys.eshakti.digitization.Dto.ResponseContents;
import com.oasys.eshakti.digitization.EShaktiApplication;

import java.util.ArrayList;
import java.util.List;

public class EnrollFPData {

    private static SQLiteDatabase database;
    private static DbHelper dataHelper;
    static Cursor cursor;

    public EnrollFPData(Activity activity) {
        dataHelper = new DbHelper(activity);
    }

    public static void openDatabase() {
        dataHelper = new DbHelper(EShaktiApplication.getInstance());
        dataHelper.onOpen(database);
        database = dataHelper.getWritableDatabase();
    }

    public static void closeDatabase() {
        if (database != null && database.isOpen()) {
            database.close();
        }
    }

    public static void insertFPData(EnrollFP memDto) {
        try {
            openDatabase();
            ContentValues values = new ContentValues();
            values.put(TableConstants.MEMBER_ID, (memDto.getId() != null && memDto.getId().length() > 0) ? memDto.getId() : "");
            values.put(TableConstants.MEMBER_NAME, (memDto.getName() != null && memDto.getName().length() > 0) ? memDto.getName() : "");
            values.put(TableConstants.MEM_DESIGNATION, (memDto.getDesignation() != null && memDto.getDesignation().length() > 0) ? memDto.getDesignation() : "");
            values.put(TableConstants.USERID, (memDto.getUserId() != null && memDto.getUserId().length() > 0) ? memDto.getUserId() : "");
            values.put(TableConstants.MEM_FINGER1, (memDto.getFinger1() != null && memDto.getFinger1().length() > 0) ? memDto.getFinger1() : "");
            values.put(TableConstants.MEM_FINGER2, (memDto.getFinger2() != null && memDto.getFinger2().length() > 0) ? memDto.getFinger2() : "");
            values.put(TableConstants.SHG_ID, (memDto.getShgId() != null && memDto.getShgId().length() > 0) ? memDto.getShgId() : "");
            database.insertWithOnConflict(TableName.TABLE_ENROLL_MEMBER_FP_DATA, TableConstants.MEMBER_ID, values, SQLiteDatabase.CONFLICT_REPLACE);
        } catch (Exception e) {
            Log.e("insertEnergyException", e.toString());
        } finally {
            closeDatabase();
        }
    }


    public static void insertENROLLMEMBER(List<ResponseContents> memDto) {
        try {
            openDatabase();
            for (int i = 0; i < memDto.size(); i++) {
                ContentValues values = new ContentValues();
                values.put(TableConstants.MEMBER_ID, (memDto.get(i).getId() != null && memDto.get(i).getId().length() > 0) ? memDto.get(i).getId() : "");
                values.put(TableConstants.MEMBER_NAME, (memDto.get(i).getName() != null &&  memDto.get(i).getName().length() > 0) ?  memDto.get(i).getName() : "");
                values.put(TableConstants.MEM_DESIGNATION, (memDto.get(i).getDesignation() != null &&  memDto.get(i).getDesignation().length() > 0) ?  memDto.get(i).getDesignation() : "");
                values.put(TableConstants.USERID, (memDto.get(i).getUserId() != null &&  memDto.get(i).getUserId().length() > 0) ?  memDto.get(i).getUserId() : "");
                values.put(TableConstants.SHG_ID, (memDto.get(i).getShgId() != null &&  memDto.get(i).getShgId().length() > 0) ?  memDto.get(i).getShgId() : "");
                database.insertWithOnConflict(TableName.TABLE_ENROLL_MEMBER_FP_DATA, TableConstants.MEMBER_ID, values, SQLiteDatabase.CONFLICT_REPLACE);
            }
        } catch (Exception e) {
            Log.e("insertEnergyException", e.toString());
        } finally {
            closeDatabase();
        }
    }


    public static List<EnrollFP> getEnrollFpList(String shg_id, String memid) {
        List<EnrollFP> enrollFPList = new ArrayList<>();
        try {
            openDatabase();
            String selectQuery = "SELECT * FROM " + TableName.TABLE_ENROLL_MEMBER_FP_DATA + " where " + TableConstants.SHG_ID + " LIKE '" + shg_id + "' AND " + TableConstants.MEMBER_ID + " LIKE '" + memid + "'";
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    EnrollFP enrollFP = new EnrollFP();
                    enrollFP.setId(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_ID)));
                    enrollFP.setName(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_NAME)));
                    enrollFP.setDesignation(cursor.getString(cursor.getColumnIndex(TableConstants.MEM_DESIGNATION)));
                    enrollFP.setFinger1(cursor.getString(cursor.getColumnIndex(TableConstants.MEM_FINGER1)));
                    enrollFP.setFinger1(cursor.getString(cursor.getColumnIndex(TableConstants.MEM_FINGER2)));
                    enrollFP.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    enrollFPList.add(enrollFP);
                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

        return enrollFPList;
    }

    public static List<EnrollFP> getAllList() {
        List<EnrollFP> enrollFPList = new ArrayList<>();
        try {
            openDatabase();
            String selectQuery = "SELECT * FROM " + TableName.TABLE_ENROLL_MEMBER_FP_DATA;
            Log.e("TABLE_SHG QUERY:", selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    EnrollFP enrollFP = new EnrollFP();
                    enrollFP.setId(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_ID)));
                    enrollFP.setName(cursor.getString(cursor.getColumnIndex(TableConstants.MEMBER_NAME)));
                    enrollFP.setDesignation(cursor.getString(cursor.getColumnIndex(TableConstants.MEM_DESIGNATION)));
                    enrollFP.setFinger1(cursor.getString(cursor.getColumnIndex(TableConstants.MEM_FINGER1)));
                    enrollFP.setFinger1(cursor.getString(cursor.getColumnIndex(TableConstants.MEM_FINGER2)));
                    enrollFP.setShgId(cursor.getString(cursor.getColumnIndex(TableConstants.SHG_ID)));
                    enrollFPList.add(enrollFP);
                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDatabase();
        }

        return enrollFPList;
    }


}
