package com.oasys.eshakti.digitization.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.Dto.MemberList;
import com.oasys.eshakti.digitization.Dto.ShgBankDetails;
import com.oasys.eshakti.digitization.EShaktiApplication;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.GetSpanText;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.database.BankTable;
import com.oasys.eshakti.digitization.database.MemberTable;
import com.oasys.eshakti.digitization.database.SHGTable;
import com.oasys.eshakti.digitization.views.Get_EdiText_Filter;
import com.oasys.eshakti.digitization.views.RaisedButton;
import com.oasys.eshakti.digitization.views.TextviewUtils;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell on 15 Dec, 2018.
 */

public class LD_EL_Loan_SB_disbursement extends Fragment implements View.OnClickListener, NewTaskListener {

    public static String TAG = LD_EL_Loan_SB_disbursement.class.getSimpleName();
    private TextView mGroupName, mCashinHand, mCashatBank, mHeader, mAutoFill_label;
    private CheckBox mAutoFill;
    private Button mSubmit_Raised_Button;
    private TableLayout mIncomeTable;
    private EditText mIncome_values;
    private static List<EditText> sIncomeFields = new ArrayList<EditText>();
    private static String sIncomeAmounts[];
    String[] confirmArr;
    String nullVlaue = "0";
    public static String sSendToServer_InternalLoanDisbursement;
    public static int sIncome_Total;

    Dialog confirmationDialog;
    private RaisedButton mEdit_RaisedButton, mOk_RaisedButton;
    private Dialog mProgressDialog;

    boolean isNaviMain = false;
    boolean isServiceCall = false;
    String mLastTrDate = null, mLastTr_ID = null;
    public static String mMemberOS_Offlineresponse = null;
    public static String[] response;
    public static String mMemberOsValues = null;
    boolean isLastTransactionValues = false;

    String ToLoanAccNo = "", outstandingAmt = "";
    ArrayList<String> mLoanBankName = new ArrayList<String>();
    ArrayList<String> mLoanName = new ArrayList<String>();
    ArrayList<String> mLoanId = new ArrayList<String>();
    ArrayList<String> mLoanOutstanding = new ArrayList<String>();
    ArrayList<String> mLoanAccNo = new ArrayList<String>();
    int mCount = 0;
    LinearLayout mMemberNameLayout;
    TextView mMemberName;
    private View rootView;


    private int mSize;
    private List<MemberList> memList;
    private ListOfShg shgDto;
    private NetworkConnection networkConnection;
    private ArrayList<ShgBankDetails> bankdetails;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_loan_from_loanacc_disburse, container, false);
        return rootView;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mSize = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, "")).size();
        memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        bankdetails = BankTable.getBankName(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        init();

    }

    private void init() {
        try {
            mGroupName = (TextView) rootView.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName()+" / "+shgDto.getPresidentName());

            mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
            mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());

            mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
            mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());

            mMemberNameLayout = (LinearLayout) rootView.findViewById(R.id.member_name_layout);
            mMemberName = (TextView) rootView.findViewById(R.id.member_name);

            /** UI Mapping **/

            mHeader = (TextView) rootView.findViewById(R.id.internal_fragmentHeader);

            mHeader.setText("");// AppStrings.mInternalTypeLoan));

            mAutoFill_label = (TextView) rootView.findViewById(R.id.internal_autofillLabel);
            mAutoFill_label.setText(AppStrings.autoFill);
            mAutoFill_label.setVisibility(View.VISIBLE);

            mAutoFill = (CheckBox) rootView.findViewById(R.id.internal_autoFill);
            mAutoFill.setVisibility(View.VISIBLE);
            mAutoFill.setOnClickListener(this);


            Log.d(TAG, String.valueOf(mSize));

            TableLayout headerTable = (TableLayout) rootView.findViewById(R.id.internal_savingsTable);

            mIncomeTable = (TableLayout) rootView.findViewById(R.id.internal_fragment_contentTable);

            TableRow savingsHeader = new TableRow(getActivity());
            savingsHeader.setBackgroundResource(R.color.tableHeader);

            ViewGroup.LayoutParams headerParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,
                    1f);

            TextView mMemberName_headerText = new TextView(getActivity());
            mMemberName_headerText
                    .setText(String.valueOf(AppStrings.memberName));
            mMemberName_headerText.setTextColor(Color.WHITE);
            mMemberName_headerText.setPadding(20, 5, 10, 5);
            mMemberName_headerText.setLayoutParams(headerParams);
            savingsHeader.addView(mMemberName_headerText);

            TextView mIncomeAmount_HeaderText = new TextView(getActivity());
            mIncomeAmount_HeaderText
                    .setText(String.valueOf(AppStrings.amount));
            mIncomeAmount_HeaderText.setTextColor(Color.WHITE);
            mIncomeAmount_HeaderText.setPadding(10, 5, 40, 5);
            mIncomeAmount_HeaderText.setLayoutParams(headerParams);
            mIncomeAmount_HeaderText.setBackgroundResource(R.color.tableHeader);
            savingsHeader.addView(mIncomeAmount_HeaderText);

            headerTable.addView(savingsHeader,
                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            for (int i = 0; i < mSize; i++) {

                TableRow indv_IncomeRow = new TableRow(getActivity());

                TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                contentParams.setMargins(10, 5, 10, 5);

                final TextView memberName_Text = new TextView(getActivity());
                memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
                        String.valueOf(memList.get(i).getMemberName())));
                memberName_Text.setTextColor(R.color.black);
                memberName_Text.setPadding(10, 0, 10, 5);
                memberName_Text.setLayoutParams(contentParams);
                memberName_Text.setWidth(200);
                memberName_Text.setSingleLine(true);
                memberName_Text.setEllipsize(TextUtils.TruncateAt.END);
                indv_IncomeRow.addView(memberName_Text);

                TableRow.LayoutParams contentEditParams = new TableRow.LayoutParams(150, ViewGroup.LayoutParams.WRAP_CONTENT);
                contentEditParams.setMargins(30, 5, 100, 5);

                mIncome_values = new EditText(getActivity());

                mIncome_values.setId(i);
                sIncomeFields.add(mIncome_values);
                mIncome_values.setGravity(Gravity.END);
                mIncome_values.setTextColor(Color.BLACK);
                mIncome_values.setPadding(5, 5, 5, 5);
                mIncome_values.setBackgroundResource(R.drawable.edittext_background);
                mIncome_values.setLayoutParams(contentEditParams);// contentParams
                // lParams
                mIncome_values.setTextAppearance(getActivity(), R.style.MyMaterialTheme);
                mIncome_values.setFilters(Get_EdiText_Filter.editText_filter());
                mIncome_values.setInputType(InputType.TYPE_CLASS_NUMBER);
                mIncome_values.setTextColor(R.color.black);
                mIncome_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        // TODO Auto-generated method stub
                        if (hasFocus) {
                            ((EditText) v).setGravity(Gravity.LEFT);
                            mMemberNameLayout.setVisibility(View.VISIBLE);
                            mMemberName.setText(memberName_Text.getText().toString().trim());
                            TextviewUtils.manageBlinkEffect(mMemberName, getActivity());
                        } else {
                            ((EditText) v).setGravity(Gravity.RIGHT);
                            mMemberNameLayout.setVisibility(View.GONE);
                            mMemberName.setText("");
                        }

                    }
                });
                indv_IncomeRow.addView(mIncome_values);

                mIncomeTable.addView(indv_IncomeRow,
                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            }

            mSubmit_Raised_Button = (Button) rootView.findViewById(R.id.internal_fragment_Submit_button);
            mSubmit_Raised_Button.setText(AppStrings.submit);
            mSubmit_Raised_Button.setOnClickListener(this);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        sIncomeAmounts = new String[sIncomeFields.size()];
        Log.d(TAG, "sIncomeAmounts size : " + sIncomeAmounts.length);
        switch (v.getId()) {
            case R.id.internal_fragment_Submit_button:
                try {
                    // isIncome = true;
                    sSendToServer_InternalLoanDisbursement = "";
                    sIncome_Total = 0;
                    confirmArr = new String[mSize];
                    StringBuilder builder = new StringBuilder();

                    for (int i = 0; i < sIncomeAmounts.length; i++) {

                        sIncomeAmounts[i] = String.valueOf(sIncomeFields.get(i).getText());
                        Log.d(TAG, "Entered Values : " + sIncomeAmounts[i] + " POS : " + i);

                        if ((sIncomeAmounts[i].equals("")) || (sIncomeAmounts[i] == null)) {
                            sIncomeAmounts[i] = nullVlaue;
                        }

                        if (sIncomeAmounts[i].matches("\\d*\\.?\\d+")) { // match a
                            // decimal
                            // number

                            int amount = (int) Math.round(Double.parseDouble(sIncomeAmounts[i]));
                            sIncomeAmounts[i] = String.valueOf(amount);
                        }

                        sSendToServer_InternalLoanDisbursement = sSendToServer_InternalLoanDisbursement
                                + String.valueOf(memList.get(i).getMemberId()) + "~" + sIncomeAmounts[i] + "~";

                        confirmArr[i] = sIncomeAmounts[i];

                        sIncome_Total = sIncome_Total + Integer.parseInt(sIncomeAmounts[i]);

                        builder.append(sIncomeAmounts[i]).append(",");
                    }

                    Log.d(TAG, sSendToServer_InternalLoanDisbursement);

                    Log.d(TAG, "TOTAL " + String.valueOf(sIncome_Total));

                    if (sIncome_Total != 0) {
                        if (EShaktiApplication.getLoanDisburseValues().equals("SBACC")) {

                            if (sIncome_Total <= Integer.parseInt(EShaktiApplication.getSBAccBalanceAmount())) {
                                if (EShaktiApplication.getSelectedType().equals("CASH")) {


                                    showConfirmationDialog();

                                } else if (EShaktiApplication.getSelectedType().equals("BANK")) {


                                    showConfirmationDialog();

                                }

                            } else {
                                TastyToast.makeText(getActivity(), AppStrings.mCheckbalanceAlert, TastyToast.LENGTH_SHORT,
                                        TastyToast.WARNING);
                            }

                        } else if (EShaktiApplication.getLoanDisburseValues().equals("LOANACC")) {

                                showConfirmationDialog();

                        } else if (EShaktiApplication.getLoanDisburseValues().equals("REPAID")) {

                                showConfirmationDialog();

                        }

                    } else {
                        TastyToast.makeText(getActivity(), AppStrings.nullDetailsAlert, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);

                        sSendToServer_InternalLoanDisbursement = "0";
                        sIncome_Total = 0;

                    }
                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }

                break;

            case R.id.internal_autoFill:

                String similiar_Income;
                // isValues = true;

                if (mAutoFill.isChecked()) {

                    try {

                        // Makes all edit fields holds the same savings
                        similiar_Income = sIncomeFields.get(0).getText().toString();

                        for (int i = 0; i < sIncomeAmounts.length; i++) {
                            sIncomeFields.get(i).setText(similiar_Income);
                            sIncomeFields.get(i).setGravity(Gravity.RIGHT);
                            sIncomeFields.get(i).clearFocus();
                            sIncomeAmounts[i] = similiar_Income;

                        }

                        /** To clear the values of EditFields in case of uncheck **/

                    } catch (ArrayIndexOutOfBoundsException e) {
                        e.printStackTrace();

                        TastyToast.makeText(getActivity(), AppStrings.adminAlert, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);
                    }
                }
                break;

            case R.id.fragment_Edit_button:

                sSendToServer_InternalLoanDisbursement = "0";
                sIncome_Total = Integer.valueOf(nullVlaue);
                mSubmit_Raised_Button.setClickable(true);
                isServiceCall = false;
                confirmationDialog.dismiss();
                break;

            case R.id.fragment_Ok_button:


                break;

        }
    }

    @Override
    public void onTaskStarted() {
        mProgressDialog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDialog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {

    }


    private void showConfirmationDialog() {
        // TODO Auto-generated method stub
        confirmationDialog = new Dialog(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_confirmation, null);
        dialogView.setLayoutParams(
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
        confirmationHeader.setText(AppStrings.confirmation);

        TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

        for (int i = 0; i < confirmArr.length; i++) {

            TableRow indv_SavingsRow = new TableRow(getActivity());

            @SuppressWarnings("deprecation")
            TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            contentParams.setMargins(10, 5, 10, 5);

            TextView memberName_Text = new TextView(getActivity());
            memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
                    String.valueOf(memList.get(i).getMemberName())));
            memberName_Text.setTextColor(R.color.black);
            memberName_Text.setPadding(5, 5, 5, 5);
            memberName_Text.setLayoutParams(contentParams);
            indv_SavingsRow.addView(memberName_Text);

            TextView confirm_values = new TextView(getActivity());
            confirm_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sIncomeAmounts[i])));
            confirm_values.setTextColor(R.color.black);
            confirm_values.setPadding(5, 5, 5, 5);
            confirm_values.setGravity(Gravity.RIGHT);
            confirm_values.setLayoutParams(contentParams);
            indv_SavingsRow.addView(confirm_values);

            confirmationTable.addView(indv_SavingsRow,
                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        }

        mEdit_RaisedButton = (RaisedButton) dialogView.findViewById(R.id.fragment_Edit_button);
        mEdit_RaisedButton.setText(AppStrings.edit);
        mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
        // 205,
        // 0));
        mEdit_RaisedButton.setOnClickListener(this);

        mOk_RaisedButton = (RaisedButton) dialogView.findViewById(R.id.fragment_Ok_button);
        mOk_RaisedButton.setText(AppStrings.yes);
        mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
        mOk_RaisedButton.setOnClickListener(this);

        confirmationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmationDialog.setCanceledOnTouchOutside(false);
        confirmationDialog.setContentView(dialogView);
        confirmationDialog.setCancelable(true);
        confirmationDialog.show();

        ViewGroup.MarginLayoutParams margin = (ViewGroup.MarginLayoutParams) dialogView.getLayoutParams();
        margin.leftMargin = 10;
        margin.rightMargin = 10;
        margin.topMargin = 10;
        margin.bottomMargin = 10;
        margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);

    }


}
