package com.oasys.eshakti.digitization.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.Dto.LoanDto;
import com.oasys.eshakti.digitization.Dto.MemberList;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.Dto.ShgBankDetails;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.GetSpanText;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.activity.LoginActivity;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;
import com.oasys.eshakti.digitization.database.BankTable;
import com.oasys.eshakti.digitization.database.MemberTable;
import com.oasys.eshakti.digitization.database.SHGTable;
import com.oasys.eshakti.digitization.model.InternalBankMFIModel;
import com.oasys.eshakti.digitization.views.Get_EdiText_Filter;
import com.oasys.eshakti.digitization.views.RaisedButton;
import com.oasys.eshakti.digitization.views.TextviewUtils;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;

import java.util.ArrayList;
import java.util.List;


public class LD_IL_Mem_Disburse extends Fragment implements View.OnClickListener, NewTaskListener {


    private View rootView;
    private TextView mGroupName, mCashinHand, mCashatBank, mHeader, mAutoFill_label;
    private CheckBox mAutoFill;
    private Button mSubmit_Raised_Button;
    private TableLayout mIncomeTable;
    private EditText mIncome_values;
    private static List<EditText> sIncomeFields = new ArrayList<EditText>();
    private static String sIncomeAmounts[];
    String[] confirmArr;
    String nullVlaue = "0";
    public static String sSendToServer_InternalLoanDisbursement;
    public static int sIncome_Total;

    Dialog confirmationDialog;
    private RaisedButton mEdit_RaisedButton, mOk_RaisedButton;
    private Dialog mProgressDialog;

    boolean isNaviMain = false;
    boolean isServiceCall = false;

    LinearLayout mMemberNameLayout;
    TextView mMemberName;

    private int mSize;
    private List<MemberList> memList;
    ArrayList<MemberList> memLoanDisbursement = new ArrayList<>();
    private ListOfShg shgDto;
    private NetworkConnection networkConnection;
    private ArrayList<LoanDto> arrLoanType;
    private LoanDto ldto = new LoanDto();
    private ArrayList<ShgBankDetails> bankdetails;
    private Dialog mProgressDilaog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_new_internalloan_disbursement, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mSize = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, "")).size();
        memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        bankdetails = BankTable.getBankName(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        arrLoanType = new ArrayList<>();
        sIncomeFields = new ArrayList<EditText>();
        init();
    }

    private void init() {
        try {
            mGroupName = (TextView) rootView.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName()+" / "+shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashinHand = (TextView) rootView.findViewById(R.id.cashinHand);
            mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashinHand.setTypeface(LoginActivity.sTypeface);

            mCashatBank = (TextView) rootView.findViewById(R.id.cashatBank);
            mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashatBank.setTypeface(LoginActivity.sTypeface);

            mMemberNameLayout = (LinearLayout) rootView.findViewById(R.id.member_name_layout);
            mMemberName = (TextView) rootView.findViewById(R.id.member_name);
         //   mMemberName.setTypeface(LoginActivity.sTypeface);

            /** UI Mapping **/

            mHeader = (TextView) rootView.findViewById(R.id.internal_fragmentHeader);
            // mHeader.setText(AppStrings.mInternalTypeLoan));
            mHeader.setText(InternalBankMFIModel.getLoanName());
            mHeader.setTypeface(LoginActivity.sTypeface);

            mAutoFill_label = (TextView) rootView.findViewById(R.id.internal_autofillLabel);
            mAutoFill_label.setText(AppStrings.autoFill);
            mAutoFill_label.setTypeface(LoginActivity.sTypeface);
            mAutoFill_label.setVisibility(View.VISIBLE);

            mAutoFill = (CheckBox) rootView.findViewById(R.id.autoFillCheck);
            mAutoFill.setVisibility(View.VISIBLE);
            mAutoFill.setOnClickListener(this);


            TableLayout headerTable = (TableLayout) rootView.findViewById(R.id.internal_savingsTable);

            mIncomeTable = (TableLayout) rootView.findViewById(R.id.internal_fragment_contentTable);

            TableRow savingsHeader = new TableRow(getActivity());
            savingsHeader.setBackgroundResource(R.color.tableHeader);

            ViewGroup.LayoutParams headerParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,
                    1f);

            TextView mMemberName_headerText = new TextView(getActivity());
            mMemberName_headerText
                    .setText(String.valueOf(AppStrings.memberName));
            mMemberName_headerText.setTypeface(LoginActivity.sTypeface);
            mMemberName_headerText.setTextColor(Color.WHITE);
            mMemberName_headerText.setPadding(20, 5, 10, 5);
            mMemberName_headerText.setLayoutParams(headerParams);
            savingsHeader.addView(mMemberName_headerText);

            TextView mIncomeAmount_HeaderText = new TextView(getActivity());
            mIncomeAmount_HeaderText
                    .setText(String.valueOf(AppStrings.amount));
            mIncomeAmount_HeaderText.setTypeface(LoginActivity.sTypeface);
            mIncomeAmount_HeaderText.setTextColor(Color.WHITE);
            mIncomeAmount_HeaderText.setPadding(10, 5, 40, 5);
            mIncomeAmount_HeaderText.setLayoutParams(headerParams);
            mIncomeAmount_HeaderText.setBackgroundResource(R.color.tableHeader);
            savingsHeader.addView(mIncomeAmount_HeaderText);

            headerTable.addView(savingsHeader,
                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            for (int i = 0; i < mSize; i++) {

                TableRow indv_IncomeRow = new TableRow(getActivity());

                TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                contentParams.setMargins(10, 5, 10, 5);

                final TextView memberName_Text = new TextView(getActivity());
                memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
                        String.valueOf(memList.get(i).getMemberName())));
                memberName_Text.setTypeface(LoginActivity.sTypeface);
                memberName_Text.setTextColor(R.color.black);
                memberName_Text.setPadding(10, 0, 10, 5);
                memberName_Text.setLayoutParams(contentParams);
                memberName_Text.setWidth(200);
                memberName_Text.setSingleLine(true);
                memberName_Text.setEllipsize(TextUtils.TruncateAt.END);
                indv_IncomeRow.addView(memberName_Text);

                TableRow.LayoutParams contentEditParams = new TableRow.LayoutParams(150, ViewGroup.LayoutParams.WRAP_CONTENT);
                contentEditParams.setMargins(30, 5, 100, 5);

                mIncome_values = new EditText(getActivity());

                mIncome_values.setId(i);
                sIncomeFields.add(mIncome_values);
                mIncome_values.setGravity(Gravity.END);
                mIncome_values.setTextColor(Color.BLACK);
                mIncome_values.setPadding(5, 5, 5, 5);
                mIncome_values.setBackgroundResource(R.drawable.edittext_background);
                mIncome_values.setLayoutParams(contentEditParams);// contentParams
                // lParams
                mIncome_values.setTextAppearance(getActivity(), R.style.MyMaterialTheme);
                mIncome_values.setFilters(Get_EdiText_Filter.editText_filter());
                mIncome_values.setInputType(InputType.TYPE_CLASS_NUMBER);
                mIncome_values.setTextColor(R.color.black);
                mIncome_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        // TODO Auto-generated method stub
                        if (hasFocus) {
                            ((EditText) v).setGravity(Gravity.LEFT);

                            mMemberNameLayout.setVisibility(View.VISIBLE);
                            mMemberName.setText(memberName_Text.getText().toString().trim());
                            TextviewUtils.manageBlinkEffect(mMemberName, getActivity());
                        } else {
                            ((EditText) v).setGravity(Gravity.RIGHT);

                            mMemberNameLayout.setVisibility(View.GONE);
                            mMemberName.setText("");
                        }

                    }
                });
                indv_IncomeRow.addView(mIncome_values);

                mIncomeTable.addView(indv_IncomeRow,
                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            }

            mSubmit_Raised_Button = (Button) rootView.findViewById(R.id.internal_fragment_Submit_button);
            mSubmit_Raised_Button.setText(AppStrings.submit);
            mSubmit_Raised_Button.setOnClickListener(this);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        sIncomeAmounts = new String[sIncomeFields.size()];
        switch (view.getId()) {
            case R.id.internal_fragment_Submit_button:

                try {
                    // isIncome = true;

                    sSendToServer_InternalLoanDisbursement = "";
                    sIncome_Total = 0;

                    confirmArr = new String[mSize];

                    StringBuilder builder = new StringBuilder();

                    if (memLoanDisbursement != null && memLoanDisbursement.size() > 0) {
                        memLoanDisbursement.clear();
                    }

                    for (int i = 0; i < mSize; i++) {

                        sIncomeAmounts[i] = String.valueOf(sIncomeFields.get(i).getText());

                        if ((sIncomeAmounts[i].equals("")) || (sIncomeAmounts[i] == null)) {
                            sIncomeAmounts[i] = nullVlaue;
                        }

                        if (sIncomeAmounts[i].matches("\\d*\\.?\\d+")) { // match
                            // a
                            // decimal
                            // number

                            int amount = (int) Math.round(Double.parseDouble(sIncomeAmounts[i]));
                            sIncomeAmounts[i] = String.valueOf(amount);
                        }


                      /*  sSendToServer_InternalLoanDisbursement = sSendToServer_InternalLoanDisbursement
                                + String.valueOf(memList.get(i).getMemberId()) + "~" + sIncomeAmounts[i] + "~";

                        confirmArr[i] = String.valueOf(memList.get(i).getMemberName()) + " "
                                + sIncomeAmounts[i];*/
                        MemberList memeber = new MemberList();
                        memeber.setMemberId(memList.get(i).getMemberId());
                        memeber.setLoanAmount(sIncomeAmounts[i]);
                        memLoanDisbursement.add(memeber);
                        sIncome_Total = sIncome_Total + Integer.parseInt(sIncomeAmounts[i]);

                        builder.append(sIncomeAmounts[i]).append(",");
                    }


                    //    InternalBankMFIModel.setValues(sSendToServer_InternalLoanDisbursement);

                    int mCheckValues = Integer.parseInt(InternalBankMFIModel.getLoan_Disbursement_Amount());

                    // Do the SP insertion

                    if (sIncome_Total <= mCheckValues) {

                        confirmationDialog = new Dialog(getActivity());

                        LayoutInflater inflater = getActivity().getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.dialog_new_confirmation, null);
                        dialogView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT));

                        TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
                        confirmationHeader.setText(AppStrings.confirmation);
                        confirmationHeader.setTypeface(LoginActivity.sTypeface);

                        TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

                        for (int i = 0; i < confirmArr.length; i++) {

                            TableRow indv_SavingsRow = new TableRow(getActivity());

                            @SuppressWarnings("deprecation")
                            TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                            contentParams.setMargins(10, 5, 10, 5);

                            TextView memberName_Text = new TextView(getActivity());
                            memberName_Text.setText(GetSpanText.getSpanString(getActivity(),
                                    String.valueOf(memList.get(i).getMemberName())));
                            memberName_Text.setTextColor(R.color.black);
                            memberName_Text.setPadding(5, 5, 5, 5);
                            memberName_Text.setLayoutParams(contentParams);
                            indv_SavingsRow.addView(memberName_Text);

                            TextView confirm_values = new TextView(getActivity());
                            confirm_values
                                    .setText(GetSpanText.getSpanString(getActivity(), String.valueOf((sIncomeAmounts[i] != null && sIncomeAmounts[i].length() > 0) ? sIncomeAmounts[i] : "0")));
                            confirm_values.setTextColor(R.color.black);
                            confirm_values.setPadding(5, 5, 5, 5);
                            confirm_values.setGravity(Gravity.RIGHT);
                            confirm_values.setLayoutParams(contentParams);
                            indv_SavingsRow.addView(confirm_values);

                            confirmationTable.addView(indv_SavingsRow,
                                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                        }

                        if (LD_InternalLoan.sMenuSelection.equals("BANK LOAN")) {
                            if (InternalBankMFIModel.getSubsidy_Amount().equals("0")
                                    && InternalBankMFIModel.getSubsidy_Reserve_Fund().equals("0")) {

                                int totalValue = sIncome_Total;

                                View rullerView1 = new View(getActivity());
                                rullerView1
                                        .setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
                                rullerView1.setBackgroundColor(Color.rgb(0, 199, 140));// rgb(255,
                                // 229,
                                // 242));
                                confirmationTable.addView(rullerView1);

                                TableRow totalRow = new TableRow(getActivity());

                                @SuppressWarnings("deprecation")
                                TableRow.LayoutParams totalParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                                totalParams.setMargins(10, 5, 10, 5);

                                TextView totalText = new TextView(getActivity());
                                totalText.setText(
                                        GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.total)));
                                totalText.setTypeface(LoginActivity.sTypeface);
                                totalText.setTextColor(R.color.black);
                                totalText.setPadding(5, 5, 5, 5);// (5, 10,
                                // 5,
                                // 10);
                                totalText.setLayoutParams(totalParams);
                                totalRow.addView(totalText);

                                TextView totalAmount = new TextView(getActivity());
                                totalAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(totalValue)));// SavingsFragment.sSavings_Total
                                totalAmount.setTextColor(R.color.black);
                                totalAmount.setPadding(5, 5, 5, 5);// (5,
                                // 10,
                                // 100,
                                // 10);
                                totalAmount.setGravity(Gravity.RIGHT);
                                totalAmount.setLayoutParams(totalParams);
                                totalRow.addView(totalAmount);

                                confirmationTable.addView(totalRow,
                                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                            } else {
                                View rullerView1 = new View(getActivity());
                                rullerView1
                                        .setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
                                rullerView1.setBackgroundColor(Color.rgb(0, 199, 140));// rgb(255,
                                // 229,
                                // 242));
                                confirmationTable.addView(rullerView1);

                                TableRow totalRow = new TableRow(getActivity());

                                @SuppressWarnings("deprecation")
                                TableRow.LayoutParams totalParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                                totalParams.setMargins(10, 5, 10, 5);

                                TextView totalText = new TextView(getActivity());
                                totalText.setText(
                                        GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.total)));
                                totalText.setTypeface(LoginActivity.sTypeface);
                                totalText.setTextColor(R.color.black);
                                totalText.setPadding(5, 5, 5, 5);// (5, 10,
                                // 5,
                                // 10);
                                totalText.setLayoutParams(totalParams);
                                totalRow.addView(totalText);

                                TextView totalAmount = new TextView(getActivity());
                                totalAmount
                                        .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sIncome_Total)));// SavingsFragment.sSavings_Total
                                totalAmount.setTextColor(R.color.black);
                                totalAmount.setPadding(5, 5, 5, 5);// (5,
                                // 10,
                                // 100,
                                // 10);
                                totalAmount.setGravity(Gravity.RIGHT);
                                totalAmount.setLayoutParams(totalParams);
                                totalRow.addView(totalAmount);

                                confirmationTable.addView(totalRow,
                                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                            }
                        } else if (!LD_InternalLoan.sMenuSelection.equals("BANK LOAN")) {

                            View rullerView1 = new View(getActivity());
                            rullerView1.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
                            rullerView1.setBackgroundColor(Color.rgb(0, 199, 140));// rgb(255,
                            // 229,
                            // 242));
                            confirmationTable.addView(rullerView1);

                            TableRow totalRow = new TableRow(getActivity());

                            @SuppressWarnings("deprecation")
                            TableRow.LayoutParams totalParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                            totalParams.setMargins(10, 5, 10, 5);

                            TextView totalText = new TextView(getActivity());
                            totalText.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.total)));
                            totalText.setTypeface(LoginActivity.sTypeface);
                            totalText.setTextColor(R.color.black);
                            totalText.setPadding(5, 5, 5, 5);// (5, 10, 5,
                            // 10);
                            totalText.setLayoutParams(totalParams);
                            totalRow.addView(totalText);

                            TextView totalAmount = new TextView(getActivity());
                            totalAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sIncome_Total)));// SavingsFragment.sSavings_Total
                            totalAmount.setTextColor(R.color.black);
                            totalAmount.setPadding(5, 5, 5, 5);// (5, 10,
                            // 100,
                            // 10);
                            totalAmount.setGravity(Gravity.RIGHT);
                            totalAmount.setLayoutParams(totalParams);
                            totalRow.addView(totalAmount);

                            confirmationTable.addView(totalRow,
                                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                        }

                        mEdit_RaisedButton = (RaisedButton) dialogView.findViewById(R.id.fragment_Edit);
                        mEdit_RaisedButton.setText(AppStrings.edit);
                        mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
                        mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
                        // 205,
                        // 0));
                        mEdit_RaisedButton.setOnClickListener(this);

                        mOk_RaisedButton = (RaisedButton) dialogView.findViewById(R.id.frag_Ok);
                        mOk_RaisedButton.setText(AppStrings.yes);
                        mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
                        mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
                        mOk_RaisedButton.setOnClickListener(this);

                        confirmationDialog.getWindow()
                                .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        confirmationDialog.setCanceledOnTouchOutside(false);
                        confirmationDialog.setContentView(dialogView);
                        confirmationDialog.setCancelable(true);
                        confirmationDialog.show();

                        ViewGroup.MarginLayoutParams margin = (ViewGroup.MarginLayoutParams) dialogView.getLayoutParams();
                        margin.leftMargin = 10;
                        margin.rightMargin = 10;
                        margin.topMargin = 10;
                        margin.bottomMargin = 10;
                        margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);

                    } else {
                        TastyToast.makeText(getActivity(), AppStrings.mLoanSanc_loanDist, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);

                        sSendToServer_InternalLoanDisbursement = "0";
                        sIncome_Total = 0;

                    }

                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }

                break;

            case R.id.autoFillCheck:

                String similiar_Income;
                // isValues = true;

                if (mAutoFill.isChecked()) {

                    try {
                        // Makes all edit fields holds the same savings
                        similiar_Income = sIncomeFields.get(0).getText().toString();

                        for (int i = 0; i < sIncomeAmounts.length; i++) {
                            sIncomeFields.get(i).setText(similiar_Income);
                            sIncomeFields.get(i).setGravity(Gravity.RIGHT);
                            sIncomeFields.get(i).clearFocus();
                            sIncomeAmounts[i] = similiar_Income;
                        }
                        /** To clear the values of EditFields in case of uncheck **/
                    } catch (ArrayIndexOutOfBoundsException e) {
                        e.printStackTrace();

                        TastyToast.makeText(getActivity(), AppStrings.adminAlert, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);
                    }
                }
                break;

            case R.id.fragment_Edit:


                confirmationDialog.dismiss();

                break;
            case R.id.frag_Ok:

                confirmationDialog.dismiss();
                if (InternalBankMFIModel.getType().equals("Cash"))
                    LD_IL_Bank_MFI_Fed_Entry.loanEntry.setModeOfCash("2");
                else {
                    LD_IL_Bank_MFI_Fed_Entry.loanEntry.setModeOfCash("1");
                }
                LD_IL_Bank_MFI_Fed_Entry.loanEntry.setLoanDisbursmentDetails(memLoanDisbursement);

                String sreqString = new Gson().toJson(LD_IL_Bank_MFI_Fed_Entry.loanEntry);

                if (LD_InternalLoan.sMenuSelection.equals("Bank Loan")) {
                    if (networkConnection.isNetworkAvailable()) {
                        onTaskStarted();
                        RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.LD_IL_BANK_DISB, sreqString, getActivity(), ServiceType.LD_IL_MEM_DISB);
                    }

                } else if (LD_InternalLoan.sMenuSelection.equals("MFI Loan")) {
                    if (networkConnection.isNetworkAvailable()) {
                        onTaskStarted();
                        RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.LD_IL_MFI_DISB, sreqString, getActivity(), ServiceType.LD_IL_MEM_DISB);
                    }

                } else {
                    if (networkConnection.isNetworkAvailable()) {
                        onTaskStarted();
                        RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.LD_IL_FED_DISB, sreqString, getActivity(), ServiceType.LD_IL_MEM_DISB);
                    }
                }

                break;
        }

    }

    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();
    }


    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        if (mProgressDilaog != null) {
            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                mProgressDilaog.dismiss();
            }
        }

        switch (serviceType) {
            case LD_IL_MEM_DISB:
                try {
                    ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                    String message = cdto.getMessage();
                    int statusCode = cdto.getStatusCode();
                    if (statusCode == Utils.Success_Code) {
                        Utils.showToast(getActivity(), message);
                        if (shgDto.getFFlag() == null || !shgDto.getFFlag().equals("1")) {
                            SHGTable.updateTransactionSHGDetails(shgDto.getShgId());
                        }
                        FragmentManager fm = getFragmentManager();
                        fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        MainFragment mainFragment =new MainFragment();
                        Bundle bundles = new Bundle();
                        bundles.putString("Transaction",MainFragment.Flag_Transaction);
                        mainFragment.setArguments(bundles);
                        NewDrawerScreen.showFragment(mainFragment);
//                        MemberDrawerScreen.showFragment(new MainFragment());

                    } else {

                        if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                        }
                        Utils.showToast(getActivity(), message);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;


        }


    }
}
