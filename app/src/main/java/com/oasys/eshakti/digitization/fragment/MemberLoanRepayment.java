package com.oasys.eshakti.digitization.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AlertDialog;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.oasys.eshakti.digitization.Dto.CashOfGroup;
import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.Dto.MemberList;
import com.oasys.eshakti.digitization.Dto.RequestDto.MemberloanRepayRequestDto;
import com.oasys.eshakti.digitization.Dto.RequestDto.RepaymentDetails;
import com.oasys.eshakti.digitization.Dto.ResponseContents;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.activity.LoginActivity;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;
import com.oasys.eshakti.digitization.database.MemberTable;
import com.oasys.eshakti.digitization.database.SHGTable;
import com.oasys.eshakti.digitization.views.CustomHorizontalScrollView;
import com.oasys.eshakti.digitization.views.CustomHorizontalScrollView.onScrollChangedListener;
import com.oasys.eshakti.digitization.views.Get_EdiText_Filter;
import com.oasys.eshakti.digitization.views.TextviewUtils;
import com.tutorialsee.lib.TastyToast;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MemberLoanRepayment extends Fragment implements View.OnClickListener, NewTaskListener {
    View view;
    private String loan_id = "";
    private String accountnumber = "";
    private String loantype = "";
    private String bankname = "";
    private TextView mloantype, mloannumber, mMemberName;
    private LinearLayout mMemberNameLayout;
    private TableLayout mLeftHeaderTable;
    private int mSize;
    private int responsesize;
    private TableLayout mRightHeaderTable;
    private TableLayout mLeftContentTable;
    private TableLayout mRightContentTable;
    private EditText mamount_values;
    private EditText mintrest_values;
    private CheckBox sCheckBox1;
    private TextView memberName_Text;
    private TextView moutstanding;
    private List<TextView> soutstandingFields;
    private List<EditText> samountFields;
    private List<EditText> sintrestFields;
    private static List<CheckBox> mCheckBoxFields;
    public String[] sAmountSavingsAmounts;
    public String[] sintrestSavingsAmount;
    private CustomHorizontalScrollView mHSRightHeader;
    private CustomHorizontalScrollView mHSRightContent;
    private Button mSubmit_Raised_Button;
    private Button mEdit_RaisedButton;
    private Button mOk_RaisedButton;
    private Button mPerviousButton;
    private Button mNextButton;
    private ArrayList<MemberList> arrMem;
    private ListOfShg shgDto;
    private Dialog mProgressDilaog;
    private NetworkConnection networkConnection;
    // private List<MemberList> memList;
    private ArrayList<RepaymentDetails> mlrlist;
    String width[] = {AppStrings.memberName, AppStrings.outstanding, AppStrings.savings, AppStrings.interest};
    int[] rightHeaderWidth = new int[width.length];
    int[] rightContentWidth = new int[width.length];
    ResponseDto responseDto;
    ArrayList<MemberList> outstandingamt;
    String nullAmount = "0";
    public static int sAmount_Total = 0, sIntrest_Total = 0;
    public static String sSendToServer_Amount, sSendToServer_Intrest;
    String nullVlaue = "0";
    String[] confirmArr;
    Dialog confirmationDialog;
    String shgId = "";
    private TextView mGroupName;
    private TextView mCashinHand;
    private TextView mCashatBank;
    private String flag = "0";
    private List<MemberList> memList;
    private ArrayList<CashOfGroup> csGrp;
    private int sum = 0;
    public static int sum_of_memberloan_repayment = 0;
    public static int finalsum_of_memberloan_repayment = 0;
    public static long timeout_saving;
    AlertDialog alertDialog;
    private TextView counterTextView;
    private TextView counterTimerStatusTxt;
    private CountDownTimer cndr;
    private static final String FORMAT_TIMER = "%02d:%02d";
    public ArrayList<ResponseContents> responseContentsList;


    public MemberLoanRepayment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_memberloanrepayment_update, container, false);
        try {
            Bundle bundle = getArguments();
            loan_id = bundle.getString("loan_id");
            accountnumber = bundle.getString("account_number");
            loantype = bundle.getString("loan_type");
            bankname = bundle.getString("bank_name");
            Log.d("LOANID", loan_id);

            if (bundle != null) {
                Attendance.flag = bundle.getString("stepwise");
//                Log.d("LOANID", flag);

                if (Attendance.flag == "1") {

                    loan_id = bundle.getString("loan_id");
                    accountnumber = bundle.getString("account_number");
                    loantype = bundle.getString("loan_type");
                    bankname = bundle.getString("bank_name");
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mSize = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, "")).size();
        //memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());

        if (Attendance.flag == "1") {
            if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {            //  onTaskStarted();
                RestClient.getRestClient(this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.NAV_DETAILS + shgDto.getId(), getActivity(), ServiceType.NAV_DETAILS);
            }

        }
        if (networkConnection.isNetworkAvailable()) {
            shgId = shgDto.getShgId();
            String url = Constants.BASE_URL + Constants.GETOUTSTANDINGAMOUNT + "?shgid=" + shgId + "&loantypeId=" + loan_id;
            RestClient.getRestClient(this).callWebServiceForGetMethod(url, getActivity(), ServiceType.GETOUTSTANDINGAMOUNT);
        }
        //  init();
    }

    public void init() {

        try {
            mGroupName = (TextView) view.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashinHand = (TextView) view.findViewById(R.id.cashinHand);
            mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashinHand.setTypeface(LoginActivity.sTypeface);

            mCashatBank = (TextView) view.findViewById(R.id.cashatBank);
            mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashatBank.setTypeface(LoginActivity.sTypeface);

            samountFields = new ArrayList<EditText>();
            sintrestFields = new ArrayList<EditText>();
            mCheckBoxFields = new ArrayList<CheckBox>();
            responseContentsList = new ArrayList<>();
            mloantype = (TextView) view.findViewById(R.id.mlr_loantype);
            mloannumber = (TextView) view.findViewById(R.id.mlr_loanAccNo);
            mMemberNameLayout = (LinearLayout) view.findViewById(R.id.mlr_member_name_layout);
            mMemberName = (TextView) view.findViewById(R.id.mlr_member_name);
            Log.d("memberdata", String.valueOf(mSize));
            mLeftHeaderTable = (TableLayout) view.findViewById(R.id.mlr_LeftHeaderTable);
            mRightHeaderTable = (TableLayout) view.findViewById(R.id.mlr_RightHeaderTable);
            mLeftContentTable = (TableLayout) view.findViewById(R.id.mlr_LeftContentTable);
            mRightContentTable = (TableLayout) view.findViewById(R.id.mlr_RightContentTable);

            mHSRightHeader = (CustomHorizontalScrollView) view.findViewById(R.id.mlr_rightHeaderHScrollView);
            mHSRightContent = (CustomHorizontalScrollView) view.findViewById(R.id.mlr_rightContentHScrollView);
            mloantype.setText(loantype + " - " + bankname);
            mloantype.setTypeface(LoginActivity.sTypeface);
            mloannumber.setText(AppStrings.Loan_Account_No + ":" + accountnumber);
            mloannumber.setTypeface(LoginActivity.sTypeface);
            mHSRightHeader.setOnScrollChangedListener(new onScrollChangedListener() {

                public void onScrollChanged(int l, int t, int oldl, int oldt) {
                    // TODO Auto-generated method stub

                    mHSRightContent.scrollTo(l, 0);

                }
            });

            mHSRightContent.setOnScrollChangedListener(new onScrollChangedListener() {
                @Override
                public void onScrollChanged(int l, int t, int oldl, int oldt) {
                    // TODO Auto-generated method stub
                    mHSRightHeader.scrollTo(l, 0);
                }
            });


            TableRow leftHeaderRow = new TableRow(getActivity());

            TableRow.LayoutParams lHeaderParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);

            TextView mMemberName_headerText = new TextView(getActivity());
            mMemberName_headerText
                    .setText("" + String.valueOf(AppStrings.memberName));
            mMemberName_headerText.setTypeface(LoginActivity.sTypeface);
            mMemberName_headerText.setTextColor(Color.WHITE);
            mMemberName_headerText.setPadding(10, 5, 10, 5);
            mMemberName_headerText.setLayoutParams(lHeaderParams);
            leftHeaderRow.addView(mMemberName_headerText);

            mLeftHeaderTable.addView(leftHeaderRow);

            TableRow rightHeaderRow = new TableRow(getActivity());
            TableRow.LayoutParams rHeaderParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            rHeaderParams.setMargins(10, 0, 20, 0);
            TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            contentParams.setMargins(10, 0, 10, 0);

            TextView moutstandingAmount_HeaderText = new TextView(getActivity());
            moutstandingAmount_HeaderText
                    .setText("" + String.valueOf(AppStrings.outstanding));
            moutstandingAmount_HeaderText.setTypeface(LoginActivity.sTypeface);
            moutstandingAmount_HeaderText.setTextColor(Color.WHITE);
            moutstandingAmount_HeaderText.setPadding(5, 5, 5, 5);
            moutstandingAmount_HeaderText.setLayoutParams(contentParams);
            moutstandingAmount_HeaderText.setGravity(Gravity.CENTER);
            moutstandingAmount_HeaderText.setSingleLine(true);
            rightHeaderRow.addView(moutstandingAmount_HeaderText);

            TextView mVAmount_HeaderText = new TextView(getActivity());
            mVAmount_HeaderText
                    .setText("" + String.valueOf(AppStrings.amount));
            mVAmount_HeaderText.setTypeface(LoginActivity.sTypeface);
            mVAmount_HeaderText.setTextColor(Color.WHITE);

            mVAmount_HeaderText.setLayoutParams(rHeaderParams);
            mVAmount_HeaderText.setSingleLine(true);
            rightHeaderRow.addView(mVAmount_HeaderText);

            TextView mVIntrestAmount_HeaderText = new TextView(getActivity());
            mVIntrestAmount_HeaderText
                    .setText("" + String.valueOf(AppStrings.interest));
            mVIntrestAmount_HeaderText.setTypeface(LoginActivity.sTypeface);
            mVIntrestAmount_HeaderText.setTextColor(Color.WHITE);

            mVIntrestAmount_HeaderText.setLayoutParams(rHeaderParams);
            mVIntrestAmount_HeaderText.setSingleLine(true);
            rightHeaderRow.addView(mVIntrestAmount_HeaderText);

            TextView cHeadView = new TextView(getActivity());
            cHeadView
                    .setText("" + String.valueOf(AppStrings.cashmode));
            cHeadView.setTypeface(LoginActivity.sTypeface);
            cHeadView.setTextColor(Color.WHITE);

            cHeadView.setLayoutParams(rHeaderParams);
            cHeadView.setSingleLine(true);
            rightHeaderRow.addView(cHeadView);

            mRightHeaderTable.addView(rightHeaderRow);
            //   getTableRowHeaderCellWidth();

            //code
            //responsesize = responseDto.getResponseContent().getMemberloanRepaymentList().size();
            for (int i = 0; i < outstandingamt.size(); i++) {

                TableRow leftContentRow = new TableRow(getActivity());

                TableRow.LayoutParams leftContentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                leftContentParams.setMargins(5, 5, 5, 5);

                memberName_Text = new TextView(getActivity());
                memberName_Text.setText(responseDto.getResponseContent().getMemberloanRepaymentList().get(i).getMemberName());
                //memberName_Text.setText(memList.get(i).getMemberName());
                memberName_Text.setTextColor(R.color.black);
                memberName_Text.setPadding(5, 5, 5, 5);
                memberName_Text.setLayoutParams(leftContentParams);
                memberName_Text.setWidth(200);
                memberName_Text.setSingleLine(true);
                memberName_Text.setEllipsize(TextUtils.TruncateAt.END);
                leftContentRow.addView(memberName_Text);

                mLeftContentTable.addView(leftContentRow);

                TableRow rightContentRow = new TableRow(getActivity());
                TableRow.LayoutParams rightContentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);// (150,60,1f);
                rightContentParams.setMargins(10, 5, 10, 5);

                TableRow.LayoutParams contentRow_Params = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 60,
                        1f);

                contentRow_Params.setMargins(10, 5, 10, 5);

                moutstanding = new TextView(getActivity());
                String out = "";
                out = responseDto.getResponseContent().getMemberloanRepaymentList().get(i).getLoanOutstanding();
                if ((out == ("null")) || (out == ("")) || (out == null)) {
                    moutstanding.setText("0");
                } else {
                    String outstandamount;
                    outstandamount = responseDto.getResponseContent().getMemberloanRepaymentList().get(i).getLoanOutstanding();
                    int oam = (int) Double.parseDouble(outstandamount);
                    moutstanding.setText(oam + "");
                    //moutstanding.setText(responseDto.getResponseContent().getMemberloanRepaymentList().get(i).getLoanOutstanding());
                }
                moutstanding.setTextColor(R.color.black);
                moutstanding.setWidth(130);
                moutstanding.setGravity(Gravity.CENTER);
                moutstanding.setLayoutParams(contentRow_Params);
                moutstanding.setPadding(10, 0, 10, 5);
                rightContentRow.addView(moutstanding);


                mamount_values = new EditText(getActivity());
                mamount_values.setId(i);
                samountFields.add(mamount_values);
                mamount_values.setWidth(130);
                mamount_values.setPadding(5, 5, 5, 5);
                mamount_values.setBackgroundResource(R.drawable.edittext_background);
                mamount_values.setLayoutParams(rightContentParams);
                mamount_values.setTextAppearance(getActivity(), R.style.MyMaterialTheme);
                mamount_values.setFilters(Get_EdiText_Filter.editText_filter());
                mamount_values.setInputType(InputType.TYPE_CLASS_NUMBER);
                mamount_values.setTextColor(R.color.black);
                // mSavings_values.setWidth(150);
                mamount_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        // TODO Auto-generated method stub
                        if (hasFocus) {
                            ((EditText) v).setGravity(Gravity.LEFT);
                            mMemberNameLayout.setVisibility(View.VISIBLE);
                            // mMemberNameLayout.setVisibility(View.VISIBLE);
                            mMemberName.setText(memberName_Text.getText().toString().trim());
                            TextviewUtils.manageBlinkEffect(mMemberName, getActivity());

                        } else {

                            ((EditText) v).setGravity(Gravity.RIGHT);

                            mMemberName.setText("");
                        }

                    }
                });
                rightContentRow.addView(mamount_values);

                mintrest_values = new EditText(getActivity());
                mintrest_values.setId(i);
                sintrestFields.add(mintrest_values);
                mintrest_values.setWidth(130);
                mintrest_values.setPadding(5, 5, 5, 5);
                mintrest_values.setBackgroundResource(R.drawable.edittext_background);
                mintrest_values.setLayoutParams(rightContentParams);
                mintrest_values.setTextAppearance(getActivity(), R.style.MyMaterialTheme);
                mintrest_values.setFilters(Get_EdiText_Filter.editText_filter());
                mintrest_values.setInputType(InputType.TYPE_CLASS_NUMBER);
                mintrest_values.setTextColor(R.color.black);
                // mVSavings_values.setWidth(150);
                mintrest_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        // TODO Auto-generated method stub
                        if (hasFocus) {
                            ((EditText) v).setGravity(Gravity.LEFT);

                            mMemberNameLayout.setVisibility(View.GONE);
                            mMemberName.setText(memberName_Text.getText().toString().trim());
                            TextviewUtils.manageBlinkEffect(mMemberName, getActivity());

                        } else {

                            ((EditText) v).setGravity(Gravity.RIGHT);
                            mMemberNameLayout.setVisibility(View.GONE);
                            mMemberName.setText("");
                        }
                    }
                });
                rightContentRow.addView(mintrest_values);

                //   mRightContentTable.addView(rightContentRow);


                TableRow.LayoutParams contentParams3 = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                contentParams3.setMargins(30, 5, 10, 5);
                sCheckBox1 = new CheckBox(getActivity());
                sCheckBox1.setId(i);
                sCheckBox1.setWidth(50);
                mCheckBoxFields.add(sCheckBox1);
                sCheckBox1.setBackgroundResource(R.drawable.btn_check_to_off_mtrl_013);
                sCheckBox1.setClickable(true);
                final int position = i;
                sCheckBox1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if (compoundButton.isChecked()) {
                            mCheckBoxFields.get(position).setChecked(true);
                        } else {
                            mCheckBoxFields.get(position).setChecked(false);
                        }

                    }
                });
                sCheckBox1.setLayoutParams(contentParams3);
                sCheckBox1.setGravity(Gravity.LEFT);
                rightContentRow.addView(sCheckBox1);
                mRightContentTable.addView(rightContentRow);


            }


            mSubmit_Raised_Button = (Button) view.findViewById(R.id.mlr_fragment_Submit_button);
            mSubmit_Raised_Button.setText(AppStrings.submit);
            mSubmit_Raised_Button.setTypeface(LoginActivity.sTypeface);
            mSubmit_Raised_Button.setOnClickListener(this);

            mPerviousButton = (Button) view.findViewById(R.id.mlr_fragment_Previousbutton);
            //  mPerviousButton.setText("Savings" + AppStrings.mPervious);
            mPerviousButton.setOnClickListener(this);

            mNextButton = (Button) view.findViewById(R.id.mlr_fragment_memberskipbutton);
            //     mNextButton.setText("Savings" + AppStrings.mNext);
            mNextButton.setOnClickListener(this);


            mPerviousButton.setVisibility(View.INVISIBLE);
            mNextButton.setVisibility(View.INVISIBLE);
            resizeMemberNameWidth();
            //      resizeRightSideTable();
            resizeBodyTableRowHeight();


        } catch (Exception e) {
            Log.e("error", e.toString());
        }
    }

    private void resizeRightSideTable() {
        // TODO Auto-generated method stub
        int rightHeaderCount = (((TableRow) mRightHeaderTable.getChildAt(0)).getChildCount());
        System.out.println("------------header count---------" + rightHeaderCount);
        for (int i = 0; i < rightHeaderCount; i++) {
            rightHeaderWidth[i] = viewWidth(((TableRow) mRightHeaderTable.getChildAt(0)).getChildAt(i));
            rightContentWidth[i] = viewWidth(((TableRow) mRightContentTable.getChildAt(0)).getChildAt(i));
        }
        for (int i = 0; i < rightHeaderCount; i++) {
            if (rightHeaderWidth[i] < rightContentWidth[i]) {
                ((TableRow) mRightHeaderTable.getChildAt(0)).getChildAt(i)
                        .getLayoutParams().width = rightContentWidth[i];
            } else {
                ((TableRow) mRightContentTable.getChildAt(0)).getChildAt(i)
                        .getLayoutParams().width = rightHeaderWidth[i];
            }
        }
    }

    private void getTableRowHeaderCellWidth() {
        int lefHeaderChildCount = ((TableRow) mLeftHeaderTable.getChildAt(0)).getChildCount();
        int rightHeaderChildCount = ((TableRow) mRightHeaderTable.getChildAt(0)).getChildCount();

        for (int x = 0; x < (lefHeaderChildCount + rightHeaderChildCount); x++) {
            if (x == 0) {
                rightHeaderWidth[x] = viewWidth(((TableRow) mLeftHeaderTable.getChildAt(0)).getChildAt(x));
            } else {
                rightHeaderWidth[x] = viewWidth(((TableRow) mRightHeaderTable.getChildAt(0)).getChildAt(x - 1));
            }
        }
    }

    // read a view's height
    private int viewHeight(View view) {
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        return view.getMeasuredHeight();
    }

    private int viewWidth(View view) {
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        return view.getMeasuredWidth();
    }

    private void resizeMemberNameWidth() {
        // TODO Auto-generated method stub
        int leftHeadertWidth = viewWidth(mLeftHeaderTable);
        int leftContentWidth = viewWidth(mLeftContentTable);

        if (leftHeadertWidth < leftContentWidth) {
            mLeftHeaderTable.getLayoutParams().width = leftContentWidth;
        } else {
            mLeftContentTable.getLayoutParams().width = leftHeadertWidth;
        }
    }

    private void resizeBodyTableRowHeight() {

        int leftContentTable_ChildCount = mLeftContentTable.getChildCount();

        for (int x = 0; x < leftContentTable_ChildCount; x++) {

            TableRow leftContentTableRow = (TableRow) mLeftContentTable.getChildAt(x);
            TableRow rightContentTableRow = (TableRow) mRightContentTable.getChildAt(x);

            int rowLeftHeight = viewHeight(leftContentTableRow);
            int rowRightHeight = viewHeight(rightContentTableRow);

            TableRow tableRow = rowLeftHeight < rowRightHeight ? leftContentTableRow : rightContentTableRow;
            int finalHeight = rowLeftHeight > rowRightHeight ? rowLeftHeight : rowRightHeight;

            this.matchLayoutHeight(tableRow, finalHeight);
        }

    }

    private void matchLayoutHeight(TableRow tableRow, int height) {

        int tableRowChildCount = tableRow.getChildCount();

        // if a TableRow has only 1 child
        if (tableRow.getChildCount() == 1) {

            View view = tableRow.getChildAt(0);
            TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();
            params.height = height - (params.bottomMargin + params.topMargin);

            return;
        }

        // if a TableRow has more than 1 child
        for (int x = 0; x < tableRowChildCount; x++) {

            View view = tableRow.getChildAt(x);

            TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();

            if (!isTheHeighestLayout(tableRow, x)) {
                params.height = height - (params.bottomMargin + params.topMargin);
                return;
            }
        }

    }

    // check if the view has the highest height in a TableRow
    private boolean isTheHeighestLayout(TableRow tableRow, int layoutPosition) {

        int tableRowChildCount = tableRow.getChildCount();
        int heighestViewPosition = -1;
        int viewHeight = 0;

        for (int x = 0; x < tableRowChildCount; x++) {
            View view = tableRow.getChildAt(x);
            int height = this.viewHeight(view);

            if (viewHeight < height) {
                heighestViewPosition = x;
                viewHeight = height;
            }
        }
        return heighestViewPosition == layoutPosition;
    }

    @Override
    public void onClick(View view) {

        sAmountSavingsAmounts = new String[samountFields.size()];
        sintrestSavingsAmount = new String[sintrestFields.size()];

        Log.d("Saving", "sSavingsAmounts size : " + samountFields.size() + "");

        switch (view.getId()) {

            case R.id.mlr_fragment_Submit_button:
                try {
                    mlrlist = new ArrayList<>();
                    sAmount_Total = 0;
                    sIntrest_Total = 0;
                    sSendToServer_Amount = "";
                    sSendToServer_Intrest = "";

                    confirmArr = new String[mSize];

                    // Do edit values here
                    int Outstandingwarning = 0;

                    for (int i = 0; i < outstandingamt.size(); i++) {

                        // sAmountSavingsAmounts[i] = samountFields.get(i).getText().toString();

                        if (samountFields.get(i).getText().toString().equals("")) {
                            sAmountSavingsAmounts[i] = "0";
                        } else {
                            sAmountSavingsAmounts[i] = samountFields.get(i).getText().toString();
                        }
                        if (sintrestFields.get(i).getText().toString().equals("")) {
                            sintrestSavingsAmount[i] = "0";
                        } else {
                            sintrestSavingsAmount[i] = sintrestFields.get(i).getText().toString();
                        }

                        String outstnd = outstandingamt.get(i).getLoanOutstanding();
                        int out = 0;

                        if ((outstnd == "") || (outstnd == null) || (outstnd == "null")) {
                            out = 0;
                        } else {
                            out = (int) Double.parseDouble(outstnd);
                            // out = Integer.parseInt(outstandingamt.get(i).getLoanOutstanding());
                        }


                        if (out >= Integer.parseInt(sAmountSavingsAmounts[i].trim())) {
                            sAmount_Total += Integer.parseInt((!samountFields.get(i).getText().toString().equals("") && samountFields.get(i).getText().toString().length() > 0) ? samountFields.get(i).getText().toString() : "0");
                            //sintrestSavingsAmount[i] = sintrestFields.get(i).getText().toString();
                            sIntrest_Total += Integer.parseInt((!sintrestFields.get(i).getText().toString().equals("") && sintrestFields.get(i).getText().toString().length() > 0) ? sintrestFields.get(i).getText().toString() : "0");
                            RepaymentDetails rpl = new RepaymentDetails();
                            rpl.setMemberId(outstandingamt.get(i).getMemberId());
                            rpl.setRepaymentAmount(sAmountSavingsAmounts[i]);
                            rpl.setInterest(sintrestSavingsAmount[i]);
                            rpl.setLoanOutstanding(outstandingamt.get(i).getLoanOutstanding());

                            //sAmountSavingsAmounts[i] = samountFields.get(i).getText().toString();

                            mlrlist.add(rpl);


                        } else {
                            Outstandingwarning = 1;

                            break;

                        }

                    }

                    sum_of_memberloan_repayment = sAmount_Total + sIntrest_Total;
                    finalsum_of_memberloan_repayment = finalsum_of_memberloan_repayment + sum_of_memberloan_repayment;
                    Log.d("sum_of_memberloan", "" + sum_of_memberloan_repayment);

                    if (mlrlist != null && mlrlist.size() > 0) {
                        mlrlist.clear();
                    }

                    int cash_count = 0;

                    for (int i = 0; i < outstandingamt.size(); i++) {
                        RepaymentDetails rpl = new RepaymentDetails();
                        rpl.setMemberId(outstandingamt.get(i).getMemberId());
                        rpl.setRepaymentAmount(sAmountSavingsAmounts[i]);
                        rpl.setInterest(sintrestSavingsAmount[i]);
                        rpl.setLoanOutstanding(outstandingamt.get(i).getLoanOutstanding());


                        if (mCheckBoxFields.get(i).isChecked()) {
                            rpl.setCash(true);
                        } else {
                            rpl.setCash(false);
                        }

                        mlrlist.add(rpl);

                        if (mlrlist.get(i).isCash()) {

                            cash_count = cash_count + 1;
                        }


                    }
                    if (mlrlist.size() == cash_count) {
                        timeout_saving = MySharedPreference.readLong(getActivity(), MySharedPreference.fttimeout, 180000);

                    } else if (cash_count == 0) {
                        timeout_saving = MySharedPreference.readLong(getActivity(), MySharedPreference.upi_timeout, 180000);

                    } else {
                        timeout_saving = Math.max(MySharedPreference.readLong(getActivity(), MySharedPreference.fttimeout, 180000), MySharedPreference.readLong(getActivity(), MySharedPreference.upi_timeout, 180000));

                    }

                    /*for (int i = 0; i < sAmountSavingsAmounts.length; i++) {

                        // Principle Amount
                        sAmountSavingsAmounts[i] = String.valueOf(samountFields.get(i).getText().toString());
                        if ((sAmountSavingsAmounts[i].equals("")) || (sAmountSavingsAmounts[i] == null)) {
                            sAmountSavingsAmounts[i] = nullAmount;
                        }

                        // Interest
                        sintrestSavingsAmount[i] = String.valueOf(sintrestFields.get(i).getText().toString());
                        if ((sintrestSavingsAmount[i].equals("")) || (sintrestSavingsAmount[i] == null)) {
                            sintrestSavingsAmount[i] = nullAmount;
                        }

                        if (sAmountSavingsAmounts[i].length() > 0) { // match
                            // a
                            // decimal
                            // number

                            int amount = (int) Math.round(Double.parseDouble(sAmountSavingsAmounts[i]));
                            sAmountSavingsAmounts[i] = String.valueOf(amount);
                        }

                        if (sintrestSavingsAmount[i].length() > 0) { // match
                            // a
                            // decimal
                            // number

                            int amount = (int) Math.round(Double.parseDouble(sintrestSavingsAmount[i]));
                            sintrestSavingsAmount[i] = String.valueOf(amount);
                        }

                        if (Integer.parseInt(outstandingamt.get(i).getLoanOutstanding()) >= Integer.parseInt(sAmountSavingsAmounts[i].trim())) {


                        } else {
                            TastyToast.makeText(getActivity(), com.yesteam.eshakti.appConstants.AppStrings.groupRepaidAlert, TastyToast.LENGTH_SHORT,
                                    TastyToast.WARNING);


                        }
                    }*/


                    if (Outstandingwarning == 0) {
                        if ((sAmount_Total != 0) || (sIntrest_Total != 0)) {

                            confirmationDialog = new Dialog(getActivity());

                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.dialog_new_confirmation, null);

                            ViewGroup.LayoutParams lParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                            dialogView.setLayoutParams(lParams);

                            TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
                            confirmationHeader.setText("" + AppStrings.confirmation);
                            confirmationHeader.setTypeface(LoginActivity.sTypeface);


                            TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

                            for (int i = 0; i < outstandingamt.size(); i++) {

                                TableRow indv_SavingsRow = new TableRow(getActivity());

                                TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                                contentParams.setMargins(10, 5, 10, 5);

                                TextView memberName_Text = new TextView(getActivity());
                                memberName_Text.setText(com.oasys.eshakti.digitization.OasysUtils.GetSpanText.getSpanString(getActivity(),
                                        outstandingamt.get(i).getMemberName()));
                                memberName_Text.setTextColor(R.color.black);
                                memberName_Text.setPadding(5, 5, 5, 5);
                                memberName_Text.setSingleLine(true);
                                memberName_Text.setLayoutParams(contentParams);
                                indv_SavingsRow.addView(memberName_Text);

                                TextView confirm_values = new TextView(getActivity());
                                confirm_values
                                        .setText(com.oasys.eshakti.digitization.OasysUtils.GetSpanText.getSpanString(getActivity(), String.valueOf(sAmountSavingsAmounts[i])));
                                confirm_values.setTextColor(R.color.black);
                                confirm_values.setPadding(5, 5, 5, 5);
                                confirm_values.setGravity(Gravity.RIGHT);
                                confirm_values.setLayoutParams(contentParams);
                                indv_SavingsRow.addView(confirm_values);

                                TextView confirm_VSvalues = new TextView(getActivity());
                                confirm_VSvalues
                                        .setText(com.oasys.eshakti.digitization.OasysUtils.GetSpanText.getSpanString(getActivity(), String.valueOf(sintrestSavingsAmount[i])));
                                confirm_VSvalues.setTextColor(R.color.black);
                                confirm_VSvalues.setPadding(5, 5, 5, 5);
                                confirm_VSvalues.setGravity(Gravity.RIGHT);
                                confirm_VSvalues.setLayoutParams(contentParams);
                                indv_SavingsRow.addView(confirm_VSvalues);

                                CheckBox sCheckBox1 = new CheckBox(getActivity());
                                sCheckBox1.setId(i);
                                sCheckBox1.setBackgroundResource(R.drawable.btn_check_to_off_mtrl_013);
                                // sCheckBox1.setPadding(5, 5, 5, 5);
                                sCheckBox1.setClickable(false);
                                sCheckBox1.setLayoutParams(contentParams);
                                if (mCheckBoxFields.get(i).isChecked())
                                    sCheckBox1.setChecked(true);
                                else
                                    sCheckBox1.setChecked(false);

                                sCheckBox1.setGravity(Gravity.LEFT);
                                indv_SavingsRow.addView(sCheckBox1);


                                confirmationTable.addView(indv_SavingsRow,
                                        new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                            }
                            View rullerView = new View(getActivity());
                            rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
                            rullerView.setBackgroundColor(Color.rgb(0, 199, 140));// rgb(255,
                            // 229,
                            // 242));
                            confirmationTable.addView(rullerView);

                            TableRow totalRow = new TableRow(getActivity());

                            TableRow.LayoutParams totalParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                            totalParams.setMargins(10, 5, 10, 5);

                            TextView totalText = new TextView(getActivity());
                            totalText.setText(com.oasys.eshakti.digitization.OasysUtils.GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.total)));
                            totalText.setTypeface(LoginActivity.sTypeface);
                            totalText.setTextColor(R.color.black);
                            totalText.setPadding(5, 5, 5, 5);// (5, 10, 5, 10);
                            totalText.setLayoutParams(totalParams);
                            totalRow.addView(totalText);

                            TextView totalAmount = new TextView(getActivity());
                            totalAmount.setText(com.oasys.eshakti.digitization.OasysUtils.GetSpanText.getSpanString(getActivity(), String.valueOf(sAmount_Total)));
                            totalAmount.setTextColor(R.color.black);
                            totalAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
                            totalAmount.setGravity(Gravity.RIGHT);
                            totalAmount.setLayoutParams(totalParams);
                            totalRow.addView(totalAmount);

                            TextView totalVSAmount = new TextView(getActivity());
                            totalVSAmount.setText(com.oasys.eshakti.digitization.OasysUtils.GetSpanText.getSpanString(getActivity(), String.valueOf(sIntrest_Total)));
                            totalVSAmount.setTextColor(R.color.black);
                            totalVSAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
                            totalVSAmount.setGravity(Gravity.RIGHT);
                            totalVSAmount.setLayoutParams(totalParams);
                            totalRow.addView(totalVSAmount);

                             /*   TextView checkAmount = new TextView(getActivity());
                                //  checkAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sVSavings_Total)));
                                checkAmount.setTextColor(R.color.black);
                                checkAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
                                checkAmount.setGravity(Gravity.RIGHT);
                                checkAmount.setVisibility(View.INVISIBLE);
                                checkAmount.setLayoutParams(totalParams);
                                totalRow.addView(checkAmount);
*/
                            confirmationTable.addView(totalRow,
                                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                            TableRow totalwalletRow = new TableRow(getActivity());

                            TableRow.LayoutParams totalwalletParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                            totalwalletParams.setMargins(10, 5, 10, 5);

                            TextView totalwalletText = new TextView(getActivity());
                            totalwalletText.setText(com.oasys.eshakti.digitization.OasysUtils.GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.walletbalanceTxt)));
                            //    totalwalletText.setTextColor(R.color.green_border_color);
                            totalwalletText.setTextColor(getActivity().getResources().getColor(R.color.green_border_color));
                            totalwalletText.setPadding(5, 5, 5, 5);// (5, 10, 5, 10);
                            totalwalletText.setLayoutParams(totalwalletParams);
                            totalwalletText.setTextSize(20);
                            totalwalletRow.addView(totalwalletText);

                            TextView totalwalletAmount = new TextView(getActivity());
                            totalwalletAmount.setText(com.oasys.eshakti.digitization.OasysUtils.GetSpanText.getSpanString(getActivity(), String.valueOf(((Integer.parseInt(shgDto.getCashInHand()))))));// + sSavings_Total+sVSavings_Total
                            totalwalletAmount.setTextColor(R.color.green_border_color);
                            totalwalletAmount.setPadding(5, 5, 25, 5);// (5, 10, 100, 10);
                            totalwalletAmount.setGravity(Gravity.RIGHT);
                            totalwalletText.setTextSize(20);
                            totalwalletAmount.setLayoutParams(totalwalletParams);
                            totalwalletRow.addView(totalwalletAmount);

                            TextView checkAmount1 = new TextView(getActivity());
                            //  checkAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sVSavings_Total)));
                            checkAmount1.setTextColor(R.color.black);
                            checkAmount1.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
                            checkAmount1.setGravity(Gravity.RIGHT);
                            checkAmount1.setVisibility(View.INVISIBLE);
                            checkAmount1.setLayoutParams(totalwalletParams);
                            totalRow.addView(checkAmount1);

//                        TextView totalVSAmount = new TextView(getActivity());
//                        totalVSAmount.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(sVSavings_Total)));
//                        totalVSAmount.setTextColor(R.color.black);
//                        totalVSAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
//                        totalVSAmount.setGravity(Gravity.RIGHT);
//                        totalVSAmount.setLayoutParams(totalParams);
//                        totalRow.addView(totalVSAmount);


                            confirmationTable.addView(totalwalletRow,
                                    new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                            mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit);
                            mEdit_RaisedButton.setText(AppStrings.edit);
                            mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
                            // 205,
                            // 0));
                            mEdit_RaisedButton.setOnClickListener(this);

                            mOk_RaisedButton = (Button) dialogView.findViewById(R.id.frag_Ok);
                            mOk_RaisedButton.setText(AppStrings.yes);
                            mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
                            mOk_RaisedButton.setOnClickListener(this);

                            confirmationDialog.getWindow()
                                    .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            confirmationDialog.setCanceledOnTouchOutside(false);
                            confirmationDialog.setContentView(dialogView);
                            confirmationDialog.setCancelable(true);
                            confirmationDialog.show();

                            ViewGroup.MarginLayoutParams margin = (ViewGroup.MarginLayoutParams) dialogView.getLayoutParams();
                            margin.leftMargin = 10;
                            margin.rightMargin = 10;
                            margin.topMargin = 10;
                            margin.bottomMargin = 10;
                            margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);

                        } else {

                            TastyToast.makeText(getActivity(), AppStrings.nullAlert, TastyToast.LENGTH_SHORT,
                                    TastyToast.WARNING);

                            sAmount_Total = 0;
                            sIntrest_Total = 0;
                            sSendToServer_Amount = "";
                            sSendToServer_Intrest = "";
                        }
                    } else {
                        TastyToast.makeText(getActivity(), AppStrings.groupRepaidAlert, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.fragment_Edit:
                sAmount_Total = 0;
                sIntrest_Total = 0;
                sSendToServer_Amount = "";
                sSendToServer_Intrest = "";
                mSubmit_Raised_Button.setClickable(true);

                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();

                break;
            case R.id.frag_Ok:

                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();

//                MemberDrawerScreen.showFragment(new Transactionpaymentsuccess());
                try {

                    Calendar calender = Calendar.getInstance();

                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    String formattedDate = df.format(calender.getTime());

                    DateFormat simple = new SimpleDateFormat("yyyy-MM-dd");
                    Date d = new Date(Long.parseLong(shgDto.getLastTransactionDate()));
                    String dateStr = simple.format(d);
                    MemberloanRepayRequestDto mlr = new MemberloanRepayRequestDto();
                    mlr.setShgId(shgId);
                    mlr.setLoanUuid(loan_id);
                    mlr.setModeOfCash("1");
                    mlr.setMobileDate(formattedDate);
                    mlr.setTransactionDate(dateStr);
                    mlr.setUserId(MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, ""));
                    mlr.setRepaymentDetails(mlrlist);

                    String sreqString = new Gson().toJson(mlr);
                    if (networkConnection.isNetworkAvailable()) {
                        onTaskStarted();

                        ViewGroup viewGroup = view.findViewById(android.R.id.content);
                        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.online_transaction_timer, viewGroup, false);
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setView(dialogView);
                        alertDialog = builder.create();
                        alertDialog.show();
                        alertDialog.setCancelable(false);
                        counterTextView = dialogView.findViewById(R.id.counterTimerTxt);
                        counterTimerStatusTxt = dialogView.findViewById(R.id.counterTimerStatusTxt);

                        RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.MEMBERLOANREPAYMENT, sreqString, getActivity(), ServiceType.MEMBERLOANREPAY);

                        cndr = new CountDownTimer(timeout_saving, 1000) { // adjust the milli seconds here
                            public void onTick(long millisUntilFinished) {
                                counterTextView.setText("" + String.format(FORMAT_TIMER,
                                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(
                                                TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                                                TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))) + " minutes");
                            }

                            @Override
                            public void onFinish() {
//                            MemberDrawerScreen.showFragment(new Digitization_Deposit_Details());
                                cndr.cancel();
                                alertDialog.dismiss();
                                TastyToast.makeText(getActivity(), AppStrings.counterDismiss, TastyToast.LENGTH_SHORT,
                                        TastyToast.WARNING);
//                            timer.cancel();
                            }


                        }.start();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.fragment_Previousbutton:
                break;
            case R.id.mlr_fragment_memberskipbutton:
                try {

                    Utils.loanTypes.remove(0);

                    if (Utils.loanTypes.size() > 0) {

                        MemberLoanRepayment memberLoanRepayment = new MemberLoanRepayment();
                        Bundle bundles = new Bundle();
                        bundles.putString("stepwise", Attendance.flag);
                        bundles.putString("loan_id", Utils.loanTypes.get(0).getLoanId());
                        bundles.putString("loan_type", Utils.loanTypes.get(0).getLoanTypeName());
                        bundles.putString("account_number", Utils.loanTypes.get(0).getAccountNumber());
                        bundles.putString("bank_name", Utils.loanTypes.get(0).getBankName());
                        memberLoanRepayment.setArguments(bundles);
                        NewDrawerScreen.showFragment(memberLoanRepayment);
                    } else {
                        LD_IL_Entry pl_DisbursementFragment = new LD_IL_Entry();
                        Bundle bundles = new Bundle();
                        bundles.putString("stepwise", Attendance.flag);
                        pl_DisbursementFragment.setArguments(bundles);
                        NewDrawerScreen.showFragment(pl_DisbursementFragment);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        try {
            switch (serviceType) {
                case GETOUTSTANDINGAMOUNT:
                    responseDto = new Gson().fromJson(result, ResponseDto.class);

                    if (responseDto != null) {

                        if (responseDto.getStatusCode() == Utils.Success_Code) {
                            outstandingamt = responseDto.getResponseContent().getMemberloanRepaymentList();
                            init();
                        } else {
                            int statusCode = responseDto.getStatusCode();
                            if (statusCode == 401) {

                                Log.e("Group Logout", "Logout Sucessfully");
                                AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                            }
                        }
                    }
                    if (Attendance.flag == "1") {
                        if (responseDto != null) {
                            for (int i = 0; i < responseDto.getResponseContent().getMemberloanRepaymentList().size(); i++) {
                                String s = responseDto.getResponseContent().getMemberloanRepaymentList().get(i).getLoanOutstanding();
                                int store = (int) Double.parseDouble(s);
                                sum = sum + store;

                            }

                            if (sum == 0) {
                                mNextButton.setVisibility(View.VISIBLE);
                            }

                        }
                    }

                    break;
                case MEMBERLOANREPAY:
                    ResponseDto resdto = new Gson().fromJson(result, ResponseDto.class);
                  /*  if (resdto.getStatusCode() == Utils.Success_Code) {
                        String message = resdto.getMessage();
                        confirmationDialog.dismiss();
                        Utils.showToast(getActivity(), resdto.getMessage());

                    } else {
                        String message = resdto.getMessage();
                        confirmationDialog.dismiss();
                        Utils.showToast(getActivity(), message);*/
                    if (Attendance.flag == "1") {
                        Utils.loanTypes.remove(0);

                        if (Utils.loanTypes.size() > 0) {

                            if (confirmationDialog.isShowing()) {
                                confirmationDialog.dismiss();
                            }
                            MemberLoanRepayment memberLoanRepayment = new MemberLoanRepayment();
                            Bundle bundles = new Bundle();
                            bundles.putString("stepwise", Attendance.flag);
                            bundles.putString("loan_id", Utils.loanTypes.get(0).getLoanId());
                            bundles.putString("loan_type", Utils.loanTypes.get(0).getLoanTypeName());
                            bundles.putString("account_number", Utils.loanTypes.get(0).getAccountNumber());
                            bundles.putString("bank_name", Utils.loanTypes.get(0).getBankName());
                            memberLoanRepayment.setArguments(bundles);
                            NewDrawerScreen.showFragment(memberLoanRepayment);
                            confirmationDialog.dismiss();

                        } else {
                            LD_IL_Entry pl_DisbursementFragment = new LD_IL_Entry();
                            Bundle bundles = new Bundle();
                            bundles.putString("stepwise", Attendance.flag);
                            pl_DisbursementFragment.setArguments(bundles);
                            NewDrawerScreen.showFragment(pl_DisbursementFragment);
                            confirmationDialog.dismiss();
                        }
                    } else if (resdto != null) {

                        if (resdto.getStatusCode() == Utils.Success_Code) {

                            TastyToast.makeText(getActivity(), "Transaction Completed",
                                    TastyToast.LENGTH_SHORT, TastyToast.SUCCESS);
                            if (alertDialog != null && alertDialog.isShowing())
                                alertDialog.dismiss();
                            if (cndr != null)
                                cndr.cancel();

                            confirmationDialog.dismiss();

                            Utils.showToast(getActivity(), resdto.getMessage());
                            if (shgDto.getFFlag() == null || !shgDto.getFFlag().equals("1")) {
                                SHGTable.updateTransactionSHGDetails(shgDto.getShgId());
                            }
                            if (resdto.getResponseContents() != null) {
                                for (int i = 0; i < resdto.getResponseContents().size(); i++) {
                                    ResponseContents contents = new ResponseContents();
                                    contents.setMemberName(resdto.getResponseContents().get(i).getMemberName());
                                    contents.setTransactionId(resdto.getResponseContents().get(i).getTransactionId());
                                    contents.setMessage(resdto.getResponseContents().get(i).getMessage());
                                    contents.setTxType(AppStrings.memberloanrepayment);

                                    responseContentsList.add(contents);

                                }
                                ViewSavingTransactionDetails viewSavingTransactionDetails = new ViewSavingTransactionDetails();
                                Bundle bundle = new Bundle();
                                bundle.putSerializable("transactiondetails", responseContentsList);
                                viewSavingTransactionDetails.setArguments(bundle);
                                NewDrawerScreen.showFragment(viewSavingTransactionDetails);

                            } else {
                                FragmentManager fm = getFragmentManager();
                                fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                MainFragment mainFragment = new MainFragment();
                                Bundle bundles = new Bundle();
                                bundles.putString("Transaction", MainFragment.Flag_Transaction);
                                mainFragment.setArguments(bundles);
                                NewDrawerScreen.showFragment(mainFragment);
                            }

//                            MemberDrawerScreen.showFragment(new MainFragment());


                        } else {

                            int statusCode = responseDto.getStatusCode();
                            if (statusCode == 401) {

                                Log.e("Group Logout", "Logout Sucessfully");
                                AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                            }
                            confirmationDialog.dismiss();

                            TastyToast.makeText(getActivity(), AppStrings.failedDigiResponse,
                                    TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                            if (alertDialog != null && alertDialog.isShowing())
                                alertDialog.dismiss();
                            if (cndr != null)
                                cndr.cancel();
                            Utils.showToast(getActivity(), resdto.getMessage());

                        }

                    } else {
                        TastyToast.makeText(getActivity(), AppStrings.failedDigiResponse,
                                TastyToast.LENGTH_SHORT, TastyToast.ERROR);
                        if (alertDialog != null && alertDialog.isShowing())
                            alertDialog.dismiss();
                        if (cndr != null)
                            cndr.cancel();
                    }
                    break;
                case NAV_DETAILS:
                    try {
                        if (result != null && result.length() > 0) {
                            GsonBuilder gsonBuilder = new GsonBuilder();
                            Gson gson = gsonBuilder.create();
                            ResponseDto mrDto = gson.fromJson(result, ResponseDto.class);
                            int statusCode = mrDto.getStatusCode();
                            Log.d("Main Frag response ", " " + statusCode);
                            if (statusCode == 400 || statusCode == 401 || statusCode == 403 || statusCode == 500 || statusCode == 503) {
                                // showMessage(statusCode);

                            } else if (statusCode == 401) {

                                Log.e("Group Logout", "Logout Sucessfully");
                                AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                            } else if (statusCode == Utils.Success_Code) {

                                csGrp = mrDto.getResponseContent().getCashOfGroup();
                                SHGTable.updateSHGDetails(csGrp.get(0), shgDto.getId());
                                shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }
        } catch (
                Exception e)

        {

        }

    }

    public void responsedetails() {

        for (int i = 0; i < responsesize; i++) {


            TableRow leftContentRow = new TableRow(getActivity());

            TableRow.LayoutParams leftContentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 60, 1f);
            leftContentParams.setMargins(5, 5, 5, 5);

            memberName_Text = new TextView(getActivity());
            memberName_Text.setText(responseDto.getResponseContent().getMemberloanRepaymentList().get(i).getMemberName());
            //memberName_Text.setText(memList.get(i).getMemberName());
            memberName_Text.setTextColor(R.color.black);
            memberName_Text.setPadding(5, 5, 5, 5);
            memberName_Text.setLayoutParams(leftContentParams);
            memberName_Text.setWidth(200);
            memberName_Text.setSingleLine(true);
            memberName_Text.setEllipsize(TextUtils.TruncateAt.END);
            leftContentRow.addView(memberName_Text);

            mLeftContentTable.addView(leftContentRow);

            TableRow rightContentRow = new TableRow(getActivity());

            TableRow.LayoutParams rightContentParams = new TableRow.LayoutParams(rightHeaderWidth[1],
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            rightContentParams.setMargins(20, 5, 20, 5);

            TableRow.LayoutParams contentRow_Params = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 60,
                    1f);

            contentRow_Params.setMargins(10, 5, 10, 5);


            moutstanding = new TextView(getActivity());
            moutstanding.setText(responseDto.getResponseContent().getMemberloanRepaymentList().get(i).getLoanOutstanding());
            moutstanding.setTextColor(R.color.black);
            moutstanding.setGravity(Gravity.CENTER);
            moutstanding.setLayoutParams(contentRow_Params);
            moutstanding.setPadding(10, 0, 10, 5);
            rightContentRow.addView(moutstanding);


            mamount_values = new EditText(getActivity());
            mamount_values.setId(i);
            // samountFields.add(mamount_values);
            mamount_values.setPadding(5, 5, 5, 5);
            mamount_values.setBackgroundResource(R.drawable.edittext_background);
            mamount_values.setLayoutParams(rightContentParams);
            mamount_values.setTextAppearance(getActivity(), R.style.MyMaterialTheme);
            mamount_values.setFilters(Get_EdiText_Filter.editText_filter());
            mamount_values.setInputType(InputType.TYPE_CLASS_NUMBER);
            mamount_values.setTextColor(R.color.black);
            // mSavings_values.setWidth(150);
            mamount_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    // TODO Auto-generated method stub
                    if (hasFocus) {
                        ((EditText) v).setGravity(Gravity.LEFT);

                        mMemberNameLayout.setVisibility(View.VISIBLE);
                        mMemberName.setText(memberName_Text.getText().toString().trim());
                        TextviewUtils.manageBlinkEffect(mMemberName, getActivity());

                    } else {

                        ((EditText) v).setGravity(Gravity.RIGHT);
                        mMemberNameLayout.setVisibility(View.GONE);
                        mMemberName.setText("");
                    }

                }
            });
            rightContentRow.addView(mamount_values);

            mintrest_values = new EditText(getActivity());
            mintrest_values.setId(i);
            // sintrestFields.add(mintrest_values);
            mintrest_values.setPadding(5, 5, 5, 5);
            mintrest_values.setBackgroundResource(R.drawable.edittext_background);
            mintrest_values.setLayoutParams(rightContentParams);
            mintrest_values.setTextAppearance(getActivity(), R.style.MyMaterialTheme);
            mintrest_values.setFilters(Get_EdiText_Filter.editText_filter());
            mintrest_values.setInputType(InputType.TYPE_CLASS_NUMBER);
            mintrest_values.setTextColor(R.color.black);
            // mVSavings_values.setWidth(150);
            mintrest_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    // TODO Auto-generated method stub
                    if (hasFocus) {
                        ((EditText) v).setGravity(Gravity.LEFT);

                        mMemberNameLayout.setVisibility(View.VISIBLE);
                        mMemberName.setText(memberName_Text.getText().toString().trim());
                        TextviewUtils.manageBlinkEffect(mMemberName, getActivity());

                    } else {

                        ((EditText) v).setGravity(Gravity.RIGHT);
                        mMemberNameLayout.setVisibility(View.GONE);
                        mMemberName.setText("");
                    }
                }
            });
            rightContentRow.addView(mintrest_values);

            mRightContentTable.addView(rightContentRow);

        }
    }
}
