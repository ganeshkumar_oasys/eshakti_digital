package com.oasys.eshakti.digitization.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.model.ListItem;

import java.util.List;

public class CustomMReportAdapter extends BaseAdapter {

    Context context;
    List<ListItem> listItems;

    public CustomMReportAdapter(Context context, List<ListItem> items) {
        this.context = context;
        this.listItems = items;
    }

    /* private view holder class */
    private class ViewHolder {
        //       ImageView listLeftImage;
        TextView listTitle;
        TextView listValue;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        try {

            ViewHolder holder = null;

            LayoutInflater mInflater = (LayoutInflater) context
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.monthly_report_adapter, null);
                holder = new ViewHolder();

                //     holder.listLeftImage = (ImageView) convertView.findViewById(R.id.img1);
                holder.listTitle = (TextView) convertView
                        .findViewById(R.id.typename);
                holder.listValue = (TextView) convertView
                        .findViewById(R.id.amount);

                convertView.setTag(holder);

            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            //   holder.listLeftImage.setVisibility(View.VISIBLE);
            ListItem listItem = (ListItem) getItem(position);

            holder.listTitle
                    .setText("" + String
                            .valueOf(listItem.getTitle()));

            //  holder.listLeftImage.setImageResource(listItem.getImageId());


            //  if (listItem.getWalletValue() != null) {
            holder.listValue
                    .setText(String
                            .valueOf(listItem.getWalletValue()));
           // holder.listValue.setVisibility(View.VISIBLE);
            //     holder.listTitle.setTypeface(LoginActivity.sTypeface);
            holder.listTitle.setSelected(true);

           /* if (EShaktiApplication.isCheckGroupListTextColor()) {

                if ("".equals("Yes")) {
                    holder.listTitle.setTextColor(Color.WHITE);
                } else {
                    holder.listTitle.setTextColor(Color.YELLOW);
                }
            }

            if (EShaktiApplication.isGroupListValues()) {
                if ("".equals("No")) {
                    holder.listTitle.setTextColor(Color.YELLOW);
                }

            } else {
                holder.listTitle.setTextColor(Color.WHITE);
            }
*/


        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return listItems.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub

        return listItems.indexOf(getItem(position));
    }

}
