package com.oasys.eshakti.digitization.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasys.eshakti.digitization.Dto.MemberSavingsSummary;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.activity.LoginActivity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Report__MemberReport_SavingSummary_Adapter extends RecyclerView.Adapter<Report__MemberReport_SavingSummary_Adapter.MyViewholder> {

    private final String savtlt, savDistlt;
    private Context context;
    private ArrayList<MemberSavingsSummary> memberSavingsSummaries;
    private String dateStr;

    public Report__MemberReport_SavingSummary_Adapter(Context context, ArrayList<MemberSavingsSummary> memberSavingsSummaries, String savtlt, String savDistlt) {
        this.context = context;
        this.memberSavingsSummaries = memberSavingsSummaries;
        this.savtlt = savtlt;
        this.savDistlt = savDistlt;

        if (this.savtlt != null && this.savtlt.length() > 0  && !this.savtlt.equals("0.0")) {
            MemberSavingsSummary ms = new MemberSavingsSummary();
            ms.setTransactionDate("TOTAL");
            ms.setTransactionAmount(savtlt);
            this.memberSavingsSummaries.add(ms);
        } else if (this.savDistlt != null && this.savDistlt.length() > 0 && !this.savDistlt.equals("0.0")) {
            MemberSavingsSummary ms = new MemberSavingsSummary();
            ms.setTransactionDate("TOTAL");
            ms.setTransactionAmount(savDistlt);
            this.memberSavingsSummaries.add(ms);
        }

    }

    @NonNull
    @Override
    public MyViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.groupsavingviews, parent, false);
        return new MyViewholder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewholder holder, int position) {

        Calendar calender = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
        String formattedDate = df.format(calender.getTime());

        DateFormat simple = new SimpleDateFormat("yyyy/MM/dd");


        if (memberSavingsSummaries.get(position).getTransactionDate() != null && !memberSavingsSummaries.get(position).getTransactionDate().equals("TOTAL")) {
            Date d = new Date(Long.parseLong(memberSavingsSummaries.get(position).getTransactionDate()));
            dateStr = simple.format(d);
            holder.groupsavings_name.setText(dateStr);
        } else {
            holder.groupsavings_name.setText(memberSavingsSummaries.get(position).getTransactionDate());
        }

        holder.groupsavings_voluntarysaving.setText(memberSavingsSummaries.get(position).getTransactionAmount());

        if (memberSavingsSummaries.get(position).getTransactionAmount() == null || memberSavingsSummaries.get(position).getTransactionAmount().equals("0.0")) {
            holder.groupsavings_voluntarysaving.setVisibility(View.GONE);
        } else {
            holder.groupsavings_voluntarysaving.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return memberSavingsSummaries.size();
    }

    class MyViewholder extends RecyclerView.ViewHolder {
        TextView groupsavings_name, groupsavings_amount, groupsavings_voluntarysaving;


        public MyViewholder(View itemView) {
            super(itemView);


            groupsavings_amount = (TextView) itemView.findViewById(R.id.mGroupreportsavings);
            groupsavings_amount.setTypeface(LoginActivity.sTypeface);
            groupsavings_amount.setVisibility(View.GONE);
            groupsavings_name = (TextView) itemView.findViewById(R.id.mGroupreportName);
          //  groupsavings_name.setTypeface(LoginActivity.sTypeface);
            groupsavings_voluntarysaving = (TextView) itemView.findViewById(R.id.mGroupreporVoluntary);
            groupsavings_voluntarysaving.setTypeface(LoginActivity.sTypeface);

        }
    }
}
