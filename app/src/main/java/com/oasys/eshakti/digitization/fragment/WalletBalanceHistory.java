package com.oasys.eshakti.digitization.fragment;


import android.app.Dialog;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.oasys.eshakti.digitization.Adapter.WalletbalanceAdapter;
import com.oasys.eshakti.digitization.Dto.HistoryDetailsDto;
import com.oasys.eshakti.digitization.Dto.HistoryDto;
import com.oasys.eshakti.digitization.Dto.Wallet_Bank_CW;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;
import com.oasys.eshakti.digitization.model.ListItem;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class WalletBalanceHistory extends Fragment implements NewTaskListener {

    private View view;
    private RecyclerView recyclerView_walletbalance_details;
    private LinearLayoutManager linearLayoutManager;
    private WalletbalanceAdapter walletbalanceAdapter;
    private String fromdateStr, todateStr, type, animator_id,user_id;
    private NetworkConnection networkConnection;
    private Dialog mProgressDilaog;
    private ArrayList<Wallet_Bank_CW> wallet_bank_cwArrayList;
    private ArrayList<ListItem> listItems;
    private TextView daily_date_time,digitization_header;
    private String currentDateStr;
    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    public WalletBalanceHistory() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_wallet_balance_history, container, false);
        init();
        
        return  view;
    }

    private void init() {

        try {

            daily_date_time =(TextView)view.findViewById(R.id.daily_date_time);

            digitization_header =(TextView)view.findViewById(R.id.digitization_header);

            Calendar calender = Calendar.getInstance();
            currentDateStr = df.format(calender.getTime());
            daily_date_time.setText(currentDateStr);

            digitization_header.setText(AppStrings.mWB_history);

            listItems = new ArrayList<ListItem>();
            Bundle bundle = getArguments();
            fromdateStr = bundle.getString("fromdate");
            todateStr = bundle.getString("todate");
            type = bundle.getString("type");
            animator_id = bundle.getString("animatorid");

            networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());

            recyclerView_walletbalance_details = (RecyclerView) view.findViewById(R.id.recyclerView_walletbalance_details);
            linearLayoutManager = new LinearLayoutManager(getActivity());
            recyclerView_walletbalance_details.setLayoutManager(linearLayoutManager);
            recyclerView_walletbalance_details.setHasFixedSize(true);

            if (type.equals(NewDrawerScreen.W_WB_REP)) {

                //TODO:: WALLET BALANCE
                HistoryDetailsDto hdto = new HistoryDetailsDto();
                hdto.setFromDate(fromdateStr);
                hdto.setUserId(MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, ""));
                hdto.setAgentId(MySharedPreference.readString(getActivity(), MySharedPreference.AGENT_ID, ""));
                hdto.setToDate(todateStr);
                String sreqString = new Gson().toJson(hdto);
                onTaskStarted();
                RestClient.getRestClient(WalletBalanceHistory.this).callRestWebService(Constants.BASE_URL + Constants.WALLET_WB_HIS, sreqString, getActivity(), ServiceType.WALLET_WB_HIS);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskStarted() {
        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
            mProgressDilaog.dismiss();
            mProgressDilaog = null;
        }
        switch (serviceType) {
            case WALLET_WB_HIS:
                if (result != null && result.length() > 0) {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    HistoryDto lrDto = gson.fromJson(result, HistoryDto.class);
                    int statusCode = lrDto.getStatusCode();
                    String message = lrDto.getMessage();
                    Log.d("response status", " " + statusCode);
                    if (statusCode == 400 || statusCode == 401 || statusCode == 403 || statusCode == 500 || statusCode == 503) {
                        // showMessage(statusCode);
                        Utils.showToast(getActivity(), message);


                        if (statusCode == 503) {

                            Utils.showToast(getActivity(), AppStrings.service_unavailable);

                        }

                    } else if (statusCode == Utils.Success_Code) {
                        Utils.showToast(getActivity(), message);


                        if(lrDto.getResponseContents()!= null && lrDto.getResponseContents().size() > 0)
                        {
                            for (int i = 0; i < lrDto.getResponseContents().size(); i++) {

                                ListItem rowItem = new ListItem();
                                rowItem.setTransactionDate(lrDto.getResponseContents().get(i).getTransactionDate());
//                                rowItem.setTransactionDescription(lrDto.getResponseContents().get(i).getTransactionDescription());
                                rowItem.setTransactionId(lrDto.getResponseContents().get(i).getTransactionId());
//                                rowItem.setAmount(lrDto.getResponseContents().get(i).getAmount());
//                                rowItem.setTransactionMode(lrDto.getResponseContents().get(i).getTransactionMode());
                                rowItem.setPreviousWalletAmount(lrDto.getResponseContents().get(i).getPreviousWalletAmount());
                                rowItem.setUpdatedWalletAmount(lrDto.getResponseContents().get(i).getUpdatedWalletAmount());
                                rowItem.setTransType(lrDto.getResponseContents().get(i).getTransType());
                                rowItem.setTransRemarks(lrDto.getResponseContents().get(i).getTransRemarks());
                                rowItem.setAmountProcessed(lrDto.getResponseContents().get(i).getAmountProcessed());
                                listItems.add(rowItem);
                            }

                            walletbalanceAdapter = new WalletbalanceAdapter(getActivity(),listItems);
                            recyclerView_walletbalance_details.setAdapter(walletbalanceAdapter);


                        }


                    }
                }
        }

    }
}
