package com.oasys.eshakti.digitization.Dto.RequestDto;

import lombok.Data;

/**
 * Created by MuthukumarPandi on 1/21/2019.
 */
@Data
public class MinutesId {
    private String id;
}
