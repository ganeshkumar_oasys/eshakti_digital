package com.oasys.eshakti.digitization.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.model.ListItem;

import java.util.List;

public class CustomTransactionListAdapter extends BaseAdapter {

    Context context;
    List<ListItem> listItems;

    public CustomTransactionListAdapter(Context context, List<ListItem> items) {
        this.context = context;
        this.listItems = items;
    }

    /* private view holder class */
    private class ViewHolder {
        TextView name;
        TextView amt;
        TextView tx_id;
        TextView sts;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        try {

            ViewHolder holder = null;
            LayoutInflater mInflater = (LayoutInflater) context
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.ly_child_ly, null);
                holder = new ViewHolder();

                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.amt = (TextView) convertView
                        .findViewById(R.id.amt);
                holder.tx_id = (TextView) convertView
                        .findViewById(R.id.tx_id);
                holder.sts = (TextView) convertView
                        .findViewById(R.id.sts);

                convertView.setTag(holder);

            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            ListItem listItem = (ListItem) getItem(position);

            holder.name.setText("" + String
                    .valueOf(listItem.getName()));
            //     holder.listTitle.setTypeface(LoginActivity.sTypeface);
            holder.amt.setText("" + String
                    .valueOf(listItem.getAmt()));
            holder.tx_id.setText("" + String
                    .valueOf(listItem.getTxId()));

            if (listItem.getSts().equals("Failed")) {
                holder.sts.setText("  " + String
                        .valueOf(listItem.getSts()));
                holder.sts.setTextColor(context.getResources().getColor(R.color.red));
            } else {
                holder.sts.setText("" + String
                        .valueOf(listItem.getSts()));
                holder.sts.setTextColor(context.getResources().getColor(R.color.black));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return listItems.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub

        return listItems.indexOf(getItem(position));
    }

}
