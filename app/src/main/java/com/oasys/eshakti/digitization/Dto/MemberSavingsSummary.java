package com.oasys.eshakti.digitization.Dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class MemberSavingsSummary implements Serializable {

    private String transactionAmount;
    private String transactionDate;
}
