package com.oasys.eshakti.digitization.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.oasys.eshakti.digitization.Adapter.CustomTransactionListAdapter;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.model.ListItem;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ViewTransactionDetails extends Fragment implements View.OnClickListener {

    private View view;
    private ListView list_details;
    private TextView f_date, t_date;
    private ArrayList<ListItem> listItems;
    String[] mLoanMenu;
    private CustomTransactionListAdapter mAdapter;

    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
    private Date openinigDate;
    private Date currentDate;
    private String fromdateStr, todateStr;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.tx_details, container, false);
//        Bundle bundle = getArguments();
//        fromdateStr = bundle.getString("fromdate");
//        todateStr = bundle.getString("todate");

        Bundle bundle =new Bundle();
        bundle.getSerializable("transactiondetails");
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }



    private void init() {

        list_details = (ListView) view.findViewById(R.id.list_details);

        listItems = new ArrayList<ListItem>();

        for (int i = 0; i < 20; i++) {
            ListItem rowItem = new ListItem();

            if (i % 2 != 0) {
                rowItem.setName("Ram");
                rowItem.setTxId("342344343414134");
                rowItem.setAmt("214234");
                if (i == 3 || i == 7) {
                    rowItem.setSts("Failed");
                } else
                    rowItem.setSts("Success");
            } else {
                rowItem.setName("Laxman");
                rowItem.setTxId("56783467534144");
                rowItem.setAmt("204234");
                rowItem.setSts("Success");
            }
            listItems.add(rowItem);
        }
        mAdapter = new CustomTransactionListAdapter(getActivity(), listItems);
        list_details.setAdapter(mAdapter);

    }

    @Override
    public void onClick(View view) {



    }
}
