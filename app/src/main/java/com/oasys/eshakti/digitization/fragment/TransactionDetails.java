package com.oasys.eshakti.digitization.fragment;


import android.app.Dialog;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.oasys.eshakti.digitization.Adapter.TransactionDetailsAdapter;
import com.oasys.eshakti.digitization.Dto.HistoryDetailsDto;
import com.oasys.eshakti.digitization.Dto.HistoryDto;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;
import com.oasys.eshakti.digitization.model.ListItem;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */

 public class TransactionDetails extends Fragment implements NewTaskListener {

    private View view;
    private RecyclerView recyclerView_transaction_details;
    private TextView daily_date_time, digitization_header;
    private String currentDateStr;
    private LinearLayoutManager linearLayoutManager;
    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    private NetworkConnection networkConnection;
    private ArrayList<ListItem> listItems;
    private String fromdateStr, todateStr, type, animator_id, user_id;
    private Dialog mProgressDilaog;
    private TransactionDetailsAdapter rechargehistoryAdapter;


    public TransactionDetails() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_transaction_details, container, false);
        init();
        return view;
    }

    private void init() {

        try {

            daily_date_time = (TextView) view.findViewById(R.id.daily_date_time);
            digitization_header = (TextView) view.findViewById(R.id.digitization_header);
            recyclerView_transaction_details = (RecyclerView) view.findViewById(R.id.recyclerView_transaction_details);

            digitization_header.setText(AppStrings.mTR_Details);
            Calendar calender = Calendar.getInstance();
            currentDateStr = df.format(calender.getTime());
            daily_date_time.setText(currentDateStr);

            networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
            listItems = new ArrayList<>();
            Bundle bundle = getArguments();
            fromdateStr = bundle.getString("fromdate");
            todateStr = bundle.getString("todate");
            type = bundle.getString("type");
            animator_id = bundle.getString("animatorid");

            linearLayoutManager = new LinearLayoutManager(getActivity());
            recyclerView_transaction_details.setLayoutManager(linearLayoutManager);
            recyclerView_transaction_details.setHasFixedSize(true);

            if (networkConnection.isNetworkAvailable()) {

                if (type.equals(NewDrawerScreen.TRANS_DET)) {

                    HistoryDetailsDto hdto = new HistoryDetailsDto();
                    hdto.setFromDate(fromdateStr);
                    hdto.setUserId(MySharedPreference.readString(getActivity(), MySharedPreference.ANIMATOR_ID, ""));
                    hdto.setToDate(todateStr);
                    String sreqString = new Gson().toJson(hdto);
                    onTaskStarted();
                    RestClient.getRestClient(TransactionDetails.this).callRestWebService(Constants.BASE_URL + Constants.WALLET_TRS_DETAILS, sreqString, getActivity(), ServiceType.WALLET_TRS_DETAILS);
                }

            } else {
                Utils.showToast(getActivity(), "NETWORK NOT AVAILABLE");
            }


        } catch (Exception e) {

        }
    }

    @Override
    public void onTaskStarted() {

        mProgressDilaog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDilaog.show();
    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {

        if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
            mProgressDilaog.dismiss();
            mProgressDilaog = null;
        }
        switch (serviceType)
        {
            case WALLET_TRS_DETAILS:

                if (result != null && result.length() > 0) {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    HistoryDto lrDto = gson.fromJson(result, HistoryDto.class);
                    int statusCode = lrDto.getStatusCode();
                    String message = lrDto.getMessage();

                    if (statusCode == 400 || statusCode == 401 || statusCode == 403 || statusCode == 500 || statusCode == 503) {

                        Utils.showToast(getActivity(), message);

                        if (statusCode == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                            if ((mProgressDilaog != null) && mProgressDilaog.isShowing()) {
                                mProgressDilaog.dismiss();
                                mProgressDilaog = null;
                            }
                        }

                        if (statusCode == 503) {

                            Utils.showToast(getActivity(), AppStrings.service_unavailable);

                        }


                    } else if (statusCode == Utils.Success_Code) {
                        Utils.showToast(getActivity(), message);

//                        if (listItems != null && listItems.size() > 0) {
//                            listItems.clear();
//                        }

                        if (lrDto.getResponseContents() != null && lrDto.getResponseContents().size() > 0) {


                            for (int i = 0; i < lrDto.getResponseContents().size(); i++) {
                                ListItem rowItem = new ListItem();
                                rowItem.setMerchantId(lrDto.getResponseContents().get(i).getMerchantId());
                                rowItem.setTransactionDate(lrDto.getResponseContents().get(i).getTransactionDate());
                                rowItem.setAmount(lrDto.getResponseContents().get(i).getAmount());
                                rowItem.setTransactionId(lrDto.getResponseContents().get(i).getTransactionId());
                                rowItem.setReferenceId(lrDto.getResponseContents().get(i).getReferenceId());
                                rowItem.setTransactionStatus(lrDto.getResponseContents().get(i).getTransactionStatus());
                                rowItem.setTransactionType(lrDto.getResponseContents().get(i).getTransactionType());
                                rowItem.setAnimatorName(lrDto.getResponseContents().get(i).getAnimatorName());
                                listItems.add(rowItem);
                            }

                            rechargehistoryAdapter = new TransactionDetailsAdapter(getActivity(),listItems);
                            recyclerView_transaction_details.setAdapter(rechargehistoryAdapter);

                        } else {
                            Utils.showToast(getActivity(), message);
                        }
                    }else {
                        Utils.showToast(getActivity(), message);
                    }

                }
                break;
        }

    }
}