package com.oasys.eshakti.digitization.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.activity.LoginActivity;

import java.util.ArrayList;


public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.MyViewholder> {

    private Context context;
    private ArrayList<DashboardDto> dashboardDtos;

    public DashboardAdapter(Context context, ArrayList<DashboardDto> dashboardDtos) {
        this.context = context;
        this.dashboardDtos = dashboardDtos;
    }

    @NonNull
    @Override
    public MyViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.dashboard_layout_item, parent, false);
        return new MyViewholder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewholder holder, int position) {
        holder.mdashboardName.setText(dashboardDtos.get(position).getName());
        holder.mdashboardName.setTypeface(LoginActivity.sTypeface);
        holder.mdashboardImage.setImageResource(dashboardDtos.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return dashboardDtos.size();
    }

    class MyViewholder extends RecyclerView.ViewHolder {
        TextView mdashboardName;
        ImageView mdashboardImage;

        public MyViewholder(View itemView) {
            super(itemView);
            mdashboardName = (TextView) itemView.findViewById(R.id.mDashboardName);
            mdashboardImage = (ImageView) itemView.findViewById(R.id.mDashItemImage);

        }
    }
}
