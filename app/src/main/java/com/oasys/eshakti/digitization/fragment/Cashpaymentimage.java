package com.oasys.eshakti.digitization.fragment;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;

/**
 * A simple {@link Fragment} subclass.
 */
public class Cashpaymentimage extends Fragment implements View.OnClickListener {
    private View view;
    private TextView wallet_finalimage_continue;


    public Cashpaymentimage() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_cashpaymentimage, container, false);
        inIt(view);
        return view;
    }

    private void inIt(View view) {

        wallet_finalimage_continue = (TextView) view.findViewById(R.id.wallet_finalimage_continue);
        wallet_finalimage_continue.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch ((v.getId())) {
            case R.id.wallet_finalimage_continue:
            WalletDetails walletDetails = new WalletDetails();
            NewDrawerScreen.showFragment(walletDetails);
            break;
        }
    }
}
