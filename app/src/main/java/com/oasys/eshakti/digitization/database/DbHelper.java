package com.oasys.eshakti.digitization.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class DbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "eshakti.db";
    private static final int DATABASE_VERSION = 1;

    private static DbHelper sDataHelper;

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);


    }

    public static String getDatabase() {
        return DATABASE_NAME;
    }

    public static DbHelper getInstance(final Context context) {
        if (sDataHelper == null) {
            sDataHelper = new DbHelper(context);
        }
        return sDataHelper;
    }

    public static DbHelper getInstance() {
        return sDataHelper;
    }

    public void init() {
        getReadableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(TableName.CREATE_LOGIN_TABLE);
            db.execSQL(TableName.CREATE_SHG_GROUP);
            db.execSQL(TableName.CREATE_MEMBER_DETAILS);
            db.execSQL(TableName.CREATE_BANK_DETAILS);
            db.execSQL(TableName.CREATE_LOAN_DETAILS);
            db.execSQL(TableName.CREATE_EXPENSE_DETAILS);
            db.execSQL(TableName.CREATE_LOAN_BANK_POL);
            db.execSQL(TableName.CREATE_LOAN_BANK_LTYPE);
            db.execSQL(TableName.CREATE_LOAN_INSTAL_TYPE);
            db.execSQL(TableName.CREATE_LOAN_BANK_DETAILS);
            db.execSQL(TableName.CREATE_LOAN_IL_TYPE_SETTING);
            db.execSQL(TableName.CREATE_LOAN_FED_SETTING);
            db.execSQL(TableName.CREATE_LOAN_MFI_SETTING);
            Log.e("Query",""+TableName.CREATE_MEMBER_BANKFULL_DETAILS);
            db.execSQL(TableName.CREATE_MEMBER_BANKFULL_DETAILS);
            db.execSQL(TableName.CREATE_ENROLL_FP_DATA);

            //db.execSQL(TableName.CREATE_TABLE_TRANSACTION);


          /*
            db.execSQL(TableName.CREATE_TABLE_TRANS_INCOME);
            db.execSQL(TableName.CREATE_TABLE_TRANS_EXPENSE);
            db.execSQL(TableName.CREATE_TABLE_TRANS_MEM_LOAN_RP);
            db.execSQL(TableName.CREATE_TABLE_TRANS_LOAN_DIS);
            db.execSQL(TableName.CREATE_TABLE_TRANS_BANKTRANS);*/

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
