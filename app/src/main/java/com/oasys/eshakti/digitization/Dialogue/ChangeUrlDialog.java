package com.oasys.eshakti.digitization.Dialogue;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.R;


/**
 * This dialog will appear on the time of user logout
 */
public class ChangeUrlDialog extends Dialog implements View.OnClickListener {


    private final Activity context;  //    Context from the user

    /*Constructor class for this dialog*/
    public ChangeUrlDialog(Activity _context) {
        super(_context);
        context = _context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setContentView(R.layout.dialog_changeurl);
        setCancelable(false);

        //    SharedPreferences prefs = context.getSharedPreferences("FPS", Context.MODE_PRIVATE);
        //    String serverUrl = MySharedPreference.readString(context, MySharedPreference.ChangeUrl, "http://192.168.3.253:8631");
        String serverUrl = MySharedPreference.readString(context, MySharedPreference.ChangeUrl, "http://ossdevapi.oasys.co");// TODO:: DEV
       // String serverUrl = MySharedPreference.readString(context, MySharedPreference.ChangeUrl, "http://osstestapi.oasys.co"); // TODO:: TEST
        Log.e("ser", serverUrl);
        ((EditText) findViewById(R.id.editTextUrl)).setText(serverUrl);
        TextView textViewTitle = (TextView) findViewById(R.id.textViewNwTitle);
        textViewTitle.setText(AppStrings.ChangeUrl);
        Button okButton = (Button) findViewById(R.id.buttonNwOk);
        okButton.setOnClickListener(this);
        Button cancelButton = (Button) findViewById(R.id.buttonNwCancel);
        cancelButton.setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonNwOk:
                if (storeInLocal()) {
                    Constants.setBaseUrl(MySharedPreference.readString(context, MySharedPreference.ChangeUrl, "http://192.168.3.253:8631"));
                    dismiss();

                     /* Intent i = new Intent(context, LoginActivity.class);
                    context.startActivity(i);*/
                }
                break;
            case R.id.buttonNwCancel:
                dismiss();
                break;
        }
    }

    /**
     * Store changed ip in shared preference
     * returns true if value present else false
     */
    private boolean storeInLocal() {
        String url = ((EditText) findViewById(R.id.editTextUrl)).getText().toString().trim();
        if (url == null || url.length() <= 5) {
            return false;
        }
        MySharedPreference.writeString(context, MySharedPreference.ChangeUrl, url);
        return true;
    }


    /**
     * Tamil text textView typeface
     * input  textView name and text string input
     */
    public void setTamilText(TextView textName, String text) {

        textName.setText(text);
    }

    /**
     * Tamil text Button typeface
     * input  textView name and text string input
     */
    public void setTamilText(Button btn, String text) {
        btn.setText(text);
    }

    /**
     * Tamil text Button typeface
     * input  textView name and text string input
     */
    public void setTamilText(EditText editText, String text) {
        editText.setHint(text);
    }

}