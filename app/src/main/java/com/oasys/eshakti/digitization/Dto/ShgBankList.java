package com.oasys.eshakti.digitization.Dto;


import java.io.Serializable;

import lombok.Data;

@Data
public class ShgBankList  implements Serializable {

    private String fixedDeposit;

    private String cashAtBankAmount;

    private String bankName;

    private String bankId;

    private String shgSavingsId;
}
