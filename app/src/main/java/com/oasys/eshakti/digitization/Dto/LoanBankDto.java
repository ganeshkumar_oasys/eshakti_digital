package com.oasys.eshakti.digitization.Dto;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.Data;

@Data
public class LoanBankDto implements Serializable {

    private ArrayList<BranchList> branchList;
    private String id;
    private String name;

}