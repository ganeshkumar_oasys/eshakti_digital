package com.oasys.eshakti.digitization.Dto;

import java.io.Serializable;

import lombok.Data;

/**
 * Created by MuthukumarPandi on 12/8/2018.
 */
@Data
public class BankBalanceDTOList implements Serializable {
    private String loanOutstanding;

    private String loanType;
}
