package com.oasys.eshakti.digitization.fragment;


import android.app.Dialog;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.Dto.LoanDto;
import com.oasys.eshakti.digitization.Dto.MemberList;
import com.oasys.eshakti.digitization.Dto.RequestDto.MembersDetailsDto;
import com.oasys.eshakti.digitization.Dto.RequestDto.MicrocreditplanDto;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;
import com.oasys.eshakti.digitization.database.MemberTable;
import com.oasys.eshakti.digitization.database.SHGTable;

import com.oasys.eshakti.digitization.views.CustomHorizontalScrollView;
import com.tutorialsee.lib.TastyToast;
import com.yesteam.eshakti.utils.Get_EdiText_Filter;
import com.yesteam.eshakti.views.ButtonFlat;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class GetBankloanFragment extends Fragment implements View.OnClickListener, NewTaskListener {
    View view;
    private TextView mGroupName, mCashinHand, mCashatBank, mHeader, mLoanType;
    public static List<EditText> sPL_Fields = new ArrayList<EditText>();
    public static List<TextView> sPOL_Fields = new ArrayList<TextView>();
    public static List<EditText> sIncome_Fields = new ArrayList<EditText>();
    public static List<EditText> sExpense_Fields = new ArrayList<EditText>();
    public static List<EditText> sBefoeLoanPay_Fields = new ArrayList<EditText>();
    public static List<EditText> sLoans_Fields = new ArrayList<EditText>();
    public static List<EditText> sSurplus_Fields = new ArrayList<EditText>();
    public static String[] sPL_Amounts, sIncomeA_Amounts, sExpense_Amounts, sBeforeLoanPay_Amounts, sLoans_Amounts,
            sSurplus_Amounts;
    private ListOfShg shgDto;
    String mSelectedPOL_Values[];
    String mSelected_POL_Values = "";
    public static String[] sPOLvalues;
    private Button mSubmit_Raised_Button, mEdit_RaisedButton, mOk_RaisedButton;
    public static int sPL_total, sIncome_total, sExpense_total, sBeforeLoanPay_total, sLoans_total, sSurplus_total;
    private TableLayout mLeftHeaderTable, mRightHeaderTable, mLeftContentTable, mRightContentTable;
    TableRow.LayoutParams incomeHeaderParams, expenseHeaderParams, blpHeaderParams, loansHeaderParams,
            surplusHeaderParams;
    private NetworkConnection networkConnection;
    private CustomHorizontalScrollView mHSRightHeader, mHSRightContent;
    ResponseDto mcpdto;
    ArrayList<LoanDto> loantypeslist;
    private List<MemberList> memList;
    RadioButton radioButton;
    int selectedId = 100;
    String mSelectedLoanType;
    private EditText mPL_values, mIncome_values, mExpense_values, mBeforeLoanPay_values, mLoans_values, mSurplus_values;
    int leftHeaderColumnWidth[], leftContentColumnWidth[], rightHeaderColumnWidth[], rightContentColumnWidth[];
    public static String sSelected_POL[], sSelected_POL_Id[];
    String nullAmount = "0";
    String loanType = "CHOOSE THE LOAN";
    boolean isError, isnullAmountError;
    Dialog confirmationDialog;
    MicrocreditplanDto mcplandto = new MicrocreditplanDto();

    private ArrayList<MembersDetailsDto> arrLoanType;
    String momid = "";
    private String flag = "0";


    public GetBankloanFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        momid = bundle.getString("bankid");
        Attendance.flag = bundle.getString("stepwise");
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());


        view = inflater.inflate(R.layout.fragment_get_bankloan, container, false);
        // Inflate the layout for this fragment

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (networkConnection.isNetworkAvailable()) {
            RestClient.getRestClient(this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.GET_MOM_LOANTYPE, getActivity(), ServiceType.GET_MOM_LOANTYPE);
        }

        sPL_Fields.clear();
        sPOL_Fields.clear();
        sIncome_Fields.clear();
        sExpense_Fields.clear();
        sBefoeLoanPay_Fields.clear();
        sLoans_Fields.clear();
        sSurplus_Fields.clear();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.mcp_fragment_Submit_button:
                arrLoanType = new ArrayList<>();
                sPL_total = 0;
                sIncome_total = 0;
                sExpense_total = 0;
                sBeforeLoanPay_total = 0;
                sLoans_total = 0;
                sSurplus_total = 0;




                sSelected_POL = new String[memList.size()];
                mSelectedPOL_Values = new String[sPL_Fields.size()];

                sPL_Amounts = new String[sPL_Fields.size()];
                sPOLvalues = new String[sPOL_Fields.size()];
                sIncomeA_Amounts = new String[sPOL_Fields.size()];
                sExpense_Amounts = new String[sPOL_Fields.size()];
                sBeforeLoanPay_Amounts = new String[sPOL_Fields.size()];
                sLoans_Amounts = new String[sPOL_Fields.size()];
                sSurplus_Amounts = new String[sPOL_Fields.size()];

                mSelected_POL_Values = "";

                sSelected_POL_Id = new String[memList.size()];

                for (int i = 0; i < sPL_Amounts.length; i++) {

                    sPL_Amounts[i] = String.valueOf(sPL_Fields.get(i).getText());

                    sIncomeA_Amounts[i] = String.valueOf(sIncome_Fields.get(i).getText());
                    sExpense_Amounts[i] = String.valueOf(sExpense_Fields.get(i).getText());
                    sBeforeLoanPay_Amounts[i] = String.valueOf(sBefoeLoanPay_Fields.get(i).getText());
                    sLoans_Amounts[i] = String.valueOf(sLoans_Fields.get(i).getText());
                    sSurplus_Amounts[i] = String.valueOf(sSurplus_Fields.get(i).getText());

                    if ((sPL_Amounts[i].equals("")) || (sPL_Amounts[i] == null)) {
                        sPL_Amounts[i] = nullAmount;
                    }

                    if ((sIncomeA_Amounts[i].equals("")) || (sIncomeA_Amounts[i] == null)) {
                        sIncomeA_Amounts[i] = nullAmount;
                    }

                    if ((sExpense_Amounts[i].equals("")) || (sExpense_Amounts[i] == null)) {
                        sExpense_Amounts[i] = nullAmount;
                    }

                    if ((sBeforeLoanPay_Amounts[i].equals("")) || (sBeforeLoanPay_Amounts[i] == null)) {
                        sBeforeLoanPay_Amounts[i] = nullAmount;
                    }

                    if ((sLoans_Amounts[i].equals("")) || (sLoans_Amounts[i] == null)) {
                        sLoans_Amounts[i] = nullAmount;
                    }
                    if ((sSurplus_Amounts[i].equals("")) || (sSurplus_Amounts[i] == null)) {
                        sSurplus_Amounts[i] = nullAmount;
                    }

                    sPOLvalues[i] = String.valueOf(sPOL_Fields.get(i).getText());

                    Log.e("POL_VALUES", sPOLvalues[i]);
                    if (sPOLvalues[i].equals(loanType)) {
                        sSelected_POL[i] = nullAmount;
                        sSelected_POL_Id[i] = "0";
                    } else if (!sPOLvalues[i].equals(loanType)) {
                        sSelected_POL[i] = sPOLvalues[i]; // sSelected_POL[i];
                        for (int j = 0; j < loantypeslist.size(); j++) {
                            if (loantypeslist.get(j).getLoanTypeName().equals(sPOLvalues[i])) {
                                // Log.e("mPOL_ID", mPOL_ID[j]);
                                sSelected_POL_Id[i] = loantypeslist.get(j).getLoanTypeId();

                            }
                        }
                    }


                    if (!sPL_Amounts[i].equals(nullAmount)) {

                        if ((sSelected_POL[i].equals(nullAmount))) {
                            isError = true;
                        }
                    }
                    if (sPL_Amounts[i].equals(nullAmount)) {
                        if ((!sSelected_POL[i].equals(nullAmount))) {
                            isnullAmountError = true;
                        }

                        if ((!sIncomeA_Amounts[i].equals(nullAmount))) {
                            isnullAmountError = true;
                        }

                        if ((!sExpense_Amounts[i].equals(nullAmount))) {
                            isnullAmountError = true;
                        }

                        if ((!sBeforeLoanPay_Amounts[i].equals(nullAmount))) {
                            isnullAmountError = true;
                        }

                        if ((!sLoans_Amounts[i].equals(nullAmount))) {
                            isnullAmountError = true;
                        }

                        if ((!sSurplus_Amounts[i].equals(nullAmount))) {
                            isnullAmountError = true;
                        }
                    }
                    if (sSelected_POL[i] != null) {
                        if (sSelected_POL[i].equals(nullAmount)) {
                            mSelected_POL_Values = mSelected_POL_Values + "NO ,";
                        } else if (!sSelected_POL[i].equals(nullAmount)) {
                            mSelected_POL_Values = mSelected_POL_Values + sSelected_POL[i] + ",";
                        }
                        //  mSelected_POL_Values = mSelected_POL_Values + sSelected_POL[i] + ",";

                        sPL_total = sPL_total + Integer.parseInt(sPL_Amounts[i]);

                        sIncome_total = sIncome_total + Integer.parseInt(sIncomeA_Amounts[i]);
                        sExpense_total = sExpense_total + Integer.parseInt(sExpense_Amounts[i]);
                        sBeforeLoanPay_total = sBeforeLoanPay_total + Integer.parseInt(sBeforeLoanPay_Amounts[i]);
                        sLoans_total = sLoans_total + Integer.parseInt(sLoans_Amounts[i]);
                        sSurplus_total = sSurplus_total + Integer.parseInt(sSurplus_Amounts[i]);


                    }
                }
                mSelectedPOL_Values = mSelected_POL_Values.split(",");
                if ((sPL_total != 0) && (!Boolean.valueOf(isError)) && (!Boolean.valueOf(isnullAmountError))) {

                    confirmationDialog = new Dialog(getActivity());

                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.dialog_confirmation, null);
                    dialogView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT));

                    TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
                    confirmationHeader.setText("CONFIRMATION");


                    TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

                    TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                    contentParams.setMargins(10, 5, 10, 5);

                    for (int i = 0; i < sPL_Amounts.length; i++) {

                        TableRow indv_SavingsRow = new TableRow(getActivity());

                        TextView memberName_Text = new TextView(getActivity());
                        memberName_Text.setText(memList.get(i).getMemberName());
                        memberName_Text.setTextColor(R.color.black);
                        memberName_Text.setPadding(5, 5, 5, 5);
                        memberName_Text.setLayoutParams(contentParams);
                        indv_SavingsRow.addView(memberName_Text);

                        TextView confirm_values = new TextView(getActivity());
                        confirm_values
                                .setText(String.valueOf(sPL_Amounts[i]));
                        confirm_values.setTextColor(R.color.black);
                        confirm_values.setPadding(5, 5, 5, 5);
                        confirm_values.setGravity(Gravity.RIGHT);
                        confirm_values.setLayoutParams(contentParams);
                        indv_SavingsRow.addView(confirm_values);

                        TextView POL_values = new TextView(getActivity());
                        POL_values.setText(String.valueOf(mSelectedPOL_Values[i]));
                        POL_values.setTextColor(R.color.black);
                        POL_values.setPadding(5, 5, 5, 5);
                        POL_values.setGravity(Gravity.RIGHT);
                        POL_values.setLayoutParams(contentParams);
                        indv_SavingsRow.addView(POL_values);

                        TextView income_values = new TextView(getActivity());
                        income_values
                                .setText(String.valueOf(sIncomeA_Amounts[i]));
                        income_values.setTextColor(R.color.black);
                        income_values.setPadding(5, 5, 5, 5);
                        income_values.setGravity(Gravity.RIGHT);
                        income_values.setLayoutParams(contentParams);
                        indv_SavingsRow.addView(income_values);

                        TextView expense_values = new TextView(getActivity());
                        expense_values
                                .setText(String.valueOf(sExpense_Amounts[i]));
                        expense_values.setTextColor(R.color.black);
                        expense_values.setPadding(5, 5, 5, 5);
                        expense_values.setGravity(Gravity.RIGHT);
                        expense_values.setLayoutParams(contentParams);
                        indv_SavingsRow.addView(expense_values);

                        TextView beforeLoanPay_values = new TextView(getActivity());
                        beforeLoanPay_values.setText(String.valueOf(sBeforeLoanPay_Amounts[i]));
                        beforeLoanPay_values.setTextColor(R.color.black);
                        beforeLoanPay_values.setPadding(5, 5, 5, 5);
                        beforeLoanPay_values.setGravity(Gravity.RIGHT);
                        beforeLoanPay_values.setLayoutParams(contentParams);
                        indv_SavingsRow.addView(beforeLoanPay_values);

                        TextView loans_values = new TextView(getActivity());
                        loans_values
                                .setText(String.valueOf(sLoans_Amounts[i]));
                        loans_values.setTextColor(R.color.black);
                        loans_values.setPadding(5, 5, 5, 5);
                        loans_values.setGravity(Gravity.RIGHT);
                        loans_values.setLayoutParams(contentParams);
                        indv_SavingsRow.addView(loans_values);

                        TextView surplus_values = new TextView(getActivity());
                        surplus_values
                                .setText(String.valueOf(sSurplus_Amounts[i]));
                        surplus_values.setTextColor(R.color.black);
                        surplus_values.setPadding(5, 5, 5, 5);
                        surplus_values.setGravity(Gravity.RIGHT);
                        surplus_values.setLayoutParams(contentParams);
                        indv_SavingsRow.addView(surplus_values);

                        confirmationTable.addView(indv_SavingsRow,
                                new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                        if (sSelected_POL_Id[i] != "0") {
                            MembersDetailsDto Mddto = new MembersDetailsDto();
                            Mddto.setAmount(sPL_Amounts[i]);
                            Mddto.setIncomeAmount(sIncomeA_Amounts[i]);
                            Mddto.setExpenseAmount(sExpense_Amounts[i]);
                            Mddto.setBeforeLoanPayAmount(sBeforeLoanPay_Amounts[i]);
                            Mddto.setLoansAmount(sLoans_Amounts[i]);
                            Mddto.setSurplusAmount(sSurplus_Amounts[i]);
                            Mddto.setMemberId(memList.get(i).getMemberId());
                            Mddto.setPurposeOfLoanId(sSelected_POL_Id[i]);
                            arrLoanType.add(Mddto);
                        }

                    }
                    View rullerView = new View(getActivity());
                    rullerView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
                    rullerView.setBackgroundColor(Color.rgb(0, 199, 140));// rgb(255,
                    // 229,
                    // 242));
                    confirmationTable.addView(rullerView);

                    TableRow totalRow = new TableRow(getActivity());

                    TextView totalText = new TextView(getActivity());
                    totalText.setText("TOTAL");
                    totalText.setTextColor(R.color.black);
                    totalText.setPadding(5, 5, 5, 5);// (5, 10, 5, 10);
                    totalText.setLayoutParams(contentParams);
                    totalRow.addView(totalText);

                    TextView totalAmount = new TextView(getActivity());
                    totalAmount.setText(String.valueOf(sPL_total));
                    totalAmount.setTextColor(R.color.black);
                    totalAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
                    totalAmount.setGravity(Gravity.RIGHT);
                    totalAmount.setLayoutParams(contentParams);
                    totalRow.addView(totalAmount);

                    TextView emptyAmount = new TextView(getActivity());
                    emptyAmount.setText("");
                    emptyAmount.setTextColor(R.color.black);
                    emptyAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
                    emptyAmount.setGravity(Gravity.RIGHT);
                    emptyAmount.setLayoutParams(contentParams);
                    totalRow.addView(emptyAmount);

                    TextView incomeAmount = new TextView(getActivity());
                    incomeAmount.setText(String.valueOf(sIncome_total));
                    incomeAmount.setTextColor(R.color.black);
                    incomeAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
                    incomeAmount.setGravity(Gravity.RIGHT);
                    incomeAmount.setLayoutParams(contentParams);
                    totalRow.addView(incomeAmount);

                    TextView expenseAmount = new TextView(getActivity());
                    expenseAmount.setText(String.valueOf(sExpense_total));
                    expenseAmount.setTextColor(R.color.black);
                    expenseAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
                    expenseAmount.setGravity(Gravity.RIGHT);
                    expenseAmount.setLayoutParams(contentParams);
                    totalRow.addView(expenseAmount);

                    TextView blpAmount = new TextView(getActivity());
                    blpAmount.setText(String.valueOf(sBeforeLoanPay_total));
                    blpAmount.setTextColor(R.color.black);
                    blpAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
                    blpAmount.setGravity(Gravity.RIGHT);
                    blpAmount.setLayoutParams(contentParams);
                    totalRow.addView(blpAmount);

                    TextView loansAmount = new TextView(getActivity());
                    loansAmount.setText(String.valueOf(sLoans_total));
                    loansAmount.setTextColor(R.color.black);
                    loansAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
                    loansAmount.setGravity(Gravity.RIGHT);
                    loansAmount.setLayoutParams(contentParams);
                    totalRow.addView(loansAmount);

                    TextView surplusAmount = new TextView(getActivity());
                    surplusAmount.setText(String.valueOf(sSurplus_total));
                    surplusAmount.setTextColor(R.color.black);
                    surplusAmount.setPadding(5, 5, 5, 5);// (5, 10, 100, 10);
                    surplusAmount.setGravity(Gravity.RIGHT);
                    surplusAmount.setLayoutParams(contentParams);
                    totalRow.addView(surplusAmount);

                    confirmationTable.addView(totalRow,
                            new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                    mEdit_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Edit_button);
                    mEdit_RaisedButton.setText(AppStrings.edit);
                    mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
                    // 205,
                    // 0));
                    mEdit_RaisedButton.setOnClickListener(this);

                    mOk_RaisedButton = (Button) dialogView.findViewById(R.id.fragment_Ok_button);
                    mOk_RaisedButton.setText(AppStrings.yes);
                    mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
                    mOk_RaisedButton.setOnClickListener(this);

                    confirmationDialog.getWindow()
                            .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    confirmationDialog.setCanceledOnTouchOutside(false);
                    confirmationDialog.setContentView(dialogView);
                    confirmationDialog.setCancelable(true);
                    confirmationDialog.show();

                    ViewGroup.MarginLayoutParams margin = (ViewGroup.MarginLayoutParams) dialogView.getLayoutParams();
                    margin.leftMargin = 10;
                    margin.rightMargin = 10;
                    margin.topMargin = 10;
                    margin.bottomMargin = 10;
                    margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);

                } /*
                 * else if (sPL_total > Integer.parseInt(SelectedGroupsTask.sCashinHand)) {
                 *
                 * TastyToast.makeText(getActivity(), AppStrings.cashinHandAlert,
                 * TastyToast.LENGTH_SHORT, TastyToast.WARNING);
                 *
                 * sSendToServer_EditInternalLoan = Reset.reset(sSendToServer_EditInternalLoan);
                 * sPL_total = Integer.parseInt(nullAmount);
                 *
                 * }
                 */ else if (Boolean.valueOf(isnullAmountError) || (sPL_total == Integer.parseInt(nullAmount))) {

                    isnullAmountError = false;
                    TastyToast.makeText(getActivity(), AppStrings.nullAlert, TastyToast.LENGTH_SHORT,
                            TastyToast.WARNING);
                    sPL_total = Integer.parseInt(nullAmount);
                    sIncome_total = Integer.parseInt(nullAmount);
                    sExpense_total = Integer.parseInt(nullAmount);
                    sBeforeLoanPay_total = Integer.parseInt(nullAmount);
                    sLoans_total = Integer.parseInt(nullAmount);
                    sSurplus_total = Integer.parseInt(nullAmount);

                } else if (Boolean.valueOf(isError)) {

                    isError = false;

                    TastyToast.makeText(getActivity(), AppStrings.choosePOLAlert, TastyToast.LENGTH_SHORT,
                            TastyToast.WARNING);
                    sPL_total = Integer.parseInt(nullAmount);
                    sIncome_total = Integer.parseInt(nullAmount);
                    sExpense_total = Integer.parseInt(nullAmount);
                    sBeforeLoanPay_total = Integer.parseInt(nullAmount);
                    sLoans_total = Integer.parseInt(nullAmount);
                    sSurplus_total = Integer.parseInt(nullAmount);

                }


                break;

            case R.id.fragment_Edit_button:
                sPL_total = Integer.parseInt(nullAmount);
                mSelected_POL_Values = "";
                confirmationDialog.dismiss();
                break;
            case R.id.fragment_Ok_button:

                String shgId = shgDto.getShgId();
                Calendar calender = Calendar.getInstance();

                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String formattedDate = df.format(calender.getTime());

                DateFormat simple = new SimpleDateFormat("yyyy-MM-dd");
                Date d = new Date(Long.parseLong(shgDto.getLastTransactionDate()));
                String dateStr = simple.format(d);
                mcplandto.setGroupId(shgId);
                mcplandto.setMobileDate(formattedDate);
                mcplandto.setTransactionDate(dateStr);
                mcplandto.setMinutesofMeetingId(momid);
                mcplandto.setMembersDetails(arrLoanType);
                String sreqString = new Gson().toJson(mcplandto);
                if (networkConnection.isNetworkAvailable()) {
                    RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.LOANAPPLICATION, sreqString, getActivity(), ServiceType.LOANAPPLICATION);
                }
                break;
        }
    }

    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        try {
            switch (serviceType) {

                case GET_MOM_LOANTYPE:
                    mcpdto = new Gson().fromJson(result, ResponseDto.class);
                    if (mcpdto.getStatusCode() == Utils.Success_Code) {
                        loantypeslist = mcpdto.getResponseContent().getPurposeOfInternalLoanTypes();
                        init();
                    }else{
                        if (mcpdto.getStatusCode() == 401) {

                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                        }
                    }
                    break;
                case LOANAPPLICATION:
                    ResponseDto resdto = new Gson().fromJson(result, ResponseDto.class);
                    // mcpdto = new Gson().fromJson(result, ResponseDto.class);

                    if (resdto.getStatusCode() == Utils.Success_Code) {

                        if (Attendance.flag == "1") {
                            confirmationDialog.dismiss();
                            StepwiseFinalReports stepwiseFinalReports = new StepwiseFinalReports();
                            FragmentManager fm = getFragmentManager();
                            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                            NewDrawerScreen.showFragment(stepwiseFinalReports);
                        }
                        else {
                            confirmationDialog.dismiss();
                            String message = resdto.getMessage();
                            FragmentManager fm = getFragmentManager();
                            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                            MainFragment mainFragment = new MainFragment();
                            Bundle bundles = new Bundle();
                            bundles.putString("Meeting",MainFragment.Flag_Meeting);
                            mainFragment.setArguments(bundles);
                            NewDrawerScreen.showFragment(mainFragment);
                            Utils.showToast(getActivity(), message);
                        }
                    }
                    else {

                        if (resdto.getStatusCode() == 401) {
                            Log.e("Group Logout", "Logout Sucessfully");
                            AppDialogUtils.showConfirmation_LogoutDialog(getActivity());
                        }
                        confirmationDialog.dismiss();
                        String message = resdto.getMessage();
                        Utils.showToast(getActivity(), message);
                    }

                    break;

            }
        } catch (Exception e) {

        }

    }

    private void onChangeWidthOfColumn() {
        // TODO Auto-generated method stub

        leftHeaderColumnWidth = new int[((TableRow) mLeftHeaderTable.getChildAt(0)).getChildCount()];

        leftContentColumnWidth = new int[((TableRow) mLeftContentTable.getChildAt(0)).getChildCount()];

        TableRow leftHeaderRow = (TableRow) mLeftHeaderTable.getChildAt(0);
        TableRow leftContentRow = (TableRow) mLeftContentTable.getChildAt(0);

        for (int i = 0; i < leftHeaderRow.getChildCount(); i++) {
            leftHeaderColumnWidth[i] = this.viewWidth(leftHeaderRow.getChildAt(i));
            leftContentColumnWidth[i] = this.viewWidth(leftContentRow.getChildAt(i));
        }

        for (int i = 0; i < leftHeaderColumnWidth.length; i++) {

            if (leftHeaderColumnWidth[i] > leftContentColumnWidth[i]) {
                TextView leftContentTextView = (TextView) leftContentRow.getChildAt(i);
                leftContentTextView.setLayoutParams(
                        new TableRow.LayoutParams(leftHeaderColumnWidth[i], ViewGroup.LayoutParams.MATCH_PARENT));
            } else {
                TextView leftHeaderTextView = (TextView) leftHeaderRow.getChildAt(i);
                leftHeaderTextView.setLayoutParams(
                        new TableRow.LayoutParams(leftContentColumnWidth[i] + 10, ViewGroup.LayoutParams.MATCH_PARENT));
            }
        }

        rightHeaderColumnWidth = new int[((TableRow) mRightHeaderTable.getChildAt(0)).getChildCount()];
        TableRow headerRow = (TableRow) mRightHeaderTable.getChildAt(0);

        rightContentColumnWidth = new int[((TableRow) mRightContentTable.getChildAt(0)).getChildCount()];

        TableRow contentRow = (TableRow) mRightContentTable.getChildAt(0);

        for (int i = 0; i < headerRow.getChildCount(); i++) {
            rightHeaderColumnWidth[i] = this.viewWidth(headerRow.getChildAt(i));

            rightContentColumnWidth[i] = this.viewWidth(contentRow.getChildAt(i));

        }

        for (int i = 0; i < rightHeaderColumnWidth.length; i++) {

            if (rightHeaderColumnWidth[i] > rightContentColumnWidth[i]) {
                if ((i != 1)) {
                    EditText editTextView = (EditText) contentRow.getChildAt(i);
                    TableRow.LayoutParams editTextParams = new TableRow.LayoutParams(rightHeaderColumnWidth[i],
                            ViewGroup.LayoutParams.MATCH_PARENT);
                    editTextParams.setMargins(10, 5, 10, 5);
                    editTextView.setLayoutParams(editTextParams);

                } else {
                    TextView contentTextView = (TextView) contentRow.getChildAt(i);
                    contentTextView.setLayoutParams(
                            new TableRow.LayoutParams(rightHeaderColumnWidth[i], ViewGroup.LayoutParams.MATCH_PARENT));
                }

            } else {
                TextView headerTextView = (TextView) headerRow.getChildAt(i);
                TableRow.LayoutParams params = new TableRow.LayoutParams(rightContentColumnWidth[i],
                        ViewGroup.LayoutParams.MATCH_PARENT);
                params.setMargins(10, 0, 10, 0);
                headerTextView.setLayoutParams(params);

            }
        }

    }

    private int viewWidth(View view) {
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        return view.getMeasuredWidth();
    }

    public void init() {
        try {

            mGroupName = (TextView) view.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());

            mCashinHand = (TextView) view.findViewById(R.id.cashinHand);
            mCashinHand.setText(com.oasys.eshakti.digitization.OasysUtils.AppStrings.cashinhand + shgDto.getCashInHand());

            mCashatBank = (TextView) view.findViewById(R.id.cashatBank);
            mCashatBank.setText(com.oasys.eshakti.digitization.OasysUtils.AppStrings.cashatBank + shgDto.getCashAtBank());


            mHeader = (TextView) view.findViewById(R.id.mcp_fragmentHeader);

            mLeftHeaderTable = (TableLayout) view.findViewById(R.id.mcp_LeftHeaderTable);
            mRightHeaderTable = (TableLayout) view.findViewById(R.id.mcp_RightHeaderTable);
            mLeftContentTable = (TableLayout) view.findViewById(R.id.mcp_LeftContentTable);
            mRightContentTable = (TableLayout) view.findViewById(R.id.mcp_RightContentTable);

            mHSRightHeader = (CustomHorizontalScrollView) view.findViewById(R.id.mcp_rightHeaderHScrollView);
            mHSRightContent = (CustomHorizontalScrollView) view.findViewById(R.id.mcp_rightContentHScrollView);
            mHSRightHeader.setOnScrollChangedListener(new CustomHorizontalScrollView.onScrollChangedListener() {

                public void onScrollChanged(int l, int t, int oldl, int oldt) {
                    // TODO Auto-generated method stub

                    mHSRightContent.scrollTo(l, 0);

                }
            });

            mHSRightContent.setOnScrollChangedListener(new CustomHorizontalScrollView.onScrollChangedListener() {
                @Override
                public void onScrollChanged(int l, int t, int oldl, int oldt) {
                    // TODO Auto-generated method stub
                    mHSRightHeader.scrollTo(l, 0);
                }
            });

            TableRow leftHeaderRow = new TableRow(getActivity());

            TableRow.LayoutParams lHeaderParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);

            TextView mMemberName_Header = new TextView(getActivity());
            mMemberName_Header.setText(String.valueOf(AppStrings.memberName));
            mMemberName_Header.setTextColor(Color.WHITE);
            mMemberName_Header.setPadding(10, 5, 10, 5);
            mMemberName_Header.setLayoutParams(lHeaderParams);
            leftHeaderRow.addView(mMemberName_Header);

            mLeftHeaderTable.addView(leftHeaderRow);

            TableRow rightHeaderRow = new TableRow(getActivity());
            TableRow.LayoutParams rHeaderParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            rHeaderParams.setMargins(10, 0, 10, 0);

            TextView mPL_Header = new TextView(getActivity());
            mPL_Header.setText(String.valueOf(AppStrings.amount));
            mPL_Header.setTextColor(Color.WHITE);
            mPL_Header.setPadding(10, 5, 10, 5);
            mPL_Header.setGravity(Gravity.CENTER);
            mPL_Header.setLayoutParams(rHeaderParams);
            mPL_Header.setBackgroundResource(R.color.tableHeader);
            rightHeaderRow.addView(mPL_Header);

            TableRow.LayoutParams POLParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            POLParams.setMargins(10, 0, 10, 0);

            TextView mPurposeOfLoan_Header = new TextView(getActivity());
            mPurposeOfLoan_Header
                    .setText(String.valueOf(AppStrings.purposeOfLoan));
            mPurposeOfLoan_Header.setTextColor(Color.WHITE);
            mPurposeOfLoan_Header.setGravity(Gravity.RIGHT);
            mPurposeOfLoan_Header.setLayoutParams(rHeaderParams);
            mPurposeOfLoan_Header.setPadding(10, 5, 10, 5);
            mPurposeOfLoan_Header.setBackgroundResource(R.color.tableHeader);
            rightHeaderRow.addView(mPurposeOfLoan_Header);

            incomeHeaderParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            rHeaderParams.setMargins(10, 0, 10, 0);

            TextView mIncome_Header = new TextView(getActivity());
            mIncome_Header.setText(String.valueOf(AppStrings.income));
            mIncome_Header.setTextColor(Color.WHITE);
            mIncome_Header.setPadding(10, 5, 10, 5);
            mIncome_Header.setGravity(Gravity.CENTER);
            mIncome_Header.setLayoutParams(rHeaderParams);
            mIncome_Header.setBackgroundResource(R.color.tableHeader);
            rightHeaderRow.addView(mIncome_Header);

            expenseHeaderParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            rHeaderParams.setMargins(10, 0, 10, 0);

            TextView mExpense_Header = new TextView(getActivity());
            mExpense_Header.setText(String.valueOf(AppStrings.mExpense));
            mExpense_Header.setTextColor(Color.WHITE);
            mExpense_Header.setPadding(10, 5, 10, 5);
            mExpense_Header.setGravity(Gravity.CENTER);
            mExpense_Header.setLayoutParams(rHeaderParams);
            mExpense_Header.setBackgroundResource(R.color.tableHeader);
            rightHeaderRow.addView(mExpense_Header);

            blpHeaderParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            rHeaderParams.setMargins(10, 0, 10, 0);

            TextView mBeforeLoanPay_Header = new TextView(getActivity());
            mBeforeLoanPay_Header
                    .setText(String.valueOf(AppStrings.mBeforeLoanPay));
            mBeforeLoanPay_Header.setTextColor(Color.WHITE);
            mBeforeLoanPay_Header.setPadding(10, 5, 10, 5);
            mBeforeLoanPay_Header.setSingleLine(true);
            mBeforeLoanPay_Header.setGravity(Gravity.RIGHT);
            mBeforeLoanPay_Header.setLayoutParams(rHeaderParams);
            mBeforeLoanPay_Header.setBackgroundResource(R.color.tableHeader);
            rightHeaderRow.addView(mBeforeLoanPay_Header);

            loansHeaderParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            rHeaderParams.setMargins(10, 0, 10, 0);

            TextView mLoans_Header = new TextView(getActivity());
            mLoans_Header.setText(String.valueOf(AppStrings.mLoans));
            mLoans_Header.setTextColor(Color.WHITE);
            mLoans_Header.setPadding(10, 5, 10, 5);
            mLoans_Header.setGravity(Gravity.CENTER);
            mLoans_Header.setLayoutParams(rHeaderParams);
            mLoans_Header.setBackgroundResource(R.color.tableHeader);
            rightHeaderRow.addView(mLoans_Header);

            surplusHeaderParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            rHeaderParams.setMargins(10, 0, 10, 0);

            TextView mSurplus_Header = new TextView(getActivity());
            mSurplus_Header.setText(String.valueOf(AppStrings.mSurplus));
            mSurplus_Header.setTextColor(Color.WHITE);
            mSurplus_Header.setPadding(10, 5, 10, 5);
            mSurplus_Header.setGravity(Gravity.CENTER);
            mSurplus_Header.setLayoutParams(rHeaderParams);
            mSurplus_Header.setBackgroundResource(R.color.tableHeader);
            rightHeaderRow.addView(mSurplus_Header);

            mRightHeaderTable.addView(rightHeaderRow);
            try {

                for (int j = 0; j < memList.size(); j++) {

                    TableRow leftContentRow = new TableRow(getActivity());

                    TableRow.LayoutParams leftContentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                    leftContentParams.setMargins(5, 5, 5, 5);

                    final TextView memberName_Text = new TextView(getActivity());
                    memberName_Text.setText(memList.get(j).getMemberName());
                    memberName_Text.setTextColor(R.color.black);
                    memberName_Text.setPadding(5, 35, 5, 5);
                    memberName_Text.setLayoutParams(leftContentParams);
                    memberName_Text.setWidth(200);
                    memberName_Text.setSingleLine(true);
                    memberName_Text.setEllipsize(TextUtils.TruncateAt.END);
                    leftContentRow.addView(memberName_Text);

                    mLeftContentTable.addView(leftContentRow);

                    TableRow rightContentRow = new TableRow(getActivity());

                    TableRow.LayoutParams rightContentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                    rightContentParams.setMargins(10, 5, 10, 5);

                    mPL_values = new EditText(getActivity());
                    mPL_values.setId(j);
                    sPL_Fields.add(mPL_values);
                    mPL_values.setInputType(InputType.TYPE_CLASS_NUMBER);
                    mPL_values.setPadding(5, 5, 5, 5);
                    mPL_values.setFilters(Get_EdiText_Filter.editText_filter());
                    mPL_values.setBackgroundResource(R.drawable.edittext_background);
                    mPL_values.setLayoutParams(rightContentParams);
                    mPL_values.setWidth(150);
                    mPL_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            // TODO Auto-generated method stub
                            if (hasFocus) {
                                ((EditText) v).setGravity(Gravity.LEFT);

                            } else {

                            }
                        }
                    });
                    rightContentRow.addView(mPL_values);

                    TableRow.LayoutParams contentRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                    contentRowParams.setMargins(10, 5, 10, 5);

                    mLoanType = new TextView(getActivity());
                    mLoanType.setText(loanType);
                    mLoanType.setId(j);
                    mLoanType.setLayoutParams(rightContentParams);// (rightContentParams);
                    mLoanType.setPadding(10, 0, 10, 5);
                    sPOL_Fields.add(mLoanType);
                    mLoanType.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(final View v) {
                            // TODO Auto-generated method stub

                            try {

                                final Dialog ChooseLoanType = new Dialog(getActivity());

                                LayoutInflater li = (LayoutInflater) getActivity()
                                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                final View dialogView = li.inflate(R.layout.dialog_loantype, null, false);

                                TextView confirmationHeader = (TextView) dialogView
                                        .findViewById(R.id.dialog_ChooseLabel);
                                confirmationHeader
                                        .setText("CHOOSE THE LOAN");

                                int radioColor = getResources().getColor(R.color.pink);
                                final RadioGroup radioGroup = (RadioGroup) dialogView
                                        .findViewById(R.id.dialog_RadioGroup);
                                radioGroup.removeAllViews();

                                for (int j = 0; j < loantypeslist.size(); j++) {

                                    radioButton = new RadioButton(getActivity());
                                    radioButton.setText(loantypeslist.get(j).getLoanTypeName());
                                    radioButton.setId(j);
                                    int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                                    if (currentapiVersion >= android.os.Build.VERSION_CODES.LOLLIPOP) {

                                        radioButton.setButtonTintList(ColorStateList.valueOf(radioColor));
                                    }
                                    radioGroup.addView(radioButton);

                                }

                                ButtonFlat okButton = (ButtonFlat) dialogView.findViewById(R.id.dialog_yes_button);
                                okButton.setText("OK");

                                okButton.setOnClickListener(new View.OnClickListener() {

                                    @Override
                                    public void onClick(View view) {
                                        // TODO Auto-generated method
                                        // stub

                                        System.out.println(">>>>>>>>>>>>  : " + radioButton.getId());

                                        selectedId = radioGroup.getCheckedRadioButtonId();
                                        //Log.e(TAG, String.valueOf(selectedId));

                                        if ((selectedId != 100) && (selectedId != (-1))) {

                                            // find the radiobutton by
                                            // returned id
                                            RadioButton radioLoanButton = (RadioButton) dialogView
                                                    .findViewById(selectedId);

                                            mSelectedLoanType = radioLoanButton.getText().toString();
                                            Log.v("On Selected LOAN TYPE", mSelectedLoanType);

                                            Log.v("view ID check : ", String.valueOf(v.getId()));

                                            TextView selectedTextView = (TextView) v.findViewById(v.getId());
                                            selectedTextView.setText(mSelectedLoanType);


                                            int id = radioButton.getId();
                                            //Log.d(TAG, String.valueOf(id));

                                            // for (int i = 0; i <
                                            // sSelected_POL.length; i++) {
                                        /*for (int i = 0; i < sSelected_POL.length; i++) {

                                            if (i == v.getId()) {
                                                sSelected_POL[i] = String.valueOf(mPOL_ID[selectedId]);


                                            } else if (sSelected_POL[i] != null) {
                                                sSelected_POL[i] = sSelected_POL[i];
                                            } else {
                                                sSelected_POL[i] = "0";
                                            }

                                        }*/

                                            ChooseLoanType.dismiss();

                                        } else {
                                            TastyToast.makeText(getActivity(), AppStrings.choosePOLAlert,
                                                    TastyToast.LENGTH_SHORT, TastyToast.ERROR);

                                        }

                                    }

                                });

                                ChooseLoanType.getWindow()
                                        .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                ChooseLoanType.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                ChooseLoanType.setCanceledOnTouchOutside(false);
                                ChooseLoanType.setContentView(dialogView);
                                ChooseLoanType.setCancelable(true);
                                ChooseLoanType.show();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    });
                    rightContentRow.addView(mLoanType);

                    mIncome_values = new EditText(getActivity());
                    mIncome_values.setId(j);
                    sIncome_Fields.add(mIncome_values);
                    mIncome_values.setInputType(InputType.TYPE_CLASS_NUMBER);
                    mIncome_values.setPadding(5, 5, 5, 5);
                    mIncome_values.setFilters(Get_EdiText_Filter.editText_filter());
                    mIncome_values.setBackgroundResource(R.drawable.edittext_background);
                    mIncome_values.setLayoutParams(rightContentParams);
                    mIncome_values.setWidth(150);
                    mIncome_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            // TODO Auto-generated method stub
                            if (hasFocus) {

                            } else {

                            }
                        }
                    });
                    rightContentRow.addView(mIncome_values);

                    mExpense_values = new EditText(getActivity());
                    mExpense_values.setId(j);
                    sExpense_Fields.add(mExpense_values);
                    mExpense_values.setInputType(InputType.TYPE_CLASS_NUMBER);
                    mExpense_values.setPadding(5, 5, 5, 5);
                    mExpense_values.setFilters(Get_EdiText_Filter.editText_filter());
                    mExpense_values.setBackgroundResource(R.drawable.edittext_background);
                    mExpense_values.setLayoutParams(rightContentParams);
                    mExpense_values.setWidth(150);
                    mExpense_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            // TODO Auto-generated method stub
                            if (hasFocus) {

                            } else {

                            }
                        }
                    });

                    rightContentRow.addView(mExpense_values);

                    mBeforeLoanPay_values = new EditText(getActivity());
                    mBeforeLoanPay_values.setId(j);
                    sBefoeLoanPay_Fields.add(mBeforeLoanPay_values);
                    mBeforeLoanPay_values.setInputType(InputType.TYPE_CLASS_NUMBER);
                    mBeforeLoanPay_values.setPadding(5, 5, 5, 5);
                    mBeforeLoanPay_values.setFilters(Get_EdiText_Filter.editText_filter());
                    mBeforeLoanPay_values.setBackgroundResource(R.drawable.edittext_background);
                    mBeforeLoanPay_values.setLayoutParams(rightContentParams);
                    mBeforeLoanPay_values.setWidth(150);
                    mBeforeLoanPay_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            // TODO Auto-generated method stub
                            if (hasFocus) {

                            } else {

                            }
                        }
                    });

                    rightContentRow.addView(mBeforeLoanPay_values);

                    mLoans_values = new EditText(getActivity());
                    mLoans_values.setId(j);
                    sLoans_Fields.add(mLoans_values);
                    mLoans_values.setInputType(InputType.TYPE_CLASS_NUMBER);
                    mLoans_values.setPadding(5, 5, 5, 5);
                    mLoans_values.setFilters(Get_EdiText_Filter.editText_filter());
                    mLoans_values.setBackgroundResource(R.drawable.edittext_background);
                    mLoans_values.setLayoutParams(rightContentParams);
                    mLoans_values.setWidth(150);
                    mLoans_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            // TODO Auto-generated method stub
                            if (hasFocus) {

                            } else {

                            }
                        }
                    });

                    rightContentRow.addView(mLoans_values);

                    mSurplus_values = new EditText(getActivity());
                    mSurplus_values.setId(j);
                    sSurplus_Fields.add(mSurplus_values);
                    mSurplus_values.setInputType(InputType.TYPE_CLASS_NUMBER);
                    mSurplus_values.setPadding(5, 5, 5, 5);
                    mSurplus_values.setFilters(Get_EdiText_Filter.editText_filter());
                    mSurplus_values.setBackgroundResource(R.drawable.edittext_background);
                    mSurplus_values.setLayoutParams(rightContentParams);
                    mSurplus_values.setWidth(150);
                    mSurplus_values.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            // TODO Auto-generated method stub
                            if (hasFocus) {

                            } else {

                            }
                        }
                    });

                    rightContentRow.addView(mSurplus_values);

                    mRightContentTable.addView(rightContentRow);

                }

            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();

                TastyToast.makeText(getActivity(), AppStrings.adminAlert, TastyToast.LENGTH_SHORT, TastyToast.WARNING);

                getActivity().finish();
            }

            mSubmit_Raised_Button = (Button) view.findViewById(R.id.mcp_fragment_Submit_button);
            mSubmit_Raised_Button.setText("SUBMIT");
            mSubmit_Raised_Button.setOnClickListener(this);

            onChangeWidthOfColumn();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
