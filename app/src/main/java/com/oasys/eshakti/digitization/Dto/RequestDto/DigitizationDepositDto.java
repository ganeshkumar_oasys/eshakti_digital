package com.oasys.eshakti.digitization.Dto.RequestDto;

import com.oasys.eshakti.digitization.Dto.ListMemberDeposit;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.Data;

@Data
public class DigitizationDepositDto implements Serializable {

    private String agentId;

    private String merchantId;

    private String shgId;

    private ArrayList<ListMemberDeposit> listMemberDeposit;

    private String animatorNameId;

    private String transactionDate;

    private String userId;

    private String group;
}
