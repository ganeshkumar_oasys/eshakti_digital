package com.oasys.eshakti.digitization.Dto;

import java.io.Serializable;

import lombok.Data;

/**
 * Created by MuthukumarPandi on 12/11/2018.
 */
@Data
public class ShgBankDTOList implements Serializable {
    private String accountNumber;

    private String branchName;

    private String bankName;

    private String bankId;
}
