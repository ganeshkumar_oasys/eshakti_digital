package com.oasys.eshakti.digitization.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.digitization.Adapter.CustomItemAdapter;
import com.oasys.eshakti.digitization.Dto.InternalLoanEntry;
import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.Dto.MemberList;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.Dto.ShgBankDetails;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.GetSpanText;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.activity.LoginActivity;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;
import com.oasys.eshakti.digitization.database.BankTable;
import com.oasys.eshakti.digitization.database.MemberTable;
import com.oasys.eshakti.digitization.database.SHGTable;
import com.oasys.eshakti.digitization.model.RowItem;
import com.oasys.eshakti.digitization.views.Get_EdiText_Filter;
import com.oasys.eshakti.digitization.views.MaterialSpinner;
import com.oasys.eshakti.digitization.views.RaisedButton;
import com.tutorialsee.lib.TastyToast;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.OasysUtils.AppDialogUtils;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Dell on 15 Dec, 2018.
 */

public class LD_EL_IncreaseLimit extends Fragment implements View.OnClickListener, NewTaskListener {

    private TextView mGroupName, mCashInHand, mCashAtBank, mHeader;
    private TextView mSanctionAmountText, mDisbursementAmountText, mSanctionAmount_value, mDisbursementAmount_value,
            mIncreaseLimitText, mBankChargesText, mBalance_Text, mBalance_value;
    private EditText mIncreaseLimit_editText, mBankCharges_editText;
    private RaisedButton mSubmitButton;
    View rootView;
    public static String increaseLimit = "", bankCharges = "";
    private MaterialSpinner mTenureSpinner;
    private String[] mTenureArr, mTenureArray;
    private List<RowItem> tenureItems;
    private CustomItemAdapter tenureAdapter;
    public static String mTenure_value = "";
    private Dialog mProgressDialog;
    Dialog confirmationDialog;
    private Button mEdit_RaisedButton, mOk_RaisedButton;
    public static String[] response;
    public static String mGroupOS_Offlineresponse = null;
    String mLastTrDate = null;
    boolean isLastTransactionValues = false;
    Date date_dashboard, date_loanDisb;

    private int mSize;
    private List<MemberList> memList;
    private ListOfShg shgDto;
    private NetworkConnection networkConnection;
    private ArrayList<ShgBankDetails> bankdetails;

    public static InternalLoanEntry lEntry = new InternalLoanEntry();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_new_increase_limit, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mSize = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, "")).size();
        memList = MemberTable.getMemberList(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        bankdetails = BankTable.getBankName(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));
        networkConnection = NetworkConnection.getNetworkConnection(getActivity().getApplicationContext());
        init();

        if (networkConnection.isNetworkAvailable()) {
            onTaskStarted();//  onTaskStarted();
            RestClient.getRestClient(this).callWebServiceForGetMethod(Constants.BASE_URL + Constants.LD_INC_LIMIT + LD_ExternalLoan.sExistingLoagSelection.getLoanId(), getActivity(), ServiceType.LD_INC_LIMIT);
        }
    }

    private void init() {
        try {
            mGroupName = (TextView) rootView.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);
            mCashInHand = (TextView) rootView.findViewById(R.id.cashinHand);
            mCashInHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashInHand.setTypeface(LoginActivity.sTypeface);
            mCashAtBank = (TextView) rootView.findViewById(R.id.cashatBank);
            mCashAtBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashAtBank.setTypeface(LoginActivity.sTypeface);
            mHeader = (TextView) rootView.findViewById(R.id.increaseLimitheader);
            mSanctionAmountText = (TextView) rootView.findViewById(R.id.sanctionAmountTextView);
            mSanctionAmount_value = (TextView) rootView.findViewById(R.id.sanctionAmount_values);
            mDisbursementAmountText = (TextView) rootView.findViewById(R.id.disbursementAmountTextView);
            mDisbursementAmount_value = (TextView) rootView.findViewById(R.id.disbursementAmount_values);
            mIncreaseLimitText = (TextView) rootView.findViewById(R.id.increase_limitTextView);
            mBankChargesText = (TextView) rootView.findViewById(R.id.bankChargesTextView);

            mBalance_Text = (TextView) rootView.findViewById(R.id.balanceAmountTextView);
            mBalance_value = (TextView) rootView.findViewById(R.id.balanceAmount_values);

            mHeader.setText(AppStrings.mIncreaseLimit);
            mSanctionAmountText.setText(AppStrings.mSanctionAmount);
            mDisbursementAmountText.setText(AppStrings.mBankCharges);
            mIncreaseLimitText.setText(AppStrings.mIncreaseLimit);
            mBankChargesText.setText(AppStrings.mBankCharges);
            mBalance_Text.setText(AppStrings.tenure);

            mHeader.setTypeface(LoginActivity.sTypeface);
            mSanctionAmountText.setTypeface(LoginActivity.sTypeface);
            mDisbursementAmountText.setTypeface(LoginActivity.sTypeface);
            mIncreaseLimitText.setTypeface(LoginActivity.sTypeface);
            mBankChargesText.setTypeface(LoginActivity.sTypeface);
            mBalance_Text.setTypeface(LoginActivity.sTypeface);


          /*  Log.e("publicValue loanBalnce", "" + "");
            if ("" != null) {
                String[] mValues = "".split("#");
                String mValues_limit = mValues[0];
                String[] mLimitValues = mValues_limit.split("~");
                mSanctionAmount_value.setText(mLimitValues[0]);
                mDisbursementAmount_value.setText(mLimitValues[1]);
                mBalance_value.setText(mLimitValues[2]);

            }
*/
            mIncreaseLimit_editText = (EditText) rootView.findViewById(R.id.increase_limit_value);
            mIncreaseLimit_editText.setInputType(InputType.TYPE_CLASS_NUMBER);
            mIncreaseLimit_editText.setPadding(5, 5, 5, 5);
            mIncreaseLimit_editText.setFilters(Get_EdiText_Filter.editText_filter());
            mIncreaseLimit_editText.setBackgroundResource(R.drawable.edittext_background);

            mBankCharges_editText = (EditText) rootView.findViewById(R.id.bankCharges_value);
            mBankCharges_editText.setInputType(InputType.TYPE_CLASS_NUMBER);
            mBankCharges_editText.setPadding(5, 5, 5, 5);
            mBankCharges_editText.setFilters(Get_EdiText_Filter.editText_filter());
            mBankCharges_editText.setBackgroundResource(R.drawable.edittext_background);

            // Tenure spinner
            mTenureSpinner = (MaterialSpinner) rootView.findViewById(R.id.increaselimit_tenure_spinner);
            mTenureSpinner.setBaseColor(R.color.grey_400);
            mTenureSpinner.setFloatingLabelText(AppStrings.tenure);
            mTenureSpinner.setPaddingSafe(10, 0, 10, 0);

            mTenureArr = new String[]{"6", "10", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23",
                    "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40",
                    "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57",
                    "58", "59", "60"};
            mTenureArray = new String[mTenureArr.length + 1];
            mTenureArray[0] = String.valueOf(AppStrings.tenure);
            for (int i = 0; i < mTenureArr.length; i++) {
                mTenureArray[i + 1] = mTenureArr[i].toString();
            }

            tenureItems = new ArrayList<RowItem>();
            for (int i = 0; i < mTenureArray.length; i++) {
                RowItem rowItem = new RowItem(mTenureArray[i]);
                tenureItems.add(rowItem);
            }
            tenureAdapter = new CustomItemAdapter(getActivity(), tenureItems);
            mTenureSpinner.setAdapter(tenureAdapter);
            mTenureSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    // TODO Auto-generated method stub
                    Log.d("TENURE POSITION", String.valueOf(position));

                    String selectedItem;
                    if (position == 0) {
                        selectedItem = mTenureArray[0];
                        mTenure_value = "0";
                    } else {
                        selectedItem = mTenureArray[position];
                        System.out.println("SELECTED TENURE:" + selectedItem);

                        mTenure_value = selectedItem;

                    }
                    Log.e("Tenure value", mTenure_value);

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    // TODO Auto-generated method stub

                }
            });

            mSubmitButton = (RaisedButton) rootView.findViewById(R.id.increaseLimit_submit);
            mSubmitButton.setText(AppStrings.submit);
            mSubmitButton.setTypeface(LoginActivity.sTypeface);
            mSubmitButton.setOnClickListener(this);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.increaseLimit_submit:

                increaseLimit = "";
                bankCharges = "";

                increaseLimit = mIncreaseLimit_editText.getText().toString();
                bankCharges = mBankCharges_editText.getText().toString();

                if (increaseLimit.equals("") || increaseLimit == null) {
                    increaseLimit = "0";
                }

                if (bankCharges.equals("") || bankCharges == null) {
                    bankCharges = "0";
                }

                if (!increaseLimit.equals("0")) {
                    // String dashBoardDate = DatePickerDialog.sDashboardDate;
                   /* String dashBoardDate = null;

                    String loanDisbArr[] = EShaktiApplication.getLoanAcc_LoanDisbursementDate().split("/");
                    String loanDisbDate = loanDisbArr[1] + "-" + loanDisbArr[0] + "-" + loanDisbArr[2];

                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

                    try {
                        date_dashboard = sdf.parse(dashBoardDate);
                        date_loanDisb = sdf.parse(loanDisbDate);
                    } catch (ParseException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }

                    if (date_dashboard.compareTo(date_loanDisb) >= 0) {*/
                    confirmationDialog = new Dialog(getActivity());

                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.dialog_new_confirmation, null);
                    dialogView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT));

                    TextView confirmationHeader = (TextView) dialogView.findViewById(R.id.confirmationHeader);
                    confirmationHeader.setText(AppStrings.confirmation);
                    confirmationHeader.setTypeface(LoginActivity.sTypeface);

                    TableLayout confirmationTable = (TableLayout) dialogView.findViewById(R.id.confirmationTable);

                    TableRow increaseLimitRow = new TableRow(getActivity());

                    @SuppressWarnings("deprecation")
                    TableRow.LayoutParams contentParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                    contentParams.setMargins(10, 5, 10, 5);

                    TextView increaseLimitText = new TextView(getActivity());
                    increaseLimitText.setText(
                            GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mIncreaseLimit)));
                    increaseLimitText.setTypeface(LoginActivity.sTypeface);
                    increaseLimitText.setTextColor(R.color.white);
                    increaseLimitText.setPadding(5, 5, 5, 5);
                    increaseLimitText.setLayoutParams(contentParams);
                    increaseLimitRow.addView(increaseLimitText);

                    TextView increaseLimit_values = new TextView(getActivity());
                    increaseLimit_values
                            .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(increaseLimit)));
                    increaseLimit_values.setTextColor(R.color.white);
                    increaseLimit_values.setPadding(5, 5, 5, 5);
                    increaseLimit_values.setGravity(Gravity.RIGHT);
                    increaseLimit_values.setLayoutParams(contentParams);
                    increaseLimitRow.addView(increaseLimit_values);

                    confirmationTable.addView(increaseLimitRow,
                            new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                    TableRow bankChargeRow = new TableRow(getActivity());

                    TextView bankChargeText = new TextView(getActivity());
                    bankChargeText
                            .setText(GetSpanText.getSpanString(getActivity(), String.valueOf(AppStrings.mBankCharges)));
                    bankChargeText.setTextColor(R.color.white);
                    bankChargeText.setPadding(5, 5, 5, 5);
                    bankChargeText.setLayoutParams(contentParams);
                    bankChargeRow.addView(bankChargeText);

                    TextView bankCharge_values = new TextView(getActivity());
                    bankCharge_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(bankCharges)));
                    bankCharge_values.setTextColor(R.color.white);
                    bankCharge_values.setPadding(5, 5, 5, 5);
                    bankCharge_values.setGravity(Gravity.RIGHT);
                    bankCharge_values.setLayoutParams(contentParams);
                    bankChargeRow.addView(bankCharge_values);

                    confirmationTable.addView(bankChargeRow,
                            new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                    TableRow tenureRow = new TableRow(getActivity());

                    TextView tenureText = new TextView(getActivity());
                    tenureText.setText(GetSpanText.getSpanString(getActivity(), String.valueOf("")));
                    tenureText.setTextColor(R.color.white);
                    tenureText.setPadding(5, 5, 5, 5);
                    tenureText.setLayoutParams(contentParams);
                    tenureRow.addView(tenureText);

                    TextView tenure_values = new TextView(getActivity());
                    tenure_values.setText(GetSpanText.getSpanString(getActivity(), String.valueOf(mTenure_value)));
                    tenure_values.setTextColor(R.color.white);
                    tenure_values.setPadding(5, 5, 5, 5);
                    tenure_values.setGravity(Gravity.RIGHT);
                    tenure_values.setLayoutParams(contentParams);
                    tenureRow.addView(tenure_values);

                    confirmationTable.addView(tenureRow,
                            new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                    mEdit_RaisedButton = (RaisedButton) dialogView.findViewById(R.id.fragment_Edit);
                    mEdit_RaisedButton.setText(AppStrings.edit);
                    mEdit_RaisedButton.setTypeface(LoginActivity.sTypeface);
                    mEdit_RaisedButton.setTextColor(Color.rgb(0, 199, 140));// (102,
                    // 205,
                    // 0));
                    mEdit_RaisedButton.setOnClickListener(this);

                    mOk_RaisedButton = (RaisedButton) dialogView.findViewById(R.id.frag_Ok);
                    mOk_RaisedButton.setText(AppStrings.yes);
                    mOk_RaisedButton.setTypeface(LoginActivity.sTypeface);
                    mOk_RaisedButton.setTextColor(Color.rgb(0, 199, 140));
                    mOk_RaisedButton.setOnClickListener(this);

                    confirmationDialog.getWindow()
                            .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    confirmationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    confirmationDialog.setCanceledOnTouchOutside(false);
                    confirmationDialog.setContentView(dialogView);
                    confirmationDialog.setCancelable(true);
                    confirmationDialog.show();

                    ViewGroup.MarginLayoutParams margin = (ViewGroup.MarginLayoutParams) dialogView.getLayoutParams();
                    margin.leftMargin = 10;
                    margin.rightMargin = 10;
                    margin.topMargin = 10;
                    margin.bottomMargin = 10;
                    margin.setMargins(margin.leftMargin, margin.topMargin, margin.rightMargin, margin.bottomMargin);
                   /* } else {
                        TastyToast.makeText(getActivity(), AppStrings.mCheckDisbursementDate, TastyToast.LENGTH_SHORT,
                                TastyToast.WARNING);
                    }*/
                } else {
                    TastyToast.makeText(getActivity(), AppStrings.nullDetailsAlert, TastyToast.LENGTH_SHORT,
                            TastyToast.WARNING);
                }

                break;

            case R.id.fragment_Edit:
                increaseLimit = "";
                bankCharges = "";
                // mTenure_value = "0";
                mSubmitButton.setClickable(true);
                confirmationDialog.dismiss();
                break;

            case R.id.frag_Ok:
                if (confirmationDialog.isShowing())
                    confirmationDialog.dismiss();
                lEntry.setBankCharge(bankCharges);
                lEntry.setLoanLedgerId(LD_ExternalLoan.sExistingLoagSelection.getLoanId());
                lEntry.setShgId(shgDto.getShgId());
                lEntry.setIncreaseLimit(increaseLimit);
                lEntry.setLoanId(LD_ExternalLoan.sExistingLoagSelection.getLoanId());

                String sreqString = new Gson().toJson(lEntry);
                if (networkConnection.isNetworkAvailable()) {           //  onTaskStarted();
                    onTaskStarted();
                    RestClient.getRestClient(this).callRestWebService(Constants.BASE_URL + Constants.LD_EL_ILIM_DIS, sreqString, getActivity(), ServiceType.LD_EL_ILIM_DIS);
                }

                break;

        }
    }

    @Override
    public void onTaskStarted() {
        mProgressDialog = AppDialogUtils.createProgressDialog(getActivity());
        mProgressDialog.show();

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {
        if (mProgressDialog != null) {
            if ((mProgressDialog != null) && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
                mProgressDialog = null;

            }

            switch (serviceType) {
                case LD_INC_LIMIT:
                    try {
                        ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                        String message = cdto.getMessage();
                        int statusCode = cdto.getStatusCode();
                        if (statusCode == Utils.Success_Code) {
                            Utils.showToast(getActivity(), message);
                            mSanctionAmount_value.setText(cdto.getResponseContent().getSanctionAmount());
                            mDisbursementAmount_value.setText(cdto.getResponseContent().getBankCharges());

                        } else {
                            if (statusCode == 401) {

                                Log.e("Group Logout", "Logout Sucessfully");
                                AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                            }
                            Utils.showToast(getActivity(), message);
                        }
                    } catch (Exception e) {

                    }
                    break;
                case LD_EL_ILIM_DIS:
                    try {
                        ResponseDto cdto = new Gson().fromJson(result.toString(), ResponseDto.class);
                        String message = cdto.getMessage();
                        int statusCode = cdto.getStatusCode();
                        if (statusCode == Utils.Success_Code) {
                            Utils.showToast(getActivity(), message);
                            if (shgDto.getFFlag() == null || !shgDto.getFFlag().equals("1")) {
                                SHGTable.updateTransactionSHGDetails(shgDto.getShgId());
                            }
                            FragmentManager fm = getFragmentManager();
                            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                            NewDrawerScreen.showFragment(new MainFragment());

                        } else {
                            if (statusCode == 401) {

                                Log.e("Group Logout", "Logout Sucessfully");
                                AppDialogUtils.showConfirmation_LogoutDialog(getActivity());

                            }
                            Utils.showToast(getActivity(), message);
                        }
                    } catch (Exception e) {

                    }
                    break;
            }

        }
    }
}