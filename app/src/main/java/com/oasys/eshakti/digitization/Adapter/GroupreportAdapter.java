package com.oasys.eshakti.digitization.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.activity.LoginActivity;

import java.util.ArrayList;

public class GroupreportAdapter extends RecyclerView.Adapter<GroupreportAdapter.MyViewholder> {

    private Context context;
    private ArrayList<String> groupreports;

    public GroupreportAdapter(Context context, ArrayList<String> groupreports) {
        this.context = context;
        this.groupreports = groupreports;
    }

    @NonNull
    @Override
    public GroupreportAdapter.MyViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.memberreportitem, parent, false);
        return new GroupreportAdapter.MyViewholder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewholder holder, final int position) {
        holder.mGroupreportName.setText(groupreports.get(position));
        holder.mGroupreportName.setTypeface(LoginActivity.sTypeface);

    }



    @Override
    public int getItemCount() {
        return groupreports.size();
    }

    class MyViewholder extends RecyclerView.ViewHolder {
        TextView mGroupreportName;


        public MyViewholder(View itemView) {
            super(itemView);
            mGroupreportName = (TextView) itemView.findViewById(R.id.mMemberreportName);

        }
    }
}
