package com.oasys.eshakti.digitization.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasys.eshakti.digitization.Dto.GroupLoanSummaryDTOList;
import com.oasys.eshakti.digitization.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class GroupLoanSummary_Otherloan_Adapter extends RecyclerView.Adapter<GroupLoanSummary_Otherloan_Adapter.MyViewholder> {

    private Context context;
    private ArrayList<GroupLoanSummaryDTOList> groupLoanSummaryDTOLists ;
    private  String dateStr;


    public GroupLoanSummary_Otherloan_Adapter(Context context, ArrayList<GroupLoanSummaryDTOList> groupLoanSummaryDTOLists) {
        this.context = context;
        this.groupLoanSummaryDTOLists = groupLoanSummaryDTOLists;
    }

    @NonNull
    @Override
    public MyViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.groupsavingviews, parent, false);
        return new MyViewholder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewholder holder, int position) {
        Calendar calender = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
        String formattedDate = df.format(calender.getTime());

        DateFormat simple = new SimpleDateFormat("yyyy/MM/dd");


        Date d = new Date(Long.parseLong(groupLoanSummaryDTOLists.get(position).getTransactiondate()));
        dateStr = simple.format(d);

        holder.groupsavings_name.setText(dateStr);
        holder.groupsavings_amount.setText(groupLoanSummaryDTOLists.get(position).getAmount());
        holder.groupsavings_voluntarysaving.setText(groupLoanSummaryDTOLists.get(position).getInterest());

    }

    @Override
    public int getItemCount() {
        return groupLoanSummaryDTOLists.size();
    }

    class MyViewholder extends RecyclerView.ViewHolder {
        TextView groupsavings_name,groupsavings_amount,groupsavings_voluntarysaving;



        public MyViewholder(View itemView) {
            super(itemView);


            groupsavings_name = (TextView) itemView.findViewById(R.id.mGroupreportName);
            groupsavings_amount = (TextView) itemView.findViewById(R.id.mGroupreportsavings);
            groupsavings_voluntarysaving = (TextView) itemView.findViewById(R.id.mGroupreporVoluntary);

        }
    }
}
