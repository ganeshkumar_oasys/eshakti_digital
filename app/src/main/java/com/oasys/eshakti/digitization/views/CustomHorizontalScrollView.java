package com.oasys.eshakti.digitization.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.HorizontalScrollView;

public class CustomHorizontalScrollView extends HorizontalScrollView {

	public CustomHorizontalScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}
	 public interface onScrollChangedListener{
	        public abstract void onScrollChanged(int l, int t, int oldl, int oldt);
	    }
	   
	  private  onScrollChangedListener mMyScrollViewListener; 

	  public void setOnScrollChangedListener(onScrollChangedListener listener){
		  mMyScrollViewListener = listener;
	  }

	  @Override
	protected void onScrollChanged(int l, int t, int oldl, int oldt) {
		// TODO Auto-generated method stub
		super.onScrollChanged(l, t, oldl, oldt);
		mMyScrollViewListener.onScrollChanged(l, t, oldl, oldt);
	}

}
