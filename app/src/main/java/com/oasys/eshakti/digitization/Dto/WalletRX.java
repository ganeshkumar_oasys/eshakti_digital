package com.oasys.eshakti.digitization.Dto;


import lombok.Data;

@Data
public class WalletRX {
    String accountNumber;
    String amount;
    String bankId;
    String date;
    String depositType;
    String merchantId;
    String receiptImage;
    String remarks;


}
