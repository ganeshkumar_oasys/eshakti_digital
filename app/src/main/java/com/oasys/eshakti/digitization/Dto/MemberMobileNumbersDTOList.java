package com.oasys.eshakti.digitization.Dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class MemberMobileNumbersDTOList implements Serializable
{
     String name;

     String userId;

     String mobileNumber;

     String shgId;

     String memberId;
}
