package com.oasys.eshakti.digitization.Adapter;


import android.content.Context;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.oasys.eshakti.digitization.Dto.ExistingLoan;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.activity.LoginActivity;
import com.yesteam.eshakti.interfaces.ExpandListItemClickListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class Transaction_GroupLoanRepayment_Adapter extends BaseExpandableListAdapter {

    private Context context;
    private ArrayList<ExistingLoan> mHeaderList;
    private HashMap<String, ArrayList<ExistingLoan>> mChildList;
    private ExpandListItemClickListener mCallBack;


    public Transaction_GroupLoanRepayment_Adapter(Context context, ArrayList<ExistingLoan> mHeaderList, HashMap<String, ArrayList<ExistingLoan>> mChildList, ExpandListItemClickListener callback) {
        this.context = context;
        this.mHeaderList = mHeaderList;
        this.mChildList = mChildList;
        this.mCallBack = callback;
    }

    @Override
    public int getGroupCount() {
        return mHeaderList.size();
    }

    @Override
    public int getChildrenCount(int position) {
//        String headSize = this.mHeaderList.get(position).getLoanId();
//        int value = this.mChildList.get(headSize).size();
        return mChildList.size();
    }

    @Override
    public ExistingLoan getGroup(int position) {
        return this.mHeaderList.get(position);
    }

    @Override
    public ExistingLoan getChild(int groupPosition, int childposition) {
        return this.mChildList.get(this.mHeaderList.get(groupPosition).getLoanId()).get(childposition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean b, View view, ViewGroup viewGroup) {
        ExistingLoan loansList = getGroup(groupPosition);

        if (view == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.grouploanrepaymentlistgroup, null);
        }

        TextView loanname = (TextView) view.findViewById(R.id.m_grouploanrepayment_field_name);
        loanname.setTypeface(LoginActivity.sTypeface);
        ImageView mHeader_arrow = (ImageView) view.findViewById(R.id.m_group_arrow);


        if (b) {
            mHeader_arrow.setImageResource(R.drawable.down_arrow);
        } else {
            mHeader_arrow.setImageResource(R.drawable.side_arrow);
        }
        loanname.setText(loansList.getLoanTypeName());
        return view;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean b, View view, final ViewGroup viewGroup) {
        LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = layoutInflater.inflate(R.layout.view_content, null);
        TextView accountnumber = (TextView) view.findViewById(R.id.shgcode);
        TextView bankname = (TextView) view.findViewById(R.id.blockname);
        TextView loandisbursement = (TextView) view.findViewById(R.id.panchayat);
        TextView accountname = (TextView) view.findViewById(R.id.shgcode_values);
        TextView loantype = (TextView) view.findViewById(R.id.blockname_values);
        TextView villagename = (TextView) view.findViewById(R.id.villagename);
        TextView villagenamevalues = (TextView) view.findViewById(R.id.villagename_vlaues);
        TextView transDate = (TextView) view.findViewById(R.id.transDate);
        TextView transDate_vlaues = (TextView) view.findViewById(R.id.transDate_vlaues);
        TextView loandisbursementdate = (TextView) view.findViewById(R.id.panchayat_values);
        Button button_text = (Button) view.findViewById(R.id.activity_next_button);
        Log.e("Adapter", "child list..." + mHeaderList.toString());
        Log.e("Adapter", "groupPosition..." + groupPosition);
        villagename.setVisibility(View.GONE);
        villagenamevalues.setVisibility(View.GONE);
        transDate.setVisibility(View.GONE);
        transDate_vlaues.setVisibility(View.GONE);
        accountnumber.setText(AppStrings.mAccountNumber+":");
        accountnumber.setTypeface(LoginActivity.sTypeface);
        bankname.setText(AppStrings.mAccountNumber+":");
        bankname.setTypeface(LoginActivity.sTypeface);
        loandisbursement.setText(AppStrings.Loan_Disbursement_Date+":");
        loandisbursement.setTypeface(LoginActivity.sTypeface);
        loantype.setTypeface(LoginActivity.sTypeface);
        accountname.setTypeface(LoginActivity.sTypeface);
        accountname.setText(mHeaderList.get(groupPosition).getAccountNumber());
        loantype.setText(mHeaderList.get(groupPosition).getBankName());
        //  loandisbursementdate.setText(mHeaderList.get(groupPosition).getDisbursmentDate());
        DateFormat simple = new SimpleDateFormat("dd/MM/yyyy");
        Date d = new Date(Long.parseLong(mHeaderList.get(groupPosition).getDisbursmentDate()));
        String dateStr = simple.format(d);
        loandisbursementdate.setText(dateStr);

        button_text.setText(AppStrings.mSelect);
        button_text.setTypeface(LoginActivity.sTypeface);

        button_text.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mCallBack.onItemClick(viewGroup, v, groupPosition);
            }
        });


        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return false;
    }
}
