package com.oasys.eshakti.digitization.Dto;


import lombok.Data;

@Data
public class Upi_VCS_Request {

    public String payerVirtualAddress;
    public String amount;
    public String virtualAddress;
    public String custRefId;
    public String remarks;

}
