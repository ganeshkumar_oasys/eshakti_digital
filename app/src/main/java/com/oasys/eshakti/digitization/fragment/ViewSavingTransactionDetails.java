package com.oasys.eshakti.digitization.fragment;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasys.eshakti.digitization.Adapter.Savings_TransactionAdapter;
import com.oasys.eshakti.digitization.Dto.ResponseContents;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewSavingTransactionDetails extends Fragment {

    private RecyclerView recyclerViewmembersavingtransactionsummary;
    private View view;
    private LinearLayoutManager linearLayoutManager;
    private Savings_TransactionAdapter savings_transactionAdapter;
    public ArrayList<ResponseContents> responseContentsList;
    private TextView daily_date_time, digitization_header;
    private String currentDateStr;
    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_view_saving_transaction_details, container, false);
        init();
        return view;
    }

    private void init() {

        Bundle bundle = getArguments();
        responseContentsList = (ArrayList<ResponseContents>) bundle.getSerializable("transactiondetails");

        daily_date_time = (TextView) view.findViewById(R.id.daily_date_time);
        digitization_header = (TextView) view.findViewById(R.id.digitization_header);

        Calendar calender = Calendar.getInstance();
        currentDateStr = df.format(calender.getTime());
        daily_date_time.setText(currentDateStr);
        digitization_header.setText(responseContentsList.get(0).getTxType() + AppStrings.mSR_Details);

        recyclerViewmembersavingtransactionsummary = (RecyclerView) view.findViewById(R.id.recyclerViewmembersavingtransactionsummary);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerViewmembersavingtransactionsummary.setLayoutManager(linearLayoutManager);
        recyclerViewmembersavingtransactionsummary.setHasFixedSize(true);
        recyclerViewmembersavingtransactionsummary.addItemDecoration(new DividerItemDecoration(getActivity(), 0));

        savings_transactionAdapter = new Savings_TransactionAdapter(getActivity(), responseContentsList);
        recyclerViewmembersavingtransactionsummary.setAdapter(savings_transactionAdapter);


    }


}
