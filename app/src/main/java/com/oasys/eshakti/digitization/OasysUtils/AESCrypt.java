package com.oasys.eshakti.digitization.OasysUtils;

import android.util.Base64;
import android.util.Log;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import Decoder.BASE64Encoder;

public class AESCrypt {

    private static final String ALGORITHM = "AES/CBC/PKCS5Padding";
    private static final String KEY = "1234$#$^@ESHAKTI";

    public static  String encryptedValue;

    public static String encrypt(String value) throws Exception
    {
        /*Key key = generateKey();
        Cipher cipher = Cipher.getInstance(AESCrypt.ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, key);
        byte [] encryptedByteValue = cipher.doFinal(value.getBytes("utf-8"));
        encryptedValue64 = Base64.encodeToString(encryptedByteValue, Base64.DEFAULT);

        Log.e("encrypt",encryptedValue64);
        decrypt(encryptedValue64);
        return encryptedValue64;*/

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

        byte[] keyBytes = new byte[16];

        byte[] b = KEY.getBytes("UTF-8");

        int len = b.length;

        if (len > keyBytes.length)

            len = keyBytes.length;

        System.arraycopy(b, 0, keyBytes, 0, len);

        SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");

        IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);

        cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);

        byte[] results = cipher.doFinal(value.getBytes("UTF-8"));

        encryptedValue = new BASE64Encoder().encode(results);

        return encryptedValue;

    }

    //Decrypt

   /* public static String decrypt(String value) throws Exception
    {
        Key key = generateKey();
        Cipher cipher = Cipher.getInstance(AESCrypt.ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, key);
        byte[] decryptedValue64 = Base64.decode(value, Base64.DEFAULT);
        byte [] decryptedByteValue = cipher.doFinal(decryptedValue64);
        String decryptedValue = new String(decryptedByteValue,"utf-8");

        Log.e("deencrypt",decryptedValue);
        return decryptedValue;

    }

    private static Key generateKey() throws Exception
    {
        Key key = new SecretKeySpec(AESCrypt.KEY.getBytes(),AESCrypt.ALGORITHM);
        return key;
    }*/
}
