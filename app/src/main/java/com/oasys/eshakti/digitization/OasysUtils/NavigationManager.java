package com.oasys.eshakti.digitization.OasysUtils;

public interface NavigationManager {
    void showFragment(String title);
}
