package com.oasys.eshakti.digitization.fragment;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.activity.NewDrawerScreen;
import com.oasys.eshakti.digitization.database.SHGTable;

/**
 * A simple {@link Fragment} subclass.
 */
public class StepwiseFinalReports extends Fragment implements View.OnClickListener {
    private View view;
    private TextView total_collection;
    private TextView total_disbursement;
    private TextView stepwise_cashin_hand;
    private TextView stepwise_cashat_bank;
    private TextView stepwise_interest;
    private TextView stepwise_charges;
    private TextView stepwise_totalrepayment;
    private TextView stepwise_subventionreceived;
    private TextView stepwise_bankcharges,done;
    private ListOfShg shgDto;
    //    private Button done;
    private int sum_of_savings_Internalloan_Memberloan =0;


    public StepwiseFinalReports() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_stepwise_final_reports, container, false);
        shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));

        sum_of_savings_Internalloan_Memberloan = Savings.sum_of_savings+Internalloan_repayment.sum_of_internalloan_repayment+ MemberLoanRepayment.finalsum_of_memberloan_repayment;
        initView(view);
        Attendance.flag="0";
        return view;
    }


    private void initView(View view) {
        total_collection = (TextView) view.findViewById(R.id.total_collection);
        String con_sum_of_savings_Internalloan_Memberloan = String.valueOf(sum_of_savings_Internalloan_Memberloan);
        Log.d("Lsuminternalloan", con_sum_of_savings_Internalloan_Memberloan);
        total_collection.setText(con_sum_of_savings_Internalloan_Memberloan);

        stepwise_cashin_hand = (TextView) view.findViewById(R.id.stepwise_cashin_hand);
        stepwise_cashin_hand.setText(shgDto.getCashInHand());

        stepwise_cashat_bank = (TextView) view.findViewById(R.id.stepwise_cashat_bank);
        stepwise_cashat_bank.setText(shgDto.getCashAtBank());

        total_disbursement = (TextView) view.findViewById(R.id.total_disbursement);
        total_disbursement.setText(String.valueOf(LD_IL_Entry.mTotalDisbursement));

        stepwise_interest = (TextView) view.findViewById(R.id.stepwise_interest);
        stepwise_interest.setText(String.valueOf(Transaction_LoanType_Details.store_interest));

        stepwise_charges = (TextView) view.findViewById(R.id.stepwise_charges);
        stepwise_charges.setText(String.valueOf(Transaction_LoanType_Details.store_charges));

        stepwise_totalrepayment = (TextView) view.findViewById(R.id.stepwise_totalrepayment);
        stepwise_totalrepayment.setText(String.valueOf(Transaction_LoanType_Details.store_repayment));

        stepwise_subventionreceived = (TextView) view.findViewById(R.id.stepwise_subventionreceived);
        stepwise_subventionreceived.setText(String.valueOf(Transaction_LoanType_Details.store_interest_subvention));

        stepwise_bankcharges = (TextView) view.findViewById(R.id.stepwise_bankcharges);
        stepwise_bankcharges.setText(String.valueOf(Transaction_LoanType_Details.store_bank_charges));

        done = (TextView) view.findViewById(R.id.done);
        done.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.done:
                MainFragment mainFragment =new MainFragment();
                Savings.sum_of_savings = 0;
                Attendance.flag="0";
                Internalloan_repayment.sum_of_internalloan_repayment = 0;
                MemberLoanRepayment.finalsum_of_memberloan_repayment =0;
                MemberLoanRepayment.sum_of_memberloan_repayment =0;
                sum_of_savings_Internalloan_Memberloan = 0;

                NewDrawerScreen.showFragment(mainFragment);
                break;
        }
    }
}
