package com.oasys.eshakti.digitization.fragment;


import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oasys.eshakti.digitization.Adapter.CustomMReportAdapter;
import com.oasys.eshakti.digitization.Dto.ListOfShg;
import com.oasys.eshakti.digitization.Dto.MonthlyReport;
import com.oasys.eshakti.digitization.Dto.RequestDto.MonthYearDto;
import com.oasys.eshakti.digitization.Dto.ResponseDto;
import com.oasys.eshakti.digitization.OasysUtils.AppStrings;
import com.oasys.eshakti.digitization.OasysUtils.Constants;
import com.oasys.eshakti.digitization.OasysUtils.MySharedPreference;
import com.oasys.eshakti.digitization.OasysUtils.NetworkConnection;
import com.oasys.eshakti.digitization.OasysUtils.ServiceType;
import com.oasys.eshakti.digitization.OasysUtils.Utils;
import com.oasys.eshakti.digitization.R;
import com.oasys.eshakti.digitization.Service.RestClient;
import com.oasys.eshakti.digitization.Service.NewTaskListener;
import com.oasys.eshakti.digitization.activity.LoginActivity;
import com.oasys.eshakti.digitization.database.SHGTable;
import com.oasys.eshakti.digitization.model.ListItem;

import java.util.ArrayList;


public class MonthlyReportDetail extends Fragment implements NewTaskListener {
    TextView Name, savingsamount, savingsvalue, subscriptionvalue, penaltyvalue, OtherIncomevalue, internal_loan_disbursed_value,
            internal_loan_repaid_value, mfi_loan_disbursed_value, mfi_loan_repaid_value, term_loan_disbursed_value, cif_loan_disbursed_value, internalloanlimit, termloanoutstanding, bulkloanlimit, bulkloanoutstanding, ccloanoutstanding, ccloanlimit, cifloanoutstanding, mfiloanoutstanding, cifloanlimit, mfiloanlimit, termloanlimit, internalloanoutstanding, cif_loan_repaid_value, cc_repaid_value, bulk_loan_disbursed_value, bulk_loan_repaid_value, cc_disbursed_value, term_loan_repaid_value;
    private String mYear, memid;
    private int mMonth;
    private View view;
    ResponseDto responseDto;
    private ListOfShg shgDto;
    private ArrayList<MonthlyReport> incomeMenu = new ArrayList<>();
    private ArrayList<ListItem> listItems;
    private CustomMReportAdapter mAdapter;
    private ListView list;
    private TextView mGroupName;
    private TextView mCashinHand;
    private TextView mCashatBank;
    private TextView mHeader;

    public MonthlyReportDetail() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_monthly_report_detail, container, false);

        Bundle bundle = getArguments();
        mMonth = bundle.getInt("month_key");
        mYear = bundle.getString("year_key");
        memid = bundle.getString("member_id");
        mMonthYearSubmit(mMonth, mYear);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        inIt();
    }

    private void inIt() {
        try {
            shgDto = SHGTable.getSHGDetails(MySharedPreference.readString(getActivity(), MySharedPreference.SHG_ID, ""));

            mGroupName = (TextView) view.findViewById(R.id.groupname);
            mGroupName.setText(shgDto.getName() + " / " + shgDto.getPresidentName());
            mGroupName.setTypeface(LoginActivity.sTypeface);

            mCashinHand = (TextView) view.findViewById(R.id.cih);
            mCashinHand.setText(AppStrings.cashinhand + shgDto.getCashInHand());
            mCashinHand.setTypeface(LoginActivity.sTypeface);

            mCashatBank = (TextView) view.findViewById(R.id.cab);
            mCashatBank.setText(AppStrings.cashatBank + shgDto.getCashAtBank());
            mCashatBank.setTypeface(LoginActivity.sTypeface);

            Name = (TextView) view.findViewById(R.id.Name);
            Name.setText(MySharedPreference.readString(getActivity(), MySharedPreference.MEM_NAME_SUMMARY, ""));

            mHeader = (TextView) view.findViewById(R.id.fragmentHeader);
            mHeader.setText(AppStrings.lastMonthReport);
            mHeader.setTypeface(LoginActivity.sTypeface);

            savingsamount = (TextView) view.findViewById(R.id.savingsamount);
          /*  savingsvalue = (TextView) view.findViewById(R.id.savingsvalue);
            subscriptionvalue = (TextView) view.findViewById(R.id.subscriptionvalue);
            penaltyvalue = (TextView) view.findViewById(R.id.penaltyvalue);
            OtherIncomevalue = (TextView) view.findViewById(R.id.OtherIncomevalue);
            internal_loan_disbursed_value = (TextView) view.findViewById(R.id.internal_loan_disbursed_value);
            internal_loan_repaid_value = (TextView) view.findViewById(R.id.internal_loan_repaid_value);
            mfi_loan_disbursed_value = (TextView) view.findViewById(R.id.mfi_loan_disbursed_value);
            mfi_loan_repaid_value = (TextView) view.findViewById(R.id.mfi_loan_repaid_value);
            term_loan_disbursed_value = (TextView) view.findViewById(R.id.term_loan_disbursed_value);
            term_loan_repaid_value = (TextView) view.findViewById(R.id.term_loan_repaid_value);

            cif_loan_disbursed_value = (TextView) view.findViewById(R.id.cif_loan_disbursed_value);
            cif_loan_repaid_value = (TextView) view.findViewById(R.id.cif_loan_repaid_value);

            bulk_loan_disbursed_value = (TextView) view.findViewById(R.id.bulk_loan_disbursed_value);
            bulk_loan_repaid_value = (TextView) view.findViewById(R.id.bulk_loan_repaid_value);

            cc_disbursed_value = (TextView) view.findViewById(R.id.cc_disbursed_value);
            cc_repaid_value = (TextView) view.findViewById(R.id.cc_repaid_value);
*/


            internalloanlimit = (TextView) view.findViewById(R.id.internalloanlimit);
            list = (ListView) view.findViewById(R.id.list);
            internalloanoutstanding = (TextView) view.findViewById(R.id.internalloanoutstanding);
            termloanlimit = (TextView) view.findViewById(R.id.termloanlimit);
            termloanoutstanding = (TextView) view.findViewById(R.id.termloanoutstanding);
            mfiloanlimit = (TextView) view.findViewById(R.id.mfiloanlimit);
            mfiloanoutstanding = (TextView) view.findViewById(R.id.mfiloanoutstanding);
            cifloanlimit = (TextView) view.findViewById(R.id.cifloanlimit);
            cifloanoutstanding = (TextView) view.findViewById(R.id.cifloanoutstanding);
            ccloanlimit = (TextView) view.findViewById(R.id.ccloanlimit);
            ccloanoutstanding = (TextView) view.findViewById(R.id.ccloanoutstanding);
            bulkloanlimit = (TextView) view.findViewById(R.id.bulkloanlimit);
            bulkloanoutstanding = (TextView) view.findViewById(R.id.bulkloanoutstanding);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void mMonthYearSubmit(int mMonth, String mYear) {
        MonthYearDto monthYearDto = new MonthYearDto();
        monthYearDto.setMonth(mMonth);
        monthYearDto.setYear(mYear);
        String nMonthYearSubmit = new Gson().toJson(monthYearDto);
        if (NetworkConnection.getNetworkConnection(getActivity()).isNetworkAvailable()) {

            RestClient.getRestClient(MonthlyReportDetail.this).callRestWebService(Constants.BASE_URL + Constants.MONTH_REPORT + memid, nMonthYearSubmit, getActivity(), ServiceType.MONTH_YEAR_REPORT);
        } else {
            Utils.showToast(getActivity(), "Network Not Available");
        }
    }

    @Override
    public void onStart() {
        super.onStart();


    }

    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result, ServiceType serviceType) {

        switch (serviceType) {
            case MONTH_YEAR_REPORT:
                try {
                    responseDto = new Gson().fromJson(result, ResponseDto.class);
                    if (responseDto.getStatusCode() == (Utils.Success_Code)) {
                        Utils.showToast(getActivity(), responseDto.getMessage());
                        if (responseDto.getResponseContent().getTotalSavingsAmount().get(0).getTotal_Savings() != null)
                            savingsamount.setText(responseDto.getResponseContent().getTotalSavingsAmount().get(0).getTotal_Savings());
                        else {
                            savingsamount.setText("0.0");
                        }
                        //  Log.d("check", "" + responseDto.getResponseContent().getMembermonthlyReport().get(0).getName());
                        //Name.setText(responseDto.getResponseContent().getMonthlyReport().get(0).getName());
                        if (responseDto.getResponseContent().getMembermonthlyReport() != null && responseDto.getResponseContent().getMembermonthlyReport().size() > 0) {
                            listItems = new ArrayList<ListItem>();
                            //  listItems1 = new ArrayList<ListItem>();
                            incomeMenu = responseDto.getResponseContent().getMembermonthlyReport();

                            for (int i = 0; i < incomeMenu.size(); i++) {
                                ListItem rowItem = new ListItem();
                                rowItem.setTitle(incomeMenu.get(i).getName());
                                rowItem.setWalletValue(incomeMenu.get(i).getAmount());
                                listItems.add(rowItem);
                            }

                            mAdapter = new CustomMReportAdapter(getActivity(), listItems);
                            list.setAdapter(mAdapter);
                        }
                        if (responseDto.getResponseContent().getMemberinternallaon().get(0).getInternalloan() != null || responseDto.getResponseContent().getMemberinternallaon().get(0).getInternalloan() != null) {
                            internalloanlimit.setText(responseDto.getResponseContent().getMemberinternallaon().get(0).getInternalloan());
                            internalloanoutstanding.setText(responseDto.getResponseContent().getMemberinternallaon().get(0).getInetrnalloan_Outstanding());
                        } else {
                            internalloanlimit.setText("0.0");
                            internalloanoutstanding.setText("0.0");

                        }
                        if (responseDto.getResponseContent().getMemberreportloansummary().get(0).getTermLoan() != null || responseDto.getResponseContent().getMemberreportloansummary().get(0).getTerm_outstanding() != null) {
                            termloanlimit.setText(responseDto.getResponseContent().getMemberreportloansummary().get(0).getTermLoan());
                            termloanoutstanding.setText(responseDto.getResponseContent().getMemberreportloansummary().get(0).getTerm_outstanding());
                        } else {
                            termloanlimit.setText("0.0");
                            termloanoutstanding.setText("0.0");
                        }

                        if (responseDto.getResponseContent().getMemberreportloansummary().get(0).getCifLoan() != null || responseDto.getResponseContent().getMemberreportloansummary().get(0).getCif_outstanding() != null) {
                            cifloanlimit.setText(responseDto.getResponseContent().getMemberreportloansummary().get(0).getCifLoan());
                            cifloanoutstanding.setText(responseDto.getResponseContent().getMemberreportloansummary().get(0).getCif_outstanding());
                        } else {
                            cifloanlimit.setText("0.0");
                            cifloanoutstanding.setText("0.0");
                        }

                        if (responseDto.getResponseContent().getMemberreportloansummary().get(0).getBulkLoan() != null || responseDto.getResponseContent().getMemberreportloansummary().get(0).getBulk_outstanding() != null) {
                            bulkloanlimit.setText(responseDto.getResponseContent().getMemberreportloansummary().get(0).getBulkLoan());
                            bulkloanoutstanding.setText(responseDto.getResponseContent().getMemberreportloansummary().get(0).getBulk_outstanding());
                        } else {
                            bulkloanlimit.setText("0.0");
                            bulkloanoutstanding.setText("0.0");
                        }

                        if (responseDto.getResponseContent().getMemberreportloansummary().get(0).getCashCredit() != null || responseDto.getResponseContent().getMemberreportloansummary().get(0).getCashCredit_outstanding() != null) {
                            ccloanlimit.setText(responseDto.getResponseContent().getMemberreportloansummary().get(0).getCashCredit());
                            ccloanoutstanding.setText(responseDto.getResponseContent().getMemberreportloansummary().get(0).getCashCredit_outstanding());
                        } else {
                            ccloanlimit.setText("0.0");
                            ccloanoutstanding.setText("0.0");
                        }

                        if (responseDto.getResponseContent().getMemberreportloansummary().get(0).getMfiLoan() != null || responseDto.getResponseContent().getMemberreportloansummary().get(0).getMfi_outstanding() != null) {
                            mfiloanlimit.setText(responseDto.getResponseContent().getMemberreportloansummary().get(0).getMfiLoan());
                            mfiloanoutstanding.setText(responseDto.getResponseContent().getMemberreportloansummary().get(0).getMfi_outstanding());
                        } else {
                            mfiloanlimit.setText("0.0");
                            mfiloanoutstanding.setText("0.0");
                        }


                     /*   if (responseDto.getResponseContent().getMembermonthlyReport().get(0).getMode().equals("Savings"))
                            savingsvalue.setText(responseDto.getResponseContent().getMembermonthlyReport().get(0).getAmount());
                        if (responseDto.getResponseContent().getMembermonthlyReport().get(0).getMode().equals("Subscription"))
                            subscriptionvalue.setText(responseDto.getResponseContent().getMembermonthlyReport().get(0).getAmount());
                        if (responseDto.getResponseContent().getMembermonthlyReport().get(0).getMode().equals("Penalty"))
                            penaltyvalue.setText(responseDto.getResponseContent().getMembermonthlyReport().get(0).getAmount());
                        if (responseDto.getResponseContent().getMembermonthlyReport().get(0).getMode().equals("Other Income"))
                            OtherIncomevalue.setText(responseDto.getResponseContent().getMembermonthlyReport().get(0).getAmount());
                        if (responseDto.getResponseContent().getMembermonthlyReport().get(0).getMode().equals("internal Loan Disbursed"))
                            internal_loan_disbursed_value.setText(responseDto.getResponseContent().getMembermonthlyReport().get(0).getAmount());
                        if (responseDto.getResponseContent().getMembermonthlyReport().get(0).getMode().equals("Internal Loan Repaid"))
                            internal_loan_repaid_value.setText(responseDto.getResponseContent().getMembermonthlyReport().get(0).getAmount());
                        if (responseDto.getResponseContent().getMembermonthlyReport().get(0).getMode().equals("MFI Loan Disbursed"))
                            mfi_loan_disbursed_value.setText(responseDto.getResponseContent().getMembermonthlyReport().get(0).getAmount());
                        if (responseDto.getResponseContent().getMembermonthlyReport().get(0).getMode().equals("MFI Loan Repaid"))
                            mfi_loan_repaid_value.setText(responseDto.getResponseContent().getMembermonthlyReport().get(0).getAmount());
                        if (responseDto.getResponseContent().getMembermonthlyReport().get(0).getMode().equals("Term Loan Disbursed"))
                            term_loan_disbursed_value.setText(responseDto.getResponseContent().getMembermonthlyReport().get(0).getAmount());
                        if (responseDto.getResponseContent().getMembermonthlyReport().get(0).getMode().equals("Term Loan Repaid"))
                            term_loan_repaid_value.setText(responseDto.getResponseContent().getMembermonthlyReport().get(0).getAmount());
                        if (responseDto.getResponseContent().getMembermonthlyReport().get(0).getMode().equals("Cash Credit Disbursed"))
                            cc_disbursed_value.setText(responseDto.getResponseContent().getMembermonthlyReport().get(0).getAmount());
                        if (responseDto.getResponseContent().getMembermonthlyReport().get(0).getMode().equals("Cash Credit Repaid"))
                            cc_repaid_value.setText(responseDto.getResponseContent().getMembermonthlyReport().get(0).getAmount());
                        if (responseDto.getResponseContent().getMembermonthlyReport().get(0).getMode().equals("CIF Loan Disbursed"))
                            cif_loan_disbursed_value.setText(responseDto.getResponseContent().getMembermonthlyReport().get(0).getAmount());
                        if (responseDto.getResponseContent().getMembermonthlyReport().get(0).getMode().equals("CIF Loan Repaid"))
                            cif_loan_repaid_value.setText(responseDto.getResponseContent().getMembermonthlyReport().get(0).getAmount());
                        if (responseDto.getResponseContent().getMembermonthlyReport().get(0).getMode().equals("Bulk Loan Disbursed"))
                            bulk_loan_disbursed_value.setText(responseDto.getResponseContent().getMembermonthlyReport().get(0).getAmount());
                        if (responseDto.getResponseContent().getMembermonthlyReport().get(0).getMode().equals("Bulk Loan Repaid"))
                            bulk_loan_repaid_value.setText(responseDto.getResponseContent().getMembermonthlyReport().get(0).getAmount());
*/

                    } else {
//                        Utils.showToastMethod(LoginActivity.this, message);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("excep", "" + e);
                }
                break;
        }
    }
}
