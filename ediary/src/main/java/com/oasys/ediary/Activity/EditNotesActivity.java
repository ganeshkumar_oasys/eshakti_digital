package com.oasys.ediary.Activity;

import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.oasys.ediary.DB.NotesProvider;
import com.oasys.ediary.Dto.Notes;
import com.oasys.ediary.R;
import com.oasys.ediary.Utils.CommonUtils;

import java.util.Calendar;
import java.util.Date;

public class EditNotesActivity extends AppCompatActivity   {

    private AppCompatEditText notesDescription;
    private Button saveBtn;
    private TextView addedDate,addedDay,addedMonth,addedTime;

    Calendar globalCal;
    private Notes notes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ediary_activity_add_notes);
        initToolbar();
        initViews();
        Intent intent = getIntent();
        globalCal = Calendar.getInstance();
        Date currentDate = new Date();
        if(intent!=null&&intent.getExtras()!=null){
           Bundle bundle =  intent.getBundleExtra("bundle");
            notes = (Notes) bundle.getSerializable("notes");
            currentDate = new Date(notes.getCreatedTs());
            notesDescription.setText(notes.getNotes());
        }
        globalCal.setTime(currentDate);
        setDetails();

    }

    private void initViews() {
        notesDescription = findViewById(R.id.diary_add_notes);
        addedDate = findViewById(R.id.diary_add_date);
        addedDay = findViewById(R.id.diary_add_day);
        addedMonth = findViewById(R.id.diary_add_month_year);
        addedTime = findViewById(R.id.diary_add_time);
        LinearLayout dateInfo = findViewById(R.id.date_info_layer);
        saveBtn = findViewById(R.id.diary_add_btn);
        saveBtn.setText("Update");
        dateInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                callDatePicker();
            }
        });
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addNotes();
            }
        });
        addedDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                callDatePicker();
            }
        });
        addedTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                callTimePicker();
            }
        });
    }

    private void setDetails() {
        addedDate.setText(String.valueOf(globalCal.get(Calendar.DATE)));
        addedDay.setText(CommonUtils.getDay(globalCal.get(Calendar.DAY_OF_WEEK)));
        addedMonth.setText(CommonUtils.getMonth(globalCal.get(Calendar.MONTH))+" "+globalCal.get(Calendar.YEAR));
        int am_pm=globalCal.get(Calendar.AM_PM);
        String Am_Pm= ((am_pm==Calendar.AM)?"am":"pm");
        addedTime.setText(String.format("%02d",globalCal.get(Calendar.HOUR))+":"+String.format("%02d",globalCal.get(Calendar.MINUTE))+" "+Am_Pm);
    }

    private void initToolbar() {
        Toolbar mToolbar =  findViewById(R.id.diary_add_toolbar);
        setSupportActionBar(mToolbar);
        setTitle(getString(R.string.add_title));
        mToolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }


    private void callTimePicker() {
    /*    TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(this,globalCal.get(Calendar.HOUR_OF_DAY),globalCal.get(Calendar.MINUTE),globalCal.get(Calendar.SECOND),false);
        timePickerDialog.show(getFragmentManager(),"Timepickerdialog");*/

        TimePickerDialog timePickerDialog = new TimePickerDialog(EditNotesActivity.this,new android.app.TimePickerDialog.OnTimeSetListener(){

            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                globalCal.set(Calendar.HOUR_OF_DAY,hourOfDay);
                globalCal.set(Calendar.MINUTE,minute);
                int am_pm=globalCal.get(Calendar.AM_PM);
                String Am_Pm= ((am_pm==Calendar.AM)?"am":"pm");
                addedTime.setText(String.format("%02d",globalCal.get(Calendar.HOUR))+":"+String.format("%02d",globalCal.get(Calendar.MINUTE))+" "+Am_Pm);

            }
        },globalCal.get(Calendar.HOUR_OF_DAY),globalCal.get(Calendar.MINUTE),false);
        timePickerDialog.show();
    }

    private void addNotes(){
        String description = notesDescription.getText().toString();
        if(!description.isEmpty()){
            updateNotes(description);
        }else{
            Toast.makeText(EditNotesActivity.this, "Enter details to save!!!", Toast.LENGTH_SHORT).show();
        }
    }

   /* @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

        globalCal.set(year,monthOfYear,dayOfMonth);
        addedDate.setText(String.valueOf(dayOfMonth));
        addedDay.setText(CommonUtils.getDay(globalCal.get(Calendar.DAY_OF_WEEK)));
        addedMonth.setText(CommonUtils.getMonth(monthOfYear)+" "+year);
    }*/

    private void callDatePicker() {
       /* DatePickerDialog dpd = DatePickerDialog.newInstance(
                EditNotesActivity.this,
                globalCal.get(Calendar.YEAR), // Initial year selection
                globalCal.get(Calendar.MONTH), // Initial month selection
                globalCal.get(Calendar.DAY_OF_MONTH) // Inital day selection
        );
        dpd.show(getFragmentManager(), "Datepickerdialog");*/

        final android.app.DatePickerDialog datePickerDialog = new android.app.DatePickerDialog(EditNotesActivity.this, new android.app.DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                globalCal.set(year,month,day);
                addedDate.setText(String.valueOf(day));
                addedDay.setText(CommonUtils.getDay(globalCal.get(Calendar.DAY_OF_WEEK)));
                addedMonth.setText(CommonUtils.getMonth(month)+" "+year);

            }
        }, globalCal.get(Calendar.YEAR), globalCal.get(Calendar.DATE), globalCal.get(Calendar.DAY_OF_MONTH));
//        datePickerDialog.getDatePicker().setMinDate(Calendar.getInstance().getTimeInMillis());

//        globalCal.set(cyear, cmonth, cday);
//        datePickerDialog.getDatePicker().setMaxDate(globalCal.getTimeInMillis());
        datePickerDialog.show();
    }

    private  void updateNotes(String data){
        ContentValues values = new ContentValues();
        String where =NotesProvider.CLM_NOTES_ID+" = "+ notes.getId();

        values.put(NotesProvider.CLM_NOTES_DESCRIPTION,
                data);
        values.put(NotesProvider.CLM_NOTES_CREATED_DATE,
                globalCal.getTimeInMillis());
        values.put(NotesProvider.CLM_NOTES_MODIFIED_DATE,
                globalCal.getTimeInMillis());
        values.put(NotesProvider.CLM_NOTES_USER_ID,
                0);
        values.put(NotesProvider.CLM_NOTES_STATUS,
                1);
        values.put(NotesProvider.CLM_NOTES_NOTIFY_FLAG,
                notes.getNotifyFlag());
        values.put(NotesProvider.CLM_NOTES_SYNC_FLAG,
                0);
         getContentResolver().update(
                NotesProvider.CONTENT_URI, values,where,null);
        Toast.makeText(EditNotesActivity.this, "Updated successfuly!!!", Toast.LENGTH_SHORT).show();
        finish();


    }

   /* @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        globalCal.set(Calendar.HOUR_OF_DAY,hourOfDay);
        globalCal.set(Calendar.MINUTE,minute);
        globalCal.set(Calendar.SECOND,second);
        int am_pm=globalCal.get(Calendar.AM_PM);
        String Am_Pm= ((am_pm==Calendar.AM)?"am":"pm");
        addedTime.setText(String.format("%02d",globalCal.get(Calendar.HOUR))+":"+String.format("%02d",globalCal.get(Calendar.MINUTE))+" "+Am_Pm);
    }*/
}
