package com.oasys.ediary.Activity;

import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;

import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;


import com.oasys.ediary.Fragment.CalendarFragment;
import com.oasys.ediary.Fragment.HomeFragment;
import com.oasys.ediary.NotificationWorker;
import com.oasys.ediary.R;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class DiaryHome extends AppCompatActivity {

    public FloatingActionButton floatingBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ediary_activity_diary_home);
        initToolbar();
        initViewPagerAndTabs();
        initNotification();
//        showNotify();
    }

    private void showNotify() {

//        getNext15MinsTask();
    }


    private void initNotification() {
        PeriodicWorkRequest request =
                new PeriodicWorkRequest.Builder(NotificationWorker.class, 15, TimeUnit.MINUTES)
                        .build();
        WorkManager.getInstance().enqueueUniquePeriodicWork("NOTIFY_WORK", ExistingPeriodicWorkPolicy.KEEP, request);

    }

    private void initToolbar() {
        Toolbar mToolbar = findViewById(R.id.diary_home_toolbar);
        setSupportActionBar(mToolbar);
        setTitle(getString(R.string.app_name)+" - eDiary");
        mToolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
    }

    private void initViewPagerAndTabs() {
        ViewPager viewPager = findViewById(R.id.diary_home_view_pager);
        PagerAdapter pagerAdapter = new PagerAdapter(getSupportFragmentManager());
        pagerAdapter.addFragment(new HomeFragment(), "Home");
        pagerAdapter.addFragment(new CalendarFragment(), "Calendar");
        viewPager.setAdapter(pagerAdapter);
        TabLayout tabLayout = findViewById(R.id.diary_home_tabLayout);
        tabLayout.setupWithViewPager(viewPager);
        floatingBtn = findViewById(R.id.home_float_btn);
        floatingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DiaryHome.this, AddNotesActivity.class);
                startActivity(intent);
            }
        });

    }



    static class PagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> fragmentTitleList = new ArrayList<>();

        public PagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public void addFragment(Fragment fragment, String title) {
            fragmentList.add(fragment);
            fragmentTitleList.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitleList.get(position);
        }
    }

/*

    private void getNext15MinsTask() {
        Log.d("notes123","inside start job........");
        Uri friends = NotesProvider.CONTENT_URI;
        long date = CommonUtils.atStartOfDay(new Date());
        long date_plus_15= new Date().getTime();
        String where =NotesProvider.CLM_NOTES_CREATED_DATE+" BETWEEN "+ date+" AND "+date_plus_15 +" AND "+NotesProvider.CLM_NOTES_NOTIFY_FLAG+" = 1";
        Cursor c = getApplicationContext().getContentResolver().query(friends, null, where, null, NotesProvider.CLM_NOTES_CREATED_DATE +" DESC");

        Log.d("notes123",c.getCount()+"------"+where);
        ArrayList<Notes> notesList=new ArrayList<>();
        while (c.moveToNext()) {
            Log.d("notes123",c.getString(c.getColumnIndex(NotesProvider.CLM_NOTES_DESCRIPTION))+"------");
            Notes notes = new Notes();
            notes.setNotes(c.getString(c.getColumnIndex(NotesProvider.CLM_NOTES_DESCRIPTION)));
            notes.setCreatedTs(c.getLong(c.getColumnIndex(NotesProvider.CLM_NOTES_CREATED_DATE)));
            notes.setId(c.getLong(c.getColumnIndex(NotesProvider.CLM_NOTES_ID)));
            notesList.add(notes);
        }
        String GROUP_KEY_WORK_EMAIL = "com.oasys.notes.notify";
        if(notesList.size()>1) {
            Notes notes = notesList.get(0);
            Intent intent123 =  new Intent(getApplicationContext(), ViewNotesActivity.class);
            intent123.putExtra("notifyMe",true);
            intent123.putExtra("Note",notes);
            PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 0,
                   intent123,PendingIntent.FLAG_UPDATE_CURRENT);
            String channel_id = createNotificationChannel(getApplicationContext());

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getApplicationContext(),channel_id)
                    .setContentTitle("Upcoming Remainder")
                    .setContentText(new SimpleDateFormat("hh:mm a").format(new Date(notes.getCreatedTs()))+":"+notes.getNotes())
                    .setAutoCancel(true)
                    .setContentIntent(pi)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setShowWhen(true)
                    .setColor(Color.RED)
                    .setGroup(GROUP_KEY_WORK_EMAIL)
                    .setLocalOnly(true);
//            administrator-Veriton-Series

            NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify((int) ((new Date(System.currentTimeMillis()).getTime() / 1000L) % Integer.MAX_VALUE) *//* ID of notification *//*, notificationBuilder.build());



        }else if(notesList.size()>1){
            PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 0,
                    new Intent(getApplicationContext(), DiaryHome.class), PendingIntent.FLAG_UPDATE_CURRENT);
            String channel_id = createNotificationChannel(getApplicationContext());
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext(),channel_id)
                    .setContentTitle("Event tracker")
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentIntent(pi)
                    .setGroup(GROUP_KEY_WORK_EMAIL)
                    .setContentText("Events received");
            NotificationCompat.InboxStyle inboxStyle =
                    new NotificationCompat.InboxStyle();

            inboxStyle.setBigContentTitle("Event tracker details:");
            for (Notes notes : notesList) {
                inboxStyle.addLine(new SimpleDateFormat("hh:mm a").format(new Date(notes.getCreatedTs()))+":"+notes.getNotes());
            }
            mBuilder.setStyle(inboxStyle);
            NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify((int) ((new Date(System.currentTimeMillis()).getTime() / 1000L) % Integer.MAX_VALUE) *//* ID of notification *//*, mBuilder.build());

        }
    }


    public static String createNotificationChannel(Context context) {

        // NotificationChannels are required for Notifications on O (API 26) and above.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            // The id of the channel.
            String channelId = "Channel_id";

            // The user-visible name of the channel.
            CharSequence channelName = "Application_name";
            // The user-visible description of the channel.
            String channelDescription = "Application_name Alert";
            int channelImportance = NotificationManager.IMPORTANCE_DEFAULT;
            boolean channelEnableVibrate = true;
//            int channelLockscreenVisibility = Notification.;

            // Initializes NotificationChannel.
            NotificationChannel notificationChannel = new NotificationChannel(channelId, channelName, channelImportance);
            notificationChannel.setDescription(channelDescription);
            notificationChannel.enableVibration(channelEnableVibrate);
//            notificationChannel.setLockscreenVisibility(channelLockscreenVisibility);

            // Adds NotificationChannel to system. Attempting to create an existing notification
            // channel with its original values performs no operation, so it's safe to perform the
            // below sequence.
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(notificationChannel);

            return channelId;
        } else {
            // Returns null for pre-O (26) devices.
            return null;
        }
    }
*/
}
