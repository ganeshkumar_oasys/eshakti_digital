package com.oasys.ediary.Activity;

import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


import com.oasys.ediary.DB.NotesProvider;
import com.oasys.ediary.Dto.Notes;
import com.oasys.ediary.R;
import com.oasys.ediary.Utils.CommonUtils;

import java.util.Calendar;
import java.util.Date;

public class ViewNotesActivity extends AppCompatActivity {

    private TextView viewedDate,viewedDay,viewedMonth,viewedTime,notesDescription;
    private Calendar globalCal;
    private Notes notes;
    private Menu menu;
    private boolean notifyFlag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ediary_activity_view_notes);
        initToolbar();
        initViews();
        Intent intent = getIntent();
        notes = (Notes) intent.getSerializableExtra("Note");
        notifyFlag =  intent.getBooleanExtra("notifyMe",false);
        globalCal = Calendar.getInstance();
        globalCal.setTime(new Date(notes.getCreatedTs()));
        setDetails();


    }

    private void initToolbar() {
        Toolbar mToolbar =  findViewById(R.id.diary_view_toolbar);
        setSupportActionBar(mToolbar);
        setTitle(getString(R.string.view_title));
        mToolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if(notifyFlag){
            Intent intent = new Intent(ViewNotesActivity.this,DiaryHome.class);
            startActivity(intent);
            finish();
        }else {
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_view, menu);
        this.menu = menu;
        if(notes.getNotifyFlag()==1){
            menu.getItem(0).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_notifications));
        }else{
            menu.getItem(0).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_notifications_off));
        }
        if(notes.getCreatedTs()<new Date().getTime()){
            menu.getItem(0).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        if(item.getItemId()==R.id.m_delete){
            deleteNotes(notes.getId());
            return true;
        }else if(item.getItemId()==R.id.m_edit){

            editNotes();
            return true;
        }else if(item.getItemId()==R.id.m_notification){

            notifyMyNotes();
            return true;
        }
        return  false;
    }

    private void notifyMyNotes() {
        if(notes.getNotifyFlag()==0) {
            notes.setNotifyFlag(1);
            ContentValues values = new ContentValues();
            String where = NotesProvider.CLM_NOTES_ID + " = " + notes.getId();
            values.put(NotesProvider.CLM_NOTES_NOTIFY_FLAG, 1);
            getContentResolver().update(
                    NotesProvider.CONTENT_URI, values, where, null);
            menu.getItem(0).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_notifications));
            Toast.makeText(ViewNotesActivity.this, "Remainder Enabled!!!", Toast.LENGTH_SHORT).show();
        }else{
            notes.setNotifyFlag(0);
            Toast.makeText(ViewNotesActivity.this, "Remainder Disabled!!!", Toast.LENGTH_SHORT).show();
            ContentValues values = new ContentValues();
            String where = NotesProvider.CLM_NOTES_ID + " = " + notes.getId();
            values.put(NotesProvider.CLM_NOTES_NOTIFY_FLAG, 0);
            menu.getItem(0).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_notifications_off));
            getContentResolver().update(
                    NotesProvider.CONTENT_URI, values, where, null);
        }
    }

    private void editNotes() {
        Intent intent = new Intent(ViewNotesActivity.this,EditNotesActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("notes",notes);
        intent.putExtra("bundle",bundle);
        startActivity(intent);
        finish();
    }

    private void deleteNotes(long id) {
        Toast.makeText(ViewNotesActivity.this, "soon........", Toast.LENGTH_SHORT).show();
        getContentResolver().delete( NotesProvider.CONTENT_URI,
                NotesProvider.CLM_NOTES_ID + " = "+id,
                null);
        finish();
    }


    private void initViews() {
        notesDescription = findViewById(R.id.diary_view_description);
        viewedDate = findViewById(R.id.diary_view_date);
        viewedDay = findViewById(R.id.diary_view_day);
        viewedMonth = findViewById(R.id.diary_view_month_year);
        viewedTime = findViewById(R.id.diary_view_time);

    }

    private void setDetails() {
        notesDescription.setText(notes.getNotes());
        viewedDate.setText(String.valueOf(globalCal.get(Calendar.DATE)));
        viewedDay.setText(CommonUtils.getDay(globalCal.get(Calendar.DAY_OF_WEEK)));
        viewedMonth.setText(CommonUtils.getMonth(globalCal.get(Calendar.MONTH))+" "+globalCal.get(Calendar.YEAR));
        int am_pm=globalCal.get(Calendar.AM_PM);
        String Am_Pm= ((am_pm==Calendar.AM)?"am":"pm");
        viewedTime.setText(String.format("%02d",globalCal.get(Calendar.HOUR))+":"+String.format("%02d",globalCal.get(Calendar.MINUTE))+" "+Am_Pm);

    }
}
