package com.oasys.ediary;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import androidx.work.ListenableWorker;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.oasys.ediary.Activity.DiaryHome;
import com.oasys.ediary.Activity.ViewNotesActivity;
import com.oasys.ediary.DB.NotesProvider;
import com.oasys.ediary.Dto.Notes;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class NotificationWorker extends Worker {

//    private Context context;

    public NotificationWorker( Context context,  WorkerParameters workerParams) {
        super(context, workerParams);
    }


    @Override
    public ListenableWorker.Result doWork() {
        getNext15MinsTask();
        return Result.success();
    }



    private void getNext15MinsTask() {
        Log.d("notes123","inside start job........");
        Uri friends = NotesProvider.CONTENT_URI;
        /*long date = CommonUtils.atStartOfDay(new Date());
        long date_plus_15= new Date().getTime();*/
        long date_cur = new Date().getTime();
        long date_plus_15= date_cur+900000;
        String where = NotesProvider.CLM_NOTES_CREATED_DATE+" BETWEEN "+ date_cur+" AND "+date_plus_15 +" AND "+ NotesProvider.CLM_NOTES_NOTIFY_FLAG+" = 1";
        Cursor c = getApplicationContext().getContentResolver().query(friends, null, where, null, NotesProvider.CLM_NOTES_CREATED_DATE +" DESC");

        Log.d("notes123",c.getCount()+"------"+where);
        ArrayList<Notes> notesList=new ArrayList<>();
        while (c.moveToNext()) {
            Log.d("notes123",c.getString(c.getColumnIndex(NotesProvider.CLM_NOTES_DESCRIPTION))+"------");
            Notes notes = new Notes();
            notes.setNotes(c.getString(c.getColumnIndex(NotesProvider.CLM_NOTES_DESCRIPTION)));
            notes.setCreatedTs(c.getLong(c.getColumnIndex(NotesProvider.CLM_NOTES_CREATED_DATE)));
            notes.setId(c.getLong(c.getColumnIndex(NotesProvider.CLM_NOTES_ID)));
            notesList.add(notes);
        }
        String GROUP_KEY_WORK_EMAIL = "com.oasys.notes.notify";
        if(notesList.size()==1) {
            Notes notes = notesList.get(0);
            Intent intent123 =  new Intent(getApplicationContext(), ViewNotesActivity.class);
            intent123.putExtra("notifyMe",true);
            intent123.putExtra("Note",notes);
            PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 0,
                    intent123,PendingIntent.FLAG_UPDATE_CURRENT);
            String channel_id = createNotificationChannel(getApplicationContext());

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getApplicationContext(),channel_id)
                    .setContentTitle("Upcoming Remainder")
                    .setContentText(new SimpleDateFormat("hh:mm a").format(new Date(notes.getCreatedTs()))+":"+notes.getNotes())
                    .setAutoCancel(true)
                    .setContentIntent(pi)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setShowWhen(true)
                    .setColor(Color.RED)
                    .setGroup(GROUP_KEY_WORK_EMAIL)
                    .setLocalOnly(true);
//            administrator-Veriton-Series

            NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify((int) ((new Date(System.currentTimeMillis()).getTime() / 1000L) % Integer.MAX_VALUE) /* ID of notification */, notificationBuilder.build());



        }else if(notesList.size()>1){
            PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 0,
                    new Intent(getApplicationContext(), DiaryHome.class), PendingIntent.FLAG_UPDATE_CURRENT);
            String channel_id = createNotificationChannel(getApplicationContext());
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext(),channel_id)
                    .setContentTitle("Event tracker")
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentIntent(pi)
                    .setGroup(GROUP_KEY_WORK_EMAIL)
                    .setContentText("Events received");
            NotificationCompat.InboxStyle inboxStyle =
                    new NotificationCompat.InboxStyle();

            inboxStyle.setBigContentTitle("Event tracker details:");
            for (Notes notes : notesList) {
                inboxStyle.addLine(new SimpleDateFormat("hh:mm a").format(new Date(notes.getCreatedTs()))+":"+notes.getNotes());
            }
            mBuilder.setStyle(inboxStyle);
            NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify((int) ((new Date(System.currentTimeMillis()).getTime() / 1000L) % Integer.MAX_VALUE) /* ID of notification */, mBuilder.build());

        }
    }


    public static String createNotificationChannel(Context context) {

        // NotificationChannels are required for Notifications on O (API 26) and above.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            // The id of the channel.
            String channelId = "Channel_id";

            // The user-visible name of the channel.
            CharSequence channelName = "Application_name";
            // The user-visible description of the channel.
            String channelDescription = "Application_name Alert";
            int channelImportance = NotificationManager.IMPORTANCE_DEFAULT;
            boolean channelEnableVibrate = true;
//            int channelLockscreenVisibility = Notification.;

            // Initializes NotificationChannel.
            NotificationChannel notificationChannel = new NotificationChannel(channelId, channelName, channelImportance);
            notificationChannel.setDescription(channelDescription);
            notificationChannel.enableVibration(channelEnableVibrate);
//            notificationChannel.setLockscreenVisibility(channelLockscreenVisibility);

            // Adds NotificationChannel to system. Attempting to create an existing notification
            // channel with its original values performs no operation, so it's safe to perform the
            // below sequence.
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(notificationChannel);

            return channelId;
        } else {
            // Returns null for pre-O (26) devices.
            return null;
        }
    }
}
