package com.oasys.ediary.Dto;

import java.io.Serializable;

public class Notes implements Serializable {

    private long id;
    private String notes;
    private long createdTs;
    private long modifiedTs;
    private boolean syncStatus;
    private int notifyFlag;
    private int status;

    //Internal purpose
    private String notesTime;
    private String notesDate;
    private String notesMonth;
    private String notesDay;


    public int getNotifyFlag() {
        return notifyFlag;
    }

    public void setNotifyFlag(int notifyFlag) {
        this.notifyFlag = notifyFlag;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public long getCreatedTs() {
        return createdTs;
    }

    public void setCreatedTs(long createdTs) {
        this.createdTs = createdTs;
    }

    public long getModifiedTs() {
        return modifiedTs;
    }

    public void setModifiedTs(long modifiedTs) {
        this.modifiedTs = modifiedTs;
    }

    public boolean isSyncStatus() {
        return syncStatus;
    }

    public void setSyncStatus(boolean syncStatus) {
        this.syncStatus = syncStatus;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
