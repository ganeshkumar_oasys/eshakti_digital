package com.oasys.ediary.DB;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import java.util.HashMap;

public class NotesProvider extends ContentProvider {

    // fields for my content provider
    public static final String PROVIDER_NAME = "com.oasys.ediary.DB.NotesProvider";
    public static final String URL = "content://" + PROVIDER_NAME + "/notes";
    public static final Uri CONTENT_URI = Uri.parse(URL);


    // fields for the database
    public static final String CLM_NOTES_ID = "id";
    public static final String CLM_NOTES_DESCRIPTION = "DESCRIPTION";
    public static final String CLM_NOTES_CREATED_DATE = "created_ts";
    public static final String CLM_NOTES_MODIFIED_DATE = "modified_ts";
    public static final String CLM_NOTES_USER_ID = "user_id";
    public static final String CLM_NOTES_SYNC_FLAG = "sync";
    public static final String CLM_NOTES_NOTIFY_FLAG = "notify";
    public static final String CLM_NOTES_STATUS = "status";

    // integer values used in content URI
    public static final int NOTES = 1;
    public static final int NOTES_ID = 2;
    DBHelper dbHelper;

    // projection map for a query
    private static HashMap<String, String> BirthMap;

    // maps content URI "patterns" to the integer values that were set above
    static final UriMatcher uriMatcher;
    static{
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, "notes", NOTES);
        uriMatcher.addURI(PROVIDER_NAME, "notes/#", NOTES_ID);
    }

    // database declarations
    private SQLiteDatabase database;
    static final String DATABASE_NAME = "OASYS_NOTES";
    static final String TABLE_NOTES = "notes";
    static final int DATABASE_VERSION = 1;

    public static final String CREATE_NOTES_TABLE =
            "CREATE TABLE "+TABLE_NOTES +" ("
                    +CLM_NOTES_ID +" INTEGER primary key AUTOINCREMENT  NOT NULL , "
                    +CLM_NOTES_DESCRIPTION +" TEXT  NOT NULL , "
                    +CLM_NOTES_CREATED_DATE +" INTEGER  NOT NULL , "
                    +CLM_NOTES_MODIFIED_DATE +" INTEGER  NOT NULL , "
                    +CLM_NOTES_USER_ID +" INTEGER  NOT NULL , "
                    +CLM_NOTES_STATUS +" INTEGER  NOT NULL , "
                    +CLM_NOTES_NOTIFY_FLAG +" INTEGER  NOT NULL , "
                    +CLM_NOTES_SYNC_FLAG +" INTEGER  NOT NULL )";

    // class that creates and manages the provider's database
    private static class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
            // TODO Auto-generated constructor stub
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_NOTES_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " +  TABLE_NOTES);
            onCreate(db);
        }

    }

    @Override
    public boolean onCreate() {
        Context context = getContext();
        dbHelper = new DBHelper(context);
        // permissions to be writable
        database = dbHelper.getWritableDatabase();

        if(database == null)
            return false;
        else
            return true;
    }


    @Override
    public Cursor query( Uri uri,  String[] projection,  String selection,  String[] selectionArgs,  String sortOrder) {
        // TODO Auto-generated method stub
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        // the TABLE_NAME to query on
        queryBuilder.setTables(TABLE_NOTES);

        switch (uriMatcher.match(uri)) {
            // maps all database column names
            case NOTES:
                queryBuilder.setProjectionMap(BirthMap);
                break;
            case NOTES_ID:
                queryBuilder.appendWhere( CLM_NOTES_ID + "=" + uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        if (sortOrder == null || sortOrder == ""){
            // No sorting-> sort on names by default
            sortOrder = CLM_NOTES_CREATED_DATE;
        }
        Cursor cursor = queryBuilder.query(database, projection, selection,
                selectionArgs, null, null, sortOrder);
        /**
         * register to watch a content URI for changes
         */
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }



    @Override
    public Uri insert( Uri uri,  ContentValues values) {
        // TODO Auto-generated method stub
        long row = database.insert(TABLE_NOTES, "", values);

        // If record is added successfully
        if(row > 0) {
            Uri newUri = ContentUris.withAppendedId(CONTENT_URI, row);
            getContext().getContentResolver().notifyChange(newUri, null);
            return newUri;
        }
        throw new SQLException("Fail to add a new record into " + uri);
    }

    @Override
    public int delete( Uri uri,  String selection,  String[] selectionArgs) {
        // TODO Auto-generated method stub
        int count = 0;

        switch (uriMatcher.match(uri)){
            case NOTES:
                // delete all the records of the table
                count = database.delete(TABLE_NOTES, selection, selectionArgs);
                break;
            case NOTES_ID:
                String id = uri.getLastPathSegment(); //gets the id
                count = database.delete( TABLE_NOTES, CLM_NOTES_ID +  " = " + id +
                        (!TextUtils.isEmpty(selection) ? " AND (" +
                                selection + ')' : ""), selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int update( Uri uri,  ContentValues values,  String selection,  String[] selectionArgs) {
        // TODO Auto-generated method stub
        int count = 0;

        switch (uriMatcher.match(uri)){
            case NOTES:
                count = database.update(TABLE_NOTES, values, selection, selectionArgs);
                break;
            case NOTES_ID:
                count = database.update(TABLE_NOTES, values, CLM_NOTES_ID +
                        " = " + uri.getLastPathSegment() +
                        (!TextUtils.isEmpty(selection) ? " AND (" +
                                selection + ')' : ""), selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI " + uri );
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }


    @Override
    public String getType(Uri uri) {
        // TODO Auto-generated method stub
        switch (uriMatcher.match(uri)){
            // Get all friend-birthday records
            case NOTES:
                return "vnd.android.cursor.dir/notes";
            // Get a particular friend
            case NOTES_ID:
                return "vnd.android.cursor.item/notes";
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }
}
