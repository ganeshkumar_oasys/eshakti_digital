package com.oasys.ediary.Fragment;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.oasys.ediary.Adapter.DailyNotesAdapter;
import com.oasys.ediary.DB.NotesProvider;
import com.oasys.ediary.Dto.Notes;
import com.oasys.ediary.R;
import com.oasys.ediary.Utils.CommonUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;

public class HomeFragment extends Fragment {
    private FloatingActionButton floatingBtn;
    private RecyclerView homeRecycler;
    private CardView noDataCard;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.ediary_fragment_home,container,false);
        init(view);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getData();
    }

    private void getData() {
        try {
            LinkedHashMap<Long, ArrayList<Notes>> map = new LinkedHashMap<>();
            Uri friends = NotesProvider.CONTENT_URI;
            String where =NotesProvider.CLM_NOTES_CREATED_DATE+" >= "+new Date().getTime();
                    Cursor c = getActivity().getContentResolver().query(friends, null, where, null, NotesProvider.CLM_NOTES_CREATED_DATE +" ASC");

            Log.d("count value",c.getCount()+"");

            while (c.moveToNext()) {
                Long date = c.getLong(c.getColumnIndex(NotesProvider.CLM_NOTES_CREATED_DATE));
             /*   Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date(date));*/
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
                Notes notes = new Notes();
                notes.setNotes(c.getString(c.getColumnIndex(NotesProvider.CLM_NOTES_DESCRIPTION)));
                notes.setNotifyFlag(c.getInt(c.getColumnIndex(NotesProvider.CLM_NOTES_NOTIFY_FLAG)));
                notes.setId(c.getLong(c.getColumnIndex(NotesProvider.CLM_NOTES_ID)));
                notes.setCreatedTs(c.getLong(c.getColumnIndex(NotesProvider.CLM_NOTES_CREATED_DATE)));
                Long key = CommonUtils.atStartOfDay(new Date(date));
                if (map.size() == 0) {
                    //add first time
                    ArrayList<Notes> notesList = new ArrayList<>();
                    notesList.add(notes);
                    map.put(key, notesList);
                } else {
                    if (map.containsKey(key)) {
                        //Update list
                        ArrayList<Notes> temp = map.get(key);
                        temp.add(notes);
                        map.put(key, temp);
                    } else {
                        //add List
                        ArrayList<Notes> notesList = new ArrayList<>();
                        notesList.add(notes);
                        map.put(key, notesList);
                    }
                }
                Log.d("long value",key+"");
            }
            Log.d("map size",map.size()+"");
            if(map.size()>0){
                setAdapter(map);
            }
        }catch (Exception e){
            noDataCard.setVisibility(View.VISIBLE);
            homeRecycler.setVisibility(View.GONE);
        }
    }

    private void setAdapter(LinkedHashMap<Long, ArrayList<Notes>> map) {
        DailyNotesAdapter notesAdapter = new DailyNotesAdapter(getActivity(),map);
        homeRecycler.setAdapter(notesAdapter);
        homeRecycler.setVisibility(View.VISIBLE);
        homeRecycler.setNestedScrollingEnabled(true);
        noDataCard.setVisibility(View.GONE);
    }

    private void init(View view) {
        homeRecycler = view.findViewById(R.id.home_recycler);
        homeRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        noDataCard = view.findViewById(R.id.home_nodata);
    }


}
