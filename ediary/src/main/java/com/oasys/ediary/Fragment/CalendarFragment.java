package com.oasys.ediary.Fragment;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.oasys.ediary.Activity.DiaryHome;
import com.oasys.ediary.Adapter.DailyNotesChildAdapter;
import com.oasys.ediary.DB.NotesProvider;
import com.oasys.ediary.Dto.Notes;
import com.oasys.ediary.R;
import com.oasys.ediary.Utils.CommonUtils;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;

import org.threeten.bp.DateTimeUtils;
import org.threeten.bp.Instant;
import org.threeten.bp.LocalDate;
import org.threeten.bp.ZoneId;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Locale;


public class CalendarFragment extends Fragment {

    final String DATE_FORMAT = "yyyy-MM-dd";
    private MaterialCalendarView calendarView;

    private TextView viewDate,viewMonth,viewDay,viewYear;
    private RecyclerView viewChildRecycler;
    private long selectedTS;
    private LinkedHashMap<Long, ArrayList<Notes>> map;
    private LinearLayout parentMainLayer;
    private RelativeLayout parentDateLayer;
    private TextView notesCount;
    private HashSet<CalendarDay> daysToDecorate;
    private DiaryHome mCallback;


    interface FragmentCallback {
        void onClickButton(CalendarFragment fragment);
    }
    
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (DiaryHome) context;
        } catch (ClassCastException e) {
            Log.e("Custom TAG", "ClassCastException", e);
        }
    }

    @Override
    public void onDetach() {
        mCallback = null;
        super.onDetach();

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.ediary_fragment_calendar,container,false);
        init(view);
        return view;
    }

    private void init(View view) {
        calendarView = view.findViewById(R.id.calendarView);
        calendarView.setShowOtherDates(MaterialCalendarView.SHOW_ALL);
        parentMainLayer = view.findViewById(R.id.main_layer_parent);
        parentDateLayer = view.findViewById(R.id.date_layer_parent);
        notesCount = view.findViewById(R.id.notes_count);
        parentDateLayer.setVisibility(View.GONE);
        final LocalDate min = getLocalDate("2019-01-01");
        /* CalendarDay date= CalendarDay.today();
           calendarView.state().edit().setMinimumDate(CalendarDay.from(date.getYear(), date.getMonth(), 1)).commit();
       */
        calendarView.setShowOtherDates(MaterialCalendarView.SHOW_ALL);
        calendarView.setSelectionColor(ContextCompat.getColor(getActivity(),R.color.blue));
            calendarView.state().edit().setMinimumDate(min).commit();
        calendarView.setSelectedDate(LocalDate.now());

        viewDate = view.findViewById(R.id.diary_view_date);
        viewMonth= view.findViewById(R.id.diary_view_month);
        viewDay = view.findViewById(R.id.diary_view_day);
        viewYear = view.findViewById(R.id.diary_view_year);
        viewChildRecycler = view.findViewById(R.id.diary_parent_recycler);
        viewChildRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));

        calendarView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(MaterialCalendarView widget, CalendarDay date, boolean selected) {
                Instant instant = Instant.from(date.getDate().atStartOfDay(ZoneId.of("GMT")));
                Date daySelected = DateTimeUtils.toDate(instant);
                selectedTS =CommonUtils.atStartOfDay(daySelected);
                setAdapter();
                if(CommonUtils.atStartOfDay(daySelected)<CommonUtils.atStartOfDay(new Date())){
                mCallback.floatingBtn.hide();
                }else{
                    mCallback.floatingBtn.show();
                }
            }
        });
        calendarView.setOnMonthChangedListener(new OnMonthChangedListener() {
            @Override
            public void onMonthChanged(MaterialCalendarView widget, CalendarDay date) {
                Instant instant = Instant.from(date.getDate().atStartOfDay(ZoneId.of("GMT")));
                Date daySelected = DateTimeUtils.toDate(instant);
                selectedTS =CommonUtils.atStartOfDay(daySelected);
                getData(daySelected);
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();

        Instant instant = Instant.from(calendarView.getSelectedDate().getDate().atStartOfDay(ZoneId.of("GMT")));
        Date daySelected = DateTimeUtils.toDate(instant);
        selectedTS =CommonUtils.atStartOfDay(daySelected);
        Log.d("date",""+daySelected);
        getData(daySelected);
    }

    LocalDate getLocalDate(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH);
        try {
            Date input = sdf.parse(date);
            Calendar cal = Calendar.getInstance();
            cal.setTime(input);
            return LocalDate.of(cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH) + 1,
                    cal.get(Calendar.DAY_OF_MONTH));


        } catch (NullPointerException e) {
            return null;
        } catch (ParseException e) {
            return null;
        }
    }


    private void getData(Date cDate) {
        try {
            daysToDecorate = new HashSet<>();
            map = new LinkedHashMap<>();
            Uri friends = NotesProvider.CONTENT_URI;
            String where =NotesProvider.CLM_NOTES_CREATED_DATE+" BETWEEN "+ CommonUtils.atStartOfMonth(cDate)+" AND "+CommonUtils.atEndOfMonth(cDate);
            Log.d("query","   "+where);
            Cursor c = getActivity().getContentResolver().query(friends, null, where, null, NotesProvider.CLM_NOTES_CREATED_DATE +" DESC");
            Log.d("count",""+c.getCount());
            while (c.moveToNext()) {
                Long date = c.getLong(c.getColumnIndex(NotesProvider.CLM_NOTES_CREATED_DATE));
                /* Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date(date));*/
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
                Notes notes = new Notes();
                notes.setNotes(c.getString(c.getColumnIndex(NotesProvider.CLM_NOTES_DESCRIPTION)));
                notes.setId(c.getLong(c.getColumnIndex(NotesProvider.CLM_NOTES_ID)));
                notes.setNotifyFlag(c.getInt(c.getColumnIndex(NotesProvider.CLM_NOTES_NOTIFY_FLAG)));
                notes.setCreatedTs(c.getLong(c.getColumnIndex(NotesProvider.CLM_NOTES_CREATED_DATE)));
                Long key = CommonUtils.atStartOfDay(new Date(date));
                Calendar calendar =Calendar.getInstance();
                calendar.setTime(new Date(date));
                CalendarDay calendarDay = CalendarDay.from(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH)+1,calendar.get(Calendar.DAY_OF_MONTH));
                daysToDecorate.add(calendarDay);
                if (map.size() == 0) {
                    //add first time
                    ArrayList<Notes> notesList = new ArrayList<>();
                    notesList.add(notes);
                    map.put(key, notesList);
                } else {
                    if (map.containsKey(key)) {
                        //Update list
                        ArrayList<Notes> temp = map.get(key);
                        temp.add(notes);
                        map.put(key, temp);
                    } else {
                        //add List
                        ArrayList<Notes> notesList = new ArrayList<>();
                        notesList.add(notes);
                        map.put(key, notesList);
                    }
                }
                Log.d("long value",key+"");

//                Log.d("date", simpleDateFormat.format(new Date(date)) + "--" + calendar.get(Calendar.DATE) + "-" + calendar.get(Calendar.YEAR) + "-" + calendar.get(Calendar.DAY_OF_MONTH) + "-" + calendar.get(Calendar.HOUR) + "-" + calendar.get(Calendar.MINUTE) + "-" + calendar.get(Calendar.MONTH));
            }
            if(map.size()>0){
                setAdapter();
                decorateCalendar();
            }
        }catch (Exception e){
           /* noDataCard.setVisibility(View.VISIBLE);
            homeRecycler.setVisibility(View.GONE);*/
        }
    }

    private void decorateCalendar() {
        calendarView.removeDecorators();/*
        calendarView.addDecorator(new DayViewDecorator() {
            @Override
            public boolean shouldDecorate(CalendarDay day) {
                return daysToDecorate.contains(day);
            }

            @Override
            public void decorate(DayViewFacade view) {
                int color = ContextCompat.getColor(getActivity(), R.color.colorAccent);
                view.addSpan(new DotSpan(10, color));
            }
        });*/
        calendarView.addDecorator(new DayViewDecorator() {
            @Override
            public boolean shouldDecorate(CalendarDay day) {
                return daysToDecorate.contains(day);
            }

            @Override
            public void decorate(DayViewFacade view) {

                view.setBackgroundDrawable(getActivity().getResources().getDrawable(R.drawable.circle_green));
            }
        });
    }

    private void setAdapter() {
        ArrayList<Notes> list = map.get(selectedTS);
        if(list!=null&&list.size()>0) {
            DailyNotesChildAdapter childAdapter = new DailyNotesChildAdapter(getActivity(), list);
            viewChildRecycler.setAdapter(childAdapter);
            parentMainLayer.setVisibility(View.VISIBLE);
            notesCount.setText("*********** Total Notes("+list.size()+") ***********");
        }else{
            parentMainLayer.setVisibility(View.GONE);
            notesCount.setText("*********** No Notes Found ***********");
        }
    }
}
