package com.oasys.ediary.Adapter;

import android.content.Context;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oasys.ediary.Dto.Notes;
import com.oasys.ediary.R;
import com.oasys.ediary.Utils.CommonUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;

public class DailyNotesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context context;
    private final LinkedHashMap<Long, ArrayList<Notes>> map;

    public DailyNotesAdapter(Context context, LinkedHashMap<Long,ArrayList<Notes>> map){
        this.context = context;
        this.map = map;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder( ViewGroup viewGroup, int i) {
        return new DailyNotesParentViewHolder(LayoutInflater.from(context).inflate(R.layout.ediary_list_item_notes_parent,viewGroup,false));
    }

    @Override
    public void onBindViewHolder( RecyclerView.ViewHolder viewHolder, int i) {
        DailyNotesParentViewHolder placeViewHolder = (DailyNotesParentViewHolder) viewHolder;
        Long firstKey = (Long) map.keySet().toArray()[i];
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(firstKey));
        placeViewHolder.viewDate.setText(String.valueOf(calendar.get(Calendar.DATE)));
        String month = CommonUtils.getMonth(calendar.get(Calendar.MONTH));
        placeViewHolder.viewMonth.setText(month.substring(0,4));
        placeViewHolder.viewDay.setText(CommonUtils.getDay(calendar.get(Calendar.DAY_OF_WEEK)));
        placeViewHolder.viewYear.setText(String.valueOf(calendar.get(Calendar.YEAR)));
        ArrayList<Notes> notesList = map.get(firstKey);
        DailyNotesChildAdapter childAdapter = new DailyNotesChildAdapter(context,notesList) ;
        placeViewHolder.viewChildRecycler.setAdapter(childAdapter);

    }

    @Override
    public int getItemCount() {
        return map.size();
    }

    public class DailyNotesParentViewHolder extends RecyclerView.ViewHolder{

        private final TextView viewDate,viewMonth,viewDay,viewYear;
        private final RecyclerView viewChildRecycler;

        public DailyNotesParentViewHolder( View itemView) {
            super(itemView);
            viewDate = itemView.findViewById(R.id.diary_view_date);
            viewMonth= itemView.findViewById(R.id.diary_view_month);
            viewDay = itemView.findViewById(R.id.diary_view_day);
            viewYear = itemView.findViewById(R.id.diary_view_year);
            viewChildRecycler = itemView.findViewById(R.id.diary_parent_recycler);
            viewChildRecycler.setLayoutManager(new LinearLayoutManager(context));
        }
    }
}
