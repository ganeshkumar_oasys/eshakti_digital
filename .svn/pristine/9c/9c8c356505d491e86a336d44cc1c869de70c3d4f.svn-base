package com.oasys.ediary.Activity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.oasys.ediary.DB.NotesProvider;
import com.oasys.ediary.R;
import com.oasys.ediary.Utils.CommonUtils;
import java.util.Calendar;
import java.util.Date;

public class AddNotesActivity extends AppCompatActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener,TimePickerDialog.OnTimeSetListener  {

    private AppCompatEditText notesDescription;
    private Button saveBtn;
    private TextView addedDate,addedDay,addedMonth,addedTime;

    Calendar globalCal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ediary_activity_add_notes);
        initToolbar();
        initViews();
        Intent intent = getIntent();
        globalCal = Calendar.getInstance();
        Date currentDate = new Date();
        if(intent!=null&&intent.getExtras()!=null){
            long ts = intent.getExtras().getLong("date");
            currentDate = new Date(ts);
        }
        globalCal.setTime(currentDate);
        setDetails();

    }

    private void initViews() {
        notesDescription = findViewById(R.id.diary_add_notes);
        addedDate = findViewById(R.id.diary_add_date);
        addedDay = findViewById(R.id.diary_add_day);
        addedMonth = findViewById(R.id.diary_add_month_year);
        addedTime = findViewById(R.id.diary_add_time);
        LinearLayout dateInfo = findViewById(R.id.date_info_layer);
        saveBtn = findViewById(R.id.diary_add_btn);
        
        dateInfo.setOnClickListener(this::onClick);
        saveBtn.setOnClickListener(this::onClick);
        addedDate.setOnClickListener(this::onClick);
        addedTime.setOnClickListener(this::onClick);
    }

    private void setDetails() {
        addedDate.setText(String.valueOf(globalCal.get(Calendar.DATE)));
        addedDay.setText(CommonUtils.getDay(globalCal.get(Calendar.DAY_OF_WEEK)));
        addedMonth.setText(CommonUtils.getMonth(globalCal.get(Calendar.MONTH))+" "+globalCal.get(Calendar.YEAR));
        int am_pm=globalCal.get(Calendar.AM_PM);
        String Am_Pm= ((am_pm==Calendar.AM)?"am":"pm");
        addedTime.setText(String.format("%02d",globalCal.get(Calendar.HOUR))+":"+String.format("%02d",globalCal.get(Calendar.MINUTE))+" "+Am_Pm);
    }

    private void initToolbar() {
        Toolbar mToolbar =  findViewById(R.id.diary_add_toolbar);
        setSupportActionBar(mToolbar);
        setTitle(getString(R.string.add_title));
        mToolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.diary_add_btn){
            addNotes();

        }else if(v.getId()==R.id.diary_add_date){
            callDatePicker();

        }else if(v.getId()==R.id.date_info_layer){
            callDatePicker();

        }else if(v.getId()==R.id.diary_add_time){
            callTimePicker();

        }
    }

    private void callTimePicker() {
        /*TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(this,globalCal.get(Calendar.HOUR_OF_DAY),globalCal.get(Calendar.MINUTE),globalCal.get(Calendar.SECOND),false);
        timePickerDialog.show(getSupportFragmentManager(),"Timepickerdialog");
*/

        TimePickerDialog timePickerDialog = new TimePickerDialog(AddNotesActivity.this,this,globalCal.get(Calendar.HOUR_OF_DAY),globalCal.get(Calendar.MINUTE),false);
//        timePickerDialog.get
        timePickerDialog.show();
    }

    private void addNotes(){
        String description = notesDescription.getText().toString();
        if(!description.isEmpty()){
            addNotes(description);
        }else{
            Toast.makeText(AddNotesActivity.this, "Enter details to save!!!", Toast.LENGTH_SHORT).show();
        }
    }



    private void callDatePicker() {
      /*  DatePickerDialog dpd = DatePickerDialog.newInstance(
                AddNotesActivity.this,
                globalCal.get(Calendar.YEAR), // Initial year selection
                globalCal.get(Calendar.MONTH), // Initial month selection
                globalCal.get(Calendar.DAY_OF_MONTH) // Inital day selection
        );
        dpd.show(getFragmentManager(), "Datepickerdialog");*/
        final android.app.DatePickerDialog datePickerDialog = new android.app.DatePickerDialog(AddNotesActivity.this,
                this,
                globalCal.get(Calendar.YEAR), // Initial year selection
                globalCal.get(Calendar.MONTH), // Initial month selection
                globalCal.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMinDate(0);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
//        globalCal.set(cyear, cmonth, cday);
//        datePickerDialog.getDatePicker().setMaxDate(globalCal.getTimeInMillis());
        datePickerDialog.show();
    }

    private  void addNotes(String data){
        ContentValues values = new ContentValues();

        values.put(NotesProvider.CLM_NOTES_DESCRIPTION,
                data);
        values.put(NotesProvider.CLM_NOTES_CREATED_DATE,
                globalCal.getTimeInMillis());
        values.put(NotesProvider.CLM_NOTES_MODIFIED_DATE,
                globalCal.getTimeInMillis());
        values.put(NotesProvider.CLM_NOTES_USER_ID,
                0);
        values.put(NotesProvider.CLM_NOTES_STATUS,
                1);
        values.put(NotesProvider.CLM_NOTES_SYNC_FLAG,
                0);
        values.put(NotesProvider.CLM_NOTES_NOTIFY_FLAG,
                1);
        Uri uri = getContentResolver().insert(
                NotesProvider.CONTENT_URI, values);
        Toast.makeText(AddNotesActivity.this, "saved successfuly!!!", Toast.LENGTH_SHORT).show();
        finish();


    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        globalCal.set(year,month,dayOfMonth);
        addedDate.setText(String.valueOf(dayOfMonth));
        addedDay.setText(CommonUtils.getDay(globalCal.get(Calendar.DAY_OF_WEEK)));
        addedMonth.setText(CommonUtils.getMonth(month)+" "+year);

    }


    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                globalCal.set(Calendar.HOUR_OF_DAY,hourOfDay);
                globalCal.set(Calendar.MINUTE,minute);
                int am_pm=globalCal.get(Calendar.AM_PM);
                String Am_Pm= ((am_pm==Calendar.AM)?"am":"pm");
                addedTime.setText(String.format("%02d",globalCal.get(Calendar.HOUR))+":"+String.format("%02d",globalCal.get(Calendar.MINUTE))+" "+Am_Pm);
    }
}
