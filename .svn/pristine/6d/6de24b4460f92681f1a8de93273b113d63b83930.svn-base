package com.yesteam.eshakti.sqlite.database;

import java.util.ArrayList;
import java.util.List;

import com.oasys.eshakti.digitization.EShaktiApplication;
import com.yesteam.eshakti.sqlite.db.model.GetGroupMemberDetails;
import com.yesteam.eshakti.sqlite.db.model.GroupDetailsUpdate;
import com.yesteam.eshakti.sqlite.db.model.GroupFOSUpdate;
import com.yesteam.eshakti.sqlite.db.model.GroupLoanOSUpdate;
import com.yesteam.eshakti.sqlite.db.model.GroupMLUpdate;
import com.yesteam.eshakti.sqlite.db.model.GroupMaster;
import com.yesteam.eshakti.sqlite.db.model.GroupMasterSingle;
import com.yesteam.eshakti.sqlite.db.model.LoanAccountNoDetailsUpdate;
import com.yesteam.eshakti.sqlite.db.model.LoanAccountUpdate;
import com.yesteam.eshakti.sqlite.db.model.TransIdUpdate;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class GroupDetailsProvider {
	private static SQLiteDatabase sqLiteDatabase;
	private static DataHelper dataHelper;
	static Cursor cursor;

	public GroupDetailsProvider(Activity activity) {
		dataHelper = new DataHelper(activity);
	}

	public static void openDatabase() {
		dataHelper = new DataHelper(EShaktiApplication.getInstance());
		dataHelper.onOpen(sqLiteDatabase);
		sqLiteDatabase = dataHelper.getWritableDatabase();

	}

	public static void closeDatabase() {
		// dataHelper.close();
		if (sqLiteDatabase != null && sqLiteDatabase.isOpen()) {
			sqLiteDatabase.close();
		}
	}

	public interface GroupMasterColumn {
		public static final String G_ID = "local_Id";
		public static final String TRAINERUSERID = "Traineruserid";
		public static final String GROUPID = "Groupid";
		public static final String GROUPRESPONSE = "Groupresponse";
		public static final String PERSONALLOANOUTSTANDING = "Personalloanoutstanding";
		public static final String MEMBERLOANOUTSTANDING = "Memberloanoutstanding";
		public static final String GROUPLOANOUTSTANDING = "Grouploanoutstanding";
		public static final String FIXEDDEPOSITOUTSTANDING = "Fixeddepositoutstanding";
		public static final String AGENTPROFILE = "Agentprofile";
		public static final String GROUPPROFILE = "Groupprofile";
		public static final String PURPOSEOFLOAN = "Purposeofloan";
		public static final String MASTEROFMINUTES = "Masterofminutes";
		public static final String INDIVIDUALGROUPMINUTES = "Individualgroupminutes";
		public static final String GROUPTRANSID = "Grouptransid";
		public static final String LOANACCBANKDETAILS = "LOANACCBANKDETAILS";
		public static final String BANKACCOUNTNODETAILS = "BANKACCOUNTNODETAILS";
		public static final String LOANACCOUNTOUTSTANDINGDETAILS = "LOANACCOUNTOUTSTANDINGDETAILS";

	}

	public static final String CREATE_GROUP_DETAILS_TABLE = "create table " + DataHelper.TABLES.TABLE_GROUP_DETAILS
			+ "(" + GroupMasterColumn.G_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + GroupMasterColumn.TRAINERUSERID
			+ " TEXT," + GroupMasterColumn.GROUPID + " text," + GroupMasterColumn.GROUPRESPONSE + " text,"
			+ GroupMasterColumn.PERSONALLOANOUTSTANDING + " text," + GroupMasterColumn.MEMBERLOANOUTSTANDING + " text,"
			+ GroupMasterColumn.GROUPLOANOUTSTANDING + " text," + GroupMasterColumn.FIXEDDEPOSITOUTSTANDING + " text,"
			+ GroupMasterColumn.AGENTPROFILE + " text," + GroupMasterColumn.GROUPPROFILE + " text,"
			+ GroupMasterColumn.PURPOSEOFLOAN + " text," + GroupMasterColumn.MASTEROFMINUTES + " text,"
			+ GroupMasterColumn.INDIVIDUALGROUPMINUTES + " text," + GroupMasterColumn.GROUPTRANSID + " text,"
			+ GroupMasterColumn.LOANACCBANKDETAILS + " text" + ")";

	public static final String CREATE_GROUP_DETAILS_TABLE_NEW = "create table " + DataHelper.TABLES.TABLE_GROUP_DETAILS
			+ "(" + GroupMasterColumn.G_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + GroupMasterColumn.TRAINERUSERID
			+ " TEXT," + GroupMasterColumn.GROUPID + " text," + GroupMasterColumn.GROUPRESPONSE + " text,"
			+ GroupMasterColumn.PERSONALLOANOUTSTANDING + " text," + GroupMasterColumn.MEMBERLOANOUTSTANDING + " text,"
			+ GroupMasterColumn.GROUPLOANOUTSTANDING + " text," + GroupMasterColumn.FIXEDDEPOSITOUTSTANDING + " text,"
			+ GroupMasterColumn.AGENTPROFILE + " text," + GroupMasterColumn.GROUPPROFILE + " text,"
			+ GroupMasterColumn.PURPOSEOFLOAN + " text," + GroupMasterColumn.MASTEROFMINUTES + " text,"
			+ GroupMasterColumn.INDIVIDUALGROUPMINUTES + " text," + GroupMasterColumn.GROUPTRANSID + " text,"
			+ GroupMasterColumn.LOANACCBANKDETAILS + " text," + GroupMasterColumn.BANKACCOUNTNODETAILS + " text,"
			+ GroupMasterColumn.LOANACCOUNTOUTSTANDINGDETAILS + " text" + ")";

	public static boolean addGroupDetails(GroupMaster groupMaster) {
		try {

			openDatabase();
			ContentValues contentValues = new ContentValues();
			contentValues.put(GroupMasterColumn.TRAINERUSERID, groupMaster.getTrainerUserId_master());

			contentValues.put(GroupMasterColumn.GROUPID, groupMaster.getGroupId_master());
			contentValues.put(GroupMasterColumn.GROUPRESPONSE, groupMaster.getGroupMasterresponse_master());
			contentValues.put(GroupMasterColumn.PERSONALLOANOUTSTANDING, groupMaster.getPersonalloanOS_master());

			contentValues.put(GroupMasterColumn.MEMBERLOANOUTSTANDING, groupMaster.getMemberloanOS_master());

			contentValues.put(GroupMasterColumn.GROUPLOANOUTSTANDING, groupMaster.getGrouploanOS_master());

			contentValues.put(GroupMasterColumn.FIXEDDEPOSITOUTSTANDING, groupMaster.getFixeddepositOS_master());

			contentValues.put(GroupMasterColumn.AGENTPROFILE, groupMaster.getAgentprofile());

			contentValues.put(GroupMasterColumn.GROUPPROFILE, groupMaster.getGroupprofile());

			contentValues.put(GroupMasterColumn.PURPOSEOFLOAN, groupMaster.getPurposeofloan());

			contentValues.put(GroupMasterColumn.MASTEROFMINUTES, groupMaster.getMasterminutes());

			contentValues.put(GroupMasterColumn.INDIVIDUALGROUPMINUTES, groupMaster.getIndividualgroupminutes());
			contentValues.put(GroupMasterColumn.GROUPTRANSID, groupMaster.getGrouptransId());
			contentValues.put(GroupMasterColumn.LOANACCBANKDETAILS, groupMaster.getLoanAccBankDetails());
			contentValues.put(GroupMasterColumn.BANKACCOUNTNODETAILS, groupMaster.getBankAccNoDetails());
			contentValues.put(GroupMasterColumn.LOANACCOUNTOUTSTANDINGDETAILS, groupMaster.getLoanOSDetails());

			long result = sqLiteDatabase.insert(DataHelper.TABLES.TABLE_GROUP_DETAILS, null, contentValues);
			return result != -1;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			closeDatabase();
		}
		return false;
	}

	public static List<GroupMaster> getGroupDetails() {
		try {
			openDatabase();
			final List<GroupMaster> groupMasters = new ArrayList<GroupMaster>();
			cursor = sqLiteDatabase.query(DataHelper.TABLES.TABLE_GROUP_DETAILS, null, null, null, null, null, null);
			if (cursor.moveToFirst()) {
				String grp_traineruserid = null;
				String grp_groupid = null;
				String grp_groupresponse = null;
				String grp_personalloanos = null;
				String grp_memberloanos = null;
				String grp_grouploanos = null;
				String grp_fixeddepositos = null;
				String grp_agentprofile = null;
				String grp_groupprofile = null;

				String grp_purposeofloan = null;
				String grp_masterminutes = null;
				String grp_individualgrpminutes = null;

				String grp_grouptransId = null;
				String grp_loanaccbankdetails = null;
				String grp_bankaccdetails = null;
				String grp_loanOSdetails = null;

				int localId = -1;
				do {
					localId = cursor.getInt(cursor.getColumnIndex(GroupMasterColumn.G_ID));
					grp_traineruserid = cursor.getString(cursor.getColumnIndex(GroupMasterColumn.TRAINERUSERID));

					grp_groupid = cursor.getString(cursor.getColumnIndex(GroupMasterColumn.GROUPID));
					grp_groupresponse = cursor.getString(cursor.getColumnIndex(GroupMasterColumn.GROUPRESPONSE));
					grp_personalloanos = cursor
							.getString(cursor.getColumnIndex(GroupMasterColumn.PERSONALLOANOUTSTANDING));

					grp_memberloanos = cursor.getString(cursor.getColumnIndex(GroupMasterColumn.MEMBERLOANOUTSTANDING));

					grp_grouploanos = cursor.getString(cursor.getColumnIndex(GroupMasterColumn.GROUPLOANOUTSTANDING));

					grp_fixeddepositos = cursor
							.getString(cursor.getColumnIndex(GroupMasterColumn.FIXEDDEPOSITOUTSTANDING));
					grp_agentprofile = cursor.getString(cursor.getColumnIndex(GroupMasterColumn.AGENTPROFILE));
					grp_groupprofile = cursor.getString(cursor.getColumnIndex(GroupMasterColumn.GROUPPROFILE));
					grp_purposeofloan = cursor.getString(cursor.getColumnIndex(GroupMasterColumn.PURPOSEOFLOAN));

					grp_masterminutes = cursor.getString(cursor.getColumnIndex(GroupMasterColumn.MASTEROFMINUTES));

					grp_individualgrpminutes = cursor
							.getString(cursor.getColumnIndex(GroupMasterColumn.INDIVIDUALGROUPMINUTES));

					grp_grouptransId = cursor.getString(cursor.getColumnIndex(GroupMasterColumn.GROUPTRANSID));
					grp_loanaccbankdetails = cursor
							.getString(cursor.getColumnIndex(GroupMasterColumn.LOANACCBANKDETAILS));

					grp_bankaccdetails = cursor
							.getString(cursor.getColumnIndex(GroupMasterColumn.BANKACCOUNTNODETAILS));
					grp_loanOSdetails = cursor
							.getString(cursor.getColumnIndex(GroupMasterColumn.LOANACCOUNTOUTSTANDINGDETAILS));

					groupMasters.add(new GroupMaster(localId, grp_traineruserid, grp_groupid, grp_groupresponse,
							grp_personalloanos, grp_memberloanos, grp_grouploanos, grp_fixeddepositos, grp_agentprofile,
							grp_groupprofile, grp_purposeofloan, grp_masterminutes, grp_individualgrpminutes,
							grp_grouptransId, grp_loanaccbankdetails, grp_bankaccdetails, grp_loanOSdetails));
				} while (cursor.moveToNext());
			}

			return groupMasters;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			cursor.close();
			closeDatabase();
		}
		return null;
	}

	public static List<GroupMaster> getSingleGroupDetails() {
		try {
			openDatabase();
			final List<GroupMaster> groupMasters = new ArrayList<GroupMaster>();

			Cursor cursor = sqLiteDatabase.query(DataHelper.TABLES.TABLE_GROUP_DETAILS, null,
					GroupMasterColumn.GROUPID + " =?", new String[] { EShaktiApplication.getGetsinglegrpId() }, null,
					null, null);

			if (cursor.moveToFirst()) {
				String grp_traineruserid = null;
				String grp_groupid = null;
				String grp_groupresponse = null;
				String grp_personalloanos = null;
				String grp_memberloanos = null;
				String grp_grouploanos = null;
				String grp_fixeddepositos = null;

				String grp_agentprofile = null;
				String grp_groupprofile = null;

				String grp_purposeofloan = null;
				String grp_masterminutes = null;
				String grp_individualgrpminutes = null;

				String grp_grouptransid = null;

				String grp_loanaccbankdetail = null;
				String grp_bankaccdetails = null;
				String grp_loanOSdetails = null;
				int localId = -1;
				do {
					localId = cursor.getInt(cursor.getColumnIndex(GroupMasterColumn.G_ID));
					grp_traineruserid = cursor.getString(cursor.getColumnIndex(GroupMasterColumn.TRAINERUSERID));

					grp_groupid = cursor.getString(cursor.getColumnIndex(GroupMasterColumn.GROUPID));
					grp_groupresponse = cursor.getString(cursor.getColumnIndex(GroupMasterColumn.GROUPRESPONSE));
					grp_personalloanos = cursor
							.getString(cursor.getColumnIndex(GroupMasterColumn.PERSONALLOANOUTSTANDING));

					grp_memberloanos = cursor.getString(cursor.getColumnIndex(GroupMasterColumn.MEMBERLOANOUTSTANDING));

					grp_grouploanos = cursor.getString(cursor.getColumnIndex(GroupMasterColumn.GROUPLOANOUTSTANDING));

					grp_fixeddepositos = cursor
							.getString(cursor.getColumnIndex(GroupMasterColumn.FIXEDDEPOSITOUTSTANDING));

					grp_agentprofile = cursor.getString(cursor.getColumnIndex(GroupMasterColumn.AGENTPROFILE));
					grp_groupprofile = cursor.getString(cursor.getColumnIndex(GroupMasterColumn.GROUPPROFILE));

					grp_purposeofloan = cursor.getString(cursor.getColumnIndex(GroupMasterColumn.PURPOSEOFLOAN));

					grp_masterminutes = cursor.getString(cursor.getColumnIndex(GroupMasterColumn.MASTEROFMINUTES));

					grp_individualgrpminutes = cursor
							.getString(cursor.getColumnIndex(GroupMasterColumn.INDIVIDUALGROUPMINUTES));

					grp_grouptransid = cursor.getString(cursor.getColumnIndex(GroupMasterColumn.GROUPTRANSID));

					grp_loanaccbankdetail = cursor
							.getString(cursor.getColumnIndex(GroupMasterColumn.LOANACCBANKDETAILS));
					grp_bankaccdetails = cursor
							.getString(cursor.getColumnIndex(GroupMasterColumn.BANKACCOUNTNODETAILS));
					grp_loanOSdetails = cursor
							.getString(cursor.getColumnIndex(GroupMasterColumn.LOANACCOUNTOUTSTANDINGDETAILS));

					groupMasters.add(new GroupMaster(localId, grp_traineruserid, grp_groupid, grp_groupresponse,
							grp_personalloanos, grp_memberloanos, grp_grouploanos, grp_fixeddepositos, grp_agentprofile,
							grp_groupprofile, grp_purposeofloan, grp_masterminutes, grp_individualgrpminutes,
							grp_grouptransid, grp_loanaccbankdetail, grp_bankaccdetails, grp_loanOSdetails));
				} while (cursor.moveToNext());
			}
			cursor.close();

			return groupMasters;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			closeDatabase();
		}
		return null;
	}

	public static boolean getSinlgeGroupMaster(GroupMasterSingle groupMasterSingle) {

		try {

			if (GroupMasterSingle.getMasterGroupId() != null) {
				openDatabase();

				Cursor cursor = sqLiteDatabase.query(DataHelper.TABLES.TABLE_GROUP_DETAILS, null,
						GroupMasterColumn.GROUPID + " =?", new String[] { GroupMasterSingle.getMasterGroupId() }, null,
						null, null);

				cursor.moveToFirst();
				String grp_traineruserid = null;
				String grp_groupid = null;
				String grp_groupresponse = null;
				String grp_personalloanos = null;
				String grp_memberloanos = null;
				String grp_grouploanos = null;
				String grp_fixeddepositos = null;

				String grp_agentprofile = null;
				String grp_groupprofile = null;

				String grp_purposeofloan = null;
				String grp_masterminutes = null;
				String grp_individualgrpminutes = null;

				String grp_grouptransid = null;

				String grp_loanaccbankdetails = null;

				String grp_bankaccdetails = null;
				String grp_loanosdetails = null;
				int localId = -1;
				System.out.println("Group id---------------" + GroupMasterSingle.getMasterGroupId());
				if (cursor.getCount() > 0) {

					localId = cursor.getInt(cursor.getColumnIndex(GroupMasterColumn.G_ID));
					grp_traineruserid = cursor.getString(cursor.getColumnIndex(GroupMasterColumn.TRAINERUSERID));

					grp_groupid = cursor.getString(cursor.getColumnIndex(GroupMasterColumn.GROUPID));
					grp_groupresponse = cursor.getString(cursor.getColumnIndex(GroupMasterColumn.GROUPRESPONSE));
					grp_personalloanos = cursor
							.getString(cursor.getColumnIndex(GroupMasterColumn.PERSONALLOANOUTSTANDING));

					grp_memberloanos = cursor.getString(cursor.getColumnIndex(GroupMasterColumn.MEMBERLOANOUTSTANDING));

					grp_grouploanos = cursor.getString(cursor.getColumnIndex(GroupMasterColumn.GROUPLOANOUTSTANDING));

					grp_fixeddepositos = cursor
							.getString(cursor.getColumnIndex(GroupMasterColumn.FIXEDDEPOSITOUTSTANDING));
					grp_agentprofile = cursor.getString(cursor.getColumnIndex(GroupMasterColumn.AGENTPROFILE));
					grp_groupprofile = cursor.getString(cursor.getColumnIndex(GroupMasterColumn.GROUPPROFILE));

					grp_purposeofloan = cursor.getString(cursor.getColumnIndex(GroupMasterColumn.PURPOSEOFLOAN));

					grp_masterminutes = cursor.getString(cursor.getColumnIndex(GroupMasterColumn.MASTEROFMINUTES));

					grp_individualgrpminutes = cursor
							.getString(cursor.getColumnIndex(GroupMasterColumn.INDIVIDUALGROUPMINUTES));
					grp_grouptransid = cursor.getString(cursor.getColumnIndex(GroupMasterColumn.GROUPTRANSID));

					grp_loanaccbankdetails = cursor
							.getString(cursor.getColumnIndex(GroupMasterColumn.LOANACCBANKDETAILS));
					grp_bankaccdetails = cursor
							.getString(cursor.getColumnIndex(GroupMasterColumn.BANKACCOUNTNODETAILS));
					grp_loanosdetails = cursor
							.getString(cursor.getColumnIndex(GroupMasterColumn.LOANACCOUNTOUTSTANDINGDETAILS));

					EShaktiApplication.setLastTransId(grp_grouptransid);

					new GetGroupMemberDetails(localId, grp_traineruserid, grp_groupid, grp_groupresponse,
							grp_personalloanos, grp_memberloanos, grp_grouploanos, grp_fixeddepositos, grp_agentprofile,
							grp_groupprofile, grp_purposeofloan, grp_masterminutes, grp_individualgrpminutes,
							grp_grouptransid, grp_loanaccbankdetails, grp_bankaccdetails, grp_loanosdetails);

					cursor.close();
					closeDatabase();
				} else {
					new GetGroupMemberDetails(localId, grp_traineruserid, grp_groupid, grp_groupresponse,
							grp_personalloanos, grp_memberloanos, grp_grouploanos, grp_fixeddepositos, grp_agentprofile,
							grp_groupprofile, grp_purposeofloan, grp_masterminutes, grp_individualgrpminutes,
							grp_grouptransid, grp_loanaccbankdetails, grp_bankaccdetails, grp_loanosdetails);

					closeDatabase();
				}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return true;
	}

	public static boolean updateGroupDetailsEntry(GroupDetailsUpdate groupDetailsUpdate) {

		try {
			openDatabase();

			// Define the updated row content.
			ContentValues updatedValues = new ContentValues();
			// Assign values for each row.
			updatedValues.put(GroupMasterColumn.GROUPRESPONSE, GroupDetailsUpdate.getGroupResponse());

			sqLiteDatabase.update(DataHelper.TABLES.TABLE_GROUP_DETAILS, updatedValues,
					GroupMasterColumn.GROUPID + "=" + GroupDetailsUpdate.getGroupId(), null);

			return false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			closeDatabase();
		}
		return true;
	}

	public static boolean updateGroupMLOS(GroupMLUpdate groupMLUpdate) {
		try {
			openDatabase();

			// Define the updated row content.
			ContentValues updatedValues = new ContentValues();
			// Assign values for each row.
			if (EShaktiApplication.isPLOS()) {

				updatedValues.put(GroupMasterColumn.GROUPRESPONSE, GroupMLUpdate.getGroupResponse());
				updatedValues.put(GroupMasterColumn.PERSONALLOANOUTSTANDING, GroupMLUpdate.getGroupMLoutstanding());
			} else {
				Log.e("Member Loan Outstanding---------->>>>>", GroupMLUpdate.getGroupMLoutstanding());
				updatedValues.put(GroupMasterColumn.GROUPRESPONSE, GroupMLUpdate.getGroupResponse());
				updatedValues.put(GroupMasterColumn.MEMBERLOANOUTSTANDING, GroupMLUpdate.getGroupMLoutstanding());
			}
			sqLiteDatabase.update(DataHelper.TABLES.TABLE_GROUP_DETAILS, updatedValues,
					GroupMasterColumn.GROUPID + "=" + GroupMLUpdate.getGroupId(), null);

			return false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			closeDatabase();
		}

		return true;

	}

	public static boolean updateGroupLOS(GroupLoanOSUpdate groupLoanOSUpdate) {
		try {
			openDatabase();

			// Define the updated row content.
			ContentValues updatedValues = new ContentValues();
			// Assign values for each row.

			updatedValues.put(GroupMasterColumn.GROUPRESPONSE, GroupLoanOSUpdate.getGroupResponse());
			updatedValues.put(GroupMasterColumn.GROUPLOANOUTSTANDING, GroupLoanOSUpdate.getGroupGLoutstanding());

			sqLiteDatabase.update(DataHelper.TABLES.TABLE_GROUP_DETAILS, updatedValues,
					GroupMasterColumn.GROUPID + "=" + GroupLoanOSUpdate.getGroupId(), null);

			return false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			closeDatabase();
		}

		return true;

	}

	public static boolean updateFixedOS(GroupFOSUpdate groupFOSUpdate) {
		try {
			openDatabase();

			// Define the updated row content.
			ContentValues updatedValues = new ContentValues();
			// Assign values for each row.

			updatedValues.put(GroupMasterColumn.GROUPRESPONSE, GroupFOSUpdate.getGroupResponse());
			updatedValues.put(GroupMasterColumn.FIXEDDEPOSITOUTSTANDING, GroupFOSUpdate.getGroupFDoutstanding());

			sqLiteDatabase.update(DataHelper.TABLES.TABLE_GROUP_DETAILS, updatedValues,
					GroupMasterColumn.GROUPID + "=" + GroupFOSUpdate.getGroupId(), null);

			return false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			closeDatabase();
		}

		return true;

	}

	public static boolean updateGroupLastTransId(TransIdUpdate transIdUpdate) {
		try {
			openDatabase();

			// Define the updated row content.
			ContentValues updatedValues = new ContentValues();
			// Assign values for each row.

			updatedValues.put(GroupMasterColumn.GROUPTRANSID, TransIdUpdate.getTransactionId());

			sqLiteDatabase.update(DataHelper.TABLES.TABLE_GROUP_DETAILS, updatedValues,
					GroupMasterColumn.GROUPID + "=" + TransIdUpdate.getGroupId(), null);

			return false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			closeDatabase();
		}

		return true;

	}

	public static boolean updateLoanAccountData(LoanAccountUpdate accountUpdate) {
		try {
			openDatabase();

			// Define the updated row content.
			ContentValues updatedValues = new ContentValues();
			// Assign values for each row.

			updatedValues.put(GroupMasterColumn.LOANACCBANKDETAILS, LoanAccountUpdate.getTransLoanacc());

			sqLiteDatabase.update(DataHelper.TABLES.TABLE_GROUP_DETAILS, updatedValues,
					GroupMasterColumn.GROUPID + "=" + LoanAccountUpdate.getGroupId(), null);

			return false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			closeDatabase();
		}

		return true;

	}

	public static boolean updateGroupOfflineData(GroupMaster groupMaster) {
		try {
			openDatabase();

			// Define the updated row content.
			ContentValues updatedValues = new ContentValues();
			// Assign values for each row.

			updatedValues.put(GroupMasterColumn.TRAINERUSERID, groupMaster.getTrainerUserId_master());

			updatedValues.put(GroupMasterColumn.GROUPID, groupMaster.getGroupId_master());
			updatedValues.put(GroupMasterColumn.GROUPRESPONSE, groupMaster.getGroupMasterresponse_master());
			updatedValues.put(GroupMasterColumn.PERSONALLOANOUTSTANDING, groupMaster.getPersonalloanOS_master());

			updatedValues.put(GroupMasterColumn.MEMBERLOANOUTSTANDING, groupMaster.getMemberloanOS_master());

			updatedValues.put(GroupMasterColumn.GROUPLOANOUTSTANDING, groupMaster.getGrouploanOS_master());

			updatedValues.put(GroupMasterColumn.FIXEDDEPOSITOUTSTANDING, groupMaster.getFixeddepositOS_master());

			updatedValues.put(GroupMasterColumn.AGENTPROFILE, groupMaster.getAgentprofile());

			updatedValues.put(GroupMasterColumn.GROUPPROFILE, groupMaster.getGroupprofile());

			updatedValues.put(GroupMasterColumn.PURPOSEOFLOAN, groupMaster.getPurposeofloan());

			updatedValues.put(GroupMasterColumn.MASTEROFMINUTES, groupMaster.getMasterminutes());

			updatedValues.put(GroupMasterColumn.INDIVIDUALGROUPMINUTES, groupMaster.getIndividualgroupminutes());
			updatedValues.put(GroupMasterColumn.GROUPTRANSID, groupMaster.getGrouptransId());
			updatedValues.put(GroupMasterColumn.LOANACCBANKDETAILS, groupMaster.getLoanAccBankDetails());
			updatedValues.put(GroupMasterColumn.BANKACCOUNTNODETAILS, groupMaster.getBankAccNoDetails());
			updatedValues.put(GroupMasterColumn.LOANACCOUNTOUTSTANDINGDETAILS, groupMaster.getLoanOSDetails());

			sqLiteDatabase.update(DataHelper.TABLES.TABLE_GROUP_DETAILS, updatedValues,
					GroupMasterColumn.GROUPID + "=" + groupMaster.getGroupId_master(), null);

			return false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			closeDatabase();
		}

		return true;

	}

	public static boolean updateLoanAccountNumber(LoanAccountNoDetailsUpdate accountNOUpdate) {
		try {
			openDatabase();

			// Define the updated row content.
			ContentValues updatedValues = new ContentValues();
			// Assign values for each row.

			updatedValues.put(GroupMasterColumn.LOANACCOUNTOUTSTANDINGDETAILS,
					LoanAccountNoDetailsUpdate.getTransLoanacc());

			sqLiteDatabase.update(DataHelper.TABLES.TABLE_GROUP_DETAILS, updatedValues,
					GroupMasterColumn.GROUPID + "=" + LoanAccountNoDetailsUpdate.getGroupId(), null);

			return false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			closeDatabase();
		}

		return true;

	}

	public static boolean deleteTransaction() {
		try {
			
			
			DataHelper dataHelper = DataHelper.getInstance(EShaktiApplication.getInstance());
			SQLiteDatabase sqLiteDatabase = dataHelper.getWritableDatabase();
			sqLiteDatabase.delete(DataHelper.TABLES.TABLE_GROUP_DETAILS, GroupMasterColumn.GROUPID  + "= ?",
					new String[] {EShaktiApplication.getEditFinancialGroupId()});

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			closeDatabase();
		}
		return true;
	}

	
	
	public static void deleteGroupDetails() {

		try {
			openDatabase();
			sqLiteDatabase.delete(DataHelper.TABLES.TABLE_GROUP_DETAILS, null, null);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			closeDatabase();
		}
	}

}
